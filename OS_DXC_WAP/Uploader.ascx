﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Uploader" Codebehind="Uploader.ascx.cs" %>
<%--<script src="PopUpStyleAndScript/google_jquery.min.js"></script>
<link href="PopUpStyleAndScript/jquery-ui.css" rel="stylesheet" />
<script src="PopUpStyleAndScript/jquery-ui.min.js"></script>
    <script type="text/javascript">
        //To show the validation summary in popup
        function WebForm_OnSubmit() {
            if (typeof (ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) {
                $("#validation_dialog").dialog({
                    title: "Validation Error!",
                    modal: true,
                    resizable: false,
                    buttons: {
                        Close: function () {
                            $(this).dialog('close');
                        }
                    }
                });
                return false;
            }
            return true;
        }
    </script>--%>
<script lang="javascript" type="text/javascript">
    function callfunction() {
        var getUploaderId = '<%=lblUpload.ClientID%>';
        if (document.getElementById(getUploaderId).innerText != null && document.getElementById(getUploaderId).innerText != "") {
            document.getElementById(getUploaderId).innerText = "";
        }
    }
    </script>

<br />
<table style="width: 100%">
    <tr>
        <td>
            <div runat="server" style="width: 100%" onclick="callfunction()">
                <asp:FileUpload ID="uploader" runat="server" Width="100%" />
            </div>
        </td>
        <td>
            <asp:Button CssClass="submitButton" type="button" ID="btnUpload" runat="server" Text="Upload"
                OnClick="btnUpload_Click" />
        </td>
   </tr>
</table>
<br />



<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="uploader"
    ErrorMessage="Please select file" ValidationGroup="FileUpload"></asp:RequiredFieldValidator>--%>


<asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ControlToValidate="uploader"
    ValidationExpression="^[A-Za-z0-9 _]*[A-Za-z0-9 ._:\\]*[\.]?[A-Za-z0-9]+$" ValidationGroup="FileUpload" 
    Display="Dynamic"  SetFocusOnError="True" ErrorMessage="Kindly rename the file only characters and numbers are allowed in file name     يجب ان يكون اسم الملف بالأحرف والأرقام الانجليزية فقط">
</asp:RegularExpressionValidator>



<br />
<asp:Label ID="lblUpload" runat="server" ForeColor="#cc0000" />
                   <%-- <div id="validation_dialog" style="display: none">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="FileUpload" 
    ShowMessageBox="True" ShowSummary="False" 
    />
                        </div>--%>
<br />
<table style="border:0" width="100%">
    <tr>
        <td style="text-align:left">Max individual file upload size is 5 MB. Note that attempting to upload large files over a slow internet connection may result in a timeout.</td>
    </tr>
    <tr>
        <td style="text-align:right;direction:rtl">الحد الأقصى لحجم كل ملف يراد تحميله 5 
            ميجا بيت (MB). لاحظ أن محاولة تحميل ملفات كبيرة عبر اتصال إنترنت بطيء قد يؤدي إلى قطع اتصالك بالموقع</td>
    </tr>
</table>

<br />
<asp:GridView ID="dvFiles" runat="server" CellPadding="4" 
    EnableModelValidation="True" ForeColor="#333333" GridLines="None" Width="100%" AutoGenerateColumns="False" 
                DataKeyNames="UploadedID, SessionID" AllowPaging="false" 
    OnRowCommand="dvFiles_RowCommand" EnableViewState="true" Visible="false" 
    OnRowDeleting="dvFiles_RowDeleting">
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
        <asp:BoundField DataField="UploadedID" HeaderText="UploadedID" 
            InsertVisible="False" ReadOnly="True" SortExpression="UploadedID" 
            Visible="false" ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="FilePath" HeaderText="FilePath" 
            SortExpression="FilePath" Visible="false" ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="DocName" HeaderText="Document Name" 
            SortExpression="DocName" Visible="false" ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="FriendlyDocName" HeaderText="Document Name" 
            SortExpression="FriendlyDocName" Visible="true" 
            ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="LastUpdateTimestamp" HeaderText="Uploaded" 
            SortExpression="LastUpdateTimestamp" Visible="true" 
            ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:ButtonField ButtonType="Button" Text="Delete?" CommandName="Delete" />
<asp:BoundField DataField="VirtualPath" HeaderText="VirtualPath" 
            SortExpression="VirtualPath" Visible="false" ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
    </Columns>
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
</asp:GridView>
    