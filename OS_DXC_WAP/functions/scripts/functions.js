var getMinHeight

function getMinHeightValue() {
    $('body').css({ 'min-height': '' })
    if ($(window).height() > $('body').height()) {
        $('body').css({ 'min-height': $(window).height() })
    }
}

$(window).load(function () {
    getMinHeightValue()
})
$(window).resize(function () {
    getMinHeightValue()
})

$(document).ready(function ($) {

    getMinHeightValue()

    $('img[usemap]').rwdImageMaps();
    $('.toggleNext').click(function () {
        $(this).next().slideToggle()
        $(this).toggleClass('activeLink')
    })
    $('.closeClick').click(function () {
        $(this).parents('.toggleBlock').slideToggle()
        $('.activeLink').removeClass('activeLink')
    })

    $('.blocElem').hover(function () {
        $.data(this, 'timer', setTimeout($.proxy(function () {
            $(this).find('.hiddenSection').slideDown(300)
        }, this), 200));
    }, function () {
        clearTimeout($.data(this, 'timer'));
        $(this).find('.hiddenSection').slideUp(300)
    })

    //$("select").dropkick({
    //    mobile: true
    //});
});

function applySameHeights() {
    if ($(window).width() > 600) {
        $('.sameHeightEntity').each(function (index, element) {
            $(this).css({ 'min-height': 'inherit' })
        });
        $('.sameHeightPads').each(function () {
            var tallest = 0;
            $(this).find('.sameHeightEntity').each(function () {
                //alert($(this).height())
                if (tallest < $(this).height()) { tallest = $(this).height(); }
                //alert(tallest);
            });
            $(this).find('.sameHeightEntity').css({ 'min-height': tallest });
            //$('div.sameHeight').css({'padding-bottom': 15});
        })
    } else {
        $('.sameHeightEntity').css({ 'height': 'auto', 'min-height': 'inherit' })
    }
}
$(document).ready(function () {
    applySameHeights()
    setTimeout(function () {
        applySameHeights()
    }, 6000)
})
$(window).load(function () {
    applySameHeights()
})
$(window).resize(function () {
    setTimeout(function () {
        applySameHeights()
    }, 600)
})


$(window).load(function () {
    $('.quotesSlider').flexslider({
        directionNav: false
    })
    $('.quotesSlider').flexslider({
        directionNav: false
    })
    $('.quotesFourCols').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 25,
        minItems: 1,
        maxItems: 4,
        controlNav: false
    });
})

function valTxtBox(obj, objClass, divErrorMessageID, styleError, errorMessage, e) {
    if (obj.value == "") {
        e.preventDefault();
        $(objClass).addClass(styleError); //if input is nothing //styleError
        if (document.getElementById(divErrorMessageID) != null) {
            document.getElementById(divErrorMessageID).innerHTML = errorMessage;
        }
        return false;
    }
    else {
        $(objClass).removeClass(styleError)//if input is correct
        return true;
    }
}


function DashBoardWhiteFont(obj, objdiv) {
    $(obj).addClass('whiteFont');
    $("#" + objdiv).removeClass('subpagesHeader');
}

function valEmail(obj, objClass, divErrorMessageID, styleError, errorMessageInvalid, e) {
    if (obj.value == "") {
        document.getElementById(divErrorMessageID).innerHTML = "";
        return true
    }
    if (validateEmail(obj.value) == false) {
        e.preventDefault();
        $(objClass).addClass(styleError); //if input is not valid
        document.getElementById(divErrorMessageID).innerHTML = errorMessageInvalid;
        return false;
    }
    else {
        $(objClass).removeClass(styleError) //if input is valid
        document.getElementById(divErrorMessageID).innerHTML = "";
        return true;
    }
}

function validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) {
        return false;
    }
}

function valFileUpload(selected, objClass, divErrorMessageID, styleError, errorMessage, e) {
    if (selected == false) {
        e.preventDefault();
        $(objClass).addClass(styleError); //if input is nothing //styleError
        document.getElementById(divErrorMessageID).innerHTML = errorMessage;
        return false;
    }
    else {
        $(objClass).removeClass(styleError)//if input is correct
        document.getElementById(divErrorMessageID).innerHTML = '';
        return true;
    }
}

function valPassword(password, retypePassword, divErrorMessageID, styleError, errorMessage, e) {
    if (password.value != "" || retypePassword.value != "") {
        if (password.value == retypePassword.value) {
            $(password).removeClass(styleError);//if input is correct
            $(retypePassword).removeClass(styleError)
            document.getElementById(divErrorMessageID).innerHTML = '';
            return true;
        }
        else {
            e.preventDefault();
            $(password).addClass(styleError); //if input is nothing //styleError
            $(retypePassword).addClass(styleError);
            document.getElementById(divErrorMessageID).innerHTML = errorMessage;
            return false;
        }
    }
   
}

function valNumeric(obj, objClass, divErrorMessageID, styleError, errorMessage, e) {
    if (!validateNumeric(obj.value)) {
        e.preventDefault();
        $(objClass).addClass(styleError); //if input is nothing //styleError
        if (document.getElementById(divErrorMessageID) != null) {
            document.getElementById(divErrorMessageID).innerHTML = errorMessage;
        }
        return false;
    }
    else {
        $(objClass).removeClass(styleError)//if input is correct
        return true;
    }
}

function validateNumeric(phone) {
    var intRegex = /^\d+$/;
    return intRegex.test(phone)
}

function valFloat(obj, objClass, divErrorMessageID, styleError, errorMessage, e) {
    if (!validateFloat(obj.value)) {
        e.preventDefault();
        $(objClass).addClass(styleError); //if input is nothing //styleError
        if (document.getElementById(divErrorMessageID) != null) {
            document.getElementById(divErrorMessageID).innerHTML = errorMessage;
        }
        return false;
    }
    else {
        $(objClass).removeClass(styleError)//if input is correct
        return true;
    }
}

function validateFloat(value) {
    var floatRegex = /[-+]?(\d*[.])?\d+/;
    return floatRegex.test(value)
}



function valMobile(obj, objClass, divErrorMessageID, styleError, errorMessage, e) {
    if (!validateMobile(obj.value)) {
        e.preventDefault();
        $(objClass).addClass(styleError); //if input is nothing //styleError
        if (document.getElementById(divErrorMessageID) != null) {
            document.getElementById(divErrorMessageID).innerHTML = errorMessage;
        }
        return false;
    }
    else {
        $(objClass).removeClass(styleError)//if input is correct
        document.getElementById(divErrorMessageID).innerHTML = '';
        return true;
    }
}

function validateMobile(mobile) {
    var intRegex = /^(9665\d\d\d\d\d\d\d\d)$/;
    return intRegex.test(mobile)
}

function valIDNumber(obj, objClass, divErrorMessageID, styleError, errorMessage, e) {
    if (!ValidateIDNumberFormat(obj.value)) {
        e.preventDefault();
        $(objClass).addClass(styleError); //if input is nothing //styleError
        if (document.getElementById(divErrorMessageID) != null) {
            document.getElementById(divErrorMessageID).innerHTML = errorMessage;
        }
        return false;
    }
    else {
        $(objClass).removeClass(styleError)//if input is correct
        document.getElementById(divErrorMessageID).innerHTML = '';
        return true;
    }
}


function ValidateIDNumberFormat(obj) {
    var matches = obj.match(/\b\d{10}\b/g);
    return matches
}

function highlightLeftMenu(link, spanID, spanClass, titleID) {
    $("#" + link).addClass("on");
    $("#" + spanID).addClass(spanClass);
    $("#" + titleID).addClass("onTitle");
}


//$(document).ready(function () {
//    $('.accordionBupa .accoTitle').click(function () {
//        $(this).parent().find($('.accoContent')).slideToggle()
//        $(this).toggleClass('active')
//        $('.accoContent').not($(this).parent().find($('.accoContent'))).slideUp()
//        $('.accordionBupa .accoTitle').not($(this)).removeClass('active')
//    });
//    $(".accordion div.expandListingContent:first").hide();
//    $(".accordion div.expandListingContent:not(:first)").hide();
//    $(".accordion a").click(function () {
//        $(this).next("div").slideToggle("slow")
//        .siblings("div:visible").slideUp("slow");
//        $(this).toggleClass("active");
//        $(this).siblings("a").removeClass("active");
//    });
//});
