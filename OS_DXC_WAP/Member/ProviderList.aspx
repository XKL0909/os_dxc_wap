﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Member_ProviderList" Codebehind="ProviderList.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1.Export, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="highlightText">
       View your provider network list and their location
    </div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server">
    <%--<asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>--%>
<table width="100%">
    <tr>
     <td align="left"><h3>
         <asp:Label ID="lblArea" runat="server" Text=""></asp:Label></h3></td>
    <td align="right"><a href="javascript: window.history.go(-1)">Go Back</a></td>
</tr>
<tr>
<td colspan="2">
  </td>
</tr>
<tr>
<td colspan="2">
    <dx:ASPxGridView ID="gvProviderResult" runat="server" 
        OnHtmlDataCellPrepared="gvProviderResult_HtmlDataCellPrepared" KeyFieldName="Provider Name"
                Width="100%" ondatabound="gvProviderResult_DataBound">
        <SettingsPager PageSize="10">
        </SettingsPager>
                
            </dx:ASPxGridView>




            </td>
</tr>
<tr>
<td colspan="2">
    <uc1:OSNav ID="OSNav1" runat="server" />
            
            </td>
</tr>
</table>
 
</asp:Content>
