using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class MyMedication1 : System.Web.UI.Page
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
    string strMemberName;

 ///  string TabFlag;     Need to create a session variable to hold this value

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        //try
        //{
        ////////        TabFlag = "M";
        strMembershipNo =  Session["MembershipNo"].ToString();
            strMemberName = Session["MemberName"].ToString();
            UpdatePanel2.Visible = false; lblTotalCost.Visible = false; Table2.Visible = false; Button1.Visible = false;
        
            Message1.Text = "No medication records found for this member. ";
        //////////////////////////DisplayMyMedication(strMembershipNo);
        if (!IsPostBack)
        {
            
            
            //the below would be replace with vlaue from session var for the user
            //tabMemberName.HeaderText = response.memberName;
        }

    }

    // function used for Tab System when user requset for dependent ceritifcate
    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
       
       // loadingImage.Visible = true;
       // System.Threading.Thread.Sleep(3000);
       // this.lblTabLoadStatus.Visible = false;// xt = "test completed";
      //  loadingImage.Visible = false;
       //lblMemberName.Text = "Dependent Name";
        UpdatePanel2.Visible = false; lblTotalCost.Visible = false; Table2.Visible = false; Button1.Visible = false;
        Message1.Text = "No medication records found for this member. ";
        
        /////////////////////////DisplayMyMedication(DDLDependentList.SelectedValue);
        
        //TextBox1.Text = "updated also";
        //TextBox3.Visible = false;
        // Code to load data from database or other data source here
    }


    // function used for Tab System to retrieve family list
    private void DisplayDependentListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        ////////////string[,] ResponseDetails;

        ////////////ResponseDetails[0,0] = "test";
        ////////////ResponseDetails[0,1] = "test";
        ////////////ResponseDetails[0,2] = "test";


        ////////string arr(2,4);
        //////// string ar()() = new String({New String() {"", "", ""}, New String() {"", "", ""}};

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table id='tblGrid' name='tblGrid' style='position:relative;'   border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  > <!-- style='behavior:url(TableCtrl.htc);' -->   <thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            DDLDependentList.Items.Clear();

            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

                //DDLDependentList.Items.Add( New  ListItem("Item 1", "1");
                //http://authors.aspalliance.com/stevesmith/articles/dotnetlistbox2.asp


               // DDLDependentList.Items.Add(new ListItem(dtl.memberName + " -- " + dtl.relationship, dtl.membershipNo));

                //DDLDependentList.DataTextField =dtl.memberName + " -- " + dtl.relationship;
                //DDLDependentList.DataValueField = dtl.membershipNo.ToString();
                //DDLDependentList.Items.Insert(.DataBind();

                

                ListItem DepList = new ListItem();
                DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                DDLDependentList.Items.Add(DepList);

            }
        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());

        Message1.Visible = false;
    }


    private void DisplayMedicationListResult(OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
           
            //dtl.proratedOverallAnnualLimit = ;

            double totalCost = 0.00;
            sbResponse.Append("<table id='table1'  border='1px' cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px;  ;'><thead><tr><th>Request Refill</th>   <th>Service Description</th> <th>Service Code</th><th>Supply Period </th><th>Supply Date From</th><th>Suppy Date to</th><th>Quantity</th><th>Estimated Cost</th></tr>	</thead><tbody> ");

            foreach ( OS_DXC_WAP.CaesarWS.MyMedicationDetail_DN  dtl in response.detail)
            {
                ///sbResponse.Append("<tr><td> <input type='checkbox'  />  </td> <td>" + dtl.servDesc + "</td> <td>" + dtl.servCode + "</td> <td>" + dtl.supplyPeriod + "</td> <td>" + String.Format("{0:d}", dtl.supplyFrom) + "</td> <td>" + String.Format("{0:d}", dtl.supplyTo) + "</td> <td>1</td> <td>" + dtl.estimateAmount + "</td></tr>");

                string i = dtl.servCode;
                CheckBox chk = new CheckBox();
                chk.ID = "CheckBox0_" + i;
                TableRow Row = new TableRow();
                TableCell Cellcheck = new TableCell();
                Cellcheck.Width = 20;
                Cellcheck.Controls.Add(chk);
                Row.Cells.Add(Cellcheck);


                TextBox txt0 = new TextBox();
                txt0.Text = dtl.servDesc;
                txt0.ID = "txt1_" + i;
                txt0.ReadOnly = true;
                TableCell Cell = new TableCell();
                Cell.Width = 200;
                Cell.Controls.Add(txt0);
                Row.Cells.Add(Cell);

                DropDownList ddl = new DropDownList();
                ddl.ID = "ddlPeriod3_" + i;            

                ListItem item2 = new ListItem();
                item2.Text = "1";
                item2.Value = "1";
                ddl.Items.Add(item2);

                ListItem item3 = new ListItem();
                item3.Text = "2";
                item3.Value = "2";
                ddl.Items.Add(item3);

                ListItem item4 = new ListItem();
                item4.Text = "3";
                item4.Value = "3";
                ddl.Items.Add(item4);

                TableCell Cell3 = new TableCell();
                Cell3.Width = 50;
                Cell3.Controls.Add(ddl);
                Row.Cells.Add(Cell3);

                TextBox txt1 = new TextBox();
                txt1.Text = String.Format("{0:d}", dtl.supplyFrom);
                txt1.ID = "txt2_" + i;
                txt1.Attributes.Add("onClick", "fPopCalendar('" + txt1.ID + "');");
                //txt1.Attributes.Add("onChange", " txt4_" + i + ".value = Format( DateAdd( 'm' , ddlPeriod3_" + i + ".value ,txt2_" + i + ".value), 'dd-mm-yyyy', '' );"); // + i + ".value =  myDate.setMonth(myDate.getMonth() + 2);");
                TableCell Cell1 = new TableCell();
                Cell1.Width = 50;
                Cell1.Controls.Add(txt1);
                Row.Cells.Add(Cell1);

                TextBox txt2 = new TextBox();
                txt2.ID = "txt4_" + i;
                txt2.Text = String.Format("{0:d}", dtl.supplyTo);
                txt2.Attributes.Add("onMouseover", " txt4_" + i + ".value = Format( DateAdd( 'm' , ddlPeriod3_" + i + ".value ,txt2_" + i + ".value), 'mm-dd-yyyy', '1' );"); // + i + ".value =  myDate.setMonth(myDate.getMonth() + 2);");
                txt2.ReadOnly = true;
                TableCell Cell2 = new TableCell();
                Cell2.Width = 50;
                Cell2.Controls.Add(txt2);
                Row.Cells.Add(Cell2);

                TextBox txt3 = new TextBox();
                txt3.ID = "txt5_" + i;
                txt3.Text = "1";
                txt3.ReadOnly = true;
                TableCell Cell4 = new TableCell();
                Cell4.Width = 20;
                Cell4.Controls.Add(txt3);
                Row.Cells.Add(Cell4);

                TextBox txt4 = new TextBox();
                txt4.ID = "txt6_" + i;
                txt4.Text = dtl.estimateAmount.ToString();
                txt4.ReadOnly = true;
                TableCell Cell5 = new TableCell();
                Cell5.Width = 20;
                Cell5.Controls.Add(txt4);
                Row.Cells.Add(Cell5);
                totalCost += dtl.estimateAmount; 

                TextBox txt5 = new TextBox();
                txt5.ID = "txt7_" + i;
                txt5.Text = dtl.diagCode;
                txt5.Visible = false; 
                TableCell Cell6 = new TableCell();
                Cell6.Width = 50;
                Cell6.Controls.Add(txt5);
                Row.Cells.Add(Cell6);

                TextBox txt6 = new TextBox();
                txt6.ID = "txt8_" + i;
                txt6.Text = dtl.diagDesc;
                txt6.Visible = false;
                TableCell Cell7 = new TableCell();
                Cell7.Width = 50;
                Cell7.Controls.Add(txt6);
                Row.Cells.Add(Cell7);

                Table2.Rows.Add(Row);               

            }
           sbResponse.Append("</tbody></table>");

            XmlDocument doc = new XmlDocument();
             doc.LoadXml(@sbResponse.ToString());

             lblTotalCost.Text = totalCost.ToString();

            CoverageListReport.Visible = true;
          // CoverageListReport.InnerHtml =   sbResponse.ToString();
        }
    }





// function used for Tab System when Tab1 [member tab] is clicked
    protected void btnTab1_Click(object sender, EventArgs args)
    {
  //////      if (TabFlag != "M")
        DisplayMyMedication(strMembershipNo);
        //.Text = "test completed";
    }

    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void btnTab2_Click(object sender, EventArgs args)
    {
        Message1.Text = "";
        Message1.Visible = false;
        //System.Threading.Thread.Sleep(3000);
        //.Text = "test completed";
        
 ///////       if(TabFlag != "D")
        {

       // UpdatePanel2.Visible = true;



        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        request.membershipNo = strMembershipNo;
        //request..membershipNo = 
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

        //request.IqamaNo =  "5523234443";
        //request.TotalNoOfFamily = "3";

        try
        {
            StringBuilder sb = new StringBuilder(200);
            
            response = ws.EnqMbrListInfo(request);
            

            if (response.errorID != "0")
            {
                sb.Append(response.errorMessage).Append("\n");
                lblErrorMessage.Text = sb.ToString();
            }
            else
            {
                DisplayDependentListResult(response);

                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlTextWriter.Formatting = Formatting.Indented;
                xs.Serialize(xmlTextWriter, response);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                this.txtBox_output.Text = XmlizedString;
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text =ex.Message;
        }

        if (DDLDependentList.Items.Count > 0)
        {
  //////          TabFlag = "D";
            DDLDependentList.Visible = true;
            this.lblTabLoadStatus.Visible = false;
            btnRequestDependentCert.Visible = true;
        }
        else
            this.lblTabLoadStatus.Text = "No dependent for this Member Found";


     }
        
   
    }






    private void DisplayMyMedication(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrRequest_DN  request;
        OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
           // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrRequest_DN();
            request.membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); 

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.ReqMyMedicationByMbr(request); // ws.ReqMyMedicationByMbr(request);

            //response = ws.EnqMbrListInfo(request);
           // Cursor.Current = Cursors.Default;


            if (response.detail != null)
            {

                lblMembershipNo.Text = request.membershipNo;
                lblMemberName.Text = request.membershipNo;
               // lblClassName.Text = response.className;
               // lblCustomerName.Text = response.companyName;
                //DDLProviderList.DataTextField = "";// response.provider_Name;
                //DDLDependentList.DataValueField = response.provider_Code;

                int totalProv = response.provider_Code.Length;
                int i;
                for (i = 0; i < totalProv ; i++)
                {
                    ListItem ProList = new ListItem();
                    ProList.Text = response.provider_Name[i].ToString();
                    ProList.Value = response.provider_Code[i].ToString();
                   DDLProviderList.Items.Add(ProList);

                }

                
                //lblCustomerName.Text = response.provider_Code.Length;

                DisplayMedicationListResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                if (tabMemberName.HeaderText == "Member")
                {
                    tabMemberName.HeaderText = strMemberName;
                    lblMemberName.Text = strMemberName;
                }


                
            }
            else
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

}



    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }


    protected void btnUpdateAll_Click(object sender, EventArgs e)
    {

        string name = null;
        string address = null;
        string dept = null;
        string droplist = null;
        string Col5 = null;
        string Col6 = null;

        string str1 = "";

        for (int i = 0; i <= Table2.Rows.Count - 1; i++)
        {

            CheckBox cb = (CheckBox)Table2.Rows[i].Cells[0].Controls[0];
            if (cb.Checked == true)
            {
                if (i != 0)
                {
                    str1 += " - ";
                }

                TextBox t1 = (TextBox)Table2.Rows[i].Cells[1].Controls[0];
                name = t1.Text;
                TextBox t2 = (TextBox)Table2.Rows[i].Cells[3].Controls[0];
                address = t2.Text;
                DropDownList ddl = (DropDownList)Table2.Rows[i].Cells[2].Controls[0];
                droplist = ddl.SelectedValue;
                droplist += "/" + ddl.SelectedItem.Text;
                TextBox t3 = (TextBox)Table2.Rows[i].Cells[4].Controls[0];

                t3.Text = name + address;
                dept = t3.Text;

                TextBox t4 = (TextBox)Table2.Rows[i].Cells[5].Controls[0];
                Col5 = t4.Text;
                TextBox t5 = (TextBox)Table2.Rows[i].Cells[6].Controls[0];
                Col6 = t5.Text;

                str1 += "[" + name + ", (" + droplist + "), " + address + ", " + dept + ", " + Col5 + ", " + Col6 + "]";
            }

        }


        CoverageListReport.Visible = true;
        CoverageListReport.InnerHtml = str1; // sbResponse.ToString();
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        ///sbResponse.Append("<tr><td> <input type='checkbox'  />  </td> <td>" + dtl.servDesc + "</td> <td>" + dtl.servCode + "</td> <td>" + dtl.supplyPeriod + "</td> <td>" + String.Format("{0:d}", dtl.supplyFrom) + "</td> <td>" + String.Format("{0:d}", dtl.supplyTo) + "</td> <td>1</td> <td>" + dtl.estimateAmount + "</td></tr>");
         string name = null;   //serv desc
         string address = null;
         string dept = null;
         string droplist = null;
         string Col5 = null;
         string Col6 = null;
         int TotalQuantity= 0;
         int TotalAmount = 0;

         string str1 = "";

         OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN();
         OS_DXC_WAP.CaesarWS.PharmacyInfoResponse_DN response;

         try
         {

             

             request.Username = WebPublication.CaesarSvcUsername;
             request.Password = WebPublication.CaesarSvcPassword;


             request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
             request.transactionType = "N";
             //request.preauthorisation_ID = null ;
             request.membershipNo = strMembershipNo;
             request.policyNo = strMembershipNo;
             request.memberID_Igama = "";
             request.memberName = strMemberName;//txt_mbr_name.Text.Trim();
             request.memberMobileNo = ""; // txt_mbr_mobile.Text.Trim();
             // request.cardIssueNumber = ""; int.Parse(txt_card_no.Text.Trim());
             request.age = 20; // int.Parse(txt_age.Text.Trim());
             request.gender = "";// txt_gender.Text.Trim();
             request.physician_Name = ""; // txt_phy_name.Text.Trim();
             request.providerCode = "21770"; // txt_pv_code.Text.Trim();
             request.providerFaxNo = ""; // txt_pv_fax.Text.Trim();
             request.patientFileNo = "";// txt_patient_file.Text.Trim();
             request.treatmentType = "O"; // txt_treatmt_type.Text.Trim();
             request.departmentType = "PHA";// txt_dept_type.Text.Trim();
             //  request.dateOfAdmission = DateTime.ParseExact("2/2/2009", "mm-ddd-yyyy", ); // dtp_admis_date.Value;
             // request.lengthOfStay = ""; // int.Parse(txt_len_stay.Text.Trim());
             request.diagnosisCode = "I10"; // txt_diag_code.Text.Trim();
             request.diagnosisDescription = ""; // txt_diag_desc.Text.Trim();    
           


             int[] Detail_ItemNo = new int[Table2.Rows.Count - 1];
             String[] Detail_BenefitHead = new String[Table2.Rows.Count - 1];
             String[] Detail_ServiceCode = new String[Table2.Rows.Count - 1];
             String[] Detail_ServiceDescription = new String[Table2.Rows.Count - 1];
             Int64[] Detail_Quantity = new Int64[Table2.Rows.Count - 1];
             double[] Detail_EstimatedCost = new double[Table2.Rows.Count - 1];
             String[] Detail_SupplyPeriod = new String[Table2.Rows.Count - 1];
             DateTime[] Detail_SupplyDateFrom = new DateTime[Table2.Rows.Count - 1];
             DateTime[] Detail_SupplyDateTo = new DateTime[Table2.Rows.Count - 1];





             for (int i = 1; i <= Table2.Rows.Count - 1; i++)
             {

                 CheckBox cb = (CheckBox)Table2.Rows[i].Cells[0].Controls[0];
                 if (cb.Checked == true)
                 {
                     if (i != 0)
                     {
                         str1 += " - ";
                     }

                     TextBox t1 = (TextBox)Table2.Rows[i].Cells[1].Controls[0];
                     name = t1.Text;
                     TextBox t2 = (TextBox)Table2.Rows[i].Cells[3].Controls[0];
                     address = t2.Text;
                     DropDownList ddl = (DropDownList)Table2.Rows[i].Cells[2].Controls[0];
                     droplist = ddl.SelectedValue;
                     //droplist += "/" + ddl.SelectedItem.Text;
                     TextBox t3 = (TextBox)Table2.Rows[i].Cells[4].Controls[0];

                     /////t3.Text = name + address;
                     dept = t3.Text;

                     TextBox t4 = (TextBox)Table2.Rows[i].Cells[5].Controls[0];
                     Col5 = t4.Text;
                     TextBox t5 = (TextBox)Table2.Rows[i].Cells[6].Controls[0];
                     Col6 = t5.Text;

                     TotalAmount += TotalAmount + int.Parse(Col6);
                     TotalQuantity += TotalQuantity + 1;
                     //str1 += "[" + name + ", (" + droplist + "), " + address + ", " + dept + ", " + Col5 + ", " + Col6 + "]";



                     //if (!isNull(TotalQuantity))
                         Detail_ItemNo.SetValue(TotalQuantity, i);
                     // if (!isNull(dgv_detail["benHead", i].Value))
                     Detail_BenefitHead.SetValue("C1", i);
                   //  if (!isNull(t1.Text))
                         Detail_ServiceCode.SetValue(t1.Text, i);
                   //  if (!isNull(t1.Text))
                         Detail_ServiceDescription.SetValue(t1.Text, i);
                  //   if (!isNull(t4.Text))
                         Detail_Quantity.SetValue(Int64.Parse(t4.Text), i);
                  //   if (!isNull(t5.Text))
                         Detail_EstimatedCost.SetValue(double.Parse(t5.Text), i);
                  //   if (!isNull(ddl.SelectedValue))
                         Detail_SupplyPeriod.SetValue(ddl.SelectedValue, i);
                  //   if (!isNull(t2.Text))
                         Detail_SupplyDateFrom.SetValue(Convert.ToDateTime(t2.Text), i);
                  //   if (!isNull(t3.Text))
                         Detail_SupplyDateTo.SetValue(Convert.ToDateTime(t3.Text), i);



                     // str1 +=   name + ", " + droplist + ", " + address + ", " + dept + ", " + Col5 + ", " + Col6  ;


                 }


             }

             request.quantity = TotalQuantity;
             request.estimatedAmount = TotalAmount;


             request.detail_ItemNo = Detail_ItemNo;
             request.detail_BenefitHead = Detail_BenefitHead;
             request.detail_ServiceCode = Detail_ServiceCode;
             request.detail_ServiceDescription = Detail_ServiceDescription;
             request.detail_Quantity = Detail_Quantity;
             request.detail_EstimatedCost = Detail_EstimatedCost;
             request.detail_SupplyPeriod = Detail_SupplyPeriod;
             request.detail_SupplyDateFrom = Detail_SupplyDateFrom;
             request.detail_SupplyDateTo = Detail_SupplyDateTo;
         }
         catch (Exception ec)
         {
             Message1.Text = ec.StackTrace;
         }

         try
         {
             StringBuilder sb = new StringBuilder(200);
             response = ws.ReqPharInfo(request);

             if (response.errorID[0] != "0")
             {
                 for (int i = 0; i < response.errorMessage.Length; i++)
                     sb.Append(response.errorMessage[i]).Append("\n");

                 Message1.Text = sb.ToString();
             }
             else
             {
                 
                 CoverageListReport.Visible = true;
                 CoverageListReport.InnerHtml = str1; 
                 //String XmlizedString = null;
                 //MemoryStream memoryStream = new MemoryStream();
                 //XmlSerializer xs = new XmlSerializer(typeof(BME_WS.PharmacyInfoResponse_DN));
                 //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                 //xmlTextWriter.Formatting = Formatting.Indented;
                 //xs.Serialize(xmlTextWriter, response);
                 //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                 //XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                 //Message1.Text = XmlizedString;
             }
         }
         catch (Exception ex)
         {

             Message1.Text += "\n" + ex.Message;
         }



        // sbResponse.ToString();
    }


    //private void UpdateReqPharInfo()
    //{
    //    OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN();
    //    OS_DXC_WAP.CaesarWS.PharmacyInfoResponse_DN response;

    //    request.Username = WebPublication.CaesarSvcUsername;
    //    request.Password = WebPublication.CaesarSvcPassword;

    //    try
    //    {
    //            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); 
    //            request.transactionType = "N";
    //            //request.preauthorisation_ID = null ;
    //            request.membershipNo = strMembershipNo;
    //            request.policyNo = strMembershipNo; 
    //            request.memberID_Igama = "";
    //            request.memberName = strMemberName;//txt_mbr_name.Text.Trim();
    //            request.memberMobileNo = ""; // txt_mbr_mobile.Text.Trim();
    //           // request.cardIssueNumber = ""; int.Parse(txt_card_no.Text.Trim());
    //            request.age = 20; // int.Parse(txt_age.Text.Trim());
    //            request.gender = "";// txt_gender.Text.Trim();
    //            request.physician_Name = ""; // txt_phy_name.Text.Trim();
    //            request.providerCode = "21770"; // txt_pv_code.Text.Trim();
    //            request.providerFaxNo = ""; // txt_pv_fax.Text.Trim();
    //            request.patientFileNo = "";// txt_patient_file.Text.Trim();
    //            request.treatmentType = "O"; // txt_treatmt_type.Text.Trim();
    //            request.departmentType = "PHA";// txt_dept_type.Text.Trim();
    //                //  request.dateOfAdmission = DateTime.ParseExact("2/2/2009", "mm-ddd-yyyy", ); // dtp_admis_date.Value;
    //           // request.lengthOfStay = ""; // int.Parse(txt_len_stay.Text.Trim());
    //            request.diagnosisCode = "I10"; // txt_diag_code.Text.Trim();
    //            request.diagnosisDescription = ""; // txt_diag_desc.Text.Trim();    


    //            request.quantity = 240; // totalquantity; // Int64.Parse(txt_quan.Text.Trim());
    //            request.estimatedAmount = 1223.50; // totalAmount; // double.Parse(txt_estim_amt.Text.Trim());

    //        int[] Detail_ItemNo = new int[dgv_detail.RowCount - 1];
    //        //String[] Detail_BenefitHead = new String[dgv_detail.RowCount - 1];
    //        //String[] Detail_ServiceCode = new String[dgv_detail.RowCount - 1];
    //        //String[] Detail_ServiceDescription = new String[dgv_detail.RowCount - 1];
    //        //Int64[] Detail_Quantity = new Int64[dgv_detail.RowCount - 1];
    //        //double[] Detail_EstimatedCost = new double[dgv_detail.RowCount - 1];
    //        //String[] Detail_SupplyPeriod = new String[dgv_detail.RowCount - 1];
    //        //DateTime[] Detail_SupplyDateFrom = new DateTime[dgv_detail.RowCount - 1];
    //        //DateTime[] Detail_SupplyDateTo = new DateTime[dgv_detail.RowCount - 1];



    //        //for (int i = 0; i < dgv_detail.RowCount - 1; i++)
    //        //{

    //        //    if (!isNull(dgv_detail["itemNo", i].Value))
    //        //        Detail_ItemNo.SetValue(int.Parse(dgv_detail["itemNo", i].Value.ToString()), i);
    //        //    if (!isNull(dgv_detail["benHead", i].Value))
    //        //        Detail_BenefitHead.SetValue(dgv_detail["benHead", i].Value.ToString(), i);
    //        //    if (!isNull(dgv_detail["serviceCode", i].Value))
    //        //        Detail_ServiceCode.SetValue(dgv_detail["serviceCode", i].Value.ToString(), i);
    //        //    if (!isNull(dgv_detail["serviceDesc", i].Value))
    //        //        Detail_ServiceDescription.SetValue(dgv_detail["serviceDesc", i].Value.ToString(), i);
    //        //    if (!isNull(dgv_detail["quantity", i].Value))
    //        //        Detail_Quantity.SetValue(Int64.Parse(dgv_detail["quantity", i].Value.ToString()), i);
    //        //    if (!isNull(dgv_detail["estimatedCost", i].Value))
    //        //        Detail_EstimatedCost.SetValue(double.Parse(dgv_detail["estimatedCost", i].Value.ToString()), i);
    //        //    if (!isNull(dgv_detail["supplyPeriod", i].Value))
    //        //        Detail_SupplyPeriod.SetValue(dgv_detail["supplyPeriod", i].Value.ToString(), i);
    //        //    if (!isNull(dgv_detail["supplyDateFrom", i].Value))
    //        //        Detail_SupplyDateFrom.SetValue(DateTime.ParseExact(dgv_detail["supplyDateFrom", i].Value.ToString(), "yyyy-MM-dd", null), i);
    //        //    if (!isNull(dgv_detail["supplyDateTo", i].Value))
    //        //        Detail_SupplyDateTo.SetValue(DateTime.ParseExact(dgv_detail["supplyDateTo", i].Value.ToString(), "yyyy-MM-dd", null), i);


    //        //}


    //        //request.detail_ItemNo = Detail_ItemNo;
    //        //request.detail_BenefitHead = Detail_BenefitHead;
    //        //request.detail_ServiceCode = Detail_ServiceCode;
    //        //request.detail_ServiceDescription = Detail_ServiceDescription;
    //        //request.detail_Quantity = Detail_Quantity;
    //        //request.detail_EstimatedCost = Detail_EstimatedCost;
    //        //request.detail_SupplyPeriod = Detail_SupplyPeriod;
    //        //request.detail_SupplyDateFrom = Detail_SupplyDateFrom;
    //        //request.detail_SupplyDateTo = Detail_SupplyDateTo;

    //    }
    //    catch (Exception ec)
    //    {
    //        Message1.Text = ec.StackTrace;
    //    }
    //    try
    //    {
    //        StringBuilder sb = new StringBuilder(200);
    //        response = ws.ReqPharInfo(request);

    //        if (response.errorID[0] != "0")
    //        {
    //            for (int i = 0; i < response.errorMessage.Length; i++)
    //                sb.Append(response.errorMessage[i]).Append("\n");

    //            Message1.Text = sb.ToString();
    //        }
    //        else
    //        {
    //            //String XmlizedString = null;
    //            //MemoryStream memoryStream = new MemoryStream();
    //            //XmlSerializer xs = new XmlSerializer(typeof(BME_WS.PharmacyInfoResponse_DN));
    //            //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
    //            //xmlTextWriter.Formatting = Formatting.Indented;
    //            //xs.Serialize(xmlTextWriter, response);
    //            //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
    //            //XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
    //            //Message1.Text = XmlizedString;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
            
    //        Message1.Text += "\n" + ex.Message;
    //    }

    //}



}