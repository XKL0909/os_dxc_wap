﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Templates/Mobile.Master"   EnableEventValidation="false"
    Inherits="MyPreauthHistoryMobileV2" Codebehind="MyPreauthHistoryMobileV2.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">


    

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

    </style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
   
     <table width="100%">
        <tr valign="middle"><td >
        <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Pre-Authorisation History" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
        <td style="text-align: right">&nbsp;</td></tr>
    
        <tr><td></td><td></td></tr>
        </table>
      
        <table width=100% ><tr align="right"><td align=left>
            <label ID="Label1" ><font size=4></font></label>  </td>
            <td><a href="#" onclick="window.print()">
                &nbsp;</a></td></tr></table>
            
            

        <asp:ScriptManager ID="ScriptManager1" runat="server" />

   <table width="100%">
                                    <tr>
                                        <td> <div class="styled-select"><asp:DropDownList   ID="DDLDependentList" runat="server" width="80px">
                            </asp:DropDownList></div></td>
                                        <td align="left"> <asp:Button ID="btnRequestDependentCert" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details" /></td>
                                    </tr>
                                </table>
 
  <div class="noPrint" >   
        
       <table style="width:100%">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
            <div id="DivProgress" 
        > 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
            </td>
        </tr>
       </table>
</div>   
    

        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
             <br /> 
             <table>
                 <tr>
                     <td class="contentBack">
                         <table>
                             <tr>
                                 <td class="contentTableLabel">
                                     <asp:Label ID="lblMembership_No" runat="server" Text="Membership No :"></asp:Label>
                                 </td>
                                 <td contentTableWhiteCell>
                                     <asp:Label ID="lblMembershipNo" runat="server" Text=""></asp:Label>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="contentTableLabel">
                                     <asp:Label ID="lblMember_Name" runat="server" Text="Member Name :"></asp:Label>
                                 </td>
                                 <td contentTableWhiteCell>
                                     <asp:Label ID="lblMemberName" runat="server"></asp:Label>
                                 </td>
                             </tr>
                         </table>
                     </td>
                 </tr>
             </table>      
       <div id="CoverageListReport"   visible=false style=" font-family:Arial; vertical-align:text-top ; height:auto; width:90%; overflow:visible;" runat="server" >
       <img src="file:///C:\Downloads\imagemap-1291\AJAX_ImageMap\animated_loading.gif" />
       </div>

            
            
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
       
           

        
   
           
</asp:Content>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
  
</asp:Content>