﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class PreauthDetails1 : System.Web.UI.Page
{
    string PreauthID;
    string ProviderCode;
    string EpisodeNo;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        //  ProviderCode = "20087";//
        ProviderCode =  Request.QueryString["PC"];
       // strProviderFax = Session["ProviderFax"].ToString();

        PreauthID = Request.QueryString["PID"];
      string  OptionType = Request.QueryString["Type"];
            EpisodeNo = Request.QueryString["EN"];


        if (OptionType == "Cancel")
        {
            EpisodeNo = Request.QueryString["EN"];
            CancelPreAuth(PreauthID, EpisodeNo);
            return;
        }



        if (!Page.IsPostBack)
        {
            DisplayPreAuthHist();
        }

      
    }


    private void DisplayPreAuthHist()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN request;
        OS_DXC_WAP.CaesarWS.RequestPreauthByIDResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN();
       //     request.episodeNo =  long.Parse(EpisodeNo);
            request.preAuthorisationID =  long.Parse(PreauthID);
            request.providerCode =  ProviderCode;
               // .membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
//            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));


            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.RequestPreauthByID(request);// ws.RequestPreauthByID(request);

            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.status.Trim() == "0")
            {

                if (response.preauthorisationStatus.Trim() == "WIP" )
                {
                    UpdatePanel2.Visible = false;
                    btnCancelRequest.Visible = false;
                    btnRenewRequest.Visible = false;
                    btnFollowUpRequest.Visible = false;
                    txtRenew.Visible = false;
                    Label1.Visible = false;
                    if ( Request.QueryString["RType"] != null && Request.QueryString["RType"].ToString() == "NR")
                        Message1.Text = "Reference No: <b>" + response.preAuthorizationID.ToString() + "</b>  <a href='CheckNewReply.aspx'>Check Reply</a>";
                    else
                        Message1.Text = "This preauthorisation is currently being processed by Bupa. Please check later.";// +response.preAuthorizationID.ToString() + "</b>  <a href='CheckNewReply.aspx'>Check Reply</a>";



                }
                else
                {
                    //lblMembershipNo.Text = request.membershipNo;
                    //lblMemberName.Text = request.membershipNo;
                    lblClass.Text = response.memberClass;
                    lbAge.Text = response.age.ToString();
                    lblDateOfVisit.Text = String.Format("{0:d}", response.dateOfAdmission);
                    lblDept.Text = response.department;
                    lblExpiryDate.Text = String.Format("{0:d}", response.expiryDate); //response.expiryDate.ToString();
                    lblIDCard.Text = response.IDCardNo;
                    lblInsurance_Co.Text = response.insurance_Company;
                    lblInsuredName.Text = response.memberName;
                    lblPatientFileNo.Text = response.patientFileNo;
                    lblDateTime.Text = response.date_Time.ToString();
                    lblPolicyHolder.Text = response.policyHolder;
                    lblPolicyNo.Text = response.membershipNo;
                    //need to check///lblPolicyNo.Text = response.
                    lblProviderFaxNo.Text = response.providerFaxNo;
                    lblProviderName.Text = response.provider_Name;
                    lblDateTime.Text = response.date_Time.ToString();
                    lblSex.Text = response.gender;
                    ////// need to check       lblApprovalNo.Text = response.
                    lblApprovalValidity.Text = String.Format("{0:d}", response.app_Validity); //response.app_Validity.ToString();
                    lblInsuranceOfficer.Text = response.insurance_Officer;
                    lblApprovalNo.Text = PreauthID;

                    if (response.chronic_Ind.Trim() != "N" || response.department == "DEN" || response.department == "OER")
                        btnFollowUpRequest.Visible = false;

                    lblComments.Text = response.comments;

                    lblPreauthorisationStatus.Text = response.preauthorisationStatus;
                    lblRoomType.Text = response.roomType;

                    StringBuilder sbResponse = new StringBuilder(2200);
                    if (response.chronic_Ind.Trim() == "Y" || response.supply_Period.Length > 0)
                        sbResponse.Append("<table   style='font-size:13px;width:100% ;' border=0><tr ><td width='15px'><b>Service Code<br>رمز الخدمة</td><td width='20px'><b>Supply Period<br>مدة الخدمة</td><td width='25px'><b>Supply From<br>من تاريخ</td><td width='25px'><b>Supply To<br>الى تاريخ</td><td width='20%' valign=top><b>Service Description<br>وصف الخدمة</td><td width='35%' valign=top><b>Notes<br>ملاحظات</b></td></tr>");
                    else
                        sbResponse.Append("<table  style='font-size:13px;width:100% ;' border=0 ><tr><td width='15px'><b>Service Code</td><td valign=top><b>Service Description</td><td valign=top><b>Notes</b></td></tr>");

                    for (int i = 0; i < response.service_Code.Length; i++)
                    {
                        sbResponse.Append("<tr>");
                        if (response.chronic_Ind.Trim() == "Y" || response.supply_Period.Length > 0)
                        {
                            sbResponse.Append("<td>");
                            if (response.service_Code[i] != null) sbResponse.Append(response.service_Code[i]);
                            sbResponse.Append("</td><td>");
                            if (response.supply_Period[i] != null) sbResponse.Append(response.supply_Period[i] + " Month/s");
                            sbResponse.Append("</td><td>");
                            if (response.supply_From[i] != null) sbResponse.Append(DateTime.ParseExact(String.Format("{0:d}", response.supply_From[i].Replace(" 00:00:00.0", "")), "yyyy-MM-dd", null).ToShortDateString());
                            sbResponse.Append("</td><td>");
                            if (response.supply_To[i] != null) sbResponse.Append(DateTime.ParseExact(String.Format("{0:d}", response.supply_To[i].Replace(" 00:00:00.0", "")), "yyyy-MM-dd", null).ToShortDateString());
                            sbResponse.Append("</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td></tr>");
                        }

                        else
                            sbResponse.Append("<td>" + response.service_Code[i] + "</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td></tr>");  // response.service_Code
                    }
                    ServiceListReport.Visible = true;
                    ServiceListReport.InnerHtml = sbResponse.ToString();

                }
                //lblNotes.Text = response.notes.ToString();
                //lblServiceCode.Text = response.service_Code.Length;
                //lblServiceDescription.Text = response.service_Desc.ToString();




                //response.
                // lblClassName.Text = response.className;
                // lblCustomerName.Text = response.companyName;

                // DisplayPreauthHistoryResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                //if (tabMemberName.HeaderText == "Member")
                //    tabMemberName.HeaderText = request.membershipNo;



            }
            else
            {
                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                txtRenew.Visible = false;
                Message1.Text = response.errorMessage;// "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }


    private void CancelPreAuth(string Preauth, string Episode)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
           OS_DXC_WAP.CaesarWS.SendPreauthCancellationRequest_DN request;
        OS_DXC_WAP.CaesarWS.SendPreauthCancellationResponse_DN response;

        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.SendPreauthCancellationRequest_DN();
            
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.preAuthorisationID = Int64.Parse(Preauth.Trim());
            request.episodeNo = Int64.Parse(Episode.Trim());
                      
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.CancelInfo(request);

            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;
            StringBuilder sbResponse = new StringBuilder(200);
            StringBuilder sb1 = new StringBuilder(200);

            if (response.status == "0")
            {
                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                txtRenew.Visible = false;

                Message1.Text = "Your request for Cancellation will be processed.";// response.errorMessage.Trim();
                
            }
            else
            {
                
                Message1.Text = response.errorMessage;// sb1.ToString(); // "Invalid information reported. Please type in the exact information.";

                

                //Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }





    //private void DisplayPreauthHistoryResult(OS_DXC_WAP.CaesarWS.EnqPreHisInfoResponse_DN response)
    //{
    //    ListItem itm;
    //    StringBuilder msge;


    //    string str1;
    //    string str2;
    //    str2 = "";
    //    str1 = "";
    //    StringBuilder sbResponse = new StringBuilder(2200);

    //    if (response.status == "0")
    //    {

    //        //dtl.proratedOverallAnnualLimit = ;


    //        sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px;  ;'><thead><tr><th>Pre-auth No</th>   <th>Episode No</th><th>Provider Name</th>   <th>Received Date/Time</th><th>Response Date/Time</th>   <th>Pre-auth Status</th></tr>	</thead><tbody> ");

    //        foreach (OS_DXC_WAP.CaesarWS.PreHisDetail_DN dtl in response.detail)
    //        {
    //            sbResponse.Append("<a href='PreauthDetails.aspx?PID=" + dtl.preAuthNo + "&PC=" + dtl.providerCode + "&EN=" + dtl.episodeID + "' style='cursor:hand;'><tr ><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString() + "</td><td>" + dtl.preAuthStatus + "</td></tr></a>");

    //            //if (dtl.detail_ProratedLimit != -1)
    //            //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
    //            //else
    //            //    sbResponse.Append("<td>Not Covered</td></tr>");


    //        }
    //        sbResponse.Append("</table>");
    //        CoverageListReport.Visible = true;
    //        CoverageListReport.InnerHtml = sbResponse.ToString();


    //    }
    //    else
    //    {
    //        //msge = new StringBuilder(100);
    //        //foreach (String s in response.errorMessage)
    //        //{
    //        //    msge.Append(s).Append("/n");
    //        //}
    //        //Message1.Text = msge.ToString();
    //    }

    //    //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    //}


    protected void lnkCancelRequest_Click(object sender, EventArgs e)
    {

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        //OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN request;
        //OS_DXC_WAP.CaesarWS.RequestPreauthByIDResponse_DN response;

        OS_DXC_WAP.CaesarWS.RequestCancellationStatusRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestCancellationStatusRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestCancellationStatusResponse_DN response;

        request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

//        request.transactionID = Int64.Parse(TransactionID.Trim());
        request.preAuthorisationID = Int64.Parse( PreauthID.Trim());
        request.providerCode = ProviderCode;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        try
        {

            StringBuilder sb = new StringBuilder(200);
            response = ws.RequestCancelStatus(request);
            if (response.status.Trim() != "0")
            {
                //if (response.errorID.Trim() != "0")
                //{
                    UpdatePanel2.Visible = false;
                    btnCancelRequest.Visible = false;
                    btnRenewRequest.Visible = false;
                   btnFollowUpRequest.Visible = false;
                    txtRenew.Visible = false;
                    Message1.Text = response.errorMessage.Trim();
                    //Message1.Text =  response.errorMessage;
                //}
                //else
                //    return "Nothing return";
            }
            else
            {

                StringBuilder sbResponse = new StringBuilder(2200);

                sbResponse.Append("<table border=1 width=100%><tr><td width=20%><b>Preauth ID</td><td><b>Episode Number</td><td><b>Episode Status</b></td><td><b>Service Description</b></td><td><b>Service Status</b></td><td><b>Approval Date</b></td><td><b>Member Name</b></td><td><b>Patient File No</b></td></tr>");

                for (int i = 0; i < response.episodeNo.Length; i++)
                {
                    var queryString = "PID=" + response.preauthorisationID[i].ToString() +  "&EN=" + response.episodeNo[i].ToString() + "&Type=Cancel";
                    queryString = Cryption.Encrypt(queryString);
                    sbResponse.Append("<a href='PreauthDetails.aspx?val=" + queryString +"'  style='cursor:hand;'><tr><td>" + response.preauthorisationID[i].ToString() + "</td><td>" + response.episodeNo[i].ToString() + "</td><td>" + response.episodeStatus[i] + "</td><td>" + response.serviceDescription[i] + "</td><td>" + response.lineItemStatus[i] + "</td><td>" + String.Format("{0:d}", response.approvalDate[i]) + "</td><td>" + response.memberName[i] + "</td><td>" + response.patientFileNumber[i] + "</td></tr><a/>");  // response.service_Code
                    ////sbResponse.Append("<a href='PreauthDetails.aspx?PID=" + response.preauthorisationID[i].ToString() + "&EN=" + response.episodeNo[i].ToString() + "&Type=Cancel'  style='cursor:hand;'><tr><td>" + response.preauthorisationID[i].ToString() + "</td><td>" + response.episodeNo[i].ToString() + "</td><td>" + response.episodeStatus[i] + "</td><td>" + response.serviceDescription[i] + "</td><td>" + response.lineItemStatus[i] + "</td><td>" +  String.Format("{0:d}", response.approvalDate[i]) + "</td><td>" + response.memberName[i] + "</td><td>" + response.patientFileNumber[i] + "</td></tr><a/>");  // response.service_Code
                }
                CancelList.Visible = true;
                CancelList.InnerHtml = sbResponse.ToString();



                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                    txtRenew.Visible = false;
               // Message1.Text = "Request Successfully send for Cancellation";
            }
        }
        catch (Exception ex)
        {
            Message1.Text= ex.Message;
        }


    }


    //private void ReqCancelList(string MemberNo, string SearchOption)
    //{
    //    ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
    //    //}
    //    //catch (Exception ex)
    //    //{
    //    //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
    //    //}


    //    OS_DXC_WAP.CaesarWS.RequestExistingPreauthStatusRequest_DN request;
    //    OS_DXC_WAP.CaesarWS.RequestExistingPreauthStatusResponse_DN response;
    //    //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
    //    //////{
    //    //////    Message1.Text = "Please insert all the fields before using the web service";
    //    //////    return;
    //    //////}
    //    try
    //    {
    //        // Cursor.Current = Cursors.WaitCursor;
    //        response = null;
    //        request = new OS_DXC_WAP.CaesarWS.RequestExistingPreauthStatusRequest_DN();
           
    //        //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
    //        //            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
    //        request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
    //        request.preAuthorisationID = Int64.Parse(PreAuthorisationID.Trim());
            
    //        request.provCode = strProviderID;
    //        //request.IqamaNo =  "5523234443";
    //        //request.TotalNoOfFamily = "3";
    //        request.Username = WebPublication.CaesarSvcUsername;
    //        request.Password = WebPublication.CaesarSvcPassword;

    //        response = ws.RequestExistPreauthStatus(request);

    //        //response = ws.EnqMbrListInfo(request);
    //        // Cursor.Current = Cursors.Default;
    //        StringBuilder sb1 = new StringBuilder(200);

    //        if (response.detail != null)
    //        {
    //           // response.

    //            //  lblMembershipNo.Text = request.membershipNo;
    //            //  lblMemberName.Text = strMemberName;
    //            // lblClassName.Text = response.className;
    //            // lblCustomerName.Text = response.companyName;

    //            DisplaySchemeList(response);
    //            //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
    //            //dataGridView1.DataBindingComplete = true;
    //            Message1.Text = "";
    //        }
    //        else
    //        {
    //            for (int i = 0; i < response.errorID.Length; i++)
    //                sb1.Append(response.errorMessage[i]).Append("<br/> ");

    //            Message1.Text = sb1.ToString(); // "Invalid information reported. Please type in the exact information.";
    //            CoverageListReport.Visible = false;
    //            //    if (response.ReferenceNo != null)
    //            Message1.Text += "."; // Branch changed successfully. Your reference number is " + response.ReferenceNo.ToString() + ".";


    //            //Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Message1.Text = ex.Message;
    //    }

    //}





    protected void btnRenewRequest_Click(object sender, EventArgs e)
    {

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.SendRenewInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.SendRenewInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.SendRenewInfoResponse_DN response;


        request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

        //        request.transactionID = Int64.Parse(TransactionID.Trim());
        request.preAuthorisationID = Int64.Parse(PreauthID.Trim());
        //request.providerCode = "20078"; // ProviderCode;
        request.treatmentDate = txtRenewalDate.CalendarDate; // DateTime.Parse("12/12/2009"); // TreatmentDate;
        

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        try
        {

            StringBuilder sb = new StringBuilder(200);
            response = ws.SendRenewInfo(request);
            if (response.status != "0")
            {
                //if (response.errorID != "0")
                //{
                    UpdatePanel2.Visible = false;
                    btnCancelRequest.Visible = false;
                    btnFollowUpRequest.Visible = false;
                    btnRenewRequest.Visible = false;
                    txtRenew.Visible = false;
                    Message1.Text = response.errorMessage;
              //  }
                //else
                //    return "Nothing return";
            }
            else
            {
                Message1.Text = "Request Successfully send for Renewal";
                lblApprovalNo.Font.Bold = true;
                lbl_approvalNo.Text = "New Approval No:";
                lblDateOfVisit.Text = String.Format("{0:d}", response.dateOfAdmission);
                lblApprovalValidity.Text = String.Format("{0:d}", response.expiryDate);
                lblDateTime.Text = response.date_Time.ToString();
                lblPreauthorisationStatus.Text = response.preauthorisationStatus;

                if (response.preauthorisationStatus.Trim() == "Rejected")
                {
                    ServiceListReport.InnerHtml = "";
                    lblComments.Text = response.comments;
                }


                lblApprovalNo.Text = response.preAuthorizationID.ToString();// request.preAuthorisationID.ToString();
                
               // lblDateOfVisit.Font.Bold = true; //.ForeColor = System.Drawing.Color.Teal;// "teal";
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message + ".<br>Please try again later";
        }

    }
    protected void btnFollowUpRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("Preauth15.aspx?PID=" + PreauthID);// response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1");

    }
    protected void txtRenew_ServerClick(object sender, EventArgs e)
    {

    }
}
