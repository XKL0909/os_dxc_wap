﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Member_reimbursementclaims" Codebehind="reimbursementclaims.aspx.cs" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register src="../Uploader.ascx" tagname="Uploader" tagprefix="uc1" %>

<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">

<script type="text/javascript">
    function child_openurl(url) {

        popupWindow = window.open(url, "_blank", 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');

    }

</script>
    <table style="width:500px">
        <tr>
            <td>
                <h2>Submit reimbursement claims</h2>
            </td>
        </tr>
        <tr>
            <td>
                <a href="../Docs/Bupa-Claim-Form-1Feb2011.pdf">Download Claim Form</a></td>
        </tr>
        <tr>
            <td>
                In submitting the reimbursement claim, please ensure claim form is completely 
                filled with accurate information. You may also submit supporting document like 
                Invoices, medical report, lab results and others.</td>
        </tr>
	        <tr><td>
            <p style="text-indent: -18.0pt; mso-list: l0 level1 lfo1; text-autospace: none;  margin-left: 36.0pt; margin-right: 0cm; margin-top: 0cm; margin-bottom: .0001pt;">
                <![if !supportLists]>
                <span style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol"><span style="mso-list:Ignore">·<span 
                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span></span><![endif]>Member will need to submit the claim within 180 
                days from the treatment date.<o:p></o:p></p><br>
            <p style="text-indent: -18.0pt; mso-list: l0 level1 lfo1; text-autospace: none;  margin-left: 36.0pt; margin-right: 0cm; margin-top: 0cm; margin-bottom: .0001pt;">
                <![if !supportLists]>
                <span style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol"><span style="mso-list:Ignore">·<span 
                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span></span><![endif]>Filling and uploading missing documents should be 
                within 90 days of the claim submission.<o:p></o:p></p>
        </td></tr>
<tr>
            <td>
              <br /><b>Please note that you will need to submit the original claims, invoices, and supporting documents through the following ways:</b><br />
            </td>
        </tr>
         <tr>
         <td>
             <p class="MsoListParagraphCxSpFirst" style="margin-left:54.0pt;mso-add-space:
auto;text-indent:-18.0pt;mso-list:l0 level1 lfo1">
                 <![if !supportLists]>
                 <span style="mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">
                 <span style="mso-list:Ignore">1.<span 
                     style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 </span></span></span><![endif]>Visit Bupa Arabia Head Office and submit the 
                 claims yourself.<o:p></o:p></p>
             <p class="MsoListParagraphCxSpLast" style="margin-left:54.0pt;mso-add-space:auto;
text-indent:-18.0pt;mso-list:l0 level1 lfo1">
                 <![if !supportLists]>
                 <span style="mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">
                 <span style="mso-list:Ignore">2.<span 
                     style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 </span></span></span><![endif]>Send the claims via the courier service DHL at 
                 Bupa Arabia Head Office, <br />Al Rawdah Street, Al Khalidiyah District, Jeddah, Saudi 
                 Arabia.<o:p></o:p></p>
         </td>
         </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <table>

                    <tr>
                        <td colspan="3" runat="server" id="Uploaderlink" visible="false" align="center">
                            <%--<a href="#" onclick="child_openurl('../SimpleUpload.aspx?UploadCategory=<%=_UploadCategory %>&UserID=<%=_Username%>&RequestID=<%=_InitialRequestID %>&t=mem');">Please Click here to upload Documents</a>--%>

                             <%
                                _urlValues = "UploadCategory=" + _UploadCategory + "&UserID="+_Username+"&RequestID="+_InitialRequestID+"&t=mem";
                                _urlValues = Cryption.Encrypt(_urlValues);

                             %>

                            <a href="#" onclick="child_openurl('../UploadRevamp.aspx?val=<%=_urlValues%>');">Please Click here to upload Documents</a>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="3" style="text-align: center">
                        
                <asp:Label ID="lblMsg" runat="server" Text="" 
                    style="color: #FF0000; font-weight: 700"></asp:Label>
           
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <uc2:OSNav ID="OSNav1" runat="server" />
                        </td>
                    </tr>
                </table></td>
        </tr>
        </table>
</asp:Content>

