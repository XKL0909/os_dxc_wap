﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Bupa.OSWeb.Business;
using DevExpress.Web.ASPxGridView;
using Bupa.Core.Utilities.Configuration;

public partial class Member_MyNetwork : System.Web.UI.Page
{
    public string membership = "";
    string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SabicContractNo");
    string strConfigSecondSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SecondSabicContractNo");

    protected void Page_Load(object sender, EventArgs e)
    {
        Member m = null;
        MemberHelper mh;

        if (Request.QueryString.Count > 0)
        {
            var queryString = Cryption.Decrypt(Request.QueryString["val"]);

            if (!string.IsNullOrEmpty(queryString))
            {
                membership = queryString;
            }
        }



        if (Session["MembershipNo"] != null)
        {
            ////membership = Session["MembershipNo"].ToString();
            // Fetch the member's details to check whether SABIC or not. and if it is sabic to hide cheque option
            mh = new MemberHelper(membership, "");
            m = mh.GetMemberDetails();

            if (m != null && (strConfigSabicContractNo == m.ContractNumber || strConfigSecondSabicContractNo == m.ContractNumber))
            {
                ImageMap1.ImageUrl ="~//images//SabicMap.png";
            }
        }

       
    }

    protected void ImageMap1_Click1(object sender, ImageMapEventArgs e)
    {
        var queryString = string.Empty;
        if (!string.IsNullOrEmpty(membership))
        {
            Session["MembershipNo"] = membership;           
            Session["loc"] = e.PostBackValue;
            queryString = Cryption.Encrypt("loc=" + e.PostBackValue + "&mem=" + membership);
            Response.Redirect("Providerlist.aspx?val="+ queryString);
            ////Response.Redirect("Providerlist.aspx?loc=" + e.PostBackValue + "&mem=" +membership );
        }
        else{
            Session["loc"] = e.PostBackValue;
            //// Response.Redirect("Providerlist.aspx?loc=" + e.PostBackValue);
            queryString = Cryption.Encrypt("loc=" + e.PostBackValue);
            Response.Redirect("Providerlist.aspx?val="+queryString);
        }



    }
}