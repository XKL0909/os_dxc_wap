using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text;
using System.Net;
using System.Diagnostics;

public partial class MembershipCertMobile : System.Web.UI.Page 
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
 ///  string TabFlag;     Need to create a session variable to hold this value

    protected void Page_Load(object sender, EventArgs e)
    {
        
        strMembershipNo =  Request["MembershipNo"];
        DisplayMemberCertResult(strMembershipNo);
        if(!IsPostBack)
         DependentList();

    }

    // function used for Tab System when user requset for dependent ceritifcate
    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
       
       // loadingImage.Visible = true;
       // System.Threading.Thread.Sleep(3000);
       // this.lblTabLoadStatus.Visible = false;// xt = "test completed";
      //  loadingImage.Visible = false;
       //lblMemberName.Text = "Dependent Name";

       DisplayMemberCertResult(DDLDependentList.SelectedValue);
      
    }


    // function used for Tab System to retrieve family list
    private void DisplayMemberListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        ////////////string[,] ResponseDetails;

        ////////////ResponseDetails[0,0] = "test";
        ////////////ResponseDetails[0,1] = "test";
        ////////////ResponseDetails[0,2] = "test";


        ////////string arr(2,4);
        //////// string ar()() = new String({New String() {"", "", ""}, New String() {"", "", ""}};

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            DDLDependentList.Items.Clear();

            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

                //DDLDependentList.Items.Add( New  ListItem("Item 1", "1");
                //http://authors.aspalliance.com/stevesmith/articles/dotnetlistbox2.asp


               // DDLDependentList.Items.Add(new ListItem(dtl.memberName + " -- " + dtl.relationship, dtl.membershipNo));

                //DDLDependentList.DataTextField =dtl.memberName + " -- " + dtl.relationship;
                //DDLDependentList.DataValueField = dtl.membershipNo.ToString();
                //DDLDependentList.Items.Insert(.DataBind();

                

                ListItem DepList = new ListItem();
                DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                DDLDependentList.Items.Add(DepList);

            }
        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }


    protected void DependentList()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        request.membershipNo = strMembershipNo;
        //request..membershipNo = 
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));


        try
        {
            StringBuilder sb = new StringBuilder(200);

            response = ws.EnqMbrListInfo(request);


            if (response.errorID != "0")
            {
                sb.Append(response.errorMessage).Append("\n");
                lblErrorMessage.Text = sb.ToString();
            }
            else
            {
                DisplayMemberListResult(response);

                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlTextWriter.Formatting = Formatting.Indented;
                xs.Serialize(xmlTextWriter, response);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                this.txtBox_output.Text = XmlizedString;
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text = ex.Message;
        }

        if (DDLDependentList.Items.Count > 0)
        {
            //////          TabFlag = "D";
            DDLDependentList.Visible = true;

            btnRequestDependentCert.Visible = true;
        }
    }


    private void DisplayMemberCertResult(string MemberNo)
    {
                    ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
           // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN();
            request.membershipNo = long.Parse(MemberNo);
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); 

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ReqPrtMbrCrtInfo(request);
           // Cursor.Current = Cursors.Default;


            if (response.memberName != null)
            {
                DisplayMbrCertInfo(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                Message1.Visible = false;

            }
            else
                UpdatePanel2.Visible = false;
          
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            UpdatePanel2.Visible = false;
        

            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Invalid Member error. The member might be inactive or terminated.";// response.errorMessage[0].ToString();

        }

        


}


    private void DisplayMbrCertInfo(OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response)
    {


        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {
            //sbResponse.Append("Client TransactionID: ").Append(response.TransactionID).Append("\n");
            lblMemberName.Text = response.memberName;
            lblMembershipNo.Text = response.membershipNo.ToString();
            lblClass.Text = response.className;
            lblOptions.Text = response.option;
            lblIssueDate.Text = DateTime.Now.ToShortDateString();
            //response.
            // lblSponsorId.Text = response.MemberName;   /// lblSponsorId ??
            lblEffectiveFrom.Text = String.Format("{0:d}", response.effFrom);
            lblCustomerName.Text = response.customerName;
            lblDateOfBirth.Text = String.Format("{0:d}", response.DOB);
            lblStaffNo.Text = response.staffNo;
            lblContribution.Text = response.deductible;
            // lblEmployeeNo.Text = response.staffNo;
            lblEffectiveTo.Text = String.Format("{0:d}", response.effTo);
            //lblGender.Text = response.MemberName;  // lblGender
            // lblStatus.Text = response.status;
            lblRoomType.Text = response.roomType;
            UpdatePanel2.Visible = true;

            //sbResponse.Append("Client Status: ").Append(response.Status).Append("\n");
            //sbResponse.Append("Client CustomerNo: ").Append(response.CustomerNo.ToString()).Append("\n");
            //sbResponse.Append("Client ClassID: ").Append(response.ClassID.ToString()).Append("\n");
            //sbResponse.Append("Client BranchCode: ").Append(response.BranchCode.Trim()).Append("\n");
            //sbResponse.Append("Client MemberName: ").Append(response.MemberName);
        }
        else
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);

        //Message1.Text = sbResponse.ToString(); //MessageBox.Show(sbResponse.ToString());


    }

  
    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }





    protected void Unnamed1_Click(object sender, ImageClickEventArgs e)
    {
        imagebutton1.Visible = false;
        imagebutton2.Visible = false;
        string HTMLfile = Request.Url.ToString();

        WebRequest myRequest = WebRequest.Create(HTMLfile);


        WebResponse myResponse = myRequest.GetResponse();


        Stream ReceiveStream = myResponse.GetResponseStream();
        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");


        StreamReader readStream = new StreamReader(ReceiveStream, encode);


        Char[] read = new Char[256];
        int count = readStream.Read(read, 0, 256);

        using (StreamWriter sw = new StreamWriter(Server.MapPath("~") + "\\Mobile\\MyPage" + Request["MembershipNo"] + ".htm"))
        {
            while (count > 0)
            {

                String str = new String(read, 0, count);
                sw.Write(str);
                count = readStream.Read(read, 0, 256);
            }
        }


        myResponse.Close();

      
        Response.Redirect("faxmobile.aspx?MembershipNo=" + Request["MembershipNo"]);
    }
    protected void imagebutton2_Click(object sender, ImageClickEventArgs e)
    {
        imagebutton1.Visible = false;
        imagebutton2.Visible = false;

        string HTMLfile = Request.Url.ToString();

        WebRequest myRequest = WebRequest.Create(HTMLfile);


        WebResponse myResponse = myRequest.GetResponse();
        Stream ReceiveStream = myResponse.GetResponseStream();
        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
        string line = null;
        string line_to_delete = "asp:imagebutton ID='imagebutton1' runat='server' ImageUrl='~/Printer.gif' onclick='Unnamed1_Click' Height='30px' Width='30px'></asp:imagebutton>";
        using (StreamReader reader = new StreamReader(ReceiveStream,encode))
        {
            using (StreamWriter writer = new StreamWriter(Server.MapPath("~") + "\\Mobile\\MyPage" + Request["MembershipNo"] + ".htm"))
            {
                while ((line = reader.ReadLine()) != null) 
                {
                    if (String.Compare(line, line_to_delete) == 0)
                        continue; writer.WriteLine(line); 
                }
            }
        } 

        /*
        Stream ReceiveStream = myResponse.GetResponseStream();
        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");

        
        StreamReader readStream = new StreamReader(ReceiveStream, encode);


        Char[] read = new Char[256];
        int count = readStream.Read(read, 0, 256);

        using (StreamWriter sw = new StreamWriter(Server.MapPath("~") + "\\Mobile\\MyPage" + Request["MembershipNo"] + ".htm"))
        {
            while (count > 0)
            {

                String str = new String(read, 0, count);
                str = str.Replace("<input type='image' name='imagebutton1' id='imagebutton1' src='../Printer.gif' border='0' />", "");
                sw.Write(str);
               
                count = readStream.Read(read, 0, 256);
            }
        }

        */
        myResponse.Close();
       
        Response.Redirect("emailMobile.aspx?MembershipNo=" + Request["MembershipNo"]);
    }
    private void saveCurrentAspxToHTML()
    {
        

    } 
}