﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Templates/Mobile.Master"   EnableEventValidation="false" Inherits="Member_faxmobile" Codebehind="faxmobile.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">


    

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

    </style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
    <div>
    
      
    <center><table s>
        <tr>
            <td colspan="2" class="style3">
                &nbsp;</td>
            <td colspan="2" class="style3">
                
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h2 style="text-align: center">
                    Send Fax</h2></td>
        </tr>
       </table>
           
          <asp:Panel ID="pnlEmailForm"  runat="server" >      
       <table table style="width:50%;"> <tr>
            <td align="left">
                <strong>Fax Number</strong></td>
            <td class="style1" style="text-align: left" colspan="2">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtFax" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td style="text-align: left">
                <asp:TextBox CssClass="textbox" ID="txtFax" runat="server" Height="25px" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
            
                <strong>Recipient name</strong></td>
            <td class="style1" style="text-align: left" colspan="2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtRec" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td style="text-align: left">
                <asp:TextBox CssClass="textbox" ID="txtRec" runat="server" Height="25" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Button ID="btnsend" runat="server" Text="Send" onclick="btnsend_Click" 
                    Width="88px" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                &nbsp;</td>
        </tr>
    </table>
    </asp:Panel>
    
    </center> </div>
    <asp:Label ID="lblMsg" runat="server" Text="" style="color:Green"></asp:Label>
</asp:Content>