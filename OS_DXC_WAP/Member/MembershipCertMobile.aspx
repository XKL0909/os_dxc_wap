﻿
<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Templates/Mobile.Master"   EnableEventValidation="false"
    Inherits="MembershipCertMobile" Codebehind="MembershipCertMobile.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">


    

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

    </style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>

    <div class="adbanner" align="right" >
        <hr>
    </div>

    <div class="noPrint" align="right">
       <label id="Label1" ><font size=4>Certificate of Membership</font></label> 
        <table width=100% ><tr><td align=right>
                <asp:imagebutton runat="server" ImageUrl="~/Member/fax.png" 
                    onclick="Unnamed1_Click" ID="imagebutton1" BorderWidth="0px"></asp:imagebutton>
                    &nbsp;<asp:imagebutton runat="server" ImageUrl="~/Member/email.png"
                      ID="imagebutton2" onclick="imagebutton2_Click" BorderWidth="0px"></asp:imagebutton>
            </td>
            </tr></table>
            
            
       
  
    &nbsp;</div>
    
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

  
        <div class="noPrint" >
        

        </div>                
        <table width="100%">
        <tr>
            <td><asp:DropDownList ID="DDLDependentList" runat="server">
                            </asp:DropDownList></td>
            <td><asp:Button ID="btnRequestDependentCert" runat="server" OnClick="btnRequestDependentCert_Click"
                                Text="View Certificate" /></td>
        </tr>
        </table>
        
                            

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
            <ProgressTemplate>
                <img src="spinner.gif" />
                Loading information ......
            </ProgressTemplate>
        </asp:UpdateProgress>
        <br />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
               
                <table style="width: 100%; padding-left:0px; padding-right:0px;" cellspacing="0"   >
                    <tr >
                        <td style="width: 499px" class="style13">
                            <b class="style11">Attention all BUPA Arabia Network</b></td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style2" style="width: 40%">
                            السادة / شبكة مستشفيات بوبا العربية
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 499px; height: 30px;">
                        </td>
                        <td class="style1" style="width: 127px; height: 30px;">
                        </td>
                        <td class="style3" style="height: 30px">
                        </td>
                    </tr>
                    <tr>
                        <td class="style11">
                            TO WHOM IT MAY CONCERN</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            <b>لمن يهمه الأمر</b></td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            &nbsp;</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            <span class="style12">Subject: </span><span class="style11">Certificate of 
                            Membership</span></td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            الموضوع: شهادة عضوية</td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            &nbsp;</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 499px" class="style8">
                            Certificate Issue Date</td>
                        <td class="style14" style="width: 127px">
                            <strong class="style10">
                                <asp:Label ID="lblIssueDate" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td class="style3">
                            تاريخ الاصدار
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 499px" class="style8">
                            This is to confirm that the below mentioned member is medically covered under 
                            the BUPA Arabia Health Care scheme with the following information:</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            إن هذه الوثيقة تؤكد ان المذكور مغطى طبيا
                            <br />
                            تحت برنامج بوبا العربية للرعاية الصحية وفقا للبيانات التالية
                        </td>
                    </tr>
                </table>
                <br>
                <table style="width: 100%">
                    <tr>
                        <td align="left" class="style9" style="width: 30%">
                            Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblMemberName" runat="server"></asp:Label></strong></td>
                        <td align="right" class="style6" style="width: 30%">
                            الاسم</td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Class Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblClass" runat="server"></asp:Label></strong></td>
                        <td align="right" class="style6">
                            درجة التغطية
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Membership No:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblMembershipNo" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            رقم العضوية
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Option*:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblOptions" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            قائمة المزايا
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Room:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblRoomType" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            درجة الإقامة
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Year of Birth:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblDateOfBirth" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            تاريخ الميلاد
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Effective From:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblEffectiveFrom" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            صالحة من تاريخ&nbsp;
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Effective To:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblEffectiveTo" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            صالحة الى تاريخ
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Contribution:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblContribution" runat="server" Text="Label"></asp:Label></strong></td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">نسبة المشاركة</span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="style9" style="width: 320px">
                            Staff No.</td>
                        <td class="style10">
                            <asp:Label ID="lblStaffNo" runat="server"></asp:Label>&nbsp;</td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">الرقم الوظيفي </span>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="style9" style="width: 320px">
                            Customer Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblCustomerName" runat="server"></asp:Label></strong></td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">أسم العميل</span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 320px">
                            &nbsp;</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 320px" class="style8">
                            All Other terms and conditions are as per policy</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            جميع شروط الغطاء تخضع لبنود التعاقد</td>
                    </tr>
                    <tr>
                        <td style="width: 320px">
                            &nbsp;</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                </table>
                <p>
                </p>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 40%" class="style8" valign="top">
                            This certificate is issued as a temporary cover for this member. Should this 
                            member need any treatment that is covered under this scheme, please provide it, 
                            and charge BUPA Arabia.</td>
                        <td>
                            &nbsp;</td>
                        <td align="right" class="th" style="width: 40%; margin-right: 13px;"
                            valign="top">
                            إن هذه الشهادة اصدرت مؤقتاً للعضو وفي حالة حاجته الى اية خدمات علاجية مدرجة تحت 
                            بنود الغطاء التأميني الخاص به الرجاء تقديم الخدمة العلاجية و ارسال&nbsp; الفواتير 
                            الى&nbsp; بوبا العربية
                        </td>
                        <td align="right" class="th" colspan="1" style="width: 2%; margin-right: 1px" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 437px" class="style8" valign="top">
                            Should you have any queries, please contact one of our advisers on the BUPA 
                            Arabia dedicated customer service line.
                        </td>
                        <td style="width: 55px">
                            &nbsp;</td>
                        <td align="right" class="th" valign="top">
                            <span dir="rtl" lang="AR-SA" style="font-size: 12pt; font-family: 'Arial','sans-serif';
                                mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-GB; mso-fareast-language: EN-GB;
                                mso-bidi-language: AR-SA">لمزيد من المعلومات الرجاء الاتصال بالهاتف المجاني 
                            لقسم خدمات العملاء.</span>
                        <td align="right" class="th" colspan="1" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 437px" class="style8" valign="top">
                            This certificate is valid only for the named insured person when accompanied 
                            with ID documents.
                            <br />
                            <br />
                            Yours sincerely<br />
                            <br />
                            Bupa Arabia<br />
                            Customer Service
                        </td>
                        <td style="width: 55px">
                            &nbsp;</td>
                        <td align="right" class="th" valign="top">
                            إن هذه الشهادة صالحة فقط للعضو المذكور اعلاه شرط توفر وثيقة اثبات رسمية
                            <br />
                            <br />
                            <br />
                            <p align="right" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: right">
                                <span dir="rtl" lang="AR-SA" style="font-family: 'Arial','sans-serif'">مع اطيب 
                                تحيات الشكر والتقدير</span><span lang="EN-GB" style="font-family: 'Arial','sans-serif'"><?xml
                                        namespace="" ns="urn:schemas-microsoft-com:office:office" prefix="o" ?><o:p></o:p></span></p>
                        </td>
                        <td align="right" class="th" colspan="1" valign="top">
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td>
                            <b><i class="style8">This is only a definition for the codes shown under the 
                            option field. Do not use for coverage checking.</i></b></td>
                    </tr>
                    <tr>
                        <td class="style8">
                            * A: Dialysis, C: Chronic, D: Dental, E: Neo-natal care, H: Other Conditions, K: 
                            Cosmetic, L: Congenital, M: Maternity, N: Natural Changes, O: Optical, P: 
                            Developmental, S: Standard Benefit, T: Screening, V: Vaccinations, Y: 
                            Physiotherapy, Z: Hazardous Sports Injury
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
        <br />
        <div>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
            <br />
            <%--          <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtenderLogin">
           
           </ajaxToolkit:ModalPopupExtender>--%>
            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
                        
           
       
        </div>
</asp:Content>