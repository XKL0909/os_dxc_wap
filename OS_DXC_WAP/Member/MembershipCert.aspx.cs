using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;

public partial class MembershipCert : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
           
            CheckSessionExpired();
        }
        catch
        {

        }

        // lblMemberName.Text = "";

        //System.Threading.Thread.Sleep(3000);
        //LinkButton1.Text = DateTime.Now.ToString();
        try
        {
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        }


        OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response;
        if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        {
            Message1.Text = "Please insert all the fields before using the web service";
            return;
        }
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN();
            request.membershipNo = long.Parse(txtMbrShp.Text.Trim());
            request.transactionID = Int32.Parse(txtTxnID.Text.Trim());

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ReqPrtMbrCrtInfo(request);
            // Cursor.Current = Cursors.Default;
            if (response != null)
            {
                DisplayMbrCertInfo(response);

                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }



    }

    private void DisplayMbrCertInfo(OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response)
    {


        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {
            //sbResponse.Append("Client TransactionID: ").Append(response.TransactionID).Append("\n");
            lblMemberName.Text = response.memberName;
            lblMembershipNo.Text =  response.membershipNo.ToString() ;
            lblClass.Text =  response.className;
            lblOptions.Text = response.option;
            lblIssueDate.Text = DateTime.Now.ToShortDateString();
            //response.
           // lblSponsorId.Text = response.MemberName;   /// lblSponsorId ??
            lblEffectiveFrom.Text = response.effFrom.ToString();
            lblCustomerName.Text = response.customerName;
            lblDateOfBirth.Text = response.DOB ;
            lblStaffNo.Text = response.staffNo;
           // lblEmployeeNo.Text = response.staffNo;
            lblEffectiveTo.Text = response.effTo.ToString();
            //lblGender.Text = response.MemberName;  // lblGender
           // lblStatus.Text = response.status;
            lblRoomType.Text = response.roomType ;
            UpdatePanel2.Visible = true;

            //sbResponse.Append("Client Status: ").Append(response.Status).Append("\n");
            //sbResponse.Append("Client CustomerNo: ").Append(response.CustomerNo.ToString()).Append("\n");
            //sbResponse.Append("Client ClassID: ").Append(response.ClassID.ToString()).Append("\n");
            //sbResponse.Append("Client BranchCode: ").Append(response.BranchCode.Trim()).Append("\n");
            //sbResponse.Append("Client MemberName: ").Append(response.MemberName);
        }
        else
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);

        Message1.Text = sbResponse.ToString(); //MessageBox.Show(sbResponse.ToString());


    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }


}
