<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="MyMed1Mobile" Codebehind="MyMed1Mobile.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    
    
    
		<script type='text/JavaScript' src='scw.js'></script>
  
    
</head>
<body style="font-family:Arial; font-size:11px;">
    <form id="form1" runat="server">
    
    
            <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />

  <div class="noPrint" >   
        
          <table width="100%">
        <tr valign="middle"><td >
        <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Medication" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
        <td style="text-align: right"><img src=Logo.gif width="168" height="50" /></td></tr>
 
        <tr><td></td><td></td></tr>
        </table>
        
        
       <aspAjax:TabContainer runat="server" ID="tabsModManager" BorderStyle="None" BackColor="AliceBlue"
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Font-Bold="True"
            Font-Size="Medium">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Font-Size="Medium" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Font-Size="Small" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="Label7" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                               <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" style="font-size:12px; font-family:Arial;" runat="server" Visible="false">
                            </asp:DropDownList></div>
                             <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details" />
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer>
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="1">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size="1"> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
 
 
 
 
 
      <div id="CoverageListReport"  visible=false style=" font-family:Verdana; ; height:auto; width:100%; overflow:hidden;" runat="server" >
       </div>   
    <asp:UpdatePanel ID="upd1" runat="server" Visible="true"><ContentTemplate>
  <asp:Label ID="Message1" runat="server" ForeColor="red"  Width="450" Text=""></asp:Label>
   
   <asp:Panel ID="PnlUserInfo" Visible="true" runat="server" >
   
       <table style="font-size:12px;" >
           
                <tr>
                    <td>
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server" Text="3456998"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblMember_Name" runat="server" Visible="false" Text="Member Name :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server"  Visible="false"  Text="Sunil Kumar Jose"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblCustomer_Name" runat="server" Text="Customer Name :"  Visible="false" ></asp:Label></td>
                    <td >
                        <asp:Label ID="lblCustomerName" runat="server" Visible="false" Text="30/05/1970"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblClass_Name" runat="server" Text="Provider Name:"></asp:Label></td>
                    <td width="90%">
                          <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLProviderList" width="100%" ValidationGroup="1" style="font-size:12px; font-family:Arial;" runat="server" Visible="true">
                            </asp:DropDownList></div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="DDLProviderList"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                            <br />
                            
                            </td>
                </tr>
          
            </table>
</asp:Panel>
    
    
      <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
            Font-Names="Arial" Font-Size="11px" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
            Width="100%">
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="CompleteName" HeaderText="Service Description" >
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" /> <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="period" HeaderText="Supply Period" >
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" /> <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="DateFrom" HeaderText="Date From" >
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" /> <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="DateTo" DataFormatString="{0:d MMM yyyy }" HeaderText="Date To"
                    HtmlEncode="False">
                    
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" /> <ItemStyle  Wrap="false" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" >
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" /> <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="Cost" HeaderText="Cost" >
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" /> <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Request Refill">
                    <ItemTemplate>
                        <asp:CheckBox ID="Chec" runat="server"  Enabled="true" Checked="true" EnableViewState="true" />
                    </ItemTemplate>
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" /> <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" SelectText="Edit" >
                    <HeaderStyle BackColor="white" />
                    <ItemStyle BackColor="white" HorizontalAlign="Left" />
                </asp:CommandField>
            </Columns>
            <RowStyle BackColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
                <SelectedRowStyle BackColor="#4CAC27" BorderStyle="None" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#E6F5FF" Font-Bold="True" ForeColor="#404040" Font-Names="Arial" Font-Size="9pt" Font-Strikeout="False" />
        </asp:GridView>

<asp:Panel ID="PanelEdit" runat="server" Visible="false">
        <table border="0">
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                </td>
                <td style="width: 100px">
                    <asp:TextBox CssClass="textbox" ID="txtName" Width="250px"  onfocus="this.blur();"  runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label6" runat="server" Text="Period"></asp:Label></td>
                <td style="width: 100px">
                    <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlPeriod" runat="server" Width="257px" onChange="AddMonths(document.form1.txtDateOfEffect.value);" >
                        <asp:ListItem Value="1">1 Month</asp:ListItem>
                        <asp:ListItem Value="2">2 Month</asp:ListItem>
                        <asp:ListItem Value="3">3 Month</asp:ListItem>
                    </asp:DropDownList></div></td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label2" runat="server" Text="Date From"></asp:Label></td>
                <td style="width: 100px">
                   
                    
                  
                    
                      <asp:TextBox CssClass="textbox" ID="txtDateOfEffect"  runat="server"  Width="250px"      onclick="scwShow(this,event);"  style="cursor:default;"  onMouseover="AddMonths(this.value);"   BackColor="MistyRose"  ></asp:TextBox>
   

   
                    
                    
                </td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label3" runat="server" Text="Date To"></asp:Label></td>
                <td style="width: 100px">
                          <asp:TextBox CssClass="textbox" ID="txtDateTo"  Width="250px"  onfocus="this.blur();"    runat="server"   onMouseover="AddMonths(document.form1.txtDateOfEffect.value);"    />
</td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label4" runat="server" Text="Quantity"></asp:Label></td>
                <td style="width: 100px">
                    <asp:TextBox CssClass="textbox" ID="txtQuantity"  Width="250px"  onfocus="this.blur();"   runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label5" runat="server" Text="Cost"></asp:Label></td>
                <td style="width: 100px">
                    <asp:TextBox CssClass="textbox" ID="txtCost"  Width="250px"  onfocus="this.blur();"   runat="server"></asp:TextBox></td>
            </tr>
        </table> 
        <br />
        <asp:Button CssClass="submitButton" ID="bntUpdate" runat="server" Text="Update Service" OnClick="bntUpdate_Click" />
           <asp:Button CssClass="submitButton" ID="btnCancel" runat="server" Text="Cancel" OnClick="bntCancel_Click" />
        <br />

</asp:Panel>

<asp:Panel ID="PnlButtons" runat="server" Visible="false">
<br />
 <input type="button" name="Check_All" value="Check All" onclick="checkAll()" />
        <input type="button" name="Check_All" value="UnCheck All" onclick="UncheckAll()" />
    
    
                <p style="text-align:right;">
        <asp:Button CssClass="submitButton" ID="btnSubmitAll"  runat="server" OnClick="btnSubmitAll_Click" Text="Submit Request"  ValidationGroup="1" 
           OnClientClick="javascript:return anyCheck1();"  />
         </p> 
    </asp:Panel>
    </ContentTemplate></asp:UpdatePanel>
    
    
    <div>

        &nbsp;&nbsp;
        <br />
      
       
        &nbsp;
     <asp:Label ID="lblErrorMessage" ForeColor="red" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
                <asp:Label  ID="lblTabLoadStatus" runat="server"></asp:Label>

        
       
          
            
        &nbsp; &nbsp; &nbsp; &nbsp;<br />          
                  <p style="display:none;">
    <asp:TextBox CssClass="textbox" Width="0%"   ID="txtNoOfChecked" runat="server"  ></asp:TextBox> 
    </p>
                 
  
  
  
  <asp:Panel ID="PnlHiddenVals" runat="server" Visible="false">
  <input id="txtAge" runat="server" />
  <input id="txtGender" runat="server" />
  <input id="txtCardNo" runat="server" />
  <input id="txtIqamaID" runat="server" />
  <input id="txtMobileNo" runat="server" />
  <input id="txtPolicy" runat="server" />
  <input id="Button2" type="button" onclick="javascript:var month = today.getMonth() + 1;alert(month);" value="1check daTE" title="date check" runat="server" />          

  </asp:Panel>
                  
                             
 
                           
 </div>
    </form>
</body>



<script language="JavaScript" type="text/javascript">
<!--



function anyCheck1() {
 var total = 0;
 var list = document.getElementsByTagName("input");
 for( var i in list){
  if( list[i].type=="checkbox" && list[i].checked )
   total=total+1;
 }
 if(total > 0)
 {
 //alert("You selected " + total + " boxes.");
 var txt1 = document.getElementById("txtNoOfChecked");
 txt1.value = total;
 return true;
  }
  else
  {
  alert("Please select atleast 1 Medication from the list.");
  return false;
  }
}





function DateAdd(objDate, strInterval, intIncrement)
    {
        if(typeof(objDate) == "string")
        {
            objDate = new Date(objDate);
 
            if (isNaN(objDate))
            {
                throw("DateAdd: Date is not a valid date");
            }
        }
        else if(typeof(objDate) != "object" || objDate.constructor.toString().indexOf("Date()") == -1)
        {
            throw("DateAdd: First parameter must be a date object");
        }
 
        if( strInterval != "M" && strInterval != "D" &&  strInterval != "Y" &&  strInterval != "h" &&  strInterval != "m" &&  strInterval != "uM" &&  strInterval != "uD" && strInterval != "uY" && strInterval != "uh" && strInterval != "um" && strInterval != "us" )
        {
            throw("DateAdd: Second parameter must be M, D, Y, h, m, uM, uD, uY, uh, um or us");
        }
 
        if(typeof(intIncrement) != "number")
        {
            throw("DateAdd: Third parameter must be a number");
        }
 
        switch(strInterval)
        {
            case "M":
            objDate.setMonth(parseInt(objDate.getMonth()) + parseInt(intIncrement));
            break;
 
            case "D":
            objDate.setDate(parseInt(objDate.getDate()) + parseInt(intIncrement));
            break;
 
            case "Y":
            objDate.setYear(parseInt(objDate.getYear()) + parseInt(intIncrement));
            break;
 
            case "h":
            objDate.setHours(parseInt(objDate.getHours()) + parseInt(intIncrement));
            break;
 
            case "m":
            objDate.setMinutes(parseInt(objDate.getMinutes()) + parseInt(intIncrement));
            break;
 
            case "s":
            objDate.setSeconds(parseInt(objDate.getSeconds()) + parseInt(intIncrement));
            break;
 
            case "uM":
            objDate.setUTCMonth(parseInt(objDate.getUTCMonth()) + parseInt(intIncrement));
            break;
 
            case "uD":
            objDate.setUTCDate(parseInt(objDate.getUTCDate()) + parseInt(intIncrement));
            break;
 
            case "uY":
            objDate.setUTCFullYear(parseInt(objDate.getUTCFullYear()) + parseInt(intIncrement));
            break;
 
            case "uh":
            objDate.setUTCHours(parseInt(objDate.getUTCHours()) + parseInt(intIncrement));
            break;
 
            case "um":
            objDate.setUTCMinutes(parseInt(objDate.getUTCMinutes()) + parseInt(intIncrement));
            break;
 
            case "us":
            objDate.setUTCSeconds(parseInt(objDate.getUTCSeconds()) + parseInt(intIncrement));
            break;
        }
       // 
       alert(objDate.toDateString());
        return objDate;
    }


//-->

</script>

<script language="javascript" type="text/javascript" src="jsDate.js"></script>

<script type="text/javascript">
<!--

function AddMonths(txt)
{

var DatePart = txt.split("/");
DatePart[0];
DatePart[1];
DatePart[2];

//alert (DatePart[1] + "/" + DatePart[0] + "/" + DatePart[2]);
var formatedDate = DatePart[1] + "/" + DatePart[0] + "/" + DatePart[2];
    var d = new Date(formatedDate);  //"04/10/2006"

    var noMonth = 27 * document.getElementById("ddlPeriod").value;
    d.setDate(d.getDate()+ noMonth);
  //  alert((d.getMonth()+1)+"/"+d.getDate()+"/"+d.getYear());
 document.getElementById("txtDateTo").value = d.getDate()+"/"+(d.getMonth()+1)+"/" + d.getYear();
//	alert( d.getDate()+"/"+(d.getMonth()+1)+"/" + d.getYear());
}

-->
</script>



 
<SCRIPT language="JavaScript">
<!-- 

<!-- Begin
function Check(chk)
{


var totalchkbox = 0;
 var list = document.getElementsByTagName("input");
 for( var i in list){
  if( list[i].type=="checkbox" )
   totalchkbox=totalchkbox+1;
}


if(document.form1.Check_All.value=="Check All"){
for (i = 0; i < totalchkbox; i++)
list[i].checked = true ;
document.form1.Check_All.value="UnCheck All";
}else{

for (i = 0; i < totalchkbox; i++)
list[i].checked = false ;
document.form1.Check_All.value="Check All";
}
}

// End -->
</script>


<SCRIPT LANGUAGE="JavaScript">
<!-- 

<!-- Begin

function checkAll()
{
var inputs = document.getElementsByTagName("input");
var checkboxes = [];
for (var i = 0; i < inputs.length; i++) {

  if (inputs[i].type == "checkbox") {
inputs[i].checked =true;
}
}
}

function UncheckAll()
{
var inputs = document.getElementsByTagName("input");
var checkboxes = [];
for (var i = 0; i < inputs.length; i++) {

  if (inputs[i].type == "checkbox") {
inputs[i].checked =false;
}
}
}
-->

</SCRIPT>


</html>

