<%@ Page Language="C#"  MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" Inherits="Member_ChangePass" Codebehind="ChangePass.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <style >
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>

  

 	<style type="text/css">
		body{
			font-family:Arial,Sans-serif;
			font-size:80%;
		}
		caption{
			padding-bottom:5px;
			
		}
		thead th,tfoot td{
			background:#e6f5ff; font-color:#ffffff;font-weight:bold;
		}
		tr.ruled{
			<%--background:#ccea68; font-weight:bold;--%>
		}    
		table{
			
			border-collapse:collapse;
		}
		th,td{
			
			border-collapse:collapse;
		}
		#mytable tr.ruled{
			<%--background:#333;
			color:#ccc;--%>
		}
    </style>
	<script type="text/javascript" src="../includes/tableruler.js"></script>
	<script type="text/javascript">
	    window.onload = function () { tableruler(); } 
    </script>

    <div>
    
    <table width="99%">
        <tr valign="middle"><td >
        <asp:Label ID="lblAdd_Employee" runat="server"  Text="Change Password" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
        <td style="text-align: right">&nbsp;</td></tr>
        
      
        </table>
    
    <asp:Panel ID="PnlChangePass" DefaultButton="JoinBtn" runat="server"      Width="99%"  Font-Names="@MS PGothic">
            <br />
            <table style=" font-family:Verdana; font-size:small; text-align: left;" Align="center" width="99%">
                <tr>
                    <td colspan="2"   valign="top">
        <asp:Label ID="Label8" runat="server" Text="Please fill in the required information below:"
            Width="336px"></asp:Label></td>
                </tr>
       
       
               <tr>
                    <td style="width: 23px ;" align="left" valign="top">
                        <asp:Label ID="Label1" runat="server"  Text="Old Password:" Font-Names="Verdana" Font-Size="Small" Width="103px"></asp:Label></td>
                    <td style="width: 330px; " valign="top">
                        <asp:TextBox CssClass="textbox" ID="txtOldPassword"  runat="server" Width="171px" ValidationGroup="RegistrationGroup" TextMode="Password" MaxLength="40" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOldPassword"
                            EnableClientScript="true" Enabled="true" ErrorMessage="Password cannot be blank."
                            Font-Bold="False" Font-Size="XX-Small" SetFocusOnError="false"
                            ValidationGroup="RegistrationGroup" Width="233px" Display="Dynamic"></asp:RequiredFieldValidator></td>
                </tr>
       
       
       
       
             
                <tr>
                    <td style="width: 23px ; " align="left" valign="top">
                        <asp:Label ID="Label7" runat="server" Height="23px" Text="New Password:" Font-Names="Verdana" Font-Size="Small" Width="135px"></asp:Label></td>
                    <td style="width: 330px; " valign="top">
                        <asp:TextBox CssClass="textbox" ID="txtPassword" runat="server" Width="171px" ValidationGroup="RegistrationGroup" TextMode="Password" MaxLength="40" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                            EnableClientScript="true" Enabled="true" ErrorMessage="Password cannot be blank."
                            Font-Bold="False"  Font-Size="XX-Small" SetFocusOnError="false"
                            ValidationGroup="RegistrationGroup" Width="233px" Display="Dynamic"></asp:RequiredFieldValidator></td>
                </tr>
                <tr >
                    <td style="width: 23px " align="left" valign="top">
                        <asp:Label ID="Label5" runat="server"  Text="Retype Password:" Font-Names="Verdana" Font-Size="Small" Width="141px"></asp:Label></td>
                    <td style="width: 330px;" valign="top" >
                        <asp:TextBox CssClass="textbox" ID="txtConfirmPassword" runat="server" Width="171px" ValidationGroup="RegistrationGroup" TextMode="Password" MaxLength="40"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtConfirmPassword"
                            Display="Dynamic" ErrorMessage="Field cannot be blank." 
                            Font-Size="X-Small" ValidationGroup="RegistrationGroup">Field cannot be blank.</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                            ControlToValidate="txtConfirmPassword" ErrorMessage="CompareValidator" Font-Names="Verdana"
                            Font-Size="X-Small" Display="Dynamic"  ValidationGroup="RegistrationGroup">Password not identical.</asp:CompareValidator></td>
                </tr>
                <tr>
                    <td style="width: 23px; " valign="top">
                    </td>
                    <td style="  width: 330px; " align="center" valign="top">
                        <%--<asp:Button CssClass="submitButton" ID="CancelBtn2" runat="server" Text="Cancel" Width="52px" OnClick="CancelBtn_Click" Height="20px" Font-Size="X-Small" />--%>
                        <asp:Button CssClass="submitButton" ID="JoinBtn" runat="server" Text="Change Password" OnClick="JoinBtn_Click"  Font-Size="Small" ValidationGroup="RegistrationGroup" Width="140px" /></td>
                </tr>
            </table>
        </asp:Panel>
    
    <asp:Label ID="lblMessage" Font-Names="verdana" Font-Size=Small  runat="server" Visible="false" Text="" ></asp:Label>
    
    </div>
     <br />
    <br />
    <uc1:OSNav ID="OSNav1" runat="server" />
</asp:Content>
 <asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
     Change you password ...
</asp:Content>