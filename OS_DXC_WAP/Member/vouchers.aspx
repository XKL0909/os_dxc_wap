﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Member_vouchers" Codebehind="vouchers.aspx.cs" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function CheckAndPrint() {
            var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

            if (document.getElementById('v1No').innerHTML.search(/E/) >= 0) {
                self.print();
                if (!isChrome)//Chrome closes it automatically
                    self.close();
            }
        }
    </script>
</head>
<body onload="CheckAndPrint();">
    <form id="form1" runat="server">
    <div>
        <table runat="server" id="tblVoucher" style="border:0px;vertical-align:central; height: 305px; width: 695px;vertical-align:bottom;text-align:right;color:white;font-weight:300;text-indent: 1.5em;z-index:9999999;" >
                        <tr>
                            <td>
                                <asp:Image ID="imgVoucher1" runat="server" Height="305px" Width="695px"/>
                                <div runat="server" id="v1Div1" style="width: 164px;  position:absolute; top: 271px; left: 366px;" >
                                    <img src="images/WhiteBG.png" style="width: 100px;height: 21px;">
                                    <div runat="server" id="v1Memb" style="position: absolute; top: 3px; left: 55px; width: 95px; color: black; font-size: 13px; text-align: center; height: 22px;"></div>
                                    <div style="position: absolute; top: 25px; left: 34px; width: 137px; color: black !important;font-size:13px;font-weight: bold; text-align: center; height: 22px;">Membership No.</div>
                                </div>
                                <div runat="server" id="v1Div2"  style="width: 164px;  position:absolute; top: 271px; left: 523px;" >
                                    <img src="images/WhiteBG.png" style="width:100px;height: 21px;">
                                    <div runat="server" id="v1No" style="position: absolute; top: 3px; left: 55px; width:90px; color:black;font-size:13px; text-align: center; height: 20px; margin-top: 0px;"></div>
                                    <div style="position: absolute; top: 25px; left: 50px; width: 113px; color: black !important;font-size:13px;font-weight: bold; text-align: center; height: 22px;">Voucher No.</div>
                                </div>
                            </td>
                    <td style="vertical-align:top">
                   <asp:Button ID="btnPrint" ClientIDMode="Static" runat="server" Text="Print" OnClick="btnPrint_Click" /></td>
           </tr>
           <tr><td style="height:0.5px;"></td></tr>
           <tr>
                <td>
                    <asp:Image ID="imgVoucher2" runat="server" Height="305px" Width="695px" />
                    <div runat="server" id="v2Div1" style="width: 164px;  position:absolute; top: 607px; left: 365px; right: 837px;" >
                        <img src="images/WhiteBG.png" style="width: 100px;height: 21px;position: absolute;top: -19px;left: 64px;">
                        <div runat="server" id="v2Memb" style="position: absolute; top: -17px; left: 55px; width: 95px; color:black;font-size:13px; text-align: center; height: 22px;"></div>
                        <div style="position: absolute; top: 7px; left: 34px; width: 137px; color: black !important;font-size:13px;font-weight: bold; text-align: center; height: 22px;">Membership No.</div>
                    </div>
                    <div runat="server" id="v2Div2"  style="width: 164px;  position:absolute; top: 607px; left: 526px;" >
                        <img src="images/WhiteBG.png" style="width:100px;height: 21px;position: absolute; top: -19px; left: 70px;">
                        <div runat="server" id="v2No"  style="position: absolute; top: -17px; left: 55px; width:90px; color:black;font-size:13px; text-align: center; height: 20px; margin-top: 0px;">&nbsp;</div>
                        <div style="position: absolute; top: 7px; left: 50px; width: 113px; color: black !important;font-size:13px;font-weight: bold; text-align: center; height: 21px;">Voucher No.</div>
                    </div>
                </td>
            </tr>
           <tr>
               <td style="height:0.5px;"></td>

           </tr>
           <tr>
                <td><asp:Image ID="imgVoucher3" runat="server" Height="305px" Width="695px" />
                    <div runat="server" id="v3Div1" style="width: 164px;  position:absolute; top: 919px; left: 366px; right: 836px;" >
                        <img src="images/WhiteBG.png" style="width: 100px;height: 21px;position: absolute; top: -15px; left: 64px;">
                        <div runat="server" id="v3Memb" style="position: absolute; top: -13px; left: 55px; width: 95px; color:black;font-size:13px; text-align: center; height: 22px;"></div>
                        <div style="position: absolute; top: 10px; left: 34px; width: 137px; color: black !important;font-size:13px;font-weight: bold; text-align: center; height: 22px;">Membership No.</div>
                    </div>
                    <div runat="server" id="v3Div2"  style="width: 164px;  position:absolute; top: 919px; left: 524px;" >
                        <img src="images/WhiteBG.png" style="width:100px;height: 21px;position: absolute; top: -15px; left: 70px;">
                        <div runat="server" id="v3No"  style="position: absolute; top: -13px; left: 55px; width:90px; color:black;font-size:13px; text-align: center; height: 20px; margin-top: 0px;">&nbsp;</div>
                        <div style="position: absolute; top: 10px; left: 50px; width: 113px; color: black !important;font-size:13px;font-weight: bold; text-align: center; height: 21px;">Voucher No.</div>
                    </div>
                </td>
            </tr>
       </table>
    </div>
    </form>
</body>
</html>
