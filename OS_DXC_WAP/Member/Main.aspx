﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/DashBoard.master" AutoEventWireup="true" Inherits="Member_Main" Codebehind="Main.aspx.cs" %>
<%@ Register src="../UserControls/DashBoardWelcome.ascx" tagname="DashBoardWelcome" tagprefix="uc1" %>
<%@ Register src="../UserControls/DashBoardOSNav.ascx" tagname="DashBoardOSNav" tagprefix="uc2" %>
<%@ Register src="../UserControls/CCHI.ascx" tagname="CCHI" tagprefix="uc3" %>
<%@ Register src="../UserControls/Tips.ascx" tagname="Tips" tagprefix="uc4" %>
<%@ Register src="../UserControls/HealthArt.ascx" tagname="HealthArt" tagprefix="uc5" %>
<%@ Register src="../UserControls/Apps.ascx" tagname="Apps" tagprefix="uc6" %>
<%@ Register src="../UserControls/Chat.ascx" tagname="Chat" tagprefix="uc7" %>
<%@ Register src="../UserControls/Downloads.ascx" tagname="Downloads" tagprefix="uc8" %>
<%@ Register src="../UserControls/MyInbox.ascx" tagname="MyInbox" tagprefix="uc9" %>
<%@ Register src="../UserControls/Hospital.ascx" tagname="Hospital" tagprefix="uc10" %>
<%@ Register src="../UserControls/draw.ascx" tagname="draw" tagprefix="uc11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderLeftNav" runat="Server">
    <style type="text/css">
        /* this for pop up window onload for sabic customers*/
        #shadowElem {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: #444444;
            opacity: 0.3;
        }

        #dropElem {
            display: none;
            position: absolute;
            top: 0;
            border-radius: 10px 10px 10px 10px;
            box-shadow: 0 0 25px 5px #999;
            padding: 20px;
            background: #E6E6E6;
        }

        #dropContent {
            position: relative;
        }

        #dropClose {
            position: absolute;
            z-index: 99999;
            cursor: pointer;
            top: -32px;
            right: -30px;
            padding: 5px;
            background-color: #444444;
            border-radius: 6px 6px 6px 6px;
            color: #fff;
        }


        #shadowElem1 {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: #444444;
            opacity: 0.3;
        }

        #dropElem1 {
            display: none;
            position: absolute;
            top: 0;
            border-radius: 10px 10px 10px 10px;
            box-shadow: 0 0 25px 5px #999;
            padding: 20px;
            background: #E6E6E6;
        }

        #dropContent1 {
            position: relative;
        }

        #dropClose1 {
            position: absolute;
            z-index: 99999;
            cursor: pointer;
            top: -32px;
            right: -30px;
            padding: 5px;
            background-color: #444444;
            border-radius: 6px 6px 6px 6px;
            color: #fff;
        }

        /*this is what we want the div to look like*/
        div.botright {
            display: block;
            /*set the div in the bottom right corner*/
            position: fixed;
            bottom: 0;
            right: 0;
            width: 300px;
            height: 180px;
            margin: 0 auto 20px auto;
            padding: 25px 15px;
            /*give it some background and border*/
            background: #00A1E4;
            border: 1px solid #7d5912;
            color: #FFFFFF;
        }

        .auto-style1 {
            text-align: left;
        }
    </style>
    <link href="css/css-notification-box.css" rel="stylesheet" type="text/css" />
    <link href="css/global.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        (function () {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', '701976493189767']);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>

    <uc3:CCHI ID="CCHI1" runat="server" />
    <uc7:Chat ID="Chat1" runat="server" />

    <table id="tblHCOforSabic" runat="server" cellpadding="4" cellspacing="1" visible="false">
        <tr>
            <td align="left" class="boxcaptionDoc">
                <div class="investorBannerTitle" align="left">Health Care Services</div>
                <div class="margBottom15">
                    <table>
                        <tr>
                            <td>
                                <img src="../images/chronic-medical-refill.jpg" style="width: 50px; height: 50px;" />
                            </td>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; font-style: normal; font-variant: normal; color: white; width: 220px;">
                                <div style="padding-top: 10px; height: 61px;">
                                    <a href="../Docs/Medical Refill - FINAL.pdf" style="color: #fff; text-decoration: none;" target="_blank">Chronic Medical Refill Service</a>
                                    <br />
                                    <br />

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 15px;">
                                <img src="../images/maternity-care.jpg" style="width: 75px; height: 75px;" />
                            </td>
                            <td style="padding-top: 15px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; font-style: normal; font-variant: normal; color: white;">
                                <div style="padding-top: 10px; height: 61px;">
                                    <a href="../Docs/Maternity Care - FINAL.pdf" target="_blank" style="color: #fff; text-decoration: none;">Maternity Care Service</a>
                                    <br />
                                    <br />
                                </div>
                            </td>
                        </tr>
                    </table>

                </div>
            </td>
        </tr>
    </table>
    <br />

    <uc10:Hospital ID="Hospital1" runat="server" />
    <noscript>
        <img height="1" width="1" alt="" style="display: none" src="https://www.facebook.com/tr?id=701976493189767&amp;ev=PixelInitialized" /></noscript>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
    <uc1:DashBoardWelcome ID="DashBoardWelcome1" runat="server" />
    &nbsp;<uc9:MyInbox ID="MyInbox1" runat="server" />
    <uc2:DashBoardOSNav ID="DashBoardOSNav1" runat="server" />
    <!-- popup message HCO  style="display:none;"-->
    <div id="dropElem">
        <!-- wrapper -->
        <div id="dropContent">
            <!-- Inside this tag put your popup contents -->
            <div id="dropClose">X</div>
            <!-- this will serve as close button -->
            <div style="border-style: solid;">
                <p style="margin: 5px; padding: 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; font-style: normal; font-variant: normal; color: #444444;">
                    Please click on the below service icons to learn more<br />
                    about our healthcare services that are offered to you
                <br />
                    exclusively from Bupa Arabia
                </p>
                <table>
                    <tr>
                        <td align="center" style="text-align: center; vertical-align: middle; padding: 5px; margin: 5px;">
                            <table>
                                <tr>
                                    <td>
                                        <img src="../images/chronic-medical-refill.jpg" style="width: 75px; height: 75px;" />
                                    </td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; font-style: normal; font-variant: normal; color: #444444; width: 220px;">
                                        <div style="border: 2px solid #4E80BD; padding-top: 10px; height: 61px;">
                                            Chronic Medical Refill Service
                                    <br />
                                            <br />
                                            <a href="../Docs/Medical Refill - FINAL.pdf" target="_blank" style="text-decoration: none;">Click here to display or to download</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 15px;">
                                        <img src="../images/maternity-care.jpg" style="width: 75px; height: 75px;" />
                                    </td>
                                    <td style="padding-top: 15px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; font-style: normal; font-variant: normal; color: #444444;">
                                        <div style="border: 2px solid #4E80BD; padding-top: 10px; height: 61px;">
                                            Maternity Care Service
                                    <br />
                                            <br />
                                            <a href="../Docs/Maternity Care - FINAL.pdf" target="_blank" style="text-decoration: none;">Click here to display or to download</a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <!-- popup message terminated style="display:none;"-->
    <div id="dropElem1">
        <!-- wrapper -->
        <div id="dropContent1">
            <!-- Inside this tag put your popup contents -->
            <div id="dropClose1">X</div>
            <!-- this will serve as close button -->
            <div style="border-style: solid;">

                <table>
                    <tr>
                        <td align="center" style="text-align: center; vertical-align: middle; padding: 5px; margin: 5px;">
                            <table>
                                <tr>
                                    <td>
                                        <img src="../images/error1.jpg" style="width: 75px; height: 75px;" />
                                    </td>
                                    <td style="font-family: Tahoma, Helvetica, sans-serif; font-size: 12px; font-weight: bold; font-style: normal; font-variant: normal; color: #444444; width: 220px;">
                                        <div style="padding-top: 10px; height: 70px;">
                                            <span> Membership may be expired, terminated or not active yet. 

                                                </span>
                                            <br />
                                            <span style="float:right; ">
                                                العضوية متوقفة أو منتهية أو غير مفعلة
                                            </span>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <div class="careerBanner" id="divBanner" runat="server" visible="false">
        <div class="careerBannerTitle">Member Exclusive</div>
        <div id="vouchers" runat="server">
            <table style="width: 100%; text-align: center">
                <tr valign="middle">
                    <td rowspan="3" valign="top" class="auto-style1">You can get your Monetary Vouchers and enjoy special offers with our partners by clicking on the desired partner’s logo, check the preview of the voucher then click on print. Each member can print as many voucher as needed.
                                <br />
                        <br />
                        <table>
                            <tr>
                                <td style="vertical-align: middle;">
                                    <a target="_blank" href="vouchers.aspx?image1=Cambridge EN-01.jpg">
                                        <img src="images/icons/Cambridge Bi.jpg" style="border: 0" height="70" width="150px" />
                                    </a>
                                </td>
                                <td style="text-align: right;">
                                    <a target="_blank" href="vouchers.aspx?image1=The Clinics EN-01.jpg">
                                        <img src="images/icons/TheClinicsLogo.png" style="border: 0;" height="116px" width="98px" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="center"><a target="_blank" href="vouchers.aspx?image1=Magrabi Dental EN-01.jpg">
                        <img src="images/icons/Magrabi.jpg" style="border: 0;" height="60px" width="80px" /></a></td>
                    <td align="center"><a target="_blank" href="vouchers.aspx?image1=Dossary Hospital EN-01.jpg">
                        <img src="images/icons/Dossari Hospital.jpg" height="40px" width="100px" /></a></td>
                    <td align="center"><a target="_blank" href="vouchers.aspx?image1=Kingdom Hospital EN-01.jpg">
                        <img src="images/icons/KHCC.png" style="border: 0;" height="60px" width="100px" /></a></td>
                </tr>
                <tr valign="middle">
                    <td align="center">
                        <a target="_blank" href="vouchers.aspx?image1=FitnessTimeReg1New.png&image2=FitnessTimeReg2New.png&image3=FitnessTimeReg3New.png">
                            <img src="images/icons/ftLogoReg.png" style="border: 0" height="55px" width="120px" />
                        </a>
                    </td>
                    <td align="center">
                        <a target="_blank" href="vouchers.aspx?image1=FitnessTimeJun1New.png&image2=FitnessTimeJun2New.png&image3=FitnessTimeJun3New.png">
                            <img src="images/icons/ftLogoJun.png" style="border: 0" height="55px" width="120px" />
                        </a>
                    </td>
                    <td align="center">
                        <a target="_blank" href="vouchers.aspx?image1=FitnessTimePro1New.png&image2=FitnessTimePro2New.png&image3=FitnessTimePro3New.png">
                            <img src="images/icons/ftLogoPro.png" style="border: 0" height="55px" width="120px" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <a target="_blank" href="vouchers.aspx?image1=Delta Fitness EN-01.jpg">
                            <img src="images/icons/DeltaIcon.jpg" height="50" width="200px" />
                        </a>
                    </td>
                    <td align="center"><a target="_blank" href="vouchers.aspx?image1=Mouwasat EN-01.jpg">
                        <img src="images/icons/MouwasatIcon.png" style="border: 0;" height="50px" width="150px" /></a></td>
                </tr>
            </table>
        </div>
    </div>

    <uc5:HealthArt ID="HealthArt1" runat="server" />
    <uc6:Apps ID="Apps1" runat="server" />
    <div class="botright" runat="server" id="Notify" visible="false">
        <table width="100%">
            <tr>
                <td valign="top">
                    <h3>Share your feedback ... </h3>
                </td>
                <td align="right" valign="bottom">
                    <asp:ImageButton ID="imgClose" ImageUrl="~/Member/images/close.png"
                        runat="server" Width="24" Height="24" Style="border: 0px"
                        OnClick="imgClose_Click" /></td>
            </tr>

            <tr>
                <td colspan="2">
                    <p>
                        Dear member, You have visited 
                    <asp:Label ID="lblProviderName" runat="server" Text=""></asp:Label>
                        last
                        <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                        .
                    </p>
                    <p>Click <a href="feedback/feedback.aspx" target="_blank">here</a> to rate your experience with the provider?</p>
                </td>
            </tr>
            <tr>

                <td align="right" colspan="2">
                    <img src="images/feedback.png" width="32" height="32" style="border: 0px"></td>
            </tr>
        </table>
<div id="dialog-message" title="Download complete">
  <p>
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
    Your files have downloaded successfully into the My Downloads folder.
  </p>
  <p>
    Currently using <b>36% of your storage space</b>.
  </p>
</div>
    </div>

    <script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
    <script type="text/javascript">
        twttr.conversion.trackPid('l50ok');</script>
    <noscript>
        <img height="1" width="1" style="display: none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l50ok&p_id=Twitter" />
        <img height="1" width="1" style="display: none;" alt="" src="//t.co/i/adsct?txn_id=l50ok&p_id=Twitter" />
    </noscript>
</asp:Content>

