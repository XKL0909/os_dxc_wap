﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true"
    Inherits="Member_MyCoverage" Codebehind="MyCoverage.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <style >
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>

  

 	<style type="text/css">
		body{
			font-family:Arial,Sans-serif;
			font-size:80%;
		}
		caption{
			padding-bottom:5px;
			
		}
		thead th,tfoot td{
			background:#e6f5ff; font-color:#ffffff;font-weight:bold;
		}
		tr.ruled{
			<%--background:#ccea68; font-weight:bold;--%>
		}    
		table{
			
			border-collapse:collapse;
		}
		th,td{
			
			border-collapse:collapse;
		}
		#mytable tr.ruled{
			<%--background:#333;
			color:#ccc;--%>
		}
    </style>
	<script type="text/javascript" src="../includes/tableruler.js"></script>
	<script type="text/javascript">
		window.onload=function(){tableruler();} 
    </script>


    



     <div  class="noPrint" > 
     
       <table width="100%">
        <tr valign="middle"><td >
        <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Coverage" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
        <td style="text-align: right">&nbsp;</td></tr>

        <tr><td></td><td></td></tr>
        </table>
        
     
        <table  class="t1"   style=" margin-right:0px;  "  width="100%" >
        <tr align="right"><td align="left"  bgcolor="transparent">
            <label ID="Label1" ><font size="4"></font></label>  </td>
            <td valign="bottom" style="height:53px;width:185px; " ><br /> 
                <a href="#" onclick="window.print()">
                &nbsp;</a></td></tr></table>
      </div>      
            
   
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />
 
  <div class="noPrint" >   
        <table style="width:100%">
            <tr>
                <td><aspAjax:TabContainer runat="server" ID="tabsModManager" BorderStyle="None" BackColor="AliceBlue"
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Font-Bold="True"
            Font-Size="Medium">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Font-Size="Medium" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Font-Size="Small" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="lblTabLoadStatus" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                                  <table>
                                    <tr>
                                        <td>
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" runat="server" Visible="false">
                            </asp:DropDownList></div></td><td>
                            <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details" /></td></tr></table>
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer></td>
            </tr>
            <tr>
                <td align="center"><div id="DivProgress" > 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div></td>
            </tr>
        </table>
       
</div>   
    <br /><br />

        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
               <fieldset>
                <legend><h1>Member Information</h1></legend>
            <table  class="contentTable" width="100%" >
           
                <tr class="grayRow">
                    <td style="width: 114px">
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server"   Text="3456998"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 114px" >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server" Text=""   ></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td style="width: 114px" >
                        <asp:Label ID="lblCustomer_Name" runat="server" Text="Customer Name" Width="140px"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblCustomerName" runat="server"    Text="30/05/1970"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 114px">
                        <asp:Label ID="lblClass_Name" runat="server" Text="Class Name"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblClassName" runat="server" Text=""   ></asp:Label></td>
                </tr>
          
            </table>
           </fieldset>
           
           <br /> 

           <fieldset>
                <legend><h1>My Coverage</h1></legend>
                <table width="100%">
                    <tr>
                        <td>
                        <div id="CoverageListReport"  visible="false" runat="server" >
      
       </div>
                        </td>
                    </tr>
                </table>
                </fieldset>
       

            
            
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="502px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
    <br />
    <br />
    <uc1:OSNav ID="OSNav1" runat="server" />
</asp:Content>
 <asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    View your policy benefit details
</asp:Content>