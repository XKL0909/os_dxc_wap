﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

public partial class Member_Default : System.Web.UI.Page
{
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("../MemberLogon.aspx");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /*** adding value for test ***/

        if (string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            Response.Redirect("../default.aspx");
        string diaRestrictedMembers = Utility.Configuration.CoreConfiguration.Instance.GetConfigValue("ShowDiabetesMemberID");
        if (diaRestrictedMembers.Length == 0)
        {
            // Should be shown for all members
            lnkDiabetesTracker.Visible = true;
        }
        else
        {
            // Restricted view - hide
            lnkDiabetesTracker.Visible = false;

            // Show for specified members only
            string[] restrictedList = diaRestrictedMembers.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            StringCollection list = new StringCollection();
            list.AddRange(restrictedList);


            string membershipNumber = Session["MembershipNo"].ToString();
            if (list.Contains(membershipNumber))
            {
                // Link should be shown for this member
                lnkDiabetesTracker.Visible = true;

            }

        }

        if (!Page.IsPostBack)
        {
            string str;
            string delimstr = "/";
            str = Page.Request.ServerVariables["HTTP_REFERER"];

            if (str == null)
            {
                //Response.Redirect("Default4.aspx");
            }
            else
            {
                string[] Arr;
                Arr = str.Split(delimstr.ToCharArray());

                str = Arr[Arr.Length - 1];
                //if (str != "Default4.aspx")
                //  Response.Redirect("Default4.aspx");
            }
        }
    }

    protected void btnDiabetes_Click(object sender, ImageClickEventArgs e)
    {
        string url = Utility.Configuration.CoreConfiguration.Instance.GetConfigValue("DiabetesTrackerURL");

        if (Session["MembershipNo"] == null)
            Response.Redirect("Default4.aspx");

        string membershipNumber = Session["MembershipNo"].ToString();
        membershipNumber = Utility.Helper.Security.EncryptDecryptQueryString.Encrypt("MemberID=" + membershipNumber, "2p0LaaPo");

        url = url + "?" + membershipNumber;
        Response.Redirect(url);
    }

    protected void Page_LoadComplete(object sender, System.EventArgs e)
    {

    }
}