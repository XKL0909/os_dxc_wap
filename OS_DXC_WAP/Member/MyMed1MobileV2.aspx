<%@ Page Language="C#" MasterPageFile="~/Templates/Mobile.Master"  AutoEventWireup="true" EnableEventValidation="false" Inherits="MyMed1MobileV2" Codebehind="MyMed1MobileV2.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>

<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    
    
		<script type='text/JavaScript' src='scw.js'></script>
  
    
    
            <asp:ScriptManager ID="ScriptManager1" runat="server" />


  
        
          <table width="100%">
        <tr valign="middle"><td >
        <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Medication" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
        <td style="text-align: right">&nbsp;</td></tr>
 
        <tr><td></td><td></td></tr>
        </table>
        
        <table width="100%">
        <tr>
            <td><asp:DropDownList ID="DDLDependentList" Width="80px" runat="server" Visible="false">
                            </asp:DropDownList>
                             </td>
            <td><asp:Button ID="btnRequestDependentCert" runat="server"  OnClick="btnRequestDependentCert_Click"
                                Text="View Details" /></td>
        </tr>
        
        </table>

    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="1">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size="1"> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
 
 
 
 
 
      <div id="CoverageListReport"  visible=false style=" font-family:Verdana; ; height:auto; width:100%; overflow:hidden;" runat="server" >
       </div>   
    <asp:UpdatePanel ID="upd1" runat="server" Visible="true"><ContentTemplate>
  <asp:Label ID="Message1" runat="server" ForeColor="red"  Width="450" Text=""></asp:Label>
   
   <asp:Panel ID="PnlUserInfo" Visible="true" runat="server" >
  <h1>Member Information</h1>
       <table class="eCommerceTable" width="100%">
           
                <tr class="grayRow">
                    <td>
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server" Text="3456998"></asp:Label></td>
                </tr>
                
                
                <tr style="display:none">
                    <td>
                        <asp:Label ID="lblClass_Name" runat="server" Text="Provider Name:"></asp:Label></td>
                    <td>
                          <div class="styled-select"><asp:DropDownList ID="DDLProviderList" ValidationGroup="1" style="font-size:12px; font-family:Arial;" runat="server" Visible="true">
                            </asp:DropDownList></div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="DDLProviderList"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                            <br />
                            
                            </td>
                </tr>
          
            </table>
         
</asp:Panel>

    
      <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
             OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
            Width="100%">
            
            <Columns>
                <asp:BoundField DataField="CompleteName" HeaderText="Service Description" >
                    <HeaderStyle /> 
                </asp:BoundField>
                <asp:BoundField DataField="period" HeaderText="Supply Period" >
                    <HeaderStyle /> 
                </asp:BoundField>
                <asp:BoundField DataField="DateFrom" HeaderText="Date From" >
                    
                </asp:BoundField>
                <asp:BoundField DataField="DateTo" DataFormatString="{0:d MMM yyyy }" HeaderText="Date To"
                    HtmlEncode="False">
                    
                    
                </asp:BoundField>
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" >
                    
                </asp:BoundField>
                <asp:BoundField DataField="Cost" HeaderText="Cost" >
                    <HeaderStyle /> 
                </asp:BoundField>
                <asp:TemplateField HeaderText="Request Refill" Visible="false">
                    <ItemTemplate>
                        <asp:CheckBox ID="Chec" runat="server"  Enabled="true" Checked="true" EnableViewState="true" />
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" SelectText="Edit" Visible="false">
                    
                </asp:CommandField>
            </Columns>
                
                
        </asp:GridView>

<asp:Panel ID="PanelEdit" runat="server" Visible="false">
        <table border="0">
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                </td>
                <td style="width: 100px">
                    <asp:TextBox CssClass="textbox" ID="txtName" Width="250px"  onfocus="this.blur();"  runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label6" runat="server" Text="Period"></asp:Label></td>
                <td style="width: 100px">
                    <div class="styled-select"><asp:DropDownList ID="ddlPeriod" runat="server" Width="257px" onChange="AddMonths(document.getElementById('txtDateOfEffect').value);" >
                        <asp:ListItem Value="1">1 Month</asp:ListItem>
                        <asp:ListItem Value="2">2 Month</asp:ListItem>
                        <asp:ListItem Value="3">3 Month</asp:ListItem>
                    </asp:DropDownList></div></td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label2" runat="server" Text="Date From"></asp:Label></td>
                <td style="width: 100px">
                   
                    
                  
                    
                      <asp:TextBox CssClass="textbox" ID="txtDateOfEffect"  runat="server"  Width="250px"      onclick="scwShow(this,event);"  style="cursor:default;"   BackColor="MistyRose"  ></asp:TextBox>
   

   
                    
                    
                </td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label3" runat="server" Text="Date To"></asp:Label></td>
                <td style="width: 100px">
                          <asp:TextBox CssClass="textbox" ID="txtDateTo"  Width="250px"  onfocus="this.blur();"    runat="server"     />
</td>
            </tr>
            <tr>
                <td style="width: 69px; height: 24px;">
                    <asp:Label ID="Label4" runat="server" Text="Quantity"></asp:Label></td>
                <td style="width: 100px; height: 24px;">
                    <asp:TextBox CssClass="textbox" ID="txtQuantity"  Width="250px"  Enabled="false" onfocus="this.blur();"   runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 69px">
                    <asp:Label ID="Label5" runat="server" Text="Cost"></asp:Label></td>
                <td style="width: 100px">
                    <asp:TextBox CssClass="textbox" ID="txtCost"  Width="250px"  onfocus="this.blur();"   runat="server"></asp:TextBox></td>
            </tr>
        </table> 
        <br />
        <asp:Button  ID="bntUpdate" runat="server" Text="Update Service" OnClick="bntUpdate_Click"  OnClientClick="AddMonths(document.getElementById('txtDateOfEffect').value);" />
           <asp:Button  ID="btnCancel" runat="server" Text="Cancel" OnClick="bntCancel_Click" />
        <br />

</asp:Panel>

<asp:Panel ID="PnlButtons" runat="server" Visible="false">
<br />
 <input type="button" name="Check_All" value="Check All" onclick="checkAll()" style="display:none" />
        <input type="button" name="Check_All" value="UnCheck All" onclick="UncheckAll()" style="display:none" />
    
    
                <p style="text-align:right;">
        <asp:Button  ID="btnSubmitAll"  runat="server" OnClick="btnSubmitAll_Click" Text="Submit Request"  ValidationGroup="1" style="display:none" 
           OnClientClick="javascript:return anyCheck1();"  />
         </p> 
    </asp:Panel>
    </ContentTemplate></asp:UpdatePanel>
    
    
    <div>

        &nbsp;&nbsp;
        <br />
      
       
        &nbsp;
     <asp:Label ID="lblErrorMessage" ForeColor="red" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
                <asp:Label  ID="lblTabLoadStatus" runat="server"></asp:Label>

        
       
          
            
        &nbsp; &nbsp; &nbsp; &nbsp;<br />          
                  <p style="display:none;">
    <asp:TextBox CssClass="textbox" Width="0%"   ID="txtNoOfChecked" runat="server"  ></asp:TextBox> 
    </p>
                 
  
  
  
  <asp:Panel ID="PnlHiddenVals" runat="server" Visible="false">
  <input id="txtAge" runat="server" />
  <input id="txtGender" runat="server" />
  <input id="txtCardNo" runat="server" />
  <input id="txtIqamaID" runat="server" />
  <input id="txtMobileNo" runat="server" />
  <input id="txtPolicy" runat="server" />
  <input id="Button2" type="button" onclick="javascript:var month = today.getMonth() + 1;alert(month);" value="1check daTE" title="date check" runat="server" />          

  </asp:Panel>
                  
                             
 
                           
 </div>

 <br />




<script language="JavaScript" type="text/javascript">
<!--



function anyCheck1() {
 var total = 0;
 var list = document.getElementsByTagName("input");
 for( var i in list){
  if( list[i].type=="checkbox" && list[i].checked )
   total=total+1;
 }
 if(total > 0)
 {
 //alert("You selected " + total + " boxes.");
 var txt1 = document.getElementById("txtNoOfChecked");
 txt1.value = total;
 return true;
  }
  else
  {
  alert("Please select atleast 1 Medication from the list.");
  return false;
  }
}





function DateAdd(objDate, strInterval, intIncrement)
    {
        if(typeof(objDate) == "string")
        {
            objDate = new Date(objDate);
 
            if (isNaN(objDate))
            {
                throw("DateAdd: Date is not a valid date");
            }
        }
        else if(typeof(objDate) != "object" || objDate.constructor.toString().indexOf("Date()") == -1)
        {
            throw("DateAdd: First parameter must be a date object");
        }
 
        if( strInterval != "M" && strInterval != "D" &&  strInterval != "Y" &&  strInterval != "h" &&  strInterval != "m" &&  strInterval != "uM" &&  strInterval != "uD" && strInterval != "uY" && strInterval != "uh" && strInterval != "um" && strInterval != "us" )
        {
            throw("DateAdd: Second parameter must be M, D, Y, h, m, uM, uD, uY, uh, um or us");
        }
 
        if(typeof(intIncrement) != "number")
        {
            throw("DateAdd: Third parameter must be a number");
        }
 
        switch(strInterval)
        {
            case "M":
            objDate.setMonth(parseInt(objDate.getMonth()) + parseInt(intIncrement));
            break;
 
            case "D":
            objDate.setDate(parseInt(objDate.getDate()) + parseInt(intIncrement));
            break;
 
            case "Y":
            objDate.setYear(parseInt(objDate.getYear()) + parseInt(intIncrement));
            break;
 
            case "h":
            objDate.setHours(parseInt(objDate.getHours()) + parseInt(intIncrement));
            break;
 
            case "m":
            objDate.setMinutes(parseInt(objDate.getMinutes()) + parseInt(intIncrement));
            break;
 
            case "s":
            objDate.setSeconds(parseInt(objDate.getSeconds()) + parseInt(intIncrement));
            break;
 
            case "uM":
            objDate.setUTCMonth(parseInt(objDate.getUTCMonth()) + parseInt(intIncrement));
            break;
 
            case "uD":
            objDate.setUTCDate(parseInt(objDate.getUTCDate()) + parseInt(intIncrement));
            break;
 
            case "uY":
            objDate.setUTCFullYear(parseInt(objDate.getUTCFullYear()) + parseInt(intIncrement));
            break;
 
            case "uh":
            objDate.setUTCHours(parseInt(objDate.getUTCHours()) + parseInt(intIncrement));
            break;
 
            case "um":
            objDate.setUTCMinutes(parseInt(objDate.getUTCMinutes()) + parseInt(intIncrement));
            break;
 
            case "us":
            objDate.setUTCSeconds(parseInt(objDate.getUTCSeconds()) + parseInt(intIncrement));
            break;
        }
       // 
       alert(objDate.toDateString());
        return objDate;
    }


//-->

</script>

<script language="javascript" type="text/javascript" src="jsDate.js"></script>

<script type="text/javascript">
<!--

function AddMonths(txt)
{

var DatePart = txt.split("/");
DatePart[0];
DatePart[1];
DatePart[2];

//alert (DatePart[1] + "/" + DatePart[0] + "/" + DatePart[2]);
var formatedDate = DatePart[1] + "/" + DatePart[0] + "/" + DatePart[2];
    var d = new Date(formatedDate);  //"04/10/2006"

    var noMonth = 27 * document.getElementById("ddlPeriod").value;
    d.setDate(d.getDate()+ noMonth);
  //  alert((d.getMonth()+1)+"/"+d.getDate()+"/"+d.getYear());
 document.getElementById("txtDateTo").value = d.getDate()+"/"+(d.getMonth()+1)+"/" + d.getYear();
//	alert( d.getDate()+"/"+(d.getMonth()+1)+"/" + d.getYear());
}

-->
</script>



 
<SCRIPT language="JavaScript">
<!-- 

<!-- Begin
function Check(chk)
{


var totalchkbox = 0;
 var list = document.getElementsByTagName("input");
 for( var i in list){
  if( list[i].type=="checkbox" )
   totalchkbox=totalchkbox+1;
}


if(document.getElementById("Check_All").value=='Check All'){
for (i = 0; i < totalchkbox; i++)
list[i].checked = true ;
document.getElementById("Check_All").value='UnCheck All';
}else{

for (i = 0; i < totalchkbox; i++)
list[i].checked = false ;
document.getElementById("Check_All").value="Check All";
}
}

// End -->
</script>


<SCRIPT LANGUAGE="JavaScript">
<!-- 

<!-- Begin

function checkAll()
{
var inputs = document.getElementsByTagName("input");
var checkboxes = [];
for (var i = 0; i < inputs.length; i++) {

  if (inputs[i].type == "checkbox") {
inputs[i].checked =true;
}
}
}

function UncheckAll()
{
var inputs = document.getElementsByTagName("input");
var checkboxes = [];
for (var i = 0; i < inputs.length; i++) {

  if (inputs[i].type == "checkbox") {
inputs[i].checked =false;
}
}
}
-->

</SCRIPT>



       



</asp:Content>