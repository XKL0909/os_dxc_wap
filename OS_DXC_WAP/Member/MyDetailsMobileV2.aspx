﻿<%@ Page Language="C#"  MasterPageFile="~/Templates/Mobile.master" AutoEventWireup="true" Inherits="MyDetailsMobileV2" Codebehind="MyDetailsMobileV2.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
  
    
    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
    
 
  
      <table width="100%">
        <tr valign="baseline"><td >
        <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Details" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
        <td style="text-align: right">&nbsp;</td></tr>
       
        <tr><td></td><td></td></tr>
        </table>
        
      
        
            
            

        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        

       
      
 
  <div class="noPrint" >   
        
       <table style="width:100%">
            <tr>
                <td>
                                                <table style="width:100%">
                                    <tr>
                                   
                                        <td>
                            <asp:DropDownList CssClass="DropDownListCssClass" Width="80px"
                                    ID="DDLDependentList" runat="server" AutoPostBack="True" 
                                    >
                            </asp:DropDownList></td>
                                   
                                        <td>
                            <asp:Button ID="btnRequestDependentCert"  runat="server" OnClick="btnRequestDependentCert_Click"
                                Text="View Details" /></td></tr>
                                                </table>

                </td>
            </tr>
            
       </table>
</div>   


        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
                
                <h1>Personal Information</h1>
            <table width="100%"  class="contentTable">
           
                <tr  class="grayRow">
                    <td class="contentTableHead">
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No " CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                 <tr >
                    <td  class="contentTableHead">
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name " CssClass="contentTableLabel" ></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td  class="contentTableHead">
                        <asp:Label ID="lblDate_Of_Birth" runat="server" Text="Date of Birth" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblDateOfBirth" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblGender_" runat="server" Text="Gender" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblGender" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td>
                        <asp:Label ID="lblType_" runat="server" Text="Type" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblType" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblNationalityT" runat="server" Text="Nationality" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblNationality" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td>
                        <asp:Label ID="lblEmployee_No" runat="server" Text="Employee No" 
                            CssClass="contentTableLabel"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblEmployeeNo" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblEmployee_ID" runat="server" Text="Saudi ID" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblEmployeeID" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td>
                        <asp:Label ID="lblSponsor_Id" runat="server" Text="Sponsor Id" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblSponsorId" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblCompany_Name" runat="server" Text="Company Name" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblCompanyName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td>
                        <asp:Label ID="lblMobileT" runat="server" Text="Mobile" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblMobile" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblEmailT" runat="server" Text="Email" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
          
            </table>
     
            <br />
               
                <h1>Policy Information</h1>

            <table class="contentTable" width="100%" >
           
                <tr class="grayRow">
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Customer No " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustID" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                 <tr >
                    <td >
                        <asp:Label ID="lblcustNameT" runat="server" Text="Customer Name " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td >
                        <asp:Label ID="lblcustSchemeT" runat="server" Text="Member Scheme" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustScheme" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblPlicysDateT" runat="server" Text="Policy Start Date" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblPlicysDate" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td>
                        <asp:Label ID="lblPlicyeDateT" runat="server" Text="Policy End Date" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblPlicyeDate" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
          
            </table>
            
            <br />
            <h1>CCHI Status</h1>
            <table  width="100%"  class="contentTable">
           
                <tr class="grayRow">
                    <td>
                        <asp:Label ID="Label26" runat="server" Text="CCHI Status " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblStatusP" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                 <tr >
                    <td >
                        <asp:Label ID="Label28" runat="server" Text="Reason " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblStatusReason" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
          
            </table>
 
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
     
   
           
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
   
</asp:Content>
