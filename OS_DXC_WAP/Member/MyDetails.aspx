﻿<%@ Page Language="C#"  MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" Inherits="MyDetails" Codebehind="MyDetails.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
  
    
    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
    <style type="text/css">
        #Content td{
	        font-family:arial;
	        font-size:12px;
	        color:#606060;
        }
        .th{
         font-weight:bold;
         color:#666;
         font-size:12px;
        }

        

@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
 
  
      <table width="100%">
        <tr valign="baseline"><td >
        <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Details" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
        <td style="text-align: right">&nbsp;</td></tr>
       
        <tr><td></td><td></td></tr>
        </table>
        
      
        <table width="100%" ><tr align="right"><td align=left>
            <label ID="Label1" ><font size=4></font></label>  </td>
            <td><a href="#" onclick="window.print()">
                <img class="noPrint" border="0" src="images/printer.gif" alt="Print" width="16" height="16" >
            </a> </td></tr></table>
            
            
   
       <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" />--%>

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }

 }


            function basicPopup(url) {
                url = url + document.getElementById('<%= hidQueryString.ClientID %>').value;
     popupWindow = window.open(url, 'popUpWindow', 'height=200,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
 }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />
 
  <div class="noPrint" >   
      
       <table style="width:100%">
            <tr>
                <td>
                <aspAjax:TabContainer runat="server" ID="tabsModManager" BorderStyle="None" BackColor="AliceBlue"
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Font-Bold="True"
            Font-Size="Medium">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Font-Size="Medium" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                       
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                     
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Font-Size="Small" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="lblTabLoadStatus" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                                <table>
                                    <tr>
                                        <td>

                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" runat="server" Visible="false">
                            </asp:DropDownList></div></td><td>
                            <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details" /></td></tr></table>
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer>
                </td>
            </tr>
            <tr>
                <td align="center">
                <div id="DivProgress" > 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
                </td>
            </tr>
       </table>
</div>   
    <br /><br />

        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
                <fieldset>
                <legend><h1>Personal Information</h1></legend>
            <table width="100%"  class="contentTable">
           
                <tr  >
                    <td class="contentTableHead">
                        <asp:HiddenField ID="hidQueryString" runat="server" />
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No " CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        <a href="#" onclick="basicPopup('editinfo.aspx?');">Edit Info</a></td>
                </tr>
                 <tr >
                    <td  class="contentTableHead">
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name " CssClass="contentTableLabel" ></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                     <td>
                         &nbsp;</td>
                </tr>
                <tr >
                    <td  class="contentTableHead">
                        <asp:Label ID="lblDate_Of_Birth" runat="server" Text="Date of Birth" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblDateOfBirth" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblGender_" runat="server" Text="Gender" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblGender" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblType_" runat="server" Text="Type" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblType" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblNationalityT" runat="server" Text="Nationality" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblNationality" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblEmployee_No" runat="server" Text="Employee No" 
                            CssClass="contentTableLabel"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblEmployeeNo" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblEmployee_ID" runat="server" Text="Saudi ID" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblEmployeeID" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblSponsor_Id" runat="server" Text="Sponsor Id" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblSponsorId" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblCompany_Name" runat="server" Text="Company Name" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblCompanyName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblMobileT" runat="server" Text="Mobile" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblMobile" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblEmailT" runat="server" Text="Email" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
          
            </table>
            </fieldset>
            <br />
                <fieldset>
                <legend><h1>Policy Information</h1></legend>

            <table class="contentTable" width="100%" >
           
                <tr >
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Customer No " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustID" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                 <tr >
                    <td >
                        <asp:Label ID="lblcustNameT" runat="server" Text="Customer Name " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td >
                        <asp:Label ID="lblcustSchemeT" runat="server" Text="Member Scheme" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustScheme" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblPlicysDateT" runat="server" Text="Policy Start Date" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblPlicysDate" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblPlicyeDateT" runat="server" Text="Policy End Date" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblPlicyeDate" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
          
            </table>
            </fieldset>
            <br />
            <fieldset>
                <legend><h1>CCHI Status</h1></legend>
            <table  width="100%"  class="contentTable">
           
                <tr >
                    <td>
                        <asp:Label ID="Label26" runat="server" Text="CCHI Status " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblStatusP" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                 <tr >
                    <td >
                        <asp:Label ID="Label28" runat="server" Text="Reason " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblStatusReason" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
          
            </table>
            </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
     
           
    <br />
    <br />
    <uc1:OSNav ID="OSNav1" runat="server" />
     
           
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    View your profile and your policy details
</asp:Content>
