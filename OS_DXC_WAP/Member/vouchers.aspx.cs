﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

public partial class Member_vouchers : System.Web.UI.Page
{
    public string img1 = "";
    public string img2 = "";
    public string img3 = "";
    public string strPartnerName = "";
    public string strMembershipNo = "";

    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        strMembershipNo = GetMemberID();
        img1 = Request["image1"];
        img2 = Request["image2"];
        img3 = Request["image3"];
        v1Memb.InnerHtml = strMembershipNo;
        v2Memb.InnerHtml = strMembershipNo;
        v3Memb.InnerHtml = strMembershipNo;

        if (!string.IsNullOrEmpty(img1))//Query string should pass image1
        {
            strPartnerName = img1.Replace(".png", "").Replace(".jpg", "").Replace(".jpeg", "");
            img1 = "images/voucher/" + img1;
            imgVoucher1.ImageUrl = img1;
            //btnPrint.Attributes.Add("onClick", "javascript: document.getElementById('btnPrint').style.visibility = 'hidden';self.print();return true;");
        }
        if (!string.IsNullOrEmpty(img2))
        {
            img2 = "images/voucher/" + img2;
            imgVoucher2.ImageUrl = img2;
        }
        else
        {
            imgVoucher2.Visible = false;
            v2Div1.Visible = false;
            v2Div2.Visible = false;
        }

        if (!string.IsNullOrEmpty(img3))
        {
            img3 = "images/voucher/" + img3;
            imgVoucher3.ImageUrl = img3;
        }
        else
        {
            imgVoucher3.Visible = false;
            v3Div1.Visible = false;
            v3Div2.Visible = false;
        }
        
    }

    protected string getImg(string _img)
    {
        return "Cambridge-.png";
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        char charSeries = 'E';
        Int32 intMembershipNo = Convert.ToInt32(Session["MembershipNo"]);
        var VoucherNo = GenerateVoucherNo(strPartnerName, charSeries, intMembershipNo);
        v1No.InnerHtml = VoucherNo;
        v2No.InnerHtml = VoucherNo;
        v3No.InnerHtml = VoucherNo;
        btnPrint.Visible = false;
    }

    public string GetMemberID()
    {
        return Convert.ToString(Session["MembershipNo"]);
    }

    public string GenerateVoucherNo(string strPartnerName, char charSeries, Int32 intMembershipNo)
    {
        SqlConnection Con = null;
      try
        {
            string strVoucherNo = "";
            Con = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
             
            SqlParameter[] arParms = new SqlParameter[4];
            arParms[0] = new SqlParameter("@Series", SqlDbType.NVarChar, 1);
            arParms[0].Value = charSeries;

            arParms[1] = new SqlParameter("@PartnerName", SqlDbType.NVarChar, 50);
            arParms[1].Value = strPartnerName;

            arParms[2] = new SqlParameter("@MembershipNo", SqlDbType.BigInt);
            arParms[2].Value = intMembershipNo;

            // @strVoucherNo Output Parameter
            arParms[3] = new SqlParameter("@strVoucherNo", SqlDbType.NVarChar, 8);
            arParms[3].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(Con, CommandType.StoredProcedure, "[GetVoucherNo]", arParms);
            Con.Close();

            if (!string.IsNullOrEmpty(Convert.ToString(arParms[3].Value)))
                strVoucherNo = Convert.ToString(arParms[3].Value);
                
            return strVoucherNo;
        }
      catch
            {
               return "0";
            }

    }


}