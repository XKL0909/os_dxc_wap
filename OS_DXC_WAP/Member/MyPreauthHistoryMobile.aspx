﻿<%@ Page Language="C#" AutoEventWireup="true"  EnableEventValidation="false"
    Inherits="MyPreauthHistory" Codebehind="MyPreauthHistoryMobile.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
       <meta http-equiv="Page-Enter" content="progid:DXImageTransform.Microsoft.Wipe(
        GradientSize=1.0, wipeStyle=1, motion='forward', duration=5)">
<meta http-equiv="Page-Exit" content="progid:DXImageTransform.Microsoft.Pixelate(duration=7)">


    

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}
.Tab .ajax__tab_header
{
    color: #000000;
    font-family:Calibri;
    font-size: 24px;
    font-weight: bold;
    background-color: #ffffff;
    margin-left: 10px;
    border:1px solid #b4cbdf;
    
}
/*Body*/
.Tab .ajax__tab_body
{
    border:1px solid #b4cbdf;
    padding-top:0px;
}
/*Tab Active*/
    </style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="styles.css" type="text/css" />
    
    <script type="text/javascript" src="scriptMobile/jquery.min.js"></script>
  <script type="text/javascript" src="scriptMobile/jquery-ui.min.js"></script>
	<!-- include smoothness jQueryUI theme -->
	<link rel="stylesheet" type="text/css" href="styleMobile/jquery-ui.css">
  
  <style type="text/css">
	body {
		font-family:Arial,Geneva,Verdana,sans-serif;
		font-size: 0.8em;
		overflow-x: hidden;
	}
	#loadingScreen {
		background: url(images/loading.gif) no-repeat 5px 8px;
		padding-left: 25px;
	}
	/* hide the close x on the loading screen */
	.loadingScreenWindow .ui-dialog-titlebar-close {
		display: none;
	}
      .style1
      {
          width: 70px;
          height: 70px;
      }
  </style>
  <script type="text/javascript">
	$(document).ready(function() {
		waitingDialog({});
		setTimeout(closeWaitingDialog, 2000);
 
	});
  </script>
</head>
<body topmargin="0px">

    <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Arabia Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    
     <table width="100%">
        <tr valign="middle">
        
        <td >
        <table>
            <tr>
                <td valign="middle">
                     <img class="style1" src="../Icons/PreauthHistory.png" /></td>
                <td valign="middle">
            <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Pre-Authorisation History" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
            </tr>
        </table>
        
            </td>
         </tr>
    
        <tr valign="middle"><td >
            <img  src="../Icons/line.jpg" height="5" width="100%"  /></td>
         </tr>
    
        </table>
      
        
            
            
   
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />
 
  <div class="noPrint" >   
        
       <aspAjax:TabContainer runat="server" ID="tabsModManager" CssClass="Tab"
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Font-Bold="True"
            Font-Size="Medium">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Font-Size="Medium" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Font-Size="Small" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="lblTabLoadStatus" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" runat="server" Visible="false">
                            </asp:DropDownList></div>
                            <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details for Dependent" />
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer>
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
             <br /> 
            <table  >
           
                <tr>
                    <td>
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server"></asp:Label></td>
                </tr>
             </table>         
       <div id="CoverageListReport"   visible=false style=" font-family:Arial;font-size:16px; vertical-align:text-top ; height:auto; width:100%; overflow:visible;" runat="server" >
       <img src="file:///C:\Downloads\imagemap-1291\AJAX_ImageMap\animated_loading.gif" />
       </div>

            
            
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
       
           

        
           
       </form> 
        
   

</body>
</html>
