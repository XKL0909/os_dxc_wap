﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Member_editinfo" Codebehind="editinfo.aspx.cs" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>

    <style type="text/css">
        .style1
        {
            text-align: center;
        }
    </style>
    
    <script type="text/javascript">
    window.onunload = refreshParent;
    function refreshParent() {
        window.opener.location.reload();
    }


    </script>

     <script type="text/javascript">
         // to allow numbers only in the numbers text boxes
         function allownumbers(e) {
             var key = window.event ? e.keyCode : e.which;
             var keychar = String.fromCharCode(key);
             var reg = new RegExp("[0-9.]")
             if (key == 8) {
                 keychar = String.fromCharCode(key);
             }
             if (key == 11) {
                 key = 8;
                 keychar = String.fromCharCode(key);
             }
             return reg.test(keychar);
         }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td colspan="2">Member No\Name: 
                    <asp:Label ID="lblMemNo" runat="server" Text="Label"></asp:Label>
                    <dx:ASPxLabel ID="lblMemName" runat="server" Text="ASPxLabel">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td class="style1">Mobile Number: </td>
                 <td>
                     <asp:TextBox ID="txtMobile" runat="server" Width="170px" MaxLength="12" onkeypress="javascript:return allownumbers(event);">
                     </asp:TextBox>
                     <asp:RequiredFieldValidator ID="rfv_mobile" runat="server" 
                         Display="Dynamic" SetFocusOnError="true" ErrorMessage="* Required Field" ControlToValidate="txtMobile" ForeColor="Red"></asp:RequiredFieldValidator>
                     <br />
                   <i style="font-size:small; color:red;"> (966xxxxxxxxx or 971xxxxxxxxx or 05xxxxxxxx)</i>
                </td>
            </tr>
            <tr>
                <td class="style1">Email Address</td>
                 <td>
                    <asp:TextBox ID="txtEmail" runat="server" Width="170px"></asp:TextBox>
                     <asp:RegularExpressionValidator ID="rev_txtMemberEmail" runat="server" 
                         ErrorMessage="* Invalid email address." ControlToValidate="txtEmail" 
                         ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                         Display="Dynamic" SetFocusOnError="true"></asp:RegularExpressionValidator>
                     
                </td>
                
            </tr>
            <tr>
                <td class="style1">&nbsp;</td>
                 <td align="right">
                     <dx:ASPxButton ID="btnSubmit" runat="server" 
                         Text="Submit" onclick="btnSubmit_Click">
                     </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td class="style1" colspan="2">
                    <dx:ASPxLabel ID="Message1" runat="server" Text="" ForeColor="Red" 
                        style="font-weight: 700">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td  colspan="2" align="left">
                   <a href="javascript:self.close();">Close</a></td>
            </tr>
        </table>
    </div>
        <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

    </form>
</body>
</html>
