﻿<%@ Page Title=""  Language="C#" MasterPageFile="~/Templates/Inner.master" Inherits="Member_ReimbursementForm" Codebehind="ReimbursementForm.aspx.cs" %>

<%@ Register src="../UserControls/ControlReimbursementForm.ascx" tagname="Form" tagprefix="uc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
        .labelClass {
            float: left;
            margin-right: 10px;
            line-height: 30px;
            width: 160px;
        }

        .smallLabelClass {
            float: left;
            margin-right: 10px;
            line-height: 30px;
            width: 106px;
        }

        .controlClass {
            float: left;
            margin-right: 10px;
        }

        .clear {
            clear: both;
        }

        .containerClass {
            height: 60px;
            width: 100%;
        }

        .controlClass input, .controlClass select {
            width: 260px;
        }

        input[type=radio] {
            width:30px;
        }

        .headerBox {
            float:left;
            width:45%;
        }
        .headerBox .labelClass {
            width: 106px;
        }
        
    </style>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">




    <table width="100%">
        <tr>
            <td style="width:70%">
                <h2>Submit reimbursement claims</h2>
            </td>
            <td style="text-align:left;width:30%">
                <a href="../Docs/ReimbursementClaimHelp.pdf" target="_blank">How to make a reimbursement claim?</a>
                <br />
                <br />
                <a href="../Docs/Bupa-Claim-Form-1Feb2011.pdf" target="_blank">Download Claim Form</a>
           </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:Form ID="Form1" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <td colspan="3">
                            <uc2:OSNav ID="OSNav1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>



</asp:Content>

