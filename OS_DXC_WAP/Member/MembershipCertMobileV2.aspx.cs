﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class MembershipCertMobileV2 : System.Web.UI.Page 
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;

    protected void Page_PreInit(object sender, EventArgs e)
    {
       
    } 

    protected void Page_Load(object sender, EventArgs e)
    {
       
            strMembershipNo = Request["MembershipNo"];
       

        if (!IsPostBack)
            
            getMemberList(strMembershipNo);

        
            frm.Visible = true;
       
        //DisplayMemberCertResult(strMembershipNo);


    }

    // function used for Tab System when user requset for dependent ceritifcate


    // function used for Tab System to retrieve family list
    private void DisplayMemberListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        if (response.status == "0")
        {
            cbMemberList.Items.Clear();


            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

                //ListItem DepList = new ListItem();
                //cbMemberList.TextField = dtl.memberName + " -- " + dtl.relationship;
                //cbMemberList.ValueField = dtl.membershipNo.ToString();
                cbMemberList.Items.Add(dtl.memberName + " -- " + dtl.relationship, dtl.membershipNo.ToString());
                //cbMemberList.Items.Add(
                /*DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                cbMemberList.Items.Add(DepList);*/
            }
        }
    }


    // function used for Tab System when Tab1 [member tab] is clicked
    protected void btnTab1_Click(object sender, EventArgs args)
    {
        //////      if (TabFlag != "M")
        DisplayMemberCertResult(strMembershipNo);
        //.Text = "test completed";
    }

    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void getMemberList(string strMembershipNo)
    {
        {
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

            OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
            OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
            request.membershipNo = strMembershipNo;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();

            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.EnqMbrListInfo(request);
                if (response.errorID != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    lblErrorMessage.Text = sb.ToString();
                }
                else
                {
                    DisplayMemberListResult(response);
                    String XmlizedString = null;
                    MemoryStream memoryStream = new MemoryStream();
                    XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                    xmlTextWriter.Formatting = Formatting.Indented;
                    xs.Serialize(xmlTextWriter, response);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                    XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                    this.txtBox_output.Text = XmlizedString;
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }




        }
    }
    private void DisplayMemberCertResult(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response;

        

        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN();
            request.membershipNo = long.Parse(MemberNo);

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();

            request.memberLoginIND = "Y";

            response = ws.ReqPrtMbrCrtInfo(request);

            if (response.memberName != null)
            {
                DisplayMbrCertInfo(response);
                Message1.Visible = false;

            }
            else
            {

                divCer.Visible = false;
                if (response.errorID[0] == "E3031")
                {
                    lblErrID.Text = "E3031";
                    Message1.Text = response.errorMessage[0];
                }
                else
                {

                    Message1.Text = response.errorMessage[0];
                }
            }


        }
        catch (Exception ex)
        {
            divCer.Visible = false;
            Message1.Text = ex.Message; /// this message would be used by transaction
        }




    }


    private void DisplayMbrCertInfo(OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response)
    {


        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {

            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request1;
            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response1;
            
            OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN request2;
            OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsResponse_DN response2;

            OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN request3;
            OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN response3;

            request1 = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            request1.membershipNo = Convert.ToString(response.membershipNo.ToString());

            request1.Username = WebPublication.CaesarSvcUsername;
            request1.Password = WebPublication.CaesarSvcPassword;
            request1.transactionID = WebPublication.GenerateTransactionID();
            response1 = ws.EnqMbrDetInfo(request1);

            request2 = new OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN();
            request2.contractNo = response1.detail[0].custNo;
            request2.membershipNo = Convert.ToString(response.membershipNo.ToString());
            request2.Username = WebPublication.CaesarSvcUsername;
            request2.Password = WebPublication.CaesarSvcPassword;
            request2.transactionID = WebPublication.GenerateTransactionID();
            request2.providerCode = "";

            response2 = ws.ReqTOB(request2);

            

            response3 = null;
            request3 = new OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN();
            request3.membershipNo = Convert.ToString(response.membershipNo.ToString()); ;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request3.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request3.Username = WebPublication.CaesarSvcUsername;
            request3.Password = WebPublication.CaesarSvcPassword;

            response3 = ws.EnqProdCovInfo(request3);


            if (response3.status == "0")
                lblamount.Text = response3.proratedOverallAnnualLimit.ToString("C").Replace("£", "");

            lblCustName.Text = response1.detail[0].customerName;
            lblCustNo.Text = response1.detail[0].custNo;
            lbllCover.Text = response1.detail[0].schemeName;
            lblMemberName.Text = response.memberName;
            lblmemName.Text = response.memberName;
            lblMembershipNo0.Text = response.membershipNo.ToString();
            lblMembershipNo.Text = response.membershipNo.ToString();
            lblClass.Text = response.className;
            lblOptions.Text = response.option;
            lblIssueDate.Text = DateTime.Now.ToShortDateString();
            lblEffectiveFrom.Text = String.Format("{0:d}", response.effFrom);
            lblCustomerName.Text = response.customerName;
            lblDateOfBirth.Text = String.Format("{0:d}", response.DOB);
            lblDateOfBirth0.Text = String.Format("{0:d}", response.DOB);
            lblStaffNo.Text = response.staffNo;
            lblContribution.Text = response.deductible;
            lblEffectiveTo.Text = String.Format("{0:d}", response.effTo);
            lblEffectiveTo0.Text = String.Format("{0:d}", response.effTo);
            lblRoomType.Text = response.roomType;
            divCer.Visible = true;

        }
        else
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);



    }


    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }



    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        Session["v"] = cbMemberList.SelectedItem.Value.ToString();
        DisplayMemberCertResult(cbMemberList.SelectedItem.Value.ToString());
        Session["Opt"] = cmOption.Value.ToString();
        switch (cmOption.Value.ToString())
        {
            case "1":
                lblToHow.Visible = true;
                lblToHowAr.Visible = true;
                lblSubject.Text = "Certificate of Membership";
                lblSubAr.Text = "شهادة عضوية";
                lblHospital2.Visible = true;
                lblHospital1.Visible = true;
                divCer.Visible = true;
                visa.Visible = false;
                lblTitle.Visible = true;
                printimg.Visible = true;
                break;
            case "2":
                lblToHow.Visible = false;
                lblToHowAr.Visible = false;
                lblSubject.Text = "Membership certificate for visa application";
                lblSubAr.Text = "شهادة عضوية لطلب التأشيرة";
                lblHospital2.Visible = false;
                lblHospital1.Visible = false;
                divCer.Visible = false;
                visa.Visible = true;
                lblTitle.Visible = false;
                printimg.Visible = true;

                break;
        }
    }

    
}