using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class MyDetails : System.Web.UI.Page 
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
   

    protected void Page_Load(object sender, EventArgs e)
    {
        string strMembershipNo;
        if (!Page.IsPostBack)
        {
            if (Session["MembershipNo"] != null)
            {
                strMembershipNo = Session["MembershipNo"].ToString();
                DisplayMemberCertResult(strMembershipNo);
            }
            else
            {
                Response.Write("Session Timed out. Please log in again");
                Response.Write("<script>window.open('../default.aspx','_parent');</script>");

            }
        }
    }

    private void DisplayDependentListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        
        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            DDLDependentList.Items.Clear();

            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

                ListItem DepList = new ListItem();
                DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                DDLDependentList.Items.Add(DepList);

            }
        }
        else
        {

        }

    }


    protected void btnTab1_Click(object sender, EventArgs args)
    {
        string strMembershipNo = string.Empty;
        if (Session["MembershipNo"] != null)
        {
            strMembershipNo = Session["MembershipNo"].ToString();
        }
        DisplayMemberCertResult(strMembershipNo);
    }

    protected void btnTab2_Click(object sender, EventArgs args)
    {
        {
            
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        
            string strMembershipNo = string.Empty;
        if (Session["MembershipNo"] != null)
        {
            strMembershipNo = Session["MembershipNo"].ToString();
        }

        request.membershipNo = strMembershipNo;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.EnqMbrListInfo(request);
            if (response.errorID != "0")
            {
                sb.Append(response.errorMessage).Append("\n");
                lblErrorMessage.Text = sb.ToString();
            }
            else
            {
                DisplayDependentListResult(response);

                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlTextWriter.Formatting = Formatting.Indented;
                xs.Serialize(xmlTextWriter, response);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                this.txtBox_output.Text = XmlizedString;
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text =ex.Message;
        }

        if (DDLDependentList.Items.Count > 0)
        {
            DDLDependentList.Visible = true;
            this.lblTabLoadStatus.Visible = false;
            btnRequestDependentCert.Visible = true;
        }
        else
            this.lblTabLoadStatus.Text = "No dependent for this Member Found";
     }
   
    }


    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
        DisplayMemberCertResult(DDLDependentList.SelectedValue);
    }


    private void DisplayMemberCertResult(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            request.membershipNo = MemberNo;
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); 
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqMbrDetInfo(request);

            if (response.detail[0].memberName != null)
            {
                DisplayMbrDetInfo(response);
                if (tabMemberName.HeaderText == "Member")
                {
                    tabMemberName.HeaderText = response.detail[0].memberName;
                }

            }
            else
            {
                UpdatePanel2.Visible = false;
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            UpdatePanel2.Visible = false;
             tabsModManager.Visible = false;

            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Invalid Member error. The member might be inactive or terminated.";// response.errorMessage[0].ToString();

        }

}


    private void DisplayMbrDetInfo(OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response)
    {
        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {
            lblMemberName.Text = response.detail[0].memberName;
            lblMembershipNo.Text = response.detail[0].membershipNo.ToString();
            lblCompanyName.Text = response.detail[0].companyName;
            lblEmployeeID.Text = response.detail[0].memberID;
            lblMobile.Text = response.detail[0].mobileNo;
            lblGender.Text =  response.detail[0].gender;
            lblDateOfBirth.Text = String.Format("{0:d}", response.detail[0].memberDOB);
            lblEmployeeNo.Text = response.detail[0].employeeNo;
            lblType.Text = response.detail[0].memberTypeDescription;
            lblEmail.Text =response.detail[0].email;
            lblSponsorId.Text = response.detail[0].sponsorID;
            lblNationality.Text = "Caeser did't retrive the nationality"; 

            lblcustID.Text = response.detail[0].custNo;
            lblcustName.Text = response.detail[0].customerName;
            lblcustScheme.Text = response.detail[0].schemeName;
            lblPlicysDate.Text = String.Format("{0:d}", response.detail[0].startDate);
            lblPlicyeDate.Text = String.Format("{0:d}", response.detail[0].endDate);
            lblNationality.Text = response.detail[0].countryName;
            lblStatusP.Text = response.detail[0].CCHIStatus;
            lblStatusReason.Text = response.detail[0].CCHIRejReason;

            hidQueryString.Value = "memberName=" + response.detail[0].memberName + "&" +
                                  "membershipNo=" + response.detail[0].membershipNo + "&" +
                                  "email=" + response.detail[0].email + "&" +
                                  "mobileNo=" + response.detail[0].mobileNo;
            //it should not be updated in case of dependencies
            //Session["memberno"] = response.detail[0].membershipNo.ToString();
            //Session["email"] = response.detail[0].email;
            //Session["membername"] = response.detail[0].memberName;
            //Session["mobile"] = response.detail[0].mobileNo;
            
            UpdatePanel2.Visible = true;

        }
        else
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);



    }

  
    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }




    
}