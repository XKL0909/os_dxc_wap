﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Member_Default" Codebehind="Default.aspx.cs" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
        <div class="sectionTitle">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><h1>Scheme Details</h1></td>
                <td align="right">&nbsp;</td>
            </tr>
        </table>
        </div>
        <div>
            <div class="userLinks">
            <table cellpadding="4" cellspacing ="1" >
                <tr>
                    <td align="left" class="boxcaption"><a href="MyDetails.aspx" ><img id="Img2" runat="server" border="0" 
                            src="~/images/icons/member/My Details.jpg"  alt="My Details" title="My Details" 
                            style="height: 85px; width:70px"/></a><br />
                        My Details</td>
                    <td  align="left" class="boxcaption"><a href="MyCoverage.aspx"><img id="Img3" runat="server" border="0" src="~/images/icons/member/My Coverage.jpg" alt="My Coverage" title="My Coverage" style="height: 85px; width:70px"/></a><br />
                        My Coverage</td>
                    <td  align="left" class="boxcaption"><a href="MyMed1.aspx"><img id="Img4" runat="server" 
                            style="height: 85px; width:70px" border="0" 
                            src="~/images/icons/member/My Medication.jpg" alt="My Medication" 
                            title="My Medication" onclick="return Img4_onclick()" /></a><br />
                        My Medication </td>
                    <td  align="left" class="boxcaption"><a href="MyNetwork.aspx"><img id="Img5" runat="server" 
                            style="height: 85px; width:70px" border="0" 
                            src="~/images/icons/member/My Network.jpg" alt="My Network" 
                            title="My Network"/></a><br />
                        My Network </td>
                </tr>
                </table></div>
            <div class="sectionTitle">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><h1>Pre authorizations / Claims History</h1></td>
                <td align="right">&nbsp;</td>
            </tr>
        </table>
        </div>
            <div class="userLinks">

        <table cellpadding="4" cellspacing ="1" >
                <tr>
                    <td align="left" class="boxcaption"><a href="ClaimHistory.aspx"><img id="Img7" runat="server" 
                            border="0" style="height: 85px; width:70px" src="~/images/icons/common/ClaimHistory.jpg" alt=" Claim History" 
                            title=" Claim History"/></a><br />
                        Claim History</td>
                    <td align="left" class="boxcaption"><a href="MyPreauthHistory.aspx"><img id="Img8" 
                            style="height: 85px; width:70px" runat="server" border="0" 
                            src="~/images/icons/common/Pre-authhistory.jpg" alt="Preauth History" 
                            title="Preauth History"/></a><br />
                        Preauth History </td>
                    <td align=left class="boxcaption"><a href="reimbursementclaims.aspx"><img id="Img21" runat="server" 
                            border="0" src="~/images/icons/common/ClaimHistory.jpg" 
                            style="height: 85px; width:70px"/></a><br />
                        Submit reimbursement</td>
                    <td align="center" style="width:*">&nbsp;</td>
                </tr>
                </table>
            
            

        </div>
            <div class="sectionTitle">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><h1>Other Services</h1></td>
                <td align="right">&nbsp;</td>
            </tr>
        </table>
        </div>
            <div class="userLinks">
            
            <table  cellpadding="4" cellspacing ="1" >
                <tr>
                    <td align="left" class="boxcaption"><a href="MembershipCert2.aspx"><img id="Img10" runat="server" 
                            style="height: 85px; width:70px" border="0" 
                            src="~/images/icons/member/Membership Certificate.jpg" 
                            alt="Certificate of Membership" title="Certificate of Membership"/></a><br />
                        Certificate</td>
                    <td align="left" class="boxcaption"> <a id="lnkDiabetesTracker"  runat="server" href="" style="border-top-width: 0px; border-left-width: 0px;
                                        font-size: 11px; border-bottom-width: 0px; color: teal; font-family: Verdana;
                                        border-right-width: 0px; text-decoration: none;" rel="lyteframe" title="" rev="width: 650px; height: 480px; scrolling: yes;" 
                                        visible="false" target="_blank">
                                        <asp:ImageButton ID="btnDiabetes" 
                            style="height: 85px; width:70px" runat="server" Visible="true"
                                        ImageUrl="~/images/icons/member/Diabetes Tracker.jpg" 
                            onclick="btnDiabetes_Click" ToolTip="Diabetes Tracker" />
                                        </a>
                        <br />
                        Diabetes Tracker  
                    </td>
                </tr>
                </table>


            
            
           
        
        </div>
        </div>
        </td>
    </tr>
</table>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function Img4_onclick() {

        }

// ]]>
    </script>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p>
        As a registered member, our online service allows you to view your profile and 
        the details of your policy benefits, print your membership certificate, view 
        your claims and pre-authorisation history, and view and print your latest 
        reimbursement statement.</p>
</asp:Content>

