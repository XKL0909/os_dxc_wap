<%@ Page Language="C#" AutoEventWireup="true" Inherits="Member_PreauthDetails" Codebehind="PreauthDetails.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
   <meta http-equiv="Page-Enter" content="progid:DXImageTransform.Microsoft.Wipe(
        GradientSize=1.0, wipeStyle=1, motion='forward', duration=5)">
<meta http-equiv="Page-Exit" content="progid:DXImageTransform.Microsoft.Pixelate(duration=7)">
 
    
    <style type="text/css" title="currentStyle">
			@import "http://localhost:54468/AJAXEnabledWebSite3/js/demos.css";
		</style>
		
		<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    
    
   <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
    
    
    
</head>
<body>
  <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Arabia Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    
      
        <table width=100% ><tr><td align=left>
            <label ID="Label1" ><font size=4>Pre-Authorisation Details</font></label>  </td>
            <td><a href="#" onclick="window.print()">
                <img class="noPrint" border="0" src="images/printer.gif" alt="Print" style="height: 33px"></a>
             
                  <img alt="Go back" style="cursor:pointer;" src="../icons/back4.png" onClick="history.go(-1)" height="33px" />&nbsp;
 </td></tr></table>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        
  <%--       <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <%=DateTime.Now.ToString() %>
            <asp:Panel ID="Panel2" runat="server">
                <asp:Image ID="Image2" runat="server" ImageUrl="spinner.gif" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
<asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel2"  ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <img src="spinner.gif" width="33" height="33" />
                Updating Page ......
            </ProgressTemplate>
        </asp:UpdateProgress>
     
        <asp:UpdatePanel ID="UpdatePanel2" runat="server"  Visible="true">
        <ContentTemplate>
            <table border="0" cellpadding="2" cellspacing="2" width="550px">
                <tr>
                    <td style="width: 463px">
                        <fieldset style="border-right: medium none; border-top: medium none; border-left: medium none;
                            border-bottom: medium none">
                            <table border="0" cellpadding="2" cellspacing="2" style="font-size: 11px; font-family: Verdana; width: 155%; vertical-align: top;">
                                <tr>
                                    <td align="left" valign="top" class="th">
                                        Provider Name:</td>
                                    <td align="left">
                                        <asp:Label ID="lblProviderName" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        <nobr>Insured Name:</nobr></td>
                                    <td align="left" colspan="3">
                                        <asp:Label ID="lblInsuredName" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        ID. Card:</td>
                                    <td align="left">
                                        <asp:Label ID="lblIDCard" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="th">
                                        Insurance Co:</td>
                                    <td align="left">
                                        <asp:Label ID="lblInsurance_Co" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Age:</td>
                                    <td align="left">
                                        <asp:Label ID="lbAge" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Sex:</td>
                                    <td align="left">
                                        <asp:Label ID="lblSex" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        Policy Holder</td>
                                    <td align="left">
                                        <asp:Label ID="lblPolicyHolder" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="th">
                                        <nobr>Patient File No.:</nobr></td>
                                    <td align="left">
                                        <asp:Label ID="lblPatientFileNo" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Dept</td>
                                    <td align="left">
                                        <asp:Label ID="lblDept" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Policy No.:</td>
                                    <td align="left">
                                        <asp:Label ID="lblPolicyNo" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        Class</td>
                                    <td align="left">
                                        <asp:Label ID="lblClass" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="th">
                                        Provider Fax No.:</td>
                                    <td align="left">
                                        <asp:Label ID="lblProviderFaxNo" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Date of Visit</td>
                                    <td align="left">
                                        <asp:Label ID="lblDateOfVisit" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Expiry Date</td>
                                    <td align="left">
                                        <asp:Label ID="lblExpiryDate" runat="server" Text=" "></asp:Label></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="width: 463px">
                        <fieldset style="border-right: medium none; border-top: medium none; border-left: medium none;
                            border-bottom: medium none; width: 714px;">
                            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                <tr>
                                    <td>
                                        <b><font style="font-size: 11px"><span style="font-size: 9pt; font-family: Verdana">
                                            Reference to your pre-authorisation request for our
                                            member (details listed above). We, BUPA Arabia replying on the membership &amp;
                                            limited medical information supplied provided in your request took the decision
                                            mentioned below </span></font></b>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="width: 463px">
                        <fieldset style="border-right: medium none; border-top: medium none; border-left: medium none;
                            border-bottom: medium none; font-size: 10px; width: 715px; font-family: Verdana;">
                            <table border="0" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td align="left" colspan="3">
                                        <b>Pre-authorisation Status:</b>&nbsp; 
                                        <asp:Label ID="lblPreauthorisationStatus" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        Approval No.:</td>
                                    <td align="left">
                                        <asp:Label ID="lblApprovalNo" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th" width="17%">
                                        Approval Validity:</td>
                                    <td align="left">
                                        <asp:Label ID="lblApprovalValidity" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        Room Type:</td>
                                    <td align="left">
                                        <asp:Label ID="lblRoomType" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="6">
                                        <u><b>Comments:</b></u> <asp:Label ID="lblComments" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="th">
                                        <u>Service Required:</u></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="7" valign="top">
                                <div id="ServiceListReport"  visible="true" style=" font-family:Verdana; ; height:auto; width:703px; overflow:hidden;" runat="server" >   </div>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="6">
                                        <b><u>Additional Comments</u></b> &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3">
                                        <b>Insurance Officer</b>&nbsp; 
                                        <asp:Label ID="lblInsuranceOfficer" runat="server" Width="152px"></asp:Label></td>
                                    <td>
                                    </td>
                                    <td colspan="3" style="width: 208px">
                                        <b>Date &amp; Time</b> 
                                        <asp:Label ID="lblDateTime" runat="server" Width="152px"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset style="border-right: medium none; border-top: medium none; border-left: medium none;
                            border-bottom: medium none; width: 716px;">
                            <div align="left">
                                <font style="font-size: 10px"><b><span style="font-family: Verdana">Kindly note:&nbsp; BUPA Arabia confirms cover
                                    for the member's treatment as specified in the coverage details field based on the
                                    limited information supplied to us during pre-authorisation. BUPA Arabia reserves
                                    the right to fully or partially deny the payment for any of the above treatment
                                    during the claim processing stage in case one of the following reasons (which does
                                    not constitute a numerous clauses of events) becomes apparent: 
                                </span></b>
                                    <ol type="1">
                                        <li><span style="font-family: Verdana">If the diagnosis, treatment or any other material fact alters from those disclosed
                                            during pre-authorisation </span></li>
                                        <li><span style="font-family: Verdana">If the line of treatment is not according to internationally recognised medical
                                            standards and in line with the M.O.H approved practices </span></li>
                                        <li><span style="font-family: Verdana">In case of forgery </span></li>
                                    </ol>
                                </font>
                            </div>
                        </fieldset>
                        <div align="center">
                            <h6>
                                <span style="font-size: 8pt; font-family: Verdana">
                                Saudi Medical Insurance Standardization - United Claim &amp; Approval Form (SMIS-UCAF
                                1.0)</span></h6>
                        </div>
                    </td>
                </tr>
            </table>


        </ContentTemplate>
        </asp:UpdatePanel>
                
        
        <asp:Label ID="Message1" runat="server" Height="15px" Text="" Width="527px"></asp:Label><br />
        <div>
         
            <asp:TextBox CssClass="textbox" ID="txtTxnID" runat="server" Visible="False">88881051</asp:TextBox>
            &nbsp; &nbsp;
            <asp:TextBox CssClass="textbox"   ID="txtMbrShp" runat="server" Visible="False">3208733</asp:TextBox>
</div>
    </form>
</body>
</html>
