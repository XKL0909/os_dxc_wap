﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="MyMedication1" Codebehind="MyMedication1.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

	<STYLE type="text/css">
		<%--input { border-style: solid; border-width: 1px; font-family: verdana, arial, sans-serif; font-size: 9px; padding: 0px; color:black; backgroundcolor: #FFFFEE}--%>
	<%--	td { border-style: solid; border-width: 1px; font-family: verdana, arial, sans-serif; font-size: 10px; padding: 0px;}--%>
	
	</STYLE>
	
	    <link rel="stylesheet" title="CSS StyleSheet" href="../calendar/cwcalendar.css" type="text/css" media="all" />
<script language="javascript" type="text/javascript" src="jsDate.js"></script>
		<script type="text/javascript" src="../calendar/calendar.js">
		
		</script>

	

    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="styles.css" type="text/css" />
</head>
<body topmargin="0px">
    <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Middle
            East Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    
      
        <table width=100% ><tr><td align=left>
            <label ID="Label1" ><font size=4>My Medication</font></label>  </td>
            <td><a href="#" onclick="window.print()">
                <img class="noPrint" border="0" src="images/printer.gif" alt="Print" style="height: 33px">
            </a> </td></tr></table>
            
            
   
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />
 
  <div class="noPrint" >   
        
       <aspAjax:TabContainer runat="server" ID="tabsModManager" BorderStyle="None" BackColor="AliceBlue"
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Font-Bold="True"
            Font-Size="Medium">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Font-Size="Medium" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Font-Size="Small" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="lblTabLoadStatus" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" runat="server" Visible="false">
                            </asp:DropDownList></div>
                            <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details for Dependent" />
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer>
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
               <br /><br /> 
            <table >
           
                <tr>
                    <td>
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server" Text="3456998"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server" Text="Sunil Kumar Jose"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblCustomer_Name" runat="server" Text="Customer Name"  Visible="false" ></asp:Label></td>
                    <td >
                        <asp:Label ID="lblCustomerName" runat="server" Visible="false" Text="30/05/1970"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblClass_Name" runat="server" Text="Class Name"></asp:Label></td>
                    <td>
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLProviderList" runat="server" Visible="true">
                            </asp:DropDownList></div></td>
                </tr>
          
            </table>
            
   
            
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
      <div id="CoverageListReport"  visible=false style=" font-family:Verdana; ; height:auto; width:100%; overflow:hidden;" runat="server" >
       </div>
                     
    <asp:Table ID="Table2" runat="server">
    <asp:TableHeaderRow>
    <asp:TableHeaderCell>Request Refill</asp:TableHeaderCell>
    <asp:TableHeaderCell>Service Description</asp:TableHeaderCell>
    <asp:TableHeaderCell><nobr>Supply-period</nobr><br/>(months)</asp:TableHeaderCell>
    <asp:TableHeaderCell>Supply date from</asp:TableHeaderCell>
    <asp:TableHeaderCell>Supply date to</asp:TableHeaderCell>
    <asp:TableHeaderCell>Quantity</asp:TableHeaderCell>
    <asp:TableHeaderCell>Estimated cost</asp:TableHeaderCell>
    </asp:TableHeaderRow>
        </asp:Table>    
     <p style="text-align:right;width:100%;" > <asp:Label ID="lblTotalCost"  runat="server" ></asp:Label>      
</p>
<asp:Button CssClass="submitButton" ID="btnUpdateAll" runat="server" Text="Submit" Visible="false" />
<br />
        <asp:Button CssClass="submitButton" ID="Button1" runat="server" Text="Submit Request" OnClick="Button1_Click" />     
           
       </form> 
        
   

</body>
</html>
