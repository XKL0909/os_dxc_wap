using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class MyCoverageMobilev2 : System.Web.UI.Page 
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
     string strMemberName;

 ///  string TabFlag;     Need to create a session variable to hold this value

    protected void Page_Load(object sender, EventArgs e)
    {
       

        strMembershipNo = Request["MembershipNo"];
        strMemberName = Request["MemberName"];
        DisplayProdCov(strMembershipNo, "");
        if (!IsPostBack)
            DependentList();
       

    }

    // function used for Tab System when user requset for dependent ceritifcate
    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
     

        DisplayProdCov(DDLDependentList.SelectedValue, DDLDependentList.SelectedItem.ToString() );

     
    }




    // function used for Tab System to retrieve family list
    private void DisplayDependentListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

       
        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            DDLDependentList.Items.Clear();

            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

              
                

                ListItem DepList = new ListItem();
                DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                DDLDependentList.Items.Add(DepList);

            }
        }
        else
        {
        
        }

      


    }

    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void DependentList()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        request.membershipNo = strMembershipNo;
        //request..membershipNo = 
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

        //request.IqamaNo =  "5523234443";
        //request.TotalNoOfFamily = "3";

        try
        {
            StringBuilder sb = new StringBuilder(200);
            
            response = ws.EnqMbrListInfo(request);
            

            if (response.errorID != "0")
            {
                sb.Append(response.errorMessage).Append("\n");
                lblErrorMessage.Text = sb.ToString();
            }
            else
            {
                DisplayDependentListResult(response);

                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlTextWriter.Formatting = Formatting.Indented;
                xs.Serialize(xmlTextWriter, response);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                this.txtBox_output.Text = XmlizedString;
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text =ex.Message;
        }

        if (DDLDependentList.Items.Count > 0)
        {
  //////          TabFlag = "D";
            DDLDependentList.Visible = true;
            
            btnRequestDependentCert.Visible = true;
        }
        

     }
   






    private void DisplayProdCov(string MemberNo, string MemberName)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN  request;
        OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
           // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN();
            request.membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); 

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.EnqProdCovInfo(request);

            //response = ws.EnqMbrListInfo(request);
           // Cursor.Current = Cursors.Default;


            if (response.className != null)
            {

                lblMembershipNo.Text = request.membershipNo;

                if (MemberName == "")
                    lblMemberName.Text = strMemberName;
                else
                    lblMemberName.Text = MemberName;

                
                lblClassName.Text = response.className;
                lblCustomerName.Text = response.companyName;

                DisplayCoverageListResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                
            }
            else
            {
                
                
                UpdatePanel2.Visible = false;
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            UpdatePanel2.Visible = false;
         
            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Invalid Member error. The member might be inactive or terminated.";// response.errorMessage[0].ToString();

        }

}

    private void DisplayCoverageListResult(OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;


        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
           
            //dtl.proratedOverallAnnualLimit = ;
            //str1 = 

            sbResponse.Append(" <table class='eCommerceTable' width='100%'><tr><td>Benefit Type</td>   <td>Benefit Limit</td></tr>");
            sbResponse.Append("<tr class='grayRow'><td>OverAll Annual Limit</td><td>" + response.proratedOverallAnnualLimit.ToString("C").Replace("�", "") + "</td>");
            response.proratedOverallAnnualLimit.ToString();
            int _row = 0;
            foreach ( OS_DXC_WAP.CaesarWS.BenefitTypeLimitDetail_DN  dtl in response.detail)
            {
                _row = _row + 1;


                if (_row % 2 == 0)
                {
                    sbResponse.Append("<tr class='grayRow'>");
                }
                else
                {
                    sbResponse.Append("<tr>");
                } 
                sbResponse.Append("<td>" + dtl.detail_BenefitType + "</td>");
                if (dtl.detail_ProratedLimit != -1)
                    sbResponse.Append("<td>" + dtl.detail_ProratedLimit.ToString("C").Replace("�", "") + "</td></tr>");
                    else
                    sbResponse.Append("<td>Not Covered</td></tr>");
                

            }
            sbResponse.Append("</table>");
           CoverageListReport.Visible = true;
           CoverageListReport.InnerHtml =   sbResponse.ToString();


        }
        else
        {
          
        }

      

    }


    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }




    
}