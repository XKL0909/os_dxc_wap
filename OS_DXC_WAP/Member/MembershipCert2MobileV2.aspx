﻿<%@ Page Language="C#" AutoEventWireup="true"   MasterPageFile="~/Templates/Mobile.Master"  EnableEventValidation="false"
    Inherits="MembershipCert2MobileV2" Codebehind="MembershipCert2MobileV2.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1.Export, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server">
    <div class="adbanner" align="right" >
        <hr>
    </div>
    <div class="noPrint" id="frm" align="right" runat="server">
      
        <table width=100% >
        <tr>
            <td align="left" colspan="2">
            <table width="100%">
                <tr>
                    <td>Please choose the purpose of your request 
                </td>
                </tr>
                <tr>
                    <td><asp:DropDownList ID="cmOption" runat="server">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="Visit Hospital" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Visa Application" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                       </td>

                </tr>
                <tr>
                    <td>Select member</dx:ASPxComboBox></td>

                </tr>
                <tr>
                    <td>
                    <asp:DropDownList ID="cbMemberList" runat="server"></asp:DropDownList>
                   
                   
                    </td>
                </tr>
               
            </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
            <td>
                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="View Certificate" 
                    onclick="ASPxButton1_Click">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
        <td align="left" >
            <asp:Label id="lblTitle" runat="server" visible="false" Text="Certificate of Membership" ></asp:Label>  </td>
            <td>&nbsp;</td></tr></table>
            
            
       
  
    </div>
  
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <script language="javascript" type="text/javascript">
            function clientActiveTabChanged(sender, args) {
                // Post back if it is the 3rd tab (0 based)
                if (sender.get_activeTabIndex() == 2) {
                    __doPostBack('<%= this.btnTab2.UniqueID %>', '');
                }

                if (sender.get_activeTabIndex() == 0) {
                    __doPostBack('<%= this.btnTab1.UniqueID %>', '');
                }

            }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button"/>
       
        <div class="noPrint" align="right" >
        
            <asp:imagebutton runat="server" ImageUrl="~/Member/fax.png" 
                    onclick="Unnamed1_Click" Height="60px" Width="60px" ID="imagebutton1"></asp:imagebutton>
                    &nbsp;<asp:imagebutton runat="server" ImageUrl="~/Member/email.png"
                     Height="60px" Width="60px" ID="imagebutton2" onclick="imagebutton2_Click"></asp:imagebutton></div>                
       
        <br />
         <div runat="server" id="divCer" visible="false">
              
               <table width="90%" border="0" cellspacing="0" cellpadding="0" class="contentTableWhite" align="center" >
               <tr>
                <td align="center">
                 <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
            <ProgressTemplate>
                <img src="../images/spinner.gif" />
                
            </ProgressTemplate>
        </asp:UpdateProgress>
                </td>
               </tr>
                <tr>
                    <td>
                    <table style="width: 100%; padding-left:0px; padding-right:0px;" cellspacing="0"   >
                    <tr >
                        <td style="width: 499px" >
                            <asp:Label ID="lblHospital1" runat="server" Text="Attention all BUPA Arabia Network" Visible="false"></asp:Label>
                        </td>
                        <td  style="width: 127px">
                            &nbsp;</td>
                        <td  style="width: 40%;text-align:right">
                            <asp:Label ID="lblHospital2" runat="server" 
                                Text="السادة / شبكة مستشفيات بوبا العربية" Visible="false"></asp:Label>
&nbsp;</td>
                    </tr>
                    <tr>
                        <td >
                            <asp:Label ID="lblToHow" Visible="false" runat="server" Text="TO WHOM IT MAY CONCERN"></asp:Label>
                        </td>
                        <td  style="width: 127px">
                            &nbsp;</td>
                        <td style="text-align:right">
                            <b><asp:Label ID="lblToHowAr" runat="server" 
                                Text="لمن يهمه الأمر" Visible="false"></asp:Label>
                            </b></td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            &nbsp;</td>
                        <td  style="width: 127px">
                            &nbsp;</td>
                        <td >
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            <span >Subject: </span>
                            <asp:Label ID="lblSubject" runat="server" Text=""></asp:Label></td>
                        <td  style="width: 127px">
                            &nbsp;</td>
                        <td Style="text-align:right; direction: rtl;">
                            الموضوع:
                            <asp:Label ID="lblSubAr" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            
                            
                            </td>
                        <td  style="width: 127px">
                            &nbsp;</td>
                        <td >
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 499px" >
                            Certificate Issue Date</td>
                        <td  style="width: 127px">
                            <strong >
                                <asp:Label ID="lblIssueDate" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td style="text-align:right">
                            تاريخ الاصدار
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 499px" >
                            This is to confirm that the below mentioned member is medically covered under 
                            the BUPA Arabia Health Care scheme with the following information:</td>
                        <td  style="width: 127px">
                            &nbsp;</td>
                        <td style="text-align:right">
                            إن هذه الوثيقة تؤكد ان المذكور مغطى طبيا
                            <br />
                            تحت برنامج بوبا العربية للرعاية الصحية وفقا للبيانات التالية
                        </td>
                    </tr>
                </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table style="width: 100%; background-color:White">
                    <tr>
                        <td align="left"  style="width: 30%">
                            Name:</td>
                        <td style="width: 45%; text-align:center" >
                            <strong>
                                <asp:Label ID="lblMemberName" runat="server"></asp:Label></strong></td>
                        <td align="right" style="text-align:right" style="width: 30%">
                            الاسم</td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 320px">
                            Class Name:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblClass" runat="server"></asp:Label></strong></td>
                        <td align="right" style="text-align:right">
                            درجة التغطية
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 320px">
                            Membership No:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblMembershipNo" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" style="text-align:right">
                            رقم العضوية
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 320px">
                            Option*:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblOptions" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" style="text-align:right">
                            قائمة المزايا
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 320px">
                            Room:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblRoomType" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" style="text-align:right">
                            درجة الإقامة
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 320px">
                            Year of Birth:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblDateOfBirth" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" style="text-align:right">
                            تاريخ الميلاد
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 320px">
                            Effective From:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblEffectiveFrom" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" style="text-align:right">
                            صالحة من تاريخ&nbsp;
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 320px">
                            Effective To:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblEffectiveTo" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" style="text-align:right">
                            صالحة الى تاريخ
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 320px">
                            Contribution:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblContribution" runat="server" Text="Label"></asp:Label></strong></td>
                        <td style="text-align:right">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">نسبة المشاركة</span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left"  style="width: 320px">
                            Staff No.</td>
                        <td style="width: 45%; text-align:center">
                            <asp:Label ID="lblStaffNo" runat="server"></asp:Label>&nbsp;</td>
                        <td style="text-align:right">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">الرقم الوظيفي </span>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left"  style="width: 320px">
                            Customer Name:</td>
                        <td style="width: 45%; text-align:center">
                            <strong>
                                <asp:Label ID="lblCustomerName" runat="server"></asp:Label></strong></td>
                        <td style="text-align:right">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">أسم العميل</span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 320px">
                            &nbsp;</td>
                        <td  style="width: 239px">
                            &nbsp;</td>
                        <td >
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 320px" >
                            All Other terms and conditions are as per policy</td>
                        <td  style="width: 239px">
                            &nbsp;</td>
                        <td style="text-align:right">
                            جميع شروط الغطاء تخضع لبنود التعاقد</td>
                    </tr>
                    <tr>
                        <td style="width: 320px">
                            &nbsp;</td>
                        <td  style="width: 239px">
                            &nbsp;</td>
                        <td >
                            &nbsp;</td>
                    </tr>
                </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table style="width: 100%">
                        <tr>
                        <td style="width: 40%"  valign="top">
                            This certificate is issued as a temporary cover for this member. Should this 
                            member need any treatment that is covered under this scheme, please provide it, 
                            and charge BUPA Arabia.</td>
                        <td>
                            &nbsp;</td>
                        <td align="right" class="th" style="margin-right: 13px;"
                            valign="top">
                            إن هذه الشهادة اصدرت مؤقتاً للعضو وفي حالة حاجته الى اية خدمات علاجية مدرجة تحت 
                            بنود الغطاء التأميني الخاص به الرجاء تقديم الخدمة العلاجية و ارسال&nbsp; الفواتير 
                            الى&nbsp; بوبا العربية
                        </td>
                    </tr>
                        <tr>
                        <td style="width: 437px"  valign="top">
                            Should you have any queries, please contact one of our advisers on the BUPA 
                            Arabia dedicated customer service line.
                        </td>
                        <td style="width: 55px">
                            &nbsp;</td>
                        <td align="right" class="th" valign="top">
                            <span dir="rtl" lang="AR-SA" style="font-size: 12pt; font-family: 'Arial','sans-serif';
                                mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-GB; mso-fareast-language: EN-GB;
                                mso-bidi-language: AR-SA">لمزيد من المعلومات الرجاء الاتصال بالهاتف المجاني 
                            لقسم خدمات العملاء.</span>
                    </tr>
                        <tr>
                        <td style="width: 437px"  valign="top">
                            This certificate is valid only for the named insured person when accompanied 
                            with ID documents.
                            <br />
                            <br />
                            Yours sincerely<br />
                            <br />
                            Bupa Arabia<br />
                            Customer Service
                        </td>
                        <td style="width: 55px">
                            &nbsp;</td>
                        <td align="right" class="th" valign="top">
                            إن هذه الشهادة صالحة فقط للعضو المذكور اعلاه شرط توفر وثيقة اثبات رسمية
                            <br />
                            <br />
                            <br />
                            <p align="right" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: right">
                                <span dir="rtl" lang="AR-SA" style="font-family: 'Arial','sans-serif'">مع اطيب 
                                تحيات الشكر والتقدير</span><span lang="EN-GB" style="font-family: 'Arial','sans-serif'"><?xml
                                        namespace="" ns="urn:schemas-microsoft-com:office:office" prefix="o" ?><o:p></o:p></span></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    




<table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td>
                            <b><i >This is only a definition for the codes shown under the 
                            option field. Do not use for coverage checking.</i></b></td>
                    </tr>
                    <tr>
                        <td >
                            * A: Dialysis, C: Chronic, D: Dental, E: Neo-natal care, H: Other Conditions, K: 
                            Cosmetic, L: Congenital, M: Maternity, N: Natural Changes, O: Optical, P: 
                            Developmental, S: Standard Benefit, T: Screening, V: Vaccinations, Y: 
                            Physiotherapy, Z: Hazardous Sports Injury
                        </td>
                    </tr>
                </table>
                    </td>
                </tr>
              </table>
               
           
         </div>


         <div runat="server" id="visa" visible="false">
              
               <table width="90%" border="0" cellspacing="0" cellpadding="0" class="contentTableWhite" align="center" >
               <tr>
                <td align="center">
                 <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="100">
            <ProgressTemplate>
                <img src="../images/spinner.gif" />
                
            </ProgressTemplate>
        </asp:UpdateProgress>
                </td>
               </tr>
                <tr>
                    <td>
                    <table style="width: 100%; padding-left:0px; padding-right:0px;" cellspacing="0"   >
                    <tr >
                        <td >
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-weight:bold">Certificate of 
                                membership<o:p></o:p></span></p>
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <b style="mso-bidi-font-weight:normal"><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>
                                &nbsp;</o:p></span></b></p>
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:12.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-weight:
bold">To Whom It May Concern<o:p></o:p></span></p>
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                <span style="mso-spacerun:yes">&nbsp;</span>Dear Sir/Madam</span><span lang="EN-GB" 
                                    style="font-size:11.0pt;mso-bidi-font-size:9.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p></o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
8.0pt;mso-bidi-font-size:6.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">This letter is to 
                                confirm that 
                            <strong>
                                <asp:Label ID="lblmemName" runat="server"></asp:Label>
                            </strong>
                                </span><b>
                                <span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-ansi-language:
EN-US;mso-fareast-language:EN-US">&nbsp;</span></b><span lang="EN-GB" 
                                    style="font-size:11.0pt;mso-bidi-font-size:9.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">is</span><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"> 
                                covered<span style="mso-spacerun:yes">&nbsp;&nbsp; </span>under BUPA Arabia’s 
                                health care program according to the following membership details:<o:p></o:p></span></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table style="width: 100%; background-color:White">
                    <tr>
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Contract name</span></td>
                        <td style="text-align:left" >
                            <strong>
                                <asp:Label ID="lblCustName" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Contract ID</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblCustNo" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Membership number</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblMembershipNo0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Year of birth</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblDateOfBirth0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Level of cover</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lbllCover" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Expiry Date</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblEffectiveTo0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table style="width: 100%" cellpadding="2" cellspacing="2">
                        <tr>
                        <td style="width: 40%"  valign="top">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td align="right" class="th" style="margin-right: 13px;"
                            valign="top">
                            &nbsp;</td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                All other terms and conditions are as per BUPA Arabia’s policy and CCHI.<o:p></o:p></span></p>
                        </td>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Please note that the 
                                above policy includes world-wide cover according to the following description:<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Medical appropriate 
                                claims are payable up to the member maximum limit <o:p></o:p></span>
                            </p>
                            <p style="text-align: justify; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <b><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                SR</span></b><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                </span><b><asp:Label ID="lblamount" runat="server" Text=""></asp:Label><span 
                                    lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                </span></b>
                                <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:10.0pt;
font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">&nbsp;subject to a 100 percent reimbursement according to the 
                                normal and customary prices of member’s designated BUPA Arabia’s network of 
                                hospitals and polyclinics.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Emergency Evacuation 
                                outside the <st1:place w:st="on"><st1:placetype w:st="on">Kingdom</st1:PlaceType>
                                of <st1:placename w:st="on">Saudi Arabia</st1:PlaceName></st1:place> is covered 
                                as part of the member maximum limit in accordance with SOS International.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                This certificate was issued upon the member request.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Should you have any 
                                queries or wish to discuss this letter any further, please contact the 
                                undersigned. <o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            &nbsp;</td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Yours sincerely</span></td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    




<table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td >
                            &nbsp;</td>
                    </tr>
                </table>
                    </td>
                </tr>
              </table>
               
           
         </div>

        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
        <asp:Label ID="lblErrID" runat="server" Visible="False"></asp:Label><br />
        <div>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
            <br />
           
            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
                        
           
       
        </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p>
        View and print your membership certificate</p>
</asp:Content>

