﻿if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
        "use strict";
        if (this == null) {
            throw new TypeError();
        }
        var t = Object(this);
        var len = t.length >>> 0;
        if (len === 0) {
            return -1;
        }
        var n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    }
}

function SetMobileNumberValidation(MobileField) {
    $(MobileField).keydown(function (e) {
        var n = e.keyCode;
        var numbers = new Array(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105);
        var allowedKeys = new Array(16, 17, 8, 9, 35, 37, 39, 46);
        if (numbers.indexOf(n) == -1 && allowedKeys.indexOf(n) == -1) {
            e.preventDefault();     // Prevent character input
        }
    });
}
function SetIDNumberValidation(IDField,TypeSelect) {
    $(IDField).keydown(function (e) {
        var n = e.keyCode;
        if ($(TypeSelect).val() != "3") {
            var numbers = new Array(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105);
            var allowedKeys = new Array(16, 17, 8, 9, 35, 37, 39, 46);
            if (numbers.indexOf(n) == -1 && allowedKeys.indexOf(n) == -1) {
                e.preventDefault();     // Prevent character input
            }
        }
    });
}

function SetAmountValidation(MobileField) {
    $(MobileField).keydown(function (e) {
        var n = e.keyCode;
        var numbers = new Array(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105);
        var allowedKeys = new Array(16, 17, 8, 9, 35, 37, 39, 46, 190,110);
        if (numbers.indexOf(n) == -1 && allowedKeys.indexOf(n) == -1) {
            e.preventDefault();     // Prevent character input
        }
    });
}

function SetIBANNumberValidation(MobileField) {
    $(MobileField).keydown(function (e) {
        var n = e.keyCode;
        var numbers = new Array(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105);
        var allowedKeys = new Array(16, 17, 8, 9, 35, 37, 39, 46);
        if (numbers.indexOf(n) == -1 && allowedKeys.indexOf(n) == -1 && $(MobileField).val().length >= 2) {
            e.preventDefault();     // Prevent character input
        }
    });
}