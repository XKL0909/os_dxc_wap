﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bupa.Web.Business.Services;
using System.Text;
using System.Web.Configuration;
using System.Configuration;
using System.Collections;

public partial class Member_editinfo : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    private string MemberShipNo;
    private Hashtable hasQueryValue;
    string membershipNo, memberName, mobileNo, email;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Session["Reset"] = true;
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            if (!IsPostBack)
            {

                hasQueryValue = new Hashtable();
                string queryStringValues = string.Empty;

                if (Request.QueryString.Count > 0)
                {
                    try
                    {



                        var val = Request.QueryString["val"];
                        queryStringValues = Cryption.Decrypt(val);
                        hasQueryValue = Cryption.GetQueryValue(queryStringValues);

                        membershipNo = hasQueryValue.ContainsKey("membershipNo") ? Convert.ToString(hasQueryValue["membershipNo"]) : string.Empty;
                        memberName = hasQueryValue.ContainsKey("memberName") ? Convert.ToString(hasQueryValue["memberName"]) : string.Empty;
                        mobileNo = hasQueryValue.ContainsKey("mobileNo") ? Convert.ToString(hasQueryValue["mobileNo"]) : string.Empty;
                        email = hasQueryValue.ContainsKey("email") ? Convert.ToString(hasQueryValue["email"]) : string.Empty;


                        lblMemNo.Text = membershipNo;
                        lblMemName.Text = " / " + memberName;


                        txtEmail.Text = email;
                        txtMobile.Text = mobileNo;

                    }
                    catch (Exception ex)
                    {
                        Response.Write("Invalid rquest!");
                        return;
                    }

                }
                else
                {
                    Response.Write("Invalid rquest!");
                    return;
                }

              
            }
            CheckSessionExpired();
        }
        catch
        {

        }
        
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {


        #region Caesar Invocation

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqUpdMbrInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqUpdMbrInfoResponse_DN response;
        request = new OS_DXC_WAP.CaesarWS.ReqUpdMbrInfoRequest_DN();

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        request.transactionID = WebPublication.GenerateTransactionID();

        //if (!string.IsNullOrEmpty(Convert.ToString(Request["mem"])))
        //{
        //    request.mbrShpNo = Request["mem"];
        //}
        //else
        //{
        //    request.mbrShpNo = Convert.ToString(Session["memberno"]);
        //}

        request.mbrShpNo = lblMemNo.Text.Trim();

        request.email = txtEmail.Text.Trim();
        request.mobileNo = txtMobile.Text.Trim();


        try
        {
            response = ws.ReqUpdMbrInfo(request);
            StringBuilder sb = new StringBuilder(200);

            if (response.status != "0")
            {

                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/> ");

                Message1.Text = sb.ToString();
                Message1.Text += ".";


            }
            else
            {
                Message1.Text = "Done";

                Message1.Visible = true;


                Response.Write("<script language=javascript>this.window.opener = null;window.open('','_self'); window.close();   </script>");




            }
        }
        catch (Exception ex)
        {


            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Error Encountered. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            Message1.Visible = true;

        }
        #endregion
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
}