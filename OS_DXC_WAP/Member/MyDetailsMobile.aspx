﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="MyDetailsMobile" Codebehind="MyDetailsMobile.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <meta name="viewport" content="width=320" />

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    
    <style type="text/css">
    @media handheld {
   .navigation {
     display: none;
   }
 }
body
{
    width:320px;
}
html, body, form, fieldset, p, div, h1, h2, h3, h4, h5, h6{
	margin:0;
	padding:0;
	-webkit-text-size-adjust:none;
	}
body{
	font-size: 10px;
	}
ul, li, ol, dl, dd, dt{
	list-style:none;
	padding:0;
	margin:0;
	}
a{
	text-decoration:none;
	}

#Content td{
	
	color:#606060;
}
.th{
 
 color:#666;
 }
.Tab .ajax__tab_header
{
    color: #000000;
    
    background-color: #ffffff;
    margin-left: 10px;
    border:1px solid #b4cbdf;
    
}
/*Body*/
.Tab .ajax__tab_body
{
    border:1px solid #b4cbdf;
    padding-top:0px;
}

@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
        .style1
        {
            height: 21px;
        }
    </style>
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="stylesMobile.css" type="text/css" />
    
    <script type="text/javascript" src="scriptMobile/jquery.min.js"></script>
  <script type="text/javascript" src="scriptMobile/jquery-ui.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="styleMobile/jquery-ui.css">
  
  
  <script type="text/javascript">
	$(document).ready(function() {
		waitingDialog({});
		setTimeout(closeWaitingDialog, 2000);
 
	});
  </script>
  
</head>
<body topmargin="0px">

    <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Arabia Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    
      <table width="100%">
        <tr valign="baseline">
        
        <td >
        <table>
            <tr>
                <td valign="middle">
                     <img class="style2" src="../Icons/MemberDetails.png" /></td>
                <td valign="middle">
            <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Details" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
            </tr>
        </table>
            </td>
            
          </tr>
       
        <tr valign="baseline"><td class="style3" >
            <img  src="../Icons/line.jpg" height="5" width="100%"  /></td>
          </tr>
       
        </table>
            
            
   
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />
 
  <div class="noPrint" >   
        
       <aspAjax:TabContainer runat="server" ID="tabsModManager" 
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Font-Size="Medium" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Font-Size="Small" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="lblTabLoadStatus" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" runat="server" Visible="true">
                            </asp:DropDownList></div>
                            <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details" />
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer>
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
             
            <table style="width:100%; height:100%" >
           
                <tr style="background-color:#e6f5ff">
                    <td >
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No "></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server"    Text="3456998"></asp:Label></td>
                </tr>
                 <tr>
                    <td class="style1" >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name "></asp:Label></td>
                    <td class="style1" >
                        <asp:Label ID="lblMemberName" runat="server"      Text="Sunil Kumar Jose"></asp:Label></td>
                </tr>
                <tr style="background-color:#e6f5ff">
                    <td >
                        <asp:Label ID="lblDate_Of_Birth" runat="server" Text="Date of Birth"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblDateOfBirth" runat="server"      Text="30/05/1970"></asp:Label></td>
                </tr>
                <tr height="21">
                    <td>
                        <asp:Label ID="lblGender_" runat="server" Text="Gender"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblGender" runat="server"      Text="Male"></asp:Label></td>
                </tr>
                <tr style="background-color:#e6f5ff">
                    <td>
                        <asp:Label ID="lblType_" runat="server" Text="Type"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblType" runat="server"      Text="Employee"></asp:Label></td>
                </tr>
                <tr height="21">
                    <td>
                        <asp:Label ID="lblStatus_" runat="server" Text="Status"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblStatus" runat="server"      Text="Active"></asp:Label></td>
                </tr>
                <tr style="background-color:#e6f5ff">
                    <td>
                        <asp:Label ID="lblEmployee_No" runat="server" Text="Employee No"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblEmployeeNo" runat="server"      Text="987"></asp:Label></td>
                </tr>
                <tr height="21">
                    <td>
                        <asp:Label ID="lblEmployee_ID" runat="server" Text="Saudi ID"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblEmployeeID" runat="server"      Text="2099797017"></asp:Label></td>
                </tr>
                <tr style="background-color:#e6f5ff">
                    <td>
                        <asp:Label ID="lblSponsor_Id" runat="server" Text="Sponsor Id"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblSponsorId" runat="server" Text="0000000000"     ></asp:Label></td>
                </tr>
                <tr height="21">
                    <td>
                        <asp:Label ID="lblCompany_Name" runat="server" Text="Company Name"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblCompanyName" runat="server"      Text="ASAS Health Care Co. Ltd. ( BUPA Arabia )"></asp:Label></td>
                </tr>
                <tr style="background-color:#e6f5ff">
                    <td>
                        <asp:Label ID="lblStart_Date" runat="server" Text="Start Date"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblStartDate" runat="server" Text="01/01/2006"     ></asp:Label></td>
                </tr>
                <tr height="21">
                    <td>
                        <asp:Label ID="lblEnd_Date" runat="server" Text="End Date"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblEndDate" runat="server" Text="31/05/2009"   ></asp:Label></td>
                </tr>
          
            </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
       
           

        
           
       </form> 
        
   

</body>
</html>
