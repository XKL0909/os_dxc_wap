﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="MyCoverage" Codebehind="MyCoverageMobile.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

   
    <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
 
    <style >
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>

    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="stylesMobile.css" type="text/css" />



 	<style type="text/css">
		body{
			font-family:Arial,Sans-serif;
			font-size:16px;
		}
		caption{
			padding-bottom:5px;
			
		}
		thead th,tfoot td{
			background:#e6f5ff; font-color:#ffffff;font-weight:bold;
		}
		tr.ruled{
			<%--background:#ccea68; font-weight:bold;--%>
		}    
		table{
			
			border-collapse:collapse;
		}
		th,td{
			
			border-collapse:collapse;
		}
		#mytable tr.ruled{
			<%--background:#333;
			color:#ccc;--%>
		}
        .style1
        {
            width: 114px;
            height: 22px;
        }
        .style2
        {
            height: 22px;
        }
        
         .BackColorTab
        {
             color: #aa976b;
    font-size: 60px;
    font-weight: bold;
    background-color: White;
    margin-left: 10px;
    

            
           
        }
        
        
        /*Header*/
.Tab .ajax__tab_header
{
    color: #000000;
    font-family:Calibri;
    font-size: 24px;
    font-weight: bold;
    background-color: #ffffff;
    margin-left: 10px;
    border:1px solid #b4cbdf;
    
}
/*Body*/
.Tab .ajax__tab_body
{
    border:1px solid #b4cbdf;
    padding-top:0px;
}
/*Tab Active*/



    </style>
	<script type="text/javascript" src="../includes/tableruler.js"></script>
	<script type="text/javascript">
		window.onload=function(){tableruler();} 
    </script>

<script type="text/javascript" src="scriptMobile/jquery.min.js"></script>
  <script type="text/javascript" src="scriptMobile/jquery-ui.min.js"></script>
	<!-- include smoothness jQueryUI theme -->
	<link rel="stylesheet" type="text/css" href="styleMobile/jquery-ui.css">
  
  <style type="text/css">
	body {
		font-family:Arial,Geneva,Verdana,sans-serif;
		font-size: 16px;
		overflow-x: hidden;
	}
	#loadingScreen {
		background: url(images/loading.gif) no-repeat 5px 8px;
		padding-left: 25px;
	}
	/* hide the close x on the loading screen */
	.loadingScreenWindow .ui-dialog-titlebar-close {
		display: none;
	}
      </style>
  <script type="text/javascript">
	$(document).ready(function() {
		waitingDialog({});
		setTimeout(closeWaitingDialog, 2000);
 
	});
  </script>
    
</head>
<body topmargin="0px" >



    <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Arabia Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    
   
    <form id="form1" runat="server">
    
     <div  class="noPrint" > 
     
       <table width="100%">
        <tr valign="top"><td >
        <table>
            <tr>
                <td valign="middle">
                    <img class="style3" src="../Icons/MyCoverage.png" /></td>
                <td valign="middle">
            <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Coverage" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
            </tr>
        </table>
            </td>
           </tr>

        <tr valign="middle"><td >
            <img  src="../Icons/line.jpg" height="5" width="100%"  /></td>
           </tr>

        </table>
        
     
      </div>      
            
   
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />
 
  <div class="noPrint"  >   
        
       <aspAjax:TabContainer runat="server" ID="tabsModManager" CssClass="Tab" 
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Font-Bold="True" Font-Size="XX-Large">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="lblTabLoadStatus" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" runat="server" Visible="false">
                            </asp:DropDownList></div>
                            <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details" />
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer>
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
               <br /><br /> 
            <table style="font-family:Arial; font-size:35px; font-weight:normal; width: 95%; height:100%" align="center">
           
                <tr style="background-color:#e6f5ff;">
                    <td style="width: 114px">
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server"   Text="3456998"></asp:Label></td>
                </tr>
                <tr style="display:none">
                    <td class="style1" >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name"></asp:Label></td>
                    <td class="style2" >
                        <asp:Label ID="lblMemberName" runat="server" Text=""  ></asp:Label></td>
                </tr>
                <tr  >
                    <td style="width: 114px" >
                        <asp:Label ID="lblCustomer_Name" runat="server" Text="Customer Name" Width="140px"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblCustomerName" runat="server"   Text="30/05/1970"></asp:Label></td>
                </tr>
                <tr style="background-color:#e6f5ff;">
                    <td style="width: 114px">
                        <asp:Label ID="lblClass_Name" runat="server" Text="Class Name"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblClassName" runat="server" Text=""  ></asp:Label></td>
                </tr>
          
            </table>
           
           <br /> 
       <div id="CoverageListReport"  visible="false" style="font-weight:normal; font-family:Arial; font-size:16; height:100%; width:95%; overflow:hidden;" align="center" runat="server" >
           &nbsp;</div>

            
            
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="502px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
       
           

        
           
       </form> 
        
   

</body>
</html>
