﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Member_CerPrint" Codebehind="CerPrint.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css" media="print">
        .lbl {
            font-size: 10px;
            font-family: Arial;
        }

        .body {
            font-family: Arial;
            font-size: 10px;
        }

        .en {
            font-family: Arial;
            font-size: 11px;
        }

        .ar {
            font-family: Arial;
            font-size: 11px;
        }

        .footer {
            font-family: Arial;
            font-size: 8px;
        }

        @page {
            size: auto;
            margin: 0;
        }
    </style>
</head>
<body >
    <form id="form1" runat="server">

    <div id="member" runat="server" visible="false">
    <table width="800px" border="0" cellspacing="2" cellpadding="2"  align="center" >
               <tr>
                   <td align="right">
                       <%--<img src="Logo.gif" width="168" height="50">--%>
                       <asp:Image runat="server" ID="Image6" ImageUrl="~/images/logo-new.jpg" Width="100" Height="100" />
                       <br />
                   </td>
               </tr>
                <tr>
                    <td>
                        <table style="width: 100%; padding-left: 0px; padding-right: 0px;" cellspacing="0">
                            <tr runat="server" id="f1" visible="false">
                                <td style="width: 474px">
                                    <%-- <asp:Label CssClass="en"   ID="lblHospital1"  runat="server" 
                                Text="Attention all BUPA Arabia Network" Visible="False"></asp:Label>--%>
                                </td>
                                <td class="ar">&nbsp;</td>
                                <td style="width: 450px; text-align: right" class="ar">
                                    <%--<asp:Label CssClass="lbl"  ID="lblHospital2" runat="server" 
                                Text="السادة / شبكة مستشفيات بوبا العربية" Visible="false"></asp:Label>--%>
                                &nbsp;</td>
                            </tr>
                            <tr runat="server" id="f2" visible="false">
                                        <%--<td class="en" >
                                    <asp:Label CssClass="en"  ID="lblToHow" Visible="False" runat="server" 
                                        Text="TO WHOM IT MAY CONCERN"></asp:Label>
                                </td>
                                <td class="ar">
                                    </td>
                                <td style="text-align:right"  class="ar">
                                    <asp:Label CssClass="ar"  ID="lblToHowAr" runat="server" 
                                        Text="لمن يهمه الأمر" Visible="false"></asp:Label>
                                    </td>--%>
                            </tr>

                            <tr>
                                        <%--<td  style="width: 474px" class="en">
                                    Subject: 
                                    <asp:Label CssClass="lbl"  ID="lblSubject" runat="server" Text=""></asp:Label></td>
                                <td class="ar">
                                    &nbsp;</td>
                                <td style="text-align:right; direction: rtl;" class="ar">
                                    الموضوع:
                                    <asp:Label CssClass="ar"  ID="lblSubAr" runat="server" Text=""></asp:Label>
                                </td>--%>
                            </tr>
                            <tr>
                                <td style="width: 474px"></td>
                                <td class="ar">&nbsp;</td>
                                <td class="ar">&nbsp;</td>
                            </tr>
                            <tr>
                           <%--<td  style="width: 474px" class="en" >
                                        Certificate Issue Date</td>
                                    <td  style="text-align:center; min-width:100px !important" class="ar">
                                        <strong >
                                            <asp:Label CssClass="lbl"  ID="lblIssueDate" runat="server"></asp:Label>
                                        </strong>
                                    </td>
                                    <td style="text-align:right" class="ar">
                                        تاريخ الاصدار
                                    </td>--%>
                                <td style="width: 127px">
                                    <strong>Insurance Companies Office
                                    </strong>
                                    <br />
                                    <br />
                                </td>
                                <td style="width: 127px">&nbsp;</td>
                                <td style="text-align: right" class="ar">
                                    <strong>مكتب شركات التأمين
                                    </strong>
                                    <br />
                                    <br />
                                </td>
                                
                            </tr>
                            <tr>
                                <td style="width: 474px" class="en">Dear Sir,<br />
                                    <br />
                                    This is to confirm that the below mentioned member is medically covered under Bupa Arabia Health Care scheme with the following information:
                                     <br />
                                    <br />
                                    <br />
                                </td>
                                <td class="ar">&nbsp;</td>
                                <td style="text-align: right" class="ar">السادة المحترمين  
                                    <br />
                                    <br />
                                    إن هذه الوثيقة تؤكد ان المذكور مغطى طبيا
                                    <br />
                                    تحت برنامج بوبا العربية للرعاية الصحية وفقا للبيانات التالية
                               
                                    <br />
                                    <br />
                                    <br />
                                </td>
                                
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%; background-color: White">
                            <tr>
                                <td align="left" style="width: 30%" class="en">Name:</td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lblMemberName" runat="server"></asp:Label></strong></td>
                                <td align="right" style="text-align: right" style="width: 30%" class="ar">:الاسم</td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px" class="en">Membership Number:</td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lblMembershipNo" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" class="ar">:رقم العضوية
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px" class="en">ID Number.</td>
                                <td style="text-align: center">
                                    <strong><asp:Label CssClass="lbl" ID="lblIDNumber" runat="server"></asp:Label>&nbsp;</strong>
                                </td>
                                <td style="text-align: right" class="ar">:رقم الهوية
                            &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px" class="en">Class Name:</td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lblClass" runat="server"></asp:Label></strong></td>
                                <td align="right" style="text-align: right" class="ar">:درجة التغطية
                                </td>
                            </tr>

                            <tr>
                                <td align="left" style="width: 320px" class="en">Network:</td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lblNetwork" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" class="ar">:شبكة
                                </td>
                            </tr>

                            <tr>
                                <td align="left" style="width: 320px" class="en">Year of Birth:</td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lblDateOfBirth" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" class="ar">:تاريخ الميلاد
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px" class="en">Deductible:</td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lbldeductible" runat="server" Text="Label"></asp:Label></strong></td>
                                <td style="text-align: right" class="ar">:نسبة المشاركة&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="left" style="width: 320px" class="en">Effective From:</td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lblEffectiveFrom" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" class="ar">:صالحة من تاريخ&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px" class="en">Effective To:</td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lblEffectiveTo" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" class="ar">:صالحة الى تاريخ
                                </td>
                            </tr>


                            <tr>
                                <td align="left" style="width: 320px" class="en">Customer Name:
                                    <br />
                                    <br />
                                    <br />
                                </td>
                                <td style="text-align: center">
                                    <strong>
                                        <asp:Label CssClass="lbl" ID="lblCustomerName" runat="server"></asp:Label></strong>
                                    <br />
                                    <br />
                                    <br />
                                </td>
                                <td style="text-align: right" class="ar">:أسم العميل&nbsp;
                                    <br />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 320px" class="en">All Other terms and conditions are as per policy</td>
                                <td style="width: 239px">&nbsp;</td>
                                <td style="text-align: right" class="ar">جميع شروط الغطاء تخضع لبنود التعاقد</td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 40%" valign="top" class="en">Should he / she need any treatment that is covered under his / her scheme, please accept this letter instead of the membership card for one visit only
                                 <br />
                                 <br />
                                </td>
                                <td>&nbsp;</td>
                                <td align="right"
                                    valign="top" class="ar">عند الحاجة لتقديم خدمة علاجية مدرجة تحت جدول المزايا الخاص بدرجة العضوية المذكورة سابقا يرجى قبول هذا الخطاب كبديل لبطاقة العضوية وذلك لمرة واحدة فقط
                                 <br />
                                 <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top" class="en">Please attach this letter and a copy of the member ID with the claim invoices.
                                    <br />
                                    <br />
                                </td>
                                <td style="width: 55px">&nbsp;
                                    <br />
                                </td>
                                <td align="right" valign="top" class="ar">.يرجى إرفاق صورة الهوية للعميل و الخطاب مع المطالبة عند إرسالها إلى بوبا العربية
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top" class="en">As per normal eligibility practice, please check the membership number for the treatment services in the hospital according to the approved protocol and the member network.
                                    <br />
                                    <br />
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="ar" valign="top" >املين منكم تدقيق اهلية رقم العضوية المذكور لتلقي الخدمات العلاجية في المستشفى حسب البروتوكول المعتمد وشبكة التغطية المعتمدة.
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top" class="en">Should you have any queries, please contact one of our advisers on the Bupa Arabia customer service line 920023009
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="ar" valign="top">في حال وجود اي استفسارات، الرجاء عدم التردد في الإتصال عل هاتف الرقم المجاني لخدمات العملاء 920023009
                                
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">
                                    <br />
                                    <br />
                                    <br />
                                    Sincerely yours,
                                </td>
                                <td style="width: 55px"> <asp:Image ID="Image4" runat="server" ImageAlign="Baseline"
                                ImageUrl="~/Member/images/BupaFrontOfficeStamp.png" 
                            style="text-align: right"  />

                                </td>
                                <td align="right" class="ar" valign="top">
                                    <br />
                                    <br />
                                    <br />
                                    مع اطيب التحيات
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">
                                    <br />
                                    <strong>Bupa Arabia
                                <br />
                                        Customer Service 
                                    </strong>
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="ar" valign="top">
                                    <br />
                                    <strong>بوبا العربية
                                <br />
                                        خدمة العملاء                                                                                                                          
                                    </strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--Footer details--%>
                   <tr>
                    <td>
                        <br />
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 40%; color: #0073C8;" valign="top"><strong>Bupa Arabia for Cooperative Insurance</strong> 
                                </td>
                                <td>&nbsp;</td>
                                <td align="right" class="ar" style="margin-right: 13px; color: #0073C8;" valign="top">
                                 بوبا العربية للتامين التعاوني
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">PO Box 23807, Jeddah 21436, Saudi Arabia
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="ar" valign="top">
                                   ص.ب 23807, جدة 21436 المملكة العربية السعودية.
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">Tel: +966 920 000 456 | Fax: +966 920 000 724
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="ar" valign="top">
                                     +966 تليفون: 456 000 920 966+ | فاكس: 724 000 920
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">www.bupa.com.sa
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="th" valign="top">
                                    www.bupa.com.sa
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 437px" valign="top">
                                    CR number: 4030178881
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="ar" valign="top">
                                   س .ت: 4030178881
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 437px" valign="top">
                                    Paid up Capital: SR800,000,000
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="ar" valign="top">
                                    رأس المل المد فوع: 800,000,000 ريا ل سعودي
                                </td>
                            </tr>
                            </table>
                        </td>
                       </tr>
              </table>
    </div>

    <div id="client" runat="server" visible="false">
    <table width="800px" border="0" cellspacing="2" cellpadding="2"  align="center" >
               <tr>
                   <td align="left" valign="top">
                       <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                   </td>
                   <td align="right">
                       <%-- <img src="../images/logoPrint.png" >--%>
                       <asp:Image runat="server" ID="Image5" ImageUrl="~/images/logo-new.jpg" Width="100" Height="100" />
                       <br>
                   </td>
               </tr>
                <tr>
                    <td colspan="2">
                    <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table width="90%" border="0" cellspacing="0" cellpadding="0" class="contentTableWhite" align="center" >
               <tr>
                <td align="center">
                    &nbsp;</td>
               </tr>
                <tr>
                    <td>
                    <table style="width: 100%; padding-left:0px; padding-right:0px;" cellspacing="0"   >
                    <tr >
                        <td >
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-weight:bold">Certificate of 
                                membership<o:p></o:p></span></p>
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <b style="mso-bidi-font-weight:normal"><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>
                                &nbsp;</o:p></span></b></p>
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:12.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-weight:
bold">To Whom It May Concern<o:p></o:p></span></p>
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                <span style="mso-spacerun:yes">&nbsp;</span>Dear Sir/Madam</span><span lang="EN-GB" 
                                    style="font-size:11.0pt;mso-bidi-font-size:9.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p></o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
8.0pt;mso-bidi-font-size:6.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">This letter is to 
                                confirm that 
                            <strong>
                                <asp:Label ID="lblmemName" runat="server"></asp:Label>
                            </strong>
                                </span><b>
                                <span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-ansi-language:
EN-US;mso-fareast-language:EN-US">&nbsp;</span></b><span lang="EN-GB" 
                                    style="font-size:11.0pt;mso-bidi-font-size:9.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">is</span><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"> 
                                covered<span style="mso-spacerun:yes">&nbsp;&nbsp; </span>under BUPA Arabia’s 
                                health care program according to the following membership details:<o:p></o:p></span></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table style="width: 100%; background-color:White">
                    <tr>
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Contract name</span></td>
                        <td style="text-align:left" >
                            <strong>
                                <asp:Label ID="lblCustName" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Contract ID</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblCustNo" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Membership number</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblMembershipNo0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Year of birth</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblDateOfBirth0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Level of cover</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lbllCover" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Expiry Date</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblEffectiveTo0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table style="width: 100%" cellpadding="2" cellspacing="2">
                        <tr>
                        <td style="width: 40%"  valign="top">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td align="right" class="th" style="margin-right: 13px;"
                            valign="top">
                            &nbsp;</td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                All other terms and conditions are as per BUPA Arabia’s policy and CCHI.<o:p></o:p></span></p>
                        </td>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Please note that the 
                                above policy includes world-wide cover according to the following description:<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Medical appropriate 
                                claims are payable up to the member maximum limit <o:p></o:p></span>
                            </p>
                            <p style="text-align: justify; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <b><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                SR</span></b><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                </span><b><asp:Label ID="lblamount" runat="server" Text="250.000"></asp:Label><span 
                                    lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                </span></b>
                                <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:10.0pt;
font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">&nbsp;subject to a 100 percent reimbursement according to the 
                                normal and customary prices of member’s designated BUPA Arabia’s network of 
                                hospitals and polyclinics.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Emergency Evacuation 
                                outside the <st1:place w:st="on"><st1:placetype w:st="on">Kingdom</st1:PlaceType>
                                of <st1:placename w:st="on">Saudi Arabia</st1:PlaceName></st1:place> is covered 
                                as part of the member maximum limit in accordance with SOS International.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                This certificate was issued upon the member request.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Should you have any 
                                queries or wish to discuss this letter any further, please contact the 
                                undersigned. <o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            &nbsp;</td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Yours sincerely</span></td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    




<table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td >
                            <table style="width:100%">

                                <tr>
                                    <td align="left"> 
                            <!-- <asp:Image ID="Image3" runat="server" 
                                ImageUrl="~/Member/images/signature.png" /> -->

                                        <br />
                            Bupa Arabia<br />
                            Customer Service</td> 
							
							<td align="right">

                            <asp:Image ID="Image1" runat="server" ImageAlign="Baseline"
                                ImageUrl="~/Member/images/stamp.jpg" Height="132px" Width="206px" 
                            style="text-align: right"  />
                                    </td>
								
                                </tr>
                            </table>
                            <br />
                            &nbsp;</td>
                    </tr>
                </table>
                    </td>
                </tr>
              </table></td>
                </tr>
                </table>
    </div>

    </form>
</body>
</html>
