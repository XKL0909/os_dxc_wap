﻿using Bupa.Core.Utilities.Configuration;
using Bupa.OSWeb.Business;
using System;
using System.Diagnostics;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;

public partial class Member_Main : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
    string strMemberName;
    bool showNotify = false;
    string  currentPhysicalPath = HttpContext.Current.Request.PhysicalPath;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
			Response.Redirect("../Client/Main.aspx");
            Member m = null; 
            MemberHelper mh;
            string strMemberID=string.Empty;
            string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SabicContractNo");
            string strConfigSecondSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SecondSabicContractNo");

            if (string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                Response.Redirect("~/default.aspx",false);
                Context.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //to get the contract no
                strMemberID = Session["MembershipNo"].ToString();
                mh = new MemberHelper(strMemberID, "");
                try
                {
                    m = mh.GetMemberDetails();
                    //CR-248: Set th vip code for vip members checking
                    Session.Add("IsVIP", m.VIP);
					/* If contract in ticked and member is not VIP and member is log-in with his membership and password, enable below codel:* */
                    ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
                    OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN request;
                    OS_DXC_WAP.CaesarWS.EnqContInfoResponse_DN response;
                    request = new OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN();
                    request.contNo = m.ContractNumber;
                    request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
                    request.Username = WebPublication.CaesarSvcUsername;
                    request.Password = WebPublication.CaesarSvcPassword;

                    response = ws.EnqContInfo(request);
                    //CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
                    Session.Add("OnlineTicked", response.onl_Reimb_Ind);
                }
                catch (Exception ex)
                {
                    if (ex.Message == "Error when getting member details: Membership number is already terminated." ||
                        ex.Message == "Invalid member number / Member not active.")
                    {
                        Session["MembershipNo"] = string.Empty;
                        RegisterClientSideScriptForTerminated();
                        Context.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            if (!IsPostBack)
                DisplayPreAuthHist(strMemberID, Convert.ToString(Session["MemberName"]));

            //this function to show pop up for sabic members based on HCO request
            if (!IsPostBack && Session["HCO_Message"] == null && m != null && (strConfigSabicContractNo == m.ContractNumber || strConfigSecondSabicContractNo == m.ContractNumber))
            {
                tblHCOforSabic.Visible = true;
                //To show the HCO Pop up message
                RegisterClientSideScriptForHCO();
                Session["HCO_Message"] = "Done;";
            }

        }
        catch (Exception ex)
        {
            // Get stack trace for the exception with source file information
            var st = new StackTrace(ex, true);
            // Get the top stack frame
            var frame = st.GetFrame(0);
            // Get the line number from the stack frame
            var line = frame.GetFileLineNumber();

            string msgHeader = "Online Service Error report: ";

            sendMail("yahya.ammouri@bupa.com.sa", msgHeader,
              "\r\nMessage: " + ex.Message
              + "\r\nMemberNo: " + Convert.ToString(Session["MembershipNo"])
              + "\r\nFile: " + currentPhysicalPath
              + "\r\nLine: " + line.ToString()
              + "\r\nLine: Stack trace:" + ex.StackTrace);

            Response.Write(msgHeader + ex.Message);

        }
    }

    public void RegisterClientSideScriptForHCO()
    {
        try
        {
            String scriptName = "PopupScript";
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsClientScriptBlockRegistered(cstype, scriptName))
            {
                StringBuilder cstext2 = new StringBuilder();
                cstext2.Append("<script type=\"text/javascript\">");
                cstext2.Append("var shadow = $('<div id=\"shadowElem\"></div>');");
                cstext2.Append("var speed = 1000;");
                cstext2.Append("$(document).ready(function () {");
                cstext2.Append("$('body').prepend(shadow);");
                cstext2.Append("});");
                cstext2.Append("$(window).load(function () {");
                cstext2.Append("screenHeight = $(window).height();");
                cstext2.Append("screenWidth = $(window).width();");
                cstext2.Append("elemWidth = $('#dropElem').outerWidth(true);");
                cstext2.Append("elemHeight = $('#dropElem').outerHeight(true);");
                cstext2.Append("leftPosition = (screenWidth / 2) - (elemWidth / 2);");
                cstext2.Append("topPosition = (screenHeight / 2) - (elemHeight / 2);");
                cstext2.Append("$('#dropElem').css({");
                cstext2.Append("'left': leftPosition + 'px',");
                cstext2.Append("'top': -elemHeight + 'px'");
                cstext2.Append("});");
                cstext2.Append("$('#dropElem').show().animate({");
                cstext2.Append("'top': topPosition");
                cstext2.Append("}, speed);");
                cstext2.Append("shadow.animate({");
                cstext2.Append("'opacity': 0.7");
                cstext2.Append("}, speed);");
                cstext2.Append("$('#dropClose').click(function () {");
                cstext2.Append("shadow.animate({");
                cstext2.Append("'opacity': 0");
                cstext2.Append("}, speed);");
                cstext2.Append("$('#dropElem').animate({");
                cstext2.Append("'top': -elemHeight + 'px'");
                cstext2.Append("}, speed, function () {");
                cstext2.Append("shadow.remove();");
                cstext2.Append("$(this).remove();");
                cstext2.Append("});");
                cstext2.Append("});");
                cstext2.Append("});");
                cstext2.Append("</script>");
                cs.RegisterStartupScript(cstype, scriptName, cstext2.ToString(), false);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void RegisterClientSideScriptForTerminated()
    {
        try
        {
            String scriptName = "PopupScript";
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (!cs.IsClientScriptBlockRegistered(cstype, scriptName))
            {
                StringBuilder cstext2 = new StringBuilder();
                cstext2.Append("<script type=\"text/javascript\">");
                cstext2.Append("var shadow = $('<div id=\"shadowElem1\"></div>');");
                cstext2.Append("var speed = 1000;");
                cstext2.Append("$(document).ready(function () {");
                cstext2.Append("$('body').prepend(shadow);");
                cstext2.Append("});");
                cstext2.Append("$(window).load(function () {");
                cstext2.Append("screenHeight = $(window).height();");
                cstext2.Append("screenWidth = $(window).width();");
                cstext2.Append("elemWidth = $('#dropElem1').outerWidth(true);");
                cstext2.Append("elemHeight = $('#dropElem1').outerHeight(true);");
                cstext2.Append("leftPosition = (screenWidth / 2) - (elemWidth / 2);");
                cstext2.Append("topPosition = (screenHeight / 2) - (elemHeight / 2);");
                cstext2.Append("$('#dropElem1').css({");
                cstext2.Append("'left': leftPosition + 'px',");
                cstext2.Append("'top': -elemHeight + 'px'");
                cstext2.Append("});");
                cstext2.Append("$('#dropElem1').show().animate({");
                cstext2.Append("'top': topPosition");
                cstext2.Append("}, speed);");
                cstext2.Append("shadow.animate({");
                cstext2.Append("'opacity': 0.7");
                cstext2.Append("}, speed);");
                cstext2.Append("$('#dropClose1').click(function () { window.location='../default.aspx';");
                cstext2.Append("shadow.animate({");
                cstext2.Append("'opacity': 0");
                cstext2.Append("}, speed);");
                cstext2.Append("$('#dropElem1').animate({");
                cstext2.Append("'top': -elemHeight + 'px'");
                cstext2.Append("}, speed, function () {");
                cstext2.Append("shadow.remove();");
                cstext2.Append("$(this).remove();");
                cstext2.Append("});");
                cstext2.Append("});");
                cstext2.Append("});");
                cstext2.Append("</script>");
                cs.RegisterStartupScript(cstype, scriptName, cstext2.ToString(), false);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void sendMail(String toemail, string strMailSubject,string strMailBody)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(toemail);
            mail.From = new MailAddress("donotreply@bupa.com.sa");
            mail.Subject = strMailSubject;
            mail.Body = strMailBody;
            SmtpClient smtp = new SmtpClient();
            smtp.UseDefaultCredentials = false;
            smtp.Send(mail);
            mail.Dispose();
        }
        catch (SmtpException ex)
        {
            throw ex;
        }
    }

    private void DisplayPreAuthHist(string MemberNo, string MemberName)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.EnqPreHisInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqPreHisInfoResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqPreHisInfoRequest_DN();
            request.membershipNo = MemberNo;
            request.transactionID = WebPublication.GenerateTransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqPreHisInfo(request);
            if (response.detail != null)
            {
                if (DisplayPreauthHistoryResult(response))
                {
                    Notify.Visible = false;
                }
                else
                {
                    Notify.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private bool DisplayPreauthHistoryResult(OS_DXC_WAP.CaesarWS.EnqPreHisInfoResponse_DN response)
    {
        try
        {
            if (response.status == "0")
            {
                foreach (OS_DXC_WAP.CaesarWS.PreHisDetail_DN dtl in response.detail)
                {
                    if ((DateTime.Now - Convert.ToDateTime(dtl.receivedDateTime)).Days <= 15)
                    {
                        showNotify = true;
                        lblProviderName.Text = dtl.providerName;
                        lblDate.Text = dtl.receivedDateTime.ToString();
                        break;
                    }
                }
            }
            return showNotify;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void imgClose_Click(object sender, ImageClickEventArgs e)
    {
        Notify.Visible = false;
    }

    public void Popup(string message, string title)
    {
        //var radAlertString = "radalert('" + message + "', 330, 210,'" + title + "');location.href = '../default.aspx';";
        //var radAlertScript = "<script language=\"javascript\">function f(){" + radAlertString + 
        //    " Sys.Application.remove_load(f);}; Sys.Application.add_load(f);" +
        //    " </script>";
        //if (ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", radAlertString, true);
        //else
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radAlertScript);

        



        //string strScript = "<script>$(function() {";
        //strScript += "$( '#dialog-message' ).dialog({";
        //strScript +=  "modal: true,buttons: {Ok: function() {";
        //strScript += " $( this ).dialog( 'close' );}}});});";
        //strScript += "window.location='../default.aspx';";
        //strScript += "</script>";
        //this.ClientScript.RegisterStartupScript(this.GetType(), "Startup", strScript);
    }
}