﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Mobile.Master" AutoEventWireup="true"
    Inherits="MyCoverageMobilev2" Codebehind="MyCoverageMobilev2.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <style >
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>

  

 	<style type="text/css">
		body{
			font-family:Arial,Sans-serif;
			font-size:80%;
		}
		caption{
			padding-bottom:5px;
			
		}
		thead th,tfoot td{
			background:#e6f5ff; font-color:#ffffff;font-weight:bold;
		}
		tr.ruled{
			<%--background:#ccea68; font-weight:bold;--%>
		}    
		table{
			
			border-collapse:collapse;
		}
		th,td{
			
			border-collapse:collapse;
		}
		#mytable tr.ruled{
			<%--background:#333;
			color:#ccc;--%>
		}
    </style>
	<script type="text/javascript" src="../includes/tableruler.js"></script>
	<script type="text/javascript">
		window.onload=function(){tableruler();} 
    </script>


    



     <div  class="noPrint" > 
     
       <table width="100%">
        <tr valign="middle"><td >
        <asp:Label ID="lblAdd_Employee" runat="server"  Text="My Coverage" Font-Size=Large Font-Names="Arial" ></asp:Label> </td>
        <td style="text-align: right">&nbsp;</td></tr>

        </table>
        
     
        <table  class="t1"   style=" margin-right:0px;  "  width="100%" >
        </table>
      </div>      
            
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" />

        

 
  <div class="noPrint" >   
        <table style="width:100%">
            <tr>
                <td><table>
                                    <tr>
                                        <td>
                            <div class="styled-select"><asp:DropDownList Width="80px" ID="DDLDependentList" runat="server" >
                            </asp:DropDownList></div></td><td>
                            <asp:Button ID="btnRequestDependentCert" Width="163px" runat="server" OnClick="btnRequestDependentCert_Click"
                                Text="View Details" /></td></tr></table></td>
            </tr>
            <tr>
                <td align="center"><div id="DivProgress" > 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div></td>
            </tr>
        </table>
       
</div>   


        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
              <h1>Member Information</h1>
            <table  class="contentTable" width="100%" >
           
                <tr class="grayRow">
                    <td style="width: 114px">
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server"   Text="3456998"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 114px" >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server" Text=""   ></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td style="width: 114px" >
                        <asp:Label ID="lblCustomer_Name" runat="server" Text="Customer Name" Width="140px"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblCustomerName" runat="server"    Text="30/05/1970"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 114px">
                        <asp:Label ID="lblClass_Name" runat="server" Text="Class Name"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblClassName" runat="server" Text=""   ></asp:Label></td>
                </tr>
          
            </table>
          
           
           <br /> 
<h1>My Coverage</h1>
                <table width="100%">
                    <tr>
                        <td>
                        <div id="CoverageListReport"  visible="false" runat="server" >
      
       </div>
                        </td>
                    </tr>
                </table>

       

            
            
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="502px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
    <br />
    <br />
    <uc1:OSNav ID="OSNav1" runat="server" />
</asp:Content>
 <asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
   
</asp:Content>