﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;

public partial class Member_faxmobile : System.Web.UI.Page
{
    

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnsend_Click(object sender, EventArgs e)
    {
        if (Request["action"] == "pre")
        {

            System.IO.File.Create(Server.MapPath("../mobile/PreAuth" + Request["PID"] + ".txt")).Close();

            string[] lines = { "Bupa Arabia","MobileApp",txtRec.Text, txtFax.Text  };
            System.IO.File.WriteAllLines(Server.MapPath("../mobile/PreAuth" + Request["PID"] + ".txt"), lines);

            



            string ftpStr = @"ftp://172.26.4.79";
            Upload(ftpStr, @"Bupame.com\MobileApp", "Fa1con", Server.MapPath("/mobile/PreAuth" + Request["PID"] + ".htm"));
            Upload(ftpStr, @"Bupame.com\MobileApp", "Fa1con", Server.MapPath("/mobile/PreAuth" + Request["PID"] + ".txt"));
            lblMsg.Text = "Fax sent successfully";
            pnlEmailForm.Visible = false;
            System.IO.File.Delete(Server.MapPath("/mobile/PreAuth" + Request["PID"] + ".htm"));
            System.IO.File.Delete(Server.MapPath("/mobile/PreAuth" + Request["PID"] + ".txt"));
        }
        else
        {
            System.IO.File.Create(Server.MapPath("../mobile/Mypage" + Request["MembershipNo"] + ".txt")).Close();

            string[] lines = { "Bupa Arabia", "MobileApp", txtRec.Text, txtFax.Text };
            System.IO.File.WriteAllLines(Server.MapPath("../mobile/Mypage" + Request["MembershipNo"] + ".txt"), lines);

            string ftpStr = @"ftp://172.26.4.79";
            Upload(ftpStr, @"Bupame.com\MobileApp", "Fa1con", Server.MapPath("/mobile/Mypage" + Request["MembershipNo"] + ".htm"));
            Upload(ftpStr, @"Bupame.com\MobileApp", "Fa1con", Server.MapPath("/mobile/Mypage" + Request["MembershipNo"]  + ".txt"));
            lblMsg.Text = "Fax sent successfully";
            pnlEmailForm.Visible = false;
            System.IO.File.Delete(Server.MapPath("/mobile/Mypage" + Request["MembershipNo"] + ".htm"));
            System.IO.File.Delete(Server.MapPath("/mobile/Mypage" + Request["MembershipNo"] + ".txt"));
        }
     
    }

    private static void Upload(string ftpServer, string userName, string password, string filename)
    {
        using (System.Net.WebClient client = new System.Net.WebClient())
        {
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.UploadFile(ftpServer + "/" + new FileInfo(filename).Name,"STOR",filename);
        }
    } 


}