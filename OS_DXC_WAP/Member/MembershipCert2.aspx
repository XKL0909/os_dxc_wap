﻿<%@ Page Language="C#" AutoEventWireup="true"   MasterPageFile="~/Templates/Inner.Master"  EnableEventValidation="false"
    Inherits="MembershipCert2" Codebehind="MembershipCert2.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1.Export, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server">

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            if ($("#RadioDiv input:radio:checked").val() == 2) {
                document.getElementById('provider_tr').style.display = "none";
            }
            else {
                document.getElementById('provider_tr').style.display = "";
            }
        });
        $(document).ready(function () {
            $('#RadioDiv input').click(function () {
                $("#info").text('Selected Value: ' + $("#RadioDiv input:radio:checked").val());
                if ($("#RadioDiv input:radio:checked").val() == 2) {
                    document.getElementById('provider_tr').style.display = "none";
                }
                else {
                    document.getElementById('provider_tr').style.display = "";
                }
            });
        });
</script>

    <div class="adbanner" align="right" >
        <hr>
    </div>
    <div class="noPrint" id="frm" align="right" runat="server">
      
        <table style="width:100%">
        <tr>
            <td align="left">
            <table>
                <tr>
                    <td>Request purpose  &nbsp;&nbsp;
                        
                </td>


                    <td>
                         <div id="RadioDiv">
                            <asp:RadioButtonList ID="rdnPurpose" runat="server" CssClass="myrblclass"   RepeatDirection="Horizontal" RepeatLayout="Flow" CellSpacing="50" Width="250px">   
                              <asp:ListItem Value="1">Visit Hospital</asp:ListItem>  
                              <asp:ListItem Value="2">Visa Application</asp:ListItem> 
                            </asp:RadioButtonList>
                        </div>

                      
                  </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Select member</td>
                    <td style="vertical-align:bottom; padding-top:20px;">
                        <dx:ASPxComboBox ID="cbMemberList" runat="server" ValueType="System.String" Width="250px">
                   
                        </dx:ASPxComboBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfv2" ControlToValidate="cbMemberList" ErrorMessage=" required" ValidationGroup="1" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="provider_tr" >
                    <td><dx:ASPxLabel runat="server" ID="lblProvName" Text="Select Provider" ClientInstanceName="lblProvName" >

                        </dx:ASPxLabel>

                    </td>
                    <td >

             <dx:ASPxComboBox ID="cboProviders"  runat="server" DropDownStyle="DropDown" 
                IncrementalFilteringMode="Contains" ClientInstanceName="ASPxComboBox1"
               
                Width="250px" />

                    </td>
                </tr>
                <tr>
                    <td>



                      
                    </td>
                    <td><asp:Label ID="lblMessage" runat="server" Font-Size="Small" ForeColor="Red" Text="Please select your request purpose and member." Visible ="false"></asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td align="right" style="text-align:right;">
                        <dx:ASPxButton ID="ASPxButton1" runat="server" Text="View Certificate"  ValidationGroup="1" 
                    onclick="ASPxButton1_Click">
                </dx:ASPxButton></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" style="color:blue;">
                
                
                

            </td>
            <td>
                
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                
            </td>
        </tr>
        <tr>
            <td align="left" style="width:50%;" >
                 <asp:Literal ID="lblSabicMessage" runat="server" ></asp:Literal>
            </td>
            <td></td>
        </tr>
                    <tr>
           <td colspan="2" style="text-align:left;">
                <table style="width:100%">
                    <tr>
                        <td style="width:1%;white-space:nowrap;">
                             <asp:RadioButton ID="rdnSendEmail" runat="server" GroupName="sending" Text="Send by Email" Visible="False" />
                            
                        </td>
                        <td>
                            <asp:TextBox ID="txtSendEmail" runat="server" Text="" Visible="False"></asp:TextBox>
                        </td>
                        <td style="width:1%;white-space:nowrap;">
                            <asp:RadioButton ID="rdnSendFax" runat="server" GroupName="sending" Text="Send by Fax" Visible="False" />
                          
                        </td>
                        <td>
                            <asp:TextBox ID="txtSendFax" runat="server" Text="" Visible="False"></asp:TextBox>

                        </td>
                        <td style="width:10%">
                            <asp:Button ID="btnSend" runat="server" Text="Send" Visible="False" />
                           
                           
                        </td>
                        <td style="width:20%; text-align:center;">
                             <a href="CerPrint.aspx" target="_blank">
                              <img border="0" src="../images/Printer.gif" alt="Print" style="" runat="server" id="printimg" visible="false">

                        </a>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label id="lblCaesarErrorMessage" runat="server" ForeColor="Red" Text="" ></asp:Label> 
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" style="font-weight:bolder; text-decoration:underline;text-align:center;">

                <asp:Label id="lblTitle" runat="server" visible="false" Text="Certificate of Membership" ></asp:Label> 
            </td>
        </tr>
        </table>
    </div>
          

        <script language="javascript" type="text/javascript">
            function clientActiveTabChanged(sender, args) {
                // Post back if it is the 3rd tab (0 based)
                if (sender.get_activeTabIndex() == 2) {
                    __doPostBack('<%= this.btnTab2.UniqueID %>', '');
                }

                if (sender.get_activeTabIndex() == 0) {
                    __doPostBack('<%= this.btnTab1.UniqueID %>', '');
                }

            }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button"/>
       
        <div class="noPrint" >
        
        </div>                
       
        <br />
         <div runat="server" id="divCer" visible="false">
               <table width="90%" border="0" cellspacing="0" cellpadding="0" class="contentTableWhite" align="center" >
               <tr>
                <td align="center">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                        <ProgressTemplate>
                            <img src="../images/spinner.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
               </tr>
                <tr>
                    <td>
                        <table style="width: 100%; padding-left: 0px; padding-right: 0px;" cellspacing="0">
                            <tr>
                                <td style="width: 499px">
                                    <%--<asp:Label ID="lblHospital1" runat="server" Text="Attention all BUPA Arabia Network" Visible="false"></asp:Label>--%>
                                </td>
                                <td style="width: 127px">&nbsp;</td>
                                <td style="width: 40%; text-align: right">
                                    <%-- <asp:Label ID="lblHospital2" runat="server" 
                                Text="السادة / شبكة مستشفيات بوبا العربية" Visible="false"></asp:Label>--%>
                                &nbsp;</td>
                            </tr>
                            <tr>
                           <%--<td >
                                     <strong >
                                        <asp:Label ID="lblToHow" Visible="False" runat="server" Text="TO "></asp:Label>
                                    </strong>
                                </td>
                                <td  style="width: 127px">
                                    <strong >
                                       <asp:Label ID="lblTo" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td style="text-align:right">
                                    <b><asp:Label ID="lblToHowAr" runat="server" 
                                        Text="الى" Visible="False"></asp:Label>
                                    </b></td>--%>
                            </tr>
                            <tr>
                                <td style="width: 499px">&nbsp;</td>
                                <td style="width: 127px">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                            <%--<td style="width: 499px">
                                    <span >Subject: </span>
                                    <asp:Label ID="lblSubject" runat="server" Text=""></asp:Label></td>
                                <td  style="width: 127px">
                                    &nbsp;</td>
                                <td Style="text-align:right; direction: rtl;">
                                    الموضوع:
                                    <asp:Label ID="lblSubAr" runat="server" Text=""></asp:Label>
                                </td>--%>
                            </tr>
                            <tr>
                                <td style="width: 499px"></td>
                                <td style="width: 127px">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                           <%--<td style="width: 499px" >
                                    Certificate Issue Date</td>
                                <td  style="width: 127px">
                                    <strong >
                                        <asp:Label ID="lblIssueDate" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td style="text-align:right">
                                    تاريخ الاصدار
                                </td>--%>
                                <td style="width: 127px">
                                    <strong>Insurance Companies Office
                                    </strong>
                                    <br />
                                    <br />
                                </td>
                                <td style="width: 127px">&nbsp;</td>
                                <td style="text-align: right" >
                                    <strong>مكتب شركات التأمين
                                    </strong>
                                    <br />
                                    <br />
                                </td>
                                
                            </tr>
                            
                            <tr>
                                <td style="width: 499px">Dear Sir,
                                    <br />
                                    <br />
                                    This is to confirm that the below mentioned member is medically covered under Bupa Arabia Health Care scheme with the following information:
                                    <br />
                                    <br />
                                </td>
                                <td style="width: 127px">&nbsp;</td>
                                <td style="text-align: right" >السادة المحترمين 
                                    <br />
                                    <br />
                                    إن هذه الوثيقة تؤكد ان المذكور مغطى طبيا
                                    <br />
                                    تحت برنامج بوبا العربية للرعاية الصحية وفقا للبيانات التالية
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%; background-color: White">
                            <tr>
                                <td align="left" style="width: 30%">Name:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblMemberName" runat="server"></asp:Label></strong></td>
                                <td align="right" style="text-align: right" style="width: 30%" class="ar">:الاسم</td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px">Membership Number:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblMembershipNo" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" >:رقم العضوية
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px">ID Number:</td>
                                <td style="width: 45%; text-align: center">
                                    <asp:Label ID="lblIDNumber" runat="server"></asp:Label>&nbsp;</td>
                                <td style="text-align: right" class="ar">
                                    :رقم الهوية
                                    &nbsp;</td>
                            </tr>

                            <tr>
                                <td align="left" style="width: 320px">Class Name:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblClass" runat="server"></asp:Label></strong></td>
                                <td align="right" style="text-align: right" >:درجة التغطية
                                </td>
                            </tr>

                            <tr>
                                <td align="left" style="width: 320px">Network:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblNetwork" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" >:شبكة
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px">Year of Birth:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblDateOfBirth" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" >:تاريخ الميلاد
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px">Deductible:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblDeductible" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" >:نسبة المشاركة
                                </td>
                            </tr>

                            <tr>
                                <td align="left" style="width: 320px">Effective From:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblEffectiveFrom" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" >:صالحة من تاريخ&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 320px">Effective To:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblEffectiveTo" runat="server"></asp:Label>
                                    </strong>
                                </td>
                                <td align="right" style="text-align: right" >:صالحة الى تاريخ
                                </td>
                            </tr>

                            <tr>
                                <td align="left" style="width: 320px">Customer Name:</td>
                                <td style="width: 45%; text-align: center">
                                    <strong>
                                        <asp:Label ID="lblCustomerName" runat="server"></asp:Label></strong></td>
                                <td style="text-align: right" >
                                    :أسم العميل</td>
                            </tr>
                            <tr>
                                <td style="width: 320px">&nbsp;</td>
                                <td style="width: 239px">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 320px">All Other terms and conditions are as per policy</td>
                                <td style="width: 239px">&nbsp;</td>
                                <td style="text-align: right" >جميع شروط الغطاء تخضع لبنود التعاقد</td>
                            </tr>
                            <tr>
                                <td style="width: 320px">&nbsp;</td>
                                <td style="width: 239px">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 40%" valign="top">Should he / she need any treatment that is covered under his / her scheme, please accept this letter instead of the membership card for one visit only 
                                 <br />
                                 <br />
                                </td>
                                <td>&nbsp;</td>
                                <td align="right"  style="margin-right: 13px;"
                                    valign="top">عند الحاجة لتقديم خدمة علاجية مدرجة تحت جدول المزايا الخاص بدرجة العضوية المذكورة سابقا يرجى قبول هذا الخطاب كبديل لبطاقة العضوية وذلك لمرة واحدة فقط
                                 <br />
                                 <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">Please attach this letter and a copy of the member ID with the claim invoices.
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right"  valign="top">
                                    يرجى إرفاق صورة الهوية للعميل و الخطاب مع المطالبة عند إرسالها إلى بوبا العربية.
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">As per normal eligibility practice, please check the membership number for the treatment services in the hospital according to the approved protocol and the member network.
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right"  valign="top">املين منكم تدقيق اهلية رقم العضوية المذكور لتلقي الخدمات العلاجية في المستشفى حسب البروتوكول المعتمد وشبكة التغطية المعتمدة.
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">Should you have any queries, please contact one of our advisers on the Bupa Arabia customer service line 920023009
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right"  valign="top">في حال وجود اي استفسارات، الرجاء عدم التردد في الإتصال عل هاتف الرقم المجاني لخدمات العملاء 920023009
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 437px" valign="top">
                                    <br />
                                    <br />
                                    <br />
                                    Sincerely yours,
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right"  valign="top">
                                    <br />
                                    <br />
                                    <br />
                                    مع اطيب التحيات
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 437px" valign="top">
                                    <br />
                                    <strong>Bupa Arabia
                                        <br />
                                        Customer Service 
                                    </strong>
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right"  valign="top">
                                    <br />
                                    <strong>بوبا العربية
                                <br />
                                        خدمة العملاء                                                                                                                          
                                    </strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--Footer details--%>
                   <tr>
                    <td>
                        <br />
                        <br />
                        <br />
                        <br />
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 40%; color: #0073C8;" valign="top"><strong>Bupa Arabia for Cooperative Insurance</strong>
                                </td>
                                <td>&nbsp;</td>
                                <td align="right" class="th" style="margin-right: 13px; color: #0073C8;" valign="top">
                                 بوبا العربية للتامين التعاوني
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">PO Box 23807, Jeddah 21436, Saudi Arabia
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right"  valign="top">
                                   ص.ب 23807, جدة 21436 المملكة العربية السعودية.
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">Tel: +966 920 000 456 | Fax: +966 920 000 724
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="th" valign="top">
                                   +966 تليفون: 456 000 920 966+ | فاكس: 724 000 920 
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 437px" valign="top">www.bupa.com.sa
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="th" valign="top">
                                    www.bupa.com.sa
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 437px" valign="top">
                                    CR number: 4030178881
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="th" valign="top">
                                   س .ت: 4030178881
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 437px" valign="top">
                                    Paid up Capital: SR800,000,000
                                </td>
                                <td style="width: 55px">&nbsp;</td>
                                <td align="right" class="th" valign="top">
                                    رأس المل المد فوع: 800,000,000 ريا ل سعودي
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
              </table>
               
           
         </div>


         <div runat="server" id="visa" visible="false">
              
               <table width="90%" border="0" cellspacing="0" cellpadding="0" class="contentTableWhite" align="center" >
               <tr>
                <td align="center">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="100">
                        <ProgressTemplate>
                            <img src="../images/spinner.gif" />

                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
               </tr>
                <tr>
                    <td>
                    <table style="width: 100%; padding-left:0px; padding-right:0px;" cellspacing="0"   >
                    <tr >
                        <td >
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-weight:bold">Certificate of membership<o:p></o:p></span></p>
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <b style="mso-bidi-font-weight:normal"><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>
                                &nbsp;</o:p></span></b></p>
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:12.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-bidi-font-weight:
bold">To Whom It May Concern<o:p></o:p></span></p>
                            <p align="center" 
                                style="text-align: center; mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                <span style="mso-spacerun:yes">&nbsp;</span>Dear Sir/Madam</span><span lang="EN-GB" 
                                    style="font-size:11.0pt;mso-bidi-font-size:9.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p></o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
8.0pt;mso-bidi-font-size:6.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">This letter is to 
                                confirm that 
                            <strong>
                                <asp:Label ID="lblmemName" runat="server"></asp:Label>
                            </strong>
                                </span><b>
                                <span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-ansi-language:
EN-US;mso-fareast-language:EN-US">&nbsp;</span></b><span lang="EN-GB" 
                                    style="font-size:11.0pt;mso-bidi-font-size:9.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">is</span><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"> 
                                covered<span style="mso-spacerun:yes">&nbsp;&nbsp; </span>under BUPA Arabia’s 
                                health care program according to the following membership details:<o:p></o:p></span></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table style="width: 100%; background-color:White">
                    <tr>
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Contract name</span></td>
                        <td style="text-align:left" >
                            <strong>
                                <asp:Label ID="lblCustName" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Contract ID</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblCustNo" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Membership number</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblMembershipNo0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Year of birth</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblDateOfBirth0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Level of cover</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lbllCover" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr >
                        <td align="left"  style="width: 169px" class="productsButtons">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Expiry Date</span></td>
                        <td style="text-align:left">
                            <strong>
                                <asp:Label ID="lblEffectiveTo0" runat="server"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table style="width: 100%" cellpadding="2" cellspacing="2">
                        <tr>
                        <td style="width: 40%"  valign="top">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td align="right" class="th" style="margin-right: 13px;"
                            valign="top">
                            &nbsp;</td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                All other terms and conditions are as per BUPA Arabia’s policy and CCHI.<o:p></o:p></span></p>
                        </td>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Please note that the 
                                above policy includes world-wide cover according to the following description:<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Medical appropriate 
                                claims are payable up to the member maximum limit <o:p></o:p></span>
                            </p>
                            <p style="text-align: justify; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <b><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                SR</span></b><span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                </span><b><asp:Label ID="lblamount" runat="server" Text=""></asp:Label><span 
                                    lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                </span></b>
                                <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:10.0pt;
font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">&nbsp;subject to a 100 percent reimbursement according to the 
                                normal and customary prices of member’s designated BUPA Arabia’s network of 
                                hospitals and polyclinics.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Emergency Evacuation 
                                outside the <st1:place w:st="on"><st1:placetype w:st="on">Kingdom</st1:PlaceType>
                                of <st1:placename w:st="on">Saudi Arabia</st1:PlaceName></st1:place> is covered 
                                as part of the member maximum limit in accordance with SOS International.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="mso-outline-level: 1; tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" 
                                    style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">
                                This certificate was issued upon the member request.<o:p></o:p></span></p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <p style="tab-stops: center 207.65pt right 415.3pt; font-size: 10.0pt; font-family: 'Times New Roman', serif; margin-left: 0in; margin-right: 0in; margin-top: 0in; margin-bottom: .0001pt;">
                                <span lang="EN-GB" style="font-size:
12.0pt;mso-bidi-font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;">Should you have any 
                                queries or wish to discuss this letter any further, please contact the 
                                undersigned. <o:p></o:p></span>
                            </p>
                        </td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            &nbsp;</td>
                    </tr>
                        <tr>
                        <td  valign="top" colspan="3">
                            <span lang="EN-GB" style="font-size:12.0pt;mso-bidi-font-size:
10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB;mso-bidi-language:AR-SA">Yours sincerely</span></td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    




<table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td >
                            &nbsp;</td>
                    </tr>
                </table>
                    </td>
                </tr>
              </table>
               
           
         </div>

        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
        <asp:Label ID="lblErrID" runat="server" Visible="False"></asp:Label><br />
        <div>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
            <br />
           
            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
                        
           
       
        </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p>
        View and print your membership certificate</p>
</asp:Content>

