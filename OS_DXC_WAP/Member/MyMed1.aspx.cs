using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class MyMed1 : System.Web.UI.Page 
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
    string strMemberName;
    DataTable DT;
    DataSet DS;

    int strAge;
    int strCardNo;
    string strGender;
    string strIqamaID;
    string strMobileNo;
    string strPolicy;


 ///  string TabFlag;     Need to create a session variable to hold this value

    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
////////        TabFlag = "M";
       
        // btnSubmitAll.Attributes.Add("onclick", "anyCheck1()"); 
        if (string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            Response.Redirect("../default.aspx");
        strMembershipNo = Session["MembershipNo"].ToString();
        strMemberName = Session["MemberName"].ToString();


        if (!Page.IsPostBack)
        {

            strMembershipNo = Session["MembershipNo"].ToString();
            strMemberName = Session["MemberName"].ToString();


            DS = new DataSet();
            DS.ReadXmlSchema(Server.MapPath("XMLFile\\MyMedicationFormat1.xml"));
            Session["Dataset"] = DS;
            DT = DS.Tables[0];

            DisplayMyMedication(strMembershipNo);
            
            ////for (int i = 0; i <= 2; i++)
            ////{
            ////   // LoadFirstRows(i);
            ////}

            Session["dt"] = DT;
            GridView1.DataSource = DS;

            GridView1.DataBind();
        }
        DS = (DataSet)Session["Dataset"];
        GridView1.DataSource = DS;

    }

    // function used for Tab System when user requset for dependent ceritifcate
    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
       
       // loadingImage.Visible = true;
       // System.Threading.Thread.Sleep(3000);
       // this.lblTabLoadStatus.Visible = false;// xt = "test completed";
      //  loadingImage.Visible = false;
       //lblMemberName.Text = "Dependent Name";

        DisplayMyMedication(DDLDependentList.SelectedValue);

        //lblCustomerName.Text = DDLDependentList.SelectedItem.ToString();//.Text;
       // GridView1.Visible= true; //.DataSource = DT;
        //GridView1.Visible
       // Message1.Visible
        //TextBox1.Text = "updated also";
        //TextBox3.Visible = false;
        // Code to load data from database or other data source here
    }


    // function used for Tab System to retrieve family list
    private void DisplayDependentListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        ////////////string[,] ResponseDetails;

        ////////////ResponseDetails[0,0] = "test";
        ////////////ResponseDetails[0,1] = "test";
        ////////////ResponseDetails[0,2] = "test";


        ////////string arr(2,4);
        //////// string ar()() = new String({New String() {"", "", ""}, New String() {"", "", ""}};

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table id='tblGrid' name='tblGrid' style='position:relative;'   border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  > <!-- style='behavior:url(TableCtrl.htc);' -->   <thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            DDLDependentList.Items.Clear();

            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

                ListItem DepList = new ListItem();
                DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                DDLDependentList.Items.Add(DepList);

            }
        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }


    private void DisplayMedicationListResult(OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
           
            //dtl.proratedOverallAnnualLimit = ;

            double totalCost = 0.00;
            if(DT == null)
            DT = (DataTable)Session["dt"];
        DT.Clear();
            sbResponse.Append("<table id='table1'  border='1px' cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px;  ;'><thead><tr><th>Request Refill</th>   <th>Service Description</th> <th>Service Code</th><th>Supply Period </th><th>Supply Date From</th><th>Suppy Date to</th><th>Quantity</th><th>Estimated Cost</th></tr>	</thead><tbody> ");



            foreach ( OS_DXC_WAP.CaesarWS.MyMedicationDetail_DN  dtl in response.detail)
            {

                DataRow dr = DT.NewRow();
                dr[0] = dtl.servDesc; //.servCode;  "1";
                dr[1] = dtl.supplyPeriod; // "2";
                dr[2] = String.Format("{0:d}", dtl.supplyFrom);
                //dr[2] = dtl.supplyFrom; // "01/01/2000";
                dr[3] = String.Format("{0:d}", dtl.supplyTo);
                //dr[3] = dtl.supplyTo; // "3";
                dr[4] =  "1";
                dr[5] = dtl.estimateAmount.ToString("0.00"); //.cost;//.estimateAmount; // "5";
                dr[6] = dtl.servCode;

                DT.Rows.Add(dr);

                


                sbResponse.Append("<tr><td> <input type='checkbox'  checked='checked'  />  </td> <td>" + dtl.servDesc + "</td> <td>" + dtl.servCode + "</td> <td>" + dtl.supplyPeriod + "</td> <td>" + String.Format("{0:d}", dtl.supplyFrom) + "</td> <td>" + String.Format("{0:d}", dtl.supplyTo) + "</td> <td>1</td> <td>" + dtl.estimateAmount + "</td></tr>");

                string i = dtl.servCode;
                CheckBox chk = new CheckBox();
                //chk.ID = "checkbox[]";
                chk.ID = "CheckBox0_" + i;
                chk.Checked = true;
                TableRow Row = new TableRow();
                TableCell Cellcheck = new TableCell();
                Cellcheck.Width = 20;
                Cellcheck.Controls.Add(chk);
                Row.Cells.Add(Cellcheck);


                TextBox txt0 = new TextBox();
                txt0.Text = dtl.servDesc;
                txt0.ID = "txt1_" + i;
                txt0.ReadOnly = true;
                txt0.Width = 150;
                TableCell Cell = new TableCell();
                Cell.Width = 200;
                Cell.Controls.Add(txt0);
                Row.Cells.Add(Cell);

                DropDownList ddl = new DropDownList();
                ddl.ID = "ddlPeriod3_" + i;            

                ListItem item2 = new ListItem();
                item2.Text = "1";
                item2.Value = "1";
                ddl.Items.Add(item2);

                ListItem item3 = new ListItem();
                item3.Text = "2";
                item3.Value = "2";
                ddl.Items.Add(item3);

                ListItem item4 = new ListItem();
                item4.Text = "3";
                item4.Value = "3";
                ddl.Items.Add(item4);
                ddl.Width = 50;
                TableCell Cell3 = new TableCell();
                Cell3.Width = 51;
                Cell3.Controls.Add(ddl);
                Row.Cells.Add(Cell3);

                TextBox txt1 = new TextBox();
                txt1.Text = String.Format("{0:d}", dtl.supplyFrom);
                txt1.ID = "txt2_" + i;
                txt1.Width = 80;
 ////               txt1.Attributes.Add("onClick", "fPopCalendar('" + txt1.ID + "');");
                //txt1.Attributes.Add("onChange", " txt4_" + i + ".value = Format( DateAdd( 'm' , ddlPeriod3_" + i + ".value ,txt2_" + i + ".value), 'dd-mm-yyyy', '' );"); // + i + ".value =  myDate.setMonth(myDate.getMonth() + 2);");
                TableCell Cell1 = new TableCell();
                Cell1.Width = 10;
                Cell1.Controls.Add(txt1);
                Row.Cells.Add(Cell1);

                TextBox txt2 = new TextBox();
                txt2.ID = "txt4_" + i;
                txt2.Text = String.Format("{0:d}", DateTime.Today.AddMonths(1)); // DateTime.Today.AddMonths(1);
                txt2.Attributes.Add("onMouseover", " txt4_" + i + ".value = Format( DateAdd( 'm' , ddlPeriod3_" + i + ".value ,txt2_" + i + ".value), 'mm-dd-yyyy', '1' );"); // + i + ".value =  myDate.setMonth(myDate.getMonth() + 2);");
                txt2.ReadOnly = true;
                txt2.Width = 80;
                TableCell Cell2 = new TableCell();
                Cell2.Width = 10;
                Cell2.Controls.Add(txt2);
                Row.Cells.Add(Cell2);

                TextBox txt3 = new TextBox();
                txt3.ID = "txt5_" + i;
                txt3.Text = "1";
                txt3.ReadOnly = true;
                txt3.Width = 70;
                TableCell Cell4 = new TableCell();
                Cell4.Width = 10;
                Cell4.Controls.Add(txt3);
                Row.Cells.Add(Cell4);

                TextBox txt4 = new TextBox();
                txt4.ID = "txt6_" + i;
                txt4.Text = dtl.estimateAmount.ToString();
                txt4.ReadOnly = true;
                txt4.Width = 70;
                TableCell Cell5 = new TableCell();
                Cell5.Width = 10;
                Cell5.Controls.Add(txt4);
                Row.Cells.Add(Cell5);
                totalCost += dtl.estimateAmount; 

                TextBox txt5 = new TextBox();
                txt5.ID = "txt7_" + i;
                txt5.Text = dtl.diagCode;
                txt5.Visible = false;
                txt5.Width = 60;
                TableCell Cell6 = new TableCell();
                Cell6.Width = 10;
                Cell6.Controls.Add(txt5);
                Row.Cells.Add(Cell6);

                TextBox txt6 = new TextBox();
                txt6.ID = "txt8_" + i;
                txt6.Text = dtl.diagDesc;
                txt6.Visible = false;
                txt6.Width = 10;
                TableCell Cell7 = new TableCell();
                Cell7.Width = 50;
                Cell7.Controls.Add(txt6);
                Row.Cells.Add(Cell7);

 //               Table2.Rows.Add(Row);               

            }
           sbResponse.Append("</tbody></table>");

            XmlDocument doc = new XmlDocument();
             doc.LoadXml(@sbResponse.ToString());

//             lblTotalCost.Text = totalCost.ToString();

            CoverageListReport.Visible = true;



          // CoverageListReport.InnerHtml =   sbResponse.ToString();
        }
    }





// function used for Tab System when Tab1 [member tab] is clicked
    protected void btnTab1_Click(object sender, EventArgs args)
    {
  //////      if (TabFlag != "M")
        DisplayMyMedication(strMembershipNo);
        //.Text = "test completed";
    }

    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void btnTab2_Click(object sender, EventArgs args)
    {

        //System.Threading.Thread.Sleep(3000);
        //.Text = "test completed";
        
 ///////       if(TabFlag != "D")
        {

       // UpdatePanel2.Visible = true;



        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        request.membershipNo = strMembershipNo;
        //request..membershipNo = 
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

        //request.IqamaNo =  "5523234443";
        //request.TotalNoOfFamily = "3";

        try
        {
            StringBuilder sb = new StringBuilder(200);
            
            response = ws.EnqMbrListInfo(request);
            

            if (response.errorID != "0")
            {
                sb.Append(response.errorMessage).Append("\n");
                lblErrorMessage.Text = sb.ToString();
            }
            else
            {
                DisplayDependentListResult(response);
                lblErrorMessage.Text = "";
                Message1.Text = "";

                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlTextWriter.Formatting = Formatting.Indented;
                xs.Serialize(xmlTextWriter, response);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                this.txtBox_output.Text = XmlizedString;
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text =ex.Message;
        }

        if (DDLDependentList.Items.Count > 0)
        {
  //////          TabFlag = "D";
            DDLDependentList.Visible = true;
            this.lblTabLoadStatus.Visible = false;
            btnRequestDependentCert.Visible = true;
        }
        else
            this.lblTabLoadStatus.Text = "No dependent for this Member Found";


     }
        
   
    }






    private void DisplayMyMedication(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrRequest_DN  request;
        OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
           // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrRequest_DN();
            request.membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = TransactionManager.TransactionID(); // long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); 

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.ReqMyMedicationByMbr(request); // ws.ReqMyMedicationByMbr(request);

            //response = ws.EnqMbrListInfo(request);
           // Cursor.Current = Cursors.Default;


            if (response.detail[0].servCode != "")
            {

                txtAge.Value = response.age.ToString();
                txtCardNo.Value = response.cardIssueNumber.ToString();
                txtGender.Value = response.gender;
                txtIqamaID.Value = response.memberID_Igama;
                txtMobileNo.Value = response.memberMobileNo;
                txtPolicy.Value = response.policyNo;


                lblMembershipNo.Text = request.membershipNo;
               // lblMemberName.Text = request.membershipNo;
                // lblClassName.Text = response.className;
                // lblCustomerName.Text = response.companyName;
                //DDLProviderList.DataTextField = "";// response.provider_Name;
                //DDLDependentList.DataValueField = response.provider_Code;

                DDLProviderList.Items.Add(new ListItem("Select", "0"));

                int totalProv = response.provider_Code.Length;
                int i;
                for (i = 0; i < totalProv; i++)
                {
                    ListItem ProList = new ListItem();
                    ProList.Text = response.provider_Name[i].ToString();
                    ProList.Value = response.provider_Code[i].ToString();
                    if(response.provider_Fax[i] != null)
                        ProList.Value += "|" + response.provider_Fax[i].ToString();
//ProList.Value = response.provider_Code[i].ToString();
                    DDLProviderList.Items.Add(ProList);

                }


                //lblCustomerName.Text = response.provider_Code.Length;
              Message1.Text= "";
              Message1.Visible = false;
                DisplayMedicationListResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;


                GridView1.DataSource = DT;

                GridView1.DataBind();

                GridView1.Visible = true;
                PnlUserInfo.Visible = true;
                PnlButtons.Visible = true;

                if (tabMemberName.HeaderText == "Member")
                {
                    tabMemberName.HeaderText = strMemberName;
                    //lblMemberName.Text = strMemberName;
                }



            }
            else
            {
                Message1.Text = "No existing medication found for this member..";
                PnlButtons.Visible = false;
                Message1.Visible = true;
               // Message1.Style["display"] = "block";
               // GridView1.DataSource = DT;
              //  GridView1.DataBind();
                GridView1.Visible = false;
                PanelEdit.Visible = false;
                PnlUserInfo.Visible = false;
                CoverageListReport.Visible = false;
            }
            //Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

}



    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }


//    protected void btnUpdateAll_Click(object sender, EventArgs e)
//    {

//        string name = null;
//        string address = null;
//        string dept = null;
//        string droplist = null;
//        string Col5 = null;
//        string Col6 = null;

//        string str1 = "";

//        for (int i = 0; i <= Table2.Rows.Count - 1; i++)
//        {

//            CheckBox cb = (CheckBox)Table2.Rows[i].Cells[0].Controls[0];
//            if (cb.Checked == true)
//            {
//                if (i != 0)
//                {
//                    str1 += " - ";
//                }

//                TextBox t1 = (TextBox)Table2.Rows[i].Cells[1].Controls[0];
//                name = t1.Text;
//                TextBox t2 = (TextBox)Table2.Rows[i].Cells[3].Controls[0];
//                address = t2.Text;
//                DropDownList ddl = (DropDownList)Table2.Rows[i].Cells[2].Controls[0];
//                droplist = ddl.SelectedValue;
//                droplist += "/" + ddl.SelectedItem.Text;
//                TextBox t3 = (TextBox)Table2.Rows[i].Cells[4].Controls[0];

//                t3.Text = name + address;
//                dept = t3.Text;

//                TextBox t4 = (TextBox)Table2.Rows[i].Cells[5].Controls[0];
//                Col5 = t4.Text;
//                TextBox t5 = (TextBox)Table2.Rows[i].Cells[6].Controls[0];
//                Col6 = t5.Text;

//                str1 += "[" + name + ", (" + droplist + "), " + address + ", " + dept + ", " + Col5 + ", " + Col6 + "]";
//            }

//        }


//        CoverageListReport.Visible = true;
//        CoverageListReport.InnerHtml = str1; // sbResponse.ToString();
//    }


//    protected void Button1_Click(object sender, EventArgs e)
//    {
//        ///sbResponse.Append("<tr><td> <input type='checkbox'  />  </td> <td>" + dtl.servDesc + "</td> <td>" + dtl.servCode + "</td> <td>" + dtl.supplyPeriod + "</td> <td>" + String.Format("{0:d}", dtl.supplyFrom) + "</td> <td>" + String.Format("{0:d}", dtl.supplyTo) + "</td> <td>1</td> <td>" + dtl.estimateAmount + "</td></tr>");
//         string name = null;   //serv desc
//         string address = null;
//         DateTime DateTo = DateTime.Now;
//         string droplist = null;
//         string Col5 = null;
//         string Col6 = null;
//         int TotalQuantity= 0;
//         int TotalAmount = 0;

//         string str1 = "";

//         OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN();
//         OS_DXC_WAP.CaesarWS.PharmacyInfoResponse_DN response;

//         try
//         {

             

//             request.Username = WebPublication.CaesarSvcUsername;
//             request.Password = WebPublication.CaesarSvcPassword;


//             request.transactionID = 8888882074;// long.Parse(String.Format("{0}{1}{2}{3}{4}{5}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
//             //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}", int.Parse(DateTime.Now.ToShortDateString,), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
//             request.transactionType = "N";
//             request.preauthorisation_ID = 0; //TODO: can this be zero ??
//             request.membershipNo = strMembershipNo;
//             request.policyNo = null; // strMembershipNo;
//             request.memberID_Igama = null;
//             request.memberName = strMemberName;//txt_mbr_name.Text.Trim();
//             request.memberMobileNo = null; // txt_mbr_mobile.Text.Trim();
//             // request.cardIssueNumber = ""; int.Parse(txt_card_no.Text.Trim());
//             request.age = 31; // int.Parse(txt_age.Text.Trim());
//             request.gender = "F";//TODO: gender name to be retrieved 
//             request.physician_Name = "abc"; // txt_phy_name.Text.Trim();
//             request.providerCode = "21770";  // txt_pv_code.Text.Trim();
//             request.providerFaxNo = ""; // txt_pv_fax.Text.Trim();
//             request.patientFileNo = "123";// txt_patient_file.Text.Trim();
//             request.treatmentType = "O"; // txt_treatmt_type.Text.Trim();
//             request.departmentType = "PHA";// txt_dept_type.Text.Trim();
//           ///    request.dateOfAdmission = Convert.ToDateTime(DateTime.Now.ToString("YYYY-MM-dd")); // DateTime.ParseExact("2/2/2009", "mm-ddd-yyyy", ); // dtp_admis_date.Value;
//               request.dateOfAdmission = DateTime.Now; //ParseExact(    Convert.ToDateTime(String.Format("{0:M/d/yyyy}", DateTime.Now));
//             ///  myDate.ToString("YYYY-MM-dd");

             
//             request.lengthOfStay = 0; // int.Parse(txt_len_stay.Text.Trim());
//             request.diagnosisCode = "I10"; // txt_diag_code.Text.Trim();
//             request.diagnosisDescription = ""; // txt_diag_desc.Text.Trim();    
//             request.verificationID = 0;


//             int[] Detail_ItemNo = new int[2];
//             String[] Detail_BenefitHead = new String[2];
//             String[] Detail_ServiceCode = new String[2];
//             String[] Detail_ServiceDescription = new String[2];
//             Int64[] Detail_Quantity = new Int64[2];
//             double[] Detail_EstimatedCost = new double[2];
//             String[] Detail_SupplyPeriod = new String[2];
//             DateTime[] Detail_SupplyDateFrom = new DateTime[2];
//             DateTime[] Detail_SupplyDateTo = new DateTime[2]; 





//             for (int i = 1; i <= Table2.Rows.Count - 1; i++)
//             {

//                 CheckBox cb = (CheckBox)Table2.Rows[i].Cells[0].Controls[0];
//                 if (cb.Checked == true)
//                 {
//                     if (i != 0)
//                     {
//                         str1 += " - ";
//                     }

//                     TextBox t1 = (TextBox)Table2.Rows[i].Cells[1].Controls[0];
//                     name = t1.Text;
//                     TextBox t2 = (TextBox)Table2.Rows[i].Cells[3].Controls[0];
//                     address = t2.Text;
//                     DropDownList ddl = (DropDownList)Table2.Rows[i].Cells[2].Controls[0];
//                     droplist = ddl.SelectedValue;
//                     //droplist += "/" + ddl.SelectedItem.Text;
//                     TextBox t3 = (TextBox)Table2.Rows[i].Cells[4].Controls[0];

//                     /////t3.Text = name + address;
//                     DateTo = DateTime.Parse(t3.Text); //, "yyyy-mm-dd");

//                     TextBox t4 = (TextBox)Table2.Rows[i].Cells[5].Controls[0];
//                     Col5 = t4.Text;
//                     TextBox t5 = (TextBox)Table2.Rows[i].Cells[6].Controls[0];
//                     Col6 = t5.Text;

//                     TotalAmount += TotalAmount + int.Parse(Col6);
//                     TotalQuantity += TotalQuantity + 1;
//                     //str1 += "[" + name + ", (" + droplist + "), " + address + ", " + dept + ", " + Col5 + ", " + Col6 + "]";



//                     //if (!isNull(TotalQuantity))
//                         Detail_ItemNo.SetValue(TotalQuantity, i);
//                     // if (!isNull(dgv_detail["benHead", i].Value))
//                     Detail_BenefitHead.SetValue("C1", i);
//                   //  if (!isNull(t1.Text))
//                         Detail_ServiceCode.SetValue(t1.Text, i);
//                   //  if (!isNull(t1.Text))
//                         Detail_ServiceDescription.SetValue(t1.Text, i);
//                  //   if (!isNull(t4.Text))
//                         Detail_Quantity.SetValue(Int64.Parse(t4.Text), i);
//                  //   if (!isNull(t5.Text))
//                         Detail_EstimatedCost.SetValue(double.Parse(t5.Text), i);
//                  //   if (!isNull(ddl.SelectedValue))
//                         Detail_SupplyPeriod.SetValue(ddl.SelectedValue, i);
  
//                     //   if (!isNull(t2.Text))
//                         t2.Text = "2009-03-03";
//                        Detail_SupplyDateFrom.SetValue(DateTime.ParseExact("1999-09-01", "yyyy-MM-dd", null), i);
//                         Detail_SupplyDateTo.SetValue(DateTime.ParseExact("1999-09-01", "yyyy-MM-dd", null), i);
//        //                 DateTime.ParseExact(DateTime.Now, "dd-MM-yyyy", null)
////Detail_SupplyDateTo.SetValue(DateTime.ParseExact(DateTo, "yyyy-MM-dd", null), i);

//                     /////////////////////////////////////////    Detail_SupplyDateFrom.SetValue(Convert.ToDateTime(t2.Text).ToString("YYYY-MM-dd"), i);
//                       //  Detail_SupplyDateTo.SetValue(Convert.ToDateTime(String.Format("{0:M/d/yyyy}", Convert.ToDateTime(t2.Text))), i);
// //Detail_SupplyDateFrom.SetValue(DateTime.ParseExact(t2.Text.ToString(), "yyyy-MM-dd", null), i);

//                  //   if (!isNull(t3.Text))
//                         /////////////////////////////////////Detail_SupplyDateTo.SetValue(Convert.ToDateTime(t3.Text).ToString("YYYY-MM-dd"), i);

//                       //  Detail_SupplyDateTo.SetValue(Convert.ToDateTime(String.Format("{0:M/d/yyyy}", Convert.ToDateTime(t3.Text))), i);
                     
//                     // Convert.ToDateTime(DateTime.Today.AddMilliseconds(1).ToString("YYYY-MM-dd")); // DateTime.ParseExact("2/2/2009", "mm-ddd-yyyy", ); // dtp_admis_date.Value;


//                     // str1 +=   name + ", " + droplist + ", " + address + ", " + dept + ", " + Col5 + ", " + Col6  ;


//     //                    DateTime.ParseExact("1999-09-01 21:34 PM", "yyyy-MM-dd HH:mm tt", null)

//                 }


//             }

//             request.quantity = TotalQuantity;
//             request.estimatedAmount = TotalAmount;


//             request.detail_ItemNo = Detail_ItemNo;
//             request.detail_BenefitHead = Detail_BenefitHead;
//             request.detail_ServiceCode = Detail_ServiceCode;
//             request.detail_ServiceDescription = Detail_ServiceDescription;
//             request.detail_Quantity = Detail_Quantity;
//             request.detail_EstimatedCost = Detail_EstimatedCost;
//             request.detail_SupplyPeriod = Detail_SupplyPeriod;
//             request.detail_SupplyDateFrom = Detail_SupplyDateFrom;
//             request.detail_SupplyDateTo = Detail_SupplyDateTo;
//         }
//            catch (Exception ec)
//         {
//             Message1.Text = ec.StackTrace;
//         }

//         try
//         {
//             StringBuilder sb = new StringBuilder(200);
//             response = ws.ReqPharInfo(request);

//             if (response.errorID[0] != "0")
//             {
//                 for (int i = 0; i < response.errorMessage.Length; i++)
//                     sb.Append(response.errorMessage[i]).Append("\n");

//                 Message1.Text = sb.ToString();
//             }
//             else
//             {
                 
//                 CoverageListReport.Visible = true;
//                 CoverageListReport.InnerHtml = str1; 
//                 //String XmlizedString = null;
//                 //MemoryStream memoryStream = new MemoryStream();
//                 //XmlSerializer xs = new XmlSerializer(typeof(BME_WS.PharmacyInfoResponse_DN));
//                 //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
//                 //xmlTextWriter.Formatting = Formatting.Indented;
//                 //xs.Serialize(xmlTextWriter, response);
//                 //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
//                 //XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
//                 //Message1.Text = XmlizedString;
//             }
//         }
//         catch (Exception ex)
//         {

//             Message1.Text += "\n" + ex.Message;
//         }



//        // sbResponse.ToString();
//    }


    //private void UpdateReqPharInfo()
    //{
    //    OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN();
    //    OS_DXC_WAP.CaesarWS.PharmacyInfoResponse_DN response;

    //    request.Username = WebPublication.CaesarSvcUsername;
    //    request.Password = WebPublication.CaesarSvcPassword;

    //    try
    //    {
    //            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); 
    //            request.transactionType = "N";
    //            //request.preauthorisation_ID = null ;
    //            request.membershipNo = strMembershipNo;
    //            request.policyNo = strMembershipNo; 
    //            request.memberID_Igama = "";
    //            request.memberName = strMemberName;//txt_mbr_name.Text.Trim();
    //            request.memberMobileNo = ""; // txt_mbr_mobile.Text.Trim();
    //           // request.cardIssueNumber = ""; int.Parse(txt_card_no.Text.Trim());
    //            request.age = 20; // int.Parse(txt_age.Text.Trim());
    //            request.gender = "";// txt_gender.Text.Trim();
    //            request.physician_Name = ""; // txt_phy_name.Text.Trim();
    //            request.providerCode = "21770"; // txt_pv_code.Text.Trim();
    //            request.providerFaxNo = ""; // txt_pv_fax.Text.Trim();
    //            request.patientFileNo = "";// txt_patient_file.Text.Trim();
    //            request.treatmentType = "O"; // txt_treatmt_type.Text.Trim();
    //            request.departmentType = "PHA";// txt_dept_type.Text.Trim();
    //                //  request.dateOfAdmission = DateTime.ParseExact("2/2/2009", "mm-ddd-yyyy", ); // dtp_admis_date.Value;
    //           // request.lengthOfStay = ""; // int.Parse(txt_len_stay.Text.Trim());
    //            request.diagnosisCode = "I10"; // txt_diag_code.Text.Trim();
    //            request.diagnosisDescription = ""; // txt_diag_desc.Text.Trim();    


    //            request.quantity = 240; // totalquantity; // Int64.Parse(txt_quan.Text.Trim());
    //            request.estimatedAmount = 1223.50; // totalAmount; // double.Parse(txt_estim_amt.Text.Trim());

    //        int[] Detail_ItemNo = new int[dgv_detail.RowCount - 1];
    //        //String[] Detail_BenefitHead = new String[dgv_detail.RowCount - 1];
    //        //String[] Detail_ServiceCode = new String[dgv_detail.RowCount - 1];
    //        //String[] Detail_ServiceDescription = new String[dgv_detail.RowCount - 1];
    //        //Int64[] Detail_Quantity = new Int64[dgv_detail.RowCount - 1];
    //        //double[] Detail_EstimatedCost = new double[dgv_detail.RowCount - 1];
    //        //String[] Detail_SupplyPeriod = new String[dgv_detail.RowCount - 1];
    //        //DateTime[] Detail_SupplyDateFrom = new DateTime[dgv_detail.RowCount - 1];
    //        //DateTime[] Detail_SupplyDateTo = new DateTime[dgv_detail.RowCount - 1];



    //        //for (int i = 0; i < dgv_detail.RowCount - 1; i++)
    //        //{

    //        //    if (!isNull(dgv_detail["itemNo", i].Value))
    //        //        Detail_ItemNo.SetValue(int.Parse(dgv_detail["itemNo", i].Value.ToString()), i);
    //        //    if (!isNull(dgv_detail["benHead", i].Value))
    //        //        Detail_BenefitHead.SetValue(dgv_detail["benHead", i].Value.ToString(), i);
    //        //    if (!isNull(dgv_detail["serviceCode", i].Value))
    //        //        Detail_ServiceCode.SetValue(dgv_detail["serviceCode", i].Value.ToString(), i);
    //        //    if (!isNull(dgv_detail["serviceDesc", i].Value))
    //        //        Detail_ServiceDescription.SetValue(dgv_detail["serviceDesc", i].Value.ToString(), i);
    //        //    if (!isNull(dgv_detail["quantity", i].Value))
    //        //        Detail_Quantity.SetValue(Int64.Parse(dgv_detail["quantity", i].Value.ToString()), i);
    //        //    if (!isNull(dgv_detail["estimatedCost", i].Value))
    //        //        Detail_EstimatedCost.SetValue(double.Parse(dgv_detail["estimatedCost", i].Value.ToString()), i);
    //        //    if (!isNull(dgv_detail["supplyPeriod", i].Value))
    //        //        Detail_SupplyPeriod.SetValue(dgv_detail["supplyPeriod", i].Value.ToString(), i);
    //        //    if (!isNull(dgv_detail["supplyDateFrom", i].Value))
    //        //        Detail_SupplyDateFrom.SetValue(DateTime.ParseExact(dgv_detail["supplyDateFrom", i].Value.ToString(), "yyyy-MM-dd", null), i);
    //        //    if (!isNull(dgv_detail["supplyDateTo", i].Value))
    //        //        Detail_SupplyDateTo.SetValue(DateTime.ParseExact(dgv_detail["supplyDateTo", i].Value.ToString(), "yyyy-MM-dd", null), i);


    //        //}


    //        //request.detail_ItemNo = Detail_ItemNo;
    //        //request.detail_BenefitHead = Detail_BenefitHead;
    //        //request.detail_ServiceCode = Detail_ServiceCode;
    //        //request.detail_ServiceDescription = Detail_ServiceDescription;
    //        //request.detail_Quantity = Detail_Quantity;
    //        //request.detail_EstimatedCost = Detail_EstimatedCost;
    //        //request.detail_SupplyPeriod = Detail_SupplyPeriod;
    //        //request.detail_SupplyDateFrom = Detail_SupplyDateFrom;
    //        //request.detail_SupplyDateTo = Detail_SupplyDateTo;

    //    }
    //    catch (Exception ec)
    //    {
    //        Message1.Text = ec.StackTrace;
    //    }
    //    try
    //    {
    //        StringBuilder sb = new StringBuilder(200);
    //        response = ws.ReqPharInfo(request);

    //        if (response.errorID[0] != "0")
    //        {
    //            for (int i = 0; i < response.errorMessage.Length; i++)
    //                sb.Append(response.errorMessage[i]).Append("\n");

    //            Message1.Text = sb.ToString();
    //        }
    //        else
    //        {
    //            //String XmlizedString = null;
    //            //MemoryStream memoryStream = new MemoryStream();
    //            //XmlSerializer xs = new XmlSerializer(typeof(BME_WS.PharmacyInfoResponse_DN));
    //            //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
    //            //xmlTextWriter.Formatting = Formatting.Indented;
    //            //xs.Serialize(xmlTextWriter, response);
    //            //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
    //            //XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
    //            //Message1.Text = XmlizedString;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
            
    //        Message1.Text += "\n" + ex.Message;
    //    }

    //}





    protected void GridView1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        int selectIndex = GridView1.SelectedIndex;

        FindData(selectIndex);
        PanelEdit.Visible = true;
        btnSubmitAll.Visible = false;
       // DateFrom.Attributes.Add("onMouseover",  " txtDate.value = Format( DateAdd( 'm' ,  ddlPeriod.SelectedValue ,DateFrom.value), 'mm-dd-yyyy', '1' );");
        //txt2.Attributes.Add("onMouseover", " txt4_" + i + ".value = Format( DateAdd( 'm' , ddlPeriod3_" + i + ".value ,txt2_" + i + ".value), 'mm-dd-yyyy', '1' );"); // + i + ".value =  myDate.setMonth(myDate.getMonth() + 2);");
       // txtDate.Attributes.Add("onClick", "fPopCalendar('" + txtDate.ID + "');");

        bntUpdate.Visible = true;
    }

    public void FindData(int SI)
    {

        DT = (DataTable)Session["dt"]; //DT = DS.Tables[0];

        txtName.Text = (String)DT.Rows[SI][0];
        ddlPeriod.SelectedValue = (String)DT.Rows[SI][1];
      txtDateOfEffect.Text   = (String)DT.Rows[SI][2];
        txtDateTo.Text = (String)DT.Rows[SI][3];
        txtQuantity.Text = (String)DT.Rows[SI][4];
        txtCost.Text = (String)DT.Rows[SI][5];
        Session["dt"] = DT;
    }


    public void updateVal(int index)
    {
        DT = (DataTable)Session["dt"];

        DT.Rows[index][0] = txtName.Text;
        DT.Rows[index][1] = ddlPeriod.SelectedValue;
        DT.Rows[index][2] = txtDateOfEffect.Text;// DateFrom.CalendarDateString;
        DT.Rows[index][3] = txtDateTo.Text; // String.Format("{0:d}", DateTime.ParseExact(txtDateOfEffect.Text, "dd/MM/yyyy", null).AddMonths(Convert.ToInt32(ddlPeriod.SelectedValue)).ToString());
      //DT.Rows[index][3] = String.Format("{0:d}", DateTime.ParseExact(txtDateOfEffect.Text, "dd/MM/yyyy", null).AddMonths(Convert.ToInt32(ddlPeriod.SelectedValue)));

    //    DT.Rows[index][3] = txtDateTo.Text;
        DT.Rows[index][4] = txtQuantity.Text;
        DT.Rows[index][5] = txtCost.Text;
    }
    public void addval()
    {
        DT = (DataTable)Session["dt"];

        DataRow dr = DT.NewRow();

        dr[0] = txtName.Text;
        dr[1] = ddlPeriod.SelectedValue;
        dr[2] = txtDateOfEffect.Text;// DateFrom.CalendarDateString;
        dr[3] = txtDateTo.Text;
        dr[4] = txtQuantity.Text;
        dr[5] = txtCost.Text;

        DT.Rows.Add(dr);


        reset();
    }
    public void reset()
    {
        txtName.Text = "";
        ddlPeriod.SelectedValue = "0";
     txtDateOfEffect.Text =  System.DateTime.Today.ToShortDateString();
        txtDateTo.Text = "";
        txtQuantity.Text = "";
        txtCost.Text = "";

        GridView1.DataBind();
    }



    protected void btnSubmitAll_Click(object sender, System.EventArgs e)
    {
        int NoOfChecked = Convert.ToInt16(txtNoOfChecked.Text);
        DT = (DataTable)Session["dt"];
        int c = DT.Columns.Count - 1;
        int r = DT.Rows.Count - 1;
        string[,] str = new string[r + 1, c + 1];
        string strr = "";

        string strServiceDesc = txtName.Text;
        string strPeriod = ddlPeriod.SelectedValue;
        string strDateFrom = txtDateOfEffect.Text; // DateFrom.CalendarDateString;
        string strDateTo = txtDateTo.Text;
        string strQuantity = txtQuantity.Text;
        string strCost = txtCost.Text;
        string strCode = "";


        int TotalQuantity = 0;
        double TotalAmount = 0;

        string str1 = "";

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.PharmacyInfoResponse_DN response;



        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        request.transactionID = TransactionManager.TransactionID();// long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));
//        request.transactionID = 8888882074;// long.Parse(String.Format("{0}{1}{2}{3}{4}{5}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
        //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}", int.Parse(DateTime.Now.ToShortDateString,), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
        request.transactionType = "N";
        request.preauthorisation_ID = 0; //TODO: can this be zero ??
        request.membershipNo = lblMembershipNo.Text;// strMembershipNo;

        request.memberID_Igama = txtIqamaID.Value; // null;
        request.memberName = strMemberName;//txt_mbr_name.Text.Trim();
        request.memberMobileNo = txtMobileNo.Value;// null; // txt_mbr_mobile.Text.Trim();
        // request.cardIssueNumber = ""; int.Parse(txt_card_no.Text.Trim());
        request.age = Convert.ToInt32(txtAge.Value); // int.Parse(txt_age.Text.Trim());
        request.gender = txtGender.Value; // "F";//TODO: gender name to be retrieved 
        request.physician_Name =  "abc"; // txt_phy_name.Text.Trim();
        request.providerCode = DDLProviderList.SelectedValue; // "21770";  // txt_pv_code.Text.Trim();

        if (DDLProviderList.SelectedValue.Contains("|") == true)
        {

            // DDLProviderList.SelectedValue.Split("|");
            string[] words = DDLProviderList.SelectedValue.Split('|');

            request.providerCode = words[0]; // txtServiceDesc.Text;
            request.providerFaxNo = words[1]; // txtServiceDesc.Text;
        }
        else 
        {
            request.providerCode = DDLProviderList.SelectedValue;
        }

       // request.providerFaxNo = "6651284"; // txt_pv_fax.Text.Trim();
        request.patientFileNo = "123";// txt_patient_file.Text.Trim();
        request.treatmentType = "O"; // txt_treatmt_type.Text.Trim();
        request.departmentType = "PHA";// txt_dept_type.Text.Trim();
        request.cardIssueNumber = Convert.ToInt32(txtCardNo.Value);
        request.policyNo = txtPolicy.Value;
        
        ///    request.dateOfAdmission = Convert.ToDateTime(DateTime.Now.ToString("YYYY-MM-dd")); // DateTime.ParseExact("2/2/2009", "mm-ddd-yyyy", ); // dtp_admis_date.Value;
        request.dateOfAdmission = DateTime.Now; //ParseExact(    Convert.ToDateTime(String.Format("{0:M/d/yyyy}", DateTime.Now));
        ///  myDate.ToString("YYYY-MM-dd");


        request.lengthOfStay = 0; // int.Parse(txt_len_stay.Text.Trim());
        request.diagnosisCode = "I10"; // txt_diag_code.Text.Trim();
        request.diagnosisDescription = ""; // txt_diag_desc.Text.Trim();    
        request.verificationID = 0;


        int[] Detail_ItemNo = new int[NoOfChecked];
        String[] Detail_BenefitHead = new String[NoOfChecked];
        String[] Detail_ServiceType = new String[NoOfChecked];
        String[] Detail_ServiceCode = new String[NoOfChecked];
        String[] Detail_ServiceDescription = new String[NoOfChecked];
        Int64[] Detail_Quantity = new Int64[NoOfChecked];
        double[] Detail_EstimatedCost = new double[NoOfChecked];
        String[] Detail_SupplyPeriod = new String[NoOfChecked];
        DateTime[] Detail_SupplyDateFrom = new DateTime[NoOfChecked];
        DateTime[] Detail_SupplyDateTo = new DateTime[NoOfChecked];

        int CheckedDataCount = 0; // this would be increement in the if condion for the request array

        for (int i = 0; i <= r; i++)
        {
            strServiceDesc = (String)DT.Rows[i][0];
            strPeriod = (String)DT.Rows[i][1];
            strDateFrom = (String)DT.Rows[i][2];
            strDateTo = (String)DT.Rows[i][3];
            strQuantity = (String)DT.Rows[i][4];
            strCost = (String)DT.Rows[i][5];
            strCode = (String)DT.Rows[i][6];


            GridViewRow gr = GridView1.Rows[i];
            CheckBox check = (CheckBox)gr.FindControl("Chec");





            if (check.Checked == true)
            {
                TotalQuantity = TotalQuantity +  Convert.ToInt16(strQuantity);
                TotalAmount = TotalAmount + Convert.ToDouble(strCost);

                if (strPeriod == "1") strDateTo = DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", null).AddDays(27).ToShortDateString(); // DateTime.Today.AddDays(27).ToShortDateString();
                if (strPeriod == "2") strDateTo = DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", null).AddDays(55).ToShortDateString(); // DateTime.Today.AddDays(27).ToShortDateString();
                if (strPeriod == "3") strDateTo = DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", null).AddDays(83).ToShortDateString(); // DateTime.Today.AddDays(27).ToShortDateString();


                Detail_ItemNo.SetValue(CheckedDataCount, CheckedDataCount);
                Detail_BenefitHead.SetValue("C1", CheckedDataCount);
                Detail_ServiceType.SetValue("ME", CheckedDataCount);
                Detail_ServiceCode.SetValue(strCode, CheckedDataCount);
                Detail_ServiceDescription.SetValue(strServiceDesc, CheckedDataCount);
                Detail_Quantity.SetValue(Int64.Parse(strQuantity), CheckedDataCount);
                Detail_EstimatedCost.SetValue(double.Parse(strCost), CheckedDataCount);
                Detail_SupplyPeriod.SetValue(strPeriod, CheckedDataCount);
                Detail_SupplyDateFrom.SetValue(DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", null), CheckedDataCount);
                Detail_SupplyDateTo.SetValue(DateTime.ParseExact(strDateTo, "dd/MM/yyyy", null), CheckedDataCount);


               // strr += "[ " + strMemberName + "," + strPeriod + "," + strDateFrom + "," + strDateTo + "," + strQuantity + "," + strCost + " ]";

                CheckedDataCount++;

            }
        }


        request.quantity = TotalQuantity;
        request.estimatedAmount = TotalAmount;


        request.detail_ItemNo = Detail_ItemNo;
        request.detail_BenefitHead = Detail_BenefitHead;
        request.detail_ServiceType = Detail_ServiceType;
        request.detail_ServiceCode = Detail_ServiceCode;
        request.detail_ServiceDescription = Detail_ServiceDescription;
        request.detail_Quantity = Detail_Quantity;
        request.detail_EstimatedCost = Detail_EstimatedCost;
        request.detail_SupplyPeriod = Detail_SupplyPeriod;
        request.detail_SupplyDateFrom = Detail_SupplyDateFrom;
        request.detail_SupplyDateTo = Detail_SupplyDateTo;




        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.ReqPharInfo(request);

            if (response.errorID[0] != "0")
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("\n");

                Message1.Text = sb.ToString();
            }
            else
            {
              //PreauthDetails.aspx?PID=1299207&PC=20016&EN=1  
                Response.Redirect("PreauthDetails1.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR");
                //Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1");
                Message1.Text = "Successfully submitted";
            }
        }
        catch (Exception ex)
        {

            Message1.Text += "\n" + ex.Message;
        }





        Response.Write(strr);
    }

    protected void bntUpdate_Click(object sender, System.EventArgs e)
    {
        updateVal(GridView1.SelectedIndex);
        GridView1.DataSource = DT;
        GridView1.DataBind();
        GridView1.SelectedIndex = -1;
        reset();
        PanelEdit.Visible = false;
        bntUpdate.Visible = false;
        btnSubmitAll.Visible = true;
    }

    protected void bntCancel_Click(object sender, System.EventArgs e)
    {
        GridView1.SelectedIndex = -1;
        reset();
        PanelEdit.Visible = false;
        bntUpdate.Visible = false;
        btnSubmitAll.Visible = true;
    }


}