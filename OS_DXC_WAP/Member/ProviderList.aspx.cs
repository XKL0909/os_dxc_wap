﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

using DevExpress.Web.ASPxGridView;

public partial class Member_ProviderList : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    private string _url = "";
    string membershipNumber = string.Empty, loc = string.Empty;
    private Hashtable hasQueryValue;
    protected void Page_Load(object sender, EventArgs e)
    {


        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqProvLocInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqProvLocInfoResponse_DN response;

        response = null;

        hasQueryValue = new Hashtable();
        string queryStringValues = string.Empty;
        if (Request.QueryString.Count > 0)
        {
            try
            {   var val = Request.QueryString["val"];
                queryStringValues = Cryption.Decrypt(val);
                hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                membershipNumber = hasQueryValue.ContainsKey("mem") ? Convert.ToString(hasQueryValue["mem"]) : string.Empty;
                loc = hasQueryValue.ContainsKey("loc") ? Convert.ToString(hasQueryValue["loc"]) : string.Empty;
            }
            catch (Exception ex)
            {
                Response.Write("Invalid rquest!");
                return;
            }

        }
        else
        {
            Response.Write("Invalid rquest!");
            return;
        }



        if (Convert.ToString(Session["loc"]) != null && Convert.ToString(Session["MembershipNo"]) != null)
        {

            switch (Convert.ToString(Session["loc"]))
            {
                case "E":
                    lblArea.Text = "Eastern Province Providers";
                    break;
                case "W":
                    lblArea.Text = "Western Province Providers";
                    break;
                case "C":
                    lblArea.Text = "Central Province Providers";
                    break;
                case "S":
                    lblArea.Text = "Southern Province Providers";
                    break;
                case "N":
                    lblArea.Text = "Northern Province Providers";
                    break;
                case "O":
                    lblArea.Text = "Outside Kingdom Providers";
                    break;
            }


            request = new OS_DXC_WAP.CaesarWS.EnqProvLocInfoRequest_DN();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                ///request.membershipNo = Request.QueryString["mem"]; //txt_mbr.Text.Trim();
                request.membershipNo = membershipNumber;
            }
            else
            {
                request.membershipNo = Session["MembershipNo"].ToString(); //txt_mbr.Text.Trim();
            }

            if (!string.IsNullOrEmpty(Convert.ToString(Session["loc"])))
            {
                ///request.region = Request.QueryString["loc"];
                request.region = loc;
            }
            else
            {
                request.region = Convert.ToString(Session["loc"]);
            }
            
            try
            {

                StringBuilder sb = new StringBuilder(200);
                response = ws.EnqProvLocInfo(request);
                //response = OS_DXC_WAP.CaesarWS. ws.EnqProvLocInfo(request);

                if (response.errorID != "0")
                {

                }
                else
                {
                    DisplayResult(response);

                }
            }
            catch (Exception ex)
            {

            }
        }
       



       
    }


    private void DisplayResult(OS_DXC_WAP.CaesarWS.EnqProvLocInfoResponse_DN response)
    {

        StringBuilder msge;

        StringBuilder sbResponse = new StringBuilder(2200);
        DataTable ProviderList = new DataTable();
        ProviderList.Columns.Add("Provider Name");
        ProviderList.Columns.Add("Address 1");
        ProviderList.Columns.Add("Address 2");
        ProviderList.Columns.Add("Address 3");
        ProviderList.Columns.Add("District");
        ProviderList.Columns.Add("Tel No 1");
        ProviderList.Columns.Add("Tel No 2");
        ProviderList.Columns.Add("latitude");
        ProviderList.Columns.Add("longitude");
        ProviderList.Columns.Add("Map Location");
        int _count = 0;
        if (response.status == "0")
        {
            foreach (OS_DXC_WAP.CaesarWS.ProvDetail_DN dtl in response.detail)
            {
                DataRow row = ProviderList.NewRow();
                row["Provider Name"] = dtl.providerName;
                row["Address 1"] = dtl.address1;
                row["Address 2"] = dtl.address2;
                row["Address 3"] = dtl.address3;
                row["District"] = dtl.district;
                row["Tel No 1"] = dtl.telNo1;
                row["Tel No 2"] = dtl.telNo2;
                row["latitude"] = dtl.latitude;
                row["longitude"] = dtl.longitude;
                row["Map Location"] = "&latitude=" + dtl.latitude + "&longitude=" + dtl.longitude + "&city=" + dtl.district;
                ProviderList.Rows.Add(row);
                if (!string.IsNullOrEmpty(Convert.ToString(dtl.latitude)))
                {
                    _count = _count + 1;
                }
            }
            DataSet dsProviders = new DataSet();
            dsProviders.Tables.Add(ProviderList);
            gvProviderResult.DataSource = dsProviders.Tables[0].DefaultView;
            gvProviderResult.DataBind();
            gvProviderResult.Visible = true;
            //Response.Write(_count.ToString());
        }
        else
        {
            msge = new StringBuilder(100);
        }
    }

    protected void gvProviderResult_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {


        if (e.DataColumn.FieldName == "Provider Name")
        {
            _url = "map.aspx?pro=" + e.CellValue.ToString();
        }



        if (e.DataColumn.FieldName == "Map Location")
        {
            _url = _url + e.CellValue;



            e.Cell.HorizontalAlign = HorizontalAlign.Center;


            HyperLink hl = new HyperLink();
            hl.NavigateUrl = _url;
            hl.ImageUrl = "~/images/maps.png";

            //hl.Text = "View";
            Int32 iLocation = _url.IndexOf("&latitude=&longitude=");

            if (iLocation >= 0)
            {
                hl.Visible = false;
            }
            e.Cell.Text = "";
            e.Cell.Controls.Add(hl);
        }



    }


    private static int IndexOf(string str, string substr)
    {
        bool match;

        for (int i = 0; i < str.Length - substr.Length + 1; ++i)
        {
            match = true;
            for (int j = 0; j < substr.Length; ++j)
            {
                if (str[i + j] != substr[j])
                {
                    match = false;
                    break;
                }
            }
            if (match) return i;
        }

        return -1;
    }

    protected void gvProviderResult_DataBound(object sender, EventArgs e)
    {
        gvProviderResult.Columns["latitude"].Visible = false;
        gvProviderResult.Columns["longitude"].Visible = false;

    }
}