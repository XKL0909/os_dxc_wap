﻿<%@ Page Language="C#"  MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true"
    Inherits="MyMedication" Codebehind="MyMedication.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">

    

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

.style1 {
	text-align: center;
}
.style2 {
	font-weight: bold;
	text-align: right;
}
.style3 {
	text-align: right;
}

.style5 {
	border-right: 1px solid #808080;
	border-bottom: 1px solid #808080;
	border-style: none none solid none;
	border-width: 0px 0px 1px 0px;
	text-align: center;
}
.style6 {
	border-right: 1px none #808080;
	border-bottom: 1px solid #808080;
	border-width: 0px 0px 1px 0px;
	text-align: right;
	border-left-style: none;
	border-top-style: none;
	border-bottom-color: #808080;
}
.style8 {
	font-family: Arial, sans-serif;
	font-size: 9pt;
}

.style9 {
	border-right: 0px none #808080;
	border-bottom: 1px solid #808080;
	text-align: left;
	font-size: 9pt;
	font-family: Arial, sans-serif;
	border-left-style: none;
	border-left-width: 0px;
	border-top-style: none;
	border-top-width: 0px;
}

.style10 {
	border-right: 0px none #808080;
	border-bottom: 1px solid #808080;
	text-align: center;
	font-family: Arial, sans-serif;
	font-size: 9pt;
	border-left-style: none;
	border-left-width: 0px;
	border-top-style: none;
	border-top-width: 0px;
	width:40%;
}

.style11 {
	font-family: Arial, sans-serif;
	font-weight: bold;
}
.style12 {
	font-family: Arial, sans-serif;
}

.style13 {
	font-size: medium;
}

.style14 {
	text-align: left;
}

</style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
</head>
      
        <table width=100% ><tr><td align=left>
            <label ID="Label1" ><font size=4>My Medication</font></label>  </td>
            <td> </td></tr></table>
            
            
   
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />
 
  <div class="noPrint" >   
        
       <aspAjax:TabContainer runat="server" ID="tabsModManager" BorderStyle="None" BackColor="AliceBlue"
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Font-Bold="True"
            Font-Size="Medium">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Font-Size="Medium" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Font-Size="Small" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="lblTabLoadStatus" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" runat="server" Visible="false">
                            </asp:DropDownList></div>
                            <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details for Dependent" />
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer>
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
               <br /><br /> 
            <table >
           
                <tr>
                    <td>
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server" Text="3456998"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server" Text="Sunil Kumar Jose"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblCustomer_Name" runat="server" Text="Customer Name"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblCustomerName" runat="server" Text="30/05/1970"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblClass_Name" runat="server" Text="Class Name"></asp:Label></td>
                    <td>
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLProviderList" runat="server" Visible="true">
                            </asp:DropDownList></div></td>
                </tr>
          
            </table>
            
       <div id="CoverageListReport"  visible=false style=" font-family:Verdana; ; height:auto; width:100%; overflow:hidden;" runat="server" >
       <img src="file:///C:\Downloads\imagemap-1291\AJAX_ImageMap\animated_loading.gif" />
       </div>
<asp:Button CssClass="submitButton" ID="btnUpdateAll" runat="server" Text="Submit" />

      <asp:Table ID="MedTable" runat="server" ></asp:Table>      
            
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
       
           

</asp:Content>