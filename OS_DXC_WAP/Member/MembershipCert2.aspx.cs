﻿using Bupa.OSWeb.Business;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Utility.Configuration;

public partial class MembershipCert2 : System.Web.UI.Page 
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;

    protected void Page_PreInit(object sender, EventArgs e)
    {
       
    } 

    protected void Page_Load(object sender, EventArgs e)
    {
        lblCaesarErrorMessage.Text = "";
        rdnPurpose.Items[0].Attributes.CssStyle.Add("margin-right", "5px");
        rdnPurpose.Items[1].Attributes.CssStyle.Add("margin-left", "15px");
        if (Session["MembershipNo"] != null)
        {
            strMembershipNo = Convert.ToString(Session["MembershipNo"]);
            if (!IsPostBack)

                fillProviderDropDownList(strMembershipNo);

            getMemberList(strMembershipNo);

            frm.Visible = true;

        }
        else
            Response.Redirect("../default.aspx");
        //if (!string.IsNullOrEmpty(Convert.ToString(Request["mem"])))
        //{
        //    strMembershipNo = Request["mem"];
        //}
        //else
        //{
        //    if (string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
        //        Response.Redirect("../default.aspx");
        //    strMembershipNo = Session["MembershipNo"].ToString();
        //}

       

    }

    private void fillProviderDropDownList(string strMembershipNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrResponse_DN response;
        request.membershipNo = strMembershipNo;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = WebPublication.GenerateTransactionID();

        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.ReqMyMedicationByMbr(request);

            ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrRequest_DN>();
            ////XmlReq.Request(request, "RequestMyMedicationByMbrRequest_DN");

            ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrResponse_DN>();
            ////XmlResp.Response(response, "RequestMyMedicationByMbrResponse_DN");


            if (response.errorID[0] != "0")
            {
                sb.Append(response.errorMessage).Append("\n");
                lblErrorMessage.Text = sb.ToString();
            }
            else
            {
                DisplayProviderListResult(response);
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text = ex.Message;
        }

    }

    private void DisplayProviderListResult(OS_DXC_WAP.CaesarWS.RequestMyMedicationByMbrResponse_DN response)
    {
        if (response.status == "0")
        {
            //ddlProvider.Items.Clear();

            cboProviders.DataSource = response.provider_Name;
            //cboProviders.TextField = "provider_Name";
            //cboProviders.ValueField = "provider_Code";
            cboProviders.DataBind();

            //foreach (string dtl in response.provider_Name)
            //{
            //    ddlProvider.Items.Add(dtl);
            //}
        }
    }

    // function used for Tab System when user requset for dependent ceritifcate


    // function used for Tab System to retrieve family list
    private void DisplayMemberListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        if (response.status == "0")
        {
            cbMemberList.Items.Clear();


            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

                //ListItem DepList = new ListItem();
                //cbMemberList.TextField = dtl.memberName + " -- " + dtl.relationship;
                //cbMemberList.ValueField = dtl.membershipNo.ToString();
                cbMemberList.Items.Add(dtl.memberName + " -- " + dtl.relationship, dtl.membershipNo.ToString());
                //cbMemberList.Items.Add(
                /*DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                cbMemberList.Items.Add(DepList);*/
            }
        }
    }


    // function used for Tab System when Tab1 [member tab] is clicked
    protected void btnTab1_Click(object sender, EventArgs args)
    {
        //////      if (TabFlag != "M")
        DisplayMemberCertResult(strMembershipNo);
        //.Text = "test completed";
    }

    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void getMemberList(string strMembershipNo)
    {
        {
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

            OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
            OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
            request.membershipNo = strMembershipNo;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();

            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.EnqMbrListInfo(request);
                if (response.errorID != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    lblErrorMessage.Text = sb.ToString();
                }
                else
                {
                    DisplayMemberListResult(response);
                    String XmlizedString = null;
                    MemoryStream memoryStream = new MemoryStream();
                    XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                    xmlTextWriter.Formatting = Formatting.Indented;
                    xs.Serialize(xmlTextWriter, response);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                    XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                    this.txtBox_output.Text = XmlizedString;


                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }




        }
    }
    private void DisplayMemberCertResult(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response;

        

        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN();
            request.membershipNo = long.Parse(MemberNo);

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();

            request.memberLoginIND = "Y";

            response = ws.ReqPrtMbrCrtInfo(request);
            
            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN>();
            //XmlReq.Request(request, "ReqPrtMbrCrtInfoRequest_DN");

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN>();
            //XmlResp.Response(response, "ReqPrtMbrCrtInfoResponse_DN");

            if (response.memberName != null)
            {
                DisplayMbrCertInfo(response);
                lblCaesarErrorMessage.Visible = false;

            }
            else
            {

                divCer.Visible = false;
                if (response.errorID[0] == "E3031")
                {
                    lblErrID.Text = "E3031";
                    lblCaesarErrorMessage.Text = response.errorMessage[0];
                }
                else
                {

                    lblCaesarErrorMessage.Text = response.errorMessage[0];
                }
            }


        }
        catch (Exception ex)
        {
            divCer.Visible = false;
            lblCaesarErrorMessage.Text = ex.Message; /// this message would be used by transaction
        }
    }


    private void DisplayMbrCertInfo(OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response)
    {


        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {

            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request1;
            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response1;
            
            OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN request2;
            OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsResponse_DN response2;

            OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN request3;
            OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN response3;

            request1 = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            request1.membershipNo = Convert.ToString(response.membershipNo.ToString());

            request1.Username = WebPublication.CaesarSvcUsername;
            request1.Password = WebPublication.CaesarSvcPassword;
            request1.transactionID = WebPublication.GenerateTransactionID();
            response1 = ws.EnqMbrDetInfo(request1);

            request2 = new OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN();
            request2.contractNo = response1.detail[0].custNo;
            request2.membershipNo = Convert.ToString(response.membershipNo.ToString());
            request2.Username = WebPublication.CaesarSvcUsername;
            request2.Password = WebPublication.CaesarSvcPassword;
            request2.transactionID = WebPublication.GenerateTransactionID();
            request2.providerCode = "";

            response2 = ws.ReqTOB(request2);

            

            response3 = null;
            request3 = new OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN();
            request3.membershipNo = Convert.ToString(response.membershipNo.ToString()); ;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request3.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request3.Username = WebPublication.CaesarSvcUsername;;
            request3.Password = WebPublication.CaesarSvcPassword;

            //response3 = ws.EnqProdCovInfo(request3);

            response3 = ws.EnqProdCovInfo(request3);

            ////Start Added for CR395 by hussam
            OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoRequest_DN requestId;
            OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoResponse_DN responseId;
            requestId = new OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoRequest_DN();
            requestId.membershipNo = Convert.ToString(response.membershipNo.ToString()); ;
            requestId.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            requestId.Username = WebPublication.CaesarSvcUsername; ;
            requestId.Password = WebPublication.CaesarSvcPassword;
            responseId = ws.ReqMbrCCHIInfo(requestId);
            ////End Added for CR395 by hussam


            if (response3.status == "0")
                lblamount.Text = response3.proratedOverallAnnualLimit.ToString("C").Replace("£", "");

            lblCustName.Text = response1.detail[0].customerName;
            lblCustNo.Text = response1.detail[0].custNo;
            lbllCover.Text = response1.detail[0].schemeName;
            lblMemberName.Text = response.memberName;
            lblmemName.Text = response.memberName;
            lblMembershipNo0.Text = response.membershipNo.ToString();
            lblMembershipNo.Text = response.membershipNo.ToString();
            lblClass.Text = response.className;
            //Added for CR395 by hussam
            lblNetwork.Text = response.networkType;
            lblIDNumber.Text = responseId.IDNumber;
            ////lblIssueDate.Text = DateTime.Now.ToShortDateString();
            ////lblEffectiveFrom.Text = String.Format("{0:d}", response.effFrom);
            lblEffectiveFrom.Text = String.Format("{0:dd-MMM-yyyy}", response.effFrom);
            lblCustomerName.Text = response.customerName;
            lblDateOfBirth.Text = String.Format("{0:d}", response.DOB);
            lblDateOfBirth0.Text = String.Format("{0:d}", response.DOB);
            //lblDeductible.Text = response.deductible;
			
			DateTime cchiEffectiveDate = new DateTime(2018, 7, 01);
            DateTime effectiveDate = Convert.ToDateTime(Session["ContractStartDate"]);

            if(effectiveDate >= cchiEffectiveDate)
            {
                lblDeductible.Text = "Hospital - " + response.dedHospital + "<br/>" + "PolyClinic - " + response.dedPolyclinic + "<br/>" + "MPN - " + response.dedMpn;
            }
            else
            {
                lblDeductible.Text = response.deductible;
            }
			
            ////lblEffectiveTo.Text = String.Format("{0:d}", response.effTo);
            ////lblEffectiveTo0.Text = String.Format("{0:d}", response.effTo);
            lblEffectiveTo.Text = String.Format("{0:dd-MMM-yyyy}", response.effTo);
            lblEffectiveTo0.Text = String.Format("{0:dd-MMM-yyyy}", response.effTo);
            divCer.Visible = true;

        }
        else
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);



    }


    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SabicContractNo");
            lblMessage.Visible = false;
            if (cbMemberList.SelectedItem != null)
            {
                Session["v"] = cbMemberList.SelectedItem.Value.ToString();
                DisplayMemberCertResult(cbMemberList.SelectedItem.Value.ToString());
                if (rdnPurpose.SelectedValue == "1")
                    Session["Opt"] = "1";
                else
                    Session["Opt"] = "2";

                MemberHelper mh = new MemberHelper(strMembershipNo, "");
                Member m = mh.GetMemberDetails();

                if (rdnPurpose.SelectedValue == "1")
                {
					//Start Commented Code By Hussam CR395
                    //lblToHow.Visible = true;
                    //lblToHowAr.Visible = true;
                    //lblTo.Visible = true;

                    //lblSubject.Text = "Certificate of Membership";
                    //lblSubAr.Text = "شهادة عضوية";
                    //lblHospital2.Visible = true;
                    //lblHospital1.Visible = true;
					//End Commented Code By Hussam
                    divCer.Visible = true;
                    visa.Visible = false;
                    lblTitle.Visible = true;

                    printimg.Visible = true;
                    //rdnSendEmail.Visible = true;
                    //rdnSendFax.Visible = true;
                    //txtSendEmail.Visible = true;
                    //txtSendFax.Visible = true;
                    //btnSend.Visible = true;

                    //to clear the sabic members guide in case of hospital because it should work incase of visa only
                    lblSabicMessage.Text = string.Empty;

                    if (cboProviders.SelectedIndex != -1)
                    {
                        //Start Commented Code By Hussam CR395
						//lblTo.Text = cboProviders.SelectedItem.Text;
                        //lblHospital1.Visible = false;
                        ////lblHospital2.Visible = false;

                        //lblToHow.Text = "Attention To:";
                        //lblToHowAr.Text = "الى السادة";
						//End Commented Code By Hussam
                    }
                    else
                    {
						//Start Commented Code By Hussam CR395
                        //lblToHow.Text = "To Whom It May Concern";
                        //lblToHowAr.Text = "الى من يهمه الأمر";
						//End Commented Code By Hussam
                    }
                }

                else if (rdnPurpose.SelectedValue == "2")
                {
					//Start Commented Code By Hussam CR395
                    //lblToHow.Visible = false;
                    //lblToHowAr.Visible = false;
                    //lblTo.Visible = false;
                    //lblSubject.Text = "Membership certificate for visa application";
                    //lblSubAr.Text = "شهادة عضوية لطلب التأشيرة";
                    //lblHospital2.Visible = false;
                    //lblHospital1.Visible = false;
					//End Commented Code By Hussam
                    divCer.Visible = false;
                    visa.Visible = true;
                    lblTitle.Visible = false;

                    printimg.Visible = true;
                    //rdnSendEmail.Visible = true;
                    //rdnSendFax.Visible = true;
                    //txtSendEmail.Visible = true;
                    //txtSendFax.Visible = true;
                    //btnSend.Visible = true;

                    //to view a message for SABIC members to guide them while viewing their certificate
                    //informing them "If name doesn't match your passport, please send your passport copy along with your membership to Sabic.certificates@bupa.com.sa"
                    // Fetch the member's details to check whether SABIC or not. and if it is sabic to hide cheque option

                    string strSabicCertificateEmailMessage = CoreConfiguration.Instance.GetConfigValue("SabicCertificateEmail");

                    if (strConfigSabicContractNo == m.ContractNumber)
                    {
                        lblSabicMessage.Text = strSabicCertificateEmailMessage.Replace("=memberid", "=Memberid=" + m.ContractNumber);
                    }
                }
            }
            else
            {
                lblMessage.Visible = true;
            }
        }
        catch (Exception ex)
        {
            divCer.Visible = false;
            lblCaesarErrorMessage.Text = ex.Message; /// this message would be used by transaction
        }
    }

    
}
//public class ClassXMLGeneration<T>
//{
//    public void Request(T classType, string req)
//    {
//        System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
//        System.IO.StreamWriter file = new StreamWriter("C:\\Lokesh\\" + req + ".xml");
//        xmlSrQ.Serialize(file, classType);
//        file.Close();


//    }
//    public void Response(T classType, string resp)
//    {
//        System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
//        System.IO.StreamWriter file = new StreamWriter("C:\\Lokesh\\" + resp + ".xml");
//        xmlSr.Serialize(file, classType);
//        file.Close();
//    }
//}