﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Member_Map" Codebehind="Map.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="highlightText">
        View your provider network list and their location
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server">
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type ="text/javascript">

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }



    function InitializeMap() {

       var Latitude = getParameterByName("latitude");
      
        var Longitude = getParameterByName("longitude");
       
        var latlng = new google.maps.LatLng(Latitude, Longitude);

        var city = getParameterByName("city");
        var Name = getParameterByName("pro");

     

        var myOptions = {
            zoom: 17,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        var marker = new google.maps.Marker
        (
            {
                position: new google.maps.LatLng(Latitude, Longitude),
                map: map,
                title: 'Click me'
            }
        );


            var infowindow = new google.maps.InfoWindow({
            content: '<div id="pic"><img src="../images/logo.gif"/><p><b>Location info:</b>' + Name + '</p><p><b>Country</b>: Saudi Arabia</p><p><b>City:</b> ' + city + '</p></div>'
            //''
        });
        google.maps.event.addListener(marker, 'click', function () {
            // Calling the open method of the infoWindow 
            infowindow.open(map, marker);
        });

    }
    window.onload = InitializeMap;

</script>
<h2><asp:Label ID="lblProvider" runat="server" Text=""></asp:Label> - Location</h2>
<table width="100%">
<tr>
    <td align="right"><a href="javascript: window.history.go(-1)">Go Back</a></td>
</tr>
    <tr>
        <td>
        <div id ="map"   style="width:700px;   height:350px">

</div>
        </td>
    </tr>
</table>

</asp:Content>
