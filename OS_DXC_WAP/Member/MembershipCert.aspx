﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="MembershipCert" Codebehind="MembershipCert.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Attention all BUPA Middle East N</title>
<link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src="../javascript/SessionMsg.js"></script>
<link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
<script type="text/javascript" src="../functions/jquery.fancybox.js"></script>

<style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

.style1 {
	text-align: center;
}
.style2 {
	font-weight: bold;
	text-align: right;
}
.style3 {
	text-align: right;
}

.style5 {
	border-right: 1px solid #808080;
	border-bottom: 1px solid #808080;
	border-style: none none solid none;
	border-width: 0px 0px 1px 0px;
	text-align: center;
}
.style6 {
	border-right: 1px none #808080;
	border-bottom: 1px solid #808080;
	border-width: 0px 0px 1px 0px;
	text-align: right;
	border-left-style: none;
	border-top-style: none;
	border-bottom-color: #808080;
}
.style8 {
	font-family: Arial, sans-serif;
	font-size: 9pt;
}

.style9 {
	border-right: 0px none #808080;
	border-bottom: 1px solid #808080;
	text-align: left;
	font-size: 9pt;
	font-family: Arial, sans-serif;
	border-left-style: none;
	border-left-width: 0px;
	border-top-style: none;
	border-top-width: 0px;
}

.style10 {
	border-right: 0px none #808080;
	border-bottom: 1px solid #808080;
	text-align: center;
	font-family: Arial, sans-serif;
	font-size: 9pt;
	border-left-style: none;
	border-left-width: 0px;
	border-top-style: none;
	border-top-width: 0px;
	width:40%;
}

.style11 {
	font-family: Arial, sans-serif;
	font-weight: bold;
}
.style12 {
	font-family: Arial, sans-serif;
}

.style13 {
	font-size: medium;
}

.style14 {
	text-align: left;
}

</style>


 <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style> 
 <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
<link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">   

<link rel="stylesheet" href="styles.css" type="text/css" />



</head>
<body topmargin=0px>

<div class="adbanner" align="right">
<img src="Logo.gif" width="168" height="50">
<br>
    <span style="font-size: 8pt; font-family: Arial">
Exclusive Agents in KSA BUPA Middle East
Customer Service Toll Free No. 800 244 0307</span>
<hr>
</div>

<div class="noPrint" align=right >

    <br />
    
    <asp:Panel ID="Panel1" runat="server">
    
    <div id="tabs">
  <ul>

    <li>
    <a href="#" title="Sunil Kumar Jose">
    <span style="color:darkblue;background-position:100% -42px; cursor: default ;">Member</span></a>
    </li>
    <li><a href="123.html"><span>Dependent</span></a></li>

  </ul>
</div>

    </asp:Panel>   
    
    
    <div>
    <br />
          <a href="#" onclick="window.print()">Print<img src="#" style="height:27px"></a>
</div>

</div>



<form runat="server">
<asp:TextBox CssClass="textbox" id="txtTxnID" runat="server">88881051</asp:TextBox>
<asp:TextBox CssClass="textbox" id="txtMbrShp" runat="server">3208733</asp:TextBox>
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
</form>
<asp:Label id="Message1" runat="server" Text="Label" Width="527px" Height="15px"></asp:Label>

<asp:Panel ID="UpdatePanel2" runat="server">


<table style="width: 100%">
	<tr>
		<td style="width: 499px" class="style13"><b class="style11">Attention all BUPA Middle 
		East Network</b></td>
		<td class="style1" style="width: 127px">&nbsp;</td>
		<td class="style2" style="width: 40%">السادة / شبكة 
		مستشفيات بوبا الشرق الأوسط </td>
	</tr>
	<tr>
		<td style="width: 499px; height: 30px;"></td>
		<td class="style1" style="width: 127px; height: 30px;"></td>
		<td class="style3" style="height: 30px"></td>
	</tr>
	<tr>
		<td class="style11">TO WHOM IT MAY CONCERN</td>
		<td class="style1" style="width: 127px">&nbsp;</td>
		<td class="style3"><b>لمن يهمه الأمر</b></td>
	</tr>
	<tr>
		<td style="width: 499px">&nbsp;</td>
		<td class="style1" style="width: 127px">&nbsp;</td>
		<td class="style3">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 499px"><span class="style12">Subject: </span>
		<span class="style11">Certificate of 
		Membership</span></td>
		<td class="style1" style="width: 127px">&nbsp;</td>
		<td class="style3">الموضوع: شهادة عضوية</td>
	</tr>
	<tr>
		<td style="width: 499px">&nbsp;</td>
		<td class="style1" style="width: 127px">&nbsp;</td>
		<td class="style3">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 499px" class="style8">Certificate Issue Date</td>
		<td class="style14" style="width: 127px"><strong class="style10">
            <asp:Label ID="lblIssueDate" runat="server"></asp:Label>
		 </strong> </td>
		<td class="style3">تاريخ الاصدار </td>
	</tr>
	<tr>
		<td style="width: 499px" class="style8">This is to confirm that the 
		below mentioned member is medically covered under the BUPA Middle East 
		Health Care scheme with the following information:</td>
		<td class="style1" style="width: 127px">&nbsp;</td>
		<td class="style3">إن هذه الوثيقة تؤكد ان المذكور مغطى طبيا <br />
		تحت برنامج بوبا الشرق الاوسط للرعاية الصحية وفقا للبيانات التالية </td>
	</tr>
	</table>
<br>
<table style="width: 100%">
	<tr>
		<td align="left" class="style9" style="width: 30%">Name:</td>
		<td class="style10" ><strong>
            <asp:Label ID="lblMemberName" runat="server"></asp:Label></strong></td>
		<td align="right" class="style6" style="width: 30%">الاسم</td>
	</tr>
	<tr class="style5">
		<td align="left" class="style9" style="width: 320px">Class Name:</td>
		<td class="style10" ><strong>
		<asp:Label id="lblClass" runat="server"></asp:Label></strong></td>
		<td align="right" class="style6">درجة التغطية </td>
	</tr>
	<tr class="style5">
		<td align="left" class="style9" style="width: 320px">Membership No:</td>
		<td class="style10" ><strong>
		<asp:Label id="lblMembershipNo" runat="server"></asp:Label>
		</strong></td>
		<td align="right" class="style6">رقم العضوية </td>
	</tr>
	<tr class="style5">
		<td align="left" class="style9" style="width: 320px">Option*:</td>
		<td class="style10" ><strong>
		<asp:Label id="lblOptions" runat="server"></asp:Label>
</strong></td>
		<td align="right" class="style6">قائمة المزايا </td>
	</tr>
	<tr class="style5">
		<td align="left" class="style9" style="width: 320px">Room:</td>
		<td class="style10" ><strong>
		<asp:Label id="lblRoomType" runat="server"></asp:Label>

</strong></td>
		<td align="right" class="style6">درجة الإقامة </td>
	</tr>
	<tr class="style5">
		<td align="left" class="style9" style="width: 320px">Year of Birth:</td>
		<td class="style10" ><strong>
		<asp:Label id="lblDateOfBirth" runat="server"></asp:Label>
</strong></td>
		<td align="right" class="style6">تاريخ الميلاد </td>
	</tr>
	<tr class="style5">
		<td align="left" class="style9" style="width: 320px">Effective From:</td>
		<td class="style10" ><strong>
            <asp:Label ID="lblEffectiveFrom" runat="server"></asp:Label> </strong> </td>
		<td align="right" class="style6">صالحة من تاريخ&nbsp;
        </td>
	</tr>
	<tr class="style5">
		<td align="left" class="style9" style="width: 320px">Effective To:</td>
		<td class="style10" ><strong>
            <asp:Label ID="lblEffectiveTo" runat="server"></asp:Label> </strong> </td>
		<td align="right" class="style6">صالحة الى تاريخ </td>
	</tr>
	<tr class="style5">
		<td align="left" class="style9" style="width: 320px">Contribution:</td>
		<td class="style10" ><strong>
            <asp:Label ID="lblContribution" runat="server" Text="Label"></asp:Label></strong></td>
		<td class="style6">
            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">نسبة المشاركة</span>&nbsp;</td>
	</tr>
	<tr>
		<td align="left" class="style9" style="width: 320px">Staff No.</td>
		<td class="style10" >
            <asp:Label ID="lblStaffNo" runat="server"></asp:Label>&nbsp;</td>
		<td class="style6">
            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">الرقم الوظيفي </span>
            &nbsp;</td>
	</tr>
	<tr>
		<td align="left" class="style9" style="width: 320px">Customer Name:</td>
		<td class="style10" ><strong>
            <asp:Label ID="lblCustomerName" runat="server"></asp:Label></strong></td>
		<td class="style6">
            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">أسم العميل</span>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 320px">&nbsp;</td>
		<td class="style1" style="width: 239px">&nbsp;</td>
		<td class="style3">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 320px" class="style8">All Other terms and conditions 
		are as per policy</td>
		<td class="style1" style="width: 239px">&nbsp;</td>
		<td class="style3">جميع شروط الغطاء تخضع لبنود
		التعاقد</td>
	</tr>
	<tr>
		<td style="width: 320px">&nbsp;</td>
		<td class="style1" style="width: 239px">&nbsp;</td>
		<td class="style3">&nbsp;</td>
	</tr>
	</table>

<p></p>
<table style="width: 100%">
	<tr>
		<td style="width: 40%" class="style8" valign="top">This certificate is issued as a 
		temporary cover for this member. Should this member need any treatment 
		that is covered under this scheme, please provide it, and charge BUPA 
		Middle East.</td>
		<td >&nbsp;</td>
		<td align="right" class="th" colSpan="3" style="width: 40%" valign="top">إن هذه الشهادة اصدرت مؤقتاً 
		للعضو وفي حالة حاجته الى اية خدمات علاجية مدرجة تحت بنود الغطاء التأميني 
		الخاص به الرجاء تقديم الخدمة العلاجية و ارسال الفواتير الى بوبا الشرق 
		الاوسط </td>
	</tr>
	<tr>
		<td style="width: 437px" class="style8" valign="top">Should you have any queries, 
		please contact one of our advisers on the BUPA Middle East dedicated 
		customer service help lines 800 244 0307. This help 
		line is open 24hrs seven days a week.</td>
		<td style="width: 55px">&nbsp;</td>
		<td align="right" class="th" colSpan="3" valign="top"> لمزيد من المعلومات الرجاء 
		الاتصال بقسم خدمات العملاء هاتف او الهاتف المجاني 0307 244	800 على مدار الساعة طوال ايام الاسبوع </td>
	</tr>
	<tr>
		<td style="width: 437px" class="style8" valign="top">This certificate is valid only 
		for the named insured person when accompanied with ID documents. </td>
		<td style="width: 55px">&nbsp;</td>
		<td align="right" class="th" colSpan="3" valign="top">إن هذه الشهادة صالحة فقط للعضو 
		المذكور اعلاه شرط توفر وثيقة اثبات رسمية </td>
	</tr>
</table>
<br />
<br />
<table border="0" cellPadding="2" cellSpacing="2" width="100%">
	<tr>
		<td><b><i class="style8">This is only a definition for the codes shown under the option 
		field. Do not use for coverage checking.</i></b></td>
	</tr>
	<tr>
		<td class="style8">* A: Dialysis, C: Chronic, D: Dental, E: Neo-natal care, H: Other 
		Conditions, K: Cosmetic, L: Congenital, M: Maternity, N: Natural 
		Changes, O: Optical, P: Developmental, S: Standard Benefit, T: 
		Screening, V: Vaccinations, Y: Physiotherapy, Z: Hazardous Sports Injury
		</td>
	</tr>
</table>
</asp:Panel>
         

</body>
</html>
