using Bupa.OSWeb.Business;
using System;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Utility.Configuration;


public partial class Member_ClaimHistory : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
    string strMemberName;

    protected void Page_Load(object sender, EventArgs e)
    {
        strMembershipNo = Session["MembershipNo"].ToString();
        strMemberName = Session["MemberName"].ToString();
        DisplayClaimHist(strMembershipNo);
        tabsModManager.Visible = true;
    }

    // function used for Tab System when user requset for dependent ceritifcate
    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
        DisplayClaimHist(DDLDependentList.SelectedValue);
        tabsModManager.Visible = true;
    }

    // function used for Tab System to retrieve family list
    private void DisplayDependentListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  " +
                "style='font:Arial;border-collapse:collapse;font-size:11px;'><thead  bgcolor='#e6f5ff'>" +
                "<tr><th>Provider Name</th>   <th>Claim Voucher No</th><th>Billed Amount</th>   <th>Received Date</th>" +
                "<th>Date of Payment</th>   <th>Cheque No</th> <th>Cheque Date</th> <th>EFT No.</th> <th>Cheque Amount</th>" +
                "<th>Bank Name</th> <th>Payee Name</th> <th>EFT Date</th> <th>Claim Status</th> <th>Remarks</th> </tr>" +
                "</thead><tbody> ");
            DDLDependentList.Items.Clear();

            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                DDLDependentList.Items.Add(DepList);
            }
        }
    }

    // function used for Tab System when Tab1 [member tab] is clicked
    protected void btnTab1_Click(object sender, EventArgs args)
    {
        DisplayClaimHist(strMembershipNo);
    }

    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void btnTab2_Click(object sender, EventArgs args)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        request.membershipNo = strMembershipNo;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = WebPublication.GenerateTransactionID();
        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.EnqMbrListInfo(request);
            if (response.errorID != "0")
            {
                sb.Append(response.errorMessage).Append("\n");
                lblErrorMessage.Text = sb.ToString();
            }
            else
            {
                DisplayDependentListResult(response);
                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlTextWriter.Formatting = Formatting.Indented;
                xs.Serialize(xmlTextWriter, response);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                this.txtBox_output.Text = XmlizedString;
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text = ex.Message;
        }

        if (DDLDependentList.Items.Count > 0)
        {
            DDLDependentList.Visible = true;
            this.lblTabLoadStatus.Visible = false;
            btnRequestDependentCert.Visible = true;
        }
        else
            this.lblTabLoadStatus.Text = "No dependent for this Member Found";
    }

    private void DisplayClaimHist(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.EnqClmHisInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqClmHisInfoResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqClmHisInfoRequest_DN();
            request.MembershipNo = MemberNo;
            request.TransactionID = WebPublication.GenerateTransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqClmHisInfo(request);
            if (response.detail != null)
            {
                lblMembershipNo.Text = request.MembershipNo;
                lblMemberName.Visible = false;
                lblMember_Name.Visible = false;
                DisplayClaimHistoryResult(response);
                if (tabMemberName.HeaderText == "Member")
                    tabMemberName.HeaderText = strMemberName;
                Message1.Visible = false;
            }
            else
                UpdatePanel2.Visible = false;
            tabsModManager.Visible = false;
            Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
        }
        catch (Exception ex)
        {
            tabsModManager.Visible = false;
            Message1.Text = ex.Message;
        }
    }

    public static int CalculateMonthDifference(DateTime startDate, DateTime endDate)
    {
        int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
        return Math.Abs(monthsApart);
    }

    private void DisplayClaimHistoryResult(OS_DXC_WAP.CaesarWS.EnqClmHisInfoResponse_DN response)
    {
        StringBuilder sbResponse = new StringBuilder(2200);
        if (response.Status == "0")
        {
            sbResponse.Append("<table><tr><td><font size=2>Please click on the <b>Claim Voucher Number</b> to view the Reimbursement Statement:</font></td><td align='right'><font size=3>الرجاء الضغط على رقم المطالبة لمشاهدة كشف حساب المطالبات</font></td><tr><table>");

            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Arial;font-color:black;" +
                                "border-collapse:collapse;font-size:11px;  ;'><thead  bgcolor='#e6f5ff'><tr>" +
                                "<th>Provider Name</th>" +
                                "<th>Voucher Number</th>" +
                                "<th>Received Date</th>" +
                                "<th>Claim Status</th>" +
                                "<th>Billed Amount</th> " +
                                "<th>Shortfall Amount</th>" +
                                "<th>Approved Amount</th>" +
                                "<th>Payment Method</th>" +
                                "<th>Payment Ref</th>" +
                                "<th>Payment Date</th>" +
                                "<th>Reason</th>" +
                                "<th>Remarks</th>" +
                                "</tr></thead><tbody> ");
            bool _isHyperLink = false;
            int _Months = 0;
            foreach (OS_DXC_WAP.CaesarWS.EnqClmHisDetail_DN dtl in response.detail)
            {
                if (dtl.ProviderName.ToString() != "Out of Network")
                {
                    break;
                }

                if (Convert.ToDateTime(dtl.ReceivedDate) <= Convert.ToDateTime("12/03/2011"))
                {
                    _isHyperLink = false;
                }
                else
                {
                    _isHyperLink = true;
                    _Months = CalculateMonthDifference(DateTime.Today, Convert.ToDateTime(dtl.ReceivedDate));
                    if (_Months >= 6)
                    {
                        _isHyperLink = false;
                    }
                }
                sbResponse.Append("<tr><td >" + dtl.ProviderName + "</td>");
                if (_isHyperLink == true)
                {
                    if (dtl.SubmitBy == "M")
                    {
                        if (dtl.ClaimsStatus != "Pending")
                        {
                            sbResponse.Append("<td><font color='black' ><a  style='color: Black;' href='../client/claimsreport.aspx?VID=" +
                                dtl.ClaimVoucherNo.ToString().Trim() + "&EN=" + "" + "'  style='cursor:hand;'>" + dtl.ClaimVoucherNo +
                                "</a></font></td>");
                        }
                        else
                        {
                            sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
                        }
                    }
                    else
                    {
                        sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
                    }
                }
                else
                {
                    if (dtl.ProviderName.ToString() == "Out of Network")
                        sbResponse.Append("<td><font color='black' ><a  style='color: Black;' href='javascript:_Alert();' style='cursor:hand;'>" +
                            dtl.ClaimVoucherNo + "</a></font></td>");
                    else
                        sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
                }
                sbResponse.Append("<td>" + String.Format("{0:d}", dtl.ReceivedDate) + "</td>");
                sbResponse.Append("<td>" + dtl.ClaimsStatus + "</td>");

                if (dtl.PresentedAmount.ToString().Contains(".") == false)
                    sbResponse.Append("<td>" + dtl.PresentedAmount + ".00</td>");
                else
                    sbResponse.Append("<td>" + dtl.PresentedAmount + "</td>");

                sbResponse.Append("<td>" + dtl.ShortfallAmt + "</td><td>" + dtl.ApprovalAmt + "</td>");





                //sbResponse.Append("<td>" + String.Format("{0:d}", dtl.PayDate) + "</td>");



                if (dtl.ChequeNo != "")
                {
                    //the cheque details "Cheque"
                    sbResponse.Append("<td>Cheque</td><td>" + dtl.ChequeNo + "</td><td>" + String.Format("{0:d}", dtl.ChequeDate) + "</td>");

                    //sbResponse.Append("<td>");
                    //if (dtl.ChequeAmount.ToString() != "-1")
                    //{
                    //    if (dtl.ChequeAmount.ToString().Contains(".") == false)
                    //        sbResponse.Append(dtl.ChequeAmount + ".00");
                    //    else
                    //        sbResponse.Append(dtl.ChequeAmount + "");
                    //}
                    //sbResponse.Append("</td>");
                }
                else
                {
                    //EFT details
                    sbResponse.Append("<td>EFT</td><td>" + dtl.EFTNO + "</td><td>" + String.Format("{0:d}", dtl.EFTDate) + "</td>");
                }


                sbResponse.Append("<td>" + dtl.RejReasonSF + "</td>");
                //sbResponse.Append("<td>" + dtl. + "</td>");
                sbResponse.Append("<td>" + dtl.Remarks + "</td></tr>");
                //sbResponse.Append("<td>" + dtl.PayeeName + "</td><td>" + dtl.ClaimsStatus + "</td><td>" + dtl.Remarks + "</td>");

                //if (dtl.detail_ProratedLimit != -1)
                //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
                //else
                //    sbResponse.Append("<td>Not Covered</td></tr>");


            }

            //sbResponse.Append("</table><table><tr><td>");
			sbResponse.Append("</table>");

            //sbResponse.Append("<font size=2>Dear Customer Please note that reimbursement claims received before <b><span  style='color:red'>March  12th, 2011 </span></b> will not have online reimbursement statement. Kindly contact our customer service advisers on 8002440307 if you need a copy of the statement.</td>");
            //sbResponse.Append("<td align='right'><font size=2>عزيزي العميل نود أن نحيطكم علما بأنه بالامكان الحصول على النسخة الكترونية من كشف حساب مطالبات الافراد النقدية المستلمة بعد تاريخ <b><span  style='color:red'>  12/3/2011 </span></b>  م وذلك عن طريق الموقع الاكتروني لبوبا العربية. في حال رغبتكم الحصول على كشوفات حساب لمطالبات قديمة قبل هذا التاريخ الرجاء الاتصال على الهاتف المجاني لخدمة العملاء لبوبا العربية 8002440307</font></td></tr></table>");

            // Fetch the member's details to check whether SABIC or not. and if it is sabic to hide cheque option
            string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SabicContractNo");
            string strSabicTollFreeNo = CoreConfiguration.Instance.GetConfigValue("SabicTollFree");
            MemberHelper mh = new MemberHelper(strMembershipNo, "");
            Member m = mh.GetMemberDetails();
            if (strConfigSabicContractNo == m.ContractNumber)
            {
                sbResponse.Replace("8002440307", strSabicTollFreeNo);
            }


            CoverageListReport.Visible = true;
            CoverageListReport.InnerHtml = sbResponse.ToString();



        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }







    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }





}