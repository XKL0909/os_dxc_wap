﻿var interval1;
function SessionExpired(milliseconds) {
    
    var x = setTimeout(function () {
        $(".fancybox5").fancybox();
        $('.fancybox5').trigger('click');
    }, milliseconds);
   
}
function SessionExpiredEnded(milliseconds, url) {

    var y = setTimeout(function () {
        window.location = url;
    }, milliseconds);
}
function closeFancyBox() {
    parent.$.fancybox.close();
}
function SessionTimeoutAlert(timeout) {
    
        var seconds = timeout / 1000;
        document.getElementById("secondsIdle").innerHTML = seconds;
        document.getElementById("Arseconds").innerHTML = seconds;
        document.getElementById("seconds").innerHTML = seconds;

        setInterval(function () {
            seconds--;
            document.getElementById("seconds").innerHTML = seconds;
            document.getElementById("Arseconds").innerHTML = seconds;
            document.getElementById("secondsIdle").innerHTML = seconds;

        }, 1000);

        setTimeout(function () {

            //Show Popup before 20 seconds of timeout.

            $find("mpeSessionTimeout").show();

        }, timeout - 120 * 1000);

        setTimeout(function () {
            //window.location = "/";
            window.location = 'https://onlineservices.bupa.com.sa/CommClear.aspx';
        }, timeout);
   
};

function ResetSession() {

    //Redirect to refresh Session.

    window.location = window.location.href;

}
function RedirectPage() {
    //Redirect to refresh Session.
    //window.location = "/";
    if (document.getElementById("seconds").innerHTML=="0")
        window.location = 'https://onlineservices.bupa.com.sa/CommClear.aspx';
}
