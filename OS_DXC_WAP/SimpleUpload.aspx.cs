using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bupa.OSWeb.Helper;

using log4net;
using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;

public partial class SimpleUpload : System.Web.UI.Page
{
    private ILog _log = LogManager.GetLogger(typeof(SimpleUpload));
    private string[] supportDocs;
    private string allowedExtent, unparsedFileSizeLimit, unparsedFileSetSizeLimit, unparsedIsBatch;
    protected void Page_Load(object sender, EventArgs e)
    {
        Tracer.WriteMemberEntry(ref _log);
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginInfo"])))
			{
				Response.Redirect("default.aspx", true);
			}
			else
			{
				if (Request["UploadCategory"] == "Submit Reimbursement" || Request["UploadCategory"] == "Submit eClaims")
				{
					btnSend.Visible = true;
					lbtnClose.Visible = false;
					allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensionseClaims);
					unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSizeeClaims);
					unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSizeeClaims);
					unparsedIsBatch = "true";
				}
				else
				{
					btnSend.Visible = false;
					lbtnClose.Visible = true;
					allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
					unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
					unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSize);
					unparsedIsBatch = Convert.ToString(Request.QueryString[UploadPublication.QSKey_BatchUpload]);
				}
				
				DataBind();
				if (unparsedIsBatch.ToLower() == "true")
				{
					// Set the limit to be different for batch uploads
					if (Request["UploadCategory"] == "Submit Reimbursement" || Request["UploadCategory"] == "Submit eClaims")
					{
						unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_BatchMaxFileSetByteSizeeClaims);
					}
					else
					{
						unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_BatchMaxFileSetByteSize);
					}
					
				}
				Tracer.WriteLine(ref _log, "unparsedFileSetSizeLimit is: " + unparsedFileSetSizeLimit);
				long fileByteLimit = long.Parse(unparsedFileSizeLimit);
				long fileSetByteLimit = long.Parse(unparsedFileSetSizeLimit);

				
				lblUploadExtension.Text = "You may upload related documents here (allowed types are: " + allowedExtent + "). Total complete size allowed for all files uploaded is " + unparsedFileSetSizeLimit + " .";

				uploader.AllowedExtensions = allowedExtent;
				uploader.MaxFileSize = fileByteLimit;
				uploader.MaxFileSetSize = fileSetByteLimit;
			}

        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
    }
    protected void lbtnClose_Click(object sender, EventArgs e)
    {
        // Get the list of attachments for this user
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(Request["RequestID"], Request["UploadCategory"]);
        DataSet ds = (DataSet)Session["DataSet"];
        
        int _count = 0;
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            supportDocs = new string[d.Tables[0].Rows.Count];
            foreach (DataRow r in d.Tables[0].Rows)
            {
                
                string attachPath = r["VirtualPath"].ToString();
                supportDocs[_count] = attachPath;
                _count++;
            }
            int _ctn = 1;
            if (Request["isresub"] == "yes")
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    if (dr["SEQ"].ToString() == Convert.ToString(Convert.ToInt32(Request["SEQ"])))
                    {
                        dr["SupportDocs"] = "";
                        for (int i = 0; i < supportDocs.Length; i++)
                        {
                            if (i == 0)
                            {
                                dr["SupportDocs"] = supportDocs[i].Replace("~/Uploads/", @"\");
                            }
                            else
                            {
                                dr["SupportDocs"] = dr["SupportDocs"] + ";" + supportDocs[i].Replace("~/Uploads/", @"\");
                            }

                        }
                    }
                }
            }
            else
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr["SEQ"].ToString() == Convert.ToString(Convert.ToInt32(Request["SEQ"]) + 1))
                    {
                        dr["SupportDocs"] = "";
                        for (int i = 0; i < supportDocs.Length; i++)
                        {
                            if (i == 0)
                            {
                                dr["SupportDocs"] = supportDocs[i].Replace("~/Uploads/", @"\");
                            }
                            else
                            {
                                dr["SupportDocs"] = dr["SupportDocs"] + ";" + supportDocs[i].Replace("~/Uploads/", @"\");
                            }

                        }
                    }
                }
            }
           
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.opener.Refresh(); window.close()", true);
        }
        else
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["SEQ"].ToString() == Convert.ToString(Convert.ToInt32(Request["SEQ"]) + 1))
                {
                   
                        dr["SupportDocs"] = "";          
                }
            }
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.opener.Refresh(); window.close()", true);
            return;
        }
        

        
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        // Send an email informing the team about a pre-auth request
        string fromEmail = "donotreply@bupa.com.sa";
        string name = "";
        string code = "";
        if (!string.IsNullOrEmpty(Request["t"]))
        {
            if (Request["t"] == "mem")
            {
                name = Convert.ToString(Session["MemberName"]);
                code = Convert.ToString(Session["MembershipNo"]);
            }
            else
            {
                name = Convert.ToString(Session["ProviderName"]);
                code = Convert.ToString(Session["ProviderID"]);

            }
        }
        else 
        {
            name = Convert.ToString(Session["ClientName"]);
            code = Convert.ToString(Session["ClientID"]);
        }
        string subject = " Attachments added by " + name + " (" + code + ")";
        string content = "Please view the attached uploaded documents added by " + name + " (" + code + ") ";

        // Get the list of attachments for this user
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(Request["RequestID"], Request["UploadCategory"]);

        StringCollection attachments = new StringCollection();
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow r in d.Tables[0].Rows)
            {
                string attachPath = r["VirtualPath"].ToString();
                attachments.Add(attachPath);
            }
        }
        else
        {
            // No attachments and therefore, nothing to email
            return;
        }

        // Get the addressees
        if (Request["t"] != "pro")
        {
            SendEmail(fromEmail, "reimbursement@bupa.com.sa", subject, content, attachments);
        }
        else
        {
            SendEmaileClaims("CcuEclaims@bupa.com.sa", "ccu@bupa.com.sa", subject, content, attachments);
        }
        
    }


    private void SendEmaileClaims(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        // Setup the mail message

        using (MailMessage mail = new MailMessage(new MailAddress(fromEmail, "Bupa Arabia CCU Online Services"), new MailAddress(toEmail, "")))
        {
            mail.Subject = subject;
            mail.Body = content;
            
            if (attachmentPaths != null)
            {

                foreach (string path in attachmentPaths)
                {
                    // Create the attachment
                    System.Net.Mail.Attachment attachment;
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        attachment = new System.Net.Mail.Attachment(Server.MapPath(path));
                        // Add the attachment
                        mail.Attachments.Add(attachment);
                    }
                }
            }
            string smtpHost = WebPublication.EmailRelayAddress();
            int smtpPort = WebPublication.EmailRelayPort();

            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
            mail.IsBodyHtml = true;
            smtp.Send(mail);
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }


    }
    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        // Setup the mail message
        if (Request["t"] == "pro")
        {
            fromEmail = "";
        }
        MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

        // Deal with the attachments
        if (attachmentPaths != null)
        {
            foreach (string path in attachmentPaths)
            {
                // Create the attachment
                System.Net.Mail.Attachment attachment;
                if (System.IO.File.Exists(Server.MapPath(path)))
                {
                    attachment = new System.Net.Mail.Attachment(Server.MapPath(path));
                    // Add the attachment
                    mail.Attachments.Add(attachment);
                }
            }
        }

        string smtpHost = WebPublication.EmailRelayAddress();
        int smtpPort = WebPublication.EmailRelayPort();

        SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
        mail.IsBodyHtml = true;
        smtp.Send(mail);
        this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
    }

}