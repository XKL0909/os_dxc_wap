﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/InnernoTitle.master" AutoEventWireup="true" Inherits="Reg_Registration" Codebehind="Registration.aspx.cs" %>

<%@ Register Src="../UserControls/SignUp.ascx" TagName="SignUp" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>Registration Form</h2>
    <style type="text/css">
        .FloatingDiv
        {
            position: fixed;
            top: 0;
            left: 0;
            text-align: center;
            padding: 200px 0 0 0;
            z-index: 999;
            width: 100%;
            height: 100%;
            background: url('../Images/Floatingimg.png') repeat top left;
        }

        .Floatimage
        {
            display: block;
            width: 100%;
            height: 100%;
            vertical-align: middle;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <uc1:SignUp ID="SignUp1" runat="server" />
    
</asp:Content>

