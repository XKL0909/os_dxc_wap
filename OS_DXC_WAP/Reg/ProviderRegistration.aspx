﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/InnernoTitle.master" AutoEventWireup="true" Inherits="Reg_ProviderRegistration" Codebehind="ProviderRegistration.aspx.cs" %>

<%@ Register Src="../UserControls/SignUpNew.ascx" TagName="SignUpNew" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>New Provider Application Form</h2>
    <style type="text/css">
        .FloatingDiv {
            position: fixed;
            top: 0;
            left: 0;
            text-align: center;
            padding: 200px 0 0 0;
            z-index: 999;
            width: 100%;
            height: 100%;
            background: url('../Images/Floatingimg.png') repeat top left;
        }

        .Floatimage {
            display: block;
            width: 100%;
            height: 100%;
            vertical-align: middle;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="Server">
    <script src="../Scripts/CommonScript.js"></script>
    <script src="../Scripts/jquery-1.10.2.1.js"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" />
    <script src="../Scripts/jquery-ui.js"></script>
    <ajaxToolkit:ToolkitScriptManager runat="server" ID="ScriptManager1">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc2:SignUpNew ID="SignUp1" runat="server" />          
            <asp:UpdateProgress ID="uprgWorkRequest"
                AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div class="FloatingDiv">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/ajax-loader.gif"
                            Style="height: 279px" meta:resourcekey="Image1Resource1" /><br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </ContentTemplate>
    </asp:UpdatePanel>
   <%-- <script type="text/javascript" language="javascript">

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

        function EndRequestHandler(sender, args) {

            if (args.get_error() != undefined) {

                args.set_errorHandled(true);

            }

        }

    </script>--%>
</asp:Content>

