﻿using Bupa.Core.Utilities.Configuration;
using OnlinseServicesDashBoard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI.HtmlChart;
using Telerik.Web.UI;

public partial class Client_ClientDashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        lbl1.Text = Session["ClientName"].ToString() + " Dashboard";
        SetDateTimeFilters();//default is -7

        DateTimeBaseUnit baseUnit = (DateTimeBaseUnit)Enum.Parse(typeof(DateTimeBaseUnit), "Days", true);
        PreAuthLineChart.PlotArea.XAxis.BaseUnit = baseUnit;

        // if (!Page.IsPostBack)
        {
            //ReimbursementClaimChart.Legend.Appearance.BackgroundColor = Color.Aqua;
            DataSet dsPreAuth = GetPreAuthData(DateTime.Now.AddDays(-7), DateTime.Now);
            DataSet dsReimbursement = GetReimbursementClaimsData(DateTime.Now.AddDays(-7), DateTime.Now);

            //charts
            DataSet dsPieChartPreAut = GetPreAuthGroupedData(dsPreAuth);
            if (dsPieChartPreAut != null && dsPieChartPreAut.Tables != null && dsPieChartPreAut.Tables.Count > 0 && dsPieChartPreAut.Tables[0].Rows.Count > 0)
            {
                LabelNoPreAuthData.Visible = false;
                PreAuthLineChart.Visible = true;

                PreAuthLineChart.DataSource = dsPieChartPreAut;
                PreAuthLineChart.DataBind();
            }
            else
            {
                PreAuthLineChart.Visible = false;
                LabelNoPreAuthData.Visible = true;
            }

            var dsReimbursementChart = GetReimbursementClaimsGroupedData(dsReimbursement);
            if (dsReimbursementChart != null && dsReimbursementChart.Count > 0)
            {
                ReimbursementClaimChart.Visible = true;
                LabelNoReimbursementClaimsData.Visible = false;
                ReimbursementClaimChart.DataSource = dsReimbursementChart;
                ReimbursementClaimChart.DataBind();
            }
            else
            {
                ReimbursementClaimChart.Visible = false;
                LabelNoReimbursementClaimsData.Visible = true;
            }

            //flags
            DataSet dsFlagsAmount = GetAmountFlags(dsPreAuth);
            if (dsFlagsAmount != null && dsFlagsAmount.Tables != null && dsFlagsAmount.Tables.Count > 0 && dsFlagsAmount.Tables[0].Rows.Count > 0)
            {
                radGridFlagAmount.Visible = true;
                LabelFlagAmount.Visible = false;
                radGridFlagAmount.DataSource = dsFlagsAmount;
                radGridFlagAmount.DataBind();
            }
            else
            {
                radGridFlagAmount.Visible = false;
                LabelFlagAmount.Visible = true;

            }


            DataSet dsFlagsFrequency = GetFrequencyFlags(dsPreAuth);
            if (dsFlagsFrequency != null && dsFlagsFrequency.Tables != null && dsFlagsFrequency.Tables.Count > 0 && dsFlagsFrequency.Tables[0].Rows.Count > 0)
            {
                radGridFlagFrequency.Visible = true;
                LabelFlagFrequency.Visible = false;

                radGridFlagFrequency.DataSource = GetFrequencyFlags(dsPreAuth);
                radGridFlagFrequency.DataBind();
            }
            else
            {
                radGridFlagFrequency.Visible = false;
                LabelFlagFrequency.Visible = true;

            }
        }
        // ReimbursementClaimChart.PlotArea.Series[0].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.White;
        /*ReimbursementClaimChart.PlotArea.Series[1].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Yellow;
        ReimbursementClaimChart.PlotArea.Series[2].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Red;
        PieSeries pieSeries = (PieSeries)ReimbursementClaimChart.PlotArea.Series[0];
        //pieSeries.Appearance.FillStyle.BackgroundColor = Color.Red;
        pieSeries.SeriesItems[0].BackgroundColor = Color.Green;
        pieSeries.SeriesItems[1].BackgroundColor = Color.Yellow;
        pieSeries.SeriesItems[2].BackgroundColor = Color.Red;*/

    }

    private DataSet GetFrequencyFlags(DataSet dsPreAuth)
    {

        DataSet frequencyFlags = new DataSet();

        var nList = dsPreAuth.Tables[0].AsEnumerable().
            GroupBy(i => new { VisitMember = i.Field<string>("MBR_NAME"), VisitDate = i.Field<string>("RECEIVED_DATE") }).
            //Where(g => g.Count() >= 2).Select(g => g.Key);
            Select(group => Tuple.Create(group.Key, group, group.Count())).Where(x => x.Item3 >= 2).OrderByDescending(x => x.Item3);

        //frequencyFlags = nList;
        //Dataset for chart
        DataTable dt = new DataTable("PreAuthFrequencyFlagDataTable");
        //dt.Columns.Add("Preauth Id", Type.GetType("System.Int32"));
        dt.Columns.Add("Recieved Date", Type.GetType("System.DateTime"));
        dt.Columns.Add("Member Name", Type.GetType("System.String"));
        dt.Columns.Add("Frequency", Type.GetType("System.Int32"));

        foreach (var dtRow in /*dsSource.Tables[0].Rows*/ nList)
        {
            //count = count + 1;
            dt.Rows.Add(DateTime.Parse(dtRow.Item1.VisitDate).ToShortDateString(), dtRow.Item1.VisitMember, dtRow.Item3);
        }

        frequencyFlags.Tables.Add(dt);
        return frequencyFlags;
    }

    private DataSet GetAmountFlags(DataSet dsPreAuth)
    {

        DataSet amountFlags = new DataSet();
        //5000+ per day per member
        //dsPreAuth.Tables[0].AsEnumerable().Where
        //var nList = dsPreAuth.Tables[0].AsEnumerable().
        //    GroupBy(i => new { VisitMember = i.Field<string>("MBR_NAME"), VisitDate = i.Field<string>("RECEIVED_DATE") }).
        //    //Where(g => g.Count() >= 2).Select(g => g.Key);
        //    Select(group => Tuple.Create(group.Key, group.FirstOrDefault()["PREAUTH_ID"].ToString(), group.Sum(x => Convert.ToDecimal(x["Line_Amount"])))).Where(x => x.Item3 >= 5000).OrderByDescending(x => x.Item3);


        var nList = dsPreAuth.Tables[0].AsEnumerable().
            //GroupBy(i => new { VisitMember = i.Field<string>("MBR_NAME"), VisitDate = i.Field<string>("RECEIVED_DATE") }).
            //Where(g => g.Count() >= 2).Select(g => g.Key);
            Select(group => Tuple.Create(group["PREAUTH_ID"], group["RECEIVED_DATE"], group["MBR_NAME"].ToString(), Convert.ToDecimal(group["Line_Amount"]))).Where(x => x.Item4 >= 5000).OrderByDescending(x => x.Item4);

        //frequencyFlags = nList;
        //Dataset for chart
        DataTable dt = new DataTable("PreAuthFrequencyFlagDataTable");
        dt.Columns.Add("Preauth Id", Type.GetType("System.Int32"));
        dt.Columns.Add("Recieved Date", Type.GetType("System.DateTime"));
        dt.Columns.Add("Member Name", Type.GetType("System.String"));
        dt.Columns.Add("Amount", Type.GetType("System.Decimal"));

        foreach (var dtRow in /*dsSource.Tables[0].Rows*/ nList)
        {
            //count = count + 1;
            dt.Rows.Add(dtRow.Item1, dtRow.Item2, dtRow.Item3, dtRow.Item4);
        }

        amountFlags.Tables.Add(dt);
        return amountFlags;
    }

    private void SetDateTimeFilters(int minLastDays = -7)
    {
        //set the date time picker for last one week
        RadDatePickerFrom.MinDate = DateTime.Now.AddDays(minLastDays);
        RadDatePickerFrom.MaxDate = DateTime.Now.Date;

        RadDatePickerTo.MinDate = DateTime.Now.AddDays(minLastDays);
        RadDatePickerTo.MaxDate = DateTime.Now.Date;
    }


    public class ReimbursementClaim
    {
        public ReimbursementClaim(string name, double pieShare, bool isExploded, string partColor)
        {
            Name = name;
            MarketShare = pieShare;
            IsExploded = isExploded;
            PartColor = partColor;
        }
        public string Name { get; set; }
        public string PartColor { get; set; }
        public double MarketShare { get; set; }
        public bool IsExploded { get; set; }
    }
    private DataSet GetReimbursementClaimsData(DateTime statDate, DateTime endDate)
    {
        var caesarConnectionString = CoreConfiguration.Instance.GetConfigValue("Caesar Connection String");
        var contractNumber = Convert.ToString(Session["ContractNumbers"]);//CoreConfiguration.Instance.GetConfigValue("FilterByContract");

        DataSet ds = DataHelper.GetReimbursementClaimsData(caesarConnectionString, statDate, endDate, contractNumber);

        //ds.Tables[0].AsEnumerable().

        return ds;
    }
    private List<ReimbursementClaim> GetReimbursementClaimsGroupedData(DataSet dsSource)
    {

        int TotalReversed = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Reverse")).Count();
        int TotalClaims = dsSource.Tables[0].Rows.Count;

        TotalClaims = TotalClaims - TotalReversed;

        double rejectedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Reject")).Count();
        double acceptedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Accept")).Count();
        double partAcceptedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("PartAccept")).Count();
        double suspendedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Suspense")).Count();


        List<ReimbursementClaim> claims = new List<ReimbursementClaim>();
        if (TotalClaims > 0)
        {
            claims.Add(new ReimbursementClaim("Rejected", Math.Ceiling(Math.Round(rejectedCount / TotalClaims * 100, 2)), true, "Red"));
            claims.Add(new ReimbursementClaim("Approved", Math.Ceiling(Math.Round(acceptedCount / TotalClaims * 100, 2)), false, "Green"));
            claims.Add(new ReimbursementClaim("Part Approved", Math.Floor(Math.Round(partAcceptedCount / TotalClaims * 100, 2)), false, "Yellow"));
            claims.Add(new ReimbursementClaim("Suspended", Math.Ceiling(Math.Round(suspendedCount / TotalClaims * 100, 2)), false, "Blue"));
        }
        return claims;
    }

    private DataSet GetPreAuthData(DateTime startDate, DateTime endDate)
    {
        var caesarConnectionString = CoreConfiguration.Instance.GetConfigValue("Caesar Connection String");
        var contractNumber = Convert.ToString(Session["ContractNumbers"]); //CoreConfiguration.Instance.GetConfigValue("FilterByContract");


        DataSet ds = DataHelper.GetPreAuthData(caesarConnectionString, startDate, endDate, contractNumber);
        //DataTable dt = ds.Tables[0];

        return ds;
    }
    private DataSet GetPreAuthGroupedData(DataSet dsSource)
    {
        //Dataset for chart
        DataSet ds = new DataSet("PreAuthsDataSet");
        DataTable dt = new DataTable("PreAuthsDataTable");
        dt.Columns.Add("PREAUTH_ID", Type.GetType("System.Int32"));
        dt.Columns.Add("RECEIVED_DATE", Type.GetType("System.DateTime"));
        //dt.Columns.Add("PREAUTH_STATUS", Type.GetType("System.String"));
        dt.Columns.Add("Line_Amount", Type.GetType("System.Decimal"));

        int count = 1;

        var query = from dr in dsSource.Tables[0].AsEnumerable()
                    group dr by dr["RECEIVED_DATE"] into newdr
                    select new
                    {
                        RECEIVED_DATE = newdr.First().Field<string>("RECEIVED_DATE"),
                        PREAUTH_COUNT = newdr.Count(),
                        EST_TOTAL_AMT = newdr.Sum(tempRec => Convert.ToDecimal(tempRec["Line_Amount"])),
                    };

        foreach (var dtRow in /*dsSource.Tables[0].Rows*/ query)
        {
            //count = count + 1;
            dt.Rows.Add(dtRow.PREAUTH_COUNT, dtRow.RECEIVED_DATE.ToString(), dtRow.EST_TOTAL_AMT.ToString());
        }

        ds.Tables.Add(dt);
        return ds;
    }

    private DataSet GetTempData()
    {



        DataSet ds = new DataSet("PreAuthsDataSet");
        DataTable dt = new DataTable("PreAuthsDataTable");
        dt.Columns.Add("PREAUTH_ID", Type.GetType("System.Int32"));
        dt.Columns.Add("RECEIVED_DATE", Type.GetType("System.DateTime"));
        //dt.Columns.Add("PREAUTH_STATUS", Type.GetType("System.String"));
        dt.Columns.Add("EST_AMT", Type.GetType("System.Decimal"));


        dt.Rows.Add(1, DateTime.Now.AddDays(-5), 10);
        dt.Rows.Add(2, DateTime.Now.AddDays(-4), 100);
        dt.Rows.Add(3, DateTime.Now.AddDays(-3), 1000);
        dt.Rows.Add(4, DateTime.Now.AddDays(-2), 3545);
        dt.Rows.Add(5, DateTime.Now.AddDays(-1), 5545);

        //dt.Rows.Add(1, DateTime.Now.AddDays(-5), 0, 140);
        //dt.Rows.Add(2, DateTime.Now.AddDays(-6), 0, 19877);
        //dt.Rows.Add(3, DateTime.Now.AddDays(-6), 0, 148);
        //dt.Rows.Add(4, DateTime.Now.AddDays(-7), 0, 350);

        ds.Tables.Add(dt);
        return ds;
    }

    protected void Refresh_Click(object sender, EventArgs e)
    {
        PreAuthLineChart.PlotArea.XAxis.DataLabelsField = "Year";
        PreAuthLineChart.PlotArea.Series[0].DataFieldY = "Rev";
        PreAuthLineChart.PlotArea.Series[0].Name = "Years";
        PreAuthLineChart.DataSourceID = "SqlDataSource1";
    }

    protected void FilterDateSetByDate_Click(object sender, EventArgs e)
    {
        PreAuthLineChart.DataSource = GetPreAuthGroupedData(GetPreAuthData(RadDatePickerFrom.SelectedDate.GetValueOrDefault(DateTime.Now.AddDays(-7)), RadDatePickerTo.SelectedDate.GetValueOrDefault(DateTime.Now)));
        PreAuthLineChart.DataBind();

        ReimbursementClaimChart.DataSource = GetReimbursementClaimsGroupedData(GetReimbursementClaimsData(RadDatePickerFrom.SelectedDate.GetValueOrDefault(DateTime.Now.AddDays(-7)), RadDatePickerTo.SelectedDate.GetValueOrDefault(DateTime.Now)));
        ReimbursementClaimChart.DataBind();
    }

    protected void radGridFlagFrequency_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
    {
        e.Column.FilterControlWidth = Unit.Pixel(85);
        e.Column.CurrentFilterFunction = GridKnownFunction.Contains;

        if (e.Column.UniqueName.ToLower() == "recieved date")
        {
            //e.Column.Visible = t;
            e.Column.HeaderText = "Recieved Date";
            e.Column.AutoPostBackOnFilter = true;

            GridBoundColumn boundColumn = e.Column as GridBoundColumn;
            boundColumn.ReadOnly = true;
            boundColumn.DataFormatString = "{0:d}";
        }
        if (e.Column.UniqueName.ToLower() == "member name")
        {
            e.Column.FilterControlWidth = Unit.Pixel(125);
        }
    }

    protected void radGridFlagAmount_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
    {
        e.Column.FilterControlWidth = Unit.Pixel(85);
        e.Column.CurrentFilterFunction = GridKnownFunction.Contains;

        if (e.Column.UniqueName.ToLower() == "recieved date")
        {
            //e.Column.Visible = t;
            e.Column.HeaderText = "Recieved Date";
            e.Column.AutoPostBackOnFilter = true;

            GridBoundColumn boundColumn = e.Column as GridBoundColumn;
            boundColumn.ReadOnly = true;
            boundColumn.DataFormatString = "{0:d}";

        }
        if (e.Column.UniqueName.ToLower() == "member name")
        {
            e.Column.FilterControlWidth = Unit.Pixel(125);
        }
    }

    protected void btnExcelAmount_Click(object sender, System.EventArgs e)
    {
        radGridFlagAmount.ExportSettings.ExportOnlyData = true;
        radGridFlagAmount.ExportSettings.IgnorePaging = true;
        radGridFlagAmount.ExportSettings.OpenInNewWindow = true;
        radGridFlagAmount.ExportSettings.FileName = string.Format("Amount-Flags Dated {0} to {1}", DateTime.Now.Date.AddDays(-7).ToShortDateString().Replace(':', '_').Replace('/', '_').Replace('\\', '_'), DateTime.Now.Date.ToShortDateString().Replace(':', '_').Replace('/', '_').Replace('\\', '_'));
        radGridFlagAmount.MasterTableView.ExportToExcel();


    }

    protected void btnExcelFrequency_Click(object sender, System.EventArgs e)
    {
        radGridFlagFrequency.ExportSettings.ExportOnlyData = true;
        radGridFlagFrequency.ExportSettings.IgnorePaging = true;
        radGridFlagFrequency.ExportSettings.OpenInNewWindow = true;
        radGridFlagFrequency.ExportSettings.FileName = string.Format("Frequecy-Flags Dated {0} to {1}", DateTime.Now.Date.AddDays(-7).ToShortDateString().Replace(':', '_').Replace('/', '_').Replace('\\', '_'), DateTime.Now.Date.ToShortDateString().Replace(':', '_').Replace('/', '_').Replace('\\', '_'));
        radGridFlagFrequency.MasterTableView.ExportToExcel();
    }




}