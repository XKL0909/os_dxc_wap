using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Utility.Configuration;
using System.Linq;
using System.Collections;
using System.Configuration;

public partial class ChangeClass : System.Web.UI.Page
{
    //CaesarHelper    
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    private string strClientID;
    private string strMemberGroup;
    private string strMemberType;
    private string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();
    // The upload category for the uploading of this page
    private string _uploadCategory = UploadCategory.MEMBER_CHGCLASS;

    protected void Page_Load(object sender, EventArgs e)
    {
        strClientID = Session["ClientID"].ToString();

        lblError.Visible = false;
        lblError.Text = "";

        if (!Page.IsPostBack)
        {
            Session["BindDependentsDataToGrid"] = null;
            /***** Fresh Page Load *****/
            if (Request.QueryString["OptionType"] == "Class")
            {
                btnChangeClass.Visible = true;
                lblAdd_Employee.Text = "Change Class";
                lnkBatchUpload.NavigateUrl = "~/Client/BatchUpload.aspx?OptionType=ChangeClass";

                // lblOptionNew.Text = "New Class";
                //  lblOptionOld.Text = "Old Class";

                //ddlChangeReason.Items.Clear();
                //ListItem DepList0 = new ListItem("Select from the list", "0");
                //ddlChangeReason.Items.Add(DepList0);
                //ListItem DepList1 = new ListItem("Member Promotions", "CLA001");
                //ddlChangeReason.Items.Add(DepList1);
                //ListItem DepList2 = new ListItem("Member Downgrade", "CLA002");
                //ddlChangeReason.Items.Add(DepList2);
                //ListItem DepList3 = new ListItem("Product Modification/Additional Class", "CLA003");
                //ddlChangeReason.Items.Add(DepList3);
                //ListItem DepList4 = new ListItem("Group/Customer Request", "CLA004");
                //ddlChangeReason.Items.Add(DepList4);
                //ListItem DepList5 = new ListItem("Master List Error", "CLA005");
                //ddlChangeReason.Items.Add(DepList5);
                //ListItem DepList6 = new ListItem("Others", "CLA006");
                //ddlChangeReason.Items.Add(DepList6);
            }

        }
        //BindDependentsDataToGrid();
        _uploadCategory = UploadCategory.MEMBER_CHGCLASS;
        //SetupUploader(uploader, _uploadCategory);
    }
    private void SetupUploader(Uploader uploaderCtrl, string uploadCategory)
    {
        uploaderCtrl.Visible = true;

        // By default, unless a user has uploaded supporting documents,
        // the Submit button will be disabled
        btnChangeClass.Enabled = false;

        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploaderCtrl.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        // Change Branch has optional upload
        if (!IsPostBack)
        {
            // Remove the upload option for Change Class as this is not applicable
            btnChangeClass.Enabled = true;
            tblUploader.Visible = false;
        }

        // Bind to allow resolving of # tags in mark-up
        //DataBind();
    }
    protected void Page_LoadComplete(object sender, System.EventArgs e)
    {
        // Enable / Disable the submission option depending on whether the user 
        // has uploaded supported documents

        // Assume the button is disabled until checked
        btnChangeClass.Enabled = false;
        //btnChangeBranch.Enabled = false;

        // Change Branch has optional upload
        if (_uploadCategory == UploadCategory.MEMBER_CHGBRANCH)
        {
            //btnChangeBranch.Enabled = true;
            tblUploader.Visible = true;
        }
        else
        {
            // Remove the upload option for Change Class as this is not applicable
            btnChangeClass.Enabled = true;
            tblUploader.Visible = true;
        }
if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {

            //DivStafflbl.Style.Add("display", "block");
            //DivStafftxt.Style.Add("display", "block");
            //lblAramcoMessage.Visible = true;
            Label1.Visible = false;
            // Get what has been uploaded
            //DataSet uploadedFileSet = uploader.UploadedFileSet;
        }
            UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);

        if (_uploadmanger.WebIndecator("SuportDocs", strClientID))
        {
            tblUploader.Visible = true;

            int uploadedCount = 0;
            //if (uploadedFileSet != null && uploadedFileSet.Tables.Count > 0)
            //{
            //    uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
            //    if (uploadedCount > 0)
            //    {
            //        btnChangeClass.Enabled = true;
            //        //btnChangeBranch.Enabled = true;
            //    }
            //    else
            //    {
            //        btnChangeClass.Enabled = false;
            //        //btnChangeBranch.Enabled = false;
            //    }
            //}
        }
        else
        {
            tblUploader.Visible = true;
            btnChangeClass.Enabled = true;
            //btnChangeBranch.Enabled = true;
        }

    }
    public string UpdateClass(string membershipNo, string memberName, string changeReason, string effectiveDate, string classOldValue,
        string classNewValue, string fileName)
    {
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;

        #region Caesar Invocation

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request;
        OS_DXC_WAP.CaesarWS.SubTxnResponse_DN response;

        request = new OS_DXC_WAP.CaesarWS.SubTxnRequest_DN();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        request.TransactionType = "CLASS CHANGE";
        //request.TransactionID = TransactionManager.TransactionID();
        request.TransactionID = WebPublication.GenerateTransactionID();

        request.BatchIndicator = "N";
        request.detail = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[1];

        request.detail[0] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
        request.detail[0].sql_type = "CSR.SUB_TXN_REC";
        request.detail[0].ContractNo = strClientID; // "10216200";
        //request.detail[0].
        request.detail[0].MbrName = memberName.Trim();
        request.detail[0].MembershipNo = membershipNo.Trim();
        request.detail[0].Reason = changeReason.Trim();// ddlChangeReason.SelectedValue;
        request.detail[0].EffectiveDate = DateTime.ParseExact(effectiveDate, "dd/MM/yyyy", null);
        request.detail[0].ExistingClass = classOldValue.Trim();//ddlBranchCodeOldValue.Text;
        request.detail[0].NewClass = classNewValue.Trim();//ddlBranchCodeNew.SelectedValue;
        request.detail[0].SupportDoc = new string[] { 
            DateTime.Now.Year + "\\" + DateTime.Now.Month + "\\" +DateTime.Now.Date + "\\" + fileName};
        //uplodCtrl.SupportDocuments(uploader.SessionID, _uploadCategory);
        if (Session["ClientUsername"] != null)
        {
            request.submitBy = Session["ClientUsername"].ToString();
        }
        try
        {
            response = ws.SubTxn(request);
            StringBuilder sb = new StringBuilder(200);

            if (response.Status != "0")
            {

                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");

                Message1.Text = sb.ToString();
                Panel1.Visible = false;
                Message1.Visible = true;

                caesarInvokeError = true;
            }
            else
            {
                //lnkChangeOption.Text = "Change class for another Employee";
                //lnkChangeOption.NavigateUrl = "changebranch.aspx?optiontype=Class";
                //lnkChangeOption.Visible = true;

                //Message1.Text = "Thank you for submitting your request. Your reference number is " + response.ReferenceNo.ToString() + ". We are now validating the submitted file.";
                //Panel1.Visible = false;
                //Message1.Visible = true;

                //caesarInvokeError = false;
                caesarReferenceID = response.ReferenceNo.ToString();
            }
        }
        catch (Exception ex)
        {
            //Panel1.Visible = false;

            //Message1.Text = ex.Message;
            //Message1.Text = "Error Encountered. Please contact the administrator for further information.";
            //Message1.Visible = true;
            throw ex;
        }

        #endregion

        if (!caesarInvokeError)
        {
            string username = "Unknown username";
            string fullName = "Unknown client";
            string[] addressees = UploadPublication.MembershipEmailAddresses();

            if (Session[WebPublication.Session_ClientUsername] != null)
                username = Session[WebPublication.Session_ClientUsername].ToString();
            if (Session[WebPublication.Session_ClientFullName] != null)
                fullName = Session[WebPublication.Session_ClientFullName].ToString();
            tblUploader.Visible = false;
        }
        return caesarReferenceID;
    }
    private bool CheckMemberTypeExist(int intNewclassID)
    {
        OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
        OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqContMbrTypeListResponse_DN response;
        request = new OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN();
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        long ls_txnid = WebPublication.GenerateTransactionID();
        request.transactionID = ls_txnid;
        request.contNo = strClientID;
        request.contCls = intNewclassID; ;
        request.grp_Type = strMemberGroup;

        response = ws.EnqContMbrTypeList(request);

        if (response.status == "0")
        {
            for (int i = 0; i < response.detail.Length; i++)
            {
                if (response.detail[i].mbrTypeDesc == strMemberType)
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            return false;
        }
    }
    /*
    private void BindDdlProfession()
    {
        if (!IsPostBack)
        {
            CaesarHelper ch = new CaesarHelper();
            ch.BindDdlProfession(ref ddProfession);
        }
    }
    private void BindDdlDistrict()
    {
        if (!IsPostBack)
        {
            CaesarHelper ch = new CaesarHelper();
            ch.BindDdlDistrict(ref ddDistrict);
        }
    }
    private void BindDdlENationality(bool IsGCC)
    {
        if (!IsPostBack)
        {
            CaesarHelper ch = new CaesarHelper();
            ch.BindNationalities(IsGCC, ref ddlENationality);
        }
    }
    protected void PopulateBranchList(string ClientID)
    {
        try
        {
            CaesarHelper ch = new CaesarHelper();
            string strError = ch.BindBranchList(ClientID, ref ddlBranchCodeNew);
            if (!string.IsNullOrWhiteSpace(strError))
                Message1.Text = strError;
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }
    }*/
    protected void PopulateClassList(string ClientID)
    {
        //CaesarHelper ch = new CaesarHelper();
        //string strError = ch.BindClassList(ClientID, ref ddlBranchCodeNew);
        //if (!string.IsNullOrWhiteSpace(strError))
        //    Message1.Text = strError;
    }
    protected void btnChangeClass_Click(object sender, EventArgs e)
    {
        //UpdateClass();
    }
    bool Check_Member_belong_to_client(string strMemberContractNo)
    {
        if (strClientID == strMemberContractNo)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        try
        {



            Session["BindDependentsDataToGrid"] = null;
			string MEmberORStaff = "";
            if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(txtMembershipNo.Text.Trim())))
                {
                    //RFVtxtBadgeNo.Enabled = false;
                    MEmberORStaff = txtMembershipNo.Text.Trim();

                }
                else if (!string.IsNullOrEmpty(Convert.ToString(txtBadgeNo.Text.Trim())))
                {
                    RequiredFieldValidator13.Enabled = false;
                    OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
                    OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN();
                    response = null;
                    request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
                    request.customerNo = strClientID;
                    request.staffNo = txtBadgeNo.Text.Trim();

                    //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
                    request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
                    //request.IqamaNo =  "5523234443";
                    //request.TotalNoOfFamily = "3";
                    request.Username = WebPublication.CaesarSvcUsername;
                    request.Password = WebPublication.CaesarSvcPassword;
                    response = ws.EnqMbrListInfo(request);
                    MEmberORStaff = Convert.ToString(response.detail[0].membershipNo);


                }

            }
            else
            {
                //RFVtxtBadgeNo.Enabled = false;
                MEmberORStaff = txtMembershipNo.Text.Trim();
            }
            MemberHelper mh = new MemberHelper(MEmberORStaff, strClientID);
            Member m = mh.GetMemberDetails();

            if (Check_Member_belong_to_client(m.ContractNumber))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(m.MemberName)))
                {
                    //txtMemberName.Text = m.MemberName.Trim();
                    strMemberType = m.MemberType;

                    switch (m.MemberType.ToString().ToLower())
                    {
                        case "employee":
                            strMemberGroup = "E";
                            break;
                        default:
                            strMemberGroup = "D";
                            break;
                    }

                    if (Request.QueryString["OptionType"] == "Class")
                    {
                        // ddlBranchCodeOld.Text = ClassName(strClientID.Trim(), m.ClassID.Trim());
                        // ddlBranchCodeOldValue.Text = m.ClassID.Trim();
                        PopulateClassList(strClientID.Trim());
                        details.Visible = true;

                        BindDependentsDataToGrid();
                    }
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "Inputted 'Membership No OR Badge Number' does not exist in your group.";

            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
            //throw ex;
        }
    }

    protected void buttonSubmit_Click(object sender, EventArgs e)
    {
        string target = Server.MapPath("~/Uploads/");
        // for edit mode
        foreach (GridDataItem item in radGridDependents.EditItems)
        {
            //RadAsyncUpload uploaderInGridRow1 = item.FindControl("AsyncUpload1") as RadAsyncUpload;//RadUpload1
            RadUpload uploaderInGridRow1 = item.FindControl("RadUpload1") as RadUpload;//
            string uniquePrefix = WebPublication.GenerateUniqueID;
            //string destinationPath = Server.MapPath(virtualPath);
            uploaderInGridRow1.TargetFolder = target;

            foreach (UploadedFile file in uploaderInGridRow1.UploadedFiles)
            {
                string fileFileName = uniquePrefix + "_" + file.FileName;
                file.SaveAs("~/Uploads/" + fileFileName);
            }
        }
    }
    private void BindDependentsDataToGrid()
    {
        //CaesarHelper ch = new CaesarHelper();
        //DataSet ds = ch.(txtMembershipNo.Text);
        //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //{
        //    // radGridDependents.DataSource = ds;
        //    //radGridDependents.DataBind();
        //    Session["BindDependentsDataToGrid"] = ds;
        //    radGridDependents.Rebind();
        //    // RadGrid1.DataSource = ds;
        //    //RadGrid1.DataBind();
        //}
    }
    protected string ClassName(string ClientID, string ClassID)
    {
        string _strClassName = "";
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ReqClsListRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqClsListRequest_DN();
        OS_DXC_WAP.CaesarWS.ReqClsListResponse_DN response;
        request.membershipNo = long.Parse(strClientID);
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();// long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
        try
        {
            response = ws.ReqClsList(request);
            if (response.status != "0")
            {
            }
            else
            {
                if (response.status == "0")
                {
                    foreach (OS_DXC_WAP.CaesarWS.ClsListDetail_DN dtl in response.detail)
                    {
                        if (Convert.ToString(dtl.classID).Trim() == ClassID.Trim())
                        {
                            _strClassName = dtl.className;
                        }
                    }
                }
            }
            return _strClassName;
        }
        catch (Exception ex)
        {
            return "No Branch";
        }
    }
    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }
    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }

    protected void radGridDependents_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        try
        {
            //if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            //{
            //    GridEditableItem item1 = e.Item as GridEditableItem;

            //    Uploader upload = item1.FindControl("uploaderInGridRow") as Uploader;
            //    RadAjaxPanel15.ResponseScripts.Add(string.Format("window['UploadId'] = '{0}';", upload.ClientID));

            //}

            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem item = e.Item as GridEditableItem;
                // access/modify the edit item template settings here
                DropDownList list = item.FindControl("NewClassList") as DropDownList;
                new Bupa.OSWeb.Business.CaesarHelper().BindClassList(txtMembershipNo.Text, ref list);

                //if (Session["updatedValue"] != null)
                {
                    string value = DataBinder.Eval(e.Item.DataItem, "ClassId").ToString();
                    value = value.Trim().Substring(0, 1);
                    ListItem li = list.Items.Cast<ListItem>()
                       .Where(x => x.Value.Contains(value))
                       .LastOrDefault();
                    list.SelectedIndex = list.Items.IndexOf(list.Items.FindByText(li.Text));
                }
            }
            else if (e.Item is GridDataItem && !e.Item.IsInEditMode && Page.IsPostBack)
            {

                GridDataItem item = e.Item as GridDataItem;
                Label label = item.FindControl("LabelRefferenceNumber") as Label;
                if (null != Session["RefferenceNumber"])
                    label.Text = Session["RefferenceNumber"].ToString();
                //Label label = item.FindControl("Label1") as Label;
                // update the label value
                if (label.Text != null)
                {
                    //label.Text = Session["updatedValue"].ToString();
                    Session["updatedValue"] = label.Text;
                }
            }
        }
        catch (Exception)
        {

        }
    }
    protected void radGridDependents_ItemUpdated(object sender, GridUpdatedEventArgs e)
    {
        GridEditableItem editedItem = e.Item as GridEditableItem;
        DropDownList list = editedItem.FindControl("List1") as DropDownList;
        Session["updatedValue"] = list.SelectedValue;
    }
    protected void radGridDependents_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        BindDependentsDataToGrid();
    }
    protected void radGridDependents_UpdateCommand(object sender, GridCommandEventArgs e)
    {
        //DataBind();
        var editableItem = ((GridEditableItem)e.Item);
        var memberId = editableItem.GetDataKeyValue("memberID");

        //GridEditableItem editedItem = e.Item as GridEditableItem;
        //UserControl userControl = (UserControl)e.Item.FindControl(GridEditFormItem.EditFormUserControlID);
        //Prepare new row to add it in the DataSource
        //DataRow[] changedRows = this.Employees.Select("memberID = " + editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["EmployeeID"]);

        //Update new values
        Hashtable newValues = new Hashtable();
        //newValues["memberName"] = (userControl.FindControl("NewClassList") as DropDownList).Text;

        GridEditableItem editItem = e.Item as GridEditableItem;
        //e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editItem);
        //string newMemberName = newValues["memberName"].ToString();

        // for edit mode
        foreach (GridDataItem item in radGridDependents.EditItems)
        {
            Label LabelMemberID1 = editItem.FindControl("LabelMemberID") as Label;
            Label LabelMemberName1 = editItem.FindControl("LabelmemberName") as Label;
            DropDownList ddlChangeClassReason1 = editItem.FindControl("ddlChangeClassReason") as DropDownList;
            TextBox txtNewDateOfEffect1 = editItem.FindControl("txtNewDateOfEffect") as TextBox;
            Label LabelCurrentClassId1 = editItem.FindControl("LabelCurrentClassId") as Label;
            DropDownList NewClassList1 = editItem.FindControl("NewClassList") as DropDownList;
            Label LabelmembershipNo1 = editItem.FindControl("LabelmembershipNo") as Label;
            Label LabelRefferenceNumber1 = editItem.FindControl("LabelRefferenceNumber") as Label;

            //RadAsyncUpload uploaderInGridRow1 = editItem.FindControl("AsyncUpload1") as RadAsyncUpload;

            //RadAsyncUpload uploaderInGridRow1 = item.FindControl("AsyncUpload1") as RadAsyncUpload;//RadUpload1
            RadUpload uploaderInGridRow1 = item.FindControl("RadUpload1") as RadUpload;//
            string target = Server.MapPath("~/Uploads/");

            string uniquePrefix = WebPublication.GenerateUniqueID;
            //string destinationPath = Server.MapPath(virtualPath);
            uploaderInGridRow1.TargetFolder = target;
            string fileFileName = string.Empty;
            foreach (UploadedFile file in uploaderInGridRow1.UploadedFiles)
            {
                fileFileName = uniquePrefix + "_" + file.FileName.Replace("\\", "-").Split('-').Last();
                file.SaveAs(target + fileFileName);
            }


            /*_uploadCategory = UploadCategory.MEMBER_CHGCLASS;
            SetupUploader(uploaderInGridRow1, _uploadCategory);*/

            Session["RefferenceNumber"]= UpdateClass(LabelmembershipNo1.Text, LabelMemberName1.Text, ddlChangeClassReason1.SelectedValue, txtNewDateOfEffect1.Text, LabelCurrentClassId1.Text.Substring(0, 1), NewClassList1.SelectedValue, fileFileName);
        }


    }

    protected void radGridDependents_NeedDataSource1(object sender, GridNeedDataSourceEventArgs e)
    {
        if (Session["BindDependentsDataToGrid"] != null)
            radGridDependents.DataSource = Session["BindDependentsDataToGrid"];
        else
            radGridDependents.DataSource = "";
    }



}