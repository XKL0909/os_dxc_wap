<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="ClientMaster.master" Inherits="CtntPreauthHistory" Debug="true" Codebehind="CtntPreauthHistory.aspx.cs" %>

<%@ MasterType VirtualPath="ClientMaster.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<asp:Content ID="Content1_1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        
<br />
        
        
         <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
               <br /><br /> 
            <table >
           
                <tr>
                    <td>
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server" Text="3456998"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server" Text="Sunil Kumar Jose"></asp:Label></td>
                </tr>
                
          
            </table>
            
       <div id="CoverageListReport"  visible=false style=" cursor:hand; font-family:Verdana;width:95%; overflow:visible;" runat="server" >
       <img src="file:///C:\Downloads\imagemap-1291\AJAX_ImageMap\animated_loading.gif" />
       </div>

            
            
            </ContentTemplate>
        </asp:UpdatePanel>
     <br />
    <asp:Label  ID="Message1" Font-Names="verdana" Font-Size="Small" Width="95%" runat="server" ></asp:Label>
    <div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
</asp:Content>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    View and print pre-authorisations of your staff for the past 6 months
</asp:Content>
