﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="BatchUpload" Theme="Office2010Blue" Codebehind="BatchUpload.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style type="text/css">
        .gridItem {
            border-color: #404040;
            border-style: solid;
            border-width: 1px;
            font-family: Arial;
            font-size: 10pt;
            font-stretch: inherit;
        }

        .gridHeader {
            background-color: #0094ff !important;
            font-weight: bold;
            color: #fbf5f5;
            font-family: Arial;
            font-size: 11pt;
            font-stretch: inherit;
        }
    </style>

    <title>Batch Upload</title>
    <script language="javascript" type="text/javascript" src="../Scripts/jsDate.js"></script>
    <script type="text/javascript" src="../functions/Login/jquery.js"></script>
    <script type="text/javascript" src="../functions/Login/jquery.anythingslider.min.js"></script>
    <script type="text/javascript" src="../functions/Login/cufon.js"></script>
    <script type="text/javascript" src="../functions/Login/Gotham_Book_400.font.js"></script>
    <script type="text/javascript" src="../functions/Login/cufon.init.js"></script>
    <script type="text/javascript" src="../functions/Login/functions.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.validate.min.js"></script>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
    <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>

    <script type='text/JavaScript'>

        function newWindow(url) {
            popupWindow = window.open(url,
                                 'popUpWindow',
                                 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');
        }

        function modalWin(url) {

            if (window.showModalDialog) {
                window.showModalDialog(url, "name", "dialogWidth:900px;dialogHeight:600px");
            }
        }

        var popupWindow = null;

        function child_openurl(url) {

            popupWindow = window.open(url, "_blank", 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');

        }

        function Refresh() {
            gvData.PerformCallback();
        }


        function parent_disable() {
            if (popupWindow && !popupWindow.closed)
                popupWindow.focus();
        }


        // <![CDATA[
        function ShowLoginWindow() {
            pcLogin.Show();
        }
        function ShowCreateAccountWindow() {
            pcCreateAccount.Show();
            tbUsername.Focus();
        }
        // ]]> 
    </script>

    <style type="text/css">
        .style1 {
            height: 43px;
        }

        .sponsors tr td, th {
            padding: 5px;
        }

        .sponsors tr th {
            background-color: #0094ff;
            color: #fbf5f5;
        }
    </style>

</head>
<body style="font-family: Arial; font-size: smaller;" onfocus="parent_disable();" onclick="parent_disable();">

    <form id="form1" runat="server" en enctype="multipart/form-data">
        <table width="100%">
            <tr valign="middle">
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Batch Upload" Font-Size="Large" Font-Names="Arial"></asp:Label>
                </td>
                <td style="text-align: right">
                    <img src="../images/logo-new.jpg" width="119" height="99" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
        </table>

        <div>
            <table>
                <tr>
                    <td>
                        <br />
                        <br />
                        <a href="<%=Page.ResolveUrl(_BatchUrlString) %>" style="font-size: small">Back</a>
                        <br />
                        <br />
                        Ensure you upload supporting documents. This is mandatory, with the exception of Change Branch. Details of the required documents can be found <a href="<%= Page.ResolveUrl("~/Docs/ReqDocs2012.pdf") %>" target="_blank">here</a>. 
        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblSample" runat="server" Text="Download a sample file from "></asp:Label>
                    </td>
                </tr>
                <tr id="tblUpload" runat="server">
                    <td>
                        <table width="100%" style="border: 0">
                            <tr>
                                <td class="style1">
                                    <asp:FileUpload ID="uploaderBatch" runat="server" Width="100%" /></td>
                                <td class="style1">
                                    <dx:ASPxButton ID="btnSubmitBatch" runat="server" Text="Upload Excel FIle" OnClick="btnSubmitBatch_Click">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="uploaderBatch" Display="Dynamic" ErrorMessage="RequiredFieldValidator">Please select the Excel file that needs to be uploaded.</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator EnableClientScript="true"
                                        ID="RegularExpressionValidator2" runat="server"
                                        ControlToValidate="uploaderBatch" Display="Dynamic"
                                        Text="Incorrect excel file." ValidationExpression="^.+\.((xls)|(XLS)|(xlsx))$"
                                        Width="194px"></asp:RegularExpressionValidator><br />
                                </td>
                            </tr>
                        </table>





                    </td>
                </tr>
            </table>
            <table width="100%" style="border: 0">
                <tr>
                    <td>
                        <asp:Label ID="lblTitle" runat="server" Text="" Font-Bold="True"></asp:Label>

                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
                        </asp:ScriptManager>



                        <dx:ASPxGridView ID="gvData" runat="server" OnHtmlDataCellPrepared="gvData_HtmlDataCellPrepared"
                            AutoGenerateColumns="False" KeyFieldName="SEQ"
                            OnRowUpdating="gvData_RowUpdating" Width="100%"
                            OnCellEditorInitialize="gvData_CellEditorInitialize" Visible="False"
                            OnCustomCallback="gvData_CustomCallback">

                            <Columns>
                                <dx:GridViewDataTextColumn Name="Upload Indecator" Visible="true" VisibleIndex="0" Width="5px">

                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn VisibleIndex="0">
                                    <EditButton Visible="True">
                                    </EditButton>
                                    <ClearFilterButton Visible="True">
                                    </ClearFilterButton>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn Name="Upload" Visible="true" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td align="center">
                                                    <%--<a href="#" onclick="child_openurl('../SimpleUpload.aspx?UploadCategory=<%=_UploadCategory %>&UserID=<%=_Username%>&RequestID=<%=_InitialRequestID %>SEQ<%# Container.VisibleIndex %>&BatchUpload=true&seq=<%# Container.VisibleIndex %>');">
                           <img src="../images/UploadDocs.png" width="22px" height="22px" /> </a>--%>
                                                    <%  _urlValues = "UploadCategory=" + _UploadCategory + "&UserID=" + _Username + "&RequestID=" + _InitialRequestID + "SEQ" + _containerValue + "&BatchUpload=true&seq=" + _containerValue + "";
                                                        _urlValues = Cryption.Encrypt(_urlValues);
                                                    %>
                                                    <a href="#" onclick="child_openurl('../UploadRevamp.aspx?val=<%=_urlValues%>');">
                                                        <img src="../images/UploadDocs.png" width="22px" height="22px" alt="" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <%--<a href="#" onclick="child_openurl('../SimpleUpload.aspx?UploadCategory=<%=_UploadCategory %>&UserID=<%=_Username%>&RequestID=<%=_InitialRequestID %>SEQ<%# Container.VisibleIndex %>&BatchUpload=true&seq=<%# Container.VisibleIndex %>');">Upload Documents</a>--%>
                                                    <a href="#" onclick="child_openurl('../UploadRevamp.aspx?val=<%=_urlValues%>');">Upload Documents</a>
                                                    <%_containerValue++;%>
                                                </td>
                                            </tr>

                                        </table>
                                    </DataItemTemplate>
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>



                            </Columns>
                            <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
                            <SettingsPopup>
                                <EditForm Width="1024" VerticalAlign="WindowCenter" HorizontalAlign="WindowCenter" />
                            </SettingsPopup>
                            <SettingsBehavior ConfirmDelete="True" />
                            <SettingsPager Mode="ShowPager" PageSize="100" />
                            <Settings ShowTitlePanel="true" ShowFilterRow="True" />
                            <Settings ShowFilterRow="True" />
                            <Images SpriteCssFilePath="~/App_Themes/Office2010Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="5px">
                                </LoadingPanel>
                            </Styles>
                            <StylesPager>
                                <PageNumber ForeColor="#3E4846">
                                </PageNumber>
                                <Summary ForeColor="#1E395B">
                                </Summary>
                            </StylesPager>
                            <StylesEditors ButtonEditCellSpacing="0">
                                <ProgressBar Height="21px">
                                </ProgressBar>
                            </StylesEditors>
                        </dx:ASPxGridView>
                    </td>
                </tr>
                <tr id="trCCHIMapping" runat="server" visible="false">
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td class="gridHeader">CCHI CAP Analyses
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Repeater ID="rptSponsors" runat="server" OnItemDataBound="OnItemDataBound">
                                        <ItemTemplate>

                                            <h2 style="font-family: Arial; font-size: 12px;"><%# FormatValidationMessge( Eval("ErrorDetails.ServiceResult").ToString()) %></h2>
                                            <asp:Repeater ID="rptSponsor" runat="server" OnItemDataBound="rptSponsor_OnItemDataBound">

                                                <HeaderTemplate>
                                                    <table class="sponsors" border="1" style="border-collapse: collapse">
                                                        <thead>
                                                            <tr>
                                                                <th>Sponsor Id</th>
                                                                <th>Type</th>
                                                                <th>Saudi Main</th>
                                                                <th>Saudi Dependent</th>
                                                                <th>Non-Saudi Main</th>
                                                                <th>Non-Saudi Dependent</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("SponsorId") %></td>
                                                        <td>
                                                            <%# Eval("CapType") %>
                                                        </td>
                                                        <td>
                                                            <div id="saudiMain" style="text-align: center" runat="server">
                                                                <%# Eval("SaudiMain") %>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div id="saudiDependent" style="text-align: center" runat="server">
                                                                <%# Eval("SaudiDependent") %>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div id="nonSaudiMain" style="text-align: center" runat="server">
                                                                <%# Eval("NonSaudiMain") %>
                                                            </div>
                                                            <td>
                                                                <div id="nonSaudiDependent" style="text-align: center" runat="server">
                                                                    <%# Eval("NonSaudiDependent") %>
                                                                </div>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td align="left">

                        <dx:ASPxButton ID="btnValidateData" runat="server"
                            Text="Submit Data" Visible="false" Width="25%"
                            OnClick="btnValidateData_Click" Height="34px">
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr>
                    <td>


                        <asp:Label ID="lblFileName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>

                        <asp:Label ID="lblRowErrors" ForeColor="red" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>

                        <asp:Label ID="lblRowSuccess" BackColor="green" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>

                        <asp:Label ID="lblMessage" runat="server" ForeColor="#FF3300" Style="font-weight: 700"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>

                        <asp:GridView ID="gridBatchResults" Font-Size="Small" Font-Names="arial" runat="server">
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>

        <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
        <div style="display: none;" id="divSessionExpired">
            <div>
                <br />
                <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                <br />
                <br />
            </div>
            <div class="inputEntity">

                <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                    <asp:Literal runat="server" Text="No"></asp:Literal></a>

            </div>
        </div>

        <h3 style="display: none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>

                <asp:LinkButton ID="lnkSession" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                    <div class="header">
                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left">
                                    <strong style="font-size: larger">Session Expiring! </strong>
                                </td>
                                <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                            </tr>
                        </table>
                    </div>

                    <div class="body">
                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="text-align: left">
                                    <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                        Do you want to extend?</strong>
                                </td>
                                <td style="text-align: right">
                                    <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                        هل تريد تمديد الوقت؟ &nbsp;</strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="footer" align="center">
                        <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                        <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                    </div>
                </asp:Panel>

                <div id="mainContent">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <script type='text/JavaScript'>
        $(function () {
            $('#<%= btnValidateData.ClientID %>').click(function () {
                $.blockUI({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
            });

        });
    </script>
</body>
</html>
