﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="ClientMaster.master"
    Inherits="_Default1" Codebehind="MembershipCert21.aspx.cs" %>

<%@ Register Src="../Date2.ascx" TagName="Date2" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1.Export, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<asp:Content ID="Content1_1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <script type='text/JavaScript' src='scw.js'></script>

    

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

    </style>
    <style>
@media print {
.noPrint { display:none; right-margin:7px; }
.Hidden { display:inline; right-margin:7px;}
}

.Hidden { display:none;}
</style>



    <SCRIPT src="datepicker/firebug.js" 
type=text/javascript></SCRIPT>
<!-- jQuery -->
<SCRIPT 
src="datepicker/jquery-1.2.6.min.js" 
type=text/javascript></SCRIPT>
<!-- required plugins -->
<SCRIPT src="datepicker/date.js" 
type=text/javascript></SCRIPT>
<!--[if IE]>
<SCRIPT 
src="datepicker/jquery.bgiframe.min.js" 
type=text/javascript></SCRIPT>
<![endif]--><!-- jquery.datePicker.js -->
<SCRIPT 
src="datepicker/jquery.datePicker.js" 
type=text/javascript></SCRIPT>
<!-- datePicker required styles --><LINK media=screen 
href="datepicker/datePicker.css" 
type=text/css rel=stylesheet><!-- page specific styles --><LINK media=screen 
href="datepicker/demo.css" 
type=text/css rel=stylesheet><!-- page specific scripts -->
<SCRIPT type=text/javascript charset=utf-8>
            $(function()
            {
				$('.date-pick').datePicker().val(new Date().asString()).trigger('change');
            });
		</SCRIPT>

    
    
<
    <div class="adbanner" align="right">
        <img src="../images/Logo.gif" width="119" height="99">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Customer Service Tel No.:(02)6630505</span>
        <hr>
    </div>
    <div class="noPrint" align="right">
      
        <table width=100% ><tr><td align=left>
            <label ID="Label1" style="width: 273px" ><font size=4>Certificate of Membership</font></label>  </td>
            <td><a href="#" onclick="window.print()">
                &nbsp;</a></td></tr></table>
            
            
       
  
    </div>
       

       
        <%--       <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <%=DateTime.Now.ToString() %>
            <asp:Panel ID="Panel2" runat="server">
                <asp:Image ID="Image2" runat="server" ImageUrl="spinner.gif" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
 <div class="noPrint" style="font-size:small;" >
        <table  style="width: 581px"><tr>
        <td valign="baseline">
        <asp:Label  ID="Label1" runat="server" Text="Membership No."></asp:Label><br />
        <asp:TextBox CssClass="textbox" ID="txtSearchMember" runat="server"></asp:TextBox><br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSearchMember"
            ErrorMessage="RequiredFieldValidator" >Membership No in Mandatory</asp:RequiredFieldValidator>
        </td>
        <td valign="baseline">
        <asp:Label ID="Label2" runat="server" Text="Purpose "></asp:Label>
        <br />
            <dx:ASPxComboBox ID="cmOption1" runat="server" ValueType="System.String">
                    <Items>
                        <dx:ListEditItem Text="Visit Hospital" Value="1" />
                        <dx:ListEditItem Text="Visa Application" Value="2" />
                    </Items>
                </dx:ASPxComboBox>
         <br />
       </td>
        <td valign="baseline" style="width: 104px"><br /><asp:Button CssClass="submitButton" ID="btnSearchCer" 
                runat="server" Text="Search" onclick="btnSearchCer_Click" />
</td></tr></table>
</div>


        </asp:Content>