﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="BatchUploadReqDocs"  Theme="Office2010Blue" Codebehind="BatchUploadReqDocs.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Batch Upload</title>

    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>

     <script type='text/JavaScript'>


         



         function newWindow(url) {
             popupWindow = window.open(url,
                                  'popUpWindow',
                                  'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');
         }

         function modalWin(url) {
            
            if (window.showModalDialog) 
            {
                window.showModalDialog(url, "name", "dialogWidth:900px;dialogHeight:600px");
            }
        }

        var popupWindow = null;

        function child_openurl(url) {

            popupWindow = window.open(url, "_blank", 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');

        }

        function Refresh() {
            gvData.PerformCallback();
        }


        function parent_disable() {
            if (popupWindow && !popupWindow.closed)
                popupWindow.focus();
        }


        // <![CDATA[
        function ShowLoginWindow() {
            pcLogin.Show();
        }
        function ShowCreateAccountWindow() {
            pcCreateAccount.Show();
            tbUsername.Focus();
        }
        // ]]> 


    </script>

</head>
<body style="font-family: Arial; font-size: smaller;" onFocus="parent_disable();" onclick="parent_disable();"> 
    <table width="100%">
        <tr valign="middle">
            <td>
                <asp:Label ID="Label2" runat="server" Text="Submit Required Documents" Font-Size="Large" Font-Names="Arial"></asp:Label>
            </td>
            <td style="text-align: right">
                <img src="../images/Logo.gif" width="119" height="99" />
            </td>
        </tr>
        <tr>
            <td>
                
        <a  target="_self"  href="<%= Page.ResolveUrl("~/client/main.aspx") %>" style="font-size:small" >Back</a>
       </td>
            <td>
            </td>
        </tr>
    </table>

    <form id="form1" runat="server" en enctype="multipart/form-data">
    <div>
    <table width="100%" style="border:0">
        <tr>
            <td> 
            <table>
            <tr style="display:none">
                <td style="height: 22px" valign="top">
                    Search Type</td>
                <td style="height: 22px" valign="top">
                    
                    <dx:ASPxComboBox ID="ddlSearchtype" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlSearchtype_SelectedIndexChanged">
                        <Items>
                            <dx:ListEditItem Text="--- Select search Type ---" Value="0" />
                            <dx:ListEditItem Text="By Reference Number" Value="1" />
                            <dx:ListEditItem Text="By Submitted Date" Value="2" />
                            <dx:ListEditItem Text="Both" Value="3" />

                        </Items>
                    </dx:ASPxComboBox>
                </td>
                <td style="height: 22px" valign="top">
                    
                    &nbsp;</td>
                <td style="height: 22px" valign="top">
                    &nbsp;</td>
            </tr>
            <tr runat="server" id="refnum" visible=true>
                <td style="height: 22px" valign="top">
                    <asp:Label ID="lbl1" Text="Reference Number" runat="server"></asp:Label>
                </td>
                <td style="height: 22px" valign="top">
                    <dx:ASPxTextBox ID="txtSearchRef" runat="server" Width="170px">
                    <ValidationSettings RequiredField-IsRequired="true" ErrorDisplayMode="ImageWithTooltip" ErrorText="Field is required" RegularExpression-ValidationExpression="^\d+$"  RegularExpression-ErrorText="Only number are allowed"/>
                    </dx:ASPxTextBox>
                </td>
                <td style="height: 22px" valign="top">
                    <dx:ASPxButton ID="btnSubmitBatch" runat="server" Text="Submit"   onclick="btnSubmitBatch_Click"  >
        </dx:ASPxButton>
                </td>
                <td style="height: 22px" valign="top">
                    &nbsp;</td>
            </tr>
            <tr  runat="server" id="submitdate" visible=false>
                <td valign="top">
                    <asp:Label ID="Label1" Text="Date Submitted" runat="server"></asp:Label>
                </td>
                <td valign="top">
                    <dx:ASPxDateEdit ID="txtSearchDate" runat="server" DisplayFormatString="dd/MM/yyyy" CssClass="osWebListFields"
                                        EditFormatString="dd/MM/yyyy" EditFormat="Custom" UseMaskBehavior="true" 
                                        Width="100px" EnableViewState="true">
                                        <ValidationSettings RequiredField-IsRequired="true" ErrorDisplayMode="ImageWithTooltip" ErrorText="Field is required" />
                                    </dx:ASPxDateEdit><br />
                </td>
                <td valign="top">
                    &nbsp;</td>
                <td valign="top">
                    &nbsp;</td>
            </tr>
            
            </table>
            </td>
         </tr>
        <tr >
            <td align="right"> 
                &nbsp;</td>
         </tr>
         </table>
    <table width="100%" style="border:0">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
          <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
        </asp:ScriptManager>

        <asp:HiddenField ID="hfReqDocValue" runat="server" />

        <dx:ASPxGridView ID="gvData" runat="server" OnHtmlDataCellPrepared="gvData_HtmlDataCellPrepared"
        AutoGenerateColumns="False" KeyFieldName="Seq" 
        OnRowUpdating="gvData_RowUpdating" Width="100%"  
        oncelleditorinitialize="gvData_CellEditorInitialize" Visible="False" 
            oncustomcallback="gvData_CustomCallback" >
        
        <Columns>
             
        <dx:GridViewDataTextColumn Name="Upload Indecator" Visible="true" VisibleIndex="0" 
                Width=5px>
                    <DataItemTemplate>
                            <img src="<%# getIndecator(Container.VisibleIndex) %>" width="22px" height="22px" />
                    </DataItemTemplate>
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
<dx:GridViewCommandColumn VisibleIndex="0">
                <EditButton Visible="True">
                </EditButton>
                <ClearFilterButton Visible="True">
                </ClearFilterButton>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Name="Upload" Visible="true" VisibleIndex="1">
                    <DataItemTemplate>
                    <table>
                        <tr>
                            <td align="center">
                              
                                <%--<a href="#" onclick="child_openurl('../SimpleUpload.aspx?UploadCategory=<%# _UploadCategory %>&UserID=<%=_Username%>&RequestID=<%=_InitialRequestID %>SEQ<%# Container.KeyValue %>&BatchUpload=true&seq=<%# Container.KeyValue %>&isresub=yes');">
                           <img src="../images/UploadDocs.png" width="22px" height="22px" /> </a>--%>

                                  <script language="javascript">                                    
                                    document.getElementById("hfReqDocValue").value = <%# Container.KeyValue %>;
                                </script>
                                
                                 <% 
                                     _containerValue = hfReqDocValue.Value;
                                     _urlValues = "UploadCategory=" + _UploadCategory + "&UserID="+_Username+"&RequestID="+_InitialRequestID+"SEQ"+_containerValue+"&BatchUpload=true&seq="+_containerValue+"&isresub=yes";
                                     _urlValues = Cryption.Encrypt(_urlValues);
                                %>
                                <a href="#" onclick="child_openurl('../UploadRevamp.aspx?val=<%=_urlValues%>');">
                           <img src="../images/UploadDocs.png" width="22px" height="22px" alt="" /> </a>


                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <%--<a href="#" onclick="child_openurl('../SimpleUpload.aspx?UploadCategory=<%# _UploadCategory %>&UserID=<%=_Username%>&RequestID=<%=_InitialRequestID %>SEQ<%# Container.KeyValue %>&BatchUpload=true&seq=<%# Container.KeyValue %>&isresub=yes');">Upload Documents</a>--%>
                                <a href="#" onclick="child_openurl('../UploadRevamp.aspx?<%=_urlValues%>');">Upload Documents</a>
                                
                            </td>
                        </tr>

                    </table>
                        
                    </DataItemTemplate>
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                


        </Columns>
        <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
        <SettingsPopup>
            <EditForm Width="1024" VerticalAlign="WindowCenter" HorizontalAlign="WindowCenter"  />
        </SettingsPopup>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsPager Mode="ShowPager" PageSize="30" />
        <Settings ShowTitlePanel="true" ShowFilterRow="True" />
                                    <settings showfilterrow="True" />
                                    <settings showfilterrow="True" />
                                    <images spritecssfilepath="~/App_Themes/Office2010Blue/{0}/sprite.css">
                                        <loadingpanelonstatusbar url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                        </loadingpanelonstatusbar>
                                        <loadingpanel url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                        </loadingpanel>
                                    </images>
                                    <imagesfiltercontrol>
                                        <loadingpanel url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                        </loadingpanel>
                                    </imagesfiltercontrol>
                                    <styles>
                                        <header imagespacing="5px" sortingimagespacing="5px">
                                        </header>
                                        <loadingpanel imagespacing="5px">
                                        </loadingpanel>
                                    </styles>
                                    <stylespager>
                                        <pagenumber forecolor="#3E4846">
                                        </pagenumber>
                                        <summary forecolor="#1E395B">
                                        </summary>
                                    </stylespager>
                                    <styleseditors buttoneditcellspacing="0">
                                        <progressbar height="21px">
                                        </progressbar>
                                    </styleseditors>
    </dx:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td align="center">

        <dx:ASPxButton ID="btnValidateData" runat="server" 
            Text="Validate and Submit Data"  Visible=false Width="25%"
            onclick="btnValidateData_Click" Height="34px" >
        </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td>

      
        <asp:Label ID="lblFileName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>

        <asp:Label ID="lblRowErrors" ForeColor="red" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>

        <asp:Label ID="lblRowSuccess" BackColor="green" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>

        <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>

        <asp:GridView ID="gridBatchResults" Font-Size="Small" Font-Names="arial" runat="server">
        </asp:GridView>
            </td>
        </tr>
    </table>
    </div>

          <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">

                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>

                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
        <asp:UpdatePanel runat="server">
            <ContentTemplate>

                <asp:LinkButton ID="lnkSession" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                </ajaxToolkit:ModalPopupExtender>

                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                    <div class="header">
                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left">
                                    <strong style="font-size: larger">Session Expiring! </strong>
                                </td>
                                <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                            </tr>
                        </table>
                    </div>

                    <div class="body">
                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="text-align: left">
                                    <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                        Do you want to extend?</strong>
                                </td>
                                <td style="text-align: right">
                                    <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                        هل تريد تمديد الوقت؟ &nbsp;</strong>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="footer" align="center">
                        <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                        <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                    </div>
                </asp:Panel>

                <div id="mainContent">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>