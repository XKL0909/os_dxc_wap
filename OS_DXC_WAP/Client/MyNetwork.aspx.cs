using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using System.IO;
using System.Xml.Serialization;
using System.Xml;

public partial class MyNetwork : System.Web.UI.Page
{
    
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;

    string strMembershipNo;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }
        strMembershipNo = Session["MembershipNo"].ToString();
    }



    private void DisplayProviderLocationResult(OS_DXC_WAP.CaesarWS.EnqProvLocInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        ////////////string[,] ResponseDetails;

        ////////////ResponseDetails[0,0] = "test";
        ////////////ResponseDetails[0,1] = "test";
        ////////////ResponseDetails[0,2] = "test";


        ////////string arr(2,4);
        //////// string ar()() = new String({New String() {"", "", ""}, New String() {"", "", ""}};

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");


            foreach ( OS_DXC_WAP.CaesarWS.ProvDetail_DN  dtl in response.detail)
            {
               // itm = new ListItem(dtl.diagCode + "|" + dtl.diagDesc + "|" + dtl.servCode + "|" + dtl.servDesc + "|" + dtl.supplyPeriod + "|" + dtl.supplyFrom.Value.ToLongDateString() + "|" + dtl.supplyTo.Value.ToLongDateString() + "|" + dtl.estimateAmount.ToString() + "|" + dtl.cost.ToString() + "|" + dtl.lastRequestDate.Value.ToLongDateString());
                sbResponse.Append("<tr   ><td><b>" + dtl.providerName + "</b></td><td>" + dtl.address1 + "</td><td>" + dtl.address2 + "</td><td>" + dtl.address3 + "</td><td>" + dtl.district + "</td><td>" + dtl.telNo1 + "</td><td>" + dtl.telNo2 + "</td></tr>");
              //  lvwResult.Items.Add(itm);
                //BulletedList1.Items.Add(itm);

            }
            
            sbResponse.Append("</tbody></table>");
            str1 = "Provider List for " + ImageMap1.AlternateText +"<hr><table border=1   class='display' id='example'><thead><tr   ><th>Provider Name</th>  <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> " + str2 + "</tbody></table>";
            // TableReport.InnerHtml = str1;
             TableReport.InnerHtml =   sbResponse.ToString();
            //Response.Write(sbResponse);
            //TextBox2.Text = sbResponse.ToString();
        }
        else
        {
            msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }


    protected void ImageMap1_Click1(object sender, ImageMapEventArgs e)
    {
       // areaClicked.Text = e.PostBackValue;

        TableReport.Visible = true;
       // System.Threading.Thread.Sleep(4000);
        btntest.Visible = true;
        btntest.Text = e.PostBackValue;

        areaClicked.Visible = true;

        
        

    //  areaClicked.Text = "Hospital List for " +  ImageMap1.HotSpots["E"].AlternateText  + "<hr>";



        btntest.Text = "Click to Close";

        //OS_DXC_WAP.CaesarWS.EnqProvLocInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqProvLocInfoRequest_DN();
        //OS_DXC_WAP.CaesarWS.EnqProvLocInfoResponse_DN  response;
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqProvLocInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqProvLocInfoResponse_DN response;

        response = null;

        request = new OS_DXC_WAP.CaesarWS.EnqProvLocInfoRequest_DN();
           request.Username = WebPublication.CaesarSvcUsername;

           request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID =  long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); //Int64.Parse(this.txt_txn.Text.Trim());
            request.membershipNo = strMembershipNo;; //txt_mbr.Text.Trim();
            request.region = e.PostBackValue;
           // request.staffNo = "";  //txt_staff.Text.Trim();
           // request.customerNo = ""; // txt_cust.Text.Trim();


            try
            {
                
                StringBuilder sb = new StringBuilder(200);
                response =  ws.EnqProvLocInfo(request);
                //response = OS_DXC_WAP.CaesarWS. ws.EnqProvLocInfo(request);
                
                if (response.errorID != "0")
                    txtBox_output.Text = response.errorMessage;
                else
                {
                    DisplayProviderLocationResult(response);

                    //String XmlizedString = null;
                    //MemoryStream memoryStream = new MemoryStream();
                    //XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqProvLocInfoResponse_DN));
                    //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                    //xmlTextWriter.Formatting = Formatting.Indented;
                    //xs.Serialize(xmlTextWriter, response);
                    //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                    //XmlizedString =  UTF8ByteArrayToString(memoryStream.ToArray());
                    //txtBox_output.Text = XmlizedString;

                }



        //         if (response.status == "0")
        //{
        //    sbResponse.Append("<table border=1 style='font:Verdana;'  class='display' id='example'><thead><tr><th>Rendering engine</th> <th>Browser</th>  <th>Platform(s)</th> <th>Engine version</th> <th>CSS grade</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> 	</tr>	</thead><tbody> ");

        //    foreach (OS_DXC_WAP.CaesarWS..MbrListDetail_DN dtl in response.detail)
        //    {
        //        itm = new ListItem(dtl.DiagCode + "|" + dtl.DiagDesc + "|" + dtl.ServCode + "|" + dtl.ServDesc + "|" + dtl.SupplyPeriod + "|" + dtl.SupplyFrom.Value.ToLongDateString() + "|" + dtl.SupplyTo.Value.ToLongDateString() + "|" + dtl.EstimateAmount.ToString() + "|" + dtl.Cost.ToString() + "|" + dtl.LastRequestDate.Value.ToLongDateString());
        //        sbResponse.Append("<tr><td>" + dtl.DiagCode + "</td><td>" + dtl.DiagDesc + "</td><td>" + dtl.ServCode + "</td><td>" + dtl.ServDesc + "</td><td>" + dtl.SupplyPeriod + "</td><td>" + dtl.SupplyFrom.Value.ToLongDateString() + "</td><td>" + dtl.SupplyTo.Value.ToLongDateString() + "</td><td>" + dtl.EstimateAmount.ToString() + "</td><td>" + dtl.Cost.ToString() + "</td><td>" + dtl.LastRequestDate.Value.ToLongDateString() + "</td></tr>"); 
        //        lvwResult.Items.Add(itm);
        //        //BulletedList1.Items.Add(itm);
                
        //    }
        //    sbResponse.Append("</tbody></table>");
        //    str1 = "<table border=2  class='display' id='example'><thead><tr><th>Rendering engine</th> <th>Browser</th>  <th>Platform(s)</th> <th>Engine version</th> <th>CSS grade</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> 	</tr>	</thead><tbody> " + str2 + "</tbody></table>";
        //    TextBox1.Text = str1;
        //    Response.Write(sbResponse);
        //    TextBox2.Text = sbResponse.ToString();
        //}
        //else
        //{
        //    msge = new StringBuilder(100);
        //    foreach (String s in response.errorMessage)
        //    {
        //        msge.Append(s).Append("/n");
        //    }
        //    Message1.Text = msge.ToString();
        //}


                ////////////if (response.errorID != "0")
                ////////////{
                ////////////    sb.Append(response.errorMessage).Append("\n");
                ////////////    MessageBox.Show(sb.ToString());
                ////////////}
                ////////////else
                ////////////{
                ////////////    String XmlizedString = null;
                ////////////    MemoryStream memoryStream = new MemoryStream();
                ////////////    XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                ////////////    XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                ////////////    xmlTextWriter.Formatting = Formatting.Indented;
                ////////////    xs.Serialize(xmlTextWriter, response);
                ////////////    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                ////////////    XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                ////////////    this.txtBox_output.Text = XmlizedString;


                ////////////}
            }
            catch (Exception ex)
            {
                //Cursor.Current = Cursors.Default;
                //MessageBox.Show(ex.Message);
            }

       // TableReport.InnerHtml = "<hr><img src='" + e.PostBackValue + ".jpg' size=25>";

    }


    protected void btntest_Click(object sender, EventArgs e)
    {
        TableReport.Visible = false;
        btntest.Visible = false;
        areaClicked.Visible = false;
        //UpdatePanel12.Visible = false;
        TableReport.InnerHtml = "";
    }




    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }


    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }

}
