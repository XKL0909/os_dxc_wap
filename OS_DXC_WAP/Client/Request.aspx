﻿<%--<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="Request.aspx.cs" Inherits="Request" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Templates/Inner.Master" Inherits="Request" Codebehind="Request.aspx.cs" %>

<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">

    <script type="text/javascript">
        function OpenDcuments() {
            window.open('Documents/CCHI-unified-manual-Web-Addition.pdf', '_newtab');
        }
    </script>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <div class="container bb">
        <div class="row bb">&nbsp;</div>
        <div class="row bb">
            <div class="clearfix bb">
                <div class="row text-left pull-left bb">
                    <%-- <a href="Request.aspx">«Back</a>--%>
                </div>
                <div class="row pull-right text-right bb ">
                    <a href="CCHICap.aspx" style="width: 100px; color: #fff" id="lnkCCHICap" class="btnTOAdd btn-primary bb" runat="server">CCHI Cap</a>
                    <asp:Button ID="btnGuide" Width="150" CssClass="btnTOAdd btn-primary bb" runat="server" Text="New Addition Guide" OnClientClick="OpenDcuments()" />
                    <asp:Button ID="btnGuideAramco" Width="150" CssClass="btnTOAdd btn-primary bb" runat="server" Text="New Addition Guide" OnClientClick="OpenDcuments()" Visible="false" />
                    <asp:Button ID="btnEmpDependent" Width="250" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Batch upload employee & dependent" OnClick="btnEmpDependent_Click" />
                    <asp:Button ID="btnEmpDependentbatch" Width="250" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Member Batch upload" OnClick="btnEmpDependentbatch_Click" />
                    <asp:Button ID="btnIndividualDependent" Width="175" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Batch upload dependent" OnClick="btnIndividualDependent_Click" />
                    <asp:Button ID="btnCreateRequest" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Create Request" OnClick="btnCreateRequest_Click" />
                </div>
            </div>

            <br />
            <div class="row bb">
                You can add members using the <span style="color: red">‘Add’</span> button in the table below Or you can upload an excel file through the<span style="color: red"> ‘Batch Upload’</span> button.
            </div>
            <div style="direction: rtl" class="row text-right bb">
                يمكنك إضافة الأعضاء مباشرة عن طريق الضغط على رمز
                 <span style="color: red">Add </span>الظاهر في الجدول أدناه، أو عن طريق تحميل ملف إكسل من خلال رمز <span style="color: red">Batch Upload</span>
            </div>
            <div class="row bb">
                <asp:Label ID="lblMessege" runat="server" />
                <asp:Repeater runat="server" ID="rptRequest">
                    <HeaderTemplate>
                        <table id="tableRequest" class="table table-striped table-bordered dt-responsive nowrap bb">
                            <thead>
                                <tr>
                                    <th>Request No</th>
                                    <th>Owner</th>
                                    <th>Contract</th>
                                    <th>Created</th>
                                    <th>Reference No</th>
                                    <th>Submitted</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%#Eval("Id") %></td>
                            <td><%#Eval("OwnerId") %></td>
                            <td><%#Eval("ContractNo") %></td>
                            <td><%#Convert.ToDateTime(Eval("Created")).ToString("dd-MM-yyyy HH:mm") %></td>
                            <td><%#Eval("ReferenceNo") %></td>
                            <td><%# Convert.ToBoolean(Eval("IsPosted"))==true ?"Yes":"No" %></td>
                            <td>
                                <a href='RequestDetail.aspx?requestid=<%# Eval("Id") %>'>Add</a>&nbsp;|&nbsp;
                                <asp:LinkButton runat="server" ID="BtnDelete" OnClick="BtnDelete" Text="Delete" CommandArgument='<%# Eval("Id") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0?true:false %>' Text="No item(s) found" />
                            </td>
                        </tr>

                    </tfoot>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <br />
    <br />
    <uc2:OSNav ID="OSNav1" runat="server" />
    <br />
</asp:Content>

<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        <%-- <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>--%>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>

</asp:Content>
