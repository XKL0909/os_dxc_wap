﻿using Caesar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services.Dto;
using UnifiedPolicy;
using UnifiedPolicy.Services;

public partial class step1e : System.Web.UI.Page
{
    private static string contractNumber;
    private long reqDetailID;

    private static AddMemberRequestDetailService _memberService;

    public step1e() : this(new AddMemberRequestDetailService())
    {

    }


    public step1e(AddMemberRequestDetailService memberService)
    {
        _memberService = memberService;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        contractNumber = Convert.ToString(Session["ClientID"]);
        txtID.Focus();
        InitDropDown();
        if (HttpContext.Current.Session["AddMember"] != null)
        {

            BindExistingValue();
        }

    }

    public void InitDropDown()
    {
        contractNumber = Convert.ToString(Session["ClientID"]);
        drplistSponsor.DataSource = CaesarManager.GetSponsor(contractNumber);
        drplistSponsor.DataValueField = "sponsorID";
        drplistSponsor.DataTextField = "sponsorID";
        drplistSponsor.DataBind();
        drplistSponsor.Items.Insert(0, new ListItem("Select Sponsor", ""));
    }

    private void BindExistingValue()
    {
        // var member = _memberService.FindByID(reqDetailID);

        var member = (AddMemberRequestDetail)HttpContext.Current.Session["AddMember"];

        drplistSponsor.SelectedValue = member.Sponsor;
        txtID.Text = member.IdNumber;

        if (member.HijriDOBYear != null)
        {
            sectionHijari.Visible = true;
            reqValtxtHYearOfBirth.Enabled = true;
            txtHYearOfBirth.Text = member.HijriDOBYear.ToString();
        }
        else
        {
            reqValtxtHYearOfBirth.Enabled = false;
        }

        if (member.DepHijriDOBYear != null)
        {
            SelAddFamily.SelectedValue = "Yes";
            sectionDepHijari.Visible = true;
            reqValtxtDepHYearOfBirth.Enabled = true;
            txtDepHYearOfBirth.Text = member.DepHijriDOBYear.ToString();
        }
        else
        {
            reqValtxtDepHYearOfBirth.Enabled = false;
        }
    }


    [WebMethod(EnableSession = true)]
    private static int DeleteFiles(int id)
    {
        var FileUploadService = new AddMemberFileuploadService();
        var result = FileUploadService.Remove(id);
        return result;
    }


    [WebMethod(EnableSession = true)]
    public static int InitSession()
    {
        HttpContext.Current.Session["AddMember"] = null;
        HttpContext.Current.Session["ReqDetailID"] = null;
        HttpContext.Current.Session["Members"] = null;
        return 1;

    }



    [WebMethod(EnableSession = true)]
    public static long AddMemberDetails(AddMemberRequestDetail member)
    {
        long result = 0;
        HttpContext.Current.Session["AddMember"] = null;
        if (HttpContext.Current.Session["AddMember"] == null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(member.DepHijriDOBYear)) && !string.IsNullOrEmpty(Convert.ToString(member.DepIqamaNo)))
            {
                member.RequestType = "ED";
            }
            member.Status = 1;
            member.IdType = GeneralHelper.GetTitleByRelation(member.IdNumber.Trim());
            member.ContNumber = contractNumber;

            HttpContext.Current.Session.Add("AddMember", member);
        }
        return result;
    }

}

