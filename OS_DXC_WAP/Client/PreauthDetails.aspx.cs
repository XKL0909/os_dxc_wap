using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class Client_PreauthDetails : System.Web.UI.Page
{
    string PreauthID;
    string ProviderCode;
    string EpisodeNo;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        PreauthID = Request.QueryString["PID"];
        ProviderCode = Request.QueryString["PC"];
        EpisodeNo = Request.QueryString["EN"];
        DisplayPreAuthHist();
    }


    private void DisplayPreAuthHist()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN request;
        OS_DXC_WAP.CaesarWS.RequestPreauthByIDResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN();
            request.episodeNo =  long.Parse(EpisodeNo);
            request.preAuthorisationID =  long.Parse(PreauthID);
            request.providerCode =  ProviderCode;
               // .membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.RequestPreauthByID(request);// ws.RequestPreauthByID(request);

            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.preAuthorizationID != null)
            {

                //lblMembershipNo.Text = request.membershipNo;
                //lblMemberName.Text = request.membershipNo;
                lblPolicyNo.Text = response.membershipNo;
                lblApprovalNo.Text = request.preAuthorisationID.ToString();
                lblClass.Text = response.memberClass;
                lbAge.Text = response.age.ToString();
                lblDateOfVisit.Text = String.Format("{0:d}", response.date_Time);
                lblDept.Text = response.department;
                lblExpiryDate.Text =  String.Format("{0:d}", response.expiryDate);
                lblIDCard.Text = response.IDCardNo;
                lblInsurance_Co.Text = response.insurance_Company;
                lblInsuredName.Text = response.memberName;
                lblPatientFileNo.Text = response.patientFileNo;
                lblPolicyHolder.Text = response.policyHolder;
                //need to check///lblPolicyNo.Text = response.
                lblProviderFaxNo.Text = response.providerFaxNo;
                lblProviderName.Text = response.provider_Name;
                lblDateTime.Text = response.date_Time.ToString();
                lblSex.Text = response.gender;
                ////// need to check       lblApprovalNo.Text = response.
                lblApprovalValidity.Text =  String.Format("{0:d}",  response.app_Validity);
                lblInsuranceOfficer.Text = response.insurance_Officer;
                
                lblPreauthorisationStatus.Text = response.preauthorisationStatus;
                lblRoomType.Text = response.roomType;
                lblComments.Text = response.comments;

                StringBuilder sbResponse = new StringBuilder(2200);
                if (response.chronic_Ind.Trim() == "Y" || response.supply_Period.Length > 0)
                    sbResponse.Append("<table border=0 width=100%><tr><td width=15px><b>Service Code</td><td width=20px><b>Supply Period</td><td width=25px><b>Supply From </td><td width=25px><b>Supply To</td><td width=20%><b>Service Description</td><td width=35%><b>Notes</b></td></tr>");
                else
                    sbResponse.Append("<table width=100%><tr><td width=20%><b>Service Code</td><td><b>Service Description</td><td><b>Notes</b></td></tr>");

                for (int i = 0; i < response.service_Code.Length; i++)
                {
                    sbResponse.Append("<tr>");
                    if (response.chronic_Ind.Trim() == "Y" || response.supply_Period.Length > 0)
                    {
                        sbResponse.Append("<td>");
                        if (response.service_Code[i] != null) sbResponse.Append(response.service_Code[i]);
                        sbResponse.Append("</td><td>");
                        if (response.supply_Period[i] != null) sbResponse.Append(response.supply_Period[i] + " Month/s");
                        sbResponse.Append("</td><td>");
                        if (response.supply_From[i] != null) sbResponse.Append(DateTime.ParseExact(String.Format("{0:d}", response.supply_From[i].Replace(" 00:00:00.0", "")), "yyyy-MM-dd", null).ToShortDateString());
                        sbResponse.Append("</td><td>");
                        if (response.supply_To[i] != null) sbResponse.Append(DateTime.ParseExact(String.Format("{0:d}", response.supply_To[i].Replace(" 00:00:00.0", "")), "yyyy-MM-dd", null).ToShortDateString());
                        sbResponse.Append("</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td></tr>");
                    }

                    else
                        sbResponse.Append("<td>" + response.service_Code[i] + "</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td></tr>");  // response.service_Code
                } 
                 ServiceListReport.Visible = true;
                ServiceListReport.InnerHtml =   sbResponse.ToString();


                //lblNotes.Text = response.notes.ToString();
                //lblServiceCode.Text = response.service_Code.Length;
                //lblServiceDescription.Text = response.service_Desc.ToString();
                
                
                

                //response.
                // lblClassName.Text = response.className;
                // lblCustomerName.Text = response.companyName;

               // DisplayPreauthHistoryResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                //if (tabMemberName.HeaderText == "Member")
                //    tabMemberName.HeaderText = request.membershipNo;



            }
            else
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }







    //private void DisplayPreauthHistoryResult(OS_DXC_WAP.CaesarWS.EnqPreHisInfoResponse_DN response)
    //{
    //    ListItem itm;
    //    StringBuilder msge;


    //    string str1;
    //    string str2;
    //    str2 = "";
    //    str1 = "";
    //    StringBuilder sbResponse = new StringBuilder(2200);

    //    if (response.status == "0")
    //    {

    //        //dtl.proratedOverallAnnualLimit = ;


    //        sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px;  ;'><thead><tr><th>Pre-auth No</th>   <th>Episode No</th><th>Provider Name</th>   <th>Received Date/Time</th><th>Response Date/Time</th>   <th>Pre-auth Status</th></tr>	</thead><tbody> ");

    //        foreach (OS_DXC_WAP.CaesarWS.PreHisDetail_DN dtl in response.detail)
    //        {
    //            sbResponse.Append("<a href='PreauthDetails.aspx?PID=" + dtl.preAuthNo + "&PC=" + dtl.providerCode + "&EN=" + dtl.episodeID + "' style='cursor:hand;'><tr ><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString() + "</td><td>" + dtl.preAuthStatus + "</td></tr></a>");

    //            //if (dtl.detail_ProratedLimit != -1)
    //            //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
    //            //else
    //            //    sbResponse.Append("<td>Not Covered</td></tr>");


    //        }
    //        sbResponse.Append("</table>");
    //        CoverageListReport.Visible = true;
    //        CoverageListReport.InnerHtml = sbResponse.ToString();


    //    }
    //    else
    //    {
    //        //msge = new StringBuilder(100);
    //        //foreach (String s in response.errorMessage)
    //        //{
    //        //    msge.Append(s).Append("/n");
    //        //}
    //        //Message1.Text = msge.ToString();
    //    }

    //    //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    //}




}
