﻿using Caesar;
using CaesarApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;
using System.Configuration;
using Bupa.Yakeen.AccessLayer.Models;
using Bupa.Models.Enums;

public partial class _RequestDetail : System.Web.UI.Page
{
    protected string ShowRequest = "";
    protected string ShowMainForm = "";
    protected string ShowYakeenInfo = "";
    protected string ShowError = "";
    protected string ShowExistingMember = "";
    protected string DisableAction = "";
    protected string lblMessge = "none";
    protected string ErrorMessage = "";
    protected bool AramcoSectionUploadFile = true;
    private int requestId = 0;
    private string contractNumber = "";
    private UnifiedRequestDetail RequestDetail = new UnifiedRequestDetail();
    protected string AramcoContractType = Convert.ToString(ConfigurationManager.AppSettings["AramcoContractType"]).ToUpper();
    private UnifiedRequestService RequestService = new UnifiedRequestService();
    private UnifiedRequestDetailService RequestDetailService = new UnifiedRequestDetailService();

    private Person YakeenPersonInfo = new Person();
    public string _ReferenceNumber = "";
    protected string isAramco()
    { //method created to hide upload colomn in the request sammury 
        string result = "";
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            AramcoSectionUploadFile = false;
            result = "none";
        }
        else
        {
            AramcoSectionUploadFile = true;
            result = "block";
        }
        return result;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ErrorDiv.Visible = false;
        btnCCHI.Enabled = true;
        ShowMessageAddMember.Visible = false;

        ShowStep(Form.ShowRequest);
        //if (!string.IsNullOrEmpty(Page.Request.QueryString["requestid"]))
        if (!string.IsNullOrEmpty(Convert.ToString(Session["RequestID"])))
        {
            //Encrypt or remove query string
            //requestId = Convert.ToInt32(Page.Request.QueryString["requestid"].Trim());
            requestId = Convert.ToInt32(Session["RequestID"]);
            contractNumber = Convert.ToString(Session["ClientID"]);
            if (string.IsNullOrEmpty(contractNumber))
                Response.Redirect("~/Default.aspx");

            var request = RequestService.FindByID(requestId);
            if (request != null && request.IsPosted != null && request.IsPosted == true)
            {
                DisableAction = request.IsPosted == true ? "none" : "";
                btnGroupActions.Visible = false;
            }
        }
        Child.Style.Add("display", "none");
        PREVMBR.Style.Add("display", "none");
        DependentEmployeeID.Style.Add("display", "none");
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            if (Convert.ToString(drpIdType.SelectedValue) != "1")
                HYOB.Style.Add("display", "none");
           
            hdnAramcoID.Value = Convert.ToString(Session["ContractType"]).ToUpper();
        }
        else
        {
            HYOB.Style.Add("display", "none");
        }
        BindRequest(requestId);

        if (!IsPostBack)
        {
            //if (Convert.ToString(Session["ContractType"]).ToUpper() != AramcoContractType){
            BindDataForEmployee();
            EnableAllControlls();
            ResetMemberType();
            CalendartxtDOB.EndDate = DateTime.Now;
        //}

        }

        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            //btnAdd.Text = "Add New";
            btnAddDependent.Visible = false;
            btnAdd.Text = "Add Member";
            lblEmployeeStaff.InnerText = "Employee Badge Number";

        }
        else
        {
            drpIdType.Items[4].Enabled = false;
            btnEmpDependentbatch.Visible = false;
        }

        if (Convert.ToString(drpIdType.SelectedValue) != "1")
            HYOB.Style.Add("display", "none");
        else
            HYOB.Style.Add("display", "block");

    }

    protected void btnEmpDependentbatch_Click(object sender, EventArgs e)
    {
        //Encrypt or remove query string
        //Session["RequestId"] = Page.Request.QueryString["requestid"];
        Response.Redirect("~/Client/BatchUpload.aspx?OptionType=" + Cryption.Encrypt("AddEmployeeDependent"));
    }

    private void ResetMemberType()
    {
        drpMemberType.Items.Clear();
        drpMemberType.Items.Insert(0, new ListItem("Select Member Type", ""));
    }

    protected void RptEmployee_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int customerID = Convert.ToInt32(e.CommandArgument.ToString());
        switch (e.CommandName)
        {
            case "Delete":
                DeleteRequest(customerID);
                Response.Redirect("RequestDetail.aspx?requestid=" + requestId);
                break;

            case "Update":
                SetRequest(customerID);
                HfRequestID.Value = customerID.ToString();
                btnAddEmpYakeen.Text = "Update";
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    btnAddEmpYakeen.Visible = true;
                    btnAddEmpYakeen.Enabled = true;
                }
                break;
        }

    }

    // get yakeen info for member can be single or dependent
    //Main Form All Validation 
    protected void btnNextEmp_Click(object sender, EventArgs e)
    {
        RequestDetailService = new UnifiedRequestDetailService();

        if (hdnEmployeeType.Value == "D")
        {
            if (drpExisitingMember.SelectedValue != "1")
            {
                var mainMember = RequestDetailService.FindByIDNumber(txtSponsor.Text.Trim(), requestId);

                //If main member is not added then asking customer to add main member first
                //else
                //setting branch code of the main member to dependent
                if (mainMember != null)
                {
                    drpBranchCode.SelectedValue = mainMember.BranchCode.Trim();
                    drpBranchCode.Enabled = false;
                }
                else
                {
                    ShowError = "Please add main member first";
                    return;
                }
            }

        }


        bool isYakeenRequire = false;
        bool isYakeenError = true;
        bool isYakeenTrunOn = true;
        bool isExist = false;

        if (drpChildAge.SelectedIndex == 0)
        {
            RequiredFieldValidatordrpChildAge.Enabled = false;
        }
        else
        {
            RequiredFieldValidatordrpChildAge.Enabled = true;
        }
        if (string.IsNullOrWhiteSpace(txtHYearOfBirth.Text))
        {
            reqValtxtHYearOfBirth.Enabled = false;
        }
        bool isYakeenResult = false;
        if (drpIdType.SelectedValue != "3")
        {

            if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            {
                divJoiningDate.Style.Add("display", "none");
                reqValtxtJoinDate.Enabled = false;
                regValtxtJoinDate.Enabled = false;
                reqValdrpPreviousMember.Enabled = false;
                DivJoinReason.Style.Add("display", "none");
                PREVMBR.Style.Add("display", "none");
                reqValtxtDepart.Enabled = false;
                reqRegExValtxtDepart.Enabled = false;
                divPreviousMember.Style.Add("display", "none");
                DivJoinReason.Style.Add("display", "none");
                reqValtxtPrevmembership.Enabled = false;
                if (!string.IsNullOrEmpty(txtID.Text.Trim()))
                {
                    isYakeenRequire = true;
                    reqValtxtExpDate.Enabled = true;
                }
                else
                {
                    reqValtxtExpDate.Enabled = false;
                }
                if (isYakeenRequire && isYakeenTrunOn)
                {

                    YakeenPersonInfo = GetYakeen();
                    if (!string.IsNullOrEmpty(YakeenPersonInfo.ErrorMessage))
                    {
                        ShowStep(Form.ShowMainForm);
                        ShowError = YakeenPersonInfo.ErrorMessage;
                        lblMessge = "inline";
                        ShowError = ShowError.Replace("<br>", "");
                        ShowError = ShowError.Replace("<br/>", "");
                        return;
                    }
                    if (!string.IsNullOrEmpty(YakeenPersonInfo.ExceptionMessage))
                    {
                        ShowStep(Form.ShowMainForm);
                        ShowError = YakeenPersonInfo.ExceptionMessage;
                        lblMessge = "inline";
                        ShowError = ShowError.Replace("<br>", "");
                        ShowError = ShowError.Replace("<br/>", "");
                        reqValdrplistSponsorAramco.Enabled = false;
                        return;
                    }
                    else
                        isYakeenError = false;
                }
                if (YakeenPersonInfo != null && !string.IsNullOrEmpty(YakeenPersonInfo.PersonID))
                {
                    isYakeenResult = true;
                }
                else
                {
                    isYakeenResult = false;
                    ShowStep(Form.ShowMainForm);
                    ShowError = "No Detail found in Yakeen";
                    lblMessge = "inline";
                    ShowError = ShowError.Replace("<br>", "");
                    ShowError = ShowError.Replace("<br/>", "");
                    reqValdrplistSponsorAramco.Enabled = false;
                    return;
                }

            }

            else
            {
                if ((drpChildAge.SelectedValue == "1" || drpChildAge.SelectedValue == "2"))
                {
                    isYakeenRequire = true;
                    if (isYakeenRequire && isYakeenTrunOn)
                    {
                        YakeenPersonInfo = GetYakeen();
                        if (YakeenPersonInfo != null && !string.IsNullOrEmpty(YakeenPersonInfo.PersonID))
                        {
                            isYakeenResult = true;
                        }
                    }

                    if (string.IsNullOrEmpty(YakeenPersonInfo.ExceptionMessage))
                    {
                        isYakeenError = false;
                        reqValdrplistSponsorAramco.Enabled = false;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtID.Text.Trim()))
                    {
                        isYakeenRequire = true;
                        reqValtxtExpDate.Enabled = true;
                    }
                    else
                    {
                        reqValtxtExpDate.Enabled = false;
                    }
                    if (!isYakeenResult && isYakeenRequire && isYakeenTrunOn)
                    {

                        YakeenPersonInfo = GetYakeen();
                        if (!string.IsNullOrEmpty(YakeenPersonInfo.ErrorMessage))
                        {
                            ShowStep(Form.ShowMainForm);
                            ShowError = YakeenPersonInfo.ErrorMessage;
                            lblMessge = "inline";
                            ShowError = ShowError.Replace("<br>", "");
                            ShowError = ShowError.Replace("<br/>", "");
                            reqValdrplistSponsorAramco.Enabled = false;
                            return;
                        }
                        if (!string.IsNullOrEmpty(YakeenPersonInfo.ExceptionMessage))
                        {
                            ShowStep(Form.ShowMainForm);
                            ShowError = YakeenPersonInfo.ExceptionMessage;
                            lblMessge = "inline";
                            ShowError = ShowError.Replace("<br>", "");
                            ShowError = ShowError.Replace("<br/>", "");
                            reqValdrplistSponsorAramco.Enabled = false;
                            return;
                        }
                        else
                            isYakeenError = false;
                    }
                    if (YakeenPersonInfo != null && !string.IsNullOrEmpty(YakeenPersonInfo.PersonID))
                    {
                        isYakeenResult = true;
                    }
                    else
                    {
                        isYakeenResult = false;
                        ShowStep(Form.ShowMainForm);
                        ShowError = "No Detail found in Yakeen";
                        lblMessge = "inline";
                        ShowError = ShowError.Replace("<br>", "");
                        ShowError = ShowError.Replace("<br/>", "");
                        reqValdrplistSponsorAramco.Enabled = false;
                        return;
                    }
                }
               
            }
        }
        else
            isYakeenError = false;


        //mapping
        if (isYakeenError == false && isYakeenRequire)
        {

            if (!string.IsNullOrWhiteSpace(YakeenPersonInfo.EnglishName))
            {
                ///// After integrating with new yakeen changes with social status, english full name is not returning in offline scenario
                ////Start
                //// Old code
                ////txtName.Text = ArabicToEnglish(YakeenPersonInfo.FullArabicName, YakeenPersonInfo.FullEnglishName);
                ////txtName.Enabled = false;
                ////New code
                txtName.Text = ArabicToEnglish(YakeenPersonInfo.FullArabicName, YakeenPersonInfo.EnglishName);
                txtName.Enabled = string.IsNullOrEmpty(txtName.Text.Trim());
                ////End
                // txtName.Text = RemoveSpecialChar(YakeenPersonInfo.EnglishName.Trim());

            }
            else if (!string.IsNullOrWhiteSpace(YakeenPersonInfo.FullEnglishName))
            {
                txtName.Text = ArabicToEnglish(YakeenPersonInfo.FullArabicName, YakeenPersonInfo.FullEnglishName);
                // txtName.Text = RemoveSpecialChar( YakeenPersonInfo.FullEnglishName.Trim());
                ////txtName.Enabled = false;
                txtName.Enabled = string.IsNullOrEmpty(txtName.Text.Trim());
            }


            if (!string.IsNullOrWhiteSpace(YakeenPersonInfo.IdExpiryDate))
            {
                txtExpDate.Text = YakeenPersonInfo.IdExpiryDate.Trim();
                txtExpDate.Enabled = false;
            }

            #region Deprectaed Code
            //if(!string.IsNullOrWhiteSpace(YakeenPersonInfo.NationalityCode))
            //{

            //    // drpNationality.SelectedValue = YakeenPersonInfo.NationalityCode.Trim();
            //    setNationalityinDropDownList(YakeenPersonInfo.NationalityCode.Trim(), drpNationality);
            //    drpNationality.Enabled = false;
            //}

            //if (!string.IsNullOrWhiteSpace(YakeenPersonInfo.OccupationCode))
            //{
            //    drpProfession.SelectedValue = YakeenPersonInfo.OccupationCode.Trim();
            //    drpProfession.Enabled = false;
            //}

            #endregion
            //gender replacement

            //SetGender(YakeenPersonInfo);

            if (YakeenPersonInfo.BirthDateGregorian != null && YakeenPersonInfo.BirthDateGregorian >= DateTime.MinValue)
            {
                txtDOB.Text = Convert.ToDateTime(YakeenPersonInfo.BirthDateGregorian).ToString("dd-MM-yyyy");
                txtDOB.Enabled = false;
            }

            if (!string.IsNullOrWhiteSpace(YakeenPersonInfo.VisaExpiryDate))
            {
                txtExpDate.Text = YakeenPersonInfo.VisaExpiryDate;
                txtExpDate.Enabled = false;
            }

        }


        if (drpMemberType.SelectedValue == "C" || drpMemberType.SelectedValue == "D" || drpMemberType.SelectedValue == "SO")
        {

        }
        else
        {
            drpChildAge.SelectedIndex = 0;
            RequiredFieldValidatordrpChildAge.Enabled = false;
        }


        //form display
        if (drpChildAge.SelectedIndex == 1)
        {
            reqValtxtHYearOfBirth.Enabled = false;
            reqValtxtID.Enabled = false;
            rfvtxtID.Enabled = false;

            //if child age is less than 1 then validation should be enabled
            reqValdrpIdType.Enabled = true;
        }

        if (drpChildAge.SelectedIndex > 0)
        {
            Child.Style.Add("display", "");
        }
        if (txtHYearOfBirth.Text.Trim() != "")
        {
            HYOB.Style.Add("display", "");
        }
        if (!string.IsNullOrWhiteSpace(txtSponsor.Text.Trim()))
        {
            DependentEmployeeID.Style.Add("display", "");

        }

        if (!string.IsNullOrWhiteSpace(txtID.Text.Trim()))
        {
            if (RequestDetailService.IsRequestExist(txtID.Text.Trim(), requestId))
            {

                isExist = true;
            }
        }

        IdType IdType = (drpIdType.SelectedValue.Trim() == "1") ? IdType.Saudi : IdType.NonSaudi;
        EmployeeType empType = (hdnEmployeeType.Value == "E") ? EmployeeType.Employee : EmployeeType.Dependent;
        SetDefaultForNationality(empType, IdType, YakeenPersonInfo, isYakeenResult);

        SetGender(YakeenPersonInfo, isYakeenResult);


        if (isExist)
        {
            ShowStep(Form.ShowMainForm);
            ShowError = "Member already exists in request.";
        }
        else
        {
            ShowStep(Form.ShowYakeenInfo);
        }


    }


    // Adding Employee Form Click
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (ShowResultMember.Visible==true)
            ShowResultMember.Visible = false;
        ResetMemberType();
        EnableAllControlls();

        //binding employee type
        BindMemberType("E");

        //no need for sponsor ID as text  
        txtSponsor.Visible = false;
        txtSponsor.Enabled = false;
        RequiredFieldtxtSponsor.Enabled = false;

        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            //enabling sponsor drop list 
            //drplistSponsor.Visible = true;
            //drplistSponsor.Enabled = true;
            reqValdrplistSponsorAramco.Enabled = true;
            reqValdrplistSponsor.Enabled = false;
            txtSopnserNew.Focus();
        }
        else
        {
            //enabling sponsor drop list 
            drplistSponsor.Visible = true;
            drplistSponsor.Enabled = true;
            reqValdrplistSponsor.Enabled = true;
            reqValdrplistSponsorAramco.Enabled = false;

            drplistSponsor.Focus();
        }
        IsEnableforSme();

        ShowStep(Form.ShowMainForm);

    }

    private void IsEnableforSme()
    {
        if (IsSme())
        {
            reqValtxtEmpNumber.Enabled = false;
            reqValtxtDepart.Enabled = false;
        }
        else
        {
            reqValtxtEmpNumber.Enabled = true;
            reqValtxtDepart.Enabled = true;
        }
    }

    private bool IsSme()
    {
        string contractType = Convert.ToString(Session["ContractType"]);
        if (contractType.Trim().ToUpper().Equals("BDRC") || contractType.Trim().ToUpper().Equals("BDSC"))
        {
            return true;
        }
        return false;
    }
    // Adding Dependent Form Click
    protected void btnAddDependent_Click(object sender, EventArgs e)
    {
        if (ShowResultMember.Visible == true)
            ShowResultMember.Visible = false;

        ResetMemberType();
        EnableAllControlls();
        //employee ID for depend memeber
        DependentEmployeeID.Style.Add("display", "");
        //binding only depends
        BindMemberType("D");

        //employee ID for depend memeber
        txtSponsor.Visible = true;
        txtSponsor.Enabled = true;
        RequiredFieldtxtSponsor.Enabled = true;

        //No need for joining date in case of dependents
        //divJoiningDate.Style.Add("display", "none");
        //reqValtxtJoinDate.Enabled = false;

        IsEnableforSme();
        //reqValtxtEmpNumber.Enabled = false;
        //reqValtxtDepart.Enabled = false;
        drpExisitingMember.Focus();
        ShowStep(Form.ShowExistingMember);
    }
    // click will insert record and show the default request form
    protected void btnAddEmpYakeen_Click(object sender, EventArgs e)
    {
        if (btnAddEmpYakeen.Text == "Add")
        {
            InsertRequest();
        }
        else
        {
            if (HfRequestID.Value != "")
            {
                UpdateRequest(Convert.ToInt32(HfRequestID.Value));
                HfRequestID.Value = "";
            }
            btnAddEmpYakeen.Text = "Add";
        }

        // BindRequest(requestId);
        Response.Redirect("RequestDetail.aspx?requestid=" + requestId);


    }
    // cancel the update secnario
    protected void btnCancelEmpYakeen_Click(object sender, EventArgs e)
    {
        ResetMemberType();
        btnAddEmpYakeen.Text = "Add";
        HfRequestID.Value = "";
    }
    // this to add dependent with employee member is having bupa membership number

    private void SetDefaultForNationality(EmployeeType empType, IdType idType, Person YakeenPersonInfo, bool yakeernError)
    {

        if (empType == EmployeeType.Employee)
        {

            if (idType == IdType.Saudi)
            {
                SetNationalityinDropDownList("966-113", drpNationality);
                SetProfession("0", drpProfession);
                drpProfession.Enabled = false;
                drpNationality.Enabled = false;
            }


            #region YakeenValidation

            if (yakeernError)
            {
                if (idType == IdType.NonSaudi)
                {
                    SetNationlityDropDownBasedOnYakeen();
                    SetProfessionDropDownBasedOnYakeen();
                }
            }
            else
            {
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    if (drpIdType.SelectedValue.Trim() != "3")
                    {
                        drpProfession.Enabled = false;
                        drpNationality.Enabled = false;

                    }
                }
                else
                {
                    drpProfession.Enabled = false;
                    drpNationality.Enabled = false;
                }
            }

            #endregion
        }

        if (empType == EmployeeType.Dependent)
        {
            //Less than 1 year baby
            if (drpChildAge.SelectedValue == "1")
            {
                //Chld/Daugher/Son
                SetProfession("99402", drpProfession);
                drpProfession.Enabled = false;

                if (idType == IdType.Saudi)
                {
                    SetNationalityinDropDownList("966-113", drpNationality);
                    drpNationality.Enabled = false;
                }
                else
                {
                    drpNationality.Enabled = true;
                }
            }
            else
            {
                if (yakeernError)
                {

                    if (idType == IdType.Saudi)
                    {
                        SetNationalityinDropDownList("966-113", drpNationality);
                        drpNationality.Enabled = false;
                    }
                    else
                    {
                        SetNationlityDropDownBasedOnYakeen();
                    }

                    SetProfessionByMemberTypeForDependent();
                    //if (!string.IsNullOrEmpty(YakeenPersonInfo.OccupationCode))
                    //{
                    //    drpProfession.Enabled = false;
                    //    bool IsMatched = SetProfession(YakeenPersonInfo.OccupationCode.Trim(), drpProfession);
                    //    if (!IsMatched)
                    //    {
                    //        drpProfession.Enabled = true;
                    //        SetProfessionByMemberTypeForDependent();
                    //    }
                    //}
                    //else
                    //{
                    //    SetProfessionByMemberTypeForDependent();
                    //}
                }
                else
                {
                    drpNationality.Enabled = true;
                    drpProfession.Enabled = true;

                }
            }

        }

    }

    private void SetGender(Person YakeenPersonInfo, bool isYakeenError)
    {
        if (isYakeenError)
        {
            if (YakeenPersonInfo.Gender != Gender.Unspecified)
            {
                if (YakeenPersonInfo.Gender == Gender.Male)
                    SetTitleGenderMaritalStatusByMale();
                else
                    SetTitleGenderMaritalStatusByFemale();
            }
            else
            {
                SetEnableOrDisableTitleGenderMarital(true);
            }

        }
        else
        {
            SetEnableOrDisableTitleGenderMarital(true);
        }
    }

    private void SetEnableOrDisableTitleGenderMarital(bool enabled)
    {
        drpGender.Enabled = enabled;
        drpTitle.Enabled = enabled;
        drpMaritalStatus.Enabled = enabled;
    }

    private void SetTitleGenderMaritalStatusByMale()
    {

        drpGender.SelectedValue = "M";
        drpTitle.SelectedValue = "Mr";

        if (drpMemberType.SelectedValue == "C" || drpMemberType.SelectedValue == "SO")
        {
            //single
            drpMaritalStatus.SelectedValue = "1";
            drpMaritalStatus.Enabled = false;
        }
        else
        {
            drpMaritalStatus.Enabled = true;
        }
        drpTitle.Enabled = false;
        drpGender.Enabled = false;
    }

    private void SetTitleGenderMaritalStatusByFemale()
    {

        drpGender.SelectedValue = "F";

        if (drpMemberType.SelectedValue == "D" || drpMemberType.SelectedValue == "C")
        {
            drpTitle.SelectedValue = "Miss";
            //single
            drpMaritalStatus.SelectedValue = "1";

            drpTitle.Enabled = false;
            drpMaritalStatus.Enabled = false;
        }
        else if (drpMemberType.SelectedValue == "S")
        {
            drpTitle.SelectedValue = "Mrs";
            //married
            drpMaritalStatus.SelectedValue = "2";


            drpTitle.Enabled = false;
            drpMaritalStatus.Enabled = false;
        }
        else
        {
            drpTitle.Enabled = true;
            drpMaritalStatus.Enabled = true;
        }
        drpGender.Enabled = false;

    }

    private void SetNationlityDropDownBasedOnYakeen()
    {
        if (!string.IsNullOrEmpty(YakeenPersonInfo.NationalityCode))
        {
            drpNationality.Enabled = false;
            bool IsMatched = SetNationalityinDropDownList(YakeenPersonInfo.NationalityCode.Trim(), drpNationality);
            if (!IsMatched)
            {
                drpNationality.Enabled = true;
            }
        }
    }

    private void SetProfessionDropDownBasedOnYakeen()
    {
        if (!string.IsNullOrEmpty(YakeenPersonInfo.OccupationCode))
        {
            drpProfession.Enabled = false;
            bool IsMatched = SetProfession(YakeenPersonInfo.OccupationCode.Trim(), drpProfession);
            if (!IsMatched)
            {
                drpProfession.Enabled = true;
            }
        }
    }

    private void SetProfessionByMemberTypeForDependent()
    {
        if (drpMemberType.SelectedItem.Value == "C" || drpMemberType.SelectedItem.Value == "SO" || drpMemberType.SelectedItem.Value == "D")
        {
            SetProfession("99402", drpProfession);
            drpProfession.Enabled = false;
        }
        else if (drpMemberType.SelectedItem.Value == "S")
        {
            SetProfession("99401", drpProfession);
            drpProfession.Enabled = false;
        }
        else
        {
            drpProfession.Enabled = true;
        }
    }

    private bool SetDropDown(string code, DropDownList ddl)
    {
        try
        {
            ListItem li = ddl.Items.Cast<ListItem>()
                        .Where(x => x.Value.Contains(code))
                        .LastOrDefault();
            ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(li.Text));

            return (li != null);
        }
        catch (Exception)
        {

        }
        return false;
    }

    private bool SetProfession(string code, DropDownList ddl)
    {
        try
        {
            ListItem li = ddl.Items.Cast<ListItem>()
                        .Where(x => x.Value.Contains(code))
                        .LastOrDefault();
            ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(li.Value));

            return (li != null);
        }
        catch (Exception)
        {

        }
        return false;
    }

    private bool SetNationalityinDropDownList(string code, DropDownList ddl)
    {
        try
        {
            ListItem li = ddl.Items.Cast<ListItem>()
                     .Where(x => x.Value.Contains(code))
                     .LastOrDefault();
            ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(li.Text));
            return (li != null);
        }
        catch (Exception)
        {

        }
        return false;
    }

    protected void btnDependentNext_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            //    if (drpExisitingMember.SelectedValue == "1")
            //    {
            //        MbrDetail_DN[] Res = CaesarManager.GetMembership(contractNumber, txtBupaMembershipNumber.Text.Trim());
            //        if (Res != null && Res.Count() > 0)
            //        {
            //            if (Res[0].membershipNo != "")
            //            {
            //                txtSponsor.Text = Res[0].memberID.Trim();
            //                try
            //                {
            //                    if (drplistSponsor.Items.FindByValue(Res[0].sponsorID.Trim()) == null)
            //                    {
            //                        drplistSponsor.Items.Add(new ListItem(Res[0].sponsorID.Trim(), Res[0].sponsorID.Trim()));
            //                    }
            //                    drplistSponsor.SelectedValue = Res[0].sponsorID;
            //                }
            //                catch (Exception ex)
            //                {

            //                    ShowStep(Form.ShowExistingMember);
            //                    ShowError = "Membership number is not valid for contract.";
            //                    return;
            //                }

            //                //If is main member setting brach id of dependenet same as employee
            //                if (!string.IsNullOrEmpty(Res[0].BranchCode))
            //                {
            //                    drpBranchCode.SelectedValue = Res[0].BranchCode;
            //                    drpBranchCode.Enabled = false;
            //                }

            //                //If membership is exists then select the dropdown

            //                if (!string.IsNullOrWhiteSpace(Res[0].sponsorID))
            //                {
            //                    drplistSponsor.SelectedValue = Res[0].sponsorID;
            //                    drplistSponsor.Enabled = false;
            //                }

            //                DependentEmployeeID.Style.Add("display", "");
            //                ShowStep(Form.ShowMainForm);
            //            }
            //            else
            //            {
            //                ShowStep(Form.ShowExistingMember);
            //                ShowError = "Membership number is invalid.";
            //            }

            //        }
            //        else
            //        {
            //            ShowStep(Form.ShowExistingMember);
            //            ShowError = "Membership number is invalid.";
            //        }
            //    }
            //    else
            //    {
            //        DependentEmployeeID.Style.Add("display", "");
            //        ShowStep(Form.ShowMainForm);
            //    }

            //    drplistSponsor.Focus();
        }
        else
        {
            if (drpExisitingMember.SelectedValue == "1")
            {
                MbrDetail_DN[] Res = CaesarManager.GetMembership(contractNumber, txtBupaMembershipNumber.Text.Trim());
                if (Res != null && Res.Count() > 0)
                {
                    if (Res[0].membershipNo != "")
                    {
                        txtSponsor.Text = Res[0].memberID.Trim();
                        try
                        {
                            if (drplistSponsor.Items.FindByValue(Res[0].sponsorID.Trim()) == null)
                            {
                                drplistSponsor.Items.Add(new ListItem(Res[0].sponsorID.Trim(), Res[0].sponsorID.Trim()));
                            }
                            drplistSponsor.SelectedValue = Res[0].sponsorID;
                        }
                        catch (Exception ex)
                        {
                            ShowStep(Form.ShowExistingMember);
                            ShowError = "Membership number is not valid for contract.";
                            return;
                        }

                        //If is main member setting brach id of dependenet same as employee
                        if (!string.IsNullOrEmpty(Res[0].BranchCode))
                        {
                            drpBranchCode.SelectedValue = Res[0].BranchCode;
                            drpBranchCode.Enabled = false;
                        }
                        //If membership is exists then select the dropdown
                        if (!string.IsNullOrWhiteSpace(Res[0].sponsorID))
                        {
                            drplistSponsor.SelectedValue = Res[0].sponsorID;
                            drplistSponsor.Enabled = false;
                        }

                        DependentEmployeeID.Style.Add("display", "");
                        ShowStep(Form.ShowMainForm);
                    }
                    else
                    {
                        ShowStep(Form.ShowExistingMember);
                        ShowError = "Membership number is invalid.";
                    }

                }
                else
                {
                    ShowStep(Form.ShowExistingMember);
                    ShowError = "Membership number is invalid.";
                }
            }
            else
            {
                DependentEmployeeID.Style.Add("display", "");
                ShowStep(Form.ShowMainForm);
            }
            drplistSponsor.Focus();
        }
    }
    protected void btnCCHI_Click(object sender, EventArgs e)
    {
        //Pushing the Data to Caesar
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            CaesarManager _ceaserManager = new CaesarManager();
            UnifiedRequestService _requestService = new UnifiedRequestService();
            //string _reqId = Request.QueryString["requestId"];
            string _reqId = Convert.ToString(Session["RequestID"]);

            string str = "<br/>";

            if (!string.IsNullOrEmpty(_reqId))
            {
                var results = NormalizeManager.Normalize(Convert.ToInt32(_reqId));
                if (results.Count > 0)
                {
                    var messges = _ceaserManager.Push(results);

                    if (!messges.Any(t => t.Type == "Error"))
                    {
                        var result = messges.Where(t => t.Type == "Success").OrderByDescending(t => t.Index).FirstOrDefault();
                        if (result != null)
                        {
                            _reqId = Convert.ToString(Session["RequestID"]);
                            if (!string.IsNullOrEmpty(_reqId))
                            {
                                var reqId = Convert.ToInt32(_reqId);
                                var request = _requestService.FindByID(reqId);
                                if (request != null)
                                {
                                    request.Modified = DateTime.Now;
                                    request.IsPosted = true;
                                    request.ReferenceNo = result.Id.ToString();
                                    var isSucess = _requestService.Update(request);
                                    if (isSucess > 0)
                                    {
                                        ShowMessageAddMember.Visible = true;
                                        ShowResultMember.Visible = false;
                                        ErrorDiv.Visible = false;
                                        btnCCHI.Enabled = false;
                                        imgSuc.Visible = true;
                                        lblResult.Text = "Add member request submitted successfully reference number is: " + result.Id.ToString();

                                    }
                                    else
                                    {

                                    }

                                }
                            }
                        }
                    }
                    if (messges.Any(t => t.Type == "Error"))
                    {
                        foreach (var item in messges)
                        {
                            var error = item;

                            str += error.Id + "||" + error.Type + "||" + error.Messege;

                        }
                        lblMessge = "inline";
                        str = str.Replace("<br>", "");
                        str = str.Replace("<br/>", "");
                        ShowError = str;

                    }
                }
            }


        }
        else
        {
            string summary = ValidationManager.AttachmentSummary(requestId);
            if (string.IsNullOrWhiteSpace(summary.Trim()))
            {
                Response.Redirect("CCHIValidator.aspx?requestId=" + Convert.ToString(requestId));
            }
            else
            {
                ShowStep(Form.ShowRequest);
                ShowError = summary;
            }
        }

    }

    [WebMethod]
    public List<ListItem> GetMemberTypeByCoverList(string cover, string type)
    {
        if (!string.IsNullOrEmpty(hdnEmployeeType.Value))
        {
            var items = new List<ListItem>();
            var response = CaesarManager.GetMemberTypeByCover(Convert.ToInt32(cover), type, contractNumber);
            if (response.status == "0")
            {
                for (int i = 0; i < response.detail.Length; i++)
                {
                    items.Add(new ListItem(response.detail[i].mbrTypeDesc, response.detail[i].mbrType));
                }
            }
            else
            {
                items.Add(new ListItem("------ Not Applicable --", "0"));
            }
            return items;
        }
        return null;
    }


    public string IsDocumentsUploaded(string requestId)
    {
        UnifiedFileuploadService UploadService = new UnifiedFileuploadService();
        var result = UploadService.FindByRequestID(int.Parse(requestId));
        if (result.Count() > 0)
        {
            return "success.png";
        }
        else
        {
            return "failure.png";
        }
    }
    protected void drpCover_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(hdnEmployeeType.Value))
        {
            var items = new List<ListItem>();
            if (drpCover.SelectedIndex != 0)
            {
                var response = new EnqContMbrTypeListResponse_DN();
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    response = CaesarManager.GetMemberTypeByCover(Convert.ToInt32(drpCover.SelectedValue), "D", contractNumber);
                }
                else
                {
                    response = CaesarManager.GetMemberTypeByCover(Convert.ToInt32(drpCover.SelectedValue), hdnEmployeeType.Value, contractNumber);

                }
                List<ContMbrTypeListDetail_DN> result = new List<ContMbrTypeListDetail_DN>();
                if (response.status == "0")
                {
                    if (hdnEmployeeType.Value == "D")
                    {
                        if (FilterMemberTypeByContacts(response.detail) != null)
                        {
                            result = FilterMemberTypeByContacts(response.detail);
                            var item = result.Select(t => new ListItem
                            {
                                Text = t.mbrTypeDesc,
                                Value = t.mbrType
                            });
                            items = item.ToList();
                        }
                        DependentEmployeeID.Style.Add("display", "");
                    }
                    else
                    {
                        result = response.detail.ToList();
                        var item = result.Select(t => new ListItem
                        {
                            Text = t.mbrTypeDesc,
                            Value = t.mbrType
                        });
                        items = item.ToList();
                        DependentEmployeeID.Style.Add("display", "none");
                    }


                }
                else
                {
                    // items.Add(new ListItem("------ Not Applicable --", "0"));
                }

                drpMemberType.DataValueField = "Value";
                drpMemberType.DataTextField = "Text";
                drpMemberType.DataSource = items;
                drpMemberType.DataBind();
                drpMemberType.Items.Insert(0, new ListItem("Select Member Type", ""));


                if (drpChildAge.SelectedIndex > 0)
                {
                    Child.Style.Add("display", "");
                }
                if (txtHYearOfBirth.Text.Trim() != "")
                {
                    if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                        HYOB.Style.Add("display", "block");
                    else
                        HYOB.Style.Add("display", "");
                }
                if (!string.IsNullOrWhiteSpace(txtSponsor.Text.Trim()))
                {
                    DependentEmployeeID.Style.Add("display", "");

                }


                ShowStep(Form.ShowMainForm);
            }
            else
            {
                ShowStep(Form.ShowMainForm);
                drpMemberType.ClearSelection();
            }

        }

    }

    private List<ContMbrTypeListDetail_DN> FilterMemberTypeByContacts(ContMbrTypeListDetail_DN[] detail)
    {
        var cover = drpCover.SelectedValue.ToString();
        //var cover = "152";
        string schems = System.Configuration.ConfigurationManager.AppSettings["ContractSchems"];
        string contractType = Convert.ToString(Session["ContractType"]);
        if (detail.Count() > 0)
        {
            var memberTypes = detail.ToList();
            if (contractType.Trim().ToUpper().Equals("BDRC") || contractType.Trim().ToUpper().Equals("BDSC"))
            {
                var childItem = memberTypes.Where(t => t.mbrTypeDesc.Trim().ToUpper().Equals("CHILD")).SingleOrDefault();
                if (childItem != null)
                {
                    var schemeList = schems.Split(',').ToList<string>();
                    if (schemeList.Contains(cover))
                    {
                        var lstMemberType = memberTypes.Where(t => t.mbrTypeDesc != childItem.mbrTypeDesc);
                        return lstMemberType.ToList();
                    }

                }
            }
            return memberTypes.ToList();
        }
        return null;
    }
    protected void btnCancelDependentNext_Click(object sender, EventArgs e)
    {
        ResetMemberType();
        if (ShowResultMember.Visible == false)
            ShowResultMember.Visible = true;
    }
    protected void btnCancelEmp_Click(object sender, EventArgs e)
    {
        ResetMemberType();
        if (ShowResultMember.Visible == false)
            ShowResultMember.Visible = true;
    }

    private static string ArabicToEnglish(string NameAr, string NameEn)
    {
        string bupa_fullname = "", bupa_arabicname = "";
        bool isArabicInEnglish = false, isEnglishInArabic = false;

        if (!string.IsNullOrWhiteSpace(NameAr))
        {
            if (Regex.IsMatch(NameAr.Trim(), "^[\u0600-\u06FF]+"))
            {
                bupa_arabicname = Regex.Replace(NameAr.Trim(), @"[^ \u0600-\u06FF]+", "");
            }
            else
            {
                bupa_arabicname = Regex.Replace(NameAr.Trim(), @"[^0-9 a-z A-Z]+", "");
                isEnglishInArabic = true;
            }
        }
        if (!string.IsNullOrWhiteSpace(NameEn))
        {
            if (Regex.IsMatch(NameEn.Trim(), "^[\u0600-\u06FF]+"))
            {
                bupa_fullname = Regex.Replace(NameEn.Trim(), @"[^ \u0600-\u06FF]+", "");
                isArabicInEnglish = true;
            }
            else
            {
                bupa_fullname = Regex.Replace(NameEn.Trim(), @"[^0-9 a-z A-Z]+", "");
            }
        }

        if (isArabicInEnglish && isEnglishInArabic)
        {
            string name = bupa_fullname;
            bupa_fullname = bupa_arabicname;
            bupa_arabicname = name;
        }
        else if (isArabicInEnglish && !isEnglishInArabic)
        {
            bupa_arabicname = bupa_fullname;
            bupa_fullname = "";
        }
        else if (isEnglishInArabic && !isArabicInEnglish)
        {
            bupa_fullname = bupa_arabicname;
            bupa_arabicname = "";
        }

        return bupa_fullname;
    }
}



public partial class _RequestDetail
{


    private Person GetYakeen()
    {
        Person MyPerson = new Person();
        try
        {
            if (ValidateID(txtID.Text, Convert.ToInt32(drpIdType.SelectedValue)))
            {
                if (txtID.Text.Trim().Length == 10)
                {
                    switch (drpIdType.SelectedValue)
                    {

                        case "1"://saudi id
                            if (txtSponsor.Text != "")
                            {
                                MyPerson = YakeenManager.GetMemberInfoFromYakeen(txtID.Text.Trim(), txtHYearOfBirth.Text.Trim());
                            }
                            else
                            {
                                MyPerson = YakeenManager.GetMemberInfoFromYakeen(txtID.Text.Trim(), txtHYearOfBirth.Text.Trim());
                            }


                            break;
                        case "2"://iqama
                            if (txtSponsor.Text != "")
                            {
                                MyPerson = YakeenManager.GetMemberInfoFromYakeen(txtID.Text.Trim(), txtSponsor.Text.Trim());
                            }
                            else
                            {
                                MyPerson = YakeenManager.GetMemberInfoFromYakeen(txtID.Text.Trim(), drplistSponsor.SelectedValue.Trim());

                            }

                            break;
                        case "3"://passport

                            if (txtSponsor.Text != "")
                            {
                                EnableAllControlls();
                                break;
                            }
                            else
                            {
                                EnableAllControlls();
                            }

                            break;
                        case "4"://boarder entry

                            if (txtSponsor.Text != "")
                            {
                                MyPerson = YakeenManager.GetMemberInfoFromYakeen(txtID.Text.Trim(), txtSponsor.Text.Trim());
                            }
                            else
                            {
                                MyPerson = YakeenManager.GetMemberInfoFromYakeen(txtID.Text.Trim(), drplistSponsor.SelectedValue.Trim());

                            }

                            break;
                    }

                }
            }
            else
            {
                MyPerson.ExceptionMessage = "Invalid Id Number.";
            }
        }
        catch (Exception ex)
        {
            MyPerson.ExceptionMessage = ex.Message + Convert.ToString(ex.InnerException);

        }
        return MyPerson;



    }






    private bool ValidateID(string strIdNo, int IdType)
    {
        long n;
        bool isNumeric = long.TryParse(strIdNo, out n);

        if (isNumeric)
        {
            if (txtID.Text.Trim().Length == 10)
            {
                switch (IdType)
                {
                    case 1:
                        if (strIdNo.StartsWith("1"))
                        {
                            if (strIdNo.Trim().Length == 10)
                            {
                                return true;
                            }
                            return false;
                        }
                        break;
                    case 2:
                        if (strIdNo.StartsWith("2"))
                        {
                            if (strIdNo.Trim().Length == 10)
                            {
                                return true;
                            }
                            return false;
                        }
                        break;
                    case 4:
                        if (strIdNo.StartsWith("4") || strIdNo.StartsWith("3") || strIdNo.StartsWith("5"))
                        {
                            if (strIdNo.Trim().Length == 10)
                            {
                                return true;
                            }
                            return false;
                        }
                        break;




                }
            }
            else
            {
                if (strIdNo != null)
                {

                    drpNationality.Enabled = true;
                    drpProfession.Enabled = true;
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        return false;

    }


    public void BindRequest(int id)
    {
        RptEmployee.DataSource = RequestDetailService.Get(id).OrderBy(t => t.SecSponsorId).ThenBy(t => t.Sponsor);
        RptEmployee.DataBind();
    }
    public void BindDataForEmployee()
    {

        drpCover.DataSource = CaesarManager.GetClass(contractNumber);
        drpCover.DataValueField = "classID";
        drpCover.DataTextField = "className";
        drpCover.DataBind();
        drpCover.Items.Insert(0, new ListItem("Select Cover", ""));
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            //drpCover.Items.RemoveAt(3);
            //drplistSponsor.DataSource = CaesarManager.GetSponsor(contractNumber);
            //drplistSponsor.DataValueField = "sponsorID";
            //drplistSponsor.DataTextField = "sponsorID";
            //drplistSponsor.DataBind();
            //drplistSponsor.Items.Insert(0, new ListItem("Select Sponsor", ""));
        }
        else
        {
            drplistSponsor.DataSource = CaesarManager.GetSponsor(contractNumber);
            drplistSponsor.DataValueField = "sponsorID";
            drplistSponsor.DataTextField = "sponsorID";
            drplistSponsor.DataBind();
            drplistSponsor.Items.Insert(0, new ListItem("Select Sponsor", ""));
        }

        drpBranchCode.DataSource = CaesarManager.GetBranch(contractNumber);
        drpBranchCode.DataValueField = "branchCode";
        drpBranchCode.DataTextField = "branchDesc";
        drpBranchCode.DataBind();
        drpBranchCode.Items.Insert(0, new ListItem("Select Branch Code", ""));

        drpDistrict.DataSource = CaesarManager.GetDistrict();
        drpDistrict.DataValueField = "distCode";
        drpDistrict.DataTextField = "distName";
        drpDistrict.DataBind();
        drpDistrict.Items.Insert(0, new ListItem("Select District", ""));

        drpProfession.DataSource = CaesarManager.GetProfession();
        drpProfession.DataValueField = "profCode";
        drpProfession.DataTextField = "profName";
        drpProfession.DataBind();
        drpProfession.Items.Insert(0, new ListItem("Select Profession", ""));

        drpNationality.DataSource = NationalityManager.Get();
        // drpNationality.DataValueField = "Code";
        drpNationality.DataValueField = "MappedCode";
        drpNationality.DataTextField = "Nationality";
        drpNationality.DataBind();
        drpNationality.Items.Insert(0, new ListItem("Select Nationality", ""));

    }
    private void BindMemberType(string mbrType)
    {
        hdnEmployeeType.Value = mbrType;

        //drpMemberType.DataSource = CaesarManager.GetEmployeeMemberType(mbrType);
        //drpMemberType.DataValueField = "mbrType";
        //drpMemberType.DataTextField = "mbrTypeDesc";
        //drpMemberType.DataBind();
        //drpMemberType.Items.Insert(0, new ListItem("Select Member Type", ""));
        //drpMemberType.SelectedIndex = 0;
    }
    private void SetRequest(int id)
    {
        RequestDetailService = new UnifiedRequestDetailService();
        RequestDetail = new UnifiedRequestDetail();
        RequestDetail = RequestDetailService.FindByID(id);
        if (RequestDetail.IsMainMemeber != null && RequestDetail.IsMainMemeber == true)
        {
            txtSponsor.Visible = false;
            txtSponsor.Enabled = false;
            RequiredFieldtxtSponsor.Enabled = false;
            if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                BindMemberType("D");
            else
                BindMemberType("E");
            BindEmployeeType(RequestDetail.Cover);
            reqValtxtEmpNumber.Enabled = true;
            reqValtxtDepart.Enabled = true;

        }
        else
        {
            DependentEmployeeID.Style.Add("display", "");
            txtSponsor.Visible = true;
            txtSponsor.Enabled = true;
            RequiredFieldtxtSponsor.Enabled = true;

            divJoiningDate.Style.Add("display", "none");
            reqValtxtJoinDate.Enabled = false;

            BindMemberType("D");
            BindEmployeeType(RequestDetail.Cover);
            txtSponsor.Enabled = false;

            reqValtxtEmpNumber.Enabled = false;
            reqValtxtDepart.Enabled = false;


        }
        //Main Form Setter
        if (!string.IsNullOrEmpty(RequestDetail.Sponsor))
        {
            if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            {
                txtSopnserNew.Text /* drplistSponsor.SelectedValue*/ = RequestDetail.Sponsor.Trim();
                //drplistSponsor.Enabled = false;
                txtSopnserNew.Enabled = false;
            }
            else
            {
                drplistSponsor.SelectedValue = RequestDetail.Sponsor.Trim();
                drplistSponsor.Enabled = false;
            }
        }
        if (!string.IsNullOrEmpty(RequestDetail.SecSponsorId))
            txtSponsor.Text = RequestDetail.SecSponsorId.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.Cover))
            drpCover.SelectedValue = RequestDetail.Cover.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.IdType))
            drpIdType.SelectedValue = RequestDetail.IdType.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.IdNumber))
            txtID.Text = RequestDetail.IdNumber.Trim();
        if (RequestDetail.HijriDOBYear != null || RequestDetail.HijriDOBYear > 0)
            txtHYearOfBirth.Text = Convert.ToString(RequestDetail.HijriDOBYear);

        //Yakeen Form
        if (!string.IsNullOrEmpty(RequestDetail.IDExpiry))
        {
            txtExpDate.Text = RequestDetail.IDExpiry.Trim();
        }
        else
        {
            reqValtxtExpDate.Enabled = false;
        }
        if (!string.IsNullOrEmpty(RequestDetail.MaritalStatus))
            drpMaritalStatus.SelectedValue = RequestDetail.MaritalStatus.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.Title))
            drpTitle.SelectedValue = RequestDetail.Title.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.Gender))
            drpGender.SelectedValue = RequestDetail.Gender.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.Name))
            txtName.Text = RequestDetail.Name;

        if (!string.IsNullOrEmpty(RequestDetail.DOB))
            txtDOB.Text = RequestDetail.DOB.Trim();

        if (!string.IsNullOrEmpty(RequestDetail.Profession))
            drpProfession.SelectedValue = RequestDetail.Profession.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.District))
            drpDistrict.SelectedValue = RequestDetail.District.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.MobileNo))
            txtMobileNumber.Text = RequestDetail.MobileNo.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.BranchCode))
            drpBranchCode.SelectedValue = RequestDetail.BranchCode;
        //removed arriavldate date
        if (!string.IsNullOrEmpty(RequestDetail.ArrivalOn))
            txtJoinDate.Text = RequestDetail.ArrivalOn.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.JoinOn))
            txtJoinDate.Text = RequestDetail.JoinOn.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.PolicyStartOn))
            txtStartOfPolicy.Text = RequestDetail.PolicyStartOn.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.EmployeeNo))
            txtEmpNumber.Text = RequestDetail.EmployeeNo.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.Department))
            txtDepart.Text = RequestDetail.Department.Trim();
        if (RequestDetail.IsReturned != null)
            drpPreviousMember.SelectedValue = RequestDetail.IsReturned == true ? "1" : "2";
        if (!string.IsNullOrEmpty(RequestDetail.PrevMembership))
        {
            txtPrevmembership.Text = RequestDetail.PrevMembership.Trim();
            PREVMBR.Style.Add("display", "");

        }
        else
        {
            reqValtxtPrevmembership.Enabled = false;
        }

        if (!string.IsNullOrEmpty(RequestDetail.JoiningReason))
            drpJoinReason.SelectedValue = RequestDetail.JoiningReason.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.Nationality))
            drpNationality.SelectedValue = RequestDetail.Nationality.Trim();
        if (!string.IsNullOrEmpty(RequestDetail.MemberType))

            drpMemberType.SelectedValue = RequestDetail.MemberType.Trim();

        if (drpMemberType.SelectedValue == "C" || drpMemberType.SelectedValue == "SO" || drpMemberType.SelectedValue == "D")
        {

            drpMemberType.Enabled = false;
        }

        if (RequestDetail.IsYakeen != null && RequestDetail.IsYakeen == true)
        {
            txtName.Enabled = false;
            txtExpDate.Enabled = false;
            if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            {
                if (!string.IsNullOrEmpty(drpNationality.SelectedValue))
                    drpNationality.Enabled = false;
                else
                    drpNationality.Enabled = true;
                if (!string.IsNullOrEmpty(drpProfession.SelectedValue))
                    drpProfession.Enabled = false;
                else
                    drpProfession.Enabled = true;
            }
            else
            {
                drpNationality.Enabled = false;
                drpProfession.Enabled = false;
            }
            drpGender.Enabled = false;
            txtDOB.Enabled = false;
            txtExpDate.Enabled = false;
            txtID.Enabled = false;
            drpIdType.Enabled = false;
        }
        else
        {
            drpChildAge.SelectedIndex = 1;
            Child.Style.Add("display", "");
        }



        btnNextEmp.Visible = false;
        btnCancelEmp.Visible = false;

        ShowRequest = "none";
        ShowMainForm = "";
        ShowYakeenInfo = "";
        btnAddEmpYakeen.Visible = false;
        btnAddEmpYakeen.Enabled = false;
        Cover.Style.Add("display", "none");
        Member.Style.Add("display", "none");
    }
    private void UpdateRequest(int requestId)
    {

        UnifiedRequestDetail ExistingRequestDetail = new UnifiedRequestDetail();
        ExistingRequestDetail = RequestDetailService.FindByID(requestId);

        RequestDetailService = new UnifiedRequestDetailService();


        RequestDetail = new UnifiedRequestDetail();
        //Removed Joining date
        RequestDetail.ArrivalOn = txtJoinDate.Text.Trim();
        RequestDetail.BranchCode = drpBranchCode.SelectedValue.Trim();
        //RequestDetail.Cover = drpCover.SelectedValue.Trim();
       
        RequestDetail.Department = txtDepart.Text.Trim();
        RequestDetail.Department = !string.IsNullOrEmpty(RequestDetail.Department) ? RequestDetail.Department.Length > 50 ? RequestDetail.Department.Substring(0, 50) : RequestDetail.Department : RequestDetail.Department;
        RequestDetail.District = drpDistrict.SelectedValue.Trim();
        RequestDetail.DOB = txtDOB.Text.Trim();

        RequestDetail.EmployeeNo = txtEmpNumber.Text.Trim();
        RequestDetail.EmployeeNo = !string.IsNullOrEmpty(RequestDetail.EmployeeNo) ? RequestDetail.EmployeeNo.Length > 15 ? RequestDetail.EmployeeNo.Substring(0, 15) : RequestDetail.EmployeeNo : RequestDetail.EmployeeNo;

        if (txtHYearOfBirth.Text != "")
            RequestDetail.HijriDOBYear = Convert.ToInt32(txtHYearOfBirth.Text.Trim());
        else
            RequestDetail.HijriDOBYear = 0;


        if (!string.IsNullOrEmpty(txtExpDate.Text))
        {
            RequestDetail.IDExpiry = txtExpDate.Text.Trim();
        }
        else
        {
            RequestDetail.IDExpiry = "";
        }

        RequestDetail.IsReturned = drpPreviousMember.SelectedValue.Trim() == "1" ? true : false;
        RequestDetail.JoiningReason = drpJoinReason.SelectedValue.Trim();
        RequestDetail.JoinOn = txtJoinDate.Text.Trim();
        //RequestDetail.MemberType = drpMemberType.SelectedValue.Trim();
        RequestDetail.MobileNo = txtMobileNumber.Text.Trim();
        RequestDetail.Name = txtName.Text.Trim();
        RequestDetail.Name = !string.IsNullOrEmpty(RequestDetail.Name) ? RequestDetail.Name.Length > 40 ? RequestDetail.Name.Substring(0, 40) : RequestDetail.Name : RequestDetail.Name;
        RequestDetail.Nationality = drpNationality.SelectedValue.Trim();
        RequestDetail.PolicyStartOn = txtStartOfPolicy.Text.Trim();
        RequestDetail.Profession = drpProfession.SelectedValue.Trim();
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            RequestDetail.Sponsor = txtSopnserNew.Text /* drplistSponsor.SelectedValue.Trim()*/;
        else
            RequestDetail.Sponsor = drplistSponsor.SelectedValue.Trim();
        RequestDetail.Title = drpTitle.SelectedValue.Trim();
        RequestDetail.Gender = drpGender.SelectedValue.Trim();
        RequestDetail.MaritalStatus = drpMaritalStatus.SelectedValue.Trim();
        RequestDetail.ContNumber = contractNumber.Trim();

        //Exception

        if (!string.IsNullOrEmpty(txtPrevmembership.Text))
        {
            if (RequestDetail.IsReturned == true)
            {
                RequestDetail.PrevMembership = txtPrevmembership.Text.Trim();
            }
            else { RequestDetail.PrevMembership = ""; }
        }
        else
        {
            RequestDetail.PrevMembership = "";
        }
        if (!string.IsNullOrEmpty(txtSponsor.Text))
        {
            RequestDetail.SecSponsorId = txtSponsor.Text.Trim();
            RequestDetail.IsMainMemeber = false;
        }
        else
        {
            RequestDetail.SecSponsorId = "";
            RequestDetail.IsMainMemeber = true;
        }
        if (!string.IsNullOrEmpty(RequestDetail.IDExpiry))
        {
            RequestDetail.IsYakeen = true;
        }
        else
        {
            RequestDetail.IsYakeen = false;
        }
        //Child

        if (drpChildAge.SelectedItem.Value == "1")
        {
            RequestDetail.IsYakeen = false;
        }
        else
        {
            RequestDetail.IdNumber = txtID.Text.Trim();
            RequestDetail.IdType = drpIdType.SelectedValue.Trim();
        }

        RequestDetail.IsIndDep = ExistingRequestDetail.IsIndDep;
        RequestDetail.MbrshipNo = ExistingRequestDetail.MbrshipNo;
        RequestDetail.Cover = ExistingRequestDetail.Cover;
        RequestDetail.MemberType = ExistingRequestDetail.MemberType;
        Cover.Style.Add("display", "none");
        Member.Style.Add("display", "none");
        reqValdrplistCoverr.Enabled = false;
        reqValdrpMemberType.Enabled = false;
        // int requestID= RequestService.Add(Request);
        RequestDetail.Id = requestId;
        var reqId = RequestDetailService.Update(RequestDetail);



    }

    private string GetNationalityValue(string value)
    {
        char[] delimiters = new char[] { '-' };
        string[] parts = value.Split(delimiters,
                        StringSplitOptions.RemoveEmptyEntries);
        return parts[0];

    }
    private void InsertRequest()
    {
        RequestDetail = new UnifiedRequestDetail();
        bool isExist = false;
        RequestDetail.ContNumber = contractNumber.Trim();
        //Removed txtArriaval date
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            RequestDetail.ArrivalOn = DateTime.Now.ToString("dd-MM-yyy");
            RequestDetail.JoinOn = DateTime.Now.ToString("dd-MM-yyy");
        }
        else
        {
            RequestDetail.ArrivalOn = txtJoinDate.Text.Trim();
            RequestDetail.JoinOn = txtJoinDate.Text.Trim();
        }
        RequestDetail.BranchCode = drpBranchCode.SelectedValue.Trim();
        RequestDetail.Cover = drpCover.SelectedValue.Trim();
       
        RequestDetail.Department = txtDepart.Text.Trim();
        RequestDetail.Department = !string.IsNullOrEmpty(RequestDetail.Department) ? RequestDetail.Department.Length > 50 ? RequestDetail.Department.Substring(0, 50) : RequestDetail.Department : RequestDetail.Department;
        RequestDetail.District = drpDistrict.SelectedValue.Trim();
        RequestDetail.DOB = txtDOB.Text.Trim();
       
        RequestDetail.EmployeeNo = txtEmpNumber.Text.Trim();
        ////RequestDetail.EmployeeNo = RequestDetail.EmployeeNo.Length > 15 ? RequestDetail.EmployeeNo.Substring(0, 15) : RequestDetail.EmployeeNo;
        RequestDetail.EmployeeNo = !string.IsNullOrEmpty(RequestDetail.EmployeeNo) ? RequestDetail.EmployeeNo.Length > 15 ? RequestDetail.EmployeeNo.Substring(0, 15) : RequestDetail.EmployeeNo : RequestDetail.EmployeeNo;
        if (txtHYearOfBirth.Text != "")
            RequestDetail.HijriDOBYear = Convert.ToInt32(txtHYearOfBirth.Text.Trim());
        else
            RequestDetail.HijriDOBYear = 0;

        RequestDetail.Id = 0;
        RequestDetail.IDExpiry = txtExpDate.Text.Trim();
        if (!string.IsNullOrWhiteSpace(txtBupaMembershipNumber.Text))
            RequestDetail.MbrshipNo = txtBupaMembershipNumber.Text.Trim();
        RequestDetail.IsReturned = drpPreviousMember.SelectedValue.Trim() == "1" ? true : false;
        RequestDetail.JoiningReason = drpJoinReason.SelectedValue.Trim();

        RequestDetail.MemberType = drpMemberType.SelectedValue.Trim();
        RequestDetail.MobileNo = txtMobileNumber.Text.Trim();
        
        RequestDetail.Name = txtName.Text.Trim();
        ////RequestDetail.Name = RequestDetail.Name.Length > 40 ? RequestDetail.Name.Substring(0, 40) : RequestDetail.Name;
        RequestDetail.Name = !string.IsNullOrEmpty(RequestDetail.Name) ? RequestDetail.Name.Length > 40 ? RequestDetail.Name.Substring(0, 40) : RequestDetail.Name : RequestDetail.Name;


        RequestDetail.Nationality = drpNationality.SelectedValue.Trim();
        RequestDetail.PolicyStartOn = txtStartOfPolicy.Text.Trim();
        RequestDetail.Profession = drpProfession.SelectedValue.Trim();
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            RequestDetail.Sponsor = txtSopnserNew.Text /* drplistSponsor.SelectedValue.Trim()*/;
        else
            RequestDetail.Sponsor = drplistSponsor.SelectedValue.Trim();
        RequestDetail.Title = drpTitle.SelectedValue.Trim();
        RequestDetail.Gender = drpGender.SelectedValue.Trim();
        RequestDetail.MaritalStatus = drpMaritalStatus.SelectedValue.Trim();

        if (drpExisitingMember.SelectedIndex == 0)
        {
            RequestDetail.IsIndDep = true;
        }
        else
        {
            RequestDetail.IsIndDep = false;
        }

        if (!string.IsNullOrEmpty(drpMemberType.SelectedValue) && drpMemberType.SelectedValue == "E")
        {
            RequestDetail.IsIndDep = false;
        }

        //Exception
        if (!string.IsNullOrEmpty(txtPrevmembership.Text))
        {
            RequestDetail.PrevMembership = txtPrevmembership.Text.Trim();
        }
        if (!string.IsNullOrEmpty(txtSponsor.Text))
        {
            RequestDetail.SecSponsorId = txtSponsor.Text.Trim();
            RequestDetail.IsMainMemeber = false;
        }
        else
        {
            RequestDetail.IsMainMemeber = true;
        }
        if (!string.IsNullOrEmpty(RequestDetail.IDExpiry))
        {
            RequestDetail.IsYakeen = true;
        }
        else
        {
            RequestDetail.IsYakeen = false;
        }
        //Child

        //if (drpChildAge.SelectedItem.Value == "1")
        //{
        //    RequestDetail.IsYakeen = false;
        //}
        //else
        //{
        //    RequestDetail.IdNumber = txtID.Text.Trim();
        //    RequestDetail.IdType = drpIdType.SelectedValue.Trim();
        //}

        if (string.IsNullOrEmpty(txtID.Text.Trim()))
        {
            RequestDetail.IsYakeen = false;
        }
        else
        {
            RequestDetail.IdNumber = txtID.Text.Trim();
            RequestDetail.IdType = drpIdType.SelectedValue.Trim();
        }


        RequestDetail.IdType = drpIdType.SelectedValue.Trim();

        if (!string.IsNullOrEmpty(RequestDetail.IDExpiry))
        {

            if (RequestDetailService.IsRequestExist("", requestId, txtName.Text.Trim()))
            {

                isExist = true;
            }

        }

        if (!isExist)
        {
            RequestDetail.RequestId = requestId;
            var reqId = RequestDetailService.Add(RequestDetail);
        }
        else
        {
            ShowError = "Member already exists in request.";
        }



    }
    private void DeleteRequest(int id)
    {
        RequestDetailService = new UnifiedRequestDetailService();
        RequestDetailService.Remove(id);
        BindRequest(requestId);
    }
    private void EnableAllControlls()
    {
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            //drplistSponsor.Enabled = true;
            //drplistSponsor.SelectedIndex = 0;
            txtSopnserNew.Enabled = true;
            txtSopnserNew.Visible = true;
            drplistSponsor.Visible = false;            
            rfvtxtID.Enabled = false;
        }
        else
        {
            txtSopnserNew.Visible = false;
            drplistSponsor.Visible = true;
            drplistSponsor.Enabled = true;
            drplistSponsor.SelectedIndex = 0;           
            rfvtxtID.Enabled = true;
        }
        txtSponsor.Enabled = true;
        txtSponsor.Text = "";
        drpCover.Enabled = true;
        drpCover.SelectedIndex = 0;
        drpIdType.Enabled = true;
        drpIdType.SelectedIndex = 0;
        drpChildAge.Enabled = true;
        drpChildAge.SelectedIndex = 0;
        txtID.Enabled = true;
        txtID.Text = "";
        txtHYearOfBirth.Enabled = true;
        txtHYearOfBirth.Text = "";
        txtExpDate.Enabled = true;
        txtExpDate.Text = "";
        drpMaritalStatus.Enabled = true;
        drpMaritalStatus.SelectedIndex = 0;
        drpTitle.Enabled = true;
        drpTitle.SelectedIndex = 0;
        drpGender.Enabled = true;
        drpGender.SelectedIndex = 0;
        txtName.Enabled = true;
        txtName.Text = "";
        txtDOB.Enabled = true;
        txtDOB.Text = "";
        drpProfession.Enabled = true;
        drpProfession.SelectedIndex = 0;
        drpDistrict.Enabled = true;
        drpDistrict.SelectedIndex = 0;
        txtMobileNumber.Enabled = true;
        txtMobileNumber.Text = "";
        drpBranchCode.Enabled = true;
        drpBranchCode.SelectedIndex = 0;

        //Removed txtarriaval date
        //txtArivalDate.Enabled = true;
        //txtArivalDate.Text = "";

        txtJoinDate.Enabled = true;
        txtJoinDate.Text = "";
        txtStartOfPolicy.Enabled = true;
        txtStartOfPolicy.Text = "";
        txtEmpNumber.Enabled = true;
        txtEmpNumber.Text = "";
        txtDepart.Enabled = true;
        txtDepart.Text = "";
        drpPreviousMember.Enabled = true;
        drpPreviousMember.SelectedIndex = 0;
        txtPrevmembership.Enabled = true;
        txtPrevmembership.Text = "";
        drpJoinReason.Enabled = true;
        drpJoinReason.SelectedIndex = 0;
        drpNationality.Enabled = true;
        drpNationality.SelectedIndex = 0;
        //drpMemberType.Enabled = true;
        drpMemberType.SelectedIndex = 0;
        btnNextEmp.Visible = true;
        btnCancelEmp.Visible = true;
        drpExisitingMember.SelectedIndex = 0;
        btnAddEmpYakeen.Visible = true;
        btnAddEmpYakeen.Enabled = true;


    }
    enum Form : int { ShowRequest = 1, ShowMainForm = 2, ShowYakeenInfo = 3, ShowError = 4, ShowExistingMember = 5 }

    private void ShowStep(Form F)
    {
        switch (F)
        {
            case Form.ShowError:
                ShowRequest = "none";
                ShowMainForm = "none";
                ShowYakeenInfo = "none";
                ShowError = "";
                ShowExistingMember = "none";
                break;
            case Form.ShowRequest:
                ShowRequest = "";
                ShowMainForm = "none";
                ShowYakeenInfo = "none";
                ShowError = "none";
                ShowExistingMember = "none";
                break;
            case Form.ShowMainForm:
                ShowRequest = "none";
                ShowMainForm = "";
                ShowYakeenInfo = "none";
                ShowError = "none";
                ShowExistingMember = "none";
                break;
            case Form.ShowExistingMember:
                ShowRequest = "none";
                ShowMainForm = "none";
                ShowYakeenInfo = "none";
                ShowError = "none";
                ShowExistingMember = "";
                break;
            case Form.ShowYakeenInfo:
                ShowRequest = "none";
                ShowMainForm = "none";
                ShowYakeenInfo = "";
                ShowError = "none";
                ShowExistingMember = "none";
                break;
            default:
                ShowRequest = "";
                ShowMainForm = "none";
                ShowYakeenInfo = "none";
                ShowError = "none";
                ShowExistingMember = "none";
                break;
        }


    }

    private void BindEmployeeType(string coverType)
    {
        var items = new List<ListItem>();
        var response = CaesarManager.GetMemberTypeByCover(Convert.ToInt32(coverType), hdnEmployeeType.Value, contractNumber);
        List<ContMbrTypeListDetail_DN> result = new List<ContMbrTypeListDetail_DN>();
        if (response.status == "0")
        {
            if (hdnEmployeeType.Value == "D")
            {
                if (FilterMemberTypeByContacts(response.detail) != null)
                {
                    result = FilterMemberTypeByContacts(response.detail);
                    var item = result.Select(t => new ListItem
                    {
                        Text = t.mbrTypeDesc,
                        Value = t.mbrType
                    });
                    items = item.ToList();
                }

            }
            else
            {
                result = response.detail.ToList();
                var item = result.Select(t => new ListItem
                {
                    Text = t.mbrTypeDesc,
                    Value = t.mbrType
                });
                items = item.ToList();

            }
        }
        drpMemberType.DataValueField = "Value";
        drpMemberType.DataTextField = "Text";
        drpMemberType.DataSource = items;
        drpMemberType.DataBind();
        drpMemberType.Items.Insert(0, new ListItem("Select Member Type", ""));
    }

    private static string RemoveSpecialChar(string str)
    {

        if (!string.IsNullOrWhiteSpace(str))
        {
            str = str.Trim();
            var result = Regex.Replace(str, @"[^0-9a-zA-Z]+", " ");
            return Regex.Replace(result, @"\s+", " ");
        }
        return str;

    }
}

//public enum EmployeeType
//{
//    Employee,
//    Dependent
//}

//public enum IdType
//{
//    Saudi,
//    NonSaudi
//}
