﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bupa.OSWeb.Helper;

public partial class Client_reimbursementclaims : System.Web.UI.Page
{
    public string _UploadCategory = UploadCategory.Submit_Reimbursement;
    public string _Username = "Unknown Sender";
    public string _InitialRequestID = string.Empty;
    string strClientID;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    public string _urlValues = string.Empty;
    public string _containerValue;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (Session["_InitialRequestID"] != null)
                _InitialRequestID = Convert.ToString(Session["_InitialRequestID"]);
        }
        else
        {
            
            _InitialRequestID = WebPublication.GenerateUniqueID;
            Session["_InitialRequestID"] = _InitialRequestID;
        }   
    }
    protected void btnRequest_Click(object sender, EventArgs e)
    {
        string _Valid = "";
        _Valid = CheckMemberExist(txtmembership.Text.Trim());
        if (_Valid == "Exist")
        {
            Uploaderlink.Visible = true;
            
            lblMsg.Text = "";
        }
        else
        {
            Uploaderlink.Visible = false;
           
            lblMsg.Text = _Valid;
        }
        
    }
    private string CheckMemberExist(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ChkMbrExistRequest_DN request;
        OS_DXC_WAP.CaesarWS.ChkMbrExistResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ChkMbrExistRequest_DN();
            request.membershipNo = MemberNo;
            request.iqamaNo = "";
            request.totalNoOfFamily = ""; 
            request.transactionID = TransactionManager.TransactionID();

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ChkMbrExist(request);

            if (response.memberName != null)
            {
                
                return "Exist";
            }
            else
            {
                return response.errorMessage[0];
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
   
}