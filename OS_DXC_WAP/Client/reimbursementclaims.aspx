﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Client_reimbursementclaims" Codebehind="reimbursementclaims.aspx.cs" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register src="../Uploader.ascx" tagname="Uploader" tagprefix="uc1" %>

<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">

<script type="text/javascript">
    function child_openurl(url) {

        popupWindow = window.open(url, "_blank", 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');

    }

</script>
    <table style="width:500px">
        <tr>
            <td>
                <h2>Submit reimbursement claims</h2>
            </td>
        </tr>
        <tr>
            <td>
                <a href="../Docs/Bupa-Claim-Form-1Feb2011.pdf">Download Claim Form</a></td>
        </tr>
        <tr>
            <td>
                In submitting the reimbursement claim, please ensure claim form is completely 
                filled with accurate information. You may also submit supporting document like 
                Invoices, medical report, lab results and others.</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td  align=right>Membership Number:</td>
                        <td align="left">
                            <dx:ASPxTextBox ID="txtmembership" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left">
                            <dx:ASPxButton ID="btnRequest" runat="server" Text="Get Details" 
                                onclick="btnRequest_Click">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" runat="server" id="Uploaderlink" visible="false" align="center">
                            <%--<a href="#" onclick="child_openurl('../SimpleUpload.aspx?UploadCategory=<%=_UploadCategory %>&UserID=<%=_Username%>&RequestID=<%=_InitialRequestID %>');">Please Click here to upload Documents</a>--%>

                            <%
                                _urlValues = "UploadCategory=" + _UploadCategory + "&UserID="+_Username+"&RequestID="+_InitialRequestID+"";
                                _urlValues = Cryption.Encrypt(_urlValues);

                             %>
                            <a href="#" onclick="child_openurl('../UploadRevamp.aspx?val=<%=_urlValues%>');">Please Click here to upload Documents</a>

                        </td>
                    </tr>
                    <tr>
                        <td style="width: 156px">&nbsp;</td>
                        <td align="right">
                            &nbsp;</td>
                        <td align="right">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center">
                        
                <asp:Label ID="lblMsg" runat="server" Text="" 
                    style="color: #FF0000; font-weight: 700"></asp:Label>
           
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <uc2:OSNav ID="OSNav1" runat="server" />
                        </td>
                    </tr>
                </table></td>
        </tr>
        </table>
</asp:Content>

