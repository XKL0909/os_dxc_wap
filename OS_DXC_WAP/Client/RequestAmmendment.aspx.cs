﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using log4net;
using Bupa.Core.Logging;
using Utility.Configuration;
using Bupa.OSWeb.Helper;
using Bupa.OSWeb.Business;

namespace Bupa.OSWeb.Client
{
    public partial class RequestAmmendment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Logger.Tracer.WriteMemberEntry();
            try
            {

            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
            }
            Logger.Tracer.WriteMemberExit();
        }

        private void SetIDNumberValidation()
        {
            //switch (ddlEIDType.SelectedValue)
            //{
            //    case "1":
            //        txtSaudiIqamaIDNumber.MaxLength = 10;
            //        rfvSuadiID.ValidationExpression = "[1][0-9]{9}";
            //        rfvSuadiID.Text = "Incorrect National ID";
            //        BindDdlENationality(false);
            //        break;
            //    case "2":
            //        txtSaudiIqamaIDNumber.MaxLength = 10;
            //        rfvSuadiID.ValidationExpression = "[2][0-9]{9}";
            //        rfvSuadiID.Text = "Incorrect Iqama ID";
            //        BindDdlENationality(false);
            //        break;
            //    case "3":
            //        txtSaudiIqamaIDNumber.MaxLength = 15;
            //        rfvSuadiID.ValidationExpression = "([A-Z]|[a-z]|[0-9]){6,15}";
            //        rfvSuadiID.Text = "Incorrect Passport Number";
            //        BindDdlENationality(true);
            //        break;
            //    case "4":
            //        txtSaudiIqamaIDNumber.MaxLength = 10;
            //        rfvSuadiID.ValidationExpression = "[3-5][0-9]{9}";
            //        rfvSuadiID.Text = "Incorrect Entry Number";
            //        BindDdlENationality(false);
            //        break;
            //}
        }

        // ideally you should keep this into any utility class and call it from the page_load event
        public static string DisableTheButton(Control pge, Control btn)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') {");
            sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
            sb.Append("this.value = 'Please wait...';");
            sb.Append("this.disabled = true;");
            sb.Append(pge.Page.GetPostBackEventReference(btn));
            sb.Append(";");
            return sb.ToString();
        }


    }
}