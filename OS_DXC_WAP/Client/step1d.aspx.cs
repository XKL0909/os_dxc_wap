﻿using Caesar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;

public partial class step1d : System.Web.UI.Page
{
    private static string contractNumber;
    private long reqDetailID;

    private static UnifiedPolicy.Services.AddMemberRequestDetailService _memberService;

    public step1d() : this(new AddMemberRequestDetailService())
    {

    }


    public step1d(AddMemberRequestDetailService memberService)
    {
        _memberService = memberService;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        txtID.Focus();
        contractNumber = Convert.ToString(Session["ClientID"]);
        
        if (HttpContext.Current.Session["AddMember"] != null)
        {

            BindExistingValue();
        }
        if (!IsPostBack)
        {
            if (drpMAinEMployee.Items.Count == 0)
            {

                AddMemberRequestDetailService newObj = new AddMemberRequestDetailService();
                List < AddMemberRequestDetail > getmain = null;
                getmain = newObj.GetMainEMployeeID('E', HttpContext.Current.Session["RequestID"].ToString());

                if (getmain != null )
                {
                    if (getmain.Count != 0)
                    {
                        drpMAinEMployee.DataSource = getmain;
                        drpMAinEMployee.DataValueField = "IdNumber";
                        drpMAinEMployee.DataTextField = "IdNumber";
                        drpMAinEMployee.DataBind();
                        drpMAinEMployee.Items.Insert(0, new ListItem("Select ...", ""));
                        drpMAinEMployee.SelectedValue = "";
                    }
                    else {
                        drpMAinEMployee.Enabled = false;
                        drpMAinEMployee.Items.Insert(0, new ListItem("No Emloyee in DB ...", ""));
                    }

                }
                else {
                    drpMAinEMployee.Enabled = false;
                    drpMAinEMployee.Items.Insert(0, new ListItem("No Emloyee in DB ...", ""));
                }


            }
            Init();
        }
    }

    public void Init()
    {

    }

    [WebMethod(EnableSession = true)]
    private static int DeleteFiles(int id)
    {
        var FileUploadService = new AddMemberFileuploadService();
        var result = FileUploadService.Remove(id);
        return result;
    }


    [WebMethod(EnableSession = true)]
    public static int InitSession()
    {
        HttpContext.Current.Session["ReqDetailID"] = null;
        HttpContext.Current.Session["Members"] = null;
        return 1;
    }

   
    [WebMethod(EnableSession = true)]
    public static long AddMemberDetails(AddMemberRequestDetail member)
    {
        long result = 0;
        HttpContext.Current.Session["AddMember"] = null;
        if (HttpContext.Current.Session["AddMember"] == null)
        {
            member.Status = 1;
            if (member.NewBorn != true)
                member.IdType = GeneralHelper.GetIdTypeFromIdNumber(member.IdNumber.Trim());
            member.ContNumber = contractNumber;
            //result = _memberService.Add(member);

            HttpContext.Current.Session.Add("AddMember", member);
        }

        return result;
    }
    private void BindExistingValue()
    {
        // var member = _memberService.FindByID(reqDetailID);

        var member = (AddMemberRequestDetail)HttpContext.Current.Session["AddMember"];
        if (!(string.IsNullOrEmpty(member.MbrshipNo)))
        {
            txtBupaMembershipNumber.Text = member.MbrshipNo;
        }
        if (!(string.IsNullOrEmpty(member.SecSponsorId)))
        {
            drpMAinEMployee.SelectedValue = member.SecSponsorId;
        }

        if (!(string.IsNullOrEmpty(member.SecSponsorId)))
        {
            txtID.Text = member.IdNumber;
        }
        

        txtHYearOfBirth.Text = member.HijriDOBYear.ToString();

        if (member.HijriDOBYear != null)
        {
            reqValtxtHYearOfBirth.Enabled = true;
        }
        else
        {
            reqValtxtHYearOfBirth.Enabled = false;
        }
    }
}