﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using System.Collections;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;

public partial class Client_dvgrid : System.Web.UI.Page
{
    DataSet ds = null;
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
        }
        catch
        {

        }

        if (!IsPostBack && !IsCallback)
        {
            ds = new DataSet();
            DataTable masterTable = new DataTable();
            masterTable.Columns.Add("ID", typeof(int));
            masterTable.Columns.Add("Data", typeof(string));
            masterTable.PrimaryKey = new DataColumn[] { masterTable.Columns["ID"] };

            DataTable detailTable = new DataTable();
            detailTable.Columns.Add("ID", typeof(int));
            detailTable.Columns.Add("MasterID", typeof(int));
            detailTable.Columns.Add("Data", typeof(string));
            detailTable.PrimaryKey = new DataColumn[] { detailTable.Columns["ID"] };
            int index = 0;
            for (int i = 0; i < 20; i++)
            {
                masterTable.Rows.Add(new object[] { i, "Master Row " + i.ToString() });
                for (int j = 0; j < 5; j++)
                    detailTable.Rows.Add(new object[] { index++, i, "Detail Row " + j.ToString() });
            }
            ds.Tables.AddRange(new DataTable[] { masterTable, detailTable });
            Session["DataSet"] = ds;
        }
        else
            ds = (DataSet)Session["DataSet"];
        ASPxGridView1.DataSource = ds.Tables[0];
        ASPxGridView1.DataBind();
    }
    protected void ASPxGridView2_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ds = (DataSet)Session["DataSet"];
        DataTable detailTable = ds.Tables[1];
        DataView dv = new DataView(detailTable);
        ASPxGridView detailGridView = (ASPxGridView)sender;
        dv.RowFilter = "MasterID = " + detailGridView.GetMasterRowKeyValue().ToString();
        detailGridView.DataSource = dv;
    }
    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ds = (DataSet)Session["DataSet"];
        ASPxGridView gridView = (ASPxGridView)sender;
        DataTable dataTable = gridView.SettingsDetail.IsDetailGrid ? ds.Tables[1] : ds.Tables[0];
        DataRow row = dataTable.Rows.Find(e.Keys[0]);
        IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();
        enumerator.Reset();
        while (enumerator.MoveNext())
            row[enumerator.Key.ToString()] = enumerator.Value;
        gridView.CancelEdit();
        e.Cancel = true;
    }
    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ds = (DataSet)Session["DataSet"];
        ASPxGridView gridView = (ASPxGridView)sender;
        DataTable dataTable = gridView.SettingsDetail.IsDetailGrid ? ds.Tables[1] : ds.Tables[0];
        DataRow row = dataTable.NewRow();
        e.NewValues["ID"] = GetNewId();
        IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();
        enumerator.Reset();
        while (enumerator.MoveNext())
            if (enumerator.Key.ToString() != "Count")
                row[enumerator.Key.ToString()] = enumerator.Value;
        gridView.CancelEdit();
        e.Cancel = true;
        dataTable.Rows.Add(row);
    }

    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        int i = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys[ASPxGridView1.KeyFieldName]);
        Control c = ASPxGridView1.FindDetailRowTemplateControl(i, "ASPxGridView2");
        e.Cancel = true;
        ds = (DataSet)Session["DataSet"];
        ds.Tables[0].Rows.Remove(ds.Tables[0].Rows.Find(e.Keys[ASPxGridView1.KeyFieldName]));

    }
    private int GetNewId()
    {
        ds = (DataSet)Session["DataSet"];
        DataTable table = ds.Tables[0];
        if (table.Rows.Count == 0) return 0;
        int max = Convert.ToInt32(table.Rows[0]["ID"]);
        for (int i = 1; i < table.Rows.Count; i++)
        {
            if (Convert.ToInt32(table.Rows[i]["ID"]) > max)
                max = Convert.ToInt32(table.Rows[i]["ID"]);
        }
        return max + 1;
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

}