﻿using Bupa.Core.Logging;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Linq;
using Telerik.Web.UI;
using OnlineServices.CCHI.Dto;
using OnlineServices.CCHI.Services;
using System.Collections;
using OS_DXC_WAP.CaesarWS;

public partial class CCHICap : System.Web.UI.Page
{
    private string clientID;
    private ServiceDepot_DNService ws;
    private  CCHIService _service;
    public string _ServiceResult = "";
    public string _Description = "";

    private Hashtable hasQueryValue;
    private string optionType;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            CapCountDetails.Visible = true;
            DisplayError.Visible = false;
            divEmpSponsorId.Visible = true;
            clientID = Session["ClientID"].ToString();
            if (clientID != null || clientID != string.Empty)
            {
                EnqSponsorIDListDtl_DN[] sponsorIdsList = GetSponsorList(clientID);
                BindDdlSponsorList(sponsorIdsList, ddlSponsor);
            }

            hasQueryValue = new Hashtable();
            string queryStringValues = string.Empty;

            if (Request.QueryString.Count > 0)
            {
                try
                {
                    var val = Request.QueryString["val"];
                    queryStringValues = Cryption.Decrypt(val);
                    hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                    optionType = hasQueryValue.ContainsKey("optiontype") ? Convert.ToString(hasQueryValue["optiontype"]) : string.Empty;
                  
                }
                catch (Exception)
                {
                    Response.Write("Invalid rquest!");
                    return;
                }
            }
           

            string query = string.Empty;
            query = optionType;
            if (!string.IsNullOrEmpty(query))
            {
                lnkBatchUpload.Attributes.Add("href", "BatchUpload.aspx?val=" + Cryption.Encrypt("optiontype="+ optionType));
                lnkBatchUpload.Visible = true;
            }
            else
                lnkBatchUpload.Visible = false;
        }
    }

    private void BindDdlSponsorList(EnqSponsorIDListDtl_DN[] list, RadComboBox ddl)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            ListItem ListItem0 = new ListItem();
            ListItem0.Text = "-Select-";
            ListItem0.Value = "0";
            ddl.DataSource = list;
            ddl.DataBind();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
    }

    private EnqSponsorIDListDtl_DN[] GetSponsorList(string contNo)
    {
        EnqSponsorIDListRequest_DN request = new EnqSponsorIDListRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.contNo = contNo;
        ws = new ServiceDepot_DNService();
        EnqSponsorIDListResponse_DN response;
        response = ws.EnqSponsorIDList(request);

        return response.detail;
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {        
        if (!string.IsNullOrEmpty(ddlSponsor.SelectedValue) && !string.IsNullOrEmpty(txtIqama.Text))
        {
            string[] ids = new string[] { ddlSponsor.SelectedValue.ToString() };
            var sponors = GetCapCount(ids, txtIqama.Text.Trim());
            bool isfailure = sponors.Where(t => t.ErrorDetails.ServiceResult.ToString().ToLower().Contains("failure")).Count() > 0;
            if (isfailure)
            {
                CCHIResponseLog(sponors);
            }
            else
            {
                DisplayError.Visible = false;
                grdViewCaps.DataSource = sponors;
                grdViewCaps.DataBind();
            }
        }
    }
    private void CCHIResponseLog(List<Sponsor> CCHIRes)
    {
        foreach (var res in CCHIRes)
        {
            _Description = res.ErrorDetails.Description + " StatusCode:" + res.ErrorDetails.StatusCode;
            _ServiceResult = res.ErrorDetails.ServiceResult.ToString();
            CapCountDetails.Visible = false;
            DisplayError.Visible = true;
            divEmpSponsorId.Visible = false;
        }
    }
    private List<OnlineServices.CCHI.Dto.Sponsor> GetCapCount(string[] sponsorIds, string iqama)
    {       
		_service = new CCHIService();
        var caps = _service.GetCapCountByIqama(sponsorIds, iqama);
        return caps;
    }
    private List<OnlineServices.CCHI.Dto.Sponsor> GetCapCount(string[] sponsorIds)
    {
         _service = new CCHIService();
         return _service.GetCapCount(sponsorIds);
    }
}