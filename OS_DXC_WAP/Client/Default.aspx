﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Client_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
        <div class="sectionTitle">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><h2>Membership Maintenance </h2></td>
            </tr>
        </table>
        </div>
        <div>
            <div class="userLinks">
            <table cellpadding="4" cellspacing ="1"  >
                <tr>
                    <td align=left class="boxcaption" id="tdAddEmployeeDependent" runat="server"><a href="AddMemberIntermediate.aspx"><img id="Img2" runat="server" border="0" 
                            src="~/images/icons/client/addemployee.jpg" style="height: 85px; width:70px"/></a><br />
                        <br />
                        Add Employee / Dependent </td>
                   <td align=left class="boxcaption" id="tdNewAdditionGuide" runat="server"><a href="../Docs/CCHI-unified-manual-Web-Addition.pdf" target="_newtab"><img id="Img3" runat="server" border="0" 
                            src="~/images/icons/common/RequiredDocument.jpg" style="height: 85px; width:70px"/></a><br />
                        <br />
                        New Addition Guide </td>
                    <td align=left class="boxcaption" id="tdChangeclass" runat="server">
                        <br />
                        <a href="#" runat="server" id="aMChangeBranch"><img id="Img4" 
                            runat="server" border="0" src="~/images/icons/client/changeclass.jpg" style="height: 85px; width:70px"/></a><br />
                        Change class</td>
                    <td align=left class="boxcaption"><a href="trackinfo.aspx" id="tdTrackinfo" runat="server"><img id="Img5" runat="server" border="0" 
                            src="~/images/icons/client/TrackInfo.jpg" style="height: 85px; width:70px"/></a><br />
                        <br />
                        Track info</td>
                    <td align=left class="boxcaption" id="tdChangeBranch" runat="server"><a href="changebranch.aspx"><img id="Img11" runat="server" border="0" src="~/images/icons/client/changebranch.jpg" style="height: 85px; width:70px" alt="" /></a><br />
                        Change Branch </td>
                    <td align=left class="boxcaption" id="tdReplaceCard" runat="server"> <a href="cardreplace.aspx    "><img id="Img12" runat="server" border="0" src="~/images/icons/client/ChangeCard.jpg" style="height: 85px; width:70px" alt="" /></a><br />
                        Replace Card Data Correction</td>
                    <td align=left class="boxcaption"  id="tdDeleteEmployee" runat="server"><a href="DeleteEmployeeEnhanced.aspx">
                        <img id="Img13" runat="server" alt="" border="0" 
                            src="~/images/icons/client/DeleteEmployee.jpg" 
                            style="height: 85px; width:70px" /></a><br />
                        Delete Employee</td>
                    <td align=left class="boxcaption" id="tdDeleteDependent" runat="server"><a href="#" id="aMDeleteDependent" runat="server">
                        <img id="Img22" runat="server" alt="" border="0" 
                            src="~/images/icons/client/Deletedependent.jpg" 
                            style="height: 85px; width:70px" /></a><br />
                        Delete Dependent </td>
                </tr>
                <tr id="trGsAdditionalDetails" runat="server">
                    <td align=left  class="boxcaption"><a href="BatchUploadReqDocs.aspx" target="_blank"><img id="Img15" runat="server" border="0" 
                            src="~/images/icons/common/RequiredDocument.jpg" style="height: 85px; width:70px"
                            /></a><br />
                        Submit Required 
                       
                        Supporting
                        Document </td>
						 <td align=left  class="boxcaption"><a href="CCHICap.aspx" target="_self"><img id="Img1" runat="server" border="0" 
                            src="~/images/icons/common/RequiredDocument.jpg" style="height: 85px; width:70px"
                            /></a><br />
                        CCHI CAP </td>
                </tr>
                </table></div>
            <div class="sectionTitle">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><h2>Member Schemes </h2></td>
            </tr>
        </table>
        </div>
            <div class="userLinks">

        <table cellpadding="4" cellspacing ="1"  id="tblGsMemberDetails" runat="server"  >
                <tr>
                    <td   align=left  class="boxcaption"><a href="ctntMydetails.aspx"><img id="Img7" runat="server" 
                            border="0" src="~/images/icons/member/My Details.jpg" style="height: 85px; width:70px" 
                            /></a><br />
                        My Details </td>
                    <td  align=left  class="boxcaption"><a href="ctntmycoverage.aspx"><img id="Img8" runat="server" 
                            border="0" src="~/images/icons/member/My Coverage.jpg" style="height: 85px; width:70px" 
                            /></a><br />
                        My Coverage  </td>
                    <td  align=left  class="boxcaption"><a href="ctntmynetwork.aspx"><img id="Img16" runat="server" border="0" 
                            src="~/images/icons/member/My Network.jpg" style="height: 85px; width:70px"
                            /></a><br />
                        My Network </td>
                    <td  align=left  class="boxcaption"><a href="#" id="aMStatement" runat="server"><img id="Img17" 
                            runat="server" border="0"  style="height: 85px; width:70px"
                            src="~/images/icons/common/AccountStatement.jpg" 
                            /></a><br />
                        Account Statements </td>
                    <td  align=left  class="boxcaption">
                        <a href="#" id="aMInvoice" runat="server">
                        <img id="Img18" runat="server" border="0" 
                            src="~/images/icons/common/OnlineInvoice.jpg" 
                            style="height: 85px; width:70px" /></a><br />
                        Online Invoice</td>
                </tr>
                </table>
            
            

        </div>
            <div class="sectionTitle">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><h2>History and Other Services </h2></td>
            </tr>
        </table>
        </div>
            <div class="userLinks">
            
            <table cellpadding="4" cellspacing ="1"  >
                <tr>
                    <td align=left class="boxcaption"><a href="ctntclaimHistory.aspx"><img id="Img10" runat="server" 
                            border="0" src="~/images/icons/common/ClaimHistory.jpg" style="height: 85px; width:70px"/></a><br />
                        <br />
                        Claim History </td>
                    <td align=left class="boxcaption"><a href="ctntPreauthhistory.aspx">
                        <img id="Img19" runat="server" border="0" 
                            src="~/images/icons/common/Pre-authhistory.jpg" style="height: 85px; width:70px"/></a><br />
                        <br />
                        Preauth History  </td>
                    <td align=left class="boxcaption"><a href="CtntMembershipCert.aspx" style="display:block"><img id="Img20" 
                            runat="server" border="0" 
                            src="~/images/icons/member/Membership Certificate.jpg" style="height: 85px; width:70px"/></a><br />
                        Certificate</td>
                    <td align=left class="boxcaption"><a href="reimbursementclaims.aspx"><img id="Img21" runat="server" 
                            border="0" src="~/images/icons/common/ClaimHistory.jpg" 
                            style="height: 85px; width:70px"/></a><br />
                        Submit reimbursement</td>
                </tr>
                </table>
        
        </div>
        </div>
        </td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p>
        Our online services allow the group secretary to view profiles and policy 
        benefits of your staff, print membership certificates, view claims and 
        pre-authorisation history of your staff, submit membership maintenance requests, 
        view and print account invoices and
    </p>
</asp:Content>

