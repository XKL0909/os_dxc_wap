﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="memberdocuments" Codebehind="memberdocuments.aspx.cs" %>
<script src="js/memberdocuments.js"></script>
<form id="form1" runat="server">

                <asp:Repeater ID="RptUpload" runat="server" >
                    <HeaderTemplate>
                        <table class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                   <%-- <th>Image</th>--%>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                         </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# Eval("FullName")%></td>
                           <%-- <td>image</td>--%>
                            <td>
                                 <button onclick="deleteFiles(<%#Eval("Id")%>)" class="btn btn-default btn-sm" type="button">Delete</button>
                            </td>
                        </tr>
                     </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"><asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0?true:false %>' Text="No item(s) found" /></td>
                            </tr>
                        </tfoot>
                       </table>
                    </FooterTemplate>
                </asp:Repeater>
 
    </form>
