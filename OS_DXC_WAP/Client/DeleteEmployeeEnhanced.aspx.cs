﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using log4net;
///using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;
using Bupa.OSWeb.Helper;
using Bupa.OSWeb.Business;
using Bupa.Core.Logging;
using System.Configuration;
using System.Text;
using System.Collections;

namespace Bupa.OSWeb.Client
{
    public partial class DeleteEmployeeEnhanced : System.Web.UI.Page
    {
        #region Fields

        private static ILog _log = LogManager.GetLogger(typeof(DeleteEmployeeEnhanced));

        // The upload category for the uploading of this page
        private string _uploadCategory = UploadCategory.MEMBER_DELEMP;
        private string _contractNumber = string.Empty;
        private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
        private string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();

        private string employeeType = string.Empty, queryStringValues = string.Empty;
        private Hashtable hasQueryValue;
        #endregion

        private void BindDdlProfession()
        {
            OS_DXC_WAP.CaesarWS.ReqProfListRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqProfListRequest_DN();
            //request.TransactionID = TransactionManager.TransactionID();
            request.transactionID = WebPublication.GenerateTransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            OS_DXC_WAP.CaesarWS.ReqProfListResponse_DN response;
            response = ws.ReqProfList(request);
            ddProfession.DataSource = response.detail;
            ddProfession.DataBind();
        }

        private void BindDdlDistrict()
        {
            OS_DXC_WAP.CaesarWS.ReqDistListRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqDistListRequest_DN();
            //request.TransactionID = TransactionManager.TransactionID();
            request.transactionID = WebPublication.GenerateTransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            OS_DXC_WAP.CaesarWS.ReqDistListResponse_DN response;
            response = ws.ReqDistList(request);
            ddDistrict.DataSource = response.detail;
            ddDistrict.DataBind();
        }

        private void BindDdlENationality(bool IsGCC)
        {
            NationalityControl MyNationalityC = new NationalityControl();
            List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
            MyNationalities.Insert(0, new Nationality(0, "", "- Select -", false));
            ddlENationality.DataValueField = "Code";
            ddlENationality.DataTextField = "Nationality1";
            ddlENationality.DataSource = MyNationalities;
            ddlENationality.DataBind();
            //ddlENationality.Items.Insert(0,new ListItem("- Select -",""));
        }

        protected void ddProfession_DataBound(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddProfession.Items.Insert(0, new ListItem("-select-", ""));
                ddProfession.SelectedIndex = 0;
            }
        }

        protected void ddDistrict_DataBound(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddDistrict.Items.Insert(0, new ListItem("-select-", ""));
                ddDistrict.SelectedIndex = 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Logger.Tracer.WriteMemberEntry();
            try
            {
                this.btnSubmit.Attributes.Add("onclick", DisableTheButton(this.Page, this.btnSubmit));

                if (Request.QueryString.Count > 0)
                {
                    try
                    {
                        var val = Request.QueryString["val"];
                        queryStringValues = Cryption.Decrypt(val);
                        hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                        employeeType = hasQueryValue.ContainsKey("EType") ? Convert.ToString(hasQueryValue["EType"]) : string.Empty;
                       
                    }
                    catch (Exception ex)
                    {
                        lblError.Text = "Invalid rquest!";
                        return;
                    }

                }
               

                lblError.Text = "";

                SetupUploader(_uploadCategory);

                ////if (Request["EType"] == "Dep")
                if (employeeType == "Dep") 
                {
                    lblDelEmp.Text = "Delete Dependent";
                    lnkBatchUpload.NavigateUrl = "~/Client/BatchUpload.aspx?val=" + Cryption.Encrypt("optiontype=deletedependent");
                }
                else
                    lnkBatchUpload.NavigateUrl = "~/Client/BatchUpload.aspx?val=" + Cryption.Encrypt("optiontype=deleteemployee");
                if (Session["ClientID"] != null)
                    _contractNumber = Session["ClientID"].ToString();
               // Tracer.WriteLine(ref _log, "_contractNumber is: " + _contractNumber);

                if (!Page.IsPostBack)
                {
                    DivStafflbl.Style.Add("display", "none");
                    DivStafftxt.Style.Add("display", "none");
                    lblAramcoMessage.Visible = false;
                    Label2.Visible = true;
                    // Hide the submission buttons on a fresh load
                    ToggleSubmissionOptions(false);
                    BindDdlProfession();
                    BindDdlDistrict();
                    BindDdlENationality(false);
                    // Set the date of the calendar to default to today
                    //deEffectiveDate.Date = DateTime.Today;
					//Aramco PId Changes by Hussamuddin
                    if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                    {
                        DivStafflbl.Style.Add("display", "block");
                        DivStafftxt.Style.Add("display", "block");
                        lblAramcoMessage.Visible = true;
                        Label2.Visible = false;
                        LblChangeBranchBanner.InnerText = "Whether your request is to add members, delete members, change scheme, change branch or replace members cards, you may submit your transaction at your convenience.";
                        lblDelEmp.Text = "Delete Member";
                        IDType.Visible = false;
                        MaritalStatus.Visible = false;
                        SaudiIDIqma.Visible = false;
                        IDExpiryDate.Visible = false;
                        Profession.Visible = false;
                        District.Visible = false;
                        Nationality.Visible = false;
                        MobileNo.Visible = false;
                        tblUploader.Visible = false;
                        rfvSuadiID.Enabled = false;
                        RegEMobileNo.Enabled = false;

                    }
                    else
                        RFVtxtBadgeNo.Enabled = false;

                }
                DataBind();
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
            }
            Logger.Tracer.WriteMemberExit();
        }

        protected void Page_LoadComplete(object sender, System.EventArgs e)
        {
            // Enable / Disable the submission option depending on whether the user 
            // has uploaded supported documents

            // Assume the button is disabled until checked
            btnSubmit.Enabled = false;

            // Get what has been uploaded
            DataSet uploadedFileSet = uploader.UploadedFileSet;


            UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);

            //if (_uploadmanger.WebIndecator("SuportDocs", _contractNumber))
            //{
            if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType){
                tblUploader.Visible = false;
                btnSubmit.Enabled = true;
            }
            else { 
                tblUploader.Visible = true;

                int uploadedCount = 0;
                if (uploadedFileSet != null && uploadedFileSet.Tables.Count > 0)
                {
                    uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                    if (uploadedCount > 0)
                    {
                        btnSubmit.Enabled = true;
                    }
                    else
                    {
                        btnSubmit.Enabled = false;
                    }

                }
            }
            //}
            //else
            //{
            //    tblUploader.Visible = false;
            //    btnSubmit.Enabled = true;

            //}


            // Uploads are not mandatory for Saudi IDs
            string saudiIqamaID = txtSaudiIqamaIDNumber.Text;
            ///Tracer.WriteLine(ref _log, "saudiIqamaID is: " + saudiIqamaID);

            // Saudi ID's begin with '1'
            if (saudiIqamaID.StartsWith("1"))
                btnSubmit.Enabled = true;

            SetIDNumberValidation();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Logger.Tracer.WriteMemberEntry();
            try
            {
                // Fetch the details
                string memberID = lblMembershipNo.Text;
                ////Tracer.WriteLine(ref _log, "memberID is: " + memberID);

                string reasonCode = ddDeleteReason.SelectedValue;
                ////Tracer.WriteLine(ref _log, "reasonCode is: " + reasonCode);

                DateTime effectiveDate = DateTime.ParseExact(txtEffectiveDate.Text, "dd/MM/yyyy", null);//deEffectiveDate.Date;
                ////Tracer.WriteLine(ref _log, "effectiveDate is: " + effectiveDate.ToString("dd/MM/yyyy"));

                // Build a member object
                MemberHelper mh = new MemberHelper(memberID, _contractNumber);
                Member m = mh.GetMemberDetails();
                m.SaudiIqamaID = txtSaudiIqamaIDNumber.Text;
                m.Nationality = ddlENationality.SelectedValue;
                m.IDType = ddlEIDType.SelectedValue;
                if (txtEIDExpDate.Text != "")
                {
                    m.IDExpiryDate = DateTime.ParseExact(txtEIDExpDate.Text, "dd/MM/yyyy", null); ;
                }
                m.Profession = ddProfession.SelectedValue;
                m.District = ddDistrict.SelectedValue;
                m.MaritalStatus = ddlEMaritalStatus.SelectedValue;
                // Attempt to delete the member through Caesar
                string caesarReferenceID = mh.DeleteMember(m, reasonCode, effectiveDate, uploader.SupportDocuments(uploader.SessionID, _uploadCategory));
                ////Tracer.WriteLine(ref _log, "caesarReferenceID is: " + caesarReferenceID);

                // Send attachments (if any)
                // Send the email related to the attachments
                string username = "Unknown username";
                string fullName = "Unknown client";
                string[] addressees = UploadPublication.MembershipEmailAddresses();

                if (Session[WebPublication.Session_ClientUsername] != null)
                    username = Session[WebPublication.Session_ClientUsername].ToString();
                if (Session[WebPublication.Session_ClientFullName] != null)
                    fullName = Session[WebPublication.Session_ClientFullName].ToString();

                // Delegate to send emails
                //uploader.Notify(uploader.SessionID, _uploadCategory, caesarReferenceID, addressees, fullName, username);

                // Hide form and display successful result.
                imgSuc.Visible = true;
                lblResult.Text = "Thank you for your deletion request. The reference number is: " + caesarReferenceID;

                panelForm.Visible = false;
                lblAramcoMessage.Visible = false;
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
                lblError.Text = ex.Message;
            }
            Logger.Tracer.WriteMemberExit();
        }

        protected void btnGetMemberDetailsClick(object sender, EventArgs e)
        {
            try
            {
                string MEmberORStaff = "";
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(txtMembershipNo.Text.Trim())))
                    {
                        RFVtxtBadgeNo.Enabled = false;
                        MEmberORStaff = txtMembershipNo.Text.Trim();

                    }
                    else if (!string.IsNullOrEmpty(Convert.ToString(txtBadgeNo.Text.Trim())))
                    {
                        frvMembershipNo.Enabled = false;

                        string memberShipNumber = BLCommon.ReturnMemberShipNumberForStaffnumber(txtBadgeNo.Text.Trim(), Convert.ToString(Session["ClientID"]));
                        
                        if(memberShipNumber.Contains(":"))
                        {
                            lblError.Text =memberShipNumber;
                        }
                        else
                            MEmberORStaff=memberShipNumber;
                        //OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
                        //OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
                        //OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN();
                        //response = null;
                        //request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
                        //request.customerNo = Convert.ToString(Session["ClientID"]); 
                        //request.staffNo = txtBadgeNo.Text.Trim();
                        ////request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
                        //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
                        ////request.IqamaNo =  "5523234443";
                        ////request.TotalNoOfFamily = "3";
                        //request.Username = WebPublication.CaesarSvcUsername;
                        //request.Password = WebPublication.CaesarSvcPassword;
                        //response = ws.EnqMbrListInfo(request);
                        //StringBuilder stringBuilder = new StringBuilder(200);
                        //if (response.errorID != "0")
                        //{
                        //    stringBuilder.Append(response.errorMessage).Append("\n");
                        //    lblError.Text = "Error :" + stringBuilder.ToString();
                        //}
                        //else
                        //     MEmberORStaff = Convert.ToString(response.detail[0].membershipNo);

                    }

                }
                else
                {
                    RFVtxtBadgeNo.Enabled = false;
                    MEmberORStaff = txtMembershipNo.Text.Trim();
                }
                // Grab the memberID
                if (!string.IsNullOrEmpty(MEmberORStaff)) { 
                string membershipNumber = MEmberORStaff;
                //Logger.Tracer.WriteLine(ref _log, "membershipNumber is: " + membershipNumber);

                // Fetch the member's details
                MemberHelper mh = new MemberHelper(membershipNumber, _contractNumber);
                Member m = mh.GetMemberDetails();
                //HttpContext.Current.Response.Write(m.Nationality);
                if (Check_Member_belong_to_client(m.ContractNumber))
                    {
                        SetupUI(m);
                       

                    }
                    else
                    {
                        lblError.Text = "Inputted 'Membership No OR Badge Number' does not exist in your group.";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
                lblError.Text = ex.Message;
            }
        }

        bool Check_Member_belong_to_client(string strMemberContractNo)
        {
            if (_contractNumber == strMemberContractNo)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    private void ToggleSubmissionOptions(bool visible)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            panelResults.Visible = visible;
            txtMembershipNo.ReadOnly = visible;
            if (visible)
                txtMembershipNo.BackColor = System.Drawing.Color.Gray;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
    }

    private void SetupUI(Member memberDetails)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            ToggleSubmissionOptions(true);

            // Member Name
            lblMemberName.Text = memberDetails.MemberName;

            // Membership Number
            lblMembershipNo.Text = memberDetails.MembershipNumber;

            // Saudi / Iqama ID
            txtSaudiIqamaIDNumber.Text = memberDetails.SaudiIqamaID;
            ddlEIDType.SelectedValue = memberDetails.IDType.Trim();
            // HttpContext.Current.Response.Write(memberDetails.Nationality);
            if (memberDetails.IDExpiryDate != null)
            txtEIDExpDate.Text = DateTime.Parse(memberDetails.IDExpiryDate.ToString(), null, System.Globalization.DateTimeStyles.None).ToShortDateString();
            ProfCodeControl MyCodeC = new ProfCodeControl();
            List<string> MyCodes = MyCodeC.GetAllProfCodesAsString();
            if (MyCodes.Contains(memberDetails.Profession))
            {
                ddProfession.SelectedValue = memberDetails.Profession.Trim();
            }
            ddDistrict.SelectedValue = memberDetails.District.Trim();
            ddlENationality.SelectedValue = memberDetails.Nationality.Trim();
            txtEMobileNo.Text = memberDetails.MOBILE.Trim();
            ddlEMaritalStatus.SelectedValue = memberDetails.MaritalStatus.Trim();

            // Date of Birth
            if (memberDetails.BirthDate != null)
            lblBirthDate.Text = memberDetails.BirthDate.ToString("dd/MM/yyyy");
            SetIDNumberValidation();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
    }

    private void SetupUploader(string uploadCategory)
    {
        uploader.Visible = true;

        // By default, unless a user has uploaded supporting documents,
        // the Submit button will be disabled
        btnSubmit.Enabled = false;

        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        // Bind to allow resolving of # tags in mark-up
        DataBind();
    }

    protected void ddlEIDType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtSaudiIqamaIDNumber.Text = "";
        SetIDNumberValidation();
    }

    private void SetIDNumberValidation()
    {
        switch (ddlEIDType.SelectedValue)
        {
            case "1":
                txtSaudiIqamaIDNumber.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[1][0-9]{9}";
                rfvSuadiID.Text = "Incorrect National ID";
                BindDdlENationality(false);
                break;
            case "2":
                txtSaudiIqamaIDNumber.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[2][0-9]{9}";
                rfvSuadiID.Text = "Incorrect Iqama ID";
                BindDdlENationality(false);
                break;
            case "3":
                txtSaudiIqamaIDNumber.MaxLength = 15;
                rfvSuadiID.ValidationExpression = "([A-Z]|[a-z]|[0-9]){6,15}";
                rfvSuadiID.Text = "Incorrect Passport Number";
                BindDdlENationality(true);
                break;
            case "4":
                txtSaudiIqamaIDNumber.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[3-5][0-9]{9}";
                rfvSuadiID.Text = "Incorrect Entry Number";
                BindDdlENationality(false);
                break;
        }
    }

    // ideally you should keep this into any utility class and call it from the page_load event
    public static string DisableTheButton(Control pge, Control btn)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') {");
        sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
        sb.Append("this.value = 'Please wait...';");
        sb.Append("this.disabled = true;");
        sb.Append(pge.Page.GetPostBackEventReference(btn));
        sb.Append(";");
        return sb.ToString();
    }

    
    }
}