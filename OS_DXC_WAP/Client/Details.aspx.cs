﻿using System;

public partial class Details : System.Web.UI.Page
{
    public Details()
    {

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("../Default.aspx", true);
        }
        else
        {
            lnkUpdD.HRef = "CCHICap.aspx?val=" + Cryption.Encrypt("optiontype=adddependent");
            lnkUpdED.HRef = "CCHICap.aspx?val=" + Cryption.Encrypt("optiontype=addemployeedependent");
            if (!IsPostBack)
            {
                hdnRequestId.Value = Convert.ToString(Session["RequestID"]);
            }
        }
    }
}