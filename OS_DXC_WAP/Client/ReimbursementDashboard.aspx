﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ReimbursementDashboard" Codebehind="ReimbursementDashboard.aspx.cs" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc1" %>


<%@ Register Src="../UserControls/Header.ascx" TagName="Header" TagPrefix="uc2" %>


<%@ Register Src="../UserControls/Footer.ascx" TagName="Footer" TagPrefix="uc3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" type="text/css" href="../Styles/bupa.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/home-slider.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/help-slider.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/lightblue.css" />
    <link href="../Scripts/Bupa/MessageBar/CSS/skin.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../Styles/Impromptu.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/OSWebStyle.css" />
    <script type='text/JavaScript' src="../Scripts/scw.js"></script>
    <script language="javascript" type="text/javascript" src="../Scripts/jsDate.js"></script>
    <script src="../Scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../functions/login/functions.js" type="text/javascript"></script>
    <script src="../Scripts/Bupa/MessageBar/jquery.messagebar.js" type="text/javascript"></script>
    <!-- Impromptu -->
    <script type="text/javascript" src='../Scripts/Bupa/Impromptu/jquery-impromptu.4.0.min.js'></script>
    <title>Bupa Arabia, Online Services </title>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-4321156-5']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

    <style type="text/css">
        #form1 {
            text-align: center;
        }
    </style>
</head>
<script language="javascript" type="text/javascript">

    /****************************************Drop Down & Rollover Menu  ********************************/
    function MM_swapImgRestore() {
        var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
    }

    function MM_swapImage() {
        var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2) ; i += 3)
            if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
    }

    function MM_findObj(n, d) {
        var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n); return x;
    }

    function MM_showHideLayers() {
        var i, p, v, obj, args = MM_showHideLayers.arguments;
        for (i = 0; i < (args.length - 2) ; i += 3) if ((obj = MM_findObj(args[i])) != null) {
            v = args[i + 2];
            if (obj.style) { obj = obj.style; v = (v == 'show') ? 'visible' : (v == 'hide') ? 'hidden' : v; }
            obj.visibility = v;
        }
    }
</script>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
        <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
        <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1">
            <div style="width: 100%; float: left">
                <telerik:RadHtmlChart runat="server" ID="ReimbursementClaimsLineChart">
                    <Appearance>
                        <FillStyle BackgroundColor="White"></FillStyle>
                    </Appearance>
                    <ChartTitle Text="Claim by status and amount on date">
                        <Appearance Align="Center" BackgroundColor="White" Position="Top">
                        </Appearance>

                    </ChartTitle>
                    <Legend>
                        <Appearance BackgroundColor="White" Position="Bottom">
                        </Appearance>
                    </Legend>
                    <PlotArea>
                        <Series>
                            <telerik:ScatterLineSeries Name="Number of claims" DataFieldX="crt_date" DataFieldY="billed_amt">
                                <TooltipsAppearance Color="White" DataFormatString="{1} claims on date:  {0:d}"></TooltipsAppearance>
                                <LabelsAppearance Visible="false">
                                </LabelsAppearance>
                            </telerik:ScatterLineSeries>
                            <telerik:ScatterLineSeries AxisName="AdditionalAxisOnY" Name="Rejected claims" DataFieldX="crt_date" DataFieldY="rjClaims">
                                <TooltipsAppearance Color="White" DataFormatString="{1} rejected claims on date: {0:d}"></TooltipsAppearance>
                                <LabelsAppearance Visible="false">
                                </LabelsAppearance>
                            </telerik:ScatterLineSeries>
                            <telerik:ScatterLineSeries AxisName="AdditionalAxisOnY1" Name="Suspended claims" DataFieldX="crt_date" DataFieldY="spClaims">
                                <TooltipsAppearance Color="White" DataFormatString="{1} suspended claims on date: {0:d}"></TooltipsAppearance>
                                <LabelsAppearance Visible="false">
                                </LabelsAppearance>
                            </telerik:ScatterLineSeries>
                            <telerik:ScatterLineSeries AxisName="AdditionalAxisOnY2" Name="Approved claims" DataFieldX="crt_date" DataFieldY="acClaims">
                                <TooltipsAppearance Color="White" DataFormatString="{1} approved claims on date: {0:d}"></TooltipsAppearance>
                                <LabelsAppearance Visible="false">
                                </LabelsAppearance>
                            </telerik:ScatterLineSeries>

                        </Series>
                        <XAxis Color="Teal">
                            <LabelsAppearance DataFormatString="d" />
                            <TitleAppearance Text="Month" />
                        </XAxis>
                        <YAxis Color="Navy">
                            <LabelsAppearance DataFormatString="{0}" />
                            <TitleAppearance Text="Value" />
                        </YAxis>
                        <AdditionalYAxes>
                            <telerik:AxisY Name="AdditionalAxisOnY" Color="Green" Width="3" Visible="false">
                                <TitleAppearance Text="Rejected" />
                            </telerik:AxisY>
                             <telerik:AxisY Name="AdditionalAxisOnY1" Color="Navy" Width="3" Visible="false">
                                <TitleAppearance Text="Suspended" />
                            </telerik:AxisY>
                             <telerik:AxisY Name="AdditionalAxisOnY2" Color="Teal" Width="3" Visible="false">
                                <TitleAppearance Text="Approved" />
                            </telerik:AxisY>

                        </AdditionalYAxes>
                    </PlotArea>
                </telerik:RadHtmlChart>
                
                <telerik:RadGrid Width="100%" ID="ReimbursementClaimDetailsGrid" runat="server" FilterMenu-Enabled="true" AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" AllowFilteringByColumn="True" ShowGroupPanel="True">
                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                        <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                    </ClientSettings>
                </telerik:RadGrid>

            </div>
        </telerik:RadAjaxPanel>
    </form>
</body>
</html>
