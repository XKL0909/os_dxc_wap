﻿

function deleteFiles(id) {
    $.ajax({
        type: "POST",
        url: "memberdocuments.aspx/DeleteMemberFiles",
        data: "{id:" + id + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });

    function OnSuccess(response) {
        var result = response.d;
        if (result > 0) {
            loadFiles();
        }
    }
}
function loadFiles() {

    $.ajax({
        type: "POST",
        url: "memberdocuments.aspx",
        success: function (data) {
            $("#gridFileUpload").html(data);
        },
        failure: function () {
            alert("Failed!");
        }
    });
}
