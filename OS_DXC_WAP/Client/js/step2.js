﻿
$(document).ready(function () {
    init();
    var maternityValue = "";
    $('[data-toggle="tooltip"]').tooltip();
    var HiddendrpProfessionVAlue = $("#HiddendrpProfessionVAlue").val();
    hideShowPro(HiddendrpProfessionVAlue);
    if ($("#hdnNewBorn").val() == "true" || $("#hdnNEwBornStep2").val() == "true") {
        newBornValues();
    }
    function init() {
        //loadDropDowns("step2.aspx/GetClass", "drpCover", "Select Scheme");
        //loadDropDowns("step2.aspx/GetBranch", "drpBranchCode", "Select Branch");
        //loadDropDowns("step2.aspx/GetDistrict", "drpDistrict", "Select District");
        //loadDropDowns("step2.aspx/GetNationality", "drpNationality", "Select Nationality");
        //loadDropDowns("step2.aspx/GetProfession", "drpProfession", "Select Profession");


    }

    function ValidatorEnable(val, enable) {

        val.enabled = (enable != false);
        ValidatorValidate(val);
        ValidatorUpdateIsValid();
    }

    $(".TextName").change(function () {
        var clientId = $(this)[0].id;
        if ($(this)[0].value != "") {
            var regex = new RegExp("[a-zA-Z ]{2,40}$");
            results = regex.exec($(this)[0].value);
            if (!results) {
                $("#" + clientId + "")[0].style.borderColor = "red";
                $(this)[0].value = "";
                return null;
            }
            else {
                $("#" + clientId + "")[0].style.borderColor = "";
                $(this)[0].value = results[0].toUpperCase();
            }
        }
        else
            $("#" + clientId + "")[0].style.borderColor = "red";
    });
    $(".ListGender").change(function () {
        var clientId = $(this)[0].id;
        if ($(this)[0].value != "0") {
            $("#" + clientId + "")[0].style.borderColor = "";
        }
        else {
            $("#" + clientId + "")[0].style.borderColor = "red";
            $(this)[0].value = "0";
        }
    });

    $(".TxtDateOfBirth").change(function () {
        var clientId = $(this)[0].id;
        if ($(this)[0].value != "") {
            var split = $(this)[0].value;
            if (split.indexOf("-") !== -1) {
                var splited = split.split("-");
                var dayofb = splited[0];
                var monthofb = splited[1];
                var yearofb = splited[2];
                if (dayofb.length == 2 && monthofb.length == 2 && yearofb.length == 4) {
                    if (dayofb > 31) {
                        $("#" + clientId + "")[0].style.borderColor = "red";
                        $(this)[0].value = "";
                        return null;
                    }
                    if (monthofb > 12) {
                        $("#" + clientId + "")[0].style.borderColor = "red";
                        $(this)[0].value = "";
                        return null;
                    }
                    var dateOfB = new Date();
                    dateOfB.setDate(dayofb);
                    dateOfB.setMonth(monthofb);
                    dateOfB.setYear(yearofb);
                    var currentdatless = new Date();
                    if (dateOfB >= currentdatless) {
                        $("#" + clientId + "")[0].style.borderColor = "red";
                        $(this)[0].value = "";
                    }
                    else {
                        $(this)[0].value = $(this)[0].value;
                        $("#" + clientId + "")[0].style.borderColor = "";
                    }

                }
                else {
                    $("#" + clientId + "")[0].style.borderColor = "red";
                    $(this)[0].value = "";
                }
            }
            else {
                $("#" + clientId + "")[0].style.borderColor = "red";
                $(this)[0].value = "";
            }
        }
        else
            $("#" + clientId + "")[0].style.borderColor = "red";
    });
    $(".TextMemType").change(function () {
        var clientId = $(this)[0].id;
        if ($(this)[0].value != "") {
            var regex = new RegExp("[a-zA-Z]+");
            results = regex.exec($(this)[0].value);
            if (!results) {
                $("#" + clientId + "")[0].style.borderColor = "red";
                $(this)[0].value = "";
                return null;
            }
            else {
                $("#" + clientId + "")[0].style.borderColor = "";
                $(this)[0].value = results[0].toUpperCase();
            }
        }
        else
            $("#" + clientId + "")[0].style.borderColor = "red";
    });

    $("#btnNext2").click(function () {

        EffectiveDAteValidation();
        var sdsdsdsdsfgs = $("#drpTitle").val();
        var valis = Page_Validators;
        for (i = 0; i < Page_Validators.length; i++) {
            if (!Page_Validators[i].isvalid) {
                var name = Page_Validators[i].id;

                return;
            }
        }
        if (Page_IsValid) {



            var member = {};
            var cover = 0;
            var reqType = $("#btnGroupType").val();
            member.Maternity = "";
            member.MissingValueName = "";
            member.MissingValueGender = "";
            member.MissingValueDOB = "";
            member.MissingValueMemType = "";
            if (reqType == "ED") {
                var i = 0;
                var m = 0;
                var deps = "";
                var drpcount = 0;
                var ncount = 0;
                var gcount = 0;
                var mcount = 0;
                var dobcount = 0;

                $('#tbleRptMembers input[type="checkbox"]').each(function () {
                    if ($(this).prop('checked') == true) {
                        deps += $(this).val() + ",";
                        maternityValue = "";
                        if ($(".ListItemdata")[m].value != "0") //maternity dropdown
                        {
                            if ($(".ListItemdata")[m].value == "W01") {
                                member.Maternity += "Covered,";
                                drpcount += 1;
                            }
                            else {
                                member.Maternity += "Not Covered,";
                                drpcount += 1;
                            }
                        }
                        else {


                            var tdItem = $(".tdMemType")[m];
                            var innterTExt = tdItem.innerText;
                            if (innterTExt == "S" && $(".ListItemdata")[m].style.display == "inline") {
                                alert("please select maternity type");
                                return false;
                            }
                            else {
                                $(".ListItemdata")[m].value = "W01";
                                member.Maternity += "Covered,";
                                drpcount += 1;
                            }

                        }

                        if ($(".TextName")[m].style.display == "inline") { //Name textbox
                            if ($(".TextName")[m].value == "")
                                $(".TextName")[m].style.borderColor = "red";
                            else {
                                member.MissingValueName += $(".TextName")[m].value + "|" + $(this).val() + ",";
                                ncount += 1;
                            }
                        }

                        if ($(".ListGender")[m].style.display == "inline") { //Gender dropdown textbox
                            if ($(".ListGender")[m].value == "0")
                                $(".ListGender")[m].style.borderColor = "red";
                            else {
                                member.MissingValueGender += $(".ListGender")[m].value + "|" + $(this).val() + ",";
                                gcount += 1;
                            }
                        }
                        if ($(".TxtDateOfBirth")[m].style.display == "inline") { //DOB textbox
                            if ($(".TxtDateOfBirth")[m].value == "")
                                $(".TxtDateOfBirth")[m].style.borderColor = "red";
                            else {
                                member.MissingValueDOB += $(".TxtDateOfBirth")[m].value + "|" + $(this).val() + ",";
                                dobcount += 1;
                            }
                        }
                        if ($(".TextMemType")[m].style.display == "inline") { //RelationShipType textbox
                            if ($(".TextMemType")[m].value == "")
                                $(".TextMemType")[m].style.borderColor = "red";
                            else {
                                member.MissingValueMemType += $(".TextMemType")[m].value + "|" + $(this).val() + ",";
                                mcount += 1;
                            }
                        }

                        i++;
                    }
                    else {

                        if ($(".TxtDateOfBirth")[m].style.display == "inline") {
                            var count = parseInt($("#HiddenDobCount").val()) - 1;

                            $("#HiddenDobCount").val(count)
                        }
                        if ($(".ListGender")[m].style.display == "inline") {

                            var count = parseInt($("#HiddenGenderCount").val()) - 1;
                            $("#HiddenGenderCount").val(count);
                        }
                        if ($(".TextMemType")[m].style.display == "inline") {
                            var count = parseInt($("#HiddenMemTypeCount").val()) - 1;
                            $("#HiddenMemTypeCount").val(count);
                        }


                    }
                    m++;
                });


                //if (drpcount != parseInt($("#HiddenMaternity").val())) {
                //    alert("please select maternity type");
                //    return false;
                //} chenges by ahmad 

                if (ncount != parseInt($("#HiddenNameCount").val())) {
                    return false;
                }
                if (gcount != parseInt($("#HiddenGenderCount").val())) {
                    return false;
                }
                if (dobcount != parseInt($("#HiddenDobCount").val())) {
                    return false;
                }
                if (mcount != parseInt($("#HiddenMemTypeCount").val())) {
                    return false;
                }

                if (i < 2) {
                    alert("please select atleast 1 member");
                    return false;
                }
                else {
                    member.Dependents = deps;
                }
                if ($("#HiddenEmployeeDepStatus").val() == "1" && $("#drpMaritalStatus").val() == "1") {
                    alert('Please select marital status as married');
                    return false;
                }




            }

            member.RequestType = reqType;
            member.Id = $("#hdnReqDetailId").val();;
            // member.Sponsor = $("#hdnSponsor").val();;
            member.Title = document.getElementById("drpTitle").value;
            member.MaritalStatus = document.getElementById("drpMaritalStatus").value;

            member.Name = $("#txtName").val();
            var HiGender = $("#drpGender").val();
            if (HiGender == "" || HiGender == null) {
                member.Gender = $("#HiddendrpGender").val();
            }
            else {
                member.Gender = HiGender;
            }
            var HiProfessio = $("#drpProfession").val();
            if (HiProfessio == "" || HiProfessio == null) {
                member.Profession = $("#HiddendrpProfession").val();
            }
            else {
                member.Profession = HiProfessio;
            }
            var HidrpNationality = $("#drpNationality").val();
            if (HidrpNationality == "" || HidrpNationality == null) {
                member.Nationality = $("#HiddendrpNationality").val();
            }
            else {
                member.Nationality = HidrpNationality;
            }
            var HidExpDat = $("#txtExpDate").val();
            if (HidExpDat == "" || HidExpDat == null) {
                member.IDExpiry = $("#HiddentxtExpDate").val();
            }
            else {
                member.IDExpiry = HidExpDat;
            }
            var HidDOB = $("#txtDOB").val();
            if (HidDOB == "" || HidDOB == null) {
                member.DOB = $("#HiddentxtDOB").val();
            }
            else {
                member.DOB = HidDOB;
            }


            member.Cover = $("#drpCover").val();
            member.MemberType = $("#drpMemberType").val();
            member.BranchCode = $("#drpBranchCode").val();
            member.District = $("#drpDistrict").val();
            member.MobileNo = $("#txtMobileNumber").val();
            member.EmployeeNo = $("#txtEmpNumber").val();
            member.Department = $("#txtDepart").val();
            member.PolicyStartOn = $("#txtStartOfPolicy").val();
            member.Status = 2;
            $.ajax({
                type: "POST",
                url: "step2.aspx/UpdateMemberDetails",
                data: "{member:" + JSON.stringify(member) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == -1) {
                        alert('Contract effective date should not be null');                        
                    }
                    else {
                        disableStep(2);
                        loadMembers();
                        triggerStep(3, "dummy", "next");
                    }
                },
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });

        }
        return false;
    })

    function loadMembers() {

        var requestId = $("#hdnRequestId").val();
        $.ajax({
            type: "POST",
            url: "requestsummary.aspx?RequestId=" + requestId,
            success: function (data) {
                $("#divRequests").html(data);
            },
            failure: function () {
                alert("Failed!");
            }
        });
    }

    $("#btnPrevious2").click(function () {
        var pagetype = $("#btnGroupType").val();
        if (pagetype == "ED") {
            pagetype = "E";
        }
        triggerStep("1", pagetype, "previ");
        return false;
    })
    $("#txtDOB").blur(function () {
        $("#HiddentxtDOB").val($("#txtDOB").val());
    });
    function loadDropDowns(url, id, title) {

        $.ajax({
            type: "POST",
            url: url,
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            timeout: 40000,
            success: function (response) {
                var result = response.d;
                var jId = "#" + id;
                $(jId).empty();
                $(jId).append('<option  value>' + title + '</option>');
                $.each(result, function (index, value) {
                    $(jId).append('<option  value="' + value.Value + '">' + value.Text + '</option>');
                });

            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });
    }

    $("#drpCover").change(function () {
        var cover = this.value;
        var reqtype = $("#btnGroupType").val();

        if (reqtype == "ED") {
            reqtype = $("#hdnSubReqType").val();
        }

        if (reqtype == "ID") {
            reqtype = $("#hdnSubReqType").val();
        }
        if (reqtype == "D") {
            $("#drpMemberType").empty();
        }


        var marital = $("#drpMaritalStatus").val();
        var dofff = $("#txtDOB").val();
        var varsplitdate = dofff.split("-");

        var dateOfBearth = new Date();
        dateOfBearth.setDate(varsplitdate[0]);
        dateOfBearth.setMonth(parseInt(varsplitdate[1]) - 1);
        dateOfBearth.setYear(varsplitdate[2]);
        var getnde = $("#hdnGender").val();
        var params = JSON.stringify({
            'cover': cover,
            'reqType': reqtype,
            'dob': dateOfBearth,
            'gender': getnde,
            'maritalstatus': marital,
        });

        $.ajax({
            type: "POST",
            url: "step2.aspx/GetMemberType",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (response) {
                var result = response.d;
                $('#drpMemberType').empty();
                if (reqtype = "E")

                    $.each(result, function (index, value) {
                        $('#drpMemberType').append('<option  value="' + value.Value + '">' + value.Text + '</option>');
                    });

            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });

    });
    $("#drpMaritalStatus").change(function () {

        var selectionMAr = $("#drpMaritalStatus").val();
        var Gender = $("#hdnGender").val();
        if (Gender == "female") {
            if ($("#btnGroupType").val() == "E") {
                $("#drpCover").val("");
            }
            if (selectionMAr == "1") {
                $("#drpTitle").val("Miss");
            }
            else {
                $("#drpTitle").val("Mrs");

            }

        }
        else {
            $("#drpTitle").val("Mr");
        }
        if ($("#HiddenEmployeeDepStatus").val() == "1" && selectionMAr == "1") {
            alert('Please select marital status as married');
            $("#drpMaritalStatus").val("");
        }

    });
    $("#drpMemberType").change(function () {
        var reqType = $("#btnGroupType").val();
        var mbrType = $("#drpMemberType").val();
        var hdnContractNumber = $("#hdnContractNumber").val();
        var hdnMAinEMpType = $("#hdnMAinEMpCoverType").val();
        var gender = $("#hdnGender").val();
        var dob =  $("#txtDOB").val();
        if (reqType == "D") {
            if(mbrType == "D" || mbrType == "C" || mbrType == "SO"){
           
                 $("#drpMaritalStatus").val("1");
            }
            else if(mbrType == "H" || mbrType == "S" || mbrType == "W" || mbrType == "WM"){
                $("#drpMaritalStatus").val("2");
            }

            if (gender == "male") {
                if (mbrType == "B" || mbrType == "C" || mbrType == "F" || mbrType == "H" || mbrType == "SO")
                    $("#drpTitle").val("Mr");
            }
            else {
                if (mbrType == "D" || mbrType == "C" || mbrType == "SI")
                    $("#drpTitle").val("Miss");
                else
                    $("#drpTitle").val("Mrs");
            }
            if (mbrType != "T") {
                $("#drpCover").attr("disabled", "disabled");

                hideShowPro('none');
                if (mbrType == "S") {
                    $("#drpMaritalStatus").val("2");
                    $("#drpMaritalStatus").attr("disabled", "disabled");

                }
                else if (mbrType == "W") {
                    $("#drpMaritalStatus").val("2");
                    $("#drpMaritalStatus").attr("disabled", "disabled");
                }

                else if (mbrType == "WM") {
                    $("#drpMaritalStatus").val("2");
                    $("#drpMaritalStatus").attr("disabled", "disabled");
                }
                else {
                    $("#drpMaritalStatus").val("1");
                    $("#drpMaritalStatus").attr("disabled", false);
                }
            }
            else {

                $("#drpCover").attr("disabled", false);
                hideShowPro('block');
            }

            var indexs = JSON.stringify({
                'mbrType': mbrType,
                'ContNumberjs': hdnContractNumber,
                'MAinEMpCoverType': hdnMAinEMpType,
                'requesrTYpejs': reqType,
                'dob': dob,
            });
            $.ajax({
                type: "POST",
                url: "step2.aspx/drpMemberTypeSelectedIndexChanged",
                data: indexs,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {
                    var result = response.d;
                    if (result > 0) {
                        $("#drpCover").val(result);


                    }
                    //need to handle else case



                },
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
            
            /* var contractType = $("#hdnSME").val();
            if(contractType == "SME"){
                        $("#drpCover").attr("disabled", false);
                }
            else {
                     $("#drpCover").attr("disabled", true);
                }*/


        }
        var params = JSON.stringify({
            'mbrType': mbrType
        });


        $.ajax({
            type: "POST",
            url: "step2.aspx/SetProfessionByMemberTypeForDependent",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (response) {
                var result = response.d;
                if (result > 0) {
                    $("#drpProfession").val(result);
                    $("#drpProfession").attr("disabled", "disabled");
                }
                else if (result == 0) {
                    $("#drpProfession").val(-1);
                    $("#drpProfession").attr("disabled", false);


                }
                //need to handle else case



            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });

    });
    $("#drpGender").change(function () {
        var CompanyType = $("#HiddenComType").val().toUpperCase();
        if ($("#btnGroupType").val() == "D") {
            if ($("#hdnNewBorn").val() == "true" || $("#hdnNEwBornStep2").val() == "true") {
                if ($("#drpGender").val() == "M") {
                    $("#drpTitle").val("Mr");

                    if (CompanyType == "BDRC" || CompanyType == "BDSC") {
                        $("#drpMemberType").val("C");
                        $("#drpMemberType").attr("disabled", "disabled");
                    }


                    else {
                        $("#drpMemberType").val("SO");
                        $("#drpMemberType").attr("disabled", "disabled");
                    }


                }
                else {

                    $("#drpTitle").val("Miss");
                    if (CompanyType == "BDRC" || CompanyType == "BDSC") {
                        $("#drpMemberType").val("C");
                        $("#drpMemberType").attr("disabled", "disabled");
                    }


                    else {
                        $("#drpMemberType").val("D");
                        $("#drpMemberType").attr("disabled", "disabled");
                    }

                }
            }
        }
    });


$("#txtStartOfPolicy").change(function () {
    });

});
function newBornValues() {
    $("#txtExpDate").attr("disabled", "disabled");
    $("#drpMaritalStatus").val("1");
    $("#drpMaritalStatus").attr("disabled", "disabled");
    $('label[for="ID Expiry"]').hide();
    $('#txtExpDate').hide();
    $('#SectionEmployeeNo').hide();
    $('#SectionDept').hide();

}
function triggerStep(id, pagetype, type) {
    var previ = "step1" + pagetype + ".aspx";
    var next = "step" + id + ".aspx";
    var nextId = (type == "previ") ? previ : next;
    var containerId = "#divStep" + id;
    var triggerId = "#step" + id;
    var reqDetId = $("#hdnReqDetailId").val();
    $("#hdnStepReqDetailId").val(reqDetId);
    $.ajax({
        type: "POST",
        url: nextId,
        success: function (data) {
            $(containerId).html(data);
            var reqDetId = $("#hdnReqDetailId").val();
            $("#hdnStepReqDetailId").val(reqDetId);
        },
        failure: function () {
            alert("Failed!");
        }
    });
    $(triggerId).trigger('click');
}
function dateCheckGerf(value) {

    ValidatorEnable(document.getElementById('Regularexpressionvalidator1'), value)
}
function dateCheckForNewBaby(dayofb, monthofb, yearofb) {

    var ddd = $("#hdnNewBorn").val();
    if ($("#hdnNewBorn").val() == "true") {
        var dateOfB = new Date();
        dateOfB.setDate(dayofb);
        dateOfB.setMonth(monthofb);
        dateOfB.setYear(yearofb);
        var currentdatless = new Date();
        if (currentdatless < dateOfB) {
            ValidatorEnable(document.getElementById('REVBabayAge'), true)
        }
        else {
            var year = parseInt(currentdatless.getFullYear());
            var nyear = year - 1;
            currentdatless.setYear(parseInt(nyear));
            if (currentdatless > dateOfB) {

                ValidatorEnable(document.getElementById('REVBabayAge'), true)
            }
            else {
                ValidatorEnable(document.getElementById('REVBabayAge'), false)
            }
        }

    }

}


function EffectiveDAteValidation() {
        var policyDate = $("#txtStartOfPolicy").val();
        var dob =  $("#txtDOB").val();
        var params = JSON.stringify({
                'policyDate': policyDate,
                'dob': dob,
            });

     $.ajax({
            type: "POST",
            url: "step2.aspx/ValidatePolicyEffectiveDate",
             data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (response) 
            {
                var result = response.d;
                if(result == '')
                {                 
                    ValidatorEnable(document.getElementById('regValtxtStartOfPolicyREange'), false);
                }
                else
                {
                    ValidatorEnable(document.getElementById('regValtxtStartOfPolicyREange'), true);
                    document.getElementById("regValtxtStartOfPolicyREange").innerHTML = response.d;
                }
            },
            failure: function (response) {
               alert(response);
            },
            error: function (response) {
                 alert(response);
            }
        });
}



function hideShowPro(vla) {
    if (vla == "block") {
        $("#DivPRofession").show();
    }
    else {
        $("#DivPRofession").hide();
    }
}


