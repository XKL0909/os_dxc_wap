﻿
$().ready(function () {
    $("#frmAddition").validate({
        rules: {

            txtMemberShip: {
                required: true,
                minlength: 2
            },
            txtID: {
                required: true,
                minlength: 10
            }

        },
        messages: {

            txtMemberShip: {
                required: "Membership required",
                minlength: "Your membership must consist of at least 2 characters"
            },
            txtID: {
                required: "Id is required",
                minlength: "Your ID must consist of at least 10 characters"
            }

        }
    });

});