﻿$(document).ready(function () {
    init();
    $('[data-toggle="tooltip"]').tooltip();
    $("#hdnNewBorn").val("false");
    function ValidatorEnable(val, enable) {

        val.enabled = (enable != false);
        ValidatorValidate(val);
        ValidatorUpdateIsValid();
    }
    var checkingTYpe;
    function init() {
        checkingTYpe = $('#hdnIdType').val();
        if (checkingTYpe) {
            if (checkingTYpe == "1") {
                $("#sectionHijari").show();
                $("#sectionDepHijari").hide();
                $("#DivDepIqama").hide();
                ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), true);
                // ValidatorEnable(document.getElementById('reqValtxtDepHYearOfBirth'), true);
                //$("#txtDepHYearOfBirth").hide();
            }
            else {
                $("#sectionHijari").hide();
                $("#sectionDepHijari").hide();
                $("#DivDepIqama").hide();
                ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);
                //  ValidatorEnable(document.getElementById('reqValtxtDepHYearOfBirth'), true);
                // $("#txtDepHYearOfBirth").hide();
            }
        }
        else {
            $("#sectionHijari").hide();
            $("#sectionDepHijari").hide();
            $("#DivDepIqama").hide();
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);
            //  ValidatorEnable(document.getElementById('reqValtxtDepHYearOfBirth'), false);
            //  $("#txtDepHYearOfBirth").hide();

        }
    }


    $("#SelAddFamily").change(function () {
        var VAlAnswer = $("#SelAddFamily").val();
        if (VAlAnswer) {
            var IDStr = $("#txtID").val();
            var vlaue = IDStr.charAt(0);
            if (VAlAnswer == "Yes") {
                $("#chkAddFamily").attr('checked', true);
                if (vlaue == "1") {
                    $("#sectionDepHijari").show();
                    $("#DivDepIqama").hide();
                    //     $("#txtDepHYearOfBirth").show();
                    $("#txtDepHYearOfBirth").val("");
                    $("#DepIqamaNo").val("");

                    ValidatorEnable(document.getElementById('reqValtxtDepHYearOfBirth'), true);
                    ValidatorEnable(document.getElementById('RequiredfieldSelAddFamily'), false);
                    ValidatorEnable(document.getElementById('DepIqamaNoRExV'), false);
                    ValidatorEnable(document.getElementById('DepIqamaNoRfv'), false);
                }
                else if (vlaue == "2") {
                    $("#sectionDepHijari").hide();
                    $("#DivDepIqama").show();
                    //     $("#txtDepHYearOfBirth").show();
                    $("#txtDepHYearOfBirth").val("");
                    $("#DepIqamaNo").val("");
                    ValidatorEnable(document.getElementById('reqValtxtDepHYearOfBirth'), false);
                    ValidatorEnable(document.getElementById('RequiredfieldSelAddFamily'), false);
                    ValidatorEnable(document.getElementById('DepIqamaNoRExV'), true);
                    ValidatorEnable(document.getElementById('DepIqamaNoRfv'), true);
                }
                else {

                }





            }
            else {
                $("#chkAddFamily").attr('checked', false);
                $("#sectionDepHijari").hide();
                $("#DivDepIqama").hide();
                //    $("#txtDepHYearOfBirth").hide();
                $("#txtDepHYearOfBirth").val("");
                $("#DepIqamaNo").val("");
                ValidatorEnable(document.getElementById('reqValtxtDepHYearOfBirth'), false);
                ValidatorEnable(document.getElementById('RequiredfieldSelAddFamily'), false);
                ValidatorEnable(document.getElementById('DepIqamaNoRExV'), false);
                ValidatorEnable(document.getElementById('DepIqamaNoRfv'), false);
            }

        }
        else {
            $("#chkAddFamily").attr('checked', false);
            $("#sectionDepHijari").hide();
            //  $("#txtDepHYearOfBirth").hide();
            //    $("#txtDepHYearOfBirth").val("");
            ValidatorEnable(document.getElementById('reqValtxtDepHYearOfBirth'), false);
            ValidatorEnable(document.getElementById('RequiredfieldSelAddFamily'), true);
        }

    });

    $("#txtID").blur(function () {
        var id = $(this).val();
        checkIdType(id);
    });

    function checkIdType(value) {
        var val = value.startsWith("1");
        if (val) {
            $("#sectionHijari").show();
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), true);
        }
        else {
            $("#sectionHijari").hide();
            $("#txtHYearOfBirth").val("");
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);
        }
    }

    function chekIdType(value) {
        if (value.startsWith("1")) {
            return true;
        }
        else if (value.startsWith("2")) {
            return true;
        }
        else if (value.startsWith("3") || value.startsWith("4") || value.startsWith("5")) {
            return true;
        }
        return false;
    }
    $('#txtID').on('keydown', function (e) {
        if (e.which == 13) {

            if ($('#txtID').val()) {
                var id = $(this).val();
                checkIdType(id);
            }
        }
    });
    $("#btnNext").click(function () {
        if (Page_IsValid) {

            var id = $("#txtID").val();



            if (chekIdType(id)) {
                $.LoadingOverlay("show");
                $("#hdnIdType").val(id.substring(0, 1));
                var member = {};
                member.RequestId = $("#hdnRequestId").val();
                member.Sponsor = $("#drplistSponsor").val();
                member.IdNumber = $("#txtID").val();
                member.RequestType = $("#btnGroupType").val();
                member.HijriDOBYear = $("#txtHYearOfBirth").val();
                var depHYearOfBirth = $("#txtDepHYearOfBirth").val();
                var DepIqamaNo = $("#DepIqamaNo").val();
                if (depHYearOfBirth != "" || DepIqamaNo != "") {
                    $("#btnGroupType").val("ED");
                    if (depHYearOfBirth != "") {
                        member.DepHijriDOBYear = depHYearOfBirth;
                        member.DepIqamaNo = 0;
                    }
                    if (DepIqamaNo != "") {
                        member.DepHijriDOBYear = 0;
                        member.DepIqamaNo = DepIqamaNo;
                    }
                    
                    member.RequestType = "ED";
                }
                else {
                    $("#btnGroupType").val("E");
                    member.RequestType = "E";
                }


                $.ajax({
                    type: "POST",
                    url: "step1e.aspx/AddMemberDetails",
                    data: "{member:" + JSON.stringify(member) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (response) {
                        alert(response.d);
                    }
                });

                function OnSuccess(response) {

                    var reqDetId = response.d;
                    //if (reqDetId > 1) {
                    //    $("#hdnReqDetailId").val(reqDetId);
                    //}


                    triggerStep(2, "next");
                }

                //triggerStep(2, "next");
            }
            else {
                alert("invalid id format");
            }
        }
        return false;
    })

    function loadMembers() {
        var requestId = $("#hdnRequestId").val();
        $.ajax({
            type: "POST",
            url: "requestsummary.aspx?RequestId=" + requestId,
            success: function (data) {
                $("#divRequests").html(data);
            },
            failure: function () {
                alert("Failed!");
            }
        });
    }

    function triggerStep(id, type) {
        id = (type == "next") ? id : prev;
        var nextId = "step" + id + ".aspx";
        var containerId = "#divStep" + id;
        var triggerId = "#step" + id;
        $.ajax({
            type: "POST",
            url: nextId,
            success: function (data) {
                $(containerId).html(data);
                loadMembers();
                $.LoadingOverlay("hide");

            },
            failure: function () {
                alert("Failed!");
            }
        });
        $(triggerId).trigger('click');

    }
});