﻿

$(document).ready(function () {
    init();
       function init() {
        $(".stepwizard").hide();
        loadMembers();
        $("#step1LoadingImage").hide();
    }

    function loadMembers() {
       
        var requestId = $("#hdnRequestId").val();
        $.ajax({
            type: "POST",
            url: "requestsummary.aspx?RequestId=" + requestId,
            success: function (data) {
                $("#divRequests").html(data);
            },
            failure: function () {
                alert("Failed!");
            }
        });
    }


    function initForms() {
        $(".stepwizard").hide();
        $("#divStep1").html("");
        $("#divStep2").html("");
        $("#divStep3").html("");
        $("#btnGroupType").html("");
        $("#hdnRequestId").html("");
        $("#hdnReqDetailId").html("");
        $("#hdnSubReqType").html("");
        $("#hdnSecSponsor").html("");
       
    }

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        prevStepWizard.removeAttr('disabled').trigger('click');
    });

   
    allNextBtn.click(function () {
       
        //debugger;
        var curStep = $(this).closest(".setup-content"),
      curStepBtn = curStep.attr("id"),
      nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
      curInputs = curStep.find("input[type='text'],input[type='url']"),
      isValid = true;
        var nextNo = parseInt(curStepBtn.slice(-1)) + 1;

        var nextId = "step" + nextNo + ".aspx";
        var containerId = "#divStep" + nextNo;
        

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

       

        if (isValid) {
            nextStepWizard.removeAttr('disabled').trigger('click');
        }
       
    });

    

    $('div.setup-panel div a.btn-primary').trigger('click');

    
    $("#btnAddDependent").click(function () {
        initForms();
        //$("#divMode").html("New");
        $("#step1LoadingImage").show();
        initSession("D");
        //setFunctionType("D");
    });

    $("#btnAddDependentWithOutMemberShip").click(function () {
        initForms();
        //$("#divMode").html("New");
        $("#step1LoadingImage").show();
        initSession("ID");
        //setFunctionType("ED");
    });


    $("#btnAddEmpDependent").click(function () {
        initForms();
       // $("#divMode").html("New");
        $("#step1LoadingImage").show();
        initSession("ED");
        //setFunctionType("ED");
    });

  
    $("#btnAddEmployee").click(function () {
        initForms();
       // $("#divMode").html("New");
        $("#step1LoadingImage").show();
        initSession("E");
        //setFunctionType("E");
    });


    function initSession(value) {
        $(".stepwizard").show();
        $.LoadingOverlay("show");
        var page = "step1" + value + ".aspx/InitSession" 
        $.ajax({
            type: "POST",
            url: page,
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: OnSuccess,
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
              
            }
        });

        function OnSuccess(response) {

            var reqDetId = response.d;
          
            setFunctionType(value);
          
        }
    }

    function initCommonSession(value) {

        var page = "step1" + value + ".aspx/InitSession"
        $.ajax({
            type: "POST",
            url: page,
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:  function (response) {
                var result = response.d;

            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);

            }
        });

      
    }

    function setFunctionType(value) {
        $("#step1").trigger('click');
        $("#btnGroupType").val(value);
        var page = "step1" + value + ".aspx"
        $.ajax({
            type: "POST",
            url: page,
            success: function (data) {
               
                $(divStep1).html(data);
                $.LoadingOverlay("hide");
            },
            failure: function () {
                alert("Failed!");

            }
        });

    }
	
});
function setReqDetail(value) {
    $("#hdnReqDetailId").val(value);
}
function disableStep(id) {
    var triggerId = "#divStep" + id;
    $(triggerId).html("");
}

