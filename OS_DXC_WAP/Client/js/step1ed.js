﻿$(document).ready(function () {
    init();

    function ValidatorEnable(val, enable) {
       
        val.enabled = (enable != false);
        ValidatorValidate(val);
        ValidatorUpdateIsValid();
    }

    function init() {
        $("#sectionHijari").hide();
        ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);
    }

    $("#txtID").blur(function () {
        var id = $(this).val();
        checkIdType(id);
    });

    function checkIdType(value) {
        var val = value.startsWith("1");

        if (val) {
            $("#sectionHijari").show();
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), true);
        }
        else {
            $("#sectionHijari").hide();
            $("#txtHijari").val("");
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);
        }
    }
    $("#chkNewBorn").change(function () {
        var isChecked = $(this).is(':checked');
        setNewBornValidation(isChecked)
    });

    function setNewBornValidation(isChecked) {
        if (isChecked) {
            ValidatorEnable(document.getElementById('reqValtxtID'), false);
        }
        else {
            ValidatorEnable(document.getElementById('reqValtxtID'), true);
        }
    }

    function chekIdType(value) {
        if (value.startsWith("1")) {
            return true;
        }
        else if (value.startsWith("2")) {
            return true;
        }
        else if (value.startsWith("3") || value.startsWith("4") || value.startsWith("5")) {
            return true;
        }
        return false;
    }

    $("#btnNext").click(function () {

        if (Page_IsValid) {
           
            var id = $("#txtID").val();
            var isChecked = $("#chkNewBorn").is(':checked');
           
                if (chekIdType(id)) {
                    $.LoadingOverlay("show");
                    var member = {};
                    member.RequestId = $("#hdnRequestId").val();
                    member.Sponsor = $("#drplistSponsor").val();
                    member.IdNumber = $("#txtID").val();
                    member.RequestType = $("#btnGroupType").val();
                    member.HijriDOBYear = $("#txtHYearOfBirth").val();
                    member.DepHijriDOBYear = $("#txtDepHYearOfBirth").val();

                    $.ajax({
                        type: "POST",
                        url: "step1ed.aspx/AddMemberDetails",
                        data: "{member:" + JSON.stringify(member) + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: OnSuccess,
                        failure: function (response) {
                            alert(response.d);
                        },
                        error: function (response) {
                            alert(response.d);
                        }
                    });

                    function OnSuccess(response) {

                        var reqDetId = response.d;
                        //if (reqDetId > 1) {
                        //    $("#hdnReqDetailId").val(reqDetId);
                        //}
                       
                      
                        triggerStep(2, "next");
                    }
                  
                }
                else {
                    alert("invalid id format");
                }
           

        }
        return false;
    })


    function triggerStep(id, type) {
        id = (type == "next") ? id : prev;
        var nextId = "step" + id + ".aspx";
        var containerId = "#divStep" + id;
        var triggerId = "#step" + id;
        $.ajax({
            type: "POST",
            url: nextId,
            success: function (data) {
                $(containerId).html(data);
                loadMembers();
                $.LoadingOverlay("hide");
            },
            failure: function () {
                alert("Failed!");
            }
        });
        $(triggerId).trigger('click');
    }
});