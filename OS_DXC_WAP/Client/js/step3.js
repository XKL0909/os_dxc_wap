﻿$(document).ready(function () {

    init();

    function init() {
        loadFiles();
    }

    $("#btnUpload").click(function (evt) {
        if (typeof FormData == "undefined") {
            alert("Due To Security Reasons This Version Of Browser is Not Supported Please Update to IE 10 or Higher Version or Use Another Browser");
        }
        else {
            var data = new FormData();
            var fileUpload = $("#FileUpload").get(0);
            var files = fileUpload.files;
            for (var i = 0; i < files.length; i++) {
                data.append(files[i].name, files[i]);
            }
            $.ajax({
                url: "FileUploadHandler.ashx",
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result == "success") {
                        loadFiles();
                    }
                    else if (result == "exterror") {
                        var error = "Please ensure you upload the correct file type (allowed extensions are: .jpg, .jpeg, .gif, .png, .bmp, .tiff, .pdf, .rtf) "
                        $("#lblError").html(error);
                    }
                    else if (result == "sizeerror") {
                        var error = "Cannot upload this file: This file exceeds the total file size allowed to be uploaded. Total allowed size is: 5MB"
                        $("#lblError").html(error);
                    }
                    else if (result == "error") {
                        var error = "general error";
                        $("#lblError").html(error);
                    }
                },
                error: function (err) {
                    alert(err.statusText)
                }
            });

            evt.preventDefault();
        }
    });

    function loadFiles() {

        $.ajax({
            type: "POST",
            url: "memberdocuments.aspx",
            success: function (data) {
                $("#gridFileUpload").html(data);
            },
            failure: function () {
                alert("Failed!");
            }
        });
    }

    $("#btnNext3").click(function () {

        var id = $("#hdnReqDetailId").val();

        $.ajax({
            type: "POST",
            url: "step3.aspx/IsAttachmentExists",
            data: "{Id:" + id + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d;
                if (result == false) {
                    alert("please upload files");

                    return false;
                }
                else {

                    updateStatus(id);
                }
            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });
        return false;
    })

    function updateStatus(reqDetailId) {
        $.ajax({
            type: "POST",
            url: "step3.aspx/UpdateStatus",
            data: "{Id:" + reqDetailId + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d;
                if (result > 0) {
                    var result = response.d;


                    //alert("success");
                    loadMembersGrid();
                    initForms();
                    initCommonSession("E");
                    $("#step1").trigger('click');

                }
                else {
                    var result = response.d;


                    alert("Error while submitting the request,please contact administrator");
                }
            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });
    }

    $("#btnPrevious3").click(function () {
        triggerStep(2, "previ");
        return false;
    })

    function loadMembersGrid(requestId) {
        var requestId = $("#hdnRequestId").val();

        $.ajax({
            type: "POST",
            url: "requestsummary.aspx?RequestId=" + requestId,
            success: function (data) {
                $("#divRequests").html(data);
            },
            failure: function () {
                alert("Failed!");
            }
        });
    }
    function initForms() {
        $(".stepwizard").hide();
        $("#divStep1").html("");
        $("#divStep2").html("");
        $("#divStep3").html("");
        $("#btnGroupType").html("");
        $("#hdnRequestId").html("");
        $("#hdnReqDetailId").html("");
        $("#hdnSubReqType").html("");
        $("#hdnSecSponsor").html("");

    }

    function initCommonSession(value) {

        var page = "step1" + value + ".aspx/InitSession"
        $.ajax({
            type: "POST",
            url: page,
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d;

            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);

            }
        });


    }

    var ul = $('#upload ul');

    $('#drop a').click(function () {
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({
        forceIframeTransport: true,
        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {

            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"' +
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo(ul);

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function () {

                if (tpl.hasClass('working')) {
                    jqXHR.abort();
                }

                tpl.fadeOut(function () {
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit();
        },

        progress: function (e, data) {


            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if (progress == 100) {
                data.context.removeClass('working');
            }
        },
        done: function (e, data) {
            // var result = data.result;
            var result = $('pre', data.result).text();
            console.log(result);
           // alert(result);
            if (result == "success") {
                            loadFiles();
                        }
                        else if (result == "exterror") {
                            var error = "Please ensure you upload the correct file type (allowed extensions are: .jpg, .jpeg, .gif, .png, .bmp, .tiff, .pdf, .rtf) "
                            $("#lblError").html(error);
                        }
                        else if (result == "sizeerror") {
                            var error = "Cannot upload this file: This file exceeds the total file size allowed to be uploaded. Total allowed size is: 5MB"
                            $("#lblError").html(error);
                        }
                        else if (result == "error") {
                            var error = "general error";
                            $("#lblError").html(error);
                        }
        },
        fail: function (e, data) {
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });


    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }
});

function triggerStep(id, type) {

    var nextId = "step" + id + ".aspx";
    var containerId = "#divStep" + id;
    var triggerId = "#step" + id;
    $.ajax({
        type: "POST",
        url: nextId,
        success: function (data) {

            $(containerId).html(data);
        },
        failure: function () {
            alert("Failed!");
        }
    });
    $(triggerId).trigger('click');
}