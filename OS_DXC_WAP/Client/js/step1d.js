﻿$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    init();
    $("#hdnNewBorn").val("false");
    var requestID = $("#hdnRequestId").val();
    $("#hdnRequestID").val($("#hdnRequestId").val());

    function ValidatorEnable(val, enable) {

        val.enabled = (enable != false);
        ValidatorValidate(val);
        ValidatorUpdateIsValid();
    }
    $('#chkNewBorn').click(function () {
        if ($(this).is(':checked')) {
            $("#txtID").val("");
            $("#txtID").attr("disabled", "disabled");
            $("#txtHYearOfBirth").val("");
            $("#txtHYearOfBirth").hide();
            ValidatorEnable(document.getElementById('reqValtxtID'), false);
            ValidatorEnable(document.getElementById('regularexpressionTXTID'), false);
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);

        } else {
            $("#txtID").attr("disabled", false);
            ValidatorEnable(document.getElementById('reqValtxtID'), true);
            ValidatorEnable(document.getElementById('regularexpressionTXTID'), true);
        }
    });
    function init() {
        var valuseOf = $("#SelMemberShipQ").val();
        if (!(valuseOf) || valuseOf=="") {
            $("#HijriYearofDiv").hide();
            $("#sectionNewBorn").hide();
            $("#DependantIDDiv").hide();
            $("#MainMemberMembershipDiv").hide();
            $("#MainMemberIdDiv").hide();

        }
        else {
            if (valuseOf == "Yes") {
                $("#HijriYearofDiv").hide();
                $("#sectionNewBorn").show();
                $("#DependantIDDiv").show();
                $("#MainMemberMembershipDiv").show();
                $("#MainMemberIdDiv").hide();
                ValidatorEnable(document.getElementById('reqValdrpMAinEMployee'), false);
                ValidatorEnable(document.getElementById('reqValdtxtBupaMembershipNumber'), true);
            }
            else {
                $("#HijriYearofDiv").hide();
                $("#sectionNewBorn").show();
                $("#DependantIDDiv").show();
                $("#MainMemberMembershipDiv").hide();
                $("#MainMemberIdDiv").show();
                ValidatorEnable(document.getElementById('reqValdrpMAinEMployee'), true);
                ValidatorEnable(document.getElementById('reqValdtxtBupaMembershipNumber'), false);
            }
        }
        var checkingTYpe = $('#hdnIdType').val();
        if (checkingTYpe) {
            if (checkingTYpe == "1") {
                return;
            }
            else {
                $("#sectionHijari").hide();
                ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);
            }
        }
        else {
            $("#sectionHijari").hide();
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);

        }

    }
    $('#txtID').on('keydown', function (e) {
        if (e.which == 13) {

            if ($('#txtID').val()) {
                var id = $(this).val();
                checkIdType(id);
            }
        }
    });

    $("#txtID").blur(function () {
        var id = $(this).val();
        checkIdType(id);
    });

    function checkIdType(value) {
        var val = value.startsWith("1");

        if (val) {
            $("#sectionHijari").show();
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), true);
        }
        else {
            $("#sectionHijari").hide();
            $("#txtHijari").val("");
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);
        }
    }
    
    $("#SelMemberShipQ").change(function () {
        var valuseOf = $("#SelMemberShipQ").val();
        if (!(valuseOf) || valuseOf == "") {
            $("#HijriYearofDiv").hide();
            $("#sectionNewBorn").hide();
            $("#DependantIDDiv").hide();
            $("#MainMemberMembershipDiv").hide();
            $("#MainMemberIdDiv").hide();

        }
        else {
            if (valuseOf == "Yes") {
                $("#HijriYearofDiv").hide();
                $("#sectionNewBorn").show();
                $("#DependantIDDiv").show();
                $("#MainMemberMembershipDiv").show();
                $("#MainMemberIdDiv").hide();
                ValidatorEnable(document.getElementById('reqValdrpMAinEMployee'), false);
                ValidatorEnable(document.getElementById('reqValdtxtBupaMembershipNumber'), true);
            }
            else {
                $("#HijriYearofDiv").hide();
                $("#sectionNewBorn").show();
                $("#DependantIDDiv").show();
                $("#MainMemberMembershipDiv").hide();
                $("#MainMemberIdDiv").show();
                ValidatorEnable(document.getElementById('reqValdrpMAinEMployee'), true);
                ValidatorEnable(document.getElementById('reqValdtxtBupaMembershipNumber'), false);
            }
        }

      
    });
    $("#chkNewBorn").change(function () {
        var isChecked = $(this).is(':checked');
        setNewBornValidation(isChecked)
    });

    function setNewBornValidation(isChecked) {
        if (isChecked) {
            ValidatorEnable(document.getElementById('reqValtxtID'), false);
            ValidatorEnable(document.getElementById('regularexpressionTXTID'), false);
            ValidatorEnable(document.getElementById('regularexpressionHijriYear'), false);
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), false);

        }
        else {
            ValidatorEnable(document.getElementById('reqValtxtID'), true);
            ValidatorEnable(document.getElementById('regularexpressionTXTID'), true);
            ValidatorEnable(document.getElementById('regularexpressionHijriYear'), true);
            ValidatorEnable(document.getElementById('reqValtxtHYearOfBirth'), true);
        }
    }

    function chekIdType(value) {
        if (value.startsWith("1")) {
            return true;
        }
        else if (value.startsWith("2")) {
            return true;
        }
        else if (value.startsWith("3") || value.startsWith("4") || value.startsWith("5")) {
            return true;
        }
        else if ($("#chkNewBorn").is(':checked')) {
            return true;
        }

        return false;
    }

    $("#btnNext").click(function () {

        if ($("#SelMemberShipQ").val()) {

            ValidatorEnable(document.getElementById('RequiredfieldSelSelMemberShipQ'), true);
        }

        if (Page_IsValid) {

            var id = $("#txtID").val();

            $("#hdnIdType").val(id.substring(0, 1));
            var isChecked = $("#chkNewBorn").is(':checked');

            if (chekIdType(id)) {
                $.LoadingOverlay("show");
                var member = {};
                member.RequestId = $("#hdnRequestId").val();
                member.SecSponsorId = $("#drpMAinEMployee").val();
                member.IdNumber = $("#txtID").val();
                member.RequestType = $("#btnGroupType").val();
                member.MbrshipNo = $("#txtBupaMembershipNumber").val();
                member.HijriDOBYear = $("#txtHYearOfBirth").val();
                member.MemberShipQ = $("#SelMemberShipQ").val();

                if (isChecked) {
                    member.NewBorn = isChecked;
                    $("#hdnNewBorn").val(isChecked);
                }
                else {
                    member.NewBorn = isChecked;
                    $("#hdnNewBorn").val("false");
                }


                $.ajax({
                    type: "POST",
                    url: "step1d.aspx/AddMemberDetails",
                    data: "{member:" + JSON.stringify(member) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var reqDetId = response.d;
                        if (reqDetId > 1) {
                            $("#hdnReqDetailId").val(reqDetId);
                        }

                        //loadMembers();
                        triggerStep(2, "next");
                    },
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (response) {
                        alert(response.d);
                    }
                });

            }
            else {
                alert("invalid id format");
            }


        }
        return false;
    })

    function loadMembers() {

        var requestId = $("#hdnRequestId").val();
        $.ajax({
            type: "POST",
            url: "requestsummary.aspx?RequestId=" + requestId,
            success: function (data) {
                $("#divRequests").html(data);
            },
            failure: function () {
                alert("Failed!");
            }
        });
    }

    function triggerStep(id, type) {

        id = (type == "next") ? id : prev;
        var nextId = "step" + id + ".aspx";
        var containerId = "#divStep" + id;
        var triggerId = "#step" + id;


        $.ajax({
            type: "POST",
            url: nextId,
            success: function (data) {
                $(containerId).html(data);
                loadMembers();
                $.LoadingOverlay("hide");
            },
            failure: function () {
                alert("Failed!");
            }
        });
        $(triggerId).trigger('click');
    }
});

