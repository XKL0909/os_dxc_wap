using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using System;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Configuration;
using System.Configuration;
using System.IO;
using OS_DXC_WAP.CaesarWS;

public partial class CardReplace : System.Web.UI.Page
{
    private ServiceDepot_DNService ws;
    private string strClientID;

    // The upload category for the uploading of this page
    private readonly string _uploadCategory = UploadCategory.MEMBER_REPCARD;
    private readonly string[] _uploadOptionalReasons = new string[] { "REP001", "REP002", "REP006" };
    private string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblError.Text = "";
            if (string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                Response.Redirect("../default.aspx");
            }

            strClientID = Session["ClientID"].ToString();

            if (!Page.IsPostBack)
            {
                lnkBatchUpload.NavigateUrl = "BatchUpload.aspx?val=" + Cryption.Encrypt("optiontype=cardreplace");

                DisableDataReasons();
                LoadCaesarSourcedData();
				//Aramco PId Changes by Hussamuddin
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    LblChangeBranchBanner.InnerText = "Whether your request is to add members, delete members, change Scheme, change branch or replace members cards, you may submit your transaction at your convenience.";
                    RadNotification1.Visible = false;
                    ddlRepReason.Items.RemoveAt(15);
                    tblUploader.Visible = false;
                    //CorrectGender.Visible=false;
                    //CorrectNationality.Visible=false;
                    //CorrectTitle.Visible=false;
                    //CorrectName.Visible=false;
                    //CorrectDateOfBirth.Visible=false;
                    //IDType.Visible=false;
                    //CorrectIqamaID.Visible=false;
                    //CorrectSponsorID.Visible=false;
                    //EmployeeNo.Visible=false;
                    //MemberType.Visible=false;
                    //IDExpiryDate.Visible=false;
                    //MaritalStatus.Visible=false;
                    //Profession.Visible=false;
                    //District.Visible=false;
                    //MobileNo.Visible = false;
                }
                else { 
                    RadNotification1.Visible = true;
                    //SetupUploader(_uploadCategory);
                }
                //Aramco PId Changes by Hussamuddin
            }
			if (Convert.ToString(Session["ContractType"]).ToUpper() != AramcoContractType)
            {
                SetupUploader(_uploadCategory);
            }
            
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    private void SetupUploader(string uploadCategory)
    {
        // Determine if upload is mandatory
        string dataReason = ddlRepReason.SelectedValue;
        
        StringCollection optionalList = new StringCollection();
        optionalList.AddRange(_uploadOptionalReasons);

        if (optionalList.IndexOf(dataReason) == -1 && dataReason.Length > 0)
        {
            // Uploads are mandatory
            EnableUploader(false);
        }
        else
        {
            // Uploads are optional
            EnableUploader(true);
        }

        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        // Bind to allow resolving of # tags in mark-up
        DataBind();
    }

    protected void Page_LoadComplete(object sender, System.EventArgs e)
    {

        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {

            lblAramcoMessage.Visible = true;
            Label1.Visible = false;
        }
        else
        {
            DivStafflbl.Visible = false;
            // Enable / Disable the submission option depending on whether the user 
            // has uploaded supported documents

            // Determine if upload is mandatory
            string dataReason = ddlRepReason.SelectedValue;

            StringCollection optionalList = new StringCollection();
            optionalList.AddRange(_uploadOptionalReasons);

            if (optionalList.IndexOf(dataReason) == -1 && dataReason.Length > 0)
            {
                // Uploads are manadatory
                EnableUploader(false);
            }
            else
            {
                // Uploads are optional
                EnableUploader(true);
            }
        }
    }

    private void EnableUploader(bool optionalUploads)
    {
        Button1.Enabled = false;

        // Get what has been uploaded
        DataSet uploadedFileSet = uploader.UploadedFileSet;

        int uploadedCount = 0;
        UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);

        if (_uploadmanger.WebIndecator("SuportDocs", strClientID))
        {
            tblUploader.Visible = true;
            if (uploadedFileSet != null && uploadedFileSet.Tables.Count > 0)
            {
                uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                if (uploadedCount > 0)
                {
                    Button1.Enabled = true;
                }
                else
                {
                    Button1.Enabled = false;
                }
            }
        }
        else
        {
            tblUploader.Visible = true;
            Button1.Enabled = true;
        }


        if (optionalUploads)
        {
            Button1.Enabled = true;
        }
        
    }

    private void DisableDataReasons()
    {
        ddlEGender.Enabled = false;
        ddlENationality.Enabled = false;
        ddlETitle.Enabled = false;
        txtCorrectName.Enabled = false;
        txtCorrectDOB.Enabled = false;
        txtCorrectIqamaID.Enabled = false;
        txtCorrectSponsorID.Enabled = false;
        txtEmployeeNo.Enabled = false;
        ddlMaritalStatus.Enabled = false;
        ddProfession.Enabled = false;
        ddDistrict.Enabled = false;
        ddlMemberType.Enabled = false;
        ddlIDType.Enabled = false;
        txtMobileNo.Enabled = false;
        txtIDExpDate.Enabled = false;
        txtEffectiveDate.Enabled = false;

        txtCorrectName.BackColor = Color.FromArgb(199, 174, 160);
        txtCorrectDOB.BackColor = Color.FromArgb(199, 174, 160);
        txtCorrectIqamaID.BackColor = Color.FromArgb(199, 174, 160);
        txtCorrectSponsorID.BackColor = Color.FromArgb(199, 174, 160);
        txtEmployeeNo.BackColor = Color.FromArgb(199, 174, 160);
        txtMobileNo.BackColor = Color.FromArgb(199, 174, 160);
        txtIDExpDate.BackColor = Color.FromArgb(199, 174, 160);
        txtEffectiveDate.BackColor = Color.FromArgb(199, 174, 160);
    }

    private void ResetDataReasons()
    {
        ddlEGender.SelectedIndex = 0;
        ddlENationality.SelectedIndex = 0;
        ddlETitle.SelectedIndex = 0;
        txtCorrectName.Text = string.Empty;
        txtCorrectDOB.Text = string.Empty;
        txtCorrectIqamaID.Text = string.Empty;
        txtCorrectSponsorID.Text = string.Empty;
        txtEmployeeNo.Text = string.Empty;
    }

    private void fncReplaceCard()
    {
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;

        #region Caesar Invocation

        ws = new ServiceDepot_DNService();

        SubTxnRequest_DN request;
        SubTxnResponse_DN response;
        request = new SubTxnRequest_DN();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.TransactionType = "CARD REPLACEMENT";
        request.TransactionID = WebPublication.GenerateTransactionID();
        request.BatchIndicator = "N";
        request.detail = new AllMbrDetail_DN[1];
        request.detail[0] = new AllMbrDetail_DN();
        request.detail[0].sql_type = "CSR.SUB_TXN_REC";
        request.detail[0].ContractNo = strClientID;
        request.detail[0].MbrName = txtMemberName.Text;
        request.memberName = new String[1];
        request.memberName[0] = txtMemberName.Text;
        string memberShipNumber = "";
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            memberShipNumber = BLCommon.ReturnMemberShipNumberForStaffnumber(txtBadgeNo.Text.Trim(), Convert.ToString(Session["ClientID"]));

            if (memberShipNumber.Contains(":"))
            {
                lblError.Text = memberShipNumber;
                return;
            }
            else
            {
                // Grab the memberID
                if (!string.IsNullOrEmpty(memberShipNumber))
                {
                    // Fetch the member's details
                    MemberHelper mh = new MemberHelper(memberShipNumber, strClientID);
                    Member m = mh.GetMemberDetails();
                    //HttpContext.Current.Response.Write(m.Nationality);
                    if (Check_Member_belong_to_client(m.ContractNumber))
                    {
                        request.detail[0].MembershipNo = memberShipNumber;
                    }
                    else
                    {
                        lblError.Text = "Inputted 'Membership No' does not exist in your group.";
                        return;
                    }
                }
            }
        }
        else
            request.detail[0].MembershipNo = txtMembershipNo.Text;

        request.detail[0].Reason = ddlRepReason.SelectedValue;
        BindCorrectData(ref request.detail[0]);
        request.detail[0].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
        try
        {
            response = ws.SubTxn(request);
            StringBuilder sb = new StringBuilder(200);
            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("\n");
                lblError.Text = sb.ToString();
                if (response.ReferenceNo != null)
                    lblError.Text += "Kindly re-check your membership number.";
                Panel1.Visible = false;
                lblError.Visible = true;
                lblAramcoMessage.Visible = false;
                caesarInvokeError = true;
            }
            else
            {
                imgSuc.Visible = true;
                Message1.Text = "Thank you for submitting your request. Your reference number is " + response.ReferenceNo.ToString() + ". We are now validating the submitted file.";
                Panel1.Visible = false;
                Message1.Visible = true;
                lblAramcoMessage.Visible = false;
                lnkChangeOption.Visible = true;
                caesarInvokeError = false;
                caesarReferenceID = response.ReferenceNo.ToString();
            }
        }
        catch (Exception ex)
        {
            Panel1.Visible = false;
            Message1.Text = ex.Message;
            lblError.Text = "Error Encountered. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            lblError.Visible = false;
            lblAramcoMessage.Visible = false;
        }

        Label1.Visible = false;

        #endregion

        if (!caesarInvokeError)
        {
            // Send the email related to the attachments
            string username = "Unknown username";
            string fullName = "Unknown client";
            string[] addressees = UploadPublication.MembershipEmailAddresses();
            if (Session[WebPublication.Session_ClientUsername] != null)
                username = Session[WebPublication.Session_ClientUsername].ToString();
            if (Session[WebPublication.Session_ClientFullName] != null)
                fullName = Session[WebPublication.Session_ClientFullName].ToString();
            // Once done, hide the uploader from the view
            tblUploader.Visible = false;
        }
    }


        private void BindCorrectData(ref AllMbrDetail_DN MyDetails) 
	{
        string correctionReason = ddlRepReason.SelectedValue;
        switch (correctionReason)
        {
            case "REP001":
                break;
            case "REP002":
                break;
            case "REP004":
                // Wrong Member Name
                MyDetails.CorrectName = txtCorrectName.Text;
                break;
            case "REP005":
                // Wrong Date of birth
                if (txtCorrectDOB.Text != "")
                    MyDetails.CorrectDOB = DateTime.ParseExact(txtCorrectDOB.Text, "dd/MM/yyyy", null);
                break;
            case "REP006":
                // Wrong Employee No.
                MyDetails.CorrectEmployeeNo = txtEmployeeNo.Text;
                break;
            case "REP007":
                // Wrong Nationality
                MyDetails.CorrectNationality = ddlENationality.SelectedValue;
                break;
            case "REP008":
            case "REP010":
                // Wrong Saudi ID/Iqama
                MyDetails.CorrectIgamaID = txtCorrectIqamaID.Text;
                break;
            case "REP009":
                // Wrong Gender
                MyDetails.CorrectGender = ddlEGender.SelectedValue;
                break;
            case "REP012":
                MyDetails.MemberType = ddlMemberType.SelectedValue;
                if (txtEffectiveDate.Text != "")
                    MyDetails.EffectiveDate = DateTime.ParseExact(txtEffectiveDate.Text, "dd/MM/yyyy", null);
                break;
            case "REP013":
                // Wrong ID Type
                MyDetails.ID_Type = ddlIDType.SelectedValue;
                break;
            case "REP014":
                // Wrong ID Expiry Date
                if (txtIDExpDate.Text != "")
                    MyDetails.ID_Expiry_Date = DateTime.ParseExact(txtIDExpDate.Text, "dd/MM/yyyy", null);
                break;

            case "REP015":
                // Wrong Profession
                MyDetails.CCHI_Job_Code = ddProfession.SelectedValue;
                break;

            case "REP016":
                // Wrong District
                MyDetails.CCHI_City_Code = ddDistrict.SelectedValue;
                break;

            case "REP017":
                // Wrong Mobile
                MyDetails.MOBILE_NO = txtMobileNo.Text;
                MyDetails.MBR_MOBILE_NO = txtMobileNo.Text;
                MyDetails.Telephone = txtMobileNo.Text;
                break;

            case "REP018":
                MyDetails.CorrectIgamaID = txtCorrectIqamaID.Text;
                MyDetails.CorrectSponsorID = txtCorrectSponsorID.Text;
                if (txtIDExpDate.Text != "")
                    MyDetails.ID_Expiry_Date = DateTime.ParseExact(txtIDExpDate.Text, "dd/MM/yyyy", null);
                MyDetails.ID_Type = ddlIDType.SelectedValue;
                MyDetails.Marital_Status = ddlMaritalStatus.SelectedValue;
                MyDetails.CCHI_City_Code = ddDistrict.SelectedValue;
                MyDetails.CCHI_Job_Code = ddProfession.SelectedValue;
                MyDetails.CorrectNationality = ddlENationality.SelectedValue;
                MyDetails.MOBILE_NO = txtMobileNo.Text;
                MyDetails.MBR_MOBILE_NO = txtMobileNo.Text;
                MyDetails.Telephone = txtMobileNo.Text;
                break;
        }
    }

    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        fncReplaceCard();
    }

    bool Check_Member_belong_to_client(string strMemberContractNo)
    {
        if (strClientID == strMemberContractNo)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        try
        {

            string MEmberORStaff = "";
            if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(txtMembershipNo.Text.Trim())))
                {
                    RFVtxtBadgeNo.Enabled = false;
                    MEmberORStaff = txtMembershipNo.Text.Trim();

                }
                else if (!string.IsNullOrEmpty(Convert.ToString(txtBadgeNo.Text.Trim())))
                {
                    RequiredFieldValidator13.Enabled = false;
                    ServiceDepot_DNService ws = new ServiceDepot_DNService();
                    EnqMbrListInfoRequest_DN request = new EnqMbrListInfoRequest_DN();
                    EnqMbrListInfoResponse_DN response = new EnqMbrListInfoResponse_DN();
                    response = null;
                    request = new EnqMbrListInfoRequest_DN();
                    request.customerNo = strClientID;
                    request.staffNo = txtBadgeNo.Text.Trim();

                    //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
                    request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
                    //request.IqamaNo =  "5523234443";
                    //request.TotalNoOfFamily = "3";
                    request.Username = WebPublication.CaesarSvcUsername;
                    request.Password = WebPublication.CaesarSvcPassword;
                    response = ws.EnqMbrListInfo(request);
                    StringBuilder sb = new StringBuilder(200);
                    if (response.errorID != "0")
                    {
                        sb.Append(response.errorMessage).Append("\n");
                        lblError.Text = sb.ToString();
                        return;
                    }
                    else
                    MEmberORStaff = Convert.ToString(response.detail[0].membershipNo);


                }

            }
            else
            {
                RFVtxtBadgeNo.Enabled = false;
                MEmberORStaff = txtMembershipNo.Text.Trim();
            }
            MemberHelper mh = new MemberHelper(MEmberORStaff, strClientID);
            Member m = mh.GetMemberDetails();

            if (Check_Member_belong_to_client(m.ContractNumber))
            {

            if (!string.IsNullOrEmpty(Convert.ToString(m.MemberName)))
            {
                details.Visible = true;
                txtMemberName.Text = m.MemberName;
                lblcGender.Text = m.Gender;
                lblNationality.Text = m.Nationality;
                if (m.BirthDate.ToString() != ""){
                    lblDOB.Text = DateTime.ParseExact(m.BirthDate.ToShortDateString(), "dd/MM/yyyy", null).ToShortDateString(); //m.BirthDate.ToShortDateString();
                }
                lblEmpID.Text = m.EmployeeNumber;
                lblID.Text = m.SaudiIqamaID;
                lblSponsor.Text = m.SponsorID;
                lblIDType.Text = m.IDType;
                if(m.IDExpiryDate.ToString() != ""){
                    lblExpiryDate.Text = DateTime.Parse(m.IDExpiryDate.ToString(), null, System.Globalization.DateTimeStyles.None).ToShortDateString();
                }

                switch (m.MemberType.ToString().ToLower())
                {
                    case "employee":
                        LoadMemberType(int.Parse(m.ClassID),"E");
                        break;
                    default:
                        LoadMemberType(int.Parse(m.ClassID), "D");
                        break;
                }
                lblProfession.Text = m.Profession;
                lblDistrict.Text = m.District;
                lblMobile.Text = m.MOBILE;
                ResetDataReasons();
                DisableDataReasons();
               string correctionReason = ddlRepReason.SelectedValue;
        switch (correctionReason)
        {
            case "REP001":
                // Lost Card
                lblAsterix.Visible = false;
                Button1.Enabled = true;
                rfvName.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvMemberType.Enabled = false;
                rfvEffectiveDate.Enabled = false;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType) { 
                CorrectGender.Visible=false;
                CorrectNationality.Visible=false;
                CorrectTitle.Visible=false;
                CorrectName.Visible=false;
                CorrectDateOfBirth.Visible=false;
                IDType.Visible=false;
                CorrectIqamaID.Visible=false;
                CorrectSponsorID.Visible=false;
                EmployeeNo.Visible=false;
                MemberType.Visible=false;
                IDExpiryDate.Visible=false;
                MaritalStatus.Visible=false;
                Profession.Visible=false;
                District.Visible=false;
                MobileNo.Visible = false;
                EffectiveDate.Visible = false;
                }
                break;

            case "REP002":
                // Damaged Card
                lblAsterix.Visible = false;
                Button1.Enabled = true;
                rfvName.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvMemberType.Enabled = false;
                rfvEffectiveDate.Enabled = false;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP004":
                // Wrong Member Name
                lblAsterix.Visible = true;
                Button1.Enabled = true;
                txtCorrectName.Enabled = true;
                txtCorrectName.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                rfvName.Enabled = true;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                txtCorrectName.Focus();
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = true;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP005":
                // Wrong Date of birth
                lblAsterix.Visible = true;
                Button1.Enabled = true;
                txtCorrectDOB.Enabled = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = true;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                txtCorrectDOB.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                txtCorrectDOB.Focus();
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = true;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP006":
                // Wrong Employee No.
                lblAsterix.Visible = false;
                txtEmployeeNo.Enabled = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = true;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                txtEmployeeNo.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                txtEmployeeNo.Focus();
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = true;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP007":
                // Wrong Nationality
                lblAsterix.Visible = false;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = true;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                Button1.Enabled = true;
                ddlENationality.Enabled = true;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = true;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP008":
            case "REP010":
                // Wrong Saudi ID/Iqama
                lblAsterix.Visible = true;
                Button1.Enabled = true;
                txtCorrectIqamaID.Enabled = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = true;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                txtCorrectIqamaID.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                txtCorrectIqamaID.Focus();
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = true;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP009":
                // Wrong Gender
                lblAsterix.Visible = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = true;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                Button1.Enabled = true;
                ddlEGender.Enabled = true;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = true;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP012":
                // Wrong Member Type
                ddlMemberType.Enabled = true;
                rfvMemberType.Enabled = true;
                txtEffectiveDate.Enabled = true;
                txtEffectiveDate.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                rfvEffectiveDate.Enabled = true;
                lblAsterix.Visible = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                Button1.Enabled = true;
                ddlEGender.Enabled = false;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = true;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = true;
                }
                break;

            case "REP013":
                // Wrong ID Type
                lblAsterix.Visible = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = true;
                rfvIDExpDate.Enabled = false;
                Button1.Enabled = true;
                ddlIDType.Enabled = true;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = true;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP014":
                // Wrong ID Expiry Date
                lblAsterix.Visible = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = true;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                Button1.Enabled = true;
                txtIDExpDate.Enabled = true;
                txtIDExpDate.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                txtIDExpDate.Focus();
                break;

            case "REP015":
                // Wrong Profession
                lblAsterix.Visible = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = true;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                Button1.Enabled = true;
                ddProfession.Enabled = true;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = true;
                    District.Visible = false;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP016":
                // Wrong District
                lblAsterix.Visible = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = true;
                rfvMobile.Enabled = false;
                revMobile.Enabled = false;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                Button1.Enabled = true;
                ddDistrict.Enabled = true;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = true;
                    MobileNo.Visible = false;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP017":
                // Wrong Mobile
                lblAsterix.Visible = true;
                rfvName.Enabled = false;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = false;
                rfvIqamaID.Enabled = false;
                rfvGender.Enabled = false;
                rfvDistrict.Enabled = false;
                rfvMobile.Enabled = true;
                revMobile.Enabled = true;
                rfvMaritalStatus.Enabled = false;
                rfvProfession.Enabled = false;
                rfvIDType.Enabled = false;
                rfvIDExpDate.Enabled = false;
                ddlMemberType.Enabled = false;
                rfvEffectiveDate.Enabled = false;
				rfvMemberType.Enabled = false;
                Button1.Enabled = true;
                txtMobileNo.Enabled = true;
                txtMobileNo.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                txtMobileNo.Focus();
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {
                    CorrectGender.Visible = false;
                    CorrectNationality.Visible = false;
                    CorrectTitle.Visible = false;
                    CorrectName.Visible = false;
                    CorrectDateOfBirth.Visible = false;
                    IDType.Visible = false;
                    CorrectIqamaID.Visible = false;
                    CorrectSponsorID.Visible = false;
                    EmployeeNo.Visible = false;
                    MemberType.Visible = false;
                    IDExpiryDate.Visible = false;
                    MaritalStatus.Visible = false;
                    Profession.Visible = false;
                    District.Visible = false;
                    MobileNo.Visible = true;
                    EffectiveDate.Visible = false;
                }
                break;

            case "REP018":
                // New CCHI Requirements
                lblAsterix.Visible = true;
                rfvDOB.Enabled = false;
                rfvEmployeeNo.Enabled = false;
                rfvNationality.Enabled = true;
                rfvIqamaID.Enabled = true;
                rfvDistrict.Enabled = true;
                rfvMobile.Enabled = true;
                revMobile.Enabled = true;
                rfvEffectiveDate.Enabled = false;
                rfvMemberType.Enabled = false;
                rfvMaritalStatus.Enabled = true;
                rfvProfession.Enabled = true;
                rfvIDType.Enabled = true;
                rfvIDExpDate.Enabled = true;
                rfvGender.Enabled = false;
                Button1.Enabled = true;
                txtCorrectIqamaID.Enabled = true;
                txtMobileNo.Enabled = true;
                txtMobileNo.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                txtMobileNo.Focus();
                ddDistrict.Enabled = true;
                ddProfession.Enabled = true;
                txtIDExpDate.Enabled = true;
                txtIDExpDate.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                txtIDExpDate.Focus();
                ddlENationality.Enabled = true;
                ddlIDType.Enabled = true;
                ddlMaritalStatus.Enabled = true;
                txtCorrectIqamaID.BackColor = ColorTranslator.FromHtml("#FFFFC0");
                txtCorrectIqamaID.Focus();
                
                break;
        }

            }

            }
            else
            {
                lblError.Text = "Inputted 'Membership No' does not exist in your group.";

            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    public void LoadMemberType(int intClsID, string grp_Type)
    {
        ServiceDepot_DNService ws;
        EnqContMbrTypeListRequest_DN request;
        EnqContMbrTypeListResponse_DN response;

        request = new EnqContMbrTypeListRequest_DN();
        ws = new ServiceDepot_DNService();

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        long ls_txnid = WebPublication.GenerateTransactionID();
        request.transactionID = ls_txnid;

        request.contNo = strClientID;
        //intClsID = Convert.ToInt32(ddlELevelOfCover.SelectedValue.ToString());
        request.contCls = intClsID;
        request.grp_Type = grp_Type; //"E";

        response = ws.EnqContMbrTypeList(request);
        lblError.Text = "";
        ddlMemberType.Items.Clear();

        if (response.status == "0")
        {
            ddlMemberType.Items.Add(new ListItem("-- Select --", "0"));
            for (int i = 0; i < response.detail.Length; i++)
            {
                ddlMemberType.Items.Add(new ListItem(response.detail[i].mbrTypeDesc, response.detail[i].mbrType));
            }
        }
        else
        {
            ddlMemberType.Items.Add(new ListItem("-- Not Applicable --", "0"));
            lblError.Text = "Level of cover is not applicable for selected member";
        }
    }

    private void BindDdlMemberType(string grptype)
    {
        ListItem ListItem0 = new ListItem();
        ListItem0.Text = "-Select-";
        ListItem0.Value = "0";

        ReqMbrTypeListRequest_DN request = new ReqMbrTypeListRequest_DN();
        request.TransactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.grp_Type = grptype; //"E";
        ws = new ServiceDepot_DNService();
        ReqMbrTypeListResponse_DN response;
        response = ws.ReqMbrTypeList(request);
        ddlMemberType.DataSource = response.detail;
        ddlMemberType.DataBind();
        ddlMemberType.Items.Insert(0, ListItem0);
    }

    private void BindDdlProfession()
    {
        ListItem ListItem0 = new ListItem();
        ListItem0.Text = "-Select-";
        ListItem0.Value = "0";

        ReqProfListRequest_DN request = new ReqProfListRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        ws = new ServiceDepot_DNService();
        ReqProfListResponse_DN response;
        response = ws.ReqProfList(request);

        ProfListDetail_DN[] MyCodes = new ProfListDetail_DN[response.detail.Length + 1];

        response.detail.CopyTo(MyCodes, 1);
        MyCodes[0] = new ProfListDetail_DN();
        MyCodes[0].profCode = "";
        MyCodes[0].profName = "-Select-";
        ddProfession.DataSource = MyCodes;
        ddProfession.DataBind();
        ddProfession.Items.Insert(0, ListItem0);
    }

    private void BindDdlDistrict()
    {
        ListItem ListItem0 = new ListItem();
        ListItem0.Text = "-Select-";
        ListItem0.Value = "0";

        ReqDistListRequest_DN request = new ReqDistListRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        ws = new ServiceDepot_DNService();
        ReqDistListResponse_DN response;
        response = ws.ReqDistList(request);
        DistListDetail_DN[] MyDistricts = new DistListDetail_DN[response.detail.Length + 1];

        response.detail.CopyTo(MyDistricts, 1);
        MyDistricts[0] = new DistListDetail_DN();
        MyDistricts[0].distCode = "";
        MyDistricts[0].distName = "-Select-";
        ddDistrict.DataSource = MyDistricts;
        ddDistrict.DataBind();
        ddDistrict.Items.Insert(0, ListItem0);
    }

    private void BindDdlENationality(bool IsGCC)
    {
        NationalityControl MyNationalityC = new NationalityControl();
        System.Collections.Generic.List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
        MyNationalities.Insert(0, new Nationality(0, "", "- Select -", false));
        ddlENationality.DataValueField = "Code";
        ddlENationality.DataTextField = "Nationality1";
        ddlENationality.DataSource = MyNationalities;
        ddlENationality.DataBind();
    }

    private void LoadCaesarSourcedData()
    {
        BindDdlProfession();
        BindDdlDistrict();
        BindDdlENationality(false);
    }

    protected void ddlRepReason_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            DesableValidation();
    }

    protected void ddlIDType_SelectedIndexChanged(object sender, EventArgs e)
    {
              
        txtCorrectIqamaID.Text = "";
        switch (ddlIDType.SelectedValue)
        {
            case "1":
                txtCorrectIqamaID.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[1][0-9]{9}";
                rfvSuadiID.Text = "Incorrect National ID";
                BindDdlENationality(false);
                break;
            case "2":
                txtCorrectIqamaID.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[2][0-9]{9}";
                rfvSuadiID.Text = "Incorrect Iqama ID";
                BindDdlENationality(false);
                break;
            case "3":
                txtCorrectIqamaID.MaxLength = 15;
                rfvSuadiID.ValidationExpression = "([A-Z]|[a-z]|[0-9]){6,15}";
                rfvSuadiID.Text = "Incorrect Passport Number";
                BindDdlENationality(true);
                break;
            case "4":
                txtCorrectIqamaID.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[3-5][0-9]{9}";
                rfvSuadiID.Text = "Incorrect Entry Number";
                BindDdlENationality(false);
                break;
        }
    }
    protected void DesableValidation()
    {
        rfvName.Enabled = false;
        rfvDOB.Enabled = false;
        rfvEmployeeNo.Enabled = false;
        rfvNationality.Enabled = false;
        rfvIqamaID.Enabled = false;
        rfvGender.Enabled = false;
        rfvDistrict.Enabled = false;
        rfvMobile.Enabled = false;
        revMobile.Enabled = false;
        rfvMaritalStatus.Enabled = false;
        rfvProfession.Enabled = false;
        rfvIDType.Enabled = false;
        rfvIDExpDate.Enabled = false;
        rfvEffectiveDate.Enabled = false;
        rfvMemberType.Enabled = false;

    }
}
