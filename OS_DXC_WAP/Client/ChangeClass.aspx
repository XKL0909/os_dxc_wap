<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" EnableEventValidation="false" Inherits="ChangeClass" Codebehind="ChangeClass.aspx.cs" %>

<%--<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>--%>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>
<%--<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>--%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server">

    <telerik:RadScriptManager ID="ScriptManager1" runat="server" EnableTheming="True">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js"></asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js"></asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js"></asp:ScriptReference>
        </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            //On insert and update buttons click temporarily disables ajax to perform upload actions  
            function conditionalPostback(sender, args) {
                var theRegexp = new RegExp("\.UpdateButton$|\.PerformInsertButton$|\.UploadSubmit$|\.btnUpload$", "ig");

                if (args.get_eventTarget().match(theRegexp)) {
                    var upload = $find(window['UploadId']);

                    //AJAX is disabled only if file is selected for upload  
                    //if (upload.getFileInputs()[0].value != "") {
                        args.set_enableAjax(false);
                    //}
                }
            }
        </script>
    </telerik:RadScriptBlock>
    <div>
        <h1>
            <asp:Label ID="lblAdd_Employee" runat="server" Text="Change Branch" Font-Size="Large"
                Font-Names="Arial"></asp:Label>
        </h1>
        <div align="right">
            <asp:HyperLink ID="lnkBatchUpload" runat="server" Font-Size="Small" NavigateUrl="~/Client/BatchUpload.aspx?OptionType=ChangeBranch" Target="_blank" Visible="true">Batch Upload</asp:HyperLink>
        </div>
        <br />
        <asp:Label ID="Message1" runat="server" Width="527px"  ForeColor="Red" Font-Size="Small"></asp:Label>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Black" Text="Fields marked with asterisk [ * ]  are mandatory."></asp:Label>

        <asp:Panel ID="Panel1" runat="server" Width="95%">
            <table>
                <tr>
                    <td>
                        <span style="color: #ff0000">*</span>Membership Number
                    </td>
                    <td style="width: 223px">
                        <asp:TextBox ID="txtMembershipNo" runat="server" CssClass="textbox"
                            MaxLength="8"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server"
                            ControlToValidate="txtMembershipNo" Display="Dynamic"
                            ErrorMessage="RequiredFieldValidator" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                        <br />
                    </td>
                    <td style="width: 200px" valign="top">
                        <asp:Button ID="Button1" runat="server" CssClass="submitButton"
                            OnClick="Button1_Click1" Text="Get Details" CausesValidation="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
            <div runat="server" id="details" visible="false">
              <%-- 
                  <asp:UpdatePanel runat="server" ID="updatePanel2">
                    <ContentTemplate>
                        <div id="entryForm">
                            <table>
                                <tr>
                                    <td style="width: 223px">
                                        <span style="color: #ff0000">*</span><asp:Label ID="lblCompleteName"
                                            runat="server" Text="Complete Name(First, Middle, Last)" Width="210px"></asp:Label>
                                    </td>
                                    <td style="width: 200px">
                                        <asp:Label ID="txtMemberName" runat="server"
                                            MaxLength="40" Style="font-weight: 700"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 213px; height: 25px">
                                        <span style="color: #ff0000">*</span><asp:Label ID="lblOptionOld" runat="server"
                                            Text="Old Branch" Width="125px"></asp:Label>
                                    </td>
                                    <td style="width: 200px; height: 25px">
                                        <div class="styled-select">
                                            <asp:Label ID="ddlBranchCodeOld" runat="server"
                                                Style="font-weight: 700">
                            
                                            </asp:Label>
                                            <asp:Label ID="ddlBranchCodeOldValue" Visible="false" runat="server" Style="font-weight: 700">
                            
                                            </asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 213px; height: 25px">
                                        <span style="color: #ff0000">*</span><asp:Label ID="lblOptionNew" runat="server"
                                            Text="New Branch" Width="125px"></asp:Label>
                                    </td>
                                    <td style="width: 200px; height: 25px">
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlBranchCodeNew" runat="server" BackColor="#eff6fc">
                                                <asp:ListItem>-Select-</asp:ListItem>
                                                <asp:ListItem Value="10290300">Branch</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlBranchCodeNew"
                                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" Width="250px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 213px; height: 25px;">
                                        <span style="color: #ff0000">*</span><asp:Label ID="lbl_Reason" runat="server" Text="Reason for change"
                                            Width="148px"></asp:Label>
                                    </td>
                                    <td style="width: 200px; height: 25px;">
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlChangeReason" runat="server" BackColor="#eff6fc">
                                                <asp:ListItem>-Select-</asp:ListItem>
                                                <asp:ListItem Value="BRA001">Member was relocated to another jobsite</asp:ListItem>
                                                <asp:ListItem Value="BRA002">Other/Customer reason</asp:ListItem>
                                                <asp:ListItem Value="BRA003">Due to Budget/Cost Centre allocation</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlChangeReason"
                                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" Width="162px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 213px">
                                        <span style="color: #ff0000">*</span><asp:Label ID="lblDateOfBirth" runat="server"
                                            Text="Effective Date" Width="100px"></asp:Label>
                                    </td>
                                    <td style="width: 200px">
                                        <asp:TextBox CssClass="textbox" ID="txtDateOfEffect" runat="server" onclick="scwShow(this,event);"
                                            onfocus="this.blur();" Style="cursor: default;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDateOfEffect"
                                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDateOfEffect"
                                            Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{2}/\d{2}/\d{4}">Invalid Date</asp:RegularExpressionValidator>
                                    </td>
                                </tr>

                            </table>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                  --%>


                <table>
                    <tr bordercolor="white" bgcolor="white">
                        <td colspan="2" align="right">
                            <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
                                runat="server" id="tblUploader">
                                <tr>
                                    <td align="left">
                                        <span style="color: #ff3300"><strong>*</strong></span><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                                    </td>
                                    <td align="right">
                                        <a href="<%= Page.ResolveUrl("~/Docs/ReqDocs2012.pdf") %>" target="_blank">Required Document List</a>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="2" align="left">
                                        <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .pmp) 5MB limit per file" /><br />
                                        <%--<BUPA:Uploader runat="server" ID="uploader" />--%>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Button CssClass="submitButton" ID="btnChangeClass" runat="server" OnClick="btnChangeClass_Click" Text="Submit Request" Visible="False" BackColor="Control" ValidationGroup="1" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />

        </asp:Panel>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel15" runat="server" Skin="Default">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="radGridDependents">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radGridDependents" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="NewClassList">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="NewClassList" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel15" LoadingPanelID="RadAjaxLoadingPanel15" ClientEvents-OnRequestStart="conditionalPostback">
            <telerik:RadGrid ID="radGridDependents" AutoGenerateColumns="False" runat="server" OnNeedDataSource="radGridDependents_NeedDataSource1"
                OnUpdateCommand="radGridDependents_UpdateCommand"
                OnItemUpdated="radGridDependents_ItemUpdated"
                OnItemDataBound="radGridDependents_ItemDataBound">

                <MasterTableView DataKeyNames="memberID" CommandItemDisplay="Top" EditMode="InPlace">
                    <Columns>
                        <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                        </telerik:GridEditCommandColumn>
                        <telerik:GridTemplateColumn UniqueName="MyMemberID" HeaderText="Saudi/Iqama ID">
                            <ItemTemplate>
                                <asp:Label ID="LabelMemberID" runat="server" Enabled="false"
                                    Text='<%# DataBinder.Eval(Container.DataItem, "memberID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--membershipNo--%><telerik:GridTemplateColumn UniqueName="MymembershipNo" HeaderText="Membership No">
                            <ItemTemplate>
                                <asp:Label ID="LabelmembershipNo" runat="server" Enabled="false"
                                    Text='<%# DataBinder.Eval(Container.DataItem, "membershipNo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--Member Name--%><telerik:GridTemplateColumn UniqueName="MymemberName" HeaderText="Member Name">
                            <ItemTemplate>
                                <asp:Label ID="LabelmemberName" runat="server" Enabled="false"
                                    Text='<%# DataBinder.Eval(Container.DataItem, "memberName") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn UniqueName="TemplateColumn" HeaderText="Current class">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server"
                                    Text='<%# DataBinder.Eval(Container.DataItem, "ClassID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <table>
                                    <tr>
                                        <td>Current Class</td>
                                        <td>New Class</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelCurrentClassId" runat="server"
                                                Text='<%# DataBinder.Eval(Container.DataItem, "ClassID") %>'>
                                            </asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="NewClassList" runat="server" />
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlChangeClassReason" runat="server" BackColor="#eff6fc">
                                                <asp:ListItem Value="0">Select from the list</asp:ListItem>
                                                <asp:ListItem Value="CLA001">Member Promotions</asp:ListItem>
                                                <asp:ListItem Value="CLA002">Member Downgrade</asp:ListItem>
                                                <asp:ListItem Value="CLA003">Product Modification/Additional Class</asp:ListItem>
                                                <asp:ListItem Value="CLA004">Group/Customer Request</asp:ListItem>
                                                <asp:ListItem Value="CLA005">Master List Error</asp:ListItem>
                                                <asp:ListItem Value="CLA006">Others</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <%--ClassNewEffectiveDate--%><telerik:GridTemplateColumn UniqueName="ClassNewEffectiveDate" HeaderText="Eff. Date">
                            <ItemTemplate>
                                <asp:Label ID="LabelClassEffDate" runat="server"
                                    Text='<%# DataBinder.Eval(Container.DataItem, "ClassEffDate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <table>
                                    <tr>
                                        <td>Current</td>
                                        <td>New</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelClassEffDateOld" runat="server"
                                                Text='<%# DataBinder.Eval(Container.DataItem, "ClassEffDate") %>'>
                                            </asp:Label></td>
                                        <td>
                                            <asp:TextBox CssClass="textbox" ID="txtNewDateOfEffect" runat="server" onclick="scwShow(this,event);"
                                                onfocus="this.blur();" Style="cursor: default;"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNewDateOfEffect"
                                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtNewDateOfEffect"
                                                Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{2}/\d{2}/\d{4}">Invalid Date</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <%--Expiry date--%><telerik:GridTemplateColumn UniqueName="MyendDate" HeaderText="Expiry date">
                            <ItemTemplate>
                                <asp:Label ID="LabelendDate" runat="server" Enabled="false"
                                    Text='<%# DataBinder.Eval(Container.DataItem, "endDate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <%--Supportive document--%><telerik:GridTemplateColumn UniqueName="MySupportiveDoc" HeaderText="Supporting document">
                            
                             <ItemTemplate>
                                <asp:Label ID="LabelRefferenceNumber" runat="server" Enabled="false"
                                    Text="">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                            <%--    <BUPA:Uploader runat="server" ID="uploaderInGridRow" />--%>

                                <telerik:RadUpload ID="RadUpload1" runat="server" />
                                <%--<asp:Button ID="UploadSubmit" runat="server" OnClick="buttonSubmit_Click" Text="Upload"
                                    CausesValidation="false" />--%>

                                <%--<telerik:RadAsyncUpload TargetFolder="~/Uploads/" runat="server" ID="AsyncUpload1" ChunkSize="1048576" HideFileInput="false" Visible="false"  />--%>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>



                        <%-- <telerik:GridBoundColumn DataField="distCode" FilterControlAltText="Filter column column" HeaderText="District">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="countryName" FilterControlAltText="Filter column column" HeaderText="Country">
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="memberName" FilterControlAltText="Filter column column" HeaderText="Member Name">
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="ClassID" FilterControlAltText="Filter column column" HeaderText="Current class"  >
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="ClassEffDate" FilterControlAltText="Filter column column" HeaderText="Current class Effective date">
                        </telerik:GridBoundColumn>--%>
                        <%--<telerik:GridBoundColumn DataField="memberID" FilterControlAltText="Filter column column" HeaderText="Saudi/Iqama ID" ForceExtractValue="Always">
                        </telerik:GridBoundColumn>--%>
                        <%-- <telerik:GridBoundColumn DataField="endDate" FilterControlAltText="Filter column column" HeaderText="Expiry date">
                        </telerik:GridBoundColumn>--%>
                        <%-- <telerik:GridBoundColumn DataField="profCode" FilterControlAltText="Filter column column" HeaderText="Profession">
                        </telerik:GridBoundColumn>--%>
                        <%-- <telerik:GridBoundColumn DataField="mobileNo" FilterControlAltText="Filter column column" HeaderText="Mobile number">
                        </telerik:GridBoundColumn>--%>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn ButtonType="ImageButton" />
                    </EditFormSettings>
                </MasterTableView>

            </telerik:RadGrid>
        </telerik:RadAjaxPanel>
    </div>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>

   <asp:TextBox ID="txtBadgeNo" runat="server"></asp:TextBox>

    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<asp:HyperLink ID="lnkChangeOption" runat="server"
        Font-Size="Small" NavigateUrl="~/Client/changebranch.aspx" Visible="False">Change branch for another Employee</asp:HyperLink>
    &nbsp;
    
    <br />
</asp:Content>
<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>

</asp:Content>
