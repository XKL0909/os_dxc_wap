﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master"  AutoEventWireup="true" Inherits="TrackInfo"
    EnableViewState="false" Codebehind="TrackInfo.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">


    <script type='text/JavaScript' src='scw.js'></script>
    <style type="text/css">
        #Content td
        {
            font-family: arial;
            font-size: 12px;
            color: #606060;
        }
        .th
        {
            font-weight: bold;
            color: #666;
            font-size: 12px;
        }
        
        </style>
    <style>
        @media print
        {
            .noPrint
            {
                display: none;
            }
            .Hidden
            {
                display: inline;
            }
        }
        
        .Hidden
        {
            display: none;
        }
    </style>
    <style type="text/css">
        body
        {
            font-family: Arial,Sans-serif;
            font-size: 80%;
        }
        caption
        {
            padding-bottom: 5px;
            font-weight: bold;
        }
        thead th, tfoot td
        {
            background: #ddd;
        }
        tr.ruled
        {
            background: #9cf;
        }
        table
        {
            border-collapse: collapse;
        }
        th, td
        {
            border-collapse: collapse;
        }
        #mytable tr.ruled
        {
            background: #333;
            color: #ccc;
        }
    </style>
    <script type="text/javascript" src="../includes/tableruler.js"></script>
    <script type="text/javascript">
        window.onload = function () { tableruler(); }
    </script>
    
    
    <fieldset style="padding:10px">
    <legend><h1><asp:Label ID="lblAdd_Employee" runat="server" Text="Track Info" Font-Size="Large"
                    Font-Names="Arial"></asp:Label></h1></legend>
                    
                    
    <table width="100%">
        <tr align="right">
            <td align="left">
              
            </td>
            <td>
                <a href="#" onclick="window.print()">
                    &nbsp;</a></td>
        </tr>
    </table>

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
    <div class="noPrint">
        <table>
            <tr>
                <td style="height: 22px" valign="top">
                    <asp:Label ID="lbl1" Text="Reference Number" runat="server"></asp:Label>
                </td>
                <td style="height: 22px" valign="top">
                    <asp:TextBox CssClass="textbox" ID="txtSearchRef" runat="server" ValidationGroup="RefGroup"></asp:TextBox>
                </td>
                <td style="height: 22px" valign="top">
                    <asp:Button CssClass="submitButton" ID="btnSearchRef" runat="server" Text="Search by Reference" OnClick="btnSearchRef_Click"
                        ValidationGroup="RefGroup"  />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSearchRef"
                        ErrorMessage="RequiredFieldValidator" ValidationGroup="RefGroup">Please fill in the Reference No. </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:Label ID="Label2" Text="Date Submitted" runat="server"></asp:Label>
                </td>
                <td valign="top">
                    <asp:TextBox CssClass="textbox" ID="txtSearchDate" runat="server" onclick="scwShow(this,event);"
                        Style="cursor: default;" ValidationGroup="1"></asp:TextBox>
                </td>
                <td valign="top">
                    <asp:Button CssClass="submitButton" ID="btnSearchDate" runat="server" Text="Search by Date" OnClick="btnSearchDate_Click"
                        ValidationGroup="DateGroup"  />
						<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtSearchDate"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="DateGroup"
                        Display="Dynamic"  Width="176px">Please fill in the Date</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:Label ID="Label3" Text="Staff Number" runat="server"></asp:Label>
                </td>
                <td valign="top">
                    <asp:TextBox CssClass="textbox" ID="txtSearchStaff" runat="server" ValidationGroup="StaffGroup"></asp:TextBox>
                </td>
                <td valign="top">
                    <asp:Button CssClass="submitButton" ID="btnSearchStaff" runat="server" Text="Search by Staff No." OnClick="btnSearchStaff_Click"
                        ValidationGroup="StaffGroup"  />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSearchStaff"
                        ErrorMessage="RequiredFieldValidator" ValidationGroup="StaffGroup">Please fill in the Staff No. </asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
       
    </div>
    <div id="DivProgress" style="position: absolute; background-color: Transparent; margin-bottom: 15px;
        margin-left: 15px;">
        <asp:UpdateProgress ID="UpdateProgress2" runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate>
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size="1">Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <br />
            <br />
            <div id="CoverageListReport" visible="false" style="font-family: Verdana; width: 100%;"
                runat="server">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label>
    <br />
    <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
    <asp:TextBox CssClass="textbox" ID="txtBox_output" Visible="false" runat="server"></asp:TextBox>
     <br />
        <uc2:OSNav ID="OSNav1" runat="server" />
        <br />
         </fieldset>
    </asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p>
        View the latest update and status of your maintenance request</p>
       
</asp:Content>
