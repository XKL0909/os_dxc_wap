using System;
using System.Web.UI.WebControls;
using System.Text;
using OS_DXC_WAP.CaesarWS;

public partial class CtntMyCoverage : System.Web.UI.Page
{
    string strClientID;
    private ServiceDepot_DNService ws;

    public CtntMyCoverage()
    {
        this.Load += new EventHandler(CtntMyCoverage_Load);
    }

    void CtntMyCoverage_Load(object sender, EventArgs e)
    {
        Master.SearchButton.Click += SearchButton_Click;
        Master.ddlStaffList.SelectedIndexChanged += DDLDependentList_SelectedIndexChanged;
        Master.SearchOpt.SelectedIndexChanged += rdbSearchOption_SelectedIndexChanged;
        strClientID = Session["ClientID"].ToString();
        Master.ddlStaffList.Visible = false;
        Master.PageTitle.Text = "My Coverage";
    }
    protected void rdbSearchOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Master.ddlStaffList.Visible == true)
        {
            Master.ddlStaffList.Visible = false;
        }
        Master.SearchText.Text = "";
        Message1.Text = "";
        if (UpdatePanel2.Visible == true)
            UpdatePanel2.Visible = false;
    }

    protected void DDLDependentList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strMemberExistStatus;
        if (Master.ddlStaffList.SelectedValue != "0")
        {
            strMemberExistStatus = CheckMemberExist(Master.ddlStaffList.SelectedValue);
            if (strMemberExistStatus != "Exist")
            {
                Message1.Text = strMemberExistStatus;
                Message1.Visible = true;
                UpdatePanel2.Visible = false;
                Master.ddlStaffList.Visible = true;
            }
            else
            {
                DisplayProdCov(Master.ddlStaffList.SelectedValue);
                Master.ddlStaffList.Visible = true;
                lblMemberName.Visible = false;
                lblMember_Name.Visible = false;
                UpdatePanel2.Visible = true;
                Message1.Visible = false;
            }
        }
        else
        {
            Message1.Text = "Please select the valid name from the drop down";
            Message1.Visible = true;
            UpdatePanel2.Visible = false;
            Master.ddlStaffList.Visible = true;
        }
    }


    private void SearchButton_Click(object sender, EventArgs e)
    {
        string strMemberExistStatus;
        strMemberExistStatus = "";
        if (Master.SearchOpt.Text != "Staff No" && Master.SearchOpt.Text != "Badge No")
        {
            strMemberExistStatus = CheckMemberExist(Master.SearchText.Text);
            if (strMemberExistStatus != "Exist")
            {
                Message1.Text = strMemberExistStatus;
                Master.ddlStaffList.Visible = false;
                UpdatePanel2.Visible = false;
                Message1.Visible = true;
            }
            else
                DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);
        }
        else
            DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);
    }

    private void DisplayMemberExistForClientResult(string SearchText, string SearchType)
    {
        ws = new ServiceDepot_DNService();
        EnqMbrListInfoRequest_DN request;
        EnqMbrListInfoResponse_DN response;
        try
        {
            response = null;
            request = new EnqMbrListInfoRequest_DN();
            request.customerNo = strClientID;

            if (SearchType == "Membership No")
                request.membershipNo = SearchText;
            else
                request.staffNo = SearchText;

            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqMbrListInfo(request);

            if (response.detail == null)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = "Inputted '" + Master.SearchOpt.Text + "' does not exist in your group.";
                UpdatePanel2.Visible = false;
                Message1.Visible = true;
            }
            else
            {
                Master.ddlStaffList.Items.Clear();
                ListItem DepList0 = new ListItem();
                DepList0.Text = "Please select from the list";
                DepList0.Value = "0";
                Master.ddlStaffList.Items.Add(DepList0);

                foreach (MbrListDetail_DN dtl in response.detail)
                {
                    ListItem DepList = new ListItem();
                    DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                    DepList.Value = dtl.membershipNo.ToString();
                    Master.ddlStaffList.Items.Add(DepList);
                }

                Master.ddlStaffList.Visible = true;
                Message1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Message1.ForeColor = System.Drawing.Color.Red;
            Message1.Text = ex.Message;
            UpdatePanel2.Visible = false;
            Message1.Visible = true;
        }
    }

    private void DisplayProdCov(string MemberNo)
    {
        ws = new ServiceDepot_DNService();
        EnqProdCovInfoRequest_DN request;
        EnqProdCovInfoResponse_DN response;
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqProdCovInfoRequest_DN();
            request.membershipNo = MemberNo;
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqProdCovInfo(request);
            if (response.className != null)
            {
                lblMembershipNo.Text = request.membershipNo;
                lblMemberName.Text = request.membershipNo; // strMemberName;
                lblClassName.Text = response.className;
                lblCustomerName.Text = response.companyName;
                DisplayCoverageListResult(response);
            }
            else
            {
                UpdatePanel2.Visible = false;
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            UpdatePanel2.Visible = false;
            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
        }
    }

    private void DisplayCoverageListResult(EnqProdCovInfoResponse_DN response)
    {
        StringBuilder sbResponse = new StringBuilder(2200);
        if (response.status == "0")
        {
            sbResponse.Append(" <table class='eCommerceTable' width='100%'><tr><td>Benefit Type</td>   <td>Benefit Limit</td></tr>");
            sbResponse.Append("<tr class='grayRow'><td>OverAll Annual Limit</td><td>" + response.proratedOverallAnnualLimit.ToString("C").Replace("�", "") + "</td>");
            response.proratedOverallAnnualLimit.ToString();
            int _row = 0;
            foreach (BenefitTypeLimitDetail_DN dtl in response.detail)
            {
                _row = _row + 1;
                if (_row % 2 == 0)
                {
                    sbResponse.Append("<tr class='grayRow'>");
                }
                else
                {
                    sbResponse.Append("<tr>");
                }
                sbResponse.Append("<td>" + dtl.detail_BenefitType + "</td>");
                //Previous code to display limit
                //if (dtl.detail_ProratedLimit != -1)
                //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit.ToString("C").Replace("�", "") + "</td></tr>");
                //else
                //    sbResponse.Append("<td>Not Covered</td></tr>");

                //To display limit from new field
                sbResponse.Append("<td>" + dtl.detail_LimitDisplay + "</td></tr>");
            }
            sbResponse.Append("</table>");
            CoverageListReport.Visible = true;
            CoverageListReport.InnerHtml = sbResponse.ToString();
        }
        else
        {
        }
    }

    private string CheckMemberExist(string MemberNo)
    {
        ws = new ServiceDepot_DNService();
        ChkMbrExistRequest_DN request;
        ChkMbrExistResponse_DN response;
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new ChkMbrExistRequest_DN();
            request.membershipNo = MemberNo;
            request.iqamaNo = "";
            request.totalNoOfFamily = "";
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ChkMbrExist(request);
            if (response.memberName != null)
            {
                return "Exist";
            }
            else
            {
                return response.errorMessage[0];
            }
        }
        catch (Exception ex)
        {
            return "Error";
        }
    }
}
