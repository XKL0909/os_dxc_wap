﻿using Caesar;
using OnlineServices.CCHI.Dto;
using OnlineServices.CCHI.Enums;
using OnlineServices.CCHI.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;

public partial class CCHIValidatorNew : System.Web.UI.Page
{
    private string _reqId, _contractNo;
    private CCHIService _cchiService;
    private RequestManager _reqManager;
    protected string lblMessge = "none";
    private CaesarManager _ceaserManager;
    private string _contractId;
    private UnifiedRequestService _requestService;
    private UnifiedSettingService _settingService;
    private UnifiedContractWhiteListService _contractNoSetting;

    private AddMemberRequestService _addMemberRequestService;
    private AddMemberRequestDetailService _addMemberRequestDetailService;


    private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public CCHIValidatorNew()
    {
        log4net.Config.XmlConfigurator.Configure();
        _cchiService = new CCHIService();
        _reqManager = new RequestManager();
        _ceaserManager = new CaesarManager();
        _requestService = new UnifiedRequestService();
        _settingService = new UnifiedSettingService();
        _contractNoSetting = new UnifiedContractWhiteListService();
        _addMemberRequestService = new AddMemberRequestService();
        _addMemberRequestDetailService = new AddMemberRequestDetailService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Session["Reset"] = true;
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);

            CheckSessionExpired();
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
        }
        catch
        {

        }
        //Encrypty Query string
        //_reqId = Request.QueryString["requestId"];
        _reqId = Convert.ToString(Session["RequestID"]);
        _contractNo = Convert.ToString(Session["ClientID"]);
        if (string.IsNullOrEmpty(_contractNo))
            Response.Redirect("~/Default.aspx");
        var setting = _settingService.FindByKey("CCHIValidationRequired");
        var contractSetting = _contractNoSetting.FindByContractID(_contractNo.ToString());
        if (setting != null && setting.SValue == "1")
        {

            if (!string.IsNullOrEmpty(_reqId))
            {
                if (IsValidRequest())
                {
                    var checkRequest = _addMemberRequestDetailService.Get(Convert.ToInt64(_reqId));
                    if (checkRequest != null && checkRequest.Count() > 0)
                    {
                        var checkStatus = checkRequest.Any(x => x.Status != 3);

                        if (!checkStatus)
                        {                            
                            var caps = _reqManager.GetCCHICountsAddMember(Convert.ToInt32(_reqId));
                            if (caps != null && caps.Count() > 0)
                            {
                                var result = _cchiService.ValidateSponsorsWithIqama(caps, _contractNo);
                                CCHIResponseLog(result, _reqId);
                                if (result.Count > 0)
                                {
                                    rptSponsors.DataSource = result;
                                    rptSponsors.DataBind();

                                    bool isfailure = result.Where(t => t.ErrorDetails.ServiceResult.ToString().ToLower().Contains("failure")).Count() > 0;
                                    if (isfailure)
                                    {
                                        btnSubmit.Enabled = false;
                                    }
                                    else
                                    {
                                        btnSubmit.Enabled = true;
                                    }
                                }
                                else
                                {
                                    btnSubmit.Enabled = false;
                                }
                            }
                        }
                        else
                        {
                            var caps = _reqManager.GetCCHICountsAddMember(Convert.ToInt32(_reqId));
                            var result = _cchiService.ValidateSponsorsWithIqama(caps, _contractNo);
                            CCHIResponseLog(result, _reqId);
                            rptSponsors.DataSource = result;
                            rptSponsors.DataBind();
                            btnSubmit.Enabled = false;
                            HDNsucces.Value = "false";
                            lblMessge = "Please add the minimum cap for Employees/Dependents as shown in the table below so you can proceed with your application /الرجاء إضافة الحد الأدنى للموظفين/التابعين الظاهر في الجدول ادناه حتى يتسنى لكم تقديم الطلب";
                        }
                    }
                    else
                    {
                        btnSubmit.Enabled = false;
                        lblMessge = "No Request Found/لايوجد رقم طلب";
                    }
                }
                else
                {
                    btnSubmit.Enabled = false;
                    lblMessge = "Invalid contract number & request Id / رقم العقد غير صحيح ";
                }
            }
            else
            {
                btnSubmit.Enabled = false;
                lblMessge = "No Request Found/ لم يتم العثور على رقم الطلب";
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(_reqId))
            {
                btnSubmit_Click(btnSubmit, new EventArgs());

            }
            btnSubmit.Visible = false;
        }
    }

    public string FormatMessge(string messge)
    {
        if (messge == "ValidationFailure")
        {
            return "Failed";
        }
        return "Success";
    }

    protected void OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CapList cap = (CapList)e.Item.DataItem;
            Repeater rptCap = e.Item.FindControl("rptSponsor") as Repeater;
            rptCap.DataSource = cap.Caps;
            rptCap.DataBind();
        }
    }

    protected void rptSponsor_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.Item.ItemIndex == 2)
            {
                System.Web.UI.HtmlControls.HtmlContainerControl saudiMain = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("saudiMain");
                System.Web.UI.HtmlControls.HtmlContainerControl saudiDependent = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("SaudiDependent");
                System.Web.UI.HtmlControls.HtmlContainerControl nonSaudiMain = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("nonSaudiMain");
                System.Web.UI.HtmlControls.HtmlContainerControl nonSaudiDependent = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("nonSaudiDependent");

                var image = GetImage(saudiMain.InnerHtml.Trim());
                saudiMain.Controls.RemoveAt(0);
                saudiMain.Controls.Add(image);

                var image1 = GetImage(saudiDependent.InnerHtml.Trim());
                saudiDependent.Controls.RemoveAt(0);
                saudiDependent.Controls.Add(image1);

                var image2 = GetImage(nonSaudiMain.InnerHtml.Trim());
                nonSaudiMain.Controls.RemoveAt(0);
                nonSaudiMain.Controls.Add(image2);

                var image3 = GetImage(nonSaudiDependent.InnerHtml.Trim());
                nonSaudiDependent.Controls.RemoveAt(0);
                nonSaudiDependent.Controls.Add(image3);
            }
        }
    }

    private System.Web.UI.HtmlControls.HtmlImage GetImage(string value)
    {
        System.Web.UI.HtmlControls.HtmlImage image = new System.Web.UI.HtmlControls.HtmlImage();
        image.Style.Add("width", "16px");
        image.Style.Add("height", "16px");
        image.Attributes.Add("title", value);
        if (value.ToLower() == "success")
        {
            image.Src = "~/Images/success.png";
        }
        else
        {
            image.Src = "~/Images/failure.png";
        }
        return image;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //Encrypt or remove query string
            //_reqId = Request.QueryString["requestId"];
            _reqId = Convert.ToString(Session["RequestID"]);
            string str = "<br/>";
            if (!string.IsNullOrEmpty(_reqId))
            {
                var results = NormalizeManager.NormalizeAddMemberRequest(Convert.ToInt32(_reqId));
                foreach (var item in results)
                {
                    if (string.IsNullOrEmpty(item.IdType))
                    {
                        item.IdType = GeneralHelper.GetIdTypeFromIdNumber(item.SecSponsorId);
                        if (string.IsNullOrEmpty(item.IdType))
                        {
                            item.IdType = GeneralHelper.GetIdTypeFromIdNumber(item.IdNumber);
                        }
                    }
                }
                var messges = _ceaserManager.PushAddMember(results);

                if (!messges.Any(t => t.Type == "Error"))
                {
                    var result = messges.Where(t => t.Type == "Success").OrderByDescending(t => t.Index).FirstOrDefault();
                    if (result != null)
                    {
                        //Encrypt or remove query string
                        //_reqId = Request.QueryString["requestId"].ToString();
                        _reqId = Convert.ToString(Session["RequestID"]);
                        if (!string.IsNullOrEmpty(_reqId))
                        {
                            var reqId = Convert.ToInt32(_reqId);
                            var request = _addMemberRequestService.FindByID(reqId);
                            if (request != null)
                            {
                                request.Modified = DateTime.Now;
                                request.IsPosted = true;
                                request.ReferenceNo = result.Id.ToString();
                                var isSucess = _addMemberRequestService.Update(request);
                                if (isSucess > 0)
                                {
                                    Response.Redirect("AddMemberSummary.aspx", false);
                                }
                                else
                                {
                                    Response.Redirect("AddMemberSummary.aspx", false);
                                }
                            }
                        }
                    }
                }

                foreach (var item in messges)
                {
                    var error = item;

                    str += error.Id + "||" + error.Type + "||" + error.Messege;
                }
                lblMessge = str;
            }
        }
        catch (Exception ex)
        {
            lblMessge = "Error Unable To Submit Date";
            logger.Error("Error While Submiting Date For Addmember " + ex.Message + " " + ex.InnerException);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddMemberIntermediate.aspx");
    }
    private void CCHIResponseLog(List<CapList> CCHIRes, string requestID)
    {
        try
        {
            foreach (var res in CCHIRes)
            {
                logger.Info("RequestID " + requestID + "Description:" + res.ErrorDetails.Description + " StatusCode:" + res.ErrorDetails.StatusCode + " ServiceResult:" + res.ErrorDetails.ServiceResult);
                foreach (var cap in res.Caps)
                {
                    if (cap.CapType == CapType.MinimumRequired)
                    {
                        logger.Info("CCHI: " + "RequestID:" + requestID + " SponsorID:" + cap.SponsorId + " SaudiMain:" + cap.SaudiMain + " SaudiDependent:" + cap.SaudiDependent + " NonSaudiMain:" + cap.NonSaudiMain + " NonSaudiDependent:" + cap.NonSaudiDependent);
                    }
                    if (cap.CapType == CapType.Available)
                    {
                        logger.Info("OS: " + "RequestID:" + requestID + " SponsorID:" + cap.SponsorId + " SaudiMain:" + cap.SaudiMain + " SaudiDependent:" + cap.SaudiDependent + " NonSaudiMain:" + cap.NonSaudiMain + " NonSaudiDependent:" + cap.NonSaudiDependent);
                    }
                    if (cap.CapType == CapType.Remarks)
                    {
                        logger.Info("Remarks: " + "RequestID:" + requestID + " SponsorID:" + cap.SponsorId + " SaudiMain:" + cap.SaudiMain + " SaudiDependent:" + cap.SaudiDependent + " NonSaudiMain:" + cap.NonSaudiMain + " NonSaudiDependent:" + cap.NonSaudiDependent);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            logger.Error("Error While Logging CCHI Response " + ex.Message + " " + ex.InnerException);
        }
    }

    private bool IsValidRequest()
    {
        var validRequest = _addMemberRequestService.FindByID(Convert.ToInt64(_reqId));
        if (validRequest != null)
            return validRequest.ContractNo == _contractNo && validRequest.Status == "Pending" && validRequest.IsPosted == false;
        else return false;

    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;
        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        string url = ConfigurationManager.AppSettings["SessionPath"];
        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }
}