﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Templates/Inner.Master" Inherits="_RequestDetail" Codebehind="RequestDetail.aspx.cs" %>

<%--<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>--%>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script type="text/javascript">
            function PopupCenter(url, title, w, h) {
                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                var top = ((height / 2) - (h / 2)) + dualScreenTop;
                var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);


                if (window.focus) {
                    newWindow.focus();
                }
            }
        </script>
        
        <asp:HiddenField runat="server" ID="hdnEmployeeType" />
        <asp:HiddenField runat="server" ID="hdnAramcoID" />
        <div class="container bb">
            <div class="row bb">&nbsp;</div>
            <div style='display: <%=ShowError%>;' class="alert alert-danger alert-dismissable">
                <strong>Error!</strong>&nbsp; <%=ShowError%>
            </div>
            <div class="row text-right bb">
                <div class="text-left pull-left bb">
                    <a href="Request.aspx">«Back</a>
                </div>
                <div runat="server" id="btnGroupActions" class="btn-group pull-right bb">
                    <asp:Button ID="btnAdd" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Add Employee" OnClick="btnAdd_Click" />
                    <asp:Button ID="btnAddDependent" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Add Dependent" OnClick="btnAddDependent_Click" />
					<asp:Button ID="btnEmpDependentbatch" Width="250" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Member Batch upload" OnClick="btnEmpDependentbatch_Click" /> 
                    <asp:Button ID="btnCCHI" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Process" OnClick="btnCCHI_Click" />
				</div>
            </div>
            <br />
            <div id="ShowResultMember"  class="row bb" style='display: <%=ShowRequest%>' runat="server">
                <div class="col bb">
                    <asp:Repeater ID="RptEmployee" runat="server" OnItemCommand="RptEmployee_ItemCommand">
                        <HeaderTemplate>
                            <table class="table table-striped table-bordered dt-responsive nowrap bb">
                                <thead>
                                    <th>Request</th>
                                    <th>Sponsor</th>
                                    <th>Name</th>
                                    <th>Id</th>
                                    <th>Cover</th>
                                    <th style="text-align: center; display: <%# isAramco()%>;" id="sd"">Uploaded?</th>
                                    <th>Actions</th>

                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# DataBinder.Eval(Container.DataItem, "RequestId") %></td>
                                <td><%# DataBinder.Eval(Container.DataItem, "Sponsor") %></td>
                                <td><%# DataBinder.Eval(Container.DataItem, "Name") %></td>
                                <td><%# DataBinder.Eval(Container.DataItem, "IdNumber") %></td>
                                <td><%# DataBinder.Eval(Container.DataItem, "Cover") %></td>
                                <td align="center" style="display: <%# isAramco() %>;">
                                    <img src="Images/<%# IsDocumentsUploaded(Eval("id").ToString()) %>" height="16" width="16" />
                                </td>
                                <td>
                                    <div style='display: <%=DisableAction%>;width: 200px'' class="bb">
                                        <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" CommandArgument='<%# Eval("Id")%>'>Show</asp:LinkButton>
                                        &nbsp;|&nbsp;
                                        <div id="divDocumentUpload" style="display: inline-block;" runat="server" visible="<%# AramcoSectionUploadFile %>">
                                            	<a href="#" onclick='PopupCenter("uploadDocument.aspx?requestid=<%# Eval("Id")%>","uploads","800","700")'>Upload Document</a>
                                            
												&nbsp;|&nbsp;
                                    	

											</div>                                        
											<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("Id")%>'>Delete</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>

                        <FooterTemplate>
                            </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            <asp:Label ID="Label1" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0?true:false %>' Text="No item(s) found" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div id="ShowMessageAddMember" runat="server" style="width:100%">
            <fieldset style="border: 1px solid #000000; padding: 10px;" >
                <legend style="width: 120px; border-bottom-style: none;">
                    <h1>
                        <asp:Label ID="lblDelEmp" runat="server" Text="Add Member" Font-Size="Large" Font-Names="verdana"></asp:Label>
                    </h1>
                </legend>

                <div style="text-align: left;">
                    <asp:Image ID="imgSuc" runat="server" Visible="false" ImageUrl="~/images/reimp_req_suc.png" /><br />
                    <asp:Label ID="lblResult" runat="server" ForeColor="Green"></asp:Label>
                </div>
            </fieldset>
        </div>
        <div id="ErrorDiv" runat="server" style='display: <%=lblMessge%>;' class="alert alert-danger alert-dismissable bb">

            <strong>Error!</strong>&nbsp; <%=ErrorMessage%>
        </div>
		<div style='display: <%=ShowExistingMember %>;' class="bb">
                <asp:Panel runat="server" DefaultButton="btnDependentNext">
                    <div id="ExitingMbr" runat="server" class="bb">
                        <label for="Sponsor">Main Member Exists?</label>
                        <asp:DropDownList ID="drpExisitingMember" runat="server" CssClass="form-control">
                            <asp:ListItem Value="2">No</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                        </asp:DropDownList>
                        <label for="Sponsor">Bupa Membership No</label>
                        <asp:TextBox ID="txtBupaMembershipNumber" CssClass="form-control" runat="server"></asp:TextBox>
                        <span id="reqSpantxtBupaMembershipNumber"></span>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredtxtBupaMembershipNumber" runat="server" FilterType="Numbers"
                            TargetControlID="txtBupaMembershipNumber" />
                        <br />
                        <asp:Button ID="btnDependentNext" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Next" OnClick="btnDependentNext_Click" />
                        <asp:Button ID="btnCancelDependentNext" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Cancel" OnClick="btnCancelDependentNext_Click" />
                    </div>
                </asp:Panel>
            </div>
            
			<asp:Panel runat="server" ID="pnlEmployee" DefaultButton="btnNextEmp">
                <div style='display: <%=ShowMainForm %>;' class="bb">
                    <div class="form-group bb">
                        <label for="Sponsor">Sponsor</label>
						<asp:TextBox CssClass="form-control" ID="txtSopnserNew" Text="7001095681" runat="server"></asp:TextBox>                        
						<asp:DropDownList CssClass="form-control" ID="drplistSponsor" runat="server" Visible="false">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="reqValdrplistSponsor" ControlToValidate="drplistSponsor" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
						<asp:RequiredFieldValidator ValidationGroup="step1" ID="reqValdrplistSponsorAramco" ControlToValidate="txtSopnserNew" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>                        
						<div id="DependentEmployeeID" runat="server" class="bb">
                            <label id="lblEmployeeID" for="Sponsor">Employee National ID/Iqama</label>
                            <asp:TextBox ID="txtSponsor" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="step1" ID="RequiredFieldtxtSponsor" ControlToValidate="txtSponsor" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group bb" runat="server" id="Cover">
                        <label for="Cover">Cover</label>
                        <asp:DropDownList ID="drpCover" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="drpCover_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="reqValdrplistCoverr" ControlToValidate="drpCover" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb" runat="server" id="Member">
                        <label for="Cover">Member</label>
                        <asp:DropDownList ID="drpMemberType" CssClass="form-control" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="reqValdrpMemberType" ControlToValidate="drpMemberType" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div id="Child" runat="server" class="form-group bb">
                        <label for="Cover">Age</label>
                        <asp:DropDownList ID="drpChildAge" CssClass="form-control" runat="server">
                            <asp:ListItem Text="Select age range" Value=""></asp:ListItem>
                            <asp:ListItem Text="Less Than One Year" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Greater Than One Year" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="RequiredFieldValidatordrpChildAge" ControlToValidate="drpChildAge" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div id="idType" class="form-group bb">
                        <label for="Cover">ID Type</label>
                        <asp:DropDownList ID="drpIdType" CssClass="form-control" runat="server">
                            <asp:ListItem Text="Select Id Type" Value=""></asp:ListItem>
                            <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
							<asp:ListItem Text="Passport" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="reqValdrpIdType" ControlToValidate="drpIdType" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div id="idNumber" class="form-group bb">
                        <label for="Cover">ID Number</label>&nbsp;&nbsp;<span id="lblIdNumber"></span>
                        <asp:TextBox CssClass="form-control" runat="server" ID="txtID" MaxLength="10"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxtxtID" runat="server" FilterType="Numbers"
                            TargetControlID="txtID" />
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="reqValtxtID" ControlToValidate="txtID" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rfvtxtID" runat="server" ControlToValidate="txtID" Display="Dynamic"  ValidationExpression="^[1-6]{1}[0-9]{9}$" ValidationGroup="step1" SetFocusOnError="true">Invalid Number.</asp:RegularExpressionValidator>
                        
                    </div>
                    <div id="HYOB" runat="server" class="form-group bb">
                        <label for="Cover">Hijri Year of Birth</label>
                        <asp:TextBox CssClass="form-control" runat="server" ID="txtHYearOfBirth" MaxLength="4"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxHYearOfBirth" runat="server" FilterType="Numbers"
                            TargetControlID="txtHYearOfBirth" />
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="reqValtxtHYearOfBirth" ControlToValidate="txtHYearOfBirth" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <asp:Button ID="btnNextEmp" ValidationGroup="step1" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Next" OnClick="btnNextEmp_Click" />
                    <asp:Button ID="btnCancelEmp" ValidationGroup="" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Cancel" OnClick="btnCancelEmp_Click" />
                </div>
            </asp:Panel>

			<asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnAddEmpYakeen">
                <div style='display: <%=ShowYakeenInfo%>;'>
                    <div id="IdExpiry" class="form-group bb">
                        <label for="ID Expiry">ID Expiry</label>
                        <asp:TextBox CssClass="form-control" ID="txtExpDate" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender TargetControlID="txtExpDate" ID="calenderExtExpDate" Format="dd-MM-yyyy" runat="server" />
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtExpDate" ControlToValidate="txtExpDate" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
  						<asp:RegularExpressionValidator ID="regValIdExpiryDate" runat="server"
                        	ControlToValidate="txtExpDate" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                        	ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ValidationGroup="step2" SetFocusOnError="true"
                        	Width="116px">Invalid Date format. Date format should be (dd-MM-yyyy) </asp:RegularExpressionValidator>						
                    </div>
                    <div class="form-group bb">
                        <label for="MaritalStatus">Marital Status</label>
                        <asp:DropDownList CssClass="form-control" ID="drpMaritalStatus" runat="server">
                            <asp:ListItem Text="Select Marital Status" Value=""></asp:ListItem>
                            <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValdrpMaritalStatus" ControlToValidate="drpMaritalStatus" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb">
                        <label for="MaritalStatus">Nationality</label>
                        <asp:DropDownList CssClass="form-control" ID="drpNationality" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValdrpNationality" ControlToValidate="drpNationality" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb">
                        <label for="Title">Title</label>
                        <asp:DropDownList CssClass="form-control" ID="drpTitle" runat="server">
                            <asp:ListItem Text="Select Title" Value=""></asp:ListItem>
                            <asp:ListItem Value="Mr">Mr</asp:ListItem>
                            <asp:ListItem Value="Miss">Miss</asp:ListItem>
                            <asp:ListItem Value="Mrs">Mrs</asp:ListItem>
                            <asp:ListItem Value="Ms">Ms</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValdrpTitle" ControlToValidate="drpTitle" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb">
                        <label for="Gender">Gender</label>
                        <asp:DropDownList CssClass="form-control" ID="drpGender" runat="server">
                            <asp:ListItem Text="Select Gender" Value=""></asp:ListItem>
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValdrpGender" ControlToValidate="drpGender" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb">
                        <label for="Name">Name</label>
                        <asp:TextBox CssClass="form-control" ID="txtName" runat="server" MaxLength="40"></asp:TextBox>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValName" ControlToValidate="txtName" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator
                            ID="reqRegExpValtxtName" runat="server" ControlToValidate="txtName" ForeColor="Red"
                            Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                            Font-Bold="False" ValidationExpression="[a-zA-Z ]{2,40}$" ValidationGroup="step2" SetFocusOnError="true"></asp:RegularExpressionValidator>
                        <div id="spanName" style="color: red"></div>
                    </div>
                    <div class="form-group bb">
                        <label for="DOB">Date Of Birth</label>
                        <asp:TextBox CssClass="form-control" ID="txtDOB" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender TargetControlID="txtDOB" ID="CalendartxtDOB" Format="dd-MM-yyyy" runat="server" />
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtDOB" ControlToValidate="txtDOB" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator><br />
                        <span style="color: red" id="spanDOB"></span>
                    </div>
                    <div class="form-group bb">
                        <label for="">Profession</label>
                        <asp:DropDownList CssClass="form-control" ID="drpProfession" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reValdrpProfession" ControlToValidate="drpProfession" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb">
                        <label for="">District</label>
                        <asp:DropDownList CssClass="form-control" ID="drpDistrict" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValdrpDistrict" ControlToValidate="drpDistrict" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb">
                        <label for="">Mobile</label>
                        <span style="font-style:italic;color:red;text-align:left">(Format should be like: 05xxxxxxxx)</span>
                        <asp:TextBox CssClass="form-control" ID="txtMobileNumber" MaxLength="10" runat="server"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxtxtMobileNumber" runat="server" FilterType="Numbers" TargetControlID="txtMobileNumber" />
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtMobileNumber" ControlToValidate="txtMobileNumber" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                        ControlToValidate="txtMobileNumber" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                                        ValidationExpression="[0][5][0-9]{8}" ValidationGroup="step2" SetFocusOnError="true"
                                        Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>

                        
                    </div>
                    <div class="form-group bb" id="BranchGroup" runat="server">
                        <label for="">Branch</label>
                        <asp:DropDownList CssClass="form-control" ID="drpBranchCode" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reValdrpBranchCode" ControlToValidate="drpBranchCode" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <%-- <div class="form-group bb">
					<label for="">Arrival Date</label>
                    <asp:TextBox CssClass="form-control bb" ID="txtArivalDate" runat="server"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender TargetControlID="txtArivalDate" ID="CalendarExtender1" Format="dd-MM-yyyy" runat="server" />
                    <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtArivalDate" ControlToValidate="txtArivalDate" runat="server"ErrorMessage="Field is mandatory" SetFocusOnError="true"  ></asp:RequiredFieldValidator>
                </div>--%>
                    <div class="form-group bb" id="divJoiningDate" runat="server">
                        <asp:Label runat="server" Font-Bold="true" ID="lblJoiningDate">Arrival Date</asp:Label>
                        <span style="font-style:italic;color:red;text-align:left">(Format should be like: dd-MM-yyyy)</span>
                        <asp:TextBox CssClass="form-control" ID="txtJoinDate" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender TargetControlID="txtJoinDate" ID="CalendartxtJoinDate" Format="dd-MM-yyyy" runat="server" />
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtJoinDate" ControlToValidate="txtJoinDate" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regValtxtJoinDate" runat="server"
                        ControlToValidate="txtJoinDate" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                        ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" ValidationGroup="step2" SetFocusOnError="true"
                        Width="116px">Invalid Date format. Date format should be (dd-MM-yyyy) </asp:RegularExpressionValidator>

                    </div>
                    <div class="form-group bb">
                        <label for="">Effective date for medical coverage </label>
                         <span style="font-style:italic;color:red;text-align:left">(Format should be like: dd-MM-yyyy)</span>
                        <asp:TextBox CssClass="form-control" ID="txtStartOfPolicy" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender TargetControlID="txtStartOfPolicy" ID="CalendartxtStartOfPolicy" Format="dd-MM-yyyy" runat="server" />
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtStartOfPolicy" ControlToValidate="txtStartOfPolicy" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regValtxtStartOfPolicy" runat="server"
                        ControlToValidate="txtStartOfPolicy" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                        ValidationExpression="^([0]?[1-9]|[1|2][0-9]|[3][0|1])[.\/-]([0]?[0-9]|[1][0-2])[.\/-]([0-9]{4}|[0-9]{2})$" ValidationGroup="step2" SetFocusOnError="true"
                        Width="116px">Invalid Date format. Date format should be (dd-MM-yyyy) </asp:RegularExpressionValidator>
					</div>
                    <div class="form-group bb">
                        <label id="lblEmployeeStaff" runat="server" for="">Employee Staff number  </label>
                        <asp:TextBox CssClass="form-control" ID="txtEmpNumber" runat="server" MaxLength="15"></asp:TextBox>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtEmpNumber" ControlToValidate="txtEmpNumber" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb">
                        <label for="">Employee Department  </label>
                        <asp:TextBox CssClass="form-control" ID="txtDepart" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtDepart" ControlToValidate="txtDepart" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator
                            ID="reqRegExValtxtDepart" runat="server" ControlToValidate="txtDepart" ForeColor="Red"
                            Display="Dynamic" ErrorMessage="Incorrect department format, please use only alpha numeric."
                            Font-Bold="False" ValidationExpression="[a-zA-Z0-9 ]+" ValidationGroup="step2" SetFocusOnError="true"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-group bb" id="divPreviousMember" runat="server">
                        <label for="">Has the member or any of its dependents previously been covered by BUPA ?</label>
                        <asp:DropDownList CssClass="form-control" ID="drpPreviousMember" runat="server">
                            <asp:ListItem Text="Select if returning member" Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValdrpPreviousMember" ControlToValidate="drpPreviousMember" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div id="PREVMBR" runat="server" class="form-group bb">
                        <label for="">Previous Membership</label>
                        <asp:TextBox CssClass="form-control" ID="txtPrevmembership" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValtxtPrevmembership" ControlToValidate="txtPrevmembership" runat="server" ErrorMessage="Field is mandatory" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group bb" id="DivJoinReason" runat="server">
                        <label for="">Join reason</label>
                        <asp:DropDownList CssClass="form-control" ID="drpJoinReason" runat="server">
                            <asp:ListItem Text="Select joing Reason" Value=""></asp:ListItem>
                            <asp:ListItem Value="RJOIN001">Member is studying outside Kingdom</asp:ListItem>
                            <asp:ListItem Value="RJOIN002">Dependent is studying outside Kingdom</asp:ListItem>
                            <asp:ListItem Value="RJOIN003">Customer forgot to add member from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN004">Customer forgot to add dependent from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN005">Main member / father forgot to add dependent from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN006">End of probationary period</asp:ListItem>
                            <asp:ListItem Value="RJOIN007">Employee is ill and needs treatment</asp:ListItem>
                            <asp:ListItem Value="RJOIN008">Dependent is ill and needs treatment</asp:ListItem>
                            <asp:ListItem Value="RJOIN009">Member was on long vacation</asp:ListItem>
                            <asp:ListItem Value="RJOIN010">Member promoted to higher grade which is entitled to add dependent/s</asp:ListItem>
                            <asp:ListItem Value="RJOIN011">Newly Hired Employee</asp:ListItem>
                            <asp:ListItem Value="RJOIN012">New Born baby</asp:ListItem>
                            <asp:ListItem Value="RJOIN013">Newly Married</asp:ListItem>
                        </asp:DropDownList>
                        <%-- <asp:RequiredFieldValidator ValidationGroup="step2" ID="reqValdrpJoinReason" ControlToValidate="drpJoinReason" runat="server"ErrorMessage="Field is mandatory" SetFocusOnError="true"  ></asp:RequiredFieldValidator>--%>
                    </div>
                    <asp:Button ID="btnAddEmpYakeen" ValidationGroup="step2" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Add" OnClick="btnAddEmpYakeen_Click" />
                    <asp:Button ID="btnCancelEmpYakeen" ValidationGroup="" CssClass="btnTOAdd btn-primary bb" runat="server" Text="Cancel" OnClick="btnCancelEmpYakeen_Click" />
                </div>
            </asp:Panel>
        </div>
        <asp:HiddenField ID="HfRequestID" runat="server" />
        <script type="text/javascript">
			$(document).ready(function () {
				function ValidatorEnable(val, enable) {
                    val.enabled = (enable != false);
                    ValidatorValidate(val);
                    ValidatorUpdateIsValid();
                }

function SetHijariValidation(idType, age)
{

    if ($("#hdnAramcoID").val() == "TPAC") {
        if (idType != "" && age != "")
        {
            if (age == "1" && idType == "1") {
                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
                $("#HYOB").show();
            }
            else if (age == "2" && idType == "1")
            {
                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), true);
                $("#HYOB").show();
            }
            else
            {
                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
                $("#HYOB").hide();
            }
        }
        else if (idType != "" && age == "")
        {
            if (idType == "1")
            {
                $("#HYOB").show();
                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), true);
            }
            else
            {
               $("#HYOB").hide();
               ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
            }
        }
    }
    else{

                    if ($("#hdnEmployeeType").val() == "E")
                    {
                        if (idType != "" && idType == '1') {
                            $("#HYOB").show();
                            ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), true);
                        }
                        else {
                            $("#HYOB").hide();
                            $('#<%=txtHYearOfBirth.ClientID %>').val("");
                            ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
                        }

                    }
                else{
                        if (idType != "" && age != "")
                        {
                            if (age == "1" && idType == "1") {
                                    ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
                                    $("#HYOB").show();
                                }
                            else if (age == "2" && idType == "1") {
                                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), true);
                                $("#HYOB").show();
                                }
                            else {
                                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
                                $("#HYOB").hide();
                                }
                       }
                        else if (idType != "" && age == "")
                        {
                                if (idType == "1")
                                 {
                                     $("#HYOB").show();
                                     ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), true);
                                 }
                            else {
                                $("#HYOB").hide();
                                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
                                 }
                        }

                    }


    }
    
    
}

			    $('#<%=drpChildAge.ClientID %>').change(function () {

            var idType = $('#<%=drpIdType.ClientID %>').val();
            var age = $(this).val();
            SetHijariValidation(idType, age);
			
					if (age == "1") {
                        $("#lblIdNumber").html("(If Child ID is Available)");
                    }
                    else {
                        $("#lblIdNumber").html("");
                    }

        });

			    
    $('#<%=drpIdType.ClientID %>').change(function () {
            var idType = $(this).val();
            var age = $('#<%=drpChildAge.ClientID %>').val();

            SetHijariValidation(idType, age);

            <%--if (this.value == '1') {
              
				$("#HYOB").show();

                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), true);
            }
            else {
                $("#HYOB").hide();

                $('#<%=txtHYearOfBirth.ClientID %>').val("");
                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
            }--%>

        });

	    $('#<%=drpPreviousMember.ClientID %>').change(function () {
            if (this.value == '1') {
                $("#PREVMBR").show();
                ValidatorEnable(document.getElementById("<%=reqValtxtPrevmembership.ClientID %>"), true);
            }
            else {
                $("#PREVMBR").hide();
                ValidatorEnable(document.getElementById("<%=reqValtxtPrevmembership.ClientID %>"), false);
            }
        });

        
     $('#<%=drpMemberType.ClientID %>').change(function () {
		 $("#lblIdNumber").html("");
            if (this.value == 'C' || this.value == 'D' || this.value == 'SO') {
                $("#Child").show();
                $('#<%=drpChildAge.ClientID %>')[0].selectedIndex = 0;
                ValidatorEnable(document.getElementById("<%=RequiredFieldValidatordrpChildAge.ClientID %>"), true);
            }
            else {
                $("#Child").hide();
                $("#idType").show();
                $("#idNumber").show();
                ValidatorEnable(document.getElementById("<%=reqValdrpIdType.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%=reqValtxtID.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%=RequiredFieldValidatordrpChildAge.ClientID %>"), false);
            }
        });

			    $('#<%=drpChildAge.ClientID %>').change(function () {
            if (this.value == '1') {
                ValidatorEnable(document.getElementById("<%=reqValtxtExpDate.ClientID %>"), false);
				// ValidatorEnable(document.getElementById("<%=reqValdrpIdType.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%=reqValtxtID.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%=reqValtxtHYearOfBirth.ClientID %>"), false);
            }
            else {
                $("#IdExpiry").show();
                ValidatorEnable(document.getElementById("<%=reqValtxtExpDate.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%=reqValdrpIdType.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%=reqValtxtID.ClientID %>"), true);
            }
        });


       <%-- $('#<%=drpCover.ClientID %>').on('change', function () {
            var val = this.value;
            var type=$("#hdnEmployeeType").val();
            GetDropDownData(val, type);

        });

        function GetDropDownData(cover, type) {

            var filter = {};
            filter.cover = cover;
            filter.type = type;

            $.ajax({
                type: "POST",
                url: "RequestDetail.aspx/GetMemberTypeByCoverList",
                data: '{cover: "++" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data)
                    {
                        $.each(data, function (){
                            $("#drpCover").append($("<option  />").val(data.Text).text(data.Value));
                        });
                    },
                    failure: function () {
                        alert("Failed!");
                    }
                });
        }--%>

        var isExisiting = $('#drpExisitingMember').val();

        if (isExisiting != undefined && isExisiting) {
            request.ExisitingMemberDisable(isExisiting);
        }

        $('#drpExisitingMember').change(function () {
            request.ExisitingMemberDisable(this.value);
        });

        $("#btnDependentNext").click(function () {
            if ($('#drpExisitingMember').val() == "2") {
                return true;
            }
            else {
                if ($("#txtBupaMembershipNumber").val() == "") {
                    $("#reqSpantxtBupaMembershipNumber").html("Field is mandatory");
                    return false;
                }
            }
            return true;
        })


    });




    var request = {};

    request.CheckLenth = function (len, value) {
        if (value != "") {
            if (value.length > len)
                return true;
        }
        return false;
    };

    request.SetArrivalDateLabel = function (val) {
        if (val == "1") {
            $('#<%=lblJoiningDate.ClientID %>').html("Joining Date");
        }
        else {
            $('#<%=lblJoiningDate.ClientID %>').html("Arrival Date");
        }

    }

    request.ValidatorEnable = function (val, enable) {
        val.enabled = (enable != false);
        ValidatorValidate(val);
        ValidatorUpdateIsValid();
    }

    request.IsNull = function (val) {
        if (val == "")
            return true;
    }

    request.ExisitingMemberDisable = function (value) {
        if (value == '2') {
            $("#txtBupaMembershipNumber").attr("disabled", "true");
            $("#txtBupaMembershipNumber").val("");


        }
        else {

            $("#txtBupaMembershipNumber").removeAttr("disabled");

        }
    }

    request.GetDateFromText = function (val) {
        if (val != "") {
            var dobDateStr = val;
            var expireDateArr = dobDateStr.split("-");
            //  var today = new Date((d.getMonth() + 1)+"/"+d.getDate()+"/"+ d.getFullYear());
            var date = new Date(expireDateArr[1] + "/" + expireDateArr[0] + "/" + expireDateArr[2]);
            return date;
        }
    }




    //drpTitle
    //drpMaritalStatus
    //drpGender

    var isdrpTitle = false;
    var isdrpMaritalStatus = false;
    var isdrpGender = false;


    isdrpTitle = $('#drpTitle').is(':disabled');

    isdrpMaritalStatus = $('#drpMaritalStatus').is(':disabled');

    isdrpGender = $('#drpGender').is(':disabled');




    $('#drpGender').change(function () {
        var memberType = $('#<%=drpMemberType.ClientID %>').val();
        var gender = $('#drpGender').val();
        var title = $('#drpTitle').val();

        ValidateTitleGender(gender, title, memberType);

    });

    $('#drpTitle').change(function () {
        var memberType = $('#<%=drpMemberType.ClientID %>').val();
        var gender = $('#drpGender').val();
        var title = $('#drpTitle').val();

        ValidateTitleGender(gender, title, memberType);
    });



    function ValidateTitleGender(gender, title, type) {
        if (gender == "M") {

            $("#drpGender").val("M");
            $("#drpTitle").val("Mr");

            if (type == "C" || type == "SO") {
                $("#drpMaritalStatus").val("1");
            }
            else {
                $("#drpMaritalStatus").val("");
            }

        }

        if (gender == "F") {

            $("#drpGender").val("F");

            if (type == "D" || type == "C") {
                $("#drpTitle").val("Miss");
                $("#drpMaritalStatus").val("1");
            }
            else if (type == "S") {
                $("#drpTitle").val("Mrs");
                $("#drpMaritalStatus").val("2");
            }
            else {
                $("#drpMaritalStatus").val("");
            }
        }

    }

    var age = $("#drpChildAge").val();







    var idtype = $('#<%=drpIdType.ClientID %>').val();
    request.SetArrivalDateLabel(idtype);

    $('#<%=drpIdType.ClientID %>').change(function () {
        var selValue = $(this).val();
        request.SetArrivalDateLabel(selValue);
    });


    $('#<%=btnAddEmpYakeen.ClientID %>').click(function () {

                var age = $("#drpChildAge").val();
                var dob = $('#<%=txtDOB.ClientID %>').val();

        if (age != "" && dob != "") {
            var dobDate = request.GetDateFromText(dob);
            console.log(dobDate);
            var d = new Date();

            var today = new Date((d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear());
            console.log(today);
            var diff = new Date(today - dobDate);
            console.log(diff);
            var days = diff / 1000 / 60 / 60 / 24;
            console.log(days);
            if (age == "1" && days > 365) {
                $("#spanDOB").html("Incorrect date of birth");
                return false;
            }
            else {
                $("#spanDOB").html("");

            }



        }

        var name = $('#<%=txtName.ClientID %>').val();
        if (request.CheckLenth(40, name)) {
            $("#spanName").html("The name exceeded the accepted number of characters “40 characters”.Please modify the name in order to process the request <br />عدد أحرف الاسم تخطت الحد المسموح 40 حرف الرجاء تعديل الاسم بحيث لا يتخطى الحد المسموح");
            $('#<%=txtName.ClientID %>').removeAttr("disabled");
            $('#<%=txtName.ClientID %>').focus();

            return false;
        }
        else {
            $("#spanName").html("");
            return true;
        }

    });


        </script>
     <br />
        <uc2:OSNav ID="OSNav1" runat="server" />
        <br />
</asp:Content>

<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        <%--<asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>--%>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>
</asp:Content>

