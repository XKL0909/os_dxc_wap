﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;
using System.Web.Configuration;
public partial class UploadDocument : System.Web.UI.Page
{
    private UnifiedFileuploadService FileUplaodService = new UnifiedFileuploadService();
    private Fileupload RequestFileUpload = new Fileupload();
    private string requestID = "";
    protected string path = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
                //Session Timeout Warning Dialog
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
           
            CheckSessionExpired();
        }
        catch
        {

        }
        
        path = UnifiedUploadManager.GetPath();
        //Encrypt or remove query string
        //if (!string.IsNullOrEmpty(Request.QueryString["requestid"]))
        //{
        //    requestID = Request.QueryString["requestid"];
        //    BindFile(Convert.ToInt32(requestID));
        //}

        if (!string.IsNullOrEmpty(Convert.ToString(Session["RequestId"])))
        {
            requestID = Convert.ToString(Session["RequestId"]);
            BindFile(Convert.ToInt32(requestID));
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {

        FileUplaodService = new UnifiedFileuploadService();
        RequestFileUpload = new Fileupload();

        if ((FileUpload.PostedFile != null) && (FileUpload.PostedFile.ContentLength > 0))
        {
            string fileName = UnifiedUploadManager.GenrateName(System.IO.Path.GetFileName(FileUpload.FileName));
            string ext = System.IO.Path.GetExtension(FileUpload.FileName);
            //Added new code to check MimeType
            string fileExtention = System.IO.Path.GetExtension(FileUpload.PostedFile.FileName).ToLower();
            Boolean fileOK = false;
            Boolean mimeOK = false;
            CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
            string mime = findMimeFromDate.CheckFindMimeFromData(FileUpload.PostedFile);
            String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
            //String[] ExcelMime = findMimeFromDate.ReturnFileExtenstions("ExcelFile");
            //ExcelMime.CopyTo(allowedMime, allowedMime.Length - 1);
            String[] allowedExtensions = { ".pdf", ".jpg", ".jpeg", ".gif", ".png", ".bmp", "rtf","tiff"};
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtention == allowedExtensions[i])
                {
                    fileOK = true;
                    break;
                }
            }
            for (int i = 0; i < allowedMime.Length; i++)
            {
                if (mime == allowedMime[i])
                {
                    mimeOK = true;
                    break;
                }
            }
            if (fileOK && mimeOK)
            {

                //var allowdExtension = ConfigurationManager.AppSettings["UploadAllowedExtensions"].ToString().Split(',');

                //if (!allowdExtension.Contains(ext.ToLower()))
                //{
                //    lblError.Text = "Please ensure you upload the correct file type (allowed extensions are: .jpg, .jpeg, .gif, .png, .bmp, .tiff, .pdf, .rtf) ";
                //    return;
                //}

                int fileSize = FileUpload.PostedFile.ContentLength;
                var maxSize = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSize"].ToString());
                if (fileSize > maxSize)
                {

                    lblError.Text = "Cannot upload this file: This file exceeds the total file size allowed to be uploaded. Total allowed size is: 5MB";
                    return;
                }

            try
            {
                FileUpload.PostedFile.SaveAs(UnifiedUploadManager.GetPath() + fileName);

                RequestFileUpload.UnifiedRequestDetailId = Convert.ToInt32(requestID);
                RequestFileUpload.Name = fileName;
                RequestFileUpload.FullName = FileUpload.FileName;
                RequestFileUpload.CreatedOn = DateTime.Now;
                FileUplaodService.Add(RequestFileUpload);
                BindFile(Convert.ToInt32(requestID));
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message.ToString();
            }

            Page.ClientScript.RegisterStartupScript(
            GetType(),
            "MyKey",
            "RefreshMainPage();",
            true);
        }
		else
        {
             lblError.Text = "Please ensure you upload the correct file type (allowed extensions are: .jpg, .jpeg, .gif, .png, .bmp, .tiff, .pdf, .rtf) ";
             return;
        }

		}

    }

    private void BindFile(int requestId)
    {
        FileUplaodService = new UnifiedFileuploadService();
        List<Fileupload> Fileuploads = new List<Fileupload>();
        Fileuploads= FileUplaodService.FindByRequestID(requestId);
        //for (int i = 0; i < Fileuploads.Count; i++)
        //{
        //    Fileuploads[i].Name = Fileuploads[i].Name.Remove(0, Fileuploads[i].Name.IndexOf('_') + 1).ToString();
        //}

        RptUpload.DataSource = Fileuploads;
        RptUpload.DataBind();      

    }
    private void DeleteRequest(int id)
    {
        FileUplaodService = new UnifiedFileuploadService();
        FileUplaodService.Remove(id);
      
    }


    protected void RptUpload_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int customerID = Convert.ToInt32(e.CommandArgument.ToString());
        switch (e.CommandName)
        {
            case "Delete":
                DeleteRequest(customerID);
                BindFile(Convert.ToInt32(requestID));
                Page.ClientScript.RegisterStartupScript(
                GetType(),
                "MyKey",
                "RefreshMainPage();",
                true);
                break;
        }
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
}