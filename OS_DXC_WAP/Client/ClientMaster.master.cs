using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Web.Configuration;

public partial class ClientMaster : System.Web.UI.MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;

    protected void Page_Load(object sender, EventArgs e)
     {

        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
        }
        catch
        {

        }
        string loggedInAs = "";
		 //Aramco PID Changes By Hussamuddin
        string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            HtmlImage AramcoSrc = (HtmlImage)AramcoImg.FindControl("AltAramcoImg");
            AramcoSrc.Src = Convert.ToString(Session["AramcoImagePath"]);
            AramcoImg.Visible = true;
            rdbSearchOption.Items[1].Text = "Badge No.";
            rdbSearchOption.Items[1].Value = "Badge No";
            RegExpVal.ErrorMessage = "Only numbers with hyphen.";
			RegExpVal.Enabled = true;
        }
        else
		{
            AramcoImg.Visible = false;
			RegExpVal.Enabled = false;
            //Aramco PID Changes By Hussamuddin
		}
			
         if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
         {
             //logouttag.Visible = true;
             //logintag.Visible = false;
             if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
             {
                 lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["MemberName"]) + " (" + Convert.ToString(Session["MembershipNo"]) + ")";



                 //lnkHome.HRef = "../member/default.aspx";
             }
             if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
             {

                 lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ClientName"]) + " (" + Convert.ToString(Session["ClientUsername"]) + ")";
                 //lnkHome.HRef = "../client/default.aspx";
             }
             if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
             {
                 lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ProviderName"]) + " (" + Convert.ToString(Session["ProviderUserName"]) + ")";
                 //lnkHome.HRef = "../provider/default.aspx";
             }
         }
         else
         {
             if (GetCurrentPageName() != "Registration.aspx" && GetCurrentPageName() != "forgotpassword.aspx")
             {
                 Response.Redirect("../default.aspx");
                 //logouttag.Visible = false;
                 //logintag.Visible = true;
             }
             else
             {
                 //backdiv.Visible = false;
             }
         }

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;

        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;
            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                responseCookie.Secure = true;
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;

    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti - XSRF token failed.");
            }
        }
    }

    public string GetCurrentPageName()
    {
        string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }

    protected void hlLogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();

        Response.Redirect("../default.aspx");
    }

    public Button SearchButton {
        get { return btnSearch; } 
} 
   public DropDownList ddlStaffList {
       get { return DDLDependentList; } 
} 

public TextBox SearchText {
    get { return txtSearch; }
    
}

public RadioButtonList SearchOpt
    {
        get { return rdbSearchOption ; }
    }

    public Label PageTitle
    {
        get { return lblPageTitle; }
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }


}
