﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Bupa.OSWeb.Helper;
using Utility.Configuration;
using Bupa.OSWeb.Business;

public partial class AddEmpDepDeclr : System.Web.UI.Page
{
    DataTable DT;
    DataSet DS;
    int param = 0;
    Boolean DependentFlag;
    private CaesarWS.ServiceDepot_DNService ws;
    private avayaWS.Service1 avaya;
    string strClientID;
    string isRowDeleted;
    bool _isBatch = false;

    // The upload category for the uploading of this page
    private string _uploadCategory = UploadCategory.MEMBER_ADDEMP;

    private void BindDdlMemberType()
    {
        ListItem ListItem0 = new ListItem();
        ListItem0.Text = "-Select-";
        ListItem0.Value = "0";

        CaesarWS.ReqMbrTypeListRequest_DN request = new CaesarWS.ReqMbrTypeListRequest_DN();
        //request.TransactionID = TransactionManager.TransactionID();
        request.TransactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.grp_Type = "E";
        ws = new CaesarWS.ServiceDepot_DNService();
        CaesarWS.ReqMbrTypeListResponse_DN response;
        response = ws.ReqMbrTypeList(request);
        ddlEMemberType.DataSource = response.detail;
        ddlEMemberType.DataBind();
        ddlEMemberType.Items.Insert(0, ListItem0);

        request.grp_Type = "D";
        response = ws.ReqMbrTypeList(request);
        ddlMemberType.DataSource = response.detail;
        ddlMemberType.DataBind();
        ddlMemberType.Items.Insert(0, ListItem0);
    }

    private void BindDdlProfession()
    {
        //ListItem ListItem0 = new ListItem();
        //ListItem0.Text = "-Select-";
        //ListItem0.Value = "0";

        CaesarWS.ReqProfListRequest_DN request = new CaesarWS.ReqProfListRequest_DN();
        //request.TransactionID = TransactionManager.TransactionID();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        ws = new CaesarWS.ServiceDepot_DNService();
        CaesarWS.ReqProfListResponse_DN response;
        response = ws.ReqProfList(request);
        
        //ProfCodeControl MyCodeC = new ProfCodeControl();
        ////List<string> MyCodes = MyCodeC.GetAllProfCodesAsString();

        //List<ProfCode> MyCodes = new List<ProfCode>();

        //ProfCode MyCode = new ProfCode();
        //for (int i = 0; i < response.detail.Length; i++) {
        //    MyCode = new ProfCode();
        //    MyCode = MyCodeC.GetProfCode(response.detail[i].profCode);
        //    if(!(MyCode != null && MyCode.ID != 0)){
        //        MyCodes.Add(MyCode);
        //    }
        //    //MyCode.ProfCodeValue = response.detail[i].profCode;
        //    //MyCodes.Add(MyCode);
        //}

        ////MyCodeC.DeleteAllProfCodes();
        //if(MyCodes.Count > 0){
        //    MyCodeC.InsertProdCodesList(MyCodes);
        //}
        //
        CaesarWS.ProfListDetail_DN[] MyCodes = new CaesarWS.ProfListDetail_DN[response.detail.Length + 1];
        
        response.detail.CopyTo(MyCodes,1);
        MyCodes[0] = new CaesarWS.ProfListDetail_DN();
        MyCodes[0].profCode = "";
        MyCodes[0].profName = "-Select-";
        ddProfession.DataSource = MyCodes;
        ddProfession.DataBind();
        //ddProfession.Items.Insert(0, ListItem0);

        ddDepProfession.DataSource = MyCodes;
        ddDepProfession.DataBind();
        //ddDepProfession.Items.Insert(0, ListItem0);
    }

    private void BindDdlDistrict()
    {
        //ListItem ListItem0 = new ListItem();
        //ListItem0.Text = "-Select-";
        //ListItem0.Value = "0";

        CaesarWS.ReqDistListRequest_DN request = new CaesarWS.ReqDistListRequest_DN();
        //request.TransactionID = TransactionManager.TransactionID();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        ws = new CaesarWS.ServiceDepot_DNService();
        CaesarWS.ReqDistListResponse_DN response;
        response = ws.ReqDistList(request);

        CaesarWS.DistListDetail_DN[] MyDistricts = new CaesarWS.DistListDetail_DN[response.detail.Length + 1];

        response.detail.CopyTo(MyDistricts, 1);
        MyDistricts[0] = new CaesarWS.DistListDetail_DN();
        MyDistricts[0].distCode = "";
        MyDistricts[0].distName = "-Select-";
        ddDistrict.DataSource = MyDistricts;
        ddDistrict.DataBind();
        //ddDistrict.Items.Insert(0, ListItem0);

        ddDepDistrict.DataSource = MyDistricts;
        ddDepDistrict.DataBind();
        //ddDepDistrict.Items.Insert(0, ListItem0);
    }

    private void BindDdlENationality(bool IsGCC) {
        NationalityControl MyNationalityC = new NationalityControl();
        List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
        MyNationalities.Insert(0, new Nationality(0, "", "- Select -", false));
        ddlENationality.DataValueField = "Code";
        ddlENationality.DataTextField = "Nationality1"; 
        ddlENationality.DataSource = MyNationalities;
        ddlENationality.DataBind();
        //ddlENationality.Items.Insert(0,new ListItem("- Select -",""));
    }

    private void BindDdlNationality(bool IsGCC)
    {
        NationalityControl MyNationalityC = new NationalityControl();
        List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
        MyNationalities.Insert(0, new Nationality(0, "", "- Select -", false));
        ddlNationality.DataValueField = "Code";
        ddlNationality.DataTextField = "Nationality1";
        ddlNationality.DataSource = MyNationalities;
        ddlNationality.DataBind();
        //ddlNationality.Items.Insert(0,new ListItem("- Select -", ""));
    }


    protected void Page_Load(object sender, System.EventArgs e)
    {
        lnkBatchUpload.ToolTip = Session.Timeout.ToString();

        if (Session["ClientID"] == null)
        {
            HttpContext.Current.Response.Redirect("/Default.aspx");
        }

        else
        {
            strClientID = Session["ClientID"].ToString();

            if (!Page.IsPostBack)
            {
                /***** Fresh Page Load *****/

                DS = new DataSet();
                DS.ReadXmlSchema(Server.MapPath("DepdFile.xml"));
                Session["Dataset"] = DS;
                DT = DS.Tables[0];

                PopulateClassList(strClientID);
                PopulateBranchList(strClientID);
                string DeclrFormRequired = IsDeclarationRequired(strClientID);
                if (DeclrFormRequired == "Y")
                {
                    PanelDeclaration.Visible = true;
                }
            }
            else
            {
                if (ddlEBranchCode.Items.Count == 0)
                {
                    PopulateBranchList(strClientID);
                }
                if (ddlLevelOfCover.Items.Count == 0 || ddlELevelOfCover.Items.Count == 0)
                {
                    PopulateClassList(strClientID);
                }
            }

            lblSaudiNote.Visible = true;

            if (Request.QueryString["EType"] == "Dep")
            {
                PanelMembershipNoOnly.Visible = true;
                PanelEmployee.Visible = false;
                DependentFlag = true;
                lblAdd_Employee.Text = "Add Dependent";

                lnkBatchUpload.NavigateUrl = "~/Client/BatchUpload.aspx?OptionType=AddDependent";

                txtFlagForAddingDepValidation.Visible = true;
                RangeValidatorForDep.Enabled = true;

                if (txtFlagForAddingDepValidation.Text != "0")
                    RangeValidatorForDep.Enabled = false;

                // Set the upload category to be for dependents
                _uploadCategory = UploadCategory.MEMBER_ADDDEP;
            }
            else
            {
                PanelDependent.Visible = true;

                Table2.Visible = true;
            }

            if (ViewState["dt"] != null)
            {
                DT = (DataTable)ViewState["dt"];
            }
     
            DataSet dsTest = (DataSet)Session["Dataset"];

            if (DT != null)
            {
                //// Bool Value to check if batch indecator is true or false 
                _isBatch = true;
                GridView1.DataSource = DT;
                GridView1.DataBind();
            }
        }
        isRowDeleted = "false";

        
        if (!IsPostBack)
        {
            LoadCaesarSourcedData();
        }
        SetupUploader(_uploadCategory);
    }

    protected void Page_LoadComplete(object sender, System.EventArgs e)
    {
        // Enable / Disable the submission option depending on whether the user 
        // has uploaded supported documents

        // Assume the button is disabled until checked
        btnSubmit.Enabled = false;

        // Get what has been uploaded
        UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet uploadedFileSet = uploader.UploadedFileSet;

        int uploadedCount = 0;

        if (_uploadmanger.WebIndecator("StaffNoInd", strClientID))
        {
            RequiredFieldValidatorEmpNo.Enabled = true;
            RequiredFieldValidatorEmpNoDep.Enabled = true;
            txtEEmployeeNo.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0");
            txtEmployeeNo.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0");
        }
        else
        {
            RequiredFieldValidatorEmpNo.Enabled = false;
            RequiredFieldValidatorEmpNoDep.Enabled = false;
            txtEEmployeeNo.BackColor = System.Drawing.Color.Transparent;
            txtEmployeeNo.BackColor = System.Drawing.Color.Transparent;

        }
        if (_uploadmanger.WebIndecator("DeptCodeInd", strClientID))
        {
            RequiredFieldValidatorDeptCode.Enabled = true;
        }
        else
        {
            RequiredFieldValidatorDeptCode.Enabled = false;
        }
        if (_uploadmanger.WebIndecator("DepdIdInd", strClientID))
        {

            rfvDepSuadiID.Enabled = true;
        }
        else
        {

            rfvDepSuadiID.Enabled = false;
        }
        if (_uploadmanger.WebIndecator("WebSupDocInd", strClientID))
        {
            tblUploader.Visible = true;
            if (uploadedFileSet != null && uploadedFileSet.Tables.Count > 0)
            {
                uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                if (uploadedCount > 0)
                    btnSubmit.Enabled = true;
            }
        }
        else
        {
            //tblUploader.Visible = false;
            btnSubmit.Enabled = true;
        }
    }

    private void SetupUploader(string uploadCategory)
    {
        // By default, unless a user has uploaded supporting documents,
        // the Submit button will be disabled
        btnSubmit.Enabled = false;

        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        // Bind to allow resolving of # tags in mark-up
        DataBind();
    }

    public void FindData(int SI)
    {
        DT = (DataTable)ViewState["dt"];

        ddlTitle.SelectedValue = (String)DT.Rows[SI][0];
        txtMemberName.Text = (String)DT.Rows[SI][1];
        txtDateOfBirth.Text = (String)DT.Rows[SI][2];
        txtSponsorId.Text = (String)DT.Rows[SI][3];
        txtEmployeeNo.Text = (String)DT.Rows[SI][4];
        txtSaudiID.Text = (String)DT.Rows[SI][5];
        txtDepartmentCode.Text = (String)DT.Rows[SI][6];
        ddlGender.SelectedValue = (String)DT.Rows[SI][7];
        ddlNationality.SelectedValue = (String)DT.Rows[SI][8];
        ddlLevelOfCover.SelectedValue = (String)DT.Rows[SI][9];
        ddlJoinReason.SelectedValue = (String)DT.Rows[SI][10];
        //ddl.SelectedValue = (String)DT.Rows[SI][11];
        txtJoiningDate.Text = (String)DT.Rows[SI][12];
        txtRequestedStartDate.Text = (String)DT.Rows[SI][13];
        txtMobileNo.Text = (String)DT.Rows[SI][14];
        ddlMemberType.SelectedValue = (String)DT.Rows[SI][15];
        ddlIDType.SelectedValue = (String)DT.Rows[SI][16];
        txtIDExpDate.Text = (String)DT.Rows[SI][17];
        ddlMaritalStatus.SelectedValue = (String)DT.Rows[SI][18];
        ddDepProfession.SelectedValue = (String)DT.Rows[SI][19];
        ddDepDistrict.SelectedValue = (String)DT.Rows[SI][20];
        //ViewState["dt"] = DT;
    }
    protected bool CheckAge(DateTime _bDate)
    {
        bool _Underage = false;
        DateTime _current = DateTime.Today;
        TimeSpan span = _current.Subtract(_bDate);
        int days = span.Days;
        if (days <= 90)
        {
            _Underage = true;
        }
        else
        {
            _Underage = false;
        }
        return _Underage;
    }
    protected void Button1_Click(object sender, System.EventArgs e)
    {
        UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);
        if (CheckAge(Convert.ToDateTime(txtDateOfBirth.Text)))
        {
            addval();
            GridView1.SelectedIndex = -1;

            if (DependentFlag == true)
            {
                int DepCounter = Int32.Parse(txtFlagForAddingDepValidation.Text) + 1;
                txtFlagForAddingDepValidation.Text = DepCounter.ToString();
                RangeValidatorForDep.Enabled = false;
            }
            btnSubmit.Enabled = true;
        }
        else
        {
            if (_uploadmanger.WebIndecator("DepdIdInd", strClientID))
            {
                if (string.IsNullOrEmpty(txtSaudiID.Text.Trim()))
                {
                    lblIDVal.Visible = true;
                    lblIDExpiryVal.Visible = true;
                }
                else
                {
                    addval();
                    GridView1.SelectedIndex = -1;

                    if (DependentFlag == true)
                    {
                        int DepCounter = Int32.Parse(txtFlagForAddingDepValidation.Text) + 1;
                        txtFlagForAddingDepValidation.Text = DepCounter.ToString();
                        RangeValidatorForDep.Enabled = false;
                    }
                    btnSubmit.Enabled = true;
                }


            }
            else
            {
                addval();
                GridView1.SelectedIndex = -1;

                if (DependentFlag == true)
                {
                    int DepCounter = Int32.Parse(txtFlagForAddingDepValidation.Text) + 1;
                    txtFlagForAddingDepValidation.Text = DepCounter.ToString();
                    RangeValidatorForDep.Enabled = false;
                }
                btnSubmit.Enabled = true;
            }
        }

    }

    public bool updateVal(int index)
    {

       if (CheckAge(Convert.ToDateTime(txtDateOfBirth.Text)))
        {
            DT = (DataTable)ViewState["dt"];
            DT.Rows[index][0] = ddlTitle.SelectedValue;
            DT.Rows[index][1] = txtMemberName.Text;
            DT.Rows[index][2] = txtDateOfBirth.Text;// CalendarDateString;
            DT.Rows[index][3] = txtSponsorId.Text;
            DT.Rows[index][4] = txtEmployeeNo.Text;
            DT.Rows[index][5] = txtSaudiID.Text;
            DT.Rows[index][6] = txtDepartmentCode.Text;
            DT.Rows[index][7] = ddlGender.SelectedValue;
            DT.Rows[index][8] = ddlNationality.SelectedValue;
            DT.Rows[index][9] = ddlLevelOfCover.SelectedValue;
            DT.Rows[index][10] = ddlJoinReason.SelectedValue;
            //DT.Rows[index][11] = ddlRelationShip.SelectedValue;
            DT.Rows[index][12] = txtJoiningDate.Text;//.CalendarDateString;
            DT.Rows[index][13] = txtRequestedStartDate.Text;// CalendarDateString;
            DT.Rows[index][14] = txtMobileNo.Text;
            DT.Rows[index][15] = ddlMemberType.SelectedValue;
            DT.Rows[index][16] = ddlIDType.SelectedValue;
            DT.Rows[index][17] = txtIDExpDate.Text;
            DT.Rows[index][18] = ddlMaritalStatus.SelectedValue;
            DT.Rows[index][19] = ddDepProfession.SelectedValue;
            DT.Rows[index][20] = ddDepDistrict.SelectedValue;
            reset();
            GridView1.DataSource = DT;
            GridView1.DataBind();
            GetAllDependentsName();

            ViewState["dt"] = DT;
            return true;
       }
        else
        {
            lblIDVal.Visible = true;
            lblIDExpiryVal.Visible = true;
            return false;
        }
    }

    public void addval()
    {
        if (ViewState["dt"] != null)
            DT = (DataTable)ViewState["dt"];
        else
        {
            DS = new DataSet();
            DS.ReadXmlSchema(Server.MapPath("DepdFile.xml"));
            Session["Dataset"] = DS;
            DT = DS.Tables[0];
        }
        DataRow dr = DT.NewRow();

        dr[0] = ddlTitle.SelectedValue;
        dr[1] = txtMemberName.Text;
        dr[2] = txtDateOfBirth.Text;
        dr[3] = txtSponsorId.Text;
        dr[4] = txtEmployeeNo.Text;
        dr[5] = txtSaudiID.Text;
        dr[6] = txtDepartmentCode.Text;
        dr[7] = ddlGender.SelectedValue;
        dr[8] = ddlNationality.SelectedValue;
        dr[9] = ddlLevelOfCover.SelectedValue;
        dr[10] = ddlJoinReason.SelectedValue;
        //dr[11] = ddlRelationShip.SelectedValue;
        dr[12] = txtJoiningDate.Text;
        dr[13] = txtRequestedStartDate.Text;
        dr[14] = txtMobileNo.Text;
        dr[15] = ddlMemberType.SelectedValue;
        dr[16] = ddlIDType.SelectedValue;
        dr[17] = txtIDExpDate.Text;
        dr[18] = ddlMaritalStatus.SelectedValue;
        dr[19] = ddDepProfession.SelectedValue;
        dr[20] = ddDepDistrict.SelectedValue;
        

        DT.Rows.Add(dr);
        reset();
        GridView1.DataSource = DT;
        GridView1.DataBind();
        GetAllDependentsName();

        ViewState["dt"] = DT;
    }

    public void reset()
    {
        txtMemberName.Text = "";
        txtDateOfBirth.Text = "";

        txtEmployeeNo.Text = "";
        txtSaudiID.Text = "";
        txtDepartmentCode.Text = "";
        ddlGender.SelectedIndex = 0;
        ddlNationality.SelectedValue = "";
        ddlLevelOfCover.SelectedIndex = 0;
        ddlJoinReason.SelectedIndex = 0;
        //ddlRelationShip.SelectedIndex = 0;
        txtJoiningDate.Text = "";
        txtRequestedStartDate.Text = "";
        txtMobileNo.Text = "";
        ddlMemberType.SelectedIndex = 0;
        ddlIDType.SelectedIndex = 0;
        txtIDExpDate.Text = "";
        ddlMaritalStatus.SelectedIndex = 0;
        ddDepProfession.SelectedIndex = 0;
        ddDepDistrict.SelectedIndex = 0;
        GridView1.DataBind();
    }

    protected void Button3_Click(object sender, System.EventArgs e)
    {

        if (updateVal(GridView1.SelectedIndex) == true)
        {
            GridView1.SelectedIndex = -1;
            reset();

            btnUpdate.Visible = false;
            btnSubmit.Visible = true;
            btnCancel.Visible = false;
            btnAdd.Visible = true;
        }
    }

    public void DeleteRow(int RowIndex)
    {
        DT = (DataTable)ViewState["dt"];
        DT.Rows[RowIndex].Delete(); isRowDeleted = "true";
        GridView1.SelectedIndex = -1;
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        reset();
        GridView1.DataSource = DT;
        GridView1.DataBind();
        GetAllDependentsName();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int categoryID = e.RowIndex;
        if (isRowDeleted != "true") DeleteRow(categoryID);
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnCancel.Visible = false;
        GridView1.DataSource = DT;
        GridView1.DataBind();
        GetAllDependentsName();

    }

    protected void GridView1_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            param = (int)Session["Parm"];
            DeleteRow(param);

            if (DependentFlag == true)
            {
                int DepCounter = Int32.Parse(txtFlagForAddingDepValidation.Text) - 1;
                txtFlagForAddingDepValidation.Text = DepCounter.ToString();
            }

        }
    }
    protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference((System.Web.UI.Control)sender, "Select$" + e.Row.RowIndex.ToString()));
    }

    protected void GridView1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        int selectIndex = GridView1.SelectedIndex;
        FindData(selectIndex);
        Session["Parm"] = selectIndex;
        param = (int)Session["Parm"];
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnSubmit.Visible = false;
        btnAdd.Visible = false;
        GridView1.DataSource = DT;
        GridView1.DataBind();
        GetAllDependentsName();



    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        GridView1.SelectedIndex = -1;
        reset();
        btnUpdate.Visible = false;
        btnCancel.Visible = false;
        btnSubmit.Visible = true;
        btnAdd.Visible = true;
        GridView1.Visible = true;
        DT = (DataTable)ViewState["dt"];
        GridView1.DataSource = DT;
        GridView1.DataBind();
        GetAllDependentsName();
    }


    protected void Button2_Click(object sender, System.EventArgs e)
    {
        lbler1.Text = "";
        lblEr.Text = "";
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;
        avayaWS.Service1 _avaya = new avayaWS.Service1();
        string strCount = "";
        bool _stop = false;
        int _count = 0;
        string _msg = "";

        #region Caesar Invocation

        Session.Timeout = 290; ;

        int c = 0;
        int r = 0;

        if (ViewState["dt"] != null)
        {
            DT = (DataTable)ViewState["dt"];
            c = DT.Columns.Count;
            r = DT.Rows.Count;
            string[,] str = new string[r + 1, c];
        }

        //if (Request.QueryString["EType"] != null && Request.QueryString["EType"] == "Dep")
        //{
        //    DT = (DataTable)ViewState["dt"];
        //    c = DT.Columns.Count;
        //    r = DT.Rows.Count;
        //    string[,] str = new string[r + 1, c];
        //    string strr = "";
        //}

        string strTitle = ddlETitle.SelectedValue;
        string strMemberName = txtEMemberName.Text;
        string strDateOfBirth = txtEDateOfBirth.Text;
        string strSponsorId = txtESponsorId.Text;
        string strEmployeeNo = txtEEmployeeNo.Text;
        string strSaudiID = txtESaudiID.Text;
        string strDepartmentCode = txtEDepartmentCode.Text;
        string strGender = ddlEGender.SelectedValue;
        string strNationality = ddlENationality.SelectedValue;
        string strLevelOfCover = ddlELevelOfCover.SelectedValue;
        string strJoinReason = ddlEJoinReason.SelectedValue;
        string strBranchCode = ddlEBranchCode.SelectedValue;
        string strJoiningDate = txtEJoiningDate.Text;//.CalendarDateString;//.Text;
        string strRequestedStartDate = txtERequestedStartDate.Text;//.CalendarDateString;
        string strMobileNo = txtEMobileNo.Text;
        string strMemberType = ddlEMemberType.SelectedValue;
        string strIDType = ddlEIDType .SelectedValue;
        string strIDExpDate = txtEIDExpDate.Text;
        string strMaritalStatus = ddlEMaritalStatus.SelectedValue;
        string strProfession = ddProfession.SelectedValue;
        string strDistrict = ddDistrict.SelectedValue;

        CaesarWS.SubTxnRequest_DN request = new CaesarWS.SubTxnRequest_DN();
        //request.TransactionID = TransactionManager.TransactionID();
        request.TransactionID = WebPublication.GenerateTransactionID();
        if (_isBatch)
        {
            request.BatchIndicator = "Y";
        }
        else
        {
            request.BatchIndicator = "N";
        }



        if (DependentFlag == false)
        {
            request.detail = new CaesarWS.AllMbrDetail_DN[r + 1];
            strCount = _avaya.GetmbrCountByID(txtESaudiID.Text, Convert.ToString(Session["ClientID"]));
            if (Convert.ToInt32(strCount) > 0)
            {
                lblEr.Text = "ID/Iqama number already registered on the same contract in our system<br> رقم الهوية/ الاقامة مسجل مسبقاً ";
                return;
            }
            else
            {
                lblEr.Text = "";
            }

            //request.detail = new CaesarWS.AllMbrDetail_DN[r];
            request.detail[0] = new CaesarWS.AllMbrDetail_DN();
            request.detail[0].ContractNo = strClientID;// "10063400"; //  inputContractNo.Value;

            //incase its employee
            request.detail[0].BranchCode = strBranchCode; // "10063400"; //ddlEBranchCode.SelectedValue; // inputBranchCode.Value;

            request.detail[0].MbrName = txtEMemberName.Text; // inputMbrName.Value;
            if (txtEDateOfBirth.Text != "")
            {
                request.detail[0].DOB = DateTime.ParseExact(txtEDateOfBirth.Text, "dd/MM/yyyy", null);
            }

            request.detail[0].ClassID = strLevelOfCover; // "1";// ddlELevelOfCover.SelectedValue; // inputClassID.Value;

            //incase of new Member it should be empty
            request.detail[0].MainMemberNo = ""; // inputMainMemberNo.Value;

            request.detail[0].DeptCode = txtEDepartmentCode.Text; // inputDeptCode.Value;
            request.detail[0].IgamaID = txtESaudiID.Text; // inputIgamaID.Value;
            request.detail[0].SponsorID = txtESponsorId.Text; // inputSponsorID.Value;
            request.detail[0].StaffNo = txtEEmployeeNo.Text; // inputStaffNo.Value;
            request.detail[0].Nationality = ddlENationality.SelectedValue; // inputNationality.Value;
            request.detail[0].Gender = ddlEGender.SelectedValue; // inputGender.Value;
            request.detail[0].RequestStartDate = DateTime.ParseExact(txtERequestedStartDate.Text, "dd/MM/yyyy", null); //txtERequestedStartDate.CalendarDate;//, "dd-MM-yyyy", null);

            //incase of new employee it would be system date
            request.detail[0].EffectiveDate = DateTime.ParseExact(txtERequestedStartDate.Text, "dd/MM/yyyy", null); // txtERequestedStartDate.CalendarDate;// DateTime.ParseExact(strRequestedStartDate, "dd-MM-yyyy", null);

            request.detail[0].JoinDate = DateTime.ParseExact(txtEJoiningDate.Text, "dd/MM/yyyy", null); // txtEJoiningDate.CalendarDate;//, "dd-MM-yyyy", null);
            request.detail[0].ArrivalDate = DateTime.ParseExact(txtEJoiningDate.Text, "dd/MM/yyyy", null); // txtEJoiningDate.CalendarDate;//, "dd-MM-yyyy", null);

            request.detail[0].PreviousMembershipIndicator = "N"; // inputPreviousMembershipIndicator.Value;

            if (chkPreviousMember.Checked == true)
            {
                request.detail[0].PreviousMembershipIndicator = "Y"; // inputPreviousMembershipIndicator.Value;    
                request.detail[0].PreviousMembershipNo = txtPreviousMember.Text; // inputPreviousMembershipNo.Value;
            }


            if (strJoinReason != "0")
                request.detail[0].Reason = ddlEJoinReason.SelectedValue; // inputReason.Value;

            request.detail[0].Title = ddlETitle.SelectedValue; // inputTitle.Value;

            //incase of employee it would be 'E' else realtion for the dep
            //request.detail[0].MemberType = "E";// inputMemberType.Value;
            request.detail[0].MemberType = strMemberType;
            request.detail[0].ID_Type = strIDType;
            if (strMaritalStatus != "0")
            {
                request.detail[0].Marital_Status = strMaritalStatus;
            }
            if (strIDExpDate != "")
            {
                request.detail[0].ID_Expiry_Date = DateTime.ParseExact(strIDExpDate, "dd/MM/yyyy", null);
            }
            request.detail[0].MBR_MOBILE_NO = txtEMobileNo.Text; // inputMBR_MOBILE_NO.Value;
            request.detail[0].SMS_PREF_LANG = "N";// inputSMS_PREF_NO.Value;
            request.detail[0].sql_type = "CSR.SUB_TXN_REC";
            request.detail[0].Telephone = txtEMobileNo.Text;
            request.detail[0].Phone = txtEMobileNo.Text;
            // request.detail[0].

            request.detail[0].Question1 = rdbQ1.Text;// r Mbr1inputDelcareQ1.Value;
            request.detail[0].Question1Case = txtQ1Box1.Text; // Mbr1inputDelcareQ1Case.Value;
            request.detail[0].Question1Name = txtQ1Box2.Text;// Mbr1inputDelcareQ1Name.Value;
            request.detail[0].Question2 = rdbQ2.Text;// Mbr1inputDelcareQ2.Value;
            request.detail[0].Question2Case = txtQ2Box1.Text; // Mbr1inputDelcareQ2Case.Value;
            request.detail[0].Question2Name = txtQ2Box2.Text; // Mbr1inputDelcareQ2Name.Value;
            request.detail[0].Question3 = rdbQ3.Text;// Mbr1inputDelcareQ3.Value;
            request.detail[0].Question3Case = txtQ3Box1.Text; // Mbr1inputDelcareQ3Case.Value;
            request.detail[0].Question3Name = txtQ3Box2.Text; // Mbr1inputDelcareQ3Name.Value;
            request.detail[0].Question4 = rdbQ4.Text;// Mbr1inputDelcareQ4.Value;
            request.detail[0].Question4Case = txtQ4Box1.Text; // Mbr1inputDelcareQ4Case.Value;
            request.detail[0].Question4Name = txtQ4Box2.Text; // Mbr1inputDelcareQ4Name.Value;
            request.detail[0].Question5 = rdbQ5.Text;// Mbr1inputDelcareQ5.Value;
            request.detail[0].Question5Case = txtQ5Box1.Text; // Mbr1inputDelcareQ5Case.Value;
            request.detail[0].Question5Name = txtQ5Box2.Text; // Mbr1inputDelcareQ5Name.Value;
            request.detail[0].Question6 = rdbQ6.Text;// Mbr1inputDelcareQ6.Value;
            request.detail[0].Question6Case = txtQ6Box1.Text; // Mbr1inputDelcareQ6Case.Value;
            request.detail[0].Question6Name = txtQ6Box2.Text; // Mbr1inputDelcareQ6Name.Value;
            request.detail[0].Question7 = rdbQ7.Text;// Mbr1inputDelcareQ7.Value;
            request.detail[0].Question7Case = txtQ7Box1.Text; // Mbr1inputDelcareQ7Case.Value;
            request.detail[0].Question7Name = txtQ7Box2.Text; // Mbr1inputDelcareQ7Name.Value;
            request.detail[0].Question8 = rdbQ8.Text;// Mbr1inputDelcareQ8.Value;
            request.detail[0].Question8Case = txtQ8Box1.Text; // Mbr1inputDelcareQ8Case.Value;
            request.detail[0].Question8Name = txtQ8Box2.Text; // Mbr1inputDelcareQ8Name.Value;
            request.detail[0].Question9 = rdbQ9.Text;// Mbr1inputDelcareQ9.Value;
            request.detail[0].Question9Case = txtQ9Box1.Text; // Mbr1inputDelcareQ9Case.Value;
            request.detail[0].Question9Name = txtQ9Box2.Text; // Mbr1inputDelcareQ9Name.Value;
            request.detail[0].Question10 = rdbQ10.Text;// Mbr1inputDelcareQ10.Value;
            request.detail[0].Question10Case = txtQ10Box1.Text; // Mbr1inputDelcareQ10Case.Value;
            request.detail[0].Question10Name = txtQ10Box2.Text; // Mbr1inputDelcareQ10Name.Value;
            request.detail[0].Question11 = rdbQ11.Text;// Mbr1inputDelcareQ11.Value;
            request.detail[0].Question11Case = txtQ11Box1.Text; // Mbr1inputDelcareQ11Case.Value;
            request.detail[0].Question11Name = txtQ11Box2.Text; // Mbr1inputDelcareQ11Name.Value;
            request.detail[0].Question12 = rdbQ12.Text;// Mbr1inputDelcareQ12.Value;
            request.detail[0].Question12Case = txtQ12Box1.Text; // Mbr1inputDelcareQ12Case.Value;
            request.detail[0].Question12Name = txtQ12Box2.Text; // Mbr1inputDelcareQ12Name.Value;
            request.detail[0].Question13 = rdbQ13.Text;// Mbr1inputDelcareQ13.Value;
            request.detail[0].Question13Case = txtQ13Box1.Text; // Mbr1inputDelcareQ13Case.Value;
            request.detail[0].Question13Name = txtQ13Box2.Text; // Mbr1inputDelcareQ13Name.Value;
            request.detail[0].Question14 = rdbQ14.Text;// Mbr1inputDelcareQ14.Value;
            request.detail[0].Question14Case = txtQ14Box1.Text; // Mbr1inputDelcareQ14Case.Value;
            request.detail[0].Question14Name = txtQ14Box2.Text; // Mbr1inputDelcareQ14Name.Value;
            request.detail[0].Question15 = rdbQ15.Text;//Mbr1inputDelcareQ15.Value;
            request.detail[0].Question15Case = txtQ15Box1.Text; // Mbr1inputDelcareQ15Case.Value;
            request.detail[0].Question15Name = txtQ15Box2.Text; // Mbr1inputDelcareQ15Name.Value;
            request.detail[0].Question16 = rdbQ16.Text;//Mbr1inputDelcareQ16.Value;
            request.detail[0].Question16Case = txtQ16Box1.Text; // Mbr1inputDelcareQ16Case.Value;
            request.detail[0].Question16Name = txtQ16Box2.Text; // Mbr1inputDelcareQ16Name.Value;
            request.detail[0].Question17 = rdbQ17.Text;//Mbr1inputDelcareQ16.Value;
            request.detail[0].Question17Case = txtQ17Box1.Text; // Mbr1inputDelcareQ16Case.Value;
            request.detail[0].Question17Name = txtQ17Box2.Text; // Mbr1inputDelcareQ16Name.Value;
            request.detail[0].Question18 = rdbQ18.Text;//Mbr1inputDelcareQ16.Value;
            request.detail[0].Question18Case = txtQ18Box1.Text; // Mbr1inputDelcareQ16Case.Value;
            request.detail[0].Question18Name = txtQ18Box2.Text; // Mbr1inputDelcareQ16Name.Value;

            //AKIF: 2014-01-02 [Uncomment below if need to send question number 19]
            request.detail[0].Question19 = rdbQ19.Text;
            request.detail[0].Question19Case = txtQ19Box1.Text;
            request.detail[0].Question19Name = txtQ19Box2.Text;
            request.detail[0].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
            request.detail[0].CCHI_City_Code = strDistrict;
            request.detail[0].CCHI_Job_Code = strProfession;
            //request.detail[0].Have_Dependents = "N";
        }
        else {
            request.detail = new CaesarWS.AllMbrDetail_DN[r];
        }


        for (int i = 0; i < r; i++)
        {
            strTitle = (String)DT.Rows[i][0];
            strMemberName = (String)DT.Rows[i][1];
            strDateOfBirth = (String)DT.Rows[i][2];
            strSponsorId = (String)DT.Rows[i][3];
            strEmployeeNo = (String)DT.Rows[i][4];
            strSaudiID = (String)DT.Rows[i][5];
            _msg = "ID/Iqama number already registered on the same contract in our system<br>  رقم الهوية/ الاقامة مسجل مسبقاً ";
            strCount = _avaya.GetmbrCountByID(strSaudiID, Convert.ToString(Session["ClientID"]));
            if (Convert.ToInt32(strCount) > 0)
            {

                _msg = _msg + "<br> " + Convert.ToString(i + 1) + " )- ID/Iqama : " + strSaudiID;
                _count = _count + 1;
            }

            strDepartmentCode = (String)DT.Rows[i][6];
            strGender = (String)DT.Rows[i][7];
            strNationality = (String)DT.Rows[i][8];
            strLevelOfCover = (String)DT.Rows[i][9];
            strJoinReason = (String)DT.Rows[i][10];
            //strBranchCode = (String)DT.Rows[i][11];
            strJoiningDate = (String)DT.Rows[i][12];
            strRequestedStartDate = (String)DT.Rows[i][13];
            strMobileNo = (String)DT.Rows[i][14];
            strMemberType = (String)DT.Rows[i][15];
            strIDType = (String)DT.Rows[i][16];
            strIDExpDate = (String)DT.Rows[i][17];
            strMaritalStatus = (String)DT.Rows[i][18];
            strProfession = (String)DT.Rows[i][19];
            strDistrict = (String)DT.Rows[i][20];

            int j;
            if (DependentFlag == true)
                j = i;
            else
                j = i + 1;

            request.detail[j] = new CaesarWS.AllMbrDetail_DN();
            request.detail[j].ContractNo = strClientID;// "10063400"; //  inputContractNo.Value;

            //incase its employee
            request.detail[j].BranchCode = ddlEBranchCode.SelectedValue; // inputBranchCode.Value;

            request.detail[j].MbrName = strMemberName;// txtEMemberName.Text; // inputMbrName.Value;
            request.detail[j].DOB = DateTime.ParseExact(strDateOfBirth, "dd/MM/yyyy", null);

            request.detail[j].ClassID = strLevelOfCover;// "1";// ddlELevelOfCover.SelectedValue; // inputClassID.Value;

            //incase of Member it should be empty
            if (DependentFlag == true)
                request.detail[j].MainMemberNo = txtMainMemberNo.Text; // ""; // inputMainMemberNo.Value;
            else
                request.detail[j].MainMemberNo = ""; // inputMainMemberNo.Value;


            request.detail[j].DeptCode = strDepartmentCode;// txtEDepartmentCode.Text; // inputDeptCode.Value;
            request.detail[j].IgamaID = strSaudiID;// txtESaudiID.Text; // inputIgamaID.Value;
            request.detail[j].SponsorID = strSponsorId;// txtESponsorId.Text; // inputSponsorID.Value;
            request.detail[j].StaffNo = strEmployeeNo;// txtEEmployeeNo.Text; // inputStaffNo.Value;
            request.detail[j].Nationality = strNationality;// ddlENationality.SelectedValue; // inputNationality.Value;
            request.detail[j].Gender = strGender;// ddlEGender.SelectedValue; // inputGender.Value;
            request.detail[j].RequestStartDate = DateTime.ParseExact(strRequestedStartDate, "dd/MM/yyyy", null);

            //incase of new employee it would be system date
            request.detail[j].EffectiveDate = DateTime.ParseExact(strRequestedStartDate, "dd/MM/yyyy", null);

            request.detail[j].JoinDate = DateTime.ParseExact(strJoiningDate, "dd/MM/yyyy", null);
            request.detail[j].ArrivalDate = DateTime.ParseExact(strJoiningDate, "dd/MM/yyyy", null);

            request.detail[j].PreviousMembershipIndicator = "N"; // inputPreviousMembershipIndicator.Value;
            request.detail[j].PreviousMembershipNo = ""; // inputPreviousMembershipNo.Value;

            if (strJoinReason != "0")
                request.detail[j].Reason = strJoinReason;// ddlEJoinReason.SelectedValue; // inputReason.Value;

            request.detail[j].Title = strTitle;// ddlETitle.SelectedValue; // inputTitle.Value;

            //incase of employee it would be 'E' else realtion for the dep
            //request.detail[j].MemberType = strBranchCode; // "E";// inputMemberType.Value;

            request.detail[j].MBR_MOBILE_NO = strMobileNo;// txtEMobileNo.Text; // inputMBR_MOBILE_NO.Value;
            request.detail[j].SMS_PREF_LANG = "N";// inputSMS_PREF_NO.Value;
            request.detail[j].sql_type = "CSR.SUB_TXN_REC";

            request.detail[j].MemberType = strMemberType;
            request.detail[j].ID_Type = strIDType;
            if (strIDExpDate != "")
            {
                request.detail[j].ID_Expiry_Date = DateTime.ParseExact(strIDExpDate, "dd/MM/yyyy", null);
                
            }
            if (strMaritalStatus != "0")
            {
                request.detail[j].Marital_Status = strMaritalStatus;
            }
            request.detail[j].CCHI_Job_Code = strProfession;
            request.detail[j].CCHI_City_Code = strDistrict;
            request.detail[j].Telephone = strMobileNo;
            request.detail[j].Phone = strMobileNo;

            request.detail[j].Question1 = rdbQ1.Text;// r Mbr1inputDelcareQ1.Value;
            request.detail[j].Question1Case = txtQ1Box1.Text; // Mbr1inputDelcareQ1Case.Value;
            request.detail[j].Question1Name = txtQ1Box2.Text;// Mbr1inputDelcareQ1Name.Value;
            request.detail[j].Question2 = rdbQ2.Text;// Mbr1inputDelcareQ2.Value;
            request.detail[j].Question2Case = txtQ2Box1.Text; // Mbr1inputDelcareQ2Case.Value;
            request.detail[j].Question2Name = txtQ2Box2.Text; // Mbr1inputDelcareQ2Name.Value;
            request.detail[j].Question3 = rdbQ3.Text;// Mbr1inputDelcareQ3.Value;
            request.detail[j].Question3Case = txtQ3Box1.Text; // Mbr1inputDelcareQ3Case.Value;
            request.detail[j].Question3Name = txtQ3Box2.Text; // Mbr1inputDelcareQ3Name.Value;
            request.detail[j].Question4 = rdbQ4.Text;// Mbr1inputDelcareQ4.Value;
            request.detail[j].Question4Case = txtQ4Box1.Text; // Mbr1inputDelcareQ4Case.Value;
            request.detail[j].Question4Name = txtQ4Box2.Text; // Mbr1inputDelcareQ4Name.Value;
            request.detail[j].Question5 = rdbQ5.Text;// Mbr1inputDelcareQ5.Value;
            request.detail[j].Question5Case = txtQ5Box1.Text; // Mbr1inputDelcareQ5Case.Value;
            request.detail[j].Question5Name = txtQ5Box2.Text; // Mbr1inputDelcareQ5Name.Value;
            request.detail[j].Question6 = rdbQ6.Text;// Mbr1inputDelcareQ6.Value;
            request.detail[j].Question6Case = txtQ6Box1.Text; // Mbr1inputDelcareQ6Case.Value;
            request.detail[j].Question6Name = txtQ6Box2.Text; // Mbr1inputDelcareQ6Name.Value;
            request.detail[j].Question7 = rdbQ7.Text;// Mbr1inputDelcareQ7.Value;
            request.detail[j].Question7Case = txtQ7Box1.Text; // Mbr1inputDelcareQ7Case.Value;
            request.detail[j].Question7Name = txtQ7Box2.Text; // Mbr1inputDelcareQ7Name.Value;
            request.detail[j].Question8 = rdbQ8.Text;// Mbr1inputDelcareQ8.Value;
            request.detail[j].Question8Case = txtQ8Box1.Text; // Mbr1inputDelcareQ8Case.Value;
            request.detail[j].Question8Name = txtQ8Box2.Text; // Mbr1inputDelcareQ8Name.Value;
            request.detail[j].Question9 = rdbQ9.Text;// Mbr1inputDelcareQ9.Value;
            request.detail[j].Question9Case = txtQ9Box1.Text; // Mbr1inputDelcareQ9Case.Value;
            request.detail[j].Question9Name = txtQ9Box2.Text; // Mbr1inputDelcareQ9Name.Value;
            request.detail[j].Question10 = rdbQ10.Text;// Mbr1inputDelcareQ10.Value;
            request.detail[j].Question10Case = txtQ10Box1.Text; // Mbr1inputDelcareQ10Case.Value;
            request.detail[j].Question10Name = txtQ10Box2.Text; // Mbr1inputDelcareQ10Name.Value;
            request.detail[j].Question11 = rdbQ11.Text;// Mbr1inputDelcareQ11.Value;
            request.detail[j].Question11Case = txtQ11Box1.Text; // Mbr1inputDelcareQ11Case.Value;
            request.detail[j].Question11Name = txtQ11Box2.Text; // Mbr1inputDelcareQ11Name.Value;
            request.detail[j].Question12 = rdbQ12.Text;// Mbr1inputDelcareQ12.Value;
            request.detail[j].Question12Case = txtQ12Box1.Text; // Mbr1inputDelcareQ12Case.Value;
            request.detail[j].Question12Name = txtQ12Box2.Text; // Mbr1inputDelcareQ12Name.Value;
            request.detail[j].Question13 = rdbQ13.Text;// Mbr1inputDelcareQ13.Value;
            request.detail[j].Question13Case = txtQ13Box1.Text; // Mbr1inputDelcareQ13Case.Value;
            request.detail[j].Question13Name = txtQ13Box2.Text; // Mbr1inputDelcareQ13Name.Value;
            request.detail[j].Question14 = rdbQ14.Text;// Mbr1inputDelcareQ14.Value;
            request.detail[j].Question14Case = txtQ14Box1.Text; // Mbr1inputDelcareQ14Case.Value;
            request.detail[j].Question14Name = txtQ14Box2.Text; // Mbr1inputDelcareQ14Name.Value;
            request.detail[j].Question15 = rdbQ15.Text;//Mbr1inputDelcareQ15.Value;
            request.detail[j].Question15Case = txtQ15Box1.Text; // Mbr1inputDelcareQ15Case.Value;
            request.detail[j].Question15Name = txtQ15Box2.Text; // Mbr1inputDelcareQ15Name.Value;
            request.detail[j].Question16 = rdbQ16.Text;//Mbr1inputDelcareQ16.Value;
            request.detail[j].Question16Case = txtQ16Box1.Text; // Mbr1inputDelcareQ16Case.Value;
            request.detail[j].Question16Name = txtQ16Box2.Text; // Mbr1inputDelcareQ16Name.Value;

            request.detail[j].Question17 = rdbQ17.Text;//Mbr1inputDelcareQ16.Value;
            request.detail[j].Question17Case = txtQ17Box1.Text; // Mbr1inputDelcareQ16Case.Value;
            request.detail[j].Question17Name = txtQ17Box2.Text; // Mbr1inputDelcareQ16Name.Value;

            request.detail[j].Question18 = rdbQ18.Text;//Mbr1inputDelcareQ16.Value;
            request.detail[j].Question18Case = txtQ18Box1.Text; // Mbr1inputDelcareQ16Case.Value;
            request.detail[j].Question18Name = txtQ18Box2.Text; // Mbr1inputDelcareQ16Name.Value;

            //AKIF: 2014-01-02 [Uncomment below if need to send question number 19]
            request.detail[j].Question19 = rdbQ19.Text;
            request.detail[j].Question19Case = txtQ19Box1.Text;
            request.detail[j].Question19Name = txtQ19Box2.Text; 

            request.detail[j].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);

            //request.detail[j].Have_Dependents = "Y";
        }
        if (_count > 0)
        {
            _stop = true;
        }
        if (_stop)
        {
            lbler1.Text = _msg;
            return;
        }
        else
        {
            lblEr.Text = "";
        }
        //new functionality added - Declaration form
        if (Session["ClientName"] == null)
            request.detail[0].GroupSecretaryName = Session["ClientName"].ToString();

        if (DependentFlag == false)
            request.detail[0].NoPeopleJoin = r + 1;
        else
            request.detail[0].NoPeopleJoin = r;


        request.memberName = new String[3];
        request.natureOfIllness = new String[3];
        request.periodOfDisability = new String[3];
        request.presentState = new String[3];
        request.previousMembershipNo = new String[3];
        request.medicalFacility = new String[3];

        //these two lines below are the constant which are required to get the function working properly.
        request.TransactionType = "ADD MEMBER02";

        for (int j = 0; j < 3; j++)
        {
            request.memberName[j] = "3";
            request.natureOfIllness[j] = "3";
            request.periodOfDisability[j] = "3";
            request.presentState[j] = "3";
            request.previousMembershipNo[j] = "3";
            request.medicalFacility[j] = "3";
        }

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        ws = new CaesarWS.ServiceDepot_DNService();
        CaesarWS.SubTxnResponse_DN response;

        try
        {

            StringBuilder sb = new StringBuilder(200);
            //HttpContext.Current.Response.Write(request.detail[0].Marital_Status);

            response = ws.SubTxn02(request);
            Message1.Text = "";
            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");
                Message1.Visible = true;
                Message1.Text += sb.ToString();
                Message1.ForeColor = System.Drawing.Color.Red;

                caesarInvokeError = true;
            }
            else
            {
                Message1.Visible = true;
                Message1.ForeColor = System.Drawing.Color.Black; // "black";
                Message1.Text = "<br/><br/><br/>Thank you for submitting your request. We are now validating the submitted file. Your reference no. is " + response.ReferenceNo.ToString() + ".<br/>";

                PanelEmployee.Visible = false;
                PanelDeclaration.Visible = false;
                PanelMembershipNoOnly.Visible = false;
                PanelDependent.Visible = false;
                GridView1.Visible = false;
                btnSubmit.Visible = false;
                tblUploader.Visible = false;
                caesarInvokeError = false;
                caesarReferenceID = response.ReferenceNo.ToString();
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message + "" + ex.InnerException;
            Message1.Visible = true;
        }

        #endregion

        if (!caesarInvokeError)
        {
            // Send the email related to the attachments
            string username = "Unknown username";
            string fullName = "Unknown client";
            string[] addressees = UploadPublication.MembershipEmailAddresses();

            if (Session[WebPublication.Session_ClientUsername] != null)
                username = Session[WebPublication.Session_ClientUsername].ToString();
            if (Session[WebPublication.Session_ClientFullName] != null)
                fullName = Session[WebPublication.Session_ClientFullName].ToString();

            // Delegate to send emails
            //uploader.Notify(uploader.SessionID, _uploadCategory, caesarReferenceID, addressees, fullName, username);

            // Once done, hide the uploader from the view
            tblUploader.Visible = false;
        }
        tblUploader.Visible = false;
    }

    // function to list all branches for the client, called by PopulateBranchList
    private void DisplayBranchListResult(CaesarWS.ReqBrhListResponse_DN response)
    {
        try
        {
            if (response.status == "0")
            {
                //sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
                ddlEBranchCode.Items.Clear();

                ListItem DepList0 = new ListItem();
                DepList0.Text = "-Select-";
                DepList0.Value = "0";
                ddlEBranchCode.Items.Add(DepList0);

                foreach (CaesarWS.BrhListDetail_DN dtl in response.detail)
                {
                    ListItem DepList = new ListItem();
                    DepList.Text = dtl.branchDesc;
                    DepList.Value = dtl.branchCode.ToString();
                    ddlEBranchCode.Items.Add(DepList);

                }
            }
            else
            {
                lblEr.Text = "Please refresh the page (Ctrl+F5)";
            }
        }
        catch (Exception ex)
        {
            lblEr.Text = ex.Message;
        }
    }

    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void PopulateBranchList(string ClientID)
    {
        {
            ws = new CaesarWS.ServiceDepot_DNService();

            CaesarWS.ReqBrhListRequest_DN request = new CaesarWS.ReqBrhListRequest_DN();
            CaesarWS.ReqBrhListResponse_DN response;
            request.membershipNo = long.Parse(strClientID);

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();

            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.ReqBrhList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                }
                else
                {
                    DisplayBranchListResult(response);
                }
            }
            catch (Exception ex)
            {
                Message1.Text = ex.Message;
            }

        }

    }
    // function to list all classes for the client, called by PopulateClassList
    private void DisplayClassListResult(CaesarWS.ReqClsListResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            //sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            ddlLevelOfCover.Items.Clear();
            ddlELevelOfCover.Items.Clear();
            ListItem DepList0 = new ListItem();
            DepList0.Text = "-Select-";
            DepList0.Value = "0";
            ddlLevelOfCover.Items.Add(DepList0);
            ddlELevelOfCover.Items.Add(DepList0);

            foreach (CaesarWS.ClsListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.className;
                DepList.Value = dtl.classID.ToString();
                ddlLevelOfCover.Items.Add(DepList);
                ddlELevelOfCover.Items.Add(DepList);

            }
        }
        else
        {
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }
    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void PopulateClassList(string ClientID)
    {

        {

            // UpdatePanel2.Visible = true;
            ws = new CaesarWS.ServiceDepot_DNService();

            CaesarWS.ReqClsListRequest_DN request = new CaesarWS.ReqClsListRequest_DN();
            CaesarWS.ReqClsListResponse_DN response;
            request.membershipNo = long.Parse(strClientID);
            //request..membershipNo = 
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();
            // DateTime.Now.ToFileTime 
            //   request.transactionID = long.Parse(String.Format("{0}{1}{2}", DateTime.Now.DayOfYear * 24, DateTime.Now.Year.ToString().Substring(3, 1), DateTime.Now.ToString().Substring(14, 5).Replace(":", "") + DateTime.Now.Millisecond.ToString().Substring(0, 1)));
            //request.transactionID = TransactionManager.TransactionID();
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";

            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.ReqClsList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                }
                else
                {
                    DisplayClassListResult(response);


                }
            }
            catch (Exception ex)
            {
                Message1.Text = ex.Message;
            }

            if (ddlLevelOfCover.Items.Count > 0)
            {
                //////          TabFlag = "D";
                // ddlBranchCode.Visible = true;
                //this.lblTabLoadStatus.Visible = false;
                //btnRequestDependentCert.Visible = true;
            }
            //else
            //  this.lblTabLoadStatus.Text = "No dependent for this Member Found";
        }

    }
    protected string IsDeclarationRequired(string ClientID)
    {

        {

            // UpdatePanel2.Visible = true;
            ws = new CaesarWS.ServiceDepot_DNService();

            CaesarWS.EnqMandaDeclareFormRequest_DN request = new CaesarWS.EnqMandaDeclareFormRequest_DN();
            CaesarWS.EnqMandaDeclareFormResponse_DN response;

            //request..membershipNo = 
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            request.transactionID = WebPublication.GenerateTransactionID();
            request.contNo = strClientID;
            //request.transactionID = TransactionManager.TransactionID();
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";

            try
            {
                StringBuilder sb = new StringBuilder(200);

                //     ExceptionManager.TrackWSPerformance("AddEmpDepDeclr", "ReqContractType", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.TransactionID.ToString(), strClientID, "", "");
                response = ws.EnqMandaDeclareForm(request);
                //   ExceptionManager.TrackWSPerformance("AddEmpDepDeclr", "ReqContractType", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.TransactionID.ToString(), strClientID,  "", "");

                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                    return response.status;
                }
                else
                {
                    return response.mandaDeclareFormIndicator.Trim();


                }
            }
            catch (Exception ex)
            {
                Message1.Text = ex.Message;
                return "";
            }


        }

    }
    public void GetAllDependentsName()
    {
        // string allNames;
        lblAllDependentsName.Text = "";
        lblEmployeeName.Text = txtEMemberName.Text;
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            if (i == 0)
            {
                lblAllDependentsName.Text = GridView1.Rows[i].Cells[1].Text;
            }
            else
            {
                lblAllDependentsName.Text = lblAllDependentsName.Text + "; " + GridView1.Rows[i].Cells[1].Text;
            }
        }
        //return allNames;
    }
    protected void txtMainMemberNo_TextChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        try
        {
            lblError.Text = "";
            MemberHelper mh = new MemberHelper(txtMainMemberNo.Text, strClientID);
            Member m = mh.GetMemberDetails();
            string strMemberNo = m.SaudiIqamaID;
            string _returnValue = m.SaudiIqamaID;
            if (strMemberNo.StartsWith("3"))
            {
                _returnValue = m.SponsorID;
            }
            if (strMemberNo.StartsWith("4"))
            {
                _returnValue = m.SponsorID;
            }
            if (strMemberNo.StartsWith("5"))
            {
                _returnValue = m.SponsorID;
            }
            if (strMemberNo.StartsWith("6"))
            {
                _returnValue = m.SponsorID;
            }
            if (!string.IsNullOrEmpty(m.SaudiIqamaID))
            {
                //HttpContext.Current.Response.Write(3);
                PanelDependent.Visible = true;
                txtSponsorId.Text = _returnValue;
                //txtSponsorId.Enabled = false;
                Table2.Visible = true;
            }
            
            //HttpContext.Current.Response.Write(m.SaudiIqamaID + "<br/>");
            //HttpContext.Current.Response.Write(m.SponsorID + "<br/>");
            //HttpContext.Current.Response.Write(m.SponsorID + "<br/>");
            //HttpContext.Current.Response.Write(m.SponsorID + "<br/>");
        }
        catch (Exception ex)
        {
            //HttpContext.Current.Response.Write(1);
            lblError.Text = ex.Message;
        }
    }
    //protected void txtESaudiID_TextChanged(object sender, EventArgs e)
    //{
    //    if (ddlENationality.SelectedValue == "966")
    //    {
    //        txtESponsorId.Text = txtESaudiID.Text;
    //        txtESponsorId.Enabled = false;
    //    }
    //    else
    //    {
    //        //txtESponsorId.Text = "";
    //        txtESponsorId.Enabled = true;
    //    }


    //}
    private void LoadCaesarSourcedData()
    {
        BindDdlMemberType();
        BindDdlProfession();
        BindDdlDistrict();
        BindDdlENationality(false);
        BindDdlNationality(false);

        //var connectionString = CoreConfiguration.Instance.GetConfigValue("Caesar Connection String");
        //ddProfession.BackColor = System.Drawing.Color.White;
        //var professions = CaesarDataManager.GetAllProfessions(connectionString);

        //ddProfession.DataTextField = "ProfessionName";
        //ddProfession.DataValueField = "Code";

        //ddProfession.DataSource = professions;
        //ddProfession.DataBind();



        //var districts = CaesarDataManager.GetAllDistricts(connectionString);

        //ddDistrict.DataTextField = "DistrictName";
        //ddDistrict.DataValueField = "Code";

        //ddDistrict.DataSource = districts;
        //ddDistrict.DataBind();
        //ddDistrict.Items.Insert(0, new ListItem("", String.Empty));
        //ddDistrict.SelectedIndex = 0;
    }
    protected void ddProfession_DataBound(object sender, EventArgs e)
    {
        //ddProfession.Items.Insert(0, new ListItem("-select-", ""));
        if (!IsPostBack)
        {
            ddProfession.SelectedIndex = 0;

            //ddDepProfession.Items.Insert(0, new ListItem("-select-", ""));
            ddDepProfession.SelectedIndex = 0;
        }
    }
    protected void ddDistrict_DataBound(object sender, EventArgs e)
    {
        //ddDistrict.Items.Insert(0, new ListItem("-select-", ""));
        if (!IsPostBack)
        {
            ddDistrict.SelectedIndex = 0;

            //ddDepDistrict.Items.Insert(0, new ListItem("-select-", ""));
            ddDepDistrict.SelectedIndex = 0;
        }
    }
    protected void ddlEIDType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtESaudiID.Text = "";
        txtESponsorId.Text = "";
        switch (ddlEIDType.SelectedValue)
        {
            case "1":
                txtESaudiID.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[1][0-9]{9}";
                rfvSuadiID.Text = "Incorrect National ID";
                rfvESponsorID.ValidationExpression = "[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
                txtESponsorId.MaxLength = 10;
                BindDdlENationality(false);
                break;
            case "2":
                txtESaudiID.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[2][0-9]{9}";
                rfvSuadiID.Text = "Incorrect Iqama ID";
                rfvESponsorID.ValidationExpression = "[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
                txtESponsorId.MaxLength = 10;
                BindDdlENationality(false);
                break;
            case "3":
                txtESaudiID.MaxLength = 15;
                rfvSuadiID.ValidationExpression = "([A-Z]|[a-z]|[0-9]){1,15}";
                rfvSuadiID.Text = "Incorrect Passport Number";
                rfvESponsorID.ValidationExpression = "([A-Z]|[a-z]|[0-9]){1,15}";
                txtESponsorId.MaxLength = 15;
                BindDdlENationality(true);

                break;
            case "4":
                txtESaudiID.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[3-5][0-9]{9}";
                rfvSuadiID.Text = "Incorrect Entry Number";
                rfvESponsorID.ValidationExpression = "[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
                txtESponsorId.MaxLength = 10;
                BindDdlENationality(false);
                break;
        }
    }
    protected void ddlIDType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtSaudiID.Text = "";
        //txtSponsorId.Text = "";
        switch (ddlIDType.SelectedValue)
        {
            case "1":
                txtSaudiID.MaxLength = 10;
                rfvDepSuadiID.ValidationExpression = "[1][0-9]{9}";
                rfvDepSuadiID.Text = "Incorrect National ID";
                rfvDepSponsorID.ValidationExpression = "[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
                txtSponsorId.MaxLength = 10;
                BindDdlNationality(false);
                break;
            case "2":
                txtSaudiID.MaxLength = 10;
                rfvDepSuadiID.ValidationExpression = "[2][0-9]{9}";
                rfvDepSuadiID.Text = "Incorrect Iqama ID";
                rfvDepSponsorID.ValidationExpression = "[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
                txtSponsorId.MaxLength = 15;
                BindDdlNationality(false);
                break;
            case "3":
                txtSaudiID.MaxLength = 15;
                rfvDepSuadiID.ValidationExpression = "([A-Z]|[a-z]|[0-9]){1,15}";
                rfvDepSponsorID.ValidationExpression = "([A-Z]|[a-z]|[0-9]){1,15}";
                rfvDepSuadiID.Text = "Incorrect Passport Number";
                txtSponsorId.MaxLength = 10;
                BindDdlNationality(true);
                break;
            case "4":
                txtSaudiID.MaxLength = 10;
                rfvDepSuadiID.ValidationExpression = "[3-5][0-9]{9}";
                rfvDepSuadiID.Text = "Incorrect Entry Number";
                rfvDepSponsorID.ValidationExpression = "[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
                txtSponsorId.MaxLength = 10;
                BindDdlNationality(false);
                break;
        }
    }
}
