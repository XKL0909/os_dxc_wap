﻿using System;
using System.Web.UI;
using System.Configuration;

public partial class Client_Main : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    public string balAmount = string.Empty;
    public string _VIBAN = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            
            string contractType = string.Empty;
            if (string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                Response.Redirect("../default.aspx");
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                if (DateTime.Now.Year == 2014 && DateTime.Now.Month == 1)
                {
                    LabelMedicalDeclarationMessage.Text = @"Dear Bupa client, " + Environment.NewLine +
                        "<br />Medical declaration on Add Employee & Add Dependent has been changed." + Environment.NewLine +
                        "<br />For additional assistance call 8002440302.";
                    Notify.Visible = true;
                }
                ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
                OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN request;
                OS_DXC_WAP.CaesarWS.EnqContInfoResponse_DN response;
                request = new OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN();
                request.contNo = Convert.ToString(Session["ClientID"]);
                request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;
                response = ws.EnqContInfo(request);
                //CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
                Session.Add("OnlineTicked", response.onl_Reimb_Ind);

                balAmount = response.bal_Amt.ToString("N");
                _VIBAN = response.vibanNo;
                //By Athar, to check Contract Effective date
                Session["ContractStartDate"] = response.eff_Date;
                //End

                //MyInbox1.contractNumber = Convert.ToString(Session["ClientID"]);
                ////CR372 Collection Communication Added By Sakthi on 12-Oct2017
                ////Start
                contractType = !string.IsNullOrEmpty(response.cont_Type) ? Convert.ToString(response.cont_Type).ToUpper().Trim() : string.Empty;
                //Aramco PID Changes By Hussamuddin
                Session["ContractType"] = contractType;
                Session["ContractEndDate"] = response.end_Date;
                Session["IsRM"] = response.cust_Seg_Corporate_Ind;

                bool isCorporate = false;
              
                switch (contractType.ToUpper().Trim())
                {
                    case "CORC":
                    case "COSC":
                        {
                            isCorporate = true;
                            break;
                        }
                    //Aramco PID Changes By Hussamuddin
                    case "TPAC":
                        {
                            Session["ContractType"] = contractType.ToUpper().Trim();
                            CCHI1.Visible = false;
                            DashBoardWelcome1.Visible = false;
                            break;
                        }
                    //Aramco PID Changes By Hussamuddin
                    default:
                        break;
                }

                if(!isCorporate)
                {
                    if (Session["UnifiedDisable"] == null)
                    {
                        PopUpOldPortal.Attributes.Add("style", "display:block");
                        Session["UnifiedDisable"] = "Yes";
                    }
                    else
                        PopUpOldPortal.Attributes.Add("style", "display:none");
                }

                
                bool isPopup = false;
                //// Added by sakthi for popup recheck
                //// Start
                string paymentIndicator = Convert.ToString(response.claim_Pay_Hold_Code);

                string customerSegment = string.Empty;
                //// Need to enable the below commented line in production release
                ////customerSegment = !string.IsNullOrEmpty(response.cust_Segment) ? response.cust_Segment.Trim().ToUpper() : string.Empty;
                isPopup = (customerSegment == "M" || customerSegment == "N");

                //if (paymentIndicator.ToUpper() == "S")
                //{
                //    isPopup = false;
                //}
                //else
                //{
                //    if (response.bal_Amt > 100)
                //        isPopup = true;
                //}


                ////End

                //Commented by Athar, to display POP up based on balance amount but not on indicator
                //string paymentIndicator = Convert.ToString(response.claim_Pay_Hold_Code);
                //if (!string.IsNullOrEmpty(paymentIndicator))
                //{
                //    switch (paymentIndicator.ToUpper().Trim())
                //    {
                //        case "R":
                //        case "A":
                //        case "N":
                //        case "W":
                //            {
                //                isPopup = true;
                //                break;
                //            }
                //        default:
                //            break;
                //    }
                //}


                //if (response.bal_Amt > 100)
                //    isPopup = true;

                isPopup = isPopup ? response.bal_Amt > 100  ? true : false : false;
                if (isPopup)
                {                
                    divCollectionDeptPopup.Attributes.Add("style", "display:block");
                }
                else
                    divCollectionDeptPopup.Attributes.Add("style", "display:none");

                bool isCredit = false;
                if (contractType.Equals("BDRC") || contractType.Equals("BDSC"))
                {
                    var input = "21-12-2018";
                    var date = DateTime.ParseExact(input, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    var result = (date - DateTime.Now.Date).Days;
                    isCredit = result > 0;
                }

                divCredit.Visible = isCredit;


                //Start Account lockout Password Expiry Popup
                if (ConfigurationManager.AppSettings["isClientAccountLockedEnabled"].ToString().ToUpper() == "TRUE")
                {
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["PasswordExpiryDate"])))
                    {
                        DateTime dtPWDExp = Convert.ToDateTime(Session["PasswordExpiryDate"]);
                        int getDays = (int)(dtPWDExp - DateTime.Today).TotalDays;
                        TimeSpan totalvalue = dtPWDExp.Subtract(DateTime.Today);
                        string ExpDateEng = "";
                        string ExpDateAr = "";
                        int ExpDays = Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpMsgDays"]);
                        if (totalvalue.Ticks > 0)
                        {
                            DateTime daysLeft = DateTime.Today.AddDays(getDays);
                            ExpDateEng = daysLeft.ToString("d-MMMM-yyyy");
                            ExpDateAr = daysLeft.ToString("d-MMMM-yyyy");
                        }
                        if (getDays == 1)
                        {
                            btnLater.Visible = true;
                            popupPasswordExpiry.Attributes.Add("style", "display:inline");
                            ExpMessage.InnerHtml = "This is a system generated alert to inform you that your password is due to expire on " + ExpDateEng + ". <br />Please reset";
                            ExpMessagear.InnerHtml = "هذا تنبيه تم إنشاؤه بواسطة النظام لإعلامك بأن كلمة المرور الخاصة بك سوف تنتهي في" + "<br />" + ExpDateEng + "<br />يرجى إعادة تعيينها";
                        }
                        else if (getDays < 0)
                        {
                            btnLater.Visible = false;
                            popupPasswordExpiry.Attributes.Add("style", "display:inline");
                            ExpMessage.InnerHtml = "Your password is expired";
                            ExpMessagear.InnerHtml = "لقد تم انتهاء صلاحية كلمة المرور الخاصة بك";
                        }
                        else if (getDays <= ExpDays)
                        {
                            btnLater.Visible = true;
                            popupPasswordExpiry.Attributes.Add("style", "display:inline");
                            ExpMessage.InnerHtml = "This is a system generated alert to inform you that your password is due to expire on " + ExpDateEng + ". <br />Please reset";
                            ExpMessagear.InnerHtml = "هذا تنبيه تم إنشاؤه بواسطة النظام لإعلامك بأن كلمة المرور الخاصة بك سوف تنتهي في" + "<br />" + ExpDateEng + "<br />يرجى إعادة تعيينها";
                        }
                        else
                        {
                            popupPasswordExpiry.Attributes.Add("style", "display:none");
                        }
                        //if (dtPWDExp <= DateTime.Today)
                        //{
                        //    btnLater.Visible = false;
                        //    popupPasswordExpiry.Attributes.Add("style", "display:none");
                        //}
                        //else if (dtPWDExp > DateTime.Today)
                        //{
                        //    popupPasswordExpiry.Attributes.Add("style", "display:none");
                        //}
                        //else
                        //{
                        //    popupPasswordExpiry.Attributes.Add("style", "display:none");
                        //}
                    }
                    else
                    {
                        popupPasswordExpiry.Attributes.Add("style", "display:none");
                    }
                }
                else
                {
                    popupPasswordExpiry.Attributes.Add("style", "display:none");
                }
                //End Account lockout Password Expiry Popup
                if (!string.IsNullOrEmpty(Convert.ToString(response.status)))
                {
                    if (Convert.ToInt32(response.status) >= 0)
                    {
                        if (response.cont_Type.Trim() == "CORC")
                        {
                            if ((Convert.ToDateTime(response.end_Date) - DateTime.Now).Days <= 30)
                            {
                                lblClient.Text = "Dear Bupa client, the healthcare cover for your company will expire on " + Convert.ToDateTime(response.end_Date).ToShortDateString() + ". To avoid any delays in Bupa services, Kindly contact your Relationship Manager for renewal. For additional assistance call 8002440302. Please disregard this message if you have renewed your policy.";
                                Notify.Visible = true;
                            }
                        }
                        else
                        {
                            if ((Convert.ToDateTime(response.end_Date) - DateTime.Now).Days <= 30)
                            {
                                lblClient.Text = "Dear Bupa member, your Bupa healthcare cover will expire on " + Convert.ToDateTime(response.end_Date).ToShortDateString() + ". , Kindly contact your sales executive for renewal. For additional assistance call 8001160500. Please disregard this message if you have renewed your policy.";
                                Notify.Visible = true;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            popupPasswordExpiry.Attributes.Add("style", "display:none");
            PopUpIsSuperUser.Attributes.Add("style", "display:none");
        }
    }
    protected void imgClose_Click(object sender, ImageClickEventArgs e)
    {
        Notify.Visible = false;
    }
}