﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master"  AutoEventWireup="true"
    Inherits="ClaimHistory" Codebehind="ClaimHistory.aspx.cs" %>
    <%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static" >

    

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

    </style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Arabia
             Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    
      
        <table width=100% ><tr><td align=left>
            <label ID="Label1" ><font size=4>My Claim History</font></label>  </td>
            <td><a href="#" onclick="window.print()">
                <img class="noPrint" border="0" src="images/printer.jpg" alt="Print" style="height: 33px; text-align: right;">
            </a> </td></tr></table>
            
            

        <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>

        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>

        <input id="btnTab1" runat="server" style="display: none;" type="button" onserverclick="btnTab1_Click" />
        <input id="btnTab2" runat="server" style="display: none;" type="button" onserverclick="btnTab2_Click" />
 
  <div class="noPrint" >   
        
       <aspAjax:TabContainer runat="server" ID="tabsModManager" BorderStyle="None" BackColor="AliceBlue"
            OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Font-Bold="True"
            Font-Size="Medium">
            <aspAjax:TabPanel runat="server" ID="tabMemberName" Enabled="true" HeaderText="Member"
                Font-Size="Medium" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab1" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>
            
       
            <aspAjax:TabPanel runat="server" ID="TabPanel1" Enabled="false" HeaderText=""
                Width="1px">
                <ContentTemplate>
                    [put your content here..2.]
                </ContentTemplate>
            </aspAjax:TabPanel>

            <aspAjax:TabPanel runat="server" ID="tabDependent" Enabled="true" HeaderText="Dependents"
                Font-Size="Small" Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updpnlArticles" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTab2" />
                        </Triggers>
                        <ContentTemplate>
                            <nobr>
                                <asp:Label runat="server" ID="lblTabLoadStatus" Visible="false" Text="Loading...">
                                    <div id="loadingImage" runat="server">
                                        <img visible="false" src="spinner.gif" width="33" height="33" />
                                    </div>
                                </asp:Label></nobr>
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="DDLDependentList" runat="server" Visible="false">
                            </asp:DropDownList></div>
                            <asp:Button CssClass="submitButton" ID="btnRequestDependentCert" Width="163px" runat="server" Visible="false" OnClick="btnRequestDependentCert_Click"
                                Text="View Details for Dependent" />
                            <!--- Rest of the controls here --->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </aspAjax:TabPanel>

       </aspAjax:TabContainer>
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
               <br /><br /> 
            <table >
           
                <tr>
                    <td>
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server" Text="3456998"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name :"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        </td>
                    <td>
                        </td>
                </tr>
          
            </table>
            
       <div id="CoverageListReport"  visible=false style=" font-family:Verdana; ; height:384px; width:100%; overflow:hidden;" runat="server" >
       <img src="file:///C:\Downloads\imagemap-1291\AJAX_ImageMap\animated_loading.gif" />
       </div>

            
            
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label>
    <br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
    <br />
    <uc2:OSNav ID="OSNav1" runat="server" />
    <br />
    <asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
       
           

        
           
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p>
        View claim history details of your staff for the past 6 months</p>
</asp:Content>
