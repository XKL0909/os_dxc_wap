﻿<%@ WebHandler Language="C#" Class="FileUploadHandler" %>

using System;
using System.Web;
using System.Linq;

public class FileUploadHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            context.Response.ContentType = "text/plain";

            var FileUploadService = new UnifiedPolicy.Services.AddMemberFileuploadService();
            var RequestFileUpload = new UnifiedPolicy.Services.Dto.Fileupload();
            var reqDetailId = (long)HttpContext.Current.Session["ReqDetailID"];


            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];

                string fileName = UnifiedUploadManager.GenrateName(System.IO.Path.GetFileName(file.FileName));
                string ext = System.IO.Path.GetExtension(file.FileName);

                var allowdExtension = System.Configuration.ConfigurationManager.AppSettings["UploadAllowedExtensions"].ToString().Split(',');

                if (!allowdExtension.Contains(ext.ToLower()))
                {
                    context.Response.Write("exterror");
                    return;
                }

                int fileSize = file.ContentLength;
                var maxSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxFileSize"].ToString());
                if (fileSize > maxSize)
                {

                    context.Response.Write("sizeerror");
                    return;
                }

                try
                {
                    //string fname = context.Server.MapPath("~/uploads/" + file.FileName);
                    //file.SaveAs(fname);

                    file.SaveAs(UnifiedUploadManager.GetPath() + fileName);

                    RequestFileUpload.UnifiedRequestDetailId = Convert.ToInt32(reqDetailId);
                    RequestFileUpload.Name = fileName;
                    RequestFileUpload.FullName = file.FileName;
                    RequestFileUpload.CreatedOn = DateTime.Now;
                    FileUploadService.Add(RequestFileUpload);
                    // BindFile(Convert.ToInt32(reqDetailId));
                }
                catch (Exception ex)
                {
                    context.Response.Write("error");
                }

                context.Response.ContentType = "text/plain";
                context.Response.Write("success");
            }





        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}