﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Data;
using System.Drawing;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Text;
using DevExpress.Web.ASPxGridView;
using System.Collections;
using System.Web.Configuration;
using System.Configuration;
using Bupa.Core.Logging;
using System.Threading;

public partial class Client_ExcelReader : System.Web.UI.Page
{
    DataSet ds = null;

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);

            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
        }
        catch
        {

        }
       

        if (!IsPostBack && !IsCallback)
        {           
            GetExcel(Server.MapPath("~/Client/ExcelTemplates/AddDependent.xls"), "Dependent");
        }
        else
        {
            ds = (DataSet)Session["DataSet"];
            gvData.DataSource = ds.Tables[0];
            gvData.DataBind();
        }
       
    }

    protected override void OnPreRender(EventArgs e)
    {
        //base.OnPreRender(e);
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
           string _contractID = Convert.ToString(Session["ClientID"]);
            if (string.IsNullOrEmpty(_contractID))
                Response.Redirect("~/Default.aspx");
        }
        catch (ThreadAbortException) { }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
        base.OnPreRender(e);


    }
    public void GetExcel(string fileName, string _type)
   
   {


       
       Application oXL;
       Workbook oWB;
       Worksheet oSheet;
       Range oRng;
       Range oRngHeader;
       try
       {    
           //  creat a Application object    
           oXL = new ApplicationClass();    
           //   get   WorkBook  object    
           oWB = oXL.Workbooks.Open(fileName, Missing.Value, Missing.Value,Missing.Value, Missing.Value, Missing.Value,Missing.Value, Missing.Value, Missing.Value,Missing.Value, Missing.Value, Missing.Value, Missing.Value,Missing.Value, Missing.Value);
           //   get   WorkSheet object    
           oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWB.Sheets[1];    
           System.Data.DataTable dt = new System.Data.DataTable("dtExcel");
           
           
           ds = new DataSet();    
           ds.Tables.Add(dt);    
           DataRow dr;    
           StringBuilder sb = new StringBuilder();    
           int jValue = oSheet.UsedRange.Cells.Columns.Count;    
           int iValue = oSheet.UsedRange.Cells.Rows.Count;
           bool _validFile = false;
           switch (_type)
           {
               case "Dependent":
                   if (jValue == 22)
                        _validFile = true;
                   break;
               case "Employee":
                   if (jValue == 21)
                       _validFile = true;
                   break;
           }
           if (_validFile)
           {
               
               //  get data columns    
               for (int j = 1; j <= jValue; j++)
               {
                   oRng = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, j];
                   string strValue = oRng.Text.ToString();
                   dt.Columns.Add(strValue, System.Type.GetType("System.String"));
               }
               ds.Tables["dtExcel"].Columns.Add("SEQ");
               if (gvData.Columns.Count <= 1)
               {
                   GridViewDataTextColumn column1 = new GridViewDataTextColumn();
                   column1.FieldName = "SEQ";
                   column1.VisibleIndex = 0;
                   gvData.Columns.Add(column1);
                   for (int _cell = 1; _cell <= jValue; _cell++)
                   {
                       oRngHeader = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, _cell];
                       string strHeaderTitle = oRngHeader.Text.ToString();
                       GridViewDataTextColumn column = new GridViewDataTextColumn();
                       column.FieldName = strHeaderTitle;
                       column.VisibleIndex = _cell;
                       gvData.Columns.Add(column);
                   }
               }
               //  get data in cell    
               for (int i = 2; i <= iValue; i++)
               {
                   dr = ds.Tables["dtExcel"].NewRow();
                   dr["SEQ"] = i - 1;
                   for (int j = 1; j <= jValue; j++)
                   {
                       oRngHeader = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, j];
                       oRng = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[i, j];
                       string strHeader = oRngHeader.Text.ToString();
                       string strValue = oRng.Text.ToString();
                       dr[strHeader] = strValue;
                      
       
                   }
                   ds.Tables["dtExcel"].Rows.Add(dr);
                   ds.Tables["dtExcel"].PrimaryKey = new DataColumn[] { dt.Columns["SEQ"] };
               }
               
               Session["DataSet"] = ds;
               
               gvData.DataSource = ds.Tables[0];
               gvData.DataBind();
           }
           else
           {
               ds = null;
           }
           
       }
       catch{}
       
       finally
       {    
           Dispose();
       }
   }
  

   protected bool CheckDataType(string _value, string _dateType)
   {
       bool _Valid = false;

       switch (_dateType)
       {
           case "Number":
               try
               {
                   _value = Convert.ToInt64(_value).ToString();
                   _Valid = true;
               }
               catch
               {
                   _Valid = false;
               }
               break;
           case "Date":
               try
               {
                   _value = Convert.ToDateTime(_value).ToShortDateString();
                   _Valid = true;
               }
               catch
               {
                   _Valid = false;
               }
               break;
       }
       return _Valid;
   }
   protected void gvData_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
   {

       try
       {
           switch (e.DataColumn.Index)
           {
               case 8:
               case 10:
               case 17:
                   if (!CheckDataType(e.CellValue.ToString(), "Date"))
                   {
                       e.Cell.BackColor = System.Drawing.Color.Red;
                       e.Cell.ForeColor = System.Drawing.Color.White;
                   }
                   break;
               case 3:
               case 7:
               case 11:
               case 13:
               case 14:
               case 15:
               case 18:
                   if (!CheckDataType(e.CellValue.ToString(), "Number"))
                   {
                       e.Cell.BackColor = System.Drawing.Color.Red;
                       e.Cell.ForeColor = System.Drawing.Color.White;
                   }
                   break;
           }
       }
       catch { }
      
   }
   protected void gvData_BeforePerformDataSelect(object sender, EventArgs e)
   {
      
      /* ds = (DataSet)Session["DataSet"];
       System.Data.DataTable detailTable = ds.Tables[0];
       DataView dv = new DataView(detailTable);
       dv.RowFilter = "SEQ = " + gvData.GetMasterRowKeyValue().ToString();
       gvData.DataSource = dv;*/

   }
   protected void gvData_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
   {
       ds = (DataSet)Session["DataSet"];
       ASPxGridView gridView = (ASPxGridView)sender;
       System.Data.DataTable dataTable = ds.Tables[0];
       DataRow row = dataTable.Rows.Find(e.Keys[0]);
       IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();
       enumerator.Reset();
       while (enumerator.MoveNext())
           row[enumerator.Key.ToString()] = enumerator.Value;
       gridView.CancelEdit();
       e.Cancel = true;

   }



   protected void gvData_HtmlEditFormCreated(object sender, ASPxGridViewEditFormEventArgs e)
   {
     
   }
   protected void gvData_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
   {
       try
       {
           switch (e.Column.Index)
           {
               case 8:
               case 10:
               case 17:
                   if (!CheckDataType(e.Value.ToString(), "Date"))
                   {

                       e.Column.EditCellStyle.BackColor = System.Drawing.Color.Red;
                       e.Column.EditCellStyle.ForeColor = System.Drawing.Color.White;
                   }
                   break;
               case 3:
               case 7:
               case 11:
               case 13:
               case 14:
               case 15:
               case 18:
                   if (!CheckDataType(e.Value.ToString(), "Number"))
                   {
                       e.Column.EditCellStyle.BackColor = System.Drawing.Color.Red;
                       e.Column.EditCellStyle.ForeColor = System.Drawing.Color.White;
                   }
                   break;
           }
       }
       catch { }
   }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

}