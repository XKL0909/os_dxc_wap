using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class Client_MyPreauthHistory : BasePage
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
    string strMemberName;
 ///  string TabFlag;     Need to create a session variable to hold this value

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }
        strMembershipNo = Session["MembershipNo"].ToString();
        //try
        //{
////////        TabFlag = "M";
        //strMembershipNo = Session["MembershipNo"].ToString();
        strMemberName = Session["MemberName"].ToString();
        DisplayPreAuthHist(strMembershipNo);
        //the below would be replace with vlaue from session var for the user
        //tabMemberName.HeaderText = response.memberName;


    }

    // function used for Tab System when user requset for dependent ceritifcate
    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
        DisplayPreAuthHist(DDLDependentList.SelectedValue);

        //TextBox1.Text = "updated also";
        //TextBox3.Visible = false;
        // Code to load data from database or other data source here
    }

    // function used for Tab System to retrieve family list
    private void DisplayDependentListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Arial;border-collapse:collapse;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            DDLDependentList.Items.Clear();

            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

                //DDLDependentList.Items.Add( New  ListItem("Item 1", "1");
                //http://authors.aspalliance.com/stevesmith/articles/dotnetlistbox2.asp


               // DDLDependentList.Items.Add(new ListItem(dtl.memberName + " -- " + dtl.relationship, dtl.membershipNo));

                //DDLDependentList.DataTextField =dtl.memberName + " -- " + dtl.relationship;
                //DDLDependentList.DataValueField = dtl.membershipNo.ToString();
                //DDLDependentList.Items.Insert(.DataBind();

                

                ListItem DepList = new ListItem();
                DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                DDLDependentList.Items.Add(DepList);

            }
        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }



// function used for Tab System when Tab1 [member tab] is clicked
    protected void btnTab1_Click(object sender, EventArgs args)
    {
  //////      if (TabFlag != "M")
        DisplayPreAuthHist(strMembershipNo);
        //.Text = "test completed";
    }

    // function used for Tab System when Tab2 [dependent tab] is clicked
    protected void btnTab2_Click(object sender, EventArgs args)
    {

        //System.Threading.Thread.Sleep(3000);
        //.Text = "test completed";
        
 ///////       if(TabFlag != "D")
        {

       // UpdatePanel2.Visible = true;



        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        request.membershipNo = strMembershipNo;
        //request..membershipNo = 
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

        //request.IqamaNo =  "5523234443";
        //request.TotalNoOfFamily = "3";

        try
        {
            StringBuilder sb = new StringBuilder(200);
            
            response = ws.EnqMbrListInfo(request);
            

            if (response.errorID != "0")
            {
                sb.Append(response.errorMessage).Append("\n");
                lblErrorMessage.Text = sb.ToString();
            }
            else
            {
                DisplayDependentListResult(response);

                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlTextWriter.Formatting = Formatting.Indented;
                xs.Serialize(xmlTextWriter, response);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                this.txtBox_output.Text = XmlizedString;
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text =ex.Message;
        }

        if (DDLDependentList.Items.Count > 0)
        {
  //////          TabFlag = "D";
            DDLDependentList.Visible = true;
            this.lblTabLoadStatus.Visible = false;
            btnRequestDependentCert.Visible = true;
        }
        else
            this.lblTabLoadStatus.Text = "No dependent for this Member Found";


     }
        
   
    }






    private void DisplayPreAuthHist(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.EnqPreHisInfoRequest_DN  request;
        OS_DXC_WAP.CaesarWS.EnqPreHisInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
           // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqPreHisInfoRequest_DN();
            request.membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); 

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.EnqPreHisInfo(request);

            //response = ws.EnqMbrListInfo(request);
           // Cursor.Current = Cursors.Default;


            if (response.detail != null)
            {

                lblMembershipNo.Text = request.membershipNo;
                lblMemberName.Text = strMemberName;
               // lblClassName.Text = response.className;
               // lblCustomerName.Text = response.companyName;

                DisplayPreauthHistoryResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                if (tabMemberName.HeaderText == "Member")
                    tabMemberName.HeaderText = strMemberName;


                
            }
            else
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

}







    private void DisplayPreauthHistoryResult(OS_DXC_WAP.CaesarWS.EnqPreHisInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;


        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {

            //dtl.proratedOverallAnnualLimit = ;


            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;border-collapse:collapse;font-size:10px; width:90%  ;'><thead><tr><th>Pre-auth No</th>   <th>Episode No</th><th>Provider Name</th>   <th>Received Date/Time</th><th>Response Date/Time</th>   <th>Pre-auth Status</th></tr>	</thead><tbody> ");

            foreach (OS_DXC_WAP.CaesarWS.PreHisDetail_DN dtl in response.detail)
            {

                var queryString = "PID=" + dtl.preAuthNo.Trim() + "&PC=" + dtl.providerCode.Trim() + "&EN=" + dtl.episodeID;
                queryString = Cryption.Encrypt(queryString);
                sbResponse.Append("<a href='PreauthDetails.aspx?val=" + queryString + "'  style='cursor:hand;'><tr ><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString() + "</td><td>" + dtl.preAuthStatus + "</td></tr></a>");


                ////sbResponse.Append("<a href='PreauthDetails.aspx?PID=" + dtl.preAuthNo + "&PC=" + dtl.providerCode + "&EN=" + dtl.episodeID + "'  style='cursor:hand;'><tr ><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString() + "</td><td>" + dtl.preAuthStatus + "</td></tr></a>");

                //                sbResponse.Append("<tr><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID  + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString()  + "</td><td>" + dtl.preAuthStatus  + "</td></tr>");
                //if (dtl.detail_ProratedLimit != -1)
                //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
                //else
                //    sbResponse.Append("<td>Not Covered</td></tr>");


            }
            sbResponse.Append("</table>");
            CoverageListReport.Visible = true;
            CoverageListReport.InnerHtml = sbResponse.ToString();


        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }







    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }




    
}