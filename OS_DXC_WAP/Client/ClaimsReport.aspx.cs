using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Collections;

public partial class ClaimsReport : System.Web.UI.Page
{
    private ReportDocument doc = null;
    private DataSet ds = null;
    String InvID = null;
    string strClientID;
    private Hashtable hasQueryValue;
    private string VoucherNumber, EpisodeNo;
    protected void Page_Init(object sender, EventArgs e)
    {
        strClientID = ""; 
    }

    public void BindReport()
    {

        if (!string.IsNullOrEmpty(VoucherNumber))
        {
            InvID = Request.QueryString["ID"];
            doc.Load(MapPath("../Reports/CLREIMSTM01_WS.rpt"));
            ds = GetData();
            doc.SetDataSource(ds.Tables[0]);
            doc.SetParameterValue("pc_report_id", "CLREIMSTM02_WS");
            doc.SetParameterValue("PC_RPT_GRP", "AL");
            doc.SetParameterValue("PC_VOU_NO", "4783683");
            CrystalReportViewer1.ReportSource = doc;
            Session["rptDocClaim"] = doc;
        }
      

       
       

    }





    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            if (Session["ClientID"] == null && string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                Response.Redirect("..\\Default.aspx", true);
                return;
            }
           
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Session["Reset"] = true;
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            CheckSessionExpired();
            
        }
        catch
        {

        }

        String Par1 = Request.QueryString["Pr"];
        try
        {

            hasQueryValue = new Hashtable();
            string queryStringValues = string.Empty;
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    var val = Request.QueryString["val"];
                    queryStringValues = Cryption.Decrypt(val);
                    hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                    ////Session["hashQueryPDetails"] = hasQueryValue;
                    VoucherNumber = hasQueryValue.ContainsKey("VID") ? Convert.ToString(hasQueryValue["VID"]) : string.Empty;
                    EpisodeNo = hasQueryValue.ContainsKey("EN") ? Convert.ToString(hasQueryValue["EN"]) : string.Empty;
                    EpisodeNo = hasQueryValue.ContainsKey("EN") ? Convert.ToString(hasQueryValue["EN"]) : string.Empty;
                    
                    doc = new ReportDocument();
                    if ((ViewState["ParametersShown"] != null) && (ViewState["ParametersShown"].ToString() == "True"))
                    {

                        BindReport();

                    }
                    else
                    {
                        BindReport();
                        ViewState["ParametersShown"] = "True";
                    }

                }
                catch (Exception ex)
                {
                    Response.Write("Invalid rquest!");
                    return;
                }

            }
            else
            {
                Response.Write("Invalid rquest!");
                return;
            }

           

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
       

    }

    private DataTable BuildTable()
    {
        DataTable dt;
        DataColumn col;

        dt = new DataTable();
        col = new DataColumn();
        col.ColumnName = "P_CONT_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 8;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "P_BRANCH_CODE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "P_MAIN_MBR_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CLAIM_ID";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "LINE_NO";
        col.DataType = Type.GetType("System.Int32");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR1";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR2";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR3";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR4";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR5";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR6";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR7";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR8";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR9";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR10";
        col.DataType = Type.GetType("System.String");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CO_NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CO_ADDR1";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CO_ADDR2";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CO_ADDR3";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CO_ADDR4";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CUST_SERV_PHONE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 20;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CUST_SERV_FAX";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 20;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CUST_SERV_FREE_LINE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 20;
        dt.Columns.Add(col);

        /*
        col = new DataColumn();
        col.ColumnName = "CUST_SERV_PHONE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 20;
        dt.Columns.Add(col);
        */

        col = new DataColumn();
        col.ColumnName = "CLM_NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CLM_MBRSHP_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CLM_STAFF_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "DEPT_CODE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "TRMT_DATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "RCPT_DATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "TOT_PRESENT_AMT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "TOT_DEDUCT_AMT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "TOT_ADJ_AMT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "SF_AMT";
        col.DataType = Type.GetType("System.Double");
        //col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "SERV_TYPE_DESC";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "REJ_CODE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "REJ_DESC";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 400;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CREJ_DESC";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 400;
        dt.Columns.Add(col);



        col = new DataColumn();
        col.ColumnName = "REJ_EXPLAIN";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 400;
        dt.Columns.Add(col);


        col = new DataColumn();
        col.ColumnName = "CREJ_EXPLAIN";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 400;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "NOTES";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 2000;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ANY_REJ_AMT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "STAFF_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        //
        return dt;
    }


    private DataSet GetData()
    {
        DataSet ds;
        DataTable dt;
        DataRow row;
        int i;
         
       OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
       OS_DXC_WAP.CaesarWS.ReqRemiStatementRequest_DN datum;
       OS_DXC_WAP.CaesarWS.ReqRemiStatementResponse_DN result;
       datum = new OS_DXC_WAP.CaesarWS.ReqRemiStatementRequest_DN();

        datum.vouNumber = VoucherNumber;

        datum.username = WebPublication.CaesarSvcUsername;
        datum.password = WebPublication.CaesarSvcPassword;
        datum.transactionID = TransactionManager.TransactionID();
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        result = ws.ReqRemiStatement(datum);
        dt = BuildTable();
        if (result.detail != null)
        {
            for (i = 0; i < result.detail.Length; i++)
            {
                row = dt.NewRow();

                row["P_CONT_NO"] = result.detail[i].contNo;
                row["P_BRANCH_CODE"] = result.detail[i].branchCode;
                row["P_MAIN_MBR_NO"] = result.detail[i].mainMbrNo;
                row["CLAIM_ID"] = result.detail[i].claimID;
                row["LINE_NO"] = result.detail[i].lineNo;
                row["ADDR1"] = result.detail[i].addr1;
                row["ADDR2"] = result.detail[i].addr2;
                row["ADDR3"] = result.detail[i].addr3;
                row["ADDR4"] = result.detail[i].addr4;
                row["ADDR5"] = result.detail[i].addr5;
                row["ADDR6"] = result.detail[i].addr6;
                row["ADDR7"] = result.detail[i].addr7;
                row["ADDR8"] = result.detail[i].addr8;
                row["ADDR9"] = result.detail[i].addr9;
                row["ADDR10"] = result.detail[i].addr10;
                row["CO_NAME"] = result.detail[i].coName;
                row["CO_ADDR1"] = result.detail[i].coAddr1;
                row["CO_ADDR2"] = result.detail[i].coAddr2;
                row["CO_ADDR3"] = result.detail[i].coAddr3;
                row["CO_ADDR4"] = result.detail[i].coAddr4;
                row["CUST_SERV_PHONE"] = result.detail[i].custServPhone;
                row["CUST_SERV_FAX"] = result.detail[i].custServFax;
                row["CUST_SERV_FREE_LINE"] = result.detail[i].custServFreeLine;
                row["CLM_NAME"] = result.detail[i].clmMbrName;
                row["CLM_MBRSHP_NO"] = result.detail[i].clmMbrNo;
                row["CLM_STAFF_NO"] = result.detail[i].clmStaffNo;
                row["DEPT_CODE"] = result.detail[i].deptCode;
                row["TRMT_DATE"] = DateTime.Parse(result.detail[i].trmtDate.ToString()).ToShortDateString();
                row["RCPT_DATE"] = DateTime.Parse(result.detail[i].rcptDate.ToString()).ToShortDateString();

                row["TOT_PRESENT_AMT"] = result.detail[i].totPresentAmt.ToString();
                row["TOT_DEDUCT_AMT"] = result.detail[i].totDeductAmt.ToString();
                row["TOT_ADJ_AMT"] = result.detail[i].totAdjAmt.ToString();
                row["SF_AMT"] = result.detail[i].sfAmt.ToString();
                row["SERV_TYPE_DESC"] = result.detail[i].servTypeDesc;
                row["REJ_CODE"] = result.detail[i].rejCode;
                row["REJ_DESC"] = result.detail[i].rejDesc;
                row["CREJ_DESC"] = result.detail[i].crejDesc;


                row["REJ_EXPLAIN"] = result.detail[i].rejExplain;
                row["CREJ_EXPLAIN"] = result.detail[i].crejExplain;

                row["ANY_REJ_AMT"] = result.detail[i].anyRejAmt.ToString();
                row["STAFF_NO"] = result.detail[i].staffNo;
                row["NOTES"] = result.detail[i].note;




                dt.Rows.Add(row);
            }
        }

        
        ds = new DataSet();
        ds.Tables.Add(dt);
        return ds;
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        //this.CrystalReportViewer1..ShowPreviousPage();
        doc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "Statement");//-" + DateTime.Today.ToShortDateString());

    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        this.CrystalReportViewer1.ShowNextPage();
    }
    protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
    {
        try
        {
            doc.Close();
        }
        catch { }
       
    }
    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
}
