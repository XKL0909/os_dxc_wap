﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Client_PlayVideo" Codebehind="PlayVideo.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Member/Dependent Guide</title>
</head>
<body>
    <form id="PlayVideo" runat="server">
    <div>
     <video width="800" height="500" controls="controls" preload="none" onclick="this.paused ? this.play() : this.pause()" autoplay="autoplay" >	
  <source src="https://onlineservices.bupa.com.sa/Client/Video/NewAddMemberGuide.mp4" type="video/mp4" />
         </video>
    </div>
    </form>
</body>
</html>

