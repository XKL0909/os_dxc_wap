﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using Bupa.OSWeb.Helper;


using log4net;
using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;
using System.Threading;
using Utility.Data;
using System.Collections.Specialized;
using System.Web.Configuration;

public partial class Client_CheckReqDocs : System.Web.UI.Page
{
    private UploadManager _uploadManager;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    private string[] _docs;
    public string _UploadCategory = UploadCategory.SUBMIT_REQUIRED_DOCS;
    public bool _UploadMandatory = true;
    public string _Username = "Unknown Client";
    public string _InitialRequestID = string.Empty;

    private ILog _log = LogManager.GetLogger(typeof(Client_CheckReqDocs));

    private readonly string _allowedExtensions = ".xls";

    private string _contractID = string.Empty;
    private string _batchOptionType;
    private string _dateErrors = string.Empty;
    private string _classErrors = string.Empty;
    private string _memberNoErrors = string.Empty;



    protected void Page_Init(object sender, EventArgs e)
    {
        Tracer.WriteMemberEntry(ref _log);
        try
        {
            _contractID = Convert.ToString(Session["ClientID"]);
            if (string.IsNullOrEmpty(_contractID))
                Response.Redirect("~/Default.aspx");
        }
        catch (ThreadAbortException) { }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
        }
        catch
        {

        }
        Tracer.WriteMemberEntry(ref _log);
        try
        {


            _dateErrors = string.Empty;
            _classErrors = string.Empty;
            _memberNoErrors = string.Empty;


            Tracer.WriteLine(ref _log, "_contractID is: " + _contractID);


        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
    }

    private bool EvaluateRequiredDocuments()
    {
        Tracer.WriteMemberEntry(ref _log);

        bool uploadedSupportingDocs = false;
        try
        {
            // First check if files have been uploaded
            int uploadedCount = 0;
            if (_UploadMandatory)
            {
                // Check that files have been uploaded
                UploadManager uploadManager = new UploadManager(Int32.MinValue, Int32.MinValue);
                DataSet uploadedFileSet = uploadManager.GetUploadedFileSet(_InitialRequestID, _UploadCategory);

                if (uploadedFileSet == null || uploadedFileSet.Tables.Count == 0 || uploadedFileSet.Tables[0].Rows.Count == 0)
                {
                    uploadedCount = 0;
                }
                else
                {
                    uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                    _docs = new string[uploadedFileSet.Tables[0].Rows.Count];
                    for (int i = 0; i < uploadedFileSet.Tables[0].Rows.Count; i++)
                    {
                        _docs[i] = Convert.ToString(uploadedFileSet.Tables[0].Rows[i][3]);
                    }
                }
            }

            /*Changed By Wessam 
            if (uploadedCount > 0)
            {
                uploadedSupportingDocs = false;
            }
            else
            {
                uploadedSupportingDocs = true;
            }*/

            if (uploadedCount > 0)
            {
                uploadedSupportingDocs = true;
            }
            else
            {
                uploadedSupportingDocs = false;
            }

        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
        return uploadedSupportingDocs;
    }


    protected void btnSearchDate_Click(object sender, EventArgs e)
    {
        GetResult();

    }
    protected void GetResult()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.MbrRejListRequest_DN wsRequest = new OS_DXC_WAP.CaesarWS.MbrRejListRequest_DN();
        OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN wsResponse = new OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN();


        DataTable dt = new DataTable();

        dt.Columns.Add("Seq");
        dt.Columns.Add("Ref No");
        dt.Columns.Add("Contract No");
        dt.Columns.Add("Member Name");
        dt.Columns.Add("Saudi ID/Iqama No.");




        wsRequest.transactionID = TransactionManager.TransactionID();
        wsRequest.Username = WebPublication.CaesarSvcUsername;
        wsRequest.Password = WebPublication.CaesarSvcPassword;
        wsRequest.refNo = txtSearchRef.Text;
        wsRequest.contNo = _contractID;
        if (!string.IsNullOrEmpty(txtSearchDate.Text))
        {
            wsRequest.subDate = Convert.ToDateTime(txtSearchDate.Text);
        }
        try
        {
            wsResponse = ws.MbrRejList(wsRequest);


            if (wsResponse.status != "0")
            {
                for (int i = 0; i < wsResponse.errorMessage.Length; i++)
                    Response.Write("Fail" + wsResponse.errorMessage[i]);
            }
            else
            {
                dvResult.Visible = true;
                foreach (OS_DXC_WAP.CaesarWS.MbrRejListDetail_DN dtl in wsResponse.detail)
                {
                    DataRow dr = dt.NewRow();
                    dr["Seq"] = dtl.SeqNo;
                    dr["Ref No"] = dtl.RefNo;
                    dr["Contract No"] = dtl.ContNo;
                    dr["Member Name"] = dtl.MbrName;
                    dr["Saudi ID/Iqama No."] = dtl.IDCardNo;

                    dt.Rows.Add(dr);
                }
                gvResult.DataSource = dt.DefaultView;
                gvResult.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString());
        }
    }
    protected void gvResult_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvResult.EditIndex = e.NewEditIndex;
        GetResult();

    }
    protected void gvResult_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvResult.EditIndex = -1;
        GetResult();
    }
    protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            e.Row.Cells[7].Visible = false;
            e.Row.Cells[8].Visible = false;
            e.Row.Cells[9].Visible = false;
            e.Row.Cells[10].Visible = false;
            if (GetUploadedSupportedFileSet(e.Row.Cells[1].Text, e.Row.Cells[0].Text))
            {
                System.Web.UI.WebControls.Image img = e.Row.FindControl("image") as System.Web.UI.WebControls.Image;
                img.ImageUrl = "~/images/correct-md.png";
            }

        }
        if (e.Row.RowType == DataControlRowType.Header)
        {


            e.Row.Cells[7].Visible = false;
            e.Row.Cells[8].Visible = false;
            e.Row.Cells[9].Visible = false;
            e.Row.Cells[10].Visible = false;


        }

    }

    protected string getImage()
    {

        return "~/Icons/missing-icon.png";

    }


    protected void gvResult_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int RowID = Convert.ToInt32(e.RowIndex);
        string seq = gvResult.Rows[e.RowIndex].Cells[0].Text;
        string refNum = gvResult.Rows[e.RowIndex].Cells[1].Text;
        string ContNo = gvResult.Rows[e.RowIndex].Cells[2].Text;
        FileUpload fileUpload = gvResult.Rows[e.RowIndex].FindControl("FileUpload1") as FileUpload;
        string _filename = "";
        Message1.Visible = false;
        if (fileUpload.HasFile)
        {
            //Added new code to check MimeType
            string fileExtention = System.IO.Path.GetExtension(fileUpload.PostedFile.FileName).ToLower();
            Boolean fileOK = false;
            Boolean mimeOK = false;
            CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
            string mime = findMimeFromDate.CheckFindMimeFromData(fileUpload.PostedFile);
            String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
            //String[] ExcelMime = findMimeFromDate.ReturnFileExtenstions("ExcelFile");
            //ExcelMime.CopyTo(allowedMime, allowedMime.Length - 1);
            String[] allowedExtensions = { ".pdf", ".jpg", ".jpeg", ".gif", ".png", ".bmp", "rtf","tiff"};
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtention == allowedExtensions[i])
                {
                    fileOK = true;
                    break;
                }
            }
            for (int i = 0; i < allowedMime.Length; i++)
            {
                if (mime == allowedMime[i])
                {
                    mimeOK = true;
                    break;
                }
            }
            if (fileOK && mimeOK)
            {
                int seed = DateTime.Now.Millisecond;
                Tracer.WriteLine(ref _log, "seed is: " + seed);

            string uniquePrefix = WebPublication.GenerateUniqueID;
            Tracer.WriteLine(ref _log, "uniquePrefix is: " + uniquePrefix);

            // Go ahead and save to the server for the moment
            _filename = uniquePrefix + "_" + fileUpload.FileName;


                fileUpload.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Uploads"), _filename));
                //Save File to Database
                AddSupportDocs(WebPublication.GenerateUniqueID, _filename, ContNo, refNum, seq);
                SaveFile();
            }
            else
            {
                Message1.Text = "Invalid File";
                Message1.Visible = true;
            }
        }

        gvResult.EditIndex = -1;
        GetResult();

    }


    public void AddSupportDocs(string sessionID, string friendlyFileName, string userID, string Refnum, string Seq)
    {
        Tracer.WriteMemberEntry(ref _log);

        UploadResult uploaded = new UploadResult();
        uploaded.ErrorReason = new StringCollection();

        try
        {
            uploaded.Result = false;


            DataParameter dpDocName = new DataParameter(System.Data.SqlDbType.NVarChar, "docName", friendlyFileName, System.Data.ParameterDirection.Input);
            DataParameter dpUserID = new DataParameter(System.Data.SqlDbType.VarChar, "userID", userID, System.Data.ParameterDirection.Input);
            DataParameter dprefNum = new DataParameter(System.Data.SqlDbType.VarChar, "refnum", Refnum, System.Data.ParameterDirection.Input);
            DataParameter dpSeq = new DataParameter(System.Data.SqlDbType.VarChar, "seq", Seq, System.Data.ParameterDirection.Input);

            DataParameter[] dpBasket = new DataParameter[] { dpDocName, dpUserID, dprefNum, dpSeq };
            DataManager.ExecuteStoredProcedureNonQuery(CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS), UploadPublication.spBupa_OSInsertUploadSupportDocs, ref dpBasket, 60000);



        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            throw;
        }
        Tracer.WriteMemberExit(ref _log);

    }

    public bool GetUploadedSupportedFileSet(string refnum, string seq)
    {
        bool _result;
        Tracer.WriteMemberEntry(ref _log);

        DataSet fileSet = new DataSet();
        try
        {
            // Get the list of files committed to the upload
            // Selection should be based on the sessionID and the upload category
            // Continue committing the file to the database
            DataParameter dpSessionID = new DataParameter(System.Data.SqlDbType.VarChar, "refnum", refnum, System.Data.ParameterDirection.Input);
            DataParameter dpUploadedSourceID = new DataParameter(System.Data.SqlDbType.VarChar, "seq", seq, System.Data.ParameterDirection.Input);

            DataParameter[] dpBasket = new DataParameter[] { dpSessionID, dpUploadedSourceID };
            fileSet = DataManager.ExecuteStoredProcedureCachedReturn(CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS), UploadPublication.spBupa_OSGetUploadSupportDocs, ref dpBasket, 6000);
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            throw;
        }

        Tracer.WriteMemberExit(ref _log);
        if (fileSet.Tables[0].Rows.Count > 0)
        {
            _result = true;
        }
        else
        {
            _result = false;
        }
        return _result;
    }
    protected void SaveFile()
    {

    }

    public DataSet GetUploadedSupportedFileSets(string refnum)
    {
        Tracer.WriteMemberEntry(ref _log);

        DataSet fileSet = new DataSet();
        try
        {

            DataParameter dprefNum = new DataParameter(System.Data.SqlDbType.VarChar, "refnum", refnum, System.Data.ParameterDirection.Input);


            DataParameter[] dpBasket = new DataParameter[] { dprefNum };
            fileSet = DataManager.ExecuteStoredProcedureCachedReturn(CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS), UploadPublication.spBupa_OSGetUploadSupportDocsSet, ref dpBasket, 6000);
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            throw;
        }

        Tracer.WriteMemberExit(ref _log);
        return fileSet;
    }

    protected void ddlSearchtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ddlSearchtype.SelectedValue)
        {
            case "1":
                refnum.Visible = true;
                submitdate.Visible = false;
                txtSearchDate.Text = "";
                RequiredFieldValidator1.Enabled = true;
                RequiredFieldValidator9.Enabled = false;
                btnSearchDate.Visible = true;
                break;
            case "2":
                refnum.Visible = false;
                txtSearchRef.Text = "";
                submitdate.Visible = true;
                RequiredFieldValidator1.Enabled = false;
                RequiredFieldValidator9.Enabled = true;
                btnSearchDate.Visible = true;
                break;
            case "3":
                refnum.Visible = true;
                submitdate.Visible = true;
                RequiredFieldValidator1.Enabled = true;
                RequiredFieldValidator9.Enabled = true;
                btnSearchDate.Visible = true;
                break;
            case "0":
                refnum.Visible = false;
                submitdate.Visible = false;
                txtSearchDate.Text = "";
                txtSearchDate.Text = "";
                RequiredFieldValidator1.Enabled = false;
                RequiredFieldValidator9.Enabled = false;
                btnSearchDate.Visible = false;
                break;
        }
    }
    protected void btnSubmitRequest_Click(object sender, EventArgs e)
    {
        DataSet _dsresult;
        string[] _ContNo, _Seq, _DocPath;


        _dsresult = GetUploadedSupportedFileSets(txtSearchRef.Text);
        DataTable dt = _dsresult.Tables[0];
        string refnu = "";
        _ContNo = new string[_dsresult.Tables[0].Rows.Count];
        _Seq = new string[_dsresult.Tables[0].Rows.Count];
        _DocPath = new string[_dsresult.Tables[0].Rows.Count];
        int _count = 0;


        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ReSubTxnRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReSubTxnResponse_DN response;

       


        request = new OS_DXC_WAP.CaesarWS.ReSubTxnRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();

        OS_DXC_WAP.CaesarWS.AllMbrDoc_DN[] allmbrdoc = new OS_DXC_WAP.CaesarWS.AllMbrDoc_DN[dt.Rows.Count];
        foreach (DataRow trow in dt.Rows)
        {
            
            allmbrdoc[_count] = new OS_DXC_WAP.CaesarWS.AllMbrDoc_DN();
            allmbrdoc[_count].sql_type = "CSR.SUB_TXN_DOC01";
            allmbrdoc[_count].CONT_NO = trow[2].ToString();
            allmbrdoc[_count].MBRSHP_NO = "";
            allmbrdoc[_count].SEQ_NO = Convert.ToInt32(trow[4].ToString());
            allmbrdoc[_count].DOC_PATH = trow[1].ToString();
            refnu = trow[3].ToString();
            _count++;
        }


        request.refNo = refnu;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.detail = allmbrdoc;
        try
        {
            response = ws.ReSubTxn(request);
            StringBuilder sb = new StringBuilder(200);
           
            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");

                Message1.Text = sb.ToString();

                if (response.refNo != null)
                    Message1.Text += "Kindly re-check your membership number.";
            }
            else
            {
                Message1.Text = "Thank you for submitting your request. Your reference number is " + Convert.ToString(txtSearchRef.Text) + ". We are now validating the submitted file.";
            }
        }
        catch (Exception ex)
        {

            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Error Encountered. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            Message1.Visible = true;
        }

    }
    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }
}