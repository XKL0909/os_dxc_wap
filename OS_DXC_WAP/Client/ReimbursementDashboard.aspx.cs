﻿using Bupa.Core.Utilities.Configuration;
using OnlinseServicesDashBoard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI.HtmlChart;

public partial class ReimbursementDashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        DateTimeBaseUnit baseUnit = (DateTimeBaseUnit)Enum.Parse(typeof(DateTimeBaseUnit), "Days", true);
        ReimbursementClaimsLineChart.PlotArea.XAxis.BaseUnit = baseUnit;

        ReimbursementClaimsLineChart.DataSource = GetReimbursementClaimsGroupedData(GetReimbursementClaimsData());
        ReimbursementClaimsLineChart.DataBind();

        ReimbursementClaimsLineChart.PlotArea.Series[0].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Black;
        ReimbursementClaimsLineChart.PlotArea.Series[1].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Red;
        ReimbursementClaimsLineChart.PlotArea.Series[2].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Gold;
        ReimbursementClaimsLineChart.PlotArea.Series[3].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Green;
    }

    private DataSet GetReimbursementClaimsGroupedData(DataSet dsSource)
    {

        //Dataset for chart
        DataSet ds = new DataSet("ReimbursementDataSet");
        DataTable dt = new DataTable("ReimbursementDataTable");
        dt.Columns.Add("billed_amt", Type.GetType("System.Decimal"));
        dt.Columns.Add("rjClaims", Type.GetType("System.String"));
        dt.Columns.Add("acClaims", Type.GetType("System.String"));
        dt.Columns.Add("spClaims", Type.GetType("System.String"));
        dt.Columns.Add("crt_date", Type.GetType("System.DateTime"));

        int TotalClaims = dsSource.Tables[0].Rows.Count;

        int rejectedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Reject")).Count();
        int acceptedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Accept")).Count();
        int suspendedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Suspense")).Count();

        var query = from dr in dsSource.Tables[0].AsEnumerable()
                    group dr by dr["crt_date"] into newdr
                    select new
                    {
                        crt_date = newdr.First().Field<object>("crt_date"),
                        billed_amt = newdr.Count(), //newdr.Sum(tempRec => Convert.ToDecimal(tempRec["billed_amt"])),
                        //Line_Item_Status = newdr.First().Field<object>("Line_Item_Status"),
                        rjClaims = newdr.Where(x => x.Field<string>("Line_Item_Status").Equals("Reject")).Count(),
                        acClaims = newdr.Where(x => x.Field<string>("Line_Item_Status").Equals("Accept")).Count(),
                        spClaims = newdr.Where(x => x.Field<string>("Line_Item_Status").Equals("Suspense")).Count(),
                    };

        foreach (var dtRow in /*dsSource.Tables[0].Rows*/ query)
        {
            dt.Rows.Add(dtRow.billed_amt.ToString(), dtRow.rjClaims, dtRow.acClaims, dtRow.spClaims, dtRow.crt_date);
        }

        ds.Tables.Add(dt);
        return ds;
    }

    private DataSet GetReimbursementClaimsData()
    {
        var caesarConnectionString = CoreConfiguration.Instance.GetConfigValue("Caesar Connection String");
        var contractNumber = Convert.ToString(Session["ClientID"]); //CoreConfiguration.Instance.GetConfigValue("FilterByContract");


        DataSet ds = DataHelper.GetReimbursementClaimsData(caesarConnectionString, DateTime.Now.AddDays(-10), DateTime.Now, contractNumber);

        ReimbursementClaimDetailsGrid.DataSource = ds;
        ReimbursementClaimDetailsGrid.DataBind();


        return ds;
    }

    public class ReimbursementClaim
    {
        public ReimbursementClaim(string name, double pieShare, bool isExploded, string partColor)
        {
            Name = name;
            MarketShare = pieShare;
            IsExploded = isExploded;
            PartColor = partColor;
        }
        public string Name { get; set; }
        public string PartColor { get; set; }
        public double MarketShare { get; set; }
        public bool IsExploded { get; set; }
    }



}