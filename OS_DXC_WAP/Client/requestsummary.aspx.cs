﻿using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;


public partial class requestsummary : System.Web.UI.Page
{
    private AddMemberRequestDetailService _memberService;
    private long reqID;

    public requestsummary() : this(new AddMemberRequestDetailService())
    {

    }

    public requestsummary(AddMemberRequestDetailService memberService)
    {
        _memberService = memberService;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Encrypt or remove query string
        //if (Request.QueryString["RequestId"] != null)
        //{
        //    reqID = long.Parse(Request.QueryString["RequestId"]);
        //    RptEmployee.DataSource = _memberService.Get(reqID);
        //    RptEmployee.DataBind();
        //    divCCHI.Visible = SetCCHIVisibility(reqID);
        //}

        reqID = long.Parse(Convert.ToString(Session["RequestId"]));
        RptEmployee.DataSource = _memberService.Get(reqID);
        RptEmployee.DataBind();
        divCCHI.Visible = SetCCHIVisibility(reqID);
    }

    [WebMethod(EnableSession = true)]
    public static long DeleteMember(AddMemberRequestDetail member)
    {
        AddMemberRequestDetailService memberService = new AddMemberRequestDetailService();
        long result = memberService.Remove(member.Id);
        return result;
    }

    [WebMethod(EnableSession = true)]
    public static int SetSessionforModification(AddMemberRequestDetail member)
    {
        HttpContext.Current.Session.Add("ReqDetailID", member.Id);
        return 1;
    }

    protected void RptEmployee_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int reqDetId = Convert.ToInt32(e.CommandArgument.ToString());
        switch (e.CommandName)
        {
            case "Update":
                break;
        }
    }

    protected bool SetCCHIVisibility(long reqID)
    {
        bool isVisible = false;
        AddMemberRequestDetailService addMemberRequestDetailService = new AddMemberRequestDetailService();
        var checkRequest = addMemberRequestDetailService.Get(Convert.ToInt64(reqID));
        if (checkRequest != null && checkRequest.Count() > 0)
        {
            var checkStatus = checkRequest.Any(x => x.Status != 3);
            isVisible = checkStatus == false ? true : false;
        }
        return isVisible;
    }
}