﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="step2" Codebehind="step2.aspx.cs" %>

<form id="form2" runat="server">
    <asp:hiddenfield runat="server" id="hdnSubReqType" />
    <asp:hiddenfield runat="server" id="hdnSecSponsor" />
    <asp:hiddenfield runat="server" id="hdnContractNumber" />
    <asp:hiddenfield runat="server" id="hdnMAinEMpCoverType" />
    <asp:hiddenfield runat="server" id="hdnGender" />
    <asp:hiddenfield runat="server" id="hdnDob" />
    <asp:hiddenfield runat="server" id="hdncaesarContractPolicyEndDate" />
    <asp:hiddenfield runat="server" id="hdncaesarContractPolicyStartDate" />
    <asp:hiddenfield runat="server" id="hdnpolicyStartDate" />
    <asp:hiddenfield runat="server" id="hdnNEwBornStep2" />
    <asp:hiddenfield runat="server" id="HiddendrpTitle" />
    <asp:hiddenfield runat="server" id="HiddendrpGender" />
    <asp:hiddenfield runat="server" id="HiddendrpMaritalStatus" />
    <asp:hiddenfield runat="server" id="HiddenComType" />
    <asp:hiddenfield runat="server" id="HiddentxtDOB" />
    <asp:hiddenfield runat="server" id="HiddendrpProfessionVAlue" />
    <asp:hiddenfield runat="server" id="HiddendrpProfession" />
    <asp:hiddenfield runat="server" id="HiddendrpNationality" />
    <asp:hiddenfield runat="server" id="HiddentxtExpDate" />
    <asp:hiddenfield runat="server" id="HiddenMaternity" />
    <asp:hiddenfield runat="server" id="HiddenEmployeeDepStatus" />
    <asp:hiddenfield runat="server" id="HiddenNameCount" />
    <asp:hiddenfield runat="server" id="HiddenGenderCount" />
    <asp:hiddenfield runat="server" id="HiddenDobCount" />
    <asp:hiddenfield runat="server" id="HiddenMemTypeCount" />
     <asp:hiddenfield runat="server" id="MemberShipQ" />
    <asp:hiddenfield runat="server" id="hdnRequestType" />
     <asp:hiddenfield runat="server" id="hdnIsRM" />
     

     <asp:hiddenfield runat="server" id="hdnSME" />
     <asp:hiddenfield runat="server" id="hdnDefaultStartDate" />
     <asp:hiddenfield runat="server" id="hdnDefaultEndDate" />
     <asp:hiddenfield runat="server" id="hdnMainMemberStartDate" />
     <asp:hiddenfield runat="server" id="hdnContractStartDate" />
     <asp:hiddenfield runat="server" id="hdnContractEndDate" />
    

    <div class="card bb">
        <div class="header bb">
            <h2>Verify </h2>
        </div>
        <div class="body bb">
            <div runat="server" id="divAlertBox" visible="false" class="alert alert-info alert-dismissable bb">
                <asp:label id="lblMessge" runat="server" text=""></asp:label>
            </div>
            <fieldset runat="server" id="SectionFieldSet">
                <%-- <legend>Details Information</legend>--%>
                <br />
                <div class="row bb">
                    <div class="col-md-12 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <h5>Personal Information</h5>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row bb">
                    <div class="col-md-4 bb" style="display: none">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="Title">Title / اللقب</label>
                                <asp:dropdownlist cssclass="form-control bb" id="drpTitle" runat="server">
                                    <asp:ListItem Text="Select Title" Value=""></asp:ListItem>
                                    <asp:ListItem Value="Mr">Mr</asp:ListItem>
                                    <asp:ListItem Value="Miss">Miss</asp:ListItem>
                                    <asp:ListItem Value="Mrs">Mrs</asp:ListItem>
                                   

                                </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValdrpTitle" controltovalidate="drpTitle" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="Name">Name / الاسم</label>
                                <asp:textbox cssclass="form-control bb" id="txtName" runat="server"></asp:textbox>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValName" controltovalidate="txtName" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                                <asp:regularexpressionvalidator
                                    id="reqRegExpValtxtName" runat="server" controltovalidate="txtName" forecolor="Red"
                                    display="Dynamic" errormessage="Incorrect name format, please use only alphabets./يرجى ادخال احرف بالحقل فقط"
                                    font-bold="False" validationexpression="[a-zA-Z ]{2,40}$" validationgroup="step2" setfocusonerror="true"></asp:regularexpressionvalidator>
                                <div id="spanName" style="color: red"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="MaritalStatus">Marital Status / الحالة الاجتماعية</label>
                                <asp:dropdownlist cssclass="form-control bb" id="drpMaritalStatus" runat="server">
                                   <%--<asp:ListItem Text="Select Marital Status" Value=""></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>--%>
                                </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValdrpMaritalStatus" controltovalidate="drpMaritalStatus" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row bb">
                    <div class="col-md-4 bb" id="DivGender" runat="server">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="Gender">Gender / الجنس</label>
                                <asp:dropdownlist cssclass="form-control bb" id="drpGender" runat="server">
                                    <asp:ListItem Text="Select Gender" Value=""></asp:ListItem>
                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValdrpGender" controltovalidate="drpGender" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb" id="DivDOB" runat="server">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="DOB">Date Of Birth (Gregorian) / تاريخ الولادة (ميلادي) </label>
                                <asp:textbox runat="server" cssclass="form-control bb" id="txtDOB" autocomplete="off"></asp:textbox>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValtxtDOB" controltovalidate="txtDOB" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                               
                                <br />
                                <asp:regularexpressionvalidator id="Regularexpressionvalidator1" runat="server"
                                    controltovalidate="txtDOB" display="Dynamic" errormessage="Baby Age Should Not Be More Than One Year"
                                    validationexpression="[0][5][0-9]{8}" validationgroup="step2" setfocusonerror="true"
                                    width="116px">Enter a Valid Date /يرجى ادخال التاريخ الصحيح</asp:regularexpressionvalidator>
                                <span style="color: red" id="spanDOB"></span>
                                <asp:regularexpressionvalidator id="REVBabayAge" runat="server"
                                    controltovalidate="txtDOB" display="Dynamic" errormessage="Baby Age Should Not Be More Than One Year"
                                    validationexpression="[0][5][0-9]{8}" validationgroup="step2" setfocusonerror="true"
                                    width="116px">Baby Age Should Be Less Than One Year/عمر الطفل يجب ان يكون اقل من سنة</asp:regularexpressionvalidator>
                                <span style="color: red" id="spanDOB"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb" id="DivPRofession" runat="server">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="">Profession / المهنة</label>
                                <asp:dropdownlist cssclass="form-control" id="drpProfession" runat="server">
                        </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reValdrpProfession" controltovalidate="drpProfession" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row bb">

                    <div class="col-md-4 bb" id="DivNAtionalty" runat="server">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="Nationality">Nationality / الجنسية</label>
                                <asp:dropdownlist cssclass="form-control bb" id="drpNationality" runat="server">
                                            </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValdrpNationality" controltovalidate="drpNationality" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb" id="DivIDExpaiary" runat="server">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="ID Expiry">ID Expiry / تاريخ انتهاء الهوية</label>
                                <asp:textbox runat="server" cssclass="form-control bb" id="txtExpDate"></asp:textbox>
                                <asp:requiredfieldvalidator validationgroup="step2" id="RequiredFieldValidator1" controltovalidate="txtExpDate" runat="server" errormessage="Field is mandatory/الحقل إجباري"></asp:requiredfieldvalidator>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row bb">
                    <div class="col-md-12 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <h5>Insurance Information</h5>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row bb">

                    <div class="col-md-4 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="Cover bb">Level Of Cover / درجة التغطية المطلوبة </label>
                                &nbsp;	&nbsp;<img src="Images/2.png" data-toggle="tooltip" data-placement="bottom" data-html="true" title="please choose the member type<br>نرجو اختيار نوع العضوية" />
                                <asp:dropdownlist id="drpCover" cssclass="form-control bb" runat="server">
                                </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValdrplistCoverr" controltovalidate="drpCover" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="Member">Member Type / نوع العضوية </label>
                                <asp:dropdownlist id="drpMemberType" cssclass="form-control bb" runat="server">
                                </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValdrpMemberType" controltovalidate="drpMemberType" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="">Branch / الفرع</label>
                                &nbsp;	&nbsp;<img src="Images/2.png" data-toggle="tooltip" data-placement="bottom" data-html="true" title="please choose the branch Which is followed by the employee<br>إختيار الفرع التابع له الموظف" />
                                <asp:dropdownlist cssclass="form-control bb" id="drpBranchCode" runat="server">
                                </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reValdrpBranchCode" controltovalidate="drpBranchCode" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row bb">
                    <div class="col-md-4 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="">District / المنطقة</label>&nbsp;	&nbsp;<img src="Images/2.png" data-toggle="tooltip" data-placement="bottom" data-html="true" title="please Choose the employee City<br>نرجو منكم اختيار مدينة الموظف" />
                                <asp:dropdownlist cssclass="form-control bb" id="drpDistrict" runat="server">
                        </asp:dropdownlist>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValdrpDistrict" controltovalidate="drpDistrict" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="">Mobile / الجوال</label>

                                <span style="font-style: italic; font-size: .8em; color: red; text-align: left">(05xxxxxxxx)</span>
                                <asp:textbox cssclass="form-control bb" id="txtMobileNumber" maxlength="10" runat="server"></asp:textbox>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValtxtMobileNumber" controltovalidate="txtMobileNumber" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                                <asp:regularexpressionvalidator id="RegularExpressionValidator14" runat="server"
                                    controltovalidate="txtMobileNumber" display="Dynamic" errormessage="RegularExpressionValidator"
                                    validationexpression="[0][5][0-9]{8}" validationgroup="step2" setfocusonerror="true"
                                    width="116px">Incorrect Mobile No./رقم الجوال غير صحيح</asp:regularexpressionvalidator>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="">Effective date / تاريخ بداية التغطية</label>
                                &nbsp;	&nbsp;<img src="Images/2.png" data-toggle="tooltip" data-placement="bottom" data-html="true" title="please choose the start medical insurance coverage date/the date Should Be 30 Days Backdated or 10 Days in Advance<br>نرجو اختيار تاريخ بداية تغطية التأمين الطبي بحيث لا يتجاوز 30 يوم للوراء و 10 ايام للأمام" />
                                <span style="font-style: italic; font-size: .8em; color: red; text-align: left">(dd-MM-yyyy)</span>
                                <asp:textbox runat="server" cssclass="form-control bb" id="txtStartOfPolicy" maxLength="10" autocomplete="off" />
                               <asp:requiredfieldvalidator validationgroup="step2" id="reqValtxtStartOfPolicy" controltovalidate="txtStartOfPolicy" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="false"></asp:requiredfieldvalidator>
                              <asp:regularexpressionvalidator id="regValtxtStartOfPolicyREange" runat="server"
                                    controltovalidate="txtStartOfPolicy" display="Dynamic" errormessage=""
                                    validationexpression="[0][5][0-9]{8}" validationgroup="step2" cssclass="valerror" setfocusonerror="false" ></asp:regularexpressionvalidator>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row bb">
                    <div class="col-md-4 bb" id="SectionEmployeeNo" runat="server">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="">Employee Staff number / الرقم الوظيفي للموظف</label>
                                <asp:textbox cssclass="form-control bb" id="txtEmpNumber" runat="server" MaxLength="15"></asp:textbox>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValtxtEmpNumber" controltovalidate="txtEmpNumber" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bb" id="SectionDept" runat="server">
                        <div class="form-group bb">
                            <div class="form-line bb">
                                <label for="">Employee Department / قسم الموظف </label>
                                <asp:textbox cssclass="form-control bb" id="txtDepart" runat="server" MaxLength="50"></asp:textbox>
                                <asp:requiredfieldvalidator validationgroup="step2" id="reqValtxtDepart" controltovalidate="txtDepart" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                                <asp:regularexpressionvalidator
                                    id="reqRegExValtxtDepart" runat="server" controltovalidate="txtDepart" forecolor="Red"
                                    display="Dynamic" errormessage="Incorrect department format, please use only alpha numeric./ارقام وحروف فقط"
                                    font-bold="False" validationexpression="[a-zA-Z0-9 ]+" validationgroup="step2" setfocusonerror="true"></asp:regularexpressionvalidator>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix bb" runat="server" id="divMemberGrid">
                    <%-- <asp:button id="btnSave"  runat="server" text="Save" class="btn btn-primary nextBtn btn-sm" />--%>
                    <asp:repeater id="RptMembers" runat="server">
                        <HeaderTemplate>
                <table id="tbleRptMembers" class="table table-striped table-bordered dt-responsive nowrap margin-top-20 bb">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>#</th>
                            <th>Name</th>
                            <th>Maternity</th>
                            <th>Gender</th>
                            <th>Date Of Birth<span style="font-style: italic; font-size: .8em; color: rgba(172, 41, 37, 1); text-align: left">  (dd-MM-yyyy)</span></th>
                            <th>MemberType</th>
                            <th>ID Number</th>
                            <th>Sponsor</th>
                        </tr>
                    </thead>
                         <tbody>
                        </HeaderTemplate>
                       <ItemTemplate>
                        <tr>
                            <td>
                                <input type="checkbox" checked="checked" <%# Container.ItemIndex == 0 ?"disabled"  :"" %> value='<%#Eval("IdNumber")%>' />
                            </td>
                            <td><%# Container.ItemIndex + 1 %></td>
                            <%--<td><%#Eval("Name")%></td>--%>
                            <td>
                               <table id="Table1" runat="server" style="width:100%">
                                    <tr style='<%# Eval("NameVisibility")%>'>
                                        <td>
                                            <input type="text" class="TextName" id="txtmemName" runat="server" style='<%# Eval("NameVisibility")%>'/>
                                        </td>
                                    </tr>
                                   <tr style='<%# Eval("AltNameVisibility")%>'>
                                       <td><%#Eval("Name")%></td>

                                   </tr>
                                </table>
                            </td>
                            <td style="width:20px">
                                <table id="ListTableItems" runat="server" style="width:100%">
                                    <tr style='<%#Eval("Visibility")%>'>
                                        <td>
                                            <select class="ListItemdata" id="DrpListOptMaternity" runat="server" name="DrpListMaternity">
                                                <option value="0">Select</option>
                                                <option value="W01">Covered</option>
                                                <option value="W02">Not Covered</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <%--<td><%#Eval("Gender")%></td>--%>
                            <td>
                                <table id="Table2" runat="server" style="width:100%">
                                    <tr style='<%# Eval("GenderVisibility")%>'>
                                        <td>
                                            <select class="ListGender" id="TxtInputGender" runat="server" name="DrpListMaternity" style='<%# Eval("GenderVisibility")%>'>
                                                <option value="0">Select Gender</option>
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                            <%--<input  id="TxtInputGender" type="text" class="ListGender" maxlength="1" runat="server" style='<%# Eval("GenderVisibility")%>'>--%>
                                        </td>
                                    </tr>
                                   <tr style='<%# Eval("AltGenderVisibility")%>'>
                                       <td><%#Eval("Gender")%></td>

                                   </tr>
                                </table>
                            </td>
                            <td>
                                <table id="Table3" runat="server" style="width:100%">
                                    <tr style='<%# Eval("DOBVisibility")%>'>
                                        <td>
                                            <input  id="TxtInputDOB" type="text" class="TxtDateOfBirth" maxlength="10" runat="server" style='<%# Eval("DOBVisibility")%>' />
                                        </td>
                                    </tr>
                                   <tr style='<%# Eval("AltDOBVisibility")%>'>
                                       <td><%#Eval("DOB")%></td>
                                   </tr>
                                </table>
                            </td>
                                
                                
                            <td>
                                <table id="Table4" runat="server" style="width:100%">
                                    <tr style='<%# Eval("MemberTypeVisibility")%>'>
                                        <td>
                                            <input  id="TxtMemberType" type="text" class="TextMemType" maxlength="2" runat="server" style='<%# Eval("MemberTypeVisibility")%>' />
                                        </td>
                                    </tr>
                                   <tr style='<%# Eval("AltMemberTypeVisibility")%>'>
                                       <td class ="tdMemType"><%#Eval("MemberType")%></td>
                                   </tr>
                                </table>
                            </td>
                            <td><%#Eval("IdNumber")%></td>
                            <td><%#Eval("Sponsor")%></td>

                        </tr>
                            </ItemTemplate>
                        <FooterTemplate>
                                </tbody>
                              <tfoot>
                                <tr>
                                    <td colspan="9">
                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0?true:false %>' Text="No item(s) found" />
                                    </td>
                                </tr>

                            </tfoot>
                 </table>
                        </FooterTemplate>
                    </asp:repeater>
                </div>

            </fieldset>
            <div runat="server" id="SectionButton" class="actions clearfix text-right bb">
                <asp:button id="btnPrevious2" visible="false" runat="server" text="Previous/ السابق" class="btn btn-primary prevBtn btn-sm pull-left bb" />
                <asp:button id="btnNext2" validationgroup="step2" runat="server" text="Next / التالي" class="btn btn-primary nextBtn btn-sm pull-right bb" data-toggle="tooltip" data-placement="bottom" data-html="true" title="please click on next to move to next  page <br>يرجى الضغط على next للإنتقال للصفحة التالية" />
            </div>

        </div>
    </div>

</form>
<script src="js/step2.js"></script>
<script type="text/javascript">
    jQuery.browser = {};
    (function () {
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();
    $(function () {
        $("#<%= txtStartOfPolicy.ClientID  %>").datepicker({ dateFormat: 'dd-mm-yy' }).val();
        $("#<%= txtDOB.ClientID  %>").datepicker({ dateFormat: 'dd-mm-yy' }).val();
        $("#<%= txtExpDate.ClientID  %>").datepicker({ dateFormat: 'dd-mm-yy' }).val();
        $('#tbleRptMembers input[type="text"]').each(function () {
            var clientId = $(this)[0].id;
            if ($(this)[0].className == "TxtDateOfBirth") {
                var clientId = $(this)[0].id;
                $("#" + clientId + "").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            }
            $("#" + clientId + "")[0].style.borderColor = "";
        });
    });
    $("#<%= txtDOB.ClientID  %>").change(function () {
        var split = $("#<%= txtDOB.ClientID  %>").val();
        if (split.indexOf("-") !== -1) {
            var splited = split.split("-");
            var monthofb = parseInt(splited[1]) - 1;
            var dayofb = parseInt(splited[0]);
            var yearofb = parseInt(splited[2]);
            
            if (yearofb > 1900) {
                dateCheckGerf(false);
            }
            else {
                dateCheckGerf(true);
            }
            dateCheckForNewBaby(dayofb, monthofb, yearofb);
        }
        else {
            alert("wrong date fromat please pick the date");
        }

    });
    $("#<%= txtStartOfPolicy.ClientID  %>").change(function () {     
        var splitStartOfPolicy = $("#<%= txtStartOfPolicy.ClientID  %>").val();
        if (splitStartOfPolicy.indexOf("-") !== -1) {
		  EffectiveDAteValidation();
		}
		else {
		    alert("wrong date, pick a date");
		}
    });


  
</script>
