﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="requestsummary" Codebehind="requestsummary.aspx.cs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>

<script>

    function nextStep(type, reqId, reqDetId, nextStep) {
        debugger
        $(".stepwizard").show();
        $("#btnGroupType").val(type.trim());
        $("#hdnReqDetailId").val(reqDetId);
        $("#hdnRequestId").val(reqId);
        initCommonSession("E", nextStep, reqDetId);
    }

    function initCommonSession(value, nextStep, reqDetId) {
        var page = "step1" + value + ".aspx/InitSession"
        $.ajax({
            type: "POST",
            url: page,
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d;
                triggerSummaryStep(nextStep, reqDetId, "next");
            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);

            }
        });
    }
    function triggerSummaryStep(id, reqDetId, type) {
        id = (type == "next") ? id : prev;
        var nextId = "step" + id + ".aspx?reqDetId=" + reqDetId;
        var containerId = "#divStep" + id;
        var triggerId = "#step" + id;
        $.ajax({
            type: "POST",
            url: nextId,
            success: function (data) {
                $(containerId).html(data);
                loadMembers();
                $.LoadingOverlay("hide");

            },
            failure: function () {
                alert("Failed!");
            }
        });
        $(triggerId).trigger('click');

    }


    function modify(id, status, type) {

        $("#divMode").html("Edit");
        $("#step1LoadingImage").show();

        var member = {};

        member.Id = id;
        member.Status = status;

        $.ajax({
            type: "POST",
            url: "requestsummary.aspx/SetSessionforModification",
            data: "{member:" + JSON.stringify(member) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: OnSuccess,
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });

        function OnSuccess(response) {

            var result = response.d;
            if (result > 0) {
                setFunctionType(type.trim());
            }
        }
    }

    function deleteRequest(id, requestType, secSpoID) {
        if (requestType == "Employee") {
            if (confirm("If You Delete The Employee All His Dependent Will Be Deleted , Continue ?") == false) {
                return;
            }
        }
        if (requestType == "Family" && !(secSpoID)) {
            if (confirm("If You Delete The Employee All His Dependent Will Be Deleted , Continue ?") == false) {
                return;
            }
        }
        $("#divMode").html("Delete");
        $.LoadingOverlay("show");
        var member = {};
        member.Id = id;
        if (requestType == "Employee") {
            member.RequestType = "E";
        }
        if (requestType == "Family") {
            member.RequestType = "ED";
        }

        $.ajax({
            type: "POST",
            url: "requestsummary.aspx/DeleteMember",
            data: "{member:" + JSON.stringify(member) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var result = response.d;
                if (result > 0) {
                    loadMembers();
                    //$("#step1LoadingImage").hide();
                    $.LoadingOverlay("hide");
                }
            },
            failure: function (response) {
                alert(response.d);
                $.LoadingOverlay("hide");
            },
            error: function (response) {
                alert(response.d);
                $.LoadingOverlay("hide");
            }
        });
    }

    function loadMembers() {
        var requestId = $("#hdnRequestId").val();
        $.ajax({
            type: "POST",
            url: "requestsummary.aspx?RequestId=" + requestId,
            success: function (data) {
                $("#divRequests").html(data);
            },
            failure: function () {
                alert("Failed!");
            }
        });
    }

    function setFunctionType(value) {
        $("#btnGroupType").val(value);
        var page = "step1" + value + ".aspx"
        $.ajax({
            type: "POST",
            url: page,
            success: function (data) {
                $("#step-1").trigger('click');
                $(divStep1).html(data);

            },
            failure: function () {
                alert("Failed!");
            }
        });

    }

    function openSolution() {
        alert('here');
        $('#solTitle a').click(function (evt) {
            evt.preventDefault();
            alert('here in');
            var divId = 'summary' + $(this).attr('id');
            document.getElementById(divId).className = '';
        });
    }

    $(document).ready(function () {
        $("#divCCHI a").click(function () {
            //Remove query string
            //var RequestId = getParameterByName('RequestId');
            //var url2 = 'CCHIValidatorNew.aspx?RequestId=' + RequestId;
            //
            var url2 = 'CCHIValidatorNew.aspx';
            window.location.href = url2;
            return false;
        });
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
</script>

<form id="form1">
    <div class="row bb">

        <div class="card bb ">
            <div class="header bb">
                <h2>Member Details </h2>
            </div>

            <div class="body bb">

                <div class="margin-33 bb">
                    <label class="control-label bb">Search / بحث</label>
                    <asp:repeater id="RptEmployee" runat="server" onitemcommand="RptEmployee_ItemCommand">
                            <HeaderTemplate>
                                <table style="margin-bottom: 0px" class="table table-bordered table-hover table-striped dataTable bb" id="searchable" role="grid" aria-describedby="searchable_info">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Sponsor</th>
                                            <th>ID</th>
                                            <th>Main Employee</th>
                                            <th>Name</th>
                                            <th>Current Step</th>
                                            <th>Next Step</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %></td>

                                    <td><%#Eval("Sponsor")%></td>
                                    <td><%#Eval("IdNumber")%></td>
                                    <td><%#Eval("SecSponsorId")%></td>
                                    <td><%#Eval("Name")%></td>

                                    <td><%# (string)Eval("StatusDescription") =="Attachment" ? "Completed" : Eval("StatusDescription") %></td>

                                        <%-- Encrypt or remove query string
                                        <td class="text-center">
                                        <button class="btn btn-danger btn-sm btn-circle"
                                            style="<%# (Eval("Status").ToString()=="3" ||(Eval("RequestType").ToString()=="ED" && Eval("IsMainMemeber").ToString()=="False") )?"display:none": "display:inline" %>"
                                            onclick="nextStep('<%#Eval("RequestType") %>',<%=Request.QueryString["RequestId"] %>,<%#Eval("Id")%>,<%# (Convert.ToInt32(Eval("Status")) + 1)%>)" type="button">
                                            <%# (Convert.ToInt32(Eval("Status")) + 1)%></button></td>--%>

                                    <td class="text-center">
                                        <button class="btn btn-danger btn-sm btn-circle"
                                            style="<%# (Eval("Status").ToString()=="3" ||(Eval("RequestType").ToString()=="ED" && Eval("IsMainMemeber").ToString()=="False") )?"display:none": "display:inline" %>"
                                            onclick="nextStep('<%#Eval("RequestType") %>',<%=Session["RequestId"] %>,<%#Eval("Id")%>,<%# (Convert.ToInt32(Eval("Status")) + 1)%>)" type="button">
                                            <%# (Convert.ToInt32(Eval("Status")) + 1)%></button></td>

                                    <td>
                                        <button onclick="deleteRequest('<%#Eval("Id") %>','<%#Eval("RequestTypeDescription")%>','<%#Eval("SecSponsorId")%>')" class="btn btn-default btn-sm" type="button">Delete / حذف</button>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                        </table>
                            </FooterTemplate>
                        </asp:repeater>
                </div>
                <div class="row bb">
                    <div id="divCCHI" runat="server" class="actions clearfix pull-right margin bb">
                        <a href="#" id="CCHIValidate" class="btn btn-primary btn-sm bb" type="button" data-toggle="tooltip" data-placement="bottom" data-html="true" title="Click on CCHI Validation if you want proceed the request<br>يتم النقر على التأكد من صحة الطلب عند رغبتكم بإكمال معالجة الطلب ">CCHI Validation
                                <br>
                            CCHI التأكد من صحة الـ </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script src="js/datatable/jquery.datatables.js"></script>
<script src="js/datatable/ZeroClipboard.js"></script>
<script src="js/datatable/datatables.tabletools.js"></script>
<script src="js/datatable/datatables.bootstrap.js"></script>
<script src="js/datatable/datatables.init.js"></script>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        InitiateSearchableDataTable.init();
        //InitiateSearchableGroupableDataTable.init();
    });
</script>
