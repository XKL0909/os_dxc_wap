﻿using System;
using System.Configuration;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;

public partial class Request : System.Web.UI.Page
{
    private UnifiedRequest _request = new UnifiedRequest();
    private UnifiedRequestService _requestService;
    private string gsName = "";
    private string contractNumber = "";
    private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    private string AramcoContractType = Convert.ToString(ConfigurationManager.AppSettings["AramcoContractType"]).ToUpper();
    public Request()
    {
        log4net.Config.XmlConfigurator.Configure();
        _requestService = new UnifiedRequestService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        gsName = Session["ClientName"].ToString();
        contractNumber = Convert.ToString(Session["ClientID"]);
        if (string.IsNullOrEmpty(contractNumber))
            Response.Redirect("~/Default.aspx");
        if (!IsPostBack)
            BindRequests();
        int requestnumber = BindRequestsAramco();
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            if (Request.QueryString["sucess"] != "true")
            {
                if (requestnumber != 0)
                {
                    Response.Redirect("RequestDetail.aspx?requestid=" + requestnumber);
                }
                else
                {
                    _requestService = new UnifiedRequestService();
                    _request = new UnifiedRequest();
                    _request.ContractNo = contractNumber.Trim();
                    _request.Created = DateTime.Now;
                    _request.Modified = DateTime.Now;
                    _request.ModifiedBy = gsName;
                    _request.CreatedBy = gsName;
                    _request.IsEnable = true;
                    _request.IsPosted = false;
                    _request.OwnerId = gsName;
                    _requestService.Add(_request);
                    requestnumber = BindRequestsAramco();
                    Response.Redirect("RequestDetail.aspx?requestid=" + requestnumber);
                }
            }
            else
            {
                lnkCCHICap.Style.Add("display", "none");
                btnEmpDependent.Visible = false;
                btnCreateRequest.Visible = false;
                btnIndividualDependent.Visible = false;
            }
        }
        else
        {
            btnGuideAramco.Visible = false;
            btnEmpDependentbatch.Visible = false;
        }
    }

    private void BindRequests()
    {
        var details = _requestService.GetAllByContract(contractNumber.Trim());
        rptRequest.DataSource = details;
        rptRequest.DataBind();
    }

    private int BindRequestsAramco()
    {
        int RequestID = 0;
        var details = _requestService.GetAllByContract(contractNumber.Trim());
        foreach (UnifiedRequest request in details)
        {
            if (request.IsPosted == false)
            {
                RequestID = request.Id;
                break;
            }
        }
        return RequestID;
    }

    protected void BtnDelete(object sender, EventArgs e)
    {
        var  id =int.Parse( ((LinkButton)sender).CommandArgument);
        try
        {
            var result = _requestService.Disable(id);
            if (result)
                lblMessege.Text = "Records deleted sucessfully";
            else
                lblMessege.Text = "Records deleted sucessfully";
        }
        catch (Exception ex)
        {
            logger.Error("===========BtnDelete Exception====================");
            logger.Info(ex.Message.ToString());
            logger.Info("============End of BtnDelete Exception==============");
           lblMessege.Text = "Error while deleting records, please try again details:"+ex.Message;
        }
       BindRequests();
    }


    protected void btnCreateRequest_Click(object sender, EventArgs e)
    {
        _requestService = new UnifiedRequestService();
        _request = new UnifiedRequest();
        _request.ContractNo = contractNumber.Trim();
        _request.Created = DateTime.Now;
        _request.Modified = DateTime.Now;
        _request.ModifiedBy = gsName;
        _request.CreatedBy = gsName;
        _request.IsEnable = true;
        _request.IsPosted = false;
        _request.OwnerId = gsName;
        _requestService.Add(_request);
        BindRequests();
    }
    protected void btnEmpDependent_Click(object sender, EventArgs e)
    {
        string optionType = Cryption.Encrypt("AddEmployeeDependent");
        //Response.Redirect("~/Client/CCHICap.aspx?OptionType=AddEmployeeDependent");
        Response.Redirect("~/Client/CCHICap.aspx?OptionType=" + optionType);
    }

    protected void btnIndividualDependent_Click(object sender, EventArgs e)
    {
        string optionType = Cryption.Encrypt("AddDependent");
        //Response.Redirect("~/Client/CCHICap.aspx?OptionType=AddDependent");
        Response.Redirect("~/Client/CCHICap.aspx?OptionType=" + optionType);
    }

    protected void btnEmpDependentbatch_Click(object sender, EventArgs e)
    {
        string optionType = Cryption.Encrypt("AddEmployeeDependent");
        //Response.Redirect("~/Client/BatchUpload.aspx?OptionType=AddEmployeeDependent");
        Response.Redirect("~/Client/BatchUpload.aspx?OptionType=" + optionType);
    }

    protected void rptRequest_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
}