
using System.Collections;
using System;
using System.Data;
using System.Configuration;

using System.Web.UI.WebControls;

using System.Text;

using OS_DXC_WAP.CaesarWS;
public partial class CtntMyDetails : System.Web.UI.Page
{
    string  strClientID ;   
    private ServiceDepot_DNService ws;

   //change the name of the constructor
    public CtntMyDetails()
    {
        //InitializeComponent();
        this.Load += new EventHandler(CtntMyDetails_Load);
        
}
    protected void rdbSearchOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Master.ddlStaffList.Visible == true)
        {
            Master.ddlStaffList.Visible = false;
        }
        Master.SearchText.Text = "";
        Message1.Text = "";
        if (UpdatePanel2.Visible == true)
            UpdatePanel2.Visible = false;

    }

//change the name of the constructor and the page title
    void CtntMyDetails_Load(object sender, EventArgs e)
{
    Master.SearchButton.Click += SearchButton_Click ;
    Master.ddlStaffList.SelectedIndexChanged += DDLDependentList_SelectedIndexChanged;
    Master.SearchOpt.SelectedIndexChanged += rdbSearchOption_SelectedIndexChanged;
    if (string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
        Response.Redirect("../default.aspx");
    strClientID = Session["ClientID"].ToString();
    Master.ddlStaffList.Visible = false;
    Master.PageTitle.Text = "My Details";
    UpdatePanel2.Visible = false;
    
}

    protected void DDLDependentList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strMemberExistStatus;
        if (Master.ddlStaffList.SelectedValue != "0")
        {
            strMemberExistStatus = CheckMemberExist(Master.ddlStaffList.SelectedValue);

            if (strMemberExistStatus != "Exist")
            {
                Message1.Text = strMemberExistStatus;
                Message1.Visible = true;
                Master.ddlStaffList.Visible = true;
                UpdatePanel2.Visible = false;
            }
            else
            {

                DisplayMemberInfoResult(Master.ddlStaffList.SelectedValue);
                Master.ddlStaffList.Visible = true;
                string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();  //Aramco PId Changes by Hussamuddin
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                    CchiStatus.Visible = false;
            }
        }
        else
        {
            Message1.Text = "Please select the valid name from the drop down";
            Message1.Visible = true;
            Master.ddlStaffList.Visible = true;
            UpdatePanel2.Visible = false;
        }
    }

    
private void SearchButton_Click(object sender, EventArgs e)
{

    string strMemberExistStatus;
    strMemberExistStatus = "";
    if (Master.SearchOpt.Text != "Staff No" && Master.SearchOpt.Text != "Badge No")
    {

        strMemberExistStatus = CheckMemberExist(Master.SearchText.Text);
        if (strMemberExistStatus != "Exist")
        {
            Message1.Text = strMemberExistStatus;
            Master.ddlStaffList.Visible = false;
        }
        else
            DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);


    }
    else
        DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);
    

//    DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);
}

    private string CheckMemberExist(string MemberNo)
    {
        ws = new ServiceDepot_DNService();

        ChkMbrExistRequest_DN request;
        ChkMbrExistResponse_DN response;
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new ChkMbrExistRequest_DN();
            request.membershipNo = MemberNo;
            request.iqamaNo = "";// IqamaNo;
            request.totalNoOfFamily = ""; // DependentNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            //            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1,1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.transactionID = TransactionManager.TransactionID();
          //  request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));


            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ChkMbrExist(request);
            // Cursor.Current = Cursors.Default;


            if (response.memberName != null)
            {
                return "Exist";
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }
            else
            {
                return response.errorMessage[0];
                //CaesarResult = "InValid";
                //CaesarMessage = response.errorMessage;
            }
            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            return "Error";
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }








 private void DisplayMemberExistForClientResult(string SearchText, string SearchType)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}
        

        EnqMbrListInfoRequest_DN request;
        EnqMbrListInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqMbrListInfoRequest_DN();
            request.customerNo = strClientID;

            if (SearchType == "Membership No")
            request.membershipNo = SearchText;
            else
            request.staffNo = SearchText;
            
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.detail == null)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = "Inputted '" + Master.SearchOpt.Text + "' does not exist in your group.";
            UpdatePanel2.Visible =false;
            Message1.Visible = true;
        }
            else
            {
                //if (SearchType == "Membership No")
                //{
                    Master.ddlStaffList.Items.Clear();

                    ListItem DepList0 = new ListItem();
                    DepList0.Text = "Please select from the list";
                    DepList0.Value = "0";
                    Master.ddlStaffList.Items.Add(DepList0);


                    foreach (MbrListDetail_DN dtl in response.detail)
                    {
                        ListItem DepList = new ListItem();
                        DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                        DepList.Value = dtl.membershipNo.ToString();
                        Master.ddlStaffList.Items.Add(DepList);


                    }

                    Master.ddlStaffList.Visible = true;
                    Message1.Visible = false;


                //}
                //else
                //{
                //    Master.ddlStaffList.Items.Clear();

                //    ListItem DepList0 = new ListItem();
                //    DepList0.Text = "Please select from the list";
                //    DepList0.Value = "0";
                //    Master.ddlStaffList.Items.Add(DepList0);
                    
                    
                //    foreach (MbrListDetail_DN dtl in response.detail)
                //    {
                //        ListItem DepList = new ListItem();
                //        DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                //        DepList.Value = dtl.membershipNo.ToString();
                //        Master.ddlStaffList.Items.Add(DepList);
                        

                //    }

                //    Master.ddlStaffList.Visible = true;
                //    Message1.Visible = false;


                //}

                
           //response.detail[0].relationship;
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }
            
            
            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }


    private void DisplayMemberInfoResult(string MemberNo)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        EnqMbrDetInfoRequest_DN request;
        EnqMbrDetInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqMbrDetInfoRequest_DN();
            request.membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.EnqMbrDetInfo(request);

            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.detail[0].memberName != null)
            {
                DisplayMbrDetInfo(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                

            }
            else
            {
                UpdatePanel2.Visible = false;
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            UpdatePanel2.Visible = false;
           // tabsModManager.Visible = false;

            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }

    }


    private void DisplayMbrDetInfo(EnqMbrDetInfoResponse_DN response)
    {


        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {
            lblMemberName.Text = response.detail[0].memberName;
            lblMembershipNo.Text = response.detail[0].membershipNo.ToString();
            lblCompanyName.Text = response.detail[0].companyName;
            lblEmployeeID.Text = response.detail[0].memberID;
            lblMobile.Text = response.detail[0].mobileNo;
            lblGender.Text = response.detail[0].gender;
            lblDateOfBirth.Text = String.Format("{0:d}", response.detail[0].memberDOB);
            lblEmployeeNo.Text = response.detail[0].employeeNo;
            lblType.Text = response.detail[0].memberTypeDescription;
            lblEmail.Text = response.detail[0].email;
            lblSponsorId.Text = response.detail[0].sponsorID;
            lblNationality.Text = "Caeser did't retrive the nationality";

            lblcustID.Text = response.detail[0].custNo;
            lblcustName.Text = response.detail[0].customerName;
            lblcustScheme.Text = response.detail[0].schemeName;
            lblPlicysDate.Text = String.Format("{0:d}", response.detail[0].startDate);
            lblPlicyeDate.Text = String.Format("{0:d}", response.detail[0].endDate);
            lblNationality.Text = response.detail[0].countryName;
            lblStatusP.Text = response.detail[0].CCHIStatus;
            lblStatusReason.Text = response.detail[0].CCHIRejReason;

            //hledit.Attributes.Add("onclick", "javascript:basicPopup(../member/editinfo.aspx?mem=" + lblMembershipNo.Text + ");");
            //lbtn.NavigateUrl = "../member/editinfo.aspx?mem=" + lblMembershipNo.Text + "&memname=" + lblMemberName.Text + "&mobile=" + lblMobile.Text + "&email=" + lblEmail.Text ;
            //lbtn.Target = "_blank";
            //Response.Redirect("../member/editinfo.aspx?mem=" );
            //hidQueryString.Value = "memberName=" + response.detail[0].memberName + "&" +
            //                      "membershipNo=" + response.detail[0].membershipNo + "&" +
            //                      "email=" + response.detail[0].email + "&" +
            //                      "mobileNo=" + response.detail[0].mobileNo;


            var quryString  = "memberName=" + response.detail[0].memberName + "&" +
                                 "membershipNo=" + response.detail[0].membershipNo + "&" +
                                 "email=" + response.detail[0].email + "&" +
                                 "mobileNo=" + response.detail[0].mobileNo;
            hidQueryString.Value = Cryption.Encrypt(quryString);



            UpdatePanel2.Visible = true;
        }
        else
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);

        //Message1.Text = sbResponse.ToString(); //MessageBox.Show(sbResponse.ToString());


    }




    
}
