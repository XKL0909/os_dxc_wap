﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Templates/Inner.Master" Inherits="CCHIValidator" Codebehind="CCHIValidator.aspx.cs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <link href="css/bootstrap.css" rel="stylesheet" rel="Stylesheet" />
    <style type="text/css">
        .FloatingDiv {
            position: fixed;
            top: 0;
            left: 0;
            text-align: center;
            padding: 200px 0 0 0;
            z-index: 999;
            width: 100%;
            height: 100%;
            background: url('../Images/Floatingimg.png') repeat top left;
        }
    </style>
   <div class="container">
            <div class="row">
                <asp:UpdatePanel ID="CCHIUpdatePanel" runat="server">
                     <Triggers>
                        <asp:PostBackTrigger ControlID="btnSubmit" />
                        <asp:PostBackTrigger ControlID="rptSponsors" />
                    </Triggers>
                    <ContentTemplate>
                         <asp:UpdateProgress ID="uprgWorkRequest" runat="server">
                        <ProgressTemplate>
                            <div class="FloatingDiv">
                                <asp:Image ID="Image1" runat="server" ImageUrl="../Images/ajax-loader.gif"
                                    Style="height: 279px" /><br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                        <div class="row text-left">
                            <a href="Request.aspx">«Back</a>
                        </div>
                        <div class="row ">
                            <div style='display: <%=lblMessge%>;' class="alert alert-danger alert-dismissable">

                                <strong>Error!</strong>&nbsp; <%=lblMessge%>
                            </div>
                        </div>
                        <div class="row">
                            <asp:Repeater ID="rptSponsors" runat="server" OnItemDataBound="OnItemDataBound">
                                <ItemTemplate>
                                    <br />
                                    <div class='alert alert-<%# Eval("ErrorDetails.ServiceResult").ToString()=="ValidationFailure" ?"danger":"success"%>'>
                                      Validation <%# FormatMessge(Eval("ErrorDetails.ServiceResult").ToString()) %>.
                                      <br />
                                        <%# Eval("ErrorDetails.ServiceResult").ToString()=="ValidationFailure"? "CCHI Error: "+ Convert.ToString(Eval("ErrorDetails.Description")):"" %>
                                    </div>
                                    <asp:Repeater ID="rptSponsor" runat="server" OnItemDataBound="rptSponsor_OnItemDataBound">
                                        <HeaderTemplate>
                                            <table class="table table-striped table-bordered dt-responsive nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Sponsor Id</th>
                                                        <th>Type</th>
                                                        <th>Saudi Main</th>
                                                        <th>Saudi Dependent</th>
                                                        <th>Non-Saudi Main</th>
                                                        <th>Non-Saudi Dependent</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("SponsorId") %></td>
                                                <td>
                                                    <%# Eval("CapType") %>
                                                </td>
                                                <td>
                                                    <div id="saudiMain" style="text-align: center" runat="server">
                                                        <%# Eval("SaudiMain") %>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="saudiDependent" style="text-align: center" runat="server">
                                                        <%# Eval("SaudiDependent") %>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="nonSaudiMain" style="text-align: center" runat="server">
                                                        <%# Eval("NonSaudiMain") %>
                                                    </div>
                                                    <td>
                                                        <div id="nonSaudiDependent" style="text-align: center" runat="server">
                                                            <%# Eval("NonSaudiDependent") %>
                                                        </div>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                                  
                        <div class="row submit">
                            <asp:Button runat="server" CssClass="btnTOAdd btn-primary bb" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click" />
                            <asp:Button runat="server" CssClass="btnTOAdd btn-primary bb" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
			<br />
        <uc2:OSNav ID="OSNav1" runat="server" />
        <br />
        </div>
     
</asp:Content>


<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>
</asp:Content>
