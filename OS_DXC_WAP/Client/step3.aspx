﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="step3" Codebehind="step3.aspx.cs" %>
<form id="upload" method="post" action="FileUploadHandler.ashx" runat="server" enctype="multipart/form-data">
    <asp:hiddenfield runat="server" id="hdnIsFileUpload" />

    <div class="card bb">
        <div class="header bb">
            <h2>Verify </h2>
        </div>
        <div class="body bb">
            <div class="row bb">
                <div class="alert alert-info alert-dismissable bb">
                    <div style="color: #337ab7; text-align: left">Max individual file upload size is 5 MB. Note that attempting to upload large files over a slow internet connection may result in a timeout.</div>
                    <div style="color: #337ab7; text-align: right; direction: rtl">
                        الحد الأقصى لحجم كل ملف يراد تحميله 5 
            ميجا بيت (MB). لاحظ أن محاولة تحميل ملفات كبيرة عبر اتصال إنترنت بطيء قد يؤدي إلى قطع اتصالك بالموقع
                    </div>
                    <div runat="server" style="color: #337ab7; text-align: center" id="DivSMEMEssage">
					<br / >
                        Please upload Medical Declaration & Copy of ID / يرجى تحميل الفحص الطبي وصورة من الهوية
                    </div>
                </div>

            </div>
            <div class="row clearfix bb">
                <div class="form-group bb">
                    <div class="form-line bb">

                        <asp:label runat="server" id="lblError"></asp:label>
                    </div>
                </div>
            </div>
            <div id="cont" class="row clearfix bb">
                <div id="drop">

                    <a>Upload Documents</a>
                    <input type="file" name="upl" multiple />
                </div>
                <div class="row clearfix margin-33 bb">
                    <div id="gridFileUpload"></div>
                </div>
                <div class="actions clearfix text-right bb">
                    <asp:button id="btnNext3" validationgroup="valGrpStep2" runat="server" text="Submit" class="btn btn-primary btn-sm pull-right bb" />
                </div>
            </div>
        </div>
    </div>
</form>
<script src="js/step3.js"></script>
<script>
</script>