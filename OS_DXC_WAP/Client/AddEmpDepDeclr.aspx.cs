﻿using Bupa.Core.Logging;
//using Bupa.Core.Logging;
using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Configuration;
using Bupa.Core.Logging;
using OS_DXC_WAP.CaesarWS;
using Bupa.Core.Utilities.Database;
using OS_DXC_WAP.PersonWSN;
///using localPersonService;
using System.Linq;
using Telerik.Web.UI;
using System.IO;

public partial class AddEmpDepDeclr : System.Web.UI.Page
{
    #region "Page Public Variables"
    DataTable DT;
    DataSet DS;
    int param = 0;
    Boolean DependentFlag;
    bool blnYakeenDisabled = false;
    bool blnViewMode = false;
    string strPassedRefNo = string.Empty;
    private ServiceDepot_DNService ws;
    private OS_DXC_WAP.avayaWS.Service1 avaya;
    string strClientID;
    bool isRowDeleted = false;
    bool _isBatch = false;
    ///Diasbled by Sakthi on 02-Nov-2016 for all yakeen call should be through person service.
    ///Start
    ///private YakeenWS.Yakeen4BupaService Yws = new YakeenWS.Yakeen4BupaService();
    ///End
    string strYakeenUsername = ConfigurationManager.AppSettings["Yakeen Username"].ToString();
    string strYakeenPassword = ConfigurationManager.AppSettings["Yakeen Password"].ToString();
    string strYakeenChargeCode = ConfigurationManager.AppSettings["Yakeen Chargecode"].ToString();
    string strYakeenRefNo = ConfigurationManager.AppSettings["Yakeen RefNo"].ToString();
    private string _uploadCategory = UploadCategory.MEMBER_ADDEMP;
    private UmAlQuraCalendar hijriCal = new UmAlQuraCalendar();
    private GregorianCalendar gregCal = new GregorianCalendar(GregorianCalendarTypes.USEnglish);
    private CultureInfo arabicCulture = new CultureInfo("ar-SA");
    private CultureInfo englishCulture = new CultureInfo("en-US");
    int dataValidityHours = 0;

    private static ILog _log = LogManager.GetLogger(typeof(AddEmpDepDeclr));

    #endregion

    protected void Page_Init(object sender, System.EventArgs e)
    {
        try
        {
            System.Net.ServicePointManager.CertificatePolicy = new CustomCertificatePolicy();
            lnkBatchUpload.ToolTip = Session.Timeout.ToString();

            if (Session["ClientID"] == null)
                HttpContext.Current.Response.Redirect("~/Default.aspx");

            strClientID = Session["ClientID"].ToString();

            //to check whether YakeenDisabled or not
            blnYakeenDisabled = CheckYakeenEnabledForTheContract(strClientID);

            //to  check whether the request Add employee or add dependent
            if (Request.QueryString["EType"] != null && Request.QueryString["EType"] == "Dep")
            {
                PanelMembershipNoOnly.Visible = true;
                PanelEmployee.Visible = false;
                lnkBtnAddNewDep.Visible = false;
                DependentFlag = true;
                lblAdd_Employee.Text = "Add Dependent";
                lnkBatchUpload.NavigateUrl = "~/Client/BatchUpload.aspx?OptionType=AddDependent";
                txtFlagForAddingDepValidation.Visible = true;
                RangeValidatorForDep.Enabled = true;
                if (txtFlagForAddingDepValidation.Text != "0")
                    RangeValidatorForDep.Enabled = false;
                // Set the upload category to be for dependents
                _uploadCategory = UploadCategory.MEMBER_ADDDEP;
            }





            this.btnSubmit.Attributes.Add("onclick", DisableTheButton(this.Page, this.btnSubmit));
        }
        catch (Exception ex)
        {
            litMainError.Text = ex.Message;
        }
    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            //to reset the error handeling message
            ResetTheErrorMessagesInPage();

            //to save the javascript status between postback
            KeepTheJavaScriptStateBetweenPostBacks();

            SetupUploader(_uploadCategory);

            if (!Page.IsPostBack)
            {
                PopulateClassList(strClientID);
                PopulateBranchList(strClientID);
                if (IsDeclarationRequired(strClientID) == "Y")
                    PanelDeclaration.Visible = true;
                LoadCaesarSourcedData();
                BindTitleDDL();
                RadNotification1.Visible = true;
            }
            else
            {
                if (ddlEBranchCode.Items.Count == 0)
                    PopulateBranchList(strClientID);

                if (ddlLevelOfCover.Items.Count == 0 || ddlELevelOfCover.Items.Count == 0)
                    PopulateClassList(strClientID);

                //to load dependent grid from ViewState
                LoadDependentsGridFromViewState();

                //Disable Rad notification since it should appear one time only
                DisableRadNotification();
            }

            if (Request.QueryString["refno"] != null)
            {
                blnViewMode = true;
                strPassedRefNo = Request.QueryString["refno"].ToString();

                LoadViewModeByRefNo(strPassedRefNo, strClientID);
            }


            #region Dependendts check with YAKEEN
            //ManageDependentsGridsForCaesarAndYakeen();
            #endregion

        }
        catch (Exception ex)
        {
            litMainError.Text = ex.Message;
            litMainError.Visible = true;
            //throw ex;
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            int uploadedCount = 0;
            UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);
            DataSet uploadedFileSet = uploader.UploadedFileSet;

            if (_uploadmanger.WebIndecator("StaffNoInd", strClientID))
            {
                RequiredFieldValidatorEmpNo.Enabled = true;
                RequiredFieldValidatorEmpNoDep.Enabled = true;
                txtEEmployeeNo.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0");
                txtEmployeeNo.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFC0");
            }
            else
            {
                RequiredFieldValidatorEmpNo.Enabled = false;
                RequiredFieldValidatorEmpNoDep.Enabled = false;
                txtEEmployeeNo.BackColor = System.Drawing.Color.Transparent;
                txtEmployeeNo.BackColor = System.Drawing.Color.Transparent;
            }

            if (_uploadmanger.WebIndecator("DeptCodeInd", strClientID))
                RequiredFieldValidatorDeptCode.Enabled = true;
            else
                RequiredFieldValidatorDeptCode.Enabled = false;

            if (!Page.IsPostBack && _uploadmanger.WebIndecator("WebSupDocInd", strClientID))
            {
                tblUploader.Visible = true;
                if (uploadedFileSet != null && uploadedFileSet.Tables.Count > 0)
                {
                    uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                    if (uploadedCount > 0)
                        btnSubmit.Enabled = true;
                }
            }
            else
                btnSubmit.Enabled = true;
        }
        catch (Exception ex)
        {
            litMainError.Text = ex.Message;
        }
    }

    protected void btnProceedEmployee_Click(object sender, EventArgs e)
    {
        try
        {
            if (blnYakeenDisabled == true)
            {
                DisableYaqeenForEmployeeIncaseYakeenDown();
            }
            else
            {
                string verificationId = ddlSponsor.SelectedItem.Text;


                Person personInfo = null;
                if (!string.IsNullOrWhiteSpace(txtEmpHijriYearofBirth.Text))
                    verificationId = txtEmpHijriYearofBirth.Text;

                //Get person infor from Person web service (integrated with Yakeen)
               
                 personInfo = GetMemberInfoFromYakeen(txtEmployeeID.Text, /*ddlSponsor.SelectedItem.Text*/verificationId);
                   
                    if (personInfo != null && string.IsNullOrEmpty(personInfo.ExceptionMessage))
                    SetEmployeeDetails(personInfo);
                    else
                    {
                        pnlEmpDetails.Visible = false;
                        lblYaqeenError.Text = personInfo.ExceptionMessage;
                        lblYaqeenError.Visible = true;
                        pnlDepemdentMain.Visible = false;
                        divEmpHijriYearOfBirth.Visible = true;
                        return;
                    }

                    //To check the name is not exceeding 40 char, because Caesar allow only 40 character maximum
                    bool blnCheck = CheckNameCharcterCount(txtEMemberName, litErrorEmployeeExceed40, ddlEIDType);
                    

            }

            if (ddlEIDType.SelectedItem.Value == "1")
                SelectSaudiNationalityForSaudiMembers(ddlENationality);
        }
        catch (Exception ex)
        {
            if (ex.Message == "The Nin format is not valid")
            {
                pnlEmpDetails.Visible = false;
                lblYaqeenError.Text = "Saudi ID No. is incorrect   |    رقم الهوية الوطنية غير صحيح";
                lblYaqeenError.Visible = true;
                pnlDepemdentMain.Visible = false;
                divEmpHijriYearOfBirth.Visible = true;
                txtEmployeeID.Focus();
            }
            else if (ex.Message.Trim() == "An Error Occurred while processing your request. Please try again later")
            {
                DisableYaqeenForEmployeeIncaseYakeenDown();
            }
            else
            {
                pnlEmpDetails.Visible = false;
                lblYaqeenError.Text = ex.Message;
                lblYaqeenError.Visible = true;
                pnlDepemdentMain.Visible = false;
                divEmpHijriYearOfBirth.Visible = true;
                SendEmailAndUnhandeledErrorMessage(ex, txtEmployeeID.Text, ddlSponsor.SelectedItem.Text);
            }
        }
    }

    private void SetOccupation(Person personInfo)
    {
        txtProfessionFromYakeen.Visible = true;
        // added by sakthi on 29jan2016 to set the occupation dropdown getting from yakeen
        if (!string.IsNullOrEmpty(personInfo.JobCode) && Convert.ToString(personInfo.JobCode).Trim() != "0")
        {
            //ListItem li = ddProfession.Items.Cast<ListItem>()
            //     .Where(x => x.Value.Contains(personInfo.JobCode))
            //     .LastOrDefault();
            ddProfession.SelectedIndex = ddProfession.Items.IndexOf(ddProfession.Items.FindItemByValue(personInfo.JobCode));
            ddProfession.Enabled = false;
        }
        else
            ddProfession.Enabled = true;

        if (!string.IsNullOrWhiteSpace(personInfo.OccupationAr))
        {
            txtProfessionFromYakeen.Text = personInfo.OccupationAr;
        }
    }

    private void SetNationality(Person personInfo)
    {
        if (txtEmployeeID.Text.StartsWith("1"))
        {
            SelectSaudiNationalityForSaudiMembers(ddlENationality);
        }
        else
        {
            setNationalityinDropDownList(personInfo.NationalityCode, ddlENationality);
            ////ddlENationality.Enabled = !string.IsNullOrEmpty(personInfo.NationalityCode) && Convert.ToString(personInfo.NationalityCode).Trim() != "0" ? false : true;
            ddlENationality.Enabled = ddlENationality.SelectedIndex > 0 ? false : true;
        }

    }

    protected void btnProceedDep_Click(object sender, EventArgs e)
    {
        try
        {
            //If the age group newborn let him proceed normal
            if (blnYakeenDisabled == true || (ddlAgeGroup.SelectedValue == "1" && txtDependentID.Text.Trim() == ""))
            {
                DisableYaqeenForDependentYakeenDownOrNewBorn();
            }
            else
            {
                try
                {
                    if (Session["DependentsNotUnderBupa"] != null)
                    {
                        List<string> dependentsNotUnderBupa = (List<string>)Session["DependentsNotUnderBupa"];
                        if (!dependentsNotUnderBupa.Exists(x => x.Equals(txtDependentID.Text)))
                            throw new Exception(string.Format("As shown in above grid for dependents  which are not covered with Bupa Insurance- You can only add members with Iqama/Saudi ID as: <br/> {0} ", string.Join(",", dependentsNotUnderBupa.ToArray())));
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }



                if (ddlIDType.SelectedItem.Value == "1")
                {
                    ProceedDependentByNationalID();
                }
                else if (ddlIDType.SelectedItem.Value == "2")
                {
                    ProceedDependentByIqamaNo();
                }
                else if (ddlIDType.SelectedItem.Value == "4")
                {
                    ProceedDependentByBorderNo();
                }

                //To check the name is not exceeding 40 char, because Caesar allow only 40 character maximum
                bool blnCheck = CheckNameCharcterCount(txtMemberName, litErrorDepExceed40, ddlIDType);
            }

            if (ddlIDType.SelectedItem.Value == "1")
                SelectSaudiNationalityForSaudiMembers(ddlNationality);

            if (ddlMemberType.SelectedItem.Value == "S")
                SetTheDefaultValuesForSpouse();

            //// CR304 disabled by sakthi on 01-Sep-2016
            //// IQAMA/Border Entry ID should not be mandatory for Less than or equal 1 year
            ////Start
            //// Old Code 
            //// if (ddlMemberType.SelectedItem.Value == "C") 
            //// New Code          
            if (ddlMemberType.SelectedItem.Value == "C" || ddlMemberType.SelectedItem.Value == "SO" || ddlMemberType.SelectedItem.Value == "D")
            {
                SetTheDefaultValuesForChild();
            }
            else
            {
                string verificationId = txtSponsorId.Text;

                if (!string.IsNullOrWhiteSpace(txtDepHijriYearofBirth.Text))
                    verificationId = txtDepHijriYearofBirth.Text;

                Person personInfo = GetMemberInfoFromYakeen(txtDependentID.Text, verificationId);
                if (personInfo != null && string.IsNullOrEmpty(personInfo.ExceptionMessage))
                {
                    setNationalityinDropDownList(personInfo.NationalityCode, ddlNationality);
                    //ddlNationality.SelectedIndex = ddlNationality.Items.IndexOf(new ListItem(personInfo.NationalityCode));
                }
                else
                {
                    pnlDependentDetail.Visible = false;
                    lblYaqeenDepError.Text = personInfo.ExceptionMessage;
                    lblYaqeenDepError.Visible = true;
                    divDepHijriYearOfBirth.Visible = true;
                    return;
                }
            }
            ////End


        }
        catch (Exception ex)
        {


            if (ex.Message == "The Nin format is not valid")
            {

                lblYaqeenDepError.Text = "Saudi ID No. is incorrect    |    رقم الهوية الوطنية غير صحيح";
                pnlDependentDetail.Visible = false;
                lblYaqeenDepError.Visible = true;
                divDepHijriYearOfBirth.Visible = true;
                txtDependentID.Focus();
            }
            else if (ex.Message.Trim() == "An Error Occurred while processing your request. Please try again later")
            {
                DisableYaqeenForDependentYakeenDownOrNewBorn();
            }
            else
            {
                pnlDependentDetail.Visible = false;
                lblYaqeenDepError.Text = ex.Message;
                lblYaqeenDepError.Visible = true;
                divDepHijriYearOfBirth.Visible = true;
                SendEmailAndUnhandeledErrorMessage(ex, txtDependentID.Text, txtSponsorId.Text);
            }
        }
    }



    protected void btnAddDep_Click(object sender, System.EventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            //To check the name is not exceeding 40 char, because Caesar allow only 40 character maximum
            if (CheckNameCharcterCount(txtMemberName, litErrorDepExceed40, ddlIDType))
            {
                litDepMainError.Text = "<span style='color:red'>The name exceeded the accepted number of characters “40 characters”." +
                                    "Please modify the name in order to process the request <br />" + Environment.NewLine +
                                "عدد أحرف الاسم تخطت الحد المسموح 40 حرف الرجاء تعديل الاسم بحيث لا يتخطى الحد المسموح" +
                                " </span>";
                txtMemberName.Focus();
                goto exitAddmember;
            }
            //UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);
            //if (_uploadmanger.WebIndecator("DepdIdInd", strClientID))
            //{
            //}

            try
            {
                DateTime dt = Convert.ToDateTime(txtJoiningDate.GregorianDate);
            }
            catch (Exception)
            {
                RadWindowManager1.RadAlert("Invalid Arrival date in KSA. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
                return;
                //throw new Exception("Invalid Joining Date");
            }

            try
            {
                DateTime dt = Convert.ToDateTime(txtRequestedStartDate.GregorianDate);
            }
            catch (Exception)
            {
                RadWindowManager1.RadAlert("Invalid Start date for medical cover. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
                return;
                //throw new Exception("Invalid Requested Date");
            }

            try
            {
                if (ddlAgeGroup.SelectedValue.Trim() != "1")
                {
                    DateTime dt = Convert.ToDateTime(txtIDExpDate.GregorianDate);
                }
                else
                    txtIDExpDate.Clear();

            }
            catch (Exception)
            {
                RadWindowManager1.RadAlert("Invalid ID Expiry date. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
                return;
                //throw new Exception("Invalid Requested Date");
            }

            try
            {
                DateTime dt = Convert.ToDateTime(txtDateOfBirth.GregorianDate);
                DateTime _current = DateTime.Today;
                TimeSpan span = _current.Subtract(dt);
                int days = span.Days;
                if(days <0)
                {
                    RadWindowManager1.RadAlert("Date of birth should not be future date", 330, 180, "Validation Message", "");
                    return;
                }
            }
            catch (Exception)
            {
                RadWindowManager1.RadAlert("Invalid Date of Birth. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
                return;
                //throw new Exception("Invalid Requested Date");
            }
            if (CheckUnderAge(Convert.ToDateTime(txtDateOfBirth.GregorianDate)) || ddlAgeGroup.SelectedValue != "1" || txtDependentID.Text.Trim() != "")
            {
                AddDependentInfoToViewState();
                gvDependents.SelectedIndex = -1;
                gvDependents.Focus();
                if (DependentFlag == true)
                {
                    int DepCounter = Int32.Parse(txtFlagForAddingDepValidation.Text) + 1;
                    txtFlagForAddingDepValidation.Text = DepCounter.ToString();
                    RangeValidatorForDep.Enabled = false;
                }
                else
                {
                    litDepMainError.Text = "Dep Flag = false";
                }
                btnSubmit.Enabled = true;
            }
            else
            {
                litDepMainError.Text = "<br /><span style='color:red'>Child age is more than 365 days. Please make sure you entered the correct date of birth and try again or select Age Group “Above 1 Year” and provide ID/Iqama number.<br /></span> " +
                                       "<br /><span style='color:red' dir='rtl'>" + "عمر الطفل يتجاوز 12 أشهر. الرجاء التأكد من تاريخ الميلاد المدخل والمحاولة مرة أخرى أواختيارالفئة العمرية (Age Group) أكبر من 12 أشهر (Above 1 Year) وتزويدنا برقم الهوية/الإقامة." + "</p></span>";
                ////litDepMainError.Text = "<br /><span style='color:red'>Child age is more than 90 days. Please make sure you entered the correct date of birth and try again or select Age Group “Older than 3 months” and provide ID/Iqama number.<br /></span> " +
                ////                       "<br /><span style='color:red' dir='rtl'>" + "عمر الطفل يتجاوز 3 أشهر. الرجاء التأكد من تاريخ الميلاد المدخل والمحاولة مرة أخرى أواختيارالفئة العمرية (Age Group) أكبر من 3 أشهر (Older than 3 months) وتزويدنا برقم الهوية/الإقامة." + "</p></span>";
                btnGetMemberDetails.Focus();
            }
        exitAddmember: ;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }

    protected void btnUpdateDep_Click(object sender, System.EventArgs e)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(txtJoiningDate.GregorianDate);
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid Arrival date in KSA. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
            return;
            //throw new Exception("Invalid Joining Date");
        }

        try
        {
            DateTime dt = Convert.ToDateTime(txtRequestedStartDate.GregorianDate);
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid Start date for medical cover. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
            return;
            //throw new Exception("Invalid Requested Date");
        }

        try
        {
            if (Convert.ToString(ViewState["AgeGroup"]) != "1")
            {
                DateTime dt = Convert.ToDateTime(txtIDExpDate.GregorianDate);
            }
            else
                txtIDExpDate.Clear();
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid ID Expiry date. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
            return;
            //throw new Exception("Invalid Requested Date");
        }

        try
        {
            DateTime dt = Convert.ToDateTime(txtDateOfBirth.GregorianDate);
            DateTime _current = DateTime.Today;
            TimeSpan span = _current.Subtract(dt);
            int days = span.Days;
            if (days < 0)
            {
                RadWindowManager1.RadAlert("Date of birth should not be future date", 330, 180, "Validation Message", "");
                return;
            }
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid Date of Birth. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
            return;
            //throw new Exception("Invalid Requested Date");
        }

        ////if (CheckUnderAge(Convert.ToDateTime(txtDateOfBirth.GregorianDate)) || ddlAgeGroup.SelectedValue != "1" || txtDependentID.Text.Trim() != "")
        if (CheckUnderAge(Convert.ToDateTime(txtDateOfBirth.GregorianDate)) || Convert.ToString(ViewState["AgeGroup"]) != "1" || txtDependentID.Text.Trim() != "")
        {           
        }
        else
        {
            litDepMainError.Text = "<br /><span style='color:red'>Child age is more than 365 days. Please make sure you entered the correct date of birth and try again or select Age Group “Above 1 Year” and provide ID/Iqama number.<br /></span> " +
                                   "<br /><span style='color:red' dir='rtl'>" + "عمر الطفل يتجاوز 12 أشهر. الرجاء التأكد من تاريخ الميلاد المدخل والمحاولة مرة أخرى أواختيارالفئة العمرية (Age Group) أكبر من 12 أشهر (Above 1 Year) وتزويدنا برقم الهوية/الإقامة." + "</p></span>";
           
            btnGetMemberDetails.Focus();
            return;
        }

        if (UpdateDependentValuesInGrid(gvDependents.SelectedIndex) == true)
        {
            gvDependents.SelectedIndex = -1;
            Reset_All_Input_Fields();
            btnUpdateDep.Visible = false;
            btnSubmit.Visible = true;
            pnlDepemdentMain.Visible = false;
            btnCancelUpdateDep.Visible = false;
            //btnAdd.Visible = true;
            lnkBtnAddNewDep.Visible = true;
        }
    }

    protected void btnCancelUpdateDep_Click(object sender, EventArgs e)
    {
        btnUpdateDep.Visible = false;
        gvDependents.SelectedIndex = -1;
        Reset_All_Input_Fields();

        btnCancelUpdateDep.Visible = false;
        btnSubmit.Visible = true;
        btnAddDep.Visible = true;
        gvDependents.Visible = true;
        DT = (DataTable)ViewState["dt"];
        gvDependents.DataSource = DT;
        gvDependents.DataBind();
        GetAllDependentsName();
        pnlDepemdentMain.Visible = false;
        lnkBtnAddNewDep.Visible = true;
    }

    protected void btnSubmit_Click(object sender, System.EventArgs e) //it was 500 lines :)
    {
        try
        {
            //To check the name is not exceeding 40 char, because Caesar allow only 40 character maximum
            if (CheckNameCharcterCount(txtEMemberName, litErrorEmployeeExceed40, ddlEIDType))
                goto exitSubmit;
            //to check if the member click submit without add and dependent 
            if (DependentFlag == true && gvDependents.Rows.Count == 0)
            {
                litDepMainError.Text = "<span style='color:red'>You shoud add at least one dependent before submit<br> يجب اضافة تابع واحد على الاقل " + "</span>";
                goto exitSubmit;
            }
            //to check if the user press submit before proceed
            if (btnProceedEmployee.Visible == true)
            {
                lblYaqeenError.Text = "Please press proceed first"; goto exitSubmit;
            }
            if (Request.QueryString["EType"] != null && Request.QueryString["EType"].ToUpper() == "DEP")
            {
            }
            else
            {
                try
                {
                    DateTime dt = Convert.ToDateTime(txtEJoiningDate.GregorianDate);
                }
                catch (Exception)
                {
                    RadWindowManager1.RadAlert("Invalid Joining date with company. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
                    return;
                    //throw new Exception("Invalid Joining Date");
                }
                try
                {
                    DateTime dt = Convert.ToDateTime(txtERequestedStartDate.GregorianDate);
                }
                catch (Exception)
                {
                    RadWindowManager1.RadAlert("Invalid Start date for medical cover. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
                    return;
                    //throw new Exception("Invalid Requested Date");
                }
            }

            if (blnViewMode)
            {
                ReSubmitRequestViewMode();
            }
            else
            {
                SubmitRequestForAddNewMode();
            }

        }
        catch (Exception ex)
        {
            Message1.ForeColor = System.Drawing.Color.Red;
            Message1.Text = ex.Message + "" + ex.InnerException;
        }
    exitSubmit: ;
    }

    protected void btnGetMemberDetails_Click(object sender, EventArgs e)
    {
        try
        {

            litDepMainError.Text = "";
            MemberHelper mh = new MemberHelper(txtMainMemberNo.Text.Trim(), strClientID);
            Member m = mh.GetMemberDetails();

            if (Check_Member_belong_to_client(m.ContractNumber))
            {
                MainMemberIqamaSaudiID = m.SaudiIqamaID;
                MainMembershipNo = txtMainMemberNo.Text.ToString().Trim();
                //DependentHijriYearOfBirth = txtEmpHijriYearofBirth0.Text.ToString().Trim();

                #region Dependendts check with YAKEEN
                //ManageDependentsGridsForCaesarAndYakeen();
                #endregion


                string strMemberNo = m.SaudiIqamaID;
                string _returnValue = m.SaudiIqamaID;
                if (strMemberNo.StartsWith("3") || strMemberNo.StartsWith("4") || strMemberNo.StartsWith("5") || strMemberNo.StartsWith("6"))
                    _returnValue = m.SponsorID;

                if (!string.IsNullOrEmpty(m.SaudiIqamaID))
                {
                    txtSponsorId.Text = _returnValue;
                    txtMainMemberNo.Enabled = false;
                    tblUploader.Visible = true;
                    lnkBtnAddNewDep_Click(sender, e);
                    btnGetMemberDetails.Enabled = false;
                }

                //to check if it is passport or data entry clear the sponser field
                if (!string.IsNullOrEmpty(m.IDType))
                {
                    if (m.IDType.Trim() == "3" || m.IDType.Trim() == "4")
                        txtSponsorId.Text = "";
                }
            }
            else
            {
                litDepMainError.Text = "<span style='color:red'>" + "Inputted 'Membership No' does not exist in your group." + "</span>";
            }



        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("Object reference not set to an instance of an object"))
            {
                litDepMainError.Text = "<span style='color:red'>" + "Please check, provided information is not valid" + "</span>";
            }
            else
            {
                litDepMainError.Text = "<span style='color:red'>" + ex.Message + "</span>";
            }
        }
    }

    protected void ddlELevelOfCover_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ServiceDepot_DNService ws;
            EnqContMbrTypeListRequest_DN request;
            EnqContMbrTypeListResponse_DN response;
            request = new EnqContMbrTypeListRequest_DN();
            ws = new ServiceDepot_DNService();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            long ls_txnid = WebPublication.GenerateTransactionID3();
            request.transactionID = ls_txnid;
            request.contNo = strClientID;
            int intClsID = Convert.ToInt32(ddlELevelOfCover.SelectedValue.ToString());
            request.contCls = intClsID;
            request.grp_Type = "E";
            response = ws.EnqContMbrTypeList(request);

            //Convert object to Xml 
            //If webservice is not responding fine, then send the request & response xml to Caesar team 
            //Below code need to enable, it will saving the request & rsponse xml in specifeied path
            //Strat
            ////ClassXMLGeneration<EnqContMbrTypeListRequest_DN> XmlReq = new ClassXMLGeneration<EnqContMbrTypeListRequest_DN>();
            ////XmlReq.Request(request, "EnqContMbrTypeList_Request");



            ////ClassXMLGeneration<EnqContMbrTypeListResponse_DN> XmlResp = new ClassXMLGeneration<EnqContMbrTypeListResponse_DN>();
            ////XmlResp.Response(response, "EnqContMbrTypeList_Response");
            //End


            ddlEMemberType.Items.Clear();
            if (response.status == "0")
            {
                ddlEMemberType.Items.Add(new ListItem("-- Select --", "0"));
                for (int i = 0; i < response.detail.Length; i++)
                {
                    ddlEMemberType.Items.Add(new ListItem(response.detail[i].mbrTypeDesc, response.detail[i].mbrType));
                }
            }
            else
            {
                ddlEMemberType.Items.Add(new ListItem("-- Not Applicable --", "0"));
                lblYaqeenError.Text = "The selected level of cover is not applicable for you.";
            }

        }
        catch (Exception ex)
        {
            lblYaqeenError.Text = ex.Message;
        }
    }

    protected void ddlLevelOfCover_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ServiceDepot_DNService ws;
            EnqContMbrTypeListRequest_DN request;
            EnqContMbrTypeListResponse_DN response;
            request = new EnqContMbrTypeListRequest_DN();
            ws = new ServiceDepot_DNService();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            long ls_txnid = WebPublication.GenerateTransactionID3();
            request.transactionID = ls_txnid;
            request.contNo = strClientID;
            int intClsID = Convert.ToInt32(ddlLevelOfCover.SelectedValue.ToString());
            request.contCls = intClsID;
            request.grp_Type = "D";
            response = ws.EnqContMbrTypeList(request);
            ddlMemberType.Items.Clear();
            lblYaqeenDepError.Text = "";
            if (response.status == "0")
            {
                ddlMemberType.Items.Add(new ListItem("-- Select --", "0"));               
                //CR-PID_CAESAR(Remove the relation type - child for BUPA Direct Contract) by Sakthi on 07-Aug-2016
                //Start
                string contractType = Convert.ToString(Session["ContractType"]);
                for (int i = 0; i < response.detail.Length; i++)
                {

                    ////bool isAdd = contractType.Trim().ToUpper().Equals("BDRC") || contractType.Trim().ToUpper().Equals("BDSC") ? response.detail[i].mbrTypeDesc.Trim().ToUpper().Equals("CHILD") ? false : true : true;
                    bool isDirectContract = contractType.Trim().ToUpper().Equals("BDRC") || contractType.Trim().ToUpper().Equals("BDSC") ? response.detail[i].mbrTypeDesc.Trim().ToUpper().Equals("CHILD") ? true : false : false;
                    bool isRemoveChild = false;                   
                    if (isDirectContract)
                    {
                        switch(ddlLevelOfCover.SelectedValue.ToString())
                        {
                            case "150":    ////Diamond Plus                
                            case "152":    ////Diamond Plus Senior
                            case "153":    ////Diamond                              
                            case "155":    ////Diamond Senior 
                            case "156":    ////Gold                         
                            case "158":    ////Gold Senior
                            case "159":    ////Silver                          
                            case "161":    ////Silver Senior
                            case "162":    ////Bronze                         
                            case "164":    ////Bronze Senior
                            case "165":    ////Blue                          
                            case "167":    ////Blue Senior
                            case "168":    ////Green                                          
                            case "170":    ////Green Senior
                            case "171":    ////Basic                            
                            case "173":    ////Basic Senior
                                {
                                    isRemoveChild = true;
                                    break;
                                }
                            default:
                                    break;

                        }
                    }
                   
                    if (!isRemoveChild)
                        ddlMemberType.Items.Add(new ListItem(response.detail[i].mbrTypeDesc, response.detail[i].mbrType));
                }
                //End
                ////ddlMemberType.Items.Add(new ListItem(response.detail[i].mbrTypeDesc, response.detail[i].mbrType));
            }
            else
            {
                ddlMemberType.Items.Add(new ListItem("-- Not Applicable --", "0"));
                lblYaqeenDepError.Visible = true;
                lblYaqeenDepError.Text = "The selected level of cover is not applicable for you.";
            }
        }
        catch (Exception ex)
        {
            lblYaqeenDepError.Text = ex.Message;
        }
    }

    protected void lnkBtnAddNewDep_Click(object sender, EventArgs e)
    {
        pnlDepemdentMain.Visible = true;

        ddlMaritalStatus.Enabled = true;
        ddDepProfession.Enabled = true;

        ddlMemberType.Enabled = true;
        ddlLevelOfCover.Enabled = true;
        ddlIDType.Enabled = true;
        txtDependentID.Enabled = true;
        txtSponsorId.Enabled = true;
        ddlAgeGroup.Enabled = true;
        txtDepHijriYearofBirth.Enabled = true;
        ddlGender.Enabled = true;
        txtDateOfBirth.Enabled = true;
        txtMemberName.Enabled = true;
        ddlNationality.Enabled = true;
        ddlTitle.Enabled = true;
        pnlDependentDetail.Visible = false;
        btnProceedDep.Visible = true;
        btnAddDep.Visible = false;
        lnkBtnAddNewDep.Visible = false;
        if (txtEmployeeID.Text.Trim() != "")
            txtSponsorId.Text = txtEmployeeID.Text;
        //to check if the main member visa entry
        if (ddlEIDType.SelectedValue == "4" && ddlSponsor.SelectedItem.Text.Trim() != "")
            txtSponsorId.Text = ddlSponsor.SelectedItem.Text;
    }

    protected string IsDeclarationRequired(string ClientID)
    {
        {
            ws = new ServiceDepot_DNService();
            EnqMandaDeclareFormRequest_DN request = new EnqMandaDeclareFormRequest_DN();
            EnqMandaDeclareFormResponse_DN response;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();
            request.contNo = strClientID;
            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.EnqMandaDeclareForm(request);

                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                    return response.status;
                }
                else
                {
                    return response.mandaDeclareFormIndicator.Trim();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public bool UpdateDependentValuesInGrid(int index)
    {
        if (CheckUnderAge(Convert.ToDateTime(txtDateOfBirth.GregorianDate)) || ddlAgeGroup.SelectedValue != "1" || txtDependentID.Text.Trim() != "")
        {
            DT = (DataTable)ViewState["dt"];
            DT.Rows[index][0] = ddlTitle.SelectedValue;
            DT.Rows[index][1] = txtMemberName.Text;
            DT.Rows[index][2] = txtDateOfBirth.GregorianDate;
            DT.Rows[index][3] = txtSponsorId.Text;
            DT.Rows[index][4] = txtEmployeeNo.Text;
            DT.Rows[index][5] = txtDependentID.Text;
            DT.Rows[index][6] = txtDepartmentCode.Text;
            DT.Rows[index][7] = ddlGender.SelectedValue;
            DT.Rows[index][8] = getNationalityForCaesarRequest(ddlNationality);
            DT.Rows[index][9] = ddlLevelOfCover.SelectedValue;
            DT.Rows[index][10] = ddlJoinReason.SelectedValue;
            DT.Rows[index][12] = txtJoiningDate.GregorianDate;
            DT.Rows[index][13] = txtRequestedStartDate.GregorianDate;
            DT.Rows[index][14] = txtMobileNo.Text;
            DT.Rows[index][15] = ddlMemberType.SelectedValue;
            DT.Rows[index][16] = ddlIDType.SelectedValue;
            DT.Rows[index][17] = txtIDExpDate.GregorianDate;
            DT.Rows[index][18] = ddlMaritalStatus.SelectedValue;
            DT.Rows[index][19] = ddDepProfession.SelectedValue;
            DT.Rows[index][20] = ddDepDistrict.SelectedValue;
            DT.Rows[index][21] = hidYakeenDependent.Value;
            DT.Rows[index]["DOBHijriYear"] = txtDepHijriYearofBirth.Text;
            Reset_All_Input_Fields();
            gvDependents.DataSource = DT;
            gvDependents.DataBind();
            GetAllDependentsName();

            ViewState["dt"] = DT;
            return true;
        }
        else
        {
            litDepMainError.Text = "<br /><span style='color:red'>Child age is more than 90 days. Please make sure you entered the correct date of birth and try again or select Age Group “Older than 3 months” and provide ID/Iqama number.<br /></span> " +
                                   "<br /><span style='color:red' dir='rtl'>" + "عمر الطفل يتجاوز 3 أشهر. الرجاء التأكد من تاريخ الميلاد المدخل والمحاولة مرة أخرى أواختيارالفئة العمرية (Age Group) أكبر من 3 أشهر (Older than 3 months) وتزويدنا برقم الهوية/الإقامة." + "</p></span>";
            btnGetMemberDetails.Focus();

            return false;
        }
    }

    public void AddDependentInfoToViewState()
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            if (ViewState["dt"] != null)
                DT = (DataTable)ViewState["dt"];
            else
            {
                DS = new DataSet();
                DS.ReadXmlSchema(Server.MapPath("DepdFile.xml"));
                Session["Dataset"] = DS;
                DT = DS.Tables[0];
            }
            DataRow dr = DT.NewRow();

            dr[0] = ddlTitle.SelectedValue;
            dr[1] = txtMemberName.Text;
            dr[2] = txtDateOfBirth.GregorianDate;
            dr[3] = txtSponsorId.Text;
            dr[4] = txtEmployeeNo.Text;
            dr[5] = txtDependentID.Text;
            dr[6] = txtDepartmentCode.Text;
            dr[7] = ddlGender.SelectedValue;
            dr[8] = getNationalityForCaesarRequest(ddlNationality); //ddlNationality.SelectedValue;
            dr[9] = ddlLevelOfCover.SelectedValue;
            dr[10] = ddlJoinReason.SelectedValue;
            dr[12] = txtJoiningDate.GregorianDate;
            dr[13] = txtRequestedStartDate.GregorianDate;
            dr[14] = txtMobileNo.Text;
            dr[15] = ddlMemberType.SelectedValue;
            dr[16] = ddlIDType.SelectedValue;
            dr[17] = txtIDExpDate.GregorianDate;
            dr[18] = ddlMaritalStatus.SelectedValue;
            dr[19] = ddDepProfession.SelectedValue;
            dr[20] = ddDepDistrict.SelectedValue;
            dr[21] = hidYakeenDependent.Value;
            dr["DOBHijriYear"] = txtDepHijriYearofBirth.Text;
            //// CR304 disabled by sakthi on 01-Sep-2016
            //// IQAMA/Border Entry ID should not be mandatory for Less than or equal 1 year
            dr["NewBornChild"] = (ddlMemberType.SelectedItem.Value == "C" || ddlMemberType.SelectedItem.Value == "SO" || ddlMemberType.SelectedItem.Value == "D") ? ddlAgeGroup.SelectedValue : "NA" ;
            dr["NationalityName"] = ddlNationality.SelectedItem.Text;
            ////
            DT.Rows.Add(dr);
            Reset_All_Input_Fields();
            gvDependents.DataSource = DT;
            gvDependents.DataBind();
            GetAllDependentsName();

            ViewState["dt"] = DT;
            pnlDepemdentMain.Visible = false;
            lnkBtnAddNewDep.Visible = true;

            BindTitleDDL();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }

    private void SetupUploader(string uploadCategory)
    {
        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        DataBind();
    }

    public void GetAllDependentsName()
    {
        lblAllDependentsName.Text = "";
        lblEmployeeName.Text = txtEMemberName.Text;
        for (int i = 0; i < gvDependents.Rows.Count; i++)
        {
            if (i == 0)
            {
                lblAllDependentsName.Text = gvDependents.Rows[i].Cells[1].Text;
            }
            else
            {
                lblAllDependentsName.Text = lblAllDependentsName.Text + "; " + gvDependents.Rows[i].Cells[1].Text;
            }
        }
    }

    #region "Page Init and Page Load Methods and Finctions"

    public bool CheckYakeenEnabledForTheContract(string strCltID)
    {
        try
        {
            string YakeenDisabledContracts = ConfigurationManager.AppSettings["ListOfYakeenDisabledContractNumber"].ToString();
            string[] YakeenDisabledContractList = YakeenDisabledContracts.Split(',');
            foreach (string contractNo in YakeenDisabledContractList)
            {
                if (strCltID == contractNo)
                {
                    return true;
                }
            }
            return false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void ResetTheErrorMessagesInPage()
    {
        try
        {
            lblEmpErrorMessage.Text = "";
            lblYaqeenError.Text = "";
            litErrorEmployeeExceed40.Visible = false;
            litErrorDepExceed40.Visible = false;
            litMainError.Text = "";
            litMainError.Visible = false;
            litDepMainError.Text = "";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void KeepTheJavaScriptStateBetweenPostBacks()
    {
        try
        {
            if (ddlMemberType.SelectedValue == "C")
            {
                divAgeGroup.Attributes.Add("style", "display:block");
            }

            if (ddlEIDType.SelectedValue == "1")
            {
                revBorder.Enabled = false;
                revIqama.Enabled = false;
            }

            else if (ddlEIDType.SelectedValue == "2")
            {
                revBorder.Enabled = false;
                revSuadi.Enabled = false;
                divEmpHijriYearOfBirth.Visible = false;
            }
            else if (ddlEIDType.SelectedValue == "4")
            {
                revIqama.Enabled = false;
                revSuadi.Enabled = false;
                divEmpHijriYearOfBirth.Visible = false;
            }

            //for Dependents
            if (ddlIDType.SelectedValue == "1")
            {
                revDepBorder.Enabled = false;
                revDepIqama.Enabled = false;
            }
            else if (ddlIDType.SelectedValue == "2")
            {
                revDepBorder.Enabled = false;
                revDepSuadi.Enabled = false;
                divDepHijriYearOfBirth.Visible = false;
            }
            else if (ddlIDType.SelectedValue == "4")
            {
                revDepIqama.Enabled = false;
                revDepSuadi.Enabled = false;
                divDepHijriYearOfBirth.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void DisableRadNotification()
    {
        try
        {
            RadNotification1.Visible = false;
            RadNotification1.Enabled = false;
            RadNotification1.ShowInterval = 0;
            RadNotification1.VisibleOnPageLoad = false;
            RadNotification1.AutoCloseDelay = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void LoadDependentsGridFromViewState()
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            if (ViewState["dt"] != null)
                DT = (DataTable)ViewState["dt"];

            if (DT != null)
            {
                _isBatch = true;
                gvDependents.DataSource = DT;
                gvDependents.DataBind();
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }
    #endregion

    #region "Yakeen Process Operations for Employee"

    private void ProceedEmployeeByNationalID()
    {
        try
        {
            //YakeenWS.citizenInfoByHijriDateOfBirthRequest SaudiMaleRequest = new YakeenWS.citizenInfoByHijriDateOfBirthRequest();
            //YakeenWS.citizenInfoByHijriDateOfBirthResult SaudiMaleResponse = new YakeenWS.citizenInfoByHijriDateOfBirthResult();

            //SaudiMaleRequest.userName = strYakeenUsername;
            //SaudiMaleRequest.password = strYakeenPassword;
            //SaudiMaleRequest.chargeCode = strYakeenChargeCode;
            //SaudiMaleRequest.nin = txtEmployeeID.Text;
            //SaudiMaleRequest.dateOfBirthHijri = txtEmpHijriYearofBirth.Text;
            //SaudiMaleRequest.referenceNumber = strYakeenRefNo;

            //SaudiMaleResponse = Yws.getCitizenInfoByHijriDateOfBirth(SaudiMaleRequest);
            //pnlEmpDetails.Visible = true;
            //txtEMemberName.Text = RemoveSpecialCharacters(SaudiMaleResponse.englishFirstName) + " " +
            //                      RemoveSpecialCharacters(SaudiMaleResponse.englishSecondName) + " " +
            //                      RemoveSpecialCharacters(SaudiMaleResponse.englishThirdName) + " " +
            //                      RemoveSpecialCharacters(SaudiMaleResponse.englishLastName);

            ////To remove extra space between name words
            //txtEMemberName.Text = Regex.Replace(txtEMemberName.Text.ToString(), @"\s+", " ");
            //txtEIDExpDate.HijriDate = SaudiMaleResponse.idExpiryDate;
            //txtEDateOfBirth.GregorianDate = SaudiMaleResponse.dateOfBirthG;
            //ddlEGender.SelectedValue = SaudiMaleResponse.gender.ToString();
            //HandleEmployeeGenderTitleFromYaqeen(SaudiMaleResponse.gender.ToString());
            //DisableYaqeenFieldsForMainMember();
            ////lblNumberOfDependentsAr.Text = SaudiMaleResponse.totalNumberOfDependents.ToString();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void DisableYaqeenForEmployeeIncaseYakeenDown()
    {
        hidYakeenEmployee.Value = "N";
        pnlEmpDetails.Visible = true;

        DisableEnableControls();
    }

    private void SendEmailAndUnhandeledErrorMessage(Exception ex, string strID, string strSponsor)
    {
        // Get stack trace for the exception with source file information
        var st = new StackTrace(ex, true);
        // Get the top stack frame
        var frame = st.GetFrame(0);
        // Get the line number from the stack frame
        var line = frame.GetFileLineNumber();

       /* string msgHeader = "Online Service Error report: ";
        sendMail("TSD@bupa.com.sa", msgHeader,
          "\r\nMessage: " + ex.Message
          + "\r\nID and Sponser: " + strID + " , " + strSponsor
          + "\r\nFile: " + HttpContext.Current.Request.PhysicalPath
          + "\r\nLine: " + line.ToString()
          + "\r\nLine: Stack trace:" + ex.StackTrace);*/

    }

    private void DisableYaqeenFieldsForMainMember()
    {
        hidYakeenEmployee.Value = "Y";
        txtEmpHijriYearofBirth.Enabled = false;
        ddlEGender.Enabled = false;
        txtEDateOfBirth.Enabled = false;
        txtEMemberName.Enabled = false;
        txtEIDExpDate.Enabled = false;

        DisableEnableControls();
    }

    private void DisableEnableControls()
    {
        txtSponsorId.Enabled = false;
        ddlEMemberType.Enabled = false;
        ddlEIDType.Enabled = false;
        txtEmployeeID.Enabled = false;
        ddlSponsor.Enabled = false;
        ddlELevelOfCover.Enabled = false;
        btnProceedEmployee.Visible = false;
        ddlNationality.Enabled = false;
        //enable controls
        lblYaqeenError.Visible = true;
        lnkBtnAddNewDep.Visible = true;
    }

    private void SelectSaudiNationalityForSaudiMembers(DropDownList Nationality)
    {
        try
        {
            Nationality.SelectedItem.Text = "Saudi Arabia";
            Nationality.SelectedItem.Value = "966";
            Nationality.Enabled = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region "Yakeen Process Operations for Dependent"

    private void ProceedDependentByNationalID()
    {
        try
        {
            //// Person Service Revamp integration implemented on 02-Nov-2016 By Sakthi
            //// Old Code Start
            //////YakeenWS.citizenInfoByHijriDateOfBirthRequest SaudiMaleRequest = new YakeenWS.citizenInfoByHijriDateOfBirthRequest();
            //////YakeenWS.citizenInfoByHijriDateOfBirthResult SaudiMaleResponse = new YakeenWS.citizenInfoByHijriDateOfBirthResult();

            //////SaudiMaleRequest.userName = strYakeenUsername;
            //////SaudiMaleRequest.password = strYakeenPassword;
            //////SaudiMaleRequest.chargeCode = strYakeenChargeCode;
            //////SaudiMaleRequest.nin = txtDependentID.Text;
            //////SaudiMaleRequest.dateOfBirthHijri = txtDepHijriYearofBirth.Text;
            //////SaudiMaleRequest.referenceNumber = strYakeenRefNo;

            //////SaudiMaleResponse = Yws.getCitizenInfoByHijriDateOfBirth(SaudiMaleRequest);
            //////pnlDependentDetail.Visible = true;
            //////txtMemberName.Text = RemoveSpecialCharacters(SaudiMaleResponse.englishFirstName) + " " +
            //////                      RemoveSpecialCharacters(SaudiMaleResponse.englishSecondName) + " " +
            //////                      RemoveSpecialCharacters(SaudiMaleResponse.englishThirdName) + " " +
            //////                      RemoveSpecialCharacters(SaudiMaleResponse.englishLastName);
            ////////To remove extra space between name words
            //////txtMemberName.Text = Regex.Replace(txtMemberName.Text.ToString(), @"\s+", " ");
            //////txtDateOfBirth.GregorianDate = SaudiMaleResponse.dateOfBirthG;
            //////ddlGender.SelectedValue = SaudiMaleResponse.gender.ToString();
            //////HandleDependentGenderTitleFromYaqeen(SaudiMaleResponse.gender.ToString());
            //////////txtIDExpDate.HijriDate = SaudiMaleResponse.idExpiryDate;
            //////if (!string.IsNullOrEmpty(Convert.ToString(SaudiMaleResponse.idExpiryDateG)))
            //////    txtIDExpDate.GregorianDate = SaudiMaleResponse.idExpiryDateG;

            //////DisableYaqeenFieldsForDependent();
            ////////lblNumberOfDependentsAr.Text = SaudiMaleResponse.totalNumberOfDependents.ToString();
            //// Old Code End
            //// New Code Start

            PersonService pWS = new PersonService();
            Person personInfo = new Person();            
            personInfo = pWS.GetPersonInfo(txtDependentID.Text, txtDepHijriYearofBirth.Text, WebApp.CommonClass.YakeenValidationHours, true, YakeenSearhByTypesYakeenSearchByType.HijriDOB, true, WebApp.CommonClass.FromUserName, WebApp.CommonClass.FromDepartmentName, WebApp.CommonClass.FromSourceName);
            pnlDependentDetail.Visible = true;
            if (personInfo != null && string.IsNullOrEmpty(personInfo.ExceptionMessage))
            {  
                txtMemberName.Text = RemoveSpecialCharacters(personInfo.EnglishFirstName) + " " +
                                      RemoveSpecialCharacters(personInfo.EnglishSecondName) + " " +
                                      RemoveSpecialCharacters(personInfo.EnglishThirdName) + " " +
                                      RemoveSpecialCharacters(personInfo.EnglishLastName);

                //To remove extra space between name words
                txtMemberName.Text = Regex.Replace(txtMemberName.Text.ToString(), @"\s+", " ");
                
                DateTime birthDateGregorian = Convert.ToDateTime(personInfo.BirthDateGregorian);
                DateTime idExpiryDate = Convert.ToDateTime(personInfo.IdExpiryDate);

                if (!string.IsNullOrEmpty(Convert.ToString(birthDateGregorian)))
                {
                    string defaultDate = Convert.ToString(birthDateGregorian);
                    if (defaultDate.Contains("01/01/0001"))
                    { }
                    else
                    txtDateOfBirth.GregorianDate = birthDateGregorian.ToString("dd/MM/yyyy");

                }

                if (!string.IsNullOrEmpty(Convert.ToString(idExpiryDate)))
                {
                    string defaultDate = Convert.ToString(idExpiryDate);
                    if (defaultDate.Contains("01/01/0001"))
                    { }
                    else
                    txtIDExpDate.GregorianDate = idExpiryDate.ToString("dd/MM/yyyy");
                }

                ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(personInfo.Gender.ToString()));    

                HandleDependentGenderTitleFromYaqeen(personInfo.Gender.ToString());               
               
                DisableYaqeenFieldsForDependent();

            }   
            else
            {
                pnlDependentDetail.Visible = false;
                lblYaqeenDepError.Text = personInfo.ExceptionMessage;
                lblYaqeenDepError.Visible = true;
                divDepHijriYearOfBirth.Visible = true;
                return;
            }
           
            //// New Code End

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void ProceedDependentByIqamaNo()
    {
        try
        {
            //// Person Service Revamp integration implemented on 02-Nov-2016 By Sakthi
            //// Old Code Start
            ////////YakeenWS.alienInfoRequest alienInfoRequest = new YakeenWS.alienInfoRequest();
            ////////YakeenWS.alienInfoResult alienInfoResult = new YakeenWS.alienInfoResult();

            ////////alienInfoRequest.userName = strYakeenUsername;
            ////////alienInfoRequest.password = strYakeenPassword;
            ////////alienInfoRequest.chargeCode = strYakeenChargeCode;
            ////////alienInfoRequest.iqamaNumber = txtDependentID.Text;
            ////////alienInfoRequest.sponsorId = txtSponsorId.Text;
            ////////alienInfoRequest.referenceNumber = strYakeenRefNo;

            ////////alienInfoResult = Yws.getAlienInfo(alienInfoRequest);
            ////////pnlDependentDetail.Visible = true;
            ////////txtMemberName.Text = RemoveSpecialCharacters(alienInfoResult.englishFirstName) + " " +
            ////////                      RemoveSpecialCharacters(alienInfoResult.englishSecondName) + " " +
            ////////                      RemoveSpecialCharacters(alienInfoResult.englishThirdName) + " " +
            ////////                      RemoveSpecialCharacters(alienInfoResult.englishLastName);

            //////////To remove extra space between name words
            ////////txtMemberName.Text = Regex.Replace(txtMemberName.Text.ToString(), @"\s+", " ");

            ////////HandleDependentGenderTitleFromYaqeen(alienInfoResult.gender.ToString());

            //////////txtDateOfBirth.Text = alienInfoResult.dateOfBirthG;
            //////////txtDateOfBirth.Text = txtDateOfBirth.Text.Replace("-", @"/");

            ////////txtDateOfBirth.GregorianDate = alienInfoResult.dateOfBirthG;
            ////////////txtIDExpDate.HijriDate = alienInfoResult.iqamaExpiryDateH;
            ///////////txtIDExpDate.GregorianDate = alienInfoResult.iqamaExpiryDateG;
            ////////if (!string.IsNullOrEmpty(Convert.ToString(alienInfoResult.iqamaExpiryDateG)))
            ////////    txtIDExpDate.GregorianDate = alienInfoResult.iqamaExpiryDateG;


            ////////ddlGender.SelectedValue = alienInfoResult.gender.ToString();

            ////////DisableYaqeenFieldsForDependent();
            //// Old Code End
            //// New Code Start
            PersonService pWS = new PersonService();
            Person personInfo = new Person();            
            personInfo = pWS.GetPersonInfo(txtDependentID.Text, txtSponsorId.Text, WebApp.CommonClass.YakeenValidationHours, true, YakeenSearhByTypesYakeenSearchByType.IqamaId, true, WebApp.CommonClass.FromUserName, WebApp.CommonClass.FromDepartmentName, WebApp.CommonClass.FromSourceName);
            pnlDependentDetail.Visible = true;
            if (personInfo != null && string.IsNullOrEmpty(personInfo.ExceptionMessage))
            {
                txtMemberName.Text = RemoveSpecialCharacters(personInfo.EnglishFirstName) + " " +
                                  RemoveSpecialCharacters(personInfo.EnglishSecondName) + " " +
                                  RemoveSpecialCharacters(personInfo.EnglishLastName) + " " +
                                  RemoveSpecialCharacters(personInfo.EnglishLastName);
                txtMemberName.Text = Regex.Replace(txtMemberName.Text.ToString(), @"\s+", " ");
                HandleDependentGenderTitleFromYaqeen(personInfo.Gender.ToString());

                DateTime birthDateGregorian = Convert.ToDateTime(personInfo.BirthDateGregorian);
                DateTime idExpiryDate = Convert.ToDateTime(personInfo.IdExpiryDate);

                if (!string.IsNullOrEmpty(Convert.ToString(birthDateGregorian)))
                {
                    string defaultDate = Convert.ToString(birthDateGregorian);
                    if (defaultDate.Contains("01/01/0001"))
                    { }
                    else
                        txtDateOfBirth.GregorianDate = birthDateGregorian.ToString("dd/MM/yyyy");

                }

                if (!string.IsNullOrEmpty(Convert.ToString(idExpiryDate)))
                {
                    string defaultDate = Convert.ToString(idExpiryDate);
                    if (defaultDate.Contains("01/01/0001"))
                    { }
                    else
                        txtIDExpDate.GregorianDate = idExpiryDate.ToString("dd/MM/yyyy");
                }

                ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(personInfo.Gender.ToString()));             

                DisableYaqeenFieldsForDependent();

            }
            else
            {
                pnlDependentDetail.Visible = false;
                lblYaqeenDepError.Text = personInfo.ExceptionMessage;
                lblYaqeenDepError.Visible = true;
                divDepHijriYearOfBirth.Visible = true;
                return;
            }
           
            //// New Code End
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void ProceedDependentByBorderNo()
    {
        try
        {
            //// Person Service Revamp integration implemented on 02-Nov-2016 By Sakthi
            //// Old Code Start
            ////YakeenWS.alienInfoByBorderNumberRequest alienInfoRequestBorder = new YakeenWS.alienInfoByBorderNumberRequest();
            ////YakeenWS.alienInfoByBorderNumberResult alienInfoResultBorder = new YakeenWS.alienInfoByBorderNumberResult();

            ////alienInfoRequestBorder.userName = strYakeenUsername;
            ////alienInfoRequestBorder.password = strYakeenPassword;
            ////alienInfoRequestBorder.chargeCode = strYakeenChargeCode;
            ////alienInfoRequestBorder.borderNumber = txtDependentID.Text;
            ////alienInfoRequestBorder.sponsorId = txtSponsorId.Text;
            ////alienInfoRequestBorder.referenceNumber = strYakeenRefNo;

            ////alienInfoResultBorder = Yws.getAlienInfoByBorderNumber(alienInfoRequestBorder);
            ////pnlDependentDetail.Visible = true;
            ////txtMemberName.Text = RemoveSpecialCharacters(alienInfoResultBorder.firstName + " " +
            ////                   alienInfoResultBorder.seconedName + " " +
            ////                   alienInfoResultBorder.grandFatherName + " " +
            ////                   alienInfoResultBorder.familyName);
            //////To remove extra space between name words
            ////txtMemberName.Text = Regex.Replace(txtMemberName.Text.ToString(), @"\s+", " ");

            ////txtDateOfBirth.GregorianDate = alienInfoResultBorder.birthDateG;

            ////////txtIDExpDate.HijriDate = alienInfoResultBorder.visaExpiryDate;
            ////if (!string.IsNullOrEmpty(Convert.ToString(alienInfoResultBorder.visaExpiryDate)))
            ////    txtIDExpDate.HijriDate = alienInfoResultBorder.visaExpiryDate;

            ////ddlGender.SelectedValue = alienInfoResultBorder.gender.ToString();

            ////HandleDependentGenderTitleFromYaqeen(alienInfoResultBorder.gender.ToString());

            ////DisableYaqeenFieldsForDependent();
            //// Old Code End
            //// New Code Start
            PersonService pWS = new PersonService();
            Person personInfo = new Person();            
            personInfo = pWS.GetPersonInfo(txtDependentID.Text, txtSponsorId.Text, WebApp.CommonClass.YakeenValidationHours, true, YakeenSearhByTypesYakeenSearchByType.BorderEntry, true, WebApp.CommonClass.FromUserName, WebApp.CommonClass.FromDepartmentName, WebApp.CommonClass.FromSourceName);

            pnlDependentDetail.Visible = true;
            if (personInfo != null && string.IsNullOrEmpty(personInfo.ExceptionMessage))
            {
                txtMemberName.Text = RemoveSpecialCharacters(personInfo.EnglishFirstName + " " +
                                   personInfo.EnglishSecondName + " " +
                                   personInfo.EnglishThirdName + " " +
                                   personInfo.EnglishLastName);
                //To remove extra space between name words
                txtMemberName.Text = Regex.Replace(txtMemberName.Text.ToString(), @"\s+", " ");

                DateTime birthDateGregorian = Convert.ToDateTime(personInfo.BirthDateGregorian);
                DateTime idExpiryDate = Convert.ToDateTime(personInfo.IdExpiryDate);

                if (!string.IsNullOrEmpty(Convert.ToString(birthDateGregorian)))
                {
                    string defaultDate = Convert.ToString(birthDateGregorian);
                    if (defaultDate.Contains("01/01/0001"))
                    { }
                    else
                        txtDateOfBirth.GregorianDate = birthDateGregorian.ToString("dd/MM/yyyy");

                }

                if (!string.IsNullOrEmpty(Convert.ToString(idExpiryDate)))
                {
                    string defaultDate = Convert.ToString(idExpiryDate);
                    if (defaultDate.Contains("01/01/0001"))
                    { }
                    else
                        txtIDExpDate.GregorianDate = idExpiryDate.ToString("dd/MM/yyyy");
                }

                ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(personInfo.Gender.ToString()));        

                HandleDependentGenderTitleFromYaqeen(personInfo.Gender.ToString());

                DisableYaqeenFieldsForDependent();
            }
            else
            {
                pnlDependentDetail.Visible = false;
                lblYaqeenDepError.Text = personInfo.ExceptionMessage;
                lblYaqeenDepError.Visible = true;
                divDepHijriYearOfBirth.Visible = true;
                return;
            }
            //// New Code End

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void DisableYaqeenForDependentYakeenDownOrNewBorn()
    {
        ddlAgeGroup.Enabled = false;
        ddlMemberType.Enabled = false;
        ddlLevelOfCover.Enabled = false;
        ddlIDType.Enabled = false;
        txtDependentID.Enabled = false;
        txtSponsorId.Enabled = false;
        txtDepHijriYearofBirth.Enabled = false;
        pnlDependentDetail.Visible = true;
        btnProceedDep.Visible = false;
        lblYaqeenDepError.Visible = false;
        btnAddDep.Visible = true;
        btnSubmit.Visible = true;
        rfv_ID.Enabled = false;
        rfvDepHijriYearOfBirth.Enabled = false;
        hidYakeenDependent.Value = "N";

        if (ddlIDType.SelectedValue == "1")
        {
            ddlNationality.SelectedItem.Text = "Saudi Arabia";
            ddlNationality.SelectedItem.Value = "966";
            ddlNationality.Enabled = false;
        }
    }

    private void DisableYaqeenFieldsForDependent()
    {
        txtDepHijriYearofBirth.Enabled = false;
        ddlAgeGroup.Enabled = false;
        ddlMemberType.Enabled = false;
        ddlLevelOfCover.Enabled = false;
        ddlIDType.Enabled = false;
        txtDependentID.Enabled = false;
        txtSponsorId.Enabled = false;
        ddlGender.Enabled = false;
        txtDateOfBirth.Enabled = false;
        txtMemberName.Enabled = false;
        if (txtIDExpDate.GregorianDate != null && txtIDExpDate.GregorianDate.ToString() != "00/00/0000" && txtIDExpDate.HijriDate != null && txtIDExpDate.HijriDate.ToString() != "00/00/0000")
            txtIDExpDate.Enabled = false;
        else
            txtIDExpDate.Enabled = true;

        btnProceedDep.Visible = false;
        lblYaqeenDepError.Visible = false;
        btnAddDep.Visible = true;
        btnSubmit.Visible = true;
        hidYakeenDependent.Value = "Y";
    }

    private void SetTheDefaultValuesForSpouse()
    {
        try
        {

            ddlMaritalStatus.SelectedValue = "2";
            ddlMaritalStatus.Enabled = false;
            ddDepProfession.SelectedValue = "99401";
            ddDepProfession.Enabled = false;

            ddlTitle.Items.Clear();

            if (ddlGender.SelectedValue == "F")
            {
                ddlTitle.Items.Add(new ListItem("--Select--", "0"));
                ddlTitle.Items.Add(new ListItem("Mrs", "Mrs"));
                ddlTitle.Items.Add(new ListItem("Ms", "Ms"));
            }
            else if (ddlGender.SelectedValue == "M")
            {
                ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
                ddlTitle.SelectedValue = "Mr";
                ddlTitle.Enabled = false;
            }
            else
            {
                ddlTitle.Items.Add(new ListItem("--Select--", "0"));
                ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
                ddlTitle.Items.Add(new ListItem("Ms", "Ms"));
                ddlTitle.Items.Add(new ListItem("Mrs", "Mrs"));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void SetTheDefaultValuesForChild()
    {
        try
        {
            ddlMaritalStatus.SelectedValue = "1";
            ddlMaritalStatus.Enabled = false;
            ddDepProfession.SelectedValue = "99402";
            ddDepProfession.Enabled = false;

            ddlTitle.Items.Clear();
            if (ddlGender.SelectedValue == "F")
            {
                ddlTitle.Items.Add(new ListItem("Miss", "Miss"));
                ddlTitle.SelectedValue = "Miss";
                ddlTitle.Enabled = false;
            }
            else if (ddlGender.SelectedValue == "M")
            {
                ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
                ddlTitle.SelectedValue = "Mr";
                ddlTitle.Enabled = false;
            }
            else
            {
                ddlTitle.Items.Add(new ListItem("--Select--", "0"));
                ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
                ddlTitle.Items.Add(new ListItem("Miss", "Miss"));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region "Gridview for dependents events"

    protected void gvDependents_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int categoryID = e.RowIndex;
        if (isRowDeleted != true) DeleteRow(categoryID);
        btnAddDep.Visible = true;
        btnUpdateDep.Visible = false;
        btnCancelUpdateDep.Visible = false;
        gvDependents.DataSource = DT;
        gvDependents.DataBind();
        GetAllDependentsName();
    }

    protected void gvDependents_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            param = Convert.ToInt32(e.CommandArgument);
            DeleteRow(param);

            if (DependentFlag == true)
            {
                int DepCounter = Int32.Parse(txtFlagForAddingDepValidation.Text) - 1;
                txtFlagForAddingDepValidation.Text = DepCounter.ToString();

                if (gvDependents.Rows.Count > 0)
                {
                    btnSubmit.Visible = true;
                }
                else
                {
                    btnSubmit.Visible = false;
                }
            }
        }
    }

    protected void gvDependents_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference((System.Web.UI.Control)sender, "Select$" + e.Row.RowIndex.ToString()));
    }

    protected void gvDependents_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        int selectIndex = gvDependents.SelectedIndex;
        GetSelectedDependentInfo_AndBindToForm(selectIndex);
        Session["Parm"] = selectIndex;
        param = (int)Session["Parm"];
        btnUpdateDep.Visible = true;
        btnCancelUpdateDep.Visible = true;

        //btnSubmit.Visible = false;
        btnProceedDep.Visible = false;
        lblYaqeenDepError.Visible = false;
        pnlDependentDetail.Visible = true;

        btnAddDep.Visible = false;
        gvDependents.DataSource = DT;
        gvDependents.DataBind();
        GetAllDependentsName();
    }

    public void DeleteRow(int RowIndex)
    {
        DT = (DataTable)ViewState["dt"];
        DT.Rows[RowIndex].Delete();
        isRowDeleted = true;
        gvDependents.SelectedIndex = -1;
        btnUpdateDep.Visible = false;
        btnAddDep.Visible = true;
        lnkBtnAddNewDep.Visible = true;
        Reset_All_Input_Fields();
        gvDependents.DataSource = DT;
        gvDependents.DataBind();

        GetAllDependentsName();
    }

    public void GetSelectedDependentInfo_AndBindToForm(int intIndex)
    {
        pnlDepemdentMain.Visible = true;
        DT = (DataTable)ViewState["dt"];
        ddlTitle.SelectedValue = (String)DT.Rows[intIndex][0];
        txtMemberName.Text = (String)DT.Rows[intIndex][1];
        txtDateOfBirth.GregorianDate = (String)DT.Rows[intIndex][2];
        txtSponsorId.Text = (String)DT.Rows[intIndex][3];
        txtEmployeeNo.Text = (String)DT.Rows[intIndex][4];
        txtDependentID.Text = (String)DT.Rows[intIndex][5];
        if (txtDependentID.Text == "")
        {
            rfvDepHijriYearOfBirth.Visible = false;
        }
        else
        {
            rfvDepHijriYearOfBirth.Visible = false;
        }
        txtDepartmentCode.Text = (String)DT.Rows[intIndex][6];
        ddlGender.SelectedValue = (String)DT.Rows[intIndex][7];    
        ddlLevelOfCover.SelectedValue = (String)DT.Rows[intIndex][9];
        ddlJoinReason.SelectedValue = (String)DT.Rows[intIndex][10];
        txtJoiningDate.GregorianDate = (String)DT.Rows[intIndex][12];
        txtRequestedStartDate.GregorianDate = (String)DT.Rows[intIndex][13];
        txtMobileNo.Text = (String)DT.Rows[intIndex][14];
        ddlMemberType.SelectedValue = (String)DT.Rows[intIndex][15];
        ddlIDType.SelectedValue = (String)DT.Rows[intIndex][16];
       
        ddlMaritalStatus.SelectedValue = (String)DT.Rows[intIndex][18];
        ddDepProfession.SelectedValue = (String)DT.Rows[intIndex][19];
        ddDepDistrict.SelectedValue = (String)DT.Rows[intIndex][20];
        hidYakeenDependent.Value = (String)DT.Rows[intIndex][21];
        txtDepHijriYearofBirth.Text = (string)DT.Rows[intIndex]["DOBHijriYear"];

        //// CR304 disabled by sakthi on 01-Sep-2016
        //// IQAMA/Border Entry ID should not be mandatory for Less than or equal 1 year
        ////ddlNationality.SelectedValue = (String)DT.Rows[intIndex][8];
        ddlNationality.SelectedIndex = ddlNationality.Items.IndexOf(ddlNationality.Items.FindByText(Convert.ToString(DT.Rows[intIndex]["NationalityName"])));
        ViewState["AgeGroup"] = Convert.ToString(DT.Rows[intIndex]["NewBornChild"]);
        if (Convert.ToString(ViewState["AgeGroup"]) != "1")
            txtIDExpDate.GregorianDate = (String)DT.Rows[intIndex][17];
        else
            txtIDExpDate.Clear();
        
       
       
        //////string ageSelection = Convert.ToString(DT.Rows[intIndex]["NewBornChild"]);
      
        //////bool isVisible = false;
        //////bool isIDValid = false;
        //////isVisible = ddlMemberType.SelectedValue == "C" || ddlMemberType.SelectedValue == "SO" || ddlMemberType.SelectedValue == "D" ? true : false;
       
       
        //////isIDValid = isVisible ? ageSelection == "2" ? true : false : true;
        //////if (isVisible)
        //////{
        //////    divAgeGroup.Attributes.Add("style", "dispaly:'block'");
        //////    ddlAgeGroup.SelectedValue = Convert.ToString(ageSelection);
        //////    rfv_ddlAgeGroup.IsValid = true;
        //////    rfv_ddlAgeGroup.Enabled = true;
        //////}
        //////else
        //////{
        //////    divAgeGroup.Attributes.Add("style", "dispaly:'none'");
        //////    ddlAgeGroup.ClearSelection();
        //////    rfv_ddlAgeGroup.IsValid = false;
        //////    rfv_ddlAgeGroup.Enabled = false;
        //////}

        //////if (isIDValid)
        //////{
        //////    rfv_ID.Enabled = true;
        //////    rfv_ID.IsValid = true;
        //////}
        //////else
        //////{
        //////    rfv_ID.Enabled = false;
        //////    rfv_ID.IsValid = false;
        //////}

        ////

    }

    #endregion

    #region "Reset Fields, Check age, Capitalise, Check Name, Gender Title, charcter count and Remove Special Characters"

    public static string RemoveSpecialCharacters(string str)
    {
        StringBuilder sb = new StringBuilder();
        if (!string.IsNullOrWhiteSpace(str))
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c == ' '))
                {
                    sb.Append(c);
                }
            }
        string strCaptalize = Capitalise(sb.ToString().ToLower());
        return strCaptalize.Trim();
    }

    public static string Capitalise(string value)
    {
        char[] array = value.ToCharArray();
        if (array.Length >= 1)
        {
            if (char.IsLower(array[0]))
            {
                array[0] = char.ToUpper(array[0]);
            }
        }
        for (int i = 1; i < array.Length; i++)
        {
            if (array[i - 1] == ' ')
            {
                if (char.IsLower(array[i]))
                {
                    array[i] = char.ToUpper(array[i]);
                }
            }
        }
        return new string(array);
    }

    public void sendMail(String toemail, string strMailSubject, string strMailBody)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(toemail);
            mail.From = new MailAddress("donotreply@bupa.com.sa");
            mail.Subject = strMailSubject;
            mail.Body = strMailBody;
            SmtpClient smtp = new SmtpClient();
            smtp.UseDefaultCredentials = false;
            smtp.Send(mail);
            mail.Dispose();
        }
        catch (SmtpException ex)
        {
            throw ex;
        }
    }

    protected bool CheckUnderAge(DateTime _bDate)
    {
        try
        {
            bool _Underage = false;
            DateTime _current = DateTime.Today;
            TimeSpan span = _current.Subtract(_bDate);
            int days = span.Days;
            ////if (days <= 90)
            if (days <= 365)
            {
                _Underage = true;
            }
            else
            {
                _Underage = false;
            }
            return _Underage;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void Reset_All_Input_Fields()
    {
        txtMemberName.Text = "";
        txtDateOfBirth.Clear();
        txtEmployeeNo.Text = "";
        txtDependentID.Text = "";
        txtDepartmentCode.Text = "";
        txtDepHijriYearofBirth.Text = "";
        ddlGender.SelectedIndex = 0;
        ddlNationality.SelectedIndex = -1;
        ddlLevelOfCover.SelectedIndex = 0;
        ddlJoinReason.SelectedIndex = 0;
        txtJoiningDate.Clear();
        txtRequestedStartDate.Clear();
        txtMobileNo.Text = "";
        ddlMemberType.SelectedIndex = 0;
        ddlAgeGroup.SelectedIndex = 0;
        divAgeGroup.Attributes.Add("style", "display:none");
        ddlIDType.SelectedIndex = 0;
        txtIDExpDate.Clear();
        ddlMaritalStatus.SelectedIndex = 0;
        ddDepProfession.SelectedIndex = 0;
        ddDepDistrict.SelectedIndex = 0;
        gvDependents.DataBind();
       
    }

    bool Check_Member_belong_to_client(string strMemberContractNo)
    {
        if (strClientID == strMemberContractNo)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void HandleEmployeeGenderTitleFromYaqeen(DropDownList ddl, string strGender)
    {
        ddl.Enabled = false;

        if (ddl.Items.Count < 2)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("0", "--Select--"));
            ddl.Items.Add(new ListItem("M", "Male"));
            ddl.Items.Add(new ListItem("F", "Female"));
        }

        if (strGender == "M" || strGender.StartsWith("M"))
        {
            ddlTitle.SelectedIndex = 1;
            ddlTitle.Enabled = false;
            //ddl.Items.RemoveAt(1);
            ddl.SelectedIndex = 1;
        }
        else if (strGender == "F" || strGender.StartsWith("F"))
        {
            ddlTitle.SelectedIndex = 2;
            ddlTitle.Enabled = false;
            //ddl.Items.RemoveAt(0);
            ddl.SelectedIndex = 2;
        }
        else
        {
            ddlTitle.SelectedIndex = 0;
            ddl.SelectedIndex = 0;
            ddl.Enabled = true;
        }
    }

    public void HandleDependentGenderTitleFromYaqeen(string strGender)
    {
        //// Person Service Revamp integration implemented on 02-Nov-2016 By Sakthi
        //// Old Code Start
        ////if (strGender == "M")
        ////{
        ////    ddlTitle.Items.Remove(ddlTitle.Items.FindByValue("Miss"));
        ////    ddlTitle.Items.Remove(ddlTitle.Items.FindByValue("Mrs"));
        ////    ddlTitle.Items.Remove(ddlTitle.Items.FindByValue("Ms"));
        ////    ddlTitle.SelectedIndex = 1;
        ////    ddlTitle.Enabled = false;
        ////}
        ////else if (strGender == "F")
        ////{
        ////    ddlTitle.Items.Remove(ddlTitle.Items.FindByValue("Mr"));
        ////}
        //// Old Code End
        //// New Code Start
        if (strGender.ToUpper().StartsWith("M"))
        {
            ddlTitle.Items.Remove(ddlTitle.Items.FindByValue("Miss"));
            ddlTitle.Items.Remove(ddlTitle.Items.FindByValue("Mrs"));
            ddlTitle.Items.Remove(ddlTitle.Items.FindByValue("Ms"));
            ddlTitle.SelectedIndex = 1;
            ddlTitle.Enabled = false;
        }
        else if (strGender.ToUpper().StartsWith("F"))
        {
            ddlTitle.Items.Remove(ddlTitle.Items.FindByValue("Mr"));
        }
        //// New Code End
    }

    private bool CheckNameCharcterCount(TextBox txtName, Literal litError, DropDownList ddlId)
    {
        //we developed this function because some names come from yaqeen exceed 40 charcter
        //Caesar allow only 40 character maximum, we allow the user to edit
        if (txtName.Text.Trim().Length > 40)
        {
            litError.Visible = true;
            litError.Text = "<span style='color:red'>The name exceeded the accepted number of characters “40 characters”." +
                                "Please modify the name in order to process the request <br />" + Environment.NewLine +
                            "عدد أحرف الاسم تخطت الحد المسموح 40 حرف الرجاء تعديل الاسم بحيث لا يتخطى الحد المسموح" +
                            " </span>";
            txtName.Enabled = true;
            txtName.Focus();
            return true;
        }
        else if (txtName.Text.Trim().Length == 0 && ddlId.SelectedValue == "0")
        {
            //this is mean add dependent request
            return false;
        }
        else if (txtName.Text.Trim().Length == 0 && ddlId.SelectedValue != "1" && blnYakeenDisabled == false)
        {
            litError.Visible = true;
            litError.Text = "<span style='color:red'>The English name is not registered on NIC system. Please write the name and make sure it doesn’t exceed 40 digits<br />" +
                                "الاسم الانجليزي غير مسجل لدى مركزالمعلومات الوطني. الرجاء كتابة الاسم والتأكد بأن لا يتجاوز عدد 40 حرف" +
                            " </span>";
            txtName.Enabled = true;
            txtName.Focus();
            return true;
        }
        else if (txtName.Text.Trim().Length == 0 && blnYakeenDisabled == false)
        {
            litError.Visible = true;
            litError.Text = "<span style='color:red'>The English name is not registered on NIC system. Please write the name and make sure it doesn’t exceed 40 digits<br />" +
                                "الاسم الانجليزي غير مسجل لدى مركزالمعلومات الوطني. الرجاء كتابة الاسم والتأكد بأن لا يتجاوز عدد 40 حرف" +
                            " </span>";
            txtName.Enabled = true;
            txtName.Focus();
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion

    #region "Bind the dropdown lists with values"

    private void LoadCaesarSourcedData()
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {

            BindDdlProfession();
            BindDdlDistrict();
            BindDdlENationality(false);
            BindDdlNationality(false);

            EnqSponsorIDListDtl_DN[] sponsorIdsList = GetSponsorList(strClientID);
            BindDdlSponsorList(sponsorIdsList, ddlSponsor);
            //BindDdlSponsorList(sponsorIdsList, ddlDepSponsor);

        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }
    private EnqSponsorIDListDtl_DN[] GetSponsorList(string contNo)
    {
        EnqSponsorIDListRequest_DN request = new EnqSponsorIDListRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.contNo = contNo;

        ws = new ServiceDepot_DNService();
        EnqSponsorIDListResponse_DN response;
        response = ws.EnqSponsorIDList(request);

        ////ClassXMLGeneration<EnqSponsorIDListRequest_DN> XmlReq = new ClassXMLGeneration<EnqSponsorIDListRequest_DN>();
        ////XmlReq.Request(request, "EnqContMbrTypeList_Request");

        ////ClassXMLGeneration<EnqSponsorIDListResponse_DN> XmlResp = new ClassXMLGeneration<EnqSponsorIDListResponse_DN>();
        ////XmlResp.Response(response, "EnqContMbrTypeList_Response");

        return response.detail;
    }
    private void BindDdlSponsorList(EnqSponsorIDListDtl_DN[] list, RadComboBox ddl)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            ListItem ListItem0 = new ListItem();
            ListItem0.Text = "-Select-";
            ListItem0.Value = "0";

            ddl.DataSource = list;
            ddl.DataBind();
            //RadComboBoxItem rcbi = new RadComboBoxItem("Add new", "Add New");
            //ddl.Items.Insert(0, rcbi);
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }

        //ddl.Items.Insert(0, ListItem0);
    }
    private void BindDdlMemberType()
    {
        ListItem ListItem0 = new ListItem();
        ListItem0.Text = "-Select-";
        ListItem0.Value = "0";
        ReqMbrTypeListRequest_DN request = new ReqMbrTypeListRequest_DN();
        request.TransactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.grp_Type = "E";

        ws = new ServiceDepot_DNService();
        ReqMbrTypeListResponse_DN response;
        response = ws.ReqMbrTypeList(request);

        ddlEMemberType.DataSource = response.detail;
        ddlEMemberType.DataBind();
        ddlEMemberType.Items.Insert(0, ListItem0);

        request.grp_Type = "D";
        response = ws.ReqMbrTypeList(request);

        ddlMemberType.DataSource = response.detail;
        ddlMemberType.DataBind();
        ddlMemberType.Items.Insert(0, ListItem0);
    }
    private void BindDdlProfession()
    {
        ReqProfListRequest_DN request = new ReqProfListRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        ws = new ServiceDepot_DNService();
        ReqProfListResponse_DN response;
        response = ws.ReqProfList(request);
        ddProfession.DataSource = response.detail;
        ddProfession.DataValueField = "profCode";
        ddProfession.DataTextField = "profName";
        ddProfession.DataBind();
    }
    private void BindDdlDistrict()
    {
        ReqDistListRequest_DN request = new ReqDistListRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        ws = new ServiceDepot_DNService();
        ReqDistListResponse_DN response;
        response = ws.ReqDistList(request);
        DistListDetail_DN[] MyDistricts = new DistListDetail_DN[response.detail.Length + 1];
        response.detail.CopyTo(MyDistricts, 1);
        MyDistricts[0] = new DistListDetail_DN();
        MyDistricts[0].distCode = "";
        MyDistricts[0].distName = "-Select-";
        ddDistrict.DataSource = MyDistricts;
        ddDistrict.DataBind();
        ddDepDistrict.DataSource = MyDistricts;
        ddDepDistrict.DataBind();
    }
    private void BindTitleDDL()
    {
        ddlTitle.Items.Clear();
        ddlTitle.Items.Add(new ListItem("--Select--", "0"));
        ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
        ddlTitle.Items.Add(new ListItem("Miss", "Miss"));
        ddlTitle.Items.Add(new ListItem("Mrs", "Mrs"));
        ddlTitle.Items.Add(new ListItem("Ms", "Ms"));
    }
    private void BindDdlENationality(bool IsGCC)
    {
        NationalityControl MyNationalityC = new NationalityControl();
        List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
        MyNationalities.Insert(0, new Nationality(0, "0", "-- Select --", false));
        ddlENationality.DataValueField = "Code";
        ddlENationality.DataTextField = "Nationality1";
        ddlENationality.DataSource = MyNationalities;
        ddlENationality.DataBind();
    }
    private void BindDdlNationality(bool IsGCC)
    {
        NationalityControl MyNationalityC = new NationalityControl();
        List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);

        MyNationalities = MyNationalities.OrderBy(q => q.Nationality1).ToList();

        MyNationalities.Insert(0, new Nationality(0, "0", "-- Select --", false));
        ddlNationality.DataValueField = "Code";
        ddlNationality.DataTextField = "Nationality1";
        ddlNationality.DataSource = MyNationalities;
        ddlNationality.DataBind();
    }
    private void DisplayBranchListResult(ReqBrhListResponse_DN response)
    {
        if (response.status == "0")
        {
            ddlEBranchCode.Items.Clear();
            ListItem DepList0 = new ListItem();
            DepList0.Text = "-- Select --";
            DepList0.Value = "0";
            ddlEBranchCode.Items.Add(DepList0);
            foreach (BrhListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.branchDesc;
                DepList.Value = dtl.branchCode.ToString();
                ddlEBranchCode.Items.Add(DepList);
            }
        }
        else
        {
            litMainError.Visible = true;
            litMainError.Text = "<span style='color:red'>Please refresh the page (Ctrl+F5)</span>";
        }
    }
    protected void PopulateBranchList(string ClientID)
    {
        {
            ws = new ServiceDepot_DNService();
            ReqBrhListRequest_DN request = new ReqBrhListRequest_DN();
            ReqBrhListResponse_DN response;
            request.membershipNo = long.Parse(strClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();

            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.ReqBrhList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                }
                else
                {
                    DisplayBranchListResult(response);
                }
            }
            catch (Exception ex)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = ex.Message;
            }
        }
    }

    private void DisplayClassListResult(ReqClsListResponse_DN response)
    {
        if (response.status == "0")
        {
            ddlLevelOfCover.Items.Clear();
            ddlELevelOfCover.Items.Clear();
            ListItem DepList0 = new ListItem();
            DepList0.Text = "-Select-";
            DepList0.Value = "0";
            ddlLevelOfCover.Items.Add(DepList0);
            ddlELevelOfCover.Items.Add(DepList0);
            foreach (ClsListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.className;
                DepList.Value = dtl.classID.ToString();
                ddlLevelOfCover.Items.Add(DepList);
                ddlELevelOfCover.Items.Add(DepList);
            }
        }
        else
        {
        }
    }
    protected void PopulateClassList(string ClientID)
    {
        {
            ws = new ServiceDepot_DNService();
            ReqClsListRequest_DN request = new ReqClsListRequest_DN();
            ReqClsListResponse_DN response;
            //request.membershipNo = long.Parse(strClientID);
            request.contractNo = long.Parse(strClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();
            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.ReqClsList(request);

                ////ClassXMLGeneration<ReqClsListRequest_DN> XmlReq = new ClassXMLGeneration<ReqClsListRequest_DN>();
                ////XmlReq.Request(request, "ReqClsList_Request");



                ////ClassXMLGeneration<ReqClsListResponse_DN> XmlResp = new ClassXMLGeneration<ReqClsListResponse_DN>();
                ////XmlResp.Response(response, "ReqClsList_Response");

                if (response.status != "0")
                {
                    sb.Append(response.errorMessage[0]).Append("\n");
                    Message1.Text = sb.ToString();
                }
                else
                {
                    DisplayClassListResult(response);
                }
            }
            catch (Exception ex)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = ex.Message;
            }

            if (ddlLevelOfCover.Items.Count > 0)
            {

            }
        }
    }
    // ideally you should keep this into any utility class and call it from the page_load event
    public static string DisableTheButton(Control pge, Control btn)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') {");
        sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
        sb.Append("this.value = 'Please wait...';");
        sb.Append("this.disabled = true;");
        sb.Append(pge.Page.GetPostBackEventReference(btn));
        sb.Append(";");
        return sb.ToString();
    }

    #endregion

    #region "Submit Button Functions and Procedures"

    private void SubmitRequestForAddNewMode()
    {
        try
        {
            int intDependentErrorCount = 0;
            string strDependentErrorMessage = "";
            int intDepCount = 0;
            if (ViewState["dt"] != null)
            {
                DT = (DataTable)ViewState["dt"];
                intDepCount = DT.Rows.Count;
            }

            OS_DXC_WAP.avayaWS.Service1 _avaya = new OS_DXC_WAP.avayaWS.Service1();

            SubTxnRequest_DN request = new SubTxnRequest_DN();
            GenerateThBasicRequestInfo(ref request);

            if (DependentFlag == false)
            {
                request.detail = new AllMbrDetail_DN[intDepCount + 1];
                if (CheckIdentityAlreadyRegisteredForMainMember(_avaya, txtEmployeeID.Text))
                    goto exitSubmit;
                GenerateTheRequestObjectForMainMember(ref request);
            }
            else
                request.detail = new AllMbrDetail_DN[intDepCount];

            for (int i = 0; i < intDepCount; i++)
            {
                CheckIdentityAlreadyRegisteredForDependent(_avaya, (String)DT.Rows[i][5], i, ref strDependentErrorMessage, ref intDependentErrorCount);
                GenerateTheRequestObjectForDependent(ref request, i);
            }
            if (intDependentErrorCount > 0)
            {

                litDepMainError.Text = "<span style='color:red'>" + strDependentErrorMessage + "</span>";
                return;
            }

            if (DependentFlag == false)
                request.detail[0].NoPeopleJoin = intDepCount + 1;
            else
                request.detail[0].NoPeopleJoin = intDepCount;

            ws = new ServiceDepot_DNService();
            SubTxnResponse_DN response;



            response = ws.SubTxn02(request);
            /////response = new SubTxnResponse_DN();


            ////Convert object to Xml 
            ////If webservice is not responding fine, then send the request & response xml to Caesar team 
            ////Below code need to enable, it will saving the request & rsponse xml in specifeied path
            ////Strat
            //ClassXMLGeneration<SubTxnRequest_DN> XmlReq = new ClassXMLGeneration<SubTxnRequest_DN>();
            //XmlReq.Request(request, "EnqContMbrTypeList_Request");



            //ClassXMLGeneration<SubTxnResponse_DN> XmlResp = new ClassXMLGeneration<SubTxnResponse_DN>();
            //XmlResp.Response(response, "EnqContMbrTypeList_Response");
            ////End

            if (response.Status != "0")
                SubmitRequestGenerateErrorFromCaesar(response);
            else
            {
                ShowSuccessfullySubmitted(response.ReferenceNo.ToString());
                pnlDependentDetail.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    exitSubmit: ;
    }

    private void ReSubmitRequestViewMode()
    {
        try
        {
            int intDependentErrorCount = 0;
            string strDependentErrorMessage = "";
            int intDepCount = 0;
            if (ViewState["dt"] != null)
            {
                DT = (DataTable)ViewState["dt"];
                intDepCount = DT.Rows.Count;
            }

            OS_DXC_WAP.avayaWS.Service1 _avaya = new OS_DXC_WAP.avayaWS.Service1();
            AllMbrDetail_DN[] allmbrdtl;
            ReSubTxnRequest_DN request = new ReSubTxnRequest_DN();
            GenerateThBasicRequestViewMode(ref request);

            if (DependentFlag == false)
            {
                allmbrdtl = new AllMbrDetail_DN[intDepCount + 1];
                if (CheckIdentityAlreadyRegisteredForMainMember(_avaya, txtEmployeeID.Text))
                    goto exitSubmit;
                GenerateTheRequestObjectForMainMemberViewMode(ref allmbrdtl);
            }
            else
                allmbrdtl = new AllMbrDetail_DN[intDepCount];

            for (int i = 0; i < intDepCount; i++)
            {
                CheckIdentityAlreadyRegisteredForDependent(_avaya, (String)DT.Rows[i][5], i, ref strDependentErrorMessage, ref intDependentErrorCount);
                GenerateTheRequestObjectForDependentViewMode(ref allmbrdtl, i);
            }
            if (intDependentErrorCount > 0)
            {
                litDepMainError.Text = "<span style='color:red'>" + strDependentErrorMessage + "</span>";
                return;
            }

            if (DependentFlag == false)
                allmbrdtl[0].NoPeopleJoin = intDepCount + 1;
            else
                allmbrdtl[0].NoPeopleJoin = intDepCount;

            ws = new ServiceDepot_DNService();
            ReSubTxnResponse_DN response;

            request.mbrDetail = allmbrdtl;
            response = ws.ReSubTxn(request);
            ////response = new ReSubTxnResponse_DN();

            if (response.Status != "0")
                SubmitRequestGenerateErrorFromCaesarViewMode(response);
            else
            {
                ShowSuccessfullySubmitted(response.refNo.ToString());
                pnlDependentDetail.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    exitSubmit: ;
    }

    private void AssignSurveyQuestionsValues(ref SubTxnRequest_DN request, int j)
    {
        request.detail[j].Question1 = rdbQ1.Text;
        request.detail[j].Question1Case = txtQ1Box1.Text;
        request.detail[j].Question1Name = txtQ1Box2.Text;
        request.detail[j].Question2 = rdbQ2.Text;
        request.detail[j].Question2Case = txtQ2Box1.Text;
        request.detail[j].Question2Name = txtQ2Box2.Text;
        request.detail[j].Question3 = rdbQ3.Text;
        request.detail[j].Question3Case = txtQ3Box1.Text;
        request.detail[j].Question3Name = txtQ3Box2.Text;
        request.detail[j].Question4 = rdbQ4.Text;
        request.detail[j].Question4Case = txtQ4Box1.Text;
        request.detail[j].Question4Name = txtQ4Box2.Text;
        request.detail[j].Question5 = rdbQ5.Text;
        request.detail[j].Question5Case = txtQ5Box1.Text;
        request.detail[j].Question5Name = txtQ5Box2.Text;
        request.detail[j].Question6 = rdbQ6.Text;
        request.detail[j].Question6Case = txtQ6Box1.Text;
        request.detail[j].Question6Name = txtQ6Box2.Text;
        request.detail[j].Question7 = rdbQ7.Text;
        request.detail[j].Question7Case = txtQ7Box1.Text;
        request.detail[j].Question7Name = txtQ7Box2.Text;
        request.detail[j].Question8 = rdbQ8.Text;
        request.detail[j].Question8Case = txtQ8Box1.Text;
        request.detail[j].Question8Name = txtQ8Box2.Text;
        request.detail[j].Question9 = rdbQ9.Text;
        request.detail[j].Question9Case = txtQ9Box1.Text;
        request.detail[j].Question9Name = txtQ9Box2.Text;
        request.detail[j].Question10 = rdbQ10.Text;
        request.detail[j].Question10Case = txtQ10Box1.Text;
        request.detail[j].Question10Name = txtQ10Box2.Text;
        request.detail[j].Question11 = rdbQ11.Text;
        request.detail[j].Question11Case = txtQ11Box1.Text;
        request.detail[j].Question11Name = txtQ11Box2.Text;
        request.detail[j].Question12 = rdbQ12.Text;
        request.detail[j].Question12Case = txtQ12Box1.Text;
        request.detail[j].Question12Name = txtQ12Box2.Text;
        request.detail[j].Question13 = rdbQ13.Text;
        request.detail[j].Question13Case = txtQ13Box1.Text;
        request.detail[j].Question13Name = txtQ13Box2.Text;
        request.detail[j].Question14 = rdbQ14.Text;
        request.detail[j].Question14Case = txtQ14Box1.Text;
        request.detail[j].Question14Name = txtQ14Box2.Text;
        request.detail[j].Question15 = rdbQ15.Text;
        request.detail[j].Question15Case = txtQ15Box1.Text;
        request.detail[j].Question15Name = txtQ15Box2.Text;
        request.detail[j].Question16 = rdbQ16.Text;
        request.detail[j].Question16Case = txtQ16Box1.Text;
        request.detail[j].Question16Name = txtQ16Box2.Text;
        request.detail[j].Question17 = rdbQ17.Text;
        request.detail[j].Question17Case = txtQ17Box1.Text;
        request.detail[j].Question17Name = txtQ17Box2.Text;
        request.detail[j].Question18 = rdbQ18.Text;
        request.detail[j].Question18Case = txtQ18Box1.Text;
        request.detail[j].Question18Name = txtQ18Box2.Text;
        request.detail[j].Question19 = rdbQ19.Text;
        request.detail[j].Question19Case = txtQ19Box1.Text;
        request.detail[j].Question19Name = txtQ19Box2.Text;

    }

    private void AssignSurveyQuestionsValuesViewMode(ref AllMbrDetail_DN[] request, int j)
    {
        request[j].Question1 = rdbQ1.Text;
        request[j].Question1Case = txtQ1Box1.Text;
        request[j].Question1Name = txtQ1Box2.Text;
        request[j].Question2 = rdbQ2.Text;
        request[j].Question2Case = txtQ2Box1.Text;
        request[j].Question2Name = txtQ2Box2.Text;
        request[j].Question3 = rdbQ3.Text;
        request[j].Question3Case = txtQ3Box1.Text;
        request[j].Question3Name = txtQ3Box2.Text;
        request[j].Question4 = rdbQ4.Text;
        request[j].Question4Case = txtQ4Box1.Text;
        request[j].Question4Name = txtQ4Box2.Text;
        request[j].Question5 = rdbQ5.Text;
        request[j].Question5Case = txtQ5Box1.Text;
        request[j].Question5Name = txtQ5Box2.Text;
        request[j].Question6 = rdbQ6.Text;
        request[j].Question6Case = txtQ6Box1.Text;
        request[j].Question6Name = txtQ6Box2.Text;
        request[j].Question7 = rdbQ7.Text;
        request[j].Question7Case = txtQ7Box1.Text;
        request[j].Question7Name = txtQ7Box2.Text;
        request[j].Question8 = rdbQ8.Text;
        request[j].Question8Case = txtQ8Box1.Text;
        request[j].Question8Name = txtQ8Box2.Text;
        request[j].Question9 = rdbQ9.Text;
        request[j].Question9Case = txtQ9Box1.Text;
        request[j].Question9Name = txtQ9Box2.Text;
        request[j].Question10 = rdbQ10.Text;
        request[j].Question10Case = txtQ10Box1.Text;
        request[j].Question10Name = txtQ10Box2.Text;
        request[j].Question11 = rdbQ11.Text;
        request[j].Question11Case = txtQ11Box1.Text;
        request[j].Question11Name = txtQ11Box2.Text;
        request[j].Question12 = rdbQ12.Text;
        request[j].Question12Case = txtQ12Box1.Text;
        request[j].Question12Name = txtQ12Box2.Text;
        request[j].Question13 = rdbQ13.Text;
        request[j].Question13Case = txtQ13Box1.Text;
        request[j].Question13Name = txtQ13Box2.Text;
        request[j].Question14 = rdbQ14.Text;
        request[j].Question14Case = txtQ14Box1.Text;
        request[j].Question14Name = txtQ14Box2.Text;
        request[j].Question15 = rdbQ15.Text;
        request[j].Question15Case = txtQ15Box1.Text;
        request[j].Question15Name = txtQ15Box2.Text;
        request[j].Question16 = rdbQ16.Text;
        request[j].Question16Case = txtQ16Box1.Text;
        request[j].Question16Name = txtQ16Box2.Text;
        request[j].Question17 = rdbQ17.Text;
        request[j].Question17Case = txtQ17Box1.Text;
        request[j].Question17Name = txtQ17Box2.Text;
        request[j].Question18 = rdbQ18.Text;
        request[j].Question18Case = txtQ18Box1.Text;
        request[j].Question18Name = txtQ18Box2.Text;
        request[j].Question19 = rdbQ19.Text;
        request[j].Question19Case = txtQ19Box1.Text;
        request[j].Question19Name = txtQ19Box2.Text;

    }

    private void ShowSuccessfullySubmitted(string strRefNo)
    {
        try
        {
            Message1.ForeColor = System.Drawing.Color.Black;
            Message1.Text = "<br/><br/><br/>Thank you for submitting your request. We are now validating the submitted file." +
                "Your reference no. is " + strRefNo + ".<br/>";
            imgSuc.Visible = true;
            lnkBtnAddNewDep.Visible = false;
            Label18.Visible = false;
            PanelEmployee.Visible = false;
            PanelDeclaration.Visible = false;
            PanelMembershipNoOnly.Visible = false;
            pnlDepemdentMain.Visible = false;
            gvDependents.Visible = false;
            btnSubmit.Visible = false;
            tblUploader.Visible = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void SubmitRequestGenerateErrorFromCaesar(SubTxnResponse_DN response)
    {
        try
        {
            StringBuilder sb = new StringBuilder(200);
            Message1.Text = "";
            if (!string.IsNullOrEmpty(Convert.ToString(response.errorMessage)))
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");
                Message1.Text += sb.ToString();
                Message1.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void SubmitRequestGenerateErrorFromCaesarViewMode(ReSubTxnResponse_DN response)
    {
        try
        {
            StringBuilder sb = new StringBuilder(200);
            Message1.Text = "";
            if (!string.IsNullOrEmpty(Convert.ToString(response.errorMessage)))
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");
                Message1.Text += sb.ToString();
                Message1.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private bool CheckIdentityAlreadyRegisteredForMainMember(OS_DXC_WAP.avayaWS.Service1 svcAvaya, string strIDNo)
    {
        try
        {
            string strCount;
            strCount = svcAvaya.GetmbrCountByID(strIDNo, Convert.ToString(Session["ClientID"]));
            if (Convert.ToInt32(strCount) > 0)
            {
                litMainError.Visible = true;
                litMainError.Text = "<span style='color:red'>ID/Iqama number already registered on the same contract in our system<br> رقم الهوية/ الاقامة مسجل مسبقاً " + "</span>";
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void CheckIdentityAlreadyRegisteredForDependent(OS_DXC_WAP.avayaWS.Service1 svcAvaya, string strIDNo, int intDepIndex, ref string strMsg, ref int intCount)
    {
        try
        {
            string strCount;
            strMsg = "ID/Iqama number already registered on the same contract in our system<br>  رقم الهوية/ الاقامة مسجل مسبقاً ";
            strCount = svcAvaya.GetmbrCountByID(strIDNo, Convert.ToString(Session["ClientID"]));
            if (Convert.ToInt32(strCount) > 0)
            {
                strMsg = strMsg + "<br> " + Convert.ToString(intDepIndex + 1) + " )- ID/Iqama : " + strIDNo;
                intCount = intCount + 1;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void GenerateThBasicRequestInfo(ref SubTxnRequest_DN request)
    {
        try
        {
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.TransactionType = "ADD MEMBER02";
            request.TransactionID = WebPublication.GenerateTransactionID();
            if (_isBatch)
            {
                request.BatchIndicator = "Y";
            }
            else
            {
                request.BatchIndicator = "N";
            }
            request.memberName = new String[3];
            request.natureOfIllness = new String[3];
            request.periodOfDisability = new String[3];
            request.presentState = new String[3];
            request.previousMembershipNo = new String[3];
            request.medicalFacility = new String[3];
            for (int j = 0; j < 3; j++)
            {
                request.memberName[j] = "3";
                request.natureOfIllness[j] = "3";
                request.periodOfDisability[j] = "3";
                request.presentState[j] = "3";
                request.previousMembershipNo[j] = "3";
                request.medicalFacility[j] = "3";
            }
            if (Session["ClientUsername"] != null)
            {
                if (Session["ClientUsername"].ToString().Length > 10)
                {
                    request.submitBy = Session["ClientUsername"].ToString().Substring(0, 10);
                }
                else
                {
                    request.submitBy = Session["ClientUsername"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void GenerateThBasicRequestViewMode(ref ReSubTxnRequest_DN request)
    {
        try
        {
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            //request.TransactionType = "ADD MEMBER02";
            request.amd_Mbr_Ind = "Y";
            request.refNo = strPassedRefNo;
            request.transactionID = WebPublication.GenerateTransactionID3();
            //if (_isBatch)
            //{
            //    request.BatchIndicator = "Y";
            //}
            //else
            //{
            //    request.BatchIndicator = "N";
            //}
            request.memberName = new String[3];
            request.natureOfIllness = new String[3];
            request.periodOfDisability = new String[3];
            request.presentState = new String[3];
            request.previousMembershipNo = new String[3];
            request.medicalFacility = new String[3];
            for (int j = 0; j < 3; j++)
            {
                request.memberName[j] = "3";
                request.natureOfIllness[j] = "3";
                request.periodOfDisability[j] = "3";
                request.presentState[j] = "3";
                request.previousMembershipNo[j] = "3";
                request.medicalFacility[j] = "3";
            }
            if (Session["ClientUsername"] != null)
            {
                if (Session["ClientUsername"].ToString().Length > 10)
                {
                    request.submitBy = Session["ClientUsername"].ToString().Substring(0, 10);
                }
                else
                {
                    request.submitBy = Session["ClientUsername"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void GenerateTheRequestObjectForMainMember(ref SubTxnRequest_DN request)
    {
        try
        {
            request.detail[0] = new AllMbrDetail_DN();
            request.detail[0].ContractNo = strClientID;
            request.detail[0].BranchCode = ddlEBranchCode.SelectedValue;
            request.detail[0].MbrName = txtEMemberName.Text;
            request.detail[0].DOB = DateTime.ParseExact(txtEDateOfBirth.GregorianDate, "dd/MM/yyyy", null);
            request.detail[0].ClassID = ddlELevelOfCover.SelectedValue;
            request.detail[0].MainMemberNo = "";
            request.detail[0].DeptCode = txtEDepartmentCode.Text;
            request.detail[0].IgamaID = txtEmployeeID.Text;
            request.detail[0].SponsorID = ddlSponsor.SelectedItem.Text;
            request.detail[0].StaffNo = txtEEmployeeNo.Text;
            //char[] delimiters = new char[] { '-' };
            //string[] parts = ddlENationality.SelectedValue.Split(delimiters,
            //                StringSplitOptions.RemoveEmptyEntries);
            request.detail[0].Nationality = getNationalityForCaesarRequest(ddlENationality);// parts[0];
            request.detail[0].Gender = ddlEGender.SelectedValue;

            request.detail[0].RequestStartDate = DateTime.ParseExact(txtERequestedStartDate.GregorianDate, "dd/MM/yyyy", null);
            request.detail[0].EffectiveDate = DateTime.ParseExact(txtERequestedStartDate.GregorianDate, "dd/MM/yyyy", null);
            request.detail[0].JoinDate = DateTime.ParseExact(txtEJoiningDate.GregorianDate, "dd/MM/yyyy", null);
            request.detail[0].ArrivalDate = DateTime.ParseExact(txtEJoiningDate.GregorianDate, "dd/MM/yyyy", null);

            request.detail[0].PreviousMembershipIndicator = "N";
            if (chkPreviousMember.Checked == true)
            {
                request.detail[0].PreviousMembershipIndicator = "Y";
                request.detail[0].PreviousMembershipNo = txtPreviousMember.Text;
            }
            if (ddlEJoinReason.SelectedValue != "0")
                request.detail[0].Reason = ddlEJoinReason.SelectedValue;

            request.detail[0].Title = ddlETitle.SelectedValue;
            request.detail[0].MemberType = ddlEMemberType.SelectedValue;
            request.detail[0].ID_Type = ddlEIDType.SelectedValue;
            if (ddlEMaritalStatus.SelectedValue != "0")
                request.detail[0].Marital_Status = ddlEMaritalStatus.SelectedValue;

            request.detail[0].ID_Expiry_Date = DateTime.ParseExact(txtEIDExpDate.GregorianDate, "dd/MM/yyyy", null);
            request.detail[0].MBR_MOBILE_NO = txtEMobileNo.Text;
            request.detail[0].SMS_PREF_LANG = "N";
            request.detail[0].sql_type = "CSR.SUB_TXN_REC";
            request.detail[0].Telephone = txtEMobileNo.Text;
            request.detail[0].Phone = txtEMobileNo.Text;
            AssignSurveyQuestionsValues(ref request, 0);
            request.detail[0].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
            request.detail[0].CCHI_City_Code = ddDistrict.SelectedValue;
            request.detail[0].CCHI_Job_Code = ddProfession.SelectedValue;
            request.detail[0].YakeenVerified = hidYakeenEmployee.Value;
            if (Session["ClientUsername"] != null)
            {
                if (Session["ClientUsername"].ToString().Length > 10)
                {
                    request.submitBy = Session["ClientUsername"].ToString().Substring(0, 10);
                }
                else
                {
                    request.submitBy = Session["ClientUsername"].ToString();
                }
            }
            if (Session["ClientName"] == null)
            {
                if (Session["ClientName"].ToString().Length > 10)
                {

                    request.detail[0].GroupSecretaryName = Session["ClientName"].ToString().Substring(0, 10);
                }
                else
                {
                    request.detail[0].GroupSecretaryName = Session["ClientName"].ToString();
                }
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void GenerateTheRequestObjectForMainMemberViewMode(ref AllMbrDetail_DN[] request)
    {
        try
        {
            request[0] = new AllMbrDetail_DN();
            request[0].ContractNo = strClientID;
            request[0].BranchCode = ddlEBranchCode.SelectedValue;
            request[0].MbrName = txtEMemberName.Text;
            request[0].DOB = DateTime.ParseExact(txtEDateOfBirth.GregorianDate, "dd/MM/yyyy", null);
            request[0].ClassID = ddlELevelOfCover.SelectedValue;
            request[0].MainMemberNo = "";
            request[0].DeptCode = txtEDepartmentCode.Text;
            request[0].IgamaID = txtEmployeeID.Text;
            request[0].SponsorID = ddlSponsor.SelectedItem.Text;
            request[0].StaffNo = txtEEmployeeNo.Text;
            //request[0].Nationality = ddlENationality.SelectedValue;
            //char[] delimiters = new char[] { '-' };
            //string[] parts = ddlENationality.SelectedValue.Split(delimiters,
            //                StringSplitOptions.RemoveEmptyEntries);
            //request[0].Nationality = parts[0];
            request[0].Nationality = getNationalityForCaesarRequest(ddlENationality);// parts[0];

            request[0].Gender = ddlEGender.SelectedValue;
            request[0].RequestStartDate = DateTime.ParseExact(txtERequestedStartDate.GregorianDate, "dd/MM/yyyy", null);
            request[0].EffectiveDate = DateTime.ParseExact(txtERequestedStartDate.GregorianDate, "dd/MM/yyyy", null);
            request[0].JoinDate = DateTime.ParseExact(txtEJoiningDate.GregorianDate, "dd/MM/yyyy", null);
            request[0].ArrivalDate = DateTime.ParseExact(txtEJoiningDate.GregorianDate, "dd/MM/yyyy", null);

            request[0].PreviousMembershipIndicator = "N";
            if (chkPreviousMember.Checked == true)
            {
                request[0].PreviousMembershipIndicator = "Y";
                request[0].PreviousMembershipNo = txtPreviousMember.Text;
            }
            if (ddlEJoinReason.SelectedValue != "0")
                request[0].Reason = ddlEJoinReason.SelectedValue;

            request[0].Title = ddlETitle.SelectedValue;
            request[0].MemberType = ddlEMemberType.SelectedValue;
            request[0].ID_Type = ddlEIDType.SelectedValue;
            if (ddlEMaritalStatus.SelectedValue != "0")
                request[0].Marital_Status = ddlEMaritalStatus.SelectedValue;

            request[0].ID_Expiry_Date = DateTime.ParseExact(txtEIDExpDate.GregorianDate, "dd/MM/yyyy", null);
            request[0].MBR_MOBILE_NO = txtEMobileNo.Text;
            request[0].SMS_PREF_LANG = "N";
            request[0].sql_type = "CSR.SUB_TXN_REC";
            request[0].Telephone = txtEMobileNo.Text;
            request[0].Phone = txtEMobileNo.Text;
            AssignSurveyQuestionsValuesViewMode(ref request, 0);
            request[0].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
            request[0].CCHI_City_Code = ddDistrict.SelectedValue;
            request[0].CCHI_Job_Code = ddProfession.SelectedValue;
            request[0].YakeenVerified = hidYakeenEmployee.Value;
            if (Session["ClientName"] == null)
                request[0].GroupSecretaryName = Session["ClientName"].ToString();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void GenerateTheRequestObjectForDependent(ref SubTxnRequest_DN request, int intDepIndex)
    {
        try
        {
            int j;
            if (DependentFlag == true)
                j = intDepIndex;
            else
                j = intDepIndex + 1;

            request.detail[j] = new AllMbrDetail_DN();
            request.detail[j].ContractNo = strClientID;
            request.detail[j].BranchCode = ddlEBranchCode.SelectedValue;
            request.detail[j].MbrName = (String)DT.Rows[intDepIndex][1];
            request.detail[j].DOB = DateTime.ParseExact((String)DT.Rows[intDepIndex][2], "dd/MM/yyyy", null);
            request.detail[j].ClassID = (String)DT.Rows[intDepIndex][9];
            if (DependentFlag == true)
                request.detail[j].MainMemberNo = txtMainMemberNo.Text;
            else
                request.detail[j].MainMemberNo = "";
            request.detail[j].DeptCode = (String)DT.Rows[intDepIndex][6];
            request.detail[j].IgamaID = (String)DT.Rows[intDepIndex][5];
            request.detail[j].SponsorID = (String)DT.Rows[intDepIndex][3];
            request.detail[j].StaffNo = (String)DT.Rows[intDepIndex][4];
            request.detail[j].Nationality = (String)DT.Rows[intDepIndex][8];
            request.detail[j].Gender = (String)DT.Rows[intDepIndex][7];
            request.detail[j].RequestStartDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][13], "dd/MM/yyyy", null);
            request.detail[j].EffectiveDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][13], "dd/MM/yyyy", null);
            request.detail[j].JoinDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][12], "dd/MM/yyyy", null);
            request.detail[j].ArrivalDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][12], "dd/MM/yyyy", null);
            request.detail[j].PreviousMembershipIndicator = "N";
            request.detail[j].PreviousMembershipNo = "";
            if ((String)DT.Rows[intDepIndex][10] != "0")
                request.detail[j].Reason = (String)DT.Rows[intDepIndex][10];
            request.detail[j].Title = (String)DT.Rows[intDepIndex][0];
            request.detail[j].MBR_MOBILE_NO = (String)DT.Rows[intDepIndex][14];
            request.detail[j].SMS_PREF_LANG = "N";
            request.detail[j].sql_type = "CSR.SUB_TXN_REC";
            request.detail[j].MemberType = (String)DT.Rows[intDepIndex][15];
            request.detail[j].ID_Type = (String)DT.Rows[intDepIndex][16];
            request.detail[j].YakeenVerified = (String)DT.Rows[intDepIndex][21];
            if ((String)DT.Rows[intDepIndex][17] != "00/00/0000")
                request.detail[j].ID_Expiry_Date = DateTime.ParseExact((String)DT.Rows[intDepIndex][17], "dd/MM/yyyy", null);
            else
                request.detail[j].ID_Expiry_Date = null;

            if ((String)DT.Rows[intDepIndex][18] != "0")
                request.detail[j].Marital_Status = (String)DT.Rows[intDepIndex][18];

            request.detail[j].CCHI_Job_Code = (String)DT.Rows[intDepIndex][19];
            request.detail[j].CCHI_City_Code = (String)DT.Rows[intDepIndex][20];
            request.detail[j].Telephone = (String)DT.Rows[intDepIndex][14];
            request.detail[j].Phone = (String)DT.Rows[intDepIndex][14];
            AssignSurveyQuestionsValues(ref request, j);
            request.detail[j].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
            if (Session["ClientUsername"] != null)
            {
                if (Session["ClientUsername"].ToString().Length > 10)
                    request.submitBy = Session["ClientUsername"].ToString().Substring(0, 10);
                else
                {
                    request.submitBy = Session["ClientUsername"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void GenerateTheRequestObjectForDependentViewMode(ref AllMbrDetail_DN[] request, int intDepIndex)
    {
        try
        {
            int j;
            if (DependentFlag == true)
                j = intDepIndex;
            else
                j = intDepIndex + 1;

            request[j] = new AllMbrDetail_DN();
            request[j].ContractNo = strClientID;
            request[j].BranchCode = ddlEBranchCode.SelectedValue;
            request[j].MbrName = (String)DT.Rows[intDepIndex][1];
            request[j].DOB = DateTime.ParseExact((String)DT.Rows[intDepIndex][2], "dd/MM/yyyy", null);
            request[j].ClassID = (String)DT.Rows[intDepIndex][9];
            if (DependentFlag == true)
                request[j].MainMemberNo = txtMainMemberNo.Text;
            else
                request[j].MainMemberNo = "";
            request[j].DeptCode = (String)DT.Rows[intDepIndex][6];
            request[j].IgamaID = (String)DT.Rows[intDepIndex][5];
            request[j].SponsorID = (String)DT.Rows[intDepIndex][3];
            request[j].StaffNo = (String)DT.Rows[intDepIndex][4];
            request[j].Nationality = (String)DT.Rows[intDepIndex][8];
            request[j].Gender = (String)DT.Rows[intDepIndex][7];
            request[j].RequestStartDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][13], "dd/MM/yyyy", null);
            request[j].EffectiveDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][13], "dd/MM/yyyy", null);
            request[j].JoinDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][12], "dd/MM/yyyy", null);
            request[j].ArrivalDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][12], "dd/MM/yyyy", null);
            request[j].PreviousMembershipIndicator = "N";
            request[j].PreviousMembershipNo = "";
            if ((String)DT.Rows[intDepIndex][10] != "0")
                request[j].Reason = (String)DT.Rows[intDepIndex][10];
            request[j].Title = (String)DT.Rows[intDepIndex][0];
            request[j].MBR_MOBILE_NO = (String)DT.Rows[intDepIndex][14];
            request[j].SMS_PREF_LANG = "N";
            request[j].sql_type = "CSR.SUB_TXN_REC";
            request[j].MemberType = (String)DT.Rows[intDepIndex][15];
            request[j].ID_Type = (String)DT.Rows[intDepIndex][16];
            request[j].YakeenVerified = (String)DT.Rows[intDepIndex][21];
            if ((String)DT.Rows[intDepIndex][17] != "00/00/0000")
                request[j].ID_Expiry_Date = DateTime.ParseExact((String)DT.Rows[intDepIndex][17], "dd/MM/yyyy", null);
            else
                request[j].ID_Expiry_Date = null;

            if ((String)DT.Rows[intDepIndex][18] != "0")
                request[j].Marital_Status = (String)DT.Rows[intDepIndex][18];

            request[j].CCHI_Job_Code = (String)DT.Rows[intDepIndex][19];
            request[j].CCHI_City_Code = (String)DT.Rows[intDepIndex][20];
            request[j].Telephone = (String)DT.Rows[intDepIndex][14];
            request[j].Phone = (String)DT.Rows[intDepIndex][14];
            AssignSurveyQuestionsValuesViewMode(ref request, j);
            request[j].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region "View Mode Functions and Procedures"
    private void LoadViewModeByRefNo(string strPassedRefNo, string strContractNo)
    {
        try
        {
            Bupa.OSWeb.Business.CaesarHelper ch = new Bupa.OSWeb.Business.CaesarHelper();
            MbrRejListResponse_DN response = ch.GetRejectedRequestDetailsByTrackInfoRefNo(strPassedRefNo, strContractNo);

            if (response.errorMessage != null && string.IsNullOrWhiteSpace(response.errorMessage[0].ToString()))
            {
                MaintainEmployeePageControllsInViewMode();
                AssignEmployeeRequestValues(response);
            }
            else
            {
                throw new Exception(response.errorMessage[0].ToString());
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("ORA"))
            {
                //Logger.Current.WriteException(ex);
                Logger.Current.WriteException(ex);
                throw new Exception("There is some internal error, please contact 'Bupa Support Team'");
            }
            else
                throw ex;
        }
    }
    private void AssignEmployeeRequestValues(MbrRejListResponse_DN response)
    {
        try
        {
            CaesarRequest caesarReq = new CaesarRequest();

            ddlELevelOfCover.SelectedValue = response.detail[0].ClsID;
            ddlELevelOfCover_SelectedIndexChanged(null, null);
            ddlEMemberType.SelectedValue = response.detail[0].MbrType;
            txtEmployeeID.Text = response.detail[0].IDCardNo;

            if (response.detail[0].IDCardNo.StartsWith("1"))
                ddlEIDType.SelectedIndex = 1;
            else if (response.detail[0].IDCardNo.StartsWith("2"))
                ddlEIDType.SelectedIndex = 2;
            else
                ddlEIDType.SelectedIndex = 3;

            ddlSponsor.SelectedItem.Text = response.detail[0].SponsorID.Trim();
            txtEDateOfBirth.GregorianDate = ReverseDate(response.detail[0].Dob.Trim());
            txtEIDExpDate.GregorianDate = ReverseDate(response.detail[0].IDExpDate.Trim());
            ddlEMaritalStatus.SelectedValue = response.detail[0].MaritalStatus.Trim();
            ddlETitle.SelectedValue = response.detail[0].Title.Trim();
            ddlEGender.SelectedValue = response.detail[0].Sex.Trim();
            txtEMemberName.Text = response.detail[0].MbrName.Trim();

            //ddlENationality.SelectedValue = setNationalityinDropDownList(response.detail[0].CtryName, ddlENationality);
            setNationalityinDropDownList(response.detail[0].CtryName, ddlENationality);
            ddProfession.SelectedValue = response.detail[0].CCHIJobCode.Trim();

            ddDistrict.SelectedValue = response.detail[0].CCHICityCode.Trim();
            txtEMobileNo.Text = response.detail[0].MbrMobileNo.Trim();
            ddlEBranchCode.SelectedValue = response.detail[0].BranchCode;
            txtEJoiningDate.GregorianDate = ReverseDate(response.detail[0].JoinDate.Trim());
            txtERequestedStartDate.GregorianDate = ReverseDate(response.detail[0].EffDate.Trim());
            txtEEmployeeNo.Text = response.detail[0].StaffNo.Trim();
            txtEDepartmentCode.Text = response.detail[0].DeptCode.Trim();
            txtPreviousMember.Text = response.detail[0].PrevMbrNo.Trim();
            ddlEJoinReason.SelectedValue = response.detail[0].SubReason.Trim();
            txtEmpHijriYearofBirth.Text = txtEDateOfBirth.HijriDate.Substring(6, 4).Trim();
            if (response.detail[0].PrevMbrInd == "Y")
                chkPreviousMember.Checked = true;

            KeepTheJavaScriptStateBetweenPostBacks();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void MaintainEmployeePageControllsInViewMode()
    {
        try
        {
            ddlELevelOfCover.Enabled = true;
            ddlEMemberType.Enabled = true;

            //Yakeen Fields
            ddlEIDType.Enabled = false;
            txtEmployeeID.Enabled = false;
            ddlSponsor.Enabled = false;
            txtEmpHijriYearofBirth.Enabled = false;
            txtEIDExpDate.Enabled = false;
            ddlETitle.Enabled = false;
            ddlGender.Enabled = false;
            txtEMemberName.Enabled = false;
            txtEDateOfBirth.Enabled = false;
            ddlENationality.Enabled = false;

            ddProfession.Enabled = true;
            ddlEMaritalStatus.Enabled = true;

            ddDistrict.Enabled = true;
            txtEMobileNo.Enabled = true;
            ddlEBranchCode.Enabled = true;
            txtEJoiningDate.Enabled = true;
            txtERequestedStartDate.Enabled = true;
            txtEEmployeeNo.Enabled = true;
            txtEDepartmentCode.Enabled = true;
            txtPreviousMember.Enabled = true;
            ddlEJoinReason.Enabled = true;

            btnProceedEmployee.Visible = false;
            pnlEmpDetails.Visible = true;

            btnSubmit.Text = "Update Request";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Add Dependent YAKEEN check

    /// <summary>NOT IN USE
    /// NOT IN USE
    /// As agreed with Asem and Abraham, no need to change any thing- keep as is
    /// 11-Aug-2015
    /// </summary>
    private void ManageDependentsGridsForCaesarAndYakeen()
    {
        return;
        /*
        if (!string.IsNullOrWhiteSpace(MainMembershipNo) && !string.IsNullOrWhiteSpace(MainMemberIqamaSaudiID) && !string.IsNullOrWhiteSpace(DependentHijriYearOfBirth))
        {
            if (!string.IsNullOrWhiteSpace(MainMembershipNo))
            {//only load missing dependents if adding dependents
                if (Request.QueryString["EType"] == "Dep")
                {
                    DataSet dsMembersInCaesar = GetDependentsInCaesar(MainMembershipNo);
                    RadGridDependenetsCoveredUnderBupa.DataSource = dsMembersInCaesar;
                    RadGridDependenetsCoveredUnderBupa.DataBind();

                    DataSet dsMembersInYakeen = new DataSet();
                    if (MainMemberIqamaSaudiID.StartsWith("1"))//is saudi
                        dsMembersInYakeen = GetDependentsFromYakeen(MainMembershipNo, MainMemberIqamaSaudiID, DependentHijriYearOfBirth, true);
                    else //not saudi
                        dsMembersInYakeen = GetDependentsFromYakeen(MainMembershipNo, MainMemberIqamaSaudiID, DependentHijriYearOfBirth, false);

                    RadGridTotalDependentsInYakeen.DataSource = dsMembersInYakeen;
                    RadGridTotalDependentsInYakeen.DataBind();

                    DataSet dsNotCoveredMembersUnderBupa = GetDependentsNotCoveredWithBupa(dsMembersInYakeen.Tables[0]);
                    RadGridDependenetsNotCoveredUnderBupa.DataSource = dsNotCoveredMembersUnderBupa;
                    RadGridDependenetsNotCoveredUnderBupa.DataBind();

                }
            }
        }
        else
        {
            LabelBupa.Visible = false;
            LabelNotBupa.Visible = false;
            LabelYakeen.Visible = false;
        }
         * */
    }

    private string MainMembershipNo { get; set; }
    private string MainMemberIqamaSaudiID { get; set; }
    private string DependentHijriYearOfBirth { get; set; }
    /// <summary>NOT IN USE
    /// NOT IN USE
    /// As agreed with Asem and Abraham, no need to change any thing- keep as is
    /// 11-Aug-2015
    /// </summary>
    private DataSet GetDependentsInCaesar(string membershipNumber)
    {
        DataSet dsResult = new DataSet();
        return dsResult;
        /*
        ServiceDepot_DNService ws = new ServiceDepot_DNService();
        EnqMbrDepGetInfoRequest_DN request = new EnqMbrDepGetInfoRequest_DN();
        EnqMbrDepGetInfoResponse_DN response;


        request.transactionID = WebPublication.GenerateTransactionID3();
        request.membershipNo = membershipNumber;
        //request.transactionID= ;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        response = ws.EnqMbrDepGetInfo(request);

        DataTable dt = MemberDependentParserForCaesar(response);

        if (dt.Rows.Count == 0)
        {
            LabelBupa.Visible = false;
            RadGridDependenetsCoveredUnderBupa.Visible = false;

        }

        dsResult.Tables.Add(dt);

        return dsResult;
         * */
    }
    /// <summary>NOT IN USE
    /// NOT IN USE
    /// As agreed with Asem and Abraham, no need to change any thing- keep as is
    /// 11-Aug-2015
    /// </summary>
    private DataSet GetDependentsFromYakeen(string membershipNumber, string iqamaOrSaudiId, string hijriDOB, bool IsSaudi)
    {
        DataSet dsResult = new DataSet();
        return dsResult;
        /*
        PersonService pWS = new PersonService();

        PersonWS.Dependent[] deps = pWS.GetDependentsInfo(iqamaOrSaudiId, int.Parse(hijriDOB), true, IsSaudi, true);
        DataTable dt = MemberDependentParserForYakeen(deps, membershipNumber);

        if (dt.Rows.Count == 0)
        {
            LabelYakeen.Visible = false;
        }

        dsResult.Tables.Add(dt);

        return dsResult;
         * */
    }
    /// <summary>NOT IN USE
    /// NOT IN USE
    /// As agreed with Asem and Abraham, no need to change any thing- keep as is
    /// 11-Aug-2015
    /// </summary>
    private DataSet GetDependentsNotCoveredWithBupa(DataTable dtMembersInYakeen)
    {
        DataSet dsResult = new DataSet();
        return dsResult;
        /*
        DataTable dt = new DataTable();
        dt = MemberDependentParserForNotCoveredDependents(dtMembersInYakeen);

        if (dt.Rows.Count == 0)
        {
            LabelNotBupa.Visible = false;
        }

        dsResult.Tables.Add(dt.Copy());

        return dsResult;
         * */
    }

    private DataTable MemberDependentParserForCaesar(EnqMbrDepGetInfoResponse_DN response)
    {
        DataTable dtResult = new DataTable();
        Session["CaesarDepIds"] = null;
        MbrDetail_DN[] detail = response.detail;
        string errorID = response.errorID;
        string errorMessage = response.errorMessage;
        string status = response.status;
        long transactionID = response.transactionID;

        dtResult = DataManager.ConvertToDataTable(detail);
        List<string> depIqmaIdsCaesar = new List<string>();
        foreach (var item in detail)
        {
            depIqmaIdsCaesar.Add(item.memberID);
        }

        Session["CaesarDepIds"] = depIqmaIdsCaesar;
        return dtResult;
    }
    private DataTable MemberDependentParserForYakeen(OS_DXC_WAP.PersonWSN.Dependent[] response, string membershipNumber)
    {
        string NameAr = string.Empty;
        string NameEn = string.Empty;
        DataTable table = CreateTableSchema();
        foreach (OS_DXC_WAP.PersonWSN.Dependent item in response)
        {
            NameEn = item.EnglishName;
            NameAr = item.ArabicName;
            table.Rows.Add(NameAr, NameEn, item.PersonID, item.Kinship, (item.Gender.ToString().ToUpper().Contains("F")) ? "Female" : "Male",
                "", item.BirthDateHijri, item.BirthDateGregorian);
        }

        return table;
    }
    private DataTable MemberDependentParserForNotCoveredDependents(DataTable membersInYakeen)
    {
        Session["DependentsNotUnderBupa"] = null;
        List<string> depsListIdsFromCaesar = new List<string>();
        depsListIdsFromCaesar = (List<string>)Session["CaesarDepIds"];

        foreach (string item in depsListIdsFromCaesar)
        {
            DataRow dr = membersInYakeen.Select().Where(x => x.Field<string>("Iqama").Equals(item)).FirstOrDefault();
            if (dr != null)
            {
                membersInYakeen.Rows.Remove(dr);
            }
        }
        List<string> depsListIdsNotUnderBupa = new List<string>();

        foreach (DataRow item in membersInYakeen.Rows)
        {
            depsListIdsNotUnderBupa.Add(item["Iqama"].ToString());
        }

        Session["DependentsNotUnderBupa"] = depsListIdsNotUnderBupa;
        return membersInYakeen;
    }

    private DataTable CreateTableSchema()
    {
        try
        {
            DataTable dtTemp = new DataTable();
            dtTemp.Columns.Add("Arabic Name", typeof(string));
            dtTemp.Columns.Add("English Name", typeof(string));
            dtTemp.Columns.Add("Iqama", typeof(string));
            dtTemp.Columns.Add("Relation", typeof(string));
            dtTemp.Columns.Add("Gender", typeof(string));
            dtTemp.Columns.Add("Nationality", typeof(string));
            dtTemp.Columns.Add("Hijri DOB", typeof(string));
            dtTemp.Columns.Add("Greg DOB", typeof(string));
            return dtTemp;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void RadGridDependenetsCoveredUnderBupa_ItemDataBound(object sender, GridItemEventArgs e)
    {
        /*
        if (e.Item is GridDataItem)
        {

            GridDataItem item = e.Item as GridDataItem;
            foreach (GridColumn column in RadGridDependenetsCoveredUnderBupa.MasterTableView.AutoGeneratedColumns)
            {
                if (string.IsNullOrWhiteSpace(column.HeaderText) || column.HeaderText.Contains("CCHI Rej Reason")
|| column.HeaderText.Equals("CCHI Status")
|| column.HeaderText.Equals("IBAN")
|| column.HeaderText.Equals("IVF_Pregnancy_IND")
|| column.HeaderText.Equals("VIP Code")
|| column.HeaderText.Equals("bank Country")
|| column.HeaderText.Equals("bank Name")
|| column.HeaderText.Equals("clm Pay To Ind")
|| column.HeaderText.Equals("company Name")
|| column.HeaderText.Equals("country Name")
|| column.HeaderText.Equals("cust No")
|| column.HeaderText.Equals("customer Name")
|| column.HeaderText.Equals("dist Code")
|| column.HeaderText.Equals("email")
|| column.HeaderText.Equals("employee No")
|| column.HeaderText.Equals("end Date")
|| column.HeaderText.Equals("net Set Name")
|| column.HeaderText.Equals("outside_KSA_Ind")
|| column.HeaderText.Equals("prof Code")
|| column.HeaderText.Equals("scheme Name")
|| column.HeaderText.Equals("sponsor ID")
|| column.HeaderText.Equals("start Date")
|| column.HeaderText.Equals("status")
|| column.HeaderText.Equals("Branch Code"))
                {
                    column.Visible = false;
                }
            }
        }
         * */
    }
    #endregion
    private Person GetMemberInfoFromYakeen(string personId, string varificationString)
    {
        //// Person Service Revamp integration implemented on 02-Nov-2016 By Sakthi
        //// Old Code Start
        //////DataSet dsResult = new DataSet();
        //////PersonService pWS = new PersonService();
        ///////////PersonServiceClient pWS = new PersonServiceClient();        

        //////Person personInfo = new Person();
        //////if (personId.StartsWith("1"))
        //////{
        //////    personInfo = pWS.GetPersonInfo(personId, varificationString, YakeenSearhByTypesYakeenSearchByType.HijriDOB, true);
        //////    ////personInfo = pWS.GetPersonInfo(personId, varificationString, YakeenSearhByTypesYakeenSearchByType.HijriDOB/*, true*/);
        //////    //personInfo.NationalityCode = "966";
        //////}
        //////else if (personId.StartsWith("2"))
        //////{
        //////    personInfo = pWS.GetPersonInfo(personId, varificationString, YakeenSearhByTypesYakeenSearchByType.IqamaId, true);
        //////    ////personInfo = pWS.GetPersonInfo(personId, varificationString, YakeenSearhByTypesYakeenSearchByType.IqamaId /*, true*/);
        //////}
        //////else
        //////{
        //////    personInfo = pWS.GetPersonInfo(personId, varificationString, YakeenSearhByTypesYakeenSearchByType.BorderEntry, true);
        //////    ////personInfo = pWS.GetPersonInfo(personId, varificationString, YakeenSearhByTypesYakeenSearchByType.BorderEntry /*, true*/);
        //////}
        //// Old Code End
        //// New Code Start
        DataSet dsResult = new DataSet();
        PersonService pWS = new PersonService();
        Person personInfo = new Person();
        if (personId.StartsWith("1"))
            personInfo = pWS.GetPersonInfo(personId, varificationString, WebApp.CommonClass.YakeenValidationHours, true, YakeenSearhByTypesYakeenSearchByType.HijriDOB, true, WebApp.CommonClass.FromUserName, WebApp.CommonClass.FromDepartmentName, WebApp.CommonClass.FromSourceName);             
        else if (personId.StartsWith("2"))
            personInfo = pWS.GetPersonInfo(personId, varificationString, WebApp.CommonClass.YakeenValidationHours, true, YakeenSearhByTypesYakeenSearchByType.IqamaId, true, WebApp.CommonClass.FromUserName, WebApp.CommonClass.FromDepartmentName, WebApp.CommonClass.FromSourceName);                    
        else
            personInfo = pWS.GetPersonInfo(personId, varificationString, WebApp.CommonClass.YakeenValidationHours, true, YakeenSearhByTypesYakeenSearchByType.BorderEntry, true, WebApp.CommonClass.FromUserName, WebApp.CommonClass.FromDepartmentName, WebApp.CommonClass.FromSourceName);                                           
        ////New Code End

        return personInfo;
    }
    private void SetEmployeeDetails(Person personInfo)
    {
        try
        {
            string strTempDate = string.Empty;
            pnlEmpDetails.Visible = true;
            txtEMemberName.Text = RemoveSpecialCharacters(personInfo.EnglishFirstName) + " " +
                                  RemoveSpecialCharacters(personInfo.EnglishSecondName) + " " +
                                  RemoveSpecialCharacters(personInfo.EnglishThirdName) + " " +
                                  RemoveSpecialCharacters(personInfo.EnglishLastName);

            //To remove extra space between name words
            txtEMemberName.Text = Regex.Replace(txtEMemberName.Text.ToString(), @"\s+", " ");
            txtEDateOfBirth.GregorianDate = personInfo.BirthDateGregorian.Value.ToString().Split(new char[] { ' ' })[0];//	"20/01/1983"	string
            HandleEmployeeGenderTitleFromYaqeen(ddlEGender, personInfo.Gender.ToString());
            //ddlEGender.SelectedValue = personInfo.Gender.ToString();
            ////txtEIDExpDate.HijriDate = personInfo.IdExpiryDate;
            ////DateTime birthDateGregorian = Convert.ToDateTime(personInfo.BirthDateGregorian);



            
            
            ////DateTime idExpiryDate = Convert.ToDateTime(personInfo.IdExpiryDate);
           

            ////if (!string.IsNullOrEmpty(Convert.ToString(idExpiryDate)))
            ////    txtEIDExpDate.GregorianDate = idExpiryDate.ToString("dd/MM/yyyy");


            DateTime birthDateGregorian = Convert.ToDateTime(personInfo.BirthDateGregorian);
            DateTime idExpiryDate = Convert.ToDateTime(personInfo.IdExpiryDate);

            if (!string.IsNullOrEmpty(Convert.ToString(birthDateGregorian)))
            {
                string defaultDate = Convert.ToString(birthDateGregorian);
                if (defaultDate.Contains("01/01/0001"))
                { }
                else
                    txtEDateOfBirth.GregorianDate = personInfo.BirthDateGregorian.Value.ToString().Split(new char[] { ' ' })[0];//	"20/01/1983"	string

            }

            if (!string.IsNullOrEmpty(Convert.ToString(idExpiryDate)))
            {
                string defaultDate = Convert.ToString(idExpiryDate);
                if (defaultDate.Contains("01/01/0001"))
                { }
                else
                    txtEIDExpDate.GregorianDate = idExpiryDate.ToString("dd/MM/yyyy");
            }

            ////if (ddlEIDType.SelectedValue == "1" || ddlEIDType.SelectedValue == "2")
            ////{
            ////    if (!string.IsNullOrEmpty(Convert.ToString(personInfo.IdExpiryDate)))
            ////        txtEIDExpDate.GregorianDate = personInfo.IdExpiryDate;
            ////}
            ////else if (ddlEIDType.SelectedValue == "4")
            ////{
            ////    if (!string.IsNullOrEmpty(Convert.ToString(personInfo.IdExpiryDate)))
            ////        txtEIDExpDate.GregorianDate = personInfo.IdExpiryDate; ////txtEIDExpDate.HijriDate = personInfo.IdExpiryDate; //// Persons service returning Gregorian as per the latest change disabled by Sakthin on 28-Aug-2016
            ////}



            SetNationality(personInfo);
            SetOccupation(personInfo);

            DisableYaqeenFieldsForMainMember();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static string ReverseDate(string s)
    {
        try
        {
            return s.Substring(8, 2) + "/" + s.Substring(5, 2) + "/" + s.Substring(0, 4);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlSponsor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        /*if (e.Value.ToLower().Equals("add new"))
        {
            txtESponsorId.Enabled = true;
            rfvEmpSponsorID.Enabled = true;
        }
        else
        {
            txtESponsorId.Text = "";
            txtESponsorId.Enabled = false;
            rfvEmpSponsorID.Enabled = false;
        }*/
    }

    private void setNationalityinDropDownList(string NationalityCode, DropDownList ddl)
    {
        try
        {
            ListItem li = ddl.Items.Cast<ListItem>()
                     .Where(x => x.Value.Contains(NationalityCode))
                     .LastOrDefault();
            ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(li.Text));
        }
        catch (Exception)
        {

        }

    }
    private string getNationalityForCaesarRequest(DropDownList ddl)
    {
        char[] delimiters = new char[] { '-' };
        string[] parts = ddl.SelectedValue.Split(delimiters,
                        StringSplitOptions.RemoveEmptyEntries);
        return parts[0];
    }


    public class ClassXMLGeneration<T>
    {
        public void Request(T classType, string req)
        {
            System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
            System.IO.StreamWriter file = new StreamWriter("C:\\Lokesh\\" + req + ".xml");
            xmlSrQ.Serialize(file, classType);
            file.Close();


        }
        public void Response(T classType, string resp)
        {
            System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
            System.IO.StreamWriter file = new StreamWriter("C:\\Lokesh\\" + resp + ".xml");
            xmlSr.Serialize(file, classType);
            file.Close();
        }

    }

}