﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" Inherits="CCHICap"
    EnableEventValidation="false" ClientIDMode="Predictable" AutoEventWireup="true" Codebehind="CCHICap.aspx.cs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register TagPrefix="BUPA" TagName="uploader" Src="~/Uploader.ascx" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucHijriGregorian.ascx" TagPrefix="BUPA" TagName="ucHijriGregorian" %>


<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>
      <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    
    <style type="text/css">
        .labelClass {
            float: left;
            margin-right: 10px;
            line-height: 30px;
            width: 205px;
        }

        .controlClass {
            float: left;
            margin-right: 10px;
        }

        .middle {
            background-color: yellow;
            display: block;
            overflow: hidden;
        }

        .clear {
            clear: both;
        }

        .containerClass {
            height: 60px;
            width: 100%;
        }

        .controlClass input, .controlClass select {
            width: 260px;
        }

        .FloatingDiv {
            position: fixed;
            top: 0;
            left: 0;
            text-align: center;
            padding: 200px 0 0 0;
            z-index: 999;
            width: 100%;
            height: 100%;
            background: url('../Images/Floatingimg.png') repeat top left;
        }

        .Floatimage {
            display: block;
            width: 100%;
            height: 100%;
            vertical-align: middle;
        }

        .button {
            font: bold 13px Arial;
            text-decoration: none;
            background-color: #EEEEEE;
            color: #0099FF;
            padding: 2px 6px 2px 6px;
            border-top: 1px solid #CCCCCC;
            border-right: 1px solid #0099FF;
            border-bottom: 1px solid #0099FF;
            border-left: 1px solid #CCCCCC;
        }
    </style>

    <%--<telerik:RadScriptManager runat="server" ID="RadScriptManager1" />--%>

    <fieldset style="padding: 10px">
        <legend>
            <h1>
                <asp:Label ID="lblCCHICap" runat="server" Text="Cap Count"></asp:Label>
            </h1>
        </legend>
       <%-- <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1">--%>
         <asp:UpdatePanel runat="server" ID="updatePnl">
                <Triggers>
                     <asp:AsyncPostBackTrigger ControlID="btnProceed" />
                </Triggers>
                <ContentTemplate>
                    <asp:UpdateProgress ID="uprgWorkRequest" runat="server">
                        <ProgressTemplate>
                            <div class="FloatingDiv">
                                <asp:Image ID="Image1" runat="server" ImageUrl="../Images/ajax-loader.gif"
                                    Style="height: 279px" /><br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <div id="divEmpSponsorId" runat="server" class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label8" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Sponsor Id"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <%--CheckBoxes="true" EnableCheckAllItemsCheckBox="true" --%>
                        <telerik:RadComboBox ID="ddlSponsor" DataValueField="sponsorID" runat="server" Width="300px" 
                            EmptyMessage="Select" DataTextField="sponsorID">
                        </telerik:RadComboBox>

                        <asp:RequiredFieldValidator
                            ID="rfvEmpSponsorID" runat="server" ControlToValidate="ddlSponsor"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="Proceed" ></asp:RequiredFieldValidator>
                    </div>
                        <div style="clear:both"></div>
                          <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblIqama" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Iqama"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <asp:TextBox CssClass="textbox" ID="txtIqama"   Width="300px" Height="17" runat="server" MaxLength="10"
                            BackColor="#eff6fc"></asp:TextBox>
                        <font color="black"></font>
                        <br />
                        <asp:RequiredFieldValidator ID="txt" runat="server"
                            ControlToValidate="txtIqama" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="Proceed" Width="119px" />
                        <br />
                         <asp:Button ID="btnProceed" Width="70px" runat="server" Text="Proceed" CssClass="submitButton" ValidationGroup="Proceed" OnClick="btnProceed_Click" />
                    </div>

                    <div style="clear:both;">&nbsp;</div>

                 
                    <div style="float: right">
                        <asp:HyperLink ID="lnkBatchUpload" runat="server" Font-Size="Small" Visible="false">Batch Upload</asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
          
            <div id="DisplayError" runat="server" style="color: Red;">
                Validation : <%= _ServiceResult %>
                <br />
                CCHI Error : <%= _Description %>
            </div>
            <div id="CapCountDetails" runat="server" style="margin-top:20px" >
                <asp:GridView runat="server" ID="grdViewCaps" Visible="true" AutoGenerateColumns="false" Width="100%" Style="margin-top: 30px;" AutoGenerateDeleteButton="false" AutoGenerateEditButton="false">
                    <HeaderStyle BackColor="#0072c6" ForeColor="White" Font-Bold="true" BorderStyle="None" Height="30px" Font-Size="16px" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" BorderStyle="None" Height="30px" Font-Size="15px" HorizontalAlign="Center" />
                    <Columns>
                        <asp:BoundField HeaderText="Sponsor Id" DataField="Minimum.SponsorId" />
                        <asp:BoundField HeaderText="Saudi Main" DataField="Minimum.SaudiMain" />
                        <asp:BoundField HeaderText="Saudi Dependent" DataField="Minimum.SaudiDependent" />
                        <asp:BoundField HeaderText="Non-Saudi Main" DataField="Minimum.NonSaudiMain" />
                        <asp:BoundField HeaderText="Non-Saudi Dependent" DataField="Minimum.NonSaudiDependent" />
                    </Columns>
                </asp:GridView>
            </div>
                    </ContentTemplate>
        </asp:UpdatePanel>
       <%-- </telerik:RadAjaxPanel>--%>

    </fieldset>
    
    <br />
        <uc2:OSNav ID="OSNav1" runat="server" />
        <br />
</asp:Content>

