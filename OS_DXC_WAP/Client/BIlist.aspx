﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Client_BIlist" Codebehind="BIlist.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        Bupa International Customefr List</p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">
    <p>
        
        <table style="width:50%" align="center">
             <tr >
                                <td class="loginLabel">
                                    Customer</td>
                                <td>
                                    <asp:DropDownList ID="ddlClientList" runat="server" 
                                        DataSourceID="SqlDataSourceBIList" DataTextField="BiName" 
                                        DataValueField="BICode" Font-Size="9pt" Height="25px" Width="170px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                                        onclick="btnSubmit_Click" />
                                </td>
                            </tr>
             <tr >
                                <td class="loginLabel" colspan="3">
                                    &nbsp;</td>
                            </tr>
             <tr >
                                <td class="loginLabel" colspan="3">
                                    <asp:SqlDataSource 
                                        ID="SqlDataSourceBIList" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>" 
                                        
                                        
                                        SelectCommand="SELECT BICode, BiName FROM BiList WHERE (BICode &lt;&gt; N'0') ORDER BY BiName">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
        </table>
        </p>
</asp:Content>

