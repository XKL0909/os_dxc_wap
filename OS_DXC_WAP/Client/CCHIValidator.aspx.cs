﻿using Caesar;
using OnlineServices.CCHI.Dto;
using OnlineServices.CCHI.Enums;
using OnlineServices.CCHI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;

public partial class CCHIValidator : System.Web.UI.Page
{
    private string _reqId, _contractNo;
    private CCHIService _cchiService;
    private RequestManager _reqManager;
    protected string lblMessge = "none";
    private CaesarManager _ceaserManager;
    private string _contractId;
    private UnifiedRequestService _requestService;
    private UnifiedSettingService _settingService;
    private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public CCHIValidator()
    {
        log4net.Config.XmlConfigurator.Configure();
        _cchiService = new CCHIService();
        _reqManager = new RequestManager();
        _ceaserManager = new CaesarManager();
        _requestService = new UnifiedRequestService();
        _settingService = new UnifiedSettingService();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Encrypt or remove query string
        //_reqId = Request.QueryString["requestId"];
        _reqId = Convert.ToString(Session["RequestID"]);
        _contractNo = Convert.ToString(Session["ClientID"]);
        if (string.IsNullOrEmpty(_contractNo))
            Response.Redirect("~/Default.aspx");
        var setting = _settingService.FindByKey("CCHIValidationRequired");

        if (setting != null && setting.SValue == "1")
        {

            if (!string.IsNullOrEmpty(_reqId))
            {
                var caps = _reqManager.GetCCHICounts(Convert.ToInt32(_reqId));
                if (caps != null && caps.Count() > 0)
                {
                    //var result = _cchiService.ValidateSponsors(caps, true);
                    var result = _cchiService.ValidateSponsorsWithIqama(caps, _contractNo);
                    CCHIResponseLog(result, _reqId);
                    if (result.Count > 0)
                    {
                        rptSponsors.DataSource = result;
                        rptSponsors.DataBind();

                        bool isfailure = result.Where(t => t.ErrorDetails.ServiceResult.ToString().ToLower().Contains("failure")).Count() > 0;
                        if (isfailure)
                        {
                            btnSubmit.Enabled = false;
                        }
                        else
                        {
                            btnSubmit.Enabled = true;
                        }
                    }
                    else
                    {
                        btnSubmit.Enabled = false;
                    }
                }
            }
            else
            {
                btnSubmit.Enabled = false;
                lblMessge = "No Request Found";
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(_reqId))
            {
                btnSubmit_Click(btnSubmit, new EventArgs());
            }
            btnSubmit.Visible = false;
        }

        // btnSubmit.Enabled = true;
        //}
    }

    public string FormatMessge(string messge)
    {
        if (messge == "ValidationFailure")
        {
            return "Failed";
        }
        return "Success";
    }

    protected void OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CapList cap = (CapList)e.Item.DataItem;

            Repeater rptCap = e.Item.FindControl("rptSponsor") as Repeater;
            rptCap.DataSource = cap.Caps;
            rptCap.DataBind();
        }
    }

    protected void rptSponsor_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            if (e.Item.ItemIndex == 2)
            {
                System.Web.UI.HtmlControls.HtmlContainerControl saudiMain = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("saudiMain");
                System.Web.UI.HtmlControls.HtmlContainerControl saudiDependent = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("SaudiDependent");
                System.Web.UI.HtmlControls.HtmlContainerControl nonSaudiMain = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("nonSaudiMain");
                System.Web.UI.HtmlControls.HtmlContainerControl nonSaudiDependent = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("nonSaudiDependent");

                var image = GetImage(saudiMain.InnerHtml.Trim());
                saudiMain.Controls.RemoveAt(0);
                saudiMain.Controls.Add(image);

                var image1 = GetImage(saudiDependent.InnerHtml.Trim());
                saudiDependent.Controls.RemoveAt(0);
                saudiDependent.Controls.Add(image1);

                var image2 = GetImage(nonSaudiMain.InnerHtml.Trim());
                nonSaudiMain.Controls.RemoveAt(0);
                nonSaudiMain.Controls.Add(image2);

                var image3 = GetImage(nonSaudiDependent.InnerHtml.Trim());
                nonSaudiDependent.Controls.RemoveAt(0);
                nonSaudiDependent.Controls.Add(image3);


            }
        }
    }

    private System.Web.UI.HtmlControls.HtmlImage GetImage(string value)
    {
        System.Web.UI.HtmlControls.HtmlImage image = new System.Web.UI.HtmlControls.HtmlImage();
        image.Style.Add("width", "16px");
        image.Style.Add("height", "16px");
        image.Attributes.Add("title", value);


        if (value.ToLower() == "success")
        {
            image.Src = "~/Images/success.png";
        }
        else
        {
            image.Src = "~/Images/failure.png";
        }

        return image;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //Encrypt or remove query string
        //_reqId = Request.QueryString["requestId"];
        _reqId = Convert.ToString(Session["RequestID"]);

        string str = "<br/>";

        if (!string.IsNullOrEmpty(_reqId))
        {
            var results = NormalizeManager.Normalize(Convert.ToInt32(_reqId));
			if (results.Count > 0)
			{           
			   var messges = _ceaserManager.Push(results);

				if (!messges.Any(t => t.Type == "Error"))
				{
					var result = messges.Where(t => t.Type == "Success").OrderByDescending(t => t.Index).FirstOrDefault();
					if (result != null)
					{
                        //Encrypt or remove query string
                        //_reqId = Request.QueryString["requestId"];
                        _reqId = Convert.ToString(Session["RequestID"]);

                        if (!string.IsNullOrEmpty(_reqId))
						{
							var reqId = Convert.ToInt32(_reqId);
							var request = _requestService.FindByID(reqId);
							if (request != null)
							{
								request.Modified = DateTime.Now;
								request.IsPosted = true;
								request.ReferenceNo = result.Id.ToString();
								var isSucess = _requestService.Update(request);
								if (isSucess > 0)
								{
									Response.Redirect("Request.aspx?sucess=true");
								}
								else
								{
									Response.Redirect("Request.aspx?sucess=false");
								}
							}
						}
					}
				}
				foreach (var item in messges)
				{
					var error = item;

					str += error.Id + "||" + error.Type + "||" + error.Messege;
				}
				lblMessge = str;
			}
		}



    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("request.aspx");
    }
    private void CCHIResponseLog(List<CapList> CCHIRes, string requestID)
    {
        try
        {
            foreach (var res in CCHIRes)
            {
                logger.Info("RequestID:" + requestID + " Description:" + res.ErrorDetails.Description + " StatusCode:" + res.ErrorDetails.StatusCode + " ServiceResult:" + res.ErrorDetails.ServiceResult);
                foreach (var cap in res.Caps)
                {
                    
                    if (cap.CapType == CapType.MinimumRequired)
                    {
                        logger.Info("CCHI: " + "RequestID:" + requestID + " SponsorID:" + cap.SponsorId + " SaudiMain:" + cap.SaudiMain + " SaudiDependent:" + cap.SaudiDependent + " NonSaudiMain:" + cap.NonSaudiMain + " NonSaudiDependent:" + cap.NonSaudiDependent);
                    }
                    if (cap.CapType == CapType.Available)
                    {
                        logger.Info("OS: " + "RequestID:" + requestID + " SponsorID:" + cap.SponsorId + " SaudiMain:" + cap.SaudiMain + " SaudiDependent:" + cap.SaudiDependent + " NonSaudiMain:" + cap.NonSaudiMain + " NonSaudiDependent:" + cap.NonSaudiDependent);
                    }                  
                    if (cap.CapType == CapType.Remarks)
                    {
                        logger.Info("Remarks: " + "RequestID:" + requestID + " SponsorID:" + cap.SponsorId + " SaudiMain:" + cap.SaudiMain + " SaudiDependent:" + cap.SaudiDependent + " NonSaudiMain:" + cap.NonSaudiMain + " NonSaudiDependent:" + cap.NonSaudiDependent);
                    }

                }
            }


        }
        catch (Exception ex)
        {
            logger.Error("Error While Logging CCHI Response " + ex.Message + " " + ex.InnerException);
        }
    }
}