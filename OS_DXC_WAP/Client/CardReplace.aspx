﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master"   AutoEventWireup="true" Inherits="CardReplace" Codebehind="CardReplace.aspx.cs" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static" >


    <script type='text/JavaScript' src='scw.js'></script>
    <style type="text/css" media="screen">
        .DatePicker
        {
            background: url(images/cal.gif) no-repeat right center;
        }
        DIV.DatePicker-Container
        {
            font-size: 9px;
        }
        DIV.DatePicker-Wrapper
        {
            border-right: #e0e0e0 1px solid;
            border-top: #e0e0e0 1px solid;
            border-left: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
            background-color: skyblue;
        }
        DIV.DatePicker-Wrapper TABLE
        {
        }
        DIV.DatePicker-Wrapper TD
        {
            padding-right: 0px;
            padding-left: 0px;
            padding-bottom: 0px;
            padding-top: 0px;
        }
        DIV.DatePicker-Wrapper TD.nav
        {
        }
        DIV.DatePicker-Wrapper TD.dayName
        {
            border-right: #e0e0e0 1px solid;
            font-weight: bold;
            border-bottom: #e0e0e0 1px solid;
            background-color: #eeeeee;
        }
        DIV.DatePicker-Wrapper TD.day
        {
            border-right: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
        }
        DIV.DatePicker-Wrapper TD.empty
        {
            border-right: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
        }
        DIV.DatePicker-Wrapper TD.current
        {
            color: #ffffff;
            background-color: #666666;
        }
        DIV.DatePicker-Wrapper TD.dp_roll
        {
            background-color: #e0e0e0;
        }
    </style>
    

    <div>
        <%--<asp:ScriptManager runat="server" ID="scm1"></asp:ScriptManager>--%>
    <fieldset style="padding:10px">
                <legend><h1>
                <asp:Label ID="lblAdd_Employee" runat="server" Text="Replace Card and Data Correction " Font-Size="Large"
                        Font-Names="Arial"></asp:Label>
                </h1></legend>                 
        
        <asp:Label ID="lblAdd_Employee1" runat="server" Text="" Font-Bold="True" Width="185px"></asp:Label>
        <div align="left"><asp:Label ID="lblAramcoMessage" runat="server" Font-Size="Small" ForeColor="Black" Text="One Field is Mandatory Either the badge or membership"></asp:Label></div>
        <div align="right"><asp:HyperLink ID="lnkBatchUpload" runat="server" Font-Size="Small"  Target="_self" Visible="true">Batch Upload</asp:HyperLink></div>
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Black" Text="Fields marked with asterisk [ * ]  are mandatory."></asp:Label>
        

        <%if (uploader.LblUpload.Text.Length > 0)
  { %>
        <br />
        <br />
        <table style="width: 750px; font-size: 17px; font-family: Arial; border-right: black 0.5pt solid;
            border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid;
            border-collapse: collapse;" runat="server" id="Table3">
            <tr>
                <td align="left">
                    <span style="color: #cc0000"><%= uploader.LblUpload.Text%></span>
                </td>
            </tr>
        </table>
        <br />
        <br />
        
<% }%>
        <div style="text-align: left;">
        <asp:Image ID="imgSuc" runat="server" Visible="false" ImageUrl="~/images/reimp_req_suc.png" /><br />
        <asp:Label ID="Message1" runat="server" Width="527px" Font-Size="Small" ForeColor="Green"></asp:Label>
            </div>
                <br />
                <br />
                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                    <br />
                    <br />
        <asp:Panel ID="Panel1" runat="server" Width="100%">
        <table style="font-size: small; width: 100%;" >
          <tr>
                    <td >
                        <span style="color: #ff0000">*</span><asp:Label ID="lblGender" runat="server" Text="Replacement Reason"
                           ></asp:Label>
                    </td>
                    <td >
                        <div class="styled-select">
<div class="styled-select">
    <asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlRepReason" runat="server" Width="277px" BackColor="#eff6fc" AutoPostBack="true"  OnSelectedIndexChanged="ddlRepReason_SelectedIndexChanged">
                            <asp:ListItem Value="">-Select-</asp:ListItem>
                            <asp:ListItem Value="REP001">Lost Card</asp:ListItem>
                            <asp:ListItem Value="REP002">Damaged Card</asp:ListItem>
                            <asp:ListItem Value="REP003" Enabled="False">Data Correction</asp:ListItem>
                            <asp:ListItem Value="REP004">Wrong Member Name</asp:ListItem>
                            <asp:ListItem Value="REP005">Wrong Date of birth</asp:ListItem>
                            <asp:ListItem Value="REP006">Wrong Employee No.</asp:ListItem>
                            <asp:ListItem Value="REP007">Wrong Nationality</asp:ListItem>
                           <%-- <asp:ListItem Value="REP008">Wrong Saudi ID/Iqama</asp:ListItem>--%>
                            <asp:ListItem Value="REP009">Wrong Gender</asp:ListItem>
                           <%-- <asp:ListItem Value="REP010">Change Entry Number to IQAMA</asp:ListItem>--%>
                            <asp:ListItem Value="REP012">Wrong Member Type</asp:ListItem>
                            <asp:ListItem Value="REP013">Wrong ID Type</asp:ListItem>
                            <asp:ListItem Value="REP014">Wrong ID Expiry Date</asp:ListItem>
                            <asp:ListItem Value="REP015">Wrong Profession</asp:ListItem>
                            <asp:ListItem Value="REP016">Wrong District</asp:ListItem>
                            <asp:ListItem Value="REP017">Wrong Mobile</asp:ListItem>
                            <asp:ListItem Value="REP018">Complete New CCHI Requirements</asp:ListItem>
                        </asp:DropDownList></div></div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlRepReason"
                            Display="Dynamic" ErrorMessage="Field is mandatory" InitialValue="" Width="162px"></asp:RequiredFieldValidator>
                    </td>
                </tr>
               <tr>
                    <td >
                        <span style="color: #ff0000">*</span>Membership No&nbsp; 
                        
                    </td>
                    <td >
                        <asp:TextBox ID="txtMembershipNo" runat="server" CssClass="textbox"  onchange="changetext('RFVtxtBadgeNo')"
                            MaxLength="8" Width="119px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button2" runat="server" CssClass="submitButton" 
                            onclick="Button1_Click1" Text="Get Details" CausesValidation="true" /><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                            ControlToValidate="txtMembershipNo" Display="Dynamic" 
                            ErrorMessage="Field is mandatory" Width="162px"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressByMemberNumber" runat="server" ControlToValidate="txtMembershipNo" Display="Dynamic" ErrorMessage="Only numbers with hyphen."
                                                    ValidationExpression="^\d+$" />
                        
                    </td>
                </tr>
                    <tr id="DivStafflbl" runat="server" style="display:table-row;">
                        <td >
                            <span style="color: #ff0000">*</span>Badge No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                       
                        <td>
                           <asp:TextBox ID="txtBadgeNo" runat="server" CssClass="textbox"  Width="119px" onchange="changetext('RequiredFieldValidator13')"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="RFVtxtBadgeNo" runat="server" ControlToValidate="txtBadgeNo" Display="Dynamic" ErrorMessage="Field is mandatory" Width="162px"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegExpValBadgeNo" runat="server" ControlToValidate="txtBadgeNo" Display="Dynamic" ErrorMessage="Only numbers with hyphen."
                                                    ValidationExpression="\d*-?\d*" />
                        </td>
            </tr>
        </table>
            <asp:UpdatePanel runat="server" ID="updatePanel2">
                <ContentTemplate>
                    <div id="entryForm">
            <table style="font-size: small; width: 89%;" runat="server" id="details" visible="false" >
                
                <tr>
                    <td style="width: 245px; height: 16px;">
                        <asp:Label ID="lblCompleteName"
                            runat="server" Text="Complete Name(First, Middle, Last)" Width="206px"></asp:Label>
                    </td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label  ID="txtMemberName" runat="server" Width="271px" 
                            style="font-weight: 700" ></asp:Label>
                    </td>
                </tr>
                
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Gender</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblcGender" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Nationality</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblNationality" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Title</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblTitle" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Date of Birth</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblDOB" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Iqama ID</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblID" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Sponsor ID</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblSponsor" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Employee ID</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblEmpID" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current ID Type</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblIDType" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current ID Expiry Date</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblExpiryDate" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Profession</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblProfession" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current District</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblDistrict" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px; height: 16px;">
                        Current Mobile</td>
                    <td style="width: 275px; height: 16px;">
                        <asp:Label ID="lblMobile" runat="server" style="font-weight: 700" 
                            Width="271px"></asp:Label>
                    </td>
                </tr>
                <tr id="CorrectGender" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label6" runat="server" Text="Correct Gender" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px"> <div class="styled-select">
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlEGender" runat="server" BackColor="#eff6fc" Width="277px">
                            <asp:ListItem>-Select-</asp:ListItem>
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList></div></div>
                        <asp:RequiredFieldValidator ID="rfvGender" runat="server" ControlToValidate="ddlEGender"
                            Display="Dynamic" Enabled="false" ErrorMessage="RequiredFieldValidator" InitialValue="-Select-"
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="CorrectNationality" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label7" runat="server" Text="Correct Nationality" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                         <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlENationality" runat="server" BackColor="#eff6fc" DataTextField="text" DataValueField="value" EnableViewState="true" Width="277px">
                            <asp:ListItem Value="">-Select-</asp:ListItem>
                            <%--<asp:ListItem Value="093">Afghanistan</asp:ListItem>
                            <asp:ListItem Value="355">Albania</asp:ListItem>
                            <asp:ListItem Value="213">Algeria</asp:ListItem>
                            <asp:ListItem Value="684">American Samoa</asp:ListItem>
                            <asp:ListItem Value="376">Andorra</asp:ListItem>
                            <asp:ListItem Value="244">Angola</asp:ListItem>
                            <asp:ListItem Value="101">Anguilla</asp:ListItem>
                            <asp:ListItem Value="102">Antigua and Barbuda</asp:ListItem>
                            <asp:ListItem Value="054">Argentine Republic</asp:ListItem>
                            <asp:ListItem Value="374">Armenia</asp:ListItem>
                            <asp:ListItem Value="297">Aruba</asp:ListItem>
                            <asp:ListItem Value="247">Ascension</asp:ListItem>
                            <asp:ListItem Value="061">Australia</asp:ListItem>
                            <asp:ListItem Value="672">Australian External Territories</asp:ListItem>
                            <asp:ListItem Value="043">Austria</asp:ListItem>
                            <asp:ListItem Value="994">Azerbaijani Republic</asp:ListItem>
                            <asp:ListItem Value="000">Bedoun</asp:ListItem>
                            <asp:ListItem Value="103">Bahamas</asp:ListItem>
                            <asp:ListItem Value="973">Bahrain</asp:ListItem>
                            <asp:ListItem Value="880">Bangladesh</asp:ListItem>
                            <asp:ListItem Value="104">Barbados</asp:ListItem>
                            <asp:ListItem Value="375">Belarus</asp:ListItem>
                            <asp:ListItem Value="032">Belgium</asp:ListItem>
                            <asp:ListItem Value="501">Belize</asp:ListItem>
                            <asp:ListItem Value="229">Benin</asp:ListItem>
                            <asp:ListItem Value="105">Bermuda</asp:ListItem>
                            <asp:ListItem Value="975">Bhutan</asp:ListItem>
                            <asp:ListItem Value="591">Bolivia</asp:ListItem>
                            <asp:ListItem Value="387">Bosnia and Herzegovina</asp:ListItem>
                            <asp:ListItem Value="267">Botswana</asp:ListItem>
                            <asp:ListItem Value="055">Brazil</asp:ListItem>
                            <asp:ListItem Value="106">British Virgin Islands</asp:ListItem>
                            <asp:ListItem Value="673">Brunei Darussalam</asp:ListItem>
                            <asp:ListItem Value="359">Bulgaria</asp:ListItem>
                            <asp:ListItem Value="226">Burkina Faso</asp:ListItem>
                            <asp:ListItem Value="257">Burundi</asp:ListItem>
                            <asp:ListItem Value="855">Cambodia</asp:ListItem>
                            <asp:ListItem Value="237">Cameroon</asp:ListItem>
                            <asp:ListItem Value="107">Canada</asp:ListItem>
                            <asp:ListItem Value="238">Cape Verde</asp:ListItem>
                            <asp:ListItem Value="108">Cayman Islands</asp:ListItem>
                            <asp:ListItem Value="236">Central African Republic</asp:ListItem>
                            <asp:ListItem Value="235">Chad</asp:ListItem>
                            <asp:ListItem Value="056">Chile</asp:ListItem>
                            <asp:ListItem Value="086">China</asp:ListItem>
                            <asp:ListItem Value="057">Colombia</asp:ListItem>
                            <asp:ListItem Value="269">Comoros</asp:ListItem>
                            <asp:ListItem Value="242">Congo</asp:ListItem>
                            <asp:ListItem Value="682">Cook Islands</asp:ListItem>
                            <asp:ListItem Value="506">Costa Rica</asp:ListItem>
                            <asp:ListItem Value="225">Cote d'lvoire</asp:ListItem>
                            <asp:ListItem Value="385">Croatia</asp:ListItem>
                            <asp:ListItem Value="053">Cuba</asp:ListItem>
                            <asp:ListItem Value="357">Cyprus</asp:ListItem>
                            <asp:ListItem Value="420">Czech Republic</asp:ListItem>
                            <asp:ListItem Value="850">South Korea</asp:ListItem>
                            <asp:ListItem Value="243">Democratic Republic Congo</asp:ListItem>
                            <asp:ListItem Value="045">Denmark</asp:ListItem>
                            <asp:ListItem Value="246">Diego Garcia</asp:ListItem>
                            <asp:ListItem Value="253">Djibouti</asp:ListItem>
                            <asp:ListItem Value="109">Dominica</asp:ListItem>
                            <asp:ListItem Value="110">Dominican Republic</asp:ListItem>
                            <asp:ListItem Value="593">Ecuador</asp:ListItem>
                            <asp:ListItem Value="020">Egypt</asp:ListItem>
                            <asp:ListItem Value="503">El Salvador</asp:ListItem>
                            <asp:ListItem Value="240">Equatorial Guinea</asp:ListItem>
                            <asp:ListItem Value="291">Eritrea</asp:ListItem>
                            <asp:ListItem Value="372">Estonia</asp:ListItem>
                            <asp:ListItem Value="251">Ethiopia</asp:ListItem>
                            <asp:ListItem Value="500">Falkland Islands</asp:ListItem>
                            <asp:ListItem Value="298">Faroe Islands</asp:ListItem>
                            <asp:ListItem Value="679">Fiji</asp:ListItem>
                            <asp:ListItem Value="358">Finland</asp:ListItem>
                            <asp:ListItem Value="033">France</asp:ListItem>
                            <asp:ListItem Value="594">French Guiana</asp:ListItem>
                            <asp:ListItem Value="689">French Polynesia</asp:ListItem>
                            <asp:ListItem Value="241">Garbonese Republic</asp:ListItem>
                            <asp:ListItem Value="220">Gambia</asp:ListItem>
                            <asp:ListItem Value="995">Georgia</asp:ListItem>
                            <asp:ListItem Value="049">Germany</asp:ListItem>
                            <asp:ListItem Value="233">Ghana</asp:ListItem>
                            <asp:ListItem Value="350">Gibraltar</asp:ListItem>
                            <asp:ListItem Value="030">Greece</asp:ListItem>
                            <asp:ListItem Value="299">Greenland</asp:ListItem>
                            <asp:ListItem Value="111">Grenada</asp:ListItem>
                            <asp:ListItem Value="590">Guadeloupe</asp:ListItem>
                            <asp:ListItem Value="112">Guam</asp:ListItem>
                            <asp:ListItem Value="502">Guatemala</asp:ListItem>
                            <asp:ListItem Value="224">Guinea</asp:ListItem>
                            <asp:ListItem Value="245">Guinea-Bissau</asp:ListItem>
                            <asp:ListItem Value="592">Guyana</asp:ListItem>
                            <asp:ListItem Value="509">Haiti</asp:ListItem>
                            <asp:ListItem Value="504">Honduras</asp:ListItem>
                            <asp:ListItem Value="852">Hongkong</asp:ListItem>
                            <asp:ListItem Value="036">Hungary</asp:ListItem>
                            <asp:ListItem Value="354">Iceland</asp:ListItem>
                            <asp:ListItem Value="091">India</asp:ListItem>
                            <asp:ListItem Value="062">Indonesia</asp:ListItem>
                            <asp:ListItem Value="098">Iran</asp:ListItem>
                            <asp:ListItem Value="964">Iraq</asp:ListItem>
                            <asp:ListItem Value="353">Ireland</asp:ListItem>
                            <asp:ListItem Value="039">Italy</asp:ListItem>
                            <asp:ListItem Value="113">Jamaica</asp:ListItem>
                            <asp:ListItem Value="081">Japan</asp:ListItem>
                            <asp:ListItem Value="962">Jordan</asp:ListItem>
                            <asp:ListItem Value="701">Kazakstan</asp:ListItem>
                            <asp:ListItem Value="254">Kenya</asp:ListItem>
                            <asp:ListItem Value="686">Kiribati</asp:ListItem>
                            <asp:ListItem Value="082">Korea</asp:ListItem>
                            <asp:ListItem Value="965">Kuwait</asp:ListItem>
                            <asp:ListItem Value="996">Kyrgyz Republic</asp:ListItem>
                            <asp:ListItem Value="856">Lao People's Demcr Republic</asp:ListItem>
                            <asp:ListItem Value="371">Latvia</asp:ListItem>
                            <asp:ListItem Value="961">Lebanon</asp:ListItem>
                            <asp:ListItem Value="266">Lesotho</asp:ListItem>
                            <asp:ListItem Value="231">Liberia</asp:ListItem>
                            <asp:ListItem Value="218">Libya</asp:ListItem>
                            <asp:ListItem Value="423">Liechtenstein</asp:ListItem>
                            <asp:ListItem Value="370">Lithuania</asp:ListItem>
                            <asp:ListItem Value="352">Luxembourg</asp:ListItem>
                            <asp:ListItem Value="853">Macau</asp:ListItem>
                            <asp:ListItem Value="261">Madagascar</asp:ListItem>
                            <asp:ListItem Value="265">Malawi</asp:ListItem>
                            <asp:ListItem Value="060">Malaysia</asp:ListItem>
                            <asp:ListItem Value="960">Maldives</asp:ListItem>
                            <asp:ListItem Value="223">Mali</asp:ListItem>
                            <asp:ListItem Value="356">Malta</asp:ListItem>
                            <asp:ListItem Value="692">Marshall Islands</asp:ListItem>
                            <asp:ListItem Value="596">Martinique</asp:ListItem>
                            <asp:ListItem Value="222">Mauritania</asp:ListItem>
                            <asp:ListItem Value="230">Mauritius</asp:ListItem>
                            <asp:ListItem Value="052">Mexico</asp:ListItem>
                            <asp:ListItem Value="691">Micronesia</asp:ListItem>
                            <asp:ListItem Value="373">Moldova</asp:ListItem>
                            <asp:ListItem Value="377">Monaco</asp:ListItem>
                            <asp:ListItem Value="976">Mongolia</asp:ListItem>
                            <asp:ListItem Value="114">Montserrat</asp:ListItem>
                            <asp:ListItem Value="212">Morocco</asp:ListItem>
                            <asp:ListItem Value="258">Mozambique</asp:ListItem>
                            <asp:ListItem Value="095">Myanmar</asp:ListItem>
                            <asp:ListItem Value="264">Namibia</asp:ListItem>
                            <asp:ListItem Value="674">Nauru</asp:ListItem>
                            <asp:ListItem Value="977">Nepal</asp:ListItem>
                            <asp:ListItem Value="031">Netherlands</asp:ListItem>
                            <asp:ListItem Value="599">Netherlands Antilles</asp:ListItem>
                            <asp:ListItem Value="687">New Caledonia</asp:ListItem>
                            <asp:ListItem Value="064">New Zealand</asp:ListItem>
                            <asp:ListItem Value="505">Nicaragua</asp:ListItem>
                            <asp:ListItem Value="227">Niger</asp:ListItem>
                            <asp:ListItem Value="234">Nigeria</asp:ListItem>
                            <asp:ListItem Value="683">Niue</asp:ListItem>
                            <asp:ListItem Value="115">Northern Mariana Islands</asp:ListItem>
                            <asp:ListItem Value="047">Norway</asp:ListItem>
                            <asp:ListItem Value="968">Oman</asp:ListItem>
                            <asp:ListItem Value="092">Pakistan</asp:ListItem>
                            <asp:ListItem Value="680">Palau</asp:ListItem>
                            <asp:ListItem Value="972">Palestine</asp:ListItem>
                            <asp:ListItem Value="507">Panama</asp:ListItem>
                            <asp:ListItem Value="675">Papua New Guinea</asp:ListItem>
                            <asp:ListItem Value="595">Paraguay</asp:ListItem>
                            <asp:ListItem Value="051">Peru</asp:ListItem>
                            <asp:ListItem Value="063">Philippines</asp:ListItem>
                            <asp:ListItem Value="048">Poland</asp:ListItem>
                            <asp:ListItem Value="351">Portugal</asp:ListItem>
                            <asp:ListItem Value="116">Puerto Rico</asp:ListItem>
                            <asp:ListItem Value="974">Qatar</asp:ListItem>
                            <asp:ListItem Value="262">Reunion</asp:ListItem>
                            <asp:ListItem Value="040">Romania</asp:ListItem>
                            <asp:ListItem Value="702">Russian Federation</asp:ListItem>
                            <asp:ListItem Value="250">Rwandese Republic</asp:ListItem>
                            <asp:ListItem Value="290">Saint Helena</asp:ListItem>
                            <asp:ListItem Value="117">Saint Kitts and Nevis</asp:ListItem>
                            <asp:ListItem Value="118">Saint Lucia</asp:ListItem>
                            <asp:ListItem Value="508">Saint Pierre and Miquelon</asp:ListItem>
                            <asp:ListItem Value="119">Saint Vincent and Grenadines</asp:ListItem>
                            <asp:ListItem Value="670">Saipan</asp:ListItem>
                            <asp:ListItem Value="685">Samoa</asp:ListItem>
                            <asp:ListItem Value="378">San Marino</asp:ListItem>
                            <asp:ListItem Value="239">Sao Tome and Principe</asp:ListItem>
                            <asp:ListItem Value="966">Saudi Arabia</asp:ListItem>
                            <asp:ListItem Value="221">Senegal</asp:ListItem>
                            <asp:ListItem Value="248">Seychelles</asp:ListItem>
                            <asp:ListItem Value="232">Sierra Leone</asp:ListItem>
                            <asp:ListItem Value="065">Singapore</asp:ListItem>
                            <asp:ListItem Value="421">Slovak Republic</asp:ListItem>
                            <asp:ListItem Value="386">Slovenia</asp:ListItem>
                            <asp:ListItem Value="677">Solomon Islands</asp:ListItem>
                            <asp:ListItem Value="252">Somali Democratic Republic</asp:ListItem>
                            <asp:ListItem Value="027">South Africa</asp:ListItem>
                            <asp:ListItem Value="034">Spain</asp:ListItem>
                            <asp:ListItem Value="094">Sri Lanka</asp:ListItem>
                            <asp:ListItem Value="249">Sudan</asp:ListItem>
                            <asp:ListItem Value="597">Suriname</asp:ListItem>
                            <asp:ListItem Value="268">Swaziland</asp:ListItem>
                            <asp:ListItem Value="046">Sweden</asp:ListItem>
                            <asp:ListItem Value="041">Switzerland</asp:ListItem>
                            <asp:ListItem Value="963">Syrian Arab Republic</asp:ListItem>
                            <asp:ListItem Value="886">Taiwan</asp:ListItem>
                            <asp:ListItem Value="992">Tajikistan</asp:ListItem>
                            <asp:ListItem Value="255">Tanzania</asp:ListItem>
                            <asp:ListItem Value="066">Thailand</asp:ListItem>
                            <asp:ListItem Value="389">Former Yugoslav Rep Macedonia</asp:ListItem>
                            <asp:ListItem Value="671">Tinian</asp:ListItem>
                            <asp:ListItem Value="228">Togolese Republic</asp:ListItem>
                            <asp:ListItem Value="690">Tokelau</asp:ListItem>
                            <asp:ListItem Value="676">Tonga</asp:ListItem>
                            <asp:ListItem Value="120">Trinidad and Tobago</asp:ListItem>
                            <asp:ListItem Value="216">Tunisia</asp:ListItem>
                            <asp:ListItem Value="090">Turkey</asp:ListItem>
                            <asp:ListItem Value="993">Turkmenistan</asp:ListItem>
                            <asp:ListItem Value="121">Turks and Caicos Islands</asp:ListItem>
                            <asp:ListItem Value="688">Tuvalu</asp:ListItem>
                            <asp:ListItem Value="971">U.A.E.</asp:ListItem>
                            <asp:ListItem Value="044">U.K.</asp:ListItem>
                            <asp:ListItem Value="001">U.S.A.</asp:ListItem>
                            <asp:ListItem Value="256">Uganda</asp:ListItem>
                            <asp:ListItem Value="380">Ukraine</asp:ListItem>
                            <asp:ListItem Value="122">United States Virgin Islands</asp:ListItem>
                            <asp:ListItem Value="598">Uruguay</asp:ListItem>
                            <asp:ListItem Value="998">Uzbekistan</asp:ListItem>
                            <asp:ListItem Value="678">Vanuatu</asp:ListItem>
                            <asp:ListItem Value="379">Vatican City State</asp:ListItem>
                            <asp:ListItem Value="058">Venezuela</asp:ListItem>
                            <asp:ListItem Value="084">Vietnam</asp:ListItem>
                            <asp:ListItem Value="681">Wallis and Futuna</asp:ListItem>
                            <asp:ListItem Value="967">Yemen</asp:ListItem>
                            <asp:ListItem Value="381">Yugoslavia</asp:ListItem>
                            <asp:ListItem Value="260">Zambia</asp:ListItem>
                            <asp:ListItem Value="263">Zimbabwe</asp:ListItem>--%>
                        </asp:DropDownList></div>
                        <asp:RequiredFieldValidator ID="rfvNationality" runat="server" ControlToValidate="ddlENationality"
                            Display="Dynamic" Enabled="false" ErrorMessage="RequiredFieldValidator" InitialValue=""
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="CorrectTitle" runat="server">
                    <td style="width: 245px; height: 32px;">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label2" runat="server" Text="Correct Title" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px; height: 32px;">
                         <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlETitle" runat="server" BackColor="#eff6fc" DataTextField="text" DataValueField="value" EnableViewState="true" Width="277px">
                            <asp:ListItem Value="">-Select-</asp:ListItem>
                            <asp:ListItem>Dr</asp:ListItem>
                            <asp:ListItem>Eng</asp:ListItem>
                            <asp:ListItem>HH</asp:ListItem>
                            <asp:ListItem>Miss</asp:ListItem>
                            <asp:ListItem Selected="True">Mr</asp:ListItem>
                            <asp:ListItem>Mrs</asp:ListItem>
                            <asp:ListItem>Ms</asp:ListItem>
                            <asp:ListItem>Shk</asp:ListItem>
                            <asp:ListItem>Sir</asp:ListItem>                        
                        </asp:DropDownList></div>
                        <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="ddlETitle"
                            Display="Dynamic" Enabled="false" ErrorMessage="RequiredFieldValidator" InitialValue="-Select-"
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="CorrectName" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label3" runat="server" Text="Correct Name" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <asp:TextBox CssClass="textbox" ID="txtCorrectName" runat="server" BackColor="#eff6fc" MaxLength="30"
                            Width="271px"></asp:TextBox>&nbsp;&nbsp;
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtCorrectName"
                            Display="Dynamic" Enabled="false" ErrorMessage="RequiredFieldValidator" InitialValue=""
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="CorrectDateOfBirth" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="lblDateOfBirth" runat="server" Text="Correct Date of Birth" Width="147px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <asp:TextBox CssClass="textbox" ID="txtCorrectDOB" Style="cursor: default;" runat="server" Width="271px"
                            onfocus="this.blur();" onclick="scwShow(this,event);" BackColor="#eff6fc"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCorrectDOB"
                            Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{2}/\d{2}/\d{4}">Invalid Date</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="rfvDOB" runat="server" ControlToValidate="txtCorrectDOB"
                            Display="Dynamic" Enabled="false" ErrorMessage="RequiredFieldValidator" InitialValue=""
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="IDType" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label10" runat="server" Text="ID Type" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlIDType" 
                                ClientIDMode="Static" runat="server" 
                                    BackColor="#eff6fc" width="271px" AutoPostBack="true" OnSelectedIndexChanged="ddlIDType_SelectedIndexChanged">
                                <asp:ListItem Value="0">-Select-</asp:ListItem>
                                <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Passport" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvIDType" runat="server" ControlToValidate="ddlIDType" InitialValue="0"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="115px" Enabled="false">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="CorrectIqamaID" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label4" runat="server" Text="Correct IqamaID" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <asp:TextBox CssClass="textbox" ID="txtCorrectIqamaID" runat="server" BackColor="#eff6fc" MaxLength="10"
                            Width="271px"></asp:TextBox><br />
                        <asp:RegularExpressionValidator
                                ID="rfvSuadiID" runat="server" ControlToValidate="txtCorrectIqamaID"
                                Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{10}"
                                Width="142px">Incorrect Saudi/Iqama ID</asp:RegularExpressionValidator>&nbsp;&nbsp;
                        <asp:RequiredFieldValidator ID="rfvIqamaID" runat="server" ControlToValidate="txtCorrectIqamaID"
                            Display="Dynamic" Enabled="false" ErrorMessage="RequiredFieldValidator" InitialValue=""
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="CorrectSponsorID" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label5" runat="server" Text="Correct SponsorID" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <asp:TextBox CssClass="textbox" ID="txtCorrectSponsorID" runat="server" BackColor="#eff6fc" MaxLength="10"
                            Width="271px"></asp:TextBox><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                            ControlToValidate="txtCorrectSponsorID" Display="Dynamic" ErrorMessage="Incorrect Sponsor ID"
                            ValidationExpression="[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]" Width="131px"></asp:RegularExpressionValidator>&nbsp;&nbsp;
                        <asp:RequiredFieldValidator ID="rfvSponsorID" runat="server" ControlToValidate="txtCorrectSponsorID"
                            Display="Dynamic" Enabled="false" ErrorMessage="RequiredFieldValidator" InitialValue=""
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="EmployeeNo" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label8" runat="server" Text="Employee No." Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <asp:TextBox CssClass="textbox" ID="txtEmployeeNo" runat="server" BackColor="#eff6fc" MaxLength="10" Width="271px"></asp:TextBox>&nbsp;&nbsp;
                        <asp:RequiredFieldValidator ID="rfvEmployeeNo" runat="server" ControlToValidate="txtEmployeeNo"
                            Display="Dynamic" Enabled="false" ErrorMessage="RequiredFieldValidator" InitialValue=""
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="MemberType" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label9" runat="server" Text="Member Type" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlMemberType" 
                                ClientIDMode="Static" runat="server" Width="271px"
                                DataTextField="mbrTypeDesc" DataValueField="mbrType" BackColor="#eff6fc" >
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvMemberType" runat="server" ControlToValidate="ddlMemberType" InitialValue="0"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="EffectiveDate" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label16" runat="server" Text="Effective Date" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                       <asp:TextBox CssClass="textbox"  ID="txtEffectiveDate" runat="server" onfocus="this.blur();"
                            onclick="scwShow(this,event);" Style="cursor: default;" BackColor="#eff6fc" width="271px"></asp:TextBox>
                        <asp:RequiredFieldValidator
                                ID="rfvEffectiveDate" runat="server" ControlToValidate="txtEffectiveDate"
                                ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" Enabled="false"
                                Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="142px"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                
                <tr id="IDExpiryDate" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label11" runat="server" Text="ID Expiry Date" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                       <asp:TextBox CssClass="textbox"  ID="txtIDExpDate" runat="server" onfocus="this.blur();"
                            onclick="scwShow(this,event);" Style="cursor: default;" BackColor="#eff6fc" width="271px"></asp:TextBox>
                        <asp:RequiredFieldValidator
                                ID="rfvIDExpDate" runat="server" ControlToValidate="txtIDExpDate"
                                ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" Enabled="false"
                                Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="142px"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="MaritalStatus" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label12" runat="server" Text="Marital Status" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlMaritalStatus" 
                                ClientIDMode="Static" runat="server" 
                                 BackColor="#eff6fc" width="271px">
                                <asp:ListItem Value="0">-Select-</asp:ListItem>
                                <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvMaritalStatus" runat="server" ControlToValidate="ddlMaritalStatus" InitialValue="0" Enabled="false"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="Profession" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label13" runat="server" Text="Profession" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <div class="styled-select">
                        <asp:DropDownList ID="ddProfession" runat="server"  BackColor="#eff6fc"  DataValueField="profCode" DataTextField="profName" width="271px"></asp:DropDownList></div>
                        <asp:RequiredFieldValidator ID="rfvProfession" runat="server" ControlToValidate="ddProfession"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" 
                            Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="District" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label14" runat="server" Text="District" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <div class="styled-select">
                            <asp:DropDownList ID="ddDistrict" runat="server" BackColor="#eff6fc" DataValueField="distCode" DataTextField="distName" width="271px">
                                <asp:ListItem Value=" "></asp:ListItem>
                            </asp:DropDownList></div>  
                        <asp:RequiredFieldValidator ID="rfvDistrict" runat="server" ControlToValidate="ddDistrict"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0"
                            Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="MobileNo" runat="server">
                    <td style="width: 245px">
                        <span style="color: #ff0000"></span>
                        <asp:Label ID="Label15" runat="server" Text="Mobile No." Width="145px"></asp:Label>
                    </td>
                    <td style="width: 275px">
                        <div class="styled-select"><asp:TextBox CssClass="textbox"  ID="txtMobileNo" runat="server" MaxLength="10" BackColor="White" width="271px"></asp:TextBox></div>
                        <asp:RegularExpressionValidator ID="revMobile" runat="server"
                            ControlToValidate="txtMobileNo" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                            ValidationExpression="[05][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]" 
                            Width="116px" Enabled="false">Incorrect Mobile No.</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="rfvMobile" runat="server" ControlToValidate="txtMobileNo"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue=""
                            Width="120px" Enabled="false">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px">
                    </td>
                    <td style="width: 275px">
                        <div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 245px">
                    </td>
                    <td style="width: 275px" align="right">
                    </td>
                </tr>
                </table>
                </div>
                </ContentTemplate>
            </asp:UpdatePanel>
                <table style="width:100%;height:auto">
                <tr>
                    <td colspan="2">
                      <br />
            <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid;
                border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid;
                border-collapse: collapse;" runat="server" id="tblUploader">
                <tr>
                    <td align="left">
                        <span style="color: #ff3300"><strong><asp:Label ID="lblAsterix" Text="*" Visible="false" runat="server" /></strong></span><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                    </td>
                    <td align="right">
                        <a href="<%= Page.ResolveUrl("~/Docs/ReqDocs2012.pdf") %>" target="_blank">Required Document List</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .pmp) 5MB limit per file" /><br />
                        <bupa:uploader runat="server" id="uploader" />
                    </td>
                </tr>
            </table>
            <p align="right">
                <asp:Button CssClass="submitButton" ID="Button1" runat="server" OnClick="Button1_Click" Text="Submit Request" Width="133px" />
            </p>
                    </td>
                </tr>
            </table>
          
        </asp:Panel>
    </div>
    &nbsp; &nbsp; &nbsp; &nbsp;<asp:HyperLink ID="lnkChangeOption" runat="server" Font-Size="Small"
        NavigateUrl="~/Client/cardreplace.aspx" Visible="False">Card replacement for another Employee</asp:HyperLink>
   
    <script type="text/javascript">
<!--

        function ActivateRequiredFields(Reason) {
            document.getElementById('rfvGender').enabled = false;
            document.getElementById('rfvNationality').enabled = false;
            // document.getElementById('rfvTitle').enabled = false;
            document.getElementById('rfvName').enabled = false;
            document.getElementById('rfvDOB').enabled = false;
            document.getElementById('rfvIqamaID').enabled = false;
            // document.getElementById('rfvSponsorID').enabled = false;
            document.getElementById('rfvEmployeeNo').enabled = false;


            document.getElementById('ddlEGender').style.backgroundColor = '#FFFFFF';
            document.getElementById('ddlENationality').style.backgroundColor = '#FFFFFF';
            document.getElementById('ddlETitle').style.backgroundColor = '#FFFFFF';
            document.getElementById('txtCorrectName').style.backgroundColor = '#FFFFFF';
            document.getElementById('txtCorrectDOB').style.backgroundColor = '#FFFFFF';
            document.getElementById('txtCorrectIqamaID').style.backgroundColor = '#FFFFFF';
            //   document.getElementById('txtCorrectSponsorID').style.backgroundColor = '#FFFFFF';
            document.getElementById('txtEmployeeNo').style.backgroundColor = '#FFFFFF';




            //alert(Reason);

            if (Reason == 'REP004') {
                document.getElementById('rfvName').enabled = true;
                document.getElementById('txtCorrectName').style.backgroundColor = '#FFFFC0';
            }

            if (Reason == 'REP005') {
                var valDOB = document.getElementById('rfvDOB'); valDOB.enabled = true;
                document.getElementById('txtCorrectDOB').style.backgroundColor = '#FFFFC0';
            }
            if (Reason == 'REP006') {
                document.getElementById('rfvEmployeeNo').enabled = true;
                document.getElementById('txtEmployeeNo').style.backgroundColor = '#FFFFC0';
            }
            if (Reason == 'REP007') {
                document.getElementById('rfvNationality').enabled = true;
                document.getElementById('ddlENationality').style.backgroundColor = '#FFFFC0';
            }

            if (Reason == 'REP008') {
                document.getElementById('rfvIqamaID').enabled = true;
                document.getElementById('txtCorrectIqamaID').style.backgroundColor = '#FFFFC0';
            }
            if (Reason == 'REP009') {
                document.getElementById('rfvGender').enabled = true;
                document.getElementById('ddlEGender').style.backgroundColor = '#FFFFC0';
            }


        }

//-->
    </script>
    <br />
    <uc2:OSNav ID="OSNav1" runat="server" />
     </fieldset>
    <script type='text/JavaScript' src='js/NumberValidation.js'></script>
    <script type="text/javascript">
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
        });

        SetMobileNumberValidation("#<%= txtMobileNo.ClientID %>");
        SetIDNumberValidation("#<%= txtCorrectIqamaID.ClientID %>", "#<%= ddlIDType.ClientID %>");

        prm.add_endRequest(function () {
            $('#entryForm').unblock();
            SetMobileNumberValidation("#<%= txtMobileNo.ClientID %>");
            SetIDNumberValidation("#<%= txtCorrectIqamaID.ClientID %>", "#<%= ddlIDType.ClientID %>");
       });

       // Progress indicator preloading.
       var preload = document.createElement('img');
       preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
        delete preload;

        function OnClientClicked() {
            var notification = $find("<%=RadNotification1.ClientID %>");
             notification._close(true);
             setInterval(function () { $find("<%= RadNotification1.ClientID %>").ajaxRequest("Notification"); }, 0);
        }

        function changetext(reqName) {
            if (reqName == "RFVtxtBadgeNo")
                ValidatorEnable(document.getElementById('<%= RFVtxtBadgeNo.ClientID %>'), false);
            else if (reqName == "RequiredFieldValidator13")
                ValidatorEnable(document.getElementById('<%= RequiredFieldValidator13.ClientID %>'), false);

    }

    </script>
    <telerik:RadNotification ID="RadNotification1" runat="server" ShowInterval="1115000"
        VisibleOnPageLoad="true" LoadContentOn="FirstShow" Width="400" Animation="Fade"
        EnableRoundedCorners="true" EnableShadow="true"
        Font-Names="Tahoma" Position="TopRight" BackColor="#66CCFF" ContentScrolling="Auto"
        AutoCloseDelay="1113000" BorderColor="#66CCFF" Title="Notification" VisibleTitlebar="True"
        BorderStyle="Solid" BorderWidth="1" ShowCloseButton="False" RenderMode="Classic">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <div class="infoIcon">
                            <img src="../images/Basic-Info.ico" alt="info icon" style="width: 35px; height: 35px;" /></div>
                    </td>
                    <td>
                        <div style="line-height: 200%;">
                            <%--english--%>
                            <span style="font-family: Arial; font-size: 12px; color: #0099ff; font-weight: normal;">
                            For wrong Saudi/Iqama ID and to change from Entry No. to Iqama No. Please submit your request to <a href="mailto:CCHI@bupa.com.sa?Subject=Card%20Replacement%20Wrong%20Saudi%20Id" target="_top" >CCHI@bupa.com.sa</a>
                            </span>
                            <br />
                            <%--arabic--%>
                            <span style="float: right; direction: rtl; font-family: Tahoma; font-size: 11px; color: #0099ff; font-weight: normal;">
                                لتصحيح رقم الإقامة، الهوية السعودية أو لتحديث الرقم من رقم حدود إلى إقامة، الرجاء إرسال الطلب إلى البريد<a href="mailto:CCHI@bupa.com.sa?Subject=Card%20Replacement%20Wrong%20Saudi%20Id" target="_top" >CCHI@bupa.com.sa</a></span>
                            <br />
                            <br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <br />
                        <a onclick="OnClientClicked()" href="javascript:void(0);" class="button">Close</a>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </ContentTemplate>
    </telerik:RadNotification>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p id="LblChangeBranchBanner" runat="server">
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
        <%--<br />
        <b>Note:</b> For wrong Saudi/Iqama ID, Please submit your request to <a href="mailto:CCHI@bupa.com.sa?Subject=Card%20Replacement%20Wrong%20Saudi%20Id" target="_top" >CCHI@bupa.com.sa</a>--%>
    </p>
   
</asp:Content>
