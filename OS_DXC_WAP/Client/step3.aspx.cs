﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;

public partial class step3 : System.Web.UI.Page
{
    private Fileupload RequestFileUpload;
    private AddMemberFileuploadService FileUploadService;
    private long reqDetailId;

    protected void Page_Load(object sender, EventArgs e)
    {
        string comtype = Convert.ToString(HttpContext.Current.Session["ContractType"]);
        if (comtype.Trim().ToUpper().Equals("BDRC") || comtype.Trim().ToUpper().Equals("BDSC"))
        {
            DivSMEMEssage.Visible = true;
        }
        else
        {
            DivSMEMEssage.Visible = false;
        }


        if (Request.QueryString["reqDetId"] != null)
        {
            var reqDetId = long.Parse(Request.QueryString["reqDetId"]);
            HttpContext.Current.Session["ReqDetailID"] = reqDetId;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN1", "setReqDetail(" + reqDetId + ");", true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN2", "disableStep(" + 1 + ");", true);
        }
        Init();

    }

    private void Init()
    {
        //  hdnIsFileUpload.Value = "";


    }

    [WebMethod(EnableSession = true)]
    public static bool IsAttachmentExists(int Id)
    {
        if (HttpContext.Current.Session["MandatoryIndicator"] != null)
        {
            var indicator = (Caesar.CaesarManager.Indicator)HttpContext.Current.Session["MandatoryIndicator"];

            if (indicator.AttachmentMandatory != null && indicator.AttachmentMandatory.ToLower().Trim() == "y")
            {
                AddMemberFileuploadService fupService = new AddMemberFileuploadService();
                var lstfiles = fupService.FindByRequestID(Id);
                if (lstfiles != null && lstfiles.Count > 0)
                    return true;
                else
                    return false;
            }
            else
            {
                return true;
            }
        }
        return false;
    }

    [WebMethod(EnableSession = true)]
    public static int UpdateStatus(long Id)
    {
        var service = new AddMemberRequestDetailService();

        var member = service.FindByID(Id);
        if (member != null)
        {
            if (member.RequestType == "ED")
            {
                var depResult = service.UpdateFamilyStatus(new AddMemberRequestDetail
                {
                    SecSponsorId = member.IdNumber,
                    Status = 3
                });
            }
        }


        var result = service.UpdateStatus(new AddMemberRequestDetail
        {
            Id = Id,
            Status = 3
        });
        return result;
    }






}