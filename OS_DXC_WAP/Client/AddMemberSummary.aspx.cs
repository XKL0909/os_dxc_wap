﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;

public partial class AddMemberSummary : System.Web.UI.Page
{
    private AddMemberRequestService _memberService;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string clientId = Convert.ToString(Session["ClientID"]);
            if (!string.IsNullOrEmpty(clientId))
            {
                _memberService = new AddMemberRequestService();
                rptAddMemberSummary.DataSource = _memberService.GetAllByContract(clientId);
                rptAddMemberSummary.DataBind();
            }
        }
    }
}