
using System.Collections;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Collections;
using OS_DXC_WAP.CaesarWS;

public partial class CtntPreauthHistory : System.Web.UI.Page
{
    string  strClientID ;   
    private ServiceDepot_DNService ws;

   //change the name of the constructor
    public CtntPreauthHistory()
    {
        //InitializeComponent();
        this.Load += new EventHandler(CtntPreauthHistory_Load);
}

//change the name of the constructor and the page title
    void CtntPreauthHistory_Load(object sender, EventArgs e)
{
    Master.SearchButton.Click += SearchButton_Click ;
    Master.ddlStaffList.SelectedIndexChanged += DDLDependentList_SelectedIndexChanged;
    Master.SearchOpt.SelectedIndexChanged += rdbSearchOption_SelectedIndexChanged;
    strClientID = Session["ClientID"].ToString();
    Master.ddlStaffList.Visible = false;
    Master.PageTitle.Text = "Preauthorisation History";
    UpdatePanel2.Visible = false;
   
}
    protected void rdbSearchOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Master.ddlStaffList.Visible == true)
        {
            Master.ddlStaffList.Visible = false;

        }
        Master.SearchText.Text = "";
        Message1.Text = "";
        if (UpdatePanel2.Visible == true)
            UpdatePanel2.Visible = false;
    }


    protected void DDLDependentList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strMemberExistStatus;
        if (Master.ddlStaffList.SelectedValue != "0")
        {
            strMemberExistStatus = CheckMemberExist(Master.ddlStaffList.SelectedValue);

            if (strMemberExistStatus != "Exist")
            {
                Message1.Text = strMemberExistStatus;
                Message1.Visible = true;
                UpdatePanel2.Visible = false;
                Master.ddlStaffList.Visible = true;
            }
            else
            {


                DisplayPreAuthHist(Master.ddlStaffList.SelectedValue);
                //if (Master.ddlStaffList.SelectedValue != "0")
                //    DisplayMemberExistForClientResult(Master.ddlStaffList.SelectedValue, "Membership No");
                Master.ddlStaffList.Visible = true;
                lblMemberName.Visible = false; //.Text = Master.ddlStaffList.SelectedItem;
                lblMember_Name.Visible = false;
                UpdatePanel2.Visible = true;
                Message1.Visible = false;
            }
            //DisplayPreAuthHist(Master.ddlStaffList.SelectedValue);
            //Master.ddlStaffList.Visible = true;
            //UpdatePanel2.Visible = true;
            //lblMemberName.Visible = false; //.Text = Master.ddlStaffList.SelectedItem;
            //lblMember_Name.Visible = false;
        }
        else
        {
            Message1.Text = "Please select the valid name from the drop down";
            Message1.Visible = true;
            UpdatePanel2.Visible = false;
            Master.ddlStaffList.Visible = true;
        }
    }


    
private void SearchButton_Click(object sender, EventArgs e)
{
    string strMemberExistStatus;
    strMemberExistStatus = "";
    if (Master.SearchOpt.Text != "Staff No" && Master.SearchOpt.Text != "Badge No")
    {

        strMemberExistStatus = CheckMemberExist(Master.SearchText.Text);
        if (strMemberExistStatus != "Exist")
        {
            Message1.Text = strMemberExistStatus;
            Master.ddlStaffList.Visible = false;
            UpdatePanel2.Visible = false;
            Message1.Visible = true;
        }
        else
            DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);


    }
    else
        DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);
}


 private void DisplayMemberExistForClientResult(string SearchText, string SearchType)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}
        

        EnqMbrListInfoRequest_DN request;
        EnqMbrListInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqMbrListInfoRequest_DN();
            request.customerNo = strClientID;

            if (SearchType == "Membership No")
            request.membershipNo = SearchText;
            else
            request.staffNo = SearchText;
            
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.detail == null)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = "Inputted '" + Master.SearchOpt.Text + "' does not exist in your group.";
            UpdatePanel2.Visible =false;
            Message1.Visible = true;
        }
            else
            {
                
                    Master.ddlStaffList.Items.Clear();

                    ListItem DepList0 = new ListItem();
                    DepList0.Text = "Please select from the list";
                    DepList0.Value = "0";
                    Master.ddlStaffList.Items.Add(DepList0);
                    
                    
                    foreach (MbrListDetail_DN dtl in response.detail)
                    {
                        ListItem DepList = new ListItem();
                        DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                        DepList.Value = dtl.membershipNo.ToString();
                        Master.ddlStaffList.Items.Add(DepList);
                        

                    }

                    Master.ddlStaffList.Visible = true;
                    Message1.Visible = false;


                

                
           //response.detail[0].relationship;
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }
            
            
            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }


    private void DisplayPreAuthHist(string MemberNo)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        EnqPreHisInfoRequest_DN request;
        EnqPreHisInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqPreHisInfoRequest_DN();
            request.membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.EnqPreHisInfo(request);

            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.detail != null)
            {

                lblMembershipNo.Text = request.membershipNo;
              //  lblMemberName.Text = strMemberName;
                // lblClassName.Text = response.className;
                // lblCustomerName.Text = response.companyName;

                DisplayPreauthHistoryResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

               
            }
            else
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }



    private void DisplayPreauthHistoryResult(EnqPreHisInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;


        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {

            //dtl.proratedOverallAnnualLimit = ;


            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border-collapse: collapse; width:99%  ;'><thead bgcolor='#e6f5ff'><tr><th>Pre-auth No</th>   <th>Episode No</th><th>Provider Name</th>   <th>Received Date/Time</th><th>Response Date/Time</th>   <th>Pre-auth Status</th><th>Details</th></tr>	</thead><tbody> ");

            foreach (PreHisDetail_DN dtl in response.detail)
            {
                
                var queryString = "PID=" + dtl.preAuthNo.Trim() + "&PC=" + dtl.providerCode.Trim() + "&EN=" + dtl.episodeID + "&ReqType=C";
                queryString = Cryption.Encrypt(queryString);
                sbResponse.Append("<tr ><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString() + "</td><td>" + dtl.preAuthStatus + "</td><td><a href='../Provider/PreauthDetails.aspx?val=" + queryString + "' style='cursor:hand;'>View</a></td></tr>");               


                ////sbResponse.Append("<tr ><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString() + "</td><td>" + dtl.preAuthStatus + "</td><td><a href='../Provider/PreauthDetails.aspx?PID=" + dtl.preAuthNo.Trim() + "&PC=" + dtl.providerCode.Trim() + "&EN=" + dtl.episodeID + "&ReqType=C&'  style='cursor:hand;'>View</a></td></tr>");

                //                sbResponse.Append("<tr><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID  + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString()  + "</td><td>" + dtl.preAuthStatus  + "</td></tr>");
                //if (dtl.detail_ProratedLimit != -1)
                //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
                //else
                //    sbResponse.Append("<td>Not Covered</td></tr>");


            }
            sbResponse.Append("</table>");
            CoverageListReport.Visible = true;
            CoverageListReport.InnerHtml = sbResponse.ToString();


        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }


    private string CheckMemberExist(string MemberNo)
    {
        ws = new ServiceDepot_DNService();

        ChkMbrExistRequest_DN request;
        ChkMbrExistResponse_DN response;
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new ChkMbrExistRequest_DN();
            request.membershipNo = MemberNo;
            request.iqamaNo = "";// IqamaNo;
            request.totalNoOfFamily = ""; // DependentNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            //            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            //request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ChkMbrExist(request);
            // Cursor.Current = Cursors.Default;


            if (response.memberName != null)
            {
                return "Exist";
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }
            else
            {
                return response.errorMessage[0];
                //CaesarResult = "InValid";
                //CaesarMessage = response.errorMessage;
            }
            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            return "Error";
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }




    
}
