﻿using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using OS_DXC_WAP.CaesarWS;
///using localPersonService;
//using pws;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using log4net;
//using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI;
using Utility.Configuration;
using Bupa.Core.Utilities.Database;
using System.Globalization;
using Bupa.Core.Logging;
using Telerik.Web.UI;
using System.Configuration;
using OnlineServices.CCHI;
using System.Collections.Generic;
using System.Linq;
using OnlineServices.CCHI.Dto;
using OnlineServices.CCHI.Services;
using OnlineServices.CCHI.Enums;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using Bupa.Yakeen.AccessLayer.Models;
using Bupa.Models.Enums;
using System.Web.Configuration;
using System.Web;

public partial class BatchUpload : System.Web.UI.Page
{
    #region Fields

    ServiceDepot_DNService ws;
    private string[] _docs;
    public string _UploadCategory = UploadCategory.MEMBER_ADDEMP;
    public bool _UploadMandatory = true;
    public string _Username = "Unknown Client";
    public string _InitialRequestID = string.Empty;
    DataSet ds = null;
    private ILog _log = LogManager.GetLogger(typeof(BatchUpload));
    bool Validate = true;
    private readonly string _allowedExtensions = ".xls .xlsx";

    private string _contractID = string.Empty;
    private string _batchOptionType;
    private string _dateErrors = string.Empty;
    private string _classErrors = string.Empty;
    private string _memberNoErrors = string.Empty;
    private CCHIService _service;
    ///private string messageCCHICap = string.Empty;
	public string _BatchUrlString = "";
    protected string AramcoContractType = Convert.ToString(ConfigurationManager.AppSettings["AramcoContractType"]).ToUpper();
    public string _urlValues =string.Empty;
    public int _containerValue;
    private Hashtable hasQueryValue;
    private string optionType;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            _contractID = Convert.ToString(Session["ClientID"]);
            if (string.IsNullOrEmpty(_contractID))
                Response.Redirect("~/Default.aspx");
        }
        catch (ThreadAbortException) { }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();

       
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //RefreshPage();


        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }
            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
        }
        catch
        {

        }

        Logger.Tracer.WriteMemberEntry();
        try
        {
            _service = new CCHIService();
           //// _batchOptionType = Request.QueryString["OptionType"];

            _dateErrors = string.Empty;
            _classErrors = string.Empty;
            _memberNoErrors = string.Empty;

            //lblSample.Text = "Download sample file from <a href='ExcelTemplates/" + _batchOptionType + ".xls' >here</a>";
            ////Tracer.WriteLine(ref _log, "_contractID is: " + _contractID);

            hasQueryValue = new Hashtable();
            string queryStringValues = string.Empty;

            if (Request.QueryString.Count > 0)
            {
                try
                {
                    var val = Request.QueryString["val"];
                    queryStringValues = Cryption.Decrypt(val);
                    hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                    optionType = hasQueryValue.ContainsKey("optiontype") ? Convert.ToString(hasQueryValue["optiontype"]) : string.Empty;
                }
                catch (Exception)
                {
                    lblMessage.Text = "Invalid rquest!";
                    return;
                }
            }

            _batchOptionType = optionType.ToLower().Trim();
            lblSample.Text = "Download sample file from <a href='ExcelTemplates/" + _batchOptionType + ".xls' >here</a>";

            switch (_batchOptionType)
            {
                case "cardreplace":
                    _UploadCategory = UploadCategory.MEMBER_BATCH_CARDREPLACE;
					 _BatchUrlString = "~/Client/cardreplace.aspx";
                    break;

                case "changeclass":
                    _UploadCategory = UploadCategory.MEMBER_BATCH_CHGCLASS;
					_BatchUrlString = "~/Client/changebranch.aspx?val=" + Cryption.Encrypt("optiontype=class");
                    break;

                case "changebranch":
                    _UploadCategory = UploadCategory.MEMBER_BATCH_CHGBRANCH;
					 _BatchUrlString = "~/Client/changebranch.aspx";
                    _UploadMandatory = false;
                    break;

                case "adddependent":
                    _UploadCategory = UploadCategory.MEMBER_BATCH_ADDDEP;
                    _BatchUrlString = "~/Client/CCHICap.aspx?val=" + Cryption.Encrypt("optiontype=adddependent");
                    break;

                case "addemployeedependent":
                    _UploadCategory = UploadCategory.MEMBER_BATCH_ADDEMPDEP;
                    if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                        if (Request.UrlReferrer.AbsolutePath == "/client/Request.aspx")
                            _BatchUrlString = "~" + Request.UrlReferrer.PathAndQuery;
                        else
                            _BatchUrlString = "~/client/RequestDetail.aspx?requestid=" + Cryption.Encrypt(Convert.ToString(Session["RequestId"]));
                    else
                        _BatchUrlString = "~/client/CCHICap.aspx?val=" + Cryption.Encrypt("optiontype=addemployeedependent");
                    break;

                case "deleteemployee":
                    _UploadCategory = UploadCategory.MEMBER_BATCH_DELEMP;
					_BatchUrlString = "~/client/DeleteEmployeeEnhanced.aspx";
                    break;
                case "deletedependent":
                    _UploadCategory = UploadCategory.MEMBER_DELDEP;
                    _BatchUrlString = "~/client/DeleteEmployeeEnhanced.aspx?val=" + Cryption.Encrypt("EType=Dep");
                    break;

            }

            // Setup the Uploader control
            if (Session[WebPublication.Session_ClientUsername] != null)
                _Username = Convert.ToString(Session[WebPublication.Session_ClientUsername]);

            if (Page.IsPostBack)
            {
                if (Session["_InitialRequestID"] != null)
                    _InitialRequestID = Convert.ToString(Session["_InitialRequestID"]);
            }
            else
            {
                // Get a unique request ID so that the attachment page recognises this request to be unique from others
                _InitialRequestID = WebPublication.GenerateUniqueID;
                Session["_InitialRequestID"] = _InitialRequestID;
            }
            if (IsPostBack)
            {
                ds = (DataSet)Session["DataSet"];
                gvData.DataSource = ds.Tables[0];
                gvData.DataBind();
                gvData.Visible = true;
                btnValidateData.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
    }
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        try
        {
            if (!string.IsNullOrEmpty(lblMessage.Text.Trim()) && lblMessage.Text.Trim().ToLower().Equals("access is denied"))
                lblMessage.Text = string.Empty;
        }
        catch (Exception)
        {  
        }
      
        
    }

    protected void btnSubmitBatch_Click(object sender, EventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();

        string finalfileName = string.Empty;
        try
        {
            lblMessage.Text = "";
            // Upload the batch file and evaluate
            if (uploaderBatch.HasFile)
            {
                
                
                bool uploadedSupportingDocs = EvaluateRequiredDocuments();
                ////Tracer.WriteLine(ref _log, "uploadedSupportingDocs is: " + uploadedSupportingDocs);

                // First check if files have been uploaded
                int uploadedCount = 0;
                if (_UploadMandatory)
                {
                    // Check that files have been uploaded
                    UploadManager uploadManager = new UploadManager(Int32.MinValue, Int32.MinValue);
                    DataSet uploadedFileSet = uploadManager.GetUploadedFileSet(_InitialRequestID, _UploadCategory);

                    uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                }

                if (uploaderBatch.HasFile)
                {
                    CheckMimeTypes validExcelfile = new CheckMimeTypes();
                    if (validExcelfile.ValidateExcelFile(uploaderBatch.PostedFile))
                    { 
                    
                            finalfileName = uploaderBatch.FileName;
                            ////Tracer.WriteLine(ref _log, "fileName is: " + finalfileName);

                            // Check that extension is valid
                            bool grantSave = IsAllowedExtension(finalfileName);
                            if (!grantSave)
                            {
                                lblRowErrors.Text = "Please only upload Excel files with extension(s): " + _allowedExtensions;
                                return;
                            }

                            // Proceed with saving the file to be unique
                            ////Tracer.WriteLine(ref _log, "Proceeding with saving the file to be unique: " + finalfileName);
                            UploadResult ur = SaveFile(finalfileName);

                            // Evaluate result of saving the file
                            if (!ur.Result)
                            {
                                // Failure
                                string error = "An error has occurred uploading the Excel batch file. Please try again later. ";
                                foreach (string err in ur.ErrorReason)
                                {
                                    error = error + err;
                                }

                                lblRowErrors.Text = error;
                                return;
                            }
                            // File was saved successfully - now begin parsing the file
                            ////Tracer.WriteLine(ref _log, "File was saved successfully - now begin parsing the file: " + ur.DestinationPath);
                            //Fill Grid with Excel Data
                            GetExcel(ur.DestinationPath, _batchOptionType);

                    }
                    else { 
                            lblRowErrors.Text = "Please select a valid file";
                            return;
                    }
                }
                else
                {
                    lblRowErrors.Text = "Please select a valid file";
                    return;
                }
            }
            else
            {
                lblRowErrors.Text = "Please select a valid file";
            }
        }
        catch (Exception ex)
        {
            throw;
            Logger.Current.WriteException(ex);
        }
        finally
        {
            if (!string.IsNullOrEmpty(finalfileName) && File.Exists(finalfileName))
            {
                File.Delete(finalfileName);
            }
        }
        Logger.Tracer.WriteMemberExit();
    }
    private bool EvaluateRequiredDocuments()
    {
        Logger.Tracer.WriteMemberEntry();

        bool uploadedSupportingDocs = false;
        try
        {
            // First check if files have been uploaded
            int uploadedCount = 0;
            if (_UploadMandatory)
            {
                // Check that files have been uploaded
                UploadManager uploadManager = new UploadManager(Int32.MinValue, Int32.MinValue);
                DataSet uploadedFileSet = uploadManager.GetUploadedFileSet(_InitialRequestID, _UploadCategory);

                if (uploadedFileSet == null || uploadedFileSet.Tables.Count == 0 || uploadedFileSet.Tables[0].Rows.Count == 0)
                {
                    uploadedCount = 0;
                }
                else
                {
                    uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                    _docs = new string[uploadedFileSet.Tables[0].Rows.Count];
                    for (int i = 0; i < uploadedFileSet.Tables[0].Rows.Count; i++)
                    {
                        _docs[i] = Convert.ToString(uploadedFileSet.Tables[0].Rows[i][3]);
                    }
                }
            }

            if (uploadedCount > 0)
            {
                uploadedSupportingDocs = true;
            }
            else
            {
                uploadedSupportingDocs = false;
            }

        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
        return uploadedSupportingDocs;
    }
    private bool IsAllowedExtension(string file)
    {
        Logger.Tracer.WriteMemberEntry();

        bool allowed = false;
        try
        {
            // Allowed extensions are a comma separated list
            string fileExtension = file.Substring(file.LastIndexOf(".") + 1);
            ////Tracer.WriteLine(ref _log, "fileExtension is: " + fileExtension);

            if (_allowedExtensions.ToLower().Contains(fileExtension.ToLower()))
                allowed = true;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        ////Tracer.WriteLine(ref _log, "allowed is: " + allowed);

        Logger.Tracer.WriteMemberExit();
        return allowed;
    }
    private UploadResult SaveFile(string fileName)
    {
        Logger.Tracer.WriteMemberEntry();

        UploadResult uploadResult = new UploadResult();
        uploadResult.Result = false;

        try
        {
            // Fetch the filename and save to the server
            ////Tracer.WriteLine(ref _log, "fileName is: " + fileName);

            // Get a random name and append to the file
            string uniquePrefix = WebPublication.GenerateUniqueID;
            ////Tracer.WriteLine(ref _log, "uniquePrefix is: " + uniquePrefix);

            // Go ahead and save to the server for the moment
            string virtualPath = "~/Uploads/" + uniquePrefix + "_" + fileName;
            ////Tracer.WriteLine(ref _log, "virtualPath is: " + virtualPath);

            string destinationPath = Server.MapPath(virtualPath);
            ////Tracer.WriteLine(ref _log, "destinationPath is: " + destinationPath);

            while (!IsUniquePath(destinationPath))
            {
                ////Tracer.WriteLine(ref _log, "destinationPath is not unique ( " + destinationPath + ")");
                uniquePrefix = WebPublication.GenerateUniqueID;
                virtualPath = "~/Uploads/" + uniquePrefix + "_" + fileName;
                destinationPath = Server.MapPath(virtualPath);

                ////Tracer.WriteLine(ref _log, "New destinationPath is ( " + destinationPath + ")");
            }

            // Save the file & fetch the session ID as this will be used as part of the upload key
            uploaderBatch.SaveAs(destinationPath);
            ////Tracer.WriteLine(ref _log, "Successfully saved Excel batch file at: " + destinationPath);

            uploadResult.Result = true;
            uploadResult.DestinationPath = destinationPath;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);

            uploadResult.Result = false;
            uploadResult.ErrorReason = new StringCollection();
            uploadResult.ErrorReason.Add("Technical message: " + ex.Message + ". Stack: " + ex.StackTrace);
        }

        Logger.Tracer.WriteMemberExit();
        return uploadResult;
    }
    private bool IsUniquePath(string destinationPath)
    {
        Logger.Tracer.WriteMemberEntry();

        bool isUnique = true;
        try
        {
            if (File.Exists(destinationPath))
                isUnique = false;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }

        Logger.Tracer.WriteMemberExit();
        return isUnique;
    }
    private System.Data.DataTable ParseBatchFile(string finalExcelFilePath)
    {
        Logger.Tracer.WriteMemberEntry();

        System.Data.DataTable parsedDate = new System.Data.DataTable();

        string sConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + finalExcelFilePath + ";Extended Properties=" + Convert.ToChar(34).ToString() + "Excel 8.0;HDR=Yes" + Convert.ToChar(34).ToString();
        System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection(sConnectionString);

        try
        {
            if (cn.State != ConnectionState.Closed)
            {
                try
                {
                    cn.Close();
                }
                catch (Exception ex)
                {
                    Logger.Current.WriteException(ex);
                }
            }
            cn.Open();

            System.Data.DataTable oTables = cn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            int i = 0;

            string sSheetName = oTables.Rows[i]["TABLE_NAME"].ToString();
            if (sSheetName.IndexOf("$") != -1)
            {
                DataSet oDataSet = new DataSet();
                OleDbDataAdapter oAdapter = new OleDbDataAdapter("SELECT * FROM [" + sSheetName + "]", cn);
                oAdapter.TableMappings.Add("Table", sSheetName);
                oAdapter.Fill(oDataSet);
                System.Data.DataTable oDataTable = oDataSet.Tables[0];

                if (oDataTable.Rows.Count > 0 & oDataTable.Columns.Count > 4)
                {
                    parsedDate = oDataTable;
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        finally
        {
            cn.Close();
        }

        Logger.Tracer.WriteMemberExit();
        return parsedDate;
    }
    public static bool IsOptionalDate(string attemptedDate)
    {
        bool Success;
        try
        {
            // For date which can be either blank or in proper date format 
            if (attemptedDate != "")
            {
                DateTime dtParse = DateTime.Parse(attemptedDate);
                Success = true;
            }
            else
                return true;
        }
        catch (FormatException)
        {
            Success = false;
        }
        return Success;
    }
    private bool IsNaturalNumber(String strNumber)
    {
        Regex objNotNaturalPattern = new Regex("[^0-9]");
        Regex objNaturalPattern = new Regex("0*[1-9][0-9]*");
        return !objNotNaturalPattern.IsMatch(strNumber) &&
        objNaturalPattern.IsMatch(strNumber);
    }
    private bool IsDate(string attemptedDate)
    {
        bool Success = false;
        try
        {
            if (attemptedDate != string.Empty)
            {
                DateTime dtParse = DateTime.Parse(attemptedDate);
                Success = true;
            }
        }
        catch (Exception) { }

        return Success;
    }
    public void CallService(OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request)
    {
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        ws.Url = CoreConfiguration.Instance.GetConfigValue("OS_DXC_WAP.CaesarWS.ServiceDepot_DN");
        ////Tracer.WriteLine(ref _log, "In batch: " + ws.Url);

        OS_DXC_WAP.CaesarWS.SubTxnResponse_DN response;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.SubTxn(request);

            ////Convert object to Xml 
            ////If webservice is not responding fine, then send the request & response xml to Caesar team 
            ////Below code need to enable, it will saving the request & rsponse xml in specifeied path
            ////Strat
            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.SubTxnRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.SubTxnRequest_DN>();
            //XmlReq.Request(request, "SubTxn_Request");

            /////response = ws.SubTxn02(request);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.SubTxnResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.SubTxnResponse_DN>();
            //XmlResp.Response(response, "SubTxn_Response");
            ////End



            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");
                lblMessage.Text = sb.ToString() + "<br/>Please try again rectifying the above errors.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Font.Bold = true;
                btnValidateData.Visible = true;
                caesarInvokeError = true;
            }
            else
            {
                lblMessage.Text = "Thank you for submitting your request. We are now validating the submitted file. Your reference no. is <b>" + response.ReferenceNo.ToString() + "</b>";

                lblRowErrors.Visible = false;
                uploaderBatch.Visible = false;
                btnSubmitBatch.Visible = false;
                RegularExpressionValidator2.Enabled = false;

                btnValidateData.Visible = false;
                gvData.Visible = false;
                caesarInvokeError = false;
                caesarReferenceID = response.ReferenceNo.ToString();
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
    }
    public void CallServiceAddMember(OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request)
    {
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        ws.Url = CoreConfiguration.Instance.GetConfigValue("OS_DXC_WAP.CaesarWS.ServiceDepot_DN");
        ////Tracer.WriteLine(ref _log, "In batch: " + ws.Url);

        OS_DXC_WAP.CaesarWS.SubTxnResponse_DN response;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        try
        {
            StringBuilder sb = new StringBuilder(200);


            ////Convert object to Xml 
            ////If webservice is not responding fine, then send the request & response xml to Caesar team 
            ////Below code need to enable, it will saving the request & rsponse xml in specifeied path
            ////Strat
            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.SubTxnRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.SubTxnRequest_DN>();
            //XmlReq.Request(request, "SubTxn_Request");

            response = ws.SubTxn02(request);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.SubTxnResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.SubTxnResponse_DN>();
            //XmlResp.Response(response, "SubTxn_Response");
            ////End






            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");
                lblMessage.Text = sb.ToString() + "<br/>Please try again rectifying the above errors.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Font.Bold = true;
                btnValidateData.Visible = true;
                caesarInvokeError = true;
            }
            else
            {
                lblMessage.Text = "Thank you for submitting your request. We are now validating the submitted file. Your reference no. is <b>" + response.ReferenceNo.ToString() + "</b>";

                lblRowErrors.Visible = false;
                uploaderBatch.Visible = false;
                btnSubmitBatch.Visible = false;
                RegularExpressionValidator2.Enabled = false;

                btnValidateData.Visible = false;
                gvData.Visible = false;
                caesarInvokeError = false;
                caesarReferenceID = response.ReferenceNo.ToString();
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
    }
    public void Notify(string uploadSessionID, string function, string referenceID, string[] addressees, string fullName, string username)
    {
        // Send an email informing the team about a pre-auth request
        string fromEmail = "donotreply@bupa.com.sa";

        string subject = function + " reference:" + referenceID + " - attachments added by " + fullName + " (" + username + ")";
        string content = "Please view the attached uploaded documents added by " + fullName + " (" + username + ") for " + function + " reference: " + referenceID;

        // Get the list of attachments for this user
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);

        StringCollection attachments = new StringCollection();
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow r in d.Tables[0].Rows)
            {
                string attachPath = r["VirtualPath"].ToString();
                attachments.Add(attachPath);
            }
        }
        else
        {
            // No attachments and therefore, nothing to email
            return;
        }

        // Get the addressees
        foreach (string toEmail in addressees)
        {
            SendEmail(fromEmail, toEmail, subject, content, attachments);
        }
    }
    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        // Setup the mail message
        MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

        // Deal with the attachments
        if (attachmentPaths != null)
        {
            foreach (string path in attachmentPaths)
            {
                // Create the attachment
                System.Net.Mail.Attachment attachment;
                if (System.IO.File.Exists(Server.MapPath(path)))
                {
                    attachment = new System.Net.Mail.Attachment(Server.MapPath(path));
                    // Add the attachment
                    mail.Attachments.Add(attachment);
                }
            }
        }

        string smtpHost = WebPublication.EmailRelayAddress();
        int smtpPort = WebPublication.EmailRelayPort();

        SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
        mail.IsBodyHtml = true;
        smtp.Send(mail);
    }
    public void GetExcel(string fileName, string _type)
    {
        Application oXL = new ApplicationClass();
        //oXL.Validation = Microsoft.Office.Core.MsoFileValidationMode.msoFileValidationSkip;
        Workbook oWB;
        Worksheet oSheet;
        Range oRng;
        Range oRngHeader;
        try
        {

            oWB = oXL.Workbooks.Open(fileName);
            //   get   WorkSheet object    
            oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWB.Sheets[1];
            System.Data.DataTable dt = new System.Data.DataTable("dtExcel");

            ds = new DataSet();
            ds.Tables.Add(dt);
            DataRow dr;
            StringBuilder sb = new StringBuilder();
            int jValue = oSheet.UsedRange.Cells.Columns.Count;
            int iValue = oSheet.UsedRange.Cells.Rows.Count;
            bool _validFile = false;
            string _strError = "";
            switch (_type)
            {
                case "adddependent":
                    if (jValue == 27)
                        _validFile = true;
                    _strError = "Add Dependent";
                    break;
                case "addemployeedependent":
                    if (jValue == 26)
                        _validFile = true;
                    _strError = "Add Employee";
                    break;
                case "cardreplace":
                    if (jValue == 13)
                        _validFile = true;
                    _strError = "Card Replace";
                    break;
                case "changebranch":
                    if (jValue == 7)
                        _validFile = true;
                    _strError = "Change Branch";
                    break;
                case "changeclass":
                    if (jValue == 7)
                        _validFile = true;
                    _strError = "Change Class";
                    break;
                case "deletedependent":
                    if (jValue == 5)
                        _validFile = true;
                    _strError = "Delete Dependent";
                    break;
                case "deleteemployee":
                    if (jValue == 5)
                        _validFile = true;
                    _strError = "Delete Employee";
                    break;

            }
            if (_validFile)
            {
                //  get data columns    
                for (int j = 1; j <= jValue; j++)
                {
                    oRng = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, j];
                    string strValue = oRng.Text.ToString();
                    dt.Columns.Add(strValue, System.Type.GetType("System.String"));
                }
                ds.Tables["dtExcel"].Columns.Add("SEQ");
                ds.Tables["dtExcel"].Columns.Add("SupportDocs");
                if (gvData.Columns.Count <= 4)
                {
                    GridViewDataTextColumn column1 = new GridViewDataTextColumn();
                    column1.FieldName = "SEQ";
                    column1.VisibleIndex = 1;
                    column1.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    column1.ReadOnly = true;

                    GridViewDataTextColumn column2 = new GridViewDataTextColumn();
                    column2.FieldName = "SupportDocs";
                    column2.VisibleIndex = 2;
                    column2.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    column2.Caption = "Support Docs";
                    column2.Visible = false;

                    //gvData.Columns.Add(column0);
                    gvData.Columns.Add(column1);
                    gvData.Columns.Add(column2);

                    for (int _cell = 1; _cell <= jValue; _cell++)
                    {
                        oRngHeader = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, _cell];
                        string strHeaderTitle = oRngHeader.Text.ToString();
                        GridViewDataTextColumn column = new GridViewDataTextColumn();
                        column.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                        column.FieldName = strHeaderTitle;

                        if (column.FieldName.Length > 12)
                        {
                            column.Caption = column.FieldName.Substring(0, 10) + " ...";
                            column.ToolTip = column.FieldName;
                        }

                        column.PropertiesTextEdit.MaxLength =
                        column.VisibleIndex = _cell + 2;
                        gvData.Columns.Add(column);
                    }
                }
                //  get data in cell    
                for (int i = 2; i <= iValue; i++)
                {
                    dr = ds.Tables["dtExcel"].NewRow();
                    dr["SEQ"] = i - 1;

                    for (int j = 1; j <= jValue; j++)
                    {
                        oRngHeader = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, j];
                        oRng = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[i, j];
                        string strHeader = oRngHeader.Text.ToString();
                        string strValue = oRng.Text.ToString();
                        dr[strHeader] = strValue;
                    }
                    //Added by Husamuddin for Aramco  
                    if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                    {
                       bool valid= CheckStaffIdValidation(dr);
                       if (valid) { 
                       ds.Tables["dtExcel"].Rows.Add(dr);
                       ds.Tables["dtExcel"].PrimaryKey = new DataColumn[] { dt.Columns["SEQ"] };
                       }
                       else
                       {
                           lblMessage.Text = "Please make sure that you are uploading the correct Data for Staff Id";
                           gvData.Visible = false;
                           btnValidateData.Visible = false;
                           lblMessage.ForeColor = System.Drawing.Color.Red;
                           lblMessage.Font.Bold = true;
                           return;
                       }
                    }
                    else { 
                        ds.Tables["dtExcel"].Rows.Add(dr);
                        ds.Tables["dtExcel"].PrimaryKey = new DataColumn[] { dt.Columns["SEQ"] };
                    }
                }

                Session["DataSet"] = ds;
                //// CCHI Unified Policy CR Changes Start
                ////New Code Start
                //No CCHI Validation for Aramco Addedd by Husamuddin
                if (Convert.ToString(Session["ContractType"]).ToUpper() != AramcoContractType)
                {
                    if (_type.Trim().ToLower().Equals("adddependent") || _type.Trim().ToLower().Equals("addemployeedependent"))
                    {
                        List<CCHISponsorDetails> lstCCHISponsorDetails = new List<CCHISponsorDetails>();
                        lstCCHISponsorDetails = GetCCHICap(_type, ds.Tables[0]);
                        if (string.IsNullOrEmpty(lblMessage.Text))
                        {
                            if (lstCCHISponsorDetails != null && lstCCHISponsorDetails.Count() > 0)
                            {
                                List<Cap<int>> sponsors = new List<Cap<int>>();
                                foreach (var sponsor in lstCCHISponsorDetails)
                                {
                                    sponsors.Add(
                                        new Cap<int>
                                        {
                                            SponsorId = sponsor.SponsorID,
                                            NonSaudiMain = sponsor.NonSaudiMain,
                                            NonSaudiDependent = sponsor.NonSaudiDependent,
                                            SaudiMain = sponsor.SaudiMain,
                                            SaudiDependent = sponsor.SaudiDependent
                                        });
                                }


                                ////var caps = _service.ValidateSponsors(sponsors, true);

                                var caps = _service.ValidateSponsorsWithIqama(sponsors, _contractID);

                                rptSponsors.DataSource = caps;
                                rptSponsors.DataBind();
                                ///isValidCCHICap = caps.Where(t => t.ErrorDetails.ServiceResult == ServiceResult.ValidationFailure).Count() > 0;
                                if (caps.Where(t => t.ErrorDetails.ServiceResult == ServiceResult.ValidationFailure).Count() > 0)
                                {
                                    btnValidateData.Visible = false;
                                    tblUpload.Visible = true;
                                }
                                else
                                {
                                    btnValidateData.Visible = true;
                                    tblUpload.Visible = false;
                                }
                                trCCHIMapping.Visible = true;
                            }


                        }
                        else
                        {
                            btnValidateData.Visible = false;
                            tblUpload.Visible = true;
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                            lblMessage.Font.Bold = true;
                        }

                        //btnValidateData.Visible = true;
                        //tblUpload.Visible = false;                  
                    }
                    else
                    {
                        btnValidateData.Visible = true;
                        tblUpload.Visible = false;
                    }

                    gvData.DataSource = ds.Tables[0];
                    gvData.DataBind();
                    gvData.Visible = true;
                }
                else
                {
                    btnValidateData.Visible = true;
                    tblUpload.Visible = false;
                    gvData.DataSource = ds.Tables[0];
                    gvData.DataBind();
                    gvData.Visible = true;
                }

            }
            else
            {
                ds = null;
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                    _strError = "Add Member";
                lblMessage.Text = "Please make sure that you are uploading the correct " + _strError + " file";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Font.Bold = true;
                gvData.Visible = false;
                btnValidateData.Visible = false;
                tblUpload.Visible = true;

                Alert.Show("Please make sure that you are uploading the correct " + _strError + " file");
            }

        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Font.Bold = true;
            gvData.Visible = false;
            btnValidateData.Visible = false;
            tblUpload.Visible = true;
        }

        finally
        {
            try
            {
                oXL.Quit();
                Marshal.ReleaseComObject(oXL);
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                foreach (Process process in Process.GetProcessesByName("Excel"))
                {
                    if (!process.HasExited)
                    {
                        process.Kill();
                    }
                }
            }
            catch (Exception ex2)
            {

                lblMessage.Text = ex2.Message;
            }

        }
    }

    public bool CheckStaffIdValidation(DataRow dr){
        bool valid=true;
        dr[0] = "Add Member";
        if (dr["Member Type"].ToString() == "TA")
        {
            if(string.IsNullOrEmpty(dr["Staff No"].ToString()))
                valid = false;
            else if (dr["Staff No"].ToString().Contains('-'))
                valid = false;
            else if (!dr["Staff No"].ToString().All(char.IsDigit))
                valid = false;
            else if (dr["Staff No"].ToString().Length < 4)
                valid = false;
            else if(dr["Staff No"].ToString().Length > 8)
                valid = false;
        }
        else
        {
            if (string.IsNullOrEmpty(dr["Staff No"].ToString()))
                valid = false;
            else if (!dr["Staff No"].ToString().Contains('-'))
                valid = false;
            else if (!dr["Staff No"].ToString().Split('-')[0].All(char.IsDigit) && dr["Staff No"].ToString().Split('-')[1].All(char.IsDigit))
                valid = false;
            else if (dr["Staff No"].ToString().Split('-')[0].Length < 4)
                valid = false;
            else if(dr["Staff No"].ToString().Split('-')[0].Length > 8)
                valid = false;
            else if(dr["Staff No"].ToString().Split('-')[1].Length>2)
                valid = false;
        }
        return valid;
    }



    public string FormatValidationMessge(string msg)
    {
        if (msg == "ValidationSuccess")
            return "<span style='color:green'>" + "Success" + "</span>";

        return "<span style='color:red'>" + "Failed" + "</span>";
    }
    protected bool CheckDataType(string _value, string _dateType)
    {
        bool _Valid = false;

        switch (_dateType)
        {
            case "Number":
                try
                {
                    _value = Convert.ToInt64(_value).ToString();
                    _Valid = true;
                }
                catch
                {
                    _Valid = false;
                }
                break;
            case "Date":
                try
                {
                    _value = Convert.ToDateTime(_value).ToShortDateString();
                    _Valid = true;
                }
                catch
                {
                    _Valid = false;
                }
                break;
        }
        return _Valid;

    }
    protected void gvData_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {


            if (e.CellValue.ToString().Contains("/"))
            {
                if (!CheckDataType(e.CellValue.ToString(), "Date"))
                {
                    e.Cell.BackColor = System.Drawing.Color.Red;
                    e.Cell.ForeColor = System.Drawing.Color.White;
                    //e.CellValue = string.Format("${0}", e.CellValue);
                    Validate = false;
                }
                else
                {
                    e.Cell.BackColor = System.Drawing.Color.Transparent;
                    e.Cell.ForeColor = System.Drawing.Color.Black;
                    Validate = true;
                }
            }
        }
        catch { }
    }  
    protected void gvData_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView gridView = (ASPxGridView)sender;
        ds = (DataSet)Session["DataSet"];
        System.Data.DataTable dataTable = ds.Tables[0];
        DataRow row = dataTable.Rows.Find(e.Keys[0]);
        IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();
        enumerator.Reset();
        while (enumerator.MoveNext())
            row[enumerator.Key.ToString()] = enumerator.Value;
        gridView.CancelEdit();
        e.Cancel = true;
    }
    protected void gvData_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        try
        {
            if (e.Value.ToString().Contains("/"))
            {
                if (!CheckDataType(e.Value.ToString(), "Date"))
                {
                    e.Column.CellStyle.BackColor = System.Drawing.Color.Red;
                    e.Column.CellStyle.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    e.Column.CellStyle.BackColor = System.Drawing.Color.Transparent;
                    e.Column.CellStyle.ForeColor = System.Drawing.Color.Black;
                }
            }
        }
        catch { }
    }
    protected string getIndecator(int RowIndex)
    {
        try
        {
            string strImagePath = "../images/untick.png";

            if (SupportDocument(_contractID))
            {
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[RowIndex]["SupportDocs"].ToString()))
                {
                    strImagePath = "../images/tick.png";
                    Validate = true;
                }
                else
                {
                    Validate = false;
                }
            }
            else
            {
                Validate = true;
                strImagePath = "../images/tick.png";
            }

            return strImagePath;
        }
        catch { return "../images/untick.png"; }
    }
    protected bool getIndecatorUpload(int RowIndex)
    {
        try
        {
            bool _upload = false;

            if (SupportDocument(_contractID))
            {
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[RowIndex]["SupportDocs"].ToString()))
                {
                    _upload = true;
                    Validate = true;
                }
                else
                {
                    _upload = false;
                    Validate = false;
                }
            }
            else
            {
                _upload = true;
            }

            return _upload;
        }
        catch { return false; }
    }
    protected bool SupportDocument(string ClientID)
    {
        bool _required = false;
        UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);
        if (_uploadmanger.WebIndecator("WebSupDocInd", ClientID))
        {
            _required = true;
        }
        else
        {
            _required = false;
        }
        return _required;
    }
    protected void btnValidateData_Click(object sender, EventArgs e)
    {
        try
        {
            string strErrorMsg = "";
            int _falseCount = 0;
            int _currentRow = 0;
            //
            if (Convert.ToString(Session["ContractType"]).ToUpper() != AramcoContractType)
            {

                for (int row = 0; row < gvData.VisibleRowCount; row++)
                {
                    if (!getIndecatorUpload(row))
                    {
                        _falseCount = _falseCount + 1;
                        _currentRow = row + 1;

                        strErrorMsg = strErrorMsg + "Please upload support documents for rows ( " + _currentRow + ")<br>";
                    }
                }
                if (_falseCount > 0)
                {
                    Validate = false;
                }
            }
            if (Validate)
            {
                SubmitDataToCaeser();
            }
            else
            {
                lblMessage.Text = strErrorMsg;
            }

        }
        catch (Exception ex) { lblMessage.Text = ex.Message; }
    }
    protected void SubmitDataToCaeser()
    {
        string strValidationResult = string.Empty;
        OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request = new OS_DXC_WAP.CaesarWS.SubTxnRequest_DN();
        request.TransactionID = WebPublication.GenerateTransactionID2();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.detail = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[ds.Tables[0].Rows.Count];

        if (ds.Tables[0].Rows.Count > 1)
        {
            request.BatchIndicator = "Y";
        }
        else if (ds.Tables[0].Rows.Count == 1)
        {
            request.BatchIndicator = "N";
        }

        WebPublication _webPub = new WebPublication();
        switch (_batchOptionType)
        {
            case "cardreplace":
                #region CardReplace


                //added by Akif
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    strValidationResult = strValidationResult + WebValidationForCardReplacement(ds.Tables[0].Rows[x], x);
                }
                if (strValidationResult != string.Empty)
                {
                    lblMessage.Text = "Please fix the below issue/s:" + "<br/>" + strValidationResult;
                    break;
                }
                //end added by Akif


                request.TransactionType = "CARD REPLACEMENT";
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    request.detail[x] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                    request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                    request.detail[x].ContractNo = _contractID;
                    request.detail[x].MbrName = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString();
                    request.memberName = new String[1];
                    request.memberName[0] = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString();
                    request.detail[x].MembershipNo = ds.Tables[0].Rows[x]["MEMBERHSIP NO"].ToString();
                    request.detail[x].Reason = ds.Tables[0].Rows[x][4].ToString();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x]["CORRECT DOB"].ToString().Trim()))
                        request.detail[x].CorrectDOB = DateTime.Parse(ds.Tables[0].Rows[x]["CORRECT DOB"].ToString().Trim());
                    request.detail[x].BranchCode = ds.Tables[0].Rows[x]["BRANCH CODE"].ToString();
                    request.detail[x].CorrectName = ds.Tables[0].Rows[x]["CORRECT NAME"].ToString();
                    request.detail[x].CorrectTitle = ds.Tables[0].Rows[x]["CORRECT TITLE"].ToString();
                    request.detail[x].CorrectIgamaID = ds.Tables[0].Rows[x]["CORRECT IQAMA ID"].ToString();
                    request.detail[x].CorrectSponsorID = ds.Tables[0].Rows[x]["CORRECT SPONSOR ID"].ToString();
                    request.detail[x].CorrectGender = ds.Tables[0].Rows[x]["CORRECT GENDER"].ToString();
                    request.detail[x].CorrectEmployeeNo = ds.Tables[0].Rows[x]["CORRECT EMPLOYEE NO"].ToString();
                    request.detail[x].CorrectNationality = ds.Tables[0].Rows[x]["CORRECT NATIONALITY"].ToString();
                    if (SupportDocument(_contractID))
                    {
                        string[] SupportDocs = ds.Tables[0].Rows[x]["SupportDocs"].ToString().Split(new char[] { ';' });
                        for (int i = 0; i < SupportDocs.Length; i++)
                        {
                            SupportDocs[i] = _webPub.DocLocation() + SupportDocs[i];
                        }
                        request.detail[x].SupportDoc = SupportDocs;
                    }
                    CallService(request);
                }
                break;
                #endregion
            case "changeclass":
                #region ChangeClass
                request.TransactionType = "CLASS CHANGE";

                //added by yahya
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    strValidationResult = strValidationResult + WebValidationForChangeClassFields(ds.Tables[0].Rows[x], x);
                    strValidationResult = strValidationResult + WebValidationForMemberTypeChangeClass(ds.Tables[0].Rows[x], x);
                }
                //end added by yahya 

                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    if (strValidationResult != string.Empty)
                    {
                        lblMessage.Text = "Please fix the below issues:" + "<br/>" + strValidationResult;
                        break;
                    }

                    request.detail[x] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                    request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                    request.detail[x].ContractNo = _contractID;
                    request.detail[x].MbrName = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString().Trim();
                    request.detail[x].MembershipNo = ds.Tables[0].Rows[x]["MEMBERSHIP NO"].ToString().Trim();
                    request.detail[x].Reason = ds.Tables[0].Rows[x][6].ToString().Trim();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][5].ToString().Trim()))
                        request.detail[x].EffectiveDate = DateTime.Parse(ds.Tables[0].Rows[x][5].ToString().Trim());
                    request.detail[x].ExistingClass = ds.Tables[0].Rows[x]["CLASS ID"].ToString().Trim();
                    request.detail[x].NewClass = ds.Tables[0].Rows[x]["NEW CLASS ID"].ToString().Trim();
                    if (SupportDocument(_contractID))
                    {
                        string[] SupportDocs = ds.Tables[0].Rows[x]["SupportDocs"].ToString().Split(new char[] { ';' });
                        for (int i = 0; i < SupportDocs.Length; i++)
                        {
                            SupportDocs[i] = _webPub.DocLocation() + SupportDocs[i];
                        }
                        request.detail[x].SupportDoc = SupportDocs;
                    }
                    CallService(request);
                }
                break;
                # endregion
            case "changebranch":
                #region ChangeBranch
                request.TransactionType = "BRANCH CHANGE";
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {

                    request.detail[x] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                    request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                    request.detail[x].ContractNo = _contractID;
                    request.detail[x].MbrName = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString();
                    request.memberName = new String[1];
                    request.memberName[0] = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString();
                    request.detail[x].MembershipNo = ds.Tables[0].Rows[x]["MEMBERSHIP NO"].ToString();
                    request.detail[x].Reason = ds.Tables[0].Rows[x][6].ToString();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][5].ToString().Trim()))
                        request.detail[x].EffectiveDate = DateTime.Parse(ds.Tables[0].Rows[x][5].ToString().Trim());

                    request.detail[x].ExistingBranch = ds.Tables[0].Rows[x]["BRANCH CODE"].ToString();
                    request.detail[x].NewBranch = ds.Tables[0].Rows[x]["NEW BRANCH CODE"].ToString();
                    request.detail[x].StaffNo = "";

                    if (SupportDocument(_contractID))
                    {
                        string[] SupportDocs = ds.Tables[0].Rows[x]["SupportDocs"].ToString().Split(new char[] { ';' });
                        for (int i = 0; i < SupportDocs.Length; i++)
                        {
                            SupportDocs[i] = _webPub.DocLocation() + SupportDocs[i];
                        }
                        request.detail[x].SupportDoc = SupportDocs;
                    }
                    CallService(request);
                }
                break;
                #endregion
            case "adddependent":
                #region AddDependent

                //added by yahya
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    strValidationResult = strValidationResult + WebValidationForAddDependentsMandetoryFields(ds.Tables[0].Rows[x], x);

                    strValidationResult = strValidationResult + WebValidationForMemberTypeAddEmployee(ds.Tables[0].Rows[x], x);
                }
                //end added by yahya 

                request.TransactionType = "ADD MEMBER02";
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {

                    if (strValidationResult != string.Empty)
                    {
                        lblMessage.Text = "Please fix the below issues:" + "<br/>" + strValidationResult;
                        break;
                    }

                    request.detail[x] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                    request.detail[x].ContractNo = _contractID;
                    request.detail[x].BranchCode = ds.Tables[0].Rows[x]["Branch Name"].ToString();
                    request.detail[x].MbrName = ds.Tables[0].Rows[x]["Member Name"].ToString();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][8].ToString()))
                        request.detail[x].DOB = DateTime.Parse(ds.Tables[0].Rows[x][8].ToString());
                    request.detail[x].ClassID = ds.Tables[0].Rows[x]["Class Name"].ToString();
                    request.detail[x].MainMemberNo = ds.Tables[0].Rows[x]["Main Member No"].ToString();

                    request.detail[x].DeptCode = ds.Tables[0].Rows[x]["Department Code"].ToString();
                    request.detail[x].IgamaID = ds.Tables[0].Rows[x]["ID Card No"].ToString();
                    //Added By Moustafa
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x]["ID Type"].ToString().Trim()))
                        request.detail[x].ID_Type = ds.Tables[0].Rows[x]["ID Type"].ToString().Trim();

                    //Added By Moustafa : 13 is ID Expiry Date
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][13].ToString().Trim()))
                        request.detail[x].ID_Expiry_Date = DateTime.Parse(ds.Tables[0].Rows[x][13].ToString().Trim());
                    ////if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][13].ToString().Trim()))
                    ////{
                    ////    request.detail[x].ID_Expiry_Date = DateTime.ParseExact(ds.Tables[0].Rows[x][13].ToString().Trim(), "MM/dd/yyyy", null);

                    ////    ////Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB"); //dd/MM/yyyy
                    ////    ////request.detail[x].ID_Expiry_Date = DateTime.Parse(ds.Tables[0].Rows[x][13].ToString().Trim());
                    ////}

                    request.detail[x].SponsorID = ds.Tables[0].Rows[x]["Sponsor ID"].ToString();
                    request.detail[x].StaffNo = ds.Tables[0].Rows[x]["Staff No"].ToString();
                    request.detail[x].Nationality = ds.Tables[0].Rows[x]["Nationality"].ToString();
                    request.detail[x].Gender = ds.Tables[0].Rows[x]["Gender"].ToString();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][6].ToString().Trim()))
                        request.detail[x].RequestStartDate = DateTime.Parse(ds.Tables[0].Rows[x][6].ToString().Trim());
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][6].ToString().Trim()))
                        request.detail[x].EffectiveDate = DateTime.Parse(ds.Tables[0].Rows[x][6].ToString().Trim());

                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][17].ToString()))
                        request.detail[x].ArrivalDate = DateTime.Parse(ds.Tables[0].Rows[x][17].ToString());
                    request.detail[x].PreviousMembershipIndicator = ds.Tables[0].Rows[x][20].ToString();
                    request.detail[x].PreviousMembershipNo = ds.Tables[0].Rows[x][21].ToString();
                    request.detail[x].Reason = ds.Tables[0].Rows[x][19].ToString();
                    request.detail[x].Title = ds.Tables[0].Rows[x]["Member Title"].ToString();
                    request.detail[x].MemberType = ds.Tables[0].Rows[x]["Member Type"].ToString();
                    request.detail[x].MBR_MOBILE_NO = ds.Tables[0].Rows[x][22].ToString();
                    request.detail[x].SMS_PREF_LANG = ds.Tables[0].Rows[x][23].ToString();
                    request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                    request.detail[x].CCHI_City_Code = ds.Tables[0].Rows[x]["CorrDist"].ToString();
                    request.detail[x].CCHI_Job_Code = ds.Tables[0].Rows[x]["ProfCode"].ToString();
                    //added by yahya
                    request.detail[x].YakeenVerified = "N";

                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x]["Marital Status"].ToString().Trim()))
                        request.detail[x].Marital_Status = ds.Tables[0].Rows[x]["Marital Status"].ToString().Trim();

                    if (SupportDocument(_contractID))
                    {
                        string[] SupportDocs = ds.Tables[0].Rows[x]["SupportDocs"].ToString().Split(new char[] { ';' });
                        for (int i = 0; i < SupportDocs.Length; i++)
                        {
                            SupportDocs[i] = _webPub.DocLocation() + SupportDocs[i];
                        }
                        request.detail[x].SupportDoc = SupportDocs;
                    }
                    CallServiceAddMember(request);
                }

                break;
                #endregion
            case "addemployeedependent":
                #region AddEmployeeDependent
                request.TransactionType = "ADD MEMBER02";

                //personservice.
                /*New logic has been implemented by Sakthi on 30-Dec-2015, Passing huge data WCF service throwing error, instead of that same logic build in the 
                Application lvel itself.*/
                /*Old Code Start*/
                //PersonServiceClient pWS = new PersonServiceClient();
                //byte[] resultDSByteArrayTemp = DataManager.DataSetToByteArray(ds);
                //byte[] resultDSByteArray = pWS.GetPersonInfoForBatchAndSyncBupaDB(resultDSByteArrayTemp, 24, YakeenSearhByTypesYakeenSearchByType.GregorianDOB,  true);
                //ds = DataManager.ByteArrayToDataSet(resultDSByteArray);
                /*Old code end*/
                /*New code start*/

                ds = ExpatriatesIqama(ds);

                /*New code end*/

                //check Excel column values
                try
                {
                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        //strValidationResult += "<br/> In Line No. " + (x + 1) + ": <br/>";

                        if (WebValidationForYakeenErrors(ref ds, x) != "")
                            strValidationResult += (x + 1) + ": <br/>" + WebValidationForYakeenErrors(ref ds, x) + "<br/>";
                        if (WebValidationForAddEmployeeDepMandetoryFields(ds.Tables[0].Rows[x], x) != "")
                            strValidationResult += (x + 1) + ": <br/>" + WebValidationForAddEmployeeDepMandetoryFields(ds.Tables[0].Rows[x], x) + " <br/>";
                        if (WebValidationForMemberTypeAddEmployee(ds.Tables[0].Rows[x], x) != "")
                            strValidationResult += (x + 1) + ": <br/>" + WebValidationForMemberTypeAddEmployee(ds.Tables[0].Rows[x], x) + " <br/>";

                        ////strValidationResult += "<br/>";
                        // + strValidationResult;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ex.Message;
                }

                gvData.DataSource = ds.Tables[0];
                gvData.DataBind();

                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {

                    if (strValidationResult != string.Empty && !strValidationResult.Contains("disabled"))
                    {
                        lblMessage.Text = "Please fix the below issues: <br/> In Line No. " + strValidationResult;
                        break;
                    }
                    request.detail[x] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                    request.detail[x].ContractNo = _contractID;
                    request.detail[x].BranchCode = ds.Tables[0].Rows[x]["Branch Code"].ToString().Trim();
                    request.detail[x].MbrName = ds.Tables[0].Rows[x]["Member Name"].ToString().Trim();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x]["Date of Birth"].ToString().Trim()))
                    {

                        // This is assuming you're absolutely sure of the format used. This is *not*
                        // necessarily the user's preferred format. You should think about where your
                        // data is coming from.
                        string dat = ds.Tables[0].Rows[x]["Date of Birth"].ToString().Trim();
                        request.detail[x].DOB = DateTime.Parse(dat);
                        //DateTime date;
                        //if (DateTime.TryParseExact(dat, "dd/MM/yyyy", CultureInfo.InvariantCulture,
                        //                           DateTimeStyles.None, out date))
                        //{
                        //    // Okay, successful parse. We now have the date. Use it, avoiding formatting
                        //    // it back to a string for as long as possible.
                        //}

                        //request.detail[x].DOB = date;
                    }
                    request.detail[x].ClassID = ds.Tables[0].Rows[x]["Class Name"].ToString().Trim();
                    request.detail[x].MainMemberNo = "";
                    request.detail[x].DeptCode = ds.Tables[0].Rows[x]["Department Code"].ToString().Trim();
                    request.detail[x].IgamaID = ds.Tables[0].Rows[x]["ID Card No"].ToString().Trim();

                    //Added By Moustafa
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x]["ID Type"].ToString().Trim()))
                        request.detail[x].ID_Type = ds.Tables[0].Rows[x]["ID Type"].ToString().Trim();

                    //Added By Moustafa : 13 is ID Expiry Date
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][12].ToString().Trim()))
                        request.detail[x].ID_Expiry_Date = DateTime.Parse(ds.Tables[0].Rows[x][12].ToString().Trim());

                    request.detail[x].SponsorID = ds.Tables[0].Rows[x]["Sponsor ID"].ToString().Trim();
                    request.detail[x].StaffNo = ds.Tables[0].Rows[x]["Staff No"].ToString().Trim();
                    request.detail[x].Nationality = ds.Tables[0].Rows[x]["Nationality Code"].ToString().Trim();
                    request.detail[x].Gender = ds.Tables[0].Rows[x]["Gender"].ToString().Trim();
                    request.detail[x].SponsorID = ds.Tables[0].Rows[x]["Sponsor ID"].ToString();
                    request.detail[x].StaffNo = ds.Tables[0].Rows[x]["Staff No"].ToString();
                    request.detail[x].Nationality = ds.Tables[0].Rows[x]["Nationality Code"].ToString();
                    if (ds.Tables[0].Rows[x]["Gender"].ToString().ToLower().StartsWith("m"))
                    {
                        request.detail[x].Gender = "M";
                    }
                    else
                    {
                        request.detail[x].Gender = "F";
                    }

                    try
                    {
                        //5 is Effective Date
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][5].ToString().Trim()))
                            request.detail[x].RequestStartDate = DateTime.Parse(ds.Tables[0].Rows[x][5].ToString().Trim());
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][5].ToString().Trim()))
                            request.detail[x].EffectiveDate = DateTime.Parse(ds.Tables[0].Rows[x][5].ToString().Trim());
                    }
                    catch (Exception)
                    {
                        string dateTemp = ds.Tables[0].Rows[x][5].ToString().Trim();
                        string convertedToddMMYYYY = Convert_MMddyyyy_To_ddMMyyyy(dateTemp);
                        request.detail[x].RequestStartDate = DateTime.Parse(convertedToddMMYYYY);
                        request.detail[x].EffectiveDate = DateTime.Parse(convertedToddMMYYYY);
                    }

                    try
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x]["Member Joining Date with the company (for employees only)"].ToString().Trim()))
                            request.detail[x].JoinDate = DateTime.Parse(ds.Tables[0].Rows[x]["Member Joining Date with the company (for employees only)"].ToString().Trim());

                    }
                    catch (Exception)
                    {
                        string dateTemp = ds.Tables[0].Rows[x]["Member Joining Date with the company (for employees only)"].ToString().Trim();
                        string convertedToddMMYYYY = Convert_MMddyyyy_To_ddMMyyyy(dateTemp);
                        request.detail[x].JoinDate = DateTime.Parse(convertedToddMMYYYY);
                    }

                    try
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x]["Arrival Date in Saudi Arabia"].ToString().Trim()))
                            request.detail[x].ArrivalDate = DateTime.Parse(ds.Tables[0].Rows[x]["Arrival Date in Saudi Arabia"].ToString().Trim());
                    }
                    catch (Exception)
                    {
                        string dateTemp = ds.Tables[0].Rows[x]["Arrival Date in Saudi Arabia"].ToString().Trim();
                        string convertedToddMMYYYY = Convert_MMddyyyy_To_ddMMyyyy(dateTemp);
                        request.detail[x].ArrivalDate = DateTime.Parse(convertedToddMMYYYY);
                    }



                    request.detail[x].PreviousMembershipIndicator = ds.Tables[0].Rows[x]["Has the member or any of his dependents previously been covered by BUPA?"].ToString().Trim();
                    request.detail[x].PreviousMembershipNo = ds.Tables[0].Rows[x]["Previous Membership Number"].ToString().Trim();

                    //18 is 'Reason why joining date is 3 months later than contract start date'
                    request.detail[x].Reason = ds.Tables[0].Rows[x][18].ToString().Trim();
                    request.detail[x].Title = ds.Tables[0].Rows[x]["Member Title"].ToString().Trim();
                    request.detail[x].MemberType = ds.Tables[0].Rows[x]["Member Type"].ToString().Trim();


                    request.detail[x].MBR_MOBILE_NO = ds.Tables[0].Rows[x][21].ToString().Trim();
                    request.detail[x].SMS_PREF_LANG = ds.Tables[0].Rows[x][22].ToString().Trim();

                    request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                    request.detail[x].CCHI_City_Code = ds.Tables[0].Rows[x]["CorrDist"].ToString().Trim();
                    request.detail[x].CCHI_Job_Code = ds.Tables[0].Rows[x]["ProfCode"].ToString().Trim();

                    //added by yahya
                    request.detail[x].YakeenVerified = "N";

                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x]["Marital Status"].ToString().Trim()))
                        request.detail[x].Marital_Status = ds.Tables[0].Rows[x]["Marital Status"].ToString().Trim();

                    if (SupportDocument(_contractID))
                    {
                        string[] SupportDocs = ds.Tables[0].Rows[x]["SupportDocs"].ToString().Split(new char[] { ';' });
                        for (int i = 0; i < SupportDocs.Length; i++)
                        {
                            SupportDocs[i] = _webPub.DocLocation() + SupportDocs[i];
                        }
                        request.detail[x].SupportDoc = SupportDocs;
                    }
                    CallServiceAddMember(request);
                }
                break;
                #endregion
            case "deleteemployee":
                #region DeleteEmployee
                request.TransactionType = "DELETE MEMBER";
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {

                    request.detail[x] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                    request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                    request.detail[x].ContractNo = _contractID;
                    request.detail[x].MbrName = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString();
                    request.memberName = new string[1];
                    request.memberName[0] = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString();
                    request.detail[x].MainMemberNo = ds.Tables[0].Rows[x]["MEMBERSHIP NO"].ToString();
                    request.detail[x].MembershipNo = ds.Tables[0].Rows[x]["MEMBERSHIP NO"].ToString();
                    request.detail[x].Reason = ds.Tables[0].Rows[x][4].ToString();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][3].ToString().Trim()))
                        request.detail[x].MemberDeleteDate = DateTime.Parse(ds.Tables[0].Rows[x][3].ToString().Trim());
                    if (SupportDocument(_contractID))
                    {
                        string[] SupportDocs = ds.Tables[0].Rows[x]["SupportDocs"].ToString().Split(new char[] { ';' });
                        for (int i = 0; i < SupportDocs.Length; i++)
                        {
                            SupportDocs[i] = _webPub.DocLocation() + SupportDocs[i];
                        }
                        request.detail[x].SupportDoc = SupportDocs;
                    }
                    CallService(request);

                }
                break;
                #endregion
            case "deletedependent":

                #region DeleteDependent
                request.TransactionType = "DELETE MEMBER";
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {

                    request.detail[x] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                    request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                    request.detail[x].ContractNo = _contractID;
                    request.detail[x].MbrName = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString();
                    request.memberName = new string[1];
                    request.memberName[0] = ds.Tables[0].Rows[x]["MEMBER NAME"].ToString();
                    request.detail[x].MainMemberNo = ds.Tables[0].Rows[x]["MEMBERSHIP NO"].ToString();
                    request.detail[x].MembershipNo = ds.Tables[0].Rows[x]["MEMBERSHIP NO"].ToString();
                    request.detail[x].Reason = ds.Tables[0].Rows[x][4].ToString();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[x][3].ToString().Trim()))
                        request.detail[x].MemberDeleteDate = DateTime.Parse(ds.Tables[0].Rows[x][3].ToString().Trim());
                    if (SupportDocument(_contractID))
                    {
                        string[] SupportDocs = ds.Tables[0].Rows[x]["SupportDocs"].ToString().Split(new char[] { ';' });
                        for (int i = 0; i < SupportDocs.Length; i++)
                        {
                            SupportDocs[i] = _webPub.DocLocation() + SupportDocs[i];
                        }
                        request.detail[x].SupportDoc = SupportDocs;
                    }
                    CallService(request);

                }
                break;
                #endregion
        }
    }
    string WebValidationForAddEmployeeDepMandetoryFields(DataRow dr, int intLineNo)
    {

        string strReturn = string.Empty;
        try
        {

            if (string.IsNullOrEmpty(dr["Marital Status"].ToString()) == true)
                strReturn = strReturn + "Marital Status is mandotry field <br/>";

            if (string.IsNullOrEmpty(dr["CorrDist"].ToString()) == true)
                strReturn = strReturn + "District is mandotry field <br/>";

            if (string.IsNullOrEmpty(dr["ProfCode"].ToString()) == true)
                strReturn = strReturn + "Profession is mandotry field <br/>";

            if (string.IsNullOrEmpty(dr["Member Type"].ToString()) == true)
                strReturn = strReturn + "Member Type is mandotry field <br/>";

            if (string.IsNullOrEmpty(dr["Class Name"].ToString()) == true)
                strReturn = strReturn + "Class Name is mandotry field <br/>";

            if (string.IsNullOrEmpty(dr["ID Type"].ToString()) == true)
                strReturn = strReturn + "ID Type is mandotry field  <br/>";

            if (string.IsNullOrEmpty(dr["ID Expiry Date\n(dd/mm/yyyy)"].ToString()) == true)
                strReturn = strReturn + "ID Expiry Date is mandotry field  <br/>";

            if (string.IsNullOrEmpty(dr["ID Card No"].ToString()) == true)
                strReturn = strReturn + "Id Card No is mandotry field  <br/>";

            if (string.IsNullOrEmpty(dr["Sponsor ID"].ToString()) == true)
                strReturn = strReturn + "Sponsor ID is mandotry field  <br/>";

            if (string.IsNullOrEmpty(dr["Mobile No\n(Must begin with \"05\" and should not have spaces)"].ToString()) == true)
                strReturn = strReturn + "Mobile No is mandotry field  <br/>";

        }
        catch (Exception ex)
        {

        }

        return strReturn;
    }
    string WebValidationForAddDependentsMandetoryFields(DataRow dr, int intLineNo)
    {

        string strReturn = string.Empty;

        if (string.IsNullOrEmpty(dr["Marital Status"].ToString()) == true)
            strReturn = strReturn + "Marital Status is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["Mobile No\n(Must begin with \"05\" and should not have spaces)"].ToString()) == true)
            strReturn = strReturn + "Mobile is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["CorrDist"].ToString()) == true)
            strReturn = strReturn + "District is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["ProfCode"].ToString()) == true)
            strReturn = strReturn + "Profession is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["ID Expiry Date\n(dd/mm/yyyy)"].ToString()) == true)
            strReturn = strReturn + "ID Expiry Date is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["Member Type"].ToString()) == true)
            strReturn = strReturn + "Member Type is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["Class Name"].ToString()) == true)
            strReturn = strReturn + "Class Name is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["Main Member No"].ToString()) == true)
            strReturn = strReturn + "Main Member No is mandotry field  <br/>";

        return strReturn;
    }
    string WebValidationForChangeClassFields(DataRow dr, int intLineNo)
    {
        string strReturn = string.Empty;

        if (string.IsNullOrEmpty(dr["MEMBERSHIP NO"].ToString()) == true)
            strReturn = strReturn + "MEMBERSHIP NO is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["CLASS ID"].ToString()) == true)
            strReturn = strReturn + "CLASS ID is mandotry field  <br/>";

        if (string.IsNullOrEmpty(dr["NEW CLASS ID"].ToString()) == true)
            strReturn = strReturn + "NEW CLASS ID is mandotry field  <br/>";

        return strReturn;
    }
    string WebValidationForMemberTypeAddEmployee(DataRow dr, int intLineNo)
    {
        string strReturn = string.Empty;

        try
        {

            if (string.IsNullOrEmpty(dr["Member Type"].ToString()) || string.IsNullOrEmpty(dr["Class Name"].ToString()))
            {
                return "";
            }

            string strMemberGroup = dr["Member Type"].ToString();

            if (strMemberGroup == "E")
                strMemberGroup = "E";
            else
                strMemberGroup = "D";

            int intNewClassID = int.Parse(dr["Class Name"].ToString());

            if (!CheckMemberTypeExist(intNewClassID, strMemberGroup))
            {
                strReturn = "The selected Class is not applicable for member type  <br/>";
            }
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
        return strReturn;
    }
    public string Convert_MMddyyyy_To_ddMMyyyy(string strDate)
    {
        Logger.Tracer.WriteMemberEntry();
        string result = string.Empty;
        try
        {
            CultureInfo enUS = new CultureInfo("en-US");
            string dateString = string.Empty;
            dateString = strDate;
            dateString = dateString.Split(' ')[0];


            string part1 = dateString.Split('/')[0];
            string part2 = dateString.Split('/')[1];
            string part3 = dateString.Split('/')[2];

            if (int.Parse(part2) > 12)
                result = dateString.Split('/')[1] + "/" + dateString.Split('/')[0] + "/" + dateString.Split('/')[2];
            else
                result = dateString;

            //// This is assuming you're absolutely sure of the format used. This is *not*
            //// necessarily the user's preferred format. You should think about where your
            //// data is coming from.
            //DateTime date;
            //if (DateTime.TryParseExact(dateString, "dd/MM/yyyy", enUS,
            //                           DateTimeStyles.None, out date))
            //{
            //    // Okay, successful parse. We now have the date. Use it, avoiding formatting
            //    // it back to a string for as long as possible.
            //}
            //result = date.ToString(); //dateString.Split('/')[1] + "/" + dateString.Split('/')[0] + "/" + dateString.Split('/')[2];
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
        return result;
    }
    string WebValidationForYakeenErrors(ref DataSet dsExcelAndYakeen, int intLineNo)
    {
        string strReturn = string.Empty;
        string strYakeenError = string.Empty;
        try
        {

            DataRow drExcel = dsExcelAndYakeen.Tables[0].Rows[intLineNo];
            DataRow drYakeen;
            if (dsExcelAndYakeen.Tables.Count > 2)
                drYakeen = dsExcelAndYakeen.Tables[2].Rows[intLineNo];
            else
                drYakeen = dsExcelAndYakeen.Tables[1].Rows[intLineNo];

            if (drYakeen["Notes"] == null || string.IsNullOrWhiteSpace(drYakeen["Notes"].ToString()))
            {
                //Name
                if (drYakeen["Name En"] != null && !string.IsNullOrWhiteSpace(drYakeen["Name En"].ToString()))
                {
                    drExcel["Member Name"] = drYakeen["Name En"].ToString();
                }

                //DOB
                if (!string.IsNullOrWhiteSpace(drYakeen["DOB(G)"].ToString()))
                {
                    drExcel["Date of Birth"] = Convert_MMddyyyy_To_ddMMyyyy(drYakeen["DOB(G)"].ToString());

                    //DateTime result;
                    //CultureInfo provider = CultureInfo.InvariantCulture;
                    //format = "g";
                    //provider = new CultureInfo("en-US");
                    //try
                    //{
                    //    drExcel["Date of Birth"] = DateTime.ParseExact(dateString, format, provider);
                    //}
                    //catch (Exception)
                    //{
                    //    DateTime date;
                    //    if (DateTime.TryParseExact(dateString, "dd/MM/yyyy", CultureInfo.InvariantCulture,DateTimeStyles.None, out date))
                    //        drExcel["Date of Birth"] = date.ToString();
                    //    else
                    //    {
                    //    }
                    //}
                }

                //Gender
                if (drYakeen["Gender"] != null && !string.IsNullOrWhiteSpace(drYakeen["Gender"].ToString()))
                    drExcel["Gender"] = drYakeen["Gender"].ToString();

                //ID Expiry date
                if (drYakeen["ID Expiry"] != null && !string.IsNullOrWhiteSpace(drYakeen["ID Expiry"].ToString()))
                    drExcel["ID Expiry Date\n(dd/mm/yyyy)"] = drYakeen["ID Expiry"].ToString();
            }
            else
            {
                strYakeenError = drYakeen["Notes"].ToString();
            }

            /*txtMemberName.Text = Regex.Replace(txtMemberName.Text.ToString(), @"\s+", " ");
            txtDateOfBirth.GregorianDate = SaudiMaleResponse.dateOfBirthG;
            ddlGender.SelectedValue = SaudiMaleResponse.gender.ToString();
            HandleDependentGenderTitleFromYaqeen(SaudiMaleResponse.gender.ToString());
            txtIDExpDate.HijriDate = SaudiMaleResponse.idExpiryDate;*/

            //return dsExcelAndYakeen;
            //if (string.IsNullOrEmpty(dr["SupportDocs"].ToString()))
            //{
            //    return "";
            //}

            //string strYakeenError = dr["SupportDocs"].ToString();
            strReturn = strYakeenError + "  <br/>";
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
        return strYakeenError;
    }
    string WebValidationForMemberTypeChangeClass(DataRow dr, int intLineNo)
    {
        string strReturn = string.Empty;
        try
        {


            if (string.IsNullOrEmpty(dr["MEMBERSHIP NO"].ToString()) || string.IsNullOrEmpty(dr["NEW CLASS ID"].ToString()))
            {
                return "";
            }

            string strMembershipNo = dr["MEMBERSHIP NO"].ToString();

            string strMemberGroup;

            MemberHelper mh = new MemberHelper(strMembershipNo, _contractID);
            Bupa.OSWeb.Business.Member m = mh.GetMemberDetails();
            if (!string.IsNullOrEmpty(Convert.ToString(m.MemberName)))
            {
                if (m.MemberType == "Employee")
                    strMemberGroup = "E";
                else
                    strMemberGroup = "D";

                int intNewClassID = int.Parse(dr["NEW CLASS ID"].ToString());

                if (!CheckMemberTypeExist(intNewClassID, strMemberGroup))
                {
                    strReturn = "The selected Class is not applicable for member type  <br/>";
                }
            }
            else
            {
                strReturn = "Membership No (" + strMembershipNo + ") not correct  <br/>";
            }

        }
        catch (Exception ex)
        {
            string error = ex.ToString();
        }
        return strReturn;
    }
    string WebValidationForCardReplacement(DataRow dr, int intLineNo)
    {
        string strReturn = string.Empty;

        string strcardReplacementReason = dr["CARD REPLACEMENT REASON\n(Input Reason Code Only)"].ToString();

        if (!string.IsNullOrEmpty(strcardReplacementReason) &&
            (strcardReplacementReason.Trim().Equals("REP008") || strcardReplacementReason.Trim().Equals("REP010")))
        {
            strReturn = "Error in Line No. " + (intLineNo + 1) + ": For wrong Saudi/Iqama ID and to change from Entry No. to Iqama No. Please submit your request to <a href='mailto:CCHI@bupa.com.sa?Subject=Card%20Replacement%20Wrong%20Saudi%20Id' target='_top' >CCHI@bupa.com.sa</a><br/>";
        }
        return strReturn;
    }
    private bool CheckMemberTypeExist(int intNewclassID, string strMemberGroup)
    {
        OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
        OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqContMbrTypeListResponse_DN response;
        request = new OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN();
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        long ls_txnid = WebPublication.GenerateTransactionID();
        request.transactionID = ls_txnid;
        request.contNo = _contractID;
        request.contCls = intNewclassID; ;
        request.grp_Type = strMemberGroup;

        response = ws.EnqContMbrTypeList(request);

        if (response.status == "0")
        {
            if (response.detail.Length > 0)
            {
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }
    }
    protected void gvData_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ds = (DataSet)Session["DataSet"];
        gvData.DataSource = ds.Tables[0];
        gvData.DataBind();
    }
    protected void gvData_DataBound(object sender, EventArgs e)
    {
        GridViewDataTextColumn col = ((ASPxGridView)sender).Columns["Main Member No"] as GridViewDataTextColumn;
        ValidateNumericDate(col, 7);
        GridViewDataTextColumn col1 = ((ASPxGridView)sender).Columns["ID Card No"] as GridViewDataTextColumn;
        ValidateNumericDate(col1, 10);
        GridViewDataTextColumn col3 = ((ASPxGridView)sender).Columns["Branch Name"] as GridViewDataTextColumn;
        ValidateNumericDate(col3, 8);
        GridViewDataTextColumn col4 = ((ASPxGridView)sender).Columns["Sponsor ID"] as GridViewDataTextColumn;
        ValidateNumericDate(col4, 10);
        GridViewDataTextColumn col5 = ((ASPxGridView)sender).Columns["Department Code"] as GridViewDataTextColumn;
        ValidateNumericDate(col5, 8);
        GridViewDataTextColumn col6 = ((ASPxGridView)sender).Columns["Nationality Code"] as GridViewDataTextColumn;
        ValidateNumericDate(col6, 3);
        GridViewDataTextColumn col7 = ((ASPxGridView)sender).Columns["MEMBERHSIP NO"] as GridViewDataTextColumn;
        ValidateNumericDate(col7, 7);
        GridViewDataTextColumn col8 = ((ASPxGridView)sender).Columns["BRANCH CODE"] as GridViewDataTextColumn;
        ValidateNumericDate(col8, 8);
        GridViewDataTextColumn col9 = ((ASPxGridView)sender).Columns["CORRECT IQAMA ID"] as GridViewDataTextColumn;
        ValidateNumericDate(col9, 10);
        GridViewDataTextColumn col11 = ((ASPxGridView)sender).Columns["CORRECT SPONSOR ID"] as GridViewDataTextColumn;
        ValidateNumericDate(col11, 10);
        GridViewDataTextColumn col12 = ((ASPxGridView)sender).Columns["CORRECT NATIONALITY"] as GridViewDataTextColumn;
        ValidateNumericDate(col12, 3);
        GridViewDataTextColumn col13 = ((ASPxGridView)sender).Columns["MEMBERSHIP NO"] as GridViewDataTextColumn;
        ValidateNumericDate(col13, 7);
        GridViewDataTextColumn col14 = ((ASPxGridView)sender).Columns["NEW BRANCH CODE"] as GridViewDataTextColumn;
        ValidateNumericDate(col14, 8);
        GridViewDataTextColumn col15 = ((ASPxGridView)sender).Columns["Nationality"] as GridViewDataTextColumn;
        ValidateNumericDate(col15, 3);
    }
    private static void ValidateNumericDate(GridViewDataTextColumn column, int maxTextLength)
    {
        if (column != null)
        {
            column.PropertiesTextEdit.MaxLength = maxTextLength;
            column.PropertiesTextEdit.ValidationSettings.RegularExpression.ValidationExpression = "[0-9.,]+";
            column.PropertiesTextEdit.ValidationSettings.RegularExpression.ErrorText = "This value should be a number";
            column.PropertiesTextEdit.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
        }
    }
    private DataSet ExpatriatesIqama(DataSet dsFromExcel)
    {
        try
        {
            //This will contain two data tables, 1st for dsExcel 2nd for DB rows if any
            ////DataSet dsFromExcel = new DataSet();

            System.Data.DataTable dt = new System.Data.DataTable();

            ///DataTable dt = new DataTable();
            dt.Columns.Add("ID Card No", typeof(string));//Card Id
            dt.Columns.Add("Sponsor ID", typeof(string));//SponsorID
            dt.Columns.Add("Name Ar", typeof(string));
            dt.Columns.Add("Name En", typeof(string));
            dt.Columns.Add("DOB(G)", typeof(string));
            dt.Columns.Add("DOB(H)", typeof(string));
            dt.Columns.Add("Gender", typeof(string));
            dt.Columns.Add("Dependents List Counter", typeof(string));
            dt.Columns.Add("Entry Date", typeof(string));
            dt.Columns.Add("Occupation Desc", typeof(string));
            dt.Columns.Add("Sponsor Name", typeof(string));
            dt.Columns.Add("Nationality Code", typeof(string));
            dt.Columns.Add("ID Expiry", typeof(string));
            dt.Columns.Add("Border Entry No.", typeof(string));
            dt.Columns.Add("Notes", typeof(string));
            //Yakeen New Fields
            dt.Columns.Add("Id Issue Place", typeof(string));
            dt.Columns.Add("Job Code", typeof(string));
            dt.Columns.Add("Sponsor Id", typeof(string));
            dt.Columns.Add("Relationship", typeof(string));
            dt.Columns.Add("Visa Expiry Date", typeof(string));
            dt.Columns.Add("Entry Port", typeof(string));




            string NameAr = "";
            string NameEn = "";
            ////dsFromExcel = DataManager.GetDataFromExcel(Convert.ToString(Session["_path"]));

            RadProgressContext context = RadProgressContext.Current;
            context.PrimaryTotal = dsFromExcel.Tables[0].Rows.Count;
            context.SecondaryTotal = 100;
            int total = dsFromExcel.Tables[0].Rows.Count;

            var sw = Stopwatch.StartNew();
            //YakeenDataValidHours
            string strValidHours = ConfigurationManager.AppSettings["YakeenDataValidHours"].ToString();
            //Yakeen Validate Saudi Info
            bool validateCitizenInfo = Boolean.Parse(ConfigurationManager.AppSettings["ValidateCitizenInfo"].ToString());
            int dataValidityHours = 0;
            int.TryParse(strValidHours, out dataValidityHours);

            //create Person web service instance
            ////PersonServiceClient pWS = new PersonServiceClient();
            PersonServiceClient pWS = new PersonServiceClient();
            Person personRecord = new Person();
            string personIdentity = string.Empty;
            string searchByValue = string.Empty;


            for (int i = 0; i < dsFromExcel.Tables[0].Rows.Count; i++)
            {
                context.PrimaryTotal = 1;
                context.PrimaryValue = 1;
                context.PrimaryPercent = 100;

                context.SecondaryTotal = total;
                context.SecondaryValue = i.ToString();
                context.SecondaryPercent = (double.Parse(i.ToString()) / double.Parse(total.ToString()) * 100).ToString("0.0");
                context.CurrentOperationText = i.ToString();

                context.TimeEstimated = (total - i) * 3 * 100;
                if (!Response.IsClientConnected)
                {
                    //Cancel button was clicked or the browser was closed, so stop processing
                    break;
                }

                var item = dsFromExcel.Tables[0].Rows[i];
                personIdentity = item["ID Card No"].ToString();
                searchByValue = item["Sponsor ID"].ToString();

                if (!validateCitizenInfo && personIdentity.StartsWith("1"))// not checking in Yakeen for Saudi Citizens
                {
                    dt.Rows.Add(personIdentity, searchByValue,
                                       ""/*Name Ar*/, ""/*Name En*/, ""/*DOB G*/, ""/*DOB H*/, ""/*Gender*/, ""/*Dependents*/, ""/*Entry Date*/, ""/*Occupation*/,
                                       ""/*Sponsor name*/, ""/*Nationality*/, ""/*Id expiry*/, ""/*Border Entry No.*/,
                        /*Yakeen new fields*/"", "", "", "", "", "",
                                       "Not checked- Saudi citizen check with YAKEEN disabled" /*Notes*/);
                }
                else
                {
                    try
                    {
                        ////if (personIdentity.StartsWith("2"))
                        ////    personRecord = pWS.GetPersonInfoForBatch(personIdentity, searchByValue, dataValidityHours, true, YakeenSearhByTypesYakeenSearchByType.IqamaId, true, validateCitizenInfo, true);
                        ////else if (personIdentity.StartsWith("3") || personIdentity.StartsWith("4"))
                        ////    personRecord = pWS.GetPersonInfoForBatch(personIdentity, searchByValue, dataValidityHours, true, YakeenSearhByTypesYakeenSearchByType.BorderEntry, true, validateCitizenInfo, true);
                        //// Person Service Revamp integration implemented on 02-Nov-2016 By Sakthi
                        //// Old Code Start
                        ////if (personIdentity.StartsWith("2"))
                        ////    personRecord = pWS.GetPersonInfoForBatch(personIdentity, searchByValue, dataValidityHours, true, YakeenSearhByTypesYakeenSearchByType.IqamaId, true, validateCitizenInfo, true, true, true);      //    personRecord = pWS.GetPersonInfoForBatch(personIdentity, searchByValue, dataValidityHours, YakeenSearhByTypesYakeenSearchByType.IqamaId, validateCitizenInfo, true);
                        ////else if (personIdentity.StartsWith("3") || personIdentity.StartsWith("4"))
                        ////    personRecord = pWS.GetPersonInfoForBatch(personIdentity, searchByValue, dataValidityHours, true, YakeenSearhByTypesYakeenSearchByType.BorderEntry, true, validateCitizenInfo, true, true, true);   //    personRecord = pWS.GetPersonInfoForBatch(personIdentity, searchByValue, dataValidityHours, YakeenSearhByTypesYakeenSearchByType.BorderEntry, validateCitizenInfo, true);
                        //// Old Code End
                        //// New Code Start                        
                        if (personIdentity.StartsWith("2"))
                            personRecord = pWS.GetPersonInfoForBatch(personIdentity, searchByValue, WebApp.CommonClass.YakeenValidationHours, YakeenSearhByTypesYakeenSearchByType.IqamaId, WebApp.CommonClass.FromUserName, WebApp.CommonClass.FromDepartmentName, WebApp.CommonClass.FromSourceName,true, true);
                        else if (personIdentity.StartsWith("3") || personIdentity.StartsWith("4"))
                            personRecord = pWS.GetPersonInfoForBatch(personIdentity, searchByValue, WebApp.CommonClass.YakeenValidationHours, YakeenSearhByTypesYakeenSearchByType.BorderEntry, WebApp.CommonClass.FromUserName, WebApp.CommonClass.FromDepartmentName, WebApp.CommonClass.FromSourceName, true,true);
                        ////New Code End

                        if (personRecord != null && string.IsNullOrEmpty(personRecord.ExceptionMessage))
                        {
                           
                            //// New code Added by Sakthi on 26-Dec-2017 Should Not pass arabic name to Caesar
                            /// Start
                            string name = string.Empty;

                            if (!string.IsNullOrWhiteSpace(personRecord.EnglishName))
                                name = ArabicToEnglish(personRecord.FullArabicName, personRecord.EnglishName);
                            else if (!string.IsNullOrWhiteSpace(personRecord.FullEnglishName))
                                name = ArabicToEnglish(personRecord.FullArabicName, personRecord.FullEnglishName);

                            if (string.IsNullOrEmpty(name.Trim()))
                                name = Convert.ToString(dsFromExcel.Tables[0].Rows[i][3]);
                           
                            NameEn = name;
                            NameAr = name;

                            ////Old Code Start
                            //NameEn = personRecord.FullEnglishName;
                            //NameAr = personRecord.FullArabicName;

                            //if (string.IsNullOrWhiteSpace(personRecord.FullArabicName))
                            //    NameAr = personRecord.ArabicName;

                            //if (string.IsNullOrWhiteSpace(personRecord.FullEnglishName))
                            //    NameEn = personRecord.EnglishName;
                           
                            ////Old Code End
                            ////End


                           

                            string notes = string.Empty;
                            //if (string.IsNullOrWhiteSpace(NameEn))
                            //{
                            //    notes = NameAr;
                            //    NameAr = "";
                            //}
                            string HijriDOB = personRecord.BirthDateHijri;
                            string GregorianDOB = personRecord.BirthDateGregorian.Value.ToString("dd/MM/yyyy");

                            string Gender = personRecord.Gender.ToString();
                            ////string IdCardExpiryDate = personRecord.IdExpiryDate.ToString();
                            string IdCardExpiryDate = string.IsNullOrWhiteSpace(personRecord.IdExpiryDate) ? "" : Convert.ToString(personRecord.IdExpiryDate).Contains("01/01/0001") ? "" : personRecord.IdExpiryDate;
                            string secondId = searchByValue;// Convert.ToString(dsOut.Tables[0].Rows[i][1]);
                            string occupation = personRecord.OccupationAr;
                            string sponsorName = personRecord.SponsorNameAr;
                            string nationality = personRecord.NationalityCode;
                            string idIssuePlace = string.IsNullOrWhiteSpace(personRecord.IdIssuePlace) ? "" : personRecord.IdIssuePlace;
                            string jobCode = string.IsNullOrWhiteSpace(personRecord.JobCode) ? "" : personRecord.JobCode;
                            string sponsorId = string.IsNullOrWhiteSpace(personRecord.SponsorName) ? "" : personRecord.SponsorName;
                            string relationship = string.IsNullOrWhiteSpace(personRecord.Relationship) ? "" : personRecord.Relationship;
                            ////string visaExpiryDate = string.IsNullOrWhiteSpace(personRecord.VisaExpiryDate) ? "" : personRecord.VisaExpiryDate;
                            string visaExpiryDate = string.IsNullOrWhiteSpace(personRecord.VisaExpiryDate) ? "" : Convert.ToString(personRecord.VisaExpiryDate).Contains("01/01/0001") ? "" : personRecord.VisaExpiryDate;
                            string entryPort = string.IsNullOrWhiteSpace(personRecord.EntryPort) ? "" : Convert.ToString(personRecord.EntryPort);

                            dt.Rows.Add(
                                        personIdentity,
                                        secondId,
                                        NameAr/*Name Ar*/,
                                        NameEn/*Name En*/,
                                        GregorianDOB/*DOB G*/,
                                        HijriDOB/*DOB H*/,
                                        Gender/*Gender*/,
                                        ""/*Dependents*/,
                                        ""/*Entry Date*/,
                                        occupation/*Occupation*/,
                                        sponsorName/*Sponsor name*/,
                                        nationality/*Nationality*/,
                                        IdCardExpiryDate/*Id expiry*/,
                                        ""/*Border Entry No.*/,
                                        notes /*Notes*/,
                                        idIssuePlace,
                                        jobCode,
                                        sponsorId,
                                        relationship,
                                        visaExpiryDate,
                                        entryPort);
                        }
                        else
                        {
                            dt.Rows.Add(personIdentity, searchByValue,
                            ""/*Name Ar*/,
                            ""/*Name En*/,
                            ""/*DOB G*/,
                            ""/*DOB H*/,
                            ""/*Gender*/,
                            ""/*Dependents*/,
                            ""/*Entry Date*/,
                            ""/*Occupation*/,
                            ""/*Sponsor name*/,
                            ""/*Nationality*/,
                            ""/*Id expiry*/,
                            ""/*Border Entry No.*/,
                            personRecord.ExceptionMessage /*Notes*/,
                            "", "", "", "", "", "");
                        }
                    }
                    catch (Exception ex)
                    {
                        dt.Rows.Add(personIdentity, searchByValue,
                            ""/*Name Ar*/,
                            ""/*Name En*/,
                            ""/*DOB G*/,
                            ""/*DOB H*/,
                            ""/*Gender*/,
                            ""/*Dependents*/,
                            ""/*Entry Date*/,
                            ""/*Occupation*/,
                            ""/*Sponsor name*/,
                            ""/*Nationality*/,
                            ""/*Id expiry*/,
                            ""/*Border Entry No.*/,
                            ex.Message /*Notes*/,
                            "", "", "", "", "", "");
                    }
                }



            }

            dsFromExcel.Tables.Add(dt);

            //Logger.Current.Warning("File varification end time:" + DateTime.Now.ToString());
            sw.Stop();
            TimeSpan time = sw.Elapsed;
            Logger.Current.Warning(string.Format("Total duration in seconds: [{0}s]", (int)sw.Elapsed.TotalSeconds));
            return dsFromExcel;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw ex;
        }
    }
    protected void OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CapList cap = (CapList)e.Item.DataItem;

            Repeater rptCap = e.Item.FindControl("rptSponsor") as Repeater;
            rptCap.DataSource = cap.Caps;
            rptCap.DataBind();
        }
    }
    private List<CCHISponsorDetails> GetCCHICap(string _type, System.Data.DataTable dtExcelValue)
    {
        List<CCHISponsorDetails> lstExcelValue = new List<CCHISponsorDetails>();
        if (dtExcelValue != null && dtExcelValue.Rows.Count > 0)
        {
            System.Data.DataTable dtOptmize = new System.Data.DataTable();
            List<AddEmployeeDependent> lstAddEmployeeDependent = new List<AddEmployeeDependent>();
            AddEmployeeDependent objAddEmployeeDependent;
            List<CCHISponsorDetails> lstCCHISponsorDetails = new List<CCHISponsorDetails>();
            CCHISponsorDetails objCCHISponsorDetails;


            if (_type.ToLower().Equals("addemployeedependent"))
            {
                #region AddEmployeeAndDependent
              
                dtOptmize = dtExcelValue.DefaultView.ToTable(true, "ID Card No", "Sponsor ID", "Member Type");

                ////Parallel.ForEach(dtOptmize.AsEnumerable(), drow =>{
                ////    objAddEmployeeDependent = new AddEmployeeDependent();
                ////    objAddEmployeeDependent.IDCardNo = Convert.ToString(drow.ItemArray[0]);
                ////    objAddEmployeeDependent.SponsorID = Convert.ToString(drow.ItemArray[1]);
                ////    objAddEmployeeDependent.MemberType = Convert.ToString(drow.ItemArray[2]);
                ////    lstAddEmployeeDependent.Add(objAddEmployeeDependent);
                ////});

                foreach (DataRow row in dtOptmize.Rows)
                {
                    objAddEmployeeDependent = new AddEmployeeDependent();
                    objAddEmployeeDependent.IDCardNo = Convert.ToString(row[0]);
                    objAddEmployeeDependent.SponsorID = Convert.ToString(row[1]);
                    objAddEmployeeDependent.MemberType = Convert.ToString(row[2]);
                    lstAddEmployeeDependent.Add(objAddEmployeeDependent);
                }

                if (lstAddEmployeeDependent != null && lstAddEmployeeDependent.Count() > 0)
                {
                    var lstSponsors = (from item in lstAddEmployeeDependent where item.MemberType.ToLower().Trim().Equals("e") select item.SponsorID).Distinct().OrderBy(SponsorID => SponsorID);
                    int saudiMain = 0, saudiDependent = 0, nonSaudiMain = 0, nonSaudiDependent = 0;

                    foreach (var item in lstSponsors)
                    {
                        saudiMain = 0; saudiDependent = 0; nonSaudiMain = 0; nonSaudiDependent = 0;
                        saudiMain = (from x in lstAddEmployeeDependent where x.SponsorID == item && x.IDCardNo.StartsWith("1") && x.MemberType.Trim().ToLower().Equals("e") select x.SponsorID).Count();
                        nonSaudiMain = (from x in lstAddEmployeeDependent where x.SponsorID == item && x.MemberType.Trim().ToLower().Equals("e") && (x.IDCardNo.StartsWith("2") || x.IDCardNo.StartsWith("3") || x.IDCardNo.StartsWith("4")) select x.SponsorID).Count();
                        var saudiIDNumbers = (from y in lstAddEmployeeDependent where y.SponsorID == item && y.IDCardNo.StartsWith("1") && y.MemberType.Trim().ToLower().Equals("e") select y.IDCardNo).Distinct().OrderBy(IDCardNo => IDCardNo);
                        var nonSaudiIDNumbers = (from y in lstAddEmployeeDependent where y.SponsorID == item && y.MemberType.Trim().ToLower().Equals("e") && (y.IDCardNo.StartsWith("2") || y.IDCardNo.StartsWith("3") || y.IDCardNo.StartsWith("4")) select y.IDCardNo).Distinct().OrderBy(IDCardNo => IDCardNo);
                        saudiDependent = (from m in lstAddEmployeeDependent join c in saudiIDNumbers on m.SponsorID equals c select new {m.IDCardNo}).Count();
                        nonSaudiDependent = (from m in lstAddEmployeeDependent join c in nonSaudiIDNumbers on m.SponsorID equals c select new { m.IDCardNo }).Count();

                        objCCHISponsorDetails = new CCHISponsorDetails();
                        objCCHISponsorDetails.SponsorID = item;
                        objCCHISponsorDetails.SaudiMain = saudiMain;
                        objCCHISponsorDetails.NonSaudiMain = nonSaudiMain;
                        objCCHISponsorDetails.SaudiDependent = saudiDependent;
                        objCCHISponsorDetails.NonSaudiDependent = nonSaudiDependent;
                        lstExcelValue.Add(objCCHISponsorDetails);

                        ////foreach (var saudiItem in saudiIDNumbers)
                        ////{
                        ////    saudiDependent += (from x in lstAddEmployeeDependent where x.SponsorID == saudiItem && x.MemberType.Trim().ToLower() != "e" select x.IDCardNo).Count();
                        ////}
                        ////foreach (var nonSaudiItem in nonSaudiIDNumbers)
                        ////{
                        ////    nonSaudiDependent += (from x in lstAddEmployeeDependent where x.SponsorID == nonSaudiItem && x.MemberType.Trim().ToLower() != "e" select x.IDCardNo).Count();
                        ////}

                       

                    }
                }
                #endregion
            }
            else
            {
                #region DependentOnly
                dtOptmize = dtExcelValue.DefaultView.ToTable(true, "ID Card No", "Sponsor ID", "Member Type", "Main Member No");
                dtOptmize.Columns.Add("MainMemberSponsorID", typeof(System.String));
                dtOptmize.Columns.Add("MainMemberIDNumber", typeof(System.String));
                DataView dv = dtOptmize.DefaultView;
                dv.Sort = "Main Member No asc";
                dtOptmize = dv.ToTable();

                DataView view = new DataView(dtOptmize);
                System.Data.DataTable distinctMainMembers = view.ToTable(true, "Main Member No");


                ///int i = 0;
                foreach (DataRow row in distinctMainMembers.Rows)
                {
                    Tuple<string, string> tuple;
                    tuple = GetMainMemberSponsorID(row[0].ToString());
                    dtOptmize.Select(string.Format("[Main Member No] = '{0}'", row[0].ToString())).ToList<DataRow>().ForEach(r => { r["MainMemberSponsorID"] = tuple.Item1; r["MainMemberIDNumber"] = tuple.Item2; });
                    dtOptmize.AcceptChanges();
                }

                ////Parallel.ForEach(dtOptmize.AsEnumerable(), drow =>
                ////{  
                ////    objAddEmployeeDependent = new AddEmployeeDependent();
                ////    objAddEmployeeDependent.IDCardNo = Convert.ToString(drow.ItemArray[0]);
                ////    objAddEmployeeDependent.SponsorID = Convert.ToString(drow.ItemArray[1]);
                ////    objAddEmployeeDependent.MemberType = Convert.ToString(drow.ItemArray[2]);
                ////    objAddEmployeeDependent.MainMembershipNo = Convert.ToString(drow.ItemArray[3]);
                ////    objAddEmployeeDependent.MainMembershipSponsorID = Convert.ToString(drow.ItemArray[4]);
                ////    objAddEmployeeDependent.MainMembershipIDCardNo = Convert.ToString(drow.ItemArray[5]);
                ////    lstAddEmployeeDependent.Add(objAddEmployeeDependent);
                ////});


                foreach (DataRow row in dtOptmize.Rows)
                {
                    objAddEmployeeDependent = new AddEmployeeDependent();
                    objAddEmployeeDependent.IDCardNo = Convert.ToString(row[0]);
                    objAddEmployeeDependent.SponsorID = Convert.ToString(row[1]);
                    objAddEmployeeDependent.MemberType = Convert.ToString(row[2]);
                    objAddEmployeeDependent.MainMembershipNo = Convert.ToString(row[3]);
                    objAddEmployeeDependent.MainMembershipSponsorID = Convert.ToString(row[4]);
                    objAddEmployeeDependent.MainMembershipIDCardNo = Convert.ToString(row[5]);
                    lstAddEmployeeDependent.Add(objAddEmployeeDependent);
                }
                if (lstAddEmployeeDependent != null && lstAddEmployeeDependent.Count() > 0)
                {

                    var lstMainMembers = (from item in lstAddEmployeeDependent select item.MainMembershipSponsorID).Distinct().OrderBy(MainMembershipSponsorID => MainMembershipSponsorID);

                    ///string itemCount = lstMainMembers.Count().ToString();
                    int saudiMain = 0, saudiDependent = 0, nonSaudiMain = 0, nonSaudiDependent = 0;
                    foreach (var item in lstMainMembers)
                    {
                        saudiMain = 0; saudiDependent = 0; nonSaudiMain = 0; nonSaudiDependent = 0;
                        saudiMain = (from x in lstAddEmployeeDependent where x.MainMembershipSponsorID == item && x.MainMembershipIDCardNo.StartsWith("1") select x.MainMembershipNo).Distinct().Count();
                        nonSaudiMain = (from x in lstAddEmployeeDependent where x.MainMembershipSponsorID == item && (x.MainMembershipIDCardNo.StartsWith("2") || x.MainMembershipIDCardNo.StartsWith("3") || x.MainMembershipIDCardNo.StartsWith("4")) select x.MainMembershipNo).Distinct().Count();
                        var saudiIDNumbers = (from y in lstAddEmployeeDependent where y.MainMembershipSponsorID == item && y.MainMembershipIDCardNo.StartsWith("1") select y.MainMembershipIDCardNo).Distinct().OrderBy(MainMembershipIDCardNo => MainMembershipIDCardNo);
                        var nonSaudiIDNumbers = (from y in lstAddEmployeeDependent where y.MainMembershipSponsorID == item && (y.MainMembershipIDCardNo.StartsWith("2") || y.MainMembershipIDCardNo.StartsWith("3") || y.MainMembershipIDCardNo.StartsWith("4")) select y.MainMembershipIDCardNo).Distinct().OrderBy(MainMembershipIDCardNo => MainMembershipIDCardNo);
                        saudiDependent = (from m in lstAddEmployeeDependent join c in saudiIDNumbers on m.MainMembershipIDCardNo equals c select new { m.IDCardNo }).Count();
                        nonSaudiDependent = (from m in lstAddEmployeeDependent join c in nonSaudiIDNumbers on m.MainMembershipIDCardNo equals c select new { m.IDCardNo }).Count();
                        
                        objCCHISponsorDetails = new CCHISponsorDetails();
                        objCCHISponsorDetails.SponsorID = item;
                        objCCHISponsorDetails.SaudiMain = saudiMain;
                        objCCHISponsorDetails.NonSaudiMain = nonSaudiMain;
                        objCCHISponsorDetails.SaudiDependent = saudiDependent;
                        objCCHISponsorDetails.NonSaudiDependent = nonSaudiDependent;
                        lstExcelValue.Add(objCCHISponsorDetails);

                        ////foreach (var saudiItem in saudiIDNumbers)
                        ////{
                        ////    saudiDependent += (from x in lstAddEmployeeDependent where x.MainMembershipIDCardNo == saudiItem select x.IDCardNo).Count();
                        ////}
                        ////foreach (var nonSaudiItem in nonSaudiIDNumbers)
                        ////{
                        ////    nonSaudiDependent += (from x in lstAddEmployeeDependent where x.MainMembershipIDCardNo == nonSaudiItem select x.IDCardNo).Count();
                        ////}

                       

                    }
                }
                #endregion
            }

        }


        return lstExcelValue;
    }
    protected Tuple<string, string> GetMainMemberSponsorID(string mainMembershipNo)
    {
        Tuple<string, string> mainMember;

        try
        {
            MemberHelper mh = new MemberHelper(mainMembershipNo, Convert.ToString(Session["ClientID"]));
            Bupa.OSWeb.Business.Member m = mh.GetMemberDetails();
            mainMember = new Tuple<string, string>(m.SponsorID, m.SaudiIqamaID);
        }
        catch (Exception ex)
        {
            lblMessage.Text += "Invalid Main Membership Number :" + mainMembershipNo + "<br/>";
            mainMember = new Tuple<string, string>("Invalid Main Member:" + mainMembershipNo, "Error in get sponsor ID:" + mainMembershipNo);
        }

        return mainMember;
    }
    protected void rptSponsor_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            if (e.Item.ItemIndex == 2)
            {
                System.Web.UI.HtmlControls.HtmlContainerControl saudiMain = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("saudiMain");
                System.Web.UI.HtmlControls.HtmlContainerControl saudiDependent = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("SaudiDependent");
                System.Web.UI.HtmlControls.HtmlContainerControl nonSaudiMain = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("nonSaudiMain");
                System.Web.UI.HtmlControls.HtmlContainerControl nonSaudiDependent = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("nonSaudiDependent");

                var image = GetImage(saudiMain.InnerHtml.Trim());
                saudiMain.Controls.RemoveAt(0);
                saudiMain.Controls.Add(image);

                var image1 = GetImage(saudiDependent.InnerHtml.Trim());
                saudiDependent.Controls.RemoveAt(0);
                saudiDependent.Controls.Add(image1);

                var image2 = GetImage(nonSaudiMain.InnerHtml.Trim());
                nonSaudiMain.Controls.RemoveAt(0);
                nonSaudiMain.Controls.Add(image2);

                var image3 = GetImage(nonSaudiDependent.InnerHtml.Trim());
                nonSaudiDependent.Controls.RemoveAt(0);
                nonSaudiDependent.Controls.Add(image3);


            }
        }
    }
    private System.Web.UI.HtmlControls.HtmlImage GetImage(string value)
    {
        System.Web.UI.HtmlControls.HtmlImage image = new System.Web.UI.HtmlControls.HtmlImage();
        image.Style.Add("width", "16px");
        image.Style.Add("height", "16px");
        image.Attributes.Add("title", value);


        if (value.ToLower() == "success")
        {
            image.Src = "images/success.png";
        }
        else
        {
            image.Src = "images/failure.png";
        }

        return image;
    }

    private static string ArabicToEnglish(string NameAr, string NameEn)
    {
        string bupa_fullname = "", bupa_arabicname = "";
        bool isArabicInEnglish = false, isEnglishInArabic = false;

        if (!string.IsNullOrWhiteSpace(NameAr))
        {
            if (Regex.IsMatch(NameAr.Trim(), "^[\u0600-\u06FF]+"))
            {
                bupa_arabicname = Regex.Replace(NameAr.Trim(), @"[^ \u0600-\u06FF]+", "");
            }
            else
            {
                bupa_arabicname = Regex.Replace(NameAr.Trim(), @"[^0-9 a-z A-Z]+", "");
                isEnglishInArabic = true;
            }
        }
        if (!string.IsNullOrWhiteSpace(NameEn))
        {
            if (Regex.IsMatch(NameEn.Trim(), "^[\u0600-\u06FF]+"))
            {
                bupa_fullname = Regex.Replace(NameEn.Trim(), @"[^ \u0600-\u06FF]+", "");
                isArabicInEnglish = true;
            }
            else
            {
                bupa_fullname = Regex.Replace(NameEn.Trim(), @"[^0-9 a-z A-Z]+", "");
            }
        }

        if (isArabicInEnglish && isEnglishInArabic)
        {
            string name = bupa_fullname;
            bupa_fullname = bupa_arabicname;
            bupa_arabicname = name;
        }
        else if (isArabicInEnglish && !isEnglishInArabic)
        {
            bupa_arabicname = bupa_fullname;
            bupa_fullname = "";
        }
        else if (isEnglishInArabic && !isArabicInEnglish)
        {
            bupa_fullname = bupa_arabicname;
            bupa_arabicname = "";
        }

        return bupa_fullname;
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }

}

public class CCHISponsorDetails
{
    public string SponsorID { get; set; }
    public int SaudiMain { get; set; }
    public int SaudiDependent { get; set; }
    public int NonSaudiMain { get; set; }
    public int NonSaudiDependent { get; set; }
    public string Status { get; set; }
    public string Remarks { get; set; }
    public string ErrorMessage { get; set; }

}

public class AddEmployeeDependent
{
    public string IDCardNo { get; set; }
    public string SponsorID { get; set; }
    public string MemberType { get; set; }
    public string MainMembershipNo { get; set; }
    public string MainMembershipSponsorID { get; set; }
    public string MainMembershipIDCardNo { get; set; }
}

/*public static class Helper
{

    public static List<T> DataTableToList<T>(this System.Data.DataTable table) where T : class, new()
    {
        try
        {
            List<T> list = new List<T>();
            foreach (var row in table.AsEnumerable())
            {
                T obj = new T();
                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }
                list.Add(obj);
            }
            return list;
        }
        catch
        {
            return null;
        }
    }
}*/

//public class ClassXMLGeneration<T>
//{

//    public void Request(T classType, string req)
//    {
//        System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
//        System.IO.StreamWriter file = new StreamWriter("C:\\CaesarXML\\" + req + ".xml");
//        xmlSrQ.Serialize(file, classType);
//        file.Close();


//    }
//    public void Response(T classType, string resp)
//    {
//        System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
//        System.IO.StreamWriter file = new StreamWriter("C:\\CaesarXML\\" + resp + ".xml");
//        xmlSr.Serialize(file, classType);
//        file.Close();
//    }

//}