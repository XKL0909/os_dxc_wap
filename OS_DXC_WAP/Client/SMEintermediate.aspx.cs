﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Client_SMEintermediate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            BupaGSUserName.Value = Convert.ToString(Session["ClientUsername"]);
            BupaGSUserPassword.Value = Convert.ToString(Session["ClientCredential"]);
            Response.Redirect("https://sme.bupa.com.sa/Login.aspx", false);
        }
        catch (Exception ex)
        {

        }
    }
}