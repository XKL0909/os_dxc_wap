﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" Inherits="Bupa.OSWeb.Client.RequestAmmendment" Codebehind="RequestAmmendment.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ExistingRequest.ascx" TagPrefix="BUPA" TagName="ExistingRequest" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <script type='text/JavaScript' src='js/NumberValidation.js'></script>
    <script type='text/JavaScript'>
        function validateAndConfirm(message) {
            var validated = Page_ClientValidate('1');
            if (validated) {
                return confirm(message);
            }
        }
    </script>
    <link rel="stylesheet" href="<%# Page.ResolveUrl("~/Styles/OSWebStyle.css") %>" type="text/css" charset="utf-8" />
    <%--<asp:ScriptManager runat="server" ID="scriptManager" />--%>

    <BUPA:ExistingRequest runat="server" ID="ExistingRequest" />

</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>
</asp:Content>
