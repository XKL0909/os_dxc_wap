using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using System.Data;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using System.Collections;

public partial class AInvoice : System.Web.UI.Page
{
    private ReportDocument doc = null;
    private DataSet ds = null;
    String InvID = null;

    private Hashtable hasQueryValue;
    string batchId, sundryPremium, fullShort, parametersShown;

    protected void Page_Init(object sender, EventArgs e)
    {
        lnkBackOption.NavigateUrl = "../creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Invoice");

        hasQueryValue = new Hashtable();
        string queryStringValues = string.Empty;

        if (Request.QueryString.Count > 0)
        {
            try
            {
                var val = Request.QueryString["val"];
                queryStringValues = Cryption.Decrypt(val);
                hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                batchId = hasQueryValue.ContainsKey("ID") ? Convert.ToString(hasQueryValue["ID"]) : string.Empty;
                parametersShown = hasQueryValue.ContainsKey("Pr") ? Convert.ToString(hasQueryValue["Pr"]) : string.Empty;
            }
            catch (Exception)
            {
                Response.Write("Invalid rquest!");
                return;
            }

        }
        else
        {
            Response.Write("Invalid rquest!");
            return;
        }


        if (!Page.IsPostBack)
        {
            //code to check if the client has permission to save doc in excel format
            if (  Session["ExportToExcel"] != null &&    Session["ExportToExcel"].ToString() == "True")
                btnExport.Visible = true; // CrystalReportViewer1.HasExportButton = true;


            ///InvID = Request.QueryString["ID"];
            doc = new ReportDocument();
            doc.Load(MapPath("BFAMIVSTM01.rpt"));
          ///  doc.Load(MapPath("BFAMIVSTM01old-wo-hf.rpt"));
            ds = GetData();


            doc.SetDataSource(ds.Tables[0]);
            doc.SetParameterValue("pc_report_id", "BFAMIVSTM01");
            doc.SetParameterValue("PC_RPT_GRP", "no group");
            doc.SetParameterValue("PC_INV_NO", InvID);
            CrystalReportViewer1.ReportSource = doc;
            Session["rptDoc3"] = doc;
            doc.Close();
            doc.Dispose();
        }
        else
        {
            CrystalReportViewer1.ReportSource = Session["rptDoc3"];
        }
    }

    public void BindReport(string ReportName)
    {
        ////InvID = Request.QueryString["ID"];
        doc.Load(MapPath(ReportName));
        ds = GetData();
        doc.SetDataSource(ds.Tables[0]);
        doc.SetParameterValue("pc_report_id", "BFAMIVSTM01");
        doc.SetParameterValue("PC_RPT_GRP", "no group");
        doc.SetParameterValue("PC_INV_NO", InvID);
        CrystalReportViewer1.ReportSource = doc;
        Session["rptDoc3"] = doc; 
        CrystalReportViewer1.ReportSource = doc;
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {

            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
        }
        catch
        {

        }
        ////String Par1 = Request.QueryString["Pr"];
        String Par1 = parametersShown;
        doc = new ReportDocument();
        if ((ViewState["ParametersShown"] != null) && (ViewState["ParametersShown"].ToString() == "True"))
        {
            BindReport("BFAMIVSTM01.rpt");

        }
        else
        {
            BindReport("BFAMIVSTM01.rpt");
            ViewState["BFAMIVSTM01"] = "True";
        }

      
    }
    private DataTable BuildTable()
    {
        DataTable rptDT;
        DataColumn rptCol;
        rptDT = new DataTable();

        rptCol = new DataColumn();
        rptCol.ColumnName = "DOC_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 8;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "INV_GROUP";
        rptCol.DataType = Type.GetType("System.String");
        ////Modified by Sakthi on 02-Feb-2017 For CR 309 Unified Policy number
        ////Start
        ////rptCol.MaxLength = 5;
        rptCol.MaxLength = 6;
        ////End
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "INV_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "DOC_TYPE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 4;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "CONT_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 8;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "BRANCH_CODE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);


        rptCol = new DataColumn();
        rptCol.ColumnName = "BRANCH_DESC";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 70;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "INV_METHOD";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 20;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "GEN_DATE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "CONT_EFF_DATE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "CONT_TERM_DATE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "DUE_DATE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "DESCRIPTION";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 500;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "INV_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "NAME";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 70;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ADDR1";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 40;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ADDR2";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 40;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ADDR3";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 40;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ADDR4";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 40;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "DIST_NAME";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 40;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "FOOTER1";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 100;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "FOOTER2";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 100;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "FOOTER3";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 100;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "FOOTER4";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 100;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "FOOTER5";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 100;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "FOOTER6";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 100;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "FIRST_ISSUE_DATE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 70;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "BATCH_RUN_DT";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "CUST_NAME";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 70;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "AV_INV_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "PLAN_DESC";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 50;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "MBR_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 8;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "MBRSHP_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 8;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "MBR_NAME";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 40;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "DEPT_CODE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 50;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "STAFF_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 15;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "CLS_ID";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 5;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "JOIN_DATE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "CHANGE_DATE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 10;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "AL_PREM_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "TTL_INV_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "FUTURE_INV_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "THIS_INV_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ADJ_CODE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 3;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "CTRY_NAME";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 200;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "SECTION_TYPE";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 4;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "INTRA_BILL_IND";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 20;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "INV_STM_OPTION";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 2;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "TOTAL_ADD_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "TOTAL_DELETE_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "TOTAL_CHANGE_PRODUCT_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "TOTAL_CHANGE_BRANCH_AMT";
        rptCol.DataType = Type.GetType("System.Decimal");
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "MAIN_BRH_IND";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 1;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "VIBAN_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 24;
        rptDT.Columns.Add(rptCol);


        ////Receipt No added by Sakthi on 19-Jan-2017 for CR267 CR329 CR311, CR338 And CR309 Unified Policy number
        ////Start
        rptCol = new DataColumn();
        rptCol.ColumnName = "POLICY_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 9;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "POLICY_INV_NO";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 14;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "PROD_IND";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 1;
        rptDT.Columns.Add(rptCol);

        ////End

        ////Enforce 75% Refund Policy  added by Sakthi on 27-Aug-2017 for CR360
        ////Start
        rptCol = new DataColumn();
        rptCol.ColumnName = "APPLY_POLICY_EXCEED";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 20;
        rptDT.Columns.Add(rptCol);
        ////End

        //// CR 378 VAT changes added by Sakthi on 28-Nov-2017
        //// Start
        rptCol = new DataColumn();
        rptCol.ColumnName = "BUPA_TIN";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 200;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "VAT_RATE";
        rptCol.DataType = Type.GetType("System.Double");        
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "VAT_AMT";
        rptCol.DataType = Type.GetType("System.Double");       
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "TOTAL_VAT_AMT";
        rptCol.DataType = Type.GetType("System.Double");       
        rptDT.Columns.Add(rptCol);
        ////End


        //// CR 392 Arabic description added by Sakthi on 13-Feb-2018
        //// Start
        rptCol = new DataColumn();
        rptCol.ColumnName = "CUST_NAME_ARAB";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "BRANCH_DESC_ARAB";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ARAB_ADDR1";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ARAB_ADDR2";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ARAB_ADDR3";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "ARAB_ADDR4";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "DIST_NAME_ARAB";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "CTRY_NAME_ARAB";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);

        rptCol = new DataColumn();
        rptCol.ColumnName = "DESCRIPTION_ARAB";
        rptCol.DataType = Type.GetType("System.String");
        rptCol.MaxLength = 250;
        rptDT.Columns.Add(rptCol);
        //// End

        return rptDT;
    }


    private DataSet GetData()
    {
        DataSet ds;
        DataTable rptDT;
        DataRow rptRow;
        int i;
       OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
       OS_DXC_WAP.CaesarWS.ReqAmendmentInvoiceRequest_DN datum;
       OS_DXC_WAP.CaesarWS.ReqAmendmentInvoiceResponse_DN response;
       datum = new OS_DXC_WAP.CaesarWS.ReqAmendmentInvoiceRequest_DN();
        ////datum.invoiceNo = InvID;
        datum.invoiceNo = batchId;
        datum.Username = WebPublication.CaesarSvcUsername;
        datum.Password = WebPublication.CaesarSvcPassword;
        datum.transactionID = TransactionManager.TransactionID();
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        response = ws.ReqAmendmentInvoice(datum);
        rptDT = BuildTable();
        for (i = 0; i < response.detail.Length; i++)
        {
            rptRow = rptDT.NewRow();
            rptRow["DOC_NO"] = response.detail[i].documentNo;
            rptRow["INV_GROUP"] = response.detail[i].invoiceGroup;
            rptRow["INV_NO"] = response.detail[i].invoiceNo;
            rptRow["DOC_TYPE"] = response.detail[i].documentType;
            rptRow["CONT_NO"] = response.detail[i].contractNo;
            rptRow["BRANCH_CODE"] = response.detail[i].branchCode;
            rptRow["BRANCH_DESC"] = response.detail[i].branchDescription;
            rptRow["INV_METHOD"] = response.detail[i].invoiceMethod;
            rptRow["GEN_DATE"] = response.detail[i].generationDate;
            rptRow["CONT_EFF_DATE"] = response.detail[i].contractEffectiveDate;
            rptRow["CONT_TERM_DATE"] = response.detail[i].contractTerminationDate;
            rptRow["DUE_DATE"] = response.detail[i].dueDate;
            rptRow["DESCRIPTION"] = response.detail[i].description;
            rptRow["INV_AMT"] = response.detail[i].invoiceAmount;
            rptRow["NAME"] = response.detail[i].name;
            rptRow["ADDR1"] = response.detail[i].addr1;
            rptRow["ADDR2"] = response.detail[i].addr2;
            rptRow["ADDR3"] = response.detail[i].addr3;
            rptRow["ADDR4"] = response.detail[i].addr4;
            rptRow["DIST_NAME"] = response.detail[i].districtName;
            rptRow["FOOTER1"] = response.detail[i].footer1;
            rptRow["FOOTER2"] = response.detail[i].footer2;
            rptRow["FOOTER3"] = response.detail[i].footer3;
            rptRow["FOOTER4"] = response.detail[i].footer4;
            rptRow["FOOTER5"] = response.detail[i].footer5;
            rptRow["FOOTER6"] = response.detail[i].footer6;
            rptRow["FIRST_ISSUE_DATE"] = response.detail[i].firstIssueDate;
            rptRow["BATCH_RUN_DT"] = response.detail[i].batchRunDate;
            rptRow["CUST_NAME"] = response.detail[i].customerName;
            rptRow["AV_INV_NO"] = response.detail[i].invoiceNo;
            rptRow["PLAN_DESC"] = response.detail[i].planDescription;
            rptRow["MBR_NO"] = response.detail[i].memberNo;
            rptRow["MBRSHP_NO"] = response.detail[i].membershipNo;
            rptRow["MBR_NAME"] = response.detail[i].memberName;
            rptRow["DEPT_CODE"] = response.detail[i].departmentCode;
            rptRow["STAFF_NO"] = response.detail[i].staffNo;
            rptRow["CLS_ID"] = response.detail[i].classID;
            rptRow["JOIN_DATE"] = response.detail[i].joinDate;
            rptRow["CHANGE_DATE"] = response.detail[i].changeDate;
            rptRow["AL_PREM_AMT"] = response.detail[i].allPremiumAmount;
            rptRow["TTL_INV_AMT"] = response.detail[i].totalnvoiceAmount;
            rptRow["FUTURE_INV_AMT"] = response.detail[i].futureInvoiceAmount;
            rptRow["THIS_INV_AMT"] = response.detail[i].thisInvoiceAmount;
            rptRow["ADJ_CODE"] = response.detail[i].adjustCode;
            rptRow["CTRY_NAME"] = response.detail[i].countryName;
            rptRow["SECTION_TYPE"] = response.detail[i].sectionType;
            rptRow["INTRA_BILL_IND"] = response.detail[i].intraBillindicator;
            rptRow["INV_STM_OPTION"] = response.detail[i].invoiceStatementOption;
            rptRow["TOTAL_ADD_AMT"] = response.detail[i].totalAddAmount;
            rptRow["TOTAL_DELETE_AMT"] = response.detail[i].totalDeleteAmount;
            rptRow["TOTAL_CHANGE_PRODUCT_AMT"] = response.detail[i].totalChangeProductAmount;
            rptRow["TOTAL_CHANGE_BRANCH_AMT"] = response.detail[i].totalChangeBranchAmount;
            rptRow["MAIN_BRH_IND"] = response.detail[i].mainBranchIndicator;
            rptRow["VIBAN_NO"] = response.detail[i].Viban;
			////Receipt No added by Sakthi on 19-Jan-2017 for CR267 CR329 CR311, CR338 And CR309 Unified Policy number
            ////Start
            rptRow["POLICY_NO"] = response.detail[i].policyNo;
            if (!string.IsNullOrEmpty(Convert.ToString(response.detail[i].policyInvNo)))
            {
                rptRow["POLICY_INV_NO"] = response.detail[i].policyInvNo;
            }
            rptRow["PROD_IND"] = "Y";
            ////End

            ////Enforce 75% Refund Policy  added by Sakthi on 27-Aug-2017 for CR360
            ////Start
            rptRow["APPLY_POLICY_EXCEED"] = response.detail[i].applyPolicyExceed;
            ////End

            //// CR 378 VAT changes added by Sakthi on 28-Nov-2017
            //// Start
            rptRow["BUPA_TIN"] = response.detail[i].bupaTin;
            rptRow["VAT_RATE"] = response.detail[i].vatRate;
            rptRow["VAT_AMT"] = response.detail[i].vatAmount;
            rptRow["TOTAL_VAT_AMT"] = response.detail[i].totalVATAmount;
            ////End

            //// CR 392 Arabic description added by Sakthi on 13-Feb-2018
            //// Start
            rptRow["CUST_NAME_ARAB"] = response.detail[i].customerNameArab;
            rptRow["BRANCH_DESC_ARAB"] = response.detail[i].branchDescriptionArab;
            rptRow["ARAB_ADDR1"] = response.detail[i].arabAddr1;
            rptRow["ARAB_ADDR2"] = response.detail[i].arabAddr2;
            rptRow["ARAB_ADDR3"] = response.detail[i].arabAddr3;
            rptRow["ARAB_ADDR4"] = response.detail[i].arabAddr4;
            rptRow["DIST_NAME_ARAB"] = response.detail[i].districtNameArab;
            rptRow["CTRY_NAME_ARAB"] = response.detail[i].countryNameArab;
            rptRow["DESCRIPTION_ARAB"] = response.detail[i].descriptionArab;
            //// End

            rptDT.Rows.Add(rptRow);
        }
        ds = new DataSet();
        ds.Tables.Add(rptDT);
        return ds;
    }



    protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
    {
        doc.Close();
        doc.Dispose();

    }


    public void btnExport_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        try
        {
            ExportOptions exportOptions;

            DiskFileDestinationOptions diskFileDestinationOptions = new DiskFileDestinationOptions();
            ExcelFormatOptions formatTypeOptions = new ExcelFormatOptions();

            //// BindReport("BFAMIVSTM01newX.rpt");
            BindReport("BFAMIVSTM01.rpt");
            

            //destination folder...
            diskFileDestinationOptions.DiskFileName = Server.MapPath("\\Client\\newrpt\\temp_invoice\\") + Session["ClientUsername"].ToString() + "-Invoice.xls";
            exportOptions = doc.ExportOptions;
            exportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            exportOptions.ExportFormatType = ExportFormatType.Excel;
            exportOptions.DestinationOptions = diskFileDestinationOptions;
            exportOptions.FormatOptions = formatTypeOptions;
            doc.Export();
            Response.Redirect("temp_invoice/" + Session["ClientUsername"].ToString() + "-Invoice.xls" , false);
            //MessageBox.Show("Successfully Exported...");
        }
        catch (Exception ex)
        {
            // MessageBox.Show(ex.ToString());
        }
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }



}
