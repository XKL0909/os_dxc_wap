using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Collections;

public partial class IInvoice : System.Web.UI.Page
{
    private ReportDocument doc = null;
    private DataSet ds = null;
    String InvID = null;
    string strClientID;
    private Hashtable hasQueryValue;

    string batchId, sundryPremium, fullShort, parametersShown;


    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }
        lnkBackOption.NavigateUrl = "../creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Invoice");
        hasQueryValue = new Hashtable();
        string queryStringValues = string.Empty;

        if (Request.QueryString.Count > 0)
        {
            try
            {
                var val = Request.QueryString["val"];
                queryStringValues = Cryption.Decrypt(val);
                hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                batchId = hasQueryValue.ContainsKey("ID") ? Convert.ToString(hasQueryValue["ID"]) : string.Empty;
                parametersShown = hasQueryValue.ContainsKey("Pr") ? Convert.ToString(hasQueryValue["Pr"]) : string.Empty;
            }
            catch (Exception)
            {
                Response.Write("Invalid rquest!");
                return;
            }

        }
        else
        {
            Response.Write("Invalid rquest!");
            return;
        }

        if (!Page.IsPostBack)
        {
            //code to check if the client has permission to save doc in excel format
            if (Session["ExportToExcel"] != null && Session["ExportToExcel"].ToString() == "True")
                btnExport.Visible = true; // CrystalReportViewer1.HasExportButton = true;

            ////InvID = Request.QueryString["ID"];
            doc = new ReportDocument();
            doc.Load(MapPath("BFISIVSTM01.rpt"));
            ds = GetData();


            doc.SetDataSource(ds.Tables[0]);
           doc.SetParameterValue("pc_report_id", "BFISIVSTM01");
            doc.SetParameterValue("pv_company_name", "BUPA");
            doc.SetParameterValue("pc_report_group", "AL");
            doc.SetParameterValue("pv_title1", "Still there?");
            doc.SetParameterValue("pd_report_date", "03/02/2010");
            doc.SetParameterValue("PC_RPT_GRP", "no group");
            doc.SetParameterValue("PC_INV_NO", "00028618");
            CrystalReportViewer1.ReportSource = doc;
            Session["rptDoc2"] = doc;
        }
        else
            CrystalReportViewer1.ReportSource = Session["rptDoc2"];

    }

    public void BindReport(string ReportName)
    {

        //if (!Page.IsPostBack)
        // {
        ////InvID = Request.QueryString["ID"];
        InvID = batchId;
        //doc = new ReportDocument();

       

        doc.Load(MapPath(ReportName));
        ds = GetData();


        doc.SetDataSource(ds.Tables[0]);
        doc.SetParameterValue("pc_report_id", "BFISIVSTM01");
        doc.SetParameterValue("pv_company_name", "BUPA");
        doc.SetParameterValue("pc_report_group", "AL");
        doc.SetParameterValue("pv_title1", "Still there?");
        doc.SetParameterValue("pd_report_date", "03/02/2010");
        doc.SetParameterValue("PC_RPT_GRP", "no group");
        doc.SetParameterValue("PC_INV_NO", "00028618"); CrystalReportViewer1.ReportSource = doc;
        Session["rptDoc2"] = doc; //CrystalReportViewer1.ReportSource = crystalReport;
        //  }
        //  else
        CrystalReportViewer1.ReportSource = doc; // Session["rptDoc3"];


    }


    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            
                //Session Timeout Warning Dialog
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
        }
        catch
        {

        }

        strClientID = Session["ClientID"].ToString();///"10490900"; //
        ////String Par1 = Request.QueryString["Pr"];
        String Par1 = parametersShown;
        doc = new ReportDocument();
        if ((ViewState["ParametersShown"] != null) && (ViewState["ParametersShown"].ToString() == "True"))
        {
            BindReport("BFISIVSTM01.rpt");
        }
        else
        {
            BindReport("BFISIVSTM01.rpt");
            ViewState["ParametersShown"] = "True";
        }
        //String InvID = Request.QueryString["ID"];
        //doc = new ReportDocument();
        //if ((ViewState["ParametersShown"] != null) && (ViewState["ParametersShown"].ToString() == "True"))
        //{

        //    BindReport(Par1);

        //}
        //else
        //{
        //    BindReport(Par1);
        //    ViewState["ParametersShown"] = "True";
        //}

    }


    private DataTable BuildTable()
    {
        DataTable dt;
        DataColumn col;
        dt = new DataTable();
        col = new DataColumn();
        col.ColumnName = "INV_TYPE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 3;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "DOC_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "VIBAN_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 24;
        dt.Columns.Add(col);

        

        
        col = new DataColumn();
        col.ColumnName = "INV_NO";
        col.MaxLength = 10;
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "DOC_TYPE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 4;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "CONT_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "BRANCH_CODE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "BRANCH_DESC";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 70;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "INV_METHOD";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 20;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "GEN_DATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "CONT_EFF_DATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "CONT_TERM_DATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "DUE_DATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "DESCRIPTION";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 500;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "IS_INV_AMT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 70;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "ADDR1";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "ADDR2";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "ADDR3";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "ADDR4";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "DIST_NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "FOOTER1";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 100;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "FOOTER2";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 100;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "FOOTER3";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 100;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "FOOTER4";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 100;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "FOOTER5";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 100;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "FOOTER6";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 100;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "FIRST_ISSUE_DATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "BATCH_RUN_DT";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "CUST_NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 70;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "INV_NO1";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "PLAN_DESC";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 50;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "MBR_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "MBRSHP_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "MBR_NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 70;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "DEPT_CODE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 50;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "STAFF_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "IV_INV_AMT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "CLS_ID";
        col.DataType = Type.GetType("System.Int32");
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "INV_METHOD1";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 20;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "CTRY_NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "PS_DUEDATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "PS_GENDATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "PS_INVAMT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "PS_INSTALLNO";
        col.DataType = Type.GetType("System.Int32");
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "PS_INVSTMOPT";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 2;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "SUBSEQUENT";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "MAIN_BRH_IND";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 1;
        dt.Columns.Add(col);
        //REF-0788 CR100 Shaman Tso Start
        col = new DataColumn();
        col.ColumnName = "BILL_PCT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);
        //REF-0788 CR100 Shaman Tso End

        ////Receipt No added by Sakthi on 19-Jan-2017 for CR267 CR329 CR311, CR338 And CR309 Unified Policy number
        ////Start
        col = new DataColumn();
        col.ColumnName = "POLICY_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 9;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "POLICY_INV_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 14;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "PROD_IND";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 1;
        dt.Columns.Add(col);

        ////End

        //// CR 378 VAT changes added by Sakthi on 28-Nov-2017
        //// Start
        col = new DataColumn();
        col.ColumnName = "BUPA_TIN";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "VAT_RATE";
        col.DataType = Type.GetType("System.Double");       
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "IS_VAT_AMT";
        col.DataType = Type.GetType("System.Double");       
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "IV_VAT_AMT";
        col.DataType = Type.GetType("System.Double");        
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "PS_VAT_AMT";
        col.DataType = Type.GetType("System.Double");        
        dt.Columns.Add(col);
        ////End

        //// CR 392 Arabic description added by Sakthi on 13-Feb-2018
        //// Start
        col = new DataColumn();
        col.ColumnName = "CUST_NAME_ARAB";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "BRANCH_DESC_ARAB";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ARAB_ADDR1";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ARAB_ADDR2";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ARAB_ADDR3";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ARAB_ADDR4";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "DIST_NAME_ARAB";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CTRY_NAME_ARAB";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "DESCRIPTION_ARAB";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 250;
        dt.Columns.Add(col);
        //// End

        return dt;
    }



    private DataSet GetData()
    {
        DataSet ds;
        DataTable dt;
        DataRow row;
        int i;
       OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
       OS_DXC_WAP.CaesarWS.ReqInstallInvoiceRequest_DN datum;
       OS_DXC_WAP.CaesarWS.ReqInstallInvoiceResponse_DN result;
       datum = new OS_DXC_WAP.CaesarWS.ReqInstallInvoiceRequest_DN();
        ////datum.invoiceNo =InvID; ///"00101290";//
        datum.invoiceNo = batchId; ///"00101290";//
       //datum.contractNo = strClientID;
        datum.Username = WebPublication.CaesarSvcUsername;
        datum.Password = WebPublication.CaesarSvcPassword;
        datum.transactionID = TransactionManager.TransactionID();
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        result = ws.ReqInstallmentInvoice(datum);

        dt = BuildTable();
        for (i = 0; i < result.detail.Length; i++)
        {
            row = dt.NewRow();

            row["VIBAN_NO"] = result.detail[i].Viban;
            row["INV_TYPE"] = result.detail[i].invoiceType;
            row["DOC_NO"] = result.detail[i].documentNo;
            row["INV_NO"] = result.detail[i].invoiceNo;
            row["DOC_TYPE"] = result.detail[i].documentType;
            row["CONT_NO"] = result.detail[i].contractNo;
            row["BRANCH_CODE"] = result.detail[i].branchCode;
            row["BRANCH_DESC"] = result.detail[i].branchDescription;
            row["INV_METHOD"] = result.detail[i].invoiceMethod;
            row["GEN_DATE"] = result.detail[i].generationDate;
            row["CONT_EFF_DATE"] = result.detail[i].contractEffectiveDate;
            row["CONT_TERM_DATE"] = result.detail[i].contractTerminationDate;
            row["DUE_DATE"] = result.detail[i].dueDate;
            row["DESCRIPTION"] = result.detail[i].description;
            row["IS_INV_AMT"] = result.detail[i].invoiceAmountIS;
            row["NAME"] = result.detail[i].name;
            row["ADDR1"] = result.detail[i].addr1;
            row["ADDR2"] = result.detail[i].addr2;
            row["ADDR3"] = result.detail[i].addr3;
            row["ADDR4"] = result.detail[i].addr4;
            row["DIST_NAME"] = result.detail[i].districtName;
            row["FOOTER1"] = result.detail[i].footer1;
            row["FOOTER2"] = result.detail[i].footer2;
            row["FOOTER3"] = result.detail[i].footer3;
            row["FOOTER4"] = result.detail[i].footer4;
            row["FOOTER5"] = result.detail[i].footer5;
            row["FOOTER6"] = result.detail[i].footer6;
            row["FIRST_ISSUE_DATE"] = result.detail[i].firstIssueDate;
            row["BATCH_RUN_DT"] = result.detail[i].batchRunDate;
            row["CUST_NAME"] = result.detail[i].customerName;
            row["INV_NO1"] = result.detail[i].invoiceNumber1;
            row["PLAN_DESC"] = result.detail[i].planDescription;
            row["MBR_NO"] = result.detail[i].memberNo;
            row["MBRSHP_NO"] = result.detail[i].membershipNo;
            row["MBR_NAME"] = result.detail[i].memberName;
            row["DEPT_CODE"] = result.detail[i].departmentCode;
            row["STAFF_NO"] = result.detail[i].staffNo;
            row["IV_INV_AMT"] = result.detail[i].invoiceAmountIV;
            if (result.detail[i].classID.Length == 0)
                row["CLS_ID"] = DBNull.Value;
            else
                row["CLS_ID"] = Int32.Parse(result.detail[i].classID);
            row["INV_METHOD1"] = result.detail[i].invoiceMethod1;
            row["CTRY_NAME"] = result.detail[i].countryName;
            row["PS_DUEDATE"] = result.detail[i].paymentDueDate;
            row["PS_GENDATE"] = result.detail[i].paymentGenDate;
            row["PS_INVAMT"] = result.detail[i].paymentAmount;
            if (result.detail[i].paymentStatementNo.Length == 0)
                row["PS_INSTALLNO"] = DBNull.Value;
            else
            row["PS_INSTALLNO"] = Int32.Parse(result.detail[i].paymentStatementNo);
            row["PS_INVSTMOPT"] = result.detail[i].paymentStatementOption;
            row["SUBSEQUENT"] = result.detail[i].subseqent;
            row["MAIN_BRH_IND"] = result.detail[i].mainBranchIndicator;
            row["BILL_PCT"] = result.detail[i].billedPct; //REF-0788 CR100 Shaman Tso

            ////Receipt No added by Sakthi on 19-Jan-2017 for CR267 CR329 CR311, CR338 And CR309 Unified Policy number
            ////Start
            row["POLICY_NO"] = result.detail[i].policyNo;
            if (!string.IsNullOrEmpty(Convert.ToString(result.detail[i].policyInvNo)))
            {
                row["POLICY_INV_NO"] = result.detail[i].policyInvNo;
            }
            row["PROD_IND"] = "Y";
            ////End

            //// CR 378 VAT changes added by Sakthi on 28-Nov-2017
            //// Start
            row["BUPA_TIN"] = result.detail[i].bupaTin;
            row["VAT_RATE"] = result.detail[i].vatRate;
            row["IS_VAT_AMT"] = result.detail[i].vatAmountIS;
            row["IV_VAT_AMT"] = result.detail[i].vatAmountIV;
            row["PS_VAT_AMT"] = result.detail[i].paymentVATAmount;
            ////End

            //// CR 392 Arabic description added by Sakthi on 13-Feb-2018
            //// Start
            row["CUST_NAME_ARAB"] = result.detail[i].customerNameArab;
            row["BRANCH_DESC_ARAB"] = result.detail[i].branchDescriptionArab;
            row["ARAB_ADDR1"] = result.detail[i].arabAddr1;
            row["ARAB_ADDR2"] = result.detail[i].arabAddr2;
            row["ARAB_ADDR3"] = result.detail[i].arabAddr3;
            row["ARAB_ADDR4"] = result.detail[i].arabAddr4;
            row["DIST_NAME_ARAB"] = result.detail[i].districtNameArab;
            row["CTRY_NAME_ARAB"] = result.detail[i].countryNameArab;
            row["DESCRIPTION_ARAB"] = result.detail[i].descriptionArab;
            //// End


            dt.Rows.Add(row);
        }
        ds = new DataSet();
        ds.Tables.Add(dt);
        return ds;
    }


    public void btnExport_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public void ExportToExcel()
    {
        try
        {
            ExportOptions exportOptions;

            DiskFileDestinationOptions diskFileDestinationOptions = new DiskFileDestinationOptions();
            ExcelFormatOptions formatTypeOptions = new ExcelFormatOptions();

            ////BindReport("BFISIVSTM01X.rpt");
            BindReport("BFISIVSTM01.rpt");
            

            //destination folder...
            diskFileDestinationOptions.DiskFileName = Server.MapPath("\\Client\\newrpt\\temp_invoice\\") + Session["ClientUsername"].ToString() + "-Invoice.xls";
            exportOptions = doc.ExportOptions;
            exportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            exportOptions.ExportFormatType = ExportFormatType.Excel;
            exportOptions.DestinationOptions = diskFileDestinationOptions;
            exportOptions.FormatOptions = formatTypeOptions;
            doc.Export();
            Response.Redirect("temp_invoice/" + Session["ClientUsername"].ToString() + "-Invoice.xls", false);
            //MessageBox.Show("Successfully Exported...");
        }
        catch (Exception ex)
        {
            // MessageBox.Show(ex.ToString());
        }
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }

}
