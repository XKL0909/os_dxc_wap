﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="CCHIValidatorNew" Codebehind="CCHIValidatorNew.aspx.cs" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CCHI Validation</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
     <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>

    <style>
        .FloatingDiv {
            position: fixed;
            top: 0;
            left: 0;
            text-align: center;
            padding: 200px 0 0 0;
            z-index: 999;
            width: 100%;
            height: 100%;
            background: url('../Images/Floatingimg.png') repeat top left;
        }
    </style>
    <script src="js/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            var dfd = $("#HDNsucces").val();
            if ($("#HDNsucces").val() == "false")
                $(".alert.alert-success").css("visibility", "hidden");
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container">
            <asp:HiddenField ID="HDNsucces" runat="server" />
            <div class="row">
                <asp:UpdatePanel ID="CCHIUpdatePanel" runat="server">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSubmit" />
                        <asp:PostBackTrigger ControlID="rptSponsors" />
                    </Triggers>
                    <ContentTemplate>
                        <asp:UpdateProgress ID="uprgWorkRequest" runat="server">
                            <ProgressTemplate>
                                <div class="FloatingDiv">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="../Images/ajax-loader.gif"
                                        Style="height: 279px" /><br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <div class="row text-left">
                            <a href="AddMemberIntermediate.aspx">«Back</a>
                        </div>
                        <div class="row ">
                            <div style='display: <%=lblMessge%>;' class="alert alert-danger alert-dismissable bb">

                                <strong>Error!</strong>&nbsp; <%=lblMessge%>
                            </div>
                        </div>
                        <div class="row">
                            <asp:Repeater ID="rptSponsors" runat="server" OnItemDataBound="OnItemDataBound">
                                <ItemTemplate>
                                    <br />
                                    <div class='alert alert-<%# Eval("ErrorDetails.ServiceResult").ToString()=="ValidationFailure" ?"danger":"success"%>'>
                                        Validation <%# FormatMessge(Eval("ErrorDetails.ServiceResult").ToString()) %>.
                                      <br />
                                        <%# Eval("ErrorDetails.ServiceResult").ToString()=="ValidationFailure"? "CCHI Error: "+ Convert.ToString(Eval("ErrorDetails.Description")):"" %>
                                    </div>
                                    <asp:Repeater ID="rptSponsor" runat="server" OnItemDataBound="rptSponsor_OnItemDataBound">
                                        <HeaderTemplate>
                                            <table class="table table-striped table-bordered dt-responsive nowrap bb">
                                                <thead>
                                                    <tr>
                                                        <th>Sponsor Id</th>
                                                        <th>Type</th>
                                                        <th>Saudi Main</th>
                                                        <th>Saudi Dependent</th>
                                                        <th>Non-Saudi Main</th>
                                                        <th>Non-Saudi Dependent</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("SponsorId") %></td>
                                                <td>
                                                    <%# Eval("CapType") %>
                                                </td>
                                                <td>
                                                    <div id="saudiMain" style="text-align: center" runat="server">
                                                        <%# Eval("SaudiMain") %>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="saudiDependent" style="text-align: center" runat="server">
                                                        <%# Eval("SaudiDependent") %>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="nonSaudiMain" style="text-align: center" runat="server">
                                                        <%# Eval("NonSaudiMain") %>
                                                    </div>
                                                    <td>
                                                        <div id="nonSaudiDependent" style="text-align: center" runat="server">
                                                            <%# Eval("NonSaudiDependent") %>
                                                        </div>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>

                        <div class="row submit">
                            <asp:Button runat="server" CssClass="btn btn-primary btn-override bb" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click" />
                            <asp:Button runat="server" CssClass="btn btn-primary btn-override bb" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">

                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>

                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>

                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                    <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                        Do you want to extend?</strong>
                                </td>
                                <td style="text-align: right">
                                    <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                        هل تريد تمديد الوقت؟ &nbsp;</strong>
                                </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                        <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                        <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                    </div>
                    </asp:Panel>

                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

    </form>
</body>
</html>
