﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Details.aspx.cs" Inherits="Details" %>--%>
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Templates/Inner.Master" Inherits="Details" Codebehind="Details.aspx.cs" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <%--<form name="form1" id="frmMain" runat="server">--%>
    <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="css/unifiedpolicy.css" rel="stylesheet" />
    <link href="css/unifiedpolicy.css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
    <script src="js/loadingoverlay.min.js"></script>
    <link rel="stylesheet" href="css/CALCSS/ui.theme.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/CALCSS/jquery-ui.css" type="text/css" media="all" />
    <script src="js/CAlJS/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/prototype.js"></script>
    <script src="js/jquery.knob.js"></script>
    <script src="js/bootstrap.js"></script>
    <!-- jQuery File Upload Dependencies -->
    <script src="js/jquery.ui.widget.js"></script>
    <script src="js/jquery.iframe-transport.js"></script>
    <script src="js/jquery.fileupload.js"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script type="text/javascript">
         function DisplayVideoOption() {
             document.getElementById('<%= divVideoPopup.ClientID %>').style.visibility = "visible";
        }
        function CloseVideoOption(val) 
        {
            document.getElementById('<%= divVideoPopup.ClientID %>').style.visibility = "hidden";
            if (val == 2)
                window.open("../Client/PlayVideo.aspx", '_blank', 'location=yes,height=550,width=900,scrollbars=yes,status=yes');
        }
    </script>
    <style type="text/css">
        .btnVideo {
            border-radius: 3px 9px 1px;
            background: #0886D6;
            padding: 2px 0px;
            width: 80px;
            height: 45px;
        }
    </style>
    <div class="container bb">
        <div class="row clearfix bb">
            <div class="col-md-12 bb">
                 <div class="divleftClassHelp">
                    <%--<a href="CCHICap.aspx" runat="server" title="CCHI Cap الحد الأدنى للإضافات">CCHI Cap&nbsp; الحد الأدنى للإضافات</a>&nbsp;	&nbsp;|&nbsp;
                    <a href="../Docs/CCHI-unified-manual-Web-Addition.pdf" target="_newtab" runat="server" title="New Addition Guide">New Addition Guide</a>&nbsp;	&nbsp;|&nbsp;
                    <a href="#" onclick="DisplayVideoOption()" runat="server" title="New Addition Video">New Addition Video</a>&nbsp;	&nbsp;|&nbsp;
                    <a href="CCHICap.aspx?OptionType=AddEmployeeDependent" title="Batch upload employee & dependent">Batch upload employee & dependent</a>&nbsp;	&nbsp;|&nbsp;
                    <a href="CCHICap.aspx?OptionType=AddDependent" title="Batch upload dependent">Batch upload dependent</a>--%>

                    <a href="CCHICap.aspx" runat="server" title="CCHI Cap الحد الأدنى للإضافات">CCHI Cap&nbsp; الحد الأدنى للإضافات</a>&nbsp;	&nbsp;|&nbsp;
                    <a href="../Docs/CCHI-unified-manual-Web-Addition.pdf" target="_newtab" runat="server" title="New Addition Guide">New Addition Guide</a>&nbsp;	&nbsp;|&nbsp;
                    <a href="#" onclick="DisplayVideoOption()" runat="server" title="New Addition Video">New Addition Video</a>&nbsp;	&nbsp;|&nbsp;
                    <a id="lnkUpdED" runat="server"  title="Batch upload employee & dependent">Batch upload employee & dependent</a>&nbsp;	&nbsp;|&nbsp;
                    <a id="lnkUpdD" runat="server" title="Batch upload dependent">Batch upload dependent</a>

                </div>
                <br /><br /><br /><br />
                <div id="HelpSupport" class="divleftClassHelp">
                    <span class="ClassHelp">Call Helpline to get support 9 2002 3009</span></br>
                        <span class="ClassArHelp">9 2002 3009 : إتصل بخط  المساعدة  لتلقي  الدعم</span>
                </div>
                <div class="btn-group pull-right bb">
                    <button type="button" id="btnAddEmployee" class="btnTOAdd btn-primary bb">Add Employee&nbsp;	&nbsp;إضافة موظف</button>
                    <button type="button" id="btnAddDependent" class="btnTOAdd btn-primary bb">Add Dependent&nbsp;	&nbsp;إضافة تابع</button>
                </div>
            </div>
        </div>
    </div>
        
    <div id="divVideoPopup" runat="server" style="top: 0; left: 0;  right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999999999999; visibility:hidden  ">
            <div style="left: 35%; top:35%; position: fixed; width:400px;">
                <table style="width: 100%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                    <tr style="background-color: #0886D6; line-height: 30px">
                        <td colspan="3" style="color: #fff; text-align: left">
                            <strong style="padding-left:5px; font-size:larger">New add employee/dependent user guide video</strong>
                            <span style="float: right; "> 
                                <a onclick="CloseVideoOption('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size:larger">X</a>
                            </span>
                        </td>
                    </tr>
                    <tr style="background-color: #fff"" >
                        <td style="text-align:center; padding: 30px 0px 30px 0px;">
                           <input type="button" id="btnPlay" runat="server"  class="btnVideo" style="color:#fff !important;" onclick="CloseVideoOption('2')" value="Play"  />
                        </td>
                        <td style="text-align:center; padding: 30px 0px 30px 0px;">
                            <input type="button" id="btnSkip" runat="server"  class="btnVideo" style="color:#fff !important;" onclick="CloseVideoOption('1')" value="Skip"  />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <div class="container bb">

        <div class="stepwizard margin-top-15">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#step-1" id="step1" type="button" class="btn btn-primary btn-circle bb">1</a>
                    <p>Step 1: Add</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-2" id="step2" type="button" class="btn btn-default btn-circle bb">2</a>
                    <p>Step 2: Verify</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" id="step3" type="button" class="btn btn-default btn-circle bb">3</a>
                    <p>Step 3: Attachment</p>
                </div>
            </div>
        </div>



        <asp:HiddenField runat="server" ID="hdnIdType" />
        <asp:HiddenField runat="server" ID="btnGroupType" />
        <asp:HiddenField runat="server" ID="hdnRequestId" />
        <asp:HiddenField runat="server" ID="hdnReqDetailId" />
        <asp:HiddenField runat="server" ID="hdnNewBorn" />

        <div class="row text-left">
        </div>
        <br />

        <div class="row setup-content" id="step-1">
            <div id="divStep1">

                <div class="alert alert-info alert-dismissable">
                    <b>Please select the menu to add employee/dependent/family
                            <img width="16" id="step1LoadingImage" height="16" src="images/loading.gif" /></b>
                </div>

            </div>
        </div>

        <div class="row setup-content" id="step-2">
            <div id="divStep2">
                <div class="alert alert-info alert-dismissable">
                    <b>Loading yakeen info, please be patient......
                            <img width="16" height="16" src="images/loading.gif" /></b>
                </div>
            </div>
        </div>

        <div class="row setup-content" id="step-3">
            <div id="divStep3">
                <div class="alert alert-info alert-dismissable">
                    <b>Loading screen......
                            <img width="16" height="16" src="images/loading.gif" /></b>
                </div>
            </div>
        </div>

        <div id="divRequests"></div>

    </div>
     <script src="js/unifiedpolicy-stepform.js"></script>
         <br />
        <uc2:OSNav ID="OSNav1" runat="server" />
        <br />
        </asp:Content>

<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        <%--<asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>--%>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>
	<script>
function setReqDetail(value) {
    $("#hdnReqDetailId").val(value);
}
</script>
</asp:Content>




