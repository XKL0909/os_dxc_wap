﻿using Bupa.Core.Utilities.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI.HtmlChart;
using OnlinseServicesDashBoard;
using Telerik.Web.UI;
using Telerik.Web.UI.HtmlChart;


public partial class PreAuthChartDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientID"])))
            Response.Redirect("~/default.aspx");

        PreAuthDetailsChart.DataSource = GetPreAuthGroupedData(GetPreAuthData());
        PreAuthDetailsChart.DataBind();

        //PreAuthDetailsChart.PlotArea.XAxis.

        PreAuthDetailsChart.PlotArea.Series[0].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Green;
        PreAuthDetailsChart.PlotArea.Series[2].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Red;
        PreAuthDetailsChart.PlotArea.Series[1].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.DarkSeaGreen;
        PreAuthDetailsChart.PlotArea.Series[3].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Purple;
        PreAuthDetailsChart.PlotArea.Series[4].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Navy;
    }


    private DataSet GetPreAuthData()
    {
        var caesarConnectionString = CoreConfiguration.Instance.GetConfigValue("Caesar Connection String");
        var contractNumber = Convert.ToString(Session["ContractNumbers"]);// Convert.ToString(Session["ClientID"]); //CoreConfiguration.Instance.GetConfigValue("FilterByContract");
        //            var contractNumber = CoreConfiguration.Instance.GetConfigValue("FilterByContract");

        DataSet ds = DataHelper.GetPreAuthData(caesarConnectionString, DateTime.Now.AddDays(-10), DateTime.Now, contractNumber);
        //DataTable dt = ds.Tables[0];

        PreAuthDetailsGrid.DataSource = ds;
        PreAuthDetailsGrid.DataBind();



        return ds;
    }

    private DataSet GetPreAuthGroupedData(DataSet dsSource)
    {
        //Dataset for chart
        DataSet ds = new DataSet("PreAuthsDataSet");
        DataTable dt = new DataTable("PreAuthsDataTable");
        //dt.Columns.Add("PREAUTH_ID", Type.GetType("System.Int32"));
        dt.Columns.Add("RECEIVED_DATE", Type.GetType("System.DateTime"));
        dt.Columns.Add("ApprovedPreAuths", Type.GetType("System.String"));
        dt.Columns.Add("RejectedPreAuths", Type.GetType("System.String"));
        dt.Columns.Add("PartApprovedPreAuths", Type.GetType("System.String"));
        dt.Columns.Add("NotRequiredPreAuths", Type.GetType("System.String"));
        dt.Columns.Add("CancelledPreAuths", Type.GetType("System.String"));
        //dt.Columns.Add("EST_AMT", Type.GetType("System.Decimal"));

        int count = 1;

        var query = from dr in dsSource.Tables[0].AsEnumerable()
                    group dr by dr["RECEIVED_DATE"] into newdr
                    select new
                    {
                        RECEIVED_DATE = newdr.First().Field<string>("RECEIVED_DATE"),
                        //PREAUTH_COUNT = newdr.Count(),
                        ApprovedPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("Approved")).Count(),
                        RejectedPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("Rejected")).Count(),
                        PartApprovedPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("Part Approved")).Count(),
                        NotRequiredPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("No Preauthorization Required")).Count(),
                        CancelledPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("Cancelled")).Count(),
                    };

        foreach (var dtRow in /*dsSource.Tables[0].Rows*/ query)
        {
            //count = count + 1;
            dt.Rows.Add(dtRow.RECEIVED_DATE, dtRow.ApprovedPreAuths.ToString(), dtRow.RejectedPreAuths.ToString(), dtRow.PartApprovedPreAuths.ToString(), dtRow.NotRequiredPreAuths.ToString(), dtRow.CancelledPreAuths.ToString());
        }

        ds.Tables.Add(dt);
        return ds;
    }
    protected void PreAuthDetailsGrid_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
    {

	e.Column.FilterControlWidth = Unit.Pixel(50);
        e.Column.CurrentFilterFunction = GridKnownFunction.Contains;

        if (e.Column.UniqueName == "REGION")
        {
            e.Column.HeaderText = "Region";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "CITY")
        {
            e.Column.HeaderText = "City";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "SCHEME_NAME")
        {
            e.Column.HeaderText = "Scheme";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "MBRSHP_NO")
        {
            e.Column.HeaderText = "Membership #";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "MBR_NAME")
        {
            e.Column.HeaderText = "Member Name";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "MBR_MOBILE_NO")
        {
            e.Column.HeaderText = "Member Mobile #";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "VIP_CODE")
        {
            e.Column.HeaderText = "VIP Code";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "SERV_TYPE")
        {
            e.Column.HeaderText = "Service Type";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "CLAIM_TYPE")
        {
            e.Column.HeaderText = "Claim Type";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "BEN_TYPE")
        {
            e.Column.HeaderText = "Ben Type";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "LINE_ICD_DESC")
        {
            e.Column.HeaderText = "ICD Description";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "EST_AMT")
        {
            e.Column.HeaderText = "Eastimated amount";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "PREAUTH_STATUS")
        {
            e.Column.HeaderText = "Pre Auth Status";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "RECEIVED_DATE")
        {
            e.Column.HeaderText = "Recieved Date";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "RESPONSE_DATE")
        {
            e.Column.HeaderText = "Response Date";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "REJ_REASON")
        {
            e.Column.HeaderText = "Rejection reason";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "PROV_NAME")
        {
            e.Column.HeaderText = "Provider Name";
            e.Column.AutoPostBackOnFilter = true;
        }
    }

    protected void btnExcel_Click(object sender, System.EventArgs e)
    {
        PreAuthDetailsGrid.ExportSettings.ExportOnlyData = true;
        PreAuthDetailsGrid.ExportSettings.IgnorePaging = true;
        PreAuthDetailsGrid.ExportSettings.OpenInNewWindow = true;
        PreAuthDetailsGrid.MasterTableView.ExportToExcel();
    }

    private void AddExpression()
    {
        GridSortExpression expression = new GridSortExpression();
        expression.FieldName = DropDownList3.SelectedItem.Value;
        expression.SetSortOrder(DropDownList2.SelectedItem.Value);
        this.PreAuthDetailsGrid.MasterTableView.SortExpressions.AddSortExpression(expression);
    }

    protected void btnAddExpression_Click(object sender, System.EventArgs e)
    {
        AddExpression();
        this.PreAuthDetailsGrid.MasterTableView.Rebind();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        this.PreAuthDetailsGrid.MasterTableView.AllowMultiColumnSorting = bool.Parse(DropDownList1.SelectedItem.Value);
        this.PreAuthDetailsGrid.Rebind();
    }

    protected void Clear_Click(object sender, System.EventArgs e)
    {
        this.PreAuthDetailsGrid.MasterTableView.SortExpressions.Clear();
        this.PreAuthDetailsGrid.MasterTableView.Rebind();
    }

    protected void PreAuthDetailsGrid_ItemCreated(object sender, GridItemEventArgs e)
    {
        //if (e.Item is GridFilteringItem)
        //{
        //    GridFilteringItem filteringItem = e.Item as GridFilteringItem;
        //    filteringItem.Width = Unit.Pixel(30);
        //    //set dimensions for the filter textbox  
        //    //TextBox box = filteringItem["ContactName"].Controls[0] as TextBox;
        //    //box.Width = Unit.Pixel(30);
        //}
    }

}
