﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Threading;
using Bupa.OSWeb.Helper;
using log4net;
using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;
using Microsoft.Office.Interop.Excel;
using DevExpress.Web.ASPxGridView;
using System.Web.Configuration;
using OS_DXC_WAP.CaesarWS;

public partial class BatchUploadReqDocs : System.Web.UI.Page
{
    #region Fields

    ServiceDepot_DNService ws;
    private string[] _docs;
    public string _UploadCategory = UploadCategory.MEMBER_ADDEMP;
    public bool _UploadMandatory = true;
    public string _Username = "Unknown Client";
    public string _InitialRequestID = string.Empty;
    DataSet ds = null;
    private ILog _log = LogManager.GetLogger(typeof(BatchUploadReqDocs));

    private readonly string _allowedExtensions = ".xls";

    private string _contractID = string.Empty;
    private string _batchOptionType;
    private string _dateErrors = string.Empty;
    private string _classErrors = string.Empty;
    private string _memberNoErrors = string.Empty;

    public string _urlValues = string.Empty;
    public string _containerValue;

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        Tracer.WriteMemberEntry(ref _log);
        try
        {
            _contractID = Convert.ToString(Session["ClientID"]);
            if (string.IsNullOrEmpty(_contractID))
                Response.Redirect("~/Default.aspx");
        }
        catch (ThreadAbortException) { }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //RefreshPage();
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
           
            CheckSessionExpired();
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
        }
        catch
        {

        }

        Tracer.WriteMemberEntry(ref _log);
        try
        {

            _batchOptionType = Request.QueryString["OptionType"];

            _dateErrors = string.Empty;
            _classErrors = string.Empty;
            _memberNoErrors = string.Empty;


            Tracer.WriteLine(ref _log, "_contractID is: " + _contractID);

            _UploadCategory = "SupportDocs";

            // Setup the Uploader control
            if (Session[WebPublication.Session_ClientUsername] != null)
                _Username = Convert.ToString(Session[WebPublication.Session_ClientUsername]);

            if (Page.IsPostBack)
            {
                if (Session["_InitialRequestID"] != null)
                    _InitialRequestID = Convert.ToString(Session["_InitialRequestID"]);
            }
            else
            {
                // Get a unique request ID so that the attachment page recognises this request to be unique from others
                _InitialRequestID = WebPublication.GenerateUniqueID;
                Session["_InitialRequestID"] = _InitialRequestID;
            }
            if (IsPostBack)
            {
                ds = (DataSet)Session["DataSet"];
                gvData.DataSource = ds.Tables[0];
                gvData.DataBind();
                gvData.Visible = true;
                btnValidateData.Visible = true;
            }


        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
    }

    protected void btnSubmitBatch_Click(object sender, EventArgs e)
    {
        Tracer.WriteMemberEntry(ref _log);

        string finalfileName = string.Empty;
        try
        {
            lblMessage.Text = "";
            GetResult();
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        finally
        {
            if (!string.IsNullOrEmpty(finalfileName) && File.Exists(finalfileName))
            {
                File.Delete(finalfileName);
            }
        }
        Tracer.WriteMemberExit(ref _log);
    }

    private bool EvaluateRequiredDocuments()
    {
        Tracer.WriteMemberEntry(ref _log);

        bool uploadedSupportingDocs = false;
        try
        {
            // First check if files have been uploaded
            int uploadedCount = 0;
            if (_UploadMandatory)
            {
                // Check that files have been uploaded
                UploadManager uploadManager = new UploadManager(Int32.MinValue, Int32.MinValue);
                DataSet uploadedFileSet = uploadManager.GetUploadedFileSet(_InitialRequestID, _UploadCategory);

                if (uploadedFileSet == null || uploadedFileSet.Tables.Count == 0 || uploadedFileSet.Tables[0].Rows.Count == 0)
                {
                    uploadedCount = 0;
                }
                else
                {
                    uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                    _docs = new string[uploadedFileSet.Tables[0].Rows.Count];
                    for (int i = 0; i < uploadedFileSet.Tables[0].Rows.Count; i++)
                    {
                        _docs[i] = Convert.ToString(uploadedFileSet.Tables[0].Rows[i][3]);
                    }
                }
            }

            /*Changed By Wessam 
            if (uploadedCount > 0)
            {
                uploadedSupportingDocs = false;
            }
            else
            {
                uploadedSupportingDocs = true;
            }*/

            if (uploadedCount > 0)
            {
                uploadedSupportingDocs = true;
            }
            else
            {
                uploadedSupportingDocs = false;
            }

        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
        return uploadedSupportingDocs;
    }

    private bool IsAllowedExtension(string file)
    {
        Tracer.WriteMemberEntry(ref _log);

        bool allowed = false;
        try
        {
            // Allowed extensions are a comma separated list
            string fileExtension = file.Substring(file.LastIndexOf(".") + 1);
            Tracer.WriteLine(ref _log, "fileExtension is: " + fileExtension);

            if (_allowedExtensions.ToLower().Contains(fileExtension.ToLower()))
                allowed = true;
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            throw;
        }
        Tracer.WriteLine(ref _log, "allowed is: " + allowed);

        Tracer.WriteMemberExit(ref _log);
        return allowed;
    }



    private bool IsUniquePath(string destinationPath)
    {
        Tracer.WriteMemberEntry(ref _log);

        bool isUnique = true;
        try
        {
            if (File.Exists(destinationPath))
                isUnique = false;
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            throw;
        }

        Tracer.WriteMemberExit(ref _log);
        return isUnique;
    }

    private System.Data.DataTable ParseBatchFile(string finalExcelFilePath)
    {
        Tracer.WriteMemberEntry(ref _log);

        System.Data.DataTable parsedDate = new System.Data.DataTable();

        string sConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + finalExcelFilePath + ";Extended Properties=" + Convert.ToChar(34).ToString() + "Excel 8.0;HDR=Yes" + Convert.ToChar(34).ToString();
        System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection(sConnectionString);

        try
        {
            if (cn.State != ConnectionState.Closed)
            {
                try
                {
                    cn.Close();
                }
                catch (Exception ce)
                {
                    Tracer.WriteException(ref _log, ce);
                }
            }
            cn.Open();

            System.Data.DataTable oTables = cn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            int i = 0;

            string sSheetName = oTables.Rows[i]["TABLE_NAME"].ToString();
            if (sSheetName.IndexOf("$") != -1)
            {
                DataSet oDataSet = new DataSet();
                OleDbDataAdapter oAdapter = new OleDbDataAdapter("SELECT * FROM [" + sSheetName + "]", cn);
                oAdapter.TableMappings.Add("Table", sSheetName);
                oAdapter.Fill(oDataSet);
                System.Data.DataTable oDataTable = oDataSet.Tables[0];

                if (oDataTable.Rows.Count > 0 & oDataTable.Columns.Count > 4)
                {
                    parsedDate = oDataTable;
                }
            }
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            throw;
        }
        finally
        {
            cn.Close();
        }

        Tracer.WriteMemberExit(ref _log);
        return parsedDate;
    }

    public static bool IsOptionalDate(string attemptedDate)
    {
        bool Success;
        try
        {
            // For date which can be either blank or in proper date format 
            if (attemptedDate != "")
            {
                DateTime dtParse = DateTime.Parse(attemptedDate);
                Success = true;
            }
            else
                return true;
        }
        catch (FormatException)
        {
            Success = false;
        }
        return Success;
    }

    private bool IsNaturalNumber(String strNumber)
    {
        Regex objNotNaturalPattern = new Regex("[^0-9]");
        Regex objNaturalPattern = new Regex("0*[1-9][0-9]*");
        return !objNotNaturalPattern.IsMatch(strNumber) &&
        objNaturalPattern.IsMatch(strNumber);
    }

    private bool IsDate(string attemptedDate)
    {
        bool Success = false;
        try
        {
            if (attemptedDate != string.Empty)
            {
                DateTime dtParse = DateTime.Parse(attemptedDate);
                Success = true;
            }
        }
        catch (Exception) { }

        return Success;
    }

    public void CallService(SubTxnRequest_DN request)
    {
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;

        ws = new ServiceDepot_DNService();
        ws.Url = CoreConfiguration.Instance.GetConfigValue("ServiceDepot_DN");
        Tracer.WriteLine(ref _log, "In batch: " + ws.Url);

        SubTxnResponse_DN response;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.SubTxn(request);

            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");
                lblMessage.Text = sb.ToString() + "<br/>Please try again rectifying the above errors.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Font.Bold = true;
                btnValidateData.Visible = true;
                caesarInvokeError = true;
            }
            else
            {
                lblMessage.Text = "Thank you for submitting your request. We are now validating the submitted file. Your reference no. is <b>" + response.ReferenceNo.ToString() + "</b>";

                lblRowErrors.Visible = false;

                btnSubmitBatch.Visible = false;


                btnValidateData.Visible = false;
                gvData.Visible = false;
                caesarInvokeError = false;
                caesarReferenceID = response.ReferenceNo.ToString();
            }


        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
    }

    public void Notify(string uploadSessionID, string function, string referenceID, string[] addressees, string fullName, string username)
    {
        // Send an email informing the team about a pre-auth request
        string fromEmail = "donotreply@bupa.com.sa";

        string subject = function + " reference:" + referenceID + " - attachments added by " + fullName + " (" + username + ")";
        string content = "Please view the attached uploaded documents added by " + fullName + " (" + username + ") for " + function + " reference: " + referenceID;

        // Get the list of attachments for this user
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);

        StringCollection attachments = new StringCollection();
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow r in d.Tables[0].Rows)
            {
                string attachPath = r["VirtualPath"].ToString();
                attachments.Add(attachPath);
            }
        }
        else
        {
            // No attachments and therefore, nothing to email
            return;
        }

        // Get the addressees
        foreach (string toEmail in addressees)
        {
            SendEmail(fromEmail, toEmail, subject, content, attachments);
        }
    }

    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        // Setup the mail message
        MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

        // Deal with the attachments
        if (attachmentPaths != null)
        {
            foreach (string path in attachmentPaths)
            {
                // Create the attachment
                System.Net.Mail.Attachment attachment;
                if (System.IO.File.Exists(Server.MapPath(path)))
                {
                    attachment = new System.Net.Mail.Attachment(Server.MapPath(path));
                    // Add the attachment
                    mail.Attachments.Add(attachment);
                }
            }
        }

        string smtpHost = WebPublication.EmailRelayAddress();
        int smtpPort = WebPublication.EmailRelayPort();

        SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
        mail.IsBodyHtml = true;
        smtp.Send(mail);
    }

    public void GetResult()
    {


        try
        {
            ////Added By Sakthi on 08-Nov-2016 for Rejected request should not able to any action
            ////Start
            int rejectedCount = 0;
            ////End
            ws = new ServiceDepot_DNService();

            MbrRejListRequest_DN wsRequest = new MbrRejListRequest_DN();
            MbrRejListResponse_DN wsResponse = new MbrRejListResponse_DN();


            System.Data.DataTable dt = new System.Data.DataTable();

            dt.Columns.Add("SupportDocs");
            dt.Columns.Add("Seq");
            dt.Columns.Add("Ref No");
            dt.Columns.Add("Contract No");
            dt.Columns.Add("Member Name");
            dt.Columns.Add("Saudi ID/Iqama No.");



            wsRequest.transactionID = TransactionManager.TransactionID();
            wsRequest.Username = WebPublication.CaesarSvcUsername;
            wsRequest.Password = WebPublication.CaesarSvcPassword;
            wsRequest.contNo = _contractID;
            if (!string.IsNullOrEmpty(txtSearchDate.Text))
            {
                wsRequest.subDate = Convert.ToDateTime(txtSearchDate.Text);
            }
            if (!string.IsNullOrEmpty(txtSearchRef.Text))
            {
                wsRequest.refNo = txtSearchRef.Text;
            }
            wsResponse = ws.MbrRejList(wsRequest);


            if (wsResponse.status != "0")
            {
                for (int i = 0; i < wsResponse.errorMessage.Length; i++)
                 lblMessage.Text = "Fail " + wsResponse.errorMessage[i];
                gvData.Visible = false;
                btnValidateData.Visible = false;
            }
            else
            {

                foreach (MbrRejListDetail_DN dtl in wsResponse.detail)
                {
                    DataRow dr = dt.NewRow();
                    dr["SupportDocs"] = "";
                    dr["Seq"] = dtl.SeqNo;
                    dr["Ref No"] = dtl.RefNo;
                    dr["Contract No"] = dtl.ContNo;
                    dr["Member Name"] = dtl.MbrName;
                    dr["Saudi ID/Iqama No."] = dtl.IDCardNo;
                    ////Added By Sakthi on 08-Nov-2016 for Rejected request should not able to any action
                    ////Start
                    if (dtl.TxnStatus.Trim().Equals("R"))
                        rejectedCount++;
                    ////End
                    dt.Rows.Add(dr);
                }

                /*try
                {
                    if (!string.IsNullOrEmpty(gvData.Columns["Seq"].Caption.ToString()))
                    {
                        gvData.Columns.Remove(gvData.Columns["Seq"]);

                    }
                    if (!string.IsNullOrEmpty(gvData.Columns["Ref No"].Caption.ToString()))
                    {
                        gvData.Columns.Remove(gvData.Columns["Ref No"]);

                    }
                    if (!string.IsNullOrEmpty(gvData.Columns["Contract No"].Caption.ToString()))
                    {
                        gvData.Columns.Remove(gvData.Columns["Contract No"]);

                    }
                    if (!string.IsNullOrEmpty(gvData.Columns["Member Name"].Caption.ToString()))
                    {
                        gvData.Columns.Remove(gvData.Columns["Member Name"]);

                    }
                    if (!string.IsNullOrEmpty(gvData.Columns["Saudi ID/Iqama No."].Caption.ToString()))
                    {
                        gvData.Columns.Remove(gvData.Columns["Saudi ID/Iqama No."]);

                    }
                }
                catch { }*/

                    GridViewDataTextColumn column1 = new GridViewDataTextColumn();
                    column1.FieldName = "SupportDocs";
                    column1.VisibleIndex = 2;
                    column1.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    column1.Caption = "Support Docs";
                    column1.Visible = false;

                    GridViewDataTextColumn column2 = new GridViewDataTextColumn();
                    column2.FieldName = "Seq";
                    column2.VisibleIndex = 2;
                    column2.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    column2.Caption = "Seq";


                    GridViewDataTextColumn column3 = new GridViewDataTextColumn();
                    column3.FieldName = "Ref No";
                    column3.VisibleIndex = 2;
                    column3.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    column3.Caption = "Ref No";


                    GridViewDataTextColumn column4 = new GridViewDataTextColumn();
                    column4.FieldName = "Contract No";
                    column4.VisibleIndex = 2;
                    column4.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    column4.Caption = "Contract No";

                    GridViewDataTextColumn column5 = new GridViewDataTextColumn();
                    column5.FieldName = "Member Name";
                    column5.VisibleIndex = 2;
                    column5.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    column5.Caption = "Member Name";


                    GridViewDataTextColumn column6 = new GridViewDataTextColumn();
                    column6.FieldName = "Saudi ID/Iqama No.";
                    column6.VisibleIndex = 2;
                    column6.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                    column6.Caption = "Saudi ID/Iqama No.";

                    

                    gvData.Columns.Add(column1);
                    gvData.Columns.Add(column2);
                    gvData.Columns.Add(column3);
                    gvData.Columns.Add(column4);
                    gvData.Columns.Add(column5);
                    gvData.Columns.Add(column6);

               

          

                ds = new DataSet();
                ds.Tables.Add(dt);
                ds.Tables[0].PrimaryKey = new DataColumn[] { dt.Columns["SEQ"] };

                Session["DataSet"] = ds;

                ////Added By Sakthi on 08-Nov-2016 for Rejected request should not able to any action
                ////Start
                ////Old Code Start
                ////gvData.DataSource = ds.Tables[0];
                ////gvData.DataBind();
                ////gvData.Visible = true;
                ////Old Code End
                ////New Code Start
                bool IsVisible = rejectedCount > 0 ? false : true;
                gvData.Columns[0].Visible = IsVisible;
                gvData.Columns[1].Visible = IsVisible;
                gvData.Columns[2].Visible = IsVisible;
                btnValidateData.Visible = IsVisible;
                gvData.DataSource = ds.Tables[0];
                gvData.DataBind();
                gvData.Visible = true;
                ////New Code End 
            }
        }

        catch { }


    }


    protected bool CheckDataType(string _value, string _dateType)
    {
        bool _Valid = false;

        switch (_dateType)
        {
            case "Number":
                try
                {
                    _value = Convert.ToInt64(_value).ToString();
                    _Valid = true;
                }
                catch
                {
                    _Valid = false;
                }
                break;
            case "Date":
                try
                {
                    _value = Convert.ToDateTime(_value).ToShortDateString();
                    _Valid = true;
                }
                catch
                {
                    _Valid = false;
                }
                break;
        }


        return _Valid;

    }
    protected void gvData_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {

        try
        {
            if (e.CellValue.ToString().Contains("/"))
            {
                if (!CheckDataType(e.CellValue.ToString(), "Date"))
                {
                    e.Cell.BackColor = System.Drawing.Color.Red;
                    e.Cell.ForeColor = System.Drawing.Color.White;

                }
                else
                {
                    e.Cell.BackColor = System.Drawing.Color.Transparent;
                    e.Cell.ForeColor = System.Drawing.Color.Transparent;

                }
            }


        }
        catch { }

    }

    protected void gvData_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView gridView = (ASPxGridView)sender;
        ds = (DataSet)Session["DataSet"];
        System.Data.DataTable dataTable = ds.Tables[0];
        DataRow row = dataTable.Rows.Find(e.Keys[0]);
        IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();
        enumerator.Reset();
        while (enumerator.MoveNext())
            row[enumerator.Key.ToString()] = enumerator.Value;
        gridView.CancelEdit();
        e.Cancel = true;




    }




    protected void gvData_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        try
        {
            if (e.Value.ToString().Contains("/"))
            {
                if (!CheckDataType(e.Value.ToString(), "Date"))
                {
                    e.Column.CellStyle.BackColor = System.Drawing.Color.Red;
                    e.Column.CellStyle.ForeColor = System.Drawing.Color.White;

                }
                else
                {
                    e.Column.CellStyle.BackColor = System.Drawing.Color.Transparent;
                    e.Column.CellStyle.ForeColor = System.Drawing.Color.Black;


                }
            }
        }
        catch { }
    }

    protected string getIndecator(int RowIndex)
    {
        try
        {
            string strImagePath = "../images/untick.png";


            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[RowIndex]["SupportDocs"].ToString()))
            {
                strImagePath = "../images/tick.png";
            }

            return strImagePath;
        }
        catch { return "../images/untick.png"; }
    }


    protected void btnValidateData_Click(object sender, EventArgs e)
    {
        try
        {
            bool _validate = true;
            lblMessage.Text = "";

            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                {
                    if (string.IsNullOrEmpty(ds.Tables[0].Rows[x]["SupportDocs"].ToString()))
                    {

                        _validate = false;
                        Alert.Show("Please upload support documents");

                    }
                }
            }
            if (_validate)
            {

                SubmitDataToCaeser();
            }

        }
        catch { }
    }

    protected void SubmitDataToCaeser()
    {

        ws = new ServiceDepot_DNService();

        ReSubTxnRequest_DN request;
        ReSubTxnResponse_DN response;

        int _countRec = 0;
        int _contDocs = 0;


        request = new ReSubTxnRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();

        foreach (DataRow trow in ds.Tables[0].Rows)
        {
            string[] SupportDocs = trow["SupportDocs"].ToString().Split(new char[] { ';' });
            _contDocs = _contDocs + SupportDocs.Length;
        }

        AllMbrDoc_DN[] allmbrdoc = new AllMbrDoc_DN[_contDocs];
        int x = 0;
        foreach (DataRow trow in ds.Tables[0].Rows)
        {
            string[] SupportDocs = trow["SupportDocs"].ToString().Split(new char[] { ';' });
            foreach (string i in SupportDocs)
            {
                allmbrdoc[x] = new AllMbrDoc_DN();
                allmbrdoc[x].sql_type = "CSR.SUB_TXN_DOC01";
                allmbrdoc[x].CONT_NO = trow[3].ToString();
                allmbrdoc[x].MBRSHP_NO = "";
                allmbrdoc[x].SEQ_NO = Convert.ToInt32(trow[1].ToString());
                WebPublication _webPub = new WebPublication();
                allmbrdoc[x].DOC_PATH = _webPub.DocLocation() +i;
                _countRec = _countRec + x;
                x = x + 1;
            }
            
        

        }


        request.refNo = txtSearchRef.Text;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.detail = allmbrdoc;
		
		
        //[AKIF] after CR 232 deployed to PROD issue in this page java.lang.null pointer is fixed with below 31-Dec-2015
        AllMbrDetail_DN[] mbrDetailAraay = new AllMbrDetail_DN[1];
        AllMbrDetail_DN mbrDetail = new AllMbrDetail_DN();
        mbrDetail.sql_type = "CSR.SUB_TXN_REC";
        mbrDetailAraay[0] = mbrDetail;// new AllMbrDetail_DN();
        request.mbrDetail = mbrDetailAraay;

        request.amd_Mbr_Ind = "N";

		
		
        try
        {
            response = ws.ReSubTxn(request);
            StringBuilder sb = new StringBuilder(200);

            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");

                lblMessage.Text = sb.ToString();

                if (response.refNo != null)
                    lblMessage.Text += "Kindly re-check your membership number.";
            }
            else
            {
                lblMessage.Text = "Thank you for submitting your request. Your reference number is " + Convert.ToString(txtSearchRef.Text) + ". We are now validating the submitted file.";
                gvData.Visible = false;
                btnValidateData.Visible = false;
            }
        }
        catch (Exception ex)
        {

            lblMessage.Text = ex.Message; /// this message would be used by transaction
            lblMessage.Text = "Error Encountered. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            lblMessage.Visible = true;
        }
    }

    protected void gvData_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ds = (DataSet)Session["DataSet"];
        gvData.DataSource = ds.Tables[0];
        gvData.DataBind();
    }
    protected void ddlSearchtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ddlSearchtype.SelectedItem.Value.ToString())
        {
            case "1":
                refnum.Visible = true;
                submitdate.Visible = false;
                txtSearchDate.Text = "";
                break;
            case "2":
                refnum.Visible = false;
                txtSearchRef.Text = "";
                submitdate.Visible = true;
                break;
            case "3":
                refnum.Visible = true;
                submitdate.Visible = true;
                break;
            case "0":
                refnum.Visible = false;
                submitdate.Visible = false;
                txtSearchDate.Text = "";
                txtSearchDate.Text = "";
                break;
        }
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }
}