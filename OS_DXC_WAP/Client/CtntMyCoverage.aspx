<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="ClientMaster.master" Inherits="CtntMyCoverage" Debug="true" Codebehind="CtntMyCoverage.aspx.cs" %>

<%@ MasterType VirtualPath="ClientMaster.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<asp:Content ID="Content1_1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
           <div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div> 
    
    <br /><br />
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" Visible="false">
        <ContentTemplate>
            <fieldset>
                <legend><h1>Member Information</h1></legend>
            <table  class="contentTable" width="100%" >
           
                <tr class="grayRow">
                    <td style="width: 114px">
                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server"   Text="3456998"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 114px" >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server" Text=""   ></asp:Label></td>
                </tr>
                <tr class="grayRow">
                    <td style="width: 114px" >
                        <asp:Label ID="lblCustomer_Name" runat="server" Text="Customer Name" Width="140px"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblCustomerName" runat="server"    Text="30/05/1970"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 114px">
                        <asp:Label ID="lblClass_Name" runat="server" Text="Class Name"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblClassName" runat="server" Text=""   ></asp:Label></td>
                </tr>
          
            </table>
           </fieldset>
            <br />
            <fieldset>
                <legend><h1>My Coverage</h1></legend>
                <table width="100%">
                    <tr>
                        <td>
                        <div id="CoverageListReport"  visible="false" runat="server" >
      
       </div>
                        </td>
                    </tr>
                </table>
                </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:Label ID="Message1" Font-Names="verdana" Font-Size="Small" Width="95%" runat="server" ></asp:Label>
    
</asp:Content>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    View policy benefit details of your staff
</asp:Content>
