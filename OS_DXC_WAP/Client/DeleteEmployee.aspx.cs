using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

using Bupa.OSWeb.Helper;
using Utility.Configuration;

public partial class DeleteEmployee : System.Web.UI.Page
{

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    private string strClientID;

    // The upload category for the uploading of this page
    private string _uploadCategory = UploadCategory.MEMBER_DELEMP;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["EType"] == "Dep")
        {
            lblAdd_Employee.Text = "Delete Dependent";
            _uploadCategory = UploadCategory.MEMBER_DELDEP;
        }

        strClientID = Session["ClientID"].ToString();
        SetupUploader(_uploadCategory);
    }

    private void SetupUploader(string uploadCategory)
    {
        uploader.Visible = true;

        // By default, unless a user has uploaded supporting documents,
        // the Submit button will be disabled
        Button1.Enabled = false;

        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        // Bind to allow resolving of # tags in mark-up
        DataBind();
    }

    protected void Page_LoadComplete(object sender, System.EventArgs e)
    {
        // Enable / Disable the submission option depending on whether the user 
        // has uploaded supported documents

        // Assume the button is disabled until checked
        Button1.Enabled = false;

        // Get what has been uploaded
        DataSet uploadedFileSet = uploader.UploadedFileSet;

        UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);

        if (_uploadmanger.WebIndecator("SuportDocs", strClientID))
        {
            tblUploader.Visible = true;

            int uploadedCount = 0;
            if (uploadedFileSet != null && uploadedFileSet.Tables.Count > 0)
            {
                uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                if (uploadedCount > 0)
                {
                    Button1.Enabled = true;
                }
                else
                {
                    Button1.Enabled = false;
                }

            }
        }
        else
        {
            //tblUploader.Visible = false;
            Button1.Enabled = true;

        }

    }

    // function used for Tab System when user requset for dependent ceritifcate
    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
    }

    private void fncDeleteEmployee()
    {
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;

        #region Caesar Invocation

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request;
        OS_DXC_WAP.CaesarWS.SubTxnResponse_DN response;
        request = new OS_DXC_WAP.CaesarWS.SubTxnRequest_DN();

        //request.TransactionID = TransactionManager.TransactionID();// long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1)); 
        request.TransactionID = WebPublication.GenerateTransactionID();
        
        request.TransactionType = "DELETE MEMBER";

        request.BatchIndicator = "N";
        request.detail = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[1];
        request.detail[0] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
        request.detail[0].sql_type = "CSR.SUB_TXN_REC";
        request.detail[0].ContractNo = strClientID;
        request.detail[0].MbrName = txtMemberName.Text;
        request.memberName = new string[1];
        request.memberName[0] = txtMemberName.Text;
        request.detail[0].IgamaID = "2222222222";
        request.detail[0].MembershipNo = txtMembershipNo.Text;
        request.detail[0].Reason = ddlDelReason.SelectedValue;
        request.detail[0].MemberDeleteDate = DateTime.ParseExact(txtDateOfDeletion.Text, "dd/MM/yyyy", null);

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.detail[0].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
        try
        {
            response = ws.SubTxn(request);
            StringBuilder sb = new StringBuilder(200);

            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");

                Message1.Text = sb.ToString();

                if (response.ReferenceNo != null)
                    Message1.Text += "Kindly re-check your membership number.";

                Panel1.Visible = false;
                Message1.Visible = true;

                caesarInvokeError = true;
            }
            else
            {
                Message1.Text = "Thank you for submitting your request. Your reference number is " + response.ReferenceNo.ToString() + ". We are now validating the submitted file.";
                Panel1.Visible = false;
                Message1.Visible = true;

                caesarInvokeError = false;
                caesarReferenceID = response.ReferenceNo.ToString();
            }
        }
        catch (Exception ex)
        {
            Panel1.Visible = false;
            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Error Encountered. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            Message1.Visible = true;
        }

        #endregion

        if (!caesarInvokeError)
        {
            // Send the email related to the attachments
            string username = "Unknown username";
            string fullName = "Unknown client";
            string[] addressees = UploadPublication.MembershipEmailAddresses();

            if (Session[WebPublication.Session_ClientUsername] != null)
                username = Session[WebPublication.Session_ClientUsername].ToString();
            if (Session[WebPublication.Session_ClientFullName] != null)
                fullName = Session[WebPublication.Session_ClientFullName].ToString();

            // Delegate to send emails
            //uploader.Notify(uploader.SessionID, _uploadCategory, caesarReferenceID, addressees, fullName, username);

            // Once done, hide the uploader from the view
            tblUploader.Visible = false;
        }
    }

    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        fncDeleteEmployee();
    }





}