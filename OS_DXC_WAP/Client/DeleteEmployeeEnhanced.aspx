﻿<%@ Page Language="C#"  MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" Inherits="Bupa.OSWeb.Client.DeleteEmployeeEnhanced"    Codebehind="DeleteEmployeeEnhanced.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Predictable">
    <script type='text/JavaScript' src='js/NumberValidation.js'></script>
    <script type='text/JavaScript'>
        function changetext(reqName) {
            if (reqName == "RFVtxtBadgeNo")
                ValidatorEnable(document.getElementById('<%= RFVtxtBadgeNo.ClientID %>'), false);
            else if (reqName == "frvMembershipNo")
                ValidatorEnable(document.getElementById('<%= frvMembershipNo.ClientID %>'), false);

        }
        function validateAndConfirm(message) {
            var validated = Page_ClientValidate('1');
            if (validated) {
                return confirm(message);
            }
        }
    </script>
    <link rel="stylesheet" href="<%# Page.ResolveUrl("~/Styles/OSWebStyle.css") %>" type="text/css" charset="utf-8" />

    <fieldset style="padding: 10px">
        <legend>
            <h1>
                <asp:Label ID="lblDelEmp" runat="server" Text="Delete Employee" Font-Size="Large" Font-Names="verdana"></asp:Label>
            </h1>
        </legend>
        <%--<asp:ScriptManager runat="server" ID="scriptManager" />--%>
        <div style="text-align: left;">
            <asp:Image ID="imgSuc" runat="server" Visible="false" ImageUrl="~/images/reimp_req_suc.png" /><br />
            <asp:Label ID="lblResult" runat="server" ForeColor="Green"></asp:Label>
        </div>
        <p align="right">
            <asp:HyperLink ID="lnkBatchUpload" runat="server" Font-Size="Small" Target="_self"  Visible="true">Batch Upload</asp:HyperLink>
        </p>
        <br />
        <asp:Label ID="Label1" runat="server" Width="527px" Font-Size="Small" Font-Names="Arial" ForeColor="Red"></asp:Label><br />
        <asp:Label ID="Label2" runat="server" Font-Size="12px" Font-Names="Arial" ForeColor="Black" Text="Fields marked with asterisk [ * ]  are mandatory."></asp:Label>
        <asp:Label ID="lblAramcoMessage" runat="server" Font-Size="Small" ForeColor="Black" Text="One Field is Mandatory Either the badge or membership"></asp:Label>
        <asp:Panel runat="server" ID="panelForm" Visible="true">
            <%if (uploader.LblUpload.Text.Length > 0)
              { %>
            <br />
            <br />
            <table style="width: 750px; font-size: 17px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
                runat="server" id="Table3">
                <tr>
                    <td align="left">
                        <span style="color: #cc0000"><%# uploader.LblUpload.Text%></span>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <% }%>
            <table style="font-size: small;" width="750px">
                <tr>
                    <td style="width: 206px">
                        <span style="color: #ff0000">*</span><asp:Label ID="lblEmployeeNo" runat="server" Text="Membership No"  />
                    </td>
                    <td>
                        <asp:TextBox CssClass="textbox" ID="txtMembershipNo" runat="server" MaxLength="8" onchange="changetext('RFVtxtBadgeNo')" ></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="frvMembershipNo" runat="server" ControlToValidate="txtMembershipNo" Display="Static" ErrorMessage="Field is mandatory" /><br />
                        <asp:RegularExpressionValidator runat="server" ID="txtMShipNoRegExp" ControlToValidate="txtMembershipNo"  ValidationExpression="^\d+$" ErrorMessage="Only Numbers."></asp:RegularExpressionValidator>
                    </td>
                    <td align="right">
                        <asp:Button CssClass="submitButton" ID="btnGetMemberDetails" runat="server" OnClick="btnGetMemberDetailsClick" Text="Retrieve Member Details" BackColor="Control" UseSubmitBehavior="false" Width="170px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="DivStafflbl" runat="server">
                            <span style="color: #ff0000">*</span>Badge No
                    <br />
                        </div>
                    </td>
                    <td style="width: 223px">
                        <div id="DivStafftxt" runat="server">

                            <asp:TextBox ID="txtBadgeNo"   runat="server" CssClass="textbox" onchange="changetext('frvMembershipNo')"></asp:TextBox>

                            <br />
                        </div>
                    </td>
                    <td style="width: 200px" valign="top">
                        <table>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="RFVtxtBadgeNo" runat="server" ControlToValidate="txtBadgeNo" Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator runat="server" ID="txtBadgeNoRegExp"  ControlToValidate="txtBadgeNo" ValidationExpression="\d*-?\d*" ErrorMessage="Only Numbers with hyphen."></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Panel runat="server" ID="panelResults">
                <asp:UpdatePanel runat="server" ID="updatePanel2">
                    <ContentTemplate>
                        <div id="entryForm">
                            <table id="tblMemberDetails" style="font-size: small;" width="750px" visible="false">
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">Please review the details below and enter the required fields</td>
                                </tr>
                                <tr>
                                    <td align="left">Membership no:&nbsp;</td>
                                    <td align="left">
                                        <asp:Label ID="lblMembershipNo" runat="server" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left">Member name:&nbsp;</td>
                                    <td align="left">
                                        <asp:Label ID="lblMemberName" runat="server" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left">Date of birth:&nbsp;</td>
                                    <td align="left">
                                        <asp:Label ID="lblBirthDate" runat="server" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <!-- Required fields for deletion -->
                                <tr id="IDType" runat="server">
                                    <td align="left"><span style="color: #ff0000"></span>ID Type&nbsp;</td>
                                    <td align="left">
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEIDType"
                                                  runat="server" ClientIDMode="Predictable"
                                                BackColor="White" AutoPostBack="true" OnSelectedIndexChanged="ddlEIDType_SelectedIndexChanged">
                                                <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Passport" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr id="MaritalStatus" runat="server">
                                    <td align="left"><span style="color: #ff0000"></span>Marital Status&nbsp;</td>
                                    <td align="left">
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEMaritalStatus"
                                                  runat="server" ClientIDMode="Predictable"
                                                BackColor="White">
                                                <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr id="SaudiIDIqma" runat="server">
                                    <td align="left"><span style="color: #ff0000"></span>Saudi ID/Iqama no:&nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox CssClass="textbox" ID="txtSaudiIqamaIDNumber" runat="server"
                                            ValidationGroup="1" BackColor="White" MaxLength="10"></asp:TextBox>
                                        <asp:RegularExpressionValidator
                                            ID="rfvSuadiID" runat="server" ControlToValidate="txtSaudiIqamaIDNumber"
                                            Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[2][0-9]{9}"
                                            ValidationGroup="1" Width="142px">Incorrect Iqama ID</asp:RegularExpressionValidator>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr id="IDExpiryDate" runat="server">
                                    <td align="left"><span style="color: #ff0000"></span>ID Expiry Date:&nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox CssClass="textbox" ID="txtEIDExpDate" runat="server" onfocus="this.blur();"
                                            onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="White"></asp:TextBox>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr id="Profession" runat="server">
                                    <td align="left"><span style="color: #ff0000"></span>Profession:&nbsp;</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddProfession" CssClass="DropDownListCssClass" ClientIDMode="Predictable" runat="server" BackColor="White" Style="width: 252px; height: 28px;" OnDataBound="ddProfession_DataBound" DataValueField="profCode" DataTextField="profName"></asp:DropDownList>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr id="District" runat="server">
                                    <td align="left"><span style="color: #ff0000"></span>District:&nbsp;</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddDistrict" CssClass="DropDownListCssClass" ClientIDMode="Predictable" runat="server" BackColor="White" Style="width: 252px; height: 28px;" OnDataBound="ddDistrict_DataBound" DataValueField="distCode" DataTextField="distName" Enabled="true"></asp:DropDownList>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr id="Nationality" runat="server">
                                    <td align="left"><span style="color: #ff0000"></span>Nationality:&nbsp;</td>
                                    <td align="left">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ClientIDMode="Predictable"  ID="ddlENationality" Font-Size="9" runat="server" DataTextField="text" DataValueField="value" BackColor="White" Style="width: 252px; height: 28px;">
                                            <asp:ListItem Value="">-Select-</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr id="MobileNo" runat="server">
                                    <td align="left"><span style="color: #ff0000"></span>Mobile No:&nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox CssClass="textbox" ID="txtEMobileNo" runat="server" MaxLength="10" BackColor="White"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="RegEMobileNo" runat="server"
                                            ControlToValidate="txtEMobileNo" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                                            ValidationExpression="[05][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]" ValidationGroup="1"
                                            Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left"><span style="color: #ff0000">*</span>Reason for deletion:&nbsp;</td>
                                    <td align="left">
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddDeleteReason" ClientIDMode="Predictable" runat="server" BackColor="#eff6fc">
                                                <asp:ListItem>-Select-</asp:ListItem>
                                                <asp:ListItem Value="DEL001">Member decided to leave KSA (Exit visa only)</asp:ListItem>
                                                <asp:ListItem Value="DEL002">Member resigned from the company</asp:ListItem>
                                                <asp:ListItem Value="DEL003">Member was terminated from work / company</asp:ListItem>
                                                <asp:ListItem Value="DEL004">Member moved to another insurer</asp:ListItem>
                                                <asp:ListItem Value="DEL005">Member moved to another sponsor</asp:ListItem>
                                                <asp:ListItem Value="DEL006">Death of member</asp:ListItem>
                                                <asp:ListItem Value="DEL007">Wrong enrolment</asp:ListItem>
                                                <asp:ListItem Value="DEL008">Dependent Left KSA</asp:ListItem>
                                                <asp:ListItem Value="DEL009">Dependent passed away</asp:ListItem>
                                                <asp:ListItem Value="DEL010">Dependent moved to another insurer</asp:ListItem>
                                                <asp:ListItem Value="DEL011">Dependent moved to another sponsor</asp:ListItem>
                                                <asp:ListItem Value="DEL012">Divorced the partner</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvDeleteReason" runat="server" ControlToValidate="ddDeleteReason" 
                                                Display="Static" ErrorMessage="Field is mandatory" InitialValue="-Select-" ValidationGroup="1" />
                                        </div>
                                    </td>
                                    <td align="left">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span style="color: #ff0000">*</span>Effective date:&nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox CssClass="textbox" ID="txtEffectiveDate" runat="server" onfocus="this.blur();"
                                            onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEffectiveDate" 
                                                Display="Static" ErrorMessage="Field is mandatory" ValidationGroup="1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
                    id="tblUploader" runat="server">
                    <tr>
                        <td align="left">
                            <span style="color: #ff3300"><strong>*</strong></span><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                        </td>
                        <td align="right">
                            <a href="<%# Page.ResolveUrl("~/Docs/ReqDocs2012.pdf") %>" target="_blank">Required Document List</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .pmp) 5MB limit per file" /><br />
                            <BUPA:Uploader runat="server" ID="uploader" />
                        </td>
                    </tr>
                </table>
                <table style="width: 750px;">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button CssClass="submitButton" ID="btnSubmit" runat="server" OnClick="btnSubmit_Click"
                                Text="Delete Member" Width="133px" BackColor="Control" ValidationGroup="1" 
                                OnClientClick="return validateAndConfirm('Are you sure you wish to delete this employee?');"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"></td>
                    </tr>
                    <tr>
                        <td align="right">&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
            <table style="width: 100%">
                <tr>
                    <td>
                        <uc2:OSNav ID="OSNav1" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="ContentPlaceHolder1">
    <p id="LblChangeBranchBanner" runat="server" >
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>
</asp:Content>