﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" Inherits="AddEmpDepDeclr"
    EnableEventValidation="false" ClientIDMode="Predictable" AutoEventWireup="true" Codebehind="AddEmpDepDeclr.aspx.cs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register TagPrefix="BUPA" TagName="uploader" Src="~/Uploader.ascx" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucHijriGregorian.ascx" TagPrefix="BUPA" TagName="ucHijriGregorian" %>
 


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <style type="text/css">
        .labelClass
        {
            float: left;
            margin-right: 10px;
            line-height: 30px;
            width: 205px;
        }

        .controlClass
        {
            float: left;
            margin-right: 10px;
        }

        .middle
        {
            background-color: yellow;
            display: block;
            overflow: hidden;
        }

        .clear
        {
            clear: both;
        }

        .containerClass
        {
            height: 60px;
            width: 100%;
        }

        .controlClass input, .controlClass select
        {
            width: 260px;
        }

        .FloatingDiv
        {
            position: fixed;
            top: 0;
            left: 0;
            text-align: center;
            padding: 200px 0 0 0;
            z-index: 999;
            width: 100%;
            height: 100%;
            background: url('../Images/Floatingimg.png') repeat top left;
        }

        .Floatimage
        {
            display: block;
            width: 100%;
            height: 100%;
            vertical-align: middle;
        }

        .button
        {
            font: bold 13px Arial;
            text-decoration: none;
            background-color: #EEEEEE;
            color: #0099FF;
            padding: 2px 6px 2px 6px;
            border-top: 1px solid #CCCCCC;
            border-right: 1px solid #0099FF;
            border-bottom: 1px solid #0099FF;
            border-left: 1px solid #CCCCCC;
        }
    </style>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $("#ddlIDType").change(function () {
                ChangeDepValidationExp();
            });

            $("#ddlMemberType").change(function () {
                ToggleMemberTypeDropdown();

            });

            $("#ddlAgeGroup").change(function () {
                ToggleAgeGroupDropdown();
            });

            $("#ddlEIDType").change(function () {
                ChangeValidationExp();
            });

        });

        prm.add_endRequest(function () {
            $("#ddlIDType").change(function () {
                ChangeDepValidationExp();
            });

            $("#ddlMemberType").change(function () {
                ToggleMemberTypeDropdown();

            });

            $("#ddlAgeGroup").change(function () {
                ToggleAgeGroupDropdown();
            });

            $("#ddlEIDType").change(function () {
                ChangeValidationExp();
            });
            ChangeDepValidationExp();
            ChangeValidationExp();
        });

        function ToggleMemberTypeDropdown() {
            if ($("#ddlMemberType").val() == "C" || $("#ddlMemberType").val() == "D" || $("#ddlMemberType").val() == "SO") {
                $("#divAgeGroup").show();
                document.getElementById('rfv_ddlAgeGroup').enabled = true;
                document.getElementById('rfv_ddlAgeGroup').isvalid = true;
            }
            else {
                $("#divAgeGroup").hide();
                document.getElementById('rfv_ddlAgeGroup').enabled = false;
                document.getElementById('rfv_ddlAgeGroup').isvalid = false;
            }
        }

        function ToggleAgeGroupDropdown() {
            if ($("#ddlAgeGroup").val() == "1") {
                ValidatorEnable(document.getElementById('rfv_ID'), false);
                ValidatorEnable(document.getElementById('rfvDepHijriYearOfBirth'), false);
                ValidatorEnable(document.getElementById('rfv_IdExpiryDateDep'), false);
            }
            else {
                $('#<%= divDepID.ClientID %>').show();
                document.getElementById('rfv_ID').enabled = true;
                document.getElementById('rfv_ID').isvalid = true;
                document.getElementById('rfvDepHijriYearOfBirth').enabled = true;
                document.getElementById('rfvDepHijriYearOfBirth').isvalid = true;
                if (document.getElementById('rfv_IdExpiryDateDep') != null) {
                    document.getElementById('rfv_IdExpiryDateDep').enabled = true;
                    document.getElementById('rfv_IdExpiryDateDep').isvalid = true;
                }
            }
        }

        function ChangeValidationExp() {
            var SelectedValue = 0;
            if ($('#ddlEIDType').length > 0) {
                SelectedValue = $("#ddlEIDType").val();
            }
            if ($('#revSuadi').length > 0) {
                ValidatorEnable(document.getElementById('revSuadi'), false);
            }
            if ($('#revIqama').length > 0) {
                ValidatorEnable(document.getElementById('revIqama'), false);
            }
            if ($('#revBorder').length > 0) {
                ValidatorEnable(document.getElementById('revBorder'), false);
            }
            if ($('#rfvEmpHijriYearOfBirth').length > 0) {
                ValidatorEnable(document.getElementById('rfvEmpHijriYearOfBirth'), false);
            }

            if (SelectedValue == 1) {
                if ($('#revSuadi').length > 0) {
                    document.getElementById('revSuadi').enabled = true;
                    document.getElementById('revSuadi').isvalid = true;
                }
                if ($('#divEmpHijriYearOfBirth').length > 0) {
                    $("#divEmpHijriYearOfBirth").show();
                }
                if ($('#rfvEmpHijriYearOfBirth').length > 0) {
                    document.getElementById('rfvEmpHijriYearOfBirth').enabled = true;
                    document.getElementById('rfvEmpHijriYearOfBirth').isvalid = true;
                }
            }
            else if (SelectedValue == 2) {
                if ($('#revIqama').length > 0) {
                    document.getElementById('revIqama').enabled = true;
                    document.getElementById('revIqama').isvalid = true;
                }
                if ($('#divEmpHijriYearOfBirth').length > 0) {
                    $("#divEmpHijriYearOfBirth").hide();
                }
            }
            else if (SelectedValue == 4) {
                if ($('#revBorder').length > 0) {
                    document.getElementById('revBorder').enabled = true;
                    document.getElementById('revBorder').isvalid = true;
                }
                if ($('#divEmpHijriYearOfBirth').length > 0) {
                    $("#divEmpHijriYearOfBirth").hide();
                }
            }
        }

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);
            }
            if (key == 11) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }

        function ChangeDepValidationExp() {
            var SelectedValue = 0;
            if ($('#ddlIDType').length > 0) {
                SelectedValue = $("#ddlIDType").val();
            }

            if ($('#revDepSuadi').length > 0) {
                ValidatorEnable(document.getElementById('revDepSuadi'), false);
            }
            if ($('#revDepIqama').length > 0) {
                ValidatorEnable(document.getElementById('revDepIqama'), false);
            }
            if ($('#revDepBorder').length > 0) {
                ValidatorEnable(document.getElementById('revDepBorder'), false);
            }

            if (SelectedValue == 1) {
                if ($('#revDepSuadi').length > 0) {
                    document.getElementById('revDepSuadi').enabled = true;
                    document.getElementById('revDepSuadi').isvalid = true;
                }
                if ($('#divDepHijriYearOfBirth').length > 0) {
                    $("#divDepHijriYearOfBirth").show();
                }
                if ($('#rfvDepHijriYearOfBirth').length > 0) {
                    document.getElementById('rfvDepHijriYearOfBirth').enabled = true;
                    document.getElementById('rfvDepHijriYearOfBirth').isvalid = true;
                }
            }
            else if (SelectedValue == 2) {
                if ($('#revDepIqama').length > 0) {
                    document.getElementById('revDepIqama').enabled = true;
                    document.getElementById('revDepIqama').isvalid = true;
                }
                if ($('#divDepHijriYearOfBirth').length > 0) {
                    $("#divDepHijriYearOfBirth").hide();
                }
                if ($('#rfvDepHijriYearOfBirth').length > 0) {
                    ValidatorEnable(document.getElementById('rfvDepHijriYearOfBirth'), false);
                }
            }
            else if (SelectedValue == 4) {
                if ($('#revDepBorder').length > 0) {
                    document.getElementById('revDepBorder').enabled = true;
                    document.getElementById('revDepBorder').isvalid = true;
                }
                if ($('#divDepHijriYearOfBirth').length > 0) {
                    $("#divDepHijriYearOfBirth").hide();
                }
                if ($('#rfvDepHijriYearOfBirth').length > 0) {
                    ValidatorEnable(document.getElementById('rfvDepHijriYearOfBirth'), false);
                }
            }

            //if new born  and nationality make the hijri year of birth not mandotory
            if ($("#ddlAgeGroup").val() == "1" & SelectedValue == 1) {
                ValidatorEnable(document.getElementById('rfvDepHijriYearOfBirth'), false);
            }
        }

        function ValidateTitleGender(gender, title, type) {
            if (gender == "M") {
                if (title == 'Miss' || title == 'Ms' || title == 'Mrs') {
                    alert('Incorrect gender.');
                    if (type == 'Emp') document.getElementById('ddlEGender').selectedIndex = 0;
                    if (type == 'Dep') document.getElementById('ddlGender').selectedIndex = 0;
                    return false;
                }
            }

            if (gender == "F") {
                if (title == 'Mr') {
                    alert('Incorrect gender.');
                    if (type == 'Emp') document.getElementById('ddlEGender').selectedIndex = 0;
                    if (type == 'Dep') document.getElementById('ddlGender').selectedIndex = 0;
                    return false;
                }
            }
        }

        function doEnable(Val, txt1, txt2, txtBox1, txtBox2) {
            var textbox1 = document.getElementById(txtBox1);
            var textbox2 = document.getElementById(txtBox2);
            if (Val == 'Y') {
                ValidatorEnable(txt1, true);
                ValidatorEnable(txt2, true);
                textbox1.disabled = false;
                textbox2.disabled = false;
                textbox1.style.backgroundColor = '#FFFFC0';
                textbox2.style.backgroundColor = '#FFFFC0';
            }
            else {
                ValidatorEnable(txt1, false);
                ValidatorEnable(txt2, false);
                textbox1.disabled = true;
                textbox2.disabled = true;
                textbox1.value = '';
                textbox2.value = '';
                textbox1.style.backgroundColor = '#FFFFFF';
                textbox2.style.backgroundColor = '#FFFFFF';
            }
        }

        function OnClientClicked() {           
            var notification = $find("<%=RadNotification1.ClientID %>");
            if (notification != null) {
                notification._close(true);
                try {
                    ///Disabled we are not using any interaval for display. 
                    <%--setInterval(function () { $find("<%=RadNotification1.ClientID%>").ajaxRequest("Bedon Nationality"); }, 0);--%>
                } catch (e) {

                }
            }

        }

    </script>

    <fieldset style="padding: 10px">
        <legend>
            <h1>
                <asp:Label ID="lblAdd_Employee" runat="server" Text="Add Employee and Dependent"></asp:Label>
            </h1>
        </legend>

        <table width="99%">
            <tr valign="middle">
                <td></td>
                <td style="text-align: right">&nbsp;</td>
            </tr>

        </table>

        <div align="right">
            <asp:HyperLink ID="lnkBatchUpload" runat="server" Font-Size="Small" NavigateUrl="~/Client/BatchUpload.aspx?OptionType=AddEmployeeDependent" Target="_blank" Visible="true">Batch Upload</asp:HyperLink>
        </div>

        <br />

        <asp:Label ID="Label18" runat="server" Font-Size="Small" ForeColor="Black" Text="Fields marked with asterisk [ * ]  are mandatory."></asp:Label>
        <br /> <asp:Label ID="Message1" runat="server" Visible="true" Enabled="true" Text="testing"></asp:Label>
       
        <br />
        <br />

        <div style="text-align: left;">
            <asp:Image ID="imgSuc" runat="server" Visible="false" ImageUrl="~/images/reimp_req_suc.png" /><br />
        </div>
        

        <div id="main_entry_form">


            <asp:UpdatePanel runat="server" ID="updatePnl">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlELevelOfCover" />
                    <asp:AsyncPostBackTrigger ControlID="ddlLevelOfCover" />
                    <asp:AsyncPostBackTrigger ControlID="lnkBtnAddNewDep" />
                    <asp:AsyncPostBackTrigger ControlID="btnProceedEmployee" />
                    <asp:AsyncPostBackTrigger ControlID="btnProceedDep" />
                    <asp:AsyncPostBackTrigger ControlID="btnAddDep" />
                    <asp:AsyncPostBackTrigger ControlID="gvDependents" />
                </Triggers>
                <ContentTemplate>
                    <asp:UpdateProgress ID="uprgWorkRequest" runat="server">
                        <ProgressTemplate>
                            <div class="FloatingDiv">
                                <asp:Image ID="Image1" runat="server" ImageUrl="../Images/ajax-loader.gif"
                                    Style="height: 279px" /><br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:Panel ID="PanelEmployee" runat="server">
                        
                        <asp:Label ID="lblEmpErrorMessage" runat="server" ForeColor="Red"></asp:Label>
                        <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
                            <tr>
                                <td style="background-color: #ffffff; color: red; height: 25px; font-size: small; font-weight: bold;" valign="top" colspan="7" align="center">
                                    <asp:Literal ID="litMainError" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="background-color: #ffffff; height: 25px;" valign="top">
                                    <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Employee Details</span></strong>
                                </td>
                            </tr>
                        </table>
                        <div class="containerClass">
                            <div class="labelClass">
                                <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label10" runat="server"
                                    Font-Bold="False" ForeColor="#000036" Text="Level of cover"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                            </div>
                            <div class="controlClass">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlELevelOfCover" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlELevelOfCover_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="ddlELevelOfCover"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="Proceed" SetFocusOnError="true"
                                    Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="containerClass">
                            <div class="labelClass">
                                <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label36" runat="server"
                                    Font-Bold="False" ForeColor="#000036" Text="Member Type"></asp:Label>
                            </div>
                            <div class="controlClass">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEMemberType"
                                        ClientIDMode="Static" runat="server"
                                        DataTextField="mbrTypeDesc" DataValueField="mbrType" BackColor="#eff6fc">
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlEMemberType"
                                    InitialValue="0" SetFocusOnError="true"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="Proceed" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="entryForm">
                            <div class="containerClass">
                                <div class="labelClass">
                                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label27" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="ID Type"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEIDType"
                                            ClientIDMode="Static" runat="server"
                                            BackColor="#eff6fc">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="ddlEIDType"
                                        InitialValue="0" SetFocusOnError="true"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="Proceed" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><span style="color: #ff3300"><strong>*<asp:Label ID="Label7"
                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Saudi ID/ Iqama ID/ Border Entry"
                                        Width="185px"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span></span>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtEmployeeID" runat="server" MaxLength="10"
                                        BackColor="#eff6fc"
                                        AutoPostBack="false"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtEmployeeID" SetFocusOnError="true"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="Proceed" Width="110px">Field is mandatory</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator
                                        ID="revSuadi" runat="server" ControlToValidate="txtEmployeeID"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[1][0-9]{9}"
                                        ValidationGroup="Proceed" Width="142px">Incorrect Saudi ID</asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator
                                        ID="revIqama" runat="server" ControlToValidate="txtEmployeeID"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[2][0-9]{9}"
                                        ValidationGroup="Proceed" Width="142px">Incorrect Iqama ID</asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator
                                        ID="revBorder" runat="server" ControlToValidate="txtEmployeeID"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[3-5][0-9]{9}"
                                        ValidationGroup="Proceed" Width="142px">Incorrect Border Entry ID</asp:RegularExpressionValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div id="divEmpSponsorId" runat="server" class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label8" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Sponsor Id"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <telerik:RadComboBox ID="ddlSponsor" runat="server" Width="200px" Height="250px"
                                        EmptyMessage="Select/start typing" DataTextField="sponsorID"
                                        OnSelectedIndexChanged="ddlSponsor_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                    <asp:TextBox CssClass="textbox" Visible="false" Enabled="false" ID="txtESponsorId" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><br />
                                    <asp:RegularExpressionValidator
                                        ID="revEmpSponsorID" runat="server" ControlToValidate="ddlSponsor"
                                        Display="Dynamic" ErrorMessage="Incorrect Sponsor ID" ValidationExpression="[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
                                        ValidationGroup="Proceed" Width="119px"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator
                                        ID="rfvEmpSponsorID" runat="server" ControlToValidate="ddlSponsor"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="Proceed" Width="109px">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div id="divEmpHijriYearOfBirth" runat="server" class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label19"
                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Hijri Year of Birth"></asp:Label>
                                    <span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtEmpHijriYearofBirth" runat="server" MaxLength="4"
                                        BackColor="#eff6fc"></asp:TextBox>
                                    <font color="black"></font>
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvEmpHijriYearOfBirth" runat="server"
                                        ControlToValidate="txtEmpHijriYearofBirth" Display="Dynamic"
                                        ErrorMessage="Field is mandatory" ValidationGroup="Proceed" Width="119px" />
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <asp:Label ID="lblYaqeenError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        <br />

                        <br />
                        <div style="text-align: left">
                            <asp:Button ID="btnProceedEmployee" runat="server" Text="Proceed" CssClass="submitButton" ValidationGroup="Proceed" OnClick="btnProceedEmployee_Click" />
                        </div>
                        <asp:Panel ID="pnlEmpDetails" runat="server" Visible="false">
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>
                                        <span style="color: #ff3300">*</span>
                                        <asp:Label ID="Label28" runat="server" Font-Bold="False"
                                            ForeColor="#000036" Text="ID Expiry Date"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span>
                                </div>
                                <div>
                                    <BUPA:ucHijriGregorian runat="server" ID="txtEIDExpDate" Enabled="true"
                                        ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                                </div>
                                <div class="clear">
                                    <asp:HiddenField ID="hidYakeenEmployee" runat="server" />
                                </div>
                            </div>

                            <div class="containerClass">
                                <div class="labelClass">
                                    <strong><span style="color: #ff3300"></span></strong>
                                    <span style="color: #ff3300">*</span>
                                    <asp:Label ID="Label29" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Marital Status"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEMaritalStatus"
                                            ClientIDMode="Static" runat="server"
                                            BackColor="#eff6fc">
                                            <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                            <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ControlToValidate="ddlEMaritalStatus"
                                        InitialValue="0" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1"
                                        Width="115px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label6" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Nationality"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlENationality" Font-Size="9" runat="server" DataTextField="text" DataValueField="value" BackColor="#eff6fc">
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" InitialValue="0"
                                        ControlToValidate="ddlENationality" Display="Dynamic" ErrorMessage="RequiredFieldValidator"
                                        ValidationGroup="1" Width="114px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label1" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Title"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlETitle"
                                            ClientIDMode="Static" runat="server"
                                            onchange="ValidateTitleGender(document.getElementById('ddlEGender').value, document.getElementById('ddlETitle').value, 'Emp');"
                                            DataTextField="text" DataValueField="value" BackColor="#eff6fc">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem>Mr</asp:ListItem>
                                            <asp:ListItem>Miss</asp:ListItem>
                                            <asp:ListItem>Mrs</asp:ListItem>
                                            <asp:ListItem>Ms</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="ddlETitle"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" InitialValue="0" Width="115px"
                                        SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label4" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Gender" Width="46px"></asp:Label><span
                                            style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ClientIDMode="Static" ID="ddlEGender" runat="server" onchange="ValidateTitleGender(document.getElementById('ddlEGender').value, document.getElementById('ddlETitle').value, 'Emp');"
                                            BackColor="#eff6fc">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="M">Male</asp:ListItem>
                                            <asp:ListItem Value="F">Female</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlEGender"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                                        Width="109px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="containerClass">
                                <div class="labelClass">
                                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label2" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Complete Name (First, Middle, Last)"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:TextBox CssClass="textbox" ID="txtEMemberName" ClientIDMode="Static" runat="server" MaxLength="40" ValidationGroup="1"
                                            BackColor="#eff6fc"></asp:TextBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtEMemberName"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" ValidationGroup="1"
                                        Width="125px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtEMemberName"
                                        Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                                        Font-Bold="False" ValidationExpression="[a-zA-Z ]+" ValidationGroup="1" SetFocusOnError="true"></asp:RegularExpressionValidator>

                                </div>
                                <div class="clear"></div>
                                <asp:Literal ID="litErrorEmployeeExceed40" runat="server" Text="" Visible="false"></asp:Literal>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*<asp:Label ID="Label5" runat="server" Font-Bold="False"
                                        ForeColor="#000036" Text="Date Of Birth"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span>
                                </div>
                                <div>
                                    <BUPA:ucHijriGregorian runat="server" ID="txtEDateOfBirth" RequiredField="True"
                                        Enabled="true" ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                                        <span style="color: #ff3300">*</span>
                                        <asp:Label ID="Label25" runat="server" Font-Bold="False" ForeColor="#000036" Text="Profession" Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <table>
                                            <tr>
                                                <td>
                                                    <telerik:RadComboBox ID="ddProfession" runat="server" Height="280" Width="265"
                                            Filter="Contains" MarkFirstMatch="true" ChangeTextOnKeyBoardNavigation="false"
                                            DataTextField="profName" DataValueField="profCode" EmptyMessage="-- Select --" BackColor="#eff6fc">
                                        </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox Text="Not available in NIC"  CssClass="textbox" style="border: none" ReadOnly="true" ID="txtProfessionFromYakeen" Visible="false" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="ddProfession"
                                        Display="Dynamic" ErrorMessage="Field is mandatory" ValidationGroup="1"
                                        Width="107px" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                                        <span style="color: #ff3300">*</span>
                                        <asp:Label ID="Label24" runat="server" Font-Bold="False" ForeColor="#000036" Text="District"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList ID="ddDistrict" runat="server" BackColor="#eff6fc" DataValueField="distCode" DataTextField="distName">
                                            <asp:ListItem Value=" "></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="ddDistrict"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="1"
                                        Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"></span><span style="color: #ff3300">
                                        <span style="color: #ff3300">*</span>
                                        <asp:Label ID="Label16" runat="server" Font-Bold="False" ForeColor="#000036" Text="Mobile No"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:TextBox CssClass="textbox" ID="txtEMobileNo" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox>
                                    </div>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                        ControlToValidate="txtEMobileNo" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                                        ValidationExpression="[0][5][0-9]{8}" ValidationGroup="1"
                                        Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ControlToValidate="txtEMobileNo"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="1"
                                        Width="120px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><span style="color: #ff3300"><strong></strong></span><span
                                        style="color: #ff3300"><strong></strong></span><asp:Label ID="Label11" runat="server"
                                            Font-Bold="False" ForeColor="#000036" Text="Branch Code"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEBranchCode" runat="server" BackColor="#eff6fc">
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvddlEBranchCode" runat="server" ControlToValidate="ddlEBranchCode"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                                        Width="120px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">*<asp:Label
                                        ID="Label13" runat="server" Font-Bold="False" ForeColor="#000036" Text="Joining date with company"
                                        Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div>
                                    <BUPA:ucHijriGregorian runat="server" ID="txtEJoiningDate" RequiredField="True"
                                        Enabled="true" ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong style="vertical-align: text-top">
                                        <span style="vertical-align: top; color: #ff3300"><strong></strong></span>*<asp:Label
                                            ID="Label14" runat="server" Font-Bold="False" ForeColor="#000036" Text="Start date for medical cover"
                                            Width="158px"></asp:Label></strong></span>
                                </div>
                                <div>
                                    <BUPA:ucHijriGregorian runat="server" ID="txtERequestedStartDate" RequiredField="True"
                                        Enabled="true" ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300">
                                        <asp:Label ID="empind" runat="server" Text="*" Visible="false"></asp:Label></span><asp:Label ID="Label9" runat="server"
                                            Font-Bold="False" ForeColor="#000036" Text="Employee No"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtEEmployeeNo" runat="server" MaxLength="15" BackColor="#eff6fc"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpNo" runat="server" ControlToValidate="txtEEmployeeNo"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="112px"
                                        SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <asp:Label ID="Label12" runat="server" Font-Bold="False" ForeColor="#000036" Text="Department Code"
                                        Width="105px"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtEDepartmentCode" runat="server" BackColor="White" MaxLength="10"
                                        Rows="10"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeptCode" runat="server" ControlToValidate="txtEDepartmentCode"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                                        Width="120px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <asp:CheckBox ID="chkPreviousMember" runat="server" onclick="if(this.checked == true) document.getElementById('RFVldtrPreviousMember').enabled  = true; else  document.getElementById('RFVldtrPreviousMember').enabled  = false;"
                                    Text="Has the member or any of its dependents previously been covered by BUPA ?" />
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <asp:Label ID="Label17" runat="server" Font-Bold="False" ForeColor="#000036" Text="Previous Membership No."></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtPreviousMember" runat="server" MaxLength="8" ValidationGroup="1"
                                        BackColor="White"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="RFVldtrPreviousMember"
                                        runat="server" ControlToValidate="txtPreviousMember" Display="Dynamic" Enabled="false"
                                        ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="115px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPreviousMember"
                                            Display="Dynamic" ValidationExpression="^[1-9][0-9]{6}(?:[0-9]{1})?$" SetFocusOnError="true"
                                            ErrorMessage="RegularExpressionValidator" ValidationGroup="1" Width="149px">Incorrect Membership No.</asp:RegularExpressionValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <asp:Label ID="Label15" runat="server" Font-Bold="False" ForeColor="#000036" Text="Join reason"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEJoinReason"
                                            runat="server" Font-Names="Arial" Font-Size="Small">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="RJOIN001">Member is studying outside Kingdom</asp:ListItem>
                                            <asp:ListItem Value="RJOIN002">Dependent is studying outside Kingdom</asp:ListItem>
                                            <asp:ListItem Value="RJOIN003">Customer forgot to add member from contract start date</asp:ListItem>
                                            <asp:ListItem Value="RJOIN004">Customer forgot to add dependent from contract start date</asp:ListItem>
                                            <asp:ListItem Value="RJOIN005">Main member / father forgot to add dependent from contract start date</asp:ListItem>
                                            <asp:ListItem Value="RJOIN006">End of probationary period</asp:ListItem>
                                            <asp:ListItem Value="RJOIN007">Employee is ill and needs treatment</asp:ListItem>
                                            <asp:ListItem Value="RJOIN008">Dependent is ill and needs treatment</asp:ListItem>
                                            <asp:ListItem Value="RJOIN009">Member was on long vacation</asp:ListItem>
                                            <asp:ListItem Value="RJOIN010">Member promoted to higher grade which is entitled to add dependent/s</asp:ListItem>
                                            <asp:ListItem Value="RJOIN011">Newly Hired Employee</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RFVldtrEJoinReason" InitialValue="0" runat="server" SetFocusOnError="true"
                                        ControlToValidate="ddlEJoinReason" ErrorMessage="RequiredFieldValidator" Font-Names="Verdana"
                                        Enabled="false" ValidationGroup="1" Display="Dynamic" Font-Size="Small" Text="Join Reason is mandatory"
                                        Width="176px"></asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/xmlfiles/Title.xml"></asp:XmlDataSource>
                            <asp:XmlDataSource ID="XmlDataSource2" runat="server" DataFile="~/xmlfiles/Nationality.xml"></asp:XmlDataSource>
                            </span>
                        </asp:Panel>
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="PanelMembershipNoOnly" runat="server" Visible="false">
                        <table style="font-size: small; width: 396px;">
                            <tr>
                                <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="2">
                                    <strong><span style="font-size: 11pt; color: #0099ff">Employee Details</span></strong>
                                </td>
                                <td style="background-color: #ffffff; height: 25px;" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 122px" valign="top">
                                    <nobr>
                                        <strong>
                                            <span style="COLOR: #ff3300">*</span>
                                        </strong>
                                        <asp:Label id="lblMainMemberNo" runat="server" Text="Membership Number" Width="128px"></asp:Label>
                                    </nobr>
                                </td>
                                <td style="width: 288px; margin-left: 40px;" valign="top">
                                    <asp:TextBox CssClass="textbox" ID="txtMainMemberNo" runat="server" Width="164px" MaxLength="8" ValidationGroup="1"
                                        BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMainMemberNo"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="250px" ValidationGroup="1"
                                        SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 288px" valign="top">
                                    <asp:Button CssClass="submitButton" ID="btnGetMemberDetails" runat="server" OnClick="btnGetMemberDetails_Click"
                                        Text="Get Details" />
                                </td>
                            </tr>
                            <%--<tr>
                                <td style="width: 122px;" valign="top">
                                    <div>
                                        <span style="color: #ff3300"><strong>*
                                        <asp:Label ID="Label37" runat="server" Font-Bold="False" ForeColor="#000036" Text="Dependent Hijri Year of Birth"></asp:Label>
                                        </strong></span>
                                    </div>
                                </td>
                                <td style="width: 288px; margin-left: 40px;" valign="top">
                                    <asp:TextBox ID="txtEmpHijriYearofBirth0" runat="server" BackColor="#eff6fc" CssClass="textbox" MaxLength="4"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEmpHijriYearOfBirth0" runat="server" ControlToValidate="txtEmpHijriYearofBirth0" Display="Dynamic" ErrorMessage="Field is mandatory" ValidationGroup="Proceed" Width="119px" />
                                </td>
                            </tr>--%>
                            <tr>
                                <td colspan="3" style="background-color: #ffffff; color: red; height: 25px; font-size: small; font-weight: bold;" valign="top">
                                    <div id="divEmpHijriYearOfBirth0" runat="server" class="containerClass">
                                        <div class="controlClass">
                                            <br />
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
                        </telerik:RadWindowManager>
                    <asp:Literal ID="litDepMainError" runat="server"></asp:Literal>
                    <asp:GridView ID="gvDependents" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        Font-Names="Arial" OnRowDeleting="gvDependents_RowDeleting" Font-Size="11px" ForeColor="#333333"
                        GridLines="None" OnRowCommand="gvDependents_RowCommand" OnRowDataBound="gvDependents_RowDataBound"
                        OnSelectedIndexChanged="gvDependents_SelectedIndexChanged" Width="635px">
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:BoundField DataField="title" HeaderText="Title">
                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CompleteName" HeaderText="Complete Name">
                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RelationShip" HeaderText="Relation">
                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DOB" DataFormatString="{0:d MMM yyyy }" HeaderText="Date of Birth"
                                HtmlEncode="False">
                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LevelCover" HeaderText="Level of Cover">
                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RequestStartDate" HeaderText="Start Date">
                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SponsorID" HeaderText="Sponsor ID">
                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="EmployeeNo" HeaderText="Employee No">
                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                            </asp:BoundField>
                            <asp:CommandField ShowSelectButton="True" SelectText="Edit">
                                <HeaderStyle BackColor="White" BorderColor="White" />
                                <ItemStyle Width="30px" HorizontalAlign="Center" BackColor="White" BorderColor="White" />
                            </asp:CommandField>
                            <asp:CommandField HeaderText="" ShowDeleteButton="True">
                                <HeaderStyle BackColor="White" BorderColor="White" />
                                <ItemStyle Width="30px" HorizontalAlign="Center" BackColor="White" BorderColor="White" />
                            </asp:CommandField>
                        </Columns>
                        <RowStyle BackColor="White" />
                        <EditRowStyle BackColor="#7C6F57" />
                        <SelectedRowStyle BackColor="#4CAC27" BorderStyle="None" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#E6F5FF" Font-Bold="True" ForeColor="#404040" Font-Names="Arial"
                            Font-Size="9pt" Font-Strikeout="False" />
                    </asp:GridView>
                    <br />
                     <asp:LinkButton ID="lnkBtnAddNewDep" runat="server" Style="border-width: thin; padding: 2px; font-size: large; text-decoration-style: none; border-style: solid; cursor: pointer; border-color: #d3d3d3; font-weight: bold;"
                            OnClick="lnkBtnAddNewDep_Click" Visible="false" CausesValidation="false"> + Add Dependent</asp:LinkButton>
                        <br />
                    <asp:Panel ID="pnlDepemdentMain" runat="server" Visible="false">


                       
                        <%--<asp:Label ID="LabelNotBupa" Text="Below dependents are not covered under Bupa Insurance" Font-Bold="true" runat="server" />
                        <telerik:RadGrid ID="RadGridDependenetsNotCoveredUnderBupa" runat="server"></telerik:RadGrid>
                        <br />
                        <asp:Label ID="LabelBupa" Text="Below dependents are already covered under Bupa Insurance" Font-Bold="true" runat="server" />
                        <telerik:RadGrid ID="RadGridDependenetsCoveredUnderBupa" OnItemDataBound="RadGridDependenetsCoveredUnderBupa_ItemDataBound" runat="server"></telerik:RadGrid>
                        <br />
                        <asp:Label ID="LabelYakeen" Text="Below is total list of dependents in Yakeen" Font-Bold="true" runat="server" />
                        <telerik:RadGrid ID="RadGridTotalDependentsInYakeen" runat="server"></telerik:RadGrid>
--%>

                        <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial" runat="server" id="dependentTab">
                            <tr>
                                <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="7" align="center">
                                    <asp:Label ID="lbler1" runat="server" Text="" Font-Bold="true" ForeColor="Red" Font-Size="Large"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="background-color: #ffffff; height: 25px;" valign="top">
                                    <span style="font-size: 11pt; color: #0099ff"><strong>Dependent Details</strong></span>
                                </td>
                            </tr>
                        </table>
                        <div class="containerClass">
                            <div class="labelClass">
                                <span style="color: #ff3300">
                                    <strong>*</strong>
                                </span>
                                <asp:Label ID="lblLevelOfCover" runat="server" Font-Bold="False" ForeColor="#000036" Text="Level of cover"></asp:Label>
                                <span style="color: #ff3300">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="controlClass">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlLevelOfCover" runat="server" BackColor="#eff6fc" OnSelectedIndexChanged="ddlLevelOfCover_SelectedIndexChanged" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlLevelOfCover"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                                    Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="containerClass">
                            <div class="labelClass">
                                <span style="color: #ff3300">
                                    <strong>*</strong></span>
                                <asp:Label ID="Label30" runat="server"
                                    Font-Bold="False" ForeColor="#000036" Text="Member Type"></asp:Label>
                            </div>
                            <div class="controlClass">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlMemberType"
                                        runat="server" DataTextField="mbrTypeDesc" DataValueField="mbrType"
                                        BackColor="#eff6fc">
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ControlToValidate="ddlMemberType"
                                    InitialValue="0" SetFocusOnError="true"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="divAgeGroup" class="containerClass" runat="server" style="display: none;">
                            <div class="labelClass">
                                <span style="color: #ff3300">
                                    <strong>*</strong>
                                </span>
                                <asp:Label ID="Label3" runat="server" Font-Bold="False" ForeColor="#000036" Text="Age group"></asp:Label>
                                <span style="color: #ff3300">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="controlClass">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlAgeGroup" runat="server" BackColor="#eff6fc">
                                        <asp:ListItem Value="0">-Select-</asp:ListItem>
                                        <asp:ListItem Value="1">Less/Equal to 1 Year</asp:ListItem>
                                        <asp:ListItem Value="2">Above 1 Year</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="rfv_ddlAgeGroup" runat="server" ControlToValidate="ddlAgeGroup"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                                    Width="107px" SetFocusOnError="true" Enabled="false">Field is mandatory</asp:RequiredFieldValidator>
                                
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="entryForm2">
                            <div class="containerClass">
                                <div class="labelClass">
                                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label31" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="ID Type"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlIDType" runat="server"
                                            BackColor="#eff6fc">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="ddlIDType"
                                        InitialValue="0" SetFocusOnError="true"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div id="divDepID" runat="server" class="containerClass">
                                <div class="labelClass">
                                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="lblSaudiID"
                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Saudi ID/ Iqama ID/ Border Entry"
                                        Width="185px"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtDependentID" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><asp:Label
                                        ID="lblSaudiNote" runat="server" Font-Bold="False" ForeColor="Black" Text=" (Must be different than the employee's Saudi Id/Iqama Id)"></asp:Label>
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfv_ID" runat="server" ControlToValidate="txtDependentID"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Enabled="true"
                                        Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator
                                        ID="revDepSuadi" runat="server" ControlToValidate="txtDependentID"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[1][0-9]{9}"
                                        ValidationGroup="2" Width="142px">Incorrect Saudi ID</asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator
                                        ID="revDepIqama" runat="server" ControlToValidate="txtDependentID"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[2][0-9]{9}"
                                        ValidationGroup="2" Width="142px">Incorrect Iqama ID</asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator
                                        ID="revDepBorder" runat="server" ControlToValidate="txtDependentID"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[3-5][0-9]{9}"
                                        ValidationGroup="2" Width="142px">Incorrect Border Entry ID</asp:RegularExpressionValidator>
                                    <asp:Label ID="lblIDVal" ForeColor="Red" runat="server" Text="This Field is mandatory" Visible="false"></asp:Label>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div id="divDepSponsorID" runat="server" class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblSponsorId"
                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Sponsor Id"></asp:Label>
                                    <span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <%--<telerik:RadComboBox ID="ddlDepSponsor" runat="server" Width="200px" Height="150px"
                                        EmptyMessage="Select/start typing" DataTextField="sponsorID"
                                        OnItemDataBound="ddlDepSponsor_ItemDataBound">
                                    </telerik:RadComboBox>--%>
                                    <asp:TextBox CssClass="textbox" ID="txtSponsorId" runat="server" MaxLength="10"
                                        BackColor="#eff6fc"></asp:TextBox>
                                    <font color="black">Sponsor ID has to be the main member’s Saudi ID/Iqama number.</font><br />
                                    <asp:RegularExpressionValidator
                                        ID="revDepSponsorID" runat="server" ControlToValidate="txtSponsorId"
                                        Display="Dynamic" ErrorMessage="Incorrect Sponsor ID" ValidationExpression="[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
                                        ValidationGroup="2" Width="129px"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator
                                        ID="rfvDepSponsorID" runat="server" ControlToValidate="txtSponsorId"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="119px">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div id="divDepHijriYearOfBirth" runat="server" class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblHijri"
                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Hijri Year of Birth"></asp:Label>
                                    <span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtDepHijriYearofBirth" runat="server" MaxLength="4"
                                        BackColor="#eff6fc"></asp:TextBox>
                                    <font color="black"></font>
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvDepHijriYearOfBirth" runat="server"
                                        ControlToValidate="txtDepHijriYearofBirth" Display="Dynamic"
                                        ErrorMessage="Field is mandatory" ValidationGroup="2" Width="119px" />
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <asp:Label ID="lblYaqeenDepError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        <br />
                        <br />
                        <div style="text-align: left">
                            <asp:Button ID="btnProceedDep" runat="server" CssClass="submitButton" Text="Proceed" ValidationGroup="2" OnClick="btnProceedDep_Click" />
                        </div>
                        <asp:Panel ID="pnlDependentDetail" runat="server" Visible="false">
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblNationality"
                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Nationality"></asp:Label><span
                                            style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlNationality" runat="server" BackColor="#eff6fc" DataTextField="text" DataValueField="value">
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlNationality"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="111px" InitialValue="0"
                                        SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear">
                                    <asp:HiddenField ID="hidYakeenDependent" runat="server" />
                                </div>
                            </div>
                            <div id="divIDExpiryDate" runat="server" class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>
                                        <span style="color: #ff3300">*</span>
                                        <asp:Label ID="Label32" runat="server" Font-Bold="False"
                                            ForeColor="#000036" Text="ID Expiry Date"></asp:Label></strong><span style="color: #ff3300">
                                                <strong></strong></span></span>
                                </div>
                                <div>
                                    <BUPA:ucHijriGregorian runat="server" ID="txtIDExpDate" Enabled="true"
                                        ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                                    <asp:Label ID="lblIDExpiryVal" ForeColor="Red" runat="server" Text="Field is mandatory" Visible="false"></asp:Label>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <strong><span style="color: #ff3300"></span></strong>
                                    <span style="color: #ff3300">*</span>
                                    <asp:Label ID="Label33" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Marital Status"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlMaritalStatus"
                                            ClientIDMode="Static" runat="server" BackColor="#eff6fc">
                                            <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                            <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ControlToValidate="ddlMaritalStatus" InitialValue="0"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="115px"
                                        SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblTitle" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Title"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlTitle" runat="server" BackColor="#eff6fc"
                                            onchange="ValidateTitleGender(document.getElementById('ddlGender').value, document.getElementById('ddlTitle').value, 'Dep');"
                                            DataTextField="text" DataValueField="value">
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTitle"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="110px"
                                        SetFocusOnError="true" InitialValue="0">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong>*</strong><asp:Label ID="lblGender" runat="server"
                                        Font-Bold="False" ForeColor="#000036" Text="Gender" Width="46px"></asp:Label><span
                                            style="color: #ff3300"><strong></strong></span></span>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlGender" runat="server" BackColor="#eff6fc"
                                            onchange="ValidateTitleGender(document.getElementById('ddlGender').value, document.getElementById('ddlTitle').value, 'Dep');">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="M">Male</asp:ListItem>
                                            <asp:ListItem Value="F">Female</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlGender"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2" SetFocusOnError="true"
                                        Width="109px">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="lblCompleteName"
                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Complete Name (First, Middle, Last)"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtMemberName" runat="server" MaxLength="40" BackColor="#eff6fc"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMemberName"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" ValidationGroup="2"
                                        Width="158px">Field is mandatory</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtMemberName"
                                        Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                                        Font-Bold="False" ValidationExpression="[a-zA-Z ]+" ValidationGroup="2"></asp:RegularExpressionValidator>
                                </div>
                                <div class="clear"></div>
                                <asp:Literal ID="litErrorDepExceed40" runat="server" Text="" Visible="false"></asp:Literal>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><span style="color: #ff3300"><strong>*</strong><asp:Label
                                        ID="lblDateOfBirth" runat="server" Font-Bold="False" ForeColor="#000036" Text="Date Of Birth"></asp:Label><span
                                            style="color: #ff3300"><strong></strong></span></span></span>
                                </div>
                                <div>
                                    <BUPA:ucHijriGregorian runat="server" ID="txtDateOfBirth" Enabled="true"
                                        ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong></strong></span><strong><span style="color: #ff3300">
                                        <span style="color: #ff3300">*</span>
                                        <asp:Label ID="Label35" runat="server" Font-Bold="False" ForeColor="#000036" Text="Profession" Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList ID="ddDepProfession" runat="server" BackColor="#eff6fc">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="99402">Son/Daughter</asp:ListItem>
                                            <asp:ListItem Value="99401">Husband/Wife</asp:ListItem>
                                            <asp:ListItem Value="99403">Father/Mother</asp:ListItem>
                                            <asp:ListItem Value="99404">Brother/Sister</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ControlToValidate="ddDepProfession"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                                        Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                                        <span style="color: #ff3300">*</span>
                                        <asp:Label ID="Label34" runat="server" Font-Bold="False" ForeColor="#000036" Text="District"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList ID="ddDepDistrict" runat="server" BackColor="#eff6fc" DataValueField="distCode" DataTextField="distName">
                                            <asp:ListItem Value=" "></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ControlToValidate="ddDepDistrict"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="2"
                                        Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong></strong></span>
                                    <span style="color: #ff3300">*</span>
                                    <asp:Label ID="lblMobileNo" runat="server" Font-Bold="False" ForeColor="#000036"
                                        Text="Mobile No"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtMobileNo" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><br />
                                    <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobileNo"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[0][5][0-9]{8}"
                                        ValidationGroup="2" Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ControlToValidate="txtMobileNo"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="2"
                                        Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">*<asp:Label
                                        ID="lblJoinDate" runat="server" Font-Bold="False" ForeColor="#000036" Text="Arrival date in KSA"></asp:Label></span></strong>
                                </div>
                                <div>
                                    <BUPA:ucHijriGregorian runat="server" ID="txtJoiningDate" Enabled="true" RequiredField="True"
                                        ValidationGroup="2" ErrorMsgForRequiredField="Please fill in the Date" />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong style="vertical-align: text-top">
                                        <span style="vertical-align: top; color: #ff3300"><strong></strong></span>*<asp:Label
                                            ID="lblRequestedStartDate" runat="server" Font-Bold="False" ForeColor="#000036"
                                            Text="Start date for medical cover" Width="160px"></asp:Label></strong></span>
                                </div>
                                <div>
                                    <BUPA:ucHijriGregorian runat="server" ID="txtRequestedStartDate" Enabled="true" RequiredField="True"
                                        ValidationGroup="2" ErrorMsgForRequiredField="Please fill in the Date" />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <span style="color: #ff3300">
                                        <asp:Label ID="empind1" runat="server" Text="*" Visible="false"></asp:Label></span><asp:Label ID="lblEmployeeNo"
                                            runat="server" Font-Bold="False" ForeColor="#000036" Text="Employee No" Width="83px"></asp:Label><span
                                                style="color: #ff3300"><strong></strong></span>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtEmployeeNo" runat="server" MaxLength="15" BackColor="#eff6fc"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidatorEmpNoDep" runat="server" ControlToValidate="txtEmployeeNo"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="112px"
                                        SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <asp:Label ID="lblDepartmentCode" runat="server" Font-Bold="False" ForeColor="#000036"
                                        Text="Department Code" Width="105px"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <asp:TextBox CssClass="textbox" ID="txtDepartmentCode" runat="server" BackColor="White" MaxLength="10"
                                        Rows="10"></asp:TextBox>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="containerClass">
                                <div class="labelClass">
                                    <asp:Label ID="lblJoinReason" runat="server" Font-Bold="False" ForeColor="#000036"
                                        Text="Join reason" Width="97px"></asp:Label>
                                </div>
                                <div class="controlClass">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlJoinReason" runat="server" Font-Names="Arial" Font-Size="Small">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="RJOIN001">Member is studying outside Kingdom</asp:ListItem>
                                            <asp:ListItem Value="RJOIN002">Dependent is studying outside Kingdom</asp:ListItem>
                                            <asp:ListItem Value="RJOIN003">Customer forgot to add member from contract start date</asp:ListItem>
                                            <asp:ListItem Value="RJOIN004">Customer forgot to add dependent from contract start date</asp:ListItem>
                                            <asp:ListItem Value="RJOIN005">Main member / father forgot to add dependent from contract start date</asp:ListItem>
                                            <asp:ListItem Value="RJOIN006">End of probationary period</asp:ListItem>
                                            <asp:ListItem Value="RJOIN007">Employee is ill and needs treatment</asp:ListItem>
                                            <asp:ListItem Value="RJOIN008">Dependent is ill and needs treatment</asp:ListItem>
                                            <asp:ListItem Value="RJOIN009">Member was on long vacation</asp:ListItem>
                                            <asp:ListItem Value="RJOIN010">Member promoted to higher grade which is entitled to add dependent/s</asp:ListItem>
                                            <asp:ListItem Value="RJOIN012">New Born baby</asp:ListItem>
                                            <asp:ListItem Value="RJOIN013">Newly Married</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RFVldtrJoinReason" InitialValue="0" runat="server"
                                        ControlToValidate="ddlJoinReason" ErrorMessage="RequiredFieldValidator" Font-Names="Verdana"
                                        Enabled="false" ValidationGroup="2" Display="Dynamic" Font-Size="Small" Text="Join Reason is mandatory"
                                        Width="176px" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </asp:Panel>
                        <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial" runat="server" id="Table4" visible="true">
                            <tr>
                                <td style="width: 120px" valign="top"></td>
                                <td bgcolor="white" style="width: 10px" valign="top"></td>
                                <td style="width: 185px" valign="top"></td>
                                <td bgcolor="white" style="width: 10px" valign="top"></td>
                                <td colspan="3" style="text-align: right" valign="top">
                                    <asp:Button CssClass="submitButton" ID="btnUpdateDep" runat="server" Text="Update Dependent" Visible="False"
                                        OnClick="btnUpdateDep_Click" BackColor="Control" ValidationGroup="2" />
                                    <asp:Button CssClass="submitButton" ID="btnCancelUpdateDep"
                                        runat="server" OnClick="btnCancelUpdateDep_Click" Text="Cancel" Visible="False" BackColor="Control" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 120px" valign="top">
                                    <asp:TextBox CssClass="textbox" ID="txtFlagForAddingDepValidation" runat="server" Width="0px" ValidationGroup="1"
                                        BackColor="Transparent" BorderColor="Transparent" Height="0px" Text="0"></asp:TextBox>
                                </td>
                                <td bgcolor="white" style="width: 10px" valign="top"></td>
                                <td style="width: 185px" valign="top"></td>
                                <td bgcolor="white" style="width: 10px" valign="top"></td>
                                <td colspan="3" style="text-align: right" valign="top">
                                    <asp:Button CssClass="submitButton" ID="btnAddDep" runat="server" OnClick="btnAddDep_Click"
                                        Text="Add Dependent" ValidationGroup="2" Visible="False" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" valign="top">
                                    <asp:RangeValidator ID="RangeValidatorForDep" runat="server" ControlToValidate="txtFlagForAddingDepValidation"
                                        Enabled="False" ErrorMessage="RangeValidator" MaximumValue="19" MinimumValue="1"
                                        ValidationGroup="1" Width="395px" Font-Bold="True">At least one dependent must be added.</asp:RangeValidator>
                                </td>
                            </tr>
                        </table>
                        <asp:XmlDataSource ID="XmlDataSourceTitle" runat="server" DataFile="~/xmlfiles/Title.xml"></asp:XmlDataSource>
                        <asp:XmlDataSource ID="XmlDataSourceNationality" runat="server" DataFile="~/xmlfiles/Nationality.xml"></asp:XmlDataSource>
                    </asp:Panel>
                    <br />
                </ContentTemplate>

            </asp:UpdatePanel>
        </div>
        <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
            runat="server" id="tblUploader">
            <tr>
                <td align="left">
                    <span style="color: #ff3300"><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                </td>
                <td align="right">
                    <a href="<%= Page.ResolveUrl("~/Docs/ReqDocs2012.pdf") %>" target="_blank">Required Document List</a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .bmp) 5MB limit per file" /><br />
                    <BUPA:uploader runat="server" ID="uploader" />
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
        </table>
        <br />
        <asp:Panel ID="PanelDeclaration" runat="server" Visible="false" Width="716px">
            <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
                id="TABLE1">
                <tr valign="top">
                    <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="5">
                        <span style="font-size: 11pt; color: #0099ff"><strong>Declaration Form For Insured Persons</strong></span>
                    </td>
                </tr>
                <tr valign="top">
                    <td align="right" valign="top">
                        <span style="font-size: 11pt; color: #0099ff">For Employee :<br />
                            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Dependent: </span>
                    </td>
                    <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="4">
                        <span style="font-size: 11pt; color: #0099ff">
                            <asp:Label ID="lblEmployeeName" runat="server" Width="384px"></asp:Label>
                            <br />
                            <asp:Label ID="lblAllDependentsName" runat="server" Width="100%"></asp:Label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="white" colspan="3" style="padding-left: 5px; font-weight: bold; font-size: 10pt; vertical-align: middle; color: white; background-color: #0099ff; text-align: left"></td>
                    <td bgcolor="white" colspan="2" style="padding-right: 5px; font-weight: bold; font-size: 11pt; vertical-align: top; color: white; direction: rtl; font-family: Arial; background-color: #0099ff; text-align: right"
                        width="155"></td>
                </tr>
                <tr>
                    <td colspan="3" style="vertical-align: middle; color: white; background-color: #ffffff; text-align: left; padding-left: 5px; font-weight: bold; font-size: 10pt;">
                        <span style="color: black">Please tick yes or no on each of these&nbsp; questions. If
                            yes please provide number of cases and names of patients</span>
                    </td>
                    <td colspan="2" style="direction: rtl; font-weight: bold; font-size: 11pt; font-family: Arial; vertical-align: top; color: white; background-color: #ffffff; text-align: right; padding-right: 5px;"
                        width="255">
                        <span style="color: black">يرجى وضع علامة تحت نعم أو لا مقابل الأسئلة التالية إذا كانت
                            الإجابة نعم يرجى تقديم عدد الحالات و أسماء المرضى</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="font-weight: bold; font-size: 9pt; font-family: Arial; text-align: left; color: #0099ff; padding-left: 5px;">Does any of your group / family members suffer from any of the below conditions
                        or disease:
                    </td>
                    <td style="width: 70px"></td>
                    <td colspan="2" style="text-align: right; font-weight: bold; font-size: 9pt; font-family: Arial; vertical-align: middle; color: #0099ff; direction: rtl; padding-right: 5px;"
                        width="155">هل يعاني أحد موظفيك / أفراد عائلتك من أي من الأمراض أو الحالات التالية:
                    </td>
                </tr>
                <tr>
                    <td style="width: 230px; border-top: #0099ff thin solid"></td>
                    <td style="font-weight: bold; font-size: 9pt; font-family: Arial; text-align: center; color: #0099ff; border-top: #0099ff thin solid; width: 70px;">Yes/No<br />
                        <br />
                        (لا / نعم)
                    </td>
                    <td style="font-weight: bold; font-size: 9pt; font-family: Arial; text-align: center; color: #0099ff; border-top: #0099ff thin solid; width: 70px;">Number of Cases
                        <br />
                        ( عدد الحالات )
                    </td>
                    <td style="font-weight: bold; font-size: 9pt; font-family: Arial; text-align: center; color: #0099ff; border-top: #0099ff thin solid; width: 70px;">Name/s<br />
                        <br />
                        (الاسماء)
                    </td>
                    <td style="direction: ltr; text-align: right; border-top: #0099ff thin solid; width: 225px;"></td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">1.	Future plans for (Artificial / Natural) organ transplants.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ1" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv1,rfv2,'txtQ1Box1','txtQ1Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv1,rfv2,'txtQ1Box1','txtQ1Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq1" runat="server" ControlToValidate="rdbQ1" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ1Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtQ1Box1"
                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="1" Enabled="False"
                            ToolTip="Field Required" Font-Bold="True" Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv1" runat="server" ControlToValidate="txtQ1Box1" Display="Dynamic" ClientIDMode="Static"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Type="Integer" ValidationGroup="1" CultureInvariantValues="True" Enabled="False">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ1Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator
                            ID="rfv2" runat="server" ControlToValidate="txtQ1Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium"
                            ToolTip="Field Required" Width="10px" Enabled="False">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">1-	خطط مستقبلية لزراعة الأعضاء (الطبيعية أو الصناعية)
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">2.	Admission case currently in the hospital or any hospital admission within the last 14 days or receiving treatment in emergency.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ2" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv3,rfv4,'txtQ2Box1','txtQ2Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv3,rfv4,'txtQ2Box1','txtQ2Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq2" runat="server" ControlToValidate="rdbQ2" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ2Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv3" runat="server" ControlToValidate="txtQ2Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator><asp:RangeValidator ID="rv2" runat="server"
                                ControlToValidate="txtQ2Box1" Display="Dynamic" ErrorMessage="RangeValidator"
                                MaximumValue="99" MinimumValue="1" Type="Integer" Enabled="False">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ2Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv4" runat="server" ControlToValidate="txtQ2Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">2-	حالات منومة في المستشفى أو الطواريء حاليا أو تم تنويمها خلال الـ 14 يوم السابقة
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">3.	Central nervous system diseases limited to: Stroke, Epilepsy.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ3" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv5,rfv6,'txtQ3Box1','txtQ3Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv5,rfv6,'txtQ3Box1','txtQ3Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq3" runat="server" ControlToValidate="rdbQ3" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ3Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv5" runat="server" ControlToValidate="txtQ3Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv3" runat="server" ControlToValidate="txtQ3Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ3Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv6" runat="server" ControlToValidate="txtQ3Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">3-	أمراض الجهاز العصبي التالية: التشنجات العصبية (الصرع)، الجلطة الدماغية
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">4.	Tumor or Cancer.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ4" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv7,rfv8,'txtQ4Box1','txtQ4Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv7,rfv8,'txtQ4Box1','txtQ4Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq4" runat="server" ControlToValidate="rdbQ4" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ4Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv7" runat="server" ControlToValidate="txtQ4Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv4" runat="server" ControlToValidate="txtQ4Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ4Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv8" runat="server" ControlToValidate="txtQ4Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">4-	الأورام والسرطان
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">5.	Heart conditions limited to: Arrhythmia, Ischaemic heart disease (IHD), Open heart surgery (CABG), Catheterization, Pacemaker, Valve disease.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ5" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv9,rfv10,'txtQ5Box1','txtQ5Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv9,rfv10,'txtQ5Box1','txtQ5Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq5" runat="server" ControlToValidate="rdbQ5" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ5Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv9" runat="server" ControlToValidate="txtQ5Box1" Display="Dynamic" ErrorMessage="*" Enabled="False"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv5" runat="server" ControlToValidate="txtQ5Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ5Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv10" runat="server" ControlToValidate="txtQ5Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">5-	أمراض القلب التالية: عدم انتظام ضربات القلب، قصور في التروية، عملية سابقة بالقلب، قسطرة الشرايين، جهاز منظم للضربات، أمراض الصمامات
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">6.	Liver disorder limited to: Hepatitis, Cirrhosis, Esophageal varices, Gallbladder stones.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ6" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv11,rfv12,'txtQ6Box1','txtQ6Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv11,rfv12,'txtQ6Box1','txtQ6Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq6" runat="server" ControlToValidate="rdbQ6" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ6Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv11" runat="server" ControlToValidate="txtQ6Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv6" runat="server" ControlToValidate="txtQ6Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Type="Integer" Enabled="False">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ6Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv12" runat="server" ControlToValidate="txtQ6Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">6-	أمراض الكبد التالية: تليف الكبد، التهاب كبدي مزمن، دوالي بالمريء، حصوات بالمرارة
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">7.	Urinary tract disorder limited to: Renal failure, Urinary tract stones.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ7" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv13,rfv14,'txtQ7Box1','txtQ7Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv13,rfv14,'txtQ7Box1','txtQ7Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq7" runat="server" ControlToValidate="rdbQ7" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ7Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv13" runat="server" ControlToValidate="txtQ7Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv7" runat="server" ControlToValidate="txtQ7Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ7Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv14" runat="server" ControlToValidate="txtQ7Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">7-	أمراض الجهاز البولي التالية: فشل كلوي، حصوات الجهاز البولي
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">8.	Autoimmune disorder limited to: Ankylosing spondylitis, Multiple sclerosis (MS), Psoriasis, Systemic lupus erythematosus (SLE), Rheumatoid, Ulcerative colitis (Crohn's).
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ8" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv15,rfv16,'txtQ8Box1','txtQ8Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv15,rfv16,'txtQ8Box1','txtQ8Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq8" runat="server" ControlToValidate="rdbQ8" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ8Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv15" runat="server" ControlToValidate="txtQ8Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv8" runat="server" ControlToValidate="txtQ8Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ8Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv16" runat="server" ControlToValidate="txtQ8Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">8-	الأمراض المناعية التالية: التهاب الفقرات المناعي، التصلب المتعدد، الصدفية، الذئبة الحمراء، الروماتيزم، التهاب القولون التقرحي
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">9.	Vascular disease limited to: Phlebitis, Varicocele, Varicose vein, Vasculitis, Aneurysm.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ9" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv17,rfv18,'txtQ9Box1','txtQ9Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv17,rfv18,'txtQ9Box1','txtQ9Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq9" runat="server" ControlToValidate="rdbQ9" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ9Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv17" runat="server" ControlToValidate="txtQ9Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv9" runat="server" ControlToValidate="txtQ9Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ9Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv18" runat="server" ControlToValidate="txtQ9Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">9-	أمراض الأوردة الدموية التالية: الدوالي، تضخم أو تمدد أو التهاب أو جلطة الأوعية الدموية
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">10.	Blood disorder limited to: Sickle cell anemia (SCD), Hemophilia, Thalassemia, Leukemia.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ10" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv19,rfv20,'txtQ10Box1','txtQ10Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv19,rfv20,'txtQ10Box1','txtQ10Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq10" runat="server" ControlToValidate="rdbQ10" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ10Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv19" runat="server" ControlToValidate="txtQ10Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv10" runat="server" ControlToValidate="txtQ10Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ10Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv20" runat="server" ControlToValidate="txtQ10Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">10-	أمراض الدم التالية: الأنيميا المنجلية، الهيموفيليا، الثلاسيميا، سرطان الدم
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">11.	Congenital disorder & Hereditary disease (Diseases resulting from defects or genetic disorder and transmitted from one generation to another, or that affect the individual during fetal life).
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ11" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv21,rfv22,'txtQ11Box1','txtQ11Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv21,rfv22,'txtQ11Box1','txtQ11Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq11" runat="server" ControlToValidate="rdbQ11" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ11Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv21" runat="server" ControlToValidate="txtQ11Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv11" runat="server" ControlToValidate="txtQ11Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ11Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv22" runat="server" ControlToValidate="txtQ11Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">11-	التشوهات الخلقية أو الأمراض الوراثية (الأمراض الناتجة عن خلل أو اضطراب الجينات والمنتقلة من جيل إلى آخر أو التي تصيب الفرد أثناء المرحلة الجنينية)
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">12.	Uncontrolled Diabetes / Hypertension cases needing admission from time to time.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ12" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv23,rfv24,'txtQ12Box1','txtQ12Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv23,rfv24,'txtQ12Box1','txtQ12Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq12" runat="server" ControlToValidate="rdbQ12" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ12Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv23" runat="server" ControlToValidate="txtQ12Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv12" runat="server" ControlToValidate="txtQ12Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ12Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv24" runat="server" ControlToValidate="txtQ12Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">12-	حالات سكري /ضغط الدم والتي تحتاج للتنويم بين الحين والآخر لأجل تنظيم المستوى في الجسم
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">13.	Eye disease limited to: Cataract, Glaucoma, Corneal & Retinal condition.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ13" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv25,rfv26,'txtQ13Box1','txtQ13Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv25,rfv26,'txtQ13Box1','txtQ13Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq13" runat="server" ControlToValidate="rdbQ13" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ13Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv25" runat="server" ControlToValidate="txtQ13Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv13" runat="server" ControlToValidate="txtQ13Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ13Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv26" runat="server" ControlToValidate="txtQ13Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">13-	أمراض العين التالية: مياه بيضاء، مياه زرقاء، أمراض القرنية، أمراض الشبكية
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">14.	Ear disease limited to: Hearing loss, Equilibrium problems, and Cochlear problems.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ14" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv27,rfv28,'txtQ14Box1','txtQ14Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv27,rfv28,'txtQ14Box1','txtQ14Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq14" runat="server" ControlToValidate="rdbQ14" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ14Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv27" runat="server" ControlToValidate="txtQ14Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv14" runat="server" ControlToValidate="txtQ14Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ14Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv28" runat="server" ControlToValidate="txtQ14Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">14-	أمراض السمع التالية: فقدان السمع، مشاكل الاتزان، مشاكل قوقعة الأذن
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">15.	Bone disease limited to: Disc prolapse, Arthritis, Scoliosis, Ligament tears, Osteoporosis.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ15" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv29,rfv30,'txtQ15Box1','txtQ15Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv29,rfv30,'txtQ15Box1','txtQ15Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq15" runat="server" ControlToValidate="rdbQ15" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ15Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2">
                        </asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv29" runat="server" ControlToValidate="txtQ15Box1" Display="Dynamic" ErrorMessage="*" Enabled="False"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv15" runat="server" ControlToValidate="txtQ15Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ15Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100">
                        </asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv30" runat="server" ControlToValidate="txtQ15Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">15-	أمراض العظام التالية: الانزلاق الغضروفي (الديسك)، تشوهات العمود الفقري، التهاب العظام المزمن، مشاكل الركبة وأربطة المفاصل، هشاشة العظام
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">16.	Tissue disease limited to: Abnormal tissue growth, Cyst, Hernia, Ulcers (Bed sores, Diabetic foot).
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ16" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv31,rfv32,'txtQ16Box1','txtQ16Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv32,rfv32,'txtQ16Box1','txtQ16Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq16" runat="server" ControlToValidate="rdbQ16" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ16Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2">

                        </asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv31" runat="server" ControlToValidate="txtQ16Box1" Display="Dynamic" ErrorMessage="*" Enabled="False"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv16" runat="server" ControlToValidate="txtQ16Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ16Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100">

                        </asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv32" runat="server" ControlToValidate="txtQ16Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">16-	نمو غير طبيعي بالأنسجة والأعضاء، تكيسات، الفتق والبواسير، قرحة الفراش، القدم السكرية
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">17.	Current pregnancy for female employee or employee's wives.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ17" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv33,rfv34,'txtQ17Box1','txtQ17Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv33,rfv34,'txtQ17Box1','txtQ17Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ControlToValidate="rdbQ17"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ17Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv33" runat="server" ControlToValidate="txtQ17Box1"
                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="1" Enabled="False"
                            ToolTip="Field Required" Font-Bold="True" Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtQ17Box1"
                            Display="Dynamic" ErrorMessage="RangeValidator" Enabled="False" MaximumValue="99" MinimumValue="1"
                            Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ17Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv34" runat="server" ControlToValidate="txtQ17Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">17-	حالات الحمل الحالية للموظفات أو زوجات الموظفين.
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">18.	Current multiple pregnancy or baby with congenital anomaly.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ18" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv35,rfv36,'txtQ18Box1','txtQ18Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv35,rfv36,'txtQ18Box1','txtQ18Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="rdbQ18"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ18Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv35" runat="server" ControlToValidate="txtQ18Box1"
                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="1" Enabled="False"
                            ToolTip="Field Required" Font-Bold="True" Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtQ18Box1"
                            Display="Dynamic" ErrorMessage="RangeValidator" MaximumValue="99" Enabled="False" MinimumValue="1"
                            Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ18Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv36" runat="server" ControlToValidate="txtQ18Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">18-	الحالات الحالية أو السابقة لحمل متعدد الأجنة أو تشوهات خلقية
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">19.	History of abortion or previous Cesarean section delivery or instrumental assisted or premature labor or baby with congenital anomaly.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ19" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv37,rfv38,'txtQ19Box1','txtQ19Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv37,rfv38,'txtQ19Box1','txtQ19Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ControlToValidate="rdbQ19"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ19Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv37" runat="server" ControlToValidate="txtQ19Box1"
                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="1" Enabled="False"
                            ToolTip="Field Required" Font-Bold="True" Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtQ19Box1"
                            Display="Dynamic" ErrorMessage="RangeValidator" MaximumValue="99" Enabled="False" MinimumValue="1"
                            Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ19Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv38" runat="server" ControlToValidate="txtQ19Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">19-	الحالات السابقة للإجهاض أو الولادات القيصرية أو المبكرة أو المتعثرة
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#0099ff" style="font-size: 10pt; width: 230px; color: #404040; font-family: Arial; height: 3px"></td>
                    <td bgcolor="#0099ff" style="width: 70px; height: 3px"></td>
                    <td bgcolor="#0099ff" style="width: 70px; height: 3px"></td>
                    <td bgcolor="#0099ff" style="width: 155px; height: 3px"></td>
                    <td bgcolor="#0099ff" style="font-size: 10pt; width: 225px; color: #404040; direction: rtl; font-family: Arial; height: 3px; text-align: right"></td>
                </tr>
            </table>
            <br />
            <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;">
                <tr>
                    <td style="width: 357px; text-align: justify; clip: rect(auto auto auto auto); height: 368px; vertical-align: top;">
                        <span style="font-size: 9pt; color: #404040; font-family: Arial; vertical-align: top; margin-top: 0px;">
                            <b>Declaration:</b>
                            <br />
                            <span style="font-size: 7.5pt;">1- I / We confirm that all mentioned data in this form is complete
                                and correct and has been discussed with all sta_ and their
                                families in a manner not inconsistent with the privacy and
                                confidentiality of the information, and the acceptance of the
                                application will be based on this data. Bupa Arabia for
                                Cooperative Insurance has the right to contact the hospitals
                                which I am / We are dealing with to provide any medical
                                information that may be needed to assess the risk.
                                <br />
                                2- I / We agree that Bupa Arabia for Cooperative Insurance have the
                                eligibility to reject the claim or the entire coverage at concealment
                                of any unexpected case or any case that arose before the
                                date of the contract, enrolment, or addition, even if the cases
                                mentioned in the medical disclosure whether the cases are
                                undiagnosed before unless if they were accepted by Bupa Arabia
                                in writing.
                                <br />
                                3- I / We undertake to perform similar declaration in the future on
                                members who will be added during the contract period or upon
                                renewal as all old and new Declaration forms are considered as
                                integral part of the current and future contracts.
                            </span>
                            <br />
                            <a>
                                <span style="font-size: 7pt;">If you have any questions please contact us at: Sales Toll-free: 800 116 0500</span>
                            </a>
                        </span>
                        Email: <a href="mailto:business@bupame.com"><span style="font-size: 7pt">business@bupame.com</span></a>
                    </td>
                    <td style="width: 15px; text-align: justify; height: 368px;"></td>
                    <td style="direction: rtl; text-align: justify; font-family: Times New Roman; font-size: 8pt; color: black; text-indent: 2pt; vertical-align: top; letter-spacing: normal; height: 368px;">
                        <b><span style="font-size: 11pt">إقـرار:</span></b>
                        <br />
                        <span style="font-size: 10pt; vertical-align: middle; color: #404040; direction: rtl; font-family: Arial; text-align: right; padding-right: 5px; clip: rect(auto 5px auto auto); text-indent: 2pt; margin-right: 5px;">1- أقر/نقر أن جميع البيانات المذكورة في في هذا النموذج كاملة وصحيحة وتمت مناقشتها مع الأفراد أو الموظفين المذكورين جميعهم وعائلاتهم بطريقة لا تتنافى مع خصوصية وسرية المعلومات، وبناء عليه فإن قبول الطلب سيتم على أساس هذه البيانات وأن شركة بوبا العربية للتأمين التعاوني لها الحق في الإتصال بالمستشفيات التي أتعامل/نتعامل معها لتزويدها بأي معلومات طبية قد تحتاج إليها لتقييم الخطر.<br />

                            2- أوافق/نوافق بأحقية شركة بوبا العربية للتأمين التعاوني في رفض المطالبة أو التغطية كليا عند عدم الإفصاح عن أي حالة متوقعة أو نشأت قبل تاريخ التعاقد أو التسجيل أو الإضافة خلال العقد ما إذا كانت من الحالات المذكورة في قسم الإفصاح الطبي أو لم تذكر سواء كانت مشخصة من قبل أو قيد التشخيص إلا إذا تم قبولها من قبل شركة بوبا العربية خطيا.-
                            <br />
                            3- أتعهد/نتعهد القيام بعملية افصاح مماثل مستقبلا على الأعضاء الذين سيتم إضافتهم خلال مدة سريان العقد أو عند التجديد حيث أن جميع نماذج الإفصاح القديمة والحديثة تعتبر جزء لا يتجزء من العقد الحالي والعقود المستقبلية.-
                            <br />
                        </span><a><span style="font-size: 9pt">الهاتف اﻟﻤﺠاني: ۰٥۰۰ ۱۱٦ ۸۰۰</span></a><a><span
                            style="font-size: 9pt"> ، الفاكس: ۱٤٦۹ ٦٦۸ ۰٢</span></a><span style="font-size: 9pt; direction: rtl; text-indent: 2pt;">
                                <br />
                                العنوان الإلكتروني :</span><a href="mailto:business@bupame.com"><span style="font-size: 9pt">
                                    business@bupame.com</span></a>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table style="width: 750px; font-size: 11px; font-family: Arial;" runat="server" id="Table2">
            <tr>
                <td align="right">
                    <asp:Button CssClass="submitButton" ID="btnSubmit" runat="server"
                        Text="Submit Request" ValidationGroup="1" OnClick="btnSubmit_Click" BackColor="Control" />
                </td>
            </tr>
        </table>
        <br />
        <uc2:OSNav ID="OSNav1" runat="server" />
        <br />
    </fieldset>
    <telerik:RadNotification ID="RadNotification1" runat="server" ShowInterval="1115000"
        VisibleOnPageLoad="true" LoadContentOn="FirstShow" Width="400" Animation="Fade"
        EnableRoundedCorners="true" EnableShadow="true"
        Font-Names="Tahoma" Position="TopRight" BackColor="#66CCFF" ContentScrolling="Auto"
        AutoCloseDelay="1113000" BorderColor="#66CCFF" Title="Bedon Nationality" VisibleTitlebar="True"
        BorderStyle="Solid" BorderWidth="1" ShowCloseButton="False" RenderMode="Classic">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <div class="infoIcon">
                            <img src="../images/Basic-Info.ico" alt="info icon" style="width: 35px; height: 35px;" />
                        </div>
                    </td>
                    <td>
                        <div style="line-height: 200%;">
                            <span style="font-family: Arial; font-size: 12px; color: #0099ff; font-weight: normal;">To add “Bedoun” member please use Bupa Arabia 
                            <a style="color: #0099ff" href="http://www.bupa.com.sa/_layouts/Bupa/Downloader.aspx?ID=2&listname=Files&LowRes=1">Maintenance Form</a>
                                and send it to <a style="color: #0099ff" href="mailto:MembershipEmail@Bupa.com.sa">MembershipEmail@Bupa.com.sa</a>
                            </span>
                            <br />
                            <span style="float: right; direction: rtl; font-family: Tahoma; font-size: 11px; color: #0099ff; font-weight: normal;">لإضافة عضو من فئة "بدون" الرجاء إستخدام  
                                <a style="color: #0099ff" href="http://www.bupa.com.sa/_layouts/Bupa/Downloader.aspx?ID=2&listname=Files&LowRes=1">نموذج بوبا العربية</a> للإضافة والحذف
                                 وإرسال الطلب إلى <a style="color: #0099ff" href="mailto:MembershipEmail@Bupa.com.sa">MembershipEmail@Bupa.com.sa</a> </span>
                            <br />
                            <br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <br />
                        <a onclick="OnClientClicked()" href="javascript:void(0);" class="button">Close</a>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </ContentTemplate>
    </telerik:RadNotification>
    <%--<asp:HiddenField  ID="hfDependentSelection" runat="server" />--%>

</asp:Content>
<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>
</asp:Content>
