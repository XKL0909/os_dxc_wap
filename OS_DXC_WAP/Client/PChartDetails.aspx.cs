﻿using Bupa.Core.Utilities.Configuration;
using OnlinseServicesDashBoard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Client_PChartDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        PreAuthDetailsChart.DataSource = GetPreAuthGroupedData(GetPreAuthData());
        PreAuthDetailsChart.DataBind();

        //PreAuthDetailsChart.PlotArea.XAxis.

        PreAuthDetailsChart.PlotArea.Series[0].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Green;
        PreAuthDetailsChart.PlotArea.Series[2].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Red;
        PreAuthDetailsChart.PlotArea.Series[1].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.DarkSeaGreen;
        PreAuthDetailsChart.PlotArea.Series[3].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Purple;
        PreAuthDetailsChart.PlotArea.Series[4].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Navy;
    }


    private DataSet GetPreAuthData()
    {
        var caesarConnectionString = CoreConfiguration.Instance.GetConfigValue("Caesar Connection String");
        var contractNumber = Convert.ToString(Session["ClientID"]);// CoreConfiguration.Instance.GetConfigValue("FilterByContract");


        DataSet ds = DataHelper.GetPreAuthData(caesarConnectionString, DateTime.Now.AddDays(-10), DateTime.Now, contractNumber);
        //DataTable dt = ds.Tables[0];

        PreAuthDetailsGrid.DataSource = ds;
        PreAuthDetailsGrid.DataBind();



        return ds;
    }

    private DataSet GetPreAuthGroupedData(DataSet dsSource)
    {
        //Dataset for chart
        DataSet ds = new DataSet("PreAuthsDataSet");
        DataTable dt = new DataTable("PreAuthsDataTable");
        //dt.Columns.Add("PREAUTH_ID", Type.GetType("System.Int32"));
        dt.Columns.Add("RECEIVED_DATE", Type.GetType("System.DateTime"));
        dt.Columns.Add("ApprovedPreAuths", Type.GetType("System.String"));
        dt.Columns.Add("RejectedPreAuths", Type.GetType("System.String"));
        dt.Columns.Add("PartApprovedPreAuths", Type.GetType("System.String"));
        dt.Columns.Add("NotRequiredPreAuths", Type.GetType("System.String"));
        dt.Columns.Add("CancelledPreAuths", Type.GetType("System.String"));
        //dt.Columns.Add("EST_AMT", Type.GetType("System.Decimal"));

        int count = 1;

        var query = from dr in dsSource.Tables[0].AsEnumerable()
                    group dr by dr["RECEIVED_DATE"] into newdr
                    select new
                    {
                        RECEIVED_DATE = newdr.First().Field<string>("RECEIVED_DATE"),
                        //PREAUTH_COUNT = newdr.Count(),
                        ApprovedPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("Approved")).Count(),
                        RejectedPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("Rejected")).Count(),
                        PartApprovedPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("Part Approved")).Count(),
                        NotRequiredPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("No Preauthorization Required")).Count(),
                        CancelledPreAuths = newdr.Where(x => x.Field<object>("PREAUTH_STATUS").Equals("Cancelled")).Count(),
                    };

        foreach (var dtRow in /*dsSource.Tables[0].Rows*/ query)
        {
            //count = count + 1;
            dt.Rows.Add(dtRow.RECEIVED_DATE, dtRow.ApprovedPreAuths.ToString(), dtRow.RejectedPreAuths.ToString(), dtRow.PartApprovedPreAuths.ToString(), dtRow.NotRequiredPreAuths.ToString(), dtRow.CancelledPreAuths.ToString());
        }

        ds.Tables.Add(dt);
        return ds;
    }
}

