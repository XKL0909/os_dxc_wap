﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Client_MasterGS" Codebehind="MasterGS.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <p>
        Bupa Customers List Under this Master Group
    </p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="Server">
    <p>

        <table style="width: 50%" align="center">
            <tr>
                <td class="loginLabel">Customer</td>
                <td>
                    <asp:DropDownList ID="ddlClientList" runat="server"
                        DataSourceID="SqlDataSourceMasterGS" DataTextField="ContractCodeAndName"
                        DataValueField="ContractCode" Font-Size="9pt" Height="25px" Width="300px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit"
                        OnClick="btnSubmit_Click" />
                </td>
            </tr>
            <tr>
                <td class="loginLabel" colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td class="loginLabel" colspan="3">
                    <asp:SqlDataSource
                        ID="SqlDataSourceMasterGS" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
                        SelectCommand="sp_GetContractListByMasterContract" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:Parameter Name="cliUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
        </table>
    </p>
</asp:Content>

