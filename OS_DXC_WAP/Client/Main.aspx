﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/DashBoard.master" AutoEventWireup="true" Inherits="Client_Main" Codebehind="Main.aspx.cs" %>

<%@ Register Src="../UserControls/DashBoardWelcome.ascx" TagName="DashBoardWelcome" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/DashBoardOSNav.ascx" TagName="DashBoardOSNav" TagPrefix="uc2" %>

<%@ Register Src="../UserControls/Tips.ascx" TagName="Tips" TagPrefix="uc3" %>

<%@ Register Src="../UserControls/HealthArt.ascx" TagName="HealthArt" TagPrefix="uc4" %>

<%@ Register Src="../UserControls/Apps.ascx" TagName="Apps" TagPrefix="uc5" %>

<%@ Register Src="../UserControls/CCHI.ascx" TagName="CCHI" TagPrefix="uc6" %>

<%@ Register Src="../UserControls/Downloads.ascx" TagName="Downloads" TagPrefix="uc7" %>

<%@ Register Src="../UserControls/MyInbox.ascx" TagName="MyInbox" TagPrefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderLeftNav" runat="Server">


    <uc6:CCHI ID="CCHI1" runat="server" />
    <style type="text/css">
        .btnCss {
            width: auto !important;
            background-color: #0886D6;
            cursor: pointer;
            color: #fff !important;
            line-height: 30px !important;
            outline: none;
            border: none;
            font-weight: bold;
            font-size: 13px !important;
        }

            .btnCss:hover {
                background-color: #00a1e4;
            }
    </style>
    <style type="text/css">
        /*this is what we want the div to look like*/
        div.botright {
            display: block;
            /*set the div in the bottom right corner*/
            position: fixed;
            bottom: 0;
            right: 0;
            width: 300px;
            height: 200px;
            margin: 0 auto 20px auto;
            padding: 25px 15px;
            /*give it some background and border*/
            background: #FF0000;
            border: 1px solid #7d5912;
        }

        .divCredit {font-family: Arial; color:#8C3107; font-size:11.5px; font-weight:bold;  background: #F5A9A9;
  background: radial-gradient(#F5A9A9, #F5A9BC);
  border-radius: 6px;
  padding: 3px 10px 5px 10px;
  height: 55px;
  text-align:justify;
  line-height: 1.6;
            }
         a.vat:link {color:#0251E7;text-decoration:underline;}
a.vat:visited {color:#0251E7;text-decoration:underline;}
a.vat:hover {text-decoration:underline;}
    </style>
    <script type="text/javascript">
        function goto(x) {
            window.location = x;
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
    <table>
        <tr>

            <td>
                <div runat="server" visible="true" id="divCredit">
                    <div class="divCredit">
                        Registration for the Value Added Tax is mandatory for establishments whose income ranges between SR 375,000 and SR 1 Million before 20th December 2018. For more information and to register your company, please visit <a href="https://www.vat.gov.sa/ar" target="_blank" class="vat">vat.gov.sa</a>
                    </div>
                    <br />
                </div>

                <uc1:DashBoardWelcome ID="DashBoardWelcome1" runat="server" />

                <uc8:MyInbox ID="MyInbox1" runat="server" />

                <uc2:DashBoardOSNav ID="DashBoardOSNav1" runat="server" />
                <!--<uc3:Tips ID="Tips1" runat="server" />-->
                <uc4:HealthArt ID="HealthArt1" runat="server" />
                <uc5:Apps ID="Apps1" runat="server" />
                <%--<uc7:Downloads ID="Downloads1" runat="server" />--%>
                <div class="botright" runat="server" id="Notify" visible="false">
                    <table width="100%">
                        <tr>
                            <td valign="top" style="color: White">
                                <h3>Message From Bupa</h3>
                            </td>
                            <td align="right" valign="bottom">
                                <asp:ImageButton ID="imgClose" ImageUrl="~/Member/images/close.png"
                                    runat="server" Width="32" Height="32" Style="border: 0px"
                                    OnClick="imgClose_Click" CausesValidation="False" /></td>
                        </tr>

                        <tr>
                            <td colspan="2" style="color: White">
                                <asp:Label ID="lblClient" ForeColor="White" runat="server"></asp:Label>
                                <asp:Label ID="LabelMedicalDeclarationMessage" ForeColor="White" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>

                            <td align="right" colspan="2">
                                <img src="../member/images/feedback.png" width="32" height="32" style="border: 0px" /></td>
                        </tr>
                    </table>


                </div>

                <div id="divCollectionDeptPopup" runat="server" style="top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 10;">
                    <div style="left: 15%; top: 28%; position: fixed;">
                        <table style="background-color: #E5F7FF; width: 80%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                            <tr style="background-color: #0886D6; line-height: 30px">
                                <td colspan="3" style="color: #fff; text-align: left">
                                    <strong style="padding-left: 5px; font-size: larger">Message from Collection Department</strong>
                                    <span style="float: right;">
                                        <strong style="font-size: x-large">رسالة من إدارة التحصيل&nbsp;</strong>
                                        <a onclick="ClosePopUp('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size: larger">X</a>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="margin: auto" colspan="3"></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 60%">
                                    <div style="padding-left: 10px; padding-right: 10px;">
                                        <asp:Panel ID="pnlOverDueEng" runat="server">
                                            <div style="margin-top: 0; margin-bottom: 0;">Dear Bupa’s Valued Customer: </div>
                                            <div style="margin-top: 0; margin-bottom: 0;">We would like to inform you about the financial status of your healthcare insurance policy and would request you to kindly settle your balance as per the stated due dates in the contract and invoices.</div>
                                            <div style="margin-top: 0; margin-bottom: 0;"><b>Contract Number:</b> <%=Session["ClientID"] %></div>
                                            <div style="margin-top: 0; margin-bottom: 0;"><b>Contract Name:</b> <%=Session["ClientName"]%></div>
                                            <div style="margin-top: 0; margin-bottom: 0;"><b>Total Balance (SAR):</b> <%= balAmount %></div><br />
                                            <div style="margin-top: 0; margin-bottom: 0;">You may contact us for any assistance through our office landlines and we will be delighted to serve you:</div>
                                            <div style="margin-top: 0; margin-bottom: 0;">Corporate & Broker Clients 920000456 Ext. (8460)</div>
                                            <div style="margin-top: 0; margin-bottom: 0;">Small and Medium Clients 920000456 Ext. (8446)</div><br />
                                            <div style="margin-top: 0; margin-bottom: 0;">If you have already settled the balance, kindly notify us and forward the payment slip or receipt to update your records via the below Collection Services email address.</div><br/>
                                            <div style="margin-top: 0; margin-bottom: 0;">To settle the balance, please directly transfer to your specified bank account IBAN Number: <%= _VIBAN %></div><br />
                                            <div style="margin-top: 0; margin-bottom: 0;">Thank you for your cooperation</div>
                                            <div style="margin-top: 0; margin-bottom: 0;">Collection Services Department – Bupa Arabia</div>
                                            <div style="margin-top: 0; margin-bottom: 0;"><a href="mailto:BupaArabia.Collection@bupa.com.sa">BupaArabia.Collection@bupa.com.sa</a>.</div>
                                        </asp:Panel>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td style="text-align: right">
                                    <div style="padding-right: 10px; padding-left: 10px;">
                                        <div style="padding-right: 10px; padding-left: 5px;">
                                            <asp:Panel ID="pnlOverDueArabic" runat="server">
                                                <div>: عزيزنا عميل بوبا العربية</div>
                                                <div>نود إفادتكم و إخطاركم بوجود مبالغ مالية مفوترة على عقد بوليصة التأمين الصحي الخاصة بكم و يجب سدادها حسب تاريخ الاستحقاق الموضح في العقد والفواتير</div>
                                                <div><%=Session["ClientID"] %> :رقم العقد</div>
                                                <div><%=Session["ClientName"]%> :اسم العقد</div>
                                                <div><%= balAmount %>:(المبلغ المستحق (بالريال</div><br/>
                                                <div>يسعدنا الرد على جميع استفساراتكم عبر الارقام التالية ليتم خدمتكم مباشرا</div><br />
                                                <div>(8460) عملاء الشركات و وسطاء الشركات 920000456 تحويلة</div>
                                                <div>(عملاء المؤسسات المتوسطة والصغيرة 920000456 تحويلة (8446</div><br />
                                                <div>.في حال تم السداد نرجوا من سعادتكم التكرم بإشعارنا عن طريق ارسال نسخه من الإيصال الى البريد الإلكتروني أدناه ليتم تحديث سجلاتكم</div><br />
                                                <div>للسداد الرجاء تحويل المبلغ مباشرة الى رقم حساب البنكي الخاص بكم (الآيبان) وهو</div>
                                                <div><%= _VIBAN %></div><br />
                                                <div>شاكرين تعاونكم</div>
                                                <div>إدارة خدمات التحصيل - بوبا العربية</div>
                                                <a href="mailto:BupaArabia.Collection@bupa.com.sa">BupaArabia.Collection@bupa.com.sa</a>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                    <%-- <input type="button" onclick="ClosePopUp('2')" class="btnCss" value="Click here for more details | انقر هنا لمزيد من التفاصيل" />--%>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
            <td>
                <div id="popupPasswordExpiry" runat="server" style="top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999;">
                    <div style="left: 40%; top: 40%; position: fixed;">
                        <table style="background-color: #fff; width: 100%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                            <tr style="background-color: #f44336; line-height: 30px">
                                <td colspan="3" style="color: #fff; text-align: left">
                                    <strong style="padding-left: 5px; font-size: larger">Password Expiry</strong>
                                    <span style="float: right;">
                                        <strong style="font-size: larger">انتهاء صلاحية كلمة المرور&nbsp;</strong>
                                        <%--<a onclick="closePWDNotification('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size: larger">X</a>--%>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="margin: auto" colspan="3"></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 60%">
                                    <div style="padding-left: 10px; padding-right: 10px;">
                                        <asp:Panel ID="Panel1" runat="server">
                                            <div style="margin-top: 0; margin-bottom: 0;">Dear Client, </div>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="Panel2">
                                            <div id="ExpMessage" runat="server" style="margin-top: 0; margin-bottom: 0;">
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td style="text-align: right">
                                    <div>
                                        <asp:Panel ID="Panel4" runat="server">
                                            <div style="margin-top: 0; margin-bottom: 0; padding-right: 10px;">،عزيزي العميل</div>
                                        </asp:Panel>
                                        <div id="ExpMessagear" runat="server" style="padding-right: 10px; text-align: right; padding-left: 10px;">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                    <input id="btnResetPassword" type="button" onclick="redirectChangePassword()" class="btnCss" value="Reset" />
                                    <input id="btnLater" runat="server" type="button" onclick="closePasswordExpWindow(1)" class="btnCss" value="Later" />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div id="PopUpIsSuperUser" runat="server" style="display: none; border: 1px solid black; z-index: 999999999999999; top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999999999999;">
                    <div style="left: 25%; top: 50%; position: fixed;">
                        <table style="border: 1px solid black; background-color: #E5F7FF; width: 50%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                            <tr style="background-color: #0886D6; line-height: 30px">
                                <td colspan="3" style="color: #fff; text-align: left">
                                    <strong style="padding-left: 5px; font-size: larger">BUPA - Online Services</strong>
                                    <span style="float: right;">
                                        <strong style="font-size: x-large">بوبا - خدمات الإلكترونية&nbsp;</strong>
                                        <a onclick="PopUpIsSuperUser('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size: larger">
                                            <img alt="Procced" src="../images/failure.png" style="width: 20px; height: 20px" /></a>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="margin: auto" colspan="3"></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 60%">
                                    <div style="padding-left: 10px; padding-right: 10px;">
                                        <asp:Panel ID="Panel3" runat="server">
                                            <div style="margin-top: 0; margin-bottom: 0;">
                                                A Concurrent user will not be able to access Bupa Arabia online services as of 23 May 2018 at 10:00 AM to share same user established to multiple users. Kindly use the option (User Management) to create additional user.
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td style="text-align: right">
                                    <div style="padding-right: 10px; padding-left: 10px;">
                                        <div style="padding-right: 10px; padding-left: 10px;">
                                            <asp:Panel ID="Panel6" runat="server">
                                                بحلول 23 مايو 2018 الساعة 10:00 صباحا لن يتمكن المستخدم المتزامن (مشاركة اسم المستخدم نفسه بين عدة مستخدمين) من الوصول إلى خدمات بوبا العربية الإكترونية. يرجى استخدام خيار (إدارة المستخدمين) لإنشاء مستخدم إضافي.
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                    <%-- <input type="button" onclick="ClosePopUp('2')" class="btnCss" value="Click here for more details | انقر هنا لمزيد من التفاصيل" />--%>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div id="PopUpOldPortal" runat="server" style="display: none; border: 1px solid black; z-index: 999999999999999; top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999999999999;">
                    <div style="left: 25%; top: 50%; position: fixed;">
                        <table style="border: 1px solid black; background-color: #E5F7FF; width: 80%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                            <tr style="background-color: #0886D6; line-height: 30px">
                                <td colspan="3" style="color: #fff; text-align: left">
                                    <strong style="padding-left: 5px; font-size: larger">BUPA - Online Services</strong>
                                    <span style="float: right;">
                                        <strong style="font-size: x-large">بوبا - خدمات الإلكترونية&nbsp;</strong>
                                        <a onclick="PopUpOldPortal('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size: larger">
                                            <img alt="Procced" src="../images/failure.png" style="width: 20px; height: 20px" /></a>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="margin: auto" colspan="3"></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 50%">
                                    <div style="padding-left: 10px; padding-right: 10px;">
                                        <asp:Panel ID="pnlOldPortalEng" runat="server">
                                            <div style="margin-top: 0; margin-bottom: 0;">
                                                Dear Customer,<br />
                                                We would like to inform you that as of Sunday 01/07/2018 the Medical Declaration form has been updated and only the new version will be accepted in the portal for all addition transactions related to your contract.
											    <br />
                                                Click <a href="https://www.bupa.com.sa/docs/default-source/customer-care-download-document/sme---declaration-form-cchi-v6c4ed278879506cfa95a5ff0000e296c7.pdf?sfvrsn=6" target="_blank">here</a> to access the updated Medical Declaration form.											
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td style="text-align: right">
                                    <div style="padding-right: 10px; padding-left: 10px;">
                                        <div style="padding-right: 10px; padding-left: 10px;">
                                            <asp:Panel ID="pnlOldPortalAra" runat="server">
                                                <span>، عزيزي العملاء</span><br />
                                                نود إبلاغكم بأنه إبتداءا من يوم الأحد الموافق 1\7\2018  تم تحديث نموذج الإفصاح و سوف يتم قبول النسخة المحدثة  فقط لأي عملية إضافة في نظام الخدمات
                                                <br />
                                                يمنكم الحصول على النسخة المحدثة في هذا الرابط <a href="https://www.bupa.com.sa/docs/default-source/customer-care-download-document/sme---declaration-form-cchi-v6c4ed278879506cfa95a5ff0000e296c7.pdf?sfvrsn=6" target="_blank">هنا</a>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                    <%-- <input type="button" onclick="ClosePopUp('2')" class="btnCss" value="Click here for more details | انقر هنا لمزيد من التفاصيل" />--%>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function ClosePopUp(value) {
            var ctrl = document.getElementById("<%=divCollectionDeptPopup.ClientID%>");
           ctrl.style.display = "none";
       }
       function PopUpIsSuperUser() {
           var ctrl = document.getElementById("<%=PopUpIsSuperUser.ClientID%>");
         ctrl.style.display = "none";
     }
     function closePasswordExpWindow(value) {
         var ctrl = document.getElementById("<%=popupPasswordExpiry.ClientID%>");
         ctrl.style.display = "none";
     }
     function redirectChangePassword(value) {
         window.location("../default.aspx");
         var ctrl = document.getElementById("<%=popupPasswordExpiry.ClientID%>");
         ctrl.style.display = "none";
     }
     function PopUpOldPortal(value) {
         var ctrl = document.getElementById("<%=PopUpOldPortal.ClientID%>");
     ctrl.style.display = "none";
 }
    </script>
</asp:Content>

