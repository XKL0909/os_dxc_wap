using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OS_DXC_WAP.CaesarWS;

public partial class CreditControl1 : System.Web.UI.Page
{
    private ServiceDepot_DNService ws;
    string strClientID, optionType;

    private Hashtable hasQueryValue;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ClientID"] == null)
        {
            HttpContext.Current.Response.Redirect("~/Default.aspx",true);
        }
        strClientID = Session["ClientID"].ToString();


        hasQueryValue = new Hashtable();
        string queryStringValues = string.Empty;

        if (Request.QueryString.Count > 0)
        {
            try
            {
                var val = Request.QueryString["val"];
                queryStringValues = Cryption.Decrypt(val);
                hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                optionType = hasQueryValue.ContainsKey("optiontype") ? Convert.ToString(hasQueryValue["optiontype"]) : string.Empty;
            }
            catch (Exception)
            {
                lblmsg.Text = "Invalid rquest!";
                return;
            }
        }
        

        if (optionType == "Invoice")
        {
            lblmsg.Text = "View and print your past and latest invoices";
        }
        else
        {
            lblmsg.Text = "View and print your past and latest statement of account";
        }
        if (!Page.IsPostBack)
        {
            PopulateBranchList(strClientID);
            if (optionType == "Invoice")
            {
                btnChangeBranch.Visible = false;
                lblAdd_Employee.Text = "Invoice";
            }
            else
                PopulateBranchList(strClientID);
            if (optionType == "Statement")
            {
                PnlStatement.Visible = true;
                btnSubmit.Text = "Print";
            }
            else if (optionType == "Invoice")
            {
                PnlInvoice.Visible = true;
            }
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (PnlStatement.Visible == true)
        {
            ////Response.Redirect("Statement.aspx?BID=" + ddlBranchCodeNew.SelectedValue + "&PS=" + RDBSundryPremium.Text + "&FS=" + RDBFullShort.Text + "&="); // + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR");
            var queryValue = Cryption.Encrypt("BID=" + ddlBranchCodeNew.SelectedValue + "&PS=" + RDBSundryPremium.Text + "&FS=" + RDBFullShort.Text);
            Response.Redirect("Statement.aspx?val=" + queryValue);
        }
        else if (PnlInvoice.Visible == true)
        {
            ReqInvoiceList("3208723", "M");
        }
    }

    private void fncChangeBranch()
    {
        ws = new ServiceDepot_DNService();
        SubTxnRequest_DN request;
        SubTxnResponse_DN response;
        request = new SubTxnRequest_DN();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.TransactionType = "BRANCH CHANGE";
        request.TransactionID = WebPublication.GenerateTransactionID();
        request.BatchIndicator = "N";
        request.detail = new AllMbrDetail_DN[1];
        request.detail[0] = new AllMbrDetail_DN();
        request.detail[0].sql_type = "SUB_TXN_REC";
        request.detail[0].ContractNo = strClientID; 
        request.memberName = new String[1];
        request.detail[0].MembershipNo = txtInvoiceNo.Text; 
        request.detail[0].EffectiveDate = DateTime.ParseExact(txtDateFrom.SelectedDate.ToString().Substring(0, 10), "dd/MM/yyyy", null);
        request.detail[0].NewBranch = ddlBranchCodeNew.SelectedValue; 
        request.detail[0].StaffNo = ""; 
        try
        {
            response = ws.SubTxn(request);
            StringBuilder sb = new StringBuilder(200);
            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/> ");

                Message1.Text = sb.ToString();
                Message1.Text += "."; 
                Message1.Visible = true;
            }
            else
            {
                Message1.Text = "Thank you for submitting your request. Your reference number is " + response.ReferenceNo.ToString() + ". We are now validating the submitted file.";
                Message1.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message; 
            Message1.Text = "Error Encountered. Please contact the administrator for further information.";
            Message1.Visible = true;
        }
    }

    private void fncChangeClass()
    {
        ws = new ServiceDepot_DNService();
        SubTxnRequest_DN request;
        SubTxnResponse_DN response;
        request = new SubTxnRequest_DN();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.TransactionType = "CLASS CHANGE";
        request.TransactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));
        request.BatchIndicator = "N";
        request.detail = new AllMbrDetail_DN[1];
        request.detail[0] = new AllMbrDetail_DN();
        request.detail[0].sql_type = "SUB_TXN_REC";
        request.detail[0].ContractNo = strClientID; 
        request.detail[0].MembershipNo = txtInvoiceNo.Text;
        request.detail[0].EffectiveDate = DateTime.ParseExact(txtDateFrom.SelectedDate.ToString().Substring(0, 10), "dd/MM/yyyy", null);
        request.detail[0].NewClass = ddlBranchCodeNew.SelectedValue; 
        try
        {
            response = ws.SubTxn(request);
            StringBuilder sb = new StringBuilder(200);
            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");
                Message1.Text = sb.ToString();
                Message1.Visible = true;
            }
            else
            {
                Message1.Text = "Thank you for submitting your request. Your reference number is " + response.ReferenceNo.ToString() + ". We are now validating the submitted file.";
                Message1.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message; 
            Message1.Text = "Error Encountered. Please contact the administrator for further information.";
            Message1.Visible = true;
        }
    }

    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }

    protected void btnChangeBranch_Click(object sender, EventArgs e)
    {
        fncChangeBranch();
    }

    // function used to fetch all branches list
    private void DisplayBranchListResult(ReqBrhListResponse_DN response)
    {
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            ddlBranchCodeNew.Items.Clear();
            ListItem DepList0 = new ListItem();
            DepList0.Text = "Please select from the list";
            DepList0.Value = "0";
            DepList0.Text = "All branches";
            DepList0.Value = "";
            ddlBranchCodeNew.Items.Add(DepList0);

            foreach (BrhListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.branchDesc;
                DepList.Value = dtl.branchCode.ToString();
                ddlBranchCodeNew.Items.Add(DepList);
            }
        }
        else
        {
        }
    }

    protected void PopulateBranchList(string ClientID)
    {
        {
            ws = new ServiceDepot_DNService();
            ReqBrhListRequest_DN request = new ReqBrhListRequest_DN();
            ReqBrhListResponse_DN response;
            request.membershipNo = long.Parse(strClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = TransactionManager.TransactionID();
            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.ReqBrhList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                }
                else
                {
                    DisplayBranchListResult(response);
                }
            }
            catch (Exception ex)
            {
                Message1.Text = ex.Message;
            }
        }
    }

    // function to display all branches of the contractid
    private void DisplayClassListResult(ReqClsListResponse_DN response)
    {
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            ddlBranchCodeNew.Items.Clear();
            ListItem DepList0 = new ListItem();
            DepList0.Text = "Please select from the list";
            DepList0.Value = "0";
            DepList0.Text = "All branches";
            DepList0.Value = "";
            ddlBranchCodeNew.Items.Add(DepList0);
            foreach (ClsListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.className;
                DepList.Value = dtl.classID.ToString();
                ddlBranchCodeNew.Items.Add(DepList);
            }
        }
        else
        {
        }
    }

    protected void PopulateClassList(string ClientID)
    {
        {
            ws = new ServiceDepot_DNService();
            ReqClsListRequest_DN request = new ReqClsListRequest_DN();
            ReqClsListResponse_DN response;
            request.membershipNo = long.Parse(strClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.ReqClsList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                }
                else
                {
                    DisplayClassListResult(response);
                }
            }
            catch (Exception ex)
            {
                Message1.Text = ex.Message;
            }
        }
    }

    protected void btnChangeClass_Click(object sender, EventArgs e)
    {
        fncChangeClass();
    }
    
    private void ReqInvoiceList(string MemberNo, string SearchOption)
    {
        ws = new ServiceDepot_DNService();
        RequestInvoiceInfoRequest_DN request;
        RequestInvoiceInfoResponse_DN response;

        try
        {
            response = null;
            request = new RequestInvoiceInfoRequest_DN();
            request.docType = RBLPanel1.Text + "V";
            if (rdnSearchCriteria.SelectedValue == "0")
            {
                request.startDate = DateTime.ParseExact(txtDateFrom.SelectedDate.ToString().Substring(0, 10), "dd/MM/yyyy", null);
                request.endDate = DateTime.ParseExact(txtDateTo.SelectedDate.ToString().Substring(0, 10), "dd/MM/yyyy", null);
            }
            else
            {
                request.invoiceNo = txtInvoiceNo.Text;
            }
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.branchCode = ddlBranchCodeNew.SelectedValue;
            request.contractNo = strClientID; 
            request.username = WebPublication.CaesarSvcUsername;
            request.password = WebPublication.CaesarSvcPassword;
            response = ws.ReqInvoiceList(request);
            StringBuilder sbResponse = new StringBuilder(200);
            StringBuilder sb1 = new StringBuilder(200);

            if (response.status == "0")
            {
                sbResponse.Append("<font size=2><b>Please click on the record below to generate the report:</b><br/><table border=1px cellspacing='0'  bordercolor='silver'  style='font:Arial; cursor:hand; border-collapse:collapse;font-size:11px;  width:90%  ;'><thead  bgcolor='#e6f5ff'><tr><th>Invoice No</th><th>Gen Date</th><th>Contract Year/Month</th><th>Invoice Amount</th><th>Details</th></tr></thead><tbody> ");
                for (int i = 0; i < response.detail.Length; i++)
                {
                    ////sbResponse.Append("<tr ><td>" + response.detail[i].invoiceNo.ToString() + "</td><td>" + String.Format("{0:d}", response.detail[i].docCrtDate) + "</td><td>" + response.detail[i].contYYMM.ToString() + "</td><td>" + response.detail[i].amount.ToString() + "</td><td><a href='newrpt\\" + RBLPanel1.Text + "Invoice.aspx?ID=" + response.detail[i].invoiceNo.ToString().Trim() + "&EN=" + "" + "' target='_blank'  style='cursor:hand;'>View</a></td></tr>");
                    var queryValue = Cryption.Encrypt("ID=" + response.detail[i].invoiceNo.ToString().Trim() + "&EN=");
                    sbResponse.Append("<tr ><td>" + response.detail[i].invoiceNo.ToString() + "</td><td>" + String.Format("{0:d}", response.detail[i].docCrtDate) + "</td><td>" + response.detail[i].contYYMM.ToString() + "</td><td>" + response.detail[i].amount.ToString() + "</td><td><a href='newrpt\\" + RBLPanel1.Text + "Invoice.aspx?val="+ queryValue + "' target='_blank'  style='cursor:hand;'>View</a></td></tr>");
                }
                sbResponse.Append("</table><br/><hr/><font size=1>Dear valuable Customer;  we welcome your inquiries , questions and comments or would like to get more information about statement , invoices & payments or in case of problem , please contact us on <a href='mailto:FinCredControl@bupame.com'>FinCredControl@bupame.com</a> </font>");
                CoverageListReport.Visible = true;
                CoverageListReport.InnerHtml = sbResponse.ToString();
                Message1.Visible = false;
            }
            else
            {
                Message1.Text = response.errorMessage[0];
                CoverageListReport.Visible = false;
                Message1.Text += ""; 
                Message1.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }
    }
}