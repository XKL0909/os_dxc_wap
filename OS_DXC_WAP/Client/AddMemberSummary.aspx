﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Templates/Inner.Master" Inherits="AddMemberSummary" Codebehind="AddMemberSummary.aspx.cs" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>

<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddMemberSummary.aspx.cs" Inherits="AddMemberSummary" %>--%>

 <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/unifiedpolicy.css" rel="stylesheet" />
     <script src="js/jquery.min.js"></script>
    

         <div class="container bb">

            <div class="row bb">
               
                <div class="card bb">
                    <div class="header bb">
                        <h2>Add Member Summary</h2>

                    </div>
                     <div id="divSummary" class="clearfix pull-right margin bb">
                <a href="#" id="backAction" class="btn btn-primary prevBtn btn-sm pull-left bb" type="button">Back</a>
                </div>
                   
                    <div class="body bb">
                        <asp:Repeater ID="rptAddMemberSummary" runat="server">
                            <HeaderTemplate>
                                <table id="rptAddMemberSummary" class="table table-striped table-bordered dt-responsive nowrap margin-top-20 bb">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Submit Date</th>
                                            <th>Refernce Number</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %></td>

                                    <td><%#Eval("Modified", "{0:d}") %></td>
                                    <td><%#Eval("ReferenceNo")%></td>
                                    <td><%#Eval("Status")%></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                              <tfoot>
                                  <tr>
                                      <td colspan="8 bb">
                                          <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0?true:false %>' Text="No item(s) found" />
                                      </td>
                                  </tr>

                              </tfoot>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#divSummary a").click(function () {
                    var url2 = "AddMemberIntermediate.aspx";
                    window.location.href = url2;
                    return false;
                });
            });
        </script>
      <uc2:OSNav ID="OSNav1" runat="server" />
        <br />
        </asp:Content>
