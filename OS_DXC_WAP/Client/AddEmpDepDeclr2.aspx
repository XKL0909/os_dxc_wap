﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" Inherits="AddEmpDepDeclr" Codebehind="AddEmpDepDeclr2.aspx.cs" %>

<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register TagPrefix="BUPA" TagName="uploader" Src="~/Uploader.ascx" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <style>
        .labelClass {
            float: left;
            margin-right: 10px;
            line-height: 30px;
            width: 205px;
        }

        .controlClass {
            float: left;
            margin-right: 10px;
        }

        .clear {
            clear: both;
        }

        .containerClass {
            height: 60px;
            width: 100%;
        }

        .controlClass input, .controlClass select {
            width: 260px;
        }
    </style>
    <script type='text/JavaScript' src='scw.js'></script>
    <script language="jscript" type="text/javascript">
        function checkSaudi(nationality) {
            if (nationality == '966') {
                document.getElementById('txtESponsorId').value = document.getElementById('txtESaudiID').value;

            } else {
                document.getElementById('txtESponsorId').value = '';


            }
        }


    </script>

    <script language="javascript" type="text/javascript">

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
             $('#entryForm2').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
         });

         prm.add_endRequest(function () {
             $('#entryForm').unblock();
             $('#entryForm2').unblock();
         });

         // Progress indicator preloading.
         var preload = document.createElement('img');
         preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
         delete preload;
    </script>

    <fieldset style="padding: 10px">
        <legend>
            <h1>
                <asp:Label ID="lblAdd_Employee" runat="server" Text="Add Employee and Dependent"></asp:Label></h1>
        </legend>

        <table width="99%">
            <tr valign="middle">
                <td></td>
                <td style="text-align: right">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
        </table>

        <div align="right">
            <asp:HyperLink ID="lnkBatchUpload" runat="server" Font-Size="Small" NavigateUrl="~/Client/BatchUpload.aspx?OptionType=AddEmployeeDependent" Target="_blank" Visible="true">Batch Upload</asp:HyperLink></div>

        <br />

        <asp:Label ID="Label18" runat="server" Font-Size="Small" ForeColor="Red" Text="Fields marked with asterisk [ * ]  are mandatory."></asp:Label>

        <%if (uploader.LblUpload.Text.Length > 0)
          { %>
        <br />
        <br />
        <table style="width: 750px; font-size: 17px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
            runat="server" id="Table3">
            <tr>
                <td align="left">
                    <span style="color: #cc0000"><%= uploader.LblUpload.Text%></span>
                </td>
            </tr>
        </table>
        <br />
        <br />

        <% }%>

        <asp:Label ID="Message1" runat="server" Visible="False"></asp:Label>
        <asp:Panel ID="PanelEmployee" runat="server">
            <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
                <tr>
                    <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="7" align="center">
                        <asp:Label ID="lblEr" runat="server" Text="" Font-Bold="true" ForeColor="Red" Font-Size="Large"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="7" style="background-color: #ffffff; height: 25px;" valign="top">
                        <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Employee Details</span></strong>
                    </td>
                </tr>
            </table>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label36" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Member Type"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEMemberType"
                            ClientIDMode="Static" runat="server"
                            DataTextField="mbrTypeDesc" DataValueField="mbrType" BackColor="#eff6fc">
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlEMemberType" InitialValue="0"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>

            <asp:UpdatePanel runat="server" ID="updatePanel1">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="txtESaudiID" />
                </Triggers>
                <ContentTemplate>
                    <div id="entryForm">
                        <div class="containerClass">
                            <div class="labelClass">
                                <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label27" runat="server"
                                    Font-Bold="False" ForeColor="#000036" Text="ID Type"></asp:Label>
                            </div>
                            <div class="controlClass">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEIDType"
                                        ClientIDMode="Static" runat="server"
                                        BackColor="#eff6fc" AutoPostBack="true" OnSelectedIndexChanged="ddlEIDType_SelectedIndexChanged">
                                        <asp:ListItem Value="0">-Select-</asp:ListItem>
                                        <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                        <%--<asp:ListItem Text="Passport" Value="3"></asp:ListItem>--%>
                                        <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="ddlEIDType" InitialValue="0"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="containerClass">
                            <div class="labelClass">
                                <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label6" runat="server"
                                    Font-Bold="False" ForeColor="#000036" Text="Nationality"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                            </div>
                            <div class="controlClass">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ClientIDMode="Static" ID="ddlENationality" Font-Size="9" onChange="addOptions(this.value,'Emp');" runat="server" DataTextField="text" DataValueField="value" BackColor="#eff6fc">
                                        <asp:ListItem Value="">-Select-</asp:ListItem>
                                        <%--<asp:ListItem Value="093">Afghanistan</asp:ListItem>
                                        <asp:ListItem Value="355">Albania</asp:ListItem>
                                        <asp:ListItem Value="213">Algeria</asp:ListItem>
                                        <asp:ListItem Value="684">American Samoa</asp:ListItem>
                                        <asp:ListItem Value="376">Andorra</asp:ListItem>
                                        <asp:ListItem Value="244">Angola</asp:ListItem>
                                        <asp:ListItem Value="101">Anguilla</asp:ListItem>
                                        <asp:ListItem Value="102">Antigua and Barbuda</asp:ListItem>
                                        <asp:ListItem Value="054">Argentine Republic</asp:ListItem>
                                        <asp:ListItem Value="374">Armenia</asp:ListItem>
                                        <asp:ListItem Value="297">Aruba</asp:ListItem>
                                        <asp:ListItem Value="247">Ascension</asp:ListItem>
                                        <asp:ListItem Value="061">Australia</asp:ListItem>
                                        <asp:ListItem Value="672">Australian External Territories</asp:ListItem>
                                        <asp:ListItem Value="043">Austria</asp:ListItem>
                                        <asp:ListItem Value="994">Azerbaijani Republic</asp:ListItem>
                                        <asp:ListItem Value="000">Bedoun</asp:ListItem>
                                        <asp:ListItem Value="103">Bahamas</asp:ListItem>
                                        <asp:ListItem Value="973">Bahrain</asp:ListItem>
                                        <asp:ListItem Value="880">Bangladesh</asp:ListItem>
                                        <asp:ListItem Value="104">Barbados</asp:ListItem>
                                        <asp:ListItem Value="375">Belarus</asp:ListItem>
                                        <asp:ListItem Value="032">Belgium</asp:ListItem>
                                        <asp:ListItem Value="501">Belize</asp:ListItem>
                                        <asp:ListItem Value="229">Benin</asp:ListItem>
                                        <asp:ListItem Value="105">Bermuda</asp:ListItem>
                                        <asp:ListItem Value="975">Bhutan</asp:ListItem>
                                        <asp:ListItem Value="591">Bolivia</asp:ListItem>
                                        <asp:ListItem Value="387">Bosnia and Herzegovina</asp:ListItem>
                                        <asp:ListItem Value="267">Botswana</asp:ListItem>
                                        <asp:ListItem Value="055">Brazil</asp:ListItem>
                                        <asp:ListItem Value="106">British Virgin Islands</asp:ListItem>
                                        <asp:ListItem Value="673">Brunei Darussalam</asp:ListItem>
                                        <asp:ListItem Value="359">Bulgaria</asp:ListItem>
                                        <asp:ListItem Value="226">Burkina Faso</asp:ListItem>
                                        <asp:ListItem Value="257">Burundi</asp:ListItem>
                                        <asp:ListItem Value="855">Cambodia</asp:ListItem>
                                        <asp:ListItem Value="237">Cameroon</asp:ListItem>
                                        <asp:ListItem Value="107">Canada</asp:ListItem>
                                        <asp:ListItem Value="238">Cape Verde</asp:ListItem>
                                        <asp:ListItem Value="108">Cayman Islands</asp:ListItem>
                                        <asp:ListItem Value="236">Central African Republic</asp:ListItem>
                                        <asp:ListItem Value="235">Chad</asp:ListItem>
                                        <asp:ListItem Value="056">Chile</asp:ListItem>
                                        <asp:ListItem Value="086">China</asp:ListItem>
                                        <asp:ListItem Value="057">Colombia</asp:ListItem>
                                        <asp:ListItem Value="269">Comoros</asp:ListItem>
                                        <asp:ListItem Value="242">Congo</asp:ListItem>
                                        <asp:ListItem Value="682">Cook Islands</asp:ListItem>
                                        <asp:ListItem Value="506">Costa Rica</asp:ListItem>
                                        <asp:ListItem Value="225">Cote d'lvoire</asp:ListItem>
                                        <asp:ListItem Value="385">Croatia</asp:ListItem>
                                        <asp:ListItem Value="053">Cuba</asp:ListItem>
                                        <asp:ListItem Value="357">Cyprus</asp:ListItem>
                                        <asp:ListItem Value="420">Czech Republic</asp:ListItem>
                                        <asp:ListItem Value="850">South Korea</asp:ListItem>
                                        <asp:ListItem Value="243">Democratic Republic Congo</asp:ListItem>
                                        <asp:ListItem Value="045">Denmark</asp:ListItem>
                                        <asp:ListItem Value="246">Diego Garcia</asp:ListItem>
                                        <asp:ListItem Value="253">Djibouti</asp:ListItem>
                                        <asp:ListItem Value="109">Dominica</asp:ListItem>
                                        <asp:ListItem Value="110">Dominican Republic</asp:ListItem>
                                        <asp:ListItem Value="593">Ecuador</asp:ListItem>
                                        <asp:ListItem Value="020">Egypt</asp:ListItem>
                                        <asp:ListItem Value="503">El Salvador</asp:ListItem>
                                        <asp:ListItem Value="240">Equatorial Guinea</asp:ListItem>
                                        <asp:ListItem Value="291">Eritrea</asp:ListItem>
                                        <asp:ListItem Value="372">Estonia</asp:ListItem>
                                        <asp:ListItem Value="251">Ethiopia</asp:ListItem>
                                        <asp:ListItem Value="500">Falkland Islands</asp:ListItem>
                                        <asp:ListItem Value="298">Faroe Islands</asp:ListItem>
                                        <asp:ListItem Value="679">Fiji</asp:ListItem>
                                        <asp:ListItem Value="358">Finland</asp:ListItem>
                                        <asp:ListItem Value="033">France</asp:ListItem>
                                        <asp:ListItem Value="594">French Guiana</asp:ListItem>
                                        <asp:ListItem Value="689">French Polynesia</asp:ListItem>
                                        <asp:ListItem Value="241">Garbonese Republic</asp:ListItem>
                                        <asp:ListItem Value="220">Gambia</asp:ListItem>
                                        <asp:ListItem Value="995">Georgia</asp:ListItem>
                                        <asp:ListItem Value="049">Germany</asp:ListItem>
                                        <asp:ListItem Value="233">Ghana</asp:ListItem>
                                        <asp:ListItem Value="350">Gibraltar</asp:ListItem>
                                        <asp:ListItem Value="030">Greece</asp:ListItem>
                                        <asp:ListItem Value="299">Greenland</asp:ListItem>
                                        <asp:ListItem Value="111">Grenada</asp:ListItem>
                                        <asp:ListItem Value="590">Guadeloupe</asp:ListItem>
                                        <asp:ListItem Value="112">Guam</asp:ListItem>
                                        <asp:ListItem Value="502">Guatemala</asp:ListItem>
                                        <asp:ListItem Value="224">Guinea</asp:ListItem>
                                        <asp:ListItem Value="245">Guinea-Bissau</asp:ListItem>
                                        <asp:ListItem Value="592">Guyana</asp:ListItem>
                                        <asp:ListItem Value="509">Haiti</asp:ListItem>
                                        <asp:ListItem Value="504">Honduras</asp:ListItem>
                                        <asp:ListItem Value="852">Hongkong</asp:ListItem>
                                        <asp:ListItem Value="036">Hungary</asp:ListItem>
                                        <asp:ListItem Value="354">Iceland</asp:ListItem>
                                        <asp:ListItem Value="091">India</asp:ListItem>
                                        <asp:ListItem Value="062">Indonesia</asp:ListItem>
                                        <asp:ListItem Value="098">Iran</asp:ListItem>
                                        <asp:ListItem Value="964">Iraq</asp:ListItem>
                                        <asp:ListItem Value="353">Ireland</asp:ListItem>
                                        <asp:ListItem Value="039">Italy</asp:ListItem>
                                        <asp:ListItem Value="113">Jamaica</asp:ListItem>
                                        <asp:ListItem Value="081">Japan</asp:ListItem>
                                        <asp:ListItem Value="962">Jordan</asp:ListItem>
                                        <asp:ListItem Value="701">Kazakstan</asp:ListItem>
                                        <asp:ListItem Value="254">Kenya</asp:ListItem>
                                        <asp:ListItem Value="686">Kiribati</asp:ListItem>
                                        <asp:ListItem Value="082">Korea</asp:ListItem>
                                        <asp:ListItem Value="965">Kuwait</asp:ListItem>
                                        <asp:ListItem Value="996">Kyrgyz Republic</asp:ListItem>
                                        <asp:ListItem Value="856">Lao People's Demcr Republic</asp:ListItem>
                                        <asp:ListItem Value="371">Latvia</asp:ListItem>
                                        <asp:ListItem Value="961">Lebanon</asp:ListItem>
                                        <asp:ListItem Value="266">Lesotho</asp:ListItem>
                                        <asp:ListItem Value="231">Liberia</asp:ListItem>
                                        <asp:ListItem Value="218">Libya</asp:ListItem>
                                        <asp:ListItem Value="423">Liechtenstein</asp:ListItem>
                                        <asp:ListItem Value="370">Lithuania</asp:ListItem>
                                        <asp:ListItem Value="352">Luxembourg</asp:ListItem>
                                        <asp:ListItem Value="853">Macau</asp:ListItem>
                                        <asp:ListItem Value="261">Madagascar</asp:ListItem>
                                        <asp:ListItem Value="265">Malawi</asp:ListItem>
                                        <asp:ListItem Value="060">Malaysia</asp:ListItem>
                                        <asp:ListItem Value="960">Maldives</asp:ListItem>
                                        <asp:ListItem Value="223">Mali</asp:ListItem>
                                        <asp:ListItem Value="356">Malta</asp:ListItem>
                                        <asp:ListItem Value="692">Marshall Islands</asp:ListItem>
                                        <asp:ListItem Value="596">Martinique</asp:ListItem>
                                        <asp:ListItem Value="222">Mauritania</asp:ListItem>
                                        <asp:ListItem Value="230">Mauritius</asp:ListItem>
                                        <asp:ListItem Value="052">Mexico</asp:ListItem>
                                        <asp:ListItem Value="691">Micronesia</asp:ListItem>
                                        <asp:ListItem Value="373">Moldova</asp:ListItem>
                                        <asp:ListItem Value="377">Monaco</asp:ListItem>
                                        <asp:ListItem Value="976">Mongolia</asp:ListItem>
                                        <asp:ListItem Value="114">Montserrat</asp:ListItem>
                                        <asp:ListItem Value="212">Morocco</asp:ListItem>
                                        <asp:ListItem Value="258">Mozambique</asp:ListItem>
                                        <asp:ListItem Value="095">Myanmar</asp:ListItem>
                                        <asp:ListItem Value="264">Namibia</asp:ListItem>
                                        <asp:ListItem Value="674">Nauru</asp:ListItem>
                                        <asp:ListItem Value="977">Nepal</asp:ListItem>
                                        <asp:ListItem Value="031">Netherlands</asp:ListItem>
                                        <asp:ListItem Value="599">Netherlands Antilles</asp:ListItem>
                                        <asp:ListItem Value="687">New Caledonia</asp:ListItem>
                                        <asp:ListItem Value="064">New Zealand</asp:ListItem>
                                        <asp:ListItem Value="505">Nicaragua</asp:ListItem>
                                        <asp:ListItem Value="227">Niger</asp:ListItem>
                                        <asp:ListItem Value="234">Nigeria</asp:ListItem>
                                        <asp:ListItem Value="683">Niue</asp:ListItem>
                                        <asp:ListItem Value="115">Northern Mariana Islands</asp:ListItem>
                                        <asp:ListItem Value="047">Norway</asp:ListItem>
                                        <asp:ListItem Value="968">Oman</asp:ListItem>
                                        <asp:ListItem Value="092">Pakistan</asp:ListItem>
                                        <asp:ListItem Value="680">Palau</asp:ListItem>
                                        <asp:ListItem Value="972">Palestine</asp:ListItem>
                                        <asp:ListItem Value="507">Panama</asp:ListItem>
                                        <asp:ListItem Value="675">Papua New Guinea</asp:ListItem>
                                        <asp:ListItem Value="595">Paraguay</asp:ListItem>
                                        <asp:ListItem Value="051">Peru</asp:ListItem>
                                        <asp:ListItem Value="063">Philippines</asp:ListItem>
                                        <asp:ListItem Value="048">Poland</asp:ListItem>
                                        <asp:ListItem Value="351">Portugal</asp:ListItem>
                                        <asp:ListItem Value="116">Puerto Rico</asp:ListItem>
                                        <asp:ListItem Value="974">Qatar</asp:ListItem>
                                        <asp:ListItem Value="262">Reunion</asp:ListItem>
                                        <asp:ListItem Value="040">Romania</asp:ListItem>
                                        <asp:ListItem Value="702">Russian Federation</asp:ListItem>
                                        <asp:ListItem Value="250">Rwandese Republic</asp:ListItem>
                                        <asp:ListItem Value="290">Saint Helena</asp:ListItem>
                                        <asp:ListItem Value="117">Saint Kitts and Nevis</asp:ListItem>
                                        <asp:ListItem Value="118">Saint Lucia</asp:ListItem>
                                        <asp:ListItem Value="508">Saint Pierre and Miquelon</asp:ListItem>
                                        <asp:ListItem Value="119">Saint Vincent and Grenadines</asp:ListItem>
                                        <asp:ListItem Value="670">Saipan</asp:ListItem>
                                        <asp:ListItem Value="685">Samoa</asp:ListItem>
                                        <asp:ListItem Value="378">San Marino</asp:ListItem>
                                        <asp:ListItem Value="239">Sao Tome and Principe</asp:ListItem>
                                        <asp:ListItem Value="966">Saudi Arabia</asp:ListItem>
                                        <asp:ListItem Value="221">Senegal</asp:ListItem>
                                        <asp:ListItem Value="248">Seychelles</asp:ListItem>
                                        <asp:ListItem Value="232">Sierra Leone</asp:ListItem>
                                        <asp:ListItem Value="065">Singapore</asp:ListItem>
                                        <asp:ListItem Value="421">Slovak Republic</asp:ListItem>
                                        <asp:ListItem Value="386">Slovenia</asp:ListItem>
                                        <asp:ListItem Value="677">Solomon Islands</asp:ListItem>
                                        <asp:ListItem Value="252">Somali Democratic Republic</asp:ListItem>
                                        <asp:ListItem Value="027">South Africa</asp:ListItem>
                                        <asp:ListItem Value="034">Spain</asp:ListItem>
                                        <asp:ListItem Value="094">Sri Lanka</asp:ListItem>
                                        <asp:ListItem Value="249">Sudan</asp:ListItem>
                                        <asp:ListItem Value="597">Suriname</asp:ListItem>
                                        <asp:ListItem Value="268">Swaziland</asp:ListItem>
                                        <asp:ListItem Value="046">Sweden</asp:ListItem>
                                        <asp:ListItem Value="041">Switzerland</asp:ListItem>
                                        <asp:ListItem Value="963">Syrian Arab Republic</asp:ListItem>
                                        <asp:ListItem Value="886">Taiwan</asp:ListItem>
                                        <asp:ListItem Value="992">Tajikistan</asp:ListItem>
                                        <asp:ListItem Value="255">Tanzania</asp:ListItem>
                                        <asp:ListItem Value="066">Thailand</asp:ListItem>
                                        <asp:ListItem Value="389">Former Yugoslav Rep Macedonia</asp:ListItem>
                                        <asp:ListItem Value="671">Tinian</asp:ListItem>
                                        <asp:ListItem Value="228">Togolese Republic</asp:ListItem>
                                        <asp:ListItem Value="690">Tokelau</asp:ListItem>
                                        <asp:ListItem Value="676">Tonga</asp:ListItem>
                                        <asp:ListItem Value="120">Trinidad and Tobago</asp:ListItem>
                                        <asp:ListItem Value="216">Tunisia</asp:ListItem>
                                        <asp:ListItem Value="090">Turkey</asp:ListItem>
                                        <asp:ListItem Value="993">Turkmenistan</asp:ListItem>
                                        <asp:ListItem Value="121">Turks and Caicos Islands</asp:ListItem>
                                        <asp:ListItem Value="688">Tuvalu</asp:ListItem>
                                        <asp:ListItem Value="971">U.A.E.</asp:ListItem>
                                        <asp:ListItem Value="044">U.K.</asp:ListItem>
                                        <asp:ListItem Value="001">U.S.A.</asp:ListItem>
                                        <asp:ListItem Value="256">Uganda</asp:ListItem>
                                        <asp:ListItem Value="380">Ukraine</asp:ListItem>
                                        <asp:ListItem Value="122">United States Virgin Islands</asp:ListItem>
                                        <asp:ListItem Value="598">Uruguay</asp:ListItem>
                                        <asp:ListItem Value="998">Uzbekistan</asp:ListItem>
                                        <asp:ListItem Value="678">Vanuatu</asp:ListItem>
                                        <asp:ListItem Value="379">Vatican City State</asp:ListItem>
                                        <asp:ListItem Value="058">Venezuela</asp:ListItem>
                                        <asp:ListItem Value="084">Vietnam</asp:ListItem>
                                        <asp:ListItem Value="681">Wallis and Futuna</asp:ListItem>
                                        <asp:ListItem Value="967">Yemen</asp:ListItem>
                                        <asp:ListItem Value="381">Yugoslavia</asp:ListItem>
                                        <asp:ListItem Value="260">Zambia</asp:ListItem>
                                        <asp:ListItem Value="263">Zimbabwe</asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="ddlENationality" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="114px">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="containerClass">
                            <div class="labelClass">
                                <span style="color: #ff3300"><span style="color: #ff3300"><strong>*<asp:Label ID="Label7"
                                    runat="server" Font-Bold="False" ForeColor="#000036" Text="Saudi ID/Iqama ID"
                                    Width="106px"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span></span>
                            </div>
                            <div class="controlClass">
                                <asp:TextBox CssClass="textbox" ID="txtESaudiID" runat="server" MaxLength="10"
                                    BackColor="#eff6fc"
                                    onblur="document.getElementById('txtSponsorId').value = document.getElementById('txtESaudiID').value;"
                                    onmouseover="document.getElementById('txtSponsorId').value = document.getElementById('txtESaudiID').value; "
                                    AutoPostBack="false"></asp:TextBox><br />
                                <asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtESaudiID"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="110px">Field is mandatory</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="rfvSuadiID" runat="server" ControlToValidate="txtESaudiID"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{10}"
                                        ValidationGroup="1" Width="142px">Incorrect Saudi/Iqama ID</asp:RegularExpressionValidator>
                                <%--<asp:RegularExpressionValidator
                                                    ID="rfvPassportID" runat="server" Enabled="false" ControlToValidate="txtESaudiID"
                                                    Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[1][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
                                                    ValidationGroup="2" Width="109px">Incorrect Saudi ID</asp:RegularExpressionValidator>--%>
                                <%--<asp:RegularExpressionValidator
                                                    ID="rfvPassportID" runat="server" Enabled="false" ControlToValidate="txtESaudiID"
                                                    Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="([A-Z]|[a-z]|[0-9]){15}"
                                                    ValidationGroup="2" Width="109px">Incorrect Saudi ID</asp:RegularExpressionValidator>--%>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="containerClass">
                            <div class="labelClass">
                                <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label8" runat="server"
                                    Font-Bold="False" ForeColor="#000036" Text="Sponsor Id"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                            </div>
                            <div class="controlClass">
                                <asp:TextBox CssClass="textbox" ID="txtESponsorId" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><br />
                                <asp:RegularExpressionValidator
                                    ID="rfvESponsorID" runat="server" ControlToValidate="txtESponsorId"
                                    Display="Dynamic" ErrorMessage="Incorrect Sponsor ID" ValidationExpression="[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
                                    ValidationGroup="1" Width="119px"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtESponsorId"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="109px">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>
                        <span style="color: #ff3300">*</span>
                        <asp:Label ID="Label28" runat="server" Font-Bold="False"
                        ForeColor="#000036" Text="ID Expiry Date"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtEIDExpDate" runat="server" onfocus="this.blur();"
                        onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox>

                    <br />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtEIDExpDate" SetFocusOnError="true"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="1"
                        Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="142px"></asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <strong><span style="color: #ff3300"></span></strong>
                    <span style="color: #ff3300">*</span>
                    <asp:Label ID="Label29" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Marital Status"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEMaritalStatus"
                            ClientIDMode="Static" runat="server"
                            BackColor="#eff6fc">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ControlToValidate="ddlEMaritalStatus" 
                        InitialValue="0" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" 
                        Width="115px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label4" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Gender" Width="46px"></asp:Label><span
                            style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ClientIDMode="Static" ID="ddlEGender" runat="server" onchange="ValidateTitleGender(document.getElementById('ddlEGender').value, document.getElementById('ddlETitle').value, 'Emp');"
                            BackColor="#eff6fc">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlEGender"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                        Width="109px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label1" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Title"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlETitle"
                            ClientIDMode="Static" runat="server"
                            onchange="ValidateTitleGender(document.getElementById('ddlEGender').value, document.getElementById('ddlETitle').value, 'Emp');"
                            DataTextField="text" DataValueField="value" BackColor="#eff6fc">
                            <asp:ListItem>Dr</asp:ListItem>
                            <asp:ListItem>Eng</asp:ListItem>
                            <asp:ListItem>HH</asp:ListItem>
                            <asp:ListItem>Miss</asp:ListItem>
                            <asp:ListItem Selected="True">Mr</asp:ListItem>
                            <asp:ListItem>Mrs</asp:ListItem>
                            <asp:ListItem>Ms</asp:ListItem>
                            <asp:ListItem>Shk</asp:ListItem>
                            <asp:ListItem>Sir</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="ddlETitle"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label2" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Complete Name (First, Middle, Last)"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:TextBox CssClass="textbox" ID="txtEMemberName" ClientIDMode="Static" runat="server" MaxLength="40" ValidationGroup="1"
                            BackColor="#eff6fc"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtEMemberName"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" ValidationGroup="1"
                        Width="125px">Field is mandatory</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtEMemberName"
                            Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                            Font-Bold="False" ValidationExpression="[a-zA-Z ]+" ValidationGroup="1"></asp:RegularExpressionValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*<asp:Label ID="Label5" runat="server" Font-Bold="False"
                        ForeColor="#000036" Text="Date Of Birth"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:TextBox CssClass="textbox" ID="txtEDateOfBirth" runat="server" onfocus="this.blur();"
                            onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtEDateOfBirth"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="1"
                        Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="142px"></asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label10" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Level of cover"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlELevelOfCover" runat="server">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="1">1-Gold</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="ddlELevelOfCover"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                        Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                        
                        <span style="color: #ff3300">*</span>
                        <asp:Label ID="Label25" runat="server" Font-Bold="False" ForeColor="#000036" Text="Profession" Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList ID="ddProfession" runat="server" BackColor="#eff6fc" OnDataBound="ddProfession_DataBound" DataValueField="profCode" DataTextField="profName"></asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="ddProfession"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="1"
                        Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                        <span style="color: #ff3300">*</span>
                        <asp:Label ID="Label24" runat="server" Font-Bold="False" ForeColor="#000036" Text="District"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList ID="ddDistrict" runat="server" BackColor="#eff6fc" OnDataBound="ddDistrict_DataBound" DataValueField="distCode" DataTextField="distName">
                            <asp:ListItem Value=" "></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="ddDistrict"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="1"
                        Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"></span><span style="color: #ff3300">
                        <span style="color: #ff3300">*</span>
                        <asp:Label ID="Label16" runat="server" Font-Bold="False" ForeColor="#000036" Text="Mobile No"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:TextBox CssClass="textbox" ID="txtEMobileNo" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox></div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                        ControlToValidate="txtEMobileNo" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                        ValidationExpression="[0][5][0-9]{8}" ValidationGroup="1"
                        Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ControlToValidate="txtEMobileNo"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="1"
                            Width="120px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><span style="color: #ff3300"><strong></strong></span><span
                        style="color: #ff3300"><strong></strong></span><asp:Label ID="Label11" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Branch Code"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEBranchCode" runat="server" BackColor="#eff6fc">
                        </asp:DropDownList>
                    </div>
<%--                    <asp:RangeValidator ID="RangeValidator7" runat="server" ErrorMessage="RangeValidator" MinimumValue="1" MaximumValue="999" ControlToValidate="ddlEBranchCode" ValidationGroup="1">Field is mandatory</asp:RangeValidator>--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeptCode" runat="server" ControlToValidate="ddlEBranchCode"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                        Width="120px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">*<asp:Label
                        ID="Label13" runat="server" Font-Bold="False" ForeColor="#000036" Text="Joining date with company"
                        Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtEJoiningDate" runat="server" onfocus="this.blur();"
                        onclick="scwShow(this,event);JoinDateValidation(this.value,'Emp');" Style="cursor: default;"
                        ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtEJoiningDate"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="1"
                        Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="142px"></asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong style="vertical-align: text-top">
                        <span style="vertical-align: top; color: #ff3300"><strong></strong></span>*<asp:Label
                            ID="Label14" runat="server" Font-Bold="False" ForeColor="#000036" Text="Start date for medical cover"
                            Width="158px"></asp:Label></strong></span>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtERequestedStartDate" runat="server" onfocus="this.blur();JoinDateValidation(document.getElementById('txtEJoiningDate').value,'Emp');"
                        onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtERequestedStartDate"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="1"
                        Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="142px"></asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300">
                        <asp:Label ID="empind" runat="server" Text="*" Visible="false"></asp:Label></span><asp:Label ID="Label9" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Employee No"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtEEmployeeNo" runat="server" MaxLength="15" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpNo" runat="server" ControlToValidate="txtEEmployeeNo"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="112px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <asp:Label ID="Label12" runat="server" Font-Bold="False" ForeColor="#000036" Text="Department Code"
                        Width="105px"></asp:Label>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtEDepartmentCode" runat="server" BackColor="White" MaxLength="10"
                        Rows="10"></asp:TextBox>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <asp:CheckBox ID="chkPreviousMember" runat="server" onclick="if(this.checked == true) document.getElementById('RFVldtrPreviousMember').enabled  = true; else  document.getElementById('RFVldtrPreviousMember').enabled  = false;"
                    Text="Has the member or any of its dependents previously been covered by BUPA ?" />
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <asp:Label ID="Label17" runat="server" Font-Bold="False" ForeColor="#000036" Text="Previous Membership No."></asp:Label>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtPreviousMember" runat="server" MaxLength="8" ValidationGroup="1"
                        BackColor="White"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RFVldtrPreviousMember"
                        runat="server" ControlToValidate="txtPreviousMember" Display="Dynamic" Enabled="false"
                        ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
<%--                    <asp:RegularExpressionValidator
                            ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPreviousMember"
                            Display="Dynamic" ValidationExpression="[3|4|5][0-9][0-9][0-9][0-9][0-9][0-9]"
                            ErrorMessage="RegularExpressionValidator" ValidationGroup="1" Width="149px">Incorrect Membership No.</asp:RegularExpressionValidator>--%>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <asp:Label ID="Label15" runat="server" Font-Bold="False" ForeColor="#000036" Text="Join reason"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEJoinReason" onChange="JoinDateValidation(this.value,'Emp');"
                            runat="server" Font-Names="Arial" Font-Size="Small">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="RJOIN001">Member is studying outside Kingdom</asp:ListItem>
                            <asp:ListItem Value="RJOIN002">Dependent is studying outside Kingdom</asp:ListItem>
                            <asp:ListItem Value="RJOIN003">Customer forgot to add member from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN004">Customer forgot to add dependent from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN005">Main member / father forgot to add dependent from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN006">End of probationary period</asp:ListItem>
                            <asp:ListItem Value="RJOIN007">Employee is ill and needs treatment</asp:ListItem>
                            <asp:ListItem Value="RJOIN008">Dependent is ill and needs treatment</asp:ListItem>
                            <asp:ListItem Value="RJOIN009">Member was on long vacation</asp:ListItem>
                            <asp:ListItem Value="RJOIN010">Member promoted to higher grade which is entitled to add dependent/s</asp:ListItem>
                            <asp:ListItem Value="RJOIN011">Newly Hired Employee</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RFVldtrEJoinReason" InitialValue="0" runat="server"
                        ControlToValidate="ddlEJoinReason" ErrorMessage="RequiredFieldValidator" Font-Names="Verdana"
                        Enabled="false" ValidationGroup="1" Display="Dynamic" Font-Size="Small" Text="Join Reason is mandatory"
                        Width="176px"></asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/xmlfiles/Title.xml"></asp:XmlDataSource>
            <asp:XmlDataSource ID="XmlDataSource2" runat="server" DataFile="~/xmlfiles/Nationality.xml"></asp:XmlDataSource>
        </asp:Panel>
        <br />
        <asp:Panel ID="PanelMembershipNoOnly" runat="server" Visible="false">
            <table style="font-size: small; width: 396px;">
                <tr>
                    <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="2">
                        <strong><span style="font-size: 11pt; color: #0099ff">Employee Details</span></strong>
                    </td>
                    <td style="background-color: #ffffff; height: 25px;" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 122px" valign="top">
                        <nobr><STRONG><SPAN style="COLOR: #ff3300">*</SPAN></STRONG><asp:Label id="lblMainMemberNo" runat="server" Text="Membership Number" Width="128px"></asp:Label></nobr>
                    </td>
                    <td style="width: 288px; margin-left: 40px;" valign="top">
                        <asp:TextBox CssClass="textbox" ID="txtMainMemberNo" runat="server" Width="164px" MaxLength="8" ValidationGroup="1"
                            BackColor="#eff6fc" OnTextChanged="txtMainMemberNo_TextChanged"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMainMemberNo"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="250px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                     
                    </td>
                    <td style="width: 288px" valign="top">
                        <asp:Button CssClass="submitButton" ID="Button1" runat="server" OnClick="Button1_Click1"
                            Text="Get Details" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" valign="top">
                        <asp:Label ID="lblError" runat="server" Font-Names="Arial" Font-Size="Small"
                            ForeColor="Red" Width="527px"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
            Font-Names="Arial" OnRowDeleting="GridView1_RowDeleting" Font-Size="11px" ForeColor="#333333"
            GridLines="None" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound"
            OnSelectedIndexChanged="GridView1_SelectedIndexChanged" Width="635px">
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="title" HeaderText="Title">
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="CompleteName" HeaderText="Complete Name">
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="RelationShip" HeaderText="Relation">
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="DOB" DataFormatString="{0:d MMM yyyy }" HeaderText="Date of Birth"
                    HtmlEncode="False">
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" Wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="LevelCover" HeaderText="Level of Cover">
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="RequestStartDate" HeaderText="Start Date">
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="SponsorID" HeaderText="Sponsor ID">
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:BoundField DataField="EmployeeNo" HeaderText="Employee No">
                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                </asp:BoundField>
                <asp:CommandField ShowSelectButton="True" SelectText="Edit">
                    <HeaderStyle BackColor="White" BorderColor="White" />
                    <ItemStyle Width="30px" HorizontalAlign="Center" BackColor="White" BorderColor="White" />
                </asp:CommandField>
                <%--                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="DeleteRow"
                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                Text="Delete" ></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderStyle BackColor="White" BorderColor="White" />
                        <ItemStyle  Width="30px" HorizontalAlign="Center"  BackColor="White" BorderColor="White" />
                    </asp:TemplateField>--%>
                <asp:CommandField HeaderText="" ShowDeleteButton="True">
                    <HeaderStyle BackColor="White" BorderColor="White" />
                    <ItemStyle Width="30px" HorizontalAlign="Center" BackColor="White" BorderColor="White" />
                </asp:CommandField>
            </Columns>
            <RowStyle BackColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <SelectedRowStyle BackColor="#4CAC27" BorderStyle="None" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#E6F5FF" Font-Bold="True" ForeColor="#404040" Font-Names="Arial"
                Font-Size="9pt" Font-Strikeout="False" />
        </asp:GridView>
        <br />
        <asp:Panel ID="PanelDependent" runat="server" Visible="false">
            <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial" runat="server" id="dependentTab">
                <tr>
                    <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="7" align="center">
                        <asp:Label ID="lbler1" runat="server" Text="" Font-Bold="true" ForeColor="Red" Font-Size="Large"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="7" style="background-color: #ffffff; height: 25px;" valign="top">
                        <span style="font-size: 11pt; color: #0099ff"><strong>Dependent Details</strong></span>
                    </td>
                </tr>
            </table>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label30" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Member Type"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlMemberType"
                            ClientIDMode="Static" runat="server"
                            DataTextField="mbrTypeDesc" DataValueField="mbrType" BackColor="#eff6fc">
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ControlToValidate="ddlMemberType" InitialValue="0"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <asp:UpdatePanel runat="server" ID="updatePanel2">
                <ContentTemplate>
                    <div id="entryForm2">
                        <div class="containerClass">
                            <div class="labelClass">
                                <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label31" runat="server"
                                    Font-Bold="False" ForeColor="#000036" Text="ID Type"></asp:Label>
                            </div>
                            <div class="controlClass">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlIDType"
                                        ClientIDMode="Static" runat="server"
                                        BackColor="#eff6fc" AutoPostBack="true" OnSelectedIndexChanged="ddlIDType_SelectedIndexChanged">
                                        <asp:ListItem Value="0">-Select-</asp:ListItem>
                                        <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                        <%--<asp:ListItem Text="Passport" Value="3"></asp:ListItem>--%>
                                        <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="ddlIDType" InitialValue="0"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblNationality"
                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Nationality"></asp:Label><span
                            style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlNationality" runat="server" BackColor="#eff6fc" onChange="addOptions(this.value,'Dep');" DataTextField="text" DataValueField="value">
                            <asp:ListItem Value="">-Select-</asp:ListItem>
                            <%--<asp:ListItem Value="093">Afghanistan</asp:ListItem>
                            <asp:ListItem Value="355">Albania</asp:ListItem>
                            <asp:ListItem Value="213">Algeria</asp:ListItem>
                            <asp:ListItem Value="684">American Samoa</asp:ListItem>
                            <asp:ListItem Value="376">Andorra</asp:ListItem>
                            <asp:ListItem Value="244">Angola</asp:ListItem>
                            <asp:ListItem Value="101">Anguilla</asp:ListItem>
                            <asp:ListItem Value="102">Antigua and Barbuda</asp:ListItem>
                            <asp:ListItem Value="054">Argentine Republic</asp:ListItem>
                            <asp:ListItem Value="374">Armenia</asp:ListItem>
                            <asp:ListItem Value="297">Aruba</asp:ListItem>
                            <asp:ListItem Value="247">Ascension</asp:ListItem>
                            <asp:ListItem Value="061">Australia</asp:ListItem>
                            <asp:ListItem Value="672">Australian External Territories</asp:ListItem>
                            <asp:ListItem Value="043">Austria</asp:ListItem>
                            <asp:ListItem Value="994">Azerbaijani Republic</asp:ListItem>
                            <asp:ListItem Value="000">Bedoun</asp:ListItem>
                            <asp:ListItem Value="103">Bahamas</asp:ListItem>
                            <asp:ListItem Value="973">Bahrain</asp:ListItem>
                            <asp:ListItem Value="880">Bangladesh</asp:ListItem>
                            <asp:ListItem Value="104">Barbados</asp:ListItem>
                            <asp:ListItem Value="375">Belarus</asp:ListItem>
                            <asp:ListItem Value="032">Belgium</asp:ListItem>
                            <asp:ListItem Value="501">Belize</asp:ListItem>
                            <asp:ListItem Value="229">Benin</asp:ListItem>
                            <asp:ListItem Value="105">Bermuda</asp:ListItem>
                            <asp:ListItem Value="975">Bhutan</asp:ListItem>
                            <asp:ListItem Value="591">Bolivia</asp:ListItem>
                            <asp:ListItem Value="387">Bosnia and Herzegovina</asp:ListItem>
                            <asp:ListItem Value="267">Botswana</asp:ListItem>
                            <asp:ListItem Value="055">Brazil</asp:ListItem>
                            <asp:ListItem Value="106">British Virgin Islands</asp:ListItem>
                            <asp:ListItem Value="673">Brunei Darussalam</asp:ListItem>
                            <asp:ListItem Value="359">Bulgaria</asp:ListItem>
                            <asp:ListItem Value="226">Burkina Faso</asp:ListItem>
                            <asp:ListItem Value="257">Burundi</asp:ListItem>
                            <asp:ListItem Value="855">Cambodia</asp:ListItem>
                            <asp:ListItem Value="237">Cameroon</asp:ListItem>
                            <asp:ListItem Value="107">Canada</asp:ListItem>
                            <asp:ListItem Value="238">Cape Verde</asp:ListItem>
                            <asp:ListItem Value="108">Cayman Islands</asp:ListItem>
                            <asp:ListItem Value="236">Central African Republic</asp:ListItem>
                            <asp:ListItem Value="235">Chad</asp:ListItem>
                            <asp:ListItem Value="056">Chile</asp:ListItem>
                            <asp:ListItem Value="086">China</asp:ListItem>
                            <asp:ListItem Value="057">Colombia</asp:ListItem>
                            <asp:ListItem Value="269">Comoros</asp:ListItem>
                            <asp:ListItem Value="242">Congo</asp:ListItem>
                            <asp:ListItem Value="682">Cook Islands</asp:ListItem>
                            <asp:ListItem Value="506">Costa Rica</asp:ListItem>
                            <asp:ListItem Value="225">Cote d'lvoire</asp:ListItem>
                            <asp:ListItem Value="385">Croatia</asp:ListItem>
                            <asp:ListItem Value="053">Cuba</asp:ListItem>
                            <asp:ListItem Value="357">Cyprus</asp:ListItem>
                            <asp:ListItem Value="420">Czech Republic</asp:ListItem>
                            <asp:ListItem Value="850">South Korea</asp:ListItem>
                            <asp:ListItem Value="243">Democratic Republic Congo</asp:ListItem>
                            <asp:ListItem Value="045">Denmark</asp:ListItem>
                            <asp:ListItem Value="246">Diego Garcia</asp:ListItem>
                            <asp:ListItem Value="253">Djibouti</asp:ListItem>
                            <asp:ListItem Value="109">Dominica</asp:ListItem>
                            <asp:ListItem Value="110">Dominican Republic</asp:ListItem>
                            <asp:ListItem Value="593">Ecuador</asp:ListItem>
                            <asp:ListItem Value="020">Egypt</asp:ListItem>
                            <asp:ListItem Value="503">El Salvador</asp:ListItem>
                            <asp:ListItem Value="240">Equatorial Guinea</asp:ListItem>
                            <asp:ListItem Value="291">Eritrea</asp:ListItem>
                            <asp:ListItem Value="372">Estonia</asp:ListItem>
                            <asp:ListItem Value="251">Ethiopia</asp:ListItem>
                            <asp:ListItem Value="500">Falkland Islands</asp:ListItem>
                            <asp:ListItem Value="298">Faroe Islands</asp:ListItem>
                            <asp:ListItem Value="679">Fiji</asp:ListItem>
                            <asp:ListItem Value="358">Finland</asp:ListItem>
                            <asp:ListItem Value="033">France</asp:ListItem>
                            <asp:ListItem Value="594">French Guiana</asp:ListItem>
                            <asp:ListItem Value="689">French Polynesia</asp:ListItem>
                            <asp:ListItem Value="241">Garbonese Republic</asp:ListItem>
                            <asp:ListItem Value="220">Gambia</asp:ListItem>
                            <asp:ListItem Value="995">Georgia</asp:ListItem>
                            <asp:ListItem Value="049">Germany</asp:ListItem>
                            <asp:ListItem Value="233">Ghana</asp:ListItem>
                            <asp:ListItem Value="350">Gibraltar</asp:ListItem>
                            <asp:ListItem Value="030">Greece</asp:ListItem>
                            <asp:ListItem Value="299">Greenland</asp:ListItem>
                            <asp:ListItem Value="111">Grenada</asp:ListItem>
                            <asp:ListItem Value="590">Guadeloupe</asp:ListItem>
                            <asp:ListItem Value="112">Guam</asp:ListItem>
                            <asp:ListItem Value="502">Guatemala</asp:ListItem>
                            <asp:ListItem Value="224">Guinea</asp:ListItem>
                            <asp:ListItem Value="245">Guinea-Bissau</asp:ListItem>
                            <asp:ListItem Value="592">Guyana</asp:ListItem>
                            <asp:ListItem Value="509">Haiti</asp:ListItem>
                            <asp:ListItem Value="504">Honduras</asp:ListItem>
                            <asp:ListItem Value="852">Hongkong</asp:ListItem>
                            <asp:ListItem Value="036">Hungary</asp:ListItem>
                            <asp:ListItem Value="354">Iceland</asp:ListItem>
                            <asp:ListItem Value="091">India</asp:ListItem>
                            <asp:ListItem Value="062">Indonesia</asp:ListItem>
                            <asp:ListItem Value="098">Iran</asp:ListItem>
                            <asp:ListItem Value="964">Iraq</asp:ListItem>
                            <asp:ListItem Value="353">Ireland</asp:ListItem>
                            <asp:ListItem Value="039">Italy</asp:ListItem>
                            <asp:ListItem Value="113">Jamaica</asp:ListItem>
                            <asp:ListItem Value="081">Japan</asp:ListItem>
                            <asp:ListItem Value="962">Jordan</asp:ListItem>
                            <asp:ListItem Value="701">Kazakstan</asp:ListItem>
                            <asp:ListItem Value="254">Kenya</asp:ListItem>
                            <asp:ListItem Value="686">Kiribati</asp:ListItem>
                            <asp:ListItem Value="082">Korea</asp:ListItem>
                            <asp:ListItem Value="965">Kuwait</asp:ListItem>
                            <asp:ListItem Value="996">Kyrgyz Republic</asp:ListItem>
                            <asp:ListItem Value="856">Lao People's Demcr Republic</asp:ListItem>
                            <asp:ListItem Value="371">Latvia</asp:ListItem>
                            <asp:ListItem Value="961">Lebanon</asp:ListItem>
                            <asp:ListItem Value="266">Lesotho</asp:ListItem>
                            <asp:ListItem Value="231">Liberia</asp:ListItem>
                            <asp:ListItem Value="218">Libya</asp:ListItem>
                            <asp:ListItem Value="423">Liechtenstein</asp:ListItem>
                            <asp:ListItem Value="370">Lithuania</asp:ListItem>
                            <asp:ListItem Value="352">Luxembourg</asp:ListItem>
                            <asp:ListItem Value="853">Macau</asp:ListItem>
                            <asp:ListItem Value="261">Madagascar</asp:ListItem>
                            <asp:ListItem Value="265">Malawi</asp:ListItem>
                            <asp:ListItem Value="060">Malaysia</asp:ListItem>
                            <asp:ListItem Value="960">Maldives</asp:ListItem>
                            <asp:ListItem Value="223">Mali</asp:ListItem>
                            <asp:ListItem Value="356">Malta</asp:ListItem>
                            <asp:ListItem Value="692">Marshall Islands</asp:ListItem>
                            <asp:ListItem Value="596">Martinique</asp:ListItem>
                            <asp:ListItem Value="222">Mauritania</asp:ListItem>
                            <asp:ListItem Value="230">Mauritius</asp:ListItem>
                            <asp:ListItem Value="052">Mexico</asp:ListItem>
                            <asp:ListItem Value="691">Micronesia</asp:ListItem>
                            <asp:ListItem Value="373">Moldova</asp:ListItem>
                            <asp:ListItem Value="377">Monaco</asp:ListItem>
                            <asp:ListItem Value="976">Mongolia</asp:ListItem>
                            <asp:ListItem Value="114">Montserrat</asp:ListItem>
                            <asp:ListItem Value="212">Morocco</asp:ListItem>
                            <asp:ListItem Value="258">Mozambique</asp:ListItem>
                            <asp:ListItem Value="095">Myanmar</asp:ListItem>
                            <asp:ListItem Value="264">Namibia</asp:ListItem>
                            <asp:ListItem Value="674">Nauru</asp:ListItem>
                            <asp:ListItem Value="977">Nepal</asp:ListItem>
                            <asp:ListItem Value="031">Netherlands</asp:ListItem>
                            <asp:ListItem Value="599">Netherlands Antilles</asp:ListItem>
                            <asp:ListItem Value="687">New Caledonia</asp:ListItem>
                            <asp:ListItem Value="064">New Zealand</asp:ListItem>
                            <asp:ListItem Value="505">Nicaragua</asp:ListItem>
                            <asp:ListItem Value="227">Niger</asp:ListItem>
                            <asp:ListItem Value="234">Nigeria</asp:ListItem>
                            <asp:ListItem Value="683">Niue</asp:ListItem>
                            <asp:ListItem Value="115">Northern Mariana Islands</asp:ListItem>
                            <asp:ListItem Value="047">Norway</asp:ListItem>
                            <asp:ListItem Value="968">Oman</asp:ListItem>
                            <asp:ListItem Value="092">Pakistan</asp:ListItem>
                            <asp:ListItem Value="680">Palau</asp:ListItem>
                            <asp:ListItem Value="972">Palestine</asp:ListItem>
                            <asp:ListItem Value="507">Panama</asp:ListItem>
                            <asp:ListItem Value="675">Papua New Guinea</asp:ListItem>
                            <asp:ListItem Value="595">Paraguay</asp:ListItem>
                            <asp:ListItem Value="051">Peru</asp:ListItem>
                            <asp:ListItem Value="063">Philippines</asp:ListItem>
                            <asp:ListItem Value="048">Poland</asp:ListItem>
                            <asp:ListItem Value="351">Portugal</asp:ListItem>
                            <asp:ListItem Value="116">Puerto Rico</asp:ListItem>
                            <asp:ListItem Value="974">Qatar</asp:ListItem>
                            <asp:ListItem Value="262">Reunion</asp:ListItem>
                            <asp:ListItem Value="040">Romania</asp:ListItem>
                            <asp:ListItem Value="702">Russian Federation</asp:ListItem>
                            <asp:ListItem Value="250">Rwandese Republic</asp:ListItem>
                            <asp:ListItem Value="290">Saint Helena</asp:ListItem>
                            <asp:ListItem Value="117">Saint Kitts and Nevis</asp:ListItem>
                            <asp:ListItem Value="118">Saint Lucia</asp:ListItem>
                            <asp:ListItem Value="508">Saint Pierre and Miquelon</asp:ListItem>
                            <asp:ListItem Value="119">Saint Vincent and Grenadines</asp:ListItem>
                            <asp:ListItem Value="670">Saipan</asp:ListItem>
                            <asp:ListItem Value="685">Samoa</asp:ListItem>
                            <asp:ListItem Value="378">San Marino</asp:ListItem>
                            <asp:ListItem Value="239">Sao Tome and Principe</asp:ListItem>
                            <asp:ListItem Value="966">Saudi Arabia</asp:ListItem>
                            <asp:ListItem Value="221">Senegal</asp:ListItem>
                            <asp:ListItem Value="248">Seychelles</asp:ListItem>
                            <asp:ListItem Value="232">Sierra Leone</asp:ListItem>
                            <asp:ListItem Value="065">Singapore</asp:ListItem>
                            <asp:ListItem Value="421">Slovak Republic</asp:ListItem>
                            <asp:ListItem Value="386">Slovenia</asp:ListItem>
                            <asp:ListItem Value="677">Solomon Islands</asp:ListItem>
                            <asp:ListItem Value="252">Somali Democratic Republic</asp:ListItem>
                            <asp:ListItem Value="027">South Africa</asp:ListItem>
                            <asp:ListItem Value="034">Spain</asp:ListItem>
                            <asp:ListItem Value="094">Sri Lanka</asp:ListItem>
                            <asp:ListItem Value="249">Sudan</asp:ListItem>
                            <asp:ListItem Value="597">Suriname</asp:ListItem>
                            <asp:ListItem Value="268">Swaziland</asp:ListItem>
                            <asp:ListItem Value="046">Sweden</asp:ListItem>
                            <asp:ListItem Value="041">Switzerland</asp:ListItem>
                            <asp:ListItem Value="963">Syrian Arab Republic</asp:ListItem>
                            <asp:ListItem Value="886">Taiwan</asp:ListItem>
                            <asp:ListItem Value="992">Tajikistan</asp:ListItem>
                            <asp:ListItem Value="255">Tanzania</asp:ListItem>
                            <asp:ListItem Value="066">Thailand</asp:ListItem>
                            <asp:ListItem Value="389">Former Yugoslav Rep Macedonia</asp:ListItem>
                            <asp:ListItem Value="671">Tinian</asp:ListItem>
                            <asp:ListItem Value="228">Togolese Republic</asp:ListItem>
                            <asp:ListItem Value="690">Tokelau</asp:ListItem>
                            <asp:ListItem Value="676">Tonga</asp:ListItem>
                            <asp:ListItem Value="120">Trinidad and Tobago</asp:ListItem>
                            <asp:ListItem Value="216">Tunisia</asp:ListItem>
                            <asp:ListItem Value="090">Turkey</asp:ListItem>
                            <asp:ListItem Value="993">Turkmenistan</asp:ListItem>
                            <asp:ListItem Value="121">Turks and Caicos Islands</asp:ListItem>
                            <asp:ListItem Value="688">Tuvalu</asp:ListItem>
                            <asp:ListItem Value="971">U.A.E.</asp:ListItem>
                            <asp:ListItem Value="044">U.K.</asp:ListItem>
                            <asp:ListItem Value="001">U.S.A.</asp:ListItem>
                            <asp:ListItem Value="256">Uganda</asp:ListItem>
                            <asp:ListItem Value="380">Ukraine</asp:ListItem>
                            <asp:ListItem Value="122">United States Virgin Islands</asp:ListItem>
                            <asp:ListItem Value="598">Uruguay</asp:ListItem>
                            <asp:ListItem Value="998">Uzbekistan</asp:ListItem>
                            <asp:ListItem Value="678">Vanuatu</asp:ListItem>
                            <asp:ListItem Value="379">Vatican City State</asp:ListItem>
                            <asp:ListItem Value="058">Venezuela</asp:ListItem>
                            <asp:ListItem Value="084">Vietnam</asp:ListItem>
                            <asp:ListItem Value="681">Wallis and Futuna</asp:ListItem>
                            <asp:ListItem Value="967">Yemen</asp:ListItem>
                            <asp:ListItem Value="381">Yugoslavia</asp:ListItem>
                            <asp:ListItem Value="260">Zambia</asp:ListItem>
                            <asp:ListItem Value="263">Zimbabwe</asp:ListItem>--%>

                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlNationality" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="111px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            
                        <div class="containerClass">
                            <div class="labelClass">
                                <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="lblSaudiID"
                                    runat="server" Font-Bold="False" ForeColor="#000036" Text="Saudi ID/Iqama ID"
                                    Width="106px"></asp:Label>
                            </div>
                            <div class="controlClass">
                                <asp:TextBox CssClass="textbox" ID="txtSaudiID" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><asp:Label
                                    ID="lblSaudiNote" runat="server" Font-Bold="False" ForeColor="Red" Text=" (Must be different than the employee's Saudi Id/Iqama Id)"
                                    Visible="False"></asp:Label>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidatorDepID"
                                                runat="server" ControlToValidate="txtSaudiID" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                                                ValidationExpression="\d{10}" ValidationGroup="2" Width="152px">Incorrect Saudi/Iqama ID</asp:RegularExpressionValidator><asp:RegularExpressionValidator
                                                    ID="VldtrSaudiID" runat="server" Enabled="false" ControlToValidate="txtSaudiID"
                                                    Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[1][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
                                                    ValidationGroup="2" Width="152px">Incorrect Saudi ID</asp:RegularExpressionValidator>--%>
                                <br />
                                <asp:RegularExpressionValidator
                                    ID="rfvDepSuadiID" runat="server" ControlToValidate="txtSaudiID"
                                    Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{10}"
                                    ValidationGroup="2" Width="142px">Incorrect Saudi/Iqama ID</asp:RegularExpressionValidator>

                                <asp:Label ID="lblIDVal" ForeColor="Red" runat="server" Text="Field is mandatory" Visible="false"></asp:Label>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="containerClass">
                            <div class="labelClass">
                                <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblSponsorId"
                                    runat="server" Font-Bold="False" ForeColor="#000036" Text="Sponsor Id"></asp:Label><span
                                        style="color: #ff3300"><strong></strong></span>
                            </div>
                            <div class="controlClass">
                                <asp:TextBox CssClass="textbox" ID="txtSponsorId" runat="server" MaxLength="10"
                                    BackColor="#eff6fc"></asp:TextBox>
                                <font color="red">Sponsor ID has to be the main member’s Saudi ID/Iqama number.</font><br />
                                <asp:RegularExpressionValidator
                                    ID="rfvDepSponsorID" runat="server" ControlToValidate="txtSponsorId"
                                    Display="Dynamic" ErrorMessage="Incorrect Sponsor ID" ValidationExpression="[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
                                    ValidationGroup="2" Width="129px"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtSponsorId"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="119px">Field is mandatory</asp:RequiredFieldValidator>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>
                        <span style="color: #ff3300">*</span>
                        <asp:Label ID="Label32" runat="server" Font-Bold="False"
                        ForeColor="#000036" Text="ID Expiry Date"></asp:Label></strong><span style="color: #ff3300">
                            <strong></strong></span></span>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtIDExpDate" runat="server" onfocus="this.blur();"
                        onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox>
                    <br />
<%--                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator31" runat="server" ControlToValidate="txtIDExpDate"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana"  ValidationGroup="2"
                        Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="142px"></asp:RequiredFieldValidator>--%>
                    <asp:Label ID="lblIDExpiryVal" ForeColor="Red" runat="server" Text="Field is mandatory" Visible="false"></asp:Label>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <strong><span style="color: #ff3300"></span></strong>
                    <span style="color: #ff3300">*</span>
                    <asp:Label ID="Label33" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Marital Status"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlMaritalStatus"
                            ClientIDMode="Static" runat="server"
                            BackColor="#eff6fc">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ControlToValidate="ddlMaritalStatus" InitialValue="0"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong><asp:Label ID="lblGender" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Gender" Width="46px"></asp:Label><span
                            style="color: #ff3300"><strong></strong></span></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlGender" runat="server" BackColor="#eff6fc"
                            onchange="ValidateTitleGender(document.getElementById('ddlGender').value, document.getElementById('ddlTitle').value, 'Dep');">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlGender"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                        Width="109px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblTitle" runat="server"
                        Font-Bold="False" ForeColor="#000036" Text="Title"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlTitle" runat="server" BackColor="#eff6fc"
                            onchange="ValidateTitleGender(document.getElementById('ddlGender').value, document.getElementById('ddlTitle').value, 'Dep');"
                            DataTextField="text" DataValueField="value">
                            <asp:ListItem>Dr</asp:ListItem>
                            <asp:ListItem>Eng</asp:ListItem>
                            <asp:ListItem>HH</asp:ListItem>
                            <asp:ListItem>Miss</asp:ListItem>
                            <asp:ListItem Selected="True">Mr</asp:ListItem>
                            <asp:ListItem>Mrs</asp:ListItem>
                            <asp:ListItem>Ms</asp:ListItem>
                            <asp:ListItem>Shk</asp:ListItem>
                            <asp:ListItem>Sir</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTitle"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="110px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="lblCompleteName"
                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Complete Name (First, Middle, Last)"></asp:Label>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtMemberName" runat="server" MaxLength="40" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMemberName"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" ValidationGroup="2"
                        Width="158px">Field is mandatory</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtMemberName"
                            Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                            Font-Bold="False" ValidationExpression="[a-zA-Z ]+" ValidationGroup="2"></asp:RegularExpressionValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><span style="color: #ff3300"><strong>*</strong><asp:Label
                        ID="lblDateOfBirth" runat="server" Font-Bold="False" ForeColor="#000036" Text="Date Of Birth"></asp:Label><span
                            style="color: #ff3300"><strong></strong></span></span></span>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtDateOfBirth" runat="server" onfocus="this.blur();"
                        onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtDateOfBirth"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="2"
                        Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="145px"></asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblLevelOfCover"
                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Level of cover"></asp:Label><span
                            style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlLevelOfCover" runat="server" BackColor="#eff6fc">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="1">1-Gold</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlLevelOfCover"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                        Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong></strong></span><strong><span style="color: #ff3300">
                        <span style="color: #ff3300">*</span>
                        <asp:Label ID="Label35" runat="server" Font-Bold="False" ForeColor="#000036" Text="Profession" Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList ID="ddDepProfession" runat="server" BackColor="#eff6fc" OnDataBound="ddProfession_DataBound" DataValueField="profCode" DataTextField="profName"></asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ControlToValidate="ddDepProfession"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="2"
                        Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                        <span style="color: #ff3300">*</span>
                        <asp:Label ID="Label34" runat="server" Font-Bold="False" ForeColor="#000036" Text="District"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList ID="ddDepDistrict" runat="server" BackColor="#eff6fc" OnDataBound="ddDistrict_DataBound" DataValueField="distCode" DataTextField="distName">
                            <asp:ListItem Value=" "></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ControlToValidate="ddDepDistrict"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="2"
                        Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong></strong></span>
                    <span style="color: #ff3300">*</span>
                    <asp:Label ID="lblMobileNo" runat="server" Font-Bold="False" ForeColor="#000036"
                        Text="Mobile No"></asp:Label>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtMobileNo" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RegularExpressionValidator
                        ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobileNo"
                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[0][5][0-9]{8}"
                        ValidationGroup="2" Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ControlToValidate="txtMobileNo"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="2"
                            Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"></span><strong><span style="color: #ff3300">*<asp:Label
                        ID="lblJoinDate" runat="server" Font-Bold="False" ForeColor="#000036" Text="Arrival date in KSA"></asp:Label></span></strong>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtJoiningDate" runat="server" onclick="scwShow(this,event);JoinDateValidation(this.value,'Dep');"
                        onlostfocus="JoinDateValidation(this.value, 'Dep');" onmouseover="JoinDateValidation(this.value, 'Dep');"
                        onfocus="JoinDateValidation(this.value, 'Dep');this.blur();" Style="cursor: default;"
                        ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator23" runat="server" ControlToValidate="txtJoiningDate"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="2"
                        Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="151px"></asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong style="vertical-align: text-top">
                        <span style="vertical-align: top; color: #ff3300"><strong></strong></span>*<asp:Label
                            ID="lblRequestedStartDate" runat="server" Font-Bold="False" ForeColor="#000036"
                            Text="Start date for medical cover" Width="160px"></asp:Label></strong></span>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtRequestedStartDate" runat="server" onfocus="this.blur();JoinDateValidation(document.getElementById('txtJoiningDate').value,'Dep');"
                        onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator24" runat="server" ControlToValidate="txtRequestedStartDate"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="2"
                        Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="147px"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator"
                        ControlToCompare="txtDateOfBirth" ControlToValidate="txtRequestedStartDate" Operator="GreaterThanEqual"
                        Type="Date" ValidationGroup="2" Display="Dynamic">Start Date cannot be earlier than Birth Date</asp:CompareValidator>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300">
                        <asp:Label ID="empind1" runat="server" Text="*" Visible="false"></asp:Label></span><asp:Label ID="lblEmployeeNo"
                            runat="server" Font-Bold="False" ForeColor="#000036" Text="Employee No" Width="83px"></asp:Label><span
                                style="color: #ff3300"><strong></strong></span>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtEmployeeNo" runat="server" MaxLength="15" BackColor="#eff6fc"></asp:TextBox><br />
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidatorEmpNoDep" runat="server" ControlToValidate="txtEmployeeNo"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="112px">Field is mandatory</asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <%--<div class="containerClass">
                <div class="labelClass">
                    <span style="color: #ff3300"><strong>*</strong></span><span style="color: #ff3300"><strong></strong></span><span
                            style="color: #ff3300"><strong></strong></span><asp:Label ID="lblRelationShip" runat="server"
                                Font-Bold="False" ForeColor="#000036" Text="Relationship"></asp:Label>
                </div>
                <div class="controlClass">
                       <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass" ID="ddlRelationShip" runat="server" BackColor="#eff6fc">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="S">Spouse</asp:ListItem>
                            <asp:ListItem Value="C">Child</asp:ListItem>
                            <asp:ListItem Value="F">Father</asp:ListItem>
                            <asp:ListItem Value="M">Mother</asp:ListItem>
                            <asp:ListItem Value="O">Other</asp:ListItem>
                        </asp:DropDownList></div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlRelationShip"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                            Width="119px">Field is mandatory</asp:RequiredFieldValidator> 
                </div>
                <div class="clear"></div>
            </div>--%>
            <div class="containerClass">
                <div class="labelClass">
                    <asp:Label ID="lblDepartmentCode" runat="server" Font-Bold="False" ForeColor="#000036"
                        Text="Department Code" Width="105px"></asp:Label>
                </div>
                <div class="controlClass">
                    <asp:TextBox CssClass="textbox" ID="txtDepartmentCode" runat="server" BackColor="White" MaxLength="10"
                        Rows="10"></asp:TextBox>
                </div>
                <div class="clear"></div>
            </div>
            <div class="containerClass">
                <div class="labelClass">
                    <asp:Label ID="lblJoinReason" runat="server" Font-Bold="False" ForeColor="#000036"
                        Text="Join reason" Width="97px"></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select">
                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlJoinReason" runat="server" Font-Names="Arial" Font-Size="Small">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="RJOIN001">Member is studying outside Kingdom</asp:ListItem>
                            <asp:ListItem Value="RJOIN002">Dependent is studying outside Kingdom</asp:ListItem>
                            <asp:ListItem Value="RJOIN003">Customer forgot to add member from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN004">Customer forgot to add dependent from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN005">Main member / father forgot to add dependent from contract start date</asp:ListItem>
                            <asp:ListItem Value="RJOIN006">End of probationary period</asp:ListItem>
                            <asp:ListItem Value="RJOIN007">Employee is ill and needs treatment</asp:ListItem>
                            <asp:ListItem Value="RJOIN008">Dependent is ill and needs treatment</asp:ListItem>
                            <asp:ListItem Value="RJOIN009">Member was on long vacation</asp:ListItem>
                            <asp:ListItem Value="RJOIN010">Member promoted to higher grade which is entitled to add dependent/s</asp:ListItem>
                            <asp:ListItem Value="RJOIN012">New Born baby</asp:ListItem>
                            <asp:ListItem Value="RJOIN013">Newly Married</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator ID="RFVldtrJoinReason" InitialValue="0" runat="server"
                        ControlToValidate="ddlJoinReason" ErrorMessage="RequiredFieldValidator" Font-Names="Verdana"
                        Enabled="false" ValidationGroup="2" Display="Dynamic" Font-Size="Small" Text="Join Reason is mandatory"
                        Width="176px"></asp:RequiredFieldValidator>
                </div>
                <div class="clear"></div>
            </div>
            <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial" runat="server" id="Table4" visible="true">
                <tr>
                    <td style="width: 120px" valign="top"></td>
                    <td bgcolor="white" style="width: 10px" valign="top"></td>
                    <td style="width: 185px" valign="top"></td>
                    <td bgcolor="white" style="width: 10px" valign="top"></td>
                    <td colspan="3" style="text-align: right" valign="top">
                        <asp:Button CssClass="submitButton" ID="btnUpdate" runat="server" Text="Update Dependent" Visible="False"
                            OnClick="Button3_Click" BackColor="Control" ValidationGroup="2" /><asp:Button CssClass="submitButton" ID="btnCancel"
                                runat="server" OnClick="btnCancel_Click" Text="Cancel" Visible="False" BackColor="Control" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px" valign="top">
                        <asp:TextBox CssClass="textbox" ID="txtFlagForAddingDepValidation" runat="server" Width="0px" ValidationGroup="1"
                            BackColor="Transparent" BorderColor="Transparent" Height="0px" Text="0"></asp:TextBox>
                    </td>
                    <td bgcolor="white" style="width: 10px" valign="top"></td>
                    <td style="width: 185px" valign="top"></td>
                    <td bgcolor="white" style="width: 10px" valign="top"></td>
                    <td colspan="3" style="text-align: right" valign="top">
                        <asp:Button CssClass="submitButton" ID="btnAdd" runat="server" OnClick="Button1_Click" onmouseover="JoinDateValidation(document.getElementById('txtJoiningDate').value,  'Dep');"
                            Text="Add Dependent" ValidationGroup="2" />
                    </td>
                </tr>
                <tr>
                    <td colspan="7" valign="top">
                        <asp:RangeValidator ID="RangeValidatorForDep" runat="server" ControlToValidate="txtFlagForAddingDepValidation"
                            Enabled="False" ErrorMessage="RangeValidator" MaximumValue="19" MinimumValue="1"
                            ValidationGroup="1" Width="395px" Font-Bold="True">At least one dependent must be added.</asp:RangeValidator>
                    </td>
                </tr>
            </table>
            <asp:XmlDataSource ID="XmlDataSourceTitle" runat="server" DataFile="~/xmlfiles/Title.xml"></asp:XmlDataSource>
            <asp:XmlDataSource ID="XmlDataSourceNationality" runat="server" DataFile="~/xmlfiles/Nationality.xml"></asp:XmlDataSource>
        </asp:Panel>

        <br />

        <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
            runat="server" id="tblUploader">
            <tr>
                <td align="left">
                    <span style="color: #ff3300"><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                </td>
                <td align="right">
                    <a href="<%= Page.ResolveUrl("~/Docs/ReqDocs2012.pdf") %>" target="_blank">Required Document List</a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .bmp) 5MB limit per file" /><br />
                    <BUPA:uploader runat="server" ID="uploader" />
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
        </table>
        <br />
        <asp:Panel ID="PanelDeclaration" runat="server" Visible="false" Width="716px">
            <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
                id="TABLE1">
                <tr valign="top">
                    <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="5">
                        <span style="font-size: 11pt; color: #0099ff"><strong>Declaration Form For Insured Persons</strong></span>
                    </td>
                </tr>
                <tr valign="top">
                    <td align="right" valign="top">
                        <span style="font-size: 11pt; color: #0099ff">For Employee :<br />
                            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Dependent: </span>
                    </td>
                    <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="4">
                        <span style="font-size: 11pt; color: #0099ff">
                            <asp:Label ID="lblEmployeeName" runat="server" Width="384px"></asp:Label>
                            <br />
                            <asp:Label ID="lblAllDependentsName" runat="server" Width="100%"></asp:Label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="white" colspan="3" style="padding-left: 5px; font-weight: bold; font-size: 10pt; vertical-align: middle; color: white; background-color: #0099ff; text-align: left"></td>
                    <td bgcolor="white" colspan="2" style="padding-right: 5px; font-weight: bold; font-size: 11pt; vertical-align: top; color: white; direction: rtl; font-family: Arial; background-color: #0099ff; text-align: right"
                        width="155"></td>
                </tr>
                <tr>
                    <td colspan="3" style="vertical-align: middle; color: white; background-color: #ffffff; text-align: left; padding-left: 5px; font-weight: bold; font-size: 10pt;">
                        <span style="color: black">Please tick yes or no on each of these&nbsp; questions. If
                            yes please provide number of cases and names of patients</span>
                    </td>
                    <td colspan="2" style="direction: rtl; font-weight: bold; font-size: 11pt; font-family: Arial; vertical-align: top; color: white; background-color: #ffffff; text-align: right; padding-right: 5px;"
                        width="255">
                        <span style="color: black">يرجى وضع علامة تحت نعم أو لا مقابل الأسئلة التالية إذا كانت
                            الإجابة نعم يرجى تقديم عدد الحالات و أسماء المرضى</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="font-weight: bold; font-size: 9pt; font-family: Arial; text-align: left; color: #0099ff; padding-left: 5px;">Does any of your group / family members suffer from any of the below conditions
                        or disease:
                    </td>
                    <td style="width: 70px"></td>
                    <td colspan="2" style="text-align: right; font-weight: bold; font-size: 9pt; font-family: Arial; vertical-align: middle; color: #0099ff; direction: rtl; padding-right: 5px;"
                        width="155">هل يعاني أحد موظفيك / أفراد عائلتك من أي من الأمراض أو الحالات التالية:
                    </td>
                </tr>
                <tr>
                    <td style="width: 230px; border-top: #0099ff thin solid"></td>
                    <td style="font-weight: bold; font-size: 9pt; font-family: Arial; text-align: center; color: #0099ff; border-top: #0099ff thin solid; width: 70px;">Yes/No<br />
                        <br />
                        (لا / نعم)
                    </td>
                    <td style="font-weight: bold; font-size: 9pt; font-family: Arial; text-align: center; color: #0099ff; border-top: #0099ff thin solid; width: 70px;">Number of Cases
                        <br />
                        ( عدد الحالات )
                    </td>
                    <td style="font-weight: bold; font-size: 9pt; font-family: Arial; text-align: center; color: #0099ff; border-top: #0099ff thin solid; width: 70px;">Name/s<br />
                        <br />
                        (الاسماء)
                    </td>
                    <td style="direction: ltr; text-align: right; border-top: #0099ff thin solid; width: 225px;"></td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">1.	Future plans for (Artificial / Natural) organ transplants.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ1" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv1,rfv2,'txtQ1Box1','txtQ1Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv1,rfv2,'txtQ1Box1','txtQ1Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq1" runat="server" ControlToValidate="rdbQ1" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ1Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtQ1Box1"
                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="1" Enabled="False"
                            ToolTip="Field Required" Font-Bold="True" Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv1" runat="server" ControlToValidate="txtQ1Box1" Display="Dynamic" ClientIDMode="Static"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Type="Integer" ValidationGroup="1" CultureInvariantValues="True" Enabled="False">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ1Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator
                            ID="rfv2" runat="server" ControlToValidate="txtQ1Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium"
                            ToolTip="Field Required" Width="10px" Enabled="False">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">1-	خطط مستقبلية لزراعة الأعضاء (الطبيعية أو الصناعية)
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">2.	Admission case currently in the hospital or any hospital admission within the last 14 days or receiving treatment in emergency.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ2" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv3,rfv4,'txtQ2Box1','txtQ2Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv3,rfv4,'txtQ2Box1','txtQ2Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq2" runat="server" ControlToValidate="rdbQ2" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ2Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv3" runat="server" ControlToValidate="txtQ2Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator><asp:RangeValidator ID="rv2" runat="server"
                                ControlToValidate="txtQ2Box1" Display="Dynamic" ErrorMessage="RangeValidator"
                                MaximumValue="99" MinimumValue="1" Type="Integer" Enabled="False">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ2Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv4" runat="server" ControlToValidate="txtQ2Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">2-	حالات منومة في المستشفى أو الطواريء حاليا أو تم تنويمها خلال الـ 14 يوم السابقة
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">3.	Central nervous system diseases limited to: Stroke, Epilepsy.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ3" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv5,rfv6,'txtQ3Box1','txtQ3Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv5,rfv6,'txtQ3Box1','txtQ3Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq3" runat="server" ControlToValidate="rdbQ3" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ3Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv5" runat="server" ControlToValidate="txtQ3Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv3" runat="server" ControlToValidate="txtQ3Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ3Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv6" runat="server" ControlToValidate="txtQ3Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">3-	أمراض الجهاز العصبي التالية: التشنجات العصبية (الصرع)، الجلطة الدماغية
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">4.	Tumor or Cancer.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ4" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv7,rfv8,'txtQ4Box1','txtQ4Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv7,rfv8,'txtQ4Box1','txtQ4Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq4" runat="server" ControlToValidate="rdbQ4" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ4Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv7" runat="server" ControlToValidate="txtQ4Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv4" runat="server" ControlToValidate="txtQ4Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ4Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv8" runat="server" ControlToValidate="txtQ4Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">4-	الأورام والسرطان
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">5.	Heart conditions limited to: Arrhythmia, Ischaemic heart disease (IHD), Open heart surgery (CABG), Catheterization, Pacemaker, Valve disease.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ5" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv9,rfv10,'txtQ5Box1','txtQ5Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv9,rfv10,'txtQ5Box1','txtQ5Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq5" runat="server" ControlToValidate="rdbQ5" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ5Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv9" runat="server" ControlToValidate="txtQ5Box1" Display="Dynamic" ErrorMessage="*" Enabled="False"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv5" runat="server" ControlToValidate="txtQ5Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ5Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv10" runat="server" ControlToValidate="txtQ5Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">5-	أمراض القلب التالية: عدم انتظام ضربات القلب، قصور في التروية، عملية سابقة بالقلب، قسطرة الشرايين، جهاز منظم للضربات، أمراض الصمامات
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">6.	Liver disorder limited to: Hepatitis, Cirrhosis, Esophageal varices, Gallbladder stones.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ6" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv11,rfv12,'txtQ6Box1','txtQ6Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv11,rfv12,'txtQ6Box1','txtQ6Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq6" runat="server" ControlToValidate="rdbQ6" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ6Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv11" runat="server" ControlToValidate="txtQ6Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv6" runat="server" ControlToValidate="txtQ6Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Type="Integer" Enabled="False">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ6Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv12" runat="server" ControlToValidate="txtQ6Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">6-	أمراض الكبد التالية: تليف الكبد، التهاب كبدي مزمن، دوالي بالمريء، حصوات بالمرارة
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">7.	Urinary tract disorder limited to: Renal failure, Urinary tract stones.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ7" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv13,rfv14,'txtQ7Box1','txtQ7Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv13,rfv14,'txtQ7Box1','txtQ7Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq7" runat="server" ControlToValidate="rdbQ7" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ7Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv13" runat="server" ControlToValidate="txtQ7Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv7" runat="server" ControlToValidate="txtQ7Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ7Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv14" runat="server" ControlToValidate="txtQ7Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">7-	أمراض الجهاز البولي التالية: فشل كلوي، حصوات الجهاز البولي
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">8.	Autoimmune disorder limited to: Ankylosing spondylitis, Multiple sclerosis (MS), Psoriasis, Systemic lupus erythematosus (SLE), Rheumatoid, Ulcerative colitis (Crohn's).
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ8" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv15,rfv16,'txtQ8Box1','txtQ8Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv15,rfv16,'txtQ8Box1','txtQ8Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq8" runat="server" ControlToValidate="rdbQ8" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ8Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv15" runat="server" ControlToValidate="txtQ8Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv8" runat="server" ControlToValidate="txtQ8Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ8Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv16" runat="server" ControlToValidate="txtQ8Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">8-	الأمراض المناعية التالية: التهاب الفقرات المناعي، التصلب المتعدد، الصدفية، الذئبة الحمراء، الروماتيزم، التهاب القولون التقرحي
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">9.	Vascular disease limited to: Phlebitis, Varicocele, Varicose vein, Vasculitis, Aneurysm.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ9" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv17,rfv18,'txtQ9Box1','txtQ9Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv17,rfv18,'txtQ9Box1','txtQ9Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq9" runat="server" ControlToValidate="rdbQ9" Display="Dynamic"
                            ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ9Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv17" runat="server" ControlToValidate="txtQ9Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv9" runat="server" ControlToValidate="txtQ9Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ9Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv18" runat="server" ControlToValidate="txtQ9Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">9-	أمراض الأوردة الدموية التالية: الدوالي، تضخم أو تمدد أو التهاب أو جلطة الأوعية الدموية
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">10.	Blood disorder limited to: Sickle cell anemia (SCD), Hemophilia, Thalassemia, Leukemia.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ10" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv19,rfv20,'txtQ10Box1','txtQ10Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv19,rfv20,'txtQ10Box1','txtQ10Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq10" runat="server" ControlToValidate="rdbQ10"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ10Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv19" runat="server" ControlToValidate="txtQ10Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv10" runat="server" ControlToValidate="txtQ10Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ10Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv20" runat="server" ControlToValidate="txtQ10Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">10-	أمراض الدم التالية: الأنيميا المنجلية، الهيموفيليا، الثلاسيميا، سرطان الدم
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">11.	Congenital disorder & Hereditary disease (Diseases resulting from defects or genetic disorder and transmitted from one generation to another, or that affect the individual during fetal life).
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ11" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv21,rfv22,'txtQ11Box1','txtQ11Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv21,rfv22,'txtQ11Box1','txtQ11Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq11" runat="server" ControlToValidate="rdbQ11"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ11Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv21" runat="server" ControlToValidate="txtQ11Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv11" runat="server" ControlToValidate="txtQ11Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ11Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv22" runat="server" ControlToValidate="txtQ11Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">11-	التشوهات الخلقية أو الأمراض الوراثية (الأمراض الناتجة عن خلل أو اضطراب الجينات والمنتقلة من جيل إلى آخر أو التي تصيب الفرد أثناء المرحلة الجنينية)
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">12.	Uncontrolled Diabetes / Hypertension cases needing admission from time to time.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ12" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv23,rfv24,'txtQ12Box1','txtQ12Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv23,rfv24,'txtQ12Box1','txtQ12Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq12" runat="server" ControlToValidate="rdbQ12"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ12Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv23" runat="server" ControlToValidate="txtQ12Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv12" runat="server" ControlToValidate="txtQ12Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ12Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv24" runat="server" ControlToValidate="txtQ12Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">12-	حالات سكري /ضغط الدم والتي تحتاج للتنويم بين الحين والآخر لأجل تنظيم المستوى في الجسم
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">13.	Eye disease limited to: Cataract, Glaucoma, Corneal & Retinal condition.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ13" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv25,rfv26,'txtQ13Box1','txtQ13Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv25,rfv26,'txtQ13Box1','txtQ13Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq13" runat="server" ControlToValidate="rdbQ13" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ13Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv25" runat="server" ControlToValidate="txtQ13Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv13" runat="server" ControlToValidate="txtQ13Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ13Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv26" runat="server" ControlToValidate="txtQ13Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">13-	أمراض العين التالية: مياه بيضاء، مياه زرقاء، أمراض القرنية، أمراض الشبكية
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">14.	Ear disease limited to: Hearing loss, Equilibrium problems, and Cochlear problems.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ14" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv27,rfv28,'txtQ14Box1','txtQ14Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv27,rfv28,'txtQ14Box1','txtQ14Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq14" runat="server" ControlToValidate="rdbQ14" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ14Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv27" runat="server" ControlToValidate="txtQ14Box1" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True" Enabled="False"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv14" runat="server" ControlToValidate="txtQ14Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ14Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv28" runat="server" ControlToValidate="txtQ14Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">14-	أمراض السمع التالية: فقدان السمع، مشاكل الاتزان، مشاكل قوقعة الأذن
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">15.	Bone disease limited to: Disc prolapse, Arthritis, Scoliosis, Ligament tears, Osteoporosis.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ15" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv29,rfv30,'txtQ15Box1','txtQ15Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv29,rfv30,'txtQ15Box1','txtQ15Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq15" runat="server" ControlToValidate="rdbQ15" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ15Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2">

                        </asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv29" runat="server" ControlToValidate="txtQ15Box1" Display="Dynamic" ErrorMessage="*" Enabled="False"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv15" runat="server" ControlToValidate="txtQ15Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ15Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100">

                        </asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv30" runat="server" ControlToValidate="txtQ15Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">15-	أمراض العظام التالية: الانزلاق الغضروفي (الديسك)، تشوهات العمود الفقري، التهاب العظام المزمن، مشاكل الركبة وأربطة المفاصل، هشاشة العظام
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">16.	Tissue disease limited to: Abnormal tissue growth, Cyst, Hernia, Ulcers (Bed sores, Diabetic foot).
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ16" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv31,rfv32,'txtQ16Box1','txtQ16Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv32,rfv32,'txtQ16Box1','txtQ16Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvq16" runat="server" ControlToValidate="rdbQ16" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ16Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2">

                        </asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv31" runat="server" ControlToValidate="txtQ16Box1" Display="Dynamic" ErrorMessage="*" Enabled="False"
                            SetFocusOnError="true" ValidationGroup="1" ToolTip="Field Required" Font-Bold="True"
                            Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rv16" runat="server" ControlToValidate="txtQ16Box1" Display="Dynamic"
                            ErrorMessage="RangeValidator" MaximumValue="99" MinimumValue="1" Enabled="False" Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ16Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100">

                        </asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv32" runat="server" ControlToValidate="txtQ16Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">16-	نمو غير طبيعي بالأنسجة والأعضاء، تكيسات، الفتق والبواسير، قرحة الفراش، القدم السكرية
                    </td>
                </tr>

                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">17.	Current pregnancy for female employee or employee's wives.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ17" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv33,rfv34,'txtQ17Box1','txtQ17Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv33,rfv34,'txtQ17Box1','txtQ17Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ControlToValidate="rdbQ17"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1" SetFocusOnError="true">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ17Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv33" runat="server" ControlToValidate="txtQ17Box1"
                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="1" Enabled="False"
                            ToolTip="Field Required" Font-Bold="True" Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtQ17Box1"
                            Display="Dynamic" ErrorMessage="RangeValidator" Enabled="False" MaximumValue="99" MinimumValue="1"
                            Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ17Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv34" runat="server" ControlToValidate="txtQ17Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">17-	حالات الحمل الحالية للموظفات أو زوجات الموظفين.
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">18.	Current multiple pregnancy or baby with congenital anomaly.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ18" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv35,rfv36,'txtQ18Box1','txtQ18Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv35,rfv36,'txtQ18Box1','txtQ18Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="rdbQ18"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ18Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv35" runat="server" ControlToValidate="txtQ18Box1"
                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="1" Enabled="False"
                            ToolTip="Field Required" Font-Bold="True" Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtQ18Box1"
                            Display="Dynamic" ErrorMessage="RangeValidator" MaximumValue="99" Enabled="False" MinimumValue="1"
                            Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ18Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv36" runat="server" ControlToValidate="txtQ18Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">18-	الحالات الحالية أو السابقة لحمل متعدد الأجنة أو تشوهات خلقية
                    </td>
                </tr>

                <tr>
                    <td style="font-size: 10pt; font-family: Arial; color: #404040; width: 230px;">19.	History of abortion or previous Cesarean section delivery or instrumental assisted or premature labor or baby with congenital anomaly.
                    </td>
                    <td style="width: 70px">
                        <asp:RadioButtonList ID="rdbQ19" runat="server" RepeatDirection="Horizontal" ForeColor="#404040">
                            <asp:ListItem onclick="doEnable('Y',rfv37,rfv38,'txtQ19Box1','txtQ19Box2')" Value="Y">Y</asp:ListItem>
                            <asp:ListItem onclick="doEnable('N',rfv37,rfv38,'txtQ19Box1','txtQ19Box2')" Value="N">N</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ControlToValidate="rdbQ19"
                            Display="Dynamic" ErrorMessage="*" ValidationGroup="1">Selection Mandatory</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 70px">
                        <asp:TextBox CssClass="textbox" ID="txtQ19Box1" runat="server" Width="30px" ForeColor="#404040" MaxLength="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv37" runat="server" ControlToValidate="txtQ19Box1"
                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="1" Enabled="False"
                            ToolTip="Field Required" Font-Bold="True" Font-Size="Medium">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtQ19Box1"
                            Display="Dynamic" ErrorMessage="RangeValidator" MaximumValue="99" Enabled="False" MinimumValue="1"
                            Type="Integer">Number required</asp:RangeValidator>
                    </td>
                    <td style="width: 155px">
                        <asp:TextBox CssClass="textbox" ID="txtQ19Box2" runat="server" ForeColor="#404040" Width="130px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv38" runat="server" ControlToValidate="txtQ19Box2" Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="true" ValidationGroup="1" Font-Bold="True" Font-Size="Medium" Enabled="False"
                            ToolTip="Field Required" Width="10px">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="direction: rtl; text-align: right; font-size: 10pt; font-family: Arial; color: #404040; width: 225px;">19-	الحالات السابقة للإجهاض أو الولادات القيصرية أو المبكرة أو المتعثرة
                    </td>
                </tr>


                <tr>
                    <td bgcolor="#0099ff" style="font-size: 10pt; width: 230px; color: #404040; font-family: Arial; height: 3px"></td>
                    <td bgcolor="#0099ff" style="width: 70px; height: 3px"></td>
                    <td bgcolor="#0099ff" style="width: 70px; height: 3px"></td>
                    <td bgcolor="#0099ff" style="width: 155px; height: 3px"></td>
                    <td bgcolor="#0099ff" style="font-size: 10pt; width: 225px; color: #404040; direction: rtl; font-family: Arial; height: 3px; text-align: right"></td>
                </tr>
            </table>
            <br />
            <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;">
                <tr>
                    <td style="width: 357px; text-align: justify; clip: rect(auto auto auto auto); height: 368px; vertical-align: top;">
                        <span style="font-size: 9pt; color: #404040; font-family: Arial; vertical-align: top; margin-top: 0px;"><b>Declaration:</b>
                            <br />
                            <span style="font-size: 7.5pt;">1- I / We confirm that all mentioned data in this form is complete
and correct and has been discussed with all sta_ and their
families in a manner not inconsistent with the privacy and
confidentiality of the information, and the acceptance of the
application will be based on this data. Bupa Arabia for
Cooperative Insurance has the right to contact the hospitals
which I am / We are dealing with to provide any medical
information that may be needed to assess the risk.
                                <br />
                                2- I / We agree that Bupa Arabia for Cooperative Insurance have the
eligibility to reject the claim or the entire coverage at concealment
of any unexpected case or any case that arose before the
date of the contract, enrolment, or addition, even if the cases
mentioned in the medical disclosure whether the cases are
undiagnosed before unless if they were accepted by Bupa Arabia
in writing.
                               <br />
                                3- I / We undertake to perform similar declaration in the future on
members who will be added during the contract period or upon
renewal as all old and new Declaration forms are considered as
integral part of the current and future contracts.

                            </span>
                            <br />
                            <a><span style="font-size: 7pt;">If you have any questions please contact us at: Sales Toll-free: 800 116 0500 </a></span>
                        </span>
			   Email: <a href="mailto:business@bupame.com"><span style="font-size: 7pt">business@bupame.com</span></a>
                    </td>
                    <td style="width: 15px; text-align: justify; height: 368px;"></td>
                    <td style="direction: rtl; text-align: justify; font-family: Times New Roman; font-size: 8pt; color: black; text-indent: 2pt; vertical-align: top; letter-spacing: normal; height: 368px;">
                        <b><span style="font-size: 11pt">إقـرار:</span></b>
                        <br />
                        <span style="font-size: 10pt; vertical-align: middle; color: #404040; direction: rtl; font-family: Arial; text-align: right; padding-right: 5px; clip: rect(auto 5px auto auto); text-indent: 2pt; margin-right: 5px;">1- أقر/نقر أن جميع البيانات المذكورة في في هذا النموذج كاملة وصحيحة وتمت مناقشتها مع الأفراد أو الموظفين المذكورين جميعهم وعائلاتهم بطريقة لا تتنافى مع خصوصية وسرية المعلومات، وبناء عليه فإن قبول الطلب سيتم على أساس هذه البيانات وأن شركة بوبا العربية للتأمين التعاوني لها الحق في الإتصال بالمستشفيات التي أتعامل/نتعامل معها لتزويدها بأي معلومات طبية قد تحتاج إليها لتقييم الخطر.<br />

                            2- أوافق/نوافق بأحقية شركة بوبا العربية للتأمين التعاوني في رفض المطالبة أو التغطية كليا عند عدم الإفصاح عن أي حالة متوقعة أو نشأت قبل تاريخ التعاقد أو التسجيل أو الإضافة خلال العقد ما إذا كانت من الحالات المذكورة في قسم الإفصاح الطبي أو لم تذكر سواء كانت مشخصة من قبل أو قيد التشخيص إلا إذا تم قبولها من قبل شركة بوبا العربية خطيا.-
                            <br />
                            3- أتعهد/نتعهد القيام بعملية افصاح مماثل مستقبلا على الأعضاء الذين سيتم إضافتهم خلال مدة سريان العقد أو عند التجديد حيث أن جميع نماذج الإفصاح القديمة والحديثة تعتبر جزء لا يتجزء من العقد الحالي والعقود المستقبلية.-
                            <br />
                        </span><a><span style="font-size: 9pt">الهاتف اﻟﻤﺠاني: ۰٥۰۰ ۱۱٦ ۸۰۰</span></a><a><span
                            style="font-size: 9pt"> ، الفاكس: ۱٤٦۹ ٦٦۸ ۰٢</span></a><span style="font-size: 9pt; direction: rtl; text-indent: 2pt;">
                                <br />
                                العنوان الإلكتروني :</span><a href="mailto:business@bupame.com"><span style="font-size: 9pt">
                                    business@bupame.com</span></a>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table style="width: 750px; font-size: 11px; font-family: Arial;" runat="server" id="Table2" visible="false">
            <tr>
                <td align="right">
                    <asp:Button CssClass="submitButton" ID="btnSubmit" runat="server"  Text="Submit Request" ValidationGroup="1" OnClick="Button2_Click" BackColor="Control" />
                </td>
            </tr>
        </table>
        <br />
        <uc2:OSNav ID="OSNav1" runat="server" />
        <br />


        <script type="text/javascript">

            function ValidateTitleGender(gender, title, type) {

                if (gender == "M") {
                    if (title == 'Miss' || title == 'Ms' || title == 'Mrs') {
                        alert('Incorrect gender.');

                        if (type == 'Emp') document.getElementById('ddlEGender').selectedIndex = 0;
                        if (type == 'Dep') document.getElementById('ddlGender').selectedIndex = 0;
                        return false;
                    }
                }

                if (gender == "F") {
                    if (title == 'Mr' || title == 'Sir') {
                        alert('Incorrect gender.');

                        if (type == 'Emp') document.getElementById('ddlEGender').selectedIndex = 0;
                        if (type == 'Dep') document.getElementById('ddlGender').selectedIndex = 0;
                        return false;
                    }
                }
            }

            function checkJoinDate() {

                if (chkObject('ddlEJoinReason')) {
                    if (document.getElementById('ddlEJoinReason').selectedIndex == 0) {
                        var myVal1 = document.getElementById('RFVldtrEJoinReason');
                        myVal1.enabled = true;
                        return false;
                    }
                }
                else {
                }
                alert();
            }

            function addOptions(val1, option) {
                var myVal = document.getElementById('vldtrSaudiID');

                if (chkObject('txtESaudiID'))
                    var myVal1 = document.getElementById('VldtrSaudiIDEmp');  // condition it - only if it exist

                if (myVal == null || myVal1 == null || val1 == null)
                    return;

                if (val1 == "966") {

                    if (option == 'Dep') {
                        myVal.enabled = true;
                    }

                    if (option == 'Emp') {

                        if (chkObject('txtESaudiID'))
                            myVal1.enabled = true;
                    }
                }

                else {
                    myVal.enabled = false;

                    if (chkObject('txtESaudiID'))
                        myVal1.enabled = false;
                }

            }

            function chkObject(theVal) {
                if (document.getElementById(theVal) != null) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function JoinDateValidation(txt, type) {
                var myVal;

                if (type == 'Emp') { myVal = document.getElementById('RFVldtrEJoinReason'); }
                if (type == 'Dep') { myVal = document.getElementById('RFVldtrJoinReason'); }

                var DatePart = txt.split("/");
                DatePart[0];
                DatePart[1];
                DatePart[2];

                var formatedDate = DatePart[1] + "/" + DatePart[0] + "/" + DatePart[2];
                var d = new Date(formatedDate);  //"04/10/2006"
                var date = new Date();

                date.setDate(date.getDate() - 90);

                if (d < date) {
                    myVal.enabled = true;
                }
                else {

                    myVal.enabled = false;

                    myVal.style.display = 'none';
                }
            }

            function doEnable(Val, txt1, txt2, txtBox1, txtBox2) {

                var textbox1 = document.getElementById(txtBox1);
                var textbox2 = document.getElementById(txtBox2);
                if (Val == 'Y') {
                    ValidatorEnable(txt1, true);
                    ValidatorEnable(txt2, true);
                    textbox1.disabled = false;
                    textbox2.disabled = false;
                    textbox1.style.backgroundColor = '#FFFFC0';
                    textbox2.style.backgroundColor = '#FFFFC0';
                }
                else {
                    ValidatorEnable(txt1, false);
                    ValidatorEnable(txt2, false);
                    textbox1.disabled = true;
                    textbox2.disabled = true;
                    textbox1.value = '';
                    textbox2.value = '';
                    textbox1.style.backgroundColor = '#FFFFFF';
                    textbox2.style.backgroundColor = '#FFFFFF';
                }

            }

            function ValidateContactOptions() {
                if (document.getElementById("PanelDeclaration") != null) {

                    //need to check the true vlaue of a control and create an if condition.    
                    var Fax = document.form1.txtFax.value;
                    var Mobile = document.form1.txtMobile.value;
                    var Telephone = document.form1.txtTelephone.value;
                    var Email = document.form1.txtEmail.value;
                    var Result = 0;
                    if (Fax != '')
                    { Result += 1; }



                    if (Telephone != '')
                    { Result += 1; }

                    if (Email != '')
                    { Result += 1; }

                    if (Result == 0) {

                        alert('Please type in at least one: Telephone or Fax or EmailID.');
                        return false;

                    }
                }
            }


            function resetElementsInDiv(div) {
                var elms = document.getElementById(div).getElementsByTagName('*');
                var maxI = elms.length;
                for (var i = 0; i < maxI; i++) {
                    var elm = elms[i];

                    if (elm.type == 'text') {
                        document.all[elm.name].value = '';
                    }
                    if (elm.type == 'radio') {
                        elm.checked = '';
                    }
                }

            }



        </script>
    </fieldset>
    <script type='text/JavaScript' src='js/NumberValidation.js'></script>
    <script language="javascript" type="text/javascript">
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
           $('#entryForm2').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
        });

        SetMobileNumberValidation("#<%= txtEMobileNo.ClientID %>");
        SetMobileNumberValidation("#<%= txtMobileNo.ClientID %>");

        SetIDNumberValidation("#<%= txtESaudiID.ClientID %>", "#<%= ddlEIDType.ClientID %>");
        SetIDNumberValidation("#<%= txtESponsorId.ClientID %>", "#<%= ddlEIDType.ClientID %>");
        SetIDNumberValidation("#<%= txtSaudiID.ClientID %>", "#<%= ddlIDType.ClientID %>");
        SetIDNumberValidation("#<%= txtSponsorId.ClientID %>", "#<%= ddlIDType.ClientID %>");

       prm.add_endRequest(function () {
           $('#entryForm').unblock();
           $('#entryForm2').unblock();
           SetIDNumberValidation("#<%= txtESaudiID.ClientID %>", "#<%= ddlEIDType.ClientID %>");
           SetIDNumberValidation("#<%= txtESponsorId.ClientID %>", "#<%= ddlEIDType.ClientID %>");
           SetIDNumberValidation("#<%= txtSaudiID.ClientID %>", "#<%= ddlIDType.ClientID %>");
           SetIDNumberValidation("#<%= txtSponsorId.ClientID %>", "#<%= ddlIDType.ClientID %>");
       });

       // Progress indicator preloading.
       var preload = document.createElement('img');
       preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
       delete preload;

        
    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
       <%-- <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>

</asp:Content>
