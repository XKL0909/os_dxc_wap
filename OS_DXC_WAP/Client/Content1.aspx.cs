
using System;
using System.Web.UI.WebControls;
using System.Text;
using OS_DXC_WAP.CaesarWS;

public partial class Content1 : System.Web.UI.Page
{

    private ServiceDepot_DNService ws;
    public Content1()
    {
        //InitializeComponent();
        this.Load +=new EventHandler(Content1_Load);
}

void  Content1_Load(object sender, EventArgs e)
{
    Master.SearchButton.Click += SearchButton_Click ;
    Master.SearchOpt.SelectedIndexChanged += rdbSearchOption_SelectedIndexChanged;
    Master.PageTitle.Text = "My Coverage";
   



 	//throw new Exception("The method or operation is not implemented.");
}

protected void rdbSearchOption_SelectedIndexChanged(object sender, EventArgs e)
{
    
    Master.SearchText.Text = "";
    Message1.Text = "";
    CoverageListReport.Visible = false;

}

private void SearchButton_Click(object sender, EventArgs e)
{
    DisplayProdCov(Master.SearchText.Text);

    //Label1.Text = Master.SearchText  + Master.SearchOpt;
    concatenate();
    //Master.SearchButton.Text = "TESTING";
}

public object concatenate()
{
   // Label1.Text = TextBox1.Text + Label1.Text;
    
    return null;
}


private void DisplayProdCov(string MemberNo)
{
    ws = new ServiceDepot_DNService();
    //}
    //catch (Exception ex)
    //{
    //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
    //}


    EnqProdCovInfoRequest_DN request;
    EnqProdCovInfoResponse_DN response;
    //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
    //////{
    //////    Message1.Text = "Please insert all the fields before using the web service";
    //////    return;
    //////}
    try
    {
        // Cursor.Current = Cursors.WaitCursor;
        response = null;
        request = new EnqProdCovInfoRequest_DN();
        request.membershipNo = MemberNo;
        //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
        request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

        //request.IqamaNo =  "5523234443";
        //request.TotalNoOfFamily = "3";
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        
        response = ws.EnqProdCovInfo(request);

        //response = ws.EnqMbrListInfo(request);
        // Cursor.Current = Cursors.Default;


        if (response.className != null)
        {

            lblMembershipNo.Text = request.membershipNo;
            lblMemberName.Text = request.membershipNo; // strMemberName;
            lblClassName.Text = response.className;
            lblCustomerName.Text = response.companyName;

            DisplayCoverageListResult(response);
            //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
            //dataGridView1.DataBindingComplete = true;

            //////if (tabMemberName.HeaderText == "Member")
            //////{
            //////    tabMemberName.HeaderText = strMemberName;
            //////}

        }
        else
        {

           // tabsModManager.Visible = false;
            UpdatePanel2.Visible = false;
            Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
        }
    }
    catch (Exception ex)
    {
        UpdatePanel2.Visible = false;
       // tabsModManager.Visible = false;

        Message1.Text = ex.Message; /// this message would be used by transaction
        Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

    }

}

private void DisplayCoverageListResult(EnqProdCovInfoResponse_DN response)
{
    ListItem itm;
    StringBuilder msge;


    string str1;
    string str2;
    str2 = "";
    str1 = "";
    StringBuilder sbResponse = new StringBuilder(2200);

    if (response.status == "0")
    {

        //dtl.proratedOverallAnnualLimit = ;
        //str1 = 

        sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px;  ;'><thead><tr><th>Benefit Type</th>   <th>Benefit Limit</th></tr>	</thead><tbody> ");
        sbResponse.Append("<tr><td>OverAll Annual Limit</td><td>" + response.proratedOverallAnnualLimit.ToString() + "</td>");
        response.proratedOverallAnnualLimit.ToString();

        foreach (BenefitTypeLimitDetail_DN dtl in response.detail)
        {
            sbResponse.Append("<tr><td>" + dtl.detail_BenefitType + "</td>");
            if (dtl.detail_ProratedLimit != -1)
                sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
            else
                sbResponse.Append("<td>Not Covered</td></tr>");


        }
        sbResponse.Append("</table>");
        CoverageListReport.Visible = true;
        CoverageListReport.InnerHtml = sbResponse.ToString();


    }
    else
    {
        //msge = new StringBuilder(100);
        //foreach (String s in response.errorMessage)
        //{
        //    msge.Append(s).Append("/n");
        //}
        //Message1.Text = msge.ToString();
    }

    //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


}


    
}
