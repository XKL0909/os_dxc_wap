using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Collections;

public partial class Client_Statement : System.Web.UI.Page
{
    private ReportDocument doc = null;
    private DataSet ds = null;
    String InvID = null;
    string strClientID;
    private Hashtable hasQueryValue;

    string batchId, sundryPremium, fullShort, parametersShown;

    protected void Page_Init(object sender, EventArgs e)
    {
        strClientID = Session["ClientID"].ToString();///"10490900"; //
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
           
            CheckSessionExpired();
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
        }
        catch
        {

        }

        try
		{

            lnkBackOption.NavigateUrl = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Statement");


            hasQueryValue = new Hashtable();
            string queryStringValues = string.Empty;

            if (Request.QueryString.Count > 0)
            {
                try
                {
                    var val = Request.QueryString["val"];
                    queryStringValues = Cryption.Decrypt(val);
                    hasQueryValue = Cryption.GetQueryValue(queryStringValues);

                    batchId = hasQueryValue.ContainsKey("BID") ? Convert.ToString(hasQueryValue["BID"]) : string.Empty;
                    sundryPremium = hasQueryValue.ContainsKey("PS") ? Convert.ToString(hasQueryValue["PS"]) : string.Empty;
                    fullShort = hasQueryValue.ContainsKey("FS") ? Convert.ToString(hasQueryValue["FS"]) : string.Empty;
                    parametersShown = hasQueryValue.ContainsKey("Pr") ? Convert.ToString(hasQueryValue["Pr"]) : string.Empty;
                }
                catch (Exception)
                {
                    Response.Write("Invalid rquest!");
                    return;
                }

            }
            else
            {
                Response.Write("Invalid rquest!");
                return;
            }

            /// String Par1 = Request.QueryString["Pr"];
            String Par1 = parametersShown;

            doc = new ReportDocument();
			if ((ViewState["ParametersShown"] != null) && (ViewState["ParametersShown"].ToString() == "True"))
			{
				BindReport();
			}
			else
			{
				BindReport();
				ViewState["ParametersShown"] = "True";
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
    }

    public void BindReport()
    {
        ////InvID = Request.QueryString["ID"];
        InvID = batchId;

        doc.Load(MapPath("BFCSTMSTM01.rpt"));
        ds = GetData();
        doc.SetDataSource(ds.Tables[0]);

        doc.SetParameterValue("pc_report_id", "BFCSTMSTM01");
        doc.SetParameterValue("pc_report_group", "AL");
        doc.SetParameterValue("pv_company_name", "dd");
        doc.SetParameterValue("pd_report_date", "01/01/2010");
        doc.SetParameterValue("pv_title1", "ddd");
        ////doc.SetParameterValue("p6", Request.QueryString["FS"]);
        doc.SetParameterValue("p6", fullShort);
        doc.SetParameterValue("PC_RPT_GRP", "AL");
        doc.SetParameterValue("PC_CONT_NO", "50000013");
        doc.SetParameterValue("PC_SHORT_FULL_IND", "S");
        doc.SetParameterValue("PC_BRANCH_CODE", "4234234");
        doc.SetParameterValue("PC_CONT_YYMM", "201005");
        ////doc.SetParameterValue("p7", Request.QueryString["PS"]);
        doc.SetParameterValue("p7", sundryPremium);
        ////doc.SetParameterValue("PC_SUNDRY_IND", Request.QueryString["PS"]);
        doc.SetParameterValue("PC_SUNDRY_IND", sundryPremium);
        CrystalReportViewer1.ReportSource = doc;
        Session["rptDoc3"] = doc;
    }

    private DataSet GetData()
    {
        DataSet ds= new DataSet();
        try
        {
            DataTable dt;
            DataRow row;
            int i;
            OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
            OS_DXC_WAP.CaesarWS.ReqCustStatementRequest_DN datum;
            OS_DXC_WAP.CaesarWS.ReqCustStatementResponse_DN result;
            datum = new OS_DXC_WAP.CaesarWS.ReqCustStatementRequest_DN();
            datum.contractNo = strClientID;
            ////datum.fullOrShort = Request.QueryString["FS"];
            datum.fullOrShort = fullShort;
            ////datum.premiumOrSundry = Request.QueryString["PS"];
            datum.premiumOrSundry = sundryPremium;
            ////if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
            if (!string.IsNullOrEmpty(batchId))
                datum.branchCode = batchId;
            datum.Username = WebPublication.CaesarSvcUsername;
            datum.Password = WebPublication.CaesarSvcPassword;
            datum.transactionID = TransactionManager.TransactionID();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            result = ws.ReqCustStatement(datum);
            dt = BuildTable();
            if (result.status == "0")
            {
                for (i = 0; i < result.detail.Length; i++)
                {
                    row = dt.NewRow();
                    row["VIBAN_NO"] = result.detail[i].Viban;
                    row["CUST_NAME"] = result.detail[i].customerName;
                    row["ADDR1"] = result.detail[i].addr1;
                    row["ADDR2"] = result.detail[i].addr2;
                    row["ADDR3"] = result.detail[i].addr3;
                    row["ADDR4"] = result.detail[i].addr4;
                    row["DISTRICT"] = result.detail[i].district;
                    row["COUNTRY"] = result.detail[i].country;
                    row["CONT_NO"] = result.detail[i].contractNo;
                    row["BRANCH_CODE"] = result.detail[i].branchCode;
                    row["BRANCH_NAME"] = result.detail[i].branchName;
                    row["SYS_DATE"] = result.detail[i].systemDate;
                    try
                    {
                        row["DOC_CRT_DATE"] = DateTime.Parse(
                                                              DateTime.Parse(result.detail[i].documentCreateDate).Day.ToString()
                                                              + "/" + DateTime.Parse(result.detail[i].documentCreateDate).Month.ToString()
                                                              + "/" + DateTime.Parse(result.detail[i].documentCreateDate).Year.ToString());
                    }
                    catch
                    {
                        row["DOC_CRT_DATE"] = "01/01/1900";
                    }
                    row["IF_NO"] = result.detail[i].ifNumber;
                    row["DOC_NO"] = result.detail[i].documentNumber;
                    row["DOC_DESC"] = result.detail[i].documentDescription;
                    row["DEBIT_AMOUT"] = result.detail[i].debitAmount;
                    row["CREDIT_AMOUNT"] = result.detail[i].creditAmount;
                    row["NAME"] = result.detail[i].name;
                    row["POSITION"] = result.detail[i].position;
                    row["TEL"] = result.detail[i].telephone;
                    row["MOBILE"] = result.detail[i].mobile;
                    row["ON_BEHALF_OF"] = result.detail[i].onBehalfOf;
                    row["THROUGH_AGENT"] = result.detail[i].throughAgent;
                    row["YYMM_FLAG"] = Int32.Parse(result.detail[i].yymmFlag);
                    row["BRANCH_FLAG"] = result.detail[i].branchFlag;
                    row["MAIN_BRH_IND"] = result.detail[i].mainBranchIndicator;
                    ////Receipt No added by Sakthi on 19-Jan-2017 for CR267 CR329 CR311, CR338 And CR309 Unified Policy number
                    ////Start
                    row["RCT_NO"] = result.detail[i].rctNo;
                    row["PROD_IND"] = "Y";
                    ////End
					
					//// CR 378 VAT changes added by Sakthi on 28-Nov-2017
					//// Start
						row["BUPA_TIN"] = result.detail[i].bupaTin;
						row["VAT_DESC"] = result.detail[i].vatDesc;
						row["VAT_AMOUNT"] = result.detail[i].vatAmount;
						row["VAT_CRD_AMOUNT"] = result.detail[i].vatCrdAmount;
					//// End

					
                    dt.Rows.Add(row);
                }
                DataView dv = dt.DefaultView;
                dv.Sort = "DOC_CRT_DATE";
                ds = new DataSet();
                ds.Tables.Add(dt);
            }
            else
            {

            }
           
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);
        }
        return ds;
    }

    private DataTable BuildTable()
    {
        DataTable dt;
        DataColumn col;

        dt = new DataTable();

        col = new DataColumn();
        col.ColumnName = "CUST_NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 70;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "VIBAN_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 24;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ADDR1";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "ADDR2";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "ADDR3";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "ADDR4";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "DISTRICT";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 40;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "COUNTRY";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CONT_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 8;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "BRANCH_CODE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 18;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "BRANCH_NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 70;
        dt.Columns.Add(col);


        col = new DataColumn();
        col.ColumnName = "SYS_DATE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "DOC_CRT_DATE";
        col.DataType = Type.GetType("System.DateTime");
        //col.MaxLength = 100;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "IF_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 70;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "DOC_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 8;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "DOC_DESC";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 120;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "DEBIT_AMOUT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "CREDIT_AMOUNT";
        col.DataType = Type.GetType("System.Double");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "NAME";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "POSITION";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "TEL";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "MOBILE";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "ON_BEHALF_OF";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "THROUGH_AGENT";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "YYMM_FLAG";
        col.DataType = Type.GetType("System.Int32");
        //  col.MaxLength = 1;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "BRANCH_FLAG";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 1;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "MAIN_BRH_IND";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 10;
        dt.Columns.Add(col);

        ////Receipt No added by Sakthi on 19-Jan-2017 for CR267 CR329 CR311, CR338 And CR309 Unified Policy number
        ////Start
        col = new DataColumn();
        col.ColumnName = "RCT_NO";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 25;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "PROD_IND";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 1;
        dt.Columns.Add(col);
        ////End

        //// CR 378 VAT changes added by Sakthi on 28-Nov-2017
        //// Start
        col = new DataColumn();
        col.ColumnName = "BUPA_TIN";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 200;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "VAT_DESC";
        col.DataType = Type.GetType("System.String");
        col.MaxLength = 45;
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "VAT_AMOUNT";
        col.DataType = Type.GetType("System.Double");        
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "VAT_CRD_AMOUNT";
        col.DataType = Type.GetType("System.Decimal");
        dt.Columns.Add(col);

        //// End

        return dt;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //this.CrystalReportViewer1..ShowPreviousPage();
        doc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "Statement");//-" + DateTime.Today.ToShortDateString());

    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        this.CrystalReportViewer1.ShowNextPage();
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientID"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["pRefNo"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }
}
