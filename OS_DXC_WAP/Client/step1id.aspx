﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="step1id" Codebehind="step1id.aspx.cs" %>



<form id="form1" runat="server">
   
    
    <div class="card bb">
        <div class="header bb">
            <h2>Add </h2>
        </div>
        <div class="body bb">
            
            <div class="row clearfix margin-top-20 bb">
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label id="lblMemberOrSponsor" class="control-label bb">Sponosr/رقم الكفيل</label>
                            <asp:dropdownlist  cssclass="form-control bb" id="drplistSponsor" runat="server"></asp:dropdownlist>
                            <asp:requiredfieldvalidator validationgroup="step1" id="reqValdrplistSponsor" controltovalidate="drplistSponsor" Display="Dynamic" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
                 <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">Employee National ID/Iqama /رقم الهوية </label>
                            <asp:textbox cssclass="form-control bb" runat="server" id="txtSecId" maxlength="10"></asp:textbox>
                            <asp:RegularExpressionValidator Display="Dynamic"  validationgroup="step1"  ID="RegularExpressionValidator1" runat="server"    
                            ErrorMessage="Length must be Ten characters/  عشرة ارقام فقط"
                            ControlToValidate="txtSecId" 
                           setfocusonerror="true"
                            ValidationExpression="^[0-9]{10,10}$" />
                            <asp:requiredfieldvalidator Display="Dynamic" validationgroup="step1" id="Requiredfieldvalidator1" controltovalidate="txtSecId" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row clearfix">
                 <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label class="control-label">ID/رقم الهوية</label>
                            <asp:textbox cssclass="form-control" runat="server" id="txtID" maxlength="10"></asp:textbox>
                            <asp:RegularExpressionValidator  validationgroup="step1"  ID="RegExp1" runat="server"    
                            ErrorMessage="Length must be 10 characters / 10 ارقام فقط" Display="Dynamic"
                            ControlToValidate="txtID" 
                           setfocusonerror="true"
                            ValidationExpression="^[0-9]{10,10}$" />
                            <asp:requiredfieldvalidator Display="Dynamic" validationgroup="step1" id="reqValtxtID" controltovalidate="txtID" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" id="sectionHijari">
                    <div class="form-group">
                        <div class="form-line">
                            <label class="control-label">Hijri Year of Birth/السنة الهجرية للولادة</label>
                             <asp:TextBox CssClass="form-control" runat="server" ID="txtHYearOfBirth" MaxLength="4"></asp:TextBox>
                      <asp:RegularExpressionValidator Display="Dynamic" runat="server" ErrorMessage="Four Numbers Only/ أربع أرقام فقط" ControlToValidate="txtHYearOfBirth"
      ValidationExpression="^[1-9]\d$"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="step1" ID="reqValtxtHYearOfBirth" ControlToValidate="txtHYearOfBirth" runat="server" errormessage="Field is mandatory/الحقل إجباري" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="actions clearfix pull-right">
                <asp:button id="btnNext" validationgroup="step1" runat="server"  text="Next" class="btn btn-primary nextBtn btn-sm pull-right" />
            </div>

        </div>
    </div>
</form>

<script src="js/step1id.js"></script>

