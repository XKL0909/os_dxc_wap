<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" Inherits="ChangeBranch" Codebehind="ChangeBranch.aspx.cs" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static" >

    <style type="text/css" media="screen">
        .DatePicker
        {
            background: url(images/cal.gif) no-repeat right center;
        }
        DIV.DatePicker-Container
        {
            font-size: 9px;
        }
        DIV.DatePicker-Wrapper
        {
            border-right: #e0e0e0 1px solid;
            border-top: #e0e0e0 1px solid;
            border-left: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
            background-color: skyblue;
        }
        DIV.DatePicker-Wrapper TABLE
        {
        }
        DIV.DatePicker-Wrapper TD
        {
            padding-right: 0px;
            padding-left: 0px;
            padding-bottom: 0px;
            padding-top: 0px;
        }
        DIV.DatePicker-Wrapper TD.nav
        {
        }
        DIV.DatePicker-Wrapper TD.dayName
        {
            border-right: #e0e0e0 1px solid;
            font-weight: bold;
            border-bottom: #e0e0e0 1px solid;
            background-color: #eeeeee;
        }
        DIV.DatePicker-Wrapper TD.day
        {
            border-right: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
        }
        DIV.DatePicker-Wrapper TD.empty
        {
            border-right: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
        }
        DIV.DatePicker-Wrapper TD.current
        {
            color: #ffffff;
            background-color: #666666;
        }
        DIV.DatePicker-Wrapper TD.dp_roll
        {
            background-color: #e0e0e0;
        }
    </style>
   
    <script language="javascript" type="text/javascript">
        function changetext(reqName) {
            if (reqName == "RFVtxtBadgeNo")
                ValidatorEnable(document.getElementById('<%= RFVtxtBadgeNo.ClientID %>'), false);
            else if (reqName == "RequiredFieldValidator13")
                ValidatorEnable(document.getElementById('<%= RequiredFieldValidator13.ClientID %>'), false);

            }

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
         });

         prm.add_endRequest(function () {
             $('#entryForm').unblock();
         });

         // Progress indicator preloading.
         var preload = document.createElement('img');
         preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
         delete preload;

    </script>
    <div>
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>--%>
    <fieldset style="padding:10px">
                <legend><h1>
                <asp:Label ID="lblAdd_Employee" runat="server" Text="Change Branch" Font-Size="Large"
                        Font-Names="Arial"></asp:Label>
                </h1></legend>                 
        
        
        <div align="right"><asp:HyperLink ID="lnkBatchUpload" runat="server" Font-Size="Small"  Target="_self" Visible="true">Batch Upload</asp:HyperLink></div>
        <br />
        <div style="text-align: left;">
        <asp:Image ID="imgSuc" runat="server" Visible="false" ImageUrl="~/images/reimp_req_suc.png" /><br />
        <asp:Label ID="Message1" runat="server" Width="527px" Font-Size="Small" ForeColor="Green"  ></asp:Label><br />
            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </div>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Black" Text="Fields marked with asterisk [ * ]  are mandatory."></asp:Label>
         <asp:Label ID="lblAramcoMessage" runat="server" Font-Size="Small" ForeColor="Black" Text="One Field is Mandatory Either the badge or membership"></asp:Label>

<%if (uploader.LblUpload.Text.Length > 0)
  { %>
        <br />
        <br />
        <table style="width: 750px; font-size: 17px; font-family: Arial; border-right: black 0.5pt solid;
            border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid;
            border-collapse: collapse;" runat="server" id="Table3">
            <tr>
                <td align="left">
                    <span style="color: #cc0000"><%= uploader.LblUpload.Text%></span>
                </td>
            </tr>
        </table>
        <br />
        <br />
        
<% }%>

        <asp:Panel ID="Panel1" runat="server" Width="95%">
        <table>
            <tr>
                <td>
                    <span style="color: #ff0000">*</span>Membership No
                </td>
                <td style="width: 223px">
                    <asp:TextBox ID="txtMembershipNo" runat="server" CssClass="textbox" onchange="changetext('RFVtxtBadgeNo')"
                        MaxLength="8"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server"
                        ControlToValidate="txtMembershipNo" Display="Dynamic"
                        ErrorMessage="RequiredFieldValidator" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegExpMemberShipno" runat="server" ControlToValidate="txtMembershipNo" Display="Dynamic" ErrorMessage="Only numbers."
                                                    ValidationExpression="^\d+$" />
                    <br />
                </td>
                <td style="width: 200px" valign="top">
                    <asp:Button ID="Button1" runat="server" CssClass="submitButton"
                        OnClick="Button1_Click1" Text="Get Details" CausesValidation="true" />
                </td>
            </tr>
              <tr>
                        <td>
                            <div id="DivStafflbl" runat="server">
                                <span style="color: #ff0000">*</span>Badge No
                    <br />
                            </div>
                        </td>
                        <td style="width: 223px">
                            <div id="DivStafftxt" runat="server">
                                <asp:TextBox ID="txtBadgeNo" runat="server" CssClass="textbox" onchange="changetext('RequiredFieldValidator13')"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFVtxtBadgeNo" runat="server" ControlToValidate="txtBadgeNo" Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="txtBadgeNoRegExpVal" runat="server" ControlToValidate="txtBadgeNo" Display="Dynamic" ErrorMessage="Only numbers with hyphen."
                                                    ValidationExpression="\d*-?\d*" />
                                <br />
                            </div>
                        </td>
                        <td style="width: 200px" valign="top">
                            <br />

                        </td>
                    </tr>
            <tr>
                <td colspan="3">
                    <%--<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>--%>
                </td>
            </tr>
        </table>
            <div  runat="server" id="details" visible="false">
            <asp:UpdatePanel runat="server" ID="updatePanel2">
                    <ContentTemplate>
                        <div id="entryForm">
            <table>
                <tr>
                    <td style="width: 223px">
                        <span style="color: #ff0000">*</span><asp:Label ID="lblCompleteName" 
                            runat="server" Text="Complete Name(First, Middle, Last)" Width="210px"></asp:Label>
                    </td>
                    <td style="width: 200px">
                        <asp:Label ID="txtMemberName" runat="server" 
                            MaxLength="40" style="font-weight: 700"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 213px; height: 25px">
                        <span style="color: #ff0000">*</span><asp:Label ID="lblOptionOld" runat="server"
                            Text="Old Branch" Width="125px"></asp:Label>
                    </td>
                    <td style="width: 200px; height: 25px">
                        <div class="styled-select"><asp:Label    ID="ddlBranchCodeOld" runat="server" 
                                style="font-weight: 700" >
                            
                        </asp:Label>
                            <asp:Label ID="ddlBranchCodeOldValue" Visible="false" runat="server" style="font-weight: 700">
                            
                        </asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 213px; height: 25px">
                        <span style="color: #ff0000">*</span><asp:Label ID="lblOptionNew" runat="server"
                            Text="New Branch" Width="125px"></asp:Label>
                    </td>
                    <td style="width: 200px; height: 25px">
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlBranchCodeNew" runat="server"  BackColor="#eff6fc">
                            <asp:ListItem>-Select-</asp:ListItem>
                            <asp:ListItem Value="10290300">Branch</asp:ListItem>
                        </asp:DropDownList></div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlBranchCodeNew"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" Width="250px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 213px; height: 25px;">
                        <span style="color: #ff0000">*</span><asp:Label ID="lbl_Reason" runat="server" Text="Reason for change"
                            Width="148px"></asp:Label>
                    </td>
                    <td style="width: 200px; height: 25px;">
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlChangeReason" runat="server"  BackColor="#eff6fc">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="BRA001">Member was relocated to another jobsite</asp:ListItem>
                            <asp:ListItem Value="BRA002">Other/Customer reason</asp:ListItem>
                            <asp:ListItem Value="BRA003">Due to Budget/Cost Centre allocation</asp:ListItem>
                        </asp:DropDownList></div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlChangeReason"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" Width="162px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 213px">
                        <span style="color: #ff0000">*</span><asp:Label ID="lblDateOfBirth" runat="server"
                            Text="Effective Date" Width="100px"></asp:Label>
                    </td>
                    <td style="width: 200px">
                        <asp:TextBox CssClass="textbox" ID="txtDateOfEffect" runat="server"  onclick="scwShow(this,event);"
                            onfocus="this.blur();" Style="cursor: default;"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDateOfEffect"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDateOfEffect"
                            Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{2}/\d{2}/\d{4}">Invalid Date</asp:RegularExpressionValidator>
                    </td>
                </tr>
                
                            <tr runat="server" id="typecontainer">
                                <td align="left"><span style="color: #ff0000"></span>ID Type&nbsp;</td>
                                <td align="left">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEIDType" ClientIDMode="Static" runat="server" BackColor="White" AutoPostBack="true" OnSelectedIndexChanged="ddlEIDType_SelectedIndexChanged">
                                            <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Passport" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                                        </asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator runat="server" ID="rfvIDType" ForeColor="Red" ControlToValidate="ddlEIDType" ValidationGroup="1" Enabled="False"> required</asp:RequiredFieldValidator>--%>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr runat="server" id="maritalstatuscontainer">
                                <td align="left"><span style="color: #ff0000"></span>Marital Status&nbsp;</td>
                                <td align="left">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEMaritalStatus" 
                                        ClientIDMode="Static" runat="server" 
                                         BackColor="White" >
                                        <asp:ListItem Value="">-Select-</asp:ListItem>
                                        <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                                    </asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator runat="server" ID="rfvMaritalStatus" ForeColor="Red" ControlToValidate="ddlEMaritalStatus" ValidationGroup="1" Enabled="False"> required</asp:RequiredFieldValidator>--%>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                             <tr  runat="server" id="idnumbercontainer">
                                <td align="left"><span style="color: #ff0000"></span>Saudi ID/Iqama no&nbsp;</td>
                                <td align="left">
                                    <asp:TextBox CssClass="textbox"  ID="txtSaudiIqamaIDNumber" runat="server" ValidationGroup="1" BackColor="White" MaxLength="10"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator runat="server" ID="rfvIDNo" ForeColor="Red" ControlToValidate="txtSaudiIqamaIDNumber" ValidationGroup="1" Display="Dynamic"> required</asp:RequiredFieldValidator>--%>
                                    <%--<asp:RegularExpressionValidator
                                                ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtSaudiIqamaIDNumber"
                                                Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{10}"
                                                ValidationGroup="1" Width="142px">Incorrect Saudi/Iqama ID</asp:RegularExpressionValidator>--%>
                                    <asp:RegularExpressionValidator
                                                ID="rfvSuadiID" runat="server" ControlToValidate="txtSaudiIqamaIDNumber"
                                                Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[2][0-9]{9}"
                                                ValidationGroup="1" Width="142px">Incorrect Iqama ID</asp:RegularExpressionValidator>
                                    <%--<asp:Label ID="lblSaudiIqamaIDNumber" runat="server" />--%>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            
                     <tr runat="server" id="expdatecontainer">
                        <td align="left"><span style="color: #ff0000"></span>ID Expiry Date&nbsp;</td>
                        <td align="left">
                            <asp:TextBox CssClass="textbox"  ID="txtEIDExpDate" runat="server" onfocus="this.blur();"
                            onclick="scwShow(this,event);" Style="cursor: default;" ValidationGroup="1" BackColor="White"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator
                                ID="rfvIDExpiryDate" runat="server" ControlToValidate="txtEIDExpDate"
                                ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" Enabled="false" ValidationGroup="1"
                                Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="142px"></asp:RequiredFieldValidator>--%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr runat="server" id="professioncontainer">
                        <td align="left"><span style="color: #ff0000"></span>Profession&nbsp;</td>
                        <td align="left">
                            <asp:DropDownList ID="ddProfession" CssClass="DropDownListCssClass" runat="server"   BackColor="White" style="width:252px;height:28px;" OnDataBound="ddProfession_DataBound" DataValueField="profCode" DataTextField="profName"></asp:DropDownList> 
                            <%--<asp:RequiredFieldValidator
                            ID="rfvProfession" runat="server" ControlToValidate="ddProfession"
                            ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" Enabled="false" ValidationGroup="1"
                            Display="Dynamic" Font-Size="Small" Text="required" Width="142px"></asp:RequiredFieldValidator>--%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr runat="server" id="districtcontainer">
                        <td align="left"><span style="color: #ff0000"></span>District&nbsp;</td>
                        <td align="left">
                            <asp:DropDownList ID="ddDistrict" CssClass="DropDownListCssClass" runat="server"   BackColor="White" style="width:252px;height:28px;" OnDataBound="ddDistrict_DataBound"  DataValueField="distCode" DataTextField="distName"></asp:DropDownList> 
                            <%--<asp:RequiredFieldValidator
                                ID="rfvDistrict" runat="server" ControlToValidate="ddDistrict"
                                ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" Enabled="false" ValidationGroup="1"
                                Display="Dynamic" Font-Size="Small" Text="required" Width="142px"></asp:RequiredFieldValidator>--%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr  runat="server" id="nationalitycontainer">
                        <td align="left"><span style="color: #ff0000"></span>Nationality&nbsp;</td>
                        <td align="left">
                                <asp:DropDownList CssClass="DropDownListCssClass" ClientIDMode="Static" ID="ddlENationality" Font-Size="9" runat="server" DataTextField="text" DataValueField="value" BackColor="White" style="width:252px;height:28px;">
                            <asp:ListItem Value="">-Select-</asp:ListItem>
                            <%--<asp:ListItem Value="093">Afghanistan</asp:ListItem>
                            <asp:ListItem Value="355">Albania</asp:ListItem>
                            <asp:ListItem Value="213">Algeria</asp:ListItem>
                            <asp:ListItem Value="684">American Samoa</asp:ListItem>
                            <asp:ListItem Value="376">Andorra</asp:ListItem>
                            <asp:ListItem Value="244">Angola</asp:ListItem>
                            <asp:ListItem Value="101">Anguilla</asp:ListItem>
                            <asp:ListItem Value="102">Antigua and Barbuda</asp:ListItem>
                            <asp:ListItem Value="054">Argentine Republic</asp:ListItem>
                            <asp:ListItem Value="374">Armenia</asp:ListItem>
                            <asp:ListItem Value="297">Aruba</asp:ListItem>
                            <asp:ListItem Value="247">Ascension</asp:ListItem>
                            <asp:ListItem Value="061">Australia</asp:ListItem>
                            <asp:ListItem Value="672">Australian External Territories</asp:ListItem>
                            <asp:ListItem Value="043">Austria</asp:ListItem>
                            <asp:ListItem Value="994">Azerbaijani Republic</asp:ListItem>
                            <asp:ListItem Value="000">Bedoun</asp:ListItem>
                            <asp:ListItem Value="103">Bahamas</asp:ListItem>
                            <asp:ListItem Value="973">Bahrain</asp:ListItem>
                            <asp:ListItem Value="880">Bangladesh</asp:ListItem>
                            <asp:ListItem Value="104">Barbados</asp:ListItem>
                            <asp:ListItem Value="375">Belarus</asp:ListItem>
                            <asp:ListItem Value="032">Belgium</asp:ListItem>
                            <asp:ListItem Value="501">Belize</asp:ListItem>
                            <asp:ListItem Value="229">Benin</asp:ListItem>
                            <asp:ListItem Value="105">Bermuda</asp:ListItem>
                            <asp:ListItem Value="975">Bhutan</asp:ListItem>
                            <asp:ListItem Value="591">Bolivia</asp:ListItem>
                            <asp:ListItem Value="387">Bosnia and Herzegovina</asp:ListItem>
                            <asp:ListItem Value="267">Botswana</asp:ListItem>
                            <asp:ListItem Value="055">Brazil</asp:ListItem>
                            <asp:ListItem Value="106">British Virgin Islands</asp:ListItem>
                            <asp:ListItem Value="673">Brunei Darussalam</asp:ListItem>
                            <asp:ListItem Value="359">Bulgaria</asp:ListItem>
                            <asp:ListItem Value="226">Burkina Faso</asp:ListItem>
                            <asp:ListItem Value="257">Burundi</asp:ListItem>
                            <asp:ListItem Value="855">Cambodia</asp:ListItem>
                            <asp:ListItem Value="237">Cameroon</asp:ListItem>
                            <asp:ListItem Value="107">Canada</asp:ListItem>
                            <asp:ListItem Value="238">Cape Verde</asp:ListItem>
                            <asp:ListItem Value="108">Cayman Islands</asp:ListItem>
                            <asp:ListItem Value="236">Central African Republic</asp:ListItem>
                            <asp:ListItem Value="235">Chad</asp:ListItem>
                            <asp:ListItem Value="056">Chile</asp:ListItem>
                            <asp:ListItem Value="086">China</asp:ListItem>
                            <asp:ListItem Value="057">Colombia</asp:ListItem>
                            <asp:ListItem Value="269">Comoros</asp:ListItem>
                            <asp:ListItem Value="242">Congo</asp:ListItem>
                            <asp:ListItem Value="682">Cook Islands</asp:ListItem>
                            <asp:ListItem Value="506">Costa Rica</asp:ListItem>
                            <asp:ListItem Value="225">Cote d'lvoire</asp:ListItem>
                            <asp:ListItem Value="385">Croatia</asp:ListItem>
                            <asp:ListItem Value="053">Cuba</asp:ListItem>
                            <asp:ListItem Value="357">Cyprus</asp:ListItem>
                            <asp:ListItem Value="420">Czech Republic</asp:ListItem>
                            <asp:ListItem Value="850">South Korea</asp:ListItem>
                            <asp:ListItem Value="243">Democratic Republic Congo</asp:ListItem>
                            <asp:ListItem Value="045">Denmark</asp:ListItem>
                            <asp:ListItem Value="246">Diego Garcia</asp:ListItem>
                            <asp:ListItem Value="253">Djibouti</asp:ListItem>
                            <asp:ListItem Value="109">Dominica</asp:ListItem>
                            <asp:ListItem Value="110">Dominican Republic</asp:ListItem>
                            <asp:ListItem Value="593">Ecuador</asp:ListItem>
                            <asp:ListItem Value="020">Egypt</asp:ListItem>
                            <asp:ListItem Value="503">El Salvador</asp:ListItem>
                            <asp:ListItem Value="240">Equatorial Guinea</asp:ListItem>
                            <asp:ListItem Value="291">Eritrea</asp:ListItem>
                            <asp:ListItem Value="372">Estonia</asp:ListItem>
                            <asp:ListItem Value="251">Ethiopia</asp:ListItem>
                            <asp:ListItem Value="500">Falkland Islands</asp:ListItem>
                            <asp:ListItem Value="298">Faroe Islands</asp:ListItem>
                            <asp:ListItem Value="679">Fiji</asp:ListItem>
                            <asp:ListItem Value="358">Finland</asp:ListItem>
                            <asp:ListItem Value="033">France</asp:ListItem>
                            <asp:ListItem Value="594">French Guiana</asp:ListItem>
                            <asp:ListItem Value="689">French Polynesia</asp:ListItem>
                            <asp:ListItem Value="241">Garbonese Republic</asp:ListItem>
                            <asp:ListItem Value="220">Gambia</asp:ListItem>
                            <asp:ListItem Value="995">Georgia</asp:ListItem>
                            <asp:ListItem Value="049">Germany</asp:ListItem>
                            <asp:ListItem Value="233">Ghana</asp:ListItem>
                            <asp:ListItem Value="350">Gibraltar</asp:ListItem>
                            <asp:ListItem Value="030">Greece</asp:ListItem>
                            <asp:ListItem Value="299">Greenland</asp:ListItem>
                            <asp:ListItem Value="111">Grenada</asp:ListItem>
                            <asp:ListItem Value="590">Guadeloupe</asp:ListItem>
                            <asp:ListItem Value="112">Guam</asp:ListItem>
                            <asp:ListItem Value="502">Guatemala</asp:ListItem>
                            <asp:ListItem Value="224">Guinea</asp:ListItem>
                            <asp:ListItem Value="245">Guinea-Bissau</asp:ListItem>
                            <asp:ListItem Value="592">Guyana</asp:ListItem>
                            <asp:ListItem Value="509">Haiti</asp:ListItem>
                            <asp:ListItem Value="504">Honduras</asp:ListItem>
                            <asp:ListItem Value="852">Hongkong</asp:ListItem>
                            <asp:ListItem Value="036">Hungary</asp:ListItem>
                            <asp:ListItem Value="354">Iceland</asp:ListItem>
                            <asp:ListItem Value="091">India</asp:ListItem>
                            <asp:ListItem Value="062">Indonesia</asp:ListItem>
                            <asp:ListItem Value="098">Iran</asp:ListItem>
                            <asp:ListItem Value="964">Iraq</asp:ListItem>
                            <asp:ListItem Value="353">Ireland</asp:ListItem>
                            <asp:ListItem Value="039">Italy</asp:ListItem>
                            <asp:ListItem Value="113">Jamaica</asp:ListItem>
                            <asp:ListItem Value="081">Japan</asp:ListItem>
                            <asp:ListItem Value="962">Jordan</asp:ListItem>
                            <asp:ListItem Value="701">Kazakstan</asp:ListItem>
                            <asp:ListItem Value="254">Kenya</asp:ListItem>
                            <asp:ListItem Value="686">Kiribati</asp:ListItem>
                            <asp:ListItem Value="082">Korea</asp:ListItem>
                            <asp:ListItem Value="965">Kuwait</asp:ListItem>
                            <asp:ListItem Value="996">Kyrgyz Republic</asp:ListItem>
                            <asp:ListItem Value="856">Lao People's Demcr Republic</asp:ListItem>
                            <asp:ListItem Value="371">Latvia</asp:ListItem>
                            <asp:ListItem Value="961">Lebanon</asp:ListItem>
                            <asp:ListItem Value="266">Lesotho</asp:ListItem>
                            <asp:ListItem Value="231">Liberia</asp:ListItem>
                            <asp:ListItem Value="218">Libya</asp:ListItem>
                            <asp:ListItem Value="423">Liechtenstein</asp:ListItem>
                            <asp:ListItem Value="370">Lithuania</asp:ListItem>
                            <asp:ListItem Value="352">Luxembourg</asp:ListItem>
                            <asp:ListItem Value="853">Macau</asp:ListItem>
                            <asp:ListItem Value="261">Madagascar</asp:ListItem>
                            <asp:ListItem Value="265">Malawi</asp:ListItem>
                            <asp:ListItem Value="060">Malaysia</asp:ListItem>
                            <asp:ListItem Value="960">Maldives</asp:ListItem>
                            <asp:ListItem Value="223">Mali</asp:ListItem>
                            <asp:ListItem Value="356">Malta</asp:ListItem>
                            <asp:ListItem Value="692">Marshall Islands</asp:ListItem>
                            <asp:ListItem Value="596">Martinique</asp:ListItem>
                            <asp:ListItem Value="222">Mauritania</asp:ListItem>
                            <asp:ListItem Value="230">Mauritius</asp:ListItem>
                            <asp:ListItem Value="052">Mexico</asp:ListItem>
                            <asp:ListItem Value="691">Micronesia</asp:ListItem>
                            <asp:ListItem Value="373">Moldova</asp:ListItem>
                            <asp:ListItem Value="377">Monaco</asp:ListItem>
                            <asp:ListItem Value="976">Mongolia</asp:ListItem>
                            <asp:ListItem Value="114">Montserrat</asp:ListItem>
                            <asp:ListItem Value="212">Morocco</asp:ListItem>
                            <asp:ListItem Value="258">Mozambique</asp:ListItem>
                            <asp:ListItem Value="095">Myanmar</asp:ListItem>
                            <asp:ListItem Value="264">Namibia</asp:ListItem>
                            <asp:ListItem Value="674">Nauru</asp:ListItem>
                            <asp:ListItem Value="977">Nepal</asp:ListItem>
                            <asp:ListItem Value="031">Netherlands</asp:ListItem>
                            <asp:ListItem Value="599">Netherlands Antilles</asp:ListItem>
                            <asp:ListItem Value="687">New Caledonia</asp:ListItem>
                            <asp:ListItem Value="064">New Zealand</asp:ListItem>
                            <asp:ListItem Value="505">Nicaragua</asp:ListItem>
                            <asp:ListItem Value="227">Niger</asp:ListItem>
                            <asp:ListItem Value="234">Nigeria</asp:ListItem>
                            <asp:ListItem Value="683">Niue</asp:ListItem>
                            <asp:ListItem Value="115">Northern Mariana Islands</asp:ListItem>
                            <asp:ListItem Value="047">Norway</asp:ListItem>
                            <asp:ListItem Value="968">Oman</asp:ListItem>
                            <asp:ListItem Value="092">Pakistan</asp:ListItem>
                            <asp:ListItem Value="680">Palau</asp:ListItem>
                            <asp:ListItem Value="972">Palestine</asp:ListItem>
                            <asp:ListItem Value="507">Panama</asp:ListItem>
                            <asp:ListItem Value="675">Papua New Guinea</asp:ListItem>
                            <asp:ListItem Value="595">Paraguay</asp:ListItem>
                            <asp:ListItem Value="051">Peru</asp:ListItem>
                            <asp:ListItem Value="063">Philippines</asp:ListItem>
                            <asp:ListItem Value="048">Poland</asp:ListItem>
                            <asp:ListItem Value="351">Portugal</asp:ListItem>
                            <asp:ListItem Value="116">Puerto Rico</asp:ListItem>
                            <asp:ListItem Value="974">Qatar</asp:ListItem>
                            <asp:ListItem Value="262">Reunion</asp:ListItem>
                            <asp:ListItem Value="040">Romania</asp:ListItem>
                            <asp:ListItem Value="702">Russian Federation</asp:ListItem>
                            <asp:ListItem Value="250">Rwandese Republic</asp:ListItem>
                            <asp:ListItem Value="290">Saint Helena</asp:ListItem>
                            <asp:ListItem Value="117">Saint Kitts and Nevis</asp:ListItem>
                            <asp:ListItem Value="118">Saint Lucia</asp:ListItem>
                            <asp:ListItem Value="508">Saint Pierre and Miquelon</asp:ListItem>
                            <asp:ListItem Value="119">Saint Vincent and Grenadines</asp:ListItem>
                            <asp:ListItem Value="670">Saipan</asp:ListItem>
                            <asp:ListItem Value="685">Samoa</asp:ListItem>
                            <asp:ListItem Value="378">San Marino</asp:ListItem>
                            <asp:ListItem Value="239">Sao Tome and Principe</asp:ListItem>
                            <asp:ListItem Value="966">Saudi Arabia</asp:ListItem>
                            <asp:ListItem Value="221">Senegal</asp:ListItem>
                            <asp:ListItem Value="248">Seychelles</asp:ListItem>
                            <asp:ListItem Value="232">Sierra Leone</asp:ListItem>
                            <asp:ListItem Value="065">Singapore</asp:ListItem>
                            <asp:ListItem Value="421">Slovak Republic</asp:ListItem>
                            <asp:ListItem Value="386">Slovenia</asp:ListItem>
                            <asp:ListItem Value="677">Solomon Islands</asp:ListItem>
                            <asp:ListItem Value="252">Somali Democratic Republic</asp:ListItem>
                            <asp:ListItem Value="027">South Africa</asp:ListItem>
                            <asp:ListItem Value="034">Spain</asp:ListItem>
                            <asp:ListItem Value="094">Sri Lanka</asp:ListItem>
                            <asp:ListItem Value="249">Sudan</asp:ListItem>
                            <asp:ListItem Value="597">Suriname</asp:ListItem>
                            <asp:ListItem Value="268">Swaziland</asp:ListItem>
                            <asp:ListItem Value="046">Sweden</asp:ListItem>
                            <asp:ListItem Value="041">Switzerland</asp:ListItem>
                            <asp:ListItem Value="963">Syrian Arab Republic</asp:ListItem>
                            <asp:ListItem Value="886">Taiwan</asp:ListItem>
                            <asp:ListItem Value="992">Tajikistan</asp:ListItem>
                            <asp:ListItem Value="255">Tanzania</asp:ListItem>
                            <asp:ListItem Value="066">Thailand</asp:ListItem>
                            <asp:ListItem Value="389">Former Yugoslav Rep Macedonia</asp:ListItem>
                            <asp:ListItem Value="671">Tinian</asp:ListItem>
                            <asp:ListItem Value="228">Togolese Republic</asp:ListItem>
                            <asp:ListItem Value="690">Tokelau</asp:ListItem>
                            <asp:ListItem Value="676">Tonga</asp:ListItem>
                            <asp:ListItem Value="120">Trinidad and Tobago</asp:ListItem>
                            <asp:ListItem Value="216">Tunisia</asp:ListItem>
                            <asp:ListItem Value="090">Turkey</asp:ListItem>
                            <asp:ListItem Value="993">Turkmenistan</asp:ListItem>
                            <asp:ListItem Value="121">Turks and Caicos Islands</asp:ListItem>
                            <asp:ListItem Value="688">Tuvalu</asp:ListItem>
                            <asp:ListItem Value="971">U.A.E.</asp:ListItem>
                            <asp:ListItem Value="044">U.K.</asp:ListItem>
                            <asp:ListItem Value="001">U.S.A.</asp:ListItem>
                            <asp:ListItem Value="256">Uganda</asp:ListItem>
                            <asp:ListItem Value="380">Ukraine</asp:ListItem>
                            <asp:ListItem Value="122">United States Virgin Islands</asp:ListItem>
                            <asp:ListItem Value="598">Uruguay</asp:ListItem>
                            <asp:ListItem Value="998">Uzbekistan</asp:ListItem>
                            <asp:ListItem Value="678">Vanuatu</asp:ListItem>
                            <asp:ListItem Value="379">Vatican City State</asp:ListItem>
                            <asp:ListItem Value="058">Venezuela</asp:ListItem>
                            <asp:ListItem Value="084">Vietnam</asp:ListItem>
                            <asp:ListItem Value="681">Wallis and Futuna</asp:ListItem>
                            <asp:ListItem Value="967">Yemen</asp:ListItem>
                            <asp:ListItem Value="381">Yugoslavia</asp:ListItem>
                            <asp:ListItem Value="260">Zambia</asp:ListItem>
                            <asp:ListItem Value="263">Zimbabwe</asp:ListItem>--%>
                        </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator
                                ID="rfvNationality" runat="server" ControlToValidate="ddlENationality"
                                ErrorMessage=" required" Font-Names="Verdana" Enabled="false" ValidationGroup="1"
                                Display="Dynamic" Font-Size="Small" Text="required" Width="142px"></asp:RequiredFieldValidator>--%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr runat="server" id="mobilecontainer">
                        <td align="left"><span style="color: #ff0000"></span>Mobile No&nbsp;</td>
                        <td align="left">
                            <asp:TextBox CssClass="textbox"  ID="txtEMobileNo" runat="server" MaxLength="10" BackColor="White"></asp:TextBox><br />
                            <asp:RegularExpressionValidator ID="revMobile" runat="server"
                                ControlToValidate="txtEMobileNo" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                                ValidationExpression="[05][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]" ValidationGroup="1"
                                Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>   
                            <%--<asp:RequiredFieldValidator ID="rfvMobile" runat="server" ControlToValidate="txtEMobileNo" Display="Dynamic" ErrorMessage="Field is mandatory" InitialValue="-Select-" ValidationGroup="1" Enabled="False"/>--%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                <tr>
                    <td style="width: 56px">
                    </td>
                    <td style="width: 200px">
                        <div>
                        </div>
                    </td>
                </tr>
                </table>
                </div>
                </ContentTemplate>
            </asp:UpdatePanel>
                <table style="width:100%; height:auto">
                <tr bordercolor="white" bgcolor="white">
                    <td colspan="2" align="right">
                    <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid;
                border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid;
                border-collapse: collapse;" runat="server" id="tblUploader">
                <tr>
                    <td align="left">
                        <span style="color: #ff3300"><strong>*</strong></span><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                    </td>    
                    <td align="right">
                        <a href="<%= Page.ResolveUrl("~/Docs/ReqDocs2012.pdf") %>" target="_blank">Required Document List</a>
                    </td>
                </tr>
                        
                
                    <tr>
                        <td colspan="2" align="left">
                            <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .pmp) 5MB limit per file" /><br />
                            <bupa:uploader runat="server" id="uploader" />
                        </td>
                    </tr>
                </table>
               <br />
                <asp:Button CssClass="submitButton" ID="btnChangeBranch" runat="server" OnClick="btnChangeBranch_Click" Text="Submit Request" BackColor="Control"  ValidationGroup="1" />
                <asp:Button CssClass="submitButton" ID="btnChangeClass" runat="server" OnClick="btnChangeClass_Click" Text="Submit Request" Visible="False"  BackColor="Control"  ValidationGroup="1" />
                    </td>
                </tr>
            </table>
            </div>
            <br />
            
        </asp:Panel>
         </fieldset>
    </div>
    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<asp:HyperLink ID="lnkChangeOption" runat="server"
        Font-Size="Small" NavigateUrl="~/Client/changebranch.aspx" Visible="False">Change branch for another Employee</asp:HyperLink>
    &nbsp;
    
    <br />
    <uc2:OSNav ID="OSNav1" runat="server" />
    
    <%--<script type='text/JavaScript'>
        $(function () {
           
            $('#<%= btnChangeClass.ClientID %>').click(function () {
                $.blockUI({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
            });
        });
    </script>--%>

    <script type='text/JavaScript' src='js/NumberValidation.js'></script>
    <script type="text/javascript">
        prm = Sys.WebForms.PageRequestManager.getInstance();

        SetMobileNumberValidation("#<%= txtEMobileNo.ClientID %>");
        SetIDNumberValidation("#<%= txtSaudiIqamaIDNumber.ClientID %>", "#<%= ddlEIDType.ClientID %>");

        prm.add_endRequest(function () {
            SetMobileNumberValidation("#<%= txtEMobileNo.ClientID %>");
            SetIDNumberValidation("#<%= txtSaudiIqamaIDNumber.ClientID %>", "#<%= ddlEIDType.ClientID %>");   
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p id="LblChangeBranchBanner" runat="server">
        Whether your request is to add members, delete members, change class, change 
        branch or replace members cards, you may submit your transaction at your 
        convenience.
    </p>
    
</asp:Content>
