﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

public partial class Client_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
			if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }
            else
            {
                aMChangeBranch.HRef = "changebranch.aspx?val=" + Cryption.Encrypt("optiontype=Class");
                aMDeleteDependent.HRef = "DeleteEmployeeEnhanced.aspx?val=" + Cryption.Encrypt("EType=Dep");
                aMStatement.HRef = "creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Statement");
                aMInvoice.HRef = "creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Invoice");

                string warning = string.Empty;
            warning = string.IsNullOrEmpty(Convert.ToString(Session["WarningEnable"])) ? string.Empty : Convert.ToString(Session["WarningEnable"]);

            if (warning.Trim().ToLower().Equals("warning"))
            {
                tdAddEmployeeDependent.Visible = false;             
                tdChangeclass.Visible = false;
                tdChangeBranch.Visible = false;

                tdReplaceCard.Visible = true;
                tdDeleteEmployee.Visible = true;
                tdDeleteDependent.Visible = true;
                tdTrackinfo.Visible = true;
                tdNewAdditionGuide.Visible = true;
                trGsAdditionalDetails.Visible = true;
                tblGsMemberDetails.Visible = true;
            }
			}
        }
        catch (Exception)
        {
            
            throw;
        }
       
    }
}