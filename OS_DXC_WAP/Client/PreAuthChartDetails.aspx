﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PreAuthChartDetails" Codebehind="PreAuthChartDetails.aspx.cs" %>


<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/Header.ascx" TagName="Header" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/Footer.ascx" TagName="Footer" TagPrefix="uc3" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" type="text/css" href="../Styles/bupa.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/home-slider.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/help-slider.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/lightblue.css" />
    <link href="../Scripts/Bupa/MessageBar/CSS/skin.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../Styles/Impromptu.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/OSWebStyle.css" />
    <script type='text/JavaScript' src="../Scripts/scw.js"></script>
    <script language="javascript" type="text/javascript" src="../Scripts/jsDate.js"></script>
    <script src="../Scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../functions/login/functions.js" type="text/javascript"></script>
    <script src="../Scripts/Bupa/MessageBar/jquery.messagebar.js" type="text/javascript"></script>
    <!-- Impromptu -->
    <script type="text/javascript" src='../Scripts/Bupa/Impromptu/jquery-impromptu.4.0.min.js'></script>
    <title>Bupa Arabia, Online Services </title>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-4321156-5']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <style type="text/css">
        #form1
        {
            text-align: center;
        }
    </style>
</head>
<script language="javascript" type="text/javascript">

    /****************************************Drop Down & Rollover Menu  ********************************/
    function MM_swapImgRestore() {
        var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
    }

    function MM_swapImage() {
        var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2); i += 3)
            if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
}

function MM_findObj(n, d) {
    var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n); return x;
}

function MM_showHideLayers() {
    var i, p, v, obj, args = MM_showHideLayers.arguments;
    for (i = 0; i < (args.length - 2); i += 3) if ((obj = MM_findObj(args[i])) != null) {
        v = args[i + 2];
        if (obj.style) { obj = obj.style; v = (v == 'show') ? 'visible' : (v == 'hide') ? 'hidden' : v; }
        obj.visibility = v;
    }
}
</script>
<body>
    <form id="form1" runat="server">
    <div class="socialMediaIcons">
        <a target="_blank" href="https://www.facebook.com/page.bupa.arabia" class="facebookIcon">
            <img src="/Style Library/images/spacer.gif" alt="Facebook" title="Facebook" width="1"
                height="1"></a> <a target="_blank" href="https://twitter.com/bupaarabia" class="twitterIcon">
                    <img src="/Style Library/images/spacer.gif" alt="Twitter" title="Twitter" width="1"
                        height="1"></a> <a target="_blank" href="https://youTube.com/bupaarabiachannel" class="youtubeIcon">
                            <img src="/Style Library/images/spacer.gif" alt="Youtube" title="Youtube" width="1"
                                height="1"></a> <a style="display: none" href="javascript:;" class="emailFriendIcon">
                                    <img src="/Style Library/images/spacer.gif" alt="Email Friend" title="Email Friend"
                                        width="1" height="1"></a> <a target="_blank" style="display: none" href="http://test.borninteractive.net:8081/wpbupa"
                                            class="blogIcon">
                                            <img src="/Style Library/images/spacer.gif" alt="Youtube" title="Youtube" width="1"
                                                height="1"></a> <a style="display: none" target="_blank" href="http://test.borninteractive.net/bupamicrosite"
                                                    class="micrositeIcon">
                                                    <img src="/Style Library/images/spacer.gif" alt="Microsite" title="Microsite" width="1"
                                                        height="1"></a>
    </div>
    <div>
        <table width="995" align="center" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <uc2:Header ID="Header1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <%--header--%>
    <div>
        <table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 1020px">
            <tr>
                <td class="centerBack" style="background: #E6E6E6 url(images/BackgroundImage.jpg) center 0 no-repeat;">
                    <div class="margBottom20">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="pageTitle">
                                                <h1>
                                                    bupa online services</h1>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="subWhiteCenter">
                        <div class="pageHeadRow">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="breadCrumb">
                                        <asp:Label ID="lblLoginName" runat="server" CssClass="Welcome" Text=""></asp:Label>
                                    </td>
                                    <td align="right">
                                        <script src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b87f0ff3925a6aa"
                                            type="text/javascript" defer="defer"></script>
                                        <table style="float: right" runat="server" id="backdiv">
                                            <tr>
                                                <td>
                                                    <a href="ChangePass.aspx" class="likeLinks">Change Password</a>
                                                </td>
                                                <td>
                                                    |
                                                </td>
                                                <td>
                                                    <a href="Main.aspx" class="likeLinks">Back to My Services</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="100%" valign="top">
                                        <img id="Img14" runat="server" src="../images/spacer.gif" alt="" width="10" height="1" />
                                        <div class="highlightText">
                                            <asp:Label Text="Preauthorizations status" runat="server" ID="lbl1" Font-Bold="True"
                                                Font-Size="24pt" ForeColor="Black"></asp:Label>
                                        </div>
                                        <div class="contentContainer">
                                            <div>
                                                <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
                                                    <script type="text/javascript">
                                                        function pageLoad(sender, eventArgs) {
                                                            if (!eventArgs.get_isPartialLoad()) {
                                                                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("InitialPageLoad");
                                                            }
                                                        }
                                                    </script>
                                                </telerik:RadCodeBlock>
                                                <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All"
                                                    EnableRoundedCorners="false" />
                                                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"
                                                    MinDisplayTime="500" />
                                                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                                                    <AjaxSettings>
                                                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="PreAuthDetailsGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                                                                <telerik:AjaxUpdatedControl ControlID="PreAuthDetailsChart" LoadingPanelID="RadAjaxLoadingPanel1" />
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="PreAuthDetailsGrid">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="PreAuthDetailsGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="PreAuthDetailsChart">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="PreAuthDetailsChart" LoadingPanelID="RadAjaxLoadingPanel1" />
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="pnlBtnExcel">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="PreAuthDetailsGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                    </AjaxSettings>
                                                </telerik:RadAjaxManager>
                                                <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                                                <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1"
                                                    ClientEvents-OnRequestStart="pnlRequestStarted">
                                                    <div style="width: 100%; float: left">
                                                    
                                                        <telerik:RadHtmlChart runat="server" ID="PreAuthDetailsChart">
                                                            <PlotArea>
                                                                <XAxis Color="Teal" DataLabelsField="RECEIVED_DATE" BaseUnit="Days" Name="Date">
                                                                    <LabelsAppearance />
                                                                    <TitleAppearance Text="Date" />
                                                                </XAxis>
                                                                <Series>
                                                                    <telerik:ColumnSeries Name="Approved" Stacked="false" GroupName="grouoname1" DataFieldY="ApprovedPreAuths">
                                                                        <LabelsAppearance Visible="false" DataFormatString="{0} approved pre auths">
                                                                        </LabelsAppearance>
                                                                        <TooltipsAppearance Color="White" DataFormatString="{0} approved pre-Auths">
                                                                        </TooltipsAppearance>
                                                                    </telerik:ColumnSeries>
                                                                    <telerik:ColumnSeries Name="Part approved" Stacked="false" GroupName="grouoname2"
                                                                        DataFieldY="PartApprovedPreAuths">
                                                                        <LabelsAppearance Visible="false" DataFormatString="{0} part approved pre auths">
                                                                        </LabelsAppearance>
                                                                        <TooltipsAppearance Color="White" DataFormatString="{0} part approved pre-Auths">
                                                                        </TooltipsAppearance>
                                                                    </telerik:ColumnSeries>
                                                                    <telerik:ColumnSeries Name="Rejected" Stacked="false" GroupName="grouoname3" DataFieldY="RejectedPreAuths">
                                                                        <LabelsAppearance Visible="false" DataFormatString="{0} rejected pre auths">
                                                                        </LabelsAppearance>
                                                                        <TooltipsAppearance Color="White" DataFormatString="{0} rejected pre-Auths">
                                                                        </TooltipsAppearance>
                                                                    </telerik:ColumnSeries>
                                                                    <telerik:ColumnSeries Name="Cancelled" Stacked="false" GroupName="grouoname4" DataFieldY="CancelledPreAuths">
                                                                        <LabelsAppearance Visible="false" DataFormatString="{0} cancelled pre auths">
                                                                        </LabelsAppearance>
                                                                        <TooltipsAppearance Color="White" DataFormatString="{0} cancelled pre-Auths">
                                                                        </TooltipsAppearance>
                                                                    </telerik:ColumnSeries>
                                                                    <telerik:ColumnSeries Name="Not required" Stacked="false" GroupName="grouoname5"
                                                                        DataFieldY="NotRequiredPreAuths">
                                                                        <LabelsAppearance Visible="false" DataFormatString="{0} not required pre auths">
                                                                        </LabelsAppearance>
                                                                        <TooltipsAppearance Color="White" DataFormatString="{0} not required pre-Auths">
                                                                        </TooltipsAppearance>
                                                                    </telerik:ColumnSeries>
                                                                </Series>
                                                            </PlotArea>
                                                            <ChartTitle Text="Pre Auth status by date">
                                                            </ChartTitle>
                                                        </telerik:RadHtmlChart>
                                                            <table style="width: 100%;">
                                                            <tr>
                                                                <script type="text/javascript">
                                                                    function pnlRequestStarted(ajaxPanel, eventArgs) {
                                                                        if (eventArgs.EventTarget == "pnlBtnExcel") {
                                                                            eventArgs.EnableAjax = false;
                                                                        }
                                                                        else {
                                                                            eventArgs.EnableAjax = true;
                                                                        }

                                                                    }

                                                                </script>
                                                                <td style="width: 10px;">
                                                                </td>
                                                                <td style="width: 100%; text-align: right; vertical-align: bottom;">
                                                                    <asp:Button ID="pnlBtnExcel" Width="150px" Text="Export to Excel" OnClick="btnExcel_Click"
                                                                        runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        
                                                         <table id="Table1" border="0">
                    <tr>
                        <td>
                            <label>Sort by</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList3" runat="server" Width="150px">
                                <asp:ListItem Value="EST_AMT">Value</asp:ListItem>
                                <asp:ListItem Value="MBR_NAME">Volume</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                            <td>
                            <label>Sort as</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList2" runat="server" Width="150px">
                                <asp:ListItem Value="Ascending" Selected="True">Ascending</asp:ListItem>
                                <asp:ListItem Value="Descending">Descending</asp:ListItem>
                                <asp:ListItem Value="None">No Sort</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnAddExpression" runat="server" Text="Apply Sort" OnClick="btnAddExpression_Click"></asp:Button>
                        </td>
                           <td>
                            <asp:Button ID="Clear" runat="server" Text="Remove sorting" OnClick="Clear_Click"></asp:Button>
                        </td>
                            <asp:DropDownList Visible="false" ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                <asp:ListItem Value="True" Selected="True">True</asp:ListItem>
                                <asp:ListItem Value="False">False</asp:ListItem>
                            </asp:DropDownList>
                    
                        <td></td>
                    </tr>
                </table>
                                                        <telerik:RadGrid OnColumnCreated="PreAuthDetailsGrid_ColumnCreated" Width="995px"
                                                            ID="PreAuthDetailsGrid" runat="server" FilterMenu-Enabled="true" AllowPaging="True"
                                                            AllowSorting="True" CellSpacing="0" GridLines="Both" AllowFilteringByColumn="True"
                                                            ShowGroupPanel="True" Skin="Default">
                                                            <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                                                <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                    </div>
                                                </telerik:RadAjaxPanel>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td width="100%" valign="top">
                                        <uc1:OSNav ID="OSNav1" runat="server" />
                                    </td>
                                </tr>--%>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <uc3:Footer ID="Footer1" runat="server" />
<%--    &nbsp;<div class="copyrightContainer">
        <table width="995" align="center" border="0" cellspacing="0" cellpadding="0" class="footerLinks">
            <tr>
                <td class="copyrightContainerPad">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                Copyright © Bupa Arabia 2012. All rights reserved. <a href="http://www.bupa.com.sa/English/Pages/Terms-Conditions.aspx">
                                    Terms &amp; Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.bupa.com.sa/English/Pages/Sitemap.aspx">Sitemap</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                                        href="http://www.bupa.com.sa/English/Pages/Privacy-Policy.aspx">Privacy Policy</a>
                            </td>
                            <td align="right">
                                Powered by <a href="http://www.borninteractive.com/">Born Interactive</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>--%>
    <script type="text/javascript" src="../functions/jquery.anythingslider.min.js"></script>
    <script type="text/javascript" src="../functions/cufon.js"></script>
    <script type="text/javascript" src="../functions/Gotham_Book_400.font.js"></script>
    <script type="text/javascript" src="../functions/cufon.init.js"></script>
    <script type="text/javascript" src="../functions/functions.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            toggleFooter();
        });

        $(function () {
            $('#slider,#Helpslider').anythingSlider();
        });
    </script>
    </form>
</body>
</html>