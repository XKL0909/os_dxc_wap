using System;
using System.Web.UI.WebControls;
using System.Text;
using OS_DXC_WAP.CaesarWS;
public partial class CtntMembershipCert : System.Web.UI.Page
{
    string  strClientID ;   
    private ServiceDepot_DNService ws;

   //change the name of the constructor
    public CtntMembershipCert()
    {
        //InitializeComponent();
        this.Load += new EventHandler(CtntMembershipCert_Load);
}

//change the name of the constructor and the page title
    void CtntMembershipCert_Load(object sender, EventArgs e)
{
    Master.SearchButton.Click += SearchButton_Click ;
    if (string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
        Response.Redirect("../default.aspx");
    strClientID = Session["ClientID"].ToString();
    Master.PageTitle.Text = "Certificate of Membership";
    Master.SearchOpt.SelectedIndexChanged += rdbSearchOption_SelectedIndexChanged;
    //Master.SearchOpt.Visible = false;
   
}
    protected void rdbSearchOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        Master.SearchText.Text = "";
        Message1.Text = "";
        if (UpdatePanel2.Visible == true)
            UpdatePanel2.Visible = false;

    }

private void SearchButton_Click(object sender, EventArgs e)
{
     var aspPanel = (Panel)this.Master.FindControl("Panel123");
    var radioButtonList = (RadioButtonList)aspPanel.FindControl("rdbSearchOption");
    if (radioButtonList.SelectedIndex == 0)
		DisplayMemberExistForClientResult(Master.SearchText.Text, "Membership No");
	else
        DisplayMemberExistForClientResult(Master.SearchText.Text, "Staff No");
    
}

// change the name of calling function
 private void DisplayMemberExistForClientResult(string SearchText, string SearchType)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}
        

        EnqMbrListInfoRequest_DN request;
        EnqMbrListInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqMbrListInfoRequest_DN();
            request.customerNo = strClientID;

            if (SearchType == "Membership No")
            request.membershipNo = SearchText;
            else
            request.staffNo = SearchText;
            
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.detail == null)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = "Inputted '" + Master.SearchOpt.Text + "' does not exist in your group.";
            UpdatePanel2.Visible =false;
            Message1.Visible = true;
        }
            else
            {
                Session["MembershipNo"] = response.detail[0].membershipNo;
                Response.Redirect("../member/MembershipCert2.aspx", true);

                ////Response.Redirect("../member/MembershipCert2.aspx?mem=" + response.detail[0].membershipNo);
                if (SearchType == "Membership No")
                {
                    //below line needs to be updated
                    DisplayMemberCertResult(Master.SearchText.Text);
                    lblMemberName.Text = response.detail[0].memberName;
                    UpdatePanel2.Visible = true;
                    Message1.Visible = false;
                }
                else
                {
                    
                }

                
           //response.detail[0].relationship;
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }
            
            
            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }








    private void DisplayMemberCertResult(string MemberNo)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        ReqPrtMbrCrtInfoRequest_DN request;
        ReqPrtMbrCrtInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new ReqPrtMbrCrtInfoRequest_DN();
            request.membershipNo = long.Parse(MemberNo);
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ReqPrtMbrCrtInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.memberName != null)
            {
                DisplayMbrCertInfo(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                


            }
            else
                UpdatePanel2.Visible = false;
            Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            UpdatePanel2.Visible = false;
            //tabsModManager.Visible = false;

            Message1.Text = ex.Message; /// this message would be used by transaction
            Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }


    private void DisplayMbrCertInfo(ReqPrtMbrCrtInfoResponse_DN response)
    {


        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {
            //sbResponse.Append("Client TransactionID: ").Append(response.TransactionID).Append("\n");
            lblMemberName.Text = response.memberName;
            lblMembershipNo.Text = response.membershipNo.ToString();
            lblClass.Text = response.className;
            lblOptions.Text = response.option;
            lblIssueDate.Text = DateTime.Now.ToShortDateString();
            //response.
            // lblSponsorId.Text = response.MemberName;   /// lblSponsorId ??
            lblEffectiveFrom.Text = String.Format("{0:d}", response.effFrom);
            lblCustomerName.Text = response.customerName;
            lblDateOfBirth.Text = String.Format("{0:d}", response.DOB);
            lblStaffNo.Text = response.staffNo;
            lblContribution.Text = response.deductible;
            // lblEmployeeNo.Text = response.staffNo;
            lblEffectiveTo.Text = String.Format("{0:d}", response.effTo);
            //lblGender.Text = response.MemberName;  // lblGender
            // lblStatus.Text = response.status;
            lblRoomType.Text = response.roomType;
            UpdatePanel2.Visible = true;

            //sbResponse.Append("Client Status: ").Append(response.Status).Append("\n");
            //sbResponse.Append("Client CustomerNo: ").Append(response.CustomerNo.ToString()).Append("\n");
            //sbResponse.Append("Client ClassID: ").Append(response.ClassID.ToString()).Append("\n");
            //sbResponse.Append("Client BranchCode: ").Append(response.BranchCode.Trim()).Append("\n");
            //sbResponse.Append("Client MemberName: ").Append(response.MemberName);
        }
        else
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);

        //Message1.Text = sbResponse.ToString(); //MessageBox.Show(sbResponse.ToString());


    }


    
}
