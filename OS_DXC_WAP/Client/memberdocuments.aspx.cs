﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;

public partial class memberdocuments : System.Web.UI.Page
{
    private long reqDetailId;
    private AddMemberFileuploadService FileUploadService;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        if (!IsPostBack)
        {
            if (HttpContext.Current.Session["ReqDetailID"] != null)
            {
                reqDetailId = (long)HttpContext.Current.Session["ReqDetailID"];
                BindFile(Convert.ToInt32(reqDetailId));
            }
        }
    }

    private void BindFile(int requestId)
    {
        FileUploadService = new AddMemberFileuploadService();
        List<Fileupload> Fileuploads = new List<Fileupload>();
        Fileuploads = FileUploadService.FindByRequestID(requestId);
        RptUpload.DataSource = Fileuploads;
        RptUpload.DataBind();
    }


    [WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static int DeleteMemberFiles(int id)
    {
        var FileUploadService = new AddMemberFileuploadService();
        var result = FileUploadService.Remove(id);
        return result;
    }
    
}


   