﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="step1ed" Codebehind="step1ed.aspx.cs" %>



<form id="form1" runat="server">
   
    <div class="card bb">
        <div class="header bb">
            <h2>Add </h2>
        </div>
        <div class="body bb">
            <div class="row clearfix margin-top-20 bb">
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                             <label id="lblMemberOrSponsor" class="control-label bb">Sponsor / رقم الكفيل</label>
                            <asp:dropdownlist  cssclass="form-control bb" id="drplistSponsor" runat="server"></asp:dropdownlist>
                            <asp:requiredfieldvalidator validationgroup="step1" id="reqValdrplistSponsor" Display="Dynamic" controltovalidate="drplistSponsor" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">ID / رقم الهوية</label>
                            <asp:textbox cssclass="form-control bb" runat="server" id="txtID" maxlength="10"></asp:textbox>
                             <asp:RegularExpressionValidator  validationgroup="step1"  ID="RegularExpressionValidator2" runat="server"    
                            ErrorMessage="Length must be 10 characters/10 ارقام فقط"
                            ControlToValidate="txtID" 
                                 Display="Dynamic"
                           setfocusonerror="true"
                            ValidationExpression="^[0-9]{10,10}$" />
                            <asp:requiredfieldvalidator validationgroup="step1" id="reqValtxtID" controltovalidate="txtID" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true" Display="Dynamic"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row clearfix bb" >
                <div class="col-md-6 bb" id="sectionHijari">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">Hijri Year of Birth / السنة الهجرية للولادة</label>
                             <asp:TextBox CssClass="form-control bb" runat="server" ID="txtHYearOfBirth" MaxLength="4"></asp:TextBox>
                      <asp:RegularExpressionValidator  validationgroup="step1"  ID="RegExp1" runat="server"    
                            ErrorMessage="Length must be 4 characters/اربع ارقام فقط"
                            ControlToValidate="txtHYearOfBirth" 
                           setfocusonerror="true"
                          Display="Dynamic"
                            ValidationExpression="^[0-9]{4,4}$" />
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="reqValtxtHYearOfBirth" ControlToValidate="txtHYearOfBirth" runat="server" errormessage="Field is mandatory/الحقل إجباري" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                 <div class="col-md-6 bb" >
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">Dependent Hijri Year of Birth / السنة الهجرية لولادة التابع</label>
                             <asp:TextBox CssClass="form-control bb" runat="server" ID="txtDepHYearOfBirth" MaxLength="4"></asp:TextBox>
                      <asp:RegularExpressionValidator  validationgroup="step1"  ID="RegularExpressionValidator1" runat="server"    
                            ErrorMessage="Length must be 4 characters/4 ارقام فقط"
                            ControlToValidate="txtDepHYearOfBirth" 
                           setfocusonerror="true"
                         Display="Dynamic"
                            ValidationExpression="^[0-9]{4,4}$" />
                        <asp:RequiredFieldValidator ValidationGroup="step1" ID="RequiredFieldValidator1" ControlToValidate="txtDepHYearOfBirth" runat="server" errormessage="Field is mandatory/الحقل إجباري"  Display="Dynamic"  SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="actions clearfix pull-right">

                <asp:button id="btnNext" validationgroup="step1" runat="server" text="Next" class="btn btn-primary nextBtn btn-sm pull-right btn-override bb" />
            </div>

        </div>
    </div>
</form>
<script src="js/step1ed.js"></script>
