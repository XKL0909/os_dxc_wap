﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="ClientMaster.master" Inherits="CtntMembershipCert" Debug="true" Codebehind="CtntMembershipCert.aspx.cs" %>

<%@ MasterType VirtualPath="ClientMaster.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>


<asp:Content ID="Content1_1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
         
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
   

       
        <asp:UpdatePanel ID="UpdatePanel2" runat="server"  Visible="false">
            <ContentTemplate>
                <br />
                <br />
                <table style="width: 100%; padding-left:0px; padding-right:0px;" cellspacing="0"   >
                    <tr >
                        <td style="width: 499px" class="style13">
                            <b class="style11">Attention all BUPA Middle East Network</b></td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style2" style="width: 40%">
                            السادة / شبكة مستشفيات بوبا الشرق الأوسط
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 499px; height: 30px;">
                        </td>
                        <td class="style1" style="width: 127px; height: 30px;">
                        </td>
                        <td class="style3" style="height: 30px">
                        </td>
                    </tr>
                    <tr>
                        <td class="style11">
                            TO WHOM IT MAY CONCERN</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            <b>لمن يهمه الأمر</b></td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            &nbsp;</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            <span class="style12">Subject: </span><span class="style11">Certificate of Membership</span></td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            الموضوع: شهادة عضوية</td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            &nbsp;</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 499px" class="style8">
                            Certificate Issue Date</td>
                        <td class="style14" style="width: 127px">
                            <strong class="style10">
                                <asp:Label ID="lblIssueDate" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td class="style3">
                            تاريخ الاصدار
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 499px" class="style8">
                            This is to confirm that the below mentioned member is medically covered under the
                            BUPA Middle East Health Care scheme with the following information:</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            إن هذه الوثيقة تؤكد ان المذكور مغطى طبيا
                            <br />
                            تحت برنامج بوبا الشرق الاوسط للرعاية الصحية وفقا للبيانات التالية
                        </td>
                    </tr>
                </table>
                <br>
                <table style="width: 100%">
                    <tr>
                        <td align="left" class="style9" style="width: 30%">
                            Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblMemberName" runat="server"></asp:Label></strong></td>
                        <td align="right" class="style6" style="width: 30%">
                            الاسم</td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Class Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblClass" runat="server"></asp:Label></strong></td>
                        <td align="right" class="style6">
                            درجة التغطية
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Membership No:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblMembershipNo" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            رقم العضوية
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Option*:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblOptions" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            قائمة المزايا
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Room:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblRoomType" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            درجة الإقامة
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Year of Birth:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblDateOfBirth" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            تاريخ الميلاد
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Effective From:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblEffectiveFrom" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            صالحة من تاريخ&nbsp;
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Effective To:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblEffectiveTo" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            صالحة الى تاريخ
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Contribution:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblContribution" runat="server" Text="Label"></asp:Label></strong></td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">نسبة المشاركة</span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="style9" style="width: 320px">
                            Staff No.</td>
                        <td class="style10">
                            <asp:Label ID="lblStaffNo" runat="server"></asp:Label>&nbsp;</td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">الرقم الوظيفي </span>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="style9" style="width: 320px">
                            Customer Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblCustomerName" runat="server"></asp:Label></strong></td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">أسم العميل</span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 320px">
                            &nbsp;</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 320px" class="style8">
                            All Other terms and conditions are as per policy</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            جميع شروط الغطاء تخضع لبنود التعاقد</td>
                    </tr>
                    <tr>
                        <td style="width: 320px">
                            &nbsp;</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                </table>
                <p>
                </p>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 40%" class="style8" valign="top">
                            This certificate is issued as a temporary cover for this member. Should this member
                            need any treatment that is covered under this scheme, please provide it, and charge
                            BUPA Middle East.</td>
                        <td>
                            &nbsp;</td>
                        <td align="right" class="th" colspan="3" style="width: 40%" valign="top">
                            إن هذه الشهادة اصدرت مؤقتاً للعضو وفي حالة حاجته الى اية خدمات علاجية مدرجة تحت
                            بنود الغطاء التأميني الخاص به الرجاء تقديم الخدمة العلاجية و ارسال الفواتير الى
                            بوبا الشرق الاوسط
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 437px" class="style8" valign="top">
                            Should you have any queries, please contact one of our advisers on the BUPA Middle
                            East dedicated customer service help lines 800 244 0307. This help
                            line is open 24hrs seven days a week.</td>
                        <td style="width: 55px">
                            &nbsp;</td>
                        <td align="right" class="th" colspan="3" valign="top">
                            لمزيد من المعلومات الرجاء الاتصال بقسم خدمات العملاء هاتف او الهاتف المجاني
                            0307 244 800 على مدار الساعة طوال ايام الاسبوع
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 437px" class="style8" valign="top">
                            This certificate is valid only for the named insured person when accompanied with
                            ID documents.
                        </td>
                        <td style="width: 55px">
                            &nbsp;</td>
                        <td align="right" class="th" colspan="3" valign="top">
                            إن هذه الشهادة صالحة فقط للعضو المذكور اعلاه شرط توفر وثيقة اثبات رسمية
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td>
                            <b><i class="style8">This is only a definition for the codes shown under the option
                                field. Do not use for coverage checking.</i></b></td>
                    </tr>
                    <tr>
                        <td class="style8">
                            * A: Dialysis, C: Chronic, D: Dental, E: Neo-natal care, H: Other Conditions, K:
                            Cosmetic, L: Congenital, M: Maternity, N: Natural Changes, O: Optical, P: Developmental,
                            S: Standard Benefit, T: Screening, V: Vaccinations, Y: Physiotherapy, Z: Hazardous
                            Sports Injury
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    
    <asp:Label ID="Message1" Font-Names="verdana" Width="95%" runat="server" ></asp:Label>
    
</asp:Content>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    View and print membership certificates for your staff
</asp:Content>
