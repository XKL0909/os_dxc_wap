using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Configuration;
using System.Linq;
using WebApp;
using OS_DXC_WAP.CaesarWS;
using System.Configuration;
using System.IO;
using System.Collections;

public partial class ChangeBranch : System.Web.UI.Page
{
    private ServiceDepot_DNService ws = new ServiceDepot_DNService();
    private string strClientID;
    private string strMemberGroup;
    private string strMemberType;
    private string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();
    // The upload category for the uploading of this page
    private string _uploadCategory = UploadCategory.MEMBER_CHGCLASS;

    string optionType;
    private Hashtable hasQueryValue;

    private void BindDdlProfession()
    {
        ReqProfListRequest_DN request = new ReqProfListRequest_DN();
        //request.TransactionID = TransactionManager.TransactionID();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        ws = new ServiceDepot_DNService();
        ReqProfListResponse_DN response;
        response = ws.ReqProfList(request);
        ddProfession.DataSource = response.detail;
        ddProfession.DataBind();
    }

    private void BindDdlDistrict()
    {
        ReqDistListRequest_DN request = new ReqDistListRequest_DN();
        //request.TransactionID = TransactionManager.TransactionID();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        ws = new ServiceDepot_DNService();
        ReqDistListResponse_DN response;
        response = ws.ReqDistList(request);
        ddDistrict.DataSource = response.detail;
        ddDistrict.DataBind();

    }

    private void BindDdlENationality(bool IsGCC)
    {
        NationalityControl MyNationalityC = new NationalityControl();
        List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
        MyNationalities.Insert(0, new Nationality(0, "", "- Select -", false));
        ddlENationality.DataValueField = "Code";
        ddlENationality.DataTextField = "Nationality1";
        ddlENationality.DataSource = MyNationalities;
        ddlENationality.DataBind();
    }

    protected void ddProfession_DataBound(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddProfession.Items.Insert(0, new ListItem("-Select-", ""));
            ddProfession.SelectedIndex = 0;
        }
    }

    protected void ddDistrict_DataBound(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddDistrict.Items.Insert(0, new ListItem("-Select-", ""));
            ddDistrict.SelectedIndex = 0;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            strClientID = Session["ClientID"].ToString();
            lblError.Visible = false;
            lblError.Text = "";

            hasQueryValue = new Hashtable();
            string queryStringValues = string.Empty;

            if (Request.QueryString.Count > 0)
            {
                try
                {
                    var val = Request.QueryString["val"];
                    queryStringValues = Cryption.Decrypt(val);
                    hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                    optionType = hasQueryValue.ContainsKey("optiontype") ? Convert.ToString(hasQueryValue["optiontype"]) : string.Empty;
                }
                catch (Exception)
                {
                    lblError.Text = "Invalid rquest!";
                    return;
                }
            }

            if (!string.IsNullOrEmpty(optionType) && optionType.ToLower().Trim() == "class")
                lnkBatchUpload.NavigateUrl = "BatchUpload.aspx?val=" + Cryption.Encrypt("optiontype=changeclass");
            else
                lnkBatchUpload.NavigateUrl = "BatchUpload.aspx?val=" + Cryption.Encrypt("optiontype=changebranch");

           
            if (!Page.IsPostBack)
            {
                DivStafflbl.Style.Add("display", "none");
                DivStafftxt.Style.Add("display", "none");
                lblAramcoMessage.Visible = false;
                Label1.Visible = true;
                /***** Fresh Page Load *****/
                BindDdlDistrict();
                BindDdlProfession();
                BindDdlENationality(false);
                ////if (Request.QueryString["OptionType"] != null && Request.QueryString["OptionType"].ToLower().Trim() == "class")
                if (!string.IsNullOrEmpty(optionType) && optionType.ToLower().Trim() == "class")
                {
                    btnChangeBranch.Visible = false;
                    btnChangeClass.Visible = true;
                    lblAdd_Employee.Text = "Change Class";
                    ///lnkBatchUpload.NavigateUrl = "~/Client/BatchUpload.aspx?OptionType=ChangeClass";

                    lblOptionNew.Text = "New Class";
                    lblOptionOld.Text = "Old Class";

                    ddlChangeReason.Items.Clear();
                    ListItem DepList0 = new ListItem("Select from the list", "0");
                    ddlChangeReason.Items.Add(DepList0);
                    ListItem DepList1 = new ListItem("Member Promotions", "CLA001");
                    ddlChangeReason.Items.Add(DepList1);
                    ListItem DepList2 = new ListItem("Member Downgrade", "CLA002");
                    ddlChangeReason.Items.Add(DepList2);
                    ListItem DepList3 = new ListItem("Product Modification/Additional Class", "CLA003");
                    ddlChangeReason.Items.Add(DepList3);
                    ListItem DepList4 = new ListItem("Group/Customer Request", "CLA004");
                    ddlChangeReason.Items.Add(DepList4);
                    ListItem DepList5 = new ListItem("Master List Error", "CLA005");
                    ddlChangeReason.Items.Add(DepList5);
                    ListItem DepList6 = new ListItem("Others", "CLA006");
                    ddlChangeReason.Items.Add(DepList6);
                }
                else
                {
                    typecontainer.Visible = false;
                    expdatecontainer.Visible = false;
                    districtcontainer.Visible = false;
                    mobilecontainer.Visible = false;
                    professioncontainer.Visible = false;
                    maritalstatuscontainer.Visible = false;
                    nationalitycontainer.Visible = false;
                    idnumbercontainer.Visible = false;
                }

                //Aramco PId Changes by Hussamuddin 
                //changed the scoop for AramcoContractType 
                if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
                {

                    DivStafflbl.Style.Add("display", "block");
                    DivStafftxt.Style.Add("display", "block");
                    lblAramcoMessage.Visible = true;
                    Label1.Visible = false;
                    ////if (Request.QueryString["OptionType"] != null && Request.QueryString["OptionType"].ToLower().Trim() == "class")
                    if (!string.IsNullOrEmpty(optionType) && optionType.ToLower().Trim() == "class")
                    {
                        lblAdd_Employee.Text = "Change Scheme";
                        lblOptionNew.Text = "New Scheme";
                        lblOptionOld.Text = "Old Scheme";
                        typecontainer.Visible = false;
                        expdatecontainer.Visible = false;
                        districtcontainer.Visible = false;
                        mobilecontainer.Visible = false;
                        professioncontainer.Visible = false;
                        maritalstatuscontainer.Visible = false;
                        nationalitycontainer.Visible = false;
                        idnumbercontainer.Visible = false;
                    }
                    LblChangeBranchBanner.InnerText = "Whether your request is to add members, delete members, change scheme, change branch or replace members cards, you may submit your transaction at your convenience.";

                }

                //Aramco PId Changes by Hussamuddin
            }
            if (Convert.ToString(Session["ContractType"]).ToUpper() != AramcoContractType)
            {
                ////if (Request.QueryString["OptionType"] == null && Request.QueryString["OptionType"] != "Class")
                if (!string.IsNullOrEmpty(optionType) && optionType.ToLower().Trim() == "class")
                {
                    _uploadCategory = UploadCategory.MEMBER_CHGCLASS;
                }
                else
                {
                    _uploadCategory = UploadCategory.MEMBER_CHGBRANCH;
                }

                SetupUploader(_uploadCategory);
            }

        }
        catch (Exception ex)
        {
            lblError.Text = "we are unable to process your request.Please try later";// ex.Message + ex.StackTrace;  
            CommonClass.LogError("ChangeBranch_button1", ex.Message, ex.InnerException, ex.StackTrace, (Session[WebPublication.Session_ClientUsername] != null ? Session[WebPublication.Session_ClientUsername].ToString() : ""));
        }
    }

    private void SetupUploader(string uploadCategory)
    {
        uploader.Visible = true;

        // By default, unless a user has uploaded supporting documents,
        // the Submit button will be disabled
        btnChangeClass.Enabled = false;

        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        // Change Branch has optional upload
        if (!IsPostBack)
        {
            if (_uploadCategory == UploadCategory.MEMBER_CHGBRANCH)
            {
                //btnChangeBranch.Enabled = true;
                tblUploader.Visible = true;
            }
            else
            {
                // Remove the upload option for Change Class as this is not applicable
                btnChangeClass.Enabled = true;
                tblUploader.Visible = false;
            }
        }

        // Bind to allow resolving of # tags in mark-up
        DataBind();
    }

    protected void Page_LoadComplete(object sender, System.EventArgs e)
    {

        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            tblUploader.Visible = false;

        }
        else
        {

            // Enable / Disable the submission option depending on whether the user 
            // has uploaded supported documents

            // Assume the button is disabled until checked
            btnChangeClass.Enabled = false;
            //btnChangeBranch.Enabled = false;

            // Change Branch has optional upload
            if (_uploadCategory == UploadCategory.MEMBER_CHGBRANCH)
            {
                //btnChangeBranch.Enabled = true;
                tblUploader.Visible = true;
            }
            else
            {
                // Remove the upload option for Change Class as this is not applicable
                btnChangeClass.Enabled = true;
                tblUploader.Visible = true;
            }

            // Get what has been uploaded
            DataSet uploadedFileSet = uploader.UploadedFileSet;

            UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);

            if (_uploadmanger.WebIndecator("SuportDocs", strClientID))
            {
                tblUploader.Visible = true;

                int uploadedCount = 0;
                if (uploadedFileSet != null && uploadedFileSet.Tables.Count > 0)
                {
                    uploadedCount = uploadedFileSet.Tables[0].Rows.Count;
                    if (uploadedCount > 0)
                    {
                        btnChangeClass.Enabled = true;
                        //btnChangeBranch.Enabled = true;
                    }
                    else
                    {
                        btnChangeClass.Enabled = false;
                        //btnChangeBranch.Enabled = false;
                    }
                }
            }
            else
            {
                tblUploader.Visible = true;
                btnChangeClass.Enabled = true;
                //btnChangeBranch.Enabled = true;
            }
        }
    }

    // function used for Tab System when user requset for dependent ceritifcate
    protected void btnRequestDependentCert_Click(object sender, EventArgs args)
    {
    }

    private void fncChangeBranch()
    {
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;

        #region Caesar Invocation

        ws = new ServiceDepot_DNService();
        SubTxnRequest_DN request;
        SubTxnResponse_DN response;
        request = new SubTxnRequest_DN();

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.TransactionType = "BRANCH CHANGE";
        request.TransactionID = WebPublication.GenerateTransactionID();
        request.BatchIndicator = "N";
        request.detail = new AllMbrDetail_DN[1];
        request.detail[0] = new AllMbrDetail_DN();
        request.detail[0].sql_type = "CSR.SUB_TXN_REC";
        request.detail[0].ContractNo = strClientID;
        request.detail[0].MbrName = txtMemberName.Text;
        request.memberName = new String[1];
        request.memberName[0] = txtMemberName.Text;
        string memberShipNumber = "";
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            if (!string.IsNullOrEmpty(txtBadgeNo.Text))
            {
                memberShipNumber = BLCommon.ReturnMemberShipNumberForStaffnumber(txtBadgeNo.Text.Trim(), Convert.ToString(Session["ClientID"]));

                if (memberShipNumber.Contains(":"))
                {
                    lblError.Text = memberShipNumber;
                    return;
                }
                else
                {
                    // Grab the memberID
                    if (!string.IsNullOrEmpty(memberShipNumber))
                    {
                        // Fetch the member's details
                        MemberHelper mh = new MemberHelper(memberShipNumber, strClientID);
                        Member m = mh.GetMemberDetails();
                        //HttpContext.Current.Response.Write(m.Nationality);
                        if (Check_Member_belong_to_client(m.ContractNumber))
                        {
                            request.detail[0].MembershipNo = memberShipNumber;
                        }
                        else
                        {
                            lblError.Visible = true;
                            lblError.Text = "Inputted 'Membership No' does not exist in your group.";
                            return;
                        }
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtMembershipNo.Text))
                {
                    request.detail[0].MembershipNo = txtMembershipNo.Text;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "Input Membership/Badge number";
                    return;
                }
            }
        }
        else
            if (!string.IsNullOrEmpty(txtMembershipNo.Text))
        {
            request.detail[0].MembershipNo = txtMembershipNo.Text;
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = "Input Membership number";
            return;
        }
        request.detail[0].Reason = ddlChangeReason.SelectedValue;
        request.detail[0].EffectiveDate = DateTime.ParseExact(txtDateOfEffect.Text, "dd/MM/yyyy", null);
        request.detail[0].ExistingBranch = ddlBranchCodeOldValue.Text.Trim();
        request.detail[0].NewBranch = ddlBranchCodeNew.SelectedValue;
        request.detail[0].StaffNo = "";
        request.detail[0].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
        try
        {
            response = ws.SubTxn(request);
            StringBuilder sb = new StringBuilder(200);

            if (response.Status != "0")
            {

                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/> ");

                lblError.Text = sb.ToString();
                lblError.Text += ".";

                Panel1.Visible = false;
                lblError.Visible = true;
                lblAramcoMessage.Visible = false;
                caesarInvokeError = true;
            }
            else
            {
                imgSuc.Visible = true;
                Message1.Text = "Thank you for submitting your request. Your reference number is " + response.ReferenceNo.ToString() + ". We are now validating the submitted file.";
                lnkChangeOption.Text = "Change branch for another Employee";
                lnkChangeOption.NavigateUrl = "changebranch.aspx";
                lnkChangeOption.Visible = true;

                Panel1.Visible = false;
                Message1.Visible = true;
                lblAramcoMessage.Visible = false;
                Label1.Visible = false;
                caesarInvokeError = false;
            }
        }
        catch (Exception ex)
        {
            Panel1.Visible = false;

            Message1.Text = ex.Message; /// this message would be used by transaction
            lblError.Text = "Error Encountered. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            lblError.Visible = true;
            lblAramcoMessage.Visible = false;
            Label1.Visible = false;
        }

        #endregion

        if (!caesarInvokeError)
        {
            // Send the email related to the attachments
            string username = "Unknown username";
            string fullName = "Unknown client";
            string[] addressees = UploadPublication.MembershipEmailAddresses();

            if (Session[WebPublication.Session_ClientUsername] != null)
                username = Session[WebPublication.Session_ClientUsername].ToString();
            if (Session[WebPublication.Session_ClientFullName] != null)
                fullName = Session[WebPublication.Session_ClientFullName].ToString();

            // Delegate to send emails
            //uploader.Notify(uploader.SessionID, _uploadCategory, caesarReferenceID, addressees, fullName, username);

            // Once done, hide the uploader from the view
            tblUploader.Visible = false;
        }
    }

    private void fncChangeClass()
    {
        bool caesarInvokeError = false;
        string caesarReferenceID = string.Empty;

        #region Caesar Invocation

        ws = new ServiceDepot_DNService();

        SubTxnRequest_DN request;
        SubTxnResponse_DN response;

        request = new SubTxnRequest_DN();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        request.TransactionType = "CLASS CHANGE";
        //request.TransactionID = TransactionManager.TransactionID();
        request.TransactionID = WebPublication.GenerateTransactionID();

        request.BatchIndicator = "N";
        request.detail = new AllMbrDetail_DN[1];

        request.detail[0] = new AllMbrDetail_DN();
        request.detail[0].sql_type = "CSR.SUB_TXN_REC";
        request.detail[0].ContractNo = strClientID; // "10216200";
        request.detail[0].MbrName = txtMemberName.Text;
        string memberShipNumber = "";
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            if (!string.IsNullOrEmpty(txtBadgeNo.Text))
            {
                memberShipNumber = BLCommon.ReturnMemberShipNumberForStaffnumber(txtBadgeNo.Text.Trim(), Convert.ToString(Session["ClientID"]));
                if (memberShipNumber.Contains(":"))
                {
                    lblError.Text = memberShipNumber;
                    return;
                }
                else
                {
                    // Grab the memberID
                    if (!string.IsNullOrEmpty(memberShipNumber))
                    {
                        // Fetch the member's details
                        MemberHelper mh = new MemberHelper(memberShipNumber, strClientID);
                        Member m = mh.GetMemberDetails();
                        //HttpContext.Current.Response.Write(m.Nationality);
                        if (Check_Member_belong_to_client(m.ContractNumber))
                        {
                            request.detail[0].MembershipNo = memberShipNumber;
                        }
                        else
                        {
                            lblError.Visible = true;
                            lblError.Text = "Inputted 'Membership No' does not exist in your group.";
                            return;
                        }
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtMembershipNo.Text))
                {
                    request.detail[0].MembershipNo = txtMembershipNo.Text;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "Input Membership/Badge number";
                    return;
                }
            }

        }
        else
            if (!string.IsNullOrEmpty(txtMembershipNo.Text))
        {
            request.detail[0].MembershipNo = txtMembershipNo.Text;
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = "Input Membership number";
            return;
        }

        request.detail[0].Reason = ddlChangeReason.SelectedValue;
        request.detail[0].EffectiveDate = DateTime.ParseExact(txtDateOfEffect.Text, "dd/MM/yyyy", null);
        request.detail[0].ExistingClass = ddlBranchCodeOldValue.Text;
        request.detail[0].NewClass = ddlBranchCodeNew.SelectedValue;
        request.detail[0].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
        request.detail[0].IgamaID = txtSaudiIqamaIDNumber.Text;
        if (Session["ClientUsername"] != null)
        {
            if (Session["ClientUsername"].ToString().Length > 10)
            {
                request.submitBy = Session["ClientUsername"].ToString().Substring(0, 10);
            }
            else
            {
                request.submitBy = Session["ClientUsername"].ToString();
            }
        }
        request.detail[0].ID_Type = ddlEIDType.SelectedValue;
        if (txtEIDExpDate.Text != "")
            request.detail[0].ID_Expiry_Date = DateTime.ParseExact(txtEIDExpDate.Text, "dd/MM/yyyy", null);

        if (txtEMobileNo.Text != "")
        {
            request.detail[0].MOBILE_NO = txtEMobileNo.Text;
            request.detail[0].Telephone = txtEMobileNo.Text;
            request.detail[0].MBR_MOBILE_NO = txtEMobileNo.Text;
            request.detail[0].Phone = txtEMobileNo.Text;
        }
        request.detail[0].CCHI_Job_Code = ddProfession.SelectedValue;
        request.detail[0].CCHI_City_Code = ddDistrict.SelectedValue;
        request.detail[0].Nationality = getNationalityForCaesarRequest(ddlENationality);
        if (ddlEMaritalStatus.SelectedValue != "")
        {
            request.detail[0].Marital_Status = ddlEMaritalStatus.SelectedValue;
        }
        try
        {
            response = ws.SubTxn(request);
            StringBuilder sb = new StringBuilder(200);
            if (response.Status != "0")
            {

                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("<br/>");

                lblError.Text = sb.ToString();
                Panel1.Visible = false;
                lblError.Visible = true;
                lblAramcoMessage.Visible = false;
                Label1.Visible = false;
                caesarInvokeError = true;
            }
            else
            {
                lnkChangeOption.Text = "Change class for another Employee";
                lnkChangeOption.NavigateUrl = "changebranch.aspx?optiontype=Class";
                lnkChangeOption.Visible = true;
                imgSuc.Visible = true;
                Message1.Text = "Thank you for submitting your request. Your reference number is " + response.ReferenceNo.ToString() + ". We are now validating the submitted file.";
                Panel1.Visible = false;
                Message1.Visible = true;
                lblAramcoMessage.Visible = false;
                Label1.Visible = false;
                caesarInvokeError = false;
                caesarReferenceID = response.ReferenceNo.ToString();
            }
        }
        catch (Exception ex)
        {
            Panel1.Visible = false;
            Message1.Text = ex.Message; /// this message would be used by transaction
            lblError.Text = "Error Encountered. Please contact the administrator for further information.";
            lblError.Visible = true;
            lblAramcoMessage.Visible = false;
            Label1.Visible = false;
        }

        #endregion

        if (!caesarInvokeError)
        {
            string username = "Unknown username";
            string fullName = "Unknown client";
            string[] addressees = UploadPublication.MembershipEmailAddresses();

            if (Session[WebPublication.Session_ClientUsername] != null)
                username = Session[WebPublication.Session_ClientUsername].ToString();
            if (Session[WebPublication.Session_ClientFullName] != null)
                fullName = Session[WebPublication.Session_ClientFullName].ToString();
            tblUploader.Visible = false;
        }
    }


    private String UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        String constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    private Byte[] StringToUTF8ByteArray(String pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        Byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }


    protected void btnChangeBranch_Click(object sender, EventArgs e)
    {
        fncChangeBranch();
    }

    // function used for Tab System to retrieve family list
    private void DisplayBranchListResult(ReqBrhListResponse_DN response)
    {
        if (response.status == "0")
        {
            ddlBranchCodeNew.Items.Clear();

            ListItem DepList0 = new ListItem();
            DepList0.Text = "Please select from the list";
            DepList0.Value = "0";
            ddlBranchCodeNew.Items.Add(DepList0);

            foreach (BrhListDetail_DN dtl in response.detail)
            {


                ListItem DepList = new ListItem();
                DepList.Text = dtl.branchDesc;
                DepList.Value = dtl.branchCode.ToString();
                ddlBranchCodeNew.Items.Add(DepList);

            }
        }
        else
        {
        }
    }

    private bool CheckMemberTypeExist(int intNewclassID)
    {
        ServiceDepot_DNService ws;
        EnqContMbrTypeListRequest_DN request;
        EnqContMbrTypeListResponse_DN response;
        request = new EnqContMbrTypeListRequest_DN();
        ws = new ServiceDepot_DNService();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        long ls_txnid = WebPublication.GenerateTransactionID();
        request.transactionID = ls_txnid;
        request.contNo = strClientID;
        request.contCls = intNewclassID; ;
        request.grp_Type = strMemberGroup;

        response = ws.EnqContMbrTypeList(request);

        if (response.status == "0")
        {
            for (int i = 0; i < response.detail.Length; i++)
            {
                if (response.detail[i].mbrTypeDesc == strMemberType)
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            return false;
        }
    }

    protected void PopulateBranchList(string ClientID)
    {
        {
            ws = new ServiceDepot_DNService();

            ReqBrhListRequest_DN request = new ReqBrhListRequest_DN();
            ReqBrhListResponse_DN response;
            request.membershipNo = long.Parse(strClientID);

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = TransactionManager.TransactionID();

            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.ReqBrhList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    lblError.Text = sb.ToString();
                }
                else
                {
                    DisplayBranchListResult(response);


                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }

    private void DisplayClassListResult(ReqClsListResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            //sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            //ddlBranchCodeOld.Items.Clear();
            ddlBranchCodeNew.Items.Clear();

            ListItem DepList0 = new ListItem();
            DepList0.Text = "Please select from the list";
            DepList0.Value = "0";
            //ddlBranchCodeOld.Items.Add(DepList0);
            ddlBranchCodeNew.Items.Add(DepList0);

            foreach (ClsListDetail_DN dtl in response.detail)
            {
                if (CheckMemberTypeExist(int.Parse(dtl.classID.ToString())))
                {
                    ListItem DepList = new ListItem();
                    DepList.Text = dtl.className;
                    DepList.Value = dtl.classID.ToString();

                    //ddlBranchCodeOld.Items.Add(DepList);
                    ddlBranchCodeNew.Items.Add(DepList);
                }
            }
        }
        else
        {
        }

    }

    protected void PopulateClassList(string ClientID)
    {

        {
            ws = new ServiceDepot_DNService();

            ReqClsListRequest_DN request = new ReqClsListRequest_DN();
            ReqClsListResponse_DN response;
            ////Disabled by sakthi on 27-Dec-2015 for request need pass membership number. not a contact number.
            ////Old code
            ////request.membershipNo = long.Parse(strClientID);
            ////New code
            request.membershipNo = long.Parse(ClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = TransactionManager.TransactionID();// long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = ws.ReqClsList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    lblError.Text = sb.ToString();
                }
                else
                {
                    DisplayClassListResult(response);


                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

    }

    protected void btnChangeClass_Click(object sender, EventArgs e)
    {
        fncChangeClass();
    }

    bool Check_Member_belong_to_client(string strMemberContractNo)
    {
        if (strClientID == strMemberContractNo)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        try
        {
            string MEmberORStaff = "";
            if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(txtMembershipNo.Text.Trim())))
                {
                    RFVtxtBadgeNo.Enabled = false;
                    MEmberORStaff = txtMembershipNo.Text.Trim();

                }
                else if (!string.IsNullOrEmpty(Convert.ToString(txtBadgeNo.Text.Trim())))
                {
                    RequiredFieldValidator13.Enabled = false;
                    EnqMbrListInfoRequest_DN request = new EnqMbrListInfoRequest_DN();
                    EnqMbrListInfoResponse_DN response = new EnqMbrListInfoResponse_DN();
                    response = null;
                    request = new EnqMbrListInfoRequest_DN();
                    request.customerNo = strClientID;
                    request.staffNo = txtBadgeNo.Text.Trim();

                    //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
                    request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
                    //request.IqamaNo =  "5523234443";
                    //request.TotalNoOfFamily = "3";
                    request.Username = WebPublication.CaesarSvcUsername;
                    request.Password = WebPublication.CaesarSvcPassword;
                    response = ws.EnqMbrListInfo(request);
                    StringBuilder sb = new StringBuilder(200);
                    if (response.errorID != "0")
                    {
                        sb.Append(response.errorMessage).Append("\n");
                        lblError.Visible = true;
                        string bageNochange = sb.ToString();
                        lblError.Text = bageNochange.Replace("staff", "Badge");
                        return;
                    }
                    else
                        MEmberORStaff = Convert.ToString(response.detail[0].membershipNo);

                }

            }
            else
            {
                RFVtxtBadgeNo.Enabled = false;
                MEmberORStaff = txtMembershipNo.Text.Trim();
            }
            MemberHelper mh = new MemberHelper(MEmberORStaff, strClientID);
            Member m = mh.GetMemberDetails();

            if (Check_Member_belong_to_client(m.ContractNumber))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(m.MemberName)))
                {

                    txtMemberName.Text = m.MemberName.Trim();
                    if (m.IDType != "")
                        ddlEIDType.SelectedValue = m.IDType.Trim();
                    if (m.IDExpiryDate.ToString() != "")
                    {
                        txtEIDExpDate.Text = DateTime.Parse(m.IDExpiryDate.ToString(), null, System.Globalization.DateTimeStyles.None).ToShortDateString();
                    }

                    strMemberType = m.MemberType;

                    switch (m.MemberType.ToString().ToLower())
                    {
                        case "employee":
                        case "senior employee":////Added by sakthi on 02-Aug-2016 
                        case "female employee with maternity":////////Added by sakthi on 17-Aug-2016 
                            strMemberGroup = "E";
                            break;
                        default:
                            strMemberGroup = "D";
                            break;
                    }

                    txtEMobileNo.Text = m.MOBILE.Trim();
                    ProfCodeControl MyCodeC = new ProfCodeControl();
                    List<string> MyCodes = MyCodeC.GetAllProfCodesAsString();

                    if (MyCodes.Contains(m.Profession.Trim()))
                    {
                        ddProfession.SelectedValue = m.Profession.Trim();
                    }
                    ddDistrict.SelectedValue = m.District.Trim();

                    //ddlENationality.SelectedValue = m.Nationality.Trim();
                    //ddlENationality.SelectedIndex = ddlENationality.Items.IndexOf(ddlENationality.Items.FindByValue(m.Nationality.Trim()));
                    setNationalityinDropDownList(m.Nationality.Trim(), ddlENationality);
                    txtSaudiIqamaIDNumber.Text = m.SaudiIqamaID;
                    ddlEMaritalStatus.SelectedValue = m.MaritalStatus.Trim();
                    ////if(Request.QueryString["OptionType"].ToString().ToLower().Trim() == "class")
                    ////string optionType = string.Empty;
                    ////optionType = Request.QueryString["OptionType"] != null ? Convert.ToString(Request.QueryString["OptionType"]).ToLower().Trim() : string.Empty;                    
                    if (optionType.ToLower().Trim() == "class")
                    {
                        ddlBranchCodeOld.Text = ClassName(strClientID.Trim(), m.ClassID.Trim());
                        ddlBranchCodeOldValue.Text = m.ClassID.Trim();
                        ////Disabled by sakthi on 27-Dec-2015 for request need pass membership number. not a contact number.
                        ////Old code
                        ////PopulateClassList(strClientID.Trim());
                        ////New code
                        PopulateClassList(MEmberORStaff);
                        details.Visible = true;
                    }
                    else
                    {
                        ddlBranchCodeOld.Text = BranchName(strClientID, m.BranchCode);
                        ddlBranchCodeOldValue.Text = m.BranchCode.Trim();
                        PopulateBranchList(strClientID);
                        details.Visible = true;
                    }
                    SetIDNumberValidation();
                }

            }

            else
            {
                lblError.Visible = true;
                lblError.Text = "Inputted 'Membership No' does not exist in your group.";

            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = "we are unableto process your request.Please try later";// ex.Message + ex.StackTrace;
            CommonClass.LogError("ChangeBranch_button1", ex.Message, ex.InnerException, ex.StackTrace, (Session[WebPublication.Session_ClientUsername] != null ? Session[WebPublication.Session_ClientUsername].ToString() : ""));
            //throw ex;
        }
    }

    protected string ClassName(string ClientID, string ClassID)
    {
        string _strClassName = "";
        ws = new ServiceDepot_DNService();

        ReqClsListRequest_DN request = new ReqClsListRequest_DN();
        ReqClsListResponse_DN response;
        //request.membershipNo = long.Parse(strClientID);
        request.contractNo = long.Parse(strClientID);
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();// long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
        try
        {

            response = ws.ReqClsList(request);
            if (response.status != "0")
            {
            }
            else
            {
                if (response.status == "0")
                {
                    foreach (ClsListDetail_DN dtl in response.detail)
                    {
                        if (Convert.ToString(dtl.classID).Trim() == ClassID.Trim())
                        {
                            _strClassName = dtl.className;
                        }
                    }
                }
            }
            return _strClassName;
        }
        catch (Exception ex)
        {
            return "No Branch";
        }
    }

    protected string BranchName(string ClientID, string BranchCode)
    {
        string _strBranchName = "";
        ws = new ServiceDepot_DNService();

        ReqBrhListRequest_DN request = new ReqBrhListRequest_DN();
        ReqBrhListResponse_DN response;
        request.membershipNo = long.Parse(strClientID);

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();

        try
        {

            response = ws.ReqBrhList(request);
            if (response.status != "0")
            {
            }
            else
            {
                if (response.status == "0")
                {
                    foreach (BrhListDetail_DN dtl in response.detail)
                    {
                        if (dtl.branchCode.Trim() == BranchCode.Trim())
                        {
                            _strBranchName = dtl.branchDesc;
                        }
                    }
                }
            }
            return _strBranchName;
        }
        catch (Exception ex)
        {
            return "No Branch";
        }
    }

    protected void ddlEIDType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtSaudiIqamaIDNumber.Text = "";
        SetIDNumberValidation();
    }

    private void SetIDNumberValidation()
    {

        switch (ddlEIDType.SelectedValue)
        {
            case "1":
                txtSaudiIqamaIDNumber.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[1][0-9]{9}";
                rfvSuadiID.Text = "Incorrect National ID";
                BindDdlENationality(false);
                break;
            case "2":
                txtSaudiIqamaIDNumber.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[2][0-9]{9}";
                rfvSuadiID.Text = "Incorrect Iqama ID";
                BindDdlENationality(false);
                break;
            case "3":
                txtSaudiIqamaIDNumber.MaxLength = 15;
                rfvSuadiID.ValidationExpression = "([A-Z]|[a-z]|[0-9]){6,15}";
                rfvSuadiID.Text = "Incorrect Passport Number";
                /////BindDdlENationality(true);
                BindDdlENationality(false);
                break;
            case "4":
                txtSaudiIqamaIDNumber.MaxLength = 10;
                rfvSuadiID.ValidationExpression = "[3-5][0-9]{9}";
                rfvSuadiID.Text = "Incorrect Entry Number";
                BindDdlENationality(false);
                break;
        }
    }
    private void setNationalityinDropDownList(string NationalityCode, DropDownList ddl)
    {
        try
        {
            ListItem li = ddl.Items.Cast<ListItem>()
                           .Where(x => x.Value.Contains(NationalityCode))
                           .LastOrDefault();
            ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(li.Text));
        }
        catch (Exception)
        {

        }
    }
    private string getNationalityForCaesarRequest(DropDownList ddl)
    {
        char[] delimiters = new char[] { '-' };
        if (!string.IsNullOrEmpty(ddl.SelectedValue))
        {
            string[] parts = ddl.SelectedValue.Split(delimiters,
                            StringSplitOptions.RemoveEmptyEntries);
            return parts[0];
        }
        else
            return string.Empty;
    }
}