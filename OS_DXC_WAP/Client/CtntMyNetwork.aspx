<%@ Page Language="C#"   MasterPageFile="ClientMaster.master"  EnableEventValidation="false" Inherits="CtntMyNetwork" Debug="true" Codebehind="CtntMyNetwork.aspx.cs" %>

<%@ MasterType VirtualPath="ClientMaster.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<asp:Content ID="Content1_1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
           <div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div> 
    
    
    
   
<asp:ImageMap  ID="ImageMap1" runat="server" Visible="false" ImageUrl="images/map.png" OnClick="ImageMap1_Click1" ForeColor="Lime"> 

       
<asp:PolygonHotSpot HotSpotMode="PostBack" NavigateUrl="~/Default.aspx"  Coordinates="77,27,103,58,90,70,74,76,57,91,34,80,25,86,20,108,7,131,26,125,34,130,40,147,56,170,57,181,62,181,64,191,70,191,75,201,68,209,68,214,77,219,77,235,87,234,94,234,102,250,107,253,105,268,120,267,129,276,149,277,143,287,164,287,202,255,204,227,191,220,192,213,182,204,188,194,198,198,202,181,213,182,246,154,256,154,264,162,268,160,268,149,281,143,304,107,267,100,199,41,168,20,141,13,141,13" AccessKey="N" AlternateText="Northern Region"  PostBackValue="N"/>


<asp:PolygonHotSpot HotSpotMode="PostBack" NavigateUrl="~/Default.aspx"  Coordinates="199,451,204,442,192,437,194,421,176,414,150,354,129,340,114,311,114,269,119,268,130,276,148,278,142,287,166,289,202,258,210,263,214,279,228,275,232,281,230,293,243,285,246,291,240,305,241,316,251,328,265,331,265,339,257,339,260,355,276,362,273,380,264,381,258,389,248,399,251,405,233,410,234,424,243,427,236,437" AccessKey="W" AlternateText="Western Region" PostBackValue="W"/>

<asp:PolygonHotSpot HotSpotMode="PostBack" NavigateUrl="~/Default.aspx"  Coordinates="294,368,258,353,260,337,269,337,267,327,248,321,240,302,246,278,234,288,233,273,213,274,207,257,202,254,204,224,194,216,185,203,197,197,203,181,214,184,250,155,261,163,266,163,271,153,273,148,318,168,337,179,341,208,355,223,369,248,370,331,358,337" AccessKey="C" AlternateText="Central Region" PostBackValue="C"/>
            

<asp:PolygonHotSpot HotSpotMode="PostBack" NavigateUrl="~/Default.aspx"  Coordinates="318,439,317,429,260,428,255,423,240,431,242,425,236,421,235,412,253,405,253,398,266,385,281,377,276,363,293,368,371,333,367,386,368,398,349,402,345,417,333,430,339,433" AccessKey="S" AlternateText="Southern Region" PostBackValue="S"/>
            

<asp:PolygonHotSpot HotSpotMode="PostBack" NavigateUrl="~/Default.aspx"  Coordinates="530,273,548,295,529,351,452,379,369,396,372,249,341,208,337,177,274,146,303,106,333,109,334,114,353,119,394,166,394,188,405,198,413,225,428,226,422,234,446,249,465,271,526,277" AccessKey="E" AlternateText="Eastern Region" PostBackValue="E"/>        


<asp:PolygonHotSpot HotSpotMode="PostBack" NavigateUrl="~/Default.aspx"  Coordinates="415,434,534,435,535,462,416,464,417,464" AccessKey="O" AlternateText="Outside KSA" PostBackValue="O"/>        

        </asp:ImageMap>
        
        
        <div id="updateProgress1"  style=" position:absolute; left: 9px; top: 121px; right:auto ">
      <asp:UpdatePanel ID="UpdatePanel2"   runat="server">
       <Triggers   >
       <asp:AsyncPostBackTrigger ControlID="ImageMap1"  />
       </Triggers> 
       <ContentTemplate>
       <asp:Button CssClass="submitButton"  ID="btntest" Visible=false Text="Check for another Provider"   runat="server" OnClick="btntest_Click" />
       <asp:Label ID="areaClicked" runat="server" Text="Provider List : " Visible=False Font-Bold="True" Font-Names="Verdana" Font-Size="Small"></asp:Label>        

<div id="TableReport"  visible=false style=" font-family:Verdana; background-color:White; background-image:url('semi-trans200.png');  height:350px; width:565px; overflow:scroll;" runat="server" >
           <img src="file:///C:\Downloads\imagemap-1291\AJAX_ImageMap\animated_loading.gif" />
       </div>
       </ContentTemplate>
        </asp:UpdatePanel>
        
        </div>  
        
    
    <asp:Label ID="Message1" Font-Names="verdana" Font-Size="Small" Width="95%" runat="server" ></asp:Label>
    
</asp:Content>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    View provider network list and their location 
</asp:Content>
