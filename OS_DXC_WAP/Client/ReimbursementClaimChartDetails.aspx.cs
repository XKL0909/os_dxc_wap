﻿using Bupa.Core.Utilities.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI.HtmlChart;
using OnlinseServicesDashBoard;
using Telerik.Web.UI;

public partial class ReimbursementClaimChartDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientID"])))
            Response.Redirect("~/default.aspx");

        DateTimeBaseUnit baseUnit = (DateTimeBaseUnit)Enum.Parse(typeof(DateTimeBaseUnit), "Days", true);
        ReimbursementClaimsLineChart.PlotArea.XAxis.BaseUnit = baseUnit;

        ReimbursementClaimsLineChart.DataSource = GetReimbursementClaimsGroupedData(GetReimbursementClaimsData());
        ReimbursementClaimsLineChart.DataBind();

        ReimbursementClaimsLineChart.PlotArea.Series[0].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Black;
        ReimbursementClaimsLineChart.PlotArea.Series[1].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Red;
        ReimbursementClaimsLineChart.PlotArea.Series[2].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Gold;
        ReimbursementClaimsLineChart.PlotArea.Series[3].Appearance.FillStyle.BackgroundColor = System.Drawing.Color.Green;

        //this.ReimbursementClaimDetailsGrid.RowFormatting += radGridView1_RowFormatting;
    }

    private DataSet GetReimbursementClaimsGroupedData(DataSet dsSource)
    {

        //Dataset for chart
        DataSet ds = new DataSet("ReimbursementDataSet");
        DataTable dt = new DataTable("ReimbursementDataTable");
        dt.Columns.Add("billed_amt", Type.GetType("System.Decimal"));
        dt.Columns.Add("rjClaims", Type.GetType("System.String"));
        dt.Columns.Add("acClaims", Type.GetType("System.String"));
        dt.Columns.Add("spClaims", Type.GetType("System.String"));
        dt.Columns.Add("crt_date", Type.GetType("System.DateTime"));

        int TotalClaims = dsSource.Tables[0].Rows.Count;

        int rejectedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Reject")).Count();
        int acceptedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Accept")).Count();
        int suspendedCount = dsSource.Tables[0].AsEnumerable().Where(x => x.Field<string>("Line_Item_Status").Equals("Suspense")).Count();

        var query = from dr in dsSource.Tables[0].AsEnumerable()
                    group dr by dr["crt_date"] into newdr
                    select new
                    {
                        crt_date = newdr.First().Field<object>("crt_date"),
                        billed_amt = newdr.Count(), //newdr.Sum(tempRec => Convert.ToDecimal(tempRec["billed_amt"])),
                        //Line_Item_Status = newdr.First().Field<object>("Line_Item_Status"),
                        rjClaims = newdr.Where(x => x.Field<string>("Line_Item_Status").Equals("Reject")).Count(),
                        acClaims = newdr.Where(x => x.Field<string>("Line_Item_Status").Equals("Accept")).Count(),
                        spClaims = newdr.Where(x => x.Field<string>("Line_Item_Status").Equals("Suspense")).Count(),
                    };

        foreach (var dtRow in /*dsSource.Tables[0].Rows*/ query)
        {
            dt.Rows.Add(dtRow.billed_amt.ToString(), dtRow.rjClaims, dtRow.acClaims, dtRow.spClaims, dtRow.crt_date);
        }

        ds.Tables.Add(dt);
        return ds;
    }

    private DataSet GetReimbursementClaimsData()
    {
        var caesarConnectionString = CoreConfiguration.Instance.GetConfigValue("Caesar Connection String");
        var contractNumber = Convert.ToString(Session["ContractNumbers"]);// Convert.ToString(Session["ClientID"]); //CoreConfiguration.Instance.GetConfigValue("FilterByContract");


        DataSet ds = DataHelper.GetReimbursementClaimsData(caesarConnectionString, DateTime.Now.AddDays(-10), DateTime.Now, contractNumber);

        ReimbursementClaimDetailsGrid.DataSource = ds;
        ReimbursementClaimDetailsGrid.DataBind();


        return ds;
    }

    public class ReimbursementClaim
    {
        public ReimbursementClaim(string name, double pieShare, bool isExploded, string partColor)
        {
            Name = name;
            MarketShare = pieShare;
            IsExploded = isExploded;
            PartColor = partColor;
        }
        public string Name { get; set; }
        public string PartColor { get; set; }
        public double MarketShare { get; set; }
        public bool IsExploded { get; set; }
    }

    protected void ReimbursementClaimDetailsGrid_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
    {
        e.Column.CurrentFilterFunction = GridKnownFunction.Contains;
        if (e.Column.UniqueName == "REMARKS")
            e.Column.FilterControlWidth = Unit.Pixel(10);
        else
            e.Column.FilterControlWidth = Unit.Pixel(50);

        if (e.Column.UniqueName.ToLower() == "paid_amount")
        {
            //e.Column.Visible = t;
            e.Column.HeaderText = "Paid amount";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "CONT_NO")
        {
            e.Column.Visible = false;
            e.Column.HeaderText = "Contract #";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "PROV_CODE")
        {
            e.Column.HeaderText = "Provider Code";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "RPROV_NAME")
        {
            e.Column.HeaderText = "Provider name";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "REGION")
        {
            e.Column.HeaderText = "Region";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "SCHEME_NAME")
        {
            e.Column.HeaderText = "Scheme";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "MBRSHP_NO")
        {
            e.Column.HeaderText = "Membership #";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "MBR_NAME")
        {
            e.Column.HeaderText = "Member name";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "MBR_MOBILE_NO")
        {
            e.Column.HeaderText = "Member mobile #";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "SERV_TYPE")
        {
            e.Column.HeaderText = "Service Type";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "CLAIM_TYPE")
        {
            e.Column.HeaderText = "Claim Type";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "BEN_TYPE")
        {
            e.Column.HeaderText = "Ben type";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "LINE_ICD_DESC")
        {
            e.Column.HeaderText = "ICD Description";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "BILLED_AMT")
        {
            e.Column.HeaderText = "Billed amount";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "LINE_ITEM_STATUS")
        {
            e.Column.HeaderText = "Line item status";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "VOU_NO")
        {
            e.Column.HeaderText = "Voucher #";
            e.Column.AutoPostBackOnFilter = true;
        }//
        else if (e.Column.UniqueName == "SUBMIT_BY")
        {
            e.Column.HeaderText = "Submit by";
            e.Column.AutoPostBackOnFilter = true;
        }//SUBMIT_BY
        else if (e.Column.UniqueName == "REJ_REASON")
        {
            e.Column.HeaderText = "Rejection reason";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "PAY_DATE")
        {
            e.Column.HeaderText = "Payment date";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "INCUR_DATE_FROM")
        {
            e.Column.HeaderText = "Treatment date";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "CRT_DATE")
        {
            e.Column.HeaderText = "Create date";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "STAFF_NO")//m.STAFF_NO,m.BRANCH_CODE,m.MBR_RELATION
        {
            e.Column.HeaderText = "Staff #";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "BRANCH_NAME")
        {
            e.Column.HeaderText = "Branch";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "MBR_RELATION")
        {
            e.Column.HeaderText = "Relation";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "VOU_RCV_DATE") //h.VOU_RCV_DATE,h.ADJ_AMT,h.REJ_AMT
        {
            e.Column.HeaderText = "Submission date";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "ADJ_AMT")
        {
            e.Column.HeaderText = "Billed amount";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "REJ_AMT")
        {
            e.Column.HeaderText = "Shortfall amount";
            e.Column.AutoPostBackOnFilter = true;
        }
        else if (e.Column.UniqueName == "REMARKS")
        {
            e.Column.HeaderText = "KPI";
            e.Column.AutoPostBackOnFilter = true;
            e.Column.FilterControlWidth = Unit.Pixel(5);
            e.Column.ItemStyle.Width = Unit.Pixel(5);
        }

    }
    protected void ReimbursementClaimDetailsGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataItem = (GridDataItem)e.Item;
            int numberOfDaysKPI = 0;
            if (!dataItem["remarks"].Text.Equals("Not paid"))
            {
                int.TryParse(dataItem["remarks"].Text, out numberOfDaysKPI);
                if (numberOfDaysKPI >= 0)// == "Green")
                {
                    if (numberOfDaysKPI <= 5)
                    {
                        dataItem.BackColor = System.Drawing.Color.PaleGreen;
                        dataItem["remarks"].BackColor = System.Drawing.Color.PaleGreen;
                        dataItem["remarks"].ForeColor = System.Drawing.Color.Black;
                    }
                    else
                    {
                        dataItem.BackColor = System.Drawing.Color.IndianRed;
                        dataItem["remarks"].BackColor = System.Drawing.Color.IndianRed;
                        dataItem["remarks"].ForeColor = System.Drawing.Color.Black;
                    }
                }
            }
            else
            {
                dataItem.BackColor = System.Drawing.Color.LightGoldenrodYellow;
                dataItem["remarks"].BackColor = System.Drawing.Color.LightGoldenrodYellow;
                dataItem["remarks"].ForeColor = System.Drawing.Color.Black;
            }
        }
    }


    protected void btnExcel_Click(object sender, System.EventArgs e)
    {
        ReimbursementClaimDetailsGrid.ExportSettings.ExportOnlyData = true;
        ReimbursementClaimDetailsGrid.ExportSettings.IgnorePaging = true;
        ReimbursementClaimDetailsGrid.ExportSettings.OpenInNewWindow = true;
        ReimbursementClaimDetailsGrid.MasterTableView.ExportToExcel();
    }

    private void AddExpression()
    {
        GridSortExpression expression = new GridSortExpression();
        expression.FieldName = DropDownList3.SelectedItem.Value;
        expression.SetSortOrder(DropDownList2.SelectedItem.Value);
        this.ReimbursementClaimDetailsGrid.MasterTableView.SortExpressions.AddSortExpression(expression);
    }

    protected void btnAddExpression_Click(object sender, System.EventArgs e)
    {
        AddExpression();
        this.ReimbursementClaimDetailsGrid.MasterTableView.Rebind();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        this.ReimbursementClaimDetailsGrid.MasterTableView.AllowMultiColumnSorting = bool.Parse(DropDownList1.SelectedItem.Value);
        this.ReimbursementClaimDetailsGrid.Rebind();
    }

    protected void Clear_Click(object sender, System.EventArgs e)
    {
        this.ReimbursementClaimDetailsGrid.MasterTableView.SortExpressions.Clear();
        this.ReimbursementClaimDetailsGrid.MasterTableView.Rebind();
    }

    protected void ReimbursementClaimDetailsGrid_ItemCreated(object sender, GridItemEventArgs e)
    {
        //if (e.Item is GridFilteringItem)
        //{
        //    GridFilteringItem filteringItem = e.Item as GridFilteringItem;
        //    filteringItem.Width = Unit.Pixel(30);
        //    //set dimensions for the filter textbox  
        //    //TextBox box = filteringItem["ContactName"].Controls[0] as TextBox;
        //    //box.Width = Unit.Pixel(30);
        //}
    }
}
