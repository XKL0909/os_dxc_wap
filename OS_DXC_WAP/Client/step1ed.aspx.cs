﻿using Caesar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;

public partial class step1ed : System.Web.UI.Page
{
    private static string contractNumber;
    private long reqDetailID;

    private static AddMemberRequestDetailService _memberService;

    public step1ed(): this(new AddMemberRequestDetailService())
    {
      
    }


    public step1ed(AddMemberRequestDetailService memberService)
    {
        _memberService = memberService;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
            contractNumber = Convert.ToString(Session["ClientID"]);
            Init();
            if (HttpContext.Current.Session["AddMember"] != null)
            {
                BindExistingValue();
            }
        //}
    }

    public void Init()
    {
        drplistSponsor.DataSource = CaesarManager.GetSponsor(contractNumber);
        drplistSponsor.DataValueField = "sponsorID";
        drplistSponsor.DataTextField = "sponsorID";
        drplistSponsor.DataBind();
        drplistSponsor.Items.Insert(0, new ListItem("Select Sponsor", ""));
    }

    private void BindExistingValue()
    {
        // var member = _memberService.FindByID(reqDetailID);

        var member = (AddMemberRequestDetail)HttpContext.Current.Session["AddMember"];
        drplistSponsor.SelectedValue = member.Sponsor;
        txtID.Text = member.IdNumber;
        txtDepHYearOfBirth.Text = member.DepHijriDOBYear.ToString();

        txtHYearOfBirth.Text = member.HijriDOBYear.ToString();

        if (member.HijriDOBYear != null)
        {
            reqValtxtHYearOfBirth.Enabled = true;
        }
        else
        {
            reqValtxtHYearOfBirth.Enabled = false;
        }
    }
    [WebMethod(EnableSession = true)]
    private static int DeleteFiles(int id)
    {
        var FileUploadService = new AddMemberFileuploadService();
        var result = FileUploadService.Remove(id);
        return result;
    }


    [WebMethod(EnableSession = true)]
    public static int InitSession()
    {
        HttpContext.Current.Session["AddMember"] = null;
        HttpContext.Current.Session["ReqDetailID"] = null;
        HttpContext.Current.Session["Members"] = null;
        return 1;

    }



    [WebMethod(EnableSession = true)]
    public static long AddMemberDetails(AddMemberRequestDetail member)
    {
        long result = 0;
        HttpContext.Current.Session["AddMember"] = null;
        if (HttpContext.Current.Session["AddMember"] == null)
        {
            member.Status = 1;
            member.IdType = GeneralHelper.GetTitleByRelation(member.IdNumber.Trim());
            member.ContNumber = contractNumber;
            HttpContext.Current.Session.Add("AddMember", member);
        }
        return result;
    }
}