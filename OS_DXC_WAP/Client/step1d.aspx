﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="step1d" Codebehind="step1d.aspx.cs" %>

<form id="form1" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>
    <div class="card bb">
        <div class="header bb">
            <h2>Add </h2>
        </div>
        <div class="body bb">
            <%-- <div class="row clearfix">
                  <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                          <label>Do you have exisiting membership Id?</label>
                            
                            <input type="radio" name="membership" value="Y" id="withMembership" checked="checked" class="with-gap" />
                                <label for="withMembership">Yes</label>
                                <input type="radio" value="N" name="membership" id="withOutMembership" class="with-gap" />
                                <label for="withOutMembership" class="m-l-20">No</label>
                        </div>
                    </div>
                </div>
              </div>--%>
            <div class="row clearfix bb" runat="server" id="MemberShipDiv">
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                             <asp:hiddenfield runat="server" id="hdnRequestID" />
                            <label for="lblMemberShipQ" class="control-label bb">Is the Main Member Already Exist? / هل الموظف عميل لدى بوبا ؟</label>
                            <asp:dropdownlist id="SelMemberShipQ" cssclass="form-control bb" runat="server">
                            <asp:ListItem Selected="True" Value=""> Select </asp:ListItem>
                            <asp:ListItem Value="Yes"> Yes </asp:ListItem>
                            <asp:ListItem Value="No"> No </asp:ListItem>
                            </asp:dropdownlist>
                            <asp:requiredfieldvalidator validationgroup="step1" id="RequiredfieldSelSelMemberShipQ" controltovalidate="SelMemberShipQ" runat="server" errormessage="Field is mandatory/الحقل إجباري" display="Dynamic" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix margin-top-20 bb">
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb" id="MainMemberMembershipDiv">
                           
                            <label id="lblMemberOrSponsor" class="control-label bb">Main Member Membership Number / رقم العضوية</label>&nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="please fill the main member membership number if the main member is already added to add Family<br>نرجو ادخال رقم عضوية الموظف الذي تتبع له العائلة المراد اضافتها اذا كان مضاف" />
                            <asp:textbox id="txtBupaMembershipNumber" cssclass="form-control bb" runat="server"></asp:textbox>
                            <asp:regularexpressionvalidator display="Dynamic" runat="server" validationgroup="step1" errormessage="Numbers Only 7 to 10 Digits/ارقام فقط من 7 الى 10 ارقام" controltovalidate="txtBupaMembershipNumber"
                                validationexpression="^[0-9]{7,10}$"></asp:regularexpressionvalidator>
                            <asp:requiredfieldvalidator display="Dynamic" validationgroup="step1" id="reqValdtxtBupaMembershipNumber" controltovalidate="txtBupaMembershipNumber" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
						                    <div class="form-group bb" id="MainMemberIdDiv">
                        <div class="form-line bb" >
                            <label class="control-label bb">Main Employee ID / رقم الهوية للموظف</label>&nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="please fill the main member ID number if the main member still is not added to add Family<br>نرجو ادخال رقم الهوية الوطنية او الاقامة او تأشيرة الدخول للموظف الذي لم يضاف بعد لاضافة عائلته" />
                            <asp:dropdownlist cssclass="form-control bb" runat="server" id="drpMAinEMployee" maxlength="10"></asp:dropdownlist>
                            <asp:requiredfieldvalidator display="Dynamic" validationgroup="step1" id="reqValdrpMAinEMployee" controltovalidate="drpMAinEMployee" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix bb">
                <div class="col-md-6 bb" id="sectionNewBorn">
                    <div class="form-group bb">
                        <input type="checkbox" value="true" name="chkNewBorn" id="chkNewBorn" />
                        <label for="lblNewBorn">New Born Baby Without ID / مولود جديد بدون رقم هوية</label>&nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="please click in the box  to add new born baby<br>نرجو النقر على المربع لاضافة مولود جديد" />
                    </div>
                </div>
            </div>
            <div class="row clearfix bb">
                <div class="col-md-6 bb" id="DependantIDDiv" >
                    <div class="form-group bb" >
                        <div class="form-line bb" >
                            <label class="control-label bb"  >Dependant ID / رقم هوية التابع</label>&nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="enter dependent ID<br>ادخل رقم الهوية الوطنية/الاقامة / تأشيرة الدخول للتابع/التابعه" />
                            <asp:textbox cssclass="form-control bb" runat="server" id="txtID" maxlength="10"></asp:textbox>
                            <asp:regularexpressionvalidator runat="server" errormessage="Numbers Only 10 Digits/10 ارقام فقط" controltovalidate="txtID"
                                validationexpression="^\d{10}$" id="regularexpressionTXTID" validationgroup="step1"></asp:regularexpressionvalidator>
                            <asp:requiredfieldvalidator validationgroup="step1" id="reqValtxtID" controltovalidate="txtID" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>

                </div>

                <div class="col-md-6 bb" id="sectionHijari">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">Hijri Year of Birth / السنة الهجرية للولادة</label>&nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="enter Hijri year only for dependent<br>ادخل السنة الهجرية فقط للتابع/التابعه" />
                            <asp:textbox cssclass="form-control bb" runat="server" id="txtHYearOfBirth" maxlength="4"></asp:textbox>
                            <asp:regularexpressionvalidator display="Dynamic" runat="server" errormessage="Four Numbers Only/ أربعة ارقام فقط" controltovalidate="txtHYearOfBirth"
                                validationexpression="^\d{4}$" id="regularexpressionHijriYear" validationgroup="step1"></asp:regularexpressionvalidator>
                            <asp:requiredfieldvalidator display="Dynamic" validationgroup="step1" id="reqValtxtHYearOfBirth" controltovalidate="txtHYearOfBirth" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
            </div>

            <div class="actions clearfix pull-right bb">

                <asp:button id="btnNext" validationgroup="step1" runat="server" text="Next / التالي" class="btn btn-primary nextBtn btn-sm pull-right bb" />
            </div>

        </div>
    </div>

</form>
<script src="js/step1d.js"></script>
