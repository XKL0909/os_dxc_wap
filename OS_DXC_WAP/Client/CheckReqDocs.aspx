﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Client_CheckReqDocs" Codebehind="CheckReqDocs.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script type='text/JavaScript' src='scw.js'></script>
    <style type="text/css">
        #Content td
        {
            font-family: arial;
            font-size: 12px;
            color: #606060;
        }
        .th
        {
            font-weight: bold;
            color: #666;
            font-size: 12px;
        }
        
        </style>
    <style>
        @media print
        {
            .noPrint
            {
                display: none;
            }
            .Hidden
            {
                display: inline;
            }
        }
        
        .Hidden
        {
            display: none;
        }
    </style>
    <style type="text/css">
        body
        {
            font-family: Arial,Helvetica,Sans-serif;
            font-size: 80%;
        }
        caption
        {
            padding-bottom: 5px;
            font-weight: bold;
        }
        thead th, tfoot td
        {
            background: #ddd;
        }
        tr.ruled
        {
            background: #9cf;
        }
        table
        {
            border-collapse: collapse;
        }
        th, td
        {
            border-collapse: collapse;
            direction: ltr;
        }
        #mytable tr.ruled
        {
            background: #333;
            color: #ccc;
        }
        .style1
        {
            height: 34px;
        }
    </style>
    <script type="text/javascript" src="../includes/tableruler.js"></script>
    <script type="text/javascript">
        window.onload = function () { tableruler(); }
    </script>
      <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
</head>
<body topmargin="0px">
    <table width="100%">
        <tr valign="baseline">
            <td>
                <asp:Label ID="lblAdd_Employee" runat="server" Text="Submit Required Documents" Font-Size="Large"
                    Font-Names="Arial"></asp:Label>
            </td>
            <td style="text-align: right">
                <img src="Logo.gif" width="168" height="50" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr size="1px" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr align="right">
            <td align="left">
                <a href="<%=Page.ResolveUrl("~/DefaultClient3-1.aspx") %>" style="font-size:small">Back</a>
                <label id="Label1">
                </label>
            </td>
            <td>
                <a href="#" onclick="window.print()">
                    <img class="noPrint" border="0" src="images/printer.gif" alt="Print" style="height: 33px;
                        text-align: right;">
                </a>
            </td>
        </tr>
    </table>

    <form id="form1" runat="server">
   
    <div class="noPrint">
        <table>
            <tr>
                <td style="height: 22px" valign="top">
                    Search Type</td>
                <td style="height: 22px" valign="top">
                    <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlSearchtype" runat="server" 
                        onselectedindexchanged="ddlSearchtype_SelectedIndexChanged" AutoPostBack="True" 
                        >
                        <asp:ListItem Value="0">--- Select search Type ---</asp:ListItem>
                        <asp:ListItem Value="1">By Reference Number</asp:ListItem>
                        <asp:ListItem Value="2">By Submitted Date</asp:ListItem>
                        <asp:ListItem Value="3">Both</asp:ListItem>
                    </asp:DropDownList></div>
                </td>
                <td style="height: 22px" valign="top">
                    &nbsp;</td>
            </tr>
            <tr runat="server" id="refnum" visible=false>
                <td style="height: 22px" valign="top">
                    <asp:Label ID="lbl1" Text="Reference Number" runat="server"></asp:Label>
                </td>
                <td style="height: 22px" valign="top">
                    <asp:TextBox CssClass="textbox" ID="txtSearchRef" runat="server" ValidationGroup="RefGroup"></asp:TextBox>
                </td>
                <td style="height: 22px" valign="top">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSearchRef"
                        ErrorMessage="RequiredFieldValidator" Enabled=false >Please fill in the Reference No. </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr  runat="server" id="submitdate" visible=false>
                <td valign="top">
                    <asp:Label ID="Label2" Text="Date Submitted" runat="server"></asp:Label>
                </td>
                <td valign="top">
                    <asp:TextBox CssClass="textbox" ID="txtSearchDate" runat="server" Width="144px" onclick="scwShow(this,event);"
                        Style="cursor: default;" ValidationGroup="1"></asp:TextBox><br />
                </td>
                <td valign="top">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtSearchDate"
                        ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" 
                        Display="Dynamic" Font-Size="Small" Enabled=false Text="Please fill in the Date" Width="176px"></asp:RequiredFieldValidator>
                </td>
            </tr>
            
            <tr>
                <td valign="top">
                    &nbsp;</td>
                <td valign="top">
                    <asp:Button CssClass="submitButton" ID="btnSearchDate" Visible=false runat="server" Text="Submit" Width="151px" 
                        onclick="btnSearchDate_Click" />
                </td>
                <td valign="top">
                    &nbsp;</td>
            </tr>
            
        </table>
        <br />
    </div>
    <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label>
 
    <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
    <asp:TextBox CssClass="textbox" ID="txtBox_output" Visible="false" runat="server"></asp:TextBox>
    <div id="dvResult" runat="server" visible="false">
    <table style="width:100%;border:0">
        <tr>
            <td><h3>Please submit the supportive documents</h3></td>
        </tr>

        <tr>
            <td></td>
        </tr>

        <tr>
            <td class="style1">
               
              </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="lblUpload" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
    <asp:GridView ID="gvResult" runat="server" CaptionAlign="Left" EnableModelValidation="True" 
                    Width="50%" onrowcancelingedit="gvResult_RowCancelingEdit" 
                    onrowdatabound="gvResult_RowDataBound" onrowediting="gvResult_RowEditing" 
                    onrowupdating="gvResult_RowUpdating" CellPadding="4" ForeColor="Black" 
                    GridLines="Horizontal" BackColor="White" BorderColor="#CCCCCC" 
                    BorderStyle="None" BorderWidth="1px">
        <Columns>
            <asp:BoundField DataField="seq" HeaderText="Seq" ReadOnly="True" 
                SortExpression="seq" />
             <asp:BoundField DataField="Ref No" HeaderText="Ref No" ReadOnly="True" 
                SortExpression="Ref No" />
                 <asp:BoundField DataField="Contract No" HeaderText="Contract No" ReadOnly="True" 
                SortExpression="Contract No" />
            <asp:BoundField DataField="Member Name" HeaderText="Member Name" 
                ReadOnly="True" SortExpression="Member Name" />
            <asp:BoundField DataField="Saudi ID/Iqama No." HeaderText="Saudi ID/Iqama No." 
                ReadOnly="True" SortExpression="Saudi ID/Iqama No." />
            <asp:TemplateField HeaderText="Support Documents"> 
                <ItemTemplate> 
                  <asp:Image  ImageUrl="~/Icons/missing-icon.png" Width="32px" Height="32px" runat="server" ID="image" /> 
                </ItemTemplate> 
               <EditItemTemplate> 
                   <asp:FileUpload ID="FileUpload1" runat="server" /> 
               </EditItemTemplate> 
          </asp:TemplateField> 
           <asp:commandField showEditButton="true" showCancelButton="true" />

        </Columns>
        
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        
        <HeaderStyle 
            HorizontalAlign="Justify" BackColor="#333333" Font-Bold="True" 
            ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
            </td>
        </tr>

        <tr>
            <td>
                
               
                <table style="width:50%; border:0px;">
                    <tr>
                        <td></td>
                        <td align="right">                    <asp:Button CssClass="submitButton" ID="btnSubmitRequest" 
                                runat="server" Text="Submit" Width="151px" 
                                onclick="btnSubmitRequest_Click" />
                </td>
                    </tr>
                </table></td>
        </tr>

    </table>
    </div>
        	            
          <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">

                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>

                    </div>
                </div>
				            <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>

                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                    <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                        Do you want to extend?</strong>
                                </td>
                                <td style="text-align: right">
                                    <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                        هل تريد تمديد الوقت؟ &nbsp;</strong>
                                </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                       </div>
                    </asp:Panel>

                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
    </form>
</body>
</html>