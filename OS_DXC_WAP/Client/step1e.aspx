﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="step1e" Codebehind="step1e.aspx.cs" %>
<form id="form1" runat="server">
    <asp:hiddenfield runat="server" id="hdnStepReqDetailId" />
    <div class="card bb">
        <div class="header bb">
            <h2>Add </h2>
        </div>
        <div class="body bb">

            <div class="row clearfix margin-top-20 bb">
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label id="lblMemberOrSponsor" class="control-label bb">Sponsor / رقم الكفيل</label> &nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="Choose the Sponsor Number Based on National Information Center<br>الرجاء اختيار رقم الكفيل التابع له الموظف وفقا لبينات مركز المعلومات الوطنية " />
                            <asp:dropdownlist cssclass="form-control bb" id="drplistSponsor" runat="server" ></asp:dropdownlist>
                            <asp:requiredfieldvalidator display="Dynamic" validationgroup="step1" id="reqValdrplistSponsor" controltovalidate="drplistSponsor" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">ID / رقم الهوية</label>&nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="National ID for Saudi / Iqama or Border Entry for expatriate <br> الهوية الوطنية للسعوديين / الإقامة أو تأشيرة الدخول للوافدين" />
                            <asp:textbox cssclass="form-control bb" runat="server" id="txtID" maxlength="10"></asp:textbox>
                            <asp:regularexpressionvalidator validationgroup="step1" id="RegExp1" runat="server"
                                errormessage="Length must be ten characters/ عشرة ارقام فقط"
                                controltovalidate="txtID"
                                setfocusonerror="true"
                                display="Dynamic"
                                validationexpression="^\d{10}$" />
                            <asp:requiredfieldvalidator display="Dynamic" validationgroup="step1" id="reqValtxtID" controltovalidate="txtID" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix bb" runat="server" id="sectionHijari">
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">Hijri Year of Birth / السنة الهجرية للولادة</label> &nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="enter Hijri year only<br>ادخل السنة الهجرية فقط" />
                            <asp:textbox cssclass="form-control bb" runat="server" id="txtHYearOfBirth" maxlength="4"></asp:textbox>
                            <asp:regularexpressionvalidator display="Dynamic" runat="server" errormessage="four Numbers Only/ اربعة ارقام فقط" controltovalidate="txtHYearOfBirth"
                                validationexpression="^\d{4}$"></asp:regularexpressionvalidator>
                            <asp:requiredfieldvalidator validationgroup="step1" display="Dynamic" id="reqValtxtHYearOfBirth" controltovalidate="txtHYearOfBirth" runat="server" errormessage="Field is mandatory/الحقل إجباري" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix bb" runat="server" id="FamilySctionDiv">
                <div class="col-md-12 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label for="lblAddFamily" class="control-label bb">Do you want to add the Current Employee family? / هل تود اضافة التابعين لهاذا الموظف ؟</label>&nbsp;	&nbsp;<img src="Images/2.png"  data-toggle="tooltip" data-placement="bottom" data-html="true"  title="please choose yes if you want to add the employee with Family/choose no if you want to add employee only<br>نرجو اختيار نعم اذا كنت تريد اضافة الموظف مع عائلته/ و اختيار لا اذا كنت تريد اضافة موظف فقط بدون عائلته." />
                            <asp:dropdownlist id="SelAddFamily" cssclass="form-control bb" runat="server">
                            <asp:ListItem Selected="True" Value=""> Select </asp:ListItem>
                            <asp:ListItem Value="Yes"> Yes </asp:ListItem>
                            <asp:ListItem Value="No"> No </asp:ListItem>
                            </asp:dropdownlist>
                            <asp:requiredfieldvalidator validationgroup="step1" id="RequiredfieldSelAddFamily" controltovalidate="SelAddFamily" runat="server" errormessage="Field is mandatory/الحقل إجباري" display="Dynamic" setfocusonerror="true"></asp:requiredfieldvalidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix bb" runat="server" id="sectionDepHijari">
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">Dependent Hijri Year of Birth / السنة الهجرية لولادة التابع</label>
                            <asp:textbox cssclass="form-control bb" runat="server" id="txtDepHYearOfBirth" maxlength="4"></asp:textbox>
                            <asp:regularexpressionvalidator validationgroup="step1" id="RegularExpressionValidator2" runat="server"
                                errormessage="Length must be 4 characters/4 ارقام فقط"
                                controltovalidate="txtDepHYearOfBirth"
                                setfocusonerror="true"
                                display="Dynamic"
                                validationexpression="^\d{4}$" />
                            <asp:requiredfieldvalidator validationgroup="step1" id="reqValtxtDepHYearOfBirth" controltovalidate="txtDepHYearOfBirth" runat="server" errormessage="Field is mandatory/الحقل إجباري" display="Dynamic" setfocusonerror="true"></asp:requiredfieldvalidator>
                            <input type="checkbox" name="chkAddFamily" id="chkAddFamily" style="display: none" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix bb" runat="server" id="DivDepIqama">
                <div class="col-md-6 bb">
                    <div class="form-group bb">
                        <div class="form-line bb">
                            <label class="control-label bb">Dependant Iqama Number / رقم اقامة التابع</label>
                            <asp:textbox cssclass="form-control bb" runat="server" id="DepIqamaNo" maxlength="10"></asp:textbox>
                            <asp:regularexpressionvalidator validationgroup="step1" id="DepIqamaNoRExV" runat="server"
                                errormessage="Length must be 10 characters/10 ارقام فقط"
                                controltovalidate="DepIqamaNo"
                                setfocusonerror="true"
                                display="Dynamic"
                                validationexpression="^\d{10}$" />
                            <asp:requiredfieldvalidator validationgroup="step1" id="DepIqamaNoRfv" controltovalidate="DepIqamaNo" runat="server" errormessage="Field is mandatory/الحقل إجباري" display="Dynamic" setfocusonerror="true"></asp:requiredfieldvalidator>
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="actions clearfix pull-right bb">
			<!-- data-toggle="tooltip" data-placement="bottom" data-html="true"  title="please click on next to move to next  page<br>يرجى الضغط على next للإنتقال للصفحة التالية" -->
                <asp:button id="btnNext" validationgroup="step1" runat="server" text="Next / التالي" class="btn btn-primary nextBtn btn-sm pull-right"  />
            </div>

        </div>
    </div>
</form>
<script src="js/step1e.js"></script>
<script>
</script>