﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Text;
using System.Web.Configuration;

public partial class excel : System.Web.UI.Page
{
    string FileToConvert;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);

            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
        }
        catch
        {

        }


        FileToConvert = Server.MapPath(".") + "\\temp.xls";
    }

    protected void convert_Click(object sender, EventArgs e)
    {
        if (fu.HasFile)
        {
            CheckMimeTypes validExcelfile = new CheckMimeTypes();
            if (validExcelfile.ValidateExcelFile(fu.PostedFile))
            {

                fu.SaveAs(FileToConvert);//temporarily saves the file to work with (will be cleaned up two lines down)
                ConvertFile();
                File.Delete(FileToConvert);//cleans up the temporary file that was stored
            }
            else
            {
                showArray.Text = "Invalid file";
            }
        }
        else
            showArray.Text = "No File Supplied";


    }
    protected void ConvertFile()
    {
        string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileToConvert + ";Extended Properties=Excel 8.0;";
        try
        {
            OleDbConnection connection = new OleDbConnection(connectionString);
            connection.Open();


            //this next line assumes that the file is in default Excel format with Sheet1 as the first sheet name, adjust accordingly
            OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connection);
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            adapter.Fill(ds);//now you have your dataset ds now filled with the data and ready for manipulation

            //the next 2 lines do exactly the same thing, just shows two ways to get the same data
            dt = ds.Tables[0];
            adapter.Fill(dt);//overwrites the previous declaration with the same information
            //now you have your datatable filled and ready for manipulation
            connection.Close();

            int number_of_columns = dt.Columns.Count;
            int number_of_rows = ds.Tables[0].Rows.Count / 2;
            string[,] sheetEntries = new string[number_of_rows, number_of_columns];
            string[] columnNames = new string[number_of_columns];

            //gets column names
            for (int j = 0; j < number_of_columns; j++)
                columnNames[j] = dt.Columns[j].ToString();



            OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request = new OS_DXC_WAP.CaesarWS.SubTxnRequest_DN();

            //request.TransactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));
            request.TransactionID = WebPublication.GenerateTransactionID();
            
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.detail = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[number_of_rows];
            request.BatchIndicator = "Y";

            //gets excel spreadsheet information
            for (int i = 0; i < number_of_rows; i++)
            {




                for (int j = 0; j < number_of_columns; j++)
                    sheetEntries[i, j] = dt.Rows[i].ItemArray.GetValue(j).ToString();

                // do the processing depending on the option of the file


                string options;
                options = "ChangeClass";

                switch (options)
                {
                    case "ChangeClass":
                        // function to validate the pattern of columns
                        request.TransactionType = "CLASS CHANGE";
                        request.detail[i] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                        request.detail[i].sql_type = "SUB_TXN_REC";
                        request.detail[i].ContractNo = "10063400";// strClient;
                        request.detail[i].MbrName = sheetEntries[i, 2];
                        //request.memberName = new string[1];
                        //request.memberName[0] = sheetEntries[i, 2];
                        request.detail[i].MembershipNo = "3208723";// sheetEntries[i, 1];
                        request.detail[i].Reason = "CLA001";// sheetEntries[i, 6];
                        request.detail[i].EffectiveDate = Convert.ToDateTime(sheetEntries[i, 5]); //, "dd/MM/yyyy", null);
                        request.detail[i].ExistingClass = "1";// sheetEntries[i, 3];
                        request.detail[i].NewClass = "1";//sheetEntries[i, 4];//
                        //request.detail[i].StaffNo = "";

                        break;








                    case "AddEmployee":

                        request.detail[i] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                        request.detail[i].ContractNo = "10063400"; //  inputContractNo.Value;

                        //incase its employee
                        request.detail[i].BranchCode = "10063400"; //ddlEBranchCode.SelectedValue; // inputBranchCode.Value;

                        request.detail[i].MbrName = sheetEntries[i, 4];//strMemberName;// txtEMemberName.Text; // inputMbrName.Value;
                        request.detail[i].DOB = DateTime.ParseExact(sheetEntries[i, 4], "dd-MM-yyyy", null);

                        request.detail[i].ClassID = sheetEntries[i, 4];// strLevelOfCover;// "1";// ddlELevelOfCover.SelectedValue; // inputClassID.Value;

                        //incase of Member it should be empty
                        ////////if (DependentFlag == true)
                        request.detail[i].MainMemberNo = sheetEntries[i, 4];// txtMainMemberNo.Text; // ""; // inputMainMemberNo.Value;
                        ////////else
                        ////////    request.detail[i].MainMemberNo = ""; // inputMainMemberNo.Value;


                        request.detail[i].DeptCode = sheetEntries[i, 4];// strDepartmentCode;// txtEDepartmentCode.Text; // inputDeptCode.Value;
                        request.detail[i].IgamaID = sheetEntries[i, 4];// strSaudiID;// txtESaudiID.Text; // inputIgamaID.Value;
                        request.detail[i].SponsorID = sheetEntries[i, 4];// strSponsorId;// txtESponsorId.Text; // inputSponsorID.Value;
                        request.detail[i].StaffNo = sheetEntries[i, 4];// strEmployeeNo;// txtEEmployeeNo.Text; // inputStaffNo.Value;
                        request.detail[i].Nationality = sheetEntries[i, 4];// strNationality;// ddlENationality.SelectedValue; // inputNationality.Value;
                        request.detail[i].Gender = sheetEntries[i, 4];// strGender;// ddlEGender.SelectedValue; // inputGender.Value;
                        request.detail[i].RequestStartDate = DateTime.ParseExact(sheetEntries[i, 4], "dd-MM-yyyy", null);

                        //incase of new employee it would be system date
                        request.detail[i].EffectiveDate = DateTime.ParseExact(sheetEntries[i, 4], "dd-MM-yyyy", null);

                        request.detail[i].JoinDate = DateTime.ParseExact(sheetEntries[i, 4], "dd-MM-yyyy", null);
                        request.detail[i].ArrivalDate = DateTime.ParseExact(sheetEntries[i, 4], "dd-MM-yyyy", null);

                        request.detail[i].PreviousMembershipIndicator = "N"; // inputPreviousMembershipIndicator.Value;
                        request.detail[i].PreviousMembershipNo = ""; // inputPreviousMembershipNo.Value;

                        if (sheetEntries[i, 4] != "0")
                            request.detail[i].Reason = sheetEntries[i, 4];//;// ddlEJoinReason.SelectedValue; // inputReason.Value;

                        request.detail[i].Title = sheetEntries[i, 4];// strTitle;// ddlETitle.SelectedValue; // inputTitle.Value;

                        //incase of employee it would be 'E' else realtion for the dep
                        request.detail[i].MemberType = sheetEntries[i, 4];// strBranchCode; // "E";// inputMemberType.Value;

                        request.detail[i].MBR_MOBILE_NO = sheetEntries[i, 4];//strMobileNo;// txtEMobileNo.Text; // inputMBR_MOBILE_NO.Value;
                        request.detail[i].SMS_PREF_LANG = "N";// inputSMS_PREF_NO.Value;
                        request.detail[i].sql_type = "SUB_TXN_REC";


                        break;

                    case "ChangeBranch":
                        break;

                    case "CardReplace":
                        break;
                    case "DeleteEmployee":
                        break;

                    default:
                        break;
                }



            }

            CallService(request);

            //now the array is filled with data and ready for manipulation

            /////////////////////////////////////////////////////////////////////////////////////////////////////
            // unneccessary, but it shows that the stuff was pushed correctly into the array //
            /////////////////////////////////////////////////////////////////////////////////////////////////////
            showArray.Text = "<h2>Data Displayed From Array</h2><table><tr>";
            foreach (string s in columnNames)
                showArray.Text += "<th>" + s + "</th>";
            showArray.Text += "</tr>";

            for (int i = 0; i < number_of_rows; i++)
            {
                showArray.Text += "<tr>";
                for (int j = 0; j < number_of_columns; j++)
                    showArray.Text += "<td>" + sheetEntries[i, j] + "</td>";
                showArray.Text += "</tr>";
            }
            showArray.Text += "</table>";

        }
        catch (Exception ex)
        { showArray.Text = "<h2>[error]</h2><b>details:</b><br />" + ex.ToString(); }
    }



    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    public void CallService(OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //OS_DXC_WAP.CaesarWS.SubTxnResponse_DN response;
        OS_DXC_WAP.CaesarWS.SubTxnResponse_DN response;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;


        try
        {

            StringBuilder sb = new StringBuilder(200);
            response = ws.SubTxn(request);

            if (response.Status != "0")
            {

                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("\n");
                showArray.Text = sb.ToString();
            }
            else
            {
                showArray.Text = "Thank you for submitting your request. We are now validating the submitted file. Your reference no. is " + response.ReferenceNo.ToString();
                //String XmlizedString = null;
                //MemoryStream memoryStream = new MemoryStream();
                //XmlSerializer xs = new XmlSerializer(typeof(OS_DXC_WAP.CaesarWS.SubTxnResponse_DN));
                //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                //xmlTextWriter.Formatting = Formatting.Indented;
                //xs.Serialize(xmlTextWriter, response);
                //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                //XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                //return XmlizedString;

            }
        }
        catch (Exception ex)
        {
            showArray.Text = ex.Message;
        }
    }


    private String[] GetExcelSheetNames(string excelFile)
    {
        OleDbConnection objConn = null;
        System.Data.DataTable dt = null;

        try
        {
            // Connection String. Change the excel file to the file you

            // will search.

            String connString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + excelFile + ";Extended Properties=Excel 8.0;";
            // Create connection object by using the preceding connection string.

            objConn = new OleDbConnection(connString);
            // Open connection with the database.

            objConn.Open();
            // Get the data table containg the schema guid.

            dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            if (dt == null)
            {
                return null;
            }

            string XlName;
            String[] excelSheets = new String[dt.Rows.Count];
            int i = 0;

            // Add the sheet name to the string array.

            foreach (DataRow row in dt.Rows)
            {
                excelSheets[i] = row["TABLE_NAME"].ToString();
                i++;
            }

            // Loop through all of the sheets if you want too...

            for (int j = 0; j < excelSheets.Length; j++)
            {
                // Query each excel sheet.

            }

            return excelSheets;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            // Clean up.

            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            if (dt != null)
            {
                dt.Dispose();
            }
        }
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }





}

