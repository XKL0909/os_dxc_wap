﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Client_ClientDashboard" Codebehind="ClientDashboard.aspx.cs" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/Header.ascx" TagName="Header" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/Footer.ascx" TagName="Footer" TagPrefix="uc3" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" type="text/css" href="../Styles/bupa.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/home-slider.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/help-slider.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/lightblue.css" />
    <link href="../Scripts/Bupa/MessageBar/CSS/skin.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../Styles/Impromptu.css" />
    <link rel="stylesheet" type="text/css" href="../Styles/OSWebStyle.css" />
    <script type='text/JavaScript' src="../Scripts/scw.js"></script>
    <script language="javascript" type="text/javascript" src="../Scripts/jsDate.js"></script>
    <script src="../Scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../functions/login/functions.js" type="text/javascript"></script>
    <script src="../Scripts/Bupa/MessageBar/jquery.messagebar.js" type="text/javascript"></script>
    <!-- Impromptu -->
    <script type="text/javascript" src='../Scripts/Bupa/Impromptu/jquery-impromptu.4.0.min.js'></script>
    <title>Bupa Arabia, Online Services </title>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-4321156-5']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <style type="text/css">
        #form1
        {
            text-align: center;
        }
    </style>
</head>
<script language="javascript" type="text/javascript">

    /****************************************Drop Down & Rollover Menu  ********************************/
    function MM_swapImgRestore() {
        var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
    }

    function MM_swapImage() {
        var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2) ; i += 3)
            if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
    }

    function MM_findObj(n, d) {
        var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n); return x;
    }

    function MM_showHideLayers() {
        var i, p, v, obj, args = MM_showHideLayers.arguments;
        for (i = 0; i < (args.length - 2) ; i += 3) if ((obj = MM_findObj(args[i])) != null) {
            v = args[i + 2];
            if (obj.style) { obj = obj.style; v = (v == 'show') ? 'visible' : (v == 'hide') ? 'hidden' : v; }
            obj.visibility = v;
        }
    }
</script>
<body>
    <form id="form1" runat="server">
    <div class="socialMediaIcons">
        <a target="_blank" href="https://www.facebook.com/page.bupa.arabia" class="facebookIcon">
            <img src="/Style Library/images/spacer.gif" alt="Facebook" title="Facebook" width="1"
                height="1"></a> <a target="_blank" href="https://twitter.com/bupaarabia" class="twitterIcon">
                    <img src="/Style Library/images/spacer.gif" alt="Twitter" title="Twitter" width="1"
                        height="1"></a> <a target="_blank" href="https://youTube.com/bupaarabiachannel" class="youtubeIcon">
                            <img src="/Style Library/images/spacer.gif" alt="Youtube" title="Youtube" width="1"
                                height="1"></a> <a style="display: none" href="javascript:;" class="emailFriendIcon">
                                    <img src="/Style Library/images/spacer.gif" alt="Email Friend" title="Email Friend"
                                        width="1" height="1"></a> <a target="_blank" style="display: none" href="http://test.borninteractive.net:8081/wpbupa"
                                            class="blogIcon">
                                            <img src="/Style Library/images/spacer.gif" alt="Youtube" title="Youtube" width="1"
                                                height="1"></a> <a style="display: none" target="_blank" href="http://test.borninteractive.net/bupamicrosite"
                                                    class="micrositeIcon">
                                                    <img src="/Style Library/images/spacer.gif" alt="Microsite" title="Microsite" width="1"
                                                        height="1"></a>
    </div>
    <div>
        <table width="995" align="center" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <uc2:Header ID="Header1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <%--header--%>
    <div>
        <table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 1020px">
            <tr>
                <td class="centerBack" style="background: #E6E6E6 url(images/BackgroundImage.jpg) center 0 no-repeat;">
                    <div class="margBottom20">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="pageTitle">
                                                    <h1>bupa online services</h1>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="subWhiteCenter">
                        <div class="pageHeadRow">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="breadCrumb">
                                        <asp:Label ID="lblLoginName" runat="server" CssClass="Welcome" Text=""></asp:Label>
                                    </td>
                                    <td align="right">
                                        <script src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b87f0ff3925a6aa"
                                            type="text/javascript" defer="defer"></script>
                                        <table style="float: right" runat="server" id="backdiv">
                                            <tr>
                                                <td>
                                                    <a href="ChangePass.aspx" class="likeLinks">Change Password</a>
                                                </td>
                                                    <td>|
                                                </td>
                                                <td>
                                                    <a href="Main.aspx" class="likeLinks">Back to My Services</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%" valign="top">
                                            <img id="Img14" runat="server" src="../images/spacer.gif" alt="" width="10" height="1" />
                                            <div class="highlightText">
                                                <asp:Label Text="SABIC Dashboard" runat="server" ID="lbl1" Font-Bold="True" Font-Size="24pt"
                                                    ForeColor="Black"></asp:Label>
                                            </div>
                                            
                                            <telerik:radscriptmanager runat="server" id="RadScriptManager15" />
                                            <telerik:radajaxloadingpanel id="RadAjaxLoadingPanel15" runat="server" skin="Default">
                                                </telerik:radajaxloadingpanel>
                                            <telerik:radskinmanager id="QsfSkinManager" runat="server" showchooser="false" />
                                            <telerik:radformdecorator id="RadFormDecorator1" runat="server" decoratedcontrols="All"
                                                enableroundedcorners="false" />
                                                <table>
                                                    <tr>
                                                        <td style="width: 250px; text-align: right">
                                                            <label for="RadTimePicker1">
                                                                Select Date:
                                                            </label>
                                                        </td>
                                                        <td>from&nbsp;
                                                        <telerik:raddatepicker id="RadDatePickerFrom" runat="server">
                                                        </telerik:raddatepicker>
                                                        </td>
                                                        <td>to&nbsp;
                                                        <telerik:raddatepicker id="RadDatePickerTo" runat="server">
                                                        </telerik:raddatepicker>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="FilterDateSetByDate" runat="server" Text="Filter by date" OnClick="FilterDateSetByDate_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            <telerik:radajaxmanager id="RadAjaxManager15" runat="server">
                                                    <AjaxSettings>
                                                        <telerik:AjaxSetting AjaxControlID="chkShowHideTimePopUp">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="chkShowPopUpOnFocus">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="chkEnableShadows">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="btnToolTip">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="btnApplyStartEndTime">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="ddlTimeInterval">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="ddlFormat">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="ddlTimeViewLayout">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="ddlPopupDirection">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="rblRenderDirection">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="rblRenderDirection"></telerik:AjaxUpdatedControl>
                                                                <telerik:AjaxUpdatedControl ControlID="RadTimePicker1" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager15">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel15" />
                                                                <telerik:AjaxUpdatedControl ControlID="PreAuthLineChart" LoadingPanelID="RadAjaxLoadingPanel15"></telerik:AjaxUpdatedControl>
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel15" />
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="Button1">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="Panel2" LoadingPanelID="RadAjaxLoadingPanel15" />
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="radGridFlagFrequency">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="radGridFlagFrequency" LoadingPanelID="RadAjaxLoadingPanel15" />
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                        <telerik:AjaxSetting AjaxControlID="radGridFlagAmount">
                                                            <UpdatedControls>
                                                                <telerik:AjaxUpdatedControl ControlID="radGridFlagAmount" LoadingPanelID="RadAjaxLoadingPanel15" />
                                                            </UpdatedControls>
                                                        </telerik:AjaxSetting>
                                                    </AjaxSettings>
                                                </telerik:radajaxmanager>
                                                <script type="text/javascript">
                                                    function pnlRequestStarted(ajaxPanel, eventArgs) {
                                                        if (eventArgs.EventTarget == "pnlBtnExcelFrequency" || eventArgs.EventTarget == "pnlBtnExcelAmounts") {
                                                            eventArgs.EnableAjax = false;
                                                        }
                                                    }

                                                                </script>
                                            <telerik:radajaxpanel runat="server" id="RadAjaxPanel15" loadingpanelid="RadAjaxLoadingPanel15" clientevents-onrequeststart="pnlRequestStarted">
                                                    <table style="text-align:left;">
                                                        <tr>
                                                            <td>
                                                                <div style="width: 450px; height: 450px; float: left">
                                                                    <asp:Label ID="LabelNoReimbursementClaimsData" runat="server" Text="<br/>Overall claims status<br /><br /><br /> <b><i>No data to display</i></b>"
                                                                            Visible="False"></asp:Label>

                                                                    <a href="ReimbursementClaimChartDetails.aspx">
                                                                        
                                                                        <telerik:RadHtmlChart runat="server" ID="ReimbursementClaimChart" Height="450px" Width="450px">
                                                                            <PlotArea>
                                                                                <Series>
                                                                                    <telerik:PieSeries ColorField="PartColor" DataFieldY="MarketShare" NameField="Name"
                                                                                        ExplodeField="IsExploded">
                                                                                        <LabelsAppearance DataFormatString="{0}%">
                                                                                        </LabelsAppearance>
                                                                                        <TooltipsAppearance Color="White" DataFormatString="{0}%">
                                                                                        </TooltipsAppearance>
                                                                                    </telerik:PieSeries>
                                                                                </Series>
                                                                            </PlotArea>
                                                                            <ChartTitle Text="Overall Claims status">
                                                                            </ChartTitle>
                                                                        </telerik:RadHtmlChart>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                            <td style="text-align:left">
                                                                <div style="width: 450px; height: 450px; float: left">
                                                                        <asp:Label ID="LabelNoPreAuthData" runat="server" Text="No data to display" Visible="False" Font-Bold="True" Font-Italic="true"></asp:Label>

                                                                    <a href="PreAuthChartDetails.aspx">
                                                                        
                                                                        <telerik:RadHtmlChart runat="server" ID="PreAuthLineChart" Width="450px" Height="450px" >
                                                                            <ChartTitle Text="Overall Pre-Auth status">
                                                                            </ChartTitle>
                                                                            <Legend>
                                                                                <Appearance BackgroundColor="White" Position="Bottom">
                                                                                </Appearance>
                                                                            </Legend>
                                                                            <PlotArea>
                                                                                <XAxis Color="Teal" DataLabelsField="RECEIVED_DATE" BaseUnit="Days" Name="Date">
                                                                                    <LabelsAppearance />
                                                                                    <TitleAppearance Text="Date" />
                                                                                </XAxis>
                                                                                <Series>
                                                                                    <telerik:ColumnSeries Name="Volume" DataFieldY="PREAUTH_ID">
                                                                                        <LabelsAppearance Visible="false" DataFormatString="{0} total pre auths">
                                                                                        </LabelsAppearance>
                                                                                        <TooltipsAppearance Color="White" DataFormatString="{0} total pre-Auths">
                                                                                        </TooltipsAppearance>
                                                                                    </telerik:ColumnSeries>
                                                                                    <telerik:ColumnSeries Name="Value" AxisName="AdditionalAxisOnY" DataFieldY="Line_Amount">
                                                                                        <LabelsAppearance Visible="false" DataFormatString="{0} total amount of pre auths">
                                                                                        </LabelsAppearance>
                                                                                        <TooltipsAppearance Color="White" DataFormatString="{0} total amount of pre auths">
                                                                                        </TooltipsAppearance>
                                                                                    </telerik:ColumnSeries>
                                                                                </Series>
                                                                                <AdditionalYAxes>
                                                                                    <telerik:AxisY Name="AdditionalAxisOnY" Color="Orange" Visible="true">
                                                                                        <TitleAppearance Text="Value" Visible="false" />
                                                                                    </telerik:AxisY>
                                                                                </AdditionalYAxes>
                                                                            </PlotArea>
                                                                        </telerik:RadHtmlChart>
                                                                    </a>
                                                                    <br />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="float:left">
                                                                    <asp:Label ID="Label2" Text="Visit frequency alerts" runat="server"   Font-Bold="True" ></asp:Label>
                                                                </div>
                                                                <div style="float: right">
                                                                    <asp:Button ID="pnlBtnExcelFrequency" Width="150px" Text="Export to Excel" OnClick="btnExcelFrequency_Click"
                                                                        runat="server" visible="false" />
                                                                </div>
                                                            </td>
                                                            <td>
                                                                 <div style="float: left">
                                                                    <asp:Label ID="Label1" Text="Visit amount alerts" runat="server"   Font-Bold="True" />
                                                                </div>
                                                                <div style="float: right">
                                                                    <asp:Button ID="pnlBtnExcelAmounts" Width="150px" Text="Export to Excel" OnClick="btnExcelAmount_Click"
                                                                        runat="server"  visible="false" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%; ">
                                                                 <asp:Label ID="LabelFlagFrequency" runat="server" Text="No data to display"
                                                                            Visible="False" Font-Bold="True" Font-Italic="true"></asp:Label>
                                                                <telerik:RadGrid runat="server" ID="radGridFlagFrequency" Width="100%" Height="400px" AllowPaging="True" AllowFilteringByColumn="true" AllowSorting="True" OnColumnCreated="radGridFlagFrequency_ColumnCreated">
                                                                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                                                        <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                                                                    </ClientSettings>
                                                                    <ExportSettings HideStructureColumns="true" ExportOnlyData="true" IgnorePaging="true"
                                                                        OpenInNewWindow="true">
                                                                    </ExportSettings>
                                                                </telerik:RadGrid>

                                                            </td>
                                                            <td style="width: 50%; ">
                                                               <asp:Label ID="LabelFlagAmount" runat="server" Text="No data to display"
                                                                            Visible="False" Font-Bold="True" Font-Italic="true"></asp:Label>
                                                                <telerik:RadGrid runat="server" ID="radGridFlagAmount" Width="100%" Height="400px" AllowPaging="True" AllowFilteringByColumn="true" AllowSorting="True" OnColumnCreated="radGridFlagAmount_ColumnCreated">
                                                                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                                                        <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                                                                    </ClientSettings>
                                                                    <ExportSettings HideStructureColumns="true" ExportOnlyData="true" IgnorePaging="true"
                                                                        OpenInNewWindow="true">
                                                                    </ExportSettings>
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </telerik:radajaxpanel>
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </fieldset>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <uc3:Footer ID="Footer1" runat="server" />
<%--    &nbsp;<div class="copyrightContainer">
        <table width="995" align="center" border="0" cellspacing="0" cellpadding="0" class="footerLinks">
            <tr>
                <td class="copyrightContainerPad">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                                <td>Copyright © Bupa Arabia 2012. All rights reserved. <a href="http://www.bupa.com.sa/English/Pages/Terms-Conditions.aspx">Terms &amp; Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.bupa.com.sa/English/Pages/Sitemap.aspx">Sitemap</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                                        href="http://www.bupa.com.sa/English/Pages/Privacy-Policy.aspx">Privacy Policy</a>
                            </td>
                                <td align="right">Powered by <a href="http://www.borninteractive.com/">Born Interactive</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>--%>
    <script type="text/javascript" src="../functions/jquery.anythingslider.min.js"></script>
    <script type="text/javascript" src="../functions/cufon.js"></script>
    <script type="text/javascript" src="../functions/Gotham_Book_400.font.js"></script>
    <script type="text/javascript" src="../functions/cufon.init.js"></script>
    <script type="text/javascript" src="../functions/functions.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            toggleFooter();
        });

        $(function () {
            $('#slider,#Helpslider').anythingSlider();
        });
    </script>
    </form>
</body>
</html>
