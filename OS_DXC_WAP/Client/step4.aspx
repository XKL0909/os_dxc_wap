﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="step4" Codebehind="step4.aspx.cs" %>

<form id="form4" runat="server">
    <div class="card bb">
        <div class="header bb">
            <h2>CCHI </h2>
        </div>
        <div class="body bb">
            <div class="actions clearfix text-right bb">
                <button id="btnPrevious4" class="btn btn-primary prevBtn btn-sm bb" type="button">Previous / السابق</button>
                <button class="btn btn-primary  btn-sm bb" type="button" data-toggle="tooltip" data-placement="bottom" data-html="true"  title="submit<br>تقديم الطلب">Submit / تقديم</button>
                <asp:button id="btnNext4" validationgroup="valGrpStep2" runat="server" text="Next / التالي" class="btn btn-primary nextBtn btn-sm pull-right bb" />
            </div>
        </div>
    </div>
</form>
<script>
$('[data-toggle="tooltip"]').tooltip();
    $(document).ready(function () {

        $("#btnPrevious4").click(function () {
            triggerStep(3, "previ");
            return false;
        })
    });

    function triggerStep(id, type) {
        var nextId = "step" + id + ".aspx";
        var containerId = "#divStep" + id;
        var triggerId = "#step" + id;
        $.ajax({
            type: "POST",
            url: nextId,
            success: function (data) {
             
                $(containerId).html(data);
            },
            failure: function () {
                alert("Failed!");
            }
        });
        $(triggerId).trigger('click');
    }
</script>