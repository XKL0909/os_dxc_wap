﻿using System;
using UnifiedPolicy.Services;

public partial class AddMemberIntermediate : System.Web.UI.Page
{
    public static string ClientId;
    public static string ClientUserName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        ClientId = Convert.ToString(Session["ClientID"]);
        ClientUserName = Convert.ToString(Session["ClientUsername"]);
        if(!string.IsNullOrEmpty(ClientId) && !string.IsNullOrEmpty(ClientUserName))
        {
			Caesar.CaesarManager service = new Caesar.CaesarManager();//// CaesarManager();
            Caesar.CaesarManager.Indicator indicator = new Caesar.CaesarManager.Indicator();
            indicator = service.GetMandatoryIndicator(ClientId);
            Session["MandatoryIndicator"] = indicator;
            AddMemberRequestService addMemberRequestService = new AddMemberRequestService();
            long id = GetUnifiedRequestID(addMemberRequestService);
            Session["RequestID"] = id;
            Response.Redirect("Details.aspx");
        }
        else
            Response.Redirect("~/Default.aspx");
    }

    private static long GetUnifiedRequestID(AddMemberRequestService service)
    {
        var result = service.Add(new UnifiedPolicy.Services.Dto.AddMemberRequest
        {
            OwnerId = ClientId,
            ContractNo = ClientId,
            Created = DateTime.Now,
            Modified = DateTime.Now,
            CreatedBy = ClientUserName,
            IsEnable = false,
            IsPosted = false,
            Status = "Pending"
        });
        return result;
    }
}