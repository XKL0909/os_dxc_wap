﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master"  AutoEventWireup="true" Inherits="CreditControl1" Codebehind="CreditControl1.aspx.cs" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">

    <script type='text/JavaScript' src='scw.js'></script>

    <style type="text/css">
        #moreinfodiv {
            display: none;
            background-color: #fff;
            border: groove 2px #000;
            font-family: Arial;
            font-size: 12px;
        }
    </style>
    <style type="text/css" media="screen">
        a {
            text-decoration: none;
            color: black;
        }
            a:hover {
                text-decoration: none;
            }
            /*BG color is a must for IE6*/
            a.tooltip span {
                display: none;
                padding: 2px 3px;
                margin-left: 8px;
                font-style: normal;
                font-size: 11px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                background: white;
                border: 1px solid green;
                color: blue;
            }

        select.style01 {
            border: 1px solid #A5A5A5;
            backgorund-color: #D0D0D0;
        }

            select.style01 option {
                border-bottom: 1px solid #dadada;
            }
        input, text, options {
            BORDER-RIGHT: #e0e0e0 0px solid;
            BORDER-BOTTOM: #e0e0e0 0px solid; 
        }
    
         .DatePicker {
            BACKGROUND: url(images/cal.gif) no-repeat right center;
        }

        DIV.DatePicker-Container {
            font-size: 9px;
        }

        DIV.DatePicker-Wrapper {
            BORDER-RIGHT: #e0e0e0 1px solid;
            BORDER-TOP: #e0e0e0 1px solid;
            BORDER-LEFT: #e0e0e0 1px solid;
            BORDER-BOTTOM: #e0e0e0 1px solid;
            BACKGROUND-COLOR: skyblue;
        }

            DIV.DatePicker-Wrapper TABLE {
            }

            DIV.DatePicker-Wrapper TD {
                PADDING-RIGHT: 0px;
                PADDING-LEFT: 0px;
                PADDING-BOTTOM: 0px;
                PADDING-TOP: 0px;
            }

                DIV.DatePicker-Wrapper TD.nav {
                }

                DIV.DatePicker-Wrapper TD.dayName {
                    BORDER-RIGHT: #e0e0e0 1px solid;
                    FONT-WEIGHT: bold;
                    BORDER-BOTTOM: #e0e0e0 1px solid;
                    BACKGROUND-COLOR: #eeeeee;
                }

                DIV.DatePicker-Wrapper TD.day {
                    BORDER-RIGHT: #e0e0e0 1px solid;
                    BORDER-BOTTOM: #e0e0e0 1px solid;
                }

                DIV.DatePicker-Wrapper TD.empty {
                    BORDER-RIGHT: #e0e0e0 1px solid;
                    BORDER-BOTTOM: #e0e0e0 1px solid;
                }

                DIV.DatePicker-Wrapper TD.current {
                    COLOR: #ffffff;
                    BACKGROUND-COLOR: #666666;
                }

                DIV.DatePicker-Wrapper TD.dp_roll {
                    BACKGROUND-COLOR: #e0e0e0;
                }
    </style>


    <style>
        input {
            background-color: #eff6fc;
            background-repeat: no-repeat;
            background-position: left;
            border: 1px solid #6297BC;
            padding-left: 16px;
        }

            input radio {
                background-color: blue;
                background-repeat: no-repeat;
                background-position: left;
                border: 1px solid #6297BC;
                <%-- padding-left:16px;
                --%>;
            }

        select option {
            background-color: #fffcc;
        }

            select option.alt {
                background-color: #fffcc;
            }
    </style>

	<%--Added by Hussamuddin for Testing Bug fix number 13--%>

    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function ValidateDataToField()
                {
                if( document.getElementById('ctl00_ContentPlaceHolderLeft_txtDateTo').value<document.getElementById('ctl00_ContentPlaceHolderLeft_txtDateFrom').value){
                    if (!Page_ClientValidate()) {
                        document.getElementById('ValidateToFromDate').innerText = "Not a valid date! Data should be less than or equal to Date From";
                        document.getElementById('ctl00_ContentPlaceHolderLeft_txtDateTo_dateInput').value = '';
						ValidatorEnable(document.getElementById("ValidateToFromDate"), true);
                    }
                                        
                }
                else{
						document.getElementById('ValidateToFromDate').innerText = '';
						ValidatorEnable(document.getElementById("ValidateToFromDate"), false);
					}
                    
					
                }
            
        </script>
    </telerik:RadScriptBlock>
     <%--Added by Hussamuddin for Testing Bug fix number 13--%>
	
	
    <script type="text/javascript">
        function doEnable(Val, check1, check2) {
            var DateFrom = document.getElementById('txtDateFrom');
            var DateTo = document.getElementById('txtDateTo');
            var InvoiceNo = document.getElementById('txtInvoiceNo');
            var CHK1 = document.getElementById(check1);
            var CHK2 = document.getElementById(check2);
            if (Val == '1') {
                if (CHK1.checked == false) {
                    CHK1.checked = true;
                }
                DateFrom.disabled = true;
                DateTo.disabled = true;
                InvoiceNo.disabled = false;
                CHK2.checked = false;
                rfvInvoiceNo.enabled = true;
                rfvDateFrom.enabled = false;
                rfvDateTo.enabled = false;
            }
            if (Val == '2') {
                if (CHK2.checked == false) {

                    CHK2.checked = true;
                }
                DateFrom.disabled = false;
                DateTo.disabled = false;
                InvoiceNo.disabled = true;
                CHK1.checked = false;
                rfvDateFrom.enabled = true;
                rfvDateTo.enabled = true;
                rfvInvoiceNo.enabled = false;
            }
        }

        function EnableInvoiceNo() {
            var DateFrom = document.getElementById('txtDateFrom');
            var DateTo = document.getElementById('txtDateTo');
            var InvoiceNo = document.getElementById('txtInvoiceNo');
            var CHK1 = document.getElementById("CheckBox1");
            if (CHK1.checked == true) {
                InvoiceNo.disabled = false;
                rfvInvoiceNo.enabled = true;

                DateFrom.disabled = true;
                DateTo.disabled = true;
                rfvDateFrom.enabled = false;
                rfvDateTo.enabled = false;
                InvoiceNo.focus();
            }
        }

        function ValidateInvoiceNo() {
            if (document.getElementById('txtDateFrom').value == '' && document.getElementById('txtDateTo').value == '') {
                alert();
                document.getElementById('rfvInvoiceNo').enabled = true;
                document.getElementById('txtInvoiceNo').style.backgroundColor = '#FFFFC0';
                alert();
                return false;
            }
            else
                return true;
        }

        function ActivateRequiredFields(Reason) {
            document.getElementById('rfvGender').enabled = false;
            document.getElementById('rfvNationality').enabled = false;
            document.getElementById('rfvName').enabled = false;
            document.getElementById('rfvDOB').enabled = false;
            document.getElementById('rfvIqamaID').enabled = false;
            document.getElementById('rfvEmployeeNo').enabled = false;
            document.getElementById('ddlEGender').style.backgroundColor = '#FFFFFF';
            document.getElementById('ddlENationality').style.backgroundColor = '#FFFFFF';
            document.getElementById('ddlETitle').style.backgroundColor = '#FFFFFF';
            document.getElementById('txtCorrectName').style.backgroundColor = '#FFFFFF';
            document.getElementById('txtCorrectDOB').style.backgroundColor = '#FFFFFF';
            document.getElementById('txtCorrectIqamaID').style.backgroundColor = '#FFFFFF';
            document.getElementById('txtEmployeeNo').style.backgroundColor = '#FFFFFF';

            if (Reason == 'REP004') {
                document.getElementById('rfvName').enabled = true;
                document.getElementById('txtCorrectName').style.backgroundColor = '#FFFFC0';
            }

            if (Reason == 'REP005') {
                var valDOB = document.getElementById('rfvDOB'); valDOB.enabled = true;
                document.getElementById('txtCorrectDOB').style.backgroundColor = '#FFFFC0';
            }
            if (Reason == 'REP006') {
                document.getElementById('rfvEmployeeNo').enabled = true;
                document.getElementById('txtEmployeeNo').style.backgroundColor = '#FFFFC0';
            }
            if (Reason == 'REP007') {
                document.getElementById('rfvNationality').enabled = true;
                document.getElementById('ddlENationality').style.backgroundColor = '#FFFFC0';
            }

            if (Reason == 'REP008') {
                document.getElementById('rfvIqamaID').enabled = true;
                document.getElementById('txtCorrectIqamaID').style.backgroundColor = '#FFFFC0';
            }
            if (Reason == 'REP009') {
                document.getElementById('rfvGender').enabled = true;
                document.getElementById('ddlEGender').style.backgroundColor = '#FFFFC0';
            }
        }

        function ToggleSearchCriteria(Val, check1, check2, txtDateFrom, txtDateTo, txtInvoiceNo) {
            var DateFrom = document.getElementById(txtDateFrom);
            var DateTo = document.getElementById(txtDateTo);
            var InvoiceNo = document.getElementById(txtInvoiceNo);
            var CHK1 = document.getElementById(check1);
            var CHK2 = document.getElementById(check2);
            if (Val == '2') {
                if (CHK1.checked == false) {
                    CHK1.checked = true;
                }
                DateFrom.disabled = true;
                DateTo.disabled = true;
                ddlBranchCodeNew.disabled = true;
                InvoiceNo.disabled = false;
                CHK2.checked = false;
            }
            if (Val == '1') {
                if (CHK2.checked == false) {

                    CHK2.checked = true;
                }
                DateFrom.disabled = false;
                DateTo.disabled = false;
                ddlBranchCodeNew.disabled = false;
                InvoiceNo.disabled = true;
                CHK1.checked = false;
            }
        }
    </script>
    <div>
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>

        <fieldset>
            <legend>
                <h1>
                    <asp:Label ID="lblAdd_Employee" runat="server" Text="Statement of Accounts" Font-Size="Large" Font-Names="Arial"></asp:Label>
                </h1>
            </legend>
            <asp:Label ID="qqq" runat="server" Text="" Font-Bold="True"></asp:Label>
            <br />
            <br />
<%--            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Red" Text="Fields marked with asterisk [ * ]  are mandatory."></asp:Label>--%>
            <asp:Panel ID="PnlInvoice" runat="server" Width="100%" Visible="False">
                <table style="font-size: small; vertical-align: text-top;" width="90%">
                    <tr>
                        <td colspan="2" style="width: 100%">
<%--                            <fieldset>
                                <legend>Type of Invoice:</legend>--%>
                                 <asp:Label ID="Label5" runat="server" Text="Type of Invoice" Width="126px" Font-Bold="True"></asp:Label>
                                    <asp:RadioButtonList CssClass="tte" ID="RBLPanel1" runat="server" RepeatColumns="3"
                                        RepeatDirection="Horizontal" Width="50%" BackColor="Transparent" BorderColor="Red" BorderStyle="None" 
                                        BorderWidth="0px" ToolTip="Select the type of invoice to be generated ( Instalment / Amendment / Sundry ).
                                                            اختر نوع الفاتورة التي ترغب في طباعتها ( أقساط / اضاقات وإلغاءات / متفرقات">
                                        <asp:ListItem Text="Installment" Value="I"></asp:ListItem>
                                        <asp:ListItem Text="Amendment" Value="A"></asp:ListItem>
                                        <asp:ListItem Text="Sundry" Value="S"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RBLPanel1"
                                        Display="Dynamic" ErrorMessage="Please select the Invoice type"></asp:RequiredFieldValidator>
                                    <br />
<%--                                    <span>Select the type of invoice to be generated ( Instalment / Amendment / Sundry ).<br />
                                        اختر نوع الفاتورة التي ترغب في طباعتها ( أقساط / اضاقات وإلغاءات / متفرقات ).
                                    </span>--%>
<%--                            </fieldset>--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="width: 100%">
                            <asp:Label ID="Label3" runat="server" Text="Search Criteria" Width="126px" Font-Bold="True"></asp:Label>
<%--                                <asp:CheckBox ID="chkDateRange" runat="server"  Checked="True" Text="Date Range:" />
                                <asp:CheckBox ID="chkInvoiceNo" runat="server"  Checked="False" Text="Invoice No:" />--%>
                            <asp:RadioButtonList ID="rdnSearchCriteria" runat="server" RepeatDirection="Horizontal" Width="35%">
                                  <asp:ListItem  Value="0">By Date Range</asp:ListItem>
                                   <asp:ListItem Value="1">By Invoice No</asp:ListItem>
                            </asp:RadioButtonList>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rdnSearchCriteria"
                                        Display="Dynamic" ErrorMessage="Please select the Search Criteria"></asp:RequiredFieldValidator>
                            <br />
                        </td>
<%--                        <td style="width: 50%">--%>
<%--                            <a class="tooltip" href="#">

                                <span>Select "Invoice No" for a specific invoice or select the "Date Range" to<br />
                                    find invoices generated during the specified range.<br />
                                    ادخل رقم الفاتورة في حال معرفتك لرقمها أو بإمكانك اختيار 
                             <br />
                                    الفترة التي تخص الفواتير التي ترغب في طباعتها عن طريق الخيار الآخر.
                                </span></a>--%>
<%--                        </td>--%>
                    </tr>
                    <tr>
                        <td style="width: 213px"></td>
                        <td style="width: 200px"></td>
                    </tr>
                    <tr>
                        <td style="width: 120px">
                            <asp:Label ID="Label2" runat="server" Text="Date From" Font-Bold="True"></asp:Label>
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                                <telerik:RadDatePicker Width="250px" ID="txtDateFrom" runat="server">
                                    <Calendar ID="Calendar1" runat="server" EnableKeyboardNavigation="true">
                                    </Calendar>
                                    <DateInput ToolTip="Enter the date from which you would like the results to be listed."
                                         DateFormat="dd/MM/yyyy" runat="server"  >
                                    </DateInput>
                                </telerik:RadDatePicker>
                            </telerik:RadAjaxPanel>

                            <asp:RequiredFieldValidator ID="rfvDateFrom" runat="server" ControlToValidate="txtDateFrom"
                                Display="Dynamic" ErrorMessage="Required Field"></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 200px">
                            <asp:Label ID="Label4" runat="server" Text="Date To" Font-Bold="True"></asp:Label>
                            <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                                <telerik:RadDatePicker Width="250px" ID="txtDateTo" runat="server" ClientEvents-OnDateSelected="ValidateDataToField">
                                    <Calendar ID="Calendar2" runat="server" EnableKeyboardNavigation="true">
                                    </Calendar>
                                    <DateInput ToolTip="Enter the date to which you would like the results to be listed."
                                        DateFormat="dd/MM/yyyy" runat="server" >
                                    </DateInput>
                                </telerik:RadDatePicker>
                            </telerik:RadAjaxPanel>
                            <asp:RequiredFieldValidator ID="rfvDateTo" runat="server" ControlToValidate="txtDateTo"
                                Display="Dynamic" ErrorMessage="Required Field"></asp:RequiredFieldValidator>
								<asp:CompareValidator Display="Dynamic" runat="server" ControlToCompare="txtDateFrom" ID="ValidateToFromDate" ControlToValidate="txtDateTo"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 156px">
                            <br />
                            <asp:Label ID="lblInvoiceNumber" runat="server" Text="Invoice Number" Width="126px" Font-Bold="True"></asp:Label><br />
                                    <asp:TextBox CssClass="textbox" EnableViewState="true" ID="txtInvoiceNo" runat="server" Width="238px" 
                                        MaxLength="8"  BackColor="White" Enabled="True" Height="20px"></asp:TextBox>
                            <br />
<%--                                    <asp:RequiredFieldValidator ID="rfvInvoiceNo" runat="server" ControlToValidate="txtInvoiceNo"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px" Enabled="False">Field is mandatory</asp:RequiredFieldValidator>--%>
                        </td>
                        <td style="width: 200px">
                            <div>


                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="PnlStatement" runat="server" Width="100%" Visible="False">
                <table border="0" style="font-size: small; vertical-align: text-top;"
                    width="90%">
                    <tr>
                        <td colspan="2" style="width: 100%">
                            <fieldset>
                                <legend>Type of Statement:</legend>
                                <a class="tooltip" href="#">
                                    <asp:RadioButtonList ID="RDBSundryPremium" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                        Width="83%">
                                        <asp:ListItem Text=" Premium" Selected="True" Value="N"> Premium</asp:ListItem>
                                        <asp:ListItem Text=" Sundry" Value="Y"> Sundry</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="RDBSundryPremium"
                                        Display="Dynamic" ErrorMessage="Please select Premium or Sundry"></asp:RequiredFieldValidator>
                                    <asp:RadioButtonList ID="RDBFullShort" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                        Width="92%">
                                        <asp:ListItem Selected="True" Text=" Full" Value="F"> Full Statement</asp:ListItem>
                                        <asp:ListItem Text=" Short" Value="S"> Short Statement</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="RDBFullShort"
                                        Display="Dynamic" ErrorMessage="Please select Full or Short"></asp:RequiredFieldValidator>
                                    <span>Select the type of statement you would like to print ;( Premium statement or sundry) ?<br />
                                        Full statement means all transactions including fully settled payments.<br />
                                        Short statement means only outstanding / unsettled transactions.
       
           <br />
                                        اختر نوع التقرير الذي ترغب به : كشف الحساب الاساسي أم كشف حساب المتفرقات؟
                                        <br />
                                        في حال اختيار كشف تفصيلي سيتم عرض جميع الحركات المالية بما في ذلك الفواتير المسددة.
                                        <br />
                                        في حال اختيار كشف مختصر سيتم عرض الحركات والفواتير غير المسددة فقط.<br />
                                    </span></a>
                                <p align="left">
                                    <font size="1" face="Arial">1. Full Statement means all transactions including fully settled payments.<br />
                                        2. Short statement means only outstanding /unsettled transactions.
                                    </font>

                                </p>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table border="0"
                style="vertical-align: text-top;" width="90%">
                <tr>
                    <td colspan="2">
                        <br />
                        <asp:Label ID="lblOptionNew" runat="server" Text="Branch" Width="55px" Font-Bold="True"></asp:Label>
                            <div class="styled-select">
                                <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlBranchCodeNew" runat="server" Width="238px" 
                                    Height="25px">
                                    <asp:ListItem>-Select-</asp:ListItem>
                                    <asp:ListItem Value="10290300">Branch</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvddlBranchCodeNew" runat="server" ControlToValidate="ddlBranchCodeNew"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" Width="250px">Field is mandatory</asp:RequiredFieldValidator>
 <%--                           <span>Select the branch for which you would like to generate the report.<br />
                                اختر الفرع للفاتورة التي ترغب في طباعتها.
                            </span>--%>
                    </td>
                </tr>
                <tr bgcolor="white" bordercolor="white" style="border: 0px;">
                    <td align="right"></td>
                    <td bordercolor="white" align="right">
                        <asp:Button CssClass="submitButton" ID="btnSubmit" runat="server" Text="Search" OnClick="Button1_Click" BackColor="Control" Width="120px" />
                    </td>
                </tr>
            </table>
            <p align="right">
                <asp:Button CssClass="submitButton" ID="btnChangeBranch" runat="server" OnClick="btnChangeBranch_Click" Text="Submit Request"
                    Width="133px" Visible="false" BackColor="Control" />
                <asp:Button CssClass="submitButton" ID="btnChangeClass" runat="server" OnClick="btnChangeClass_Click" Text="Submit Request"
                    Visible="False" Width="133px" />
            </p>
            <br />
            <asp:Label ID="Message1" runat="server" Width="527px" Font-Size="Small"></asp:Label>
            <table>
                <tr>
                    <td>
                        <div style="overflow: hidden; width: 95%; font-family: Verdana; height: auto;" id="CoverageListReport"
                            runat="server" visible="false">
                            <img src="file:///C:\Downloads\imagemap-1291\AJAX_ImageMap\animated_loading.gif" />
                        </div>
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblResult" runat="server" Width="344px"></asp:Label>
            &nbsp;<uc1:OSNav ID="OSNav1" runat="server" />
        </fieldset>
    </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
</asp:Content>
