using System;
using System.Web.UI.WebControls;
using System.Text;

using Bupa.OSWeb.Business;
using Utility.Configuration;
using OS_DXC_WAP.CaesarWS;

public partial class CtntClaimHistory : System.Web.UI.Page
{
    //string strMembershipNo;
    string  strClientID ;   
    private ServiceDepot_DNService ws;

   //change the name of the constructor
    public CtntClaimHistory()
    {
        //InitializeComponent();
        this.Load += new EventHandler(CtntClaimHistory_Load);
}

//change the name of the constructor and the page title
    void CtntClaimHistory_Load(object sender, EventArgs e)
{
    Master.SearchButton.Click += SearchButton_Click;
    Master.ddlStaffList.SelectedIndexChanged += DDLDependentList_SelectedIndexChanged;
    Master.SearchOpt.SelectedIndexChanged += rdbSearchOption_SelectedIndexChanged;
    strClientID = Session["ClientID"].ToString();
    //Master.ddlStaffList.Visible = false;
    Master.PageTitle.Text = "Claim History";
   
}
    protected void rdbSearchOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Master.ddlStaffList.Visible == true)
        {
            Master.ddlStaffList.Visible = false;
        }
        Master.SearchText.Text = "";
        Message1.Text = "";
        if (UpdatePanel2.Visible == true)
            UpdatePanel2.Visible = false;
    }
private void SearchButton_Click(object sender, EventArgs e)
{
    string strMemberExistStatus;
    strMemberExistStatus = "";
    if (Master.SearchOpt.Text != "Staff No" && Master.SearchOpt.Text != "Badge No")
    {

        strMemberExistStatus = CheckMemberExist(Master.SearchText.Text);
        if (strMemberExistStatus != "Exist")
                {
                    Message1.Text = strMemberExistStatus;
                    Master.ddlStaffList.Visible = false;
                }
                else
            DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);


    }
    else
        DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);
    


 

    
}


    private string CheckMemberExist(string MemberNo)
    {
        ws = new ServiceDepot_DNService();

        ChkMbrExistRequest_DN request;
        ChkMbrExistResponse_DN response;
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new ChkMbrExistRequest_DN();
            request.membershipNo = MemberNo;
            request.iqamaNo = "";// IqamaNo;
            request.totalNoOfFamily = ""; // DependentNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            //            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            //request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));
            //request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1,2 ) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 2));
            //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ChkMbrExist(request);
            // Cursor.Current = Cursors.Default;


            if (response.memberName != null)
            {
                return "Exist";
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }
            else
            {
                return response.errorMessage[0];
                //CaesarResult = "InValid";
                //CaesarMessage = response.errorMessage;
            }
            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            return "Error";
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }




    protected void DDLDependentList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strMemberExistStatus;
        if (Master.ddlStaffList.SelectedValue != "0")
        {
            strMemberExistStatus = CheckMemberExist(Master.ddlStaffList.SelectedValue);

            if (strMemberExistStatus != "Exist")
            {
                Message1.Text = strMemberExistStatus;
                Message1.Visible = true;
                UpdatePanel2.Visible = false;
                Master.ddlStaffList.Visible = true;
            }
            else
            {


                DisplayClaimHist(Master.ddlStaffList.SelectedValue);
                //if (Master.ddlStaffList.SelectedValue != "0")
                //    DisplayMemberExistForClientResult(Master.ddlStaffList.SelectedValue, "Membership No");
                Master.ddlStaffList.Visible = true;
                lblMemberName.Visible = false; //.Text = Master.ddlStaffList.SelectedItem;
                lblMember_Name.Visible = false;
                UpdatePanel2.Visible = true;
                Message1.Visible = false;
            }
        }
        else
        {
            Message1.Text = "Please select the valid name from the drop down";
            Message1.Visible = true;
            UpdatePanel2.Visible = false;
        }
    }




    private void DisplayMemberExistForClientResult(string SearchText, string SearchType)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        EnqMbrListInfoRequest_DN request;
        EnqMbrListInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqMbrListInfoRequest_DN();
            request.customerNo = strClientID;

            if (SearchType == "Membership No")
                request.membershipNo = SearchText;
            else
                request.staffNo = SearchText;

            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.detail == null)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = "Inputted '" + Master.SearchOpt.Text  + "' does not exist in your group.";
                UpdatePanel2.Visible = false;
                Message1.Visible = true;
                Master.ddlStaffList.Visible = false;
            }
            else
            {

                Master.ddlStaffList.Items.Clear();

                ListItem DepList0 = new ListItem();
                DepList0.Text = "Please select from the list";
                DepList0.Value = "0";
                Master.ddlStaffList.Items.Add(DepList0);


                foreach (MbrListDetail_DN dtl in response.detail)
                {
                    ListItem DepList = new ListItem();
                    DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                    DepList.Value = dtl.membershipNo.ToString();
                    Master.ddlStaffList.Items.Add(DepList);


                }

                Master.ddlStaffList.Visible = true;
                Message1.Visible = false;





                //response.detail[0].relationship;
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }


            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }






    private void DisplayClaimHist(string MemberNo)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        EnqClmHisInfoRequest_DN request;
        EnqClmHisInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqClmHisInfoRequest_DN();
            request.MembershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.TransactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.EnqClmHisInfo(request);

            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.Status == "0")
            {

                lblMembershipNo.Text = request.MembershipNo;
                lblMemberName.Visible = true;
                lblMember_Name.Visible = true;
                ////////lblMemberName.Text = strMemberName;
                // lblClassName.Text = response.className;
                // lblCustomerName.Text = response.companyName;

                DisplayClaimHistoryResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

              
            }
            else
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }

    public static int CalculateMonthDifference(DateTime startDate, DateTime endDate)
    {
        int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
        return Math.Abs(monthsApart);
    }


    //private void DisplayClaimHistoryResult(EnqClmHisInfoResponse_DN response)
    //{
    //    ListItem itm;
    //    StringBuilder msge;


    //    string str1;
    //    string str2;
    //    str2 = "";
    //    str1 = "";
    //    StringBuilder sbResponse = new StringBuilder(2200);

    //    if (response.Status == "0")
    //    {

    //        //dtl.proratedOverallAnnualLimit = ;


    //        //            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;border-collapse:collapse;font-size:10px;  ;'><thead  bgcolor='#e6f5ff'><tr><th>Provider Name</th>   <th>Claim Voucher No</th><th>Billed Amount</th>   <th>Received Date</th><th>Date of Payment</th>   <th>Cheque No</th> <th>Cheque Date</th> <th>Cheque Amount</th> <th>Bank Name</th> <th>Payee Name</th> <th>EFT Date</th> <th>Claim Status</th></tr>	</thead><tbody> ");

    //        //    sbResponse.Append("<a href='../Client/claimsreport.aspx?VID=4783695' ><hr></a><br>");


    //        sbResponse.Append("<table><tr><td><font size=2>Please click on the <b>Claim Voucher Number</b> to view the Reimbursement Statement:</font></td><td align='right'><font size=3>الرجاء الضغط على رقم المطالبة لمشاهدة كشف حساب المطالبات</font></td><tr><table>");

    //        sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Arial;font-color:black;border-collapse:collapse;font-size:11px;  ;'><thead  bgcolor='#e6f5ff'><tr><th>Provider Name</th>   <th>Claim Voucher No</th> <th>Received Date</th> <th>Billed Amount</th>   <th>Date of Payment</th>   <th>Cheque No</th> <th>Cheque Date</th> <th>Cheque Amount</th> <th>EFT No.</th> <th>EFT Date</th> <th>Payee Name</th>  <th>Claim Status</th> <th>Remarks</th> </tr>	</thead><tbody> ");
    //        bool _isHyperLink = false;
    //        int _Months = 0;
    //        foreach (EnqClmHisDetail_DN dtl in response.detail)
    //        {
    //            if (Convert.ToDateTime(dtl.ReceivedDate) <= Convert.ToDateTime("12/03/2011"))
    //            {
    //                _isHyperLink = false;
    //            }
    //            else
    //            {
    //                _isHyperLink = true;
    //                _Months = CalculateMonthDifference(DateTime.Today, Convert.ToDateTime(dtl.ReceivedDate));
    //                if (_Months >= 6)
    //                {
    //                    _isHyperLink = false;
    //                }

    //            }
    //            sbResponse.Append("<tr><td >" + dtl.ProviderName + "</td>");
    //            if (_isHyperLink == true)
    //            {
    //                if (dtl.ProviderName.ToString() == "Out of Network")
    //                {
    //                    if (dtl.ClaimsStatus != "Pending")
    //                    {
    //                        sbResponse.Append("<td><font color='black' ><a  style='color: Black;' href='claimsreport.aspx?VID=" + dtl.ClaimVoucherNo.ToString().Trim() + "&EN=" + "" + "'  style='cursor:hand;'>" + dtl.ClaimVoucherNo + "</a></font></td>");
    //                    }
    //                    else
    //                    {
    //                        sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
    //                    }
    //                }
    //                else
    //                {
    //                    sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
    //                }
    //            }
    //            else
    //            {
    //                if (dtl.ProviderName.ToString() == "Out of Network")
    //                    sbResponse.Append("<td><font color='black' ><a  style='color: Black;' href='javascript:_Alert();' style='cursor:hand;'>" + dtl.ClaimVoucherNo + "</a></font></td>");
    //                else
    //                    sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
    //            }
    //            sbResponse.Append("<td>" + String.Format("{0:d}", dtl.ReceivedDate) + "</td>");


    //            if (dtl.PresentedAmount.ToString().Contains(".") == false)
    //                sbResponse.Append("<td>" + dtl.PresentedAmount + ".00</td>");
    //            else
    //                sbResponse.Append("<td>" + dtl.PresentedAmount + "</td>");


    //            sbResponse.Append("<td>" + String.Format("{0:d}", dtl.PayDate) + "</td><td>" + dtl.ChequeNo + "</td><td>" + String.Format("{0:d}", dtl.ChequeDate) + "</td><td>");

    //            if (dtl.ChequeAmount.ToString() != "-1")
    //            {
    //                //sbResponse.Append(dtl.ChequeAmount);

    //                if (dtl.ChequeAmount.ToString().Contains(".") == false)
    //                    sbResponse.Append(dtl.ChequeAmount + ".00");
    //                else
    //                    sbResponse.Append(dtl.ChequeAmount + "");
    //            }


    //            sbResponse.Append("</td><td>" + dtl.EFTNO + "</td><td>" + String.Format("{0:d}", dtl.EFTDate) + "</td>");

    //            //if (dtl.ChequeAmount != -1)
    //            //    sbResponse.Append(dtl.ChequeAmount);

    //            //<td>" + dtl.BankName + "</td>
    //            sbResponse.Append("<td>" + dtl.PayeeName + "</td><td>" + dtl.ClaimsStatus + "</td><td>" + dtl.Remarks + "</td></tr>");
    //            //if (dtl.detail_ProratedLimit != -1)
    //            //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
    //            //else
    //            //    sbResponse.Append("<td>Not Covered</td></tr>");


    //        }
    //        sbResponse.Append("</table><table><tr><td>");

    //        sbResponse.Append("<font size=2>Dear Customer Please note that reimbursement claims received before <b><span  style='color:red'>March  12th, 2011 </span></b> will not have online reimbursement statement. Kindly contact our customer service advisers on 8002440307 if you need a copy of the statement.</td>");
    //        sbResponse.Append("<td align='right'><font size=2>عزيزي العميل نود أن نحيطكم علما بأنه بالامكان الحصول على النسخة الكترونية من كشف حساب مطالبات الافراد النقدية المستلمة بعد تاريخ <b><span  style='color:red'>  12/3/2011 </span></b>  م وذلك عن طريق الموقع الاكتروني لبوبا العربية. في حال رغبتكم الحصول على كشوفات حساب لمطالبات قديمة قبل هذا التاريخ الرجاء الاتصال على الهاتف المجاني لخدمة العملاء لبوبا العربية 8002440307</font></td></tr></table>");

    //        CoverageListReport.Visible = true;
    //        CoverageListReport.InnerHtml = sbResponse.ToString();


    //    }
    //    else
    //    {
    //        //msge = new StringBuilder(100);
    //        //foreach (String s in response.errorMessage)
    //        //{
    //        //    msge.Append(s).Append("/n");
    //        //}
    //        //Message1.Text = msge.ToString();
    //    }

    //    //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    //}


    private void DisplayClaimHistoryResult(EnqClmHisInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;


        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.Status == "0")
        {

            //dtl.proratedOverallAnnualLimit = ;


            //            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;border-collapse:collapse;font-size:10px;  ;'><thead  bgcolor='#e6f5ff'><tr><th>Provider Name</th>   <th>Claim Voucher No</th><th>Billed Amount</th>   <th>Received Date</th><th>Date of Payment</th>   <th>Cheque No</th> <th>Cheque Date</th> <th>Cheque Amount</th> <th>Bank Name</th> <th>Payee Name</th> <th>EFT Date</th> <th>Claim Status</th></tr>	</thead><tbody> ");

            //    sbResponse.Append("<a href='../Client/claimsreport.aspx?VID=4783695' ><hr></a><br>");


            sbResponse.Append("<table><tr><td><font size=2>Please click on the <b>Claim Voucher Number</b> to view the Reimbursement Statement:</font></td><td align='right'><font size=3>الرجاء الضغط على رقم المطالبة لمشاهدة كشف حساب المطالبات</font></td><tr><table>");

            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Arial;font-color:black;" +
                                "border-collapse:collapse;font-size:11px;  ;'><thead  bgcolor='#e6f5ff'><tr>" +
                                "<th>Provider Name</th>" +
                                "<th>Voucher Number</th>" +
                                "<th>Received Date</th>" +
                                "<th>Claim Status</th>" +
                                "<th>Billed Amount</th> " +
                                "<th>Shortfall Amount</th>" +
                                "<th>Approved Amount</th>" +

                                //"<th>Date of Payment</th>" +
                //"<th>Cheque No</th>" + 
                //"<th>Cheque Date</th>"+ 
                //"<th>Cheque Amount</th>" +
                                "<th>Payment Method</th>" +
                                "<th>Payment Ref</th>" +
                                "<th>Payment Date</th>" +
                //"<th>EFT No.</th>" + 
                //"<th>EFT Date</th> " + 
                //"<th>Payee Name</th>" + 
                                  "<th>Reason</th>" +
                //"<th>Pending Reason</th>" +
                                "<th>Remarks</th>" +



                                "</tr></thead><tbody> ");
            bool _isHyperLink = false;
            int _Months = 0;
            foreach (EnqClmHisDetail_DN dtl in response.detail)
            {
                if (dtl.ProviderName.ToString() != "Out of Network")
                {
                    break;
                } 

                if (Convert.ToDateTime(dtl.ReceivedDate) <= Convert.ToDateTime("12/03/2011"))
                {
                    _isHyperLink = false;
                }
                else
                {
                    _isHyperLink = true;
                    _Months = CalculateMonthDifference(DateTime.Today, Convert.ToDateTime(dtl.ReceivedDate));
                    if (_Months >= 6)
                    {
                        _isHyperLink = false;
                    }

                }
                sbResponse.Append("<tr><td >" + dtl.ProviderName + "</td>");
                if (_isHyperLink == true)
                {
                    if (dtl.ProviderName.ToString() == "Out of Network")
                    {
                        if (dtl.ClaimsStatus != "Pending")
                        {
                            string queryString = Cryption.Encrypt("VID=" + dtl.ClaimVoucherNo.ToString().Trim() + "&EN=");
                            sbResponse.Append("<td><font color='black' ><a  style='color: Black;' href='../client/claimsreport.aspx?val=" + queryString + "'  style='cursor:hand;'>" + dtl.ClaimVoucherNo +
                                "</a></font></td>");
                        }
                        else
                        {
                            sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
                        }
                    }
                    else
                    {
                        sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
                    }
                }
                else
                {
                    if (dtl.ProviderName.ToString() == "Out of Network")
                        sbResponse.Append("<td><font color='black' ><a  style='color: Black;' href='javascript:_Alert();' style='cursor:hand;'>" +
                            dtl.ClaimVoucherNo + "</a></font></td>");
                    else
                        sbResponse.Append("<td>" + dtl.ClaimVoucherNo + "</td>");
                }
                sbResponse.Append("<td>" + String.Format("{0:d}", dtl.ReceivedDate) + "</td>");
                sbResponse.Append("<td>" + dtl.ClaimsStatus + "</td>");

                if (dtl.PresentedAmount.ToString().Contains(".") == false)
                    sbResponse.Append("<td>" + dtl.PresentedAmount + ".00</td>");
                else
                    sbResponse.Append("<td>" + dtl.PresentedAmount + "</td>");

                sbResponse.Append("<td>" + dtl.ShortfallAmt + "</td><td>" + dtl.ApprovalAmt + "</td>");





                //sbResponse.Append("<td>" + String.Format("{0:d}", dtl.PayDate) + "</td>");



                if (dtl.ChequeNo != "")
                {
                    //the cheque details "Cheque"
                    sbResponse.Append("<td>Cheque</td><td>" + dtl.ChequeNo + "</td><td>" + String.Format("{0:d}", dtl.ChequeDate) + "</td>");

                    //sbResponse.Append("<td>");
                    //if (dtl.ChequeAmount.ToString() != "-1")
                    //{
                    //    if (dtl.ChequeAmount.ToString().Contains(".") == false)
                    //        sbResponse.Append(dtl.ChequeAmount + ".00");
                    //    else
                    //        sbResponse.Append(dtl.ChequeAmount + "");
                    //}
                    //sbResponse.Append("</td>");
                }
                else
                {
                    //EFT details
                    sbResponse.Append("<td>EFT</td><td>" + dtl.EFTNO + "</td><td>" + String.Format("{0:d}", dtl.EFTDate) + "</td>");
                }


                sbResponse.Append("<td>" + dtl.RejReasonSF + "</td>");
                //sbResponse.Append("<td>" + dtl. + "</td>");
                sbResponse.Append("<td>" + dtl.Remarks + "</td></tr>");
                //sbResponse.Append("<td>" + dtl.PayeeName + "</td><td>" + dtl.ClaimsStatus + "</td><td>" + dtl.Remarks + "</td>");

                //if (dtl.detail_ProratedLimit != -1)
                //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
                //else
                //    sbResponse.Append("<td>Not Covered</td></tr>");


            }

            //sbResponse.Append("</table><table><tr><td>");
			sbResponse.Append("</table>");

            //sbResponse.Append("<font size=2>Dear Customer Please note that reimbursement claims received before <b><span  style='color:red'>March  12th, 2011 </span></b> will not have online reimbursement statement. Kindly contact our customer service advisers on 8002440307 if you need a copy of the statement.</td>");
            //sbResponse.Append("<td align='right'><font size=2>عزيزي العميل نود أن نحيطكم علما بأنه بالامكان الحصول على النسخة الكترونية من كشف حساب مطالبات الافراد النقدية المستلمة بعد تاريخ <b><span  style='color:red'>  12/3/2011 </span></b>  م وذلك عن طريق الموقع الاكتروني لبوبا العربية. في حال رغبتكم الحصول على كشوفات حساب لمطالبات قديمة قبل هذا التاريخ الرجاء الاتصال على الهاتف المجاني لخدمة العملاء لبوبا العربية 8002440307</font></td></tr></table>");

            // Fetch the member's details to check whether SABIC or not. and if it is sabic to hide cheque option
            string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SabicContractNo");
            string strSabicTollFreeNo = CoreConfiguration.Instance.GetConfigValue("SabicTollFree");
            MemberHelper mh = new MemberHelper(lblMembershipNo.Text, "");
            Member m = mh.GetMemberDetails();
            if (strConfigSabicContractNo == m.ContractNumber)
            {
                sbResponse.Replace("8002440307", strSabicTollFreeNo);
            }


            CoverageListReport.Visible = true;
            CoverageListReport.InnerHtml = sbResponse.ToString();



        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }

    
}
