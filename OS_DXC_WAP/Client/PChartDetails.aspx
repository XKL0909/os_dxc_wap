﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Client_PChartDetails" Codebehind="PChartDetails.aspx.cs" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <form id="form2" runat="server">
        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
        <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
        <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1">
            <div style="width: 100%; float: left">
                <telerik:RadHtmlChart runat="server" ID="PreAuthDetailsChart">
                    <PlotArea>
                        <XAxis Color="Teal" DataLabelsField="RECEIVED_DATE" BaseUnit="Days" Name="Date">
                            <LabelsAppearance />
                            <TitleAppearance Text="Date" />
                        </XAxis>
                        <Series>
                            <telerik:ColumnSeries Name="Approved" Stacked="false" GroupName="grouoname1" DataFieldY="ApprovedPreAuths">
                                <LabelsAppearance Visible="false" DataFormatString="{0} approved pre auths">
                                </LabelsAppearance>
                                <TooltipsAppearance Color="White" DataFormatString="{0} approved pre-Auths"></TooltipsAppearance>
                            </telerik:ColumnSeries>
                            <telerik:ColumnSeries Name="Part approved" Stacked="false" GroupName="grouoname2" DataFieldY="PartApprovedPreAuths">
                                <LabelsAppearance Visible="false" DataFormatString="{0} part approved pre auths">
                                </LabelsAppearance>
                                <TooltipsAppearance Color="White" DataFormatString="{0} part approved pre-Auths"></TooltipsAppearance>
                            </telerik:ColumnSeries>
                            <telerik:ColumnSeries Name="Rejected" Stacked="false" GroupName="grouoname3" DataFieldY="RejectedPreAuths">
                                <LabelsAppearance Visible="false" DataFormatString="{0} rejected pre auths">
                                </LabelsAppearance>
                                <TooltipsAppearance Color="White" DataFormatString="{0} rejected pre-Auths"></TooltipsAppearance>
                            </telerik:ColumnSeries>
                            <telerik:ColumnSeries Name="Cancelled" Stacked="false" GroupName="grouoname4" DataFieldY="CancelledPreAuths">
                                <LabelsAppearance Visible="false" DataFormatString="{0} cancelled pre auths">
                                </LabelsAppearance>
                                <TooltipsAppearance Color="White" DataFormatString="{0} cancelled pre-Auths"></TooltipsAppearance>
                            </telerik:ColumnSeries>
                            <telerik:ColumnSeries Name="Not required" Stacked="false" GroupName="grouoname5" DataFieldY="NotRequiredPreAuths">
                                <LabelsAppearance Visible="false" DataFormatString="{0} not required pre auths">
                                </LabelsAppearance>
                                <TooltipsAppearance Color="White" DataFormatString="{0} not required pre-Auths"></TooltipsAppearance>
                            </telerik:ColumnSeries>

                        </Series>
                    </PlotArea>
                    <ChartTitle Text="Pre Auth status by date">
                    </ChartTitle>
                </telerik:RadHtmlChart>

                <telerik:RadGrid Width="100%" ID="PreAuthDetailsGrid" runat="server" FilterMenu-Enabled="true" AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" AllowFilteringByColumn="True" ShowGroupPanel="True">
                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                        <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                    </ClientSettings>
                </telerik:RadGrid>

            </div>

        </telerik:RadAjaxPanel>
    </form>
    </div>
    </form>
</body>
</html>
