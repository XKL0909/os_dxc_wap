﻿using Caesar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services.Dto;
using UnifiedPolicy;
using UnifiedPolicy.Services;

public partial class step1id : System.Web.UI.Page
{
   private static string contractNumber;
    private long reqDetailID;

    private static UnifiedPolicy.Services.AddMemberRequestDetailService _memberService;

    public step1id(): this(new AddMemberRequestDetailService())
    {
      
    }


    public step1id(AddMemberRequestDetailService memberService)
    {
        _memberService = memberService;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtID.Focus();
        if (!IsPostBack)
        {
            InitDropDown();
        }
    }

    public void InitDropDown()
    {
        contractNumber = Convert.ToString(Session["ClientID"]);
        drplistSponsor.DataSource = CaesarManager.GetSponsor(contractNumber);
        drplistSponsor.DataValueField = "sponsorID";
        drplistSponsor.DataTextField = "sponsorID";
        drplistSponsor.DataBind();
        drplistSponsor.Items.Insert(0, new ListItem("Select Sponsor", ""));
    }

    [WebMethod(EnableSession = true)]
    private static int DeleteFiles(int id)
    {
        var FileUploadService = new AddMemberFileuploadService();
        var result = FileUploadService.Remove(id);
        return result;
    }


    [WebMethod(EnableSession = true)]
    public static int InitSession()
    {
        HttpContext.Current.Session["ReqDetailID"] = null;
        HttpContext.Current.Session["Members"] = null;
        return 1;
    }

    [WebMethod(EnableSession = true)]
    public static long AddMemberDetails(AddMemberRequestDetail member)
    {
        long result = 0;

        if (HttpContext.Current.Session["ReqDetailID"] == null)
        {
            member.Status = 1;
            member.IdType = member.IdNumber.Trim().Substring(0, 1);
            member.ContNumber = contractNumber;
            result = _memberService.Add(member);

            if (result > 0)
            {
                HttpContext.Current.Session.Add("ReqDetailID", result);
                return result;
            }
        }
        else
        {
            result = long.Parse(HttpContext.Current.Session["ReqDetailID"].ToString());
            return result;
        }
        return result;
    }
   
}

