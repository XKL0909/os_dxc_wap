<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master"  AutoEventWireup="true" Inherits="DeleteEmployee" Codebehind="DeleteEmployee.aspx.cs" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <script type='text/JavaScript' src='scw.js'></script>
    <style type="text/css" media="screen">
        .DatePicker
        {
            background: url(images/cal.gif) no-repeat right center;
        }
        DIV.DatePicker-Container
        {
            font-size: 9px;
        }
        DIV.DatePicker-Wrapper
        {
            border-right: #e0e0e0 1px solid;
            border-top: #e0e0e0 1px solid;
            border-left: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
            background-color: skyblue;
        }
        DIV.DatePicker-Wrapper TABLE
        {
        }
        DIV.DatePicker-Wrapper TD
        {
            padding-right: 0px;
            padding-left: 0px;
            padding-bottom: 0px;
            padding-top: 0px;
        }
        DIV.DatePicker-Wrapper TD.nav
        {
        }
        DIV.DatePicker-Wrapper TD.dayName
        {
            border-right: #e0e0e0 1px solid;
            font-weight: bold;
            border-bottom: #e0e0e0 1px solid;
            background-color: #eeeeee;
        }
        DIV.DatePicker-Wrapper TD.day
        {
            border-right: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
        }
        DIV.DatePicker-Wrapper TD.empty
        {
            border-right: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
        }
        DIV.DatePicker-Wrapper TD.current
        {
            color: #ffffff;
            background-color: #666666;
        }
        DIV.DatePicker-Wrapper TD.dp_roll
        {
            background-color: #e0e0e0;
        }
    </style>
    <script src="../includes/MooTools.c.js" type="text/javascript"></script>
    <script src="../includes/DatePicker.js" type="text/javascript"></script>
    

    <script type='text/JavaScript'>
        $(function () {

            $('#<%= Button1.ClientID %>').click(function () {
                $.blockUI({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
            });
        });
    </script>
    <table width="95%">
        <tr valign="baseline">
            <td>
                <asp:Label ID="lblAdd_Employee" runat="server" Text="Delete Employee" Font-Size="Large"
                    Font-Names="verdana"></asp:Label>
            </td>
            <td style="text-align: right">
                <img src="Logo.gif" width="168" height="50" />
            </td>
        </tr>
    </table>
    <div>
        <br />
        <a href="<%=Page.ResolveUrl("~/DefaultClient3-1.aspx") %>" style="font-size:small">Back</a>
        <br />
        <br />
        <asp:HyperLink ID="lnkBatchUpload" runat="server" Font-Size="Small" NavigateUrl="~/Client/BatchUpload.aspx?OptionType=DeleteEmployee"
            Visible="true">Batch Upload</asp:HyperLink>
        <br />
        <br />
        <asp:Label ID="Message1" runat="server" Width="527px" Font-Size="Small"></asp:Label><br />
        <asp:Label ID="Label1" runat="server" Font-Size="12px" Font-Names="Arial" ForeColor="Red" Text="Fields marked with asterisk [ * ]  are mandatory."></asp:Label>

<%if (uploader.LblUpload.Text.Length > 0)
  { %>
        <br />
        <br />
        <table style="width: 750px; font-size: 17px; font-family: Arial; border-right: black 0.5pt solid;
            border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid;
            border-collapse: collapse;" runat="server" id="Table3">
            <tr>
                <td align="left">
                    <span style="color: #cc0000"><%= uploader.LblUpload.Text%></span>
                </td>
            </tr>
        </table>
        <br />
        <br />
<% }%>

        <asp:Panel ID="Panel1" runat="server" Width="95%">
            <br />
            <table style="font-size: small;" width="90%" >
                <tr>
                    <td style="width: 216px; height: 16px;">
                        <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="lblCompleteName"
                            runat="server" Text="Complete Name(First, Middle, Last)" Width="206px"></asp:Label>
                    </td>
                    <td style="width: 200px; height: 16px;">
                        <asp:TextBox CssClass="textbox" ID="txtMemberName" runat="server" Width="271px" MaxLength="40"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMemberName"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtMemberName"
                            Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                            ValidationExpression="[a-zA-Z ]+"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 206px">
                        <span style="color: #ff0000">*</span><asp:Label ID="lblEmployeeNo" runat="server"
                            Text="Membership Number" Width="145px"></asp:Label>
                    </td>
                    <td style="width: 200px">
                        <asp:TextBox CssClass="textbox" ID="txtMembershipNo" runat="server" Width="271px" MaxLength="7"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMembershipNo"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtMembershipNo"
                            Display="Dynamic" ValidationExpression="[3|4|5|6|7|8|9][0-9][0-9][0-9][0-9][0-9][0-9]"
                            ErrorMessage="RegularExpressionValidator">Incorrect Membership No.</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 206px; height: 25px;">
                        <span style="color: #ff0000">*</span><asp:Label ID="lblGender" runat="server" Text="Reason for Deletion"
                            Width="148px"></asp:Label>
                    </td>
                    <td style="width: 200px; height: 25px;">
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlDelReason" runat="server" Width="289px" BackColor="#FFFFC0">
                            <asp:ListItem>-Select-</asp:ListItem>
                            <asp:ListItem Value="DEL001">Member decided to leave KSA (Exit visa only)</asp:ListItem>
                            <asp:ListItem Value="DEL002">Member resigned from the company</asp:ListItem>
                            <asp:ListItem Value="DEL003">Member was terminated from work / company</asp:ListItem>
                            <asp:ListItem Value="DEL004">Member moved to another insurer</asp:ListItem>
                            <asp:ListItem Value="DEL005">Member moved to another sponsor</asp:ListItem>
                            <asp:ListItem Value="DEL006">Death of member</asp:ListItem>
                            <asp:ListItem Value="DEL007">Wrong enrolment</asp:ListItem>
                            <asp:ListItem Value="DEL008">Dependent Left KSA</asp:ListItem>
                            <asp:ListItem Value="DEL009">Dependent passed away</asp:ListItem>
                            <asp:ListItem Value="DEL010">Dependent moved to another insurer</asp:ListItem>
                            <asp:ListItem Value="DEL011">Dependent moved to another sponsor</asp:ListItem>
                            <asp:ListItem Value="DEL012">Divorced the partner</asp:ListItem>
                        </asp:DropDownList></div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlDelReason"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="-Select-"
                            Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 206px">
                        <span style="color: #ff0000">*</span><asp:Label ID="lblDateOfBirth" runat="server"
                            Text="Effective Date" Width="100px"></asp:Label>
                    </td>
                    <td style="width: 200px">
                        <asp:TextBox CssClass="textbox" ID="txtDateOfDeletion" Style="cursor: default;" runat="server" Width="271px"
                            onclick="scwShow(this,event);" onfocus="this.blur();"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDateOfDeletion"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDateOfDeletion"
                            Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\d{2}/\d{2}/\d{4}">Invalid Date</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 206px">
                    </td>
                    <td style="width: 100px">
                        <div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 206px">
                    </td>
                    <td style="width: 290px" align="right">
                    </td>
                </tr>
            </table>
            <br />
            <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid;
                border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid;
                border-collapse: collapse;" runat="server" id="tblUploader">
                <tr>
                    <td align="left">
                        <span style="color: #ff3300"><strong>*</strong></span><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                    </td>
                    <td align="right">
                        <a href="<%= Page.ResolveUrl("~/Docs/ReqDocs.xps") %>" target="_blank">Required Document List</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .tiff, .rtf, .png, .pmp) 2MB limit per file" /><br />
                        <bupa:uploader runat="server" id="uploader" />
                    </td>
                </tr>
            </table>
            <p align="right">
                <asp:Button CssClass="submitButton" ID="Button1" runat="server" OnClick="Button1_Click" Text="Delete Member" Width="133px" BackColor="Control" />
            </p>
        </asp:Panel>
    </div>
    
</asp:Content>
