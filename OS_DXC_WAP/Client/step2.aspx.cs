﻿using BO;
using Caesar;
using CaesarApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Bupa.Models.Enums;

public partial class step2 : System.Web.UI.Page
{

    private string requestTypeToD;
    private string contractNumber;
    private static bool iSYakeenValidation;
    private AddMemberRequestDetailService _memberService;
    private string requesrTYpe;
    public string ContNumber;
    private string conEffdate;
    private string MAinEmpStartDate;

    private string genderType, disableScheme, requestType, dependentRegular;
    private int socialStatus = 0;
    private static bool isMainMemberSenior = false;



    public step2() : this(new AddMemberRequestDetailService()) { }
    public step2(AddMemberRequestDetailService memberService)
    {
        _memberService = memberService;
    }
    public void enableDisableValidation()
    {
        ////regValtxtStartOfPolicyREange.Enabled = false;

        REVBabayAge.Enabled = false;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            hdnIsRM.Value = Convert.ToString(HttpContext.Current.Session["IsRM"]);
            hdnIsRM.Value = "Y";
            HiddenComType.Value = Convert.ToString(HttpContext.Current.Session["ContractType"]);
            Regularexpressionvalidator1.Enabled = false;
            enableDisableValidation();
            contractNumber = Convert.ToString(Session["ClientID"]);
            conEffdate = Convert.ToString(Session["ContractEndDate"]);
            initDropDown();
            GetYakeenInfo();
            hdnSME.Value = IsSme() ? "SME" : "CORP";
            Session["RequestType"] = requestType;

            ////Added by Sakthi on 24-May-2018 SME contract Disable Regular and (S) scheme 
            //// Start      
            if (IsSme() && (requestType == "ED" || requestType == "E"))
            {
                string flag = string.Empty;
                if (requestType == "ED")
                {
                    if (genderType == "M" || (genderType == "F" && dependentRegular == "Y"))
                        flag = "R";
                    else if (genderType == "F" && dependentRegular == "N")
                        flag = "S";
                }
                else if (requestType == "E")
                {
                    if (genderType == "M" || (genderType == "F" && socialStatus == 2))
                        flag = "R";
                    else if (genderType == "F" && (socialStatus == 1 || socialStatus == 2 || socialStatus == 3 || socialStatus == 4))
                        flag = "S";
                }

                if (flag == "R")
                    drpCover.Items.Cast<ListItem>().Where(item => item.Text.Contains("(S)")).ToList().ForEach(drpCover.Items.Remove);
                else if (flag == "S")
                    drpCover.Items.Cast<ListItem>().Where(item => !item.Text.Contains("(S)")).ToList().ForEach(drpCover.Items.Remove);

                if (!string.IsNullOrEmpty(txtDOB.Text) && GeneralHelper.GetAgeFromDob(Convert.ToDateTime(txtDOB.Text)) >= 65)
                {
                    drpCover.Items.Cast<ListItem>().Where(item => !item.Text.ToLower().Contains("senior")).ToList().ForEach(drpCover.Items.Remove);
                    isMainMemberSenior = true;

                }
                else
                {
                    isMainMemberSenior = false;
                    drpCover.Items.Cast<ListItem>().Where(item => item.Text.ToLower().Contains("senior")).ToList().ForEach(drpCover.Items.Remove);
                }
                if (drpCover.Items != null && drpCover.Items.Count > 0)
                {
                    if (drpCover.Items[0].Text.Trim().ToLower() != "select scheme")
                        drpCover.Items.Insert(0, new ListItem("Select Scheme", ""));
                }
                else
                    drpCover.Items.Insert(0, new ListItem("Select Scheme", ""));
            }

            drpCover.Items.Cast<ListItem>().Where(item => item.Text.ToLower().Contains("dummy")).ToList().ForEach(drpCover.Items.Remove);

            if (!(string.IsNullOrEmpty(conEffdate)))
            {
                contrctEffdate(conEffdate);
            }

            hideShowFields();

            Session["MemberSatrtDate"] = hdnpolicyStartDate.Value;
            Session["NewBornStep2"] = hdnNEwBornStep2.Value;
        }
        catch (Exception ex)
        {
            SetDisableControlIfErrorOcuurs(ex.Message);
        }

    }
    protected void hideShowFields()
    {

        if (drpTitle.SelectedValue == null)
        {
            drpTitle.SelectedValue = "";
        }
        if (drpGender.SelectedValue == null || drpGender.SelectedValue == "")
        {
            DivGender.Visible = true;
        }
        else
        {
            if (drpGender.SelectedValue == "F")
            {
                HiddendrpGender.Value = "F";
                hdnGender.Value = "female";
            }
            if (drpGender.SelectedValue == "M")
            {
                HiddendrpGender.Value = "M";
                hdnGender.Value = "male";
            }

            DivGender.Visible = false;
        }
        if (string.IsNullOrEmpty(txtDOB.Text))
        {
            //DivDOB.Visible = true;
        }
        else
        {
            HiddentxtDOB.Value = txtDOB.Text;
            //DivDOB.Visible = false;
        }

        if (drpProfession.SelectedValue == null || drpProfession.SelectedValue == "")
        {
            if (requestTypeToD != "D")
            {
                // Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "hideShowPro('block')", true);
                HiddendrpProfessionVAlue.Value = "block";
            }

            else
            {
                HiddendrpProfessionVAlue.Value = "none";
                // Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "hideShowPro('none')", true);
            }

        }
        else
        {
            HiddendrpProfession.Value = drpProfession.SelectedValue;
            DivPRofession.Visible = false;
        }
        if (drpNationality.SelectedValue == null || drpNationality.SelectedValue == "")
        {
            DivNAtionalty.Visible = true;
        }
        else
        {
            HiddendrpNationality.Value = drpNationality.SelectedValue;
            DivNAtionalty.Visible = false;
        }
        if (hdnNEwBornStep2.Value == "false" || hdnNEwBornStep2.Value == "")
        {
            if (string.IsNullOrEmpty(txtExpDate.Text))
            {
                DivIDExpaiary.Visible = true;

            }
            else
            {
                HiddentxtExpDate.Value = txtExpDate.Text;
                DivIDExpaiary.Visible = false;
            }
        }
        else
        {
            RequiredFieldValidator1.Enabled = false;
            reqValtxtEmpNumber.Enabled = false;
            reqValtxtDepart.Enabled = false;
            reqRegExValtxtDepart.Enabled = false;
            DivIDExpaiary.Visible = false;
        }




    }
    protected void contrctEffdate(string date)
    {
        string[] valuesp = splitDate(date);
        hdncaesarContractPolicyEndDate.Value = valuesp[0] + "-" + valuesp[1] + "-" + valuesp[2];
        int yearmun = Convert.ToInt32(valuesp[2]);
        yearmun = yearmun - 1;
        hdncaesarContractPolicyStartDate.Value = valuesp[0] + "-" + valuesp[1] + "-" + yearmun.ToString();
    }
    protected void PolicyDate(string date)
    {
        string[] valuesp = splitDate(date);
        hdnpolicyStartDate.Value = valuesp[0] + "-" + valuesp[1] + "-" + valuesp[2];
    }

    private void initDropDown()
    {
        drpCover.DataSource = CaesarManager.GetClass(contractNumber);
        drpCover.DataValueField = "classID";
        drpCover.DataTextField = "className";
        drpCover.DataBind();
        drpCover.Items.Insert(0, new ListItem("Select Scheme", ""));





        BrhListDetail_DN[] rowsList = CaesarManager.GetBranch(contractNumber);
        if (rowsList.Length == 1)
        {
            drpBranchCode.DataSource = rowsList;
            drpBranchCode.DataValueField = "branchCode";
            drpBranchCode.DataTextField = "branchDesc";
            drpBranchCode.DataBind();
            drpBranchCode.SelectedValue = rowsList[0].branchCode;
            drpBranchCode.Enabled = false;
        }
        else
        {
            drpBranchCode.DataSource = rowsList;
            drpBranchCode.DataValueField = "branchCode";
            drpBranchCode.DataTextField = "branchDesc";
            drpBranchCode.DataBind();
            drpBranchCode.Items.Insert(0, new ListItem("Select Branch Code", ""));
        }
        drpDistrict.DataSource = CaesarManager.GetDistrict();
        drpDistrict.DataValueField = "distCode";
        drpDistrict.DataTextField = "distName";
        drpDistrict.DataBind();
        drpDistrict.Items.Insert(0, new ListItem("Select District", ""));
        drpProfession.DataSource = CaesarManager.GetProfession();
        drpProfession.DataValueField = "profCode";
        drpProfession.DataTextField = "profName";
        drpProfession.DataBind();
        drpProfession.Items.Insert(0, new ListItem("Select Profession", ""));
        drpNationality.DataSource = NationalityManager.Get();
        drpNationality.DataValueField = "MappedCode";
        drpNationality.DataTextField = "Nationality";
        drpNationality.DataBind();
        drpNationality.Items.Insert(0, new ListItem("Select Nationality", ""));
    }
    private void GetYakeenInfo()
    {
        AddMemberRequestDetail member;
        bool issaudi = false;
        bool isSenior = false;

        try
        {
            ResetSessionIncaseOfModification();
            if (HttpContext.Current.Session["AddMember"] != null || Request.QueryString["reqDetId"] != null)
            {

                member = (HttpContext.Current.Session["AddMember"] != null) ? (AddMemberRequestDetail)HttpContext.Current.Session["AddMember"] : _memberService.FindByID(long.Parse(Request.QueryString["reqDetId"].ToString()));

                if (member.RequestType == "ID")
                {
                    hdnSubReqType.Value = "D";
                    requestType = "D";
                }

                List<YakeenDetail> yakeenDetail = new List<YakeenDetail>();

                if (member != null)
                {

                    requestTypeToD = member.RequestType.Trim();
                    hdnRequestType.Value = member.RequestType.Trim();
                    if (member.RequestType.Trim() == "E")
                    {
                        requestType = "E";

                        if (string.IsNullOrEmpty(member.HijriDOBYear.ToString()))
                        {
                            yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.Sponsor);
                        }
                        else
                            yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.HijriDOBYear.ToString(), 0, 0);
                        if (yakeenDetail != null)
                        {
                            BindMaritalStatus(member.IdNumber);
                            SetMaritalStatus(yakeenDetail);
                        }
                    }


                    //with membership

                    if (member.RequestType.Trim() == "D")
                    {
                        requestType = "D";
                        requesrTYpe = "D";
                        string comtype = Convert.ToString(HttpContext.Current.Session["ContractType"]);
                        hdnContractNumber.Value = HttpContext.Current.Session["ClientID"].ToString();
                        AddMemberRequestDetailService service = new AddMemberRequestDetailService();
                        bool foundMEmber = false;
                        if (member.NewBorn == null || member.NewBorn == false)
                        {

                            if (string.IsNullOrEmpty(member.HijriDOBYear.ToString()))
                            {
                                if (!(string.IsNullOrEmpty((member.MbrshipNo))))
                                {
                                    foundMEmber = true;
                                    OnlineServices.CCHI.Dto.Member mem = GetMemberShipInfoNew(member.MbrshipNo, contractNumber);
                                    if (mem.Status != "Failed")
                                    {
                                        member.SecSponsorId = mem.IDCardNo;
                                        yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.SecSponsorId, 0, 0);
                                        if (yakeenDetail != null)
                                        {
                                            txtDOB.Text = yakeenDetail[0].DOB != null ? Convert.ToDateTime(yakeenDetail[0].DOB).ToString("dd-MM-yyyy") : string.Empty;
                                            isSenior = IsSenior();
                                        }
                                        member.Sponsor = setCseaerINformation(mem);
                                        hdnMAinEMpCoverType.Value = mem.ClassID;


                                    }
                                    else
                                    {

                                        SetDisableControlIfErrorOcuurs("Invalid Membership ID");
                                        return;
                                    }

                                }
                                else if (!(string.IsNullOrEmpty(member.SecSponsorId)) && foundMEmber == false)
                                {
                                    AddMemberRequestDetail OBj = new AddMemberRequestDetail();
                                    yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.SecSponsorId, 1, 0);
                                    if (yakeenDetail != null)
                                    {
                                        txtDOB.Text = yakeenDetail[0].DOB != null ? Convert.ToDateTime(yakeenDetail[0].DOB).ToString("dd-MM-yyyy") : string.Empty;
                                        isSenior = IsSenior();
                                    }

                                    hdnMAinEMpCoverType.Value = Convert.ToString(service.GetCoverOFMainEMployeeID("E", member.RequestId, member.SecSponsorId));
                                    OBj = getSopncerID(member.RequestId.ToString(), member.SecSponsorId);
                                    if (OBj != null)
                                    {
                                        member.Sponsor = OBj.Sponsor;
                                        setinformationFOrmDetils(OBj, (bool)member.NewBorn);
                                    }
                                    string dfdf = comtype.Trim().ToUpper();
                                    if (comtype.Trim().ToUpper().Equals("BDRC") || comtype.Trim().ToUpper().Equals("BDSC"))
                                    {

                                        ////Enabling dependent scheme for  sme on 18-Jul-2018 on sakthi
                                        //// Start                                        
                                        EnablingSMEDependentScheme(hdnContractNumber.Value, hdnMAinEMpCoverType.Value);
                                        ////old code start 
                                        //drpCover.SelectedValue = drpMemberTypeSelectedIndexChanged("", hdnContractNumber.Value, hdnMAinEMpCoverType.Value, "D");                                       
                                        //drpCover.Enabled = false;
                                        //// old code end
                                        ////End


                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(hdnMAinEMpCoverType.Value))
                                            drpCover.Enabled = true;
                                        else
                                        {
                                            drpCover.SelectedValue = hdnMAinEMpCoverType.Value;
                                            drpCover.Enabled = false;
                                        }
                                    }
                                    //drpCover.SelectedValue = hdnMAinEMpCoverType.Value;
                                    //drpCover.Enabled = false;
                                    List<Dropdown> memberList = GetMemberType(drpCover.SelectedValue, "D", DateTime.Now, null, "");
                                    drpMemberType.Items.Clear();
                                    foreach (Dropdown item in memberList)
                                    {
                                        if (isSenior)
                                        {
                                            if (item.Value.Trim().ToUpper() != "SO" && item.Value.Trim().ToUpper() != "D" && item.Value.Trim().ToUpper() != "C")
                                                drpMemberType.Items.Add(new ListItem(item.Text, item.Value));
                                        }
                                        else
                                            drpMemberType.Items.Add(new ListItem(item.Text, item.Value));
                                    }
                                    drpMemberType.Items.Insert(0, new ListItem("Select MemberType", ""));


                                }

                                else
                                {
                                    SetDisableControlIfErrorOcuurs(yakeenDetail[0].ExceptionMessage);
                                    return;

                                }
                            }
                            else
                            {
                                bool memberShip = false;
                                if (!(string.IsNullOrEmpty((member.MbrshipNo))))
                                {
                                    memberShip = true;
                                    OnlineServices.CCHI.Dto.Member mem = GetMemberShipInfoNew(member.MbrshipNo, contractNumber);
                                    if (mem.Status != "Failed")
                                    {
                                        member.SecSponsorId = mem.IDCardNo;
                                        yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.HijriDOBYear.ToString(), 0, 0);
                                        if (yakeenDetail != null)
                                        {
                                            txtDOB.Text = yakeenDetail[0].DOB != null ? Convert.ToDateTime(yakeenDetail[0].DOB).ToString("dd-MM-yyyy") : string.Empty;
                                            isSenior = IsSenior();
                                        }
                                        member.Sponsor = setCseaerINformation(mem);
                                    }
                                    else
                                    {
                                        SetDisableControlIfErrorOcuurs("Invalid Membership ID");
                                        return;
                                    }
                                }
                                else if (!(string.IsNullOrEmpty((member.SecSponsorId))) && memberShip == false)
                                {
                                    yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.HijriDOBYear.ToString(), 0, 0);
                                    if (yakeenDetail != null)
                                    {
                                        txtDOB.Text = yakeenDetail[0].DOB != null ? Convert.ToDateTime(yakeenDetail[0].DOB).ToString("dd-MM-yyyy") : string.Empty;
                                        isSenior = IsSenior();
                                    }
                                    hdnMAinEMpCoverType.Value = Convert.ToString(service.GetCoverOFMainEMployeeID("E", member.RequestId, member.SecSponsorId));
                                    if (comtype.Trim().ToUpper().Equals("BDRC") || comtype.Trim().ToUpper().Equals("BDSC"))
                                    {
                                        ////Enabling dependent scheme for  sme on 18-Jul-2018 on sakthi
                                        //// Start   
                                        EnablingSMEDependentScheme(hdnContractNumber.Value, hdnMAinEMpCoverType.Value);
                                        //old code start                                   
                                        //drpCover.SelectedValue = drpMemberTypeSelectedIndexChanged("", hdnContractNumber.Value, hdnMAinEMpCoverType.Value, "D");
                                        ////hdnMAinEMpCoverType.Value = drpCover.SelectedValue;
                                        //drpCover.Enabled = false;
                                        //old code end
                                        ////End
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(hdnMAinEMpCoverType.Value))
                                            drpCover.Enabled = true;
                                        else
                                        {
                                            drpCover.SelectedValue = hdnMAinEMpCoverType.Value;
                                            drpCover.Enabled = false;
                                        }

                                    }
                                    //drpCover.SelectedValue = hdnMAinEMpCoverType.Value;
                                    //drpCover.Enabled = false;
                                    AddMemberRequestDetail OBj = new AddMemberRequestDetail();
                                    OBj = getSopncerID(member.RequestId.ToString(), member.SecSponsorId);
                                    if (OBj != null)
                                    {
                                        member.Sponsor = OBj.Sponsor;
                                        setinformationFOrmDetils(OBj, (bool)member.NewBorn);
                                    }
                                    List<Dropdown> memberList = GetMemberType(drpCover.SelectedValue, "D", DateTime.Now, null, "");
                                    drpMemberType.Items.Clear();
                                    foreach (Dropdown item in memberList)
                                    {
                                        if (isSenior)
                                        {
                                            if (item.Value.Trim().ToUpper() != "SO" && item.Value.Trim().ToUpper() != "D" && item.Value.Trim().ToUpper() != "C")
                                                drpMemberType.Items.Add(new ListItem(item.Text, item.Value));
                                        }
                                        else
                                            drpMemberType.Items.Add(new ListItem(item.Text, item.Value));
                                    }
                                    drpMemberType.Items.Insert(0, new ListItem("Select MemberType", ""));

                                }
                                else
                                {
                                    SetDisableControlIfErrorOcuurs(yakeenDetail[0].ExceptionMessage);
                                    return;
                                }

                            }

                            if (yakeenDetail != null)
                            {
                                BindMaritalStatus(member.IdNumber);
                                SetMaritalStatus(yakeenDetail);
                            }


                        }
                        else //new born baby 
                        {
                            hdnNEwBornStep2.Value = "true";

                            BindMaritalStatus("NA");

                            AddMemberRequestDetailService newObj = new AddMemberRequestDetailService();

                            OnlineServices.CCHI.Dto.Member mem = null;
                            if (!(string.IsNullOrEmpty(member.MbrshipNo)))
                            {

                                mem = GetMemberShipInfoNew(member.MbrshipNo, contractNumber);
                                if (mem.Status != "Failed")
                                {
                                    member.SecSponsorId = mem.IDCardNo;
                                    member.Sponsor = setCseaerINformation(mem);

                                }
                                else
                                {

                                    SetDisableControlIfErrorOcuurs("Invalid Membership ID");
                                    return;
                                }


                            }
                            else if (!string.IsNullOrEmpty(member.SecSponsorId))
                            {
                                HttpContext.Current.Session["SecSponsorId"] = member.SecSponsorId;
                                hdnMAinEMpCoverType.Value = Convert.ToString((service.GetCoverOFMainEMployeeID("E", member.RequestId, member.SecSponsorId)));

                                if (comtype.Trim().ToUpper().Equals("BDRC") || comtype.Trim().ToUpper().Equals("BDSC"))
                                {

                                    ////Enabling dependent scheme for  sme on 18-Jul-2018 on sakthi
                                    //// Start   
                                    EnablingSMEDependentScheme(hdnContractNumber.Value, hdnMAinEMpCoverType.Value);
                                    //old code start    
                                    //drpCover.SelectedValue = drpMemberTypeSelectedIndexChanged("", hdnContractNumber.Value, hdnMAinEMpCoverType.Value, "D");
                                    ////hdnMAinEMpCoverType.Value = drpCover.SelectedValue;
                                    //drpCover.Enabled = false;
                                    //old code end
                                    ////End
                                }
                                else
                                {
                                    drpCover.SelectedValue = hdnMAinEMpCoverType.Value;
                                    drpCover.Enabled = false;

                                }
                                //drpCover.SelectedValue = hdnMAinEMpCoverType.Value;
                                //drpCover.Enabled = false;
                                AddMemberRequestDetail OBj = new AddMemberRequestDetail();
                                OBj = getSopncerID(member.RequestId.ToString(), member.SecSponsorId);
                                if (OBj != null)
                                {
                                    member.Sponsor = OBj.Sponsor;
                                    setinformationFOrmDetils(OBj, (bool)member.NewBorn);
                                }
                                List<Dropdown> memberList = GetMemberType(drpCover.SelectedValue, "D", DateTime.Now, null, "");
                                drpMemberType.Items.Clear();
                                foreach (Dropdown item in memberList)
                                {
                                    drpMemberType.Items.Add(new ListItem(item.Text, item.Value));
                                }
                                drpMemberType.Items.Insert(0, new ListItem("Select MemberType", ""));


                            }

                        }
                    }

                    #region Deprectaed
                    if (member.RequestType.Trim() == "ID")
                    {
                        //SecSponsorId mandatory
                        if (string.IsNullOrEmpty(member.HijriDOBYear.ToString()))
                        {
                            yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.SecSponsorId, 0, 0);
                            SetExistingMemberInfoForDependent(member.Sponsor, member.RequestId);
                        }
                        else
                        {
                            yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.HijriDOBYear.ToString(), 0, 0);
                            SetExistingMemberInfoForDependent(member.Sponsor, member.RequestId);
                        }

                        if (yakeenDetail != null)
                        {
                            BindMaritalStatus(member.IdNumber);
                            SetMaritalStatus(yakeenDetail);
                        }
                    }
                    #endregion

                    #region Family

                    if (member.RequestType.Trim() == "ED")
                    {
                        requestType = "ED";
                        if (!string.IsNullOrEmpty(member.HijriDOBYear.ToString()) && !string.IsNullOrEmpty(member.DepHijriDOBYear.ToString()))
                        {
                            yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.HijriDOBYear.ToString(), (long)member.DepHijriDOBYear);
                            issaudi = true;
                        }
                        else
                        {

                            yakeenDetail = YakeenManager.GetMembersDetailsFromYakeen(member.IdNumber, member.Sponsor, (long)member.DepIqamaNo);
                            issaudi = false;
                        }

                        if (yakeenDetail != null)
                        {
                            BindMaritalStatus(member.IdNumber);
                            SetMaritalStatus(yakeenDetail);
                        }
                    }
                    #endregion

                }
                if (member.NewBorn == null || member.NewBorn == false)
                {
                    if (yakeenDetail != null && string.IsNullOrEmpty(yakeenDetail[0].ExceptionMessage))
                    {
                        iSYakeenValidation = true;
                        //when yakeen is loading the information then inserting the request to database with status 1
                        if (HttpContext.Current.Session["AddMember"] != null)
                        {
                            if (member.RequestType.Trim() != "ED")
                                AddMemberDetails(member);
                        }
                        else
                        {
                            var reqDetId = member.Id;
                            HttpContext.Current.Session["ReqDetailID"] = reqDetId;
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN1", "setReqDetail(" + reqDetId + ");", true);
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN2", "disableStep(" + 1 + ");", true);
                        }

                        if (member.RequestType.Trim() == "ED")
                        {

                            PrepareDataForFamily(yakeenDetail, member, issaudi);

                        }
                        else
                        {
                            divMemberGrid.Visible = false;
                        }
                        SetFiledsByYakeenInfo(yakeenDetail, member);
                    }
                    else
                    {
                        SetDisableControlIfErrorOcuurs(yakeenDetail[0].ExceptionMessage);
                        return;
                    }
                }
                else
                {
                    addMemberDetailsForNewBorn(member);
                    SetProfessionForBaby(member.NewBorn);
                }

            }
            else
            {
                SetDisableControlIfErrorOcuurs("Empty session");
                return;
            }
        }
        catch (Exception ex)
        {
            SetDisableControlIfErrorOcuurs(ex.Message);
        }
    }
    public AddMemberRequestDetail getSopncerID(string REquestNo, string MianEmpID)
    {
        AddMemberRequestDetail reslt = new AddMemberRequestDetail();
        AddMemberRequestDetailService serv = new AddMemberRequestDetailService();
        int outt = 0;
        int.TryParse(REquestNo, out outt);
        if (outt != 0)
        {
            reslt = serv.FindByIDNumber(MianEmpID, outt);

        }

        return reslt;
    }
    public void addMemberDetailsForNewBorn(AddMemberRequestDetail member)
    {
        iSYakeenValidation = false;
        //when yakeen is loading the information then inserting the request to database with status 1
        if (HttpContext.Current.Session["AddMember"] != null)
        {
            AddMemberDetails(member);
        }
        else
        {
            var reqDetId = member.Id;
            HttpContext.Current.Session["ReqDetailID"] = reqDetId;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN1", "setReqDetail(" + reqDetId + ");", true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN2", "disableStep(" + 1 + ");", true);
        }
    }
    public void ResetSessionIncaseOfModification()
    {
        if (Request.QueryString["reqDetId"] != null)
        {
            HttpContext.Current.Session["AddMember"] = null;
        }
    }
    public void PrepareDataForFamily(List<YakeenDetail> yakeenDetail, AddMemberRequestDetail member, bool isSUdi)
    {
        try
        {
            hdnSubReqType.Value = "E";

            int i = 0;
            List<AddMemberRequestDetail> members = new List<AddMemberRequestDetail>();
            int drpvalue = 0;
            int misNameCount = 0;
            int misGenderCount = 0;
            int misDobCount = 0;
            int misMemTypeCount = 0;
            HiddenMaternity.Value = "0";
            HiddenEmployeeDepStatus.Value = "0";
            HiddenDobCount.Value = "0";
            HiddenGenderCount.Value = "0";
            HiddenNameCount.Value = "0";
            HiddenMemTypeCount.Value = "0";

            if (yakeenDetail != null)
            {
                var marital = yakeenDetail.Where(item => item.Relationship == "S" || item.Relationship == "SO" || item.Relationship == "D" || item.Relationship == "C").ToList();
                dependentRegular = marital != null && marital.Count > 0 ? "Y" : "N";
            }
            foreach (var item in yakeenDetail)
            {
                var mbr = new AddMemberRequestDetail();
                string occupationCode = "";
                mbr.RequestId = member.RequestId;
                mbr.IdNumber = (i == 0) ? member.IdNumber : item.IDNumber;
                mbr.Sponsor = (i == 0) ? member.Sponsor : member.Sponsor;
                mbr.MemberType = (i == 0) ? null : item.Relationship;
                mbr.Name = GeneralHelper.ArabicToEnglish(item.ArabicFullName, item.EnglishFullName);
                mbr.RequestType = "ED";
                mbr.DOB = Convert.ToDateTime(item.DOB).ToString("dd-MM-yyyy");
                mbr.IDExpiry = !string.IsNullOrEmpty(item.IdExpiryDate) ? item.IdExpiryDate : item.VisaExpiryDate;
                //mbr.Gender = (item.Gender == Gender.Male && item.Gender != Gender.Unspecified) ? "M" : "F";
                mbr.Gender = (item.Gender == Gender.Male && item.Gender != Gender.Unspecified) ? "M" : (item.Gender == Gender.Female && item.Gender != Gender.Unspecified) ? "F" : ""; ;
                if (i == 0)
                {
                    genderType = mbr.Gender;
                    ////seniorAge = GeneralHelper.GetAgeFromDob(Convert.ToDateTime(mbr.DOB));
                }
                if (!string.IsNullOrEmpty(Convert.ToDateTime(item.DOB).ToString()) && mbr.Gender == "F")
                {
                    var age = GeneralHelper.GetAgeFromDob(Convert.ToDateTime(item.DOB));
                    if (age > 45 && mbr.MemberType == "S")
                    {
                        mbr.Visibility = "display:inline";
                        drpvalue += 1;
                        HiddenMaternity.Value = "" + drpvalue + "";
                    }
                    else if (age <= 45 && mbr.MemberType == "S")
                    {
                        mbr.Visibility = "display:none";
                        drpvalue += 1;
                        HiddenMaternity.Value = "Covered,";
                    }
                    else
                        mbr.Visibility = "display:none";
                }
                else
                    mbr.Visibility = "display:none";

                string[] MTypeList = Convert.ToString("D,SO,S,C").Split(',');
                if (MTypeList.Contains(mbr.MemberType))
                {
                    HiddenEmployeeDepStatus.Value = "1";
                }

                //&& !string.IsNullOrEmpty(mbr.IDExpiry)
                if (i == 0)
                {
                    occupationCode = item.OccupationCode;
                    if (isSUdi == true)
                        members.Add(mbr);
                }

                if (i > 0)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(mbr.DOB)) || item.DOB <= DateTime.MinValue)
                    {
                        mbr.DOBVisibility = "display:inline";
                        mbr.AltDOBVisibility = "display:none";
                        misDobCount += 1;
                        HiddenDobCount.Value = "" + misDobCount + "";
                    }
                    else
                    {
                        mbr.DOBVisibility = "display:none";
                        mbr.AltDOBVisibility = "display:inline";
                    }
                    if (string.IsNullOrEmpty(mbr.Gender) || item.Gender == Gender.Unspecified)
                    {
                        mbr.GenderVisibility = "display:inline";
                        mbr.AltGenderVisibility = "display:none";
                        misGenderCount += 1;
                        HiddenGenderCount.Value = "" + misGenderCount + "";
                    }
                    else
                    {
                        mbr.GenderVisibility = "display:none";
                        mbr.AltGenderVisibility = "display:inline";
                    }

                    if (string.IsNullOrEmpty(mbr.Name))
                    {
                        mbr.NameVisibility = "display:inline; width:200%";
                        mbr.AltNameVisibility = "display:none";
                        misNameCount += 1;
                        HiddenNameCount.Value = "" + misNameCount + "";
                    }
                    else
                    {
                        mbr.NameVisibility = "display:none";
                        mbr.AltNameVisibility = "display:inline";
                    }
                    if (string.IsNullOrEmpty(mbr.MemberType))
                    {
                        mbr.MemberTypeVisibility = "display:inline";
                        mbr.AltMemberTypeVisibility = "display:none";
                        misMemTypeCount += 1;
                        HiddenMemTypeCount.Value = "" + misMemTypeCount + "";
                    }
                    else
                    {
                        mbr.MemberTypeVisibility = "display:none";
                        mbr.AltMemberTypeVisibility = "display:inline";
                    }
                }
                else
                {
                    mbr.DOBVisibility = "display:none";
                    mbr.GenderVisibility = "display:none";
                    mbr.NameVisibility = "display:none";
                    mbr.MemberTypeVisibility = "display:none";
                }
                i++;
                if (!string.IsNullOrEmpty(item.Relationship))
                {
                    members.Add(mbr);
                }

            }
            if (members.Count == 1)
            {
                SetDisableControlIfErrorOcuurs("Not allowed to add only 1 member");
                return;
            }
            else if (members.Count > 1)
            {
                Session["Members"] = members;
                CheckSessionForFamilyAndAddMember(member);
                RptMembers.DataSource = members;
                RptMembers.DataBind();
            }
            else
            {
                SetDisableControlIfErrorOcuurs("Either name or id expiry or reltion ship is empty");
                return;
            }
        }
        catch (Exception ex)
        {
            SetDisableControlIfErrorOcuurs("Error occured while PrepareDataForFamily");
        }

    }



    private void CheckSessionForFamilyAndAddMember(AddMemberRequestDetail member)
    {
        if (HttpContext.Current.Session["AddMember"] != null)
        {
            AddMemberDetails(member);
        }
        else
        {
            var reqDetId = member.Id;
            HttpContext.Current.Session["ReqDetailID"] = reqDetId;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN1", "setReqDetail(" + reqDetId + ");", true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN2", "disableStep(" + 1 + ");", true);
        }
    }
    public void SetDisableControlIfErrorOcuurs(string messge)
    {
        btnPrevious2.Visible = true;
        divAlertBox.Visible = true;
        lblMessge.Text = messge;
        btnNext2.Visible = false;
        SectionFieldSet.Visible = false;
        // SectionButton.Visible = false;
    }
    public void AddMemberDetails(AddMemberRequestDetail member)
    {
        try
        {

            if (member.RequestId > 0)
            {

                if (!_memberService.IsRequestExist(member.IdNumber.Trim(), member.RequestId))
                {
                    member.Status = 1;
                    member.IdType = GeneralHelper.GetIdTypeFromIdNumber(member.IdNumber);
                    member.ContNumber = contractNumber;
                    if (member.RequestType == "E" || member.RequestType == "ED")
                    {
                        member.IsMainMemeber = true;
                    }
                    if (member.RequestType == "D")
                    {
                        member.IsMainMemeber = false;
                    }
                    var reqDetId = _memberService.Add(member);
                    HttpContext.Current.Session["ReqDetailID"] = (long)reqDetId;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN1", "setReqDetail(" + reqDetId + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "paramFN2", "disableStep(" + 1 + ");", true);
                }
                else
                {

                    SetDisableControlIfErrorOcuurs("Member already added in request");
                }
            }

        }
        catch (Exception ex)
        {

            SetDisableControlIfErrorOcuurs(ex.Message);

        }
    }
    private long GetMemberShipInfo(AddMemberRequestDetail member)
    {
        long result = 0;
        MbrDetail_DN[] Res = CaesarManager.GetMembership(contractNumber, member.MbrshipNo);
        if (Res != null && Res.Count() > 0)
        {
            if (Res[0].membershipNo != "")
            {
                member.Sponsor = Res[0].sponsorID.Trim();
                member.SecSponsorId = Res[0].memberID;
                member.BranchCode = Res[0].BranchCode.Trim();


                result = _memberService.Update(member);

                return result;
            }
        }

        return result;
    }
    private void SetExistingMemberInfoForDependent(string secSponsor, long reqID)
    {
        var member = _memberService.FindMainMemberBySecSponsorId(secSponsor, reqID);
        if (member != null)
        {
            SetTextBoxAndEnableDisableByValue(txtExpDate, ControlType.TextBox, member.PolicyStartOn);
            GeneralHelper.SetDropDown(member.Cover.Trim(), drpCover);
            GeneralHelper.SetDropDown(member.BranchCode.Trim(), drpCover);
            GeneralHelper.SetDropDown(member.District.Trim(), drpCover);
        }

    }

    private void SetFiledsByYakeenInfo(List<YakeenDetail> details, AddMemberRequestDetail member)
    {
        YakeenDetail detail = details[0];
        SetTextBoxAndEnableDisableByValue(txtName, ControlType.TextBox, GeneralHelper.ArabicToEnglish(detail.ArabicFullName, detail.EnglishFullName));
        if (txtName.Text.Length > 40)
        {
            txtName.Enabled = true;
            reqRegExpValtxtName.Enabled = true;
        }

        SetTextBoxAndEnableDisableByValue(txtExpDate, ControlType.TextBox, detail.IdExpiryDate);
        SetTextBoxAndEnableDisableByValue(txtExpDate, ControlType.TextBox, detail.VisaExpiryDate);

        var age = GeneralHelper.GetAgeFromDob(Convert.ToDateTime(detail.DOB));


        if (detail.DOB != null && detail.DOB > DateTime.MinValue)
        {
            SetTextBoxAndEnableDisableByValue(txtDOB, ControlType.TextBox, Convert.ToDateTime(detail.DOB).ToString("dd-MM-yyyy"));
        }
        else
        {
            txtDOB.Enabled = true;
        }

        IdType IdType = (member.IdNumber.Trim().StartsWith("1")) ? IdType.Saudi : IdType.NonSaudi;
        EmployeeType empType = (member.RequestType.Trim().Equals("E") || member.RequestType.Trim().Equals("ED")) ? EmployeeType.Employee : EmployeeType.Dependent;

        SetGender(detail.Gender, true);
        SetNationality(empType, IdType, detail.NationalityCode);
        SetProfession(empType, IdType, detail.OccupationCode);
        SetEmployeeDeptValidation();
        hdnGender.Value = detail.Gender.ToString();
        hdnDob.Value = Convert.ToDateTime(detail.DOB).ToString("dd-MM-yyyy");////hdnDob.Value = detail.DOB.ToString();


    }
    private void SetEmployeeDeptValidation()
    {
        if (Session["MandatoryIndicator"] != null)
        {
            var indicator = (Caesar.CaesarManager.Indicator)Session["MandatoryIndicator"];


            if (IsSme())
            {
                if (!string.IsNullOrEmpty(indicator.EmployeeNumberMandatory) && indicator.EmployeeNumberMandatory.ToLower().Trim() == "y")
                {
                    SectionEmployeeNo.Visible = true;
                    reqValtxtEmpNumber.Enabled = true;

                    SectionDept.Visible = true;
                    reqValtxtDepart.Enabled = true;
                    reqRegExValtxtDepart.Enabled = true;
                }
                else
                {
                    SectionEmployeeNo.Visible = false;
                    reqValtxtEmpNumber.Enabled = false;

                    SectionDept.Visible = false;
                    reqValtxtDepart.Enabled = false;
                    reqRegExValtxtDepart.Enabled = false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(indicator.EmployeeNumberMandatory) && indicator.EmployeeNumberMandatory.ToLower().Trim() == "y")
                {
                    SectionEmployeeNo.Visible = true;
                    reqValtxtEmpNumber.Enabled = true;

                    SectionDept.Visible = true;
                    reqValtxtDepart.Enabled = true;
                    reqRegExValtxtDepart.Enabled = true;
                }
                else
                {
                    SectionEmployeeNo.Visible = true;
                    reqValtxtEmpNumber.Enabled = false;


                    SectionDept.Visible = true;
                    reqValtxtDepart.Enabled = false;
                    reqRegExValtxtDepart.Enabled = false;
                }
            }

        }

    }
    private void SetTextBoxAndEnableDisableByValue(Control control, ControlType controltype, string value)
    {
        if (controltype == ControlType.TextBox)
        {
            var txt = (TextBox)control;
            txt.Enabled = false;
            if (!string.IsNullOrEmpty(value) || !string.IsNullOrWhiteSpace(value))
            {
                txt.Text = value.Trim();
            }
            else
            {
                txt.Enabled = true;
            }

        }
    }
    private void SetGender(Gender gender, bool isYakeenValid)
    {
        if (isYakeenValid)
        {
            if (gender != Gender.Unspecified)
            {
                if (gender == Gender.Male)
                    SetTitleGenderMaritalStatusByMale();
                else
                    SetTitleGenderMaritalStatusByFemale();
            }
            else
            {
                SetEnableOrDisableTitleGenderMarital(true);
            }

        }
        else
        {
            SetEnableOrDisableTitleGenderMarital(true);
        }
    }
    private void SetEnableOrDisableTitleGenderMarital(bool enabled)
    {
        drpGender.Enabled = enabled;
        drpTitle.Enabled = enabled;
        drpMaritalStatus.Enabled = enabled;
    }
    private void SetTitleGenderMaritalStatusByMale()
    {
        drpTitle.Items.Clear();

        drpTitle.Items.Insert(0, new ListItem("Mr", "Mr"));
        drpGender.SelectedValue = "M";
        drpTitle.SelectedValue = "Mr";
        drpTitle.Enabled = false;

        if (drpMemberType.SelectedValue == "C" || drpMemberType.SelectedValue == "SO")
        {
            //single
            drpMaritalStatus.SelectedValue = "1";
            drpMaritalStatus.Enabled = false;
        }
        else
        {
            ////drpMaritalStatus.Enabled = true;
            drpMaritalStatus.Enabled = drpMaritalStatus.SelectedValue == "";
        }
        drpTitle.Enabled = false;
        drpGender.Enabled = false;
    }
    private void SetTitleGenderMaritalStatusByFemale()
    {
        drpTitle.Items.Clear();
        drpTitle.Items.Insert(0, new ListItem("Select", ""));
        drpTitle.Items.Insert(1, new ListItem("Miss", "Miss"));
        drpTitle.Items.Insert(2, new ListItem("Mrs", "Mrs"));
        drpGender.SelectedValue = "F";
        if (drpMemberType.SelectedValue == "D" || drpMemberType.SelectedValue == "C")
        {
            drpTitle.SelectedValue = "Miss";
            //single
            drpMaritalStatus.SelectedValue = "1";

            drpTitle.Enabled = false;
            drpMaritalStatus.Enabled = false;
        }
        else if (drpMemberType.SelectedValue == "S")
        {
            drpTitle.SelectedValue = "Mrs";
            //married
            drpMaritalStatus.SelectedValue = "2";

            drpTitle.Enabled = false;
            drpMaritalStatus.Enabled = false;
        }
        else
        {
            drpTitle.Enabled = true;
            ///drpMaritalStatus.Enabled = false;
            //// Added by Sakthi
            if (drpMaritalStatus.SelectedValue == "2")
                drpTitle.SelectedValue = "Mrs";
            else
                drpTitle.SelectedValue = "Miss";


            drpMaritalStatus.Enabled = drpMaritalStatus.SelectedValue == "";
        }
        drpGender.Enabled = false;

    }
    private void SetNationality(EmployeeType empType, IdType idType, string code)
    {

        if (empType == EmployeeType.Employee || empType == EmployeeType.Dependent)
        {
            if (idType == IdType.Saudi)
            {
                GeneralHelper.SetDropDownContains("966-113", drpNationality);
            }

            if (idType == IdType.NonSaudi)
            {
                GeneralHelper.SetDropDownContains(code.Trim(), drpNationality);
            }

        }
    }
    private void SetProfessionForBaby(bool? isChild)
    {
        if (isChild != null && isChild == true)
        {
            GeneralHelper.SetDropDown("99402", drpProfession);
        }
    }
    private void SetProfession(EmployeeType empType, IdType idType, string code)
    {

        if (empType == EmployeeType.Employee)
        {
            if (idType == IdType.Saudi)
            {
                GeneralHelper.SetDropDown("0", drpProfession);
            }

            if (idType == IdType.NonSaudi)
            {
                GeneralHelper.SetDropDown(code.Trim(), drpProfession);
            }

        }


    }
    [WebMethod(EnableSession = true)]
    public static int SetProfessionByMemberTypeForDependent(string mbrType)
    {
        int result = 0;
        if (mbrType == "C" || mbrType == "SO" || mbrType == "D")
        {
            result = 99402;
        }
        else if (mbrType == "F" || mbrType == "M" || mbrType == "MB")
        {
            result = 99403;
        }
        else if (mbrType == "B" || mbrType == "SI")
        {
            result = 99404;
        }
        else if (mbrType == "S" || mbrType == "H" || mbrType == "W" || mbrType == "WM")
        {
            result = 99401;
        }
        else if (mbrType == "T")
        {
            result = 0;

        }
        return result;

    }
    #region Webmethods-Dropdowns

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Dropdown> GetClass()
    {
        var items = new List<Dropdown>();
        var contNo = HttpContext.Current.Session["ClientID"].ToString();
        var item = CaesarManager.GetClass(contNo).Select(t => new Dropdown
        {
            Text = t.className,
            Value = t.classID.ToString()
        });
        items = item.ToList();
        return items;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Dropdown> GetBranch()
    {
        var items = new List<Dropdown>();
        var contNo = HttpContext.Current.Session["ClientID"].ToString();
        var item = CaesarManager.GetBranch(contNo).Select(t => new Dropdown
        {
            Text = t.branchDesc,
            Value = t.branchCode
        });
        items = item.ToList();
        return items;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Dropdown> GetDistrict()
    {
        var items = new List<Dropdown>();
        var contNo = HttpContext.Current.Session["ClientID"].ToString();
        var item = CaesarManager.GetDistrict().Select(t => new Dropdown
        {
            Text = t.distName,
            Value = t.distCode
        });
        items = item.ToList();
        return items;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Dropdown> GetProfession()
    {
        var items = new List<Dropdown>();
        var item = CaesarManager.GetProfession().Select(t => new Dropdown
        {
            Text = t.profName,
            Value = t.profCode
        });
        items = item.ToList();
        return items;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Dropdown> GetNationality()
    {
        var items = new List<Dropdown>();
        var item = NationalityManager.Get().Select(t => new Dropdown
        {
            Text = t.Nationality,
            Value = t.MappedCode
        });
        items = item.ToList();
        return items;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Dropdown> GetMemberType(string cover, string reqType, DateTime dob, string gender, string maritalstatus)
    {
        var contNo = HttpContext.Current.Session["ClientID"].ToString();
        var items = new List<Dropdown>();
        if (!string.IsNullOrEmpty(cover) && !string.IsNullOrEmpty(reqType))
        {

            var response = CaesarManager.GetMemberTypeByCover(Convert.ToInt32(cover), reqType, contNo);
            List<ContMbrTypeListDetail_DN> result = new List<ContMbrTypeListDetail_DN>();
            if (response.status == "0")
            {
                if (reqType == "D")
                {

                    if (CaesarHelper.FilterMemberTypeByContacts(response.detail, cover) != null)
                    {
                        result = CaesarHelper.FilterMemberTypeByContacts(response.detail, cover);
                        var item = result.Select(t => new Dropdown
                        {
                            Text = t.mbrTypeDesc,
                            Value = t.mbrType,
                            prioriy = 10
                        });
                        item.ToList().Add(new Dropdown
                        {
                            Text = "Select MemberType",
                            prioriy = 1

                        });
                        items = item.OrderByDescending(t => t.prioriy).ToList();
                    }

                }
                else
                {
                    result = response.detail.ToList();
                    var item = result.Select(t => new Dropdown
                    {
                        Text = t.mbrTypeDesc,
                        Value = t.mbrType
                    }).ToList();

                    var removeItem = new Dropdown
                    {
                        Text = "Select MemberType",
                        prioriy = 1

                    };

                    item.Add(removeItem);

                    if (dob != null && gender != null)
                    {
                        var age = GeneralHelper.GetAgeFromDob(Convert.ToDateTime(dob));

                        if (age >= 65)
                        {

                            item = item.Where(t => t.Value == "SE" || t.Value == "E").ToList();
                            if (item.Count > 1)
                            {
                                item = item.Where(t => t.Value == "SE").ToList();

                            }
                            item.Remove(removeItem);
                        }
                        else
                        {
                            if (gender != null)
                            {
                                // item.Where(t => t.Value == "E");
                                if (gender.ToLower() == "male")
                                {
                                    item = item.Where(t => t.Value == "E").ToList();
                                    item.Remove(removeItem);
                                }
                                else if (gender.ToLower() == "female")
                                {
                                    if (age >= 45)
                                    {
                                        if (maritalstatus == "2")
                                        {
                                            item = item.Where(t => t.Value == "FE" || t.Value == "E").ToList();
                                            item.Remove(removeItem);
                                        }

                                        else
                                        {
                                            item = item.Where(t => t.Value == "E").ToList();
                                            item.Remove(removeItem);
                                        }
                                    }
                                    else
                                    {
                                        if (maritalstatus == "2")
                                        {
                                            item = item.Where(t => t.Value == "FE" || t.Value == "E").ToList();
                                            item.Remove(removeItem);
                                        }

                                        else
                                        {
                                            item = item.Where(t => t.Value == "E").ToList();
                                            item.Remove(removeItem);
                                        }

                                    }


                                }
                            }
                        }
                    }


                    items = item.OrderByDescending(t => t.prioriy).ToList();
                }
            }

        }

        return items;
    }
    #endregion
    [WebMethod(EnableSession = true)]
    public static long UpdateMemberDetails(AddMemberRequestDetail member)
    {
        long result = 0;
        AddMemberRequestDetailService memberService;
        member.IsYakeen = iSYakeenValidation;
        member.JoiningReason = GeneralHelper.GetJoiningReason(member.NewBorn);
        member.IdNumber = Convert.ToString(HttpContext.Current.Session["SecSponsorId"]);
        member.IDExpiry = GeneralHelper.GetFormatDate(member.IDExpiry);
        member.Title = !string.IsNullOrEmpty(member.Title) ? GetTitle(member.Title, member.MemberType, member.Gender)  : member.Title;

        int mainMemberAge = 0;

        if (member.RequestType == "E" || member.RequestType == "ED")
        {
            member.IsMainMemeber = true;
        }
        if (member.RequestType == "D")
        {
            member.IsMainMemeber = false;
        }
        if (HttpContext.Current.Session["ReqDetailID"] != null)
        {
            member.Status = 2;
            memberService = new AddMemberRequestDetailService();
            member.PolicyStartOn = !string.IsNullOrEmpty(member.PolicyStartOn) ? member.PolicyStartOn.ToLower().Contains("nan-nan-nan") ? ContractEffectiveDate() : member.PolicyStartOn : string.Empty;
            ////member.IDExpiry = DateTime.ParseExact(item.DOB, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (string.IsNullOrEmpty(member.PolicyStartOn))
            {
                return -1;
            }
            ////Added by Sakthi on 23-May-2018 SME contract Son/Daughter/Child should pass as "C"
            //// Start
            if (IsSme())
            {
                if (member != null)
                {
                    member.MemberType = GetSMEChildMemperType(member.MemberType);
                    member.Title = SetChildTitle(member.MemberType, member.Gender, member.Title);
                }
            }
            ////End
            result = memberService.Update(member);
            if (HttpContext.Current.Session["Members"] != null)
            {
                var dependents = (List<AddMemberRequestDetail>)HttpContext.Current.Session["Members"];
                int i = 0;
                int m = 0;
                if (member.Dependents != "")
                {
                    var deps = member.Dependents.Split(',').ToList();
                    string mainIdNumber = "";
                    foreach (var item in dependents)
                    {
                        if (i == 0)
                        {
                            ////Added by Sakthi on 23-May-2018 SME contract Son/Daughter/Child should pass as "C"
                            //// Start
                            if (IsSme())
                            {
                                member.MemberType = GetSMEChildMemperType(member.MemberType);
                                member.Name = !string.IsNullOrEmpty(member.Name) ? member.Name.Length > 40 ? member.Name.Substring(0, 40) : member.Name : member.Name;
                                member.EmployeeNo = !string.IsNullOrEmpty(member.EmployeeNo) ? member.EmployeeNo.Length > 15 ? member.EmployeeNo.Substring(0, 15) : member.EmployeeNo : member.EmployeeNo;
                                member.Department = !string.IsNullOrEmpty(member.Department) ? member.Department.Length > 40 ? member.Department.Substring(0, 40) : member.Department : member.Department;
                                mainMemberAge = GeneralHelper.GetAgeFromDob(DateTime.ParseExact(member.DOB, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                item.Title = SetChildTitle(member.MemberType, item.Gender, item.Title);
                            }
                            ////End    

                            item.MemberType = member.MemberType;
                        }
                        if (i > 0)
                        {
                            if (deps.Contains(item.IdNumber.Trim()))
                            {
                                var clientId = HttpContext.Current.Session["ClientID"].ToString();
                                memberService = new AddMemberRequestDetailService();
                                if (!string.IsNullOrEmpty(member.MissingValueName) && member.MissingValueName.Contains(','))
                                {
                                    string[] memName = member.MissingValueName.Split(',');
                                    foreach (string str in memName)
                                    {
                                        if (str.Length > 0 && str.Contains('|') && str.Split('|')[1] == item.IdNumber)
                                        {
                                            item.Name = str.Split('|')[0];
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(member.MissingValueMemType) && member.MissingValueMemType.Contains(','))
                                {
                                    string[] memName = member.MissingValueMemType.Split(',');
                                    foreach (string str in memName)
                                    {
                                        if (str.Length > 0 && str.Contains('|') && str.Split('|')[1] == item.IdNumber)
                                        {
                                            item.MemberType = str.Split('|')[0];
                                            break;
                                        }
                                    }
                                }
                                else
                                    item.MemberType = item.MemberType;

                                if (!string.IsNullOrEmpty(member.MissingValueGender) && member.MissingValueGender.Contains(','))
                                {
                                    string[] memName = member.MissingValueGender.Split(',');
                                    foreach (string str in memName)
                                    {
                                        if (str.Length > 0 && str.Contains('|') && str.Split('|')[1] == item.IdNumber)
                                        {
                                            item.Gender = str.Split('|')[0];
                                            break;
                                        }
                                    }

                                }
                                if (!string.IsNullOrEmpty(member.MissingValueDOB) && member.MissingValueDOB.Contains(','))
                                {
                                    string[] memName = member.MissingValueDOB.Split(',');
                                    foreach (string str in memName)
                                    {
                                        if (str.Length > 0 && str.Contains('|') && str.Split('|')[1] == item.IdNumber)
                                        {
                                            item.DOB = str.Split('|')[0];
                                            break;
                                        }
                                    }

                                }

                                var dob = DateTime.ParseExact(item.DOB, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                var age = GeneralHelper.GetAgeFromDob(dob);

                                item.Nationality = member.Nationality;
                                item.Title = GeneralHelper.GetTitleByRelation(item.MemberType.Trim());
                                item.Profession = GeneralHelper.GetProfessionByRelation(item.MemberType.Trim()).ToString();
                                item.MaritalStatus = GeneralHelper.GetMaritialStatusByRelation(item.MemberType.Trim()).ToString();
                                item.JoiningReason = member.JoiningReason;
                                item.Cover = member.Cover;
                                if (item.MemberType.Trim() != "S")
                                {
                                    if (IsSme())
                                    {
                                        if (isMainMemberSenior)
                                        {
                                            if (age >= 65)
                                                item.Cover = member.Cover;
                                            else
                                                item.Cover = CaesarManager.GetDependentSingleSchemeIsMainMemberSenior(clientId, member.Cover).ToString();
                                        }
                                        else
                                        {
                                            if (age >= 65)
                                                item.Cover = CaesarManager.GetSMEDependentSeniorScheme(clientId, member.Cover).ToString();
                                            else
                                                item.Cover = CaesarManager.GetSMEDependentScheme(clientId, member.Cover).ToString();
                                        }


                                    }
                                    else
                                    {
                                        item.Cover = member.Cover;
                                    }
                                }
                                else if (item.MemberType.Trim() == "S")
                                {
                                    if (IsSme())
                                    {

                                        if (isMainMemberSenior)
                                        {
                                            if (age >= 65)
                                            {
                                                item.Cover = member.Cover;
                                            }
                                            else if (age > 45)
                                            {
                                                try
                                                {
                                                    if (!string.IsNullOrEmpty(member.Maternity) && member.Maternity.Contains(','))
                                                    {
                                                        string[] matDet = member.Maternity.Split(',');
                                                        if (matDet[m] == "Covered") //Wife With Maternity items.Count>0 &&
                                                        {
                                                            m += 1;
                                                            item.Cover = CaesarManager.GetDependentRegularSchemeIsMainMemberSenior(clientId, member.Cover).ToString();
                                                        }
                                                        else
                                                        {
                                                            if (matDet[m] == "Not Covered")
                                                            {
                                                                m += 1;
                                                                item.Cover = CaesarManager.GetDependentSingleSchemeIsMainMemberSenior(clientId, member.Cover).ToString();
                                                            }
                                                            else
                                                                item.Cover = CaesarManager.GetDependentRegularSchemeIsMainMemberSenior(clientId, member.Cover).ToString();
                                                        }

                                                    }
                                                    else
                                                        item.Cover = CaesarManager.GetDependentRegularSchemeIsMainMemberSenior(clientId, member.Cover).ToString();
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                CaesarManager.GetDependentRegularSchemeIsMainMemberSenior(clientId, member.Cover).ToString();
                                            }
                                        }
                                        else
                                        {


                                            //var dob = DateTime.ParseExact(item.DOB, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                            //var age = GeneralHelper.GetAgeFromDob(dob);
                                            if (age >= 65)
                                            {
                                                //senio scheme same like main member scheme
                                                //// New logic
                                                //// Start
                                                ////item.Cover = member.Cover;
                                                item.Cover = CaesarManager.GetSMEDependentSeniorScheme(clientId, member.Cover).ToString();
                                                //// End
                                            }
                                            else if (age > 45)
                                            {
                                                //item.Cover = member.Cover;
                                                try
                                                {
                                                    //Call the CAESAR services for dropDownList items for Maternity
                                                    if (!string.IsNullOrEmpty(member.Maternity) && member.Maternity.Contains(','))
                                                    {
                                                        string[] matDet = member.Maternity.Split(',');
                                                        if (matDet[m] == "Covered") //Wife With Maternity items.Count>0 &&
                                                        {
                                                            m += 1;
                                                            item.Cover = member.Cover;
                                                        }
                                                        else
                                                        {
                                                            if (matDet[m] == "Not Covered")
                                                            {
                                                                m += 1;
                                                                item.Cover = CaesarManager.GetSMEDependentScheme(clientId, member.Cover).ToString();
                                                            }
                                                            else
                                                                item.Cover = member.Cover;
                                                        }

                                                    }
                                                    else
                                                        item.Cover = member.Cover;
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                            {
                                                //// SME spouse is female & < 45 years old, the system should set as main member scheme. by Sakthi 01-Apr-2018.
                                                //// the system is wrongly setting single scheme
                                                //// Start
                                                //// Old code
                                                //item.Cover = CaesarManager.GetRegularScheme(HttpContext.Current.Session["ClientID"].ToString(), member.Cover);
                                                //// New Code 
                                                //// New logic
                                                //// Start
                                                item.Cover = member.Cover;
                                                //item.Cover = CaesarManager.GetRegularScheme(clientId, member.Cover).ToString();
                                                ////End  
                                            }
                                        }

                                    }
                                    else
                                    {
                                        item.Cover = member.Cover;
                                    }
                                }
                                else
                                {
                                    item.Cover = member.Cover;
                                }

                                if (IsSme() && item.MemberType.Trim() != "S")
                                {
                                    string[] SmeContSchemes = ConfigurationManager.AppSettings["ContractSchems"].ToString().Split(',');
                                    if (SmeContSchemes.Contains(item.Cover))
                                    {
                                        continue;
                                    }
                                }

                                item.SecSponsorId = mainIdNumber;
                                item.IdType = GeneralHelper.GetIdTypeFromIdNumber(item.IdNumber.Trim());
                                item.ContNumber = clientId;
                                item.IsYakeen = iSYakeenValidation;

                                item.BranchCode = member.BranchCode;
                                item.District = member.District;
                                item.MobileNo = member.MobileNo;
                                item.IsMainMemeber = false;
                                ////item.EmployeeNo = member.EmployeeNo.Length > 15 ? member.EmployeeNo.Substring(0, 15) : member.EmployeeNo;
                                item.EmployeeNo = member.EmployeeNo;
                                item.Department = member.Department;
                                item.EmployeeNo = !string.IsNullOrEmpty(item.EmployeeNo) ? item.EmployeeNo.Length > 15 ? item.EmployeeNo.Substring(0, 15) : item.EmployeeNo : item.EmployeeNo;
                                if (string.IsNullOrEmpty(item.IDExpiry))
                                {
                                    item.IDExpiry = member.IDExpiry;
                                }
                                ////item.Department = member.Department;

                                item.Department = !string.IsNullOrEmpty(item.Department) ? item.Department.Length > 40 ? item.Department.Substring(0, 40) : item.Department : item.Department;
                                item.Name = !string.IsNullOrEmpty(item.Name) ? item.Name.Length > 40 ? item.Name.Substring(0, 40) : item.Name : item.Name;

                                ////item.PolicyStartOn = member.PolicyStartOn;

                                item.PolicyStartOn = !string.IsNullOrEmpty(member.PolicyStartOn) ? member.PolicyStartOn.ToLower().Contains("nan-nan-nan") ? ContractEffectiveDate() : member.PolicyStartOn : string.Empty;
                                if (string.IsNullOrEmpty(item.PolicyStartOn))
                                {
                                    return -1;
                                }
                                item.Status = member.Status;

                                string itemType = GetMemberTypeForEmployee(member.Cover, item.MemberType, item);
                                if (!string.IsNullOrEmpty(itemType))
                                    item.MemberType = itemType;

                                ////Added by Sakthi on 23-May-2018 SME contract Son/Daughter/Child should pass as "C"
                                //// Start
                                if (IsSme())
                                {
                                    item.MemberType = GetSMEChildMemperType(item.MemberType);
                                    item.Title = SetChildTitle(item.MemberType, item.Gender, item.Title);
                                }
                                ////End

                                item.Title = !string.IsNullOrEmpty(item.Title) ? GetTitle(item.Title, item.MemberType, item.Gender) : item.Title;

                                memberService.Add(item);
                            }
                        }
                        else
                        {
                            mainIdNumber = item.IdNumber;
                        }
                        i++;
                    }
                }
            }

            if (result > 0)
            {
                return result;
            }
        }

        return result;
    }
    public static string GetMemberTypeForEmployee(string cover, string MemType, AddMemberRequestDetail Memitem)
    {
        var Contract = HttpContext.Current.Session["ClientID"].ToString();
        string items = "";
        if (!string.IsNullOrEmpty(cover))
        {
            var response = CaesarManager.GetMemberTypeByCover(Convert.ToInt32(cover), "D", Contract);
            List<ContMbrTypeListDetail_DN> result = new List<ContMbrTypeListDetail_DN>();
            List<ContMbrTypeListDetail_DN> item = new List<ContMbrTypeListDetail_DN>();
            if (response.status == "0")
            {
                result = response.detail.ToList();
                if (MemType == "S")
                {
                    var dob = DateTime.ParseExact(Memitem.DOB, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    var age = GeneralHelper.GetAgeFromDob(dob);
                    item = result.Where(m => m.mbrType == "W" || m.mbrType == "WM").Select(t => t).ToList();
                    if (item.Count > 0 && item.Where(t => t.mbrType == "W").Count() > 0 && item.Where(t => t.mbrType == "WM").Count() > 0)
                    {
                        items = age > 45 ? "WM" : "W";
                        return items;
                    }
                    else if (item.Count > 0 && item.Where(t => t.mbrType == "W").Count() > 0)
                    {
                        items = age > 45 ? "WM" : "W";
                        return items;
                    }
                    else if (item.Count > 0 && item.Where(t => t.mbrType == "WM").Count() > 0)
                    {
                        items = age > 45 ? "WM" : "W";
                        return items;
                    }
                    else
                    {
                        items = MemType;
                        return items;
                    }

                }
                else if (MemType == "SO" || MemType == "D")
                {
                    item = result.Where(m => m.mbrType == "C").Select(t => t).ToList();
                    if (item.Count > 0)
                    {
                        items = "C";
                        return items;
                    }
                    else
                    {
                        items = MemType;
                        return items;
                    }

                }
                else
                {
                    items = MemType;
                }

            }
        }
        return items;
    }




    public static List<Dropdown> GetMemberTypeForMaternity(string cover, string MemType)
    {
        var Contract = HttpContext.Current.Session["ClientID"].ToString();
        var items = new List<Dropdown>();
        if (!string.IsNullOrEmpty(cover))
        {
            var response = CaesarManager.GetMemberTypeByCover(Convert.ToInt32(cover), "D", Contract);
            List<ContMbrTypeListDetail_DN> result = new List<ContMbrTypeListDetail_DN>();
            if (response.status == "0")
            {
                result = response.detail.ToList();
                var item = result.Where(m => m.mbrType == "W" || m.mbrType == "WM").Select(t => new Dropdown
                {
                    Text = t.mbrTypeDesc,
                    Value = t.mbrType
                }).ToList();
                items = item.OrderByDescending(t => t.Value).ToList();
            }
        }
        return items;
    }


    private static bool IsSme()
    {
        string comtype = Convert.ToString(HttpContext.Current.Session["ContractType"]);
        if (comtype.Trim().ToUpper().Equals("BDRC") || comtype.Trim().ToUpper().Equals("BDSC"))
        {
            return true;
        }
        return false;
    }
    private string[] splitDateDB(string dateB)
    {
        string[] valuesp = null;
        if (!(string.IsNullOrEmpty(dateB)))
        {
            string value = dateB;


            valuesp = value.Split('-');


            return valuesp;
        }
        else
        {
            return valuesp;
        }

    }
    private string[] splitDate(string dateB)
    {
        string[] valuesp = null;
        if (!(string.IsNullOrEmpty(dateB)))
        {
            string value = dateB;


            string valueSpl = value.Substring(0, value.IndexOf(':') - 2);
            valuesp = valueSpl.Split('/');

            return valuesp;
        }
        else
        {
            return valuesp;
        }

    }

    private static string ContractEffectiveDate()
    {
        string dateContractEffectiveDate = Convert.ToString(HttpContext.Current.Session["ContractEffectiveDate"]);
        var contractEffectiveDateNew = string.Empty;
        if (!string.IsNullOrEmpty(dateContractEffectiveDate))
        {
            dateContractEffectiveDate = dateContractEffectiveDate.Substring(0, dateContractEffectiveDate.IndexOf(':') - 2);
            contractEffectiveDateNew = !string.IsNullOrWhiteSpace(dateContractEffectiveDate) ? dateContractEffectiveDate.Trim().Replace("/", "-") : string.Empty;
        }
        return contractEffectiveDateNew;

    }

    private void setinformationFOrmDetils(AddMemberRequestDetail mm, bool newborn)
    {
        string comtype = Convert.ToString(HttpContext.Current.Session["ContractType"]);
        if (!(string.IsNullOrEmpty(mm.District)))
        {
            GeneralHelper.SetDropDown(mm.District, drpDistrict);

        }

        if (newborn)
        {
            if (!(string.IsNullOrEmpty(mm.Nationality)))
                drpNationality.SelectedValue = mm.Nationality;
        }
        if (!(string.IsNullOrEmpty(mm.EmployeeNo)))
        {
            txtEmpNumber.Text = mm.EmployeeNo;
            txtEmpNumber.Enabled = false;

        }
        if (!(string.IsNullOrEmpty(mm.Department)))
        {
            txtDepart.Text = mm.Department;
            txtDepart.Enabled = false;

        }
        if (!(string.IsNullOrEmpty(mm.Cover)))
        {
            if (comtype.Trim().ToUpper().Equals("BDRC") || comtype.Trim().ToUpper().Equals("BDSC"))
            {

                ////Enabling dependent scheme for  sme on 18-Jul-2018 on sakthi
                //// Start   
                EnablingSMEDependentScheme(hdnContractNumber.Value, hdnMAinEMpCoverType.Value);
                //old code start    
                //drpCover.SelectedValue = drpMemberTypeSelectedIndexChanged("", hdnContractNumber.Value, hdnMAinEMpCoverType.Value, "D");
                ////hdnMAinEMpCoverType.Value = drpCover.SelectedValue;
                //drpCover.Enabled = false;
                //old code end    

                ////End 
            }
            else
            {
                drpCover.SelectedValue = mm.Cover;
                drpCover.Enabled = false;

            }
            //drpCover.SelectedValue = mm.Cover;
            //drpCover.Enabled = false;
            List<Dropdown> list = GetMemberType(drpCover.SelectedValue, "D", DateTime.MinValue, null, "");
            drpMemberType.Items.Clear();
            //drpMemberType.DataTextField = list.;
            if (list.Count > 0)
            {
                foreach (Dropdown pr in list)
                {

                    drpMemberType.Items.Add(new ListItem(pr.Text, pr.Value));
                }

                drpMemberType.DataBind();
                drpMemberType.Items.Insert(0, new ListItem("Select MemberType", ""));
            }
        }
        if (!(string.IsNullOrEmpty(mm.MobileNo)))
        {
            txtMobileNumber.Text = mm.MobileNo;

        }

        if (!(string.IsNullOrEmpty(mm.BranchCode)))
        {

            GeneralHelper.SetDropDown(mm.BranchCode, drpBranchCode);
        }
        if (!(string.IsNullOrEmpty(mm.PolicyStartOn.ToString())))
        {

            hdnpolicyStartDate.Value = mm.PolicyStartOn.ToString();
            txtStartOfPolicy.Text = mm.PolicyStartOn.ToString();
            //txtStartOfPolicy.Enabled = false;
        }
    }
    private string setCseaerINformation(OnlineServices.CCHI.Dto.Member mm)
    {
        string comtype = Convert.ToString(HttpContext.Current.Session["ContractType"]);
        if (string.IsNullOrEmpty(txtExpDate.Text))
        {
            if (!(string.IsNullOrEmpty(mm.IdExpiryDate.ToString())))
            {
                string[] VAlue = splitDate(mm.IdExpiryDate.ToString());

                string monthofb = VAlue[0];
                string dayofb = VAlue[1];
                string yearofb = VAlue[2];
                txtExpDate.Text = VAlue[1] + "-" + VAlue[0] + "-" + VAlue[2];

            }
        }
        if (!(string.IsNullOrEmpty(mm.District)))
        {
            drpDistrict.SelectedValue = mm.District;
            GeneralHelper.SetDropDown(mm.District, drpDistrict);

        }
        if (!(string.IsNullOrEmpty(mm.EmployeeNumber)))
        {
            txtEmpNumber.Text = mm.EmployeeNumber;
            txtEmpNumber.Enabled = false;

        }
        if (!(string.IsNullOrEmpty(mm.Nationality)))
        {
            GeneralHelper.SetDropDownContains(mm.Nationality.Trim(), drpNationality);
        }
        try
        {
            if (!(string.IsNullOrEmpty(mm.Department)))
            {
                txtDepart.Text = mm.Department;
                txtDepart.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            Response.Write("<script>alert(" + ex.Message + ");</script>");

        }

        if (!(string.IsNullOrEmpty(mm.ClassID)))
        {
            if (comtype.Trim().ToUpper().Equals("BDRC") || comtype.Trim().ToUpper().Equals("BDSC"))
            {

                ////Enabling dependent scheme for  sme on 18-Jul-2018 on sakthi
                //// Start   
                hdnMAinEMpCoverType.Value = mm.ClassID;
                EnablingSMEDependentScheme(hdnContractNumber.Value, mm.ClassID);
                //old code start  
                //drpCover.SelectedValue = drpMemberTypeSelectedIndexChanged("", hdnContractNumber.Value, mm.ClassID, "D");
                ////// SME main member scheme is overriding, this should not override. by Sakthi on 01-Apr-2018
                //////Start        
                //////Old code        
                //////hdnMAinEMpCoverType.Value = drpCover.SelectedValue;
                ////// New code
                //hdnMAinEMpCoverType.Value = mm.ClassID;
                //////End 
                //drpCover.Enabled = false;
                //old code end    
            }
            else
            {
                drpCover.SelectedValue = drpMemberTypeSelectedIndexChanged("", hdnContractNumber.Value, mm.ClassID, "D", txtDOB.Text);
                drpCover.SelectedValue = mm.ClassID;
                drpCover.Enabled = false;

            }
            //drpCover.SelectedValue =;
            //drpCover.Enabled = false;
            List<Dropdown> list = GetMemberType(drpCover.SelectedValue, "D", DateTime.MinValue, null, "");
            drpMemberType.Items.Clear();
            //drpMemberType.DataTextField = list.;
            if (list.Count > 0)
            {
                foreach (Dropdown pr in list)
                {
                    drpMemberType.Items.Add(new ListItem(pr.Text, pr.Value));
                }
                drpMemberType.DataBind();
                drpMemberType.Items.Insert(0, new ListItem("Select MemberType", ""));
            }

        }
        if (!(string.IsNullOrEmpty(mm.MobileNumber)))
        {
            txtMobileNumber.Text = mm.MobileNumber;

        }
        if (!(string.IsNullOrEmpty(mm.BranchCode)))
        {
            GeneralHelper.SetDropDown(mm.BranchCode, drpBranchCode);

        }

        if (!(string.IsNullOrEmpty(mm.InsuranceEffectiveDate.ToString())))
        {
            string[] VAlue = splitDate(mm.InsuranceEffectiveDate.ToString());
            string monthofb = VAlue[0];
            string dayofb = VAlue[1];
            string yearofb = VAlue[2];
            txtStartOfPolicy.Text = VAlue[0] + "-" + VAlue[1] + "-" + VAlue[2];
            hdnpolicyStartDate.Value = VAlue[0] + "-" + VAlue[1] + "-" + VAlue[2];

        }

        return mm.SponsorID;
    }
    private OnlineServices.CCHI.Dto.Member GetMemberShipInfoNew(string membershipNumber, string contractNumber)
    {
        CaesarManager service = new CaesarManager();
        OnlineServices.CCHI.Dto.Member memberNew = service.GetMemberDetails(membershipNumber, contractNumber);
        return memberNew;
    }
    [WebMethod(EnableSession = true)]
    public static string drpMemberTypeSelectedIndexChanged(string mbrType, string ContNumberjs, string MAinEMpCoverType, string requesrTYpejs, string dob)
    {
        string comtype = Convert.ToString(HttpContext.Current.Session["ContractType"]);
        string result = "";
        if (requesrTYpejs == "D")
        {
            var mainMemberSchemeName = CaesarManager.GetMainMemberSchemeName(ContNumberjs, MAinEMpCoverType);

            if (comtype.Trim().ToUpper().Equals("BDRC") || comtype.Trim().ToUpper().Equals("BDSC"))
            {

                bool isCheckSenior = false;
                bool isRegular = false;
                if (!string.IsNullOrEmpty(dob))
                {
                    var dobValue = DateTime.ParseExact(dob.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    var age = GeneralHelper.GetAgeFromDob(dobValue);
                    isCheckSenior = age >= 65;
                    isRegular = age < 45;
                }

                if (mbrType != "S")
                {
                    if (mainMemberSchemeName.ToLower().Contains("senior"))
                    {
                        result = isCheckSenior ? MAinEMpCoverType : CaesarManager.GetDependentSingleSchemeIsMainMemberSenior(ContNumberjs, MAinEMpCoverType);
                    }
                    else
                    {
                        result = isCheckSenior ? CaesarManager.GetSMEDependentSeniorScheme(ContNumberjs, MAinEMpCoverType) : CaesarManager.GetSMEDependentScheme(ContNumberjs, MAinEMpCoverType);
                    }
                }
                else
                {

                    if (mainMemberSchemeName.ToLower().Contains("senior"))
                    {
                        result = isCheckSenior ? MAinEMpCoverType : isRegular ? CaesarManager.GetDependentRegularSchemeIsMainMemberSenior(ContNumberjs, MAinEMpCoverType) : CaesarManager.GetDependentSingleSchemeIsMainMemberSenior(ContNumberjs, MAinEMpCoverType);
                    }
                    else
                    {
                        result = isCheckSenior ? CaesarManager.GetSMEDependentSeniorScheme(ContNumberjs, MAinEMpCoverType) : isRegular ? MAinEMpCoverType : CaesarManager.GetSMEDependentScheme(ContNumberjs, MAinEMpCoverType);
                    }
                }

            }
            else
            {
                //result = CaesarManager.GetRegularScheme(ContNumberjs, MAinEMpCoverType);
                result = MAinEMpCoverType;
            }

        }
        return result;

    }
    [WebMethod(EnableSession = true)]



    ////Added by Sakthi on 24-May-2018 SME contract Disable Regular and (S) scheme 
    //// Start      
    private static string GetSMEChildMemperType(string memberType)
    {
        string type = memberType;
        if (IsSme() && !string.IsNullOrEmpty(memberType))
        {
            memberType = memberType.ToUpper().Trim();
            type = memberType.Equals("SO") || memberType.Equals("D") || memberType.Equals("C") ? "C" : memberType;
        }
        return type;
    }

    private static string SetChildTitle(string memberType, string gender, string title)
    {
        string titleType = title;
        if (!string.IsNullOrEmpty(memberType) && !string.IsNullOrEmpty(gender))
        {
            memberType = memberType.Trim().ToUpper();
            gender = gender.Trim().ToUpper();
            if (memberType.Equals("C") && gender.Equals("F"))
                titleType = "Miss";
            else if (memberType.Equals("C") && gender.Equals("M"))
                titleType = "Mr";
        }
        return titleType;
    }

    private void BindMaritalStatus(string idNumber)
    {
        string flag = string.Empty;
        flag = !string.IsNullOrEmpty(idNumber) ? idNumber.Trim() : string.Empty;
        drpMaritalStatus.Items.Clear();
        drpMaritalStatus.Items.Insert(0, new ListItem("Select Marital Status", ""));
        drpMaritalStatus.Items.Insert(1, new ListItem("Single", "1"));

        switch (flag)
        {
            case "1":
                {
                    drpMaritalStatus.Items.Insert(2, new ListItem("Married", "2"));
                    break;
                }
            case "NA":
                break;
            default:
                {
                    drpMaritalStatus.Items.Insert(2, new ListItem("Married", "2"));
                    drpMaritalStatus.Items.Insert(3, new ListItem("Divorced", "3"));
                    drpMaritalStatus.Items.Insert(4, new ListItem("Widowed", "4"));
                    break;
                }
        }

    }

    private void SetMaritalStatus(List<YakeenDetail> yakeenDetail)
    {
        if (yakeenDetail != null && yakeenDetail.Count > 0)
        {
            if (yakeenDetail[0].SocialStatus > 0)
            {
                drpMaritalStatus.ClearSelection();
                drpMaritalStatus.Items.FindByValue(Convert.ToString(yakeenDetail[0].SocialStatus)).Selected = true;
                socialStatus = yakeenDetail[0].SocialStatus;
            }
            string genderValue = Convert.ToString(yakeenDetail[0].Gender);
            if (!string.IsNullOrEmpty(genderValue.Trim()))
                genderType = genderValue.ToLower().Equals("male") ? "M" : genderValue.ToLower().Equals("female") ? "F" : "";
        }

    }


    private void EnablingSMEDependentScheme(string contractNumber, string mainMemberClassId)
    {
        var mainMemberClassName = string.Empty;
        mainMemberClassName = CaesarManager.GetMainMemberSchemeName(contractNumber, mainMemberClassId);
        mainMemberClassName = CaesarManager.RemoveSpecialCoverName(mainMemberClassName).Trim();
        if (!string.IsNullOrEmpty(mainMemberClassName))
        {
            string[] classNames = mainMemberClassName.Split('-');
            if (classNames.Length >= 2)
            {
                drpCover.Items.Cast<ListItem>().Where(item => !item.Text.ToLower().Contains(classNames[1].ToLower().Trim())).ToList().ForEach(drpCover.Items.Remove);
            }
        }
        drpCover.Enabled = false;
        drpCover.SelectedValue = drpMemberTypeSelectedIndexChanged("", contractNumber, mainMemberClassId, "D", txtDOB.Text);

        if (drpCover.Items[0].Text.Trim().ToLower() != "select scheme")
            drpCover.Items.Insert(0, new ListItem("Select Scheme", ""));

    }




    [WebMethod(EnableSession = true)]
    public static string ValidatePolicyEffectiveDate(string policyDate, string dob)
    {

        string result = string.Empty; ////
        try
        {
            int defaultStartDate = Convert.ToInt32(DateTime.Now.AddDays(-30).ToString("yyyyMMdd"));
            int defaultEndDate = Convert.ToInt32(DateTime.Now.AddDays(10).ToString("yyyyMMdd"));
            var contStartDate = Convert.ToString(HttpContext.Current.Session["ContractStartDate"]);
            var contEndDate = Convert.ToString(HttpContext.Current.Session["ContractEndDate"]);
            int contractStartDate = !string.IsNullOrEmpty(contStartDate) ? Convert.ToInt32(Convert.ToDateTime(contStartDate).ToString("yyyyMMdd")) : defaultStartDate;
            int contractEndDate = !string.IsNullOrEmpty(contEndDate) ? Convert.ToInt32(Convert.ToDateTime(contEndDate).ToString("yyyyMMdd")) : defaultEndDate;
            var mainMbrStartDate = Convert.ToString(HttpContext.Current.Session["MemberSatrtDate"]);
            int mainMemberSatrtDate = !string.IsNullOrEmpty(mainMbrStartDate) ? Convert.ToInt32(Convert.ToDateTime(mainMbrStartDate).ToString("yyyyMMdd")) : 0;
            var retentionMode = Convert.ToString(HttpContext.Current.Session["IsRM"]);
            var requestType = Convert.ToString(HttpContext.Current.Session["RequestType"]);

            if (!string.IsNullOrEmpty(policyDate.Trim()))
            {
                int policyStartDate = Convert.ToInt32(Convert.ToDateTime(policyDate.Trim()).ToString("yyyyMMdd"));

                if (requestType == "E" || requestType == "ED")
                {

                    if (retentionMode == "Y" && !IsSme())
                        result = policyStartDate >= contractStartDate && policyStartDate <= defaultEndDate ? string.Empty : "Addition date must be equal to inception date or after \n تاريخ الإضافة يجب ان يكون بنفس تاريخ بداية العقد او بعده";
                    else
                        result = policyStartDate >= defaultStartDate && policyStartDate <= defaultEndDate ? string.Empty : "Effective Date Should Be 30 Days Backdated or 10 Days in Advance \n يجب ان يكون التاريخ خلال 30 يوم الى الوراء أو عشرة ايام للامام";
                }
                else
                {
                    if (Convert.ToString(HttpContext.Current.Session["NewBornStep2"]) == "true")
                    {
                        int dobValue = !string.IsNullOrEmpty(dob) ? Convert.ToInt32(Convert.ToDateTime(dob).ToString("yyyyMMdd")) : 0;
                        if (dobValue > 0)
                        {
                            if (policyStartDate < dobValue)
                                result = "New born baby addition date must be equal to DOB date or after\n يجب أن يكون تاريخ إضافة مولود جديد يساوي تاريخ الميلاد أو بعده";
                            else
                            {
                                if (mainMemberSatrtDate > 0)
                                    result = policyStartDate >= mainMemberSatrtDate && policyStartDate <= defaultEndDate ? string.Empty : "Dependent addition date must be equal to Main Member date or after\n تاريخ إضافة التابع  يجب ان يكون بنفس تاريخ الموظف أو بعده";
                                else
                                    result = policyStartDate >= contractStartDate && policyStartDate <= defaultEndDate ? string.Empty : "Dependent addition date must be equal to contract effective date or after\n يجب أن يكون تاريخ الإضافة المعتمد مساويًا للتاريخ الفعلي للعقد أو بعده";
                            }
                        }
                        else
                            result = "Please enter DOB!";
                    }
                    else
                    {
                        if (mainMemberSatrtDate > 0 && retentionMode == "Y" && !IsSme())
                            result = policyStartDate >= mainMemberSatrtDate && policyStartDate <= defaultEndDate ? string.Empty : "Dependent addition date must be equal to Main Member date or after\n تاريخ إضافة التابع  يجب ان يكون بنفس تاريخ الموظف أو بعده";
                        else
                            result = policyStartDate >= defaultStartDate && policyStartDate <= defaultEndDate ? string.Empty : "Effective Date Should Be 30 Days Backdated or 10 Days in Advance \n يجب ان يكون التاريخ خلال 30 يوم الى الوراء أو عشرة ايام للامام";
                    }
                }

            }
            else
                result = "please choose the start medical insurance coverage date/the date Should Be 30 Days Backdated or 10 Days in Advance\nنرجو اختيار تاريخ بداية تغطية التأمين الطبي بحيث لا يتجاوز 30 يوم للوراء و 10 ايام للأمام";
        }
        catch (Exception)
        {
            return "Invalid date!";
        }
        return result;

    }
    private bool IsSenior()
    {
        bool isSenior = false;
        try
        {
            var dob = txtDOB.Text.Trim();
            if (!string.IsNullOrEmpty(dob))
            {
                var dobValues = DateTime.ParseExact(dob, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var age = GeneralHelper.GetAgeFromDob(dobValues);
                isSenior = age >= 65;
            }

        }
        catch (Exception)
        {

            throw;
        }
        return isSenior;
    }
    
    private static string GetTitle(string tile, string memberType, string gender)
    {
        string memberTitle = tile;
        try
        {
            if (string.IsNullOrEmpty(memberTitle))
            {
                switch (memberType)
                {
                   
                    case "SI":
                        memberTitle = "Miss";
                        break;
                    case "B":
                        memberTitle = "Mr";
                        break;                   
                    case "M":
                        memberTitle = "Mrs";
                        break;                    
                    case "F":
                        memberTitle = "Mr";
                        break;                   
                    case "T":
                        {
                            if(gender == "M")
                                memberTitle = "Mr";
                            else
                                memberTitle = "Miss";
                            break;
                        }
                }
            }

        }
        catch (Exception)
        {

            throw;
        }
        return memberTitle;
    }

    




}