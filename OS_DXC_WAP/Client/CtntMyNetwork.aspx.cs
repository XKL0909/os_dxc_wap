using System;
using System.Web.UI.WebControls;
using System.Text;
using OS_DXC_WAP.CaesarWS;
public partial class CtntMyNetwork : System.Web.UI.Page
{
    string  strClientID ;
    string strMembershipNo;
    private ServiceDepot_DNService ws;

   //change the name of the constructor
    public CtntMyNetwork()
    {
        //InitializeComponent();
        this.Load += new EventHandler(CtntMyNetwork_Load);
}

//change the name of the constructor and the page title
    void CtntMyNetwork_Load(object sender, EventArgs e)
{
    Master.SearchButton.Click += SearchButton_Click ;
    Master.ddlStaffList.SelectedIndexChanged += DDLDependentList_SelectedIndexChanged;
    Master.SearchOpt.SelectedIndexChanged += rdbSearchOption_SelectedIndexChanged;
    strClientID = Session["ClientID"].ToString();
    Master.ddlStaffList.Visible = false;
    Master.PageTitle.Text = "My Network";
   
}
    protected void rdbSearchOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Master.ddlStaffList.Visible == true)
        {
            Master.ddlStaffList.Visible = false;
        }
        Master.SearchText.Text = "";
        Message1.Text = "";
        if (UpdatePanel2.Visible == true)
            UpdatePanel2.Visible = false;

    }

    protected void DDLDependentList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strMemberExistStatus;
        if (Master.ddlStaffList.SelectedValue != "0")
        {
            strMemberExistStatus = CheckMemberExist(Master.ddlStaffList.SelectedValue);

            if (strMemberExistStatus != "Exist")
            {
                Message1.Text = strMemberExistStatus;
                Message1.Visible = true;
                UpdatePanel2.Visible = false;
                Master.ddlStaffList.Visible = true;
                ImageMap1.Visible = false;
            }
            else
            {


                ImageMap1.Visible = true;
                TableReport.Visible = false;
                btntest.Visible = false;
                //if (Master.ddlStaffList.SelectedValue != "0")
                //    DisplayMemberExistForClientResult(Master.ddlStaffList.SelectedValue, "Membership No");


                //if (Master.ddlStaffList.SelectedValue != "0")
                //    DisplayMemberExistForClientResult(Master.ddlStaffList.SelectedValue, "Membership No");
                Master.ddlStaffList.Visible = true;
                //lblMemberName.Visible = false; //.Text = Master.ddlStaffList.SelectedItem;
                //lblMember_Name.Visible = false;
                UpdatePanel2.Visible = true;
                Message1.Visible = false;
                Response.Redirect("../member/MyNetwork.aspx?val=" + Cryption.Encrypt(Master.ddlStaffList.SelectedValue));
            }
        }
        else
        {
            Message1.Text = "Please select the valid name from the drop down";
            Message1.Visible = true;
            UpdatePanel2.Visible = false;
            Master.ddlStaffList.Visible = true;
            ImageMap1.Visible = false;
        }
        //DisplayMemberInfoResult(Master.ddlStaffList.SelectedValue);
    }

    
private void SearchButton_Click(object sender, EventArgs e)
{
    string strMemberExistStatus;
    strMemberExistStatus = "";
    if (Master.SearchOpt.Text != "Staff No" && Master.SearchOpt.Text != "Badge No")
    {

        strMemberExistStatus = CheckMemberExist(Master.SearchText.Text);
        if (strMemberExistStatus != "Exist")
        {
            Message1.Text = strMemberExistStatus;
            Master.ddlStaffList.Visible = false;
            UpdatePanel2.Visible = false;
            Message1.Visible = true;
        }
        else
            DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);


    }
    else
        DisplayMemberExistForClientResult(Master.SearchText.Text, Master.SearchOpt.Text);      //DisplayMemberExistForClientResult(Master.SearchText, Master.SearchOpt);
}

 private void DisplayMemberExistForClientResult(string SearchText, string SearchType)
    {
        ws = new ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}
        

        EnqMbrListInfoRequest_DN request;
        EnqMbrListInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new EnqMbrListInfoRequest_DN();
            request.customerNo = strClientID;

            if (SearchType == "Membership No")
            {
                request.membershipNo = SearchText;
                strMembershipNo = SearchText;
            }

            else
                request.staffNo = SearchText;
            
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
           // request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
     //       request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;;
            response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.status != "0" )
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = "Inputted '" + Master.SearchOpt.Text + "' does not exist in your group.";
            UpdatePanel2.Visible =false;
            Message1.Visible = true;
        }
            else
            {
       //         if (SearchType == "Membership No")
       //         {
       //             //below line needs to be updated
       ///////////////             DisplayProdCov(SearchText);
       //            /// lblMemberName.Text = response.detail[0].memberName;
       //             ImageMap1.Visible = true;
       //             UpdatePanel2.Visible = true;
       //             Message1.Visible = false;



       //         }
       //         else
       //         {
                    Master.ddlStaffList.Items.Clear();

                    ListItem DepList0 = new ListItem();
                    DepList0.Text = "Please select from the list";
                    DepList0.Value = "0";
                    Master.ddlStaffList.Items.Add(DepList0);
                    
                    
                    foreach (MbrListDetail_DN dtl in response.detail)
                    {
                        ListItem DepList = new ListItem();
                        DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                        DepList.Value = dtl.membershipNo.ToString();
                        Master.ddlStaffList.Items.Add(DepList);
                        

                    }

                    Master.ddlStaffList.Visible = true;
                    Message1.Visible = false;


               // }

                
           //response.detail[0].relationship;
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }
            
            
            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }


    private void DisplayProviderLocationResult(EnqProvLocInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        ////////////string[,] ResponseDetails;

        ////////////ResponseDetails[0,0] = "test";
        ////////////ResponseDetails[0,1] = "test";
        ////////////ResponseDetails[0,2] = "test";


        ////////string arr(2,4);
        //////// string ar()() = new String({New String() {"", "", ""}, New String() {"", "", ""}};

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Arial;border-collapse:collapse;font-size:10px; border:solid thin ;'  class='display' id='example'><thead bgcolor='#e6f5ff'><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");


            foreach (ProvDetail_DN dtl in response.detail)
            {
                // itm = new ListItem(dtl.diagCode + "|" + dtl.diagDesc + "|" + dtl.servCode + "|" + dtl.servDesc + "|" + dtl.supplyPeriod + "|" + dtl.supplyFrom.Value.ToLongDateString() + "|" + dtl.supplyTo.Value.ToLongDateString() + "|" + dtl.estimateAmount.ToString() + "|" + dtl.cost.ToString() + "|" + dtl.lastRequestDate.Value.ToLongDateString());
                sbResponse.Append("<tr   ><td><b>" + dtl.providerName + "</b></td><td>" + dtl.address1 + "</td><td>" + dtl.address2 + "</td><td>" + dtl.address3 + "</td><td>" + dtl.district + "</td><td>" + dtl.telNo1 + "</td><td>" + dtl.telNo2 + "</td></tr>");
                //  lvwResult.Items.Add(itm);
                //BulletedList1.Items.Add(itm);

            }

            sbResponse.Append("</tbody></table>");
            str1 = "Provider List for " + ImageMap1.AlternateText + "<hr><table border=1   class='display' id='example'><thead><tr   ><th>Provider Name</th>  <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> " + str2 + "</tbody></table>";
            // TableReport.InnerHtml = str1;
            TableReport.InnerHtml = sbResponse.ToString();
            //Response.Write(sbResponse);
            //TextBox2.Text = sbResponse.ToString();
            TableReport.Visible = true;
        }
        else
        {
            msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }


    protected void ImageMap1_Click1(object sender, ImageMapEventArgs e)
    {
        // areaClicked.Text = e.PostBackValue;

        TableReport.Visible = true;
        // System.Threading.Thread.Sleep(4000);
        btntest.Visible = true;
        btntest.Text = e.PostBackValue;

        areaClicked.Visible = true;




        //  areaClicked.Text = "Hospital List for " +  ImageMap1.HotSpots["E"].AlternateText  + "<hr>";



        btntest.Text = "Check Providers in another region.";

        //EnqProvLocInfoRequest_DN request = new EnqProvLocInfoRequest_DN();
        //EnqProvLocInfoResponse_DN  response;
        ws = new ServiceDepot_DNService();

        EnqProvLocInfoRequest_DN request;
        EnqProvLocInfoResponse_DN response;

        response = null;

        request = new EnqProvLocInfoRequest_DN();
        request.Username = WebPublication.CaesarSvcUsername;

        request.Password = WebPublication.CaesarSvcPassword;;
        //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); //Int64.Parse(this.txt_txn.Text.Trim());
        request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        request.membershipNo = Master.ddlStaffList.SelectedValue; // strMembershipNo;  //txt_mbr.Text.Trim();
        request.region = e.PostBackValue;
        // request.staffNo = "";  //txt_staff.Text.Trim();
        // request.customerNo = ""; // txt_cust.Text.Trim();


        try
        {

            StringBuilder sb = new StringBuilder(200);
            response = ws.EnqProvLocInfo(request);
            //response =  ws.EnqProvLocInfo(request);

            if (response.errorID != "0")
                Message1.Text = response.errorMessage;
            else
            {
                DisplayProviderLocationResult(response);

                //String XmlizedString = null;
                //MemoryStream memoryStream = new MemoryStream();
                //XmlSerializer xs = new XmlSerializer(typeof(EnqProvLocInfoResponse_DN));
                //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                //xmlTextWriter.Formatting = Formatting.Indented;
                //xs.Serialize(xmlTextWriter, response);
                //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                //XmlizedString =  UTF8ByteArrayToString(memoryStream.ToArray());
                //txtBox_output.Text = XmlizedString;

            }



            //         if (response.status == "0")
            //{
            //    sbResponse.Append("<table border=1 style='font:Verdana;'  class='display' id='example'><thead><tr><th>Rendering engine</th> <th>Browser</th>  <th>Platform(s)</th> <th>Engine version</th> <th>CSS grade</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> 	</tr>	</thead><tbody> ");

            //    foreach (.MbrListDetail_DN dtl in response.detail)
            //    {
            //        itm = new ListItem(dtl.DiagCode + "|" + dtl.DiagDesc + "|" + dtl.ServCode + "|" + dtl.ServDesc + "|" + dtl.SupplyPeriod + "|" + dtl.SupplyFrom.Value.ToLongDateString() + "|" + dtl.SupplyTo.Value.ToLongDateString() + "|" + dtl.EstimateAmount.ToString() + "|" + dtl.Cost.ToString() + "|" + dtl.LastRequestDate.Value.ToLongDateString());
            //        sbResponse.Append("<tr><td>" + dtl.DiagCode + "</td><td>" + dtl.DiagDesc + "</td><td>" + dtl.ServCode + "</td><td>" + dtl.ServDesc + "</td><td>" + dtl.SupplyPeriod + "</td><td>" + dtl.SupplyFrom.Value.ToLongDateString() + "</td><td>" + dtl.SupplyTo.Value.ToLongDateString() + "</td><td>" + dtl.EstimateAmount.ToString() + "</td><td>" + dtl.Cost.ToString() + "</td><td>" + dtl.LastRequestDate.Value.ToLongDateString() + "</td></tr>"); 
            //        lvwResult.Items.Add(itm);
            //        //BulletedList1.Items.Add(itm);

            //    }
            //    sbResponse.Append("</tbody></table>");
            //    str1 = "<table border=2  class='display' id='example'><thead><tr><th>Rendering engine</th> <th>Browser</th>  <th>Platform(s)</th> <th>Engine version</th> <th>CSS grade</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> <th>Rendering engine</th> 	</tr>	</thead><tbody> " + str2 + "</tbody></table>";
            //    TextBox1.Text = str1;
            //    Response.Write(sbResponse);
            //    TextBox2.Text = sbResponse.ToString();
            //}
            //else
            //{
            //    msge = new StringBuilder(100);
            //    foreach (String s in response.errorMessage)
            //    {
            //        msge.Append(s).Append("/n");
            //    }
            //    Message1.Text = msge.ToString();
            //}


            ////////////if (response.errorID != "0")
            ////////////{
            ////////////    sb.Append(response.errorMessage).Append("\n");
            ////////////    MessageBox.Show(sb.ToString());
            ////////////}
            ////////////else
            ////////////{
            ////////////    String XmlizedString = null;
            ////////////    MemoryStream memoryStream = new MemoryStream();
            ////////////    XmlSerializer xs = new XmlSerializer(typeof(EnqMbrListInfoResponse_DN));
            ////////////    XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            ////////////    xmlTextWriter.Formatting = Formatting.Indented;
            ////////////    xs.Serialize(xmlTextWriter, response);
            ////////////    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
            ////////////    XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
            ////////////    this.txtBox_output.Text = XmlizedString;


            ////////////}
        }
        catch (Exception ex)
        {
            //Cursor.Current = Cursors.Default;
            Message1.Text = ex.Message;
        }

        // TableReport.InnerHtml = "<hr><img src='" + e.PostBackValue + ".jpg' size=25>";

    }


    protected void btntest_Click(object sender, EventArgs e)
    {
        TableReport.Visible = false;
        btntest.Visible = false;
        areaClicked.Visible = false;
        //UpdatePanel12.Visible = false;
        TableReport.InnerHtml = "";
    }



    private string CheckMemberExist(string MemberNo)
    {
        ws = new ServiceDepot_DNService();

        ChkMbrExistRequest_DN request;
        ChkMbrExistResponse_DN response;
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new ChkMbrExistRequest_DN();
            request.membershipNo = MemberNo;
            request.iqamaNo = "";// IqamaNo;
            request.totalNoOfFamily = ""; // DependentNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            //            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
           //request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));
//            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)); //Int64.Parse(this.txt_txn.Text.Trim());
           request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;;
            response = ws.ChkMbrExist(request);
            // Cursor.Current = Cursors.Default;


            if (response.memberName != null)
            {
                return "Exist";
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;
            }
            else
            {
                return response.errorMessage[0];
                //CaesarResult = "InValid";
                //CaesarMessage = response.errorMessage;
            }
            //string msg1;
            //  msg1 = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            return "Error";
            //UpdatePanel2.Visible = false;

            ////////////////////Message1.Text = ex.Message; /// this message would be used by transaction
            ////////////////////Message1.Text = "Invalid Member error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }




    }




    
}
