﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Client_ExcelReader"  Codebehind="ExcelReader.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    <br />
  
    <br />
    <dx:ASPxGridView ID="gvData" runat="server" OnHtmlDataCellPrepared="gvData_HtmlDataCellPrepared"
        AutoGenerateColumns="False" KeyFieldName="SEQ" OnBeforePerformDataSelect="gvData_BeforePerformDataSelect"
        OnRowUpdating="gvData_RowUpdating" Width="100%"  
        oncelleditorinitialize="gvData_CellEditorInitialize" 
        onhtmleditformcreated="gvData_HtmlEditFormCreated" Theme="Office2010Blue" 
        >
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0">
                <EditButton Visible="True">
                </EditButton>
                <ClearFilterButton Visible="True">
                </ClearFilterButton>
            </dx:GridViewCommandColumn>
           
        </Columns>
        <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
        <SettingsPopup>
            <EditForm Width="1024" VerticalAlign="WindowCenter" HorizontalAlign="WindowCenter"  />
        </SettingsPopup>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsPager Mode="ShowPager" PageSize="30" />
        <Settings ShowTitlePanel="true" ShowFilterRow="True" />
                                    <settings showfilterrow="True" />
                                    <images spritecssfilepath="~/App_Themes/Office2010Blue/{0}/sprite.css">
                                        <loadingpanelonstatusbar url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                        </loadingpanelonstatusbar>
                                        <loadingpanel url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                        </loadingpanel>
                                    </images>
                                    <imagesfiltercontrol>
                                        <loadingpanel url="~/App_Themes/Office2010Blue/GridView/Loading.gif">
                                        </loadingpanel>
                                    </imagesfiltercontrol>
                                    <styles cssfilepath="~/App_Themes/Office2010Blue/{0}/styles.css" 
                                        csspostfix="Office2010Blue">
                                        <header imagespacing="5px" sortingimagespacing="5px">
                                        </header>
                                        <loadingpanel imagespacing="5px">
                                        </loadingpanel>
                                    </styles>
                                    <stylespager>
                                        <pagenumber forecolor="#3E4846">
                                        </pagenumber>
                                        <summary forecolor="#1E395B">
                                        </summary>
                                    </stylespager>
                                    <styleseditors buttoneditcellspacing="0">
                                        <progressbar height="21px">
                                        </progressbar>
                                    </styleseditors>
    </dx:ASPxGridView>
 
        <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">

                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>

                    </div>
                </div>
				            <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>

                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>

                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
    </form>
</body>
</html>
