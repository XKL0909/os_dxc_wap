<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="ClientMaster.master" EnableEventValidation="false" Inherits="CtntMyDetails" Debug="true" Codebehind="CtntMyDetails.aspx.cs" %>

<%@ MasterType VirtualPath="ClientMaster.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<asp:Content ID="Content1_1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        

         <script language="javascript" type="text/javascript">


             function basicPopup(url) {
              //   alert('hi');
                 url = url + document.getElementById('<%=hidQueryString.ClientID %>').value;
                 popupWindow = window.open(url, 'popUpWindow', 'height=200,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
             }

        </script>
        <br /><br />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
            <ContentTemplate>
                <fieldset>
                <legend><h1>Personal Information</h1></legend>
            <table class="contentTable" width="100%" >
           
                <tr  >
                    <td class="contentTableHeader">
                        <asp:HiddenField ID="hidQueryString" runat="server" />

                        <asp:Label ID="lblMembership_No" runat="server" Text="Membership No " CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMembershipNo" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        <%--<asp:HyperLink ID="lbtn" Text="Edit Info" runat="server" ></asp:HyperLink>--%>
                         <a href="#" onclick="basicPopup('../member/editinfo.aspx?val=');">Edit Info</a>

                    </td>
                </tr> 
                 <tr >
                    <td >
                        <asp:Label ID="lblMember_Name" runat="server" Text="Member Name " CssClass="contentTableLabel" ></asp:Label></td>
                    <td >
                        <asp:Label ID="lblMemberName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                     <td>
                         &nbsp;</td>
                </tr>
                <tr >
                    <td >
                        <asp:Label ID="lblDate_Of_Birth" runat="server" Text="Date of Birth" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblDateOfBirth" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblGender_" runat="server" Text="Gender" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblGender" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblType_" runat="server" Text="Type" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblType" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblNationalityT" runat="server" Text="Nationality" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblNationality" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblEmployee_No" runat="server" Text="Employee No" 
                            CssClass="contentTableLabel"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblEmployeeNo" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblEmployee_ID" runat="server" Text="Saudi ID" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblEmployeeID" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblSponsor_Id" runat="server" Text="Sponsor Id" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblSponsorId" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblCompany_Name" runat="server" Text="Company Name" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblCompanyName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblMobileT" runat="server" Text="Mobile" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblMobile" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblEmailT" runat="server" Text="Email" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server"  CssClass="contentTableWhiteCell"></asp:Label></td>
                    <td>
                        &nbsp;</td>
                </tr>
          
            </table>
            </fieldset>
            <br />
                <fieldset>
                <legend><h1>Policy Information</h1></legend>

            <table class="contentTable" width="100%" >
           
                <tr >
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Customer No " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustID" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                 <tr >
                    <td >
                        <asp:Label ID="lblcustNameT" runat="server" Text="Customer Name " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustName" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td >
                        <asp:Label ID="lblcustSchemeT" runat="server" Text="Member Scheme" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblcustScheme" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblPlicysDateT" runat="server" Text="Policy Start Date" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblPlicysDate" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                <tr >
                    <td>
                        <asp:Label ID="lblPlicyeDateT" runat="server" Text="Policy End Date" 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblPlicyeDate" runat="server"    CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
          
            </table>
            </fieldset>
            <br />
            <fieldset runat="server" id="CchiStatus">
                <legend><h1>CCHI Status</h1></legend>
            <table  width="100%"  class="contentTable">
           
                <tr >
                    <td>
                        <asp:Label ID="Label26" runat="server" Text="CCHI Status " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblStatusP" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
                 <tr >
                    <td >
                        <asp:Label ID="Label28" runat="server" Text="Reason " 
                            CssClass="contentTableLabel"></asp:Label></td>
                    <td >
                        <asp:Label ID="lblStatusReason" runat="server"   CssClass="contentTableWhiteCell"></asp:Label></td>
                </tr>
          
            </table>
            </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
     
    <asp:Label ID="Message1" Font-Names="verdana" Font-Size="Small" Width="95%" runat="server" ></asp:Label>
    <div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
</asp:Content>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    View personal profile and policy details of your staff
</asp:Content>
