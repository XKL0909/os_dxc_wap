﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="UploadDocument" Codebehind="UploadDocument.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bupa | Upload Document</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
</head>
<body>
    <form id="form1" class="form-horizontal" runat="server">
         <script type="text/javascript">
             function RefreshMainPage() {
                 window.opener.location.reload();
             }
    </script>
        <div runat="server" class="container">

            <table class="table">
                <tr>
                    <td style="color: #337ab7; text-align: left">Max individual file upload size is 5 MB. Note that attempting to upload large files over a slow internet connection may result in a timeout.</td>
                </tr>
                <tr>
                    <td style="color: #337ab7; text-align: right; direction: rtl">الحد الأقصى لحجم كل ملف يراد تحميله 5 
            ميجا بيت (MB). لاحظ أن محاولة تحميل ملفات كبيرة عبر اتصال إنترنت بطيء قد يؤدي إلى قطع اتصالك بالموقع</td>
                </tr>
            </table>

            <div class="form-group">
                <asp:Label runat="server" ID="lblError"></asp:Label>
            </div>
            <div class="form-group">
                <%--<label for="exampleInputFile" class="col-md-2">Upload Documents</label>--%>
                <asp:FileUpload ID="FileUpload" runat="server" CssClass="form-control col-md-2 text-left" Style="width: 23%;" />
                <asp:Button ID="btnUpload" CssClass="col-md-2 btn btn-primary" runat="server" Style="width: 10%; margin-left: 10px" class="col-md-2 btn btn-default" Text="Upload" OnClick="btnUpload_Click" />
            </div>


            <div class="form-group">
                <asp:Repeater ID="RptUpload" runat="server" OnItemCommand="RptUpload_ItemCommand">
                    <HeaderTemplate>
                        <table class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <th>File Name</th>

                                <th>Delete</th>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <%--<td><%# Eval("name").ToString().Remove(0, Eval("name").ToString().IndexOf('-')+1).ToString() %></td>--%>
                            <td><%# Eval("FullName")%></td>
                            <td>
                                <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("Id")%>'>Delete</asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        </tbody>
                            <tfoot>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0?true:false %>' Text="No item(s) found" />
                                                                            </td>
                                                                        </tr>

                                                                    </tfoot>
                </table>
                                                                

                    </FooterTemplate>
                </asp:Repeater>
            </div>

        </div>
        <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="lnkSession" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                    <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                </asp:Panel>
                <div id="mainContent">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
   
</body>
</html>
