using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class TrackInfo : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
    string strMemberName;
    string strClientID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            Response.Redirect("../default.aspx");

        strClientID = Session["ClientID"].ToString();
		//Aramco PId Changes by Hussamuddin
        string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();  //Aramco PId Changes by Hussamuddin
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            Label3.Text = "Badge Number";
            btnSearchStaff.Text = "Search by Badge No.";
        }
        //Aramco PId Changes by Hussamuddin
    }

    protected void btnSearchRef_Click(object sender, EventArgs e)
    {
        DisplayTrackInfo(txtSearchRef.Text, "RefNo");
        txtSearchStaff.Text = "";
    }
    protected void btnSearchDate_Click(object sender, EventArgs e)
    {
        DisplayTrackInfo(txtSearchDate.Text, "SubmitDate");
        txtSearchRef.Text = "";
        txtSearchStaff.Text = "";
    }
    protected void btnSearchStaff_Click(object sender, EventArgs e)
    {
        DisplayTrackInfo(txtSearchStaff.Text, "StaffNo");
        txtSearchRef.Text = "";
    }

    private void DisplayTrackInfo(string strSearchText, string strSearchOption)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqTraInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqTraInfoResponse_DN response;
        request = new OS_DXC_WAP.CaesarWS.ReqTraInfoRequest_DN();
        request.TransactionID = TransactionManager.TransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.ContractNo = strClientID;

        if (strSearchOption == "RefNo")
            request.ReferenceNo = strSearchText;
        if (strSearchOption == "SubmitDate")
            request.SubmitDate = Convert.ToDateTime(strSearchText);// 
        if (strSearchOption == "StaffNo")
            request.StaffNo = strSearchText;

        try
        {
            response = ws.ReqTraInfo(request);
            StringBuilder sb = new StringBuilder(200);
            if (response.Status != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i]).AppendLine();

                if (response.errorMessage[0] == "No error message.")
                {
                    Message1.Text = "No records found matching the criteria.";
                    Message1.Visible = true;
                    CoverageListReport.Visible = false;
                }
                else
                    Message1.Text = sb.ToString();
            }
            else
            {
                if (response.detail != null)
                {
                    DisplayTrackInfoResult(response);
                    Message1.Text = "";
                }
                else
                    Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }
    }

    private void DisplayTrackInfoResult(OS_DXC_WAP.CaesarWS.ReqTraInfoResponse_DN response)
    {
        int intCounter = 0;
        StringBuilder sbResponse = new StringBuilder(2200);
        if (response.Status == "0")
        {
            sbResponse.Append("<table id='myTable' class='ruler' border=1px cellspacing='0' bordercolor='skyblue' " +
                "style='font:Arial;font-size:11px;'>" +
                "<thead bgcolor='#e6f5ff' forecolor=black><tr><th>Ref.No</th><th>M.ship No</th><th>Member Name</th>" +
                "<th>Class Name</th><th>Branch Name</th><th>Process Date</th><th>Submit Date</th><th>Adjustment Type</th> " +
                "<th>Status</th><th>Rejection Reason</th><th>Rejection Reason(Arabic)</th></tr></thead><tbody>");

            /* POSSBILE VALUES for Request update type
Add Member
Change Class
Change Branch
Card Replacement
Change Member Type
Delete Member
             */

            DataTable dt = new DataTable();
            dt.Columns.Add("ReferenceNo");
            dt.Columns.Add("Name");

            foreach (OS_DXC_WAP.CaesarWS.TraInfoDetail_DN dtl in response.detail)
            {
                #region Validate for Old records in order to remove them
                bool AddRecord = true;
                if (dt.Rows.Count == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr["ReferenceNo"] = dtl.ReferenceNo;
                    dr["Name"] = dtl.MemberName;
                    dt.Rows.Add(dr);
                }
                else
                {
                    //check for same record 
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["Name"].ToString() == dtl.MemberName && dr["ReferenceNo"].ToString() == dtl.ReferenceNo)
                        {
                            AddRecord = false;
                            break;
                        }
                    }
                    if (AddRecord)
                    {
                        DataRow dr = dt.NewRow();
                        dr["ReferenceNo"] = dtl.ReferenceNo;
                        dr["Name"] = dtl.MemberName;
                        dt.Rows.Add(dr);
                    }

                }
                #endregion
                if (AddRecord)
                {
                    string trackInfoRefLink = string.Empty;
                    var queryString = string.Empty;
                    if (dtl.AdjustmentType.Equals("Add Member"))//add emp/Dep
                    {
                        ////trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?refType=AE&refno=" + dtl.ReferenceNo + "'>";
                        queryString = Cryption.Encrypt("refType=AE&refno = " + dtl.ReferenceNo); 
                        trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?val=" + queryString + "'>";
                    }
                    else if (dtl.AdjustmentType.Equals("Delete Member"))//delete emp
                    {
                        ////trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?refType=DE&refno=" + dtl.ReferenceNo + "'>";
                        queryString = Cryption.Encrypt("refType=DE&refno=" + dtl.ReferenceNo);
                        trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?val=" + queryString + "'>";
                    }
                    else if (dtl.AdjustmentType.Equals("Delete Member"))//delete dep
                    {
                        ////trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?refType=DD&EType=Dep&refno=" + dtl.ReferenceNo + "'>";
                        queryString = Cryption.Encrypt("refType=DD&EType=Dep&refno=" + dtl.ReferenceNo);
                        trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?val=" + queryString + "'>";
                    }
                    else if (dtl.AdjustmentType.Equals("Change Branch"))//change branch
                    {
                        ////trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?refType=CB&refno=" + dtl.ReferenceNo + "'>";
                        queryString = Cryption.Encrypt("refType=CB&refno=" + dtl.ReferenceNo);
                        trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?val=" + queryString + "'>";
                    }
                    else if (dtl.AdjustmentType.Equals("Change Class"))//change class
                    {
                        ////trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?refType=CC&optiontype=Class&refno=" + dtl.ReferenceNo + "'>";
                        queryString = Cryption.Encrypt("refType=CC&optiontype=Class&refno=" + dtl.ReferenceNo);
                        trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?val=" + queryString + "'>";
                    }
                    else if (dtl.AdjustmentType.Equals("Card Replacement"))//card replacement
                    {
                        ////trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?refType=CR&refno=" + dtl.ReferenceNo + "'>";
                        queryString = Cryption.Encrypt("refType=CR&refno=" + dtl.ReferenceNo);
                        trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?val=" + queryString + "'>";
                    }
                    else
                    {
                        ////trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?refType=AE&refno=" + dtl.ReferenceNo + "'>";
                        queryString = Cryption.Encrypt("refType=AE&refno=" + dtl.ReferenceNo);
                        trackInfoRefLink = "<tr><td><a href='RequestAmmendment.aspx?val=" + queryString + "'>";
                    }

                    trackInfoRefLink += dtl.ReferenceNo + "</a></td><td>" + dtl.MembershipNo + "</td><td>" + dtl.MemberName + "</td><td>" + dtl.ClassName + "</td><td>" + dtl.BranchName + "</td><td>" + String.Format("{0:d}", dtl.ProcessDate) + "</td><td>" + /*(dtl.SubmitDate != null ? Convert.ToDateTime(dtl.SubmitDate).ToString("dd MMM yyyy hh:mm:ss tt") : "")*/ String.Format("{0:d}", dtl.SubmitDate) + "</td><td>" + dtl.AdjustmentType + "</td><td>";

                    ////if (dtl.TransactionStatus == "R" || dtl.TransactionStatus == "P")
					if (dtl.TransactionStatus == "P")
                    {
                        sbResponse.Append(trackInfoRefLink);
                    }
                    else
                    {
                        trackInfoRefLink = "<tr><td>";
                        trackInfoRefLink += dtl.ReferenceNo + "</td><td>" + dtl.MembershipNo + "</td><td>" + dtl.MemberName + "</td><td>" + dtl.ClassName + "</td><td>" + dtl.BranchName + "</td><td>" + String.Format("{0:d}", dtl.ProcessDate) + "</td><td>" + /*(dtl.SubmitDate != null ? Convert.ToDateTime(dtl.SubmitDate).ToString("dd MMM yyyy hh:mm:ss tt") : "") */String.Format("{0:d}", dtl.SubmitDate) + "</td><td>" + dtl.AdjustmentType + "</td><td>";
                        sbResponse.Append(trackInfoRefLink);
                    }

                    if (dtl.TransactionStatus == "P")
                        sbResponse.Append("Pending</td><td>");
                    if (dtl.TransactionStatus == "R")
                        sbResponse.Append("Rejected by Memebership</td><td>");
                    if (dtl.TransactionStatus == "S")
                        sbResponse.Append("Success</td><td>");
                    if (dtl.TransactionStatus == "F")
                        sbResponse.Append("Rejected by System</td><td>");
                    if (dtl.TransactionStatus == "Z")
                        sbResponse.Append("Rejected by System</td><td>");
                    if (dtl.TransactionStatus == "U")
                        sbResponse.Append("Cards for printing/delivery</td><td>");
                    if (dtl.TransactionStatus == "M")
                        sbResponse.Append("Rejected by Memebership</td><td>");
                    if (dtl.TransactionStatus == "W")
                        sbResponse.Append("Work In Progress</td><td>");
                    if (dtl.rejMessage != null)
                    {
                        //if (dtl.TransactionStatus == "F" || dtl.TransactionStatus == "R" || dtl.TransactionStatus == "Z" || dtl.TransactionStatus == "M")
                        if (dtl.TransactionStatus == "R" || dtl.TransactionStatus == "P" || dtl.TransactionStatus == "Z")
                        {
                            intCounter = 0;
                            sbResponse.Append("<table>");
                            foreach (string strArr in dtl.rejMessage)
                            {
                                if (strArr != null && strArr.Trim() != "")
                                {
                                    intCounter = intCounter + 1;
                                    sbResponse.Append("<tr><td><b>" + intCounter + ".</b> " + strArr + "</td></tr>");
                                }
                            }
                            sbResponse.Append("</table></td>");
                        }
                    }
                    else
                    {
                        sbResponse.Append(" ");
                    }
                    sbResponse.Append("<td>");
                    if (dtl.arabicRejMessage != null)
                    {
                        intCounter = 0;
                        sbResponse.Append("<table dir='rtl'>");
                        foreach (string strArr1 in dtl.arabicRejMessage)
                        {
                            if (strArr1 != null && strArr1.Trim() != "")
                            {
                                intCounter = intCounter + 1;
                                sbResponse.Append("<tr><td><b>" + intCounter + ".</b> " + strArr1 + "</td></tr>");
                            }
                        }
                        sbResponse.Append("</table>");
                    }
                }
            }
            sbResponse.Append("</td></tr>");
        }
        sbResponse.Append("</tbody></table>");
        CoverageListReport.Visible = true;
        CoverageListReport.InnerHtml = sbResponse.ToString();
    }
}