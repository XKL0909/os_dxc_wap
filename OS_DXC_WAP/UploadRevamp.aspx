﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="UploadRevamp" Codebehind="UploadRevamp.aspx.cs" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/UploaderRevamp.ascx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Upload Files</title>
    <script type="text/javascript" src="../functions/jquery.js"></script>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
    <script type="text/javascript"  src="js/jquery.dataTables.js"></script>

</head>
<body>
    <form id="frmSimpleUpload" runat="server">

            <%--<%if (uploader.LblUpload.Text.Length > 0)
  { %>
        <br />
        <br />
        <table style="width: 750px; font-size: 17px; font-family: Arial; border-right: black 0.5pt solid;
            border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid;
            border-collapse: collapse;" runat="server" id="Table3">
            <tr>
                <td align="left">
                    <span style="color: #cc0000"><%= uploader.LblUpload.Text%></span>
                </td>
            </tr>
        </table>
        <br />
        <br />
        
<% }%>--%>

        <asp:Label runat="server" ID="lblUploadExtension" />
         <asp:Label runat="server" ID="lblMessage" ForeColor="Red" />
        <BUPA:Uploader runat="server" ID="uploader" 
             SessionID="<%= RequestID %>"
            UploadCategory="<%= UploadCategory %>"
            UserID="<%= UserID  %>"  />
           <%-- SessionID='<%# Request.QueryString[Bupa.OSWeb.Helper.UploadPublication.QSKey_RequestID] %>' 
            UploadCategory='<%# Request.QueryString[Bupa.OSWeb.Helper.UploadPublication.QSKey_UploadCategory] %>'
            UserID='<%# Request.QueryString[Bupa.OSWeb.Helper.UploadPublication.QSKey_UserID] %>' --%>
           
            
        <a href="JavaScript:window.close()" style="display:none">Close</a>
            <asp:LinkButton ID="lbtnClose" runat="server" onclick="lbtnClose_Click">Close</asp:LinkButton>
            <dx:ASPxButton ID="btnSend" runat="server" Text="Send" Visible="false" 
                onclick="btnSend_Click">
            </dx:ASPxButton>

        <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

    </form>
</body>
</html>
