﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using extensions;
using System.Web.UI;

public partial class _Default : System.Web.UI.Page
{
    string sRefererURL = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
            divPopupConcurrentLogin.Visible = false;
            if (!IsPostBack)
            {
                //SSL/TLS Pre-Implementation Message Pop-up
                if (ConfigurationManager.AppSettings["EnableDisableBrowserComp"].ToString().ToUpper() == "TRUE")
                {
                    System.Web.HttpBrowserCapabilities browser = Request.Browser;
                    String version = Convert.ToString(browser.MajorVersion);
                    String bname = browser.Browser;
                    string[] BrowserList = ConfigurationManager.AppSettings["CompatableBrowsersList"].ToString().ToLower().Split('|');
                    bool displaypopup = false;

                    foreach (string browserversion in BrowserList)
                    {
                        if (bname.ToLower() == browserversion.Split(',')[0].ToLower() && Convert.ToInt16(version) < Convert.ToInt16(browserversion.Split(',')[1]))
                        {
                            displaypopup = true;
                            break;
                        }
                    }

                    if (displaypopup)
                    {
                        Numberdays.InnerText = (Convert.ToString((13 - DateTime.Now.Day) + 1));
                        ArNumberdays.InnerText = (Convert.ToString((13 - DateTime.Now.Day) + 1));
                        PopupUnSupported.Attributes.Add("style", "display:inline;top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999;");
                        PopupUnSupported.Visible = true;
                    }
                    else
                        PopupUnSupported.Visible = false;
                }
                //SSL/TLS Pre-Implementation Message Pop-up
				
				Session["CaptchaImageText"] = BLCommon.GenerateRandomCode();
                CaptchaText.InnerHtml = Session["CaptchaImageText"].ToString();
            }
            ListItem removeItem = cbLoginType.Items.FindByValue("0");
            if (removeItem != null)
            {
                cbLoginType.Items.Remove(removeItem);
            }

            if (Request.QueryString["LoginType"] != null)
                switch (Request.QueryString["LoginType"].ToString())
                {
                    case "1":
                        cbLoginType.SelectedValue = "1";
                        break;
                    case "2":
                        cbLoginType.SelectedValue = "2";
                        break;
                    case "3":
                        cbLoginType.SelectedValue = "3";
                        break;
                }

            else
            {
                //Response.Redirect("https://onlineservices.bupa.com.sa/");
            }

            //if (Request.Cookies["userLogin"] != null)
            //{
            //   if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            //    {
            //        Response.Redirect("/member/main.aspx");
            //    }
            //    if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            //    {
            //        Response.Redirect("/client/main.aspx");
            //    }
            //    if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            //    {
            //        Response.Redirect("/provider/main.aspx");
            //    } 
            //}

            if (!string.IsNullOrEmpty(Request.UrlReferrer.ToString()) && Request.UrlReferrer.ToString().Contains("MemberVerification"))
            {
                sRefererURL = Request.UrlReferrer.ToString();
            }
        }
        catch (Exception ex) { }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        String version = browser.MajorVersion + "." + browser.MinorVersion;
        String bname = browser.Browser + " " + version;
        Session["browserName"] = bname;
		switch (cbLoginType.SelectedValue.ToString())
        {
            case "0":
                if (LoginMember(txtUsername.Text.Trim(), txtPassword.Text.Trim()))
                {
                    Response.Redirect("member/main.aspx");
                }
                else
                {

                }
                break;
            case "1":
                if (txtUsername.Text.Trim() == Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["BIUserName"]))
                {
                    if (LoginBI(txtUsername.Text.Trim(), txtPassword.Text.Trim()))
                    {
                        Response.Redirect("client/BiList.aspx");
                    }
                }
                else
                {
                    //SetClientLoginRedirect(txtUsername.Text.Trim(), txtPassword.Text.Trim(), hdfLCPTP.Value);
					if (txtimgcode.Text == Session["CaptchaImageText"].ToString())
                    {
                        //divCaptchaImage.Visible = false;
                        txtimgcode.Text = "";
                        //WrongCaptcha.Style.Add("visibility", "hidden");
                        SetClientLoginRedirect(txtUsername.Text.Trim(), txtPassword.Text.Trim(), hdfLCPTP.Value);
                    }
                    else
                    {
                        txtimgcode.Text = "";
                        //divCaptchaImage.Visible = true;
                        //WrongCaptcha.Style.Add("visibility", "visible");
                        Session["CaptchaImageText"] = BLCommon.GenerateRandomCode();
                        CaptchaText.InnerText = Session["CaptchaImageText"].ToString();
                        lblError.Text = "Invalid Captcha. Please enter valid Captcha text.";
                    }
                }
                break;
            case "2":               
                //SetProviderLoginRedirect(txtUsername.Text.Trim(), txtPassword.Text.Trim(), hdfLCPTP.Value);
				if (txtimgcode.Text == Session["CaptchaImageText"].ToString())
                {
                    //divCaptchaImage.Visible = false;
                    txtimgcode.Text = "";
                    //WrongCaptcha.Style.Add("visibility", "hidden");
                    SetProviderLoginRedirect(txtUsername.Text.Trim(), txtPassword.Text.Trim(), hdfLCPTP.Value);
                }
                else
                {
                    txtimgcode.Text = "";
                    //divCaptchaImage.Visible = true;
                    //WrongCaptcha.Style.Add("visibility", "visible");
                    Session["CaptchaImageText"] = BLCommon.GenerateRandomCode();
                    CaptchaText.InnerText = Session["CaptchaImageText"].ToString();
                    lblError.Text = "Invalid Captcha. Please enter valid Captcha text.";
                }
                break;
            case "3":
                if (LoginBroker(txtUsername.Text.Trim(), txtPassword.Text.Trim()))
                {
                    Response.Redirect("broker/default.aspx");
                }
                break;
        }
    
   }
   
    protected void CaptchaValidate_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Session["CaptchaImageText"] = BLCommon.GenerateRandomCode();
            CaptchaText.InnerText = Session["CaptchaImageText"].ToString();
            if (txtimgcode.Text == "" || txtimgcode.Text != "")
            {
                txtimgcode.Text = "";
                //divCaptchaImage.Visible = true;
                return;
            }
        }
        catch (Exception ex) { }
    }

    protected void addcookie(string name, string value)
    {
        string _type = "";
        if (name == "MembershipNo")
        {
            _type = "1";
        }

        if (name == "ProviderNo")
        {
            _type = "2";
        }

        if (name == "ClientNo")
        {
            _type = "3";
        }

        string _time = DateTime.Now.ToString();
        HttpCookie usrCookie = new HttpCookie("userLogin");
        usrCookie.Values["UserID"] = value;
        usrCookie.Values["UserType"] = _type;
        usrCookie.Values["lastVisit"] = _time;
        usrCookie.Values["Encryption"] = String.Format("{0}&{1}&{2}", value, _type, _time).Encrypt();
        usrCookie.Domain = ".bupa.com.sa";
        Response.Cookies.Add(usrCookie);
    }

    protected bool LoginMember(string _username, string _password)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }
            // Set up parameters (2 input and 3 output) 
            SqlParameter[] arParms = new SqlParameter[5];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = txtUsername.Text.Trim();
            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 30);
            arParms[1].Value = txtPassword.Text.Trim();
            arParms[2] = new SqlParameter("@MembershipNo", SqlDbType.NVarChar, 30);
            arParms[2].Direction = ParameterDirection.Output;
            arParms[3] = new SqlParameter("@FullName1", SqlDbType.NVarChar, 100);
            arParms[3].Direction = ParameterDirection.Output;
            arParms[4] = new SqlParameter("@Email", SqlDbType.NVarChar, 30);
            arParms[4].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spCheckUserExist", arParms);

            // Display results in text box using the values of output parameters	
            fullname = arParms[3].Value + ""; //, " + arParms[3].Value + ", " + arParms[4].Value ;

            Session.Timeout = 90;
            Session["MemberName"] = fullname;
            Session["MembershipNo"] = arParms[2].Value; //TxtLogin.Text; 
            addcookies(Convert.ToString(arParms[2].Value), Convert.ToString(arParms[1].Value), "mem");
            addcookie("MembershipNo", Convert.ToString(arParms[2].Value));
            Session["LoginInfo"] = arParms[2].Value;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                _valid = true;
            }
            else
            {
                lblError.ForeColor = System.Drawing.Color.Red;

                lblError.Text = "Authentication Failed. Incorrect Membership No. or Password. Please try again.";
            }
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect Membership No. or Password. Please try again.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        lblError.Visible = true;
        return _valid;
    }

    protected IDataReader LoginClientAccountLockOut(string _username, string _password)
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
        cmd.Connection = cn;
        cmd = new SqlCommand("spValidateClient_Hash", cn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@User_Name", _username);
        cmd.Parameters.AddWithValue("@Password", _password);
        cn.Open();
        IDataReader Idr = cmd.ExecuteReader();
        return Idr;
    }
    protected IDataReader LoginProviderAccountLockOut(string _username, string _password)
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
        cmd.Connection = cn;
        cmd = new SqlCommand("Spvalidateprovider_hash", cn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@User_Name", _username);
        cmd.Parameters.AddWithValue("@Password", _password);
        cn.Open();
        IDataReader Idr = cmd.ExecuteReader();
        return Idr;
    }

 
    protected bool LoginClient(string _username, string _password)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }

            // Set up parameters (2 input and 3 output) 
            SqlParameter[] arParms = new SqlParameter[12];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = txtUsername.Text.Trim();
            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 30);
            arParms[1].Value = txtPassword.Text.Trim();
            arParms[2] = new SqlParameter("@ClientID", SqlDbType.Char, 8);
            arParms[2].Direction = ParameterDirection.Output;
            arParms[3] = new SqlParameter("@ClientName", SqlDbType.NVarChar, 200);
            arParms[3].Direction = ParameterDirection.Output;
            arParms[4] = new SqlParameter("@ClientNotes", SqlDbType.NVarChar, 500);
            arParms[4].Direction = ParameterDirection.Output;
            arParms[5] = new SqlParameter("@ExportToExcel", SqlDbType.Bit, 1);
            arParms[5].Direction = ParameterDirection.Output;
            arParms[6] = new SqlParameter("@ClientNo", SqlDbType.Int);
            arParms[6].Direction = ParameterDirection.Output;
            arParms[7] = new SqlParameter("@ContractNumbers", SqlDbType.NVarChar, 5000);
            arParms[7].Direction = ParameterDirection.Output;
            arParms[8] = new SqlParameter("@ShowDashboardLink", SqlDbType.Bit, 1);
            arParms[8].Direction = ParameterDirection.Output;
            arParms[9] = new SqlParameter("@CRefNo", SqlDbType.Int);
            arParms[9].Direction = ParameterDirection.Output;
            arParms[10] = new SqlParameter("@Active", SqlDbType.Bit, 1);
            arParms[10].Direction = ParameterDirection.Output;
            arParms[11] = new SqlParameter("@isSuperUser", SqlDbType.Bit, 1);
            arParms[11].Direction = ParameterDirection.Output;


            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spValidateClient", arParms);
            ////SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spValidateClient_UserManagement", arParms);


            // Display results in text box using the values of output parameters	
            fullname = arParms[3].Value + ""; //, " + arParms[3].Value + ", " + arParms[4].Value ;

            //Session value intitiated to Be used for the entire session.
            Session.Timeout = 90;
            Session["ClientName"] = fullname;
            Session["ClientID"] = arParms[2].Value;
            Session["ClientUsername"] = arParms[0].Value;
            Session["ExportToExcel"] = arParms[5].Value.ToString().Trim();
			Session["ClientCredential"] = arParms[1].Value;
            Session["ContractNumbers"] = arParms[7].Value.ToString().Trim();
            Session["ShowDashboardLink"] = arParms[8].Value.ToString().Trim();
            Session["ReferenceNumber"] = arParms[9].Value.ToString().Trim();
            Session["SuperUser"] = Convert.ToString(arParms[11].Value);
            addcookies(Convert.ToString(arParms[0].Value), Convert.ToString(arParms[1].Value), "client");
            addcookie("ClientNo", Convert.ToString(arParms[6].Value));
            Session["LoginInfo"] = arParms[2].Value;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                _valid = true;
            }
            else
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "Authentication Failed. Incorrect Login in ID or Password. Please try again.";
            }
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        return _valid;
    }


    protected bool LoginClientWithHash(string _username, string _password, string _LCpassword)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }
            //3 Input Parameters
            SqlParameter[] arParms = new SqlParameter[13];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = _username.Trim();
            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 64);
            arParms[1].Value = _password.Trim();
            arParms[2] = new SqlParameter("@LCPassword", SqlDbType.VarChar, 256);
            arParms[2].Value = _LCpassword.Trim();
            arParms[3] = new SqlParameter("@ClientID", SqlDbType.Char, 8);
            arParms[3].Direction = ParameterDirection.Output;
            arParms[4] = new SqlParameter("@ClientName", SqlDbType.NVarChar, 200);
            arParms[4].Direction = ParameterDirection.Output;
            arParms[5] = new SqlParameter("@ClientNotes", SqlDbType.NVarChar, 500);
            arParms[5].Direction = ParameterDirection.Output;
            arParms[6] = new SqlParameter("@ExportToExcel", SqlDbType.Bit, 1);
            arParms[6].Direction = ParameterDirection.Output;
            arParms[7] = new SqlParameter("@ClientNo", SqlDbType.Int);
            arParms[7].Direction = ParameterDirection.Output;
            arParms[8] = new SqlParameter("@ContractNumbers", SqlDbType.NVarChar, 5000);
            arParms[8].Direction = ParameterDirection.Output;
            arParms[9] = new SqlParameter("@ShowDashboardLink", SqlDbType.Bit, 1);
            arParms[9].Direction = ParameterDirection.Output;
            arParms[10] = new SqlParameter("@CRefNo", SqlDbType.Int);
            arParms[10].Direction = ParameterDirection.Output;
            arParms[11] = new SqlParameter("@Active", SqlDbType.Bit, 1);
            arParms[11].Direction = ParameterDirection.Output;
            arParms[12] = new SqlParameter("@isSuperUser", SqlDbType.Bit, 1);
            arParms[12].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spValidateClientWithHash", arParms);
            fullname = arParms[4].Value + "";
            //Session.Timeout = 90;
            Session["ClientName"] = fullname;
            Session["ClientID"] = arParms[3].Value;
            Session["ClientUsername"] = arParms[0].Value;
            Session["ExportToExcel"] = arParms[6].Value.ToString().Trim();
            Session["ClientCredential"] = arParms[1].Value;
            Session["ContractNumbers"] = arParms[8].Value.ToString().Trim();
            Session["ShowDashboardLink"] = arParms[9].Value.ToString().Trim();
            Session["ReferenceNumber"] = arParms[7].Value.ToString().Trim();
            Session["SuperUser"] = Convert.ToString(arParms[12].Value);
            addcookies(Convert.ToString(arParms[0].Value), Convert.ToString(arParms[1].Value), "client");
            addcookie("ClientNo", Convert.ToString(arParms[7].Value));
            Session["LoginInfo"] = arParms[3].Value;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                _valid = true;
            }
            else
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "Authentication Failed. Incorrect Login in ID or Password. Please try again.";
            }
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        return _valid;
    }

    protected bool LoginProviderWithHash(string _username, string _password, string _lcPassword)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }

            // Set up parameters (3 input and 8 output) 
            SqlParameter[] arParms = new SqlParameter[13];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = txtUsername.Text.Trim();
            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 64);
            arParms[1].Value = txtPassword.Text.Trim();
            arParms[2] = new SqlParameter("@LCPassword", SqlDbType.VarChar, 256);
            arParms[2].Value = _lcPassword;
            arParms[3] = new SqlParameter("@ProviderID", SqlDbType.Char, 20);
            arParms[3].Direction = ParameterDirection.Output;
            arParms[4] = new SqlParameter("@ProviderName", SqlDbType.NVarChar, 200);
            arParms[4].Direction = ParameterDirection.Output;
            arParms[5] = new SqlParameter("@ProviderFax", SqlDbType.NVarChar, 200);
            arParms[5].Direction = ParameterDirection.Output;
            arParms[6] = new SqlParameter("@ProviderNo", SqlDbType.Int);
            arParms[6].Direction = ParameterDirection.Output;
            arParms[7] = new SqlParameter("@SwipCardEnabled", SqlDbType.Bit);
            arParms[7].Direction = ParameterDirection.Output;
            arParms[8] = new SqlParameter("@PreAuthAllowed", SqlDbType.Bit);
            arParms[8].Direction = ParameterDirection.Output;
            arParms[9] = new SqlParameter("@HRMEnabled", SqlDbType.Bit);
            arParms[9].Direction = ParameterDirection.Output;
            arParms[10] = new SqlParameter("@pRefNo", SqlDbType.Int);
            arParms[10].Direction = ParameterDirection.Output;
            arParms[11] = new SqlParameter("@Active", SqlDbType.Bit, 1);
            arParms[11].Direction = ParameterDirection.Output;
            arParms[12] = new SqlParameter("@isSuperUser", SqlDbType.Bit, 1);
            arParms[12].Direction = ParameterDirection.Output;
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spValidateProviderPA_New_WithHash", arParms);

            fullname = arParms[4].Value + "";

            Session["ProviderName"] = fullname;
            Session["ProviderFax"] = arParms[5].Value + "";
            Session["ProviderUserName"] = txtUsername.Text.Trim(); // fullname;
            Session["ProviderID"] = arParms[3].Value.ToString().Trim();
            Session["PreAuthAllowed"] = arParms[8].Value.ToString().Trim();
            Session["HRMEnabled"] = arParms[9].Value.ToString().Trim();
            Session["pRefNo"] = arParms[10].Value.ToString().Trim();
            BLCommon.PreAuthAllowed = Convert.ToBoolean(arParms[8].Value.ToString().Trim());
            BLCommon.HRMEnabled = Convert.ToBoolean(arParms[9].Value.ToString().Trim());
            Session["SwipCardEnabled"] = arParms[7].Value.ToString().Trim();
            addcookies(Convert.ToString(arParms[0].Value), Convert.ToString(arParms[1].Value), "provider");
            addcookie("ProviderNo", Convert.ToString(arParms[6].Value));
            Session["LoginInfo"] = arParms[3].Value.ToString().Trim();
            Session["SuperUser"] = Convert.ToString(arParms[12].Value);
            if (!string.IsNullOrEmpty(Convert.ToString(arParms[3].Value.ToString().Trim())))
            {
                _valid = true;
            }
            else
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
            }
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        return _valid;
    }

    protected bool LoginBI(string _username, string _password)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }

            // Set up parameters (2 input and 3 output) 
            SqlParameter[] arParms = new SqlParameter[6];

            // @User_Name Input Parameter 
            // assign value = 1
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = txtUsername.Text;

            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 30);
            arParms[1].Value = txtPassword.Text;

            // @ProductName Output Parameter
            arParms[2] = new SqlParameter("@ClientID", SqlDbType.Char, 8);
            arParms[2].Direction = ParameterDirection.Output;

            // @UnitPrice Output Parameter
            arParms[3] = new SqlParameter("@ClientName", SqlDbType.NVarChar, 200);
            arParms[3].Direction = ParameterDirection.Output;

            // @QtyPerUnit Output Parameter
            arParms[4] = new SqlParameter("@ClientNotes", SqlDbType.NVarChar, 500);
            arParms[4].Direction = ParameterDirection.Output;

            // @ExportToExcel Output Parameter
            arParms[5] = new SqlParameter("@ExportToExcel", SqlDbType.Bit, 1);
            arParms[5].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spBupa_OSCheckClientExist", arParms);

            // Display results in text box using the values of output parameters	
            fullname = arParms[3].Value + ""; //, " + arParms[3].Value + ", " + arParms[4].Value ;

            //Session value intitiated to Be used for the entire session.
            //Session.Timeout = 90;



            Session["ClientName"] = fullname;

            Session["ClientID"] = arParms[2].Value;  //ddlClientList.SelectedValue; // arParms[2].Value;
            Session["Language"] = "EN";
            Session["ClientUsername"] = txtUsername.Text; // "EN";  
            Session["TimeOut"] = DateTime.Now.AddMinutes(30.00); // TxtLogin.Text; // "EN"; 
            Session["ExportToExcel"] = arParms[5].Value.ToString().Trim();
            addcookie("ClientNo", Convert.ToString(arParms[2].Value));
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }

        if (fullname != "")
        {
            _valid = true;
        }
        else
        {
            _valid = false;
        }
        return _valid;
    }
protected bool LoginClientWithConcur(string _username, string _password, string _LCPassword,bool IsAccLockOut)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        Session["HashPassKey"] = _password;
        Session["LCHashPassKey"] = _LCPassword;
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }

            SqlParameter[] arParms = new SqlParameter[17];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            //arParms[0].Value = txtUsername.Text.Trim();
            arParms[0].Value = _username.Trim();
            arParms[1] = new SqlParameter("@HashPassword ", SqlDbType.NVarChar, 64);
            //arParms[1].Value = txtPassword.Text.Trim();
            arParms[1].Value = _password.Trim();
            arParms[2] = new SqlParameter("@LCPassword", SqlDbType.NVarChar, 64);
            arParms[2].Value = _LCPassword.Trim();
            arParms[3] = new SqlParameter("@ClientID", SqlDbType.Char, 8);
            arParms[3].Direction = ParameterDirection.Output;
            arParms[4] = new SqlParameter("@ClientName", SqlDbType.NVarChar, 200);
            arParms[4].Direction = ParameterDirection.Output;
            arParms[5] = new SqlParameter("@ClientNotes", SqlDbType.NVarChar, 500);
            arParms[5].Direction = ParameterDirection.Output;
            arParms[6] = new SqlParameter("@ExportToExcel", SqlDbType.Bit, 1);
            arParms[6].Direction = ParameterDirection.Output;
            arParms[7] = new SqlParameter("@ClientNo", SqlDbType.Int);
            arParms[7].Direction = ParameterDirection.Output;
            arParms[8] = new SqlParameter("@ContractNumbers", SqlDbType.NVarChar, 5000);
            arParms[8].Direction = ParameterDirection.Output;
            arParms[9] = new SqlParameter("@ShowDashboardLink", SqlDbType.Bit, 1);
            arParms[9].Direction = ParameterDirection.Output;
            arParms[10] = new SqlParameter("@IsLoggedIn", SqlDbType.Bit, 1);
            arParms[10].Direction = ParameterDirection.Output;
            arParms[11] = new SqlParameter("@CRefNo", SqlDbType.Int);
            arParms[11].Direction = ParameterDirection.Output;
            arParms[12] = new SqlParameter("@Active", SqlDbType.Bit, 1);
            arParms[12].Direction = ParameterDirection.Output;
            arParms[13] = new SqlParameter("@isSuperUser", SqlDbType.Bit, 1);
            arParms[13].Direction = ParameterDirection.Output;
            arParms[14] = new SqlParameter("@NoOfAttepmts", SqlDbType.Int);
            arParms[14].Direction = ParameterDirection.Output;
            arParms[15] = new SqlParameter("@IsAccLokOut", SqlDbType.Bit, 1);
            arParms[15].Value = IsAccLockOut;
            arParms[16] = new SqlParameter("@UserExists", SqlDbType.Bit, 1);
            arParms[16].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spAuthenticateClient", arParms);


            if (!string.IsNullOrEmpty(Convert.ToString(arParms[3].Value)))
            {
                fullname = arParms[4].Value + "";
                //Session.Timeout = 90;
                Session["ClientUsername"] = arParms[0].Value;
                Session["ClientName"] = fullname;
                Session["ClientID"] = arParms[3].Value;
                Session["ExportToExcel"] = arParms[6].Value.ToString().Trim();
                Session["ClientCredential"] = arParms[1].Value;
                Session["ContractNumbers"] = arParms[8].Value.ToString().Trim();
                Session["ShowDashboardLink"] = arParms[9].Value.ToString().Trim();
                //Session["IsClientLoggedIn"] = arParms[10].Value;
                Session["ClientRefNo"] = arParms[7].Value;
                addcookies(Convert.ToString(arParms[0].Value), Convert.ToString(arParms[1].Value), "client");
                addcookie("ClientNo", Convert.ToString(arParms[6].Value));
                Session["LoginInfo"] = arParms[3].Value;
                Session["ReferenceNumber"] = arParms[11].Value.ToString().Trim();
                Session["SuperUser"] = Convert.ToString(arParms[13].Value);
                //Session["NoOfAttempts"] = Convert.ToString(arParms[14].Value);

              
                _valid = true;
            }

            Session["CliUserNotExists"] = arParms[16].Value;
            Session["IsClientLoggedIn"] = (!string.IsNullOrEmpty(arParms[10].Value.ToString())) ? arParms[10].Value : false;
            Session["NoOfAttempts"] = (!string.IsNullOrEmpty(arParms[14].Value.ToString())) ? arParms[14].Value : 0;

        }
        catch (Exception ex)
        {
            Session["ClientUsername"] = null;
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        return _valid;
    }
  protected bool LoginProviderWithConcur(string _username, string _password, string _lcPassword, bool IsLockedOut)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        Session["HashProviderPassKey"] = _password;
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }
            SqlParameter[] arParms = arParms = new SqlParameter[17];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 50);
            arParms[0].Value = _username;
            arParms[1] = new SqlParameter("@HashPassword", SqlDbType.NVarChar, 64);
            arParms[1].Value = _password;
            arParms[2] = new SqlParameter("@ProviderID", SqlDbType.Char, 20);
            arParms[2].Direction = ParameterDirection.Output;
            arParms[3] = new SqlParameter("@ProviderName", SqlDbType.NVarChar, 200);
            arParms[3].Direction = ParameterDirection.Output;
            arParms[4] = new SqlParameter("@ProviderFax", SqlDbType.NVarChar, 200);
            arParms[4].Direction = ParameterDirection.Output;
            arParms[5] = new SqlParameter("@ProviderNo", SqlDbType.Int);
            arParms[5].Direction = ParameterDirection.Output;
            arParms[6] = new SqlParameter("@SwipCardEnabled", SqlDbType.Bit,1);
            arParms[6].Direction = ParameterDirection.Output;
            arParms[7] = new SqlParameter("@PreAuthAllowed", SqlDbType.Bit,1);
            arParms[7].Direction = ParameterDirection.Output;
            arParms[8] = new SqlParameter("@HRMEnabled", SqlDbType.Bit,1);
            arParms[8].Direction = ParameterDirection.Output;
            arParms[9] = new SqlParameter("@pRefNo", SqlDbType.Int);
            arParms[9].Direction = ParameterDirection.Output;
            arParms[10] = new SqlParameter("@Active", SqlDbType.Bit, 1);
            arParms[10].Direction = ParameterDirection.Output;
            arParms[11] = new SqlParameter("@isSuperUser", SqlDbType.Bit, 1);
            arParms[11].Direction = ParameterDirection.Output;
            arParms[12] = new SqlParameter("@IsProLoggedIn", SqlDbType.Bit, 1);
            arParms[12].Direction = ParameterDirection.Output;
            arParms[13] = new SqlParameter("@NoOfAttepmts", SqlDbType.Int);
            arParms[13].Direction = ParameterDirection.Output;
            arParms[14] = new SqlParameter("@IsAccLokOut", SqlDbType.Bit, 1);
            arParms[14].Value = IsLockedOut;
            arParms[15] = new SqlParameter("@LCPassword", SqlDbType.NVarChar, 64);
            arParms[15].Value = _lcPassword.Trim();
            arParms[16] = new SqlParameter("@UserExists", SqlDbType.Bit, 1);
            arParms[16].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spAuthenticateProvider", arParms);
            if (!string.IsNullOrEmpty(Convert.ToString(arParms[2].Value.ToString().Trim())))
            {
                addcookies(Convert.ToString(arParms[0].Value), Convert.ToString(arParms[1].Value), "provider");
                fullname = arParms[3].Value + "";
                //Session.Timeout = 90;
                Session["ProviderName"] = fullname;
                Session["ProviderID"] = arParms[2].Value.ToString().Trim();
                Session["LoginInfo"] = arParms[2].Value.ToString().Trim();
                Session["ProviderUserName"] = _username.Trim();
                Session["ProviderFax"] = arParms[4].Value + "";
                addcookie("ProviderNo", Convert.ToString(arParms[5].Value));
                Session["SwipCardEnabled"] = arParms[6].Value.ToString().Trim();
                Session["PreAuthAllowed"] = arParms[7].Value.ToString().Trim();
                Session["HRMEnabled"] = arParms[8].Value.ToString().Trim();
                Session["pRefNo"] = arParms[9].Value.ToString().Trim();

                BLCommon.PreAuthAllowed = (!string.IsNullOrEmpty(arParms[7].Value.ToString())) ? Convert.ToBoolean(arParms[7].Value.ToString().Trim()) : false;
                BLCommon.HRMEnabled = (!string.IsNullOrEmpty(arParms[8].Value.ToString())) ? Convert.ToBoolean(arParms[8].Value.ToString().Trim()) : false;
                Session["SuperUser"] = Convert.ToString(arParms[11].Value);
              

                _valid = true;
            }
            Session["ProUserNotExists"] = arParms[16].Value;
            Session["IsProLoggedIn"] = (!string.IsNullOrEmpty(arParms[12].Value.ToString())) ? arParms[12].Value : false;
            Session["NoOfAttempts"] = (!string.IsNullOrEmpty(arParms[13].Value.ToString())) ? arParms[13].Value : 0;
        }
        catch (Exception ex)
        {
            Session["ProviderUserName"] = null;
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
        }
        finally
        {
           
            if (connection != null)
                connection.Dispose();
        }
        return _valid;
    }
	
	
    protected bool LoginProvider(string _username, string _password)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }

            // Set up parameters (2 input and 3 output) 
            SqlParameter[] arParms = new SqlParameter[12];

            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = txtUsername.Text.Trim();
            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 30);
            arParms[1].Value = txtPassword.Text.Trim();
            arParms[2] = new SqlParameter("@ProviderID", SqlDbType.Char, 20);
            arParms[2].Direction = ParameterDirection.Output;
            arParms[3] = new SqlParameter("@ProviderName", SqlDbType.NVarChar, 200);
            arParms[3].Direction = ParameterDirection.Output;
            arParms[4] = new SqlParameter("@ProviderFax", SqlDbType.NVarChar, 200);
            arParms[4].Direction = ParameterDirection.Output;
            arParms[5] = new SqlParameter("@ProviderNo", SqlDbType.Int);
            arParms[5].Direction = ParameterDirection.Output;
            arParms[6] = new SqlParameter("@SwipCardEnabled", SqlDbType.Bit);
            arParms[6].Direction = ParameterDirection.Output;
            arParms[7] = new SqlParameter("@PreAuthAllowed", SqlDbType.Bit);
            arParms[7].Direction = ParameterDirection.Output;
            arParms[8] = new SqlParameter("@HRMEnabled", SqlDbType.Bit);
            arParms[8].Direction = ParameterDirection.Output;            
            arParms[9] = new SqlParameter("@pRefNo", SqlDbType.Int);
            arParms[9].Direction = ParameterDirection.Output;
            arParms[10] = new SqlParameter("@Active", SqlDbType.Bit, 1);
            arParms[10].Direction = ParameterDirection.Output;
            arParms[11] = new SqlParameter("@isSuperUser", SqlDbType.Bit, 1);
            arParms[11].Direction = ParameterDirection.Output;
            //SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spValidateProvider", arParms);
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spValidateProviderPA_New", arParms);

            // Display results in text box using the values of output parameters	
            fullname = arParms[3].Value + ""; //, " + arParms[3].Value + ", " + arParms[4].Value ;

            //Session value intitiated to Be used for the entire session.
            Session.Timeout = 90;
            Session["ProviderName"] = fullname;
            Session["ProviderFax"] = arParms[4].Value + "";
            Session["ProviderUserName"] = txtUsername.Text.Trim(); // fullname;
            Session["ProviderID"] = arParms[2].Value.ToString().Trim();
            Session["PreAuthAllowed"] = arParms[7].Value.ToString().Trim();
            Session["HRMEnabled"] = arParms[8].Value.ToString().Trim();
            Session["pRefNo"] = arParms[9].Value.ToString().Trim();
            Session["ReferenceNumber"] = arParms[9].Value.ToString().Trim();
            BLCommon.PreAuthAllowed = Convert.ToBoolean(arParms[7].Value.ToString().Trim());
            BLCommon.HRMEnabled = Convert.ToBoolean(arParms[8].Value.ToString().Trim());
            Session["SwipCardEnabled"] = arParms[6].Value.ToString().Trim();
            addcookies(Convert.ToString(arParms[0].Value), Convert.ToString(arParms[1].Value), "provider");
            addcookie("ProviderNo", Convert.ToString(arParms[5].Value));
            Session["LoginInfo"] = arParms[2].Value.ToString().Trim();
            Session["SuperUser"] = Convert.ToString(arParms[11].Value); 
            if (!string.IsNullOrEmpty(Convert.ToString(arParms[2].Value.ToString().Trim())))
            {
                _valid = true;
            }
            else
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
            }
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        return _valid;
    }

    protected void addcookies(string username, string password, string _type)
    {

    }

    //private SqlConnection GetConnection(string connectionString)
    //{
    //    SqlConnection connection = new SqlConnection(connectionString);
    //    connection.Open();
    //    return connection;
    //}

    protected bool LoginBroker(string _username, string _password)
    {
        bool _valid = false;
        SqlConnection connection = null;
        string fullname = "";
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                _valid = false;
            }

            // Set up parameters (2 input and 3 output) 
            SqlParameter[] arParms = new SqlParameter[7];
            arParms[0] = new SqlParameter("@UserName", SqlDbType.NVarChar, 30);
            arParms[0].Value = _username;
            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 30);
            arParms[1].Value = _password;
            arParms[2] = new SqlParameter("@BrokerNo", SqlDbType.Char, 8);
            arParms[2].Direction = ParameterDirection.Output;
            arParms[3] = new SqlParameter("@BrokerName", SqlDbType.NVarChar, 200);
            arParms[3].Direction = ParameterDirection.Output;

            arParms[4] = new SqlParameter("@BrokerNotes", SqlDbType.NVarChar, 500);
            arParms[4].Direction = ParameterDirection.Output;
            arParms[5] = new SqlParameter("@ExportToExcel", SqlDbType.Bit, 1);
            arParms[5].Direction = ParameterDirection.Output;
            arParms[6] = new SqlParameter("@BrokerFullName", SqlDbType.NVarChar, 200);
            arParms[6].Direction = ParameterDirection.Output;


            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spValidateBroker", arParms);

            // Display results in text box using the values of output parameters	
            fullname = arParms[6].Value + ""; //, " + arParms[3].Value + ", " + arParms[4].Value ;

            //Session value intitiated to Be used for the entire session.
            Session.Timeout = 90;
            Session["BrokerName"] = arParms[3].Value;
            Session["BrokerFullName"] = fullname;
            Session["BrokerNo"] = arParms[2].Value;
            Session["BrokerUsername"] = arParms[0].Value;
            Session["ExportToExcel"] = arParms[5].Value.ToString().Trim();
            Session["LoginInfo"] = arParms[2].Value;


            addcookies(Convert.ToString(arParms[0].Value), Convert.ToString(arParms[1].Value), "broker");
            addcookie("BrokerNo", Convert.ToString(arParms[2].Value));
            if (!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
            {
                _valid = true;
            }
            else
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
            }
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "Authentication Failed. Incorrect User Name or Password. Please try again.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        return _valid;
    }

    private void UpdateBrowserDetails(string BrowserName, string RefNo, string Type)
    {
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[9];

                arParms[0] = new SqlParameter("@BrowserName", SqlDbType.NVarChar, 100);
                arParms[0].Value = BrowserName;
                arParms[1] = new SqlParameter("@Prefno", SqlDbType.Int);
                arParms[1].Value = Int32.Parse(RefNo.Trim());
                arParms[2] = new SqlParameter("@Type", SqlDbType.NVarChar, 50);
                arParms[2].Value = Type;
                //SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spValidateProvider", arParms);
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "[DetectBrowser]", arParms);

            }
        }
        catch (Exception ex)
        {

        }
    }

    private void SetClientLoginRedirect(string userName, string pwd, string lcPWD)
    {
        bool IsAccLokOut = (ConfigurationManager.AppSettings["isClientAccountLockedEnabled"].ToString().ToUpper() == "TRUE") ? true : false;
        bool IsConCurrLogIn = (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y") ? true : false;
        var isAuthenticated = LoginClientWithConcur(userName, pwd, lcPWD, IsAccLokOut);

        int NoOfAttempts = Convert.ToInt32(Session["NoOfAttempts"]);
        var IsCliLoggIn = Convert.ToBoolean(Session["IsClientLoggedIn"]);

        var isUsernNotExists = Convert.ToBoolean(Session["CliUserNotExists"]);

        if (isUsernNotExists)
        {
            lblError.ForeColor = System.Drawing.Color.Red;
            lblError.Text = "Authentication Failed. Incorrect Login in ID or Password. Please try again.";
            return;
        }


        if (IsAccLokOut)
        {

            if (NoOfAttempts == 0)
            {
                txtPassword.Text = "";
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "You have exceeded the number of password attempts allowed, please use the option (Forget password) to retrieve your password/لقد تجاوزت عدد المحاولات المسموح بها لكلمة المرور، يرجى استخدام خيار (نسيت كلمة المرور) لإستعادة كلمة المرور";
                return;
            }
        }

        if (isAuthenticated && IsConCurrLogIn)
        {
            if (IsCliLoggIn)
            {
                lblConcurMsg.Text = "Client " + Convert.ToString(Session["ClientUsername"]) + " is already logged in to BUPA Online Services.";
                lblConcurMsgAr.Text = "  .متصل بخدمات بوبا العربية وعلى قيد الاستخدام حالياً " + Convert.ToString(Session["ClientUsername"]);
                int currentID = BLCommon.InsertClientLogIn(Convert.ToString(Session["ClientUsername"]));
                Session["CurrentLogID"] = currentID;
                divPopupConcurrentLogin.Visible = true;
                return;
            }
        }

        if (isAuthenticated)
        {
            HttpCookie usrCookie = Request.Cookies["userLogin"];
            String RefNo = usrCookie.Values["UserID"];
            UpdateBrowserDetails(Convert.ToString(Session["browserName"]), RefNo, "Client");
            BLCommon.UpdateClientLoginStatus(Convert.ToInt32(Session["ClientRefNo"]), true);
            int currentID = BLCommon.InsertClientLogIn(Convert.ToString(Session["ClientUsername"]));
            Session["CurrentLogID"] = currentID;
            if (Session["ContractNumbers"] != null && Session["ContractNumbers"].ToString().Contains(","))
            {
                Response.Redirect("client/MasterGS.aspx");
            }
            else
            {
                Response.Redirect("client/main.aspx", false);
            }
        }
        else
        {
            lblError.ForeColor = System.Drawing.Color.Red;
            lblError.Text = "Authentication Failed. Incorrect Login in ID or Password. Please try again.";

        }


    }

    private void SetProviderLoginRedirect(string proUserName, string proPWD, string lcProPWD)
    {

        bool IsAccLokOut = (ConfigurationManager.AppSettings["isProviderAccountLockedEnabled"].ToString().ToUpper() == "TRUE") ? true : false;
        bool IsConCurrLogIn = (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y") ? true : false;
        var isAuthenticated = LoginProviderWithConcur(proUserName, proPWD, lcProPWD, IsAccLokOut);

        int NoOfAttempts = Convert.ToInt32(Session["NoOfAttempts"]);
        var IsCliLoggIn = Convert.ToBoolean(Session["IsProLoggedIn"]);


        var isUsernNotExists = Convert.ToBoolean(Session["ProUserNotExists"]);

        if (isUsernNotExists)
        {
            lblError.ForeColor = System.Drawing.Color.Red;
            lblError.Text = "Authentication Failed. Incorrect Login in ID or Password. Please try again.";
            return;
        }

        if (IsAccLokOut)
        {
            if (NoOfAttempts == 0)
            {
                txtPassword.Text = "";
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "You have exceeded the number of password attempts allowed, please use the option (Forget password) to retrieve your password/لقد تجاوزت عدد المحاولات المسموح بها لكلمة المرور، يرجى استخدام خيار (نسيت كلمة المرور) لإستعادة كلمة المرور";
                return;
            }
        }

        if (isAuthenticated && IsConCurrLogIn)
        {
            if (IsCliLoggIn)
            {
                lblConcurMsg.Text = "Provider " + Convert.ToString(Session["ProviderUserName"]) + " is already logged in to BUPA Online Services.";
                lblConcurMsgAr.Text = "  .متصل بخدمات بوبا العربية وعلى قيد الاستخدام حالياً " + Convert.ToString(Session["ProviderUserName"]);
                int currentID = BLCommon.InsertProviderLogIn(Convert.ToString(Session["ProviderUserName"]));
                Session["ProCurrentLogID"] = currentID;
                divPopupConcurrentLogin.Visible = true;
                return;
            }
        }

        if (isAuthenticated)
        {
            UpdateBrowserDetails(Session["browserName"].ToString(), Session["pRefNo"].ToString(), "Provider");
            BLCommon.UpdateProviderLoginStatus(Convert.ToInt32(Session["pRefNo"]), true);
            int currentID = BLCommon.InsertProviderLogIn(Convert.ToString(Session["ProviderUserName"]));
            Session["ProCurrentLogID"] = currentID;

            if (!string.IsNullOrEmpty(sRefererURL) && sRefererURL.Contains("MemberVerification"))
            {
                Response.Redirect(sRefererURL);
            }
            else
            {
                Response.Redirect("provider/main.aspx", false);
            }
        }
        else
        {

            lblError.ForeColor = System.Drawing.Color.Red;
            lblError.Text = "Authentication Failed. Incorrect Login in ID or Password. Please try again.";

        }


    }


 
    //private void UpdateClientLoginStatus(int cRefNO, bool loginStatus)
    //{
    //    try
    //    {
    //        using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
    //        {
    //            SqlParameter[] arParms = new SqlParameter[2];
    //            arParms[0] = new SqlParameter("@CRefNo", SqlDbType.Int);
    //            arParms[0].Value = cRefNO;
    //            arParms[1] = new SqlParameter("@IsLoggedIn", SqlDbType.Bit);
    //            arParms[1].Value = loginStatus;
    //            SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "UpdateClientLoginStatus", arParms);
    //        }
    //    }
    //    catch(Exception ex)
    //    {

    //    }
    //}

    protected void btnTryAgain_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
        Response.Redirect("default.aspx");
    }

   protected void btnConcurContinue_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientUsername"])))
            {
               
                BLCommon.UpdateConcurClientLogOff(Convert.ToString(Session["ClientUsername"]), Convert.ToInt32(Convert.ToString(Session["CurrentLogID"])));
                if (Session["ContractNumbers"] != null && Session["ContractNumbers"].ToString().Contains(","))
                {
                    Response.Redirect("client/MasterGS.aspx");
                }
                else
                {
                    Response.Redirect("client/main.aspx", false);
                }
            }
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["ProviderUserName"])))
            {
                BLCommon.UpdateConcurProviderLogOffTime(Convert.ToString(Session["ProviderUserName"]),Convert.ToInt32(Session["ProCurrentLogID"]));
                if (!string.IsNullOrEmpty(sRefererURL) && sRefererURL.Contains("MemberVerification"))
                {
                    Response.Redirect(sRefererURL);
                }
                else
                {
                    Response.Redirect("provider/main.aspx", false);
                }
                // SetProviderLoginRedirect(Convert.ToString(Session["ProviderUserName"]), Convert.ToString(Session["HashProviderPassKey"]), Convert.ToString(Session["LCHashProviderPassKey"]));
            }

        }
        catch (Exception ex)
        {

        }
    }
	//Original Code
    //if (LoginClient(txtUsername.Text.Trim(), txtPassword.Text.Trim()))
    //{
    //    HttpCookie usrCookie = Request.Cookies["userLogin"];
    //    String RefNo = usrCookie.Values["UserID"];
    //    UpdateBrowserDetails(Convert.ToString(Session["browserName"]), RefNo, "Client");
    //    if (Session["ContractNumbers"] != null && Session["ContractNumbers"].ToString().Contains(","))
    //    {
    //        Response.Redirect("client/MasterGS.aspx");
    //    }
    //    else
    //    {
    //        Response.Redirect("client/main.aspx");
    //    }
    //}
}


