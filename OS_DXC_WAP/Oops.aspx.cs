﻿using Bupa.Core.Logging;
using System;
using System.Text.RegularExpressions;

public partial class Oops : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       Regex tagRegex = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");
        string fullname1 = Request.QueryString["aspxerrorpath"];
        bool hasTags = tagRegex.IsMatch(fullname1);
        if (hasTags)
            Response.Redirect("https://www.google.com.sa/", true);
    }
}