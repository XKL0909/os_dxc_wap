/**
 * Copyright (C) 2002-2003, CodeHouse.com. All rights reserved.
 * CodeHouse(TM) is a registered trademark.
 *
 * THIS SOURCE CODE MAY BE USED FREELY PROVIDED THAT
 * IT IS NOT MODIFIED OR DISTRIBUTED, AND IT IS USED
 * ON A PUBLICLY ACCESSIBLE INTERNET WEB SITE.
 *
 * Script Name: Cool Redirect
 *
 * You can obtain this script at http://www.codehouse.com
 */

function coolRedirect(url, msg)
{
   var TARG_ID = "COOL_REDIRECT";
   var DEF_MSG = "If this page doesn't redirect in five seconds, please click <a href='" + url + "'>here</a>. Redirecting";

   if( ! msg )
   {
      msg = DEF_MSG;
   }

   if( ! url )
   {
      throw new Error('You didn\'t include the "url" parameter');
   }


   var e = document.getElementById(TARG_ID);

   if( ! e )
   {
      throw new Error('"COOL_REDIRECT" element id not found');
   }

   var cTicks = parseInt(e.innerHTML);

   var timer = setInterval(function () {
       if (cTicks) {
           e.innerHTML = --cTicks;
           DEF_MSG = DEF_MSG + " . ";
           document.body.innerHTML = DEF_MSG;
       }
       else {
           clearInterval(timer);
           document.body.innerHTML = msg;
           location = url;
       }

   }, 1000);
}