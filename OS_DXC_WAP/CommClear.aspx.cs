﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CommClear : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) ||
            !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])) || !string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])) ||
            !string.IsNullOrEmpty(Convert.ToString(Request.Cookies["userLogin"])))
        {
            HttpCookie usrCookie = new HttpCookie("userLogin");
            usrCookie.Domain = ".bupa.com.sa";
            usrCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(usrCookie);
            //Session.Abandon();
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
                {
                    BLCommon.UpdateClientLoginStatus(Convert.ToInt32(Session["ClientRefNo"]), false);
                    BLCommon.UpdateClientLogOff(Convert.ToInt32(Session["CurrentLogID"]));
                }
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
                {
                    BLCommon.UpdateProviderLoginStatus(Convert.ToInt32(Session["pRefNo"]), false);
                    BLCommon.UpdateProviderLogOffTime(Convert.ToInt32(Session["ProCurrentLogID"]));
                }
            }
            Session.Clear();
            Response.Redirect("~/default.aspx");
        }
        else
        {
            Response.Redirect("~/default.aspx");
        }
   

    }
}