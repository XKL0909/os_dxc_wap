using System;
using System.Collections;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using log4net;
using Bupa.Core.Logging;

using Bupa.OSWeb.Helper;
using Utility.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using OS_DXC_WAP.CaesarWS;
using Bupa.Core.Logging;

public partial class UploaderRevamp : System.Web.UI.UserControl
{
    private ILog _log = LogManager.GetLogger(typeof(UploaderRevamp));

    private UploadManager _uploadManager;
    private DataSet _uploadedFileSet = new DataSet();

    private string _sessionID = string.Empty;
    private string _userID = string.Empty;
    private string _uploadCategory = string.Empty;
    private string _allowedExtensions = string.Empty;

    private bool _fileSizeDefined = false;
    private long _maxFileSize;
    private string _folder;
    private bool _fileSetSizeDefined = false;
    private long _maxFileSetByteSize;
    private Hashtable hasQueryValue;
    // private string UploadCategory;
    public string RequestID;
    public string isresub;
    public int SEQ;
    public string t;
    public string BatchUpload;


    public int FilesCount { get { return dvFiles.Rows.Count; } }

    public string SessionID
    {
        get { return _sessionID; }
        set { _sessionID = value; }
    }

    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    public string UploadCategory
    {
        get { return _uploadCategory; }
        set { _uploadCategory = value; }
    }

    public string AllowedExtensions
    {
        get { return _allowedExtensions; }
        set { _allowedExtensions = value; }
    }

    public long MaxFileSize
    {
        get { return _maxFileSize; }
        set
        {
            _maxFileSize = value;
            _fileSizeDefined = true;
        }
    }

    public long MaxFileSetSize
    {
        get { return _maxFileSetByteSize; }
        set
        {
            _maxFileSetByteSize = value;
            _fileSetSizeDefined = true;
        }

    }

    public Label LblUpload
    {
        get { return lblUpload; }
    }

    public DataSet UploadedFileSet
    {
        get
        {
            if (_uploadedFileSet.Tables.Count == 0)
            {
                // Reload the fileset
                _uploadedFileSet = ShowUploadedList();
            }
            return _uploadedFileSet;
        }
    }

    public UploaderRevamp()
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {

        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }
        Logger.Tracer.WriteMemberExit();
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        //HttpContext.Current.Response.Write("1" + SessionID);
        Logger.Tracer.WriteMemberEntry();
        try
        {

            if (Session["hashQuery"] != null)
            {
                hasQueryValue = (Hashtable)Session["hashQuery"];
                if (hasQueryValue != null && hasQueryValue.Count > 0)
                {
                    UploadCategory = hasQueryValue.ContainsKey("UploadCategory") ? Convert.ToString(hasQueryValue["UploadCategory"]) : string.Empty;
                    RequestID = hasQueryValue.ContainsKey("RequestID") ? Convert.ToString(hasQueryValue["RequestID"]) : string.Empty;
                    isresub = hasQueryValue.ContainsKey("isresub") ? Convert.ToString(hasQueryValue["isresub"]) : string.Empty;
                    SEQ = hasQueryValue.ContainsKey("seq") ? Convert.ToInt32(hasQueryValue["seq"]) : 0;
                    t = hasQueryValue.ContainsKey("t") ? Convert.ToString(hasQueryValue["t"]) : string.Empty;
                    BatchUpload = hasQueryValue.ContainsKey("BatchUpload") ? Convert.ToString(hasQueryValue["BatchUpload"]) : string.Empty;
                    UserID = hasQueryValue.ContainsKey("UserID") ? Convert.ToString(hasQueryValue["UserID"]) : string.Empty;
                }

            }

            _sessionID = RequestID;
            _uploadCategory = UploadCategory;
            _userID = UserID;



            // Stop Caching in IE 
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

            // Stop Caching in Firefox 
            Response.Cache.SetNoStore();

            _folder = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHFOLDER_PATH);

            if (!_fileSetSizeDefined)
            {
                _maxFileSetByteSize = long.Parse(CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSize));
            }
            ////Tracer.WriteLine(ref _log, "_maxFileSetByteSize is: " + _maxFileSetByteSize);

            if (!_fileSizeDefined)
            {
                // Set the max file size that can be uploaded to be 5MB
                _maxFileSize = 5242880;
            }
            //if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["optiontype"])) && Convert.ToString(Request.QueryString["optiontype"]).ToLower() == "class")
            //{
            //    _uploadCategory = Bupa.OSWeb.Helper.UploadCategory.MEMBER_CHGCLASS;
            //}
            // Set the allowed extensions to be uploaded - note that this can be overriden by the host
            if (UploadCategory == "Submit Reimbursement" || UploadCategory == "Submit eClaims")
            {
                _allowedExtensions = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensionseClaims);
            }
            else
            {
                _allowedExtensions = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
            }


            // Set the default values for the upload manager = note that this can also be overriden by the host
            _uploadManager = new UploadManager(_maxFileSetByteSize, _maxFileSize);

            lblUpload.Text = string.Empty;
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }
        Logger.Tracer.WriteMemberExit();
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
			bool isvalidFile = false;
            lblUpload.Text = "";
            if (uploader.HasFile)
            {
                if (UploadCategory == "Submit Reimbursement" || UploadCategory == "Submit eClaims")
                {
                    CheckMimeTypes validExcelfile = new CheckMimeTypes();
                    if (validExcelfile.ValidateExcelFile(uploader.PostedFile))
                    {
                        isvalidFile = true; 
                    }
                    else
                    {
                        lblUpload.Text = "Please select a valid file";
                        return;
                    }
                }
                else
                {
                    Boolean fileOK = false;
                    Boolean mimeOK = false;
                    System.Web.HttpPostedFile file = uploader.PostedFile;
                    CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
                    string mime=findMimeFromDate.CheckFindMimeFromData(file);
                    String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
                    String fileExtension = System.IO.Path.GetExtension(uploader.FileName).ToLower();
                    String[] allowedExtensions = _allowedExtensions.Split(',');
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                            break;
                        }
                    }
                    for (int i = 0; i < allowedMime.Length; i++)
                    {
                        if (mime == allowedMime[i])
                        {
                            mimeOK = true;
                            break;
                        }
                    }
                    if (fileOK && mimeOK)
                    {
                        isvalidFile = true;
                    }
                    else
                    {
                        lblUpload.Text = "Please select a valid file";
                        return;
                    }
                }
                
                if (isvalidFile)
                {
                    string fileName = uploader.FileName;
                    ////Tracer.WriteLine(ref _log, "fileName is: " + fileName);

                    bool grantSave = IsAllowedExtension(fileName);
                    ////Tracer.WriteLine(ref _log, "grantSave is: " + grantSave);
                    if (!grantSave)
                    {
                        // Extension not allowed for upload
                        lblUpload.Text = "Please ensure you upload the correct file type (allowed extensions are: " + _allowedExtensions + ")";
                        return;
                    }

                    // Proceed with saving the file
                    UploadResult uploaded = new UploadResult();
                    if (Session["peauthupload"] != null && Session["peauthupload"].ToString() == "Yes")
                    {
                        uploaded = SaveFileToFolder(fileName, _folder);
                    }
                    else
                    {
                        uploaded = SaveFile(fileName);
                    }

                    ////Tracer.WriteLine(ref _log, "uploaded is: " + uploaded.Result);

                    // Check if the upload was successful
                    if (!uploaded.Result)
                    {
                        // Parse the errors
                        string errors = "Cannot upload this file: ";
                        foreach (string errorMessage in uploaded.ErrorReason)
                        {
                            errors = errors + errorMessage;
                        }

                        // File size has been exceeded
                        lblUpload.Text = errors;
                    }
                }
                else
                {
                    lblUpload.Text = "Please select a valid file";
                }

            }
            else
            {
                lblUpload.Text = "Please select a valid file";
            }
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }
        Logger.Tracer.WriteMemberExit();
    }

    protected void dvFiles_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            string currentCommand = e.CommandName;
            if (e.CommandName == "Delete")
            {
                ////Tracer.WriteLine(ref _log, "Deleting....");
                DataSet d = ShowUploadedList();

                if (d != null)
                {
                    // Two items need to be deleted.
                    // The first is the record in the database and the second is the actual file itself
                    int currentRowIndex = Int32.Parse(e.CommandArgument.ToString());
                    ////int uploadedID = Int32.Parse(dvFiles.DataKeys[currentRowIndex].Value.ToString());
                    int uploadedID = Int32.Parse(dvFiles.DataKeys[currentRowIndex][0].ToString());
                    ////Tracer.WriteLine(ref _log, "uploadedID is: " + uploadedID);

                    // Get the file path to be deleted
                    DataTable t = d.Tables[0];
                    ////Tracer.WriteLine(ref _log, "Fetched datatable for upload deletion");

                    DataRow[] dr = t.Select("UploadedID = " + uploadedID);

                    string deletedFilePath = string.Empty;
                    foreach (DataRow r in dr)
                    {
                        deletedFilePath = r["FilePath"].ToString();

                        ////Tracer.WriteLine(ref _log, "deletedFilePath is: " + deletedFilePath);
                        _uploadManager.Remove(uploadedID, deletedFilePath);
                        break;
                    }

                    //if (deletedFilePath.Length == 0)
                    ////Tracer.WriteLine(ref _log, "Unable to delete uploaded file - uploadedID is: " + uploadedID);
                }
                else
                {
                    lblUpload.Text = "Error retrieving filelist before deletion - please contact support";
                }
            }
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
    }

    protected void dvFiles_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    public void SetupUploader(string functionName, string userID, string allowedExtensions, long maxFileSize)
    {
        this.UserID = userID;
        this.UploadCategory = functionName;

        this.AllowedExtensions = allowedExtensions;
        this.MaxFileSize = maxFileSize;

        // Require the upload manager to deal with uploads
        _uploadManager = new UploadManager(_maxFileSetByteSize, maxFileSize);

        if (Page.IsPostBack && ViewState[UploadPublication.Session_RequestID] != null)
        {
            // Retrieve the requestID for uploads to remember what was uploaded

            ////Tracer.WriteLine(ref _log, "Retrieve the requestID for uploads to remember what was uploaded");

            if (!string.IsNullOrEmpty(Convert.ToString(Request["sess"])))
            {
                this.SessionID = Convert.ToString(Request["sess"]).ToString();
                ////Tracer.WriteLine(ref _log, "Retrieved this.SessionID is: " + this.SessionID);
            }
            else
            {
                if (ViewState[UploadPublication.Session_RequestID] != null)
                {
                    this.SessionID = ViewState[UploadPublication.Session_RequestID].ToString();
                    ////Tracer.WriteLine(ref _log, "Retrieved this.SessionID is: " + this.SessionID);
                }
            }
        }
        else
        {
            // Generate the request ID for the upload
            if (!string.IsNullOrEmpty(Convert.ToString(Request["sess"])))
            {
                this.SessionID = Convert.ToString(Request["sess"]);
            }
            else
            {
                this.SessionID = UploadManager.GetNextRequest();
            }




            ////Tracer.WriteLine(ref _log, "this.SessionID for GetNextRequest is: " + this.SessionID);

            // Store in the ViewState to retrieve on postbacks
            ViewState[UploadPublication.Session_RequestID] = this.SessionID;
        }
    }

    public void Notify(string uploadSessionID, string function, string referenceID, string[] addressees, string fullName, string username)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            // Send an email informing the team about a pre-auth request
            string fromEmail = "Membership_online_doc@bupa.com.sa";

            string subject = function + " reference:" + referenceID + " - attachments added by " + fullName + " (" + username + ")";
            string content = "Please view the attached uploaded documents added by " + fullName + " (" + username + ") for " + function + " reference: " + referenceID;

            // Get the list of attachments for this user
            Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
            DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);

            StringCollection attachments = new StringCollection();
            if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow r in d.Tables[0].Rows)
                {
                    string attachPath = r["VirtualPath"].ToString();
                    attachments.Add(attachPath);
                }
            }
            else
            {
                // No attachments and therefore, nothing to email
                return;
            }

            // Get the addressees
            foreach (string toEmail in addressees)
            {
                SendEmail(fromEmail, toEmail, subject, content, attachments);
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
    }

    private UploadResult SaveFile(string fileName)
    {
        Logger.Tracer.WriteMemberEntry();

        UploadResult uploadResult = new UploadResult();
        uploadResult.Result = false;
        string virtualPath = "";
        string serverPath = string.Empty;

        try
        {
            // Fetch the filename and save to the server
            ////Tracer.WriteLine(ref _log, "fileName is: " + fileName);

            // Get a random name and append to the file
            int seed = DateTime.Now.Millisecond;
            ////Tracer.WriteLine(ref _log, "seed is: " + seed);

            string uniquePrefix = WebPublication.GenerateUniqueID;
            ////Tracer.WriteLine(ref _log, "uniquePrefix is: " + uniquePrefix);
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            // Go ahead and save to the server for the moment
            if (!string.IsNullOrEmpty(Convert.ToString(Session["PreauthUpload"])))
            {
                string strPreauth = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHFOLDER_PATH);
                virtualPath = "~/" + strPreauth + "/" + uniquePrefix + "_" + fileName;
            }
            else if (url.ToLower().Contains("reimbursementform"))
            {
                serverPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentServerPath);
                //////string strPreauth = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHFOLDER_PATH);
                //////virtualPath = "~/ClaimsUpload/" + uniquePrefix + "_" + fileName;
            }
            else
            {
                virtualPath = "~/Uploads/" + uniquePrefix + "_" + fileName;
            }

            string destinationPath = string.Empty;

            if (url.ToLower().Contains("reimbursementform"))
            {
                using (NetworkShareAccesser.Access(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServer"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServerDomain"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServerUser"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServerPassword"])))
                {
                    destinationPath = serverPath + uniquePrefix + "_" + fileName;
                    uploader.SaveAs(serverPath + uniquePrefix + "_" + fileName);
                    virtualPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentVirtualPath) + "/" + uniquePrefix + "_" + fileName;

                }
                //if(string.IsNullOrEmpty(_uploadCategory.Trim()))
                //// if SessionID is null, setting reimbursement session id by sakthi on  29-Mar-2016
                ////Start
                _uploadCategory = string.IsNullOrEmpty(_uploadCategory.Trim()) ? Bupa.OSWeb.Helper.UploadCategory.Submit_Reimbursement : _uploadCategory;
                if (string.IsNullOrEmpty(_sessionID.Trim()))
                {
                    if (dvFiles != null && dvFiles.Rows.Count > 0)
                    {
                        foreach (GridViewRow row in dvFiles.Rows)
                        {
                            string theDataKey = dvFiles.DataKeys[row.RowIndex][1].ToString();
                            if (!string.IsNullOrEmpty(theDataKey.Trim()))
                            {
                                this.SessionID = theDataKey;
                                _sessionID = this.SessionID;
                                break;
                            }
                        }
                        _sessionID = string.IsNullOrEmpty(_sessionID.Trim()) ? UploadManager.GetNextRequest() : _sessionID;
                    }
                    else
                    {
                        this.SessionID = UploadManager.GetNextRequest();
                        _sessionID = this.SessionID;
                    }
                    ViewState[UploadPublication.Session_RequestID] = this.SessionID;
                }

                ////End
            }
            else
            {
                ////Tracer.WriteLine(ref _log, "virtualPath is: " + virtualPath);
                destinationPath = Server.MapPath(virtualPath);
                ////Tracer.WriteLine(ref _log, "destinationPath is: " + destinationPath);

                // Save the file & fetch the session ID as this will be used as part of the upload key
                uploader.SaveAs(destinationPath);
                ////Tracer.WriteLine(ref _log, "_sessionID is: " + _sessionID);
            }

            ////string destinationPath = Server.MapPath(virtualPath);
            ////uploader.SaveAs(destinationPath);
            //if(string.IsNullOrEmpty(_uploadCategory.Trim()))
            //// if SessionID is null, setting reimbursement session id by sakthi on  29-Mar-2016
            ////Start
            //if (string.IsNullOrEmpty(Convert.ToString(Session["UploadSessionId"])))
            //{
            //    if (dvFiles != null && dvFiles.Rows.Count > 0)
            //    {
            //        foreach (GridViewRow row in dvFiles.Rows)
            //        {
            //            string theDataKey = dvFiles.DataKeys[row.RowIndex][1].ToString();
            //            if (!string.IsNullOrEmpty(theDataKey.Trim()))
            //            {
            //                this.SessionID = theDataKey;
            //                _sessionID = this.SessionID;
            //                break;
            //            }
            //        }
            //        _sessionID = string.IsNullOrEmpty(_sessionID.Trim()) ? UploadManager.GetNextRequest() : _sessionID;
            //        this.SessionID = _sessionID;
            //    }
            //    else
            //    {
            //        this.SessionID = UploadManager.GetNextRequest();
            //        _sessionID = this.SessionID;
            //    }
            //    ViewState[UploadPublication.Session_RequestID] = this.SessionID;
            //    Session["UploadSessionId"] = this.SessionID;
            //}
            //else{
            //    _sessionID = Convert.ToString(Session["UploadSessionId"]);
            //}

            ////End
            uploadResult = _uploadManager.Add(_sessionID, _uploadCategory, destinationPath, virtualPath, fileName, _userID);
            ////Tracer.WriteLine(ref _log, "uploadResult is: " + uploadResult.Result);
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }

        Logger.Tracer.WriteMemberExit();
        return uploadResult;
    }


    private UploadResult SaveFileToFolder(string fileName, string Folder)
    {
        Logger.Tracer.WriteMemberEntry();
        fileName = fileName.Trim();
        Folder = Folder.Trim();
        UploadResult uploadResult = new UploadResult();
        uploadResult.Result = false;

        try
        {
            // Fetch the filename and save to the server
            ////Tracer.WriteLine(ref _log, "fileName is: " + fileName);

            // Get a random name and append to the file
            int seed = DateTime.Now.Millisecond;
            ////Tracer.WriteLine(ref _log, "seed is: " + seed);

            string uniquePrefix = WebPublication.GenerateUniqueID;
            ////Tracer.WriteLine(ref _log, "uniquePrefix is: " + uniquePrefix);

            // Go ahead and save to the server for the moment
            string virtualPath = "~/" + Folder + "/" + uniquePrefix + "_" + fileName;
            ////Tracer.WriteLine(ref _log, "virtualPath is: " + virtualPath);

            string destinationPath = Server.MapPath(virtualPath);
            ////Tracer.WriteLine(ref _log, "destinationPath is: " + destinationPath);

            // Save the file & fetch the session ID as this will be used as part of the upload key
            uploader.SaveAs(destinationPath);
            ////Tracer.WriteLine(ref _log, "_sessionID is: " + _sessionID);
            uploadResult = _uploadManager.Add(_sessionID, _uploadCategory, destinationPath, virtualPath, fileName, _userID);
            ////Tracer.WriteLine(ref _log, "uploadResult is: " + uploadResult.Result);
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }

        Logger.Tracer.WriteMemberExit();
        return uploadResult;
    }

    private DataSet ShowUploadedList()
    {
        Logger.Tracer.WriteMemberEntry();

        DataSet ds = null;
        dvFiles.DataSource = null;
        try
        {
            // Get the table with the latest uploaded fileset
            ds = _uploadManager.GetUploadedFileSet(_sessionID, _uploadCategory);


            ////dvFiles.DataSource = ds;
            dvFiles.DataSource = ds.Tables[0];
            dvFiles.DataBind();

            dvFiles.Visible = true;
            _uploadedFileSet = ds;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }

        Logger.Tracer.WriteMemberExit();
        return ds;

    }

    private bool IsAllowedExtension(string file)
    {
        Logger.Tracer.WriteMemberEntry();

        bool allowed = false;
        try
        {
            // Allowed extensions are a comma separated list
            string fileExtension = file.Substring(file.LastIndexOf(".") + 1);
            ////Tracer.WriteLine(ref _log, "fileExtension is: " + fileExtension);

            if (_allowedExtensions.ToLower().Contains(fileExtension.ToLower()))
                allowed = true;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }
        ////Tracer.WriteLine(ref _log, "allowed is: " + allowed);

        Logger.Tracer.WriteMemberExit();
        return allowed;
    }

    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        try
        {
            // Setup the mail message
            MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

            // Deal with the attachments
            if (attachmentPaths != null)
            {
                foreach (string path in attachmentPaths)
                {
                    // Create the attachment
                    try
                    {
                        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(path));

                        // Add the attachment
                        mail.Attachments.Add(attachment);
                    }
                    catch (Exception ex) { }////Tracer.WriteLine(ref _log, "error attachments  is: " + ex.Message); }
                }
            }

            string smtpHost = WebPublication.EmailRelayAddress();
            int smtpPort = WebPublication.EmailRelayPort();

            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
            mail.IsBodyHtml = true;
            smtp.Send(mail);
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
    }




    public string[] SupportDocuments(string uploadSessionID, string function)
    {
        string[] _supportDocuments;
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);
        int _count = 0;
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            _supportDocuments = new string[d.Tables[0].Rows.Count];
            WebPublication _webPub = new WebPublication();
            foreach (DataRow r in d.Tables[0].Rows)
            {

                string attachPath = r["VirtualPath"].ToString();
                attachPath = attachPath.Replace("/", "\\");
                attachPath = attachPath.Replace("~", "");
                attachPath = attachPath.Replace("\\Uploads\\", "\\");

                _supportDocuments[_count] = _webPub.DocLocation() + attachPath;
                _count++;
            }
            return _supportDocuments;
        }
        else
        {
            return new string[0];
        }

    }


    public PreauthDoc_DN[] preauthSupportDocuments(string uploadSessionID, string function, long Transaction)
    {

        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);
        OS_DXC_WAP.CaesarWS.PreauthDoc_DN[] _supportDocuments = new OS_DXC_WAP.CaesarWS.PreauthDoc_DN[0];
        int _count = 0;
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            _supportDocuments = new PreauthDoc_DN[d.Tables[0].Rows.Count];
            WebPublication _webPub = new WebPublication();
            foreach (DataRow r in d.Tables[0].Rows)
            {

                string attachPath = r["VirtualPath"].ToString();
                attachPath = attachPath.Replace("/", "\\");
                attachPath = attachPath.Replace("~", "");
                attachPath = attachPath.Replace("\\preauthUpload\\", "\\");
                _supportDocuments[_count] = new PreauthDoc_DN();
                _supportDocuments[_count].DOC_PATH = _webPub.DocLocation() + attachPath;
                _supportDocuments[_count].sql_type = "CSR.PREA_REQ_DOC01";
                _supportDocuments[_count].TXN_ID = Transaction;
                _count++;
            }
            return _supportDocuments;
        }
        else
        {
            return null;
        }

    }

    public void ClaerUploader()
    {
        try
        {
            dvFiles.DataSource = null;
            dvFiles.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }
}