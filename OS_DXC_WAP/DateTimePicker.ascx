<%@ Control Language="VB" AutoEventWireup="false" Inherits="DateTimePicker" Codebehind="DateTimePicker.ascx.vb" %>
    Date:<asp:TextBox CssClass="textbox" ID="DateTextBox" runat="server" Width="150" autocomplete="off" />
        <br />
        <asp:Panel ID="Panel1" runat="server" CssClass="popupControl" BorderStyle="solid" BackColor="AliceBlue"
            BorderColor="darkblue" BorderWidth="1">
            <asp:UpdatePanel runat="server" ID="up1">
                <ContentTemplate>
                    <center>
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlMonth" runat="server" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged"
                            AutoPostBack="true" Width="90">
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList></div>
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlYear" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"
                            AutoPostBack="True" Width="60">
                        </asp:DropDownList></div>
                        <asp:Calendar ID="Calendar1" runat="server" Width="150px" DayNameFormat="Shortest"
                            BackColor="White" BorderColor="#3366CC" CellPadding="0" Font-Names="Verdana"
                            Font-Size="8pt" ForeColor="#003399" OnSelectionChanged="Calendar1_SelectionChanged"
                            ShowNextPrevMonth="False" ShowTitle="False" BorderWidth="1px" FirstDayOfWeek="Sunday"
                            Height="150px" SelectedDate="2007-08-08">
                            <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                            <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                            <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                            <WeekendDayStyle BackColor="#CCCCFF" />
                            <OtherMonthDayStyle ForeColor="#999999" />
                            <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                            <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                            <TitleStyle BackColor="#003399" Font-Size="10pt" BorderColor="#3366CC" Font-Bold="True"
                                BorderWidth="1px" ForeColor="#CCCCFF" Height="25px" />
                        </asp:Calendar>
                        <asp:LinkButton ID="lbtnToday" runat="server"  Text=":: Today"></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lbtnClearDate" runat="server" Text="::Clear Date"></asp:LinkButton>
                        
                    </center>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="DateTextBox"
            PopupControlID="Panel1" Position="Bottom" />
        