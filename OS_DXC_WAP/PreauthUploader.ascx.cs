using Bupa.OSWeb.Helper;
using OS_DXC_WAP.CaesarWS;
using log4net;
using System;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Configuration;
using Tracer = Utility.Diagnostic.FileTrace;



public partial class PreauthUploader : System.Web.UI.UserControl
{
private ILog _log = LogManager.GetLogger(typeof(PreauthUploader));

    private UploadManager _uploadManager;
    private DataSet _uploadedFileSet = new DataSet();

    private string _sessionID = string.Empty;
    private string _userID = string.Empty;
    private string _uploadCategory = string.Empty;
    private string _allowedExtensions = string.Empty;

    private bool _fileSizeDefined = false;
    private long _maxFileSize;
    private string _folder;
    private bool _fileSetSizeDefined = false;
    private long _maxFileSetByteSize;

    public string SessionID
    {
        get { return _sessionID; }
        set { _sessionID = value; }
    }

    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    public string UploadCategory
    {
        get { return _uploadCategory; }
        set { _uploadCategory = value; }
    }

    public string AllowedExtensions
    {
        get { return _allowedExtensions; }
        set { _allowedExtensions = value; }
    }

    public long MaxFileSize
    {
        get { return _maxFileSize; }
        set 
        { 
            _maxFileSize = value;
            _fileSizeDefined = true;
        }
    }

    public long MaxFileSetSize
    {
        get { return _maxFileSetByteSize; }
        set 
        { 
            _maxFileSetByteSize = value;
            _fileSetSizeDefined = true;
        }

    }

    public Label LblUpload
    {
        get { return lblUpload; }
    }

    public DataSet UploadedFileSet
    {
        get
        {
            if (_uploadedFileSet.Tables.Count == 0)
            {
                // Reload the fileset
                _uploadedFileSet = ShowUploadedList();
            }
            return _uploadedFileSet;
        }
    }

    public PreauthUploader()
    {
        Tracer.WriteMemberEntry(ref _log);
        try
        {

        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            //throw;
        }
        Tracer.WriteMemberExit(ref _log);
    }


  
    protected void Page_Load(object sender, EventArgs e)
    {
        Tracer.WriteMemberEntry(ref _log);
        try
        {

           

           
            // Stop Caching in IE 
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

            // Stop Caching in Firefox 
            Response.Cache.SetNoStore();

            _folder = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHFOLDER_PATH);

            if (!_fileSetSizeDefined)
            {
                _maxFileSetByteSize = long.Parse(CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSize));
            }
            Tracer.WriteLine(ref _log, "_maxFileSetByteSize is: " + _maxFileSetByteSize);

            if (!_fileSizeDefined)
            {
                // Set the max file size that can be uploaded to be 5MB
                _maxFileSize = 5242880;
            }

            // Set the allowed extensions to be uploaded - note that this can be overriden by the host
            if (Request["UploadCategory"] == "Submit Reimbursement" || Request["UploadCategory"] == "Submit eClaims")
            {
                _allowedExtensions = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensionseClaims);
            }
            else
            {
                _allowedExtensions = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
            }
            

            // Set the default values for the upload manager = note that this can also be overriden by the host
            _uploadManager = new UploadManager(_maxFileSetByteSize, _maxFileSize);

            lblUpload.Text = string.Empty;
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            //throw;
        }
        Tracer.WriteMemberExit(ref _log);
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        Tracer.WriteMemberEntry(ref _log);
        try
        {
			bool isvalidFile = false;
            if (uploader.HasFile)
            {
                
                 if (Request["UploadCategory"] == "Submit Reimbursement" || Request["UploadCategory"] == "Submit eClaims")
                {
                    CheckMimeTypes validExcelfile = new CheckMimeTypes();
                    if (validExcelfile.ValidateExcelFile(uploader.PostedFile))
                    {
                        isvalidFile = true;
                    }
                    else
                    {
                        lblUpload.Text = "Please select a valid file";
                        return;
                    }
                }
                else
                {
                    Boolean fileOK = false;
                    Boolean mimeOK = false;
                    System.Web.HttpPostedFile file = uploader.PostedFile;
                    CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
                    string mime = findMimeFromDate.CheckFindMimeFromData(file);
                    String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
                    String fileExtension = System.IO.Path.GetExtension(uploader.FileName).ToLower();
                    String[] allowedExtensions = _allowedExtensions.Split(',');
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                            break;
                        }
                    }
                    for (int i = 0; i < allowedMime.Length; i++)
                    {
                        if (mime == allowedMime[i])
                        {
                            mimeOK = true;
                            break;
                        }
                    }
                    if (fileOK && mimeOK)
                    {
                        isvalidFile = true;
                    }
                    else
                    {
                        lblUpload.Text = "Please select a valid file";
                        return;
                    }
                }

                if (isvalidFile)
                {

                    string fileName = uploader.FileName;

                    Tracer.WriteLine(ref _log, "fileName is: " + fileName);

                    bool grantSave = IsAllowedExtension(fileName);
                    Tracer.WriteLine(ref _log, "grantSave is: " + grantSave);
                    if (!grantSave)
                    {
                        // Extension not allowed for upload
                        lblUpload.Text = "Please ensure you upload the correct file type (allowed extensions are: " + _allowedExtensions + ")";
                        return;
                    }

                    // Proceed with saving the file
                    UploadResult uploaded = new UploadResult();
                    uploaded = SaveFileToFolder(fileName, _folder);



                    Tracer.WriteLine(ref _log, "uploaded is: " + uploaded.Result);

                    // Check if the upload was successful
                    if (!uploaded.Result)
                    {
                        // Parse the errors
                        string errors = "Cannot upload this file: ";
                        foreach (string errorMessage in uploaded.ErrorReason)
                        {
                            errors = errors + errorMessage;
                        }

                        // File size has been exceeded
                        lblUpload.Text = errors;
                    }
                }
                else
                {
                    lblUpload.Text = "Please select a valid file";
                }
            }

            else
            {
                lblUpload.Text = "Please select a valid file";
            }
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            //throw;
        }
        Tracer.WriteMemberExit(ref _log);
    }

    protected void dvFiles_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Tracer.WriteMemberEntry(ref _log);
        try
        {
            string currentCommand = e.CommandName;
            if (e.CommandName == "Delete")
            {
                Tracer.WriteLine(ref _log, "Deleting....");
                DataSet d = ShowUploadedList();

                if (d != null)
                {
                    // Two items need to be deleted.
                    // The first is the record in the database and the second is the actual file itself
                    int currentRowIndex = Int32.Parse(e.CommandArgument.ToString());
                    int uploadedID = Int32.Parse(dvFiles.DataKeys[currentRowIndex].Value.ToString());
                    Tracer.WriteLine(ref _log, "uploadedID is: " + uploadedID);

                    // Get the file path to be deleted
                    DataTable t = d.Tables[0];
                    Tracer.WriteLine(ref _log, "Fetched datatable for upload deletion");

                    DataRow[] dr = t.Select("UploadedID = " + uploadedID);

                    string deletedFilePath = string.Empty;
                    foreach (DataRow r in dr)
                    {
                        deletedFilePath = r["FilePath"].ToString();

                        Tracer.WriteLine(ref _log, "deletedFilePath is: " + deletedFilePath);
                        _uploadManager.RemovePreAuth(uploadedID, deletedFilePath);
                        break;
                    }

                    if (deletedFilePath.Length == 0)
                        Tracer.WriteLine(ref _log, "Unable to delete uploaded file - uploadedID is: " + uploadedID);
                }
                else
                {
                    lblUpload.Text = "Error retrieving filelist before deletion - please contact support";
                }
            }
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
    }

    protected void dvFiles_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    public void SetupUploader(string functionName, string userID, string allowedExtensions, long maxFileSize)
    {
        this.UserID = userID;
        this.UploadCategory = functionName;

        this.AllowedExtensions = allowedExtensions;
        this.MaxFileSize = maxFileSize;

        // Require the upload manager to deal with uploads
        _uploadManager = new UploadManager(_maxFileSetByteSize, maxFileSize);

        if (Page.IsPostBack && ViewState[UploadPublication.Session_RequestID] != null)
        {
            // Retrieve the requestID for uploads to remember what was uploaded
            
            Tracer.WriteLine(ref _log, "Retrieve the requestID for uploads to remember what was uploaded");

            // if (!string.IsNullOrEmpty(Convert.ToString(Request["sess"])))
            // {
                // this.SessionID = Convert.ToString(Request["sess"]).ToString();
                // Tracer.WriteLine(ref _log, "Retrieved this.SessionID is: " + this.SessionID);
            // }
            // else
            // {
                // if (ViewState[UploadPublication.Session_RequestID] != null)
                // {
                    // this.SessionID = ViewState[UploadPublication.Session_RequestID].ToString();
                    // Tracer.WriteLine(ref _log, "Retrieved this.SessionID is: " + this.SessionID);
                // }
            // }
			
			 if (!string.IsNullOrEmpty(Convert.ToString(Session["UploadSession"])))
            {
                this.SessionID = Convert.ToString(Session["UploadSession"]);
                ////this.SessionID = Cryption.Decrypt(this.SessionID, Cryption.KeyValue, Cryption.SaltValue, Cryption.HashAlgorithm, Cryption.PasswordIterations, Cryption.InitVector, Cryption.KeySize);
                ////Tracer.WriteLine(ref _log, "Retrieved this.SessionID is: " + this.SessionID);
            }
            else
            {
                if (ViewState[UploadPublication.Session_RequestID] != null)
                {
                    this.SessionID = ViewState[UploadPublication.Session_RequestID].ToString();
                    ////this.SessionID = Cryption.Decrypt(this.SessionID, Cryption.KeyValue, Cryption.SaltValue, Cryption.HashAlgorithm, Cryption.PasswordIterations, Cryption.InitVector, Cryption.KeySize);
                    ////Tracer.WriteLine(ref _log, "Retrieved this.SessionID is: " + this.SessionID);
                }
            }
        }
        else
        {
            // Generate the request ID for the upload
            // if (!string.IsNullOrEmpty(Convert.ToString(Request["sess"])))
            // {
                // this.SessionID = Convert.ToString(Request["sess"]);
            // }
            // else
            // {
                // this.SessionID = UploadManager.GetNextRequest();
            // }
			
			if (!string.IsNullOrEmpty(Convert.ToString(Session["UploadSession"])))
            {
                this.SessionID = Convert.ToString(Session["UploadSession"]);
                ////this.SessionID = Cryption.Decrypt(this.SessionID, Cryption.KeyValue, Cryption.SaltValue, Cryption.HashAlgorithm, Cryption.PasswordIterations, Cryption.InitVector, Cryption.KeySize);
            }
            else
            {
                this.SessionID = UploadManager.GetNextRequest();
            }
            
            
            
            
            Tracer.WriteLine(ref _log, "this.SessionID for GetNextRequest is: " + this.SessionID);

            // Store in the ViewState to retrieve on postbacks
            ViewState[UploadPublication.Session_RequestID] = this.SessionID;
        }
    }

    public void Notify(string uploadSessionID, string function, string referenceID, string[] addressees, string fullName, string username)
    {
        Tracer.WriteMemberEntry(ref _log);
        try
        {
            // Send an email informing the team about a pre-auth request
            string fromEmail = "Membership_online_doc@bupa.com.sa";

            string subject = function + " reference:" + referenceID + " - attachments added by " + fullName + " (" + username + ")";
            string content = "Please view the attached uploaded documents added by " + fullName + " (" + username + ") for " + function + " reference: " + referenceID;

            // Get the list of attachments for this user
            Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
            DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);

            StringCollection attachments = new StringCollection();
            if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow r in d.Tables[0].Rows)
                {
                    string attachPath = r["VirtualPath"].ToString();
                    attachments.Add(attachPath);
                }
            }
            else
            {
                // No attachments and therefore, nothing to email
                return;
            }

            // Get the addressees
            foreach (string toEmail in addressees)
            {
                SendEmail(fromEmail, toEmail, subject, content, attachments);
            }
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
    }

    private UploadResult SaveFile(string fileName)
    {
        Tracer.WriteMemberEntry(ref _log);

        UploadResult uploadResult = new UploadResult();
        uploadResult.Result = false;
        string virtualPath = "";
        try
        {
            // Fetch the filename and save to the server
            Tracer.WriteLine(ref _log, "fileName is: " + fileName);

            // Get a random name and append to the file
            int seed = DateTime.Now.Millisecond;
            Tracer.WriteLine(ref _log, "seed is: " + seed);

            string uniquePrefix = WebPublication.GenerateUniqueID;
            Tracer.WriteLine(ref _log, "uniquePrefix is: " + uniquePrefix);

            string strPreauth = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHFOLDER_PATH);

            // Go ahead and save to the server for the moment
            virtualPath = "~/" + strPreauth + "/" + uniquePrefix + "_" + fileName;
            
            Tracer.WriteLine(ref _log, "virtualPath is: " + virtualPath);

            string destinationPath = Server.MapPath(virtualPath);
            Tracer.WriteLine(ref _log, "destinationPath is: " + destinationPath);

            // Save the file & fetch the session ID as this will be used as part of the upload key
            uploader.SaveAs(destinationPath);
            Tracer.WriteLine(ref _log, "_sessionID is: " + _sessionID);

            uploadResult = _uploadManager.Add(_sessionID, _uploadCategory, destinationPath, virtualPath, fileName, _userID);
            Tracer.WriteLine(ref _log, "uploadResult is: " + uploadResult.Result);
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            //throw;
        }

        Tracer.WriteMemberExit(ref _log);
        return uploadResult;
    }


    private UploadResult SaveFileToFolder(string fileName, string Folder)
    {
        Tracer.WriteMemberEntry(ref _log);
        fileName = fileName.Trim();
        Folder = Folder.Trim();
        UploadResult uploadResult = new UploadResult();
        uploadResult.Result = false;

        try
        {
            // Fetch the filename and save to the server
            Tracer.WriteLine(ref _log, "fileName is: " + fileName);

            string extension = Path.GetExtension(fileName);
            string newfileName = TransactionManager.TransactionID().ToString() + "_" + Session["ProviderID"] + extension;

            // Go ahead and save to the server for the moment
            string virtualPath = "~/" + Folder + "/" + newfileName;
            Tracer.WriteLine(ref _log, "virtualPath is: " + virtualPath);
            string destinationPath = Server.MapPath(virtualPath);
            Tracer.WriteLine(ref _log, "destinationPath is: " + destinationPath);

            // Save the file & fetch the session ID as this will be used as part of the upload key
            uploader.SaveAs(destinationPath);
            Tracer.WriteLine(ref _log, "_sessionID is: " + _sessionID);


           

            
            string SourcePath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHDESTINATION_PATH);
            MoveFile(destinationPath, SourcePath + @"\" + getDirectoryinfo() + @"\" + newfileName);




            uploadResult = _uploadManager.AddPreauthDocs(_sessionID, _uploadCategory, destinationPath, virtualPath, fileName, _userID);
            Tracer.WriteLine(ref _log, "uploadResult is: " + uploadResult.Result);
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            //throw;
        }

        Tracer.WriteMemberExit(ref _log);
        return uploadResult;
    }

    private DataSet ShowUploadedList()
    {
        Tracer.WriteMemberEntry(ref _log);

        DataSet ds = null;
        dvFiles.DataSource = null;
        try
        {
            // Get the table with the latest uploaded fileset
                ds = _uploadManager.GetUploadedPreauthFileSet(_sessionID, _uploadCategory);
            

            dvFiles.DataSource = ds;
            dvFiles.DataBind();

            dvFiles.Visible = true;
            _uploadedFileSet = ds;
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            //throw;
        }

        Tracer.WriteMemberExit(ref _log);
        return ds;

    }

    private bool IsAllowedExtension(string file)
    {
        Tracer.WriteMemberEntry(ref _log);

        bool allowed = false;
        try
        {
            // Allowed extensions are a comma separated list
            string fileExtension = file.Substring(file.LastIndexOf(".") + 1);
            Tracer.WriteLine(ref _log, "fileExtension is: " + fileExtension);

            if (_allowedExtensions.ToLower().Contains(fileExtension.ToLower()))
                allowed = true;
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
            //throw;
        }
        Tracer.WriteLine(ref _log, "allowed is: " + allowed);

        Tracer.WriteMemberExit(ref _log);
        return allowed;
    }

    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        try
        {
            // Setup the mail message
            MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

            // Deal with the attachments
            if (attachmentPaths != null)
            {
                foreach (string path in attachmentPaths)
                {
                    // Create the attachment
                    try
                    {
                        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(path));

                        // Add the attachment
                        mail.Attachments.Add(attachment);
                    }
                    catch (Exception ex) { Tracer.WriteLine(ref _log, "error attachments  is: " + ex.Message); }
                }
            }

            string smtpHost = WebPublication.EmailRelayAddress();
            int smtpPort = WebPublication.EmailRelayPort();

            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
            mail.IsBodyHtml = true;
            smtp.Send(mail);
        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
    }


    public PreauthDoc_DN[] preauthSupportDocuments(string uploadSessionID, string function, long Transaction)
    {
        
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedPreauthFileSet(uploadSessionID, function);
        OS_DXC_WAP.CaesarWS.PreauthDoc_DN[] _supportDocuments = new OS_DXC_WAP.CaesarWS.PreauthDoc_DN[0];
        int _count = 0;
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            _supportDocuments = new PreauthDoc_DN[d.Tables[0].Rows.Count];
            WebPublication _webPub = new WebPublication();
            foreach (DataRow r in d.Tables[0].Rows)
            {

                string attachPath = r["VirtualPath"].ToString();
                attachPath = attachPath.Replace("/", "\\");
                attachPath = attachPath.Replace("~", "");
                attachPath = attachPath.Replace("\\preauthUpload\\", "\\");
                _supportDocuments[_count] = new PreauthDoc_DN();
                _supportDocuments[_count].DOC_PATH = _webPub.DocLocation() + attachPath;
                _supportDocuments[_count].sql_type = "CSR.PREA_REQ_DOC01";
                _supportDocuments[_count].TXN_ID = Transaction;
                _count++;
            }
            return _supportDocuments;
        }
        else
        {
            return null;
        }

    }


    public string getDirectoryinfo()
    {
        string strDirectory = "";
        string strYear = System.DateTime.Today.Year.ToString();
        string strMonth = System.DateTime.Today.Month.ToString();
        string strDay = System.DateTime.Today.Day.ToString();

        strDirectory = strYear + @"\" + strMonth + @"\" + strDay;

        return strDirectory;
    }
    public void ClaerUploader()
    {
        try
        {
            dvFiles.DataSource = null;
            dvFiles.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void MoveFile(string sourcePath, string destinationPath)
    {
        //MTOM.MTOMService svc = new MTOM.MTOMService();
        //svc.UploadFile(sourcePath,destinationPath);
    }

}