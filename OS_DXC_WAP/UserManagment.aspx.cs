﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserManagment : System.Web.UI.Page
{

    private bool isProvider = false;
    private string groupId = string.Empty;
    private int referenceNumber = 0;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Master.FindControl("aa").Visible = false;
            lblMessage.Text = string.Empty;
            if (string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) && string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
                Response.Redirect("CommClear.aspx", true);

            referenceNumber = Convert.ToInt32(Session["ReferenceNumber"]);
            isProvider = !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"]));
            groupId = isProvider ? Convert.ToString(Session["ProviderID"]) : Convert.ToString(Session["ClientID"]);
            if (!Page.IsPostBack)
            {
                if (Session["SuperUser"] != null && Convert.ToString(Session["SuperUser"]).ToLower().Contains("true"))
                    GetUserDetails();
            }

            if (!string.IsNullOrEmpty(groupId) && isProvider==false)
            {
                HlnkCreateUser.NavigateUrl = "../Client/ClientManageUser.aspx";
            }
            else
            {
                HlnkCreateUser.NavigateUrl = "../Provider/ProviderManageUser.aspx";
            }

        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message + ex.InnerException;
        }

    } 

    protected void gvUserManagment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lbtnAction = (LinkButton)e.Row.Cells[5].FindControl("lbtnAction");

                Label lbl = (Label)e.Row.FindControl("lblIsUserActPending");

                var isUserActivationPending = bool.Parse(lbl.Text);
                if (isUserActivationPending)
                {
                    //lbtnAction.Text = "Pending for Activation";
                    lbtnAction.Visible = false;
                    e.Row.Cells[5].Text = "Pending for Activation";
                }
                else
                {
                    lbtnAction.Text = e.Row.Cells[4].Text.ToUpper().Contains("YES") ? "Inactive" : "Activate";
                }
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message + ex.InnerException;
        }
       
    }

    protected void gvUserManagment_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int rowIndex = int.Parse(e.CommandArgument.ToString());
            int itemNumber = Convert.ToInt32(this.gvUserManagment.DataKeys[rowIndex][0]);
            string status = Convert.ToString(this.gvUserManagment.DataKeys[rowIndex][3]);
            UserDetails userDetails = new UserDetails();
            UserDetails objUserDetails = new UserDetails();
            userDetails.ReferenceNumber = itemNumber;
            userDetails.Status = isProvider ? "Y" : "N";
            userDetails.UpdatedBy = referenceNumber;
            userDetails.IsActive = status.ToUpper().Contains("YES") ? false : true;
            objUserDetails.SetActivation(userDetails);
            GetUserDetails();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message + ex.InnerException;
        }
        
    }

    private void GetUserDetails()
    {
        try
        {
            UserDetails userDetails = new UserDetails();
            List<UserDetails> lstUserDetails = new List<UserDetails>();
            userDetails.UserGroupID = groupId;
            userDetails.ReferenceNumber = referenceNumber;
            userDetails.Status = isProvider ? "Y" : "N";
            lstUserDetails = userDetails.GetUserDetails(userDetails);
            gvUserManagment.DataSource = lstUserDetails;
            gvUserManagment.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }

   
}