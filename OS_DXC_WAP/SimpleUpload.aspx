<%@ Page Language="C#" AutoEventWireup="true" Inherits="SimpleUpload" Codebehind="SimpleUpload.aspx.cs" %>
<%@ Register TagPrefix="BUPA" TagName="Uploader" Src="~/Uploader.ascx" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Upload Files</title>
</head>
<body>
    <form id="frmSimpleUpload" runat="server">

            <%--<%if (uploader.LblUpload.Text.Length > 0)
  { %>
        <br />
        <br />
        <table style="width: 750px; font-size: 17px; font-family: Arial; border-right: black 0.5pt solid;
            border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid;
            border-collapse: collapse;" runat="server" id="Table3">
            <tr>
                <td align="left">
                    <span style="color: #cc0000"><%= uploader.LblUpload.Text%></span>
                </td>
            </tr>
        </table>
        <br />
        <br />
        
<% }%>--%>

        <asp:Label runat="server" ID="lblUploadExtension" />
        <BUPA:Uploader runat="server" ID="uploader" 
            SessionID='<%# Request.QueryString[Bupa.OSWeb.Helper.UploadPublication.QSKey_RequestID] %>' 
            UploadCategory='<%# Request.QueryString[Bupa.OSWeb.Helper.UploadPublication.QSKey_UploadCategory] %>'
            UserID='<%# Request.QueryString[Bupa.OSWeb.Helper.UploadPublication.QSKey_UserID] %>' />
            
        <a href="JavaScript:window.close()" style="display:none">Close</a>
            <asp:LinkButton ID="lbtnClose" runat="server" onclick="lbtnClose_Click">Close</asp:LinkButton>
            <dx:ASPxButton ID="btnSend" runat="server" Text="Send" Visible="false" 
                onclick="btnSend_Click">
            </dx:ASPxButton>
    </form>
</body>
</html>
