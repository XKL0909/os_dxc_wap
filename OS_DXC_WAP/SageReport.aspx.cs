﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DownloadSageReport : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
        }
        catch
        {

        }
        if (Page.IsPostBack == false)
        {
           
            
            if (Request.QueryString["id"] != null)
            {
                string reportId = Request.QueryString["id"];
                iframe1.Attributes["src"] = "http://bjsqconsage/ReportServer/Pages/ReportViewer.aspx?%2fBusiness+Agreement+English%2fBusiness+Agreement+English&rs:Command=Render&rs:ClearSession=True&rs:Format=pdf&id=" + reportId;
            }
        }
    }

    private MemoryStream downloadData(string url)
    {
        try
        {
            //progress.Value = 0;
            //Application.DoEvents();
            //Get a data stream from the url
            WebRequest req = WebRequest.Create(url);
            req.Credentials = CredentialCache.DefaultNetworkCredentials;
            WebResponse response = req.GetResponse();
            Stream stream = response.GetResponseStream();

            //Download in chuncks
            Byte[] buffer = new Byte[1024];

            //Get Total Size
            Int32 dataLength = (Int32)response.ContentLength;

            //With the total data we can set up our progress indicators
            //progress.Maximum = dataLength;
            //Application.DoEvents();
            //Download to memory
            //Note: adjust the streams here to download directly to the hard drive
            MemoryStream memStream = new MemoryStream();
            while (true)
            {
                //Try to read the data
                Int32 bytesRead = stream.Read(buffer, 0, buffer.Length);

                if (bytesRead == 0)
                {
                    //progress.Value = progress.Maximum;
                    //Application.DoEvents();
                    break;
                }
                else
                {
                    //Write the downloaded data
                    memStream.Write(buffer, 0, bytesRead);
                    //if (progress.Value + bytesRead <= progress.Maximum)
                    //{
                    //    progress.Value += bytesRead;
                    //    progress.Refresh();
                    //    Application.DoEvents();
                    //}
                }
            }

            stream.Close();
            return memStream;
        }
        catch (Exception ex)
        {
            //May not be connected to the internet
            //Or the URL might not exist
            //throw new Exception("There was an error accessing the URL.");
            throw ex;
        }
    }

    private MemoryStream downloadDataWithCredentials(string url)
    {
        try
        {
            //progress.Value = 0;
            //Application.DoEvents();
            //Get a data stream from the url
            WebRequest req = WebRequest.Create(url);
            req.Credentials = new NetworkCredential("CRMAdmin@bupame.com", "CRM@Bupa2012");
            WebResponse response = req.GetResponse();
            Stream stream = response.GetResponseStream();

            //Download in chuncks
            Byte[] buffer = new Byte[1024];

            //Get Total Size
            Int32 dataLength = (Int32)response.ContentLength;

            //With the total data we can set up our progress indicators
            //progress.Maximum = dataLength;
            //Application.DoEvents();
            //Download to memory
            //Note: adjust the streams here to download directly to the hard drive
            MemoryStream memStream = new MemoryStream();
            while (true)
            {
                //Try to read the data
                Int32 bytesRead = stream.Read(buffer, 0, buffer.Length);

                if (bytesRead == 0)
                {
                    //progress.Value = progress.Maximum;
                    //Application.DoEvents();
                    break;
                }
                else
                {
                    //Write the downloaded data
                    memStream.Write(buffer, 0, bytesRead);
                    //if (progress.Value + bytesRead <= progress.Maximum)
                    //{
                    //    progress.Value += bytesRead;
                    //    progress.Refresh();
                    //    Application.DoEvents();
                    //}
                }
            }

            stream.Close();
            return memStream;
        }
        catch (Exception ex)
        {
            //May not be connected to the internet
            //Or the URL might not exist
            throw ex;
        }
    }

    public static void Download(String strURLFileandPath, String strFileSaveFileandPath)
    {
        HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(strURLFileandPath);
        HttpWebResponse ws = (HttpWebResponse)wr.GetResponse();
        Stream str = ws.GetResponseStream();
        byte[] inBuf = new byte[100000];
        int bytesToRead = (int)inBuf.Length;
        int bytesRead = 0;
        while (bytesToRead > 0)
        {
            int n = str.Read(inBuf, bytesRead, bytesToRead);
            if (n == 0)
                break;
            bytesRead += n;
            bytesToRead -= n;
        }
        FileStream fstr = new FileStream(strFileSaveFileandPath, FileMode.OpenOrCreate, FileAccess.Write);
        fstr.Write(inBuf, 0, bytesRead);
        str.Close();
        fstr.Close();
    }
    protected void btnDownloadReport_Click(object sender, EventArgs e)
    {
        try
        {


            //Response.ContentType = "Application/pdf";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=Test_PDF.pdf");
            //Response.TransmitFile();
            //Response.End(); 
            string reportId = Request.QueryString["id"];
            if (reportId != "")
            {
                string url = "http://bjsqconsage/ReportServer/Pages/ReportViewer.aspx?%2fBusiness+Agreement+English%2fBusiness+Agreement+English&rs:Command=Render&rs:ClearSession=True&rs:Format=pdf&id=" + reportId;
                string FileSavepath = HttpContext.Current.Request.PhysicalApplicationPath + reportId;

                MemoryStream streamdata = downloadDataWithCredentials(url);
                byte[] buffer = streamdata.ToArray();
                //using (Stream stream = new FileStream("fileName", FileMode.Create))
                //    stream.Write(buffer, 0, buffer.Length);

                Response.ContentType = "Application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=SageReport_" + reportId);
                Context.Response.OutputStream.Write(buffer, 0, buffer.Length);
                Context.Response.Flush();
                //streamdata.Flush();
                //streamdata.Close();

                //using (WebClient client = new WebClient())
                //{

                //    client.Proxy = null;
                //    client.Credentials = CredentialCache.DefaultNetworkCredentials;
                //    client.DownloadFile(new Uri(url), FileSavepath);
                //    // client.OpenReadAsync(new Uri(url), UriKind.Absolute);
                //    Response.ContentType = "Application/pdf";
                //    Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileSavepath);
                //    Context.Response.OutputStream.Write(buffer, 0, buffer.Length);
                //    Context.Response.Flush();
                //}
            }
            else
            {
                Response.Write("Invalid Request");
            }
        }
        catch (Exception ex)
        {
            //Response.Write("We are unable to process your request.");
            Response.Write(ex.Message);
        }
    }
    protected void btnDownlaodWithoutCredentials_Click(object sender, EventArgs e)
    {
        try
        {


            //Response.ContentType = "Application/pdf";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=Test_PDF.pdf");
            //Response.TransmitFile();
            //Response.End(); 

            string reportId = Request.QueryString["id"];
            if (reportId != "")
            {
                string url = "http://bjsqconsage/ReportServer/Pages/ReportViewer.aspx?%2fBusiness+Agreement+English%2fBusiness+Agreement+English&rs:Command=Render&rs:ClearSession=True&rs:Format=pdf&id=" + reportId;
                string FileSavepath = HttpContext.Current.Request.PhysicalApplicationPath + reportId;
                Response.Write(url + "<br />");
                Response.Write(FileSavepath + "<br />");

                MemoryStream streamdata = downloadData(url);
                byte[] buffer = streamdata.ToArray();
                Response.Write(buffer.Length);


                Response.ContentType = "Application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=SageReport_" + reportId);
                Context.Response.OutputStream.Write(buffer, 0, buffer.Length);
                Context.Response.Flush();
                streamdata.Flush();
                streamdata.Close();


            }
            else
            {
                Response.Write("Invalid Request");
            }
        }
        catch (Exception ex)
        {
            //Response.Write("We are unable to process your request.");
            Response.Write(ex.Message);
        }
    }
    protected void btnEnableFrame_Click(object sender, EventArgs e)
    {
        iframe1.Visible = true;
    }
    protected void btnWebClient_Click(object sender, EventArgs e)
    {
        string reportId = Request.QueryString["id"];
        string url = "http://bjsqconsage/ReportServer/Pages/ReportViewer.aspx?%2fBusiness+Agreement+English%2fBusiness+Agreement+English&rs:Command=Render&rs:ClearSession=True&rs:Format=pdf&id=" + reportId;
        string FileSavepath = HttpContext.Current.Request.PhysicalApplicationPath + reportId;


        using (WebClient client = new WebClient())
        {

            client.Proxy = null;
            client.Credentials = CredentialCache.DefaultNetworkCredentials;
            client.DownloadFile(new Uri(url), FileSavepath);

            byte[] bytes = File.ReadAllBytes(FileSavepath);
            //client.OpenReadAsync(new Uri(url), UriKind.Absolute);
            Response.ContentType = "Application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileSavepath);
            Context.Response.OutputStream.Write(bytes, 0, bytes.Length);
            Context.Response.Flush();
        }
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
}