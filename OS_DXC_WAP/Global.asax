﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        log4net.Config.XmlConfigurator.Configure();
        //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        //HttpApplication httpApp = this.Context.ApplicationInstance;
        //HttpException httpEx = httpApp.Server.GetLastError() as HttpException;
        //if (httpEx != null)
        //{
        //    if (
        //        httpEx.WebEventCode == System.Web.Management.WebEventCodes.AuditInvalidViewStateFailure
        //        ||
        //        httpEx.WebEventCode == System.Web.Management.WebEventCodes.InvalidViewState
        //        ||
        //        httpEx.WebEventCode == System.Web.Management.WebEventCodes.InvalidViewStateMac
        //        ||
        //        httpEx.WebEventCode == System.Web.Management.WebEventCodes.RuntimeErrorViewStateFailure
        //        )
        //    {
        //        HttpContext.Current.ClearError();
        //        Response.Write("Error: An invalid viewstate has been detected (WebEventCode: " + httpEx.WebEventCode.ToString() + ").");
        //    }
        //}
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
 protected void Application_BeginRequest(){
   /* if (!Context.Request.IsSecureConnection){
       // Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:").Replace(":1982", ""));
		Uri oldUri = new Uri(Context.Request.Url.ToString().Replace("http:", "https:"));
		UriBuilder builder = new UriBuilder(oldUri);
		builder.Port = -1;
		Uri newUri = builder.Uri;
		Response.Redirect(newUri.ToString());
}*/
	//HttpContext.Current.Response.AddHeader("x-frame-options", "SAMEORIGIN");
 }

    public void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
    {

        if (Request.HttpMethod != "GET")
        {
            return;
        }

        var hasPostParams = (Request.QueryString["__EVENTTARGET"] ??
                               Request.QueryString["__VIEWSTATE"] ??
                               Request.QueryString["__EVENTARGUMENT"] ??
                               Request.QueryString["__EVENTVALIDATION"]) != null;

        if (hasPostParams)
        {
            Exception ex = new Exception();           
            Response.Redirect("login.aspx", true);
        }
    }

       
</script>
