﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class checkCookies : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ReadCookie();

    }


    protected void ReadCookie()
    {
        
        String strCookieName = "userLogin";
     
        HttpCookie cookie = HttpContext.Current.Request.Cookies[strCookieName];

        if (cookie == null)
        {
            Response.Write("Cookie <b>" + strCookieName + "</b>  not found. <br><hr>");
        }
        else
        {
            String strCookieValue = cookie.Value.ToString();
            Response.Write("The " + strCookieName + " cookie contains: <b>" + strCookieValue + "</b><br><hr>");
        }


        
    }
}