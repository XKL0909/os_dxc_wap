﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Print.master" AutoEventWireup="true" Inherits="Visitor_Econtract" Codebehind="Econtract.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="Server">

    <script src="Content/tabcontent.js"></script>
    <link href="Content/tabcontent.css" rel="stylesheet" />
    <link href="Content/jquery-ui.css" rel="stylesheet" />
    <table style="width: 100%; padding-right: 10px;">
     <tr>
         <td height="15px">

         </td>
     </tr>

           <tr>
         <td>
             <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
         </td>
     </tr>
        <tr>
            <td>
                <div style="width: 850px; margin: 0 auto; padding: 20px 0 0px; min-height: 630px;" runat="server" id="divContract" visible="false">
                    <ul class="tabs" data-persist="false">
                        <li><a id="a1" href="#view1">English </a></li>
                        <li><a id="a2" href="#view2">Arabic</a></li>

                    </ul>
                    <div class="tabcontents">
                        <div id="view1" style="height: 600px;overflow-y: scroll;padding-left:50px;padding-right:30px;">
                            <asp:Panel runat="server" ID="pnlView1">
                             
                                <table>
                                    <tr>
                                        <td>

<p class=MsoNormal><span lang=EN-GB><o:p>&nbsp;</o:p></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left
 width=512 style='width:384.05pt;background:#0079C8;border-collapse:collapse;
 mso-table-overlap:never;mso-yfti-tbllook:480;margin-left:-2.25pt;margin-right:
 -2.25pt;mso-table-anchor-vertical:page;mso-table-anchor-horizontal:margin;
 mso-table-left:left;mso-table-top:197.1pt;mso-padding-alt:9.9pt 6.5pt 9.9pt 6.5pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:27.2pt'>
  <td width=512 colspan=2 valign=top style='width:384.05pt;background:#00AEEF;
  padding:0in 9.9pt 0in 9.9pt;height:27.2pt'>
  <p class=BUPFPDeptorCamp style='mso-element:frame;mso-element-wrap:around;
  mso-element-anchor-horizontal:margin;mso-element-left:left;mso-element-top:
  197.1pt;mso-height-rule:exactly'><span lang=EN-GB>Product Policy</span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:3.65pt'>
  <td width=512 colspan=2 valign=top style='width:384.05pt;background:#00AEEF;
  padding:0in 9.9pt 0in 9.9pt;height:3.65pt'>
  <p class=BUPAnchor style='mso-element:frame;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:197.1pt;mso-height-rule:exactly'><v:shapetype id="_x0000_t75"
   coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe"
   filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_22" o:spid="_x0000_s1026" type="#_x0000_t75"
   style='position:absolute;margin-left:.25pt;margin-top:0;width:331.65pt;
   height:2.2pt;z-index:251659264;visibility:visible;mso-wrap-style:square;
   mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
   mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
   mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
   mso-position-horizontal-relative:text;mso-position-vertical:absolute;
   mso-position-vertical-relative:text;mso-width-percent:0;
   mso-height-percent:0;mso-width-relative:page;mso-height-relative:page'
   o:allowincell="f">
   <v:imagedata src="Bupa%20Arabia%20Inbound%20Travel%20Health%20Insurance%20Product%20-%20Product%20Policy%20English%20V2%200_files/image001.emz"
    o:title=""/>
   <w:anchorlock/>
  </v:shape></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:96.85pt'>
  <td width=512 colspan=2 valign=top style='width:384.05pt;background:#00AEEF;
  padding:0in 9.9pt 0in 9.9pt;height:96.85pt'>
  <p class=MsoNormal style='margin-bottom:8.0pt;line-height:normal;mso-element:
  frame;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:
  margin;mso-element-top:197.1pt;mso-height-rule:exactly'><span lang=EN-GB
  style='font-size:30.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:white;mso-fareast-language:EN-GB'>Bupa Inbound Travel Health Insurance Policy
  (to KSA)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes;height:20.4pt'>
  <td width=256 valign=bottom style='width:192.0pt;background:#00AEEF;
  padding:0in 0in 0in 9.9pt;height:20.4pt'>
  <p class=BUPFPURL style='mso-element:frame;mso-element-wrap:around;
  mso-element-anchor-horizontal:margin;mso-element-left:left;mso-element-top:
  197.1pt;mso-height-rule:exactly'><span lang=EN-GB>bupa.com.sa</span></p>
  </td>
  <td width=256 valign=bottom style='width:192.05pt;background:#00AEEF;
  padding:0in 9.9pt 0in 0in;height:20.4pt'>
  <p class=BUPFPDate style='mso-element:frame;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-top:197.1pt;mso-height-rule:exactly'><span lang=EN-GB
  style='color:windowtext;background:yellow;mso-highlight:yellow'>08.October.2015<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<w:Sdt SdtDocPart="t" DocPartType="Cover Pages" DocPartUnique="t" ID="13908099">
 <p class=MsoNormal><span lang=EN-GB><o:p>&nbsp;</o:p><w:sdtPr></w:sdtPr></span></p>
 <p class=MsoNormal><span lang=EN-GB><o:p>&nbsp;</o:p></span></p>
 <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left
  width="80%" style='width:80.0%;border-collapse:collapse;mso-yfti-tbllook:
  1184;mso-table-lspace:9.35pt;margin-left:7.1pt;mso-table-rspace:9.35pt;
  margin-right:7.1pt;mso-table-anchor-vertical:margin;mso-table-anchor-horizontal:
  margin;mso-table-left:left;mso-table-top:bottom;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
  <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
   <td width=511 valign=top style='width:383.6pt;padding:.15in 5.75pt .15in 5.75pt'>
   <p class=MsoNoSpacing style='mso-element:frame;mso-element-frame-hspace:
   9.35pt;mso-element-wrap:around;mso-element-anchor-horizontal:margin;
   mso-element-top:bottom;mso-height-rule:exactly'><span lang=EN-GB
   style='color:#0079C8;mso-themecolor:accent1;mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></p>
   </td>
  </tr>
 </table>
 <p class=MsoNormal><span lang=EN-GB><o:p>&nbsp;</o:p></span></p>
 <p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
 Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
 <p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
 Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
 <p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
 Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
 <span lang=EN-GB style='font-size:11.0pt;line-height:115%;font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
 mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-bidi;
 mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-GB;mso-fareast-language:
 EN-US;mso-bidi-language:AR-SA'><br clear=all style='mso-special-character:
 line-break;page-break-before:always'>
 </span>
 <p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
 Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
</w:Sdt>

<h1><b><span lang=EN-GB style='font-size:16.0pt;mso-ascii-font-family:Arial;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'>Inbound Travel Health Insurance Agreement<o:p></o:p></span></b></h1>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><u><span lang=EN-GB
style='font-family:"Arial","sans-serif"'><o:p><span style='text-decoration:
 none'>&nbsp;</span></o:p></span></u></b></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><u><span lang=EN-GB
style='font-family:"Arial","sans-serif"'><o:p><span style='text-decoration:
 none'>&nbsp;</span></o:p></span></u></b></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'>Subject
to the prevailing laws of the Kingdom of Saudi Arabia, and whereas the
Policyholder (hereinafter referred to as the insured), the holder of the
agreement has applied to <b>Bupa Arabia for Cooperative Insurance </b>(hereinafter
referred to as “Bupa”) for the purpose of taking a CCHI regulated Inbound (to
the Kingdom of Saudi Arabia) Health Travel Insurance cover for himself/herself,
whose name is included in the list attached to this agreement, and has paid the
subscription fees for the cover.<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'>In
accordance with the above, Bupa agrees with the policyholder to cover the
expenses of providing Inbound Health Travel Insurance to himself/herself by
virtue of this agreement to the extent and in the manner specified therein
subject always to the conditions, definitions, limits and scope of the coverage
included in this.<o:p></o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'>Whereas
the inbound health travel insurance cover is available primarily through online
purchase, the insured hereby agrees that by clicking the ‘Agree’ button on
Policy Page of the Bupa e-commerce page has the same effect as signing this Inbound
Health Travel Insurance agreement. <o:p></o:p></span></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=595
 style='width:6.2in;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span lang=EN-GB style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow;
  mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></p>
  </td>
  <td width=167 valign=top style='width:125.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span lang=EN-GB style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow;
  mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></p>
  </td>
  <td width=179 valign=top style='width:134.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=167 valign=top style='width:125.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Bupa Arabia<o:p></o:p></span></p>
  </td>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=179 valign=top style='width:134.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Insured</span><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

</div>

<b><span lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-ansi-language:EN-GB;
mso-fareast-language:EN-US;mso-bidi-language:AR-SA'><br clear=all
style='page-break-before:always'>
</span></b>

<p class=MsoListParagraph style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l20 level1 lfo25'><a name="_Toc422989395"><![if !supportLists]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>A.<span
style='font:7.0pt "Times New Roman"'> </span></span></span></b><![endif]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'>Policy Information Table<o:p></o:p></span></b></a></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=676
 style='width:507.35pt;margin-left:-30.6pt;border-collapse:collapse;border:
 none;mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:12.2pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>Date of Insurance
  Policy Purchase </span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'>(DD/MM/YYYY):</span></span><span style='mso-bookmark:_Toc422989395'><span
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
  mso-ansi-language:EN-US'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.2pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB dir=LTR style='font-size:
  9.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#7F7F7F;
  mso-themecolor:text1;mso-themetint:128;background:yellow;mso-highlight:yellow'>&lt;
  DD/MM/YYYY &gt;</span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;background:yellow;mso-highlight:yellow;mso-ansi-language:EN-US'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:1;height:39.55pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Insurance
  Company Name:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 style='width:256.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:39.55pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB dir=LTR style='font-size:
  9.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Bupa Arabia for Cooperative
  Insurance.<o:p></o:p></span></span></p>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB dir=LTR style='font-size:
  9.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>C.R. 4030178881<o:p></o:p></span></span></p>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB dir=LTR style='font-size:
  9.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>P.O. Box. 23807,
  Jeddah 21436,<o:p></o:p></span></span></p>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB dir=LTR style='font-size:
  9.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Kingdom of Saudi
  Arabia</span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:2;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Insurance
  Company Code:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 style='width:256.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'>102</span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:3;height:4.9pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:4.9pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Policy
  Scheme:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 style='width:256.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:4.9pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'>Health Insurance for Visitors of the Kingdom of Saudi Arabia</span></span><span
  style='mso-bookmark:_Toc422989395'><i><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></i></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:4;height:12.55pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:"Times New Roman"'>Policy Duration:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 style='width:256.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.55pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Number of Days &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:5;height:3.5pt'>
  <td width=676 colspan=2 style='width:507.35pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:white;mso-background-themecolor:
  background1;padding:0in 5.4pt 0in 5.4pt;height:3.5pt'><span style='mso-bookmark:
  _Toc422989395'></span>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><b><span
  lang=EN-GB style='font-size:5.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:white;mso-themecolor:background1'><o:p>&nbsp;</o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:6;height:3.55pt'>
  <td width=676 colspan=2 style='width:507.35pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:black;mso-background-themecolor:text1;padding:0in 5.4pt 0in 5.4pt;
  height:3.55pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><b><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:white;mso-themecolor:background1'>Policy Information</span></b></span><span
  style='mso-bookmark:_Toc422989395'><b><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:white;mso-themecolor:background1;mso-ansi-language:EN-US'><o:p></o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:7;height:7.05pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.05pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Policy
  Number (Membership Number):<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.05pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Policy Number &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:8;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Passport
  Number:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Passport Number &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:9;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Start
  Date of Cover</span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'>:</span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; DD/MM/YYYY &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:10;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>End
  Date of Cover</span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'>:</span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; DD/MM/YYYY &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:11;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Policy
  Premium (Fee):<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Amount in USD &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:12;height:3.55pt'>
  <td width=676 colspan=2 style='width:507.35pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:black;mso-background-themecolor:text1;padding:0in 5.4pt 0in 5.4pt;
  height:3.55pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><b><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:white;mso-themecolor:background1'>Main Policyholder
  Information <span dir=RTL></span><span dir=RTL><span dir=RTL></span><span
  style='mso-spacerun:yes'> </span></span></span></b></span><span
  style='mso-bookmark:_Toc422989395'><b><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  mso-ansi-language:EN-US'><o:p></o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:13;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Policyholder
  Name:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Full Name &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:14;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:"Times New Roman"'>Gender:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Gender &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:15;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Date of
  Birth</span></span><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:9.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>:</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; DD/MM/YYYY &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:16;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Nationality:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Nationality &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:17;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Postal
  Address in Country of Residence:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Postal Address &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:18;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>City in
  Country of Residence:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; City &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:19;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Zip
  Code in Country of Residence:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Zip Code &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:20;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>P.O.
  Box in Country of Residence:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; P.O. Box &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:21;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Mobile
  Number:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Mobile Number &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:22;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>E-mail
  Address:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Email &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:23;height:4.9pt'>
  <td width=676 colspan=2 style='width:507.35pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:black;mso-background-themecolor:text1;padding:0in 5.4pt 0in 5.4pt;
  height:4.9pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><b><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:white;mso-themecolor:background1;background:black;
  mso-shading-themecolor:text1'>Dependent Policyholder Information (Optional)</span></b></span><span
  style='mso-bookmark:_Toc422989395'><b><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  mso-ansi-language:EN-US'><o:p></o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:24;height:2.85pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:2.85pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>Name:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:2.85pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Full Name &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:25;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>Gender:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Gender &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:26;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>Relation:</span></span><span
  style='mso-bookmark:_Toc422989395'><span style='font-size:10.0pt;font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  mso-ansi-language:EN-US'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; Relationship &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:27;mso-yfti-lastrow:yes;height:7.6pt'>
  <td width=334 style='width:250.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  mso-ansi-language:EN-US'>Date of Birth:<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=342 valign=top style='width:256.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:
  yellow;mso-highlight:yellow'>&lt; DD/MM/YYYY &gt;</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:9.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:yellow;
  mso-highlight:yellow'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
</table>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-family:"Arial","sans-serif"'>Bupa agrees with the
Customer to provide him/her with benefits throughout the policy period as per
the table marked in section C. In consideration of this, the Customer agrees to
pay Bupa the quoted Premium at the time as specified in the above table.<o:p></o:p></span></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:6.0pt;margin-left:.25in;mso-add-space:auto;text-indent:-.25in;
line-height:normal;mso-list:l3 level1 lfo2'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><span
lang=EN-GB style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>Visitors of the Kingdom of Saudi Arabia
based on an official visit visa or transit visas (with the exclusion of Hajj
and Umrah visitors) are eligible to purchase this policy.<o:p></o:p></span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:6.0pt;margin-left:.25in;mso-add-space:auto;text-indent:-.25in;
line-height:normal;mso-list:l3 level1 lfo2'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><span
lang=EN-GB style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>This policy is renewable only at time of
visitor’s visa extension/renewal via Bupa’s sales channels. <o:p></o:p></span></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:6.0pt;margin-left:.25in;mso-add-space:auto;text-indent:-.25in;
line-height:normal;mso-list:l3 level1 lfo2'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><span
lang=EN-GB style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>One policy is issued per visiting person.
Accompanying dependents of a visiting person should be issued one policy per
person.<o:p></o:p></span></span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:6.0pt;margin-left:.25in;mso-add-space:auto;text-indent:-.25in;
line-height:normal;mso-list:l3 level1 lfo2'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><span
lang=EN-GB style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>This policy is not amendable. <o:p></o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>An electronic medical declaration form is required to be filled by
the insured person, his sponsor or guardian (refer to Section G Appendix A).<o:p></o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:12.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:12.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:12.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:12.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:12.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:12.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=595
 style='width:6.2in;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=167 valign=top style='width:125.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><span style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=179 valign=top style='width:134.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><span style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph;line-height:normal'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow;
  mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:10.0pt;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=167 valign=top style='width:125.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Bupa Arabia<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=179 valign=top style='width:134.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Insured</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
</table>

<p class=MsoListParagraph style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l20 level1 lfo25'><span style='mso-bookmark:_Toc422989395'><a
name="_Toc422989399"><![if !supportLists]><b><span lang=EN-GB style='font-size:
16.0pt;line-height:115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>B.<span
style='font:7.0pt "Times New Roman"'> </span></span></span></b><![endif]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'>Definitions</span></b></a></span><span
style='mso-bookmark:_Toc422989395'><b><span lang=EN-GB style='font-size:16.0pt;
line-height:115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p></o:p></span></b></span></p>

<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span
style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>For the
purposes of this insurance policy, the following words and phrases shall carry
the meanings assigned to them throughout this policy or annexes thereto: <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Insurance<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Proof of insurance coverage
under the policy with its schedule, annexes or endorsements. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Insurance Duration<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The period stated in the
policy schedule during which the insurance is valid. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Inception Date<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The date stated in the policy
schedule on which insurance coverage commences.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Benefit<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The costs of providing health
services included in the insurance coverage within the limits specified in the
policy schedule. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Insurance Coverage<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The basic health benefits
available to the beneficiary as specified in the policy. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Coverage Limits<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The maximum amount of the
company’s liability as specified in the policy schedule with respect to any
insured person.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Home Country<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The insured country of
residence.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Insurance Parties<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Insured, health insurance
companies, TPAs and health service providers. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Insurance Company <o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The Cooperative Insurance
Company (Bupa) which is licensed by the Saudi Arabian Monetary Agency to work
within the Kingdom and which was approved by the Council of Cooperative Health
Insurance to practice cooperative health insurance works.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Policyholder<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The natural or legal person or
entity in whom the policy is issued.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>The Insured<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The natural person covered by
the policy, who is issued an official visit or transit visa to enter the
Kingdom of Saudi Arabia (with the exclusion of Pilgrims and Umrah visitors).<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Service Provider<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A health facility
(governmental / nongovernmental) licensed to provide health services in the
Kingdom in accordance with relevant laws and rules and accredited by the
Council, such as: hospitals, diagnostic centres, clinics, pharmacies,
laboratories, and physiotherapy or radiotherapy centres. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Preferred Provider Network (PPN)<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A group of healthcare service
providers accredited by the Council and designated by the insurance company to
provide healthcare to the insured and bill the insurance company directly
whenever an insured individual presents a valid insurance card/document,
provided that said network includes the following levels of healthcare
services: <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l26 level1 lfo4'><span
style='mso-bookmark:_Toc422989395'><![if !supportLists]><span lang=EN-GB
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;color:#0070C0;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>First Level
(Primary healthcare) <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l26 level1 lfo4'><span
style='mso-bookmark:_Toc422989395'><![if !supportLists]><span lang=EN-GB
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;color:#0070C0;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Second Level
(General hospitals) <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
6.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l26 level1 lfo4'><span
style='mso-bookmark:_Toc422989395'><![if !supportLists]><span lang=EN-GB
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;color:#0070C0;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Third Level
(Specialized or referral hospitals)<o:p></o:p></span></span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Licensed Physician<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A medical practitioner holding
an appropriate qualification licensed to practice medicine by the Saudi
Commission for Health Specialties and is accepted by the insured and the
company to provide treatment for which compensation may be claimed under the
policy. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>15.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Illness<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A sickness or disease suffered
by the insured which necessitates medical treatment by a licensed physician
before and during the insurance duration. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>16.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Traffic Accident<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Unintentional collision of a
private or public mechanical or electric vehicle, whether a car or bus, with
another vehicle, whether stationary or moving, or with a fixed object such as a
building, barrier, post, tree or the like or with a pedestrian, on any road or
street, leading to bodily injuries ranging from minor to serious injuries and
may lead to physical disability, death or partial or total loss of property. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>17.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Violent External Means<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Any means resulting in an
accident or injury to the insured. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>18.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Personal Risks <o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Any activity known to involve
the risk of exposing a person to an illness or an accident, or is expected to
aggravate a prior illness or injury. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>19.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Emergency Case<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The urgent medical treatment
necessitated by the medical condition of the insured as a result of an accident
or an urgent health condition requiring quick medical intervention. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>20.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Hospitalization<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Registering an insured
individual as an in-patient staying overnight in a hospital following a
referral from a competent physician. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>21.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Allergy <o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The sensitivity of a person to
certain types of food, medicine, weather or pollen or any allergens of plants,
insects, animals, minerals or other elements or materials causing such person
to develop bodily reactions from direct or indirect contact with such materials
resulting in conditions like asthma, indigestion, itching, hay fever, eczema or
headache. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>22.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Congenital Deformity<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The functional, chemical or
metabolic defect usually existing before birth, whether hereditary or due to
environmental factors as commonly known in the medical community. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>23.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Pregnancy and Delivery<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Any pregnancy and/or delivery,
including natural delivery, caesarean section and abortion (considering the
excluded cases mentioned in the Standard Exclusions section). <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>24.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Emergency Medical Evacuation<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Transferring a patient to the
nearest medical centre within or out of the Kingdom of Saudi Arabia where the
required health services are available. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>25.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Psychological Disorders<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Intellectual, mode, cognitive
or memory or total or partial mental disorder. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>26.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Disability Cases <o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A term covering all forms of
organ malfunction/dysfunction, limited activity and restricted participation. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>27.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Rehabilitation (Physiotherapy)<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A complementary part of
comprehensive healthcare service and its applications for rehabilitating a
person suffering from constant weakness to the highest level of performance in
family and social life which, in turn, would enhance the healthcare system as
measured by cost-benefit analysis. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>28.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Premium (Subscription)<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The amount payable by the
insured to the insurance company in return for the insurance coverage provided
by the policy during the insurance duration. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>29.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Basis of Direct Billing or Company Billing<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The non-payment facility
granted to the insured at one or more service providers designated by the insurance
company whereby all costs are directly billed to the company. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>30.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Basis for Compensation <o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The method adopted to pay
compensation to the insured for any reimbursable expenses which the insured
person incurs and for which they submit claims. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>31.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Recoverable Expenses<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Actual expenses incurred for
services, supplies and equipment not excluded under the Standard Exclusions
section of the policy provided they are prescribed by a licensed physician as a
result of an illness suffered by the insured. Said expenses shall be necessary,
reasonable and customary in the relevant time and place. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>32.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Claim<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A request presented to the
insurance company or representative thereof by a service provider, an insured
person to recover the expenses of healthcare services covered by the policy.
Claims shall be accompanied by supporting financial and medical documents. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>33.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Claim Supporting Documents <o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Documents proving the
insured's age, nationality and identity, as well as the validity of the
insurance coverage, circumstances of the event giving rise to a claim and
payment of relevant costs, in addition to other documents such as police
reports, invoices, receipts, prescriptions, physician reports, referrals and
recommendations and any other documents that may be required by the company. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>34.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Reimbursement of Expenses Relating to Traffic
Accidents<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A medical claim resulting from
a traffic accident to cover a person injured in said accident, whether such
person was the cause of the accident or otherwise. If such claimed expenses are
recoverable by the injured person (i.e. they are covered under any other
insurance plan, scheme or the like), the insurance company that is first
notified shall be liable to cover the injured person, provide him with medical
treatment and reimburse such expenses, and shall subrogate the insured, injured
person, in recourse to third parties to pay their proportionate share of said
claim. <o:p></o:p></span></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>35.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Reasonable and Customary Medical Expenses<o:p></o:p></span></b></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.0pt;text-indent:-.25in;mso-list:l21 level1 lfo17'><span
style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Medical
expenses agreed upon by the insurance company and service provider which are in
line with fees charged by the majority of licensed physicians or hospitals in
the Kingdom and are common in the market. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
6.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l21 level1 lfo17'><span
style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The medical
expenses which do not differ significantly from what a licensed physician
considers acceptable as being usual and customary for a similar illness. Such
medical expenses may be claimed under the policy. <o:p></o:p></span></span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>36.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Costs of Corpse Repatriation to Home Country<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Costs of preparation and
repatriation of a corpse to the home country.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>37.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>The Law<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>The laws of the Council of
Cooperative Health Insurance of Saudi Arabia (CCHI).<o:p></o:p></span></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>38.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Fraud<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Intentional deceit by an
insurance party leading to obtaining benefits, funds or privileges which are
excluded or exceed the limits for a person or entity. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>39.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Abuse<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Practices by an insurance
party that may lead to obtaining benefits or privileges they are not entitled
to, but without the intent of deceit, fraud, misrepresentation or distortion of
facts in order to obtain such benefit. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>40.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Misleading <o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>Practices by persons or
entities that do not fall within the definition of fraud. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>41.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Endorsement <o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>A document issued by the
company, upon a written request from the insured, on the company’s official
forms dated and signed by an authorized employee to establish the validity of
any amendment to the policy in a manner that does not affect the basic
coverage. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l2 level1 lfo16'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>42.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>TPA<o:p></o:p></span></b></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>This refers to Third Party
Administration.<o:p></o:p></span></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989395'><span
lang=EN-GB style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:9.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<p class=Default style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
style='font-size:9.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=595
 style='width:6.2in;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=167 valign=top style='width:125.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><span style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span style='mso-bookmark:_Toc422989395'><span lang=EN-GB
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=179 valign=top style='width:134.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><span style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span style='mso-bookmark:_Toc422989395'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=167 valign=top style='width:125.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Bupa Arabia<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=179 valign=top style='width:134.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Customer</span></span><span
  style='mso-bookmark:_Toc422989395'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
</table>

<p class=MsoListParagraph style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l20 level1 lfo25'><span style='mso-bookmark:_Toc422989395'><![if !supportLists]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>C.<span
style='font:7.0pt "Times New Roman"'> </span></span></span></b><![endif]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'>Table of Benefits (ToB)</span></b></span><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>Note: </span></b><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>All eligible benefits, as per the table
of benefits below, shall be covered by the policy on reimbursement basis or
direct claims from selected providers.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
lang=EN-GB style='font-size:2.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
lang=EN-GB style='font-size:2.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<table class=TableGrid1 border=1 cellspacing=0 cellpadding=0 width=678
 style='width:508.15pt;margin-left:-28.0pt;border-collapse:collapse;border:
 none;mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:26.95pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;mso-border-alt:
  solid windowtext .5pt;background:#0079C8;mso-background-themecolor:text2;
  padding:0in 5.4pt 0in 5.4pt;height:26.95pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:115%'><b><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;color:white;mso-themecolor:background1'>Benefit<o:p></o:p></span></b></p>
  </td>
  <td width=363 style='width:272.3pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#0079C8;mso-background-themecolor:text2;padding:0in 5.4pt 0in 5.4pt;
  height:26.95pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
  color:white;mso-themecolor:background1;mso-ansi-language:EN-US'>Limit<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Network of Service
  Providers<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
  color:#7F7F7F;mso-themecolor:text1;mso-themetint:128;background:yellow;
  mso-highlight:yellow;mso-ansi-language:EN-US'>&lt; Selected Network &gt;<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Maximum Policy
  Coverage Limit<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>SR 100,000<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Examination and
  Treatment of Emergency Cases<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to the maximum limit<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>In-Patient
  Emergency Treatment<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to the maximum limit<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Deductible<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Nil<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:34.95pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:34.95pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>In-Patient
  Accommodation and Daily Charges<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:34.95pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Shared Room<o:p></o:p></span></p>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-size:4.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to SR 600 per Day<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Treatment of
  Emergency Pregnancy and Delivery Cases<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to SR 5,000<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Treatment and
  Delivery of Premature Babies<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to the maximum limit<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Traveling and
  Companionship of One Direct Family Member<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to SR 5,000<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;height:35.8pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:35.8pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Companion
  Accommodation and Daily Charges<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:35.8pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Shared Room <o:p></o:p></span></p>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-size:4.0pt;
  font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to SR 150 per Day<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Dental Emergency
  Treatment</span><span lang=AR-SA dir=RTL style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to SR 500<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Emergency
  Treatment of Injuries Resulting from Automotive Accidents<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to the maximum limit<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Emergency Dialysis<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to the maximum limit<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;height:29.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:29.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Emergency Medical
  Evacuation Inside and Outside the Kingdom of Saudi Arabia<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:29.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to the maximum limit<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15;height:30.55pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:30.55pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Repatriation of
  Mortal Remains to Country of Origin<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.55pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>Covered up to SR 10,000<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes;height:23.2pt'>
  <td width=314 style='width:235.85pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Geographic Limits<o:p></o:p></span></p>
  </td>
  <td width=363 style='width:272.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.2pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=EN-GB style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'>The Kingdom of Saudi Arabia<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><b><span
lang=EN-GB style='font-size:3.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><b><span
lang=EN-GB style='font-size:9.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><b><span
lang=EN-GB style='font-size:9.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Notes on
Benefits:<o:p></o:p></span></b></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l3 level1 lfo2'><![if !supportLists]><span
lang=EN-GB style='font-size:8.0pt;line-height:115%;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol'><span
style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-size:9.0pt;
line-height:115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>All benefits are applicable on emergency basis only. </span><span
lang=EN-GB style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=595
 style='width:6.2in;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span lang=EN-GB style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow;
  mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></p>
  </td>
  <td width=167 valign=top style='width:125.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span lang=EN-GB style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow;
  mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></p>
  </td>
  <td width=179 valign=top style='width:134.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=167 valign=top style='width:125.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Bupa Arabia<o:p></o:p></span></p>
  </td>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=179 valign=top style='width:134.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Insured</span><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoListParagraph style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l20 level1 lfo25'><a name="_Toc422989397"><![if !supportLists]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0079C8;mso-themecolor:accent1'><span
style='mso-list:Ignore'>D.<span style='font:7.0pt "Times New Roman"'> </span></span></span></b><![endif]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0079C8;mso-themecolor:accent1'>Recoverable
Expenses / Benefits<o:p></o:p></span></b></a></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
style='mso-bookmark:_Toc422989397'><b><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></span></p>

<p class=MsoNormal style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989397'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>For the purpose of this policy, recoverable expenses shall mean the
actual expenses incurred for services, supplies and equipment which are not
excluded in section E of this policy, provided they are prescribed by a
licensed physician as a result of an illness suffered by the insured, and
should consider expenses are necessary, reasonable and customary in the
relevant time and place. <o:p></o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989397'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>Accordingly, the recoverable expenses are subject to emergency
cases and shall include:<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l9 level1 lfo13'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Health
benefits: <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l10 level1 lfo14'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>All expenses of
medical examination, diagnosis, treatment and medicines as shown in the policy
schedule. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l10 level1 lfo14'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>All expenses of
hospitalization. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l10 level1 lfo14'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>C.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Cases of
pregnancy and delivery. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l10 level1 lfo14'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>D.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Treatment of
dental and gum diseases, with the limitation in the case of tooth treatment to
emergencies, including filling, root canal treatment, abscess extracting, in
addition to customary follow-up medical procedures and antibiotics and
painkillers. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l10 level1 lfo14'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>E.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Cases of
premature babies, which shall be covered subject to the maximum of the mother's
benefit. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l10 level1 lfo14'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>F.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Cases of urgent
renal dialysis. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l10 level1 lfo14'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>G.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Urgent medical
evacuation, inside and outside the Kingdom. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l10 level1 lfo14'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>H.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Injuries due to
traffic accidents.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt'><span style='mso-bookmark:_Toc422989397'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l9 level1 lfo13'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The costs of
repatriating the corpse of an insured individual to his/her home country.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in'><span style='mso-bookmark:_Toc422989397'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in'><span style='mso-bookmark:_Toc422989397'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l20 level1 lfo25'><span style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0079C8;mso-themecolor:accent1'><span
style='mso-list:Ignore'>E.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0079C8;mso-themecolor:accent1'>Standard
Exclusions<o:p></o:p></span></b></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
style='mso-bookmark:_Toc422989397'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-bottom:6.0pt'><span style='mso-bookmark:_Toc422989397'><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>The following conditions and treatments are not covered by the Bupa
Inbound Travel Health Insurance policy:<o:p></o:p></span></b></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Any medical
treatment and check-ups that can be postponed until the return of the insurance
beneficiary to his/her home country, including rehabilitation. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Sicknesses
resulting from abuse of some medicines, stimulants or tranquilizers, or from
use of alcohol, narcotics and the like.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Cosmetic
treatment or surgery unless necessitated by a bodily injury not excluded in
this section, and approved by a licensed and selected by Bupa doctor.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>General
check-ups, inoculations, drugs or preventive measures not required for medical
treatment covered under this policy.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Treatment received
by a beneficiary free of charge.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Rest cures,
general health cures and treatment in social welfare institutions.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Any illness or
injury resulting directly from the insured profession, or resulting from the
insured participation in official competitions.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Medically
recognized venereal or sexually transmitted diseases.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Costs of treatment
following diagnosis of HIV or any disease related to HIV, including AIDS and
its derivatives, alternatives or other forms.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>All costs
related to tooth implant, dentures, fixed or movable bridges or orthodontic
treatment, unless resulting from violent external means.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Vision or
hearing correction tests and visual or hearing aids, unless requested by a
licensed physician.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The
beneficiary's transportation expenses within and amongst cities in the Kingdom
by other than ambulances of the Saudi Red Crescent or licensed ambulances.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Hair loss,
baldness or artificial hair.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Psychological,
mental or nervous disorders, unless of an acute nature as specified in the
policy schedule.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>15.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Allergy tests
of any nature, unless relating to medicines, diagnosis or treatment.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>16.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Equipment,
means, drugs and procedures, or hormone treatment aimed at regulating
reproduction, contraception, fertility, infertility, impotence, secondary
sterility, in-vitro fertilization or any other method of artificial
fertilization.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>17.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Any congenital
weakness or deformity unless it is life threatening.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>18.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Any costs or
additional expenses incurred by the beneficiary's companion during a hospital
stay, except for hospital room and board charges for one companion such as a
mother companying her child aged up to twelve years or whenever medically
necessary as assessed by the attending physician.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>19.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Treatment of
acne or any treatment relating to obesity or overweight, excluding covered
medicines.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>20.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Organ or marrow
transplant, or implant of artificial organs to replace any organ of the body.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>21.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Personal risks
set forth in the (Definitions) section of this policy.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>22.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Artificial and
ancillary limps.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>23.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Natural changes
related to menopause, including menstrual disorders.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>24.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>With the
exception of emergency conditions (see definitions), the insurance does not
cover treatment for chronic diseases nor pre-existing conditions. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>25.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Coverage for
illnesses, injuries, infections, physical weaknesses, fatigue, physical
disability and their resulting conditions, if they occur, or of their symptoms
occur, prior to visiting the Kingdom of Saudi Arabia or after the expiration of
the policy.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>26.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Herbal therapy
and natural treatments as well as any other form of alternative medicine
procedures and medications. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>27.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Resistance of
insurance beneficiary to medical evacuation or a return to country of origin. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>28.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Transportation
and treatment that has not been previously arranged by Bupa. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>29.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Illegal
abortions (according to the laws of the Kingdom of Saudi Arabia) or any case of
pregnancy or delivery as well as legal abortions that were not reported during
the time of acquiring healthcare insurance coverage.<span
style='mso-spacerun:yes'>  </span><o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>30.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Suicide,
self-afflicted physical or mental injuries and active participation in
dangerous sports competitions and shows.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>31.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Resistance or
refusal cases of the insurance beneficiary to follow instructions provided by
the company’s physician or by treating practitioner.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>32.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Any unnecessary
treatment or conditions that do not require immediate care and that are not
related to the emergency conditions covered under the insurance. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l15 level1 lfo26'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>33.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The company
shall not proceed to a medical evacuation or repatriation of the insurance
beneficiary in the following cases: <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.0pt;text-indent:-.25in;mso-list:l7 level1 lfo27'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>If there is no
medical requirement or approval for an evacuation or repatriation<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-18.35pt;mso-list:l7 level1 lfo27'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>In case of
pregnancy of more than six months<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-18.35pt;mso-list:l7 level1 lfo27'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>C.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>In case the
beneficiary suffers from mental or psychological disorder unless sedated in the
hospital <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-18.35pt;mso-list:l7 level1 lfo27'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>D.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Repatriation of
the insured mortal remains to a country other than the country of origin.<o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-18.35pt;mso-list:l7 level1 lfo27'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>E.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>In case of
surface wounds or minor injuries such as joint twisting, superficial fissures
or a common illness that can be treated by local doctors, and the beneficiary
shall not be prevented from continuing to sojourn in the host country nor
forced to return to home country. <o:p></o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>This policy- will not cover the health
benefits and repatriation of the remains to country of origin if claims are
directly arising- from the following:<o:p></o:p></span></b></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l17 level1 lfo15'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>War, invasion,
foreign enemy actions, aggressive actions (whether war declared or not). <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l17 level1 lfo15'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Ionic radiation
and contamination with radioactive material resulting from nuclear fuel or any
nuclear waste resulting from the burning of nuclear fuel. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l17 level1 lfo15'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The
radioactive, poisonous, explosive properties or any other hazardous properties
of any nuclear materials stored or any of their components. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l17 level1 lfo15'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The insured
involvement or participation in the service of the armed forces, police or in
any of their operations. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l17 level1 lfo15'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Riots, strikes
terrorism or any similar acts. <o:p></o:p></span></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l17 level1 lfo15'><span
style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Chemical,
biological or bacteria-logical accidents and reactions, if such related to a
job-related injury or occupational hazards.<o:p></o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:red'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:red'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:red'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:red'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:red'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:red'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:red'><o:p>&nbsp;</o:p></span></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:red'><o:p>&nbsp;</o:p></span></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=595
 style='width:6.2in;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
  <td width=167 valign=top style='width:125.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><span style='mso-bookmark:_Toc422989397'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989397'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989397'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989397'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span style='mso-bookmark:_Toc422989397'><span lang=EN-GB
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
  <td width=179 valign=top style='width:134.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><span style='mso-bookmark:_Toc422989397'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span style='mso-bookmark:_Toc422989397'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989397'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span style='mso-bookmark:_Toc422989397'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
  <td width=167 valign=top style='width:125.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989397'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Bupa Arabia<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989397'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989397'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989397'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989397'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
  <td width=179 valign=top style='width:134.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989397'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Insured</span></span><span
  style='mso-bookmark:_Toc422989397'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989397'></span>
 </tr>
</table>

<p class=MsoListParagraphCxSpFirst style='margin-left:.25in;mso-add-space:auto;
text-indent:-.25in;mso-list:l20 level1 lfo25'><span style='mso-bookmark:_Toc422989397'><![if !supportLists]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0079C8;mso-themecolor:accent1'><span
style='mso-list:Ignore'>F.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><b><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0079C8;mso-themecolor:accent1'>General
Terms &amp; Conditions</span></b></span><b><span lang=EN-GB style='font-size:
16.0pt;line-height:115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0079C8;mso-themecolor:accent1'><o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto'><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:red'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Proof of Validity<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>The policy represent the basic level of
insurance coverage granted to the beneficiary starting from the date of his
legal entry to the Kingdom of Saudi Arabia.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Eligibility of Coverage<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>All visitor visa applicants including
their dependents whom would like to enter the Kingdom of Saudi Arabia for the
purpose of visitation – or extension of their stay – or transit (excluding Hajj
&amp; Umrah visitors) as per the stipulated resolution by the Council of
Ministers of Saudi Arabia No. 180 dated 2/5/1435 AH.<span
style='mso-spacerun:yes'>  </span><o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Payment of Premium<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>The insured shall pay the full policy
premium due as agreed upon with Bupa upon applying for his visitor visa to the
Kingdom of Saudi Arabia or when applying to extend his stay, and policy premium
shall be determined by Bupa <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Termination of Beneficiary Insurance Coverage<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;margin-bottom:.0001pt'><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>Coverage of any
benefit under this policy shall automatically terminate in the following cases:
<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.0pt;text-indent:-.25in;mso-list:l19 level1 lfo18'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>If the policy
period ends as defined in the policy schedule.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l19 level1 lfo18'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Upon exhaustion
of the maximum benefit limit provided in the policy.</span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA dir=RTL style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in;mso-add-space:auto'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>Payment of recoverable expenses in
respect of any emergency case that requires continued hospitalization on the
date of termination of coverage shall continue for the period necessary for
treatment of such emergency case, provided that the maximum amount of coverage
provided is limited to the maximum policy coverage limit.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Verification of the Insured’s Health Condition<o:p></o:p></span></b></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l5 level1 lfo8'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Bupa may have
the insured, for whom a claim was submitted for recoverable expenses, examined
by an accredited medical facility for a second medical opinion at the expense
of Bupa for up to two times within the period of his stay in the Kingdom of
Saudi Arabian.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
6.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l5 level1 lfo8'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The insured
shall cooperate with Bupa and allow all necessary measures that may be
reasonably required by and paid for by Bupa for the purpose of supporting its
liabilities, claims or compensations from third parties to which the insured is
deemed liable. The insured may not waive such rights without Bupa's explicit or
implicit consent. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Second Medical Opinions<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>Bupa maintains the right to submit any
medical case for a second medical opinion and the company shall bear the costs
incurring from such request. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Non-Duplication of Benefits<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>In case of a claim for recoverable
expenses due under the policy for an insured who is also covered for the same
expenses under another insurance plan, scheme or the like, Bupa shall be liable
for the coverage of such expenses and shall subrogate the insured in his claims
against third parties for payment of their proportionate share of such claim.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Basis of Direct Billing of Bupa by the PPN<o:p></o:p></span></b></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l14 level1 lfo9'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The insured
shall receive the emergency healthcare treatment from the PPN agreed upon with Bupa
without being required to pay costs of such services.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l14 level1 lfo9'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The service
providers, assigned by Bupa, shall send all invoices relating to medical
expenses incurred in accordance with the policy. Bupa shall assess and process
such expenses and advise the insured whenever expenses reach the maximum
benefit limit. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
6.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l14 level1 lfo9'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>C.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Upon informing
the insured, Bupa may delete any service provider assigned for purposes of the
policy during its validity and appoint substitutes of the same level. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Basis for Compensation (Claim Reimbursement)<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>Bupa shall, in accordance with the
policy's terms, conditions, limitations and exclusions, compensate the insured
within a period not exceeding 30 business days from the date of submitting the
claim on the basis of prevailing prices. <o:p></o:p></span></p>

<p class=MsoNormalCxSpLast style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in;mso-add-space:auto'><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>The insured should
submit reimbursement claims to Bupa within 60 days from the date of the medical
intervention, taking into account the following: <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l4 level1 lfo10'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The claims will
be reimbursed by Bupa after ascertaining its validity and coverage upon receipt
of a completed claims form and submitting with it detailed original invoices as
well as any related documents such as medical history files, travel tickets and
documents. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l4 level1 lfo10'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Reimbursement
amount shall not exceed, under any condition, the limit of coverage. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l4 level1 lfo10'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>C.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The payable
amounts of eligible reimbursement claims are subject to reasonable and
customary charges of the standard costs of services in the Kingdom of Saudi
Arabia.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l4 level1 lfo10'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>D.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Bupa should be
notified immediately in case of death, hospitalization, urgent return to home
country, medical evacuation or accompaniment. The notification should clearly
detail the medical information related to the illness or injury.<span
style='mso-spacerun:yes'>   </span><o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l4 level1 lfo10'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>E.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The
notification should be lodged via a telephone or e-mail to the emergency
services of Bupa, which operates for 24 hours a day. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l4 level1 lfo10'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>F.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The insured
should cooperate with Bupa and notify it concerning any claim for reimbursement
or any action taken against any third party. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l4 level1 lfo10'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>G.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>In addition,
the insured must look after the interest of Bupa and be careful to notify it
and to follow due procedure in case of a claim submitted to a third party. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Cancelation and Refunds <o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in;mso-add-space:auto'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>The insured has no right to cancel the
policy when it comes into force, except in the case of non-entry into Saudi
Arabia, and accordingly the premium is refunded to the insured.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Approvals</span></b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'> <o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in;mso-add-space:auto'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>Bupa shall respond to approval requests
from service providers to provide health service to the insured within a period
not exceeding 60 minutes.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Gender <o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in;mso-add-space:auto'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>For purposes of the policy, words
denoting the masculine gender shall be deemed to include the feminine gender as
well. <o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Notices <o:p></o:p></span></b></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l8 level1 lfo11'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>All notices or
correspondences between the insurance parties shall be formal. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l8 level1 lfo11'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>Bupa shall
notify the insured of the date of expiry of the policy.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
6.0pt;margin-left:45.35pt;text-indent:-.25in;mso-list:l8 level1 lfo11'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>C.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>The insured
shall notify the company of any changes to contact details.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
6.0pt;margin-left:45.35pt'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;
text-indent:-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>A Pledge Not to Visit for Treatment Purposes <o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>The insured or his sponsor or legal
guardian shall pledge not to use this insurance document in order to receive
treatment for an existing condition or a previously diagnosed condition and not
to visit the Kingdom of Saudi Arabia with the intention of receiving treatment
for those conditions under this coverage program. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>15.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Compliance with Policy Provisions <o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>As a precondition to any liability of Bupa,
the insured shall strictly comply with and execute all requirements,
conditions, obligations and commitments stated in the policy. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>16.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Penalties<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>Any disagreement or dispute arising out
of or relating to this policy shall be settled in accordance with article (14)
of the law.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;mso-add-space:auto;text-indent:
-.25in;mso-list:l25 level1 lfo3'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>17.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>Approval of The Policy<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>This policy will be electronically
sanctioned through the entities authorized to do so.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.0pt;
margin-left:.25in'><b><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.0pt;
margin-left:.25in'><b><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>In case of any differences in wording or
meanings, the signed Arabic contract will always prevail over the signed
English contract, if applicable.<o:p></o:p></span></b></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>Customer Service Manager<o:p></o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>Bupa Arabia</span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA dir=RTL style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>P.O. Box 23807<o:p></o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>Jeddah 21436<o:p></o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>Kingdom of Saudi Arabia<o:p></o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>Or <o:p></o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>Call us on:<o:p></o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>(</span><strong><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-theme-font:minor-bidi;background:white;
mso-bidi-font-weight:normal'>800 244 0307</span></strong><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>)<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;tab-stops:
261.1pt'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:
"Arial","sans-serif";mso-bidi-font-weight:bold'><span style='mso-tab-count:
1'>                                                          </span><o:p></o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center'><span lang=EN-GB style='font-size:16.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.45in;margin-bottom:.0001pt;text-align:center'><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=595
 style='width:6.2in;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span lang=EN-GB style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow;
  mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></p>
  </td>
  <td width=167 valign=top style='width:125.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span lang=EN-GB style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow;
  mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></p>
  </td>
  <td width=179 valign=top style='width:134.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=167 valign=top style='width:125.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Bupa Arabia<o:p></o:p></span></p>
  </td>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=179 valign=top style='width:134.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Insured</span><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<b><span lang=EN-GB style='font-size:16.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:#0079C8;mso-themecolor:accent1;mso-ansi-language:
EN-GB;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'><br clear=all
style='page-break-before:always'>
<span style='mso-bookmark:_Toc422989398'></span></span></b>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0079C8;mso-themecolor:accent1'>Declaration of Policyholder<o:p></o:p></span></b></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
3.0pt;margin-left:.25in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
12.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l12 level1 lfo22'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>I acknowledge
that the information mentioned in this agreement is complete and correct.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
12.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l12 level1 lfo22'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>I agree that
the acceptance of my application will be based on the information in this
agreement, and Bupa has the right to contact the hospitals which I am dealing
with to provide any medical information that may be needed.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
12.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l12 level1 lfo22'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>I acknowledge
that failure to declaring a medical condition (within the cases listed in the
medical declaration form) to Bupa may lead to the rejection of reimbursement
claims, rejection of service at PPN, and cancelation of insurance coverage. The
policyholder information, medical declaration form and table of benefits are
considered as integral part of the current and future contracts.</span><span
lang=EN-GB style='font-family:"Arial","sans-serif";color:red'> </span><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
12.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l12 level1 lfo22'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif"'>I,
</span><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>the insured, have
read the terms and conditions of this document with its own table of benefits,
and have approved it.<o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:0in;margin-right:0in;margin-bottom:
12.0pt;margin-left:.25in;text-indent:-.25in;mso-list:l12 level1 lfo22'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-fareast-font-family:
Arial;color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=EN-GB style='font-family:"Arial","sans-serif"'>I,
</span><span lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'>the insured,</span><span
lang=EN-GB style='font-family:"Arial","sans-serif"'> acknowledge and agree that
in case of my eligibility for any surplus from the insurance policy after the
expiration of it, I allow Bupa to donate this surplus for the benefit of
charities.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:12.0pt'><span style='font-family:"Arial","sans-serif";
mso-ansi-language:EN-US'>By signing below, the parties agree that this contract
is the entire contract and agree to all the terms and conditions of this
contract, including the agreement, as well as the subsequent annexes.</span><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'>This contract is subject to the laws, provisions
and regulations adopted by the </span><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>Council of Cooperative Healthcare
Insurance (CCHI).<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:0in'><span lang=EN-GB style='font-family:"Arial","sans-serif";
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:red'><span
style='mso-spacerun:yes'> </span><o:p></o:p></span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=595
 style='width:6.2in;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><a name="_Toc422989400"><span lang=EN-GB style='font-size:
  10.0pt;line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></a></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
  <td width=167 valign=top style='width:125.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><span style='mso-bookmark:_Toc422989400'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989400'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989400'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989400'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right'><span style='mso-bookmark:_Toc422989400'><span lang=EN-GB
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'>Signature<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
  <td width=179 valign=top style='width:134.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><span style='mso-bookmark:_Toc422989400'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span style='mso-bookmark:_Toc422989400'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=105 valign=top style='width:78.4pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989400'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;text-justify:inter-ideograph'><span style='mso-bookmark:_Toc422989400'><span
  lang=EN-GB style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
  <td width=167 valign=top style='width:125.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989400'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Bupa Arabia<o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
  <td width=24 valign=top style='width:.25in;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989400'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989400'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
  <td width=121 valign=top style='width:91.0pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989400'></span>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989400'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
  <td width=179 valign=top style='width:134.0pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
  style='mso-bookmark:_Toc422989400'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-bidi-font-weight:bold'>Insured</span></span><span
  style='mso-bookmark:_Toc422989400'><span lang=EN-GB style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p></o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989400'></span>
 </tr>
</table>

<span style='mso-bookmark:_Toc422989400'></span>

<p class=MsoNormal><span lang=EN-GB style='color:red'><o:p>&nbsp;</o:p></span></p>

                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>

                      
                        <div id="view2" style="height: 600px;overflow-y: scroll;padding-left:50px;padding-right:30px;">
                            <asp:Panel runat="server" ID="pnlView2">
                             
                                 <table>
                                    <tr>
                                        <td>
                                            <div class=WordSection1>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=right
 width=512 style='width:384.05pt;background:#0079C8;border-collapse:collapse;
 mso-table-overlap:never;mso-yfti-tbllook:480;margin-left:-2.25pt;margin-right:
 -2.25pt;mso-table-anchor-vertical:page;mso-table-anchor-horizontal:margin;
 mso-table-left:right;mso-table-top:197.1pt;mso-padding-alt:9.9pt 6.5pt 9.9pt 6.5pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:27.2pt'>
  <td width=512 colspan=2 valign=top style='width:384.05pt;background:#00AEEF;
  padding:0in 9.9pt 0in 9.9pt;height:27.2pt'>
  <p class=BUPFPDeptorCamp dir=RTL style='text-align:right;direction:rtl;
  unicode-bidi:embed;mso-element:frame;mso-element-wrap:around;mso-element-anchor-horizontal:
  margin;mso-element-left:right;mso-element-top:197.1pt;mso-height-rule:exactly'><span
  lang=AR-SA style='font-size:16.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-hansi-font-family:Arial'>&#1608;&#1579;&#1610;&#1602;&#1577;
  &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;</span><span lang=EN-GB
  dir=LTR style='font-size:16.0pt'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:3.65pt'>
  <td width=512 colspan=2 valign=top style='width:384.05pt;background:#00AEEF;
  padding:0in 9.9pt 0in 9.9pt;height:3.65pt'>
  <p class=BUPAnchor style='mso-element:frame;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-left:right;mso-element-top:197.1pt;mso-height-rule:exactly'><v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_22" o:spid="_x0000_s1026" type="#_x0000_t75"
   style='position:absolute;margin-left:32.5pt;margin-top:0;width:331.65pt;
   height:2.2pt;z-index:251659264;visibility:visible;mso-wrap-style:square;
   mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
   mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
   mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
   mso-position-horizontal-relative:text;mso-position-vertical:absolute;
   mso-position-vertical-relative:text;mso-width-percent:0;
   mso-height-percent:0;mso-width-relative:page;mso-height-relative:page'
   o:allowincell="f">
   <v:imagedata src="Bupa%20Arabia%20Inbound%20Travel%20Health%20Insurance%20Product%20-%20Product%20Policy%20Arabic%20V3%200_files/image001.emz"
    o:title=""/>
   <w:anchorlock/>
  </v:shape></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:96.85pt'>
  <td width=512 colspan=2 valign=top style='width:384.05pt;background:#00AEEF;
  padding:0in 9.9pt 0in 9.9pt;height:96.85pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:8.0pt;text-align:right;
  line-height:normal;direction:rtl;unicode-bidi:embed;mso-element:frame;
  mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:
  margin;mso-element-left:right;mso-element-top:197.1pt;mso-height-rule:exactly'><span
  lang=AR-SA style='font-size:28.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-fareast-font-family:"Times New Roman";
  mso-hansi-font-family:Arial;color:white;mso-fareast-language:EN-GB'>&#1608;&#1579;&#1610;&#1602;&#1577;
  &#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
  &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1586;&#1575;&#1574;&#1585;&#1610;&#1606;
  &#1575;&#1604;&#1602;&#1575;&#1583;&#1605;&#1610;&#1606;
  &#1604;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
  &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;</span><span
  lang=EN-GB dir=LTR style='font-size:28.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:white;mso-fareast-language:EN-GB'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes;height:20.4pt'>
  <td width=256 valign=bottom style='width:192.0pt;background:#00AEEF;
  padding:0in 0in 0in 9.9pt;height:20.4pt'>
  <p class=BUPFPURL style='mso-element:frame;mso-element-wrap:around;
  mso-element-anchor-horizontal:margin;mso-element-left:right;mso-element-top:
  197.1pt;mso-height-rule:exactly'><span lang=EN-GB>bupa.com.sa</span></p>
  </td>
  <td width=256 valign=bottom style='width:192.05pt;background:#00AEEF;
  padding:0in 9.9pt 0in 0in;height:20.4pt'>
  <p class=BUPFPDate style='mso-element:frame;mso-element-wrap:around;
  mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;
  mso-element-left:right;mso-element-top:197.1pt;mso-height-rule:exactly'><span
  dir=RTL></span><span lang=AR-SA dir=RTL style='font-size:12.0pt;mso-ansi-font-size:
  11.0pt;font-family:"Times New Roman","serif";mso-ascii-font-family:Arial;
  mso-hansi-font-family:Arial;color:windowtext;background:yellow;mso-highlight:
  yellow'><span dir=RTL></span>8 . &#1571;&#1603;&#1578;&#1608;&#1576;&#1585; .
  2015</span><span lang=EN-GB style='color:windowtext;background:yellow;
  mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<w:Sdt SdtDocPart="t" DocPartType="Cover Pages" DocPartUnique="t" ID="13908099">
 <p class=MsoNormal><span lang=EN-GB><o:p>&nbsp;</o:p><w:sdtPr></w:sdtPr></span></p>
 <p class=MsoNormal><span lang=EN-GB><o:p>&nbsp;</o:p></span></p>
 <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left
  width="80%" style='width:80.0%;border-collapse:collapse;mso-yfti-tbllook:
  1184;mso-table-lspace:9.35pt;margin-left:7.1pt;mso-table-rspace:9.35pt;
  margin-right:7.1pt;mso-table-anchor-vertical:margin;mso-table-anchor-horizontal:
  margin;mso-table-left:left;mso-table-top:bottom;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
  <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
   <td width=511 valign=top style='width:383.6pt;padding:.15in 5.75pt .15in 5.75pt'>
   <p class=MsoNoSpacing style='mso-element:frame;mso-element-frame-hspace:
   9.35pt;mso-element-wrap:around;mso-element-anchor-horizontal:margin;
   mso-element-top:bottom;mso-height-rule:exactly'><span lang=EN-GB
   style='color:#0079C8;mso-themecolor:accent1;mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></p>
   </td>
  </tr>
 </table>
 <p class=MsoNormal><span lang=EN-GB><o:p>&nbsp;</o:p></span></p>
 <p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
 Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
 <p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
 Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
 <p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
 Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
 <span lang=EN-GB style='font-size:11.0pt;line-height:115%;font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
 mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-bidi;
 mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-GB;mso-fareast-language:
 EN-US;mso-bidi-language:AR-SA'><br clear=all style='mso-special-character:
 line-break;page-break-before:always'>
 </span>
 <p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif";
 mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
 Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>
</w:Sdt>

<h1 dir=RTL style='text-align:right;direction:rtl;unicode-bidi:embed'><b><span
lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></h1>

<h1 dir=RTL style='text-align:right;direction:rtl;unicode-bidi:embed'><b><span
lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1578;&#1601;&#1575;&#1602;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1589;&#1581;&#1610; &#1593;&#1604;&#1609; &#1575;&#1604;&#1586;&#1575;&#1574;&#1585;&#1610;&#1606;
&#1575;&#1604;&#1602;&#1575;&#1583;&#1605;&#1610;&#1606; &#1604;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577; &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;<o:p></o:p></span></b></h1>

<p class=MsoNormal dir=RTL style='text-align:right;direction:rtl;unicode-bidi:
embed'><u><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ansi-font-weight:
bold'><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p class=MsoNormal dir=RTL style='text-align:right;direction:rtl;unicode-bidi:
embed'><u><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ansi-font-weight:
bold'><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p class=MsoNormal dir=RTL style='text-align:right;line-height:150%;direction:
rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ansi-font-weight:bold'>&#1608;&#1601;&#1602;&#1575;&#1611; &#1604;&#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;
&#1575;&#1604;&#1587;&#1575;&#1574;&#1583;&#1577; &#1601;&#1610; &#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577; &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;&#1548;
&#1601;&#1573;&#1606; &#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; (&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607;) &#1571;&#1608; &#1603;&#1601;&#1610;&#1604;&#1607;
&#1571;&#1608; &#1608;&#1604;&#1610; &#1571;&#1605;&#1585;&#1607; &#1602;&#1583;
&#1578;&#1602;&#1583;&#1605; &#1604;&#1588;&#1585;&#1603;&#1577;
&#1576;&#1608;&#1576;&#1575; &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
&#1604;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610; (&#1608;&#1610;&#1588;&#1575;&#1585;
&#1573;&#1604;&#1610;&#1607;&#1575; &#1601;&#1610; &#1607;&#1584;&#1575;
&#1575;&#1604;&#1605;&#1587;&#1578;&#1606;&#1583; &#1576;&#1575;&#1587;&#1605; &quot;&#1576;&#1608;&#1576;&#1575;&quot;)
&#1604;&#1594;&#1585;&#1590; &#1588;&#1585;&#1575;&#1569; &#1608;&#1579;&#1610;&#1602;&#1577;
&#1578;&#1571;&#1605;&#1610;&#1606; &#1589;&#1581;&#1610; &#1593;&#1604;&#1609;
&#1575;&#1604;&#1586;&#1575;&#1574;&#1585;&#1610;&#1606; &#1575;&#1604;&#1602;&#1575;&#1583;&#1605;&#1610;&#1606;
&#1604;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577; &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;
(&#1608;&#1575;&#1604;&#1578;&#1610; &#1578;&#1593;&#1583;
&#1573;&#1581;&#1583;&#1609;
&#1575;&#1604;&#1605;&#1606;&#1578;&#1580;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
&#1575;&#1604;&#1582;&#1575;&#1590;&#1593;&#1577;
&#1604;&#1571;&#1606;&#1592;&#1605;&#1577; &#1605;&#1580;&#1604;&#1587;
&#1575;&#1604;&#1590;&#1605;&#1575;&#1606; &#1575;&#1604;&#1589;&#1581;&#1610;
&#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610;) &#1604;&#1606;&#1601;&#1587;&#1607;
&#1571;&#1608; &#1605;&#1603;&#1601;&#1608;&#1604;&#1607;&#1548; &#1608;&#1575;&#1587;&#1605;&#1607;
&#1605;&#1583;&#1585;&#1580; &#1601;&#1610; &#1580;&#1583;&#1608;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1575;&#1604;&#1605;&#1585;&#1601;&#1602;
&#1576;&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;&#1548;
&#1608;&#1583;&#1601;&#1593; &#1575;&#1604;&#1602;&#1587;&#1591;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610; &#1604;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif"'><o:p></o:p></span></b></p>

<p class=MsoNormal dir=RTL style='text-align:right;line-height:150%;direction:
rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif"'>&#1608;&#1601;&#1602;&#1575;&#1611;
&#1604;&#1571;&#1593;&#1604;&#1575;&#1607;&#1548; &#1578;&#1578;&#1601;&#1602; &#1576;&#1608;&#1576;&#1575;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577; &#1605;&#1593; &#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1571;&#1608; <span
style='mso-ansi-font-weight:bold'>&#1603;&#1601;&#1610;&#1604;&#1607;
&#1571;&#1608; &#1608;&#1604;&#1610;&#1607; </span>&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1606;&#1601;&#1602;&#1575;&#1578; &#1578;&#1608;&#1601;&#1610;&#1585; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1589;&#1581;&#1610; &#1604;&#1604;&#1586;&#1575;&#1574;&#1585;&#1610;&#1606;
&#1575;&#1604;&#1602;&#1575;&#1583;&#1605;&#1610;&#1606; &#1573;&#1604;&#1609; <span
style='mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577; &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;</span>
&#1604;&#1607; &#1571;&#1608; &#1604;&#1605;&#1603;&#1601;&#1608;&#1604;&#1607;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1593;&#1604;&#1609; &#1575;&#1604;&#1606;&#1581;&#1608;
&#1575;&#1604;&#1605;&#1606;&#1589;&#1608;&#1589; &#1593;&#1604;&#1610;&#1607; &#1601;&#1610;&#1607;
&#1608;&#1576;&#1606;&#1575;&#1569;&#1611; &#1593;&#1604;&#1609; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
&#1608;&#1575;&#1604;&#1578;&#1593;&#1585;&#1610;&#1601;&#1575;&#1578; &#1608;&#1575;&#1604;&#1581;&#1583;&#1608;&#1583;
&#1608;&#1575;&#1604;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569;&#1575;&#1578;
&#1608;&#1606;&#1591;&#1575;&#1602;
&#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;
&#1575;&#1604;&#1605;&#1594;&#1591;&#1575;&#1577; &#1608;&#1575;&#1604;&#1605;&#1583;&#1585;&#1580;&#1577;
&#1601;&#1610; &#1580;&#1583;&#1608;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;.</span><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif"'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='text-align:right;line-height:150%;direction:
rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif"'>&#1608;&#1576;&#1606;&#1575;&#1569;&#1611;
&#1593;&#1604;&#1609; &#1571;&#1606; &#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577; &#1604;&#1604;&#1586;&#1575;&#1574;&#1585;&#1610;&#1606;
&#1575;&#1604;&#1602;&#1575;&#1583;&#1605;&#1610;&#1606; &#1573;&#1604;&#1609; <span
style='mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577; &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;</span>
&#1605;&#1578;&#1575;&#1581;&#1577; &#1604;&#1604;&#1588;&#1585;&#1575;&#1569;
&#1593;&#1576;&#1585; &#1575;&#1604;&#1573;&#1606;&#1578;&#1585;&#1606;&#1578;
&#1576;&#1588;&#1603;&#1604; &#1585;&#1574;&#1610;&#1587;&#1610;&#1548; &#1601;&#1573;&#1606;
&#1575;&#1604;&#1606;&#1602;&#1585; &#1593;&#1604;&#1609; &#1586;&#1585; &quot;&#1605;&#1608;&#1575;&#1601;&#1602;&quot;
&#1593;&#1604;&#1609; &#1589;&#1601;&#1581;&#1577; &#1608;&#1579;&#1610;&#1602;&#1577;
&#1578;&#1571;&#1605;&#1610;&#1606; &#1576;&#1608;&#1576;&#1575;
&#1575;&#1604;&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1577;
&#1575;&#1604;&#1582;&#1575;&#1589;&#1577; &#1576;&#1607;&#1584;&#1575;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606; &#1610;&#1602;&#1608;&#1605;
&#1576;&#1605;&#1602;&#1575;&#1605; &#1578;&#1608;&#1602;&#1610;&#1593;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1571;&#1608; <span
style='mso-ansi-font-weight:bold'>&#1603;&#1601;&#1610;&#1604;&#1607;
&#1571;&#1608; &#1608;&#1604;&#1610;&#1607; </span>&#1593;&#1604;&#1609; &#1575;&#1578;&#1601;&#1575;&#1602;&#1610;&#1577;
&#1607;&#1584;&#1575; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1608;&#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
&#1593;&#1604;&#1609; &#1576;&#1606;&#1608;&#1583;
&#1608;&#1588;&#1585;&#1608;&#1591; &#1608;&#1581;&#1583;&#1608;&#1583;
&#1608;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569;&#1575;&#1578;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif"'><o:p></o:p></span></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=EN-GB style='font-size:16.0pt;line-height:
115%;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=AR-SA dir=RTL style='font-size:22.0pt;line-height:
115%;font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=AR-SA dir=RTL style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=AR-SA dir=RTL style='font-size:9.0pt;line-height:
115%;font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=606
 style='width:454.5pt;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;direction:rtl;unicode-bidi:
  embed'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;mso-border-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;mso-border-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;tab-stops:center 66.6pt;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
  &#1604;&#1607;</span><span lang=AR-SA style='font-size:10.0pt;line-height:
  115%;font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'><span
  style='mso-tab-count:1'>            </span></span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

</div>

<h1 dir=RTL style='margin-top:0in;margin-right:19.45pt;margin-bottom:10.0pt;
margin-left:0in;text-align:right;text-indent:-.25in;mso-list:l24 level1 lfo24;
direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=AR-SA
style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-font-family:
Arial;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>&#1571;&#8204;.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b><span lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:
Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'>&#1580;&#1583;&#1608;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span></b><b><span
lang=EN-GB dir=LTR style='font-size:16.0pt;mso-ascii-font-family:Arial;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><o:p></o:p></span></b></h1>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=676
 style='width:507.1pt;margin-left:-30.35pt;border-collapse:collapse;border:
 none;mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:12.2pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.2pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman";color:#333333;background:yellow;
  mso-highlight:yellow'><span dir=RTL></span>&lt; &#1610;&#1608;&#1605;/ &#1588;&#1607;&#1585;/&#1587;&#1606;&#1577;
  &gt;</span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow;mso-ansi-language:EN-US'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.2pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1578;&#1575;&#1585;&#1610;&#1582;
  &#1573;&#1576;&#1585;&#1575;&#1605; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
  &#1608;&#1588;&#1585;&#1575;&#1569; &#1608;&#1579;&#1610;&#1602;&#1577; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:39.55pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.55pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:"Times New Roman";
  mso-hansi-font-family:"Times New Roman"'>&#1588;&#1585;&#1603;&#1577; &#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577; &#1604;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
  &#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610;<o:p></o:p></span></p>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1587;&#1580;&#1604;
  &#1578;&#1580;&#1575;&#1585;&#1610; &#1585;&#1602;&#1605; 4030178881<o:p></o:p></span></p>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1589;&#1606;&#1583;&#1608;&#1602;
  &#1576;&#1585;&#1610;&#1583; 23807&#1548; &#1580;&#1583;&#1577; 21436<o:p></o:p></span></p>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
  &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:39.55pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1587;&#1605; &#1588;&#1585;&#1603;&#1577;
  &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=EN-GB dir=LTR style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman"'>102</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1585;&#1605;&#1586; &#1588;&#1585;&#1603;&#1577;
  &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:3.5pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:3.5pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:"Times New Roman";
  mso-hansi-font-family:"Times New Roman"'>&#1578;&#1571;&#1605;&#1610;&#1606; &#1589;&#1581;&#1610;
  &#1593;&#1604;&#1609; &#1575;&#1604;&#1586;&#1575;&#1574;&#1585;&#1610;&#1606;</span><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'> </span><span lang=AR-SA
  style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-font-family:
  "Times New Roman";mso-fareast-font-family:"Times New Roman";mso-hansi-font-family:
  "Times New Roman"'>&#1575;&#1604;&#1602;&#1575;&#1583;&#1605;&#1610;&#1606; &#1604;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577; &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;</span><i><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></i></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3.5pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;tab-stops:center 117.65pt;direction:rtl;
  unicode-bidi:embed'><span lang=AR-SA style='font-size:10.0pt;font-family:
  "Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>&#1606;&#1608;&#1593;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:12.55pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.55pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=EN-GB style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333'><span dir=RTL></span><span style='mso-spacerun:yes'> </span></span><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'>&lt; &#1571;&#1610;&#1575;&#1605;
  &gt;</span><span lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.55pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1605;&#1583;&#1577; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;:</span><span
  lang=EN-GB dir=LTR style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:2.5pt'>
  <td width=348 style='width:261.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:white;mso-background-themecolor:
  background1;padding:0in 5.4pt 0in 5.4pt;height:2.5pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:white;mso-themecolor:background1'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;background:white;mso-background-themecolor:
  background1;padding:0in 5.4pt 0in 5.4pt;height:2.5pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:white;mso-themecolor:background1'><o:p>&nbsp;</o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:3.55pt'>
  <td width=676 colspan=2 style='width:507.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:black;mso-background-themecolor:text1;padding:0in 5.4pt 0in 5.4pt;
  height:3.55pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:white;mso-themecolor:background1'>&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span></b><b><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:white;mso-themecolor:background1'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:7.05pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1585;&#1602;&#1605; &#1575;&#1604;&#1593;&#1590;&#1608;&#1610;&#1577; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1585;&#1602;&#1605; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
  (&#1585;&#1602;&#1605; &#1575;&#1604;&#1593;&#1590;&#1608;&#1610;&#1577;):</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1585;&#1602;&#1605; &#1575;&#1604;&#1580;&#1608;&#1575;&#1586; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1585;&#1602;&#1605; &#1580;&#1608;&#1575;&#1586;
  &#1575;&#1604;&#1587;&#1601;&#1585;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1610;&#1608;&#1605;/ &#1588;&#1607;&#1585;/&#1587;&#1606;&#1577; &gt;<o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1578;&#1575;&#1585;&#1610;&#1582;
  &#1576;&#1583;&#1575;&#1610;&#1577; &#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;:</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1610;&#1608;&#1605;/ &#1588;&#1607;&#1585;/&#1587;&#1606;&#1577; &gt;<o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1578;&#1575;&#1585;&#1610;&#1582;
  &#1575;&#1606;&#1578;&#1607;&#1575;&#1569; &#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;:</span><span
  dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;&#1583;&#1608;&#1604;&#1575;&#1585;
  &#1571;&#1605;&#1585;&#1610;&#1603;&#1610; &gt;</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:"Times New Roman";color:#333333;background:
  yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1602;&#1610;&#1605;&#1577;</span><span
  dir=LTR></span><span lang=AR-SA dir=LTR style='font-size:10.0pt;font-family:
  "Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'><span
  dir=LTR></span> </span><span lang=AR-SA style='font-size:10.0pt;font-family:
  "Times New Roman","serif";mso-fareast-font-family:"Times New Roman"'>&#1608;&#1579;&#1610;&#1602;&#1577;
  &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;:</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12;height:3.55pt'>
  <td width=676 colspan=2 style='width:507.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:black;mso-background-themecolor:text1;padding:0in 5.4pt 0in 5.4pt;
  height:3.55pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:white;mso-themecolor:background1'>&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1581;&#1575;&#1605;&#1604;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
  &#1575;&#1604;&#1585;&#1574;&#1610;&#1587;&#1610;</span></b><b><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:white;mso-themecolor:background1'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1575;&#1587;&#1605;
  &#1575;&#1604;&#1603;&#1575;&#1605;&#1604; &gt;</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:"Times New Roman";color:#333333;background:
  yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1587;&#1605; &#1581;&#1575;&#1605;&#1604;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;:</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1580;&#1606;&#1587; &gt;<o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1604;&#1580;&#1606;&#1587;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1610;&#1608;&#1605;/ &#1588;&#1607;&#1585;/&#1587;&#1606;&#1577; &gt;<o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1578;&#1575;&#1585;&#1610;&#1582;
  &#1575;&#1604;&#1605;&#1610;&#1604;&#1575;&#1583;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1580;&#1606;&#1587;&#1610;&#1577; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1604;&#1580;&#1606;&#1587;&#1610;&#1577;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:17;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1593;&#1606;&#1608;&#1575;&#1606; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1593;&#1606;&#1608;&#1575;&#1606;
  &#1575;&#1604;&#1573;&#1602;&#1575;&#1605;&#1577; &#1601;&#1610; &#1576;&#1604;&#1583;
  &#1575;&#1604;&#1573;&#1602;&#1575;&#1605;&#1577;:</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:18;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1605;&#1583;&#1610;&#1606;&#1577; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1604;&#1605;&#1583;&#1610;&#1606;&#1577;
  &#1601;&#1610; &#1576;&#1604;&#1583; &#1575;&#1604;&#1573;&#1602;&#1575;&#1605;&#1577;:</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:19;height:12.2pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:12.2pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1585;&#1605;&#1586;
  &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1610; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:12.2pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1604;&#1585;&#1605;&#1586;
  &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1610; &#1601;&#1610; &#1576;&#1604;&#1583;
  &#1575;&#1604;&#1573;&#1602;&#1575;&#1605;&#1577;:</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:20;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1589;&#1606;&#1583;&#1608;&#1602;
  &#1575;&#1604;&#1576;&#1585;&#1610;&#1583; &gt;</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:"Times New Roman";color:#333333;background:
  yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1589;&#1606;&#1583;&#1608;&#1602;
  &#1575;&#1604;&#1576;&#1585;&#1610;&#1583; &#1601;&#1610; &#1576;&#1604;&#1583;
  &#1575;&#1604;&#1573;&#1602;&#1575;&#1605;&#1577;:</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:21;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1585;&#1602;&#1605; &#1575;&#1604;&#1578;&#1608;&#1575;&#1589;&#1604; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1585;&#1602;&#1605; &#1575;&#1604;&#1580;&#1608;&#1575;&#1604;:</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:22;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1576;&#1585;&#1610;&#1583;
  &#1575;&#1604;&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1604;&#1576;&#1585;&#1610;&#1583;
  &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;:</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:23;height:4.9pt'>
  <td width=676 colspan=2 style='width:507.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:black;mso-background-themecolor:text1;padding:0in 5.4pt 0in 5.4pt;
  height:4.9pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:white;mso-themecolor:background1'>&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1593;&#1575;&#1604;
  (&#1575;&#1582;&#1578;&#1610;&#1575;&#1585;&#1610;)</span></b><b><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:white;mso-themecolor:background1'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:24;height:2.85pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:2.85pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1575;&#1587;&#1605;
  &#1575;&#1604;&#1603;&#1575;&#1605;&#1604; &gt;</span><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:"Times New Roman";mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:"Times New Roman";color:#333333;background:
  yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:2.85pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1604;&#1575;&#1587;&#1605;
  &#1575;&#1604;&#1603;&#1575;&#1605;&#1604;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:25;height:2.85pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:2.85pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1580;&#1606;&#1587; &gt;<o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:2.85pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1575;&#1604;&#1580;&#1606;&#1587;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:26;height:7.6pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1589;&#1604;&#1577; &#1575;&#1604;&#1602;&#1585;&#1575;&#1576;&#1577; &gt;</span><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:"Times New Roman";
  color:#333333;background:yellow;mso-highlight:yellow'><o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:7.6pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1589;&#1604;&#1577; &#1602;&#1585;&#1575;&#1576;&#1577;:<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:27;mso-yfti-lastrow:yes;height:6.65pt'>
  <td width=348 style='width:261.0pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:6.65pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  "Times New Roman";mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1610;&#1608;&#1605;/ &#1588;&#1607;&#1585;/&#1587;&#1606;&#1577; &gt;<o:p></o:p></span></p>
  </td>
  <td width=328 valign=top style='width:246.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:6.65pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;font-family:"Times New Roman","serif";
  mso-fareast-font-family:"Times New Roman"'>&#1578;&#1575;&#1585;&#1610;&#1582;
  &#1575;&#1604;&#1605;&#1610;&#1604;&#1575;&#1583;:<o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal dir=RTL style='margin-bottom:3.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-bottom:3.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif"'>&#1578;&#1604;&#1578;&#1586;&#1605;
&#1576;&#1608;&#1576;&#1575; &#1576;&#1578;&#1602;&#1583;&#1610;&#1605; &#1605;&#1606;&#1575;&#1601;&#1593;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1591;&#1608;&#1575;&#1604;
&#1601;&#1578;&#1585;&#1577; &#1587;&#1585;&#1610;&#1575;&#1606;&#1607;&#1575;
&#1604;&#1581;&#1575;&#1605;&#1604;&#1607;&#1575; &#1608;&#1601;&#1602;&#1575;&#1611;
&#1604;&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;
&#1575;&#1604;&#1605;&#1601;&#1589;&#1604; &#1601;&#1610;
&#1601;&#1602;&#1585;&#1577; &quot;&#1578;&quot; &#1605;&#1606;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;. &#1608;&#1610;&#1608;&#1575;&#1601;&#1602;
&#1581;&#1575;&#1605;&#1604; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1593;&#1604;&#1609;
&#1583;&#1601;&#1593; &#1603;&#1575;&#1605;&#1604; &#1602;&#1610;&#1605;&#1577;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577; &#1604;&#1576;&#1608;&#1576;&#1575;
&#1601;&#1610; &#1575;&#1604;&#1578;&#1575;&#1585;&#1610;&#1582; &#1575;&#1604;&#1605;&#1581;&#1583;&#1583;
&#1601;&#1610; &#1575;&#1604;&#1580;&#1583;&#1608;&#1604; &#1571;&#1593;&#1604;&#1575;&#1607;.</span><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif"'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l5 level1 lfo2;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><span lang=EN-GB style='font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol'><span
style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-language:EN-US'>&#1610;&#1587;&#1578;&#1591;&#1610;&#1593;
&#1603;&#1604; &#1586;&#1575;&#1574;&#1585;
&#1604;&#1571;&#1585;&#1575;&#1590;&#1610;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577; &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;
&#1588;&#1585;&#1575;&#1569; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1608;&#1575;&#1604;&#1578;&#1605;&#1578;&#1593;
&#1576;&#1605;&#1606;&#1575;&#1601;&#1593; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1591;&#1576;&#1610; (&#1576;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569;
&#1575;&#1604;&#1581;&#1580;&#1575;&#1580;
&#1608;&#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1585;&#1610;&#1606;).</span><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l5 level1 lfo2;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><span lang=EN-GB style='font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol'><span
style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-language:EN-US'>&#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1578;&#1580;&#1583;&#1610;&#1583; &#1601;&#1602;&#1591;
&#1601;&#1610; &#1581;&#1575;&#1604;
&#1578;&#1605;&#1583;&#1610;&#1583;/&#1578;&#1580;&#1583;&#1610;&#1583;
&#1578;&#1571;&#1588;&#1610;&#1585;&#1577;
&#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577;&#1548; &#1608;&#1601;&#1610;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1581;&#1575;&#1604;&#1577;
&#1610;&#1578;&#1605; &#1575;&#1604;&#1578;&#1580;&#1583;&#1610;&#1583;
&#1605;&#1606; &#1582;&#1604;&#1575;&#1604; &#1573;&#1581;&#1583;&#1609;
&#1602;&#1606;&#1608;&#1575;&#1578; &#1575;&#1604;&#1576;&#1610;&#1593; <span
style='mso-spacerun:yes'>    </span>&#1604;&#1576;&#1608;&#1576;&#1575;.</span><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;
text-indent:-.25in;line-height:normal;mso-list:l5 level1 lfo2;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><span style='font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-ansi-language:
EN-US'><span style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-language:EN-US'>&#1578;&#1589;&#1583;&#1585; &#1608;&#1579;&#1610;&#1602;&#1577;
&#1608;&#1575;&#1581;&#1583;&#1577; &#1604;&#1603;&#1604; &#1586;&#1575;&#1574;&#1585;
&#1602;&#1575;&#1583;&#1605;</span><span dir=LTR></span><span lang=AR-SA
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span> </span><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-language:EN-US'>&#1604;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;</span><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
</span><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
</span><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;
(&#1576;&#1605;&#1575; &#1601;&#1610; &#1584;&#1604;&#1603; &#1575;&#1604;&#1605;&#1585;&#1575;&#1601;&#1602;&#1610;&#1606;
&#1604;&#1604;&#1586;&#1575;&#1574;&#1585; &#1605;&#1606; &#1571;&#1601;&#1585;&#1575;&#1583;
&#1571;&#1587;&#1585;&#1578;&#1607;).</span><span dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:
EN-US'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l5 level1 lfo2;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><span lang=EN-GB style='font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol'><span
style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1594;&#1610;&#1585;
&#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1578;&#1593;&#1583;&#1610;&#1604;.</span><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:3.0pt;margin-right:0in;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1610;&#1580;&#1576;
&#1578;&#1593;&#1576;&#1574;&#1577; &#1606;&#1605;&#1608;&#1584;&#1580; &#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581;
&#1575;&#1604;&#1591;&#1576;&#1610; &#1575;&#1604;&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
&#1604;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1605;&#1606; &#1602;&#1576;&#1604; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1571;&#1608; &#1605;&#1606; &#1602;&#1576;&#1604; &#1603;&#1601;&#1610;&#1604;&#1607;
&#1571;&#1608; &#1608;&#1604;&#1610; &#1571;&#1605;&#1585;&#1607; (&#1575;&#1604;&#1585;&#1580;&#1575;&#1569;
&#1575;&#1604;&#1606;&#1592;&#1585; &#1601;&#1610; &#1601;&#1602;&#1585;&#1577;
&quot;&#1583;&quot; &#1576;&#1582;&#1589;&#1608;&#1589;
&#1606;&#1605;&#1608;&#1584;&#1580;
&#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581;)&#1548; &#1608;&#1604;&#1606;
&#1578;&#1594;&#1591;&#1610; &#1576;&#1608;&#1576;&#1575; &#1571;&#1610;
&#1593;&#1604;&#1577; &#1604;&#1605; &#1610;&#1578;&#1605;
&#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581; &#1593;&#1606;&#1607;&#1575;.<o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:3.0pt;margin-right:0in;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:3.0pt;margin-right:0in;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:3.0pt;margin-right:0in;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:3.0pt;margin-right:0in;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:3.0pt;margin-right:0in;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=606
 style='width:454.5pt;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'><a name="_Toc422989395"></a>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span style='mso-bookmark:
  _Toc422989395'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span style='mso-bookmark:
  _Toc422989395'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><span lang=AR-SA style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span></span><span
  style='mso-bookmark:_Toc422989395'><b style='mso-bidi-font-weight:normal'><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=228 valign=top style='width:171.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><b style='mso-bidi-font-weight:normal'><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;direction:rtl;unicode-bidi:
  embed'><span style='mso-bookmark:_Toc422989395'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><span lang=AR-SA style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span></span><span
  style='mso-bookmark:_Toc422989395'><b style='mso-bidi-font-weight:normal'><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span style='mso-bookmark:
  _Toc422989395'><span lang=AR-SA style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow;
  mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span></span><span
  style='mso-bookmark:_Toc422989395'><b style='mso-bidi-font-weight:normal'><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span style='mso-bookmark:
  _Toc422989395'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
  dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span
  style='mso-bookmark:_Toc422989395'><b style='mso-bidi-font-weight:normal'><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=228 valign=top style='width:171.0pt;border:none;mso-border-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;tab-stops:center 66.6pt;
  direction:rtl;unicode-bidi:embed'><span style='mso-bookmark:_Toc422989395'><span
  lang=AR-SA style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
  &#1604;&#1607;</span></span><span style='mso-bookmark:_Toc422989395'><span
  lang=AR-SA style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><span style='mso-tab-count:1'>            </span></span></span><span
  style='mso-bookmark:_Toc422989395'><b style='mso-bidi-font-weight:normal'><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif"'><o:p></o:p></span></b></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span style='mso-bookmark:
  _Toc422989395'><span lang=AR-SA style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'><span
  style='mso-bookmark:_Toc422989395'></span>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span style='mso-bookmark:
  _Toc422989395'><span lang=AR-SA style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></span></p>
  </td>
  <span style='mso-bookmark:_Toc422989395'></span>
 </tr>
</table>

</div>

<span style='mso-bookmark:_Toc422989395'></span>

<h1 dir=RTL style='margin-top:0in;margin-right:19.45pt;margin-bottom:10.0pt;
margin-left:0in;text-align:right;text-indent:-.25in;mso-list:l24 level1 lfo24;
direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=AR-SA
style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-font-family:
Arial;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>&#1576;&#8204;.<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b><span lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:
Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'>&#1575;&#1604;&#1578;&#1593;&#1585;&#1610;&#1601;&#1575;&#1578;</span></b><b><span
lang=EN-GB dir=LTR style='font-size:16.0pt;mso-ascii-font-family:Arial;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><o:p></o:p></span></b></h1>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1604;&#1571;&#1594;&#1585;&#1575;&#1590;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1601;&#1573;&#1606; &#1575;&#1604;&#1603;&#1604;&#1605;&#1575;&#1578;
&#1608;&#1575;&#1604;&#1593;&#1576;&#1575;&#1585;&#1575;&#1578;
&#1608;&#1575;&#1604;&#1578;&#1593;&#1575;&#1576;&#1610;&#1585;
&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577; &#1587;&#1608;&#1601;
&#1578;&#1601;&#1587;&#1585; &#1571;&#1610;&#1606;&#1605;&#1575;
&#1608;&#1585;&#1583;&#1578; &#1601;&#1610;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1571;&#1608;
&#1605;&#1604;&#1575;&#1581;&#1602;&#1607;&#1575; &#1571;&#1608;
&#1605;&#1585;&#1601;&#1602;&#1575;&#1578;&#1607;&#1575;
&#1608;&#1601;&#1602;&#1575;&#1611;
&#1604;&#1604;&#1578;&#1593;&#1585;&#1610;&#1601;&#1575;&#1578;
&#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577;
&#1571;&#1583;&#1606;&#1575;&#1607;: <o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1606;&#1592;&#1575;&#1605;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1606;&#1592;&#1575;&#1605;
&#1575;&#1604;&#1590;&#1605;&#1575;&#1606; &#1575;&#1604;&#1589;&#1581;&#1610;
&#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610;
&#1575;&#1604;&#1605;&#1591;&#1576;&#1602; &#1601;&#1610;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1582;&#1575;&#1589;
&#1576;&#1575;&#1604;&#1586;&#1575;&#1574;&#1585;&#1610;&#1606;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1576;&#1610;&#1606;&#1577;
&#1575;&#1604;&#1583;&#1575;&#1604;&#1577; &#1593;&#1604;&#1609;
&#1602;&#1610;&#1575;&#1605; &#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577; &#1576;&#1605;&#1608;&#1580;&#1576;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1605;&#1593; &#1580;&#1583;&#1608;&#1604;&#1607;&#1575;
&#1608;&#1605;&#1604;&#1575;&#1581;&#1602;&#1607;&#1575; &#1571;&#1608;
&#1605;&#1585;&#1575;&#1601;&#1602;&#1607;&#1575;.</span><span lang=EN-GB
dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1605;&#1583;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1583;&#1577;
&#1575;&#1604;&#1605;&#1576;&#1610;&#1606;&#1577; &#1601;&#1610;
&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1610; &#1610;&#1576;&#1602;&#1609; &#1601;&#1610;&#1607;&#1575;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606; &#1587;&#1575;&#1585;&#1610;
&#1575;&#1604;&#1605;&#1601;&#1593;&#1608;&#1604;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1605;&#1583;&#1577;
&#1587;&#1585;&#1610;&#1575;&#1606;
&#1575;&#1604;&#1605;&#1601;&#1593;&#1608;&#1604;
(&#1587;&#1585;&#1610;&#1575;&#1606;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;)</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1593;&#1583;&#1583;
&#1575;&#1604;&#1571;&#1610;&#1575;&#1605; &#1575;&#1604;&#1578;&#1610;
&#1587;&#1578;&#1603;&#1608;&#1606;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1582;&#1604;&#1575;&#1604;&#1607;&#1575; &#1606;&#1575;&#1601;&#1584;&#1577; &#1575;&#1604;&#1605;&#1601;&#1593;&#1608;&#1604;
&#1608;&#1578;&#1576;&#1583;&#1571; &#1605;&#1606;
&#1578;&#1575;&#1585;&#1610;&#1582; &#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
&#1575;&#1604;&#1606;&#1592;&#1575;&#1605;&#1610; &#1573;&#1604;&#1609; &#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1578;&#1575;&#1585;&#1610;&#1582;
&#1575;&#1604;&#1575;&#1576;&#1578;&#1583;&#1575;&#1569;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1578;&#1575;&#1585;&#1610;&#1582;
&#1575;&#1604;&#1605;&#1576;&#1610;&#1606; &#1601;&#1610;
&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1608;&#1575;&#1604;&#1584;&#1610; &#1578;&#1576;&#1583;&#1571; &#1593;&#1606;&#1583;&#1607;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1606;&#1601;&#1602;&#1575;&#1578;
&#1578;&#1608;&#1601;&#1610;&#1585; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577; &#1575;&#1604;&#1578;&#1610;
&#1578;&#1588;&#1605;&#1604;&#1607;&#1575; &#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
&#1590;&#1605;&#1606; &#1575;&#1604;&#1581;&#1583;&#1608;&#1583;
&#1575;&#1604;&#1605;&#1576;&#1610;&#1606;&#1577; &#1601;&#1610;
&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577;
&#1575;&#1604;&#1571;&#1587;&#1575;&#1587;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1578;&#1575;&#1581;&#1577;
&#1604;&#1604;&#1605;&#1587;&#1578;&#1601;&#1610;&#1583; &#1608;&#1575;&#1604;&#1605;&#1581;&#1583;&#1583;&#1577;
&#1601;&#1610; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><span dir=LTR></span><span
lang=AR-SA dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><span dir=LTR></span>
</span><span lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1581;&#1583;&#1608;&#1583;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1581;&#1583;
&#1575;&#1604;&#1571;&#1602;&#1589;&#1609;
&#1604;&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1577; &#1576;&#1608;&#1576;&#1575;
&#1603;&#1605;&#1575; &#1607;&#1608; &#1605;&#1581;&#1583;&#1583; &#1601;&#1610;
&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1576;&#1575;&#1604;&#1606;&#1587;&#1576;&#1577;
&#1604;&#1571;&#1610; &#1588;&#1582;&#1589; &#1605;&#1572;&#1605;&#1606; &#1604;&#1607;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1608;&#1591;&#1606;
&#1575;&#1604;&#1571;&#1589;&#1604;&#1610;</span></b><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1603;&#1575;&#1606;
&#1575;&#1604;&#1584;&#1610; &#1610;&#1608;&#1580;&#1583; &#1576;&#1607;
&#1575;&#1604;&#1605;&#1602;&#1585; &#1575;&#1604;&#1583;&#1575;&#1574;&#1605;
&#1608;&#1575;&#1604;&#1585;&#1574;&#1610;&#1587; &#1604;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607;.</span><span lang=EN-GB dir=LTR style='font-size:11.0pt;
font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-theme-font:minor-bidi;
color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1571;&#1591;&#1585;&#1575;&#1601;
&#1575;&#1604;&#1593;&#1604;&#1575;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1581;&#1605;&#1604;&#1577; &#1575;&#1604;&#1608;&#1579;&#1575;&#1574;&#1602;
&#1608;&#1588;&#1585;&#1603;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1589;&#1581;&#1610; &#1608;&#1588;&#1585;&#1603;&#1575;&#1578;
&#1573;&#1583;&#1575;&#1585;&#1577;
&#1605;&#1591;&#1575;&#1604;&#1576;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1589;&#1581;&#1610; (</span><span dir=LTR style='font-size:
11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-theme-font:
minor-bidi;color:windowtext;mso-ansi-language:EN-US'>TPA</span><span dir=RTL></span><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><span dir=RTL></span>)
&#1608;&#1605;&#1602;&#1583;&#1605;&#1608;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577;.</span><span lang=EN-GB dir=LTR
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1588;&#1585;&#1603;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1588;&#1585;&#1603;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610; (&#1576;&#1608;&#1576;&#1575;)
&#1575;&#1604;&#1605;&#1589;&#1585;&#1581; &#1604;&#1607;&#1575;
&#1576;&#1575;&#1604;&#1593;&#1605;&#1604; &#1601;&#1610;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577; &#1605;&#1606;
&#1602;&#1576;&#1604; &#1605;&#1572;&#1587;&#1587;&#1577;
&#1575;&#1604;&#1606;&#1602;&#1583; &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;
&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;
&#1608;&#1575;&#1604;&#1578;&#1610; &#1578;&#1605;
&#1578;&#1571;&#1607;&#1610;&#1604;&#1607;&#1575;
&#1604;&#1605;&#1605;&#1575;&#1585;&#1587;&#1577;
&#1571;&#1593;&#1605;&#1575;&#1604; &#1575;&#1604;&#1590;&#1605;&#1575;&#1606;
&#1575;&#1604;&#1589;&#1581;&#1610;
&#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610; &#1605;&#1606;
&#1602;&#1576;&#1604; &#1605;&#1580;&#1604;&#1587;
&#1575;&#1604;&#1590;&#1605;&#1575;&#1606; &#1575;&#1604;&#1589;&#1581;&#1610;
&#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1588;&#1582;&#1589;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1593;&#1610; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1593;&#1606;&#1608;&#1610; &#1575;&#1604;&#1584;&#1610; &#1589;&#1583;&#1585;&#1578;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1576;&#1575;&#1587;&#1605;&#1607;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:red'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; </span></b><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1588;&#1582;&#1589;
&#1575;&#1604;&#1605;&#1578;&#1602;&#1583;&#1605;
&#1604;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1578;&#1571;&#1588;&#1610;&#1585;&#1577; &#1583;&#1582;&#1608;&#1604;
&#1573;&#1604;&#1609; &#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;
&#1576;&#1594;&#1585;&#1590; &#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577; –
&#1571;&#1608; &#1578;&#1605;&#1583;&#1610;&#1583;&#1607;&#1575; –
&#1571;&#1608; &#1576;&#1594;&#1585;&#1590;
&#1575;&#1604;&#1605;&#1585;&#1608;&#1585;&#1548; &#1575;&#1604;&#1584;&#1610;
&#1578;&#1602;&#1585;&#1585;&#1578;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1604;&#1589;&#1575;&#1604;&#1581;&#1607; &#1576;&#1605;&#1608;&#1580;&#1576;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1605;&#1602;&#1583;&#1605;&#1610;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577;</span></b><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1585;&#1601;&#1602;
&#1575;&#1604;&#1589;&#1581;&#1610;
(&#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;/&#1594;&#1610;&#1585;
&#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;)
&#1575;&#1604;&#1605;&#1589;&#1585;&#1581; &#1604;&#1607;
&#1576;&#1578;&#1602;&#1583;&#1610;&#1605;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577; &#1601;&#1610;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1608;&#1601;&#1602;&#1575;&#1611; &#1604;&#1604;&#1571;&#1606;&#1592;&#1605;&#1577;
&#1608;&#1575;&#1604;&#1602;&#1608;&#1575;&#1593;&#1583; &#1584;&#1575;&#1578;
&#1575;&#1604;&#1593;&#1604;&#1575;&#1602;&#1577;
&#1608;&#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583; &#1605;&#1606; &#1575;&#1604;&#1605;&#1580;&#1604;&#1587;&#1548;
&#1608;&#1593;&#1604;&#1609; &#1587;&#1576;&#1610;&#1604;
&#1575;&#1604;&#1605;&#1579;&#1575;&#1604;:
&#1605;&#1587;&#1578;&#1588;&#1601;&#1609;&#1548; &#1605;&#1585;&#1603;&#1586;
&#1578;&#1588;&#1582;&#1610;&#1589;&#1548;
&#1593;&#1610;&#1575;&#1583;&#1577;&#1548; &#1589;&#1610;&#1583;&#1604;&#1610;&#1577;&#1548;
&#1605;&#1582;&#1578;&#1576;&#1585;&#1548; &#1605;&#1585;&#1603;&#1586;
&#1593;&#1604;&#1575;&#1580; &#1591;&#1576;&#1610;&#1593;&#1610; &#1571;&#1608;
&#1605;&#1585;&#1603;&#1586; &#1593;&#1604;&#1575;&#1580;
&#1576;&#1575;&#1604;&#1573;&#1588;&#1593;&#1575;&#1593;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>15.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1588;&#1576;&#1603;&#1577;
&#1605;&#1602;&#1583;&#1605;&#1610; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;
&#1605;&#1602;&#1583;&#1605;&#1610; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1575;&#1604;&#1589;&#1581;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583;&#1610;&#1606; &#1605;&#1606; &#1605;&#1580;&#1604;&#1587;
&#1575;&#1604;&#1590;&#1605;&#1575;&#1606; &#1575;&#1604;&#1589;&#1581;&#1610; &#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610;
&#1608;&#1575;&#1604;&#1605;&#1581;&#1583;&#1583;&#1610;&#1606; &#1605;&#1606;
&#1602;&#1576;&#1604; &#1576;&#1608;&#1576;&#1575; &#1604;&#1578;&#1602;&#1583;&#1610;&#1605;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1604;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1608;&#1610;&#1578;&#1605; &#1584;&#1604;&#1603;
&#1576;&#1575;&#1604;&#1602;&#1610;&#1583;
&#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1593;&#1604;&#1609; &#1581;&#1587;&#1575;&#1576;
&#1576;&#1608;&#1576;&#1575; &#1593;&#1606;&#1583;
&#1573;&#1576;&#1585;&#1575;&#1586; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1576;&#1591;&#1575;&#1602;&#1577;/&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
&#1578;&#1571;&#1605;&#1610;&#1606; &#1587;&#1575;&#1585;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1601;&#1593;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1571;&#1606; &#1578;&#1578;&#1590;&#1605;&#1606; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1588;&#1576;&#1603;&#1577;
&#1605;&#1587;&#1578;&#1608;&#1610;&#1575;&#1578; &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577;:</span><span lang=EN-GB
dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-right:.75in;text-align:right;text-indent:
-.25in;mso-list:l32 level1 lfo26;direction:rtl;unicode-bidi:embed'><![if !supportLists]><span
lang=EN-GB style='font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol;color:#0079C8;mso-themecolor:text2'><span
style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1587;&#1578;&#1608;&#1609;
&#1575;&#1604;&#1571;&#1608;&#1604;
(&#1575;&#1604;&#1585;&#1593;&#1575;&#1610;&#1577;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577;
&#1575;&#1604;&#1571;&#1608;&#1604;&#1610;&#1577;)</span><span lang=EN-GB
dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-right:.75in;text-align:right;text-indent:
-.25in;mso-list:l32 level1 lfo26;direction:rtl;unicode-bidi:embed'><![if !supportLists]><span
lang=EN-GB style='font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol;color:#0079C8;mso-themecolor:text2'><span
style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1587;&#1578;&#1608;&#1609;
&#1575;&#1604;&#1579;&#1575;&#1606;&#1610;
(&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1610;&#1575;&#1578;
&#1575;&#1604;&#1593;&#1575;&#1605;&#1577;)</span><span lang=EN-GB dir=LTR
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-right:.75in;text-align:right;text-indent:
-.25in;mso-list:l32 level1 lfo26;direction:rtl;unicode-bidi:embed'><![if !supportLists]><span
lang=EN-GB style='font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol;color:#0079C8;mso-themecolor:text2'><span
style='mso-list:Ignore'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1587;&#1578;&#1608;&#1609;
&#1575;&#1604;&#1579;&#1575;&#1604;&#1579;
(&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1610;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1582;&#1589;&#1589;&#1610;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1585;&#1580;&#1593;&#1610;&#1577;)</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.75in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>16.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1591;&#1576;&#1610;&#1576;
&#1575;&#1604;&#1605;&#1585;&#1582;&#1589;</span></b><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1605;&#1586;&#1575;&#1608;&#1604;
&#1605;&#1607;&#1606;&#1577; &#1575;&#1604;&#1591;&#1576; &#1576;&#1593;&#1583;
&#1581;&#1589;&#1608;&#1604;&#1607; &#1593;&#1604;&#1609;
&#1575;&#1604;&#1605;&#1572;&#1607;&#1604;
&#1575;&#1604;&#1593;&#1604;&#1605;&#1610;
&#1575;&#1604;&#1605;&#1606;&#1575;&#1587;&#1576;
&#1608;&#1575;&#1604;&#1605;&#1585;&#1582;&#1589; &#1604;&#1607;
&#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1575;&#1611;
&#1576;&#1605;&#1586;&#1575;&#1608;&#1604;&#1577; &#1605;&#1607;&#1606;&#1577;
&#1575;&#1604;&#1591;&#1576; &#1605;&#1606; &#1602;&#1576;&#1604;
&#1575;&#1604;&#1607;&#1610;&#1574;&#1577;
&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;
&#1604;&#1604;&#1578;&#1582;&#1589;&#1589;&#1575;&#1578;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577; &#1608;&#1610;&#1603;&#1608;&#1606;
&#1605;&#1572;&#1607;&#1604;&#1575;&#1611;&#1548;
&#1608;&#1605;&#1602;&#1576;&#1608;&#1604;&#1575;&#1611; &#1605;&#1606; &#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1608;&#1576;&#1608;&#1576;&#1575;
&#1604;&#1578;&#1602;&#1583;&#1610;&#1605;
&#1575;&#1604;&#1605;&#1593;&#1575;&#1604;&#1580;&#1577;
&#1575;&#1604;&#1578;&#1610; &#1610;&#1605;&#1603;&#1606;
&#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577;
&#1576;&#1575;&#1604;&#1578;&#1593;&#1608;&#1610;&#1590; &#1593;&#1606;
&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;&#1607;&#1575;
&#1575;&#1604;&#1605;&#1575;&#1583;&#1610;&#1577;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><span lang=EN-GB
dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>17.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1593;&#1604;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1585;&#1590;
&#1571;&#1608; &#1575;&#1604;&#1583;&#1575;&#1569; &#1575;&#1604;&#1584;&#1610;
&#1610;&#1589;&#1610;&#1576; &#1575;&#1604;&#1588;&#1582;&#1589;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;.
&#1608;&#1610;&#1578;&#1591;&#1604;&#1576; &#1576;&#1575;&#1604;&#1590;&#1585;&#1608;&#1585;&#1577;
&#1575;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1605;&#1593;&#1575;&#1604;&#1580;&#1577; &#1591;&#1576;&#1610;&#1577;
&#1605;&#1606; &#1591;&#1576;&#1610;&#1576; &#1605;&#1585;&#1582;&#1589; &#1602;&#1576;&#1604;
&#1608;&#1582;&#1604;&#1575;&#1604; &#1605;&#1583;&#1577; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1605;&#1594;&#1591;&#1575;&#1577;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><span lang=EN-GB
dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>18.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1581;&#1575;&#1583;&#1579;</span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-ansi-font-weight:bold'> </span><b
style='mso-ansi-font-weight:normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1587;&#1610;&#1585;</span></b><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1607;&#1608;
&#1575;&#1589;&#1591;&#1583;&#1575;&#1605; &#1594;&#1610;&#1585;
&#1605;&#1602;&#1589;&#1608;&#1583; &#1608;&#1594;&#1610;&#1585;
&#1605;&#1585;&#1594;&#1608;&#1576; &#1601;&#1610;&#1607;
&#1604;&#1593;&#1585;&#1576;&#1577; &#1571;&#1608; &#1605;&#1585;&#1603;&#1576;&#1577;
&#1605;&#1610;&#1603;&#1575;&#1606;&#1610;&#1603;&#1610;&#1577; &#1571;&#1608;
&#1603;&#1607;&#1585;&#1576;&#1575;&#1574;&#1610;&#1577;
&#1587;&#1608;&#1575;&#1569; &#1603;&#1575;&#1606;&#1578; &#1587;&#1610;&#1575;&#1585;&#1577;
&#1571;&#1608; &#1581;&#1575;&#1601;&#1604;&#1577;&#1548;
&#1582;&#1575;&#1589;&#1577; &#1571;&#1608; &#1593;&#1575;&#1605;&#1577;&#1548;
&#1605;&#1593; &#1593;&#1585;&#1576;&#1577; &#1571;&#1608;
&#1605;&#1585;&#1603;&#1576;&#1577; &#1571;&#1582;&#1585;&#1609;&#1548;
&#1601;&#1610; &#1581;&#1575;&#1604;&#1577; &#1608;&#1602;&#1608;&#1601;
&#1571;&#1608; &#1581;&#1585;&#1603;&#1577;&#1548; &#1571;&#1608;
&#1605;&#1593; &#1580;&#1587;&#1605; &#1579;&#1575;&#1576;&#1578;
&#1603;&#1575;&#1604;&#1576;&#1606;&#1575;&#1569; &#1571;&#1608;
&#1575;&#1604;&#1581;&#1575;&#1580;&#1586; &#1571;&#1608; &#1575;&#1604;&#1593;&#1605;&#1608;&#1583;
&#1571;&#1608; &#1575;&#1604;&#1588;&#1580;&#1585;&#1577; &#1571;&#1608;
&#1605;&#1575; &#1588;&#1575;&#1576;&#1607;&#1548; &#1571;&#1608;
&#1605;&#1593; &#1588;&#1582;&#1589; &#1605;&#1606; &#1575;&#1604;&#1605;&#1588;&#1575;&#1577;&#1548;
&#1601;&#1610; &#1571;&#1610; &#1605;&#1606;
&#1575;&#1604;&#1591;&#1585;&#1602; &#1571;&#1608;
&#1575;&#1604;&#1588;&#1608;&#1575;&#1585;&#1593;&#1548; &#1605;&#1605;&#1575;
&#1602;&#1583; &#1610;&#1572;&#1583;&#1610; &#1573;&#1604;&#1609;
&#1573;&#1589;&#1575;&#1576;&#1575;&#1578; &#1580;&#1587;&#1583;&#1610;&#1577;
&#1582;&#1601;&#1610;&#1601;&#1577; &#1575;&#1604;&#1588;&#1583;&#1577;
&#1573;&#1604;&#1609; &#1588;&#1583;&#1610;&#1583;&#1577; &#1575;&#1604;&#1582;&#1591;&#1608;&#1585;&#1577;&#1548;
&#1608;&#1602;&#1583; &#1578;&#1589;&#1604; &#1604;&#1581;&#1583;
&#1575;&#1604;&#1573;&#1593;&#1575;&#1602;&#1577;
&#1575;&#1604;&#1580;&#1587;&#1583;&#1610;&#1577; &#1571;&#1608; &#1573;&#1604;&#1609;
&#1575;&#1604;&#1608;&#1601;&#1575;&#1577; &#1571;&#1608;
&#1582;&#1587;&#1575;&#1574;&#1585; &#1605;&#1575;&#1583;&#1610;&#1577;
&#1580;&#1586;&#1574;&#1610;&#1577; &#1571;&#1608; &#1603;&#1604;&#1610;&#1577;
&#1601;&#1610; &#1575;&#1604;&#1605;&#1605;&#1578;&#1604;&#1603;&#1575;&#1578;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>19.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1608;&#1587;&#1575;&#1574;&#1591;
&#1575;&#1604;&#1582;&#1575;&#1585;&#1580;&#1610;&#1577;
&#1575;&#1604;&#1593;&#1606;&#1610;&#1601;&#1577; </span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1571;&#1610;
&#1608;&#1587;&#1575;&#1574;&#1591; &#1610;&#1606;&#1578;&#1580;
&#1593;&#1606;&#1607;&#1575; &#1581;&#1575;&#1583;&#1579; &#1571;&#1608;
&#1573;&#1589;&#1575;&#1576;&#1577; &#1604;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607;.</span><span lang=EN-GB dir=LTR style='font-size:11.0pt;
font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-theme-font:minor-bidi;
color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>20.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1571;&#1582;&#1591;&#1575;&#1585;
&#1575;&#1604;&#1588;&#1582;&#1589;&#1610;&#1577;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1571;&#1610; &#1601;&#1593;&#1604;
&#1610;&#1602;&#1608;&#1605; &#1576;&#1607; &#1575;&#1604;&#1588;&#1582;&#1589;
&#1571;&#1608; &#1571;&#1610; &#1605;&#1605;&#1575;&#1585;&#1587;&#1577;
&#1610;&#1602;&#1608;&#1605; &#1576;&#1607;&#1575;
&#1575;&#1604;&#1588;&#1582;&#1589; &#1605;&#1578;&#1593;&#1575;&#1585;&#1601; &#1593;&#1604;&#1609;
&#1571;&#1606;&#1607;&#1575; &#1605;&#1606;&#1591;&#1608;&#1610;&#1577; &#1593;&#1604;&#1609;
&#1605;&#1582;&#1575;&#1591;&#1585; &#1575;&#1604;&#1578;&#1593;&#1585;&#1590; &#1604;&#1604;&#1573;&#1589;&#1575;&#1576;&#1577;
&#1576;&#1605;&#1585;&#1590; &#1571;&#1608; &#1581;&#1575;&#1583;&#1579;&#1548;
&#1571;&#1608; &#1605;&#1578;&#1608;&#1602;&#1593; &#1571;&#1606;
&#1578;&#1581;&#1583;&#1579; &#1605;&#1590;&#1575;&#1593;&#1601;&#1577;
&#1604;&#1605;&#1585;&#1590; &#1571;&#1608; &#1573;&#1589;&#1575;&#1576;&#1577;
&#1587;&#1575;&#1576;&#1602;&#1577;.</span><span lang=EN-GB dir=LTR
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>21.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1581;&#1575;&#1604;&#1577;
&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1593;&#1604;&#1575;&#1580;
&#1575;&#1604;&#1591;&#1576;&#1610; &#1575;&#1604;&#1591;&#1575;&#1585;&#1574;
&#1575;&#1604;&#1584;&#1610; &#1578;&#1602;&#1578;&#1590;&#1610;&#1607;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1577; &#1575;&#1604;&#1591;&#1576;&#1610;&#1577;
&#1604;&#1604;&#1605;&#1587;&#1578;&#1601;&#1610;&#1583; &#1573;&#1579;&#1585;
&#1608;&#1602;&#1608;&#1593; &#1581;&#1583;&#1579;&#1548; &#1571;&#1608;
&#1593;&#1575;&#1585;&#1590; &#1571;&#1608; &#1581;&#1575;&#1604;&#1577; &#1589;&#1581;&#1610;&#1577;
&#1591;&#1575;&#1585;&#1574;&#1577; &#1578;&#1587;&#1578;&#1583;&#1593;&#1610;
&#1575;&#1604;&#1578;&#1583;&#1582;&#1604; &#1575;&#1604;&#1591;&#1576;&#1610;
&#1575;&#1604;&#1587;&#1585;&#1610;&#1593;.</span><span lang=EN-GB dir=LTR
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>22.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1578;&#1606;&#1608;&#1610;&#1605;
&#1601;&#1610; &#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609;</span></b><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1578;&#1587;&#1580;&#1610;&#1604;
&#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1603;&#1605;&#1585;&#1610;&#1590; &#1605;&#1606;&#1608;&#1605;
&#1601;&#1610; &#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609;
&#1581;&#1578;&#1609; &#1589;&#1576;&#1575;&#1581;
&#1575;&#1604;&#1610;&#1608;&#1605; &#1575;&#1604;&#1578;&#1575;&#1604;&#1610;
&#1593;&#1604;&#1609; &#1575;&#1604;&#1571;&#1602;&#1604;
&#1576;&#1606;&#1575;&#1569;&#1611; &#1593;&#1604;&#1609;
&#1578;&#1581;&#1608;&#1610;&#1604; &#1605;&#1606; &#1591;&#1576;&#1610;&#1576;
&#1605;&#1585;&#1582;&#1589;.</span><span lang=EN-GB dir=LTR style='font-size:
11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-theme-font:
minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>23.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1581;&#1587;&#1575;&#1587;&#1610;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1578;&#1581;&#1587;&#1587;
&#1575;&#1604;&#1601;&#1585;&#1583;&#1548; &#1576;&#1589;&#1601;&#1577; &#1582;&#1575;&#1589;&#1577;&#1548;
&#1604;&#1571;&#1606;&#1608;&#1575;&#1593; &#1605;&#1593;&#1610;&#1606;&#1577; &#1605;&#1606;
&#1575;&#1604;&#1594;&#1584;&#1575;&#1569;&#1548;
&#1575;&#1604;&#1583;&#1608;&#1575;&#1569;&#1548; &#1575;&#1604;&#1591;&#1602;&#1587;&#1548;
&#1594;&#1576;&#1575;&#1585; &#1575;&#1604;&#1591;&#1604;&#1593; &#1571;&#1608;
&#1571;&#1610;&#1577; &#1605;&#1581;&#1583;&#1579;&#1575;&#1578; &#1571;&#1582;&#1585;&#1609;
&#1605;&#1606; &#1575;&#1604;&#1606;&#1576;&#1575;&#1578;&#1575;&#1578;&#1548; &#1575;&#1604;&#1581;&#1588;&#1585;&#1575;&#1578;&#1548;
&#1575;&#1604;&#1581;&#1610;&#1608;&#1575;&#1606;&#1575;&#1578;&#1548; &#1575;&#1604;&#1605;&#1593;&#1575;&#1583;&#1606;&#1548;
&#1575;&#1604;&#1593;&#1606;&#1575;&#1589;&#1585; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1608;&#1575;&#1583;
&#1575;&#1604;&#1571;&#1582;&#1585;&#1609;&#1548; &#1581;&#1610;&#1579;
&#1610;&#1593;&#1575;&#1606;&#1610; &#1575;&#1604;&#1601;&#1585;&#1583;
&#1585;&#1583;&#1608;&#1583; &#1601;&#1593;&#1604;
&#1576;&#1583;&#1606;&#1610;&#1577; &#1610;&#1587;&#1576;&#1576;&#1607;&#1575;
&#1575;&#1604;&#1575;&#1578;&#1589;&#1575;&#1604;
&#1575;&#1604;&#1605;&#1576;&#1575;&#1588;&#1585; &#1571;&#1608; &#1594;&#1610;&#1585;
&#1575;&#1604;&#1605;&#1576;&#1575;&#1588;&#1585; &#1576;&#1578;&#1604;&#1603;
&#1575;&#1604;&#1605;&#1608;&#1575;&#1583; &#1605;&#1605;&#1575;
&#1610;&#1578;&#1587;&#1576;&#1576; &#1601;&#1610;
&#1581;&#1575;&#1604;&#1575;&#1578; &#1605;&#1579;&#1604; &#1575;&#1604;&#1585;&#1576;&#1608;&#1548;
&#1587;&#1608;&#1569; &#1575;&#1604;&#1607;&#1590;&#1605;&#1548; &#1575;&#1604;&#1581;&#1603;&#1575;&#1603;&#1548;
&#1575;&#1604;&#1581;&#1605;&#1609; &#1575;&#1604;&#1602;&#1588;&#1610;&#1577;&#1548;
&#1575;&#1604;&#1571;&#1603;&#1586;&#1610;&#1605;&#1575;&#1548; &#1575;&#1604;&#1589;&#1583;&#1575;&#1593;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>24.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1578;&#1588;&#1608;&#1607; &#1575;&#1604;&#1582;&#1604;&#1602;&#1610;</span></b><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1582;&#1604;&#1604;
&#1575;&#1604;&#1608;&#1592;&#1610;&#1601;&#1610; &#1571;&#1608;
&#1575;&#1604;&#1603;&#1610;&#1605;&#1610;&#1575;&#1574;&#1610; &#1571;&#1608;
&#1575;&#1604;&#1576;&#1606;&#1575;&#1574;&#1610; &#1575;&#1604;&#1584;&#1610;
&#1610;&#1603;&#1608;&#1606; &#1605;&#1608;&#1580;&#1608;&#1583;&#1575;&#1611;
&#1593;&#1575;&#1583;&#1577; &#1602;&#1576;&#1604;
&#1575;&#1604;&#1608;&#1604;&#1575;&#1583;&#1577; &#1587;&#1608;&#1575;&#1569;
&#1576;&#1575;&#1604;&#1608;&#1585;&#1575;&#1579;&#1577; &#1571;&#1608;
&#1606;&#1578;&#1610;&#1580;&#1577;
&#1604;&#1604;&#1593;&#1608;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1576;&#1610;&#1574;&#1610;&#1577; &#1581;&#1587;&#1576;
&#1575;&#1604;&#1593;&#1585;&#1601; &#1575;&#1604;&#1591;&#1576;&#1610;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>25.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1581;&#1605;&#1604; &#1608;&#1575;&#1604;&#1608;&#1604;&#1575;&#1583;&#1577;<o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1571;&#1610;
&#1581;&#1575;&#1604;&#1577; &#1581;&#1605;&#1604; &#1608;/ &#1571;&#1608;
&#1608;&#1604;&#1575;&#1583;&#1577; &#1608;&#1610;&#1588;&#1605;&#1604;
&#1584;&#1604;&#1603; &#1575;&#1604;&#1608;&#1604;&#1575;&#1583;&#1577;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1593;&#1610;&#1577;
&#1608;&#1575;&#1604;&#1602;&#1610;&#1589;&#1585;&#1610;&#1577;
&#1608;&#1575;&#1604;&#1573;&#1580;&#1607;&#1575;&#1590; (&#1605;&#1593;
&#1605;&#1585;&#1575;&#1593;&#1575;&#1577;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1590;&#1605;&#1606;
&#1575;&#1604;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569;&#1575;&#1578;
&#1575;&#1604;&#1594;&#1610;&#1585; &#1605;&#1594;&#1591;&#1575;&#1577;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;).</span><span lang=EN-GB
dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>26.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1573;&#1582;&#1604;&#1575;&#1569;
&#1575;&#1604;&#1591;&#1576;&#1610; &#1575;&#1604;&#1591;&#1575;&#1585;&#1574;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1606;&#1602;&#1604;
&#1575;&#1604;&#1605;&#1585;&#1610;&#1590; &#1573;&#1604;&#1609;
&#1571;&#1602;&#1585;&#1576; &#1605;&#1585;&#1603;&#1586; &#1591;&#1576;&#1610;
&#1583;&#1575;&#1582;&#1604; &#1608;&#1582;&#1575;&#1585;&#1580;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1578;&#1578;&#1608;&#1601;&#1585; &#1601;&#1610;&#1607;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577; &#1575;&#1604;&#1578;&#1610;
&#1610;&#1581;&#1578;&#1575;&#1580;&#1607;&#1575; &#1575;&#1604;&#1605;&#1585;&#1610;&#1590;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>27.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578; &#1575;&#1604;&#1606;&#1601;&#1587;&#1610;&#1577;
<o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1590;&#1591;&#1585;&#1575;&#1576;
&#1601;&#1610; &#1575;&#1604;&#1578;&#1601;&#1603;&#1610;&#1585; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1586;&#1575;&#1580; &#1571;&#1608;
&#1575;&#1604;&#1573;&#1583;&#1585;&#1575;&#1603; &#1571;&#1608;
&#1575;&#1604;&#1584;&#1575;&#1603;&#1585;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1602;&#1583;&#1585;&#1575;&#1578;
&#1575;&#1604;&#1593;&#1602;&#1604;&#1610;&#1577;
&#1576;&#1593;&#1590;&#1607;&#1575; &#1571;&#1608; &#1603;&#1604;&#1607;&#1575;.</span><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-size:11.0pt;font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-theme-font:minor-bidi;
color:windowtext'><span dir=LTR></span> </span><span lang=AR-SA
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>28.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1573;&#1593;&#1575;&#1602;&#1577;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1605;&#1589;&#1591;&#1604;&#1581;&#1575;&#1611;
&#1580;&#1575;&#1605;&#1593;&#1575;&#1611; &#1610;&#1590;&#1605;
&#1578;&#1581;&#1578; &#1605;&#1592;&#1604;&#1578;&#1607;
&#1575;&#1604;&#1571;&#1588;&#1603;&#1575;&#1604; &#1575;&#1604;&#1605;&#1582;&#1578;&#1604;&#1601;&#1577;
&#1604;&#1604;&#1575;&#1593;&#1578;&#1604;&#1575;&#1604;/&#1575;&#1604;&#1582;&#1604;&#1604;
&#1575;&#1604;&#1593;&#1590;&#1608;&#1610;&#1548;
&#1608;&#1605;&#1581;&#1583;&#1608;&#1583;&#1610;&#1577;
&#1575;&#1604;&#1606;&#1588;&#1575;&#1591;&#1548; &#1608;&#1575;&#1604;&#1602;&#1610;&#1608;&#1583;
&#1575;&#1604;&#1578;&#1610; &#1578;&#1581;&#1583; &#1605;&#1606;
&#1575;&#1604;&#1605;&#1588;&#1575;&#1585;&#1603;&#1577;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>29.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1573;&#1593;&#1575;&#1583;&#1577; &#1575;&#1604;&#1578;&#1571;&#1607;&#1610;&#1604;/
&#1575;&#1604;&#1593;&#1604;&#1575;&#1580; &#1575;&#1604;&#1591;&#1576;&#1610;&#1593;&#1610;</span></b><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1580;&#1586;&#1569;
&#1605;&#1603;&#1605;&#1604; &#1604;&#1604;&#1585;&#1593;&#1575;&#1610;&#1577;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577;
&#1575;&#1604;&#1588;&#1575;&#1605;&#1604;&#1577;
&#1608;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1578;&#1607;&#1575; &#1605;&#1606;
&#1571;&#1580;&#1604; &#1573;&#1593;&#1575;&#1583;&#1577;
&#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1584;&#1610;
&#1610;&#1593;&#1575;&#1606;&#1610; &#1605;&#1606; &#1590;&#1593;&#1601;
&#1605;&#1587;&#1578;&#1605;&#1585; &#1573;&#1604;&#1609;
&#1571;&#1601;&#1590;&#1604; &#1605;&#1587;&#1578;&#1608;&#1609; &#1605;&#1606;
&#1575;&#1604;&#1571;&#1583;&#1575;&#1569; &#1601;&#1610; &#1581;&#1610;&#1575;&#1578;&#1607;
&#1575;&#1604;&#1571;&#1587;&#1585;&#1610;&#1577; &#1608;&#1575;&#1604;&#1575;&#1580;&#1578;&#1605;&#1575;&#1593;&#1610;&#1577;
&#1608;&#1575;&#1604;&#1584;&#1610; &#1576;&#1583;&#1608;&#1585;&#1607; &#1587;&#1610;&#1586;&#1610;&#1583;
&#1605;&#1606; &#1601;&#1575;&#1593;&#1604;&#1610;&#1577; &#1606;&#1592;&#1575;&#1605;
&#1575;&#1604;&#1585;&#1593;&#1575;&#1610;&#1577; &#1575;&#1604;&#1589;&#1581;&#1610;&#1577;
&#1575;&#1604;&#1584;&#1610; &#1610;&#1605;&#1603;&#1606; &#1571;&#1606; &#1610;&#1602;&#1575;&#1587;
&#1576;&#1575;&#1604;&#1578;&#1603;&#1604;&#1601;&#1577; &#1608;&#1578;&#1581;&#1604;&#1610;&#1604;
&#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>30.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1602;&#1587;&#1591; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;
(&#1575;&#1604;&#1575;&#1588;&#1578;&#1585;&#1575;&#1603;)</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1576;&#1604;&#1594;
&#1575;&#1604;&#1608;&#1575;&#1580;&#1576; &#1575;&#1604;&#1583;&#1601;&#1593; &#1605;&#1606;
&#1602;&#1576;&#1604; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1604;&#1576;&#1608;&#1576;&#1575; &#1605;&#1602;&#1575;&#1576;&#1604;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1610; &#1578;&#1608;&#1601;&#1585;&#1607;&#1575;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1582;&#1604;&#1575;&#1604;
&#1605;&#1583;&#1577; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;. <o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>31.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1571;&#1587;&#1575;&#1587;
&#1575;&#1604;&#1578;&#1602;&#1610;&#1610;&#1583;
&#1575;&#1604;&#1605;&#1576;&#1575;&#1588;&#1585; &#1571;&#1608;
&#1593;&#1604;&#1609; &#1581;&#1587;&#1575;&#1576;
&#1575;&#1604;&#1588;&#1585;&#1603;&#1577;</span></b><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1578;&#1587;&#1607;&#1610;&#1604;&#1575;&#1578;
&#1593;&#1583;&#1605; &#1575;&#1604;&#1583;&#1601;&#1593;
&#1575;&#1604;&#1605;&#1608;&#1601;&#1585;&#1577;
&#1604;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1604;&#1583;&#1609;
&#1605;&#1602;&#1583;&#1605; &#1571;&#1608; &#1605;&#1602;&#1583;&#1605;&#1610;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1605;&#1593;&#1610;&#1606;&#1610;&#1606; &#1605;&#1606;
&#1602;&#1576;&#1604; &#1576;&#1608;&#1576;&#1575; &#1581;&#1610;&#1579;
&#1610;&#1578;&#1605; &#1608;&#1601;&#1602;&#1575;&#1611;
&#1604;&#1584;&#1604;&#1603; &#1602;&#1610;&#1583; &#1580;&#1605;&#1610;&#1593;
&#1578;&#1604;&#1603; &#1575;&#1604;&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1593;&#1604;&#1609;
&#1581;&#1587;&#1575;&#1576; &#1576;&#1608;&#1576;&#1575;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>32.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1571;&#1587;&#1575;&#1587; &#1578;&#1593;&#1608;&#1610;&#1590;
&#1575;&#1604;&#1576;&#1583;&#1604; <o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1571;&#1587;&#1604;&#1608;&#1576;
&#1575;&#1604;&#1605;&#1578;&#1576;&#1593; &#1604;&#1578;&#1593;&#1608;&#1610;&#1590;
&#1581;&#1575;&#1605;&#1604; &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1593;&#1606; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578; &#1575;&#1604;&#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577; &#1575;&#1604;&#1578;&#1610;
&#1610;&#1578;&#1581;&#1605;&#1604;&#1607;&#1575; &#1575;&#1604;&#1588;&#1582;&#1589;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1608;&#1610;&#1602;&#1583;&#1605;
&#1593;&#1606;&#1607;&#1575; &#1605;&#1591;&#1575;&#1604;&#1576;&#1577;. </span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>33.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1575;&#1604;&#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1575;&#1604;&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1575;&#1604;&#1601;&#1593;&#1604;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1606;&#1601;&#1602;&#1577;
&#1605;&#1602;&#1575;&#1576;&#1604; &#1582;&#1583;&#1605;&#1575;&#1578;
&#1608;&#1605;&#1608;&#1575;&#1583; &#1608;&#1571;&#1580;&#1607;&#1586;&#1577;
&#1594;&#1610;&#1585; &#1605;&#1587;&#1578;&#1579;&#1606;&#1575;&#1577;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1575;&#1604;&#1602;&#1587;&#1605;
&#1575;&#1604;&#1579;&#1575;&#1604;&#1579; &#1605;&#1606;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1575;&#1604;&#1605;&#1585;&#1601;&#1602;&#1577; &#1576;&#1607;&#1584;&#1607;
&#1575;&#1604;&#1604;&#1575;&#1574;&#1581;&#1577;&#1548; &#1593;&#1604;&#1609;
&#1571;&#1606; &#1610;&#1589;&#1601;&#1607;&#1575; &#1591;&#1576;&#1610;&#1576;
&#1605;&#1585;&#1582;&#1589; &#1576;&#1587;&#1576;&#1576; &#1593;&#1604;&#1577;
&#1578;&#1593;&#1585;&#1590; &#1604;&#1607;&#1575;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1588;&#1585;&#1610;&#1591;&#1577;
&#1571;&#1606; &#1578;&#1603;&#1608;&#1606; &#1578;&#1604;&#1603;
&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1590;&#1585;&#1608;&#1585;&#1610;&#1577;
&#1608;&#1605;&#1593;&#1602;&#1608;&#1604;&#1577; &#1608;&#1605;&#1593;&#1578;&#1575;&#1583;&#1577;
&#1601;&#1610; &#1575;&#1604;&#1608;&#1602;&#1578;
&#1608;&#1575;&#1604;&#1605;&#1603;&#1575;&#1606; &#1575;&#1604;&#1584;&#1610;
&#1578;&#1605;&#1578; &#1601;&#1610;&#1607;.</span><span lang=EN-GB dir=LTR
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>34.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1591;&#1604;&#1576;
&#1605;&#1602;&#1583;&#1605; &#1573;&#1604;&#1609; &#1588;&#1585;&#1603;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606; &#1571;&#1608; &#1605;&#1606;
&#1610;&#1605;&#1579;&#1604;&#1607;&#1575; &#1605;&#1606; &#1605;&#1602;&#1583;&#1605;
&#1582;&#1583;&#1605;&#1577; &#1571;&#1608; &#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1571;&#1608; &#1605;&#1606; &#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;&#1548;
&#1576;&#1594;&#1585;&#1590; &#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577;
&#1602;&#1610;&#1605;&#1577; &#1606;&#1601;&#1602;&#1575;&#1578;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1588;&#1605;&#1608;&#1604;&#1577; &#1590;&#1605;&#1606;
&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;&#1548;
&#1608;&#1575;&#1604;&#1605;&#1588;&#1601;&#1608;&#1593;
&#1576;&#1575;&#1604;&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1575;&#1604;&#1610;&#1577;
&#1608;&#1575;&#1604;&#1591;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1572;&#1610;&#1583;&#1577; &#1604;&#1607;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>35.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1572;&#1610;&#1583;&#1577; &#1604;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577;</span></b><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1580;&#1605;&#1610;&#1593;
&#1575;&#1604;&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1610; &#1578;&#1579;&#1576;&#1578;
&#1608;&#1578;&#1572;&#1610;&#1583; &#1593;&#1605;&#1585;
&#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1608;&#1580;&#1606;&#1587;&#1610;&#1578;&#1607;
&#1608;&#1607;&#1608;&#1610;&#1578;&#1607;
&#1608;&#1587;&#1585;&#1610;&#1575;&#1606;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
&#1608;&#1605;&#1604;&#1575;&#1576;&#1587;&#1575;&#1578;
&#1608;&#1602;&#1608;&#1593; &#1575;&#1604;&#1581;&#1583;&#1579; &#1575;&#1604;&#1606;&#1575;&#1588;&#1574;&#1577;
&#1593;&#1606;&#1607; &#1578;&#1604;&#1603;
&#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577; &#1608;&#1605;&#1575;
&#1610;&#1579;&#1576;&#1578; &#1587;&#1583;&#1575;&#1583;
&#1575;&#1604;&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;. &#1603;&#1605;&#1575;
&#1578;&#1588;&#1605;&#1604; &#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578; &#1571;&#1582;&#1585;&#1609;
&#1605;&#1579;&#1604; &#1578;&#1602;&#1585;&#1610;&#1585;
&#1575;&#1604;&#1588;&#1585;&#1591;&#1577;&#1548; &#1608;&#1575;&#1604;&#1601;&#1608;&#1575;&#1578;&#1610;&#1585;&#1548;
&#1608;&#1575;&#1604;&#1573;&#1610;&#1589;&#1575;&#1604;&#1575;&#1578;&#1548;
&#1608;&#1575;&#1604;&#1608;&#1589;&#1601;&#1575;&#1578;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577;&#1548; &#1578;&#1602;&#1585;&#1610;&#1585;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1576;&#1548; &#1575;&#1604;&#1573;&#1581;&#1575;&#1604;&#1577;
&#1608;&#1575;&#1604;&#1578;&#1608;&#1589;&#1610;&#1575;&#1578;&#1548; &#1608;&#1571;&#1610;&#1577;
&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578; &#1571;&#1589;&#1604;&#1610;&#1577;
&#1571;&#1582;&#1585;&#1609; &#1602;&#1583;
&#1578;&#1591;&#1604;&#1576;&#1607;&#1575; &#1576;&#1608;&#1576;&#1575;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>36.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1578;&#1593;&#1608;&#1610;&#1590;
&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578; &#1601;&#1610;
&#1581;&#1575;&#1583;&#1579; &#1575;&#1604;&#1587;&#1610;&#1585;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1605;&#1591;&#1575;&#1604;&#1576;&#1577;
&#1591;&#1576;&#1610;&#1577; &#1606;&#1575;&#1580;&#1605;&#1577; &#1593;&#1606;
&#1581;&#1575;&#1583;&#1579; &#1587;&#1610;&#1585;
&#1604;&#1578;&#1594;&#1591;&#1610;&#1577; &#1588;&#1582;&#1589; &#1605;&#1589;&#1575;&#1576;
&#1606;&#1578;&#1610;&#1580;&#1577; &#1607;&#1584;&#1575;
&#1575;&#1604;&#1581;&#1575;&#1583;&#1579; &#1608;&#1587;&#1608;&#1575;&#1569;
&#1603;&#1575;&#1606; &#1607;&#1608; &#1575;&#1604;&#1589;&#1575;&#1583;&#1605;
&#1571;&#1608; &#1575;&#1604;&#1605;&#1589;&#1583;&#1608;&#1605;&#1548;
&#1608;&#1573;&#1584;&#1575; &#1603;&#1575;&#1606;&#1578;
&#1606;&#1601;&#1602;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577; &#1607;&#1584;&#1607;
&#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577;
&#1604;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1605;&#1589;&#1575;&#1576;
&#1576;&#1605;&#1593;&#1606;&#1609; &#1571;&#1606;&#1607;&#1575; &#1605;&#1594;&#1591;&#1575;&#1577;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1571;&#1610; &#1582;&#1591;&#1577;
&#1571;&#1608; &#1576;&#1585;&#1606;&#1575;&#1605;&#1580; &#1571;&#1608;
&#1578;&#1571;&#1605;&#1610;&#1606; &#1570;&#1582;&#1585; &#1571;&#1608;
&#1605;&#1575; &#1588;&#1575;&#1576;&#1607; &#1584;&#1604;&#1603;&#1548;
&#1601;&#1573;&#1606; &#1593;&#1604;&#1609; &#1588;&#1585;&#1603;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606; &#1575;&#1604;&#1578;&#1610;
&#1578;&#1605; &#1573;&#1576;&#1604;&#1575;&#1594;&#1607;&#1575;
&#1571;&#1608;&#1604;&#1575;&#1611;
&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1577;
&#1578;&#1594;&#1591;&#1610;&#1577; &#1575;&#1604;&#1588;&#1582;&#1589;
&#1575;&#1604;&#1605;&#1589;&#1575;&#1576; &#1608;&#1605;&#1578;&#1575;&#1576;&#1593;&#1577;
&#1593;&#1604;&#1575;&#1580;&#1607; &#1591;&#1576;&#1610;&#1575;&#1611;
&#1608;&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1577;
&#1578;&#1593;&#1608;&#1610;&#1590; &#1578;&#1604;&#1603;
&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;&#1548;
&#1608;&#1578;&#1581;&#1604; &#1605;&#1581;&#1604;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;&#1548; &#1571;&#1610;
&#1575;&#1604;&#1588;&#1582;&#1589;
&#1575;&#1604;&#1605;&#1589;&#1575;&#1576;&#1548; &#1601;&#1610; &#1571;&#1610;
&#1605;&#1591;&#1575;&#1604;&#1576;&#1577; &#1604;&#1604;&#1594;&#1610;&#1585;
&#1576;&#1583;&#1601;&#1593; &#1581;&#1589;&#1578;&#1607;&#1605;
&#1575;&#1604;&#1606;&#1587;&#1576;&#1610;&#1577; &#1605;&#1606;
&#1578;&#1604;&#1603; &#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>37.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577; &#1575;&#1604;&#1605;&#1593;&#1602;&#1608;&#1604;&#1577;
&#1608;&#1575;&#1604;&#1605;&#1593;&#1578;&#1575;&#1583;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:6.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l36 level1 lfo17;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1571;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1587;&#1593;&#1575;&#1585;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1575;&#1604;&#1591;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1578;&#1601;&#1602; &#1593;&#1604;&#1610;&#1607;&#1575;
&#1576;&#1610;&#1606; &#1576;&#1608;&#1576;&#1575;
&#1608;&#1605;&#1602;&#1583;&#1605; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1578;&#1610; &#1578;&#1578;&#1608;&#1575;&#1601;&#1602; &#1605;&#1593;
&#1605;&#1587;&#1578;&#1608;&#1609;
&#1575;&#1604;&#1571;&#1578;&#1593;&#1575;&#1576; &#1575;&#1604;&#1578;&#1610;
&#1610;&#1578;&#1602;&#1575;&#1590;&#1575;&#1607;&#1575;
&#1594;&#1575;&#1604;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1571;&#1591;&#1576;&#1575;&#1569; &#1575;&#1604;&#1605;&#1585;&#1582;&#1589;&#1610;&#1606;
&#1571;&#1608;
&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1610;&#1575;&#1578; &#1601;&#1610;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577; &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577; &#1601;&#1602;&#1591; &#1608;&#1575;&#1604;&#1605;&#1578;&#1593;&#1575;&#1585;&#1601;
&#1593;&#1604;&#1610;&#1607;&#1575; &#1601;&#1610;
&#1575;&#1604;&#1587;&#1608;&#1602;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:6.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l36 level1 lfo17;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1576;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1587;&#1593;&#1575;&#1585;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577; &#1575;&#1604;&#1578;&#1610;
&#1604;&#1575; &#1578;&#1582;&#1578;&#1604;&#1601; &#1580;&#1584;&#1585;&#1610;&#1575;&#1611;
&#1593;&#1605;&#1575; &#1610;&#1593;&#1578;&#1576;&#1585;&#1607; &#1575;&#1604;&#1591;&#1576;&#1610;&#1576;
&#1575;&#1604;&#1605;&#1585;&#1582;&#1589; &#1605;&#1602;&#1576;&#1608;&#1604;&#1575;&#1611;
&#1576;&#1575;&#1593;&#1578;&#1576;&#1575;&#1585;&#1607;
&#1593;&#1575;&#1583;&#1610;&#1575;&#1611;
&#1608;&#1591;&#1576;&#1610;&#1593;&#1610;&#1575;&#1611;
&#1576;&#1575;&#1604;&#1606;&#1587;&#1576;&#1577; &#1604;&#1571;&#1610;
&#1593;&#1604;&#1577; &#1605;&#1605;&#1575;&#1579;&#1604;&#1577; &#1608;&#1578;&#1578;&#1605;
&#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577;
&#1576;&#1575;&#1604;&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1578;&#1593;&#1604;&#1602;&#1577; &#1576;&#1607;&#1575;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpLast dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>38.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1573;&#1593;&#1575;&#1583;&#1577; &#1585;&#1601;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1578;&#1608;&#1601;&#1609; &#1573;&#1604;&#1609;
&#1605;&#1608;&#1591;&#1606;&#1607; &#1575;&#1604;&#1571;&#1589;&#1604;&#1610;</span></b><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1580;&#1605;&#1610;&#1593; &#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1578;&#1580;&#1607;&#1610;&#1586; &#1608;&#1573;&#1593;&#1575;&#1583;&#1577; &#1580;&#1579;&#1605;&#1575;&#1606;
&#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1573;&#1604;&#1609; &#1605;&#1608;&#1591;&#1606;&#1607;
&#1575;&#1604;&#1571;&#1589;&#1604;&#1610;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>39.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1575;&#1581;&#1578;&#1610;&#1575;&#1604;</span></b><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1602;&#1610;&#1575;&#1605;
&#1571;&#1610; &#1591;&#1585;&#1601; &#1605;&#1606;
&#1571;&#1591;&#1585;&#1575;&#1601;
&#1575;&#1604;&#1593;&#1604;&#1575;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
&#1576;&#1575;&#1604;&#1582;&#1583;&#1575;&#1593; &#1575;&#1604;&#1605;&#1578;&#1593;&#1605;&#1583;
&#1575;&#1604;&#1584;&#1610; &#1610;&#1606;&#1578;&#1580; &#1593;&#1606;&#1607;
&#1575;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1605;&#1606;&#1575;&#1601;&#1593; &#1571;&#1608;
&#1571;&#1605;&#1608;&#1575;&#1604; &#1571;&#1608; &#1578;&#1602;&#1583;&#1610;&#1605;
&#1605;&#1606;&#1575;&#1601;&#1593;
&#1605;&#1587;&#1578;&#1579;&#1606;&#1575;&#1577; &#1571;&#1608;
&#1578;&#1578;&#1580;&#1575;&#1608;&#1586;
&#1575;&#1604;&#1581;&#1583;&#1608;&#1583;
&#1575;&#1604;&#1605;&#1587;&#1605;&#1608;&#1581; &#1576;&#1607;&#1575; &#1573;&#1604;&#1609;
&#1575;&#1604;&#1601;&#1585;&#1583; &#1571;&#1608;
&#1575;&#1604;&#1580;&#1607;&#1577;.</span><span lang=EN-GB dir=LTR
style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>40.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1573;&#1587;&#1575;&#1569;&#1577; &#1575;&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;</span></b><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><o:p></o:p></span></b></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1602;&#1610;&#1575;&#1605;
&#1571;&#1610; &#1591;&#1585;&#1601; &#1605;&#1606;
&#1571;&#1591;&#1585;&#1575;&#1601;
&#1575;&#1604;&#1593;&#1604;&#1575;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577; &#1576;&#1605;&#1605;&#1575;&#1585;&#1587;&#1575;&#1578;
&#1602;&#1583; &#1578;&#1572;&#1583;&#1610; &#1573;&#1604;&#1609;
&#1575;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1605;&#1606;&#1575;&#1601;&#1593; &#1594;&#1610;&#1585;
&#1605;&#1582;&#1608;&#1604;&#1610;&#1606;
&#1604;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1610;&#1607;&#1575;
&#1608;&#1604;&#1603;&#1606; &#1576;&#1583;&#1608;&#1606; &#1602;&#1589;&#1583;
&#1575;&#1604;&#1578;&#1583;&#1604;&#1610;&#1587;
&#1608;&#1575;&#1604;&#1575;&#1581;&#1578;&#1610;&#1575;&#1604; &#1571;&#1608;
&#1578;&#1593;&#1605;&#1583; &#1575;&#1604;&#1603;&#1584;&#1576;
&#1608;&#1578;&#1588;&#1608;&#1610;&#1607;
&#1575;&#1604;&#1581;&#1602;&#1575;&#1574;&#1602; &#1576;&#1594;&#1585;&#1590;
&#1575;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>41.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1578;&#1590;&#1604;&#1610;&#1604;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1581;&#1583;&#1608;&#1579;
&#1587;&#1604;&#1608;&#1603;&#1610;&#1575;&#1578; &#1605;&#1606;
&#1571;&#1588;&#1582;&#1575;&#1589; &#1571;&#1608;
&#1580;&#1607;&#1575;&#1578;&#1548; &#1576;&#1581;&#1610;&#1579; &#1604;&#1575;
&#1578;&#1602;&#1593; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1587;&#1604;&#1608;&#1603;&#1610;&#1575;&#1578;
&#1578;&#1581;&#1578; &#1578;&#1593;&#1585;&#1610;&#1601;
&#1575;&#1604;&#1575;&#1581;&#1578;&#1610;&#1575;&#1604;.</span><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>42.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1604;&#1581;&#1602;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1605;&#1587;&#1578;&#1606;&#1583;
&#1578;&#1589;&#1583;&#1585;&#1607; &#1576;&#1608;&#1576;&#1575;
&#1593;&#1604;&#1609; &#1606;&#1605;&#1608;&#1584;&#1580;
&#1585;&#1587;&#1605;&#1610; &#1605;&#1572;&#1585;&#1582;
&#1608;&#1605;&#1608;&#1602;&#1593; &#1593;&#1604;&#1610;&#1607; &#1605;&#1606;
&#1605;&#1608;&#1592;&#1601; &#1605;&#1582;&#1608;&#1604;
&#1576;&#1584;&#1604;&#1603; &#1603;&#1583;&#1604;&#1610;&#1604;
&#1593;&#1604;&#1609; &#1589;&#1581;&#1577; &#1571;&#1610;
&#1578;&#1593;&#1583;&#1610;&#1604; &#1601;&#1610;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;&#1548; &#1576;&#1581;&#1610;&#1579;
&#1604;&#1575; &#1610;&#1605;&#1587;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1571;&#1587;&#1575;&#1587;&#1610;&#1577;&#1548;
&#1576;&#1606;&#1575;&#1569;&#1611; &#1593;&#1604;&#1609; &#1591;&#1604;&#1576;
&#1582;&#1591;&#1610; &#1605;&#1606; &#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><span lang=EN-GB
dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l3 level1 lfo16;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>43.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1605;&#1604;&#1581;&#1602;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span></b><span dir=LTR></span><span
lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-size:11.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
mso-bidi-theme-font:minor-bidi;color:windowtext'>&#1578;&#1605;
&#1578;&#1582;&#1589;&#1610;&#1589; &#1605;&#1604;&#1581;&#1602;
&#1604;&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1608;&#1610;&#1581;&#1578;&#1608;&#1610; &#1593;&#1604;&#1609;
&#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1575;&#1578;
&#1608;&#1575;&#1604;&#1573;&#1580;&#1585;&#1575;&#1569;&#1575;&#1578;
&#1584;&#1575;&#1578; &#1575;&#1604;&#1593;&#1604;&#1575;&#1602;&#1577;
&#1576;&#1578;&#1591;&#1576;&#1610;&#1602; &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.<o:p></o:p></span></p>

<p class=Default dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=EN-GB dir=LTR style='font-size:11.0pt;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-theme-font:minor-bidi;color:windowtext'><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=606
 style='width:454.5pt;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;direction:rtl;unicode-bidi:
  embed'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;mso-border-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;tab-stops:center 66.6pt;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
  &#1604;&#1607;</span><span lang=AR-SA style='font-size:10.0pt;line-height:
  115%;font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'><span
  style='mso-tab-count:1'>            </span></span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

</div>

<h1 dir=RTL style='margin-top:0in;margin-right:19.45pt;margin-bottom:10.0pt;
margin-left:0in;text-align:right;text-indent:-.25in;mso-list:l24 level1 lfo24;
direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=AR-SA
style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-font-family:
Arial;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>&#1578;&#8204;.<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b><span lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:
Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'>&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;</span></b><b><span
lang=EN-GB dir=LTR style='font-size:16.0pt;mso-ascii-font-family:Arial;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><o:p></o:p></span></b></h1>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><b><span
lang=EN-GB style='font-size:2.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-fareast-font-family:"Times New Roman"'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:right;direction:rtl;unicode-bidi:embed'><b><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1605;&#1604;&#1575;&#1581;&#1592;&#1577;:</span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'> </span><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi'>&#1580;&#1605;&#1610;&#1593;
&#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593; &#1575;&#1604;&#1605;&#1587;&#1578;&#1581;&#1602;&#1577;&#1548;
&#1608;&#1601;&#1602;&#1575;&#1611; &#1604;&#1604;&#1580;&#1583;&#1608;&#1604; &#1571;&#1583;&#1606;&#1575;&#1607;&#1548;
&#1578;&#1594;&#1591;&#1610;&#1607;&#1575; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577; &#1593;&#1604;&#1609;
&#1571;&#1587;&#1575;&#1587; &#1578;&#1593;&#1608;&#1610;&#1590;
&#1575;&#1604;&#1576;&#1583;&#1604; &#1571;&#1608; &#1593;&#1604;&#1609;
&#1571;&#1587;&#1575;&#1587; &#1575;&#1604;&#1578;&#1602;&#1610;&#1610;&#1583;
&#1575;&#1604;&#1605;&#1576;&#1575;&#1588;&#1585; &#1605;&#1606; &#1588;&#1576;&#1603;&#1577;
&#1605;&#1602;&#1583;&#1605;&#1610; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583;&#1610;&#1606;.</span><span
lang=EN-GB dir=LTR style='font-size:2.0pt;line-height:115%'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><span
lang=EN-GB style='font-size:2.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<table class=TableGrid1 border=1 cellspacing=0 cellpadding=0 width=677
 style='width:507.55pt;margin-left:-28.0pt;border-collapse:collapse;border:
 none;mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:42.6pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;mso-border-alt:
  solid windowtext .5pt;background:#0079C8;mso-background-themecolor:text2;
  padding:0in 5.4pt 0in 5.4pt;height:42.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-size:12.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
  color:white;mso-themecolor:background1'>&#1575;&#1604;&#1581;&#1583;</span></b><b><span
  dir=LTR style='font-size:12.0pt;font-family:"Tahoma","sans-serif";mso-fareast-font-family:
  Calibri;color:white;mso-themecolor:background1;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
  <td width=310 style='width:232.6pt;border:solid windowtext 1.0pt;border-left:
  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  background:#0079C8;mso-background-themecolor:text2;padding:0in 5.4pt 0in 5.4pt;
  height:42.6pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-size:12.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
  color:white;mso-themecolor:background1'>&#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;</span></b><b><span
  lang=EN-GB dir=LTR style='font-size:12.0pt;font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
  color:white;mso-themecolor:background1'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
  color:#333333;background:yellow;mso-highlight:yellow'><span dir=RTL></span>&lt;
  &#1575;&#1604;&#1588;&#1576;&#1603;&#1577;
  &#1575;&#1604;&#1605;&#1582;&#1578;&#1575;&#1585;&#1577; &gt;<o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1588;&#1576;&#1603;&#1577;
  &#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1610;&#1575;&#1578; &#1608;&#1575;&#1604;&#1605;&#1587;&#1578;&#1608;&#1589;&#1601;&#1575;&#1578;
  &#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583;&#1577;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  dir=RTL></span><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
  minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span
  dir=RTL></span>100,000 &#1585;.&#1587;.</span><span lang=EN-GB dir=LTR
  style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1581;&#1583;</span></b><span dir=LTR></span><b><span
  lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span> </span></b><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
  &#1575;&#1604;&#1571;&#1602;&#1589;&#1609; &#1604;&#1603;&#1604;
  &#1588;&#1582;&#1589; &#1593;&#1606; &#1605;&#1583;&#1577;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; <o:p></o:p></span></b></p>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1608;&#1610;&#1588;&#1605;&#1604;
  &#1584;&#1604;&#1603; &#1575;&#1604;&#1581;&#1583;&#1608;&#1583;
  &#1575;&#1604;&#1583;&#1606;&#1610;&#1575;
  &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1601;&#1610;
  &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span
  lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
  &#1575;&#1604;&#1571;&#1602;&#1589;&#1609; &#1604;&#1607;&#1584;&#1607;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1606;&#1601;&#1602;&#1575;&#1578; &#1605;&#1593;&#1575;&#1610;&#1606;&#1577;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1608;&#1593;&#1604;&#1575;&#1580;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
  &#1575;&#1604;&#1571;&#1602;&#1589;&#1609; &#1604;&#1607;&#1584;&#1607;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1578;&#1606;&#1608;&#1610;&#1605;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1576;&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1604;&#1575;
  &#1610;&#1608;&#1580;&#1583;</span><span lang=EN-GB dir=LTR style='font-family:
  "Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
  Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1606;&#1587;&#1576;&#1577;
  &#1575;&#1604;&#1578;&#1581;&#1605;&#1604;</span></b><b><span dir=LTR
  style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:45.2pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.2pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609;: &#1594;&#1585;&#1601;&#1577;
  &#1605;&#1588;&#1578;&#1585;&#1603;&#1577;<o:p></o:p></span></p>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1576;&#1581;&#1583;
  &#1571;&#1602;&#1589;&#1609; 600 &#1585;.&#1587;.
  &#1604;&#1604;&#1610;&#1608;&#1605;
  &#1575;&#1604;&#1608;&#1575;&#1581;&#1583;</span><span lang=EN-GB dir=LTR
  style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:45.2pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1581;&#1583;
  &#1575;&#1604;&#1573;&#1602;&#1575;&#1605;&#1577;
  &#1608;&#1575;&#1604;&#1573;&#1593;&#1575;&#1588;&#1577;
  &#1575;&#1604;&#1610;&#1608;&#1605;&#1610;
  &#1604;&#1604;&#1605;&#1585;&#1610;&#1590;</span></b><b><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1571;&#1602;&#1589;&#1609; 5,000 &#1585;.&#1587;.
  &#1582;&#1604;&#1575;&#1604; &#1605;&#1583;&#1577;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1606;&#1601;&#1602;&#1575;&#1578;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1581;&#1605;&#1604;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1608;&#1575;&#1604;&#1608;&#1604;&#1575;&#1583;&#1577;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
  &#1575;&#1604;&#1571;&#1602;&#1589;&#1609; &#1604;&#1607;&#1584;&#1607;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
  &#1608;&#1604;&#1575;&#1583;&#1577; &#1608;&#1593;&#1604;&#1575;&#1580;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1571;&#1591;&#1601;&#1575;&#1604;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1605;&#1576;&#1578;&#1587;&#1585;&#1610;&#1606;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1571;&#1602;&#1589;&#1609; 5,000 &#1585;.&#1587;.
  &#1582;&#1604;&#1575;&#1604; &#1605;&#1583;&#1577;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
  &#1587;&#1601;&#1585;</span></b><span dir=LTR></span><b><span lang=AR-SA
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span> </span></b><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1608;&#1605;&#1585;&#1575;&#1601;&#1602;&#1577;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1593;&#1590;&#1608;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1571;&#1587;&#1585;&#1577;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1605;&#1576;&#1575;&#1588;&#1585;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1608;&#1575;&#1581;&#1583;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;height:46.3pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:46.3pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609;: &#1594;&#1585;&#1601;&#1577;
  &#1605;&#1588;&#1578;&#1585;&#1603;&#1577;<o:p></o:p></span></p>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1571;&#1602;&#1589;&#1609; 150 &#1585;.&#1587;.
  &#1604;&#1604;&#1610;&#1608;&#1605;
  &#1575;&#1604;&#1608;&#1575;&#1581;&#1583;</span><span lang=EN-GB dir=LTR
  style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:46.3pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1581;&#1583;
  &#1575;&#1604;&#1573;&#1602;&#1575;&#1605;&#1577;
  &#1608;&#1575;&#1604;&#1573;&#1593;&#1575;&#1588;&#1577;
  &#1575;&#1604;&#1610;&#1608;&#1605;&#1610;
  &#1604;&#1604;&#1605;&#1585;&#1575;&#1601;&#1602;</span></b><b><span
  lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1571;&#1602;&#1589;&#1609; 500 &#1585;.&#1587;.
  &#1582;&#1604;&#1575;&#1604; &#1605;&#1583;&#1577;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
  &#1593;&#1604;&#1575;&#1580;</span></b><span dir=LTR></span><b><span
  lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span> </span></b><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1571;&#1587;&#1606;&#1575;&#1606;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
  &#1575;&#1604;&#1571;&#1602;&#1589;&#1609; &#1604;&#1607;&#1584;&#1607;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1573;&#1589;&#1575;&#1576;&#1575;&#1578;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1606;&#1575;&#1578;&#1580;&#1577;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1593;&#1606;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1581;&#1608;&#1575;&#1583;&#1579;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1587;&#1610;&#1585;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
  &#1575;&#1604;&#1571;&#1602;&#1589;&#1609; &#1604;&#1607;&#1584;&#1607;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
  &#1575;&#1604;&#1594;&#1587;&#1610;&#1604;</span></b><span dir=LTR></span><b><span
  lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span> </span></b><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1603;&#1604;&#1608;&#1610;
  &#1604;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;</span></b><span dir=LTR></span><b><span
  lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span> </span></b><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
  &#1575;&#1604;&#1571;&#1602;&#1589;&#1609; &#1604;&#1607;&#1584;&#1607;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; </span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1573;&#1582;&#1604;&#1575;&#1569;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1591;&#1576;&#1610;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1583;&#1575;&#1582;&#1604;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1608;&#1582;&#1575;&#1585;&#1580;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
  &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1605;&#1594;&#1591;&#1609; &#1576;&#1581;&#1583;
  &#1571;&#1602;&#1589;&#1609; 10,000 &#1585;.&#1587;.
  &#1582;&#1604;&#1575;&#1604; &#1605;&#1583;&#1577;
  &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span><span lang=EN-GB
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'>&#1573;&#1593;&#1575;&#1583;&#1577;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1585;&#1601;&#1575;&#1578;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1605;&#1578;&#1608;&#1601;&#1610;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1573;&#1604;&#1609;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1605;&#1608;&#1591;&#1606;&#1607;</span></b><span
  dir=LTR></span><b><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'><span dir=LTR></span>
  </span></b><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
  mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
  minor-latin;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
  mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US'>&#1575;&#1604;&#1571;&#1589;&#1604;&#1610;</span></b><b><span
  dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;
  mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
  minor-bidi;mso-ansi-language:EN-US'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes;height:30.05pt'>
  <td width=367 style='width:274.95pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:center;line-height:normal;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
  &#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;</span><span
  lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>
  </td>
  <td width=310 style='width:232.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:30.05pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:normal;direction:rtl;unicode-bidi:embed'><b><span
  lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
  mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
  Arial;mso-bidi-theme-font:minor-bidi'>&#1606;&#1591;&#1575;&#1602;
  &#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;</span></b><b><span
  lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
  minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:minor-bidi;
  mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><b><span
lang=EN-GB style='font-size:3.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:Calibri;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:6.0pt;margin-left:0in;mso-add-space:auto;text-align:right;
line-height:normal;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpMiddle dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:6.0pt;margin-left:0in;mso-add-space:auto;text-align:right;
line-height:normal;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraphCxSpLast dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:6.0pt;margin-left:0in;mso-add-space:auto;text-align:right;
line-height:normal;direction:rtl;unicode-bidi:embed'><span lang=EN-GB dir=LTR
style='font-size:3.0pt;font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=606
 style='width:454.5pt;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;direction:rtl;unicode-bidi:
  embed'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;tab-stops:101.3pt;direction:rtl;unicode-bidi:embed'><span
  lang=AR-SA style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;<span style='mso-tab-count:
  1'>                      </span></span><b style='mso-bidi-font-weight:normal'><span
  lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;mso-border-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;tab-stops:center 66.6pt;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
  &#1604;&#1607;</span><span lang=AR-SA style='font-size:10.0pt;line-height:
  115%;font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'><span
  style='mso-tab-count:1'>            </span></span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

</div>

<h1 dir=RTL style='margin-top:0in;margin-right:22.5pt;margin-bottom:10.0pt;
margin-left:0in;text-align:right;text-indent:-.25in;mso-list:l24 level1 lfo24;
direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=AR-SA
style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-font-family:
Arial;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>&#1579;&#8204;.<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b><span lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:
Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'>&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1575;&#1604;&#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577; /
&#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;</span></b><b><span
lang=EN-GB dir=LTR style='font-size:16.0pt;mso-ascii-font-family:Arial;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><o:p></o:p></span></b></h1>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt'><b><span
lang=EN-GB style='font-size:2.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-fareast-font-family:"Times New Roman"'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal dir=RTL style='margin-bottom:6.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi'>&#1604;&#1571;&#1594;&#1585;&#1575;&#1590;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1601;&#1573;&#1606; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1575;&#1604;&#1602;&#1575;&#1576;&#1604;&#1577; &#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577;
&#1578;&#1593;&#1606;&#1610;
&#1575;&#1604;&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1575;&#1604;&#1601;&#1593;&#1604;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1606;&#1601;&#1602;&#1577; &#1605;&#1602;&#1575;&#1576;&#1604;
&#1582;&#1583;&#1605;&#1575;&#1578; &#1608;&#1605;&#1608;&#1575;&#1583;
&#1608;&#1571;&#1580;&#1607;&#1586;&#1577; &#1594;&#1610;&#1585;
&#1605;&#1587;&#1578;&#1579;&#1606;&#1575;&#1577; &#1576;&#1605;&#1608;&#1580;&#1576;
&#1575;&#1604;&#1601;&#1602;&#1585;&#1577; &quot;&#1580;&quot; &#1605;&#1606;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;&#1548;
&#1593;&#1604;&#1609; &#1571;&#1606; &#1610;&#1589;&#1601;&#1607;&#1575;
&#1591;&#1576;&#1610;&#1576; &#1605;&#1585;&#1582;&#1589;
&#1576;&#1587;&#1576;&#1576; &#1593;&#1604;&#1577; &#1578;&#1593;&#1585;&#1590;
&#1604;&#1607;&#1575; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1588;&#1585;&#1610;&#1591;&#1577; &#1571;&#1606; &#1578;&#1603;&#1608;&#1606;
&#1578;&#1604;&#1603; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1590;&#1585;&#1608;&#1585;&#1610;&#1577; &#1608;&#1605;&#1593;&#1602;&#1608;&#1604;&#1577;
&#1608;&#1605;&#1593;&#1578;&#1575;&#1583;&#1577; &#1601;&#1610;
&#1575;&#1604;&#1608;&#1602;&#1578;
&#1608;&#1575;&#1604;&#1605;&#1603;&#1575;&#1606; &#1575;&#1604;&#1584;&#1610;
&#1578;&#1605;&#1578; &#1601;&#1610;&#1607;.<o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-bottom:6.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi'>&#1608;&#1576;&#1606;&#1575;&#1569;&#1611;
&#1593;&#1604;&#1610;&#1607;&#1548; &#1587;&#1608;&#1601;
&#1578;&#1588;&#1605;&#1604; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1575;&#1604;&#1602;&#1575;&#1576;&#1604;&#1577; &#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577;
&#1601;&#1602;&#1591; &#1604;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;&#1548; &#1603;&#1605;&#1575;
&#1610;&#1604;&#1610;:<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l15 level1 lfo13;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577;:</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l21 level1 lfo42;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1571;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1580;&#1605;&#1610;&#1593;
&#1605;&#1589;&#1575;&#1585;&#1610;&#1601; &#1575;&#1604;&#1603;&#1588;&#1601;
&#1575;&#1604;&#1591;&#1576;&#1610;
&#1608;&#1575;&#1604;&#1578;&#1588;&#1582;&#1610;&#1589;
&#1608;&#1575;&#1604;&#1593;&#1604;&#1575;&#1580; &#1608;&#1575;&#1604;&#1571;&#1583;&#1608;&#1610;&#1577;&#1548;
&#1608;&#1601;&#1602;&#1575;&#1611; &#1604;&#1580;&#1583;&#1608;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l21 level1 lfo42;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1576;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1580;&#1605;&#1610;&#1593;
&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1575;&#1604;&#1578;&#1606;&#1608;&#1610;&#1605;
&#1576;&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l21 level1 lfo42;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1578;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1581;&#1605;&#1604;
&#1608;&#1575;&#1604;&#1608;&#1604;&#1575;&#1583;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l21 level1 lfo42;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1579;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1605;&#1585;&#1575;&#1590;
&#1575;&#1604;&#1571;&#1587;&#1606;&#1575;&#1606;
&#1608;&#1575;&#1604;&#1604;&#1579;&#1577;&#1548;
&#1608;&#1610;&#1606;&#1581;&#1589;&#1585; &#1593;&#1604;&#1575;&#1580;
&#1575;&#1604;&#1571;&#1587;&#1606;&#1575;&#1606; &#1601;&#1610;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578; &#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;
&#1576;&#1605;&#1575; &#1601;&#1610; &#1584;&#1604;&#1603;
&#1575;&#1604;&#1581;&#1588;&#1608; &#1608;&#1593;&#1604;&#1575;&#1580;
&#1575;&#1604;&#1593;&#1589;&#1576; &#1608;&#1587;&#1581;&#1576;
&#1575;&#1604;&#1582;&#1585;&#1575;&#1580; &#1608;&#1605;&#1575;
&#1610;&#1578;&#1591;&#1604;&#1576;&#1607;
&#1575;&#1587;&#1578;&#1603;&#1605;&#1575;&#1604; &#1607;&#1584;&#1575;
&#1575;&#1604;&#1593;&#1604;&#1575;&#1580; &#1605;&#1606;
&#1573;&#1580;&#1585;&#1575;&#1569;&#1575;&#1578; &#1591;&#1576;&#1610;&#1577;
&#1605;&#1578;&#1593;&#1575;&#1585;&#1601; &#1593;&#1604;&#1610;&#1607;&#1575;
&#1608;&#1603;&#1584;&#1604;&#1603;
&#1575;&#1604;&#1605;&#1590;&#1575;&#1583;&#1575;&#1578;
&#1575;&#1604;&#1581;&#1610;&#1608;&#1610;&#1577;
&#1608;&#1605;&#1587;&#1603;&#1606;&#1575;&#1578;
&#1575;&#1604;&#1571;&#1604;&#1605;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l21 level1 lfo42;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1580;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1571;&#1591;&#1601;&#1575;&#1604;
&#1575;&#1604;&#1605;&#1576;&#1578;&#1587;&#1585;&#1610;&#1606;&#1548;
&#1608;&#1578;&#1594;&#1591;&#1609; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578; &#1603;&#1580;&#1586;&#1569;
&#1605;&#1606; &#1578;&#1594;&#1591;&#1610;&#1577; &#1575;&#1604;&#1571;&#1605;
&#1608;&#1610;&#1582;&#1590;&#1593; &#1604;&#1604;&#1581;&#1583;
&#1575;&#1604;&#1571;&#1602;&#1589;&#1609;
&#1604;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
&#1575;&#1604;&#1582;&#1575;&#1589;&#1577; &#1576;&#1575;&#1604;&#1571;&#1605;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l21 level1 lfo42;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1581;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1594;&#1587;&#1610;&#1604;
&#1575;&#1604;&#1603;&#1604;&#1608;&#1610;
&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l21 level1 lfo42;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1582;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1573;&#1582;&#1604;&#1575;&#1569;
&#1575;&#1604;&#1591;&#1576;&#1610; &#1575;&#1604;&#1591;&#1575;&#1585;&#1574;
&#1583;&#1575;&#1582;&#1604; &#1608;&#1582;&#1575;&#1585;&#1580;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l21 level1 lfo42;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1583;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1573;&#1589;&#1575;&#1576;&#1575;&#1578;
&#1575;&#1604;&#1606;&#1575;&#1578;&#1580;&#1577; &#1593;&#1606;
&#1581;&#1608;&#1575;&#1583;&#1579; &#1575;&#1604;&#1587;&#1610;&#1585;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l15 level1 lfo13;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1578;&#1580;&#1607;&#1610;&#1586; &#1608;&#1573;&#1593;&#1575;&#1583;&#1577;
&#1580;&#1579;&#1605;&#1575;&#1606; &#1575;&#1604;&#1586;&#1575;&#1574;&#1585;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1575;&#1604;&#1605;&#1578;&#1608;&#1601;&#1609; &#1573;&#1604;&#1609;
&#1605;&#1608;&#1591;&#1606;&#1607; &#1575;&#1604;&#1571;&#1589;&#1604;&#1610;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoNormal dir=RTL style='margin-bottom:3.0pt;text-align:right;
line-height:150%;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<h1 dir=RTL style='margin-top:0in;margin-right:22.5pt;margin-bottom:10.0pt;
margin-left:0in;text-align:right;text-indent:-.25in;mso-list:l24 level1 lfo24;
direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=AR-SA
style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-font-family:
Arial;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>&#1580;&#8204;.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b><span lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:
Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'>&#1575;&#1604;&#1578;&#1581;&#1583;&#1610;&#1583;&#1575;&#1578;
&#1608;&#1575;&#1604;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569;&#1575;&#1578;</span></b><b><span
lang=EN-GB dir=LTR style='font-size:16.0pt;mso-ascii-font-family:Arial;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><o:p></o:p></span></b></h1>

<p class=MsoNormal dir=RTL style='margin-bottom:6.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1604;&#1606; &#1578;&#1594;&#1591;&#1610;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577; &#1571;&#1608; &#1571;&#1610;
&#1605;&#1591;&#1575;&#1604;&#1576;&#1575;&#1578; &#1578;&#1606;&#1588;&#1571; &#1593;&#1605;&#1575;
&#1610;&#1604;&#1610;: </span></b><b><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1593;&#1604;&#1575;&#1580;
&#1608;&#1575;&#1604;&#1601;&#1581;&#1608;&#1589;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577; &#1575;&#1604;&#1578;&#1610;
&#1605;&#1606; &#1575;&#1604;&#1605;&#1605;&#1603;&#1606; &#1571;&#1606;
&#1578;&#1572;&#1580;&#1604; &#1581;&#1578;&#1609; &#1593;&#1608;&#1583;&#1577;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1604;&#1608;&#1591;&#1606;&#1607;&#1548; &#1576;&#1605;&#1575; &#1601;&#1610;
&#1584;&#1604;&#1603; &#1573;&#1593;&#1575;&#1583;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1607;&#1610;&#1604; &#1581;&#1587;&#1576;
&#1578;&#1602;&#1583;&#1610;&#1585; &#1575;&#1604;&#1591;&#1576;&#1610;&#1576;
&#1575;&#1604;&#1605;&#1585;&#1582;&#1589;&#1548;
&#1575;&#1604;&#1605;&#1593;&#1575;&#1604;&#1580;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1571;&#1605;&#1585;&#1575;&#1590;
&#1575;&#1604;&#1578;&#1610; &#1578;&#1606;&#1588;&#1571;
&#1576;&#1601;&#1593;&#1604; &#1573;&#1587;&#1575;&#1569;&#1577;
&#1575;&#1587;&#1578;&#1593;&#1605;&#1575;&#1604; &#1576;&#1593;&#1590; &#1575;&#1604;&#1571;&#1583;&#1608;&#1610;&#1577;
&#1571;&#1608; &#1575;&#1604;&#1605;&#1606;&#1588;&#1591;&#1575;&#1578;
&#1571;&#1608; &#1575;&#1604;&#1605;&#1607;&#1583;&#1574;&#1575;&#1578;
&#1571;&#1608; &#1576;&#1601;&#1593;&#1604; &#1578;&#1593;&#1575;&#1591;&#1610;
&#1575;&#1604;&#1605;&#1608;&#1575;&#1583;
&#1575;&#1604;&#1603;&#1581;&#1608;&#1604;&#1610;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1582;&#1583;&#1585;&#1575;&#1578; &#1571;&#1608;
&#1605;&#1575; &#1588;&#1575;&#1576;&#1607; &#1584;&#1604;&#1603;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1580;&#1585;&#1575;&#1581;&#1577;
&#1571;&#1608; &#1575;&#1604;&#1605;&#1593;&#1575;&#1604;&#1580;&#1577;
&#1575;&#1604;&#1578;&#1580;&#1605;&#1610;&#1604;&#1610;&#1577;&#1548;
&#1573;&#1604;&#1575; &#1573;&#1584;&#1575; &#1575;&#1587;&#1578;&#1583;&#1593;&#1578;&#1607;&#1575;
&#1573;&#1589;&#1575;&#1576;&#1577; &#1580;&#1587;&#1583;&#1610;&#1577;
&#1593;&#1585;&#1590;&#1610;&#1577; &#1594;&#1610;&#1585;
&#1605;&#1587;&#1578;&#1579;&#1606;&#1575;&#1577; &#1601;&#1610; &#1607;&#1584;&#1575;
&#1575;&#1604;&#1602;&#1587;&#1605; &#1608;&#1571;&#1602;&#1585;&#1607;&#1575;
&#1591;&#1576;&#1610;&#1576; &#1605;&#1585;&#1582;&#1589;
&#1608;&#1605;&#1582;&#1578;&#1575;&#1585; &#1605;&#1606; &#1602;&#1576;&#1604;
&#1576;&#1608;&#1576;&#1575;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1601;&#1581;&#1608;&#1589;&#1575;&#1578;
&#1575;&#1604;&#1588;&#1575;&#1605;&#1604;&#1577;
&#1608;&#1575;&#1604;&#1604;&#1602;&#1575;&#1581;&#1575;&#1578; &#1571;&#1608;
&#1575;&#1604;&#1593;&#1602;&#1575;&#1602;&#1610;&#1585; &#1571;&#1608;
&#1575;&#1604;&#1608;&#1587;&#1575;&#1574;&#1604;
&#1575;&#1604;&#1608;&#1602;&#1575;&#1574;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1610; &#1604;&#1575;
&#1578;&#1578;&#1591;&#1604;&#1576;&#1607;&#1575;
&#1605;&#1593;&#1575;&#1604;&#1580;&#1577; &#1591;&#1576;&#1610;&#1577;
&#1605;&#1606;&#1589;&#1608;&#1589; &#1593;&#1604;&#1610;&#1607;&#1575;
&#1601;&#1610; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1593;&#1575;&#1604;&#1580;&#1577;
&#1575;&#1604;&#1578;&#1610; &#1610;&#1578;&#1604;&#1602;&#1575;&#1607;&#1575; &#1575;&#1604;&#1588;&#1582;&#1589;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1576;&#1583;&#1608;&#1606;
&#1605;&#1602;&#1575;&#1576;&#1604;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1575;&#1587;&#1578;&#1580;&#1605;&#1575;&#1605;
&#1608;&#1575;&#1604;&#1606;&#1602;&#1575;&#1607;&#1577;
&#1608;&#1575;&#1604;&#1578;&#1580;&#1605;&#1610;&#1604; &#1608;&#1576;&#1585;&#1575;&#1605;&#1580;
&#1575;&#1604;&#1589;&#1581;&#1577; &#1575;&#1604;&#1576;&#1583;&#1606;&#1610;&#1577;
&#1575;&#1604;&#1593;&#1575;&#1605;&#1577; &#1608;&#1575;&#1604;&#1593;&#1604;&#1575;&#1580;
&#1601;&#1610; &#1583;&#1608;&#1585; &#1575;&#1604;&#1585;&#1593;&#1575;&#1610;&#1577;
&#1575;&#1604;&#1575;&#1580;&#1578;&#1605;&#1575;&#1593;&#1610;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1610; &#1593;&#1604;&#1577; &#1571;&#1608;
&#1573;&#1589;&#1575;&#1576;&#1577; &#1578;&#1606;&#1588;&#1571;
&#1603;&#1606;&#1578;&#1610;&#1580;&#1577;
&#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1604;&#1605;&#1607;&#1606;&#1577;
&#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607;&#1548;
&#1608;&#1575;&#1604;&#1573;&#1589;&#1575;&#1576;&#1575;&#1578;
&#1606;&#1578;&#1610;&#1580;&#1577; &#1575;&#1588;&#1578;&#1585;&#1575;&#1603;
&#1601;&#1610; &#1605;&#1587;&#1575;&#1576;&#1602;&#1575;&#1578;
&#1585;&#1587;&#1605;&#1610;&#1577;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1593;&#1575;&#1604;&#1580;&#1577;
&#1575;&#1604;&#1571;&#1605;&#1585;&#1575;&#1590;
&#1575;&#1604;&#1578;&#1606;&#1575;&#1587;&#1604;&#1610;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1578;&#1610; &#1578;&#1606;&#1578;&#1602;&#1604;
&#1576;&#1575;&#1604;&#1575;&#1578;&#1589;&#1575;&#1604;
&#1575;&#1604;&#1580;&#1606;&#1587;&#1610;
&#1575;&#1604;&#1605;&#1578;&#1593;&#1575;&#1585;&#1601;
&#1593;&#1604;&#1610;&#1607;&#1575; &#1591;&#1576;&#1610;&#1575;&#1611;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1593;&#1604;&#1575;&#1580; &#1575;&#1604;&#1601;&#1578;&#1585;&#1577; &#1575;&#1604;&#1604;&#1575;&#1581;&#1602;&#1577;
&#1604;&#1578;&#1588;&#1582;&#1610;&#1589; (&#1601;&#1610;&#1585;&#1608;&#1587;
&#1606;&#1602;&#1589; &#1575;&#1604;&#1605;&#1606;&#1575;&#1593;&#1577; &#1575;&#1604;&#1576;&#1588;&#1585;&#1610;&#1577;)
</span><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>HIV</span></b><span dir=RTL></span><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'><span dir=RTL></span> </span><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;
&#1575;&#1604;&#1593;&#1604;&#1604; &#1584;&#1575;&#1578;
&#1575;&#1604;&#1589;&#1604;&#1577; &#1576;&#1575;&#1604;&#1600;</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'>HIV </span></b><span
dir=RTL></span><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'><span dir=RTL></span><span
style='mso-spacerun:yes'> </span>&#1576;&#1605;&#1575;
&#1601;&#1610;&#1607;&#1575; &#1605;&#1585;&#1590; &#1575;&#1604;&#1573;&#1610;&#1583;&#1586;
(&#1606;&#1602;&#1589; &#1575;&#1604;&#1605;&#1606;&#1575;&#1593;&#1577; &#1575;&#1604;&#1605;&#1603;&#1578;&#1587;&#1576;&#1577;)
&#1571;&#1608; &#1605;&#1588;&#1578;&#1602;&#1575;&#1578;&#1607;&#1575; &#1571;&#1608;
&#1605;&#1585;&#1575;&#1583;&#1601;&#1575;&#1578;&#1607;&#1575; &#1571;&#1608;
&#1571;&#1588;&#1603;&#1575;&#1604;&#1607;&#1575;
&#1575;&#1604;&#1571;&#1582;&#1585;&#1609;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1580;&#1605;&#1610;&#1593;
&#1575;&#1604;&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
&#1575;&#1604;&#1605;&#1578;&#1593;&#1604;&#1602;&#1577;
&#1576;&#1586;&#1585;&#1593; &#1575;&#1604;&#1571;&#1587;&#1606;&#1575;&#1606; &#1571;&#1608;
&#1578;&#1585;&#1603;&#1610;&#1576;
&#1575;&#1604;&#1571;&#1587;&#1606;&#1575;&#1606;
&#1575;&#1604;&#1589;&#1606;&#1575;&#1593;&#1610;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1580;&#1587;&#1608;&#1585;
&#1575;&#1604;&#1579;&#1575;&#1576;&#1578;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1578;&#1581;&#1585;&#1603;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1578;&#1602;&#1608;&#1610;&#1605;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
&#1575;&#1604;&#1606;&#1592;&#1575;&#1585;&#1575;&#1578;
&#1608;&#1575;&#1604;&#1587;&#1605;&#1575;&#1593;&#1575;&#1578; &#1608;&#1575;&#1582;&#1578;&#1576;&#1575;&#1585;&#1575;&#1578;
&#1608;&#1593;&#1605;&#1604;&#1610;&#1575;&#1578; &#1578;&#1589;&#1581;&#1610;&#1581;
&#1575;&#1604;&#1606;&#1592;&#1585; &#1571;&#1608;
&#1575;&#1604;&#1587;&#1605;&#1593;
&#1608;&#1575;&#1604;&#1608;&#1587;&#1575;&#1574;&#1604;
&#1575;&#1604;&#1576;&#1589;&#1585;&#1610;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1587;&#1605;&#1593;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1587;&#1575;&#1593;&#1583;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1575;&#1606;&#1578;&#1602;&#1575;&#1604; &#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1583;&#1575;&#1582;&#1604; &#1608;&#1576;&#1610;&#1606;
&#1605;&#1583;&#1606; &#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1576;&#1608;&#1587;&#1575;&#1574;&#1604; &#1606;&#1602;&#1604;
&#1594;&#1610;&#1585; &#1605;&#1585;&#1582;&#1589;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1578;&#1587;&#1575;&#1602;&#1591;
&#1575;&#1604;&#1588;&#1593;&#1585; &#1571;&#1608;
&#1575;&#1604;&#1589;&#1604;&#1593; &#1571;&#1608;
&#1575;&#1604;&#1588;&#1593;&#1585;
&#1575;&#1604;&#1605;&#1587;&#1578;&#1593;&#1575;&#1585;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1593;&#1575;&#1604;&#1580;&#1577;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578; &#1575;&#1604;&#1606;&#1601;&#1587;&#1610;&#1577;
&#1571;&#1608;
&#1575;&#1604;&#1575;&#1590;&#1591;&#1585;&#1575;&#1576;&#1575;&#1578;
&#1575;&#1604;&#1593;&#1602;&#1604;&#1610;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1593;&#1589;&#1576;&#1610;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>15.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1582;&#1578;&#1576;&#1575;&#1585;&#1575;&#1578;
&#1575;&#1604;&#1581;&#1587;&#1575;&#1587;&#1610;&#1577;
&#1605;&#1607;&#1605;&#1575; &#1603;&#1575;&#1606;&#1578;
&#1591;&#1576;&#1610;&#1593;&#1578;&#1607;&#1575;
&#1576;&#1582;&#1604;&#1575;&#1601; &#1578;&#1604;&#1603;
&#1575;&#1604;&#1605;&#1578;&#1593;&#1604;&#1602;&#1577; &#1576;&#1608;&#1589;&#1601;
&#1571;&#1583;&#1608;&#1610;&#1577; &#1575;&#1604;&#1593;&#1604;&#1575;&#1580;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>16.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1571;&#1580;&#1607;&#1586;&#1577;
&#1608;&#1575;&#1604;&#1608;&#1587;&#1575;&#1574;&#1604;
&#1608;&#1575;&#1604;&#1593;&#1602;&#1575;&#1602;&#1610;&#1585;
&#1608;&#1575;&#1604;&#1573;&#1580;&#1585;&#1575;&#1569;&#1575;&#1578; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1593;&#1575;&#1604;&#1580;&#1577;
&#1576;&#1575;&#1604;&#1607;&#1585;&#1605;&#1608;&#1606;&#1575;&#1578;
&#1576;&#1607;&#1583;&#1601; &#1578;&#1606;&#1592;&#1610;&#1605;
&#1575;&#1604;&#1606;&#1587;&#1604; &#1571;&#1608; &#1605;&#1606;&#1593;
&#1575;&#1604;&#1581;&#1605;&#1604; &#1571;&#1608;
&#1581;&#1589;&#1608;&#1604;&#1607; &#1571;&#1608;
&#1575;&#1604;&#1593;&#1602;&#1605; &#1571;&#1608;
&#1575;&#1604;&#1593;&#1580;&#1586; &#1575;&#1604;&#1580;&#1606;&#1587;&#1610; &#1571;&#1608;
&#1606;&#1602;&#1589; &#1575;&#1604;&#1582;&#1589;&#1608;&#1576;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1578;&#1582;&#1589;&#1610;&#1576;
&#1576;&#1608;&#1575;&#1587;&#1591;&#1577;
&#1575;&#1604;&#1571;&#1606;&#1575;&#1576;&#1610;&#1576; &#1571;&#1608;
&#1571;&#1610; &#1608;&#1587;&#1575;&#1574;&#1604; &#1571;&#1582;&#1585;&#1609;
&#1604;&#1604;&#1578;&#1604;&#1602;&#1610;&#1581;
&#1575;&#1604;&#1575;&#1589;&#1591;&#1606;&#1575;&#1593;&#1610;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>17.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1590;&#1593;&#1601; &#1571;&#1608;
&#1575;&#1604;&#1578;&#1588;&#1608;&#1607;
&#1575;&#1604;&#1582;&#1604;&#1602;&#1610; &#1573;&#1604;&#1575;
&#1573;&#1584;&#1575; &#1603;&#1575;&#1606;&#1578; &#1578;&#1588;&#1603;&#1604;
&#1582;&#1591;&#1608;&#1585;&#1577; &#1593;&#1604;&#1609;
&#1581;&#1610;&#1575;&#1577; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607;.</span><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>18.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1610;
&#1578;&#1603;&#1575;&#1604;&#1610;&#1601; &#1571;&#1608;
&#1605;&#1589;&#1575;&#1585;&#1610;&#1601;
&#1573;&#1590;&#1575;&#1601;&#1610;&#1577;
&#1610;&#1578;&#1603;&#1576;&#1583;&#1607;&#1575;
&#1575;&#1604;&#1605;&#1585;&#1575;&#1601;&#1602;
&#1604;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1571;&#1579;&#1606;&#1575;&#1569;
&#1578;&#1606;&#1608;&#1610;&#1605;&#1607; &#1571;&#1608;
&#1573;&#1602;&#1575;&#1605;&#1578;&#1607;
&#1576;&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609;
&#1593;&#1583;&#1575; &#1606;&#1601;&#1602;&#1577;
&#1575;&#1604;&#1573;&#1602;&#1575;&#1605;&#1577;
&#1608;&#1575;&#1604;&#1573;&#1593;&#1575;&#1588;&#1577; &#1601;&#1610;
&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609;
&#1604;&#1605;&#1585;&#1575;&#1601;&#1602; &#1608;&#1575;&#1581;&#1583;
&#1604;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;&#1548; &#1603;&#1605;&#1585;&#1575;&#1601;&#1602;&#1577;
&#1575;&#1604;&#1571;&#1605; &#1604;&#1591;&#1601;&#1604;&#1607;&#1575;
&#1581;&#1578;&#1609; &#1587;&#1606;
&#1575;&#1604;&#1579;&#1575;&#1606;&#1610;&#1577; &#1593;&#1588;&#1585;&#1577;&#1548;
&#1571;&#1608; &#1581;&#1610;&#1579;&#1605;&#1575;
&#1578;&#1602;&#1578;&#1590;&#1610;
&#1575;&#1604;&#1590;&#1585;&#1608;&#1585;&#1577;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577; &#1584;&#1604;&#1603;
&#1581;&#1587;&#1576; &#1578;&#1602;&#1583;&#1610;&#1585; &#1575;&#1604;&#1591;&#1576;&#1610;&#1576;
&#1575;&#1604;&#1605;&#1593;&#1575;&#1604;&#1580;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>19.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1593;&#1575;&#1604;&#1580;&#1577;
&#1575;&#1604;&#1576;&#1579;&#1608;&#1585; (&#1581;&#1576;
&#1575;&#1604;&#1588;&#1576;&#1575;&#1576;) &#1571;&#1608;
&#1571;&#1610;&#1577; &#1605;&#1593;&#1575;&#1604;&#1580;&#1577;
&#1578;&#1578;&#1593;&#1604;&#1602;
&#1576;&#1575;&#1604;&#1587;&#1605;&#1606;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1576;&#1583;&#1575;&#1606;&#1577;&#1548; &#1593;&#1583;&#1575;
&#1575;&#1604;&#1571;&#1583;&#1608;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1594;&#1591;&#1575;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>20.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1581;&#1575;&#1604;&#1575;&#1578;
&#1586;&#1585;&#1593; &#1575;&#1604;&#1571;&#1593;&#1590;&#1575;&#1569;
&#1575;&#1604;&#1605;&#1606;&#1602;&#1608;&#1604;&#1577;
&#1608;&#1575;&#1604;&#1606;&#1582;&#1575;&#1593;
&#1575;&#1604;&#1593;&#1592;&#1605;&#1610; &#1571;&#1608; &#1586;&#1585;&#1593;
&#1575;&#1604;&#1571;&#1593;&#1590;&#1575;&#1569;
&#1575;&#1604;&#1575;&#1589;&#1591;&#1606;&#1575;&#1593;&#1610;&#1577;
&#1575;&#1604;&#1576;&#1583;&#1610;&#1604;&#1577; &#1575;&#1604;&#1578;&#1610;
&#1578;&#1581;&#1604; &#1605;&#1581;&#1604; &#1571;&#1610;
&#1593;&#1590;&#1608; &#1576;&#1575;&#1604;&#1580;&#1587;&#1605;
&#1576;&#1588;&#1603;&#1604; &#1603;&#1604;&#1610; &#1571;&#1608;
&#1580;&#1586;&#1574;&#1610;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>21.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1571;&#1582;&#1591;&#1575;&#1569;
&#1575;&#1604;&#1588;&#1582;&#1589;&#1610;&#1577; &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577;
&#1605;&#1606; &#1602;&#1587;&#1605; &#1575;&#1604;&#1578;&#1593;&#1585;&#1610;&#1601;&#1575;&#1578;
&#1605;&#1606; &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>22.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1571;&#1591;&#1585;&#1575;&#1601;
&#1575;&#1604;&#1589;&#1606;&#1575;&#1593;&#1610;&#1577; &#1608;&#1575;&#1604;&#1571;&#1591;&#1585;&#1575;&#1601;
&#1575;&#1604;&#1605;&#1587;&#1575;&#1593;&#1583;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><s><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p></o:p></span></s></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>23.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1594;&#1610;&#1585;&#1575;&#1578;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1593;&#1610;&#1577; &#1604;&#1587;&#1606; &#1575;&#1604;&#1610;&#1571;&#1587;
&#1604;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;&#1575; &#1608;&#1610;&#1588;&#1605;&#1604;
&#1584;&#1604;&#1603; &#1578;&#1594;&#1610;&#1585;&#1575;&#1578; &#1575;&#1604;&#1591;&#1605;&#1579;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>24.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1576;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;&#1548; &#1604;&#1575;
&#1610;&#1594;&#1591;&#1610; &#1607;&#1584;&#1575; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1571;&#1610; &#1606;&#1601;&#1602;&#1575;&#1578; &#1593;&#1604;&#1575;&#1580;
&#1575;&#1604;&#1571;&#1605;&#1585;&#1575;&#1590;
&#1575;&#1604;&#1605;&#1586;&#1605;&#1606;&#1577;
&#1575;&#1604;&#1605;&#1587;&#1578;&#1602;&#1585;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1571;&#1605;&#1585;&#1575;&#1590;
&#1575;&#1604;&#1605;&#1608;&#1580;&#1608;&#1583;&#1577;
&#1605;&#1587;&#1576;&#1602;&#1575;&#1611;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>25.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1606;&#1601;&#1602;&#1575;&#1578;
&#1571;&#1610;</span><span dir=LTR></span><b style='mso-bidi-font-weight:normal'><span
lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1585;&#1590;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1573;&#1589;&#1575;&#1576;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1590;&#1593;&#1601;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1580;&#1587;&#1583;&#1610;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1593;&#1580;&#1586;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1576;&#1583;&#1606;&#1610;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1608;&#1575;&#1604;&#1606;&#1578;&#1575;&#1574;&#1580;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1605;&#1578;&#1585;&#1578;&#1576;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1593;&#1604;&#1610;&#1607;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1608;&#1575;&#1604;&#1578;&#1610;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1578;&#1581;&#1583;&#1579;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608; &#1578;&#1592;&#1607;&#1585;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1593;&#1585;&#1575;&#1590;&#1607;&#1575;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1602;&#1576;&#1604;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1578;&#1575;&#1585;&#1610;&#1582; &#1587;&#1585;&#1610;&#1575;&#1606;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1571;&#1608;
&#1576;&#1593;&#1583; &#1575;&#1606;&#1578;&#1607;&#1575;&#1569;
&#1578;&#1594;&#1591;&#1610;&#1577; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>26.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1593;&#1604;&#1575;&#1580;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1576;&#1608;&#1575;&#1587;&#1591;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1571;&#1593;&#1588;&#1575;&#1576;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1571;&#1583;&#1608;&#1610;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1591;&#1576;&#1610;&#1593;&#1610;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1608;&#1571;&#1610;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1591;&#1585;&#1602;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1582;&#1585;&#1609;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1604;&#1591;&#1576;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1576;&#1583;&#1610;&#1604;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>27.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1602;&#1575;&#1608;&#1605;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1607; &#1604;&#1604;&#1573;&#1582;&#1604;&#1575;&#1569;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1591;&#1576;&#1610;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1593;&#1608;&#1583;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1604;&#1608;&#1591;&#1606;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span>.<o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>28.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1606;&#1602;&#1604;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1608;&#1575;&#1604;&#1593;&#1604;&#1575;&#1580;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1584;&#1610;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1604;&#1605;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1578;&#1605;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1578;&#1585;&#1578;&#1610;&#1576;&#1607;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1587;&#1576;&#1602;&#1575;&#1611;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1605;&#1593;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;<span
style='color:red'>.</span></span><b style='mso-bidi-font-weight:normal'><s><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:red'><o:p></o:p></span></s></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>29.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1593;&#1605;&#1604;&#1610;&#1575;&#1578;
&#1575;&#1604;&#1573;&#1580;&#1607;&#1575;&#1590; &#1594;&#1610;&#1585; &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;&#1610;
(&#1608;&#1601;&#1602;&#1575;&#1611;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;)&#1548;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608; &#1581;&#1575;&#1604;&#1575;&#1578; &#1575;&#1604;&#1581;&#1605;&#1604;
&#1608;&#1575;&#1604;&#1608;&#1604;&#1575;&#1583;&#1577; &#1571;&#1608; &#1575;&#1604;&#1573;&#1580;&#1607;&#1575;&#1590;
&#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;&#1610; &#1575;&#1604;&#1594;&#1610;&#1585;
&#1605;&#1601;&#1589;&#1581; &#1593;&#1606;&#1607;&#1575; &#1601;&#1610; &#1591;&#1604;&#1576;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>30.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1578;&#1593;&#1605;&#1583;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1575;&#1606;&#1578;&#1581;&#1575;&#1585; &#1571;&#1608; &#1573;&#1610;&#1584;&#1575;&#1569;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1606;&#1601;&#1587;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1576;&#1583;&#1606;&#1610;&#1575;&#1611;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1606;&#1601;&#1587;&#1610;&#1575;&#1611;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1588;&#1575;&#1585;&#1603;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1606;&#1588;&#1591;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1601;&#1610;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1610;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1593;&#1585;&#1590;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1606;&#1575;&#1601;&#1587;&#1575;&#1578;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1604;&#1604;&#1585;&#1610;&#1575;&#1590;&#1575;&#1578;
&#1575;&#1604;&#1582;&#1591;&#1585;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>31.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1602;&#1575;&#1608;&#1605;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1585;&#1601;&#1590;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1607;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1593;&#1583;&#1605;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1578;&#1586;&#1575;&#1605;&#1607;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1576;&#1575;&#1604;&#1578;&#1608;&#1580;&#1610;&#1607;&#1575;&#1578;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1591;&#1576;&#1610;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1602;&#1583;&#1605;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1576;&#1608;&#1575;&#1587;&#1591;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1591;&#1576;&#1610;&#1576;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1576;&#1608;&#1576;&#1575;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1608;&#1575;&#1604;&#1591;&#1576;&#1610;&#1576;
&#1575;&#1604;&#1605;&#1593;&#1575;&#1604;&#1580;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>32.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1610;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1593;&#1604;&#1575;&#1580;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1594;&#1610;&#1585;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1590;&#1585;&#1608;&#1585;&#1610;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1605;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1603;&#1606;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1590;&#1585;&#1608;&#1585;&#1610;&#1575;&#1611;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1608;&#1605;&#1585;&#1578;&#1576;&#1591;&#1575;&#1611;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1605;&#1576;&#1575;&#1588;&#1585;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1576;&#1575;&#1604;&#1581;&#1575;&#1604;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1594;&#1591;&#1575;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1608;&#1601;&#1602;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span>.<o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l9 level1 lfo45;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>33.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1606;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1578;&#1602;&#1608;&#1605;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1576;&#1608;&#1576;&#1575;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1576;&#1575;&#1604;&#1573;&#1582;&#1604;&#1575;&#1569;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1591;&#1576;&#1610;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1573;&#1593;&#1575;&#1583;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1604;&#1607;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1601;&#1610;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577;:</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.0pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l18 level1 lfo46;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1571;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1573;&#1584;&#1575;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1603;&#1575;&#1606; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607;</span><span dir=LTR></span><b style='mso-bidi-font-weight:normal'><span
lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1594;&#1610;&#1585;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1605;&#1582;&#1608;&#1604;&#1575;&#1611;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1591;&#1576;&#1610;&#1575;&#1611;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1604;&#1584;&#1604;&#1603;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l18 level1 lfo46;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1576;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1581;&#1605;&#1604; &#1575;&#1604;&#1578;&#1610;
&#1610;&#1579;&#1576;&#1578; &#1581;&#1583;&#1608;&#1579;&#1607;&#1575;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1604;&#1571;&#1603;&#1579;&#1585;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1606;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1587;&#1578;&#1577;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1588;&#1607;&#1585;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l18 level1 lfo46;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1578;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1573;&#1584;&#1575;
&#1603;&#1575;&#1606; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1610;&#1593;&#1575;&#1606;&#1610;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1606;
&#1575;&#1590;&#1591;&#1585;&#1575;&#1576;&#1575;&#1578;
&#1593;&#1602;&#1604;&#1610;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1593;&#1589;&#1576;&#1610;&#1577;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1575;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1604;&#1605;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1606;&#1608;&#1605;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1601;&#1610;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l18 level1 lfo46;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1579;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1573;&#1593;&#1575;&#1583;&#1577;
&#1585;&#1601;&#1575;&#1578; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1575;&#1604;&#1605;&#1578;&#1608;&#1601;&#1609;
&#1573;&#1604;&#1609; &#1576;&#1604;&#1583; &#1594;&#1610;&#1585;
&#1605;&#1608;&#1591;&#1606;&#1607; &#1575;&#1604;&#1571;&#1589;&#1604;&#1610;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l18 level1 lfo46;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1580;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1573;&#1584;&#1575;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1603;&#1575;&#1606; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607;</span><span dir=LTR></span><b style='mso-bidi-font-weight:normal'><span
lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1593;&#1575;&#1606;&#1610;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1605;&#1606;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1580;&#1585;&#1608;&#1581;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1591;&#1601;&#1610;&#1601;&#1577;&#1548;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1573;&#1589;&#1575;&#1576;&#1575;&#1578;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1576;&#1587;&#1610;&#1591;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1605;&#1579;&#1604;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1575;&#1604;&#1578;&#1608;&#1575;&#1569;&#1575;&#1578;&#1548;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1571;&#1608;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1603;&#1587;&#1608;&#1585;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1576;&#1587;&#1610;&#1591;&#1577;&#1548;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1605;&#1585;&#1590; &#1605;&#1593;&#1578;&#1583;&#1604;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1610;&#1605;&#1603;&#1606;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1593;&#1575;&#1604;&#1580;&#1578;&#1607;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1605;&#1606;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1602;&#1576;&#1604;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1571;&#1591;&#1576;&#1575;&#1569;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1581;&#1604;&#1610;&#1610;&#1606;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1608;&#1604;&#1575;</span><span dir=LTR></span><b style='mso-bidi-font-weight:
normal'><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span> </span></b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1605;&#1606;&#1593;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1604;&#1605;&#1588;&#1578;&#1585;&#1603;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1606;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1575;&#1587;&#1578;&#1605;&#1585;&#1575;&#1585;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1585;&#1581;&#1604;&#1577;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1587;&#1601;&#1585;&#1607;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;</span><span
dir=LTR></span><b style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><span dir=LTR></span> </span></b><span lang=AR-SA style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:
bold'>&#1573;&#1593;&#1575;&#1583;&#1578;&#1607;</span><span dir=LTR></span><b
style='mso-bidi-font-weight:normal'><span lang=AR-SA dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><span dir=LTR></span>
</span></b><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;mso-ansi-font-weight:bold'>&#1573;&#1604;&#1609;
&#1608;&#1591;&#1606;&#1607;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoNormal dir=RTL style='margin-top:6.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1604;&#1606; &#1578;&#1594;&#1591;&#1610;
&#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577; &#1608;&#1573;&#1593;&#1575;&#1583;&#1577;
&#1575;&#1604;&#1580;&#1579;&#1605;&#1575;&#1606; &#1573;&#1604;&#1609; &#1575;&#1604;&#1605;&#1608;&#1591;&#1606;
&#1575;&#1604;&#1571;&#1589;&#1604;&#1610; &#1601;&#1610; &#1581;&#1575;&#1604;&#1577;
&#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1575;&#1578; &#1575;&#1604;&#1606;&#1575;&#1588;&#1574;&#1577;
&#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1593;&#1606;: <o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l25 level1 lfo38;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1581;&#1585;&#1576;&#1548;
&#1575;&#1604;&#1594;&#1586;&#1608;&#1548; &#1571;&#1593;&#1605;&#1575;&#1604; &#1575;&#1604;&#1593;&#1583;&#1608;&#1575;&#1606;
&#1575;&#1604;&#1571;&#1580;&#1606;&#1576;&#1610;&#1548; &#1571;&#1593;&#1605;&#1575;&#1604;
&#1575;&#1604;&#1593;&#1583;&#1608;&#1575;&#1606; (&#1587;&#1608;&#1575;&#1569;
&#1571;&#1593;&#1604;&#1606;&#1578; &#1575;&#1604;&#1581;&#1585;&#1576; &#1571;&#1605;
&#1604;&#1605; &#1578;&#1593;&#1604;&#1606;).</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l25 level1 lfo38;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1573;&#1588;&#1593;&#1575;&#1593;&#1575;&#1578;
&#1575;&#1604;&#1571;&#1610;&#1608;&#1606;&#1610;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1578;&#1604;&#1608;&#1579;
&#1576;&#1575;&#1604;&#1606;&#1588;&#1575;&#1591;
&#1575;&#1604;&#1573;&#1588;&#1593;&#1575;&#1593;&#1610; &#1605;&#1606;
&#1571;&#1610; &#1608;&#1602;&#1608;&#1583; &#1606;&#1608;&#1608;&#1610; &#1571;&#1608;
&#1571;&#1610;&#1577; &#1606;&#1601;&#1575;&#1610;&#1575;&#1578; &#1606;&#1608;&#1608;&#1610;&#1577;
&#1606;&#1575;&#1578;&#1580;&#1577; &#1593;&#1606; &#1575;&#1581;&#1578;&#1585;&#1575;&#1602;
&#1608;&#1602;&#1608;&#1583; &#1606;&#1608;&#1608;&#1610;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l25 level1 lfo38;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1582;&#1589;&#1575;&#1574;&#1589;
&#1575;&#1604;&#1605;&#1588;&#1593;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1587;&#1575;&#1605;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1578;&#1601;&#1580;&#1585;&#1577; &#1571;&#1608;
&#1571;&#1610; &#1582;&#1589;&#1575;&#1574;&#1589; &#1582;&#1591;&#1585;&#1577;
&#1571;&#1582;&#1585;&#1609; &#1604;&#1571;&#1610;
&#1578;&#1580;&#1605;&#1593;&#1575;&#1578; &#1606;&#1608;&#1608;&#1610;&#1577; &#1571;&#1608;
&#1604;&#1571;&#1610; &#1605;&#1606;
&#1605;&#1585;&#1603;&#1576;&#1575;&#1578;&#1607;&#1575;
&#1575;&#1604;&#1606;&#1608;&#1608;&#1610;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l25 level1 lfo38;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1605;&#1586;&#1575;&#1608;&#1604;&#1577;
&#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1571;&#1608; &#1605;&#1588;&#1575;&#1585;&#1603;&#1578;&#1607;
&#1601;&#1610; &#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1602;&#1608;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1587;&#1604;&#1581;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1588;&#1585;&#1591;&#1577; &#1571;&#1608;
&#1593;&#1605;&#1604;&#1610;&#1575;&#1578;&#1607;&#1575;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l25 level1 lfo38;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1593;&#1605;&#1575;&#1604; &#1575;&#1604;&#1588;&#1594;&#1576;&#1548;
&#1575;&#1604;&#1573;&#1590;&#1585;&#1575;&#1576;&#1548; &#1575;&#1604;&#1573;&#1585;&#1607;&#1575;&#1576;&#1548;
&#1575;&#1604;&#1575;&#1593;&#1578;&#1583;&#1575;&#1569; &#1571;&#1608;
&#1605;&#1575; &#1610;&#1588;&#1575;&#1576;&#1607;&#1607;&#1575; &#1605;&#1606;
&#1571;&#1593;&#1605;&#1575;&#1604;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l25 level1 lfo38;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1581;&#1608;&#1575;&#1583;&#1579;
&#1571;&#1608; &#1575;&#1604;&#1578;&#1601;&#1575;&#1593;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1603;&#1610;&#1605;&#1575;&#1608;&#1610;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1576;&#1610;&#1608;&#1604;&#1608;&#1580;&#1610;&#1577;
&#1571;&#1608;
&#1575;&#1604;&#1576;&#1603;&#1578;&#1585;&#1610;&#1608;&#1604;&#1608;&#1580;&#1610;&#1577;&#1548;
&#1573;&#1584;&#1575; &#1603;&#1575;&#1606;&#1578; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1581;&#1608;&#1575;&#1583;&#1579; &#1571;&#1608;
&#1575;&#1604;&#1578;&#1601;&#1575;&#1593;&#1604;&#1575;&#1578;
&#1606;&#1575;&#1578;&#1580;&#1577; &#1593;&#1606;
&#1573;&#1589;&#1575;&#1576;&#1575;&#1578; &#1593;&#1605;&#1604; &#1571;&#1608;
&#1576;&#1587;&#1576;&#1576; &#1605;&#1582;&#1575;&#1591;&#1585;
&#1605;&#1607;&#1606;&#1610;&#1577;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:
embed'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
style='font-size:12.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=606
 style='width:454.5pt;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;direction:rtl;unicode-bidi:
  embed'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;mso-border-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;tab-stops:center 66.6pt;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
  &#1604;&#1607;</span><span lang=AR-SA style='font-size:10.0pt;line-height:
  115%;font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'><span
  style='mso-tab-count:1'>            </span></span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

</div>

<h1 dir=RTL style='margin-top:0in;margin-right:22.5pt;margin-bottom:10.0pt;
margin-left:0in;text-align:right;text-indent:-.25in;mso-list:l24 level1 lfo24;
direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=AR-SA
style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-font-family:
Arial;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>&#1581;&#8204;.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b><span lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:
Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'>&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
&#1575;&#1604;&#1593;&#1575;&#1605;&#1577;</span></b><b><span lang=EN-GB
dir=LTR style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><o:p></o:p></span></b></h1>

<p class=MsoListParagraphCxSpFirst dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1573;&#1579;&#1576;&#1575;&#1578;
&#1587;&#1585;&#1610;&#1575;&#1606;
&#1575;&#1604;&#1605;&#1601;&#1593;&#1608;&#1604;
(&#1587;&#1585;&#1610;&#1575;&#1606;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;)</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:6.0pt;margin-left:0in;mso-add-space:auto;text-align:right;
direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>&#1578;&#1605;&#1579;&#1604;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1575;&#1604;&#1581;&#1583;
&#1575;&#1604;&#1571;&#1587;&#1575;&#1587;&#1610; &#1605;&#1606;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1602;&#1583;&#1605;&#1577;
&#1604;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;&#1548;
&#1608;&#1578;&#1603;&#1608;&#1606; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1587;&#1575;&#1585;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1601;&#1593;&#1608;&#1604; &#1605;&#1606;
&#1578;&#1575;&#1585;&#1610;&#1582; &#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
&#1573;&#1604;&#1609; &#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1572;&#1607;&#1604;&#1608;&#1606;
&#1604;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>&#1580;&#1605;&#1610;&#1593;
&#1575;&#1604;&#1605;&#1578;&#1602;&#1583;&#1605;&#1610;&#1606;
&#1604;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1578;&#1571;&#1588;&#1610;&#1585;&#1577; &#1583;&#1582;&#1608;&#1604;
&#1573;&#1604;&#1609; &#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1576;&#1594;&#1585;&#1590; &#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577; –
&#1571;&#1608; &#1578;&#1605;&#1583;&#1610;&#1583;&#1607;&#1575; –
&#1571;&#1608; &#1576;&#1594;&#1585;&#1590;
&#1575;&#1604;&#1605;&#1585;&#1608;&#1585;
(&#1576;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569; &#1594;&#1585;&#1590;
&#1575;&#1604;&#1581;&#1580; &#1571;&#1608;
&#1575;&#1604;&#1593;&#1605;&#1585;&#1577;) &#1608;&#1603;&#1584;&#1604;&#1603;
&#1605;&#1585;&#1575;&#1601;&#1602;&#1610;&#1607;&#1605;
&#1608;&#1584;&#1604;&#1603; &#1581;&#1587;&#1576; &#1605;&#1575;
&#1606;&#1589; &#1593;&#1604;&#1610;&#1607; &#1602;&#1585;&#1575;&#1585;
&#1605;&#1580;&#1604;&#1587; &#1575;&#1604;&#1608;&#1586;&#1585;&#1575;&#1569;
&#1585;&#1602;&#1605; (180) &#1576;&#1578;&#1575;&#1585;&#1610;&#1582;
2/5/1435&#1607;&#1600;. </span><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1587;&#1583;&#1575;&#1583;
&#1575;&#1604;&#1602;&#1587;&#1591;
(&#1575;&#1604;&#1575;&#1588;&#1578;&#1585;&#1575;&#1603;)</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
3.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1610;&#1604;&#1578;&#1586;&#1605; &#1605;&#1602;&#1583;&#1605;
&#1575;&#1604;&#1591;&#1604;&#1576; &#1576;&#1587;&#1583;&#1575;&#1583;
&#1575;&#1588;&#1578;&#1585;&#1575;&#1603;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1575;&#1604;&#1605;&#1578;&#1601;&#1602; &#1593;&#1604;&#1610;&#1607;
&#1605;&#1593; &#1576;&#1608;&#1576;&#1575; &#1608;&#1584;&#1604;&#1603;
&#1593;&#1606;&#1583; &#1591;&#1604;&#1576;
&#1578;&#1571;&#1588;&#1610;&#1585;&#1577;
&#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577; &#1571;&#1608;
&#1591;&#1604;&#1576; &#1578;&#1605;&#1583;&#1610;&#1583; &#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577;.
&#1608;&#1610;&#1578;&#1605; &#1575;&#1581;&#1578;&#1587;&#1575;&#1576;
&#1602;&#1610;&#1605;&#1577; &#1575;&#1604;&#1602;&#1587;&#1591; &#1605;&#1606;
&#1602;&#1576;&#1604; &#1576;&#1608;&#1576;&#1575;.</span><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1606;&#1578;&#1607;&#1575;&#1569;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
&#1604;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>&#1578;&#1606;&#1578;&#1607;&#1610;
&#1578;&#1594;&#1591;&#1610;&#1577; &#1571;&#1610;
&#1575;&#1587;&#1578;&#1581;&#1602;&#1575;&#1602;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;
&#1578;&#1604;&#1602;&#1575;&#1574;&#1610;&#1575;&#1611; &#1601;&#1610;
&#1571;&#1610; &#1605;&#1606; &#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577;:<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l40 level1 lfo29;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1571;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1593;&#1606;&#1583;
&#1575;&#1606;&#1578;&#1607;&#1575;&#1569; &#1605;&#1583;&#1577;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1603;&#1605;&#1575;
&#1607;&#1610; &#1605;&#1581;&#1583;&#1583;&#1577; &#1601;&#1610;
&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l40 level1 lfo29;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1576;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1593;&#1606;&#1583;
&#1575;&#1587;&#1578;&#1606;&#1601;&#1575;&#1583; &#1575;&#1604;&#1581;&#1583;
&#1575;&#1604;&#1571;&#1602;&#1589;&#1609;
&#1604;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577; &#1575;&#1604;&#1584;&#1610;
&#1578;&#1606;&#1589; &#1593;&#1604;&#1610;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.<o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;mso-add-space:auto;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>&#1608;&#1610;&#1587;&#1578;&#1605;&#1585;
&#1571;&#1583;&#1575;&#1569; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1575;&#1604;&#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577; &#1604;&#1571;&#1610;
&#1593;&#1604;&#1577; &#1580;&#1575;&#1585;&#1610;&#1577;
&#1578;&#1578;&#1591;&#1604;&#1576;
&#1575;&#1587;&#1578;&#1605;&#1585;&#1575;&#1585;
&#1575;&#1604;&#1578;&#1606;&#1608;&#1610;&#1605;
&#1576;&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609; &#1604;&#1593;&#1604;&#1575;&#1580;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1577;
&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577; &#1576;&#1593;&#1583;
&#1578;&#1575;&#1585;&#1610;&#1582; &#1575;&#1606;&#1578;&#1607;&#1575;&#1569;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577; &#1608;&#1584;&#1604;&#1603;
&#1581;&#1578;&#1609; &#1575;&#1587;&#1578;&#1606;&#1601;&#1575;&#1583;
&#1581;&#1583; &#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577; &#1575;&#1604;&#1571;&#1602;&#1589;&#1609;
&#1604;&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.</span><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1578;&#1581;&#1602;&#1602;
&#1588;&#1585;&#1603;&#1577; &#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1605;&#1606; &#1581;&#1575;&#1604;&#1577;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l35 level1 lfo32;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1571;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1581;&#1602; &#1604;&#1576;&#1608;&#1576;&#1575;&#1548;
&#1608;&#1610;&#1580;&#1576; &#1571;&#1606; &#1578;&#1578;&#1575;&#1581;
&#1604;&#1607;&#1575; &#1575;&#1604;&#1601;&#1585;&#1589;&#1577; &#1605;&#1606;
&#1582;&#1604;&#1575;&#1604; &#1580;&#1607;&#1577; &#1591;&#1576;&#1610;&#1577;
&#1605;&#1593;&#1578;&#1605;&#1583;&#1577;&#1548; &#1601;&#1581;&#1589;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1575;&#1604;&#1584;&#1610; &#1602;&#1583;&#1605;&#1578;
&#1576;&#1588;&#1571;&#1606;&#1607; &#1605;&#1591;&#1575;&#1604;&#1576;&#1577;
&#1593;&#1606; &#1606;&#1601;&#1602;&#1575;&#1578;
&#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577;
&#1593;&#1604;&#1609; &#1581;&#1587;&#1575;&#1576;&#1607;&#1575;
&#1576;&#1581;&#1583; &#1571;&#1602;&#1589;&#1609; &#1605;&#1585;&#1578;&#1575;&#1606;&#1548;
&#1608;&#1584;&#1604;&#1603; &#1582;&#1604;&#1575;&#1604;
&#1601;&#1578;&#1585;&#1577; &#1578;&#1608;&#1575;&#1580;&#1583;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1601;&#1610;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l35 level1 lfo32;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1576;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1593;&#1604;&#1609;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1571;&#1606;
&#1610;&#1578;&#1593;&#1575;&#1608;&#1606; &#1608;&#1610;&#1587;&#1605;&#1581;
&#1576;&#1575;&#1604;&#1602;&#1610;&#1575;&#1605; &#1593;&#1604;&#1609;
&#1606;&#1601;&#1602;&#1577; &#1576;&#1608;&#1576;&#1575;
&#1576;&#1575;&#1604;&#1571;&#1593;&#1605;&#1575;&#1604;
&#1575;&#1604;&#1590;&#1585;&#1608;&#1585;&#1610;&#1577;
&#1575;&#1604;&#1578;&#1610; &#1578;&#1591;&#1604;&#1576;&#1607;&#1575; &#1576;&#1608;&#1576;&#1575;
&#1601;&#1610; &#1581;&#1583;&#1608;&#1583;
&#1575;&#1604;&#1605;&#1593;&#1602;&#1608;&#1604; &#1576;&#1602;&#1589;&#1583;
&#1578;&#1593;&#1586;&#1610;&#1586; &#1571;&#1610;&#1577;
&#1581;&#1602;&#1608;&#1602; &#1571;&#1608; &#1605;&#1591;&#1575;&#1604;&#1576;&#1575;&#1578;
&#1571;&#1608; &#1578;&#1593;&#1608;&#1610;&#1590;&#1575;&#1578;
&#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577; &#1605;&#1606;
&#1575;&#1604;&#1594;&#1610;&#1585;&#1548; &#1578;&#1579;&#1576;&#1578;
&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1578;&#1607;
&#1593;&#1606;&#1607;&#1575;. &#1608;&#1604;&#1575;
&#1610;&#1580;&#1608;&#1586; &#1604;&#1607;
&#1575;&#1604;&#1578;&#1606;&#1575;&#1586;&#1604; &#1593;&#1606;
&#1575;&#1604;&#1581;&#1602;&#1608;&#1602; &#1575;&#1604;&#1605;&#1575;&#1604;&#1610;&#1577;
&#1573;&#1604;&#1575; &#1576;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577; &#1576;&#1608;&#1576;&#1575;
&#1575;&#1604;&#1589;&#1585;&#1610;&#1581;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1590;&#1605;&#1606;&#1610;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpLast dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1585;&#1571;&#1610;
&#1575;&#1604;&#1591;&#1576;&#1610; &#1575;&#1604;&#1579;&#1575;&#1606;&#1610;</span></b><span
dir=LTR></span><span lang=AR-SA dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:red;mso-bidi-font-weight:bold'><span
dir=LTR></span> </span><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:red;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1578;&#1581;&#1578;&#1601;&#1592; &#1576;&#1608;&#1576;&#1575;
&#1575;&#1604;&#1581;&#1602; &#1601;&#1610; &#1578;&#1602;&#1583;&#1610;&#1605;
&#1571;&#1610; &#1581;&#1575;&#1604;&#1577; &#1591;&#1576;&#1610;&#1577;
&#1604;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1585;&#1571;&#1610; &#1591;&#1576;&#1610; &#1579;&#1575;&#1606;&#1613;
&#1608;&#1578;&#1578;&#1581;&#1605;&#1604; &#1576;&#1608;&#1576;&#1575;
&#1575;&#1604;&#1578;&#1603;&#1575;&#1604;&#1610;&#1601;
&#1575;&#1604;&#1605;&#1578;&#1603;&#1576;&#1583;&#1577; &#1605;&#1606;
&#1607;&#1584;&#1575; &#1575;&#1604;&#1591;&#1604;&#1576;.</span><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1593;&#1583;&#1605;
&#1575;&#1586;&#1583;&#1608;&#1575;&#1580;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1601;&#1610; &#1581;&#1575;&#1604;&#1577; &#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577;
&#1593;&#1606; &#1606;&#1601;&#1602;&#1575;&#1578; &#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1575;&#1587;&#1578;&#1593;&#1575;&#1590;&#1577; &#1602;&#1575;&#1576;&#1604;&#1577;
&#1604;&#1604;&#1571;&#1583;&#1575;&#1569; &#1604;&#1588;&#1582;&#1589; &#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;&#1548;
&#1608;&#1610;&#1603;&#1608;&#1606; &#1605;&#1594;&#1591;&#1609;
&#1571;&#1610;&#1590;&#1575;&#1611; &#1576;&#1575;&#1604;&#1606;&#1587;&#1576;&#1577;
&#1604;&#1578;&#1604;&#1603; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578; &#1576;&#1605;&#1608;&#1580;&#1576;
&#1571;&#1610; &#1582;&#1591;&#1577; &#1571;&#1608;
&#1576;&#1585;&#1606;&#1575;&#1605;&#1580; &#1571;&#1608; &#1578;&#1571;&#1605;&#1610;&#1606;
&#1570;&#1582;&#1585; &#1571;&#1608; &#1605;&#1575; &#1588;&#1575;&#1576;&#1607;
&#1584;&#1604;&#1603;&#1548; &#1601;&#1601;&#1610; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1577; &#1578;&#1603;&#1608;&#1606; &#1576;&#1608;&#1576;&#1575;
&#1605;&#1587;&#1572;&#1608;&#1604;&#1577; &#1593;&#1606; &#1578;&#1594;&#1591;&#1610;&#1577;
&#1578;&#1604;&#1603; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578; &#1608;&#1578;&#1581;&#1604;
&#1605;&#1581;&#1604; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1601;&#1610; &#1605;&#1591;&#1575;&#1604;&#1576;&#1577; &#1575;&#1604;&#1594;&#1610;&#1585;
&#1576;&#1583;&#1601;&#1593; &#1581;&#1589;&#1578;&#1607;&#1605; &#1575;&#1604;&#1606;&#1587;&#1576;&#1610;&#1577;
&#1605;&#1606; &#1578;&#1604;&#1603; &#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577;.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1571;&#1587;&#1575;&#1587;
&#1575;&#1604;&#1578;&#1602;&#1610;&#1610;&#1583;
&#1575;&#1604;&#1605;&#1576;&#1575;&#1588;&#1585; &#1593;&#1604;&#1609;
&#1581;&#1587;&#1575;&#1576; &#1575;&#1604;&#1588;&#1585;&#1603;&#1577;
&#1604;&#1583;&#1609; &#1588;&#1576;&#1603;&#1577;
&#1605;&#1602;&#1583;&#1605;&#1610; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l12 level1 lfo34;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1571;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1581;&#1602;
&#1604;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1578;&#1604;&#1602;&#1610;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577; &#1604;&#1583;&#1609;
&#1588;&#1576;&#1603;&#1577; &#1605;&#1602;&#1583;&#1605;&#1610;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1575;&#1604;&#1605;&#1578;&#1601;&#1602;
&#1593;&#1604;&#1610;&#1607;&#1575; &#1605;&#1593; &#1576;&#1608;&#1576;&#1575;
&#1608;&#1583;&#1608;&#1606; &#1571;&#1606; &#1610;&#1591;&#1604;&#1576;
&#1605;&#1606;&#1607; &#1578;&#1587;&#1583;&#1610;&#1583; &#1578;&#1604;&#1603;
&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l12 level1 lfo34;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1576;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1585;&#1587;&#1604;
&#1605;&#1602;&#1583;&#1605;&#1608; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1605;&#1615;&#1593;&#1610;&#1617;&#1614;&#1606;&#1610;&#1606;
&#1605;&#1606; &#1576;&#1608;&#1576;&#1575; &#1580;&#1605;&#1610;&#1593;
&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1578;&#1603;&#1576;&#1583;&#1577;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1582;&#1604;&#1575;&#1604; &#1601;&#1578;&#1585;&#1577; &#1604;&#1575;
&#1578;&#1578;&#1580;&#1575;&#1608;&#1586; &#1593;&#1606; 30
&#1610;&#1608;&#1605;&#1548; &#1608;&#1578;&#1602;&#1608;&#1605; &#1576;&#1608;&#1576;&#1575;
&#1576;&#1578;&#1602;&#1610;&#1610;&#1605; &#1578;&#1604;&#1603;
&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1608;&#1605;&#1593;&#1575;&#1604;&#1580;&#1578;&#1607;&#1575;
&#1608;&#1573;&#1588;&#1593;&#1575;&#1585;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1593;&#1606;&#1583;
&#1576;&#1604;&#1608;&#1594; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1581;&#1583; &#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
&#1575;&#1604;&#1571;&#1602;&#1589;&#1609;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l12 level1 lfo34;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1578;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1576;&#1608;&#1576;&#1575;
&#1575;&#1604;&#1581;&#1602; &#1601;&#1610; &#1581;&#1584;&#1601;
&#1571;&#1608; &#1575;&#1587;&#1578;&#1576;&#1583;&#1575;&#1604; &#1571;&#1610;
&#1605;&#1606; / &#1571;&#1608; &#1580;&#1605;&#1610;&#1593;
&#1605;&#1602;&#1583;&#1605;&#1610; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577; &#1575;&#1604;&#1605;&#1615;&#1593;&#1610;&#1617;&#1614;&#1606;&#1610;&#1606;
&#1604;&#1571;&#1594;&#1585;&#1575;&#1590; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1582;&#1604;&#1575;&#1604;
&#1605;&#1583;&#1577; &#1587;&#1585;&#1610;&#1575;&#1606;&#1607;&#1575;&#1548;
&#1588;&#1585;&#1610;&#1591;&#1577;
&#1575;&#1604;&#1578;&#1606;&#1587;&#1610;&#1602; &#1605;&#1593;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1608;&#1578;&#1593;&#1610;&#1610;&#1606; &#1576;&#1583;&#1610;&#1604;
&#1593;&#1606;&#1607;&#1605; &#1576;&#1606;&#1601;&#1587;
&#1575;&#1604;&#1605;&#1587;&#1578;&#1608;&#1609;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;
text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1571;&#1587;&#1575;&#1587;
&#1578;&#1593;&#1608;&#1610;&#1590; &#1575;&#1604;&#1576;&#1583;&#1604;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1578;&#1602;&#1608;&#1605; &#1576;&#1608;&#1576;&#1575;
&#1608;&#1601;&#1602;&#1575;&#1611; &#1604;&#1571;&#1581;&#1603;&#1575;&#1605;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1608;&#1588;&#1585;&#1608;&#1591;&#1607;&#1575;
&#1608;&#1578;&#1581;&#1583;&#1610;&#1583;&#1575;&#1578;&#1607;&#1575; &#1608;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569;&#1575;&#1578;&#1607;&#1575;
&#1576;&#1578;&#1593;&#1608;&#1610;&#1590;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1582;&#1604;&#1575;&#1604; &#1605;&#1583;&#1577; &#1604;&#1575;
&#1578;&#1586;&#1610;&#1583; &#1593;&#1606; 30 &#1610;&#1608;&#1605;
&#1593;&#1605;&#1604; &#1605;&#1606; &#1578;&#1575;&#1585;&#1610;&#1582;
&#1578;&#1602;&#1583;&#1610;&#1605;
&#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577; &#1581;&#1587;&#1576;
&#1575;&#1604;&#1571;&#1587;&#1593;&#1575;&#1585;
&#1575;&#1604;&#1587;&#1575;&#1574;&#1583;&#1577;
&#1608;&#1610;&#1606;&#1576;&#1594;&#1610; &#1593;&#1604;&#1609; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1578;&#1602;&#1583;&#1610;&#1605;
&#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577; &#1573;&#1604;&#1609; &#1576;&#1608;&#1576;&#1575;
&#1582;&#1604;&#1575;&#1604; &#1601;&#1578;&#1585;&#1577; &#1604;&#1575;
&#1578;&#1578;&#1580;&#1575;&#1608;&#1586; &#1593;&#1606; (60)
&#1610;&#1608;&#1605;&#1575;&#1611; &#1605;&#1606;
&#1578;&#1575;&#1585;&#1610;&#1582; &#1578;&#1603;&#1576;&#1583;
&#1578;&#1604;&#1603; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;&#1548;
&#1605;&#1593; &#1605;&#1585;&#1575;&#1593;&#1575;&#1577; &#1605;&#1575;
&#1610;&#1604;&#1610;:</span><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l42 level1 lfo35;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1571;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1587;&#1608;&#1601;
&#1610;&#1578;&#1605; &#1578;&#1587;&#1583;&#1610;&#1583;
&#1575;&#1604;&#1578;&#1593;&#1608;&#1610;&#1590; &#1576;&#1593;&#1583;
&#1605;&#1608;&#1575;&#1601;&#1602;&#1577; &#1576;&#1608;&#1576;&#1575;
&#1593;&#1604;&#1609; &#1603;&#1608;&#1606; &#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578;
&#1605;&#1594;&#1591;&#1575;&#1577;
&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1575;&#1611; &#1576;&#1593;&#1583;
&#1575;&#1587;&#1578;&#1603;&#1605;&#1575;&#1604;
&#1606;&#1605;&#1608;&#1584;&#1580; &#1591;&#1604;&#1576;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;
&#1608;&#1578;&#1587;&#1604;&#1610;&#1605;&#1607; &#1604;&#1576;&#1608;&#1576;&#1575;
&#1608;&#1605;&#1585;&#1601;&#1602; &#1605;&#1593;&#1607;
&#1575;&#1604;&#1601;&#1608;&#1575;&#1578;&#1610;&#1585;
&#1575;&#1604;&#1571;&#1589;&#1604;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1601;&#1589;&#1604;&#1577;
&#1576;&#1575;&#1604;&#1573;&#1590;&#1575;&#1601;&#1577; &#1573;&#1604;&#1609;
&#1571;&#1610; &#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
&#1571;&#1582;&#1585;&#1609; &#1584;&#1575;&#1578; &#1589;&#1604;&#1577;
&#1605;&#1579;&#1604; &#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578; &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577;
&#1608;&#1578;&#1584;&#1575;&#1603;&#1585;
&#1575;&#1604;&#1591;&#1610;&#1585;&#1575;&#1606;
&#1608;&#1605;&#1587;&#1578;&#1606;&#1583;&#1575;&#1578;
&#1575;&#1604;&#1587;&#1601;&#1585;.</span><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l42 level1 lfo35;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1576;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1604;&#1606;
&#1610;&#1578;&#1580;&#1575;&#1608;&#1586; &#1605;&#1576;&#1604;&#1594;
&#1575;&#1604;&#1578;&#1593;&#1608;&#1610;&#1590; &#1578;&#1581;&#1578;
&#1571;&#1610; &#1592;&#1585;&#1601; &#1605;&#1606;
&#1575;&#1604;&#1592;&#1585;&#1608;&#1601; &#1581;&#1583;
&#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
&#1575;&#1604;&#1571;&#1602;&#1589;&#1609;.</span><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l42 level1 lfo35;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><s><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1578;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></s></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1578;&#1603;&#1608;&#1606;
&#1605;&#1576;&#1575;&#1604;&#1594;
&#1575;&#1604;&#1578;&#1593;&#1608;&#1610;&#1590;
&#1602;&#1575;&#1589;&#1585;&#1577; &#1593;&#1604;&#1609;
&#1575;&#1604;&#1606;&#1601;&#1602;&#1575;&#1578; &#1575;&#1604;&#1605;&#1593;&#1578;&#1575;&#1583;&#1577;
&#1608;&#1575;&#1604;&#1605;&#1571;&#1604;&#1608;&#1601;&#1577;
&#1608;&#1575;&#1604;&#1605;&#1602;&#1576;&#1608;&#1604;&#1577; &#1601;&#1610;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;.</span><b
style='mso-bidi-font-weight:normal'><s><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p></o:p></span></s></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l42 level1 lfo35;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1579;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1580;&#1576;
&#1573;&#1582;&#1591;&#1575;&#1585; &#1576;&#1608;&#1576;&#1575;
&#1601;&#1608;&#1585;&#1575;&#1611; &#1601;&#1610; &#1581;&#1575;&#1604;
&#1575;&#1604;&#1608;&#1601;&#1575;&#1577; &#1571;&#1608;
&#1575;&#1604;&#1583;&#1582;&#1608;&#1604;
&#1604;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1609; &#1571;&#1608;
&#1575;&#1604;&#1593;&#1608;&#1583;&#1577;
&#1575;&#1604;&#1591;&#1575;&#1585;&#1574;&#1577;
&#1604;&#1604;&#1608;&#1591;&#1606; &#1571;&#1608;
&#1575;&#1604;&#1573;&#1582;&#1604;&#1575;&#1569;
&#1575;&#1604;&#1591;&#1576;&#1610; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1585;&#1575;&#1601;&#1602;&#1577;&#1548;
&#1608;&#1610;&#1606;&#1576;&#1594;&#1610; &#1571;&#1606;
&#1610;&#1578;&#1590;&#1605;&#1606; &#1607;&#1584;&#1575;
&#1575;&#1604;&#1573;&#1582;&#1591;&#1575;&#1585; &#1593;&#1604;&#1609;
&#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
&#1575;&#1604;&#1591;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1605;&#1578;&#1593;&#1604;&#1602;&#1577;
&#1576;&#1575;&#1604;&#1605;&#1585;&#1590; &#1571;&#1608;
&#1575;&#1604;&#1573;&#1589;&#1575;&#1576;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l42 level1 lfo35;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1580;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1606;&#1576;&#1594;&#1610;
&#1571;&#1606; &#1610;&#1578;&#1605;
&#1575;&#1604;&#1573;&#1582;&#1591;&#1575;&#1585;
&#1576;&#1608;&#1575;&#1587;&#1591;&#1577;
&#1575;&#1604;&#1607;&#1575;&#1578;&#1601; &#1571;&#1608;
&#1575;&#1604;&#1576;&#1585;&#1610;&#1583;
&#1575;&#1604;&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;
&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1591;&#1608;&#1575;&#1585;&#1574; &#1604;&#1583;&#1609; &#1576;&#1608;&#1576;&#1575;
&#1575;&#1604;&#1578;&#1610; &#1578;&#1593;&#1605;&#1604; 24
&#1587;&#1575;&#1593;&#1577;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l42 level1 lfo35;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1581;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1606;&#1576;&#1594;&#1610;
&#1593;&#1604;&#1609; &#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1608;&#1571;&#1610;
&#1588;&#1582;&#1589; &#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606; &#1605;&#1593; &#1576;&#1608;&#1576;&#1575;
&#1608;&#1573;&#1582;&#1591;&#1575;&#1585;&#1607;&#1575;
&#1601;&#1608;&#1585;&#1575;&#1611; &#1576;&#1588;&#1571;&#1606; &#1571;&#1610;
&#1591;&#1604;&#1576; &#1589;&#1585;&#1601;
&#1578;&#1593;&#1608;&#1610;&#1590;&#1575;&#1578; &#1571;&#1608; &#1581;&#1602;
&#1575;&#1578;&#1582;&#1575;&#1584; &#1573;&#1580;&#1585;&#1575;&#1569; &#1590;&#1583;
&#1571;&#1610; &#1591;&#1585;&#1601; &#1570;&#1582;&#1585;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l42 level1 lfo35;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1582;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1593;&#1604;&#1575;&#1608;&#1577;
&#1593;&#1604;&#1609; &#1584;&#1604;&#1603;&#1548;
&#1610;&#1606;&#1576;&#1594;&#1610; &#1571;&#1606; &#1610;&#1593;&#1605;&#1604;
&#1581;&#1575;&#1605;&#1604; &#1608;&#1579;&#1610;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606; &#1593;&#1604;&#1609;
&#1573;&#1593;&#1604;&#1575;&#1605; &#1576;&#1608;&#1576;&#1575;
&#1608;&#1571;&#1606; &#1610;&#1578;&#1582;&#1584;
&#1575;&#1604;&#1573;&#1580;&#1585;&#1575;&#1569;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1602;&#1576;&#1608;&#1604;&#1577; &#1601;&#1610;
&#1581;&#1575;&#1604;&#1577; &#1578;&#1602;&#1583;&#1610;&#1605; &#1591;&#1604;&#1576;
&#1589;&#1585;&#1601; &#1578;&#1593;&#1608;&#1610;&#1590; &#1605;&#1606;
&#1591;&#1585;&#1601; &#1570;&#1582;&#1585; &#1608;&#1584;&#1604;&#1603;
&#1604;&#1581;&#1605;&#1575;&#1610;&#1577; &#1605;&#1589;&#1575;&#1604;&#1581; &#1576;&#1608;&#1576;&#1575;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;
text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1573;&#1604;&#1594;&#1575;&#1569;
&#1608;&#1575;&#1604;&#1575;&#1587;&#1578;&#1585;&#1583;&#1575;&#1583; </span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
3.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1604;&#1575; &#1610;&#1581;&#1602;
&#1604;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607;
&#1573;&#1604;&#1594;&#1575;&#1569; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1605;&#1578;&#1609; &#1605;&#1575; &#1576;&#1583;&#1569;
&#1587;&#1585;&#1610;&#1575;&#1606;&#1607;&#1575; &#1573;&#1604;&#1575;
&#1601;&#1610; &#1581;&#1575;&#1604;&#1577; &#1593;&#1583;&#1605;
&#1583;&#1582;&#1608;&#1604;&#1607; &#1573;&#1604;&#1609; &#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;&#1548;
&#1608;&#1610;&#1578;&#1605; &#1593;&#1604;&#1609; &#1607;&#1584;&#1575;
&#1575;&#1604;&#1571;&#1587;&#1575;&#1587; &#1573;&#1593;&#1575;&#1583;&#1577;
&#1602;&#1610;&#1605;&#1577; &#1575;&#1604;&#1602;&#1587;&#1591;. <s><o:p></o:p></s></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1575;&#1578;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;mso-add-space:auto;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>&#1610;&#1578;&#1605;
&#1575;&#1604;&#1585;&#1583; &#1593;&#1604;&#1609; &#1591;&#1604;&#1576;
&#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1575;&#1578; &#1605;&#1606;
&#1602;&#1576;&#1604; &#1576;&#1608;&#1576;&#1575; &#1573;&#1604;&#1609;
&#1605;&#1602;&#1583;&#1605;&#1610; &#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1593;&#1604;&#1609; &#1578;&#1602;&#1583;&#1610;&#1605;
&#1575;&#1604;&#1582;&#1583;&#1605;&#1577;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577; &#1604;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1582;&#1604;&#1575;&#1604; &#1605;&#1583;&#1577;
&#1604;&#1575; &#1578;&#1586;&#1610;&#1583; &#1593;&#1606; 60
&#1583;&#1602;&#1610;&#1602;&#1577; &#1605;&#1606; &#1608;&#1602;&#1578;
&#1591;&#1604;&#1576; &#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1589;&#1610;&#1594;&#1577; &#1575;&#1604;&#1580;&#1606;&#1587;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
3.0pt;margin-left:0in;mso-add-space:auto;text-align:right;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>&#1604;&#1571;&#1594;&#1585;&#1575;&#1590;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1601;&#1573;&#1606; &#1575;&#1604;&#1603;&#1604;&#1605;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1587;&#1578;&#1593;&#1605;&#1604;&#1577; &#1576;&#1589;&#1610;&#1594;&#1577;
&#1575;&#1604;&#1605;&#1584;&#1603;&#1585; &#1578;&#1593;&#1578;&#1576;&#1585;
&#1605;&#1606;&#1583;&#1585;&#1580;&#1577; &#1603;&#1584;&#1604;&#1603;
&#1593;&#1604;&#1609; &#1575;&#1604;&#1573;&#1606;&#1575;&#1579;.<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpFirst dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1573;&#1588;&#1593;&#1575;&#1585;&#1575;&#1578;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l23 level1 lfo40;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1571;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1580;&#1576; &#1571;&#1606;
&#1578;&#1603;&#1608;&#1606; &#1580;&#1605;&#1610;&#1593;
&#1575;&#1604;&#1573;&#1588;&#1593;&#1575;&#1585;&#1575;&#1578; &#1571;&#1608;
&#1575;&#1604;&#1605;&#1582;&#1575;&#1591;&#1576;&#1575;&#1578;
&#1576;&#1610;&#1606; &#1571;&#1591;&#1585;&#1575;&#1601;
&#1575;&#1604;&#1593;&#1604;&#1575;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577; &#1576;&#1589;&#1601;&#1577;
&#1585;&#1587;&#1605;&#1610;&#1577;.</span><b style='mso-bidi-font-weight:normal'><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l23 level1 lfo40;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1576;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1610;&#1580;&#1576;
&#1593;&#1604;&#1609; &#1576;&#1608;&#1576;&#1575;
&#1573;&#1588;&#1593;&#1575;&#1585; &#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1576;&#1578;&#1575;&#1585;&#1610;&#1582;
&#1575;&#1606;&#1578;&#1607;&#1575;&#1569; &#1608;&#1579;&#1610;&#1602;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:45.35pt;
margin-bottom:3.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l23 level1 lfo40;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Arial;mso-fareast-theme-font:
minor-bidi;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>&#1578;&#8204;.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
&#1604;&#1607; &#1605;&#1615;&#1604;&#1586;&#1605;
&#1576;&#1573;&#1588;&#1593;&#1575;&#1585; &#1576;&#1608;&#1576;&#1575;
&#1593;&#1606;&#1583; &#1578;&#1594;&#1610;&#1585; &#1571;&#1610;
&#1605;&#1606; &#1576;&#1610;&#1575;&#1606;&#1575;&#1578;
&#1575;&#1604;&#1575;&#1578;&#1589;&#1575;&#1604;
&#1575;&#1604;&#1582;&#1575;&#1589;&#1577; &#1576;&#1607;. </span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpLast dir=RTL style='margin-top:0in;margin-right:
.25in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:
auto;text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:
rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><b style='mso-ansi-font-weight:
normal'><span lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0'>&#1575;&#1604;&#1578;&#1593;&#1607;&#1583;
&#1576;&#1593;&#1583;&#1605; &#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577;
&#1604;&#1594;&#1585;&#1590; &#1575;&#1604;&#1593;&#1604;&#1575;&#1580;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1610;&#1578;&#1593;&#1607;&#1583;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1575;&#1604;&#1605;&#1583;&#1585;&#1580;
&#1601;&#1610; &#1580;&#1583;&#1608;&#1604; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1571;&#1608;
&#1603;&#1601;&#1610;&#1604;&#1607; &#1571;&#1608; &#1608;&#1604;&#1610;&#1607;
&#1576;&#1593;&#1583;&#1605; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1604;&#1578;&#1594;&#1591;&#1610;&#1577; &#1571;&#1610;
&#1593;&#1604;&#1575;&#1580; &#1605;&#1593;&#1604;&#1608;&#1605;
&#1604;&#1583;&#1610;&#1607; &#1571;&#1608; &#1605;&#1588;&#1582;&#1589;
&#1605;&#1587;&#1576;&#1602;&#1575;&#1611;
&#1608;&#1586;&#1610;&#1575;&#1585;&#1577;
&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577; &#1576;&#1607;&#1583;&#1601;
&#1575;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
&#1607;&#1584;&#1575; &#1575;&#1604;&#1593;&#1604;&#1575;&#1580; &#1605;&#1606;
&#1590;&#1605;&#1606; &#1576;&#1585;&#1606;&#1575;&#1605;&#1580;
&#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>15.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1578;&#1602;&#1610;&#1583;
&#1576;&#1571;&#1581;&#1603;&#1575;&#1605;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span></b><span lang=EN-GB
dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1573;&#1606;&#1607; &#1604;&#1605;&#1606;
&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
&#1575;&#1604;&#1587;&#1575;&#1576;&#1602;&#1577;
&#1604;&#1578;&#1581;&#1602;&#1602; &#1571;&#1610;
&#1575;&#1604;&#1578;&#1586;&#1575;&#1605; &#1593;&#1604;&#1609; &#1576;&#1608;&#1576;&#1575;
&#1571;&#1606; &#1610;&#1603;&#1608;&#1606;
&#1575;&#1604;&#1605;&#1572;&#1605;&#1606; &#1604;&#1607; &#1602;&#1583;
&#1606;&#1601;&#1584; &#1608;&#1578;&#1602;&#1610;&#1583;
&#1578;&#1605;&#1575;&#1605;&#1575;&#1611; &#1576;&#1580;&#1605;&#1610;&#1593;
&#1575;&#1604;&#1575;&#1588;&#1578;&#1585;&#1575;&#1591;&#1575;&#1578;
&#1608;&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
&#1608;&#1575;&#1604;&#1608;&#1575;&#1580;&#1576;&#1575;&#1578; &#1608;&#1575;&#1604;&#1575;&#1604;&#1578;&#1586;&#1575;&#1605;&#1575;&#1578;
&#1608;&#1575;&#1604;&#1578;&#1593;&#1607;&#1583;&#1575;&#1578; &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577;
&#1601;&#1610; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>16.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1580;&#1586;&#1575;&#1569;&#1575;&#1578;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1571;&#1610; &#1582;&#1604;&#1575;&#1601; &#1571;&#1608;
&#1606;&#1586;&#1575;&#1593; &#1610;&#1606;&#1588;&#1571; &#1571;&#1608;
&#1610;&#1578;&#1593;&#1604;&#1602; &#1576;&#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1610;&#1578;&#1605;
&#1575;&#1604;&#1601;&#1589;&#1604; &#1601;&#1610;&#1607;
&#1576;&#1605;&#1608;&#1580;&#1576; &#1571;&#1581;&#1603;&#1575;&#1605;
&#1575;&#1604;&#1606;&#1592;&#1575;&#1605; &#1608;&#1584;&#1604;&#1603;
&#1608;&#1601;&#1602;&#1575;&#1611; &#1604;&#1604;&#1605;&#1575;&#1583;&#1577;
(14) &#1605;&#1606; &#1575;&#1604;&#1606;&#1592;&#1575;&#1605;.<o:p></o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;mso-add-space:auto;
text-align:right;text-indent:-.25in;mso-list:l44 level1 lfo3;direction:rtl;
unicode-bidi:embed'><![if !supportLists]><b><span lang=EN-GB style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:
Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;color:#0070C0'><span
style='mso-list:Ignore'>17.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b style='mso-ansi-font-weight:normal'><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'>&#1575;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;
&#1593;&#1604;&#1609; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span></b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-bidi-font-weight:bold'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:.25in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1610;&#1578;&#1605; &#1573;&#1602;&#1585;&#1575;&#1585;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1575;&#1611;
&#1593;&#1576;&#1585; &#1575;&#1604;&#1580;&#1607;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1582;&#1608;&#1604;&#1577; &#1576;&#1584;&#1604;&#1603;.<o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-bottom:3.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><b><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal dir=RTL style='margin-bottom:3.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><b><span lang=AR-SA style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'>&#1601;&#1610; &#1581;&#1575;&#1604; &#1608;&#1580;&#1608;&#1583;
&#1571;&#1610; &#1575;&#1582;&#1578;&#1604;&#1575;&#1601;&#1575;&#1578; &#1601;&#1610;
&#1575;&#1604;&#1589;&#1610;&#1575;&#1594;&#1577; &#1571;&#1608; &#1575;&#1604;&#1605;&#1593;&#1575;&#1606;&#1610;
&#1576;&#1610;&#1606; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
&#1608;&#1575;&#1604;&#1573;&#1606;&#1580;&#1604;&#1610;&#1586;&#1610;&#1577;&#1548;
&#1601;&#1573;&#1606; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
&#1578;&#1603;&#1608;&#1606; &#1607;&#1610; &#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583;&#1577;.<o:p></o:p></span></b></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;tab-stops:200.2pt center 245.7pt;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-size:12.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'>&#1605;&#1583;&#1610;&#1585;
&#1582;&#1583;&#1605;&#1577; &#1575;&#1604;&#1593;&#1605;&#1604;&#1575;&#1569;<o:p></o:p></span></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;tab-stops:200.2pt center 245.7pt;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-size:12.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;<o:p></o:p></span></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;tab-stops:200.2pt center 245.7pt;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-size:12.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'>&#1589;&#1606;&#1583;&#1608;&#1602;
&#1576;&#1585;&#1610;&#1583; 23807<o:p></o:p></span></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;tab-stops:200.2pt center 245.7pt;direction:rtl;
unicode-bidi:embed'><span lang=AR-SA style='font-size:12.0pt;line-height:115%;
font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'>&#1580;&#1583;&#1577;
21436<o:p></o:p></span></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
style='font-size:12.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1605;&#1604;&#1603;&#1577;
&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;
&#1575;&#1604;&#1587;&#1593;&#1608;&#1583;&#1610;&#1577;<o:p></o:p></span></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
style='font-size:12.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ansi-font-weight:bold'>&#1571;&#1608;<o:p></o:p></span></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
style='font-size:12.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ansi-font-weight:bold'>&#1575;&#1578;&#1589;&#1604; &#1593;&#1604;&#1609;:<o:p></o:p></span></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
style='font-size:12.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ansi-font-weight:bold'>(0337 244 800)<o:p></o:p></span></p>

<p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;margin-bottom:
.0001pt;text-align:center;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
style='font-size:18.0pt;line-height:115%;font-family:"Arial","sans-serif";
mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=606
 style='width:454.5pt;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;direction:rtl;unicode-bidi:
  embed'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;mso-border-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;tab-stops:center 66.6pt;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
  &#1604;&#1607;</span><span lang=AR-SA style='font-size:10.0pt;line-height:
  115%;font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'><span
  style='mso-tab-count:1'>            </span></span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

</div>

<h1 dir=RTL style='margin-top:0in;margin-right:22.5pt;margin-bottom:10.0pt;
margin-left:0in;text-align:right;text-indent:-.25in;mso-list:l24 level1 lfo24;
direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span lang=AR-SA
style='font-size:16.0pt;mso-ascii-font-family:Arial;mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-font-family:
Arial;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;color:#0070C0'><span style='mso-list:Ignore'>&#1582;&#8204;.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span></b><![endif]><span
dir=RTL></span><b><span lang=AR-SA style='font-size:16.0pt;mso-ascii-font-family:
Arial;mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'>&#1573;&#1602;&#1585;&#1575;&#1585; &#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;</span></b><b><span
lang=EN-GB dir=LTR style='font-size:16.0pt;mso-ascii-font-family:Arial;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Arial;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><o:p></o:p></span></b></h1>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:3.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:
embed'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:6.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l4 level1 lfo43;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1602;&#1585; &#1571;&#1606; &#1575;&#1604;&#1576;&#1610;&#1575;&#1606;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1584;&#1603;&#1608;&#1585;&#1577; &#1601;&#1610; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1603;&#1575;&#1605;&#1604;&#1577;
&#1608;&#1589;&#1581;&#1610;&#1581;&#1577;.</span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:6.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l4 level1 lfo43;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1608;&#1575;&#1601;&#1602;
&#1593;&#1604;&#1609; &#1571;&#1606; &#1602;&#1576;&#1608;&#1604;
&#1591;&#1604;&#1576;&#1610; &#1587;&#1610;&#1578;&#1605; &#1593;&#1604;&#1609;
&#1571;&#1587;&#1575;&#1587; &#1607;&#1584;&#1607;
&#1575;&#1604;&#1576;&#1610;&#1575;&#1606;&#1575;&#1578; &#1608;&#1571;&#1606; &#1576;&#1608;&#1576;&#1575;
&#1610;&#1605;&#1603;&#1606;&#1607;&#1575;
&#1575;&#1604;&#1575;&#1578;&#1589;&#1575;&#1604;
&#1576;&#1575;&#1604;&#1605;&#1587;&#1578;&#1588;&#1601;&#1610;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1610; &#1571;&#1578;&#1593;&#1575;&#1605;&#1604; &#1605;&#1593;&#1607;&#1575;
&#1604;&#1578;&#1586;&#1608;&#1610;&#1583;&#1607;&#1575; &#1576;&#1571;&#1610; &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
&#1591;&#1576;&#1610;&#1577; &#1602;&#1583; &#1578;&#1581;&#1578;&#1575;&#1580;&#1607;&#1575;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:6.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l4 level1 lfo43;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1602;&#1585; &#1571;&#1606; &#1593;&#1583;&#1605;
&#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581; &#1604;&#1576;&#1608;&#1576;&#1575;
&#1593;&#1606; &#1581;&#1575;&#1604;&#1577; &#1591;&#1576;&#1610;&#1577; (&#1590;&#1605;&#1606;
&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1605;&#1583;&#1585;&#1580;&#1577; &#1601;&#1610;
&#1606;&#1605;&#1608;&#1584;&#1580;
&#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581; &#1575;&#1604;&#1591;&#1576;&#1610;)
&#1602;&#1583; &#1610;&#1572;&#1583;&#1610; &#1573;&#1604;&#1609;
&#1585;&#1601;&#1590; &#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1577; &#1576;&#1575;&#1604;&#1605;&#1606;&#1601;&#1593;&#1577;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606;&#1610;&#1577;&#1548; &#1608;&#1573;&#1604;&#1609;
&#1573;&#1604;&#1594;&#1575;&#1569; &#1575;&#1604;&#1578;&#1594;&#1591;&#1610;&#1577;.
&#1608;&#1578;&#1593;&#1583; &#1576;&#1610;&#1575;&#1606;&#1575;&#1578; &#1581;&#1575;&#1605;&#1604;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1608;&#1606;&#1605;&#1608;&#1584;&#1580;
&#1575;&#1604;&#1573;&#1601;&#1589;&#1575;&#1581; &#1576;&#1575;&#1604;&#1581;&#1575;&#1604;&#1575;&#1578;
&#1575;&#1604;&#1589;&#1581;&#1610;&#1577; &#1608;&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593;&#1548;
&#1580;&#1586;&#1569;&#1575;&#1611; &#1604;&#1575;
&#1610;&#1578;&#1580;&#1586;&#1571; &#1605;&#1606;
&#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;. </span><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";
mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:
Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:6.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l4 level1 lfo43;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1602;&#1585;&#1571;&#1578; &#1571;&#1606;&#1575;
&#1581;&#1575;&#1605;&#1604; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1571;&#1581;&#1603;&#1575;&#1605;
&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1605;&#1593;
&#1580;&#1583;&#1608;&#1604; &#1575;&#1604;&#1605;&#1606;&#1575;&#1601;&#1593; &#1575;&#1604;&#1582;&#1575;&#1589;&#1577;
&#1576;&#1607;&#1575; &#1608;&#1608;&#1575;&#1601;&#1602;&#1578; &#1593;&#1604;&#1610;&#1607;&#1575;.</span><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p></o:p></span></b></p>

<p class=MsoListParagraph dir=RTL style='margin-top:0in;margin-right:.25in;
margin-bottom:6.0pt;margin-left:0in;text-align:right;text-indent:-.25in;
mso-list:l4 level1 lfo43;direction:rtl;unicode-bidi:embed'><![if !supportLists]><b><span
lang=EN-GB style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:Arial;mso-fareast-theme-font:minor-bidi;mso-hansi-theme-font:
minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;
color:#0070C0'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span dir=RTL></span><span lang=AR-SA
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi;mso-ansi-font-weight:bold'>&#1571;&#1602;&#1585; &#1608;&#1571;&#1608;&#1575;&#1601;&#1602;
&#1571;&#1606;&#1575; &#1581;&#1575;&#1605;&#1604; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
&#1607;&#1606;&#1575; &#1576;&#1571;&#1606;&#1607; &#1601;&#1610;
&#1581;&#1575;&#1604;&#1577; &#1578;&#1571;&#1607;&#1604;&#1610;
&#1604;&#1571;&#1610; &#1601;&#1575;&#1574;&#1590; &#1605;&#1606;
&#1593;&#1605;&#1604;&#1610;&#1575;&#1578;
&#1575;&#1604;&#1578;&#1571;&#1605;&#1610;&#1606; &#1576;&#1593;&#1583;
&#1575;&#1606;&#1578;&#1607;&#1575;&#1569; &#1605;&#1583;&#1577; &#1608;&#1579;&#1610;&#1602;&#1578;&#1610;&#1548;
&#1576;&#1571;&#1606; &#1571;&#1587;&#1605;&#1581; &#1604;&#1576;&#1608;&#1576;&#1575;
&#1608;&#1571;&#1608;&#1603;&#1604;&#1607;&#1575; &#1576;&#1571;&#1606;
&#1578;&#1578;&#1576;&#1585;&#1593; &#1576;&#1607;&#1584;&#1575;
&#1575;&#1604;&#1601;&#1575;&#1574;&#1590; &#1604;&#1589;&#1575;&#1604;&#1581;
&#1575;&#1604;&#1580;&#1607;&#1575;&#1578; &#1575;&#1604;&#1582;&#1610;&#1585;&#1610;&#1577;.<o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-bottom:3.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><b style='mso-ansi-font-weight:normal'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal dir=RTL style='margin-bottom:3.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi;mso-bidi-font-weight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:6.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi'>&#1605;&#1606; &#1582;&#1604;&#1575;&#1604; &#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;
&#1571;&#1583;&#1606;&#1575;&#1607;&#1548; &#1610;&#1578;&#1601;&#1602; &#1575;&#1604;&#1591;&#1585;&#1601;&#1575;&#1606;
&#1593;&#1604;&#1609; &#1571;&#1606; &#1607;&#1584;&#1575; &#1575;&#1604;&#1593;&#1602;&#1583;
&#1607;&#1608; &#1593;&#1602;&#1583; &#1603;&#1575;&#1605;&#1604; &#1608;&#1605;&#1608;&#1575;&#1601;&#1602;
&#1593;&#1604;&#1609; &#1580;&#1605;&#1610;&#1593; &#1571;&#1581;&#1603;&#1575;&#1605;
&#1608;&#1588;&#1585;&#1608;&#1591; &#1607;&#1584;&#1575; &#1575;&#1604;&#1593;&#1602;&#1583;&#1548;
&#1576;&#1605;&#1575; &#1601;&#1610; &#1584;&#1604;&#1603; &#1580;&#1605;&#1610;&#1593;
&#1605;&#1585;&#1575;&#1601;&#1602;&#1607;.</span><span lang=EN-GB dir=LTR
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-top:6.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><b><span
lang=EN-GB dir=LTR style='font-family:"Arial","sans-serif";mso-ascii-theme-font:
minor-bidi;mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;
mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal dir=RTL style='margin-top:6.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:right;direction:rtl;unicode-bidi:embed'><span
lang=AR-SA style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'>&#1607;&#1584;&#1575; &#1575;&#1604;&#1593;&#1602;&#1583;
&#1582;&#1575;&#1590;&#1593;
&#1604;&#1604;&#1602;&#1608;&#1575;&#1606;&#1610;&#1606; &#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605;
&#1608;&#1575;&#1604;&#1571;&#1606;&#1592;&#1605;&#1577; &#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583;&#1577;
&#1605;&#1606; &#1602;&#1576;&#1604; &#1605;&#1580;&#1604;&#1587; &#1575;&#1604;&#1590;&#1605;&#1575;&#1606;
&#1575;&#1604;&#1589;&#1581;&#1610; &#1575;&#1604;&#1578;&#1593;&#1575;&#1608;&#1606;&#1610;.<o:p></o:p></span></p>

<p class=MsoNormal dir=RTL style='margin-bottom:3.0pt;text-align:right;
direction:rtl;unicode-bidi:embed'><span lang=EN-GB dir=LTR style='font-family:
"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;mso-hansi-theme-font:minor-bidi;
mso-bidi-font-family:Arial;mso-bidi-theme-font:minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=AR-SA dir=RTL
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=AR-SA dir=RTL
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=AR-SA dir=RTL
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=AR-SA dir=RTL
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=AR-SA dir=RTL
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=AR-SA dir=RTL
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=AR-SA dir=RTL
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=AR-SA dir=RTL
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:3.0pt'><span lang=EN-GB
style='font-family:"Arial","sans-serif";mso-ascii-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:Arial;mso-bidi-theme-font:
minor-bidi'><o:p>&nbsp;</o:p></span></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=606
 style='width:454.5pt;border-collapse:collapse;mso-yfti-tbllook:160;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;direction:rtl;unicode-bidi:
  embed'><b style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1578;&#1608;&#1602;&#1610;&#1593;</span><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td width=222 valign=top style='width:166.5pt;border:none;mso-border-top-alt:
  solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1576;&#1608;&#1576;&#1575;
  &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif";background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=78 valign=top style='width:58.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right dir=RTL style='margin-bottom:0in;margin-bottom:
  .0001pt;text-align:left;direction:rtl;unicode-bidi:embed'><b
  style='mso-bidi-font-weight:normal'><span lang=EN-GB dir=LTR
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  background:yellow;mso-highlight:yellow'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=228 valign=top style='width:171.0pt;border:none;mso-border-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:justify;text-justify:inter-ideograph;tab-stops:center 66.6pt;
  direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:10.0pt;
  line-height:115%;font-family:"Arial","sans-serif";background:yellow;
  mso-highlight:yellow;mso-ansi-font-weight:bold'>&#1575;&#1604;&#1605;&#1572;&#1605;&#1606;
  &#1604;&#1607;</span><span lang=AR-SA style='font-size:10.0pt;line-height:
  115%;font-family:"Arial","sans-serif";mso-ansi-font-weight:bold'><span
  style='mso-tab-count:1'>            </span></span><b style='mso-bidi-font-weight:
  normal'><span lang=EN-GB dir=LTR style='font-size:10.0pt;line-height:115%;
  font-family:"Arial","sans-serif"'><o:p></o:p></span></b></p>
  </td>
  <td width=18 valign=top style='width:13.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=42 valign=top style='width:31.5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal dir=RTL style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;direction:rtl;unicode-bidi:embed'><span lang=AR-SA
  style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif";
  mso-ansi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-GB><o:p>&nbsp;</o:p></span></p>

</div>

                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                          </div>
                    </div>
                
            </td>
        </tr>
    </table>
</asp:Content>

