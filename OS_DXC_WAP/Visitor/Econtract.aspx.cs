﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp;
//using WebSupergoo.ABCpdf9;


public partial class Visitor_Econtract : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        string ContractNo = string.Empty;
        string strDecryptedURL = string.Empty;
        try
        {
            

            Thread thFileDelete = new Thread(new ThreadStart(DeleteTempFiles));
            thFileDelete.Start();

            strDecryptedURL = ValidateURL(Request.Url.ToString());

            var uri = new Uri(strDecryptedURL);
            ContractNo = HttpUtility.ParseQueryString(uri.Query).Get("PID");


            //ContractNo = "70029144";
            //strDecryptedURL = "Lang=A";


            //    OS_DXC_WAP.CaesarWS.serv ws;
            OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
            OS_DXC_WAP.CaesarWS.ReqGetInboundTravelRequest_DN request;
            OS_DXC_WAP.CaesarWS.ReqGetInboundTravelResponse_DN response;



            request = new OS_DXC_WAP.CaesarWS.ReqGetInboundTravelRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            response = new OS_DXC_WAP.CaesarWS.ReqGetInboundTravelResponse_DN();
            //InboundDependentDetail_DN det = new InboundDependentDetail_DN();
            //InboundDependentDetail_DN[] detailArray;
            //detailArray = new InboundDependentDetail_DN[4];


            request.contractNo = ContractNo;
            //request.Username = WebPublication.CaesarSvcUsername;
            //request.Password = WebPublication.CaesarSvcPassword;
           


            response = ws.GetInboundTravel(request);


            // Response.Write(response.passportNo);

            //return;

            //for (int i = 0; i < detailArray.Length; i++)
            //{
            //    det = new InboundDependentDetail_DN();
            //    det.DOB = DateTime.Now;
            //    det.Gender = "Male";
            //    det.MembershipNo = "dependent Mem Num " + i.ToString();
            //    det.Name = "Dep Name " + i.ToString();
            //    det.Relation = "Relation " + i.ToString();
            //    detailArray[i] = det;
            //}
            //response.detail = detailArray;

            //response = ws.GetInboundTravel(request);
            // Response.Write("response.status: " + response.status);
            if (response.status == 0)
            {

                if (strDecryptedURL.Contains("Lang=A"))
                {
                    CreateAgreementUsingABCPDF(ContractNo, "Arabic", response);
                }
                if (strDecryptedURL.Contains("Lang=E"))
                {
                    CreateAgreementUsingABCPDF(ContractNo, "English", response);
                }


                //if (Request["lang"] != null && Request["lang"] == "ar")
                //    CreateAgreementUsingABCPDF(ContractNo, "Arabic", response);
                //else
                //    CreateAgreementUsingABCPDF(ContractNo, "English", response);
            }


        }
        catch (Exception ex)
        {
            divContract.Visible = false;
            if (ex.Message.Contains("Base-64"))
            {
                lblError.Text = "Invalid URL";
            }
            else
            {
			 lblError.Text = "We are unable to process your request.Please try later   31" + ex.Message + ex.InnerException;
                /*lblError.Text = "We are unable to process your request.Please try later" ;*/
               // Response.Write("Error: " + ex.Message + ",stackalloc: " + ex.StackTrace);
                CommonClass.LogError("PolicyDocument", ex.Message, ex.InnerException, ex.StackTrace, "");
            }
        }
    }

    protected string ValidateURL(string strURL)
    {
        string DecryptedURL = "";
        string key = "BupaARABIAcchiVISITORproductOSCONTRACTengarKEY2015";

        string url = strURL;

        string prefix_url = url.Substring(0, url.IndexOf("?") + 1);

        string suffix_url = url.Substring(url.IndexOf("?") + 1);



        string toDecryptBase64 = suffix_url;

        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
        System.Text.Decoder utf8Decode = encoder.GetDecoder();




        byte[] toDecryptArray = Convert.FromBase64String(toDecryptBase64);




        byte[] keyArray;

        System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();

        MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
        keyArray = hashmd5.ComputeHash(UTF32Encoding.UTF8.GetBytes(key));

        hashmd5.Clear();



        TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

        tdes.Key = keyArray;
        tdes.Mode = CipherMode.ECB;
        tdes.Padding = PaddingMode.PKCS7;

        ICryptoTransform cTransform = tdes.CreateDecryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(toDecryptArray, 0, toDecryptArray.Length);
        tdes.Clear();



        DecryptedURL = prefix_url + UTF8Encoding.UTF8.GetString(resultArray);

        return DecryptedURL;

    }

    private bool CreateAgreementUsingABCPDF(string contractNo, string FileType, OS_DXC_WAP.CaesarWS.ReqGetInboundTravelResponse_DN ContractDetails)
    {
        bool IsAgreementCreated = false;
        bool CreateHTMLFile = true;
        bool CreateHeader = true;
        string strRandomNumber = DateTime.Now.ToString("hhssddMMyyyy");
        try
        {
            //string PDFlocation = "C:\\htmlimport.pdf";
            // PDFlocation = PDFlocation.Replace('\'', ' ');
            string PDFFileName = contractNo + strRandomNumber.ToString() + ".pdf";
            string Applicationpath = HttpContext.Current.Request.PhysicalApplicationPath;
            string HTMLStaticFileName = "";
            if (FileType == "English")
            {
                HTMLStaticFileName = "Policy English New.html";
            }
            else
            {
                HTMLStaticFileName = "Policy Arabic New.html";
            }
            string HTMLFilePath = Applicationpath + "Visitor\\" + HTMLStaticFileName;
            string HTMLFileName = contractNo + strRandomNumber.ToString() + ".htm";
            string HTMLFileCreationPath = Applicationpath + "\\Visitor\\" + HTMLFileName;
            string PDFFileSavePath = Applicationpath + "\\Visitor\\" + PDFFileName;
            string HTMLReadURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Visitor/" + HTMLFileName;
            string PDFReadURL = HTMLReadURL.Substring(0, HTMLReadURL.Length - 3) + "pdf";


            StringBuilder BodyText = new StringBuilder();
            using (var sr = new StreamReader(HTMLFilePath))
            {
                BodyText.Append(sr.ReadToEnd());
            }

            #region UpdateParameters

            BodyText.Replace(" <!--PrintDate-->", DateTime.Now.ToString("dd/MM/yyyy"));

            BodyText.Replace(" <!--PolicyPurchaseDate-->", Convert.ToDateTime(ContractDetails.purchaseDate).ToString("dd MMM yyyy"));
            BodyText.Replace("<!--PolicyDuration-->", ContractDetails.policyDuration.ToString());
            BodyText.Replace("<!--PolicyNumber-->", contractNo);
            BodyText.Replace("<!--PassportNumber-->", ContractDetails.passportNo.ToString());

            if (ContractDetails.startDateCover != null)
            {
                BodyText.Replace("<!--PolicyCoverStartDate-->", Convert.ToDateTime(ContractDetails.startDateCover).ToString("dd MMM yyyy"));
                BodyText.Replace("<!--PolicyDate-->", Convert.ToDateTime(ContractDetails.startDateCover).ToString("dd MMM yyyy"));
            }
            else
            {
                BodyText.Replace("<!--PolicyCoverStartDate-->", string.Empty);
                BodyText.Replace("<!--PolicyDate-->", string.Empty);
            }

            if (ContractDetails.endDateCover != null)
                BodyText.Replace("<!--PolicyCoverEndDate-->", Convert.ToDateTime(ContractDetails.endDateCover).ToString("dd MMM yyyy"));
            else
                BodyText.Replace("<!--PolicyCoverEndDate-->", string.Empty);
            BodyText.Replace("<!--PolicyPremiumAmount-->", ContractDetails.policyPremium.ToString());
            BodyText.Replace("<!--MainPolicyHolderName-->", ContractDetails.policyholderName.ToString());
            BodyText.Replace("<!--MainPolicyHolderGender-->", ContractDetails.policyholderGender.ToString());
            BodyText.Replace("<!--MainPolicyHolderDOB-->", Convert.ToDateTime(ContractDetails.policyholderDOB).ToString("dd MMM yyyy"));
            BodyText.Replace("<!--MainPolicyHolderNationality-->", ContractDetails.policyholderNationality.ToString());
            BodyText.Replace("<!--MainPolicyHolderPostalAddress-->", ContractDetails.policyholderPostalAddress != null ? ContractDetails.policyholderPostalAddress.ToString() : "");
            BodyText.Replace("<!--MainPolicyHolderCity-->", ContractDetails.policyholderCity.ToString());
            BodyText.Replace("<!--MainPolicyHolderZipCode-->", ContractDetails.policyholderZipCode.ToString());
            BodyText.Replace("<!--MainPolicyHolderPOBox-->", ContractDetails.policyholderPOBox.ToString());
            BodyText.Replace("<!--MainPolicyHolderMobileNumber-->", ContractDetails.policyholderMobileNo.ToString());
            BodyText.Replace("<!--MainPolicyHolderEmailAddress-->", ContractDetails.policyholderEmail.ToString());            
            BodyText.Replace("<!--NetworkProvider-->", ContractDetails.networkType.Trim());
            BodyText.Replace("<!--PolicyHolderMembershipNo-->", ContractDetails.policyholderMembershipNo);
            BodyText.Replace("<!--MainPolicyHolderMaritalStatus-->", ContractDetails.MaritalStatus);  
            BodyText.Replace("<!--ENumber-->", ContractDetails.ENumber);
            BodyText.Replace("<!--VisaNumber-->", ContractDetails.VisaNo);
            BodyText.Replace("<!--VisaValidity-->", ContractDetails.VisaValidity.ToString());
            BodyText.Replace("<!--VisaType-->", ContractDetails.VisaType);
            BodyText.Replace("<!--VisitType-->", ContractDetails.VisitType);
            BodyText.Replace("<!--MainPolicyHolderBenType-->", ContractDetails.BeneficiaryType);
            #endregion

            #region Update Dependant Details
            if (ContractDetails.detail.Length > 0)
            {

                BodyText.Replace("#DependedTable{display: none;}", "");
                for (int i = 0; i < ContractDetails.detail.Length; i++)
                {
                    BodyText.Replace("<!--DepName" + (i + 1).ToString() + "-->", ContractDetails.detail[i].Name);
                    BodyText.Replace("<!--DepNationality" + (i + 1).ToString() + "-->", ContractDetails.policyholderNationality);
                    BodyText.Replace("<!--DepMaritalStatus" + (i + 1).ToString() + "-->", ContractDetails.detail[i].MaritalStatus);
                    BodyText.Replace("<!--DepGender" + (i + 1).ToString() + "-->", ContractDetails.detail[i].Gender);
                    BodyText.Replace("<!--DepBenType" + (i + 1).ToString() + "-->", ContractDetails.detail[i].BeneficiaryType);
                    BodyText.Replace("<!--DepRel" + (i + 1).ToString() + "-->", ContractDetails.detail[i].Relation);

                    BodyText.Replace("<!--DepMemNo" + (i + 1).ToString() + "-->", ContractDetails.detail[i].MembershipNo);
                    if (ContractDetails.detail[i].DOB != null)
                        BodyText.Replace("<!--DepDOB" + (i + 1).ToString() + "-->", ContractDetails.detail[i].DOB.Value.ToString("dd MM yyyy"));
                }

            }
            //if (FileType == "English")
            //{
            //    #region update dependants
            //    if (ContractDetails.detail.Length > 0)
            //    {
            //        StringBuilder sbDependants = new StringBuilder();
            //        sbDependants.Append(" <tr>");
            //        sbDependants.Append("  <td colspan='2' style='background-color:black;color:white;text-align:center;font-weight:bold;'>");
            //        sbDependants.Append("   Dependent Policyholder Information  ");
            //        sbDependants.Append("  </td>");
            //        sbDependants.Append("  </tr>");
            //        if (ContractDetails.detail.Length <= 2)
            //        {
            //            for (int z = 0; z < ContractDetails.detail.Length; z++)
            //            {
            //                sbDependants.Append("<tr>");
            //                sbDependants.Append(" <td>Name:</td>");
            //                sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].Name + "</td>");
            //                sbDependants.Append(" </tr>");

            //                sbDependants.Append("<tr>");
            //                sbDependants.Append(" <td>Membership Number:</td>");
            //                sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
            //                sbDependants.Append(" </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("  <td>Gender:</td>");
            //                sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
            //                sbDependants.Append("   </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("     <td>Relation:</td>");
            //                sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
            //                sbDependants.Append("  </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("     <td>Date of Birth:</td>");
            //                sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
            //                sbDependants.Append(" </tr>");
            //            }

            //            BodyText.Replace("<!--DependantDetails-->", sbDependants.ToString());
            //        }
            //        else
            //        {
            //            for (int z = 0; z < 2; z++)
            //            {
            //                sbDependants.Append("<tr>");
            //                sbDependants.Append(" <td>Name:</td>");
            //                sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].Name + "</td>");
            //                sbDependants.Append(" </tr>");

            //                sbDependants.Append("<tr>");
            //                sbDependants.Append(" <td>Membership Number:</td>");
            //                sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
            //                sbDependants.Append(" </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("  <td>Gender:</td>");
            //                sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
            //                sbDependants.Append("   </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("     <td>Relation:</td>");
            //                sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
            //                sbDependants.Append("  </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("     <td>Date of Birth:</td>");
            //                sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
            //                sbDependants.Append(" </tr>");
            //            }
            //            BodyText.Replace("<!--DependantDetails-->", sbDependants.ToString());

            //            sbDependants = new StringBuilder();
            //            // sbDependants.Append("<div style='page-break-after:always'>&nbsp;</div>");
            //            sbDependants.Append("<table width='0' class='tableStyle'><br />");

            //            for (int z = 2; z < ContractDetails.detail.Length; z++)
            //            {
            //                sbDependants.Append("<tr>");
            //                sbDependants.Append(" <td  width='350px'>Name:</td>");
            //                sbDependants.Append("<td style='text-align:center;'  width='350px'>" + ContractDetails.detail[z].Name + "</td>");
            //                sbDependants.Append(" </tr>");

            //                sbDependants.Append("<tr>");
            //                sbDependants.Append(" <td>Membership Number:</td>");
            //                sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
            //                sbDependants.Append(" </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("  <td>Gender:</td>");
            //                sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
            //                sbDependants.Append("   </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("     <td>Relation:</td>");
            //                sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
            //                sbDependants.Append("  </tr>");

            //                sbDependants.Append(" <tr>");
            //                sbDependants.Append("     <td>Date of Birth:</td>");
            //                sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
            //                sbDependants.Append(" </tr>");
            //            }
            //            sbDependants.Append("</table> <br /><br /><br />");
            //            BodyText.Replace("<!--DependantDetailsAdditional-->", sbDependants.ToString());
            //        }
            //    }


            //    #endregion
            //}
            //else
            //{
            //    try
            //    {

            //        #region update dependants
            //        if (ContractDetails.detail.Length > 0)
            //        {
            //            StringBuilder sbDependants = new StringBuilder();
            //            sbDependants.Append(" <tr>");
            //            sbDependants.Append("  <td colspan='2' style='background-color:black;color:white;text-align:center;font-weight:bold;'>");
            //            sbDependants.Append("   معلومات المعال ");
            //            sbDependants.Append("  </td>");
            //            sbDependants.Append("  </tr>");
            //            if (ContractDetails.detail.Length <= 2)
            //            {
            //                for (int z = 0; z < ContractDetails.detail.Length; z++)
            //                {
            //                    sbDependants.Append("<tr>");
            //                    sbDependants.Append(" <td>الاسم الكامل:</td>");
            //                    sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].Name + "</td>");
            //                    sbDependants.Append(" </tr>");

            //                    sbDependants.Append("<tr>");
            //                    sbDependants.Append(" <td>رقم العضوية:</td>");
            //                    sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
            //                    sbDependants.Append(" </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("  <td>الجنس:</td>");
            //                    sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
            //                    sbDependants.Append("   </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("     <td>صلة قرابة:</td>");
            //                    sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
            //                    sbDependants.Append("  </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("     <td>تاريخ الميلاد:</td>");
            //                    sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
            //                    sbDependants.Append(" </tr>");
            //                }

            //                BodyText.Replace("<!--DependantDetails-->", sbDependants.ToString());

            //            }
            //            else
            //            {
            //                for (int z = 0; z < 2; z++)
            //                {
            //                    sbDependants.Append("<tr>");
            //                    sbDependants.Append(" <td>الاسم الكامل:</td>");
            //                    sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].Name + "</td>");
            //                    sbDependants.Append(" </tr>");

            //                    sbDependants.Append("<tr>");
            //                    sbDependants.Append(" <td>رقم العضوية:</td>");
            //                    sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
            //                    sbDependants.Append(" </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("  <td>الجنس:</td>");
            //                    sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
            //                    sbDependants.Append("   </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("     <td>صلة قرابة:</td>");
            //                    sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
            //                    sbDependants.Append("  </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("     <td>تاريخ الميلاد:</td>");
            //                    sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
            //                    sbDependants.Append(" </tr>");
            //                }
            //                BodyText.Replace("<!--DependantDetails-->", sbDependants.ToString());

            //                sbDependants = new StringBuilder();
            //                // sbDependants.Append("<div style='page-break-after:always'>&nbsp;</div>");
            //                sbDependants.Append("<table width='0' class='tableStyle'><br />");

            //                for (int z = 2; z < ContractDetails.detail.Length; z++)
            //                {
            //                    sbDependants.Append("<tr>");
            //                    sbDependants.Append(" <td width='350px'>الاسم الكامل:</td>");
            //                    sbDependants.Append("<td style='text-align:center;' width='350px'>" + ContractDetails.detail[z].Name + "</td>");
            //                    sbDependants.Append(" </tr>");

            //                    sbDependants.Append("<tr>");
            //                    sbDependants.Append(" <td>رقم العضوية:</td>");
            //                    sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
            //                    sbDependants.Append(" </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("  <td>الجنس:</td>");
            //                    sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
            //                    sbDependants.Append("   </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("     <td>صلة قرابة:</td>");
            //                    sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
            //                    sbDependants.Append("  </tr>");

            //                    sbDependants.Append(" <tr>");
            //                    sbDependants.Append("     <td>تاريخ الميلاد:</td>");
            //                    sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
            //                    sbDependants.Append(" </tr>");
            //                }
            //                sbDependants.Append("</table> <br /><br /><br />");
            //                BodyText.Replace("<!--DependantDetailsAdditional-->", sbDependants.ToString());
            //            }
            //        }


            //        #endregion

            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //#endregion
            //}
            #endregion


            if (CreateHTMLFile == true)
            {
                int q = 1;
            CheckFile:
                if (File.Exists(HTMLFileCreationPath))
                {
                    HTMLFileName = HTMLFileName.Substring(0, HTMLFileName.Length - 4) + "_" + (q) + ".htm";
                    HTMLFileCreationPath = Applicationpath + "\\Visitor\\" + HTMLFileName;
                    HTMLReadURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Visitor/" + HTMLFileName;
                    q++;
                    goto CheckFile;
                }

            }

            #region make all the Images as Public
            //BodyText.Replace("<--CompanySeal-->", HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Images/CompanySeal.jpg");
            //BodyText.Replace("<--ManagerSeal-->", HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Images/ManagerSeal.jpg");

            #endregion



            #region make all the Images as Public
            //BodyText.Replace("Jet-Airways-Logo.gif",ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString()+ "/PNR/Jet-Airways-Logo.gif");
            //BodyText.Replace("logo_jet.png", ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString() + "/PNR/logo_jet.png");
            //BodyText.Replace("Baggage_DimensionsETI.jpg", ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString() + "/PNR/Baggage_DimensionsETI.jpg");
            //BodyText.Replace("logo_JetKonnect.gif", ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString() + "/PNR/logo_JetKonnect.gif");
            //BodyText.Replace("AdvertisementImage.jpg", ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString() + "/PNR/AdvertisementImage.jpg");

            #endregion

            if (CreateHTMLFile == true)
            {
                System.IO.File.WriteAllText(HTMLFileCreationPath, BodyText.ToString(), Encoding.UTF8);
            }

            Response.Redirect(HTMLReadURL, false);

            /* #region code for creating pdf commented this block because it was creating pdf with old images
             //System.IO.File.ReadAllLines(@"C:\WriteText.htm");

             if (XSettings.InstallLicense(ConfigurationManager.AppSettings["ABDPDFLicense"].ToString()))
             {
                 string licenseResponse = XSettings.LicenseDescription;
             }
             else
             {

             }


             Doc theDoc = new Doc();

             theDoc.Rect.Position(10, 10);
             theDoc.Rect.Height = 710;
             theDoc.Rect.Width = 690;
             // theDoc.Rect.Height = 740;
             //theDoc.Rect.Position(0, 40);
             theDoc.HtmlOptions.AddLinks = true;
             theDoc.HtmlOptions.PageCacheClear();
             theDoc.HtmlOptions.PageCacheEnabled = false;

             //Thread.Sleep(5000);
             theDoc.Encryption.Type = 2;
             theDoc.Encryption.CanCopy = false;
             //theDoc.Encryption.OwnerPassword = "JetAirwaysPDF";
             theDoc.Rect.Inset(10, 10);
             XColor xcolour = new XColor();
             xcolour.Color = System.Drawing.Color.White;
             //theDoc.Color.SetColor(xcolour);// = xcolour; //for 8.1 version
             theDoc.Color.Color = System.Drawing.Color.White;  //for 6.1 version  

             //int pagecount = theDoc.AddImageHtml(BodyText.ToString(), true, 800, true);
             int pagecount = 0;
             if (CreateHTMLFile == true)
             {
                 pagecount = theDoc.AddImageUrl(HTMLReadURL + "?Dummy" + DateTime.Now.ToString("ddMMyyyyhhmmss"), true, 1000, true);
             }
             else
             {
                 pagecount = theDoc.AddImageHtml(BodyText.ToString());
             }
             //int pagecount = theDoc.AddImageUrl(HTMLReadURL);
             bool isfirstpage = true;
             while (true)
             {

                 theDoc.FrameRect();
                 if (!theDoc.Chainable(pagecount))
                 {
                     // theDoc.Page = theDoc.AddPage();
                     break;
                 }
                 //if (isfirstpage)
                 //{
                 //    theDoc.Rect.Height = 790;
                 //    isfirstpage = false;
                 //}
                 //else
                 //{
                 //    theDoc.Rect.Height = 710;

                 //}
                 theDoc.Page = theDoc.AddPage();
                 pagecount = theDoc.AddImageToChain(pagecount);
             }
             if (CreateHeader == true)
             {
                 #region Header & Footer

                 int theCount = theDoc.PageCount;

                 if (FileType == "English")
                 {
                     for (int i = 2; i <= theCount; i++)
                     {
                         theDoc.Rect.String = "20 10 500 50";
                         theDoc.Rect.Position(30, 710);

                         //System.Drawing.Color c = System.Drawing.ColorTranslator.FromHtml("#468DCB");
                         //theDoc.Color.Color = c;
                         //theDoc.PageNumber = i;
                         //theDoc.FillRect();
                         System.Drawing.Color cText = System.Drawing.ColorTranslator.FromHtml("#468DCB");
                         theDoc.Color.Color = cText;
                         string theFont = "Arial";
                         theDoc.Font = theDoc.AddFont(theFont);
                         theDoc.FontSize = 10;
                         theDoc.PageNumber = i;
                         theDoc.AddText("Bupa Inbound Travel Health Insurance Product Policy (to KSA)");

                         //the below string values are as follows.First one is left position inside rectangle second is margin from top
                         //third is right position and fourth is height
                         theDoc.Rect.String = "650 0 700 60";  // last parameter is height 
                         //the below parametrs are rectangle posistions within the above range
                         theDoc.Rect.Position(510, 710); //first parameter is the rect position from left and second is the height position from bottom                               
                         string imagefilePath = HttpContext.Current.Server.MapPath("Bupalogo.jpg");
                         Bitmap myBmp = (Bitmap)Bitmap.FromFile(imagefilePath);
                         theDoc.AddImage(myBmp);

                         #region Footer
                         theDoc.Rect.String = "30 15 300 50";
                         theDoc.Rect.Position(30, 15);
                         theDoc.Color.Color = System.Drawing.Color.DarkGray;
                         theDoc.Font = theDoc.AddFont(theFont);
                         theDoc.FontSize = 10;
                         theDoc.AddText("Bupa Arabia");

                         theDoc.Rect.String = "20 15 670 50";
                         theDoc.Rect.Position(500, 15);
                         theDoc.Color.Color = System.Drawing.Color.DarkGray;
                         theDoc.Font = theDoc.AddFont(theFont);
                         theDoc.FontSize = 10;
                         theDoc.AddText(i.ToString());
                         #endregion

                     }
                 }
                 else
                 {
                     for (int i = 2; i <= theCount; i++)
                     {
                         theDoc.Rect.String = "20 0 500 50";
                         theDoc.Rect.Position(270, 710);

                         //System.Drawing.Color c = System.Drawing.ColorTranslator.FromHtml("#468DCB");
                         //theDoc.Color.Color = c;
                         //theDoc.PageNumber = i;
                         //theDoc.FillRect();
                         System.Drawing.Color cText = System.Drawing.ColorTranslator.FromHtml("#468DCB");
                         theDoc.Color.Color = cText;
                         string theFont = "Arial";
                         theDoc.Font = theDoc.AddFont(theFont);
                         theDoc.FontSize = 10;
                         theDoc.PageNumber = i;
                         //theDoc.AddText("وثيقة بوبا العربية التأمينية على الزائرين القادمين للمملكة العربية السعودية ");

                         theDoc.Rect.String = "30 0 80 50";  // last parameter is height 
                         //the below parametrs are rectangle posistions within the above range
                         theDoc.Rect.Position(30, 710); //first parameter is the rect position from left and second is the height position from bottom                               
                         string imagefilePath = HttpContext.Current.Server.MapPath("Bupalogo.jpg");
                         Bitmap myBmp = (Bitmap)Bitmap.FromFile(imagefilePath);
                         theDoc.AddImage(myBmp);

                         theDoc.Rect.String = "80 0 200 50";  // last parameter is height 
                         theDoc.Rect.Position(200, 710); //first parameter is the rect position from left and second is the height position from bottom                               

                         theDoc.FontSize = 10;
                         theDoc.AddText("");
                         //the below string values are as follows.First one is left position inside rectangle second is margin from top
                         //third is right position and fourth is height

                         theDoc.Rect.String = "300 0 585 50";  // last parameter is height 
                         //the below parametrs are rectangle posistions within the above range
                         theDoc.Rect.Position(300, 705); //first parameter is the rect position from left and second is the height position from bottom                               
                         //imagefilePath = HttpContext.Current.Server.MapPath("PolicyHeaderArabic.png");
                         //myBmp = (Bitmap)Bitmap.FromFile(imagefilePath);
                         //theDoc.AddImage(myBmp);
                         StringBuilder sb = new StringBuilder();

                         sb.Append("<html lang='ar'>");
                         sb.Append("<body>");
                         sb.Append("<table style='font-family: Arial (Body CS); color: #0070c0; font-size: 30px; width: 800px; direction: rtl;'>");
                         sb.Append("<tr>");
                         sb.Append("<td>وثيقة بوبا العربية التأمينية على الزائرين القادمين للمملكة العربية السعودية </td>");
                         sb.Append("</tr>");
                         sb.Append("</table>");
                         sb.Append("</body>");
                         sb.Append("</html>");
                         theDoc.AddImageHtml(sb.ToString());


                         //foooter
                         theDoc.Rect.String = "20 15 200 40";
                         theDoc.Rect.Position(50, 15);
                         theDoc.Color.Color = System.Drawing.Color.DarkGray;
                         theDoc.Font = theDoc.AddFont(theFont);
                         theDoc.FontSize = 9;
                         theDoc.AddText(i.ToString());

                         theDoc.Rect.String = "400 15 680 50";
                         theDoc.Rect.Position(400, 15);
                         // imagefilePath = HttpContext.Current.Server.MapPath("PolicyFooterArabic.png");
                         // myBmp = (Bitmap)Bitmap.FromFile(imagefilePath);
                         // theDoc.AddImage(myBmp);
                         sb = new StringBuilder();

                         sb.Append("<html lang='ar'>");
                         sb.Append("<body>");
                         sb.Append("<table style='font-family: Arial (Body CS); color: Gray; font-size: 30px; width: 500px; direction: rtl;'>");
                         sb.Append("<tr>");
                         sb.Append("<td>بوبا العربية </td>");
                         sb.Append("</tr>");
                         sb.Append("</table>");
                         sb.Append("</body>");
                         sb.Append("</html>");
                         theDoc.AddImageHtml(sb.ToString());
                         //theDoc.Color.Color = System.Drawing.Color.Black;
                         //theDoc.Font = theDoc.AddFont(theFont);
                         //theDoc.Font = theDoc.EmbedFont("Times-Roman", LanguageType.Unicode, false, true);
                         //theDoc.FontSize = 11;
                         //theDoc.AddText("بوبا العربية"); 
                     }
                 }
                 #endregion
             }
             for (int i = 1; i <= theDoc.PageCount; i++)
             {
                 theDoc.PageNumber = i;
                 theDoc.Flatten();
             }
             //reset back to page 1 so the pdf starts displaying there
             if (theDoc.PageCount > 0)
                 theDoc.PageNumber = 1;
             theDoc.SetInfo(theDoc.Root, "/OpenAction", "[ " + theDoc.Page.ToString() + " 0 R /XYZ null null 1 ]");
             // theDoc1.Append(theDoc);
             theDoc.Save(PDFFileSavePath);
             // ErrorInfo.LogError("ABC PDF", PDFFilePath + " has been generated", "New File", "", "", strStaffid);
             theDoc.Clear();
             #endregion
             ////Response.Redirect(PDFReadURL, false);*/
        }
        catch (ExecutionEngineException ex)
        {
            //Response.Write(ex.Message + ex.StackTrace);
            throw ex;
            //runcount++;
            //ErrorInfo.LogError("ABC PDF Error", ex.Message, "EngineException", "", ex.StackTrace, strStaffid);
            //if (runcount < 4)
            //{
            //    goto runloop;
            //}
            //else
            //{
            //    throw ex;
            //}
            //CommonClass.LogError("CreateAgreement1", ex.Message, ex.InnerException, ex.StackTrace);
        }
        catch (Exception ex)
        {
            // Response.Write(ex.Message + ex.StackTrace);
            throw ex;
            //runcount++;
            //ErrorInfo.LogError("ABC PDF Error", ex.Message, "PDF", "", ex.StackTrace, strStaffid);
            //if (runcount < 4)
            //{
            //    goto runloop;
            //}
            //else
            //{
            //    throw ex;
            //}
            //CommonClass.LogError("CreateAgreement2", ex.Message, ex.InnerException, ex.StackTrace);
        }
        return IsAgreementCreated;
    }

    //private bool CreateAgreementUsingABCPDF(string contractNo, string FileType, OS_DXC_WAP.CaesarWS.ReqGetInboundTravelResponse_DN ContractDetails)
    //{
    //    bool IsAgreementCreated = false;
    //    bool CreateHTMLFile = true;
    //    bool CreateHeader = true;
    //    string strRandomNumber = DateTime.Now.ToString("hhssddMMyyyy");
    //    try
    //    {
    //        //string PDFlocation = "C:\\htmlimport.pdf";
    //        // PDFlocation = PDFlocation.Replace('\'', ' ');
    //        string PDFFileName = contractNo + strRandomNumber.ToString() + ".pdf";
    //        string Applicationpath = HttpContext.Current.Request.PhysicalApplicationPath;
    //        string HTMLStaticFileName = "";
    //        if (FileType == "English")
    //        {
    //            HTMLStaticFileName = "Policy English New.html";
    //        }
    //        else
    //        {
    //            HTMLStaticFileName = "Policy Arabic New.html";
    //        }
    //        string HTMLFilePath = Applicationpath + "Visitor\\" + HTMLStaticFileName;
    //        string HTMLFileName = contractNo + strRandomNumber.ToString() + ".htm";
    //        string HTMLFileCreationPath = Applicationpath + "\\Visitor\\" + HTMLFileName;
    //        string PDFFileSavePath = Applicationpath + "\\Visitor\\" + PDFFileName;
    //        string HTMLReadURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Visitor/" + HTMLFileName;
    //        string PDFReadURL = HTMLReadURL.Substring(0, HTMLReadURL.Length - 3) + "pdf";


    //        StringBuilder BodyText = new StringBuilder();
    //        using (var sr = new StreamReader(HTMLFilePath))
    //        {
    //            BodyText.Append(sr.ReadToEnd());
    //        }

    //        #region UpdateParameters

    //        BodyText.Replace(" <!--PolicyPurchaseDate-->", Convert.ToDateTime(ContractDetails.purchaseDate).ToString("dd MMM yyyy"));
    //        BodyText.Replace("<!--PolicyDuration-->", ContractDetails.policyDuration.ToString());
    //        BodyText.Replace("<!--PolicyNumber-->", contractNo);
    //        BodyText.Replace("<!--PassportNumber-->", ContractDetails.passportNo.ToString());
    //        BodyText.Replace("<!--PolicyCoverStartDate-->", Convert.ToDateTime(ContractDetails.startDateCover).ToString("dd MMM yyyy"));
    //        BodyText.Replace("<!--PolicyCoverEndDate-->", Convert.ToDateTime(ContractDetails.endDateCover).ToString("dd MMM yyyy"));
    //        BodyText.Replace("<!--PolicyPremiumAmount-->", ContractDetails.policyPremium.ToString());
    //        BodyText.Replace("<!--MainPolicyHolderName-->", ContractDetails.policyholderName.ToString());
    //        BodyText.Replace("<!--MainPolicyHolderGender-->", ContractDetails.policyholderGender.ToString());
    //        BodyText.Replace("<!--MainPolicyHolderDOB-->", Convert.ToDateTime(ContractDetails.policyholderDOB).ToString("dd MMM yyyy"));
    //        BodyText.Replace("<!--MainPolicyHolderNationality-->", ContractDetails.policyholderNationality.ToString());
    //        BodyText.Replace("<!--MainPolicyHolderPostalAddress-->", ContractDetails.policyholderPostalAddress != null ? ContractDetails.policyholderPostalAddress.ToString() : "");
    //        BodyText.Replace("<!--MainPolicyHolderCity-->", ContractDetails.policyholderCity.ToString());
    //        BodyText.Replace("<!--MainPolicyHolderZipCode-->", ContractDetails.policyholderZipCode.ToString());
    //        BodyText.Replace("<!--MainPolicyHolderPOBox-->", ContractDetails.policyholderPOBox.ToString());
    //        BodyText.Replace("<!--MainPolicyHolderMobileNumber-->", ContractDetails.policyholderMobileNo.ToString());
    //        BodyText.Replace("<!--MainPolicyHolderEmailAddress-->", ContractDetails.policyholderEmail.ToString());
    //        BodyText.Replace("<!--PolicyDate-->", Convert.ToDateTime(ContractDetails.startDateCover).ToString("dd MMM yyyy"));
    //        BodyText.Replace("<!--NetworkProvider-->", ContractDetails.networkType.Trim());
    //        BodyText.Replace("<!--PolicyHolderMembershipNo-->", ContractDetails.policyholderMembershipNo);
    //        #region Update Dependant Details
    //        if (FileType == "English")
    //        {
    //            #region update dependants
    //            if (ContractDetails.detail.Length > 0)
    //            {
    //                StringBuilder sbDependants = new StringBuilder();
    //                sbDependants.Append(" <tr>");
    //                sbDependants.Append("  <td colspan='2' style='background-color:black;color:white;text-align:center;font-weight:bold;'>");
    //                sbDependants.Append("   Dependent Policyholder Information  ");
    //                sbDependants.Append("  </td>");
    //                sbDependants.Append("  </tr>");
    //                if (ContractDetails.detail.Length <= 2)
    //                {
    //                    for (int z = 0; z < ContractDetails.detail.Length; z++)
    //                    {
    //                        sbDependants.Append("<tr>");
    //                        sbDependants.Append(" <td>Name:</td>");
    //                        sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].Name + "</td>");
    //                        sbDependants.Append(" </tr>");

    //                        sbDependants.Append("<tr>");
    //                        sbDependants.Append(" <td>Membership Number:</td>");
    //                        sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
    //                        sbDependants.Append(" </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("  <td>Gender:</td>");
    //                        sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
    //                        sbDependants.Append("   </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("     <td>Relation:</td>");
    //                        sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
    //                        sbDependants.Append("  </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("     <td>Date of Birth:</td>");
    //                        sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
    //                        sbDependants.Append(" </tr>");
    //                    }

    //                    BodyText.Replace("<!--DependantDetails-->", sbDependants.ToString());
    //                }
    //                else
    //                {
    //                    for (int z = 0; z < 2; z++)
    //                    {
    //                        sbDependants.Append("<tr>");
    //                        sbDependants.Append(" <td>Name:</td>");
    //                        sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].Name + "</td>");
    //                        sbDependants.Append(" </tr>");

    //                        sbDependants.Append("<tr>");
    //                        sbDependants.Append(" <td>Membership Number:</td>");
    //                        sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
    //                        sbDependants.Append(" </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("  <td>Gender:</td>");
    //                        sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
    //                        sbDependants.Append("   </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("     <td>Relation:</td>");
    //                        sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
    //                        sbDependants.Append("  </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("     <td>Date of Birth:</td>");
    //                        sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
    //                        sbDependants.Append(" </tr>");
    //                    }
    //                    BodyText.Replace("<!--DependantDetails-->", sbDependants.ToString());

    //                    sbDependants = new StringBuilder();
    //                    // sbDependants.Append("<div style='page-break-after:always'>&nbsp;</div>");
    //                    sbDependants.Append("<table width='0' class='tableStyle'><br />");

    //                    for (int z = 2; z < ContractDetails.detail.Length; z++)
    //                    {
    //                        sbDependants.Append("<tr>");
    //                        sbDependants.Append(" <td  width='350px'>Name:</td>");
    //                        sbDependants.Append("<td style='text-align:center;'  width='350px'>" + ContractDetails.detail[z].Name + "</td>");
    //                        sbDependants.Append(" </tr>");

    //                        sbDependants.Append("<tr>");
    //                        sbDependants.Append(" <td>Membership Number:</td>");
    //                        sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
    //                        sbDependants.Append(" </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("  <td>Gender:</td>");
    //                        sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
    //                        sbDependants.Append("   </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("     <td>Relation:</td>");
    //                        sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
    //                        sbDependants.Append("  </tr>");

    //                        sbDependants.Append(" <tr>");
    //                        sbDependants.Append("     <td>Date of Birth:</td>");
    //                        sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
    //                        sbDependants.Append(" </tr>");
    //                    }
    //                    sbDependants.Append("</table> <br /><br /><br />");
    //                    BodyText.Replace("<!--DependantDetailsAdditional-->", sbDependants.ToString());
    //                }
    //            }


    //            #endregion
    //        }
    //        else
    //        {
    //            try
    //            {

    //                #region update dependants
    //                if (ContractDetails.detail.Length > 0)
    //                {
    //                    StringBuilder sbDependants = new StringBuilder();
    //                    sbDependants.Append(" <tr>");
    //                    sbDependants.Append("  <td colspan='2' style='background-color:black;color:white;text-align:center;font-weight:bold;'>");
    //                    sbDependants.Append("   معلومات المعال ");
    //                    sbDependants.Append("  </td>");
    //                    sbDependants.Append("  </tr>");
    //                    if (ContractDetails.detail.Length <= 2)
    //                    {
    //                        for (int z = 0; z < ContractDetails.detail.Length; z++)
    //                        {
    //                            sbDependants.Append("<tr>");
    //                            sbDependants.Append(" <td>الاسم الكامل:</td>");
    //                            sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].Name + "</td>");
    //                            sbDependants.Append(" </tr>");

    //                            sbDependants.Append("<tr>");
    //                            sbDependants.Append(" <td>رقم العضوية:</td>");
    //                            sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
    //                            sbDependants.Append(" </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("  <td>الجنس:</td>");
    //                            sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
    //                            sbDependants.Append("   </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("     <td>صلة قرابة:</td>");
    //                            sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
    //                            sbDependants.Append("  </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("     <td>تاريخ الميلاد:</td>");
    //                            sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
    //                            sbDependants.Append(" </tr>");
    //                        }

    //                        BodyText.Replace("<!--DependantDetails-->", sbDependants.ToString());

    //                    }
    //                    else
    //                    {
    //                        for (int z = 0; z < 2; z++)
    //                        {
    //                            sbDependants.Append("<tr>");
    //                            sbDependants.Append(" <td>الاسم الكامل:</td>");
    //                            sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].Name + "</td>");
    //                            sbDependants.Append(" </tr>");

    //                            sbDependants.Append("<tr>");
    //                            sbDependants.Append(" <td>رقم العضوية:</td>");
    //                            sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
    //                            sbDependants.Append(" </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("  <td>الجنس:</td>");
    //                            sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
    //                            sbDependants.Append("   </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("     <td>صلة قرابة:</td>");
    //                            sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
    //                            sbDependants.Append("  </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("     <td>تاريخ الميلاد:</td>");
    //                            sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
    //                            sbDependants.Append(" </tr>");
    //                        }
    //                        BodyText.Replace("<!--DependantDetails-->", sbDependants.ToString());

    //                        sbDependants = new StringBuilder();
    //                        // sbDependants.Append("<div style='page-break-after:always'>&nbsp;</div>");
    //                        sbDependants.Append("<table width='0' class='tableStyle'><br />");

    //                        for (int z = 2; z < ContractDetails.detail.Length; z++)
    //                        {
    //                            sbDependants.Append("<tr>");
    //                            sbDependants.Append(" <td width='350px'>الاسم الكامل:</td>");
    //                            sbDependants.Append("<td style='text-align:center;' width='350px'>" + ContractDetails.detail[z].Name + "</td>");
    //                            sbDependants.Append(" </tr>");

    //                            sbDependants.Append("<tr>");
    //                            sbDependants.Append(" <td>رقم العضوية:</td>");
    //                            sbDependants.Append("<td style='text-align:center;'>" + ContractDetails.detail[z].MembershipNo + "</td>");
    //                            sbDependants.Append(" </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("  <td>الجنس:</td>");
    //                            sbDependants.Append("   <td style='text-align:center;'>" + ContractDetails.detail[z].Gender + "</td>");
    //                            sbDependants.Append("   </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("     <td>صلة قرابة:</td>");
    //                            sbDependants.Append("     <td style='text-align:center;'>" + ContractDetails.detail[z].Relation + "</td>");
    //                            sbDependants.Append("  </tr>");

    //                            sbDependants.Append(" <tr>");
    //                            sbDependants.Append("     <td>تاريخ الميلاد:</td>");
    //                            sbDependants.Append("     <td style='text-align:center; direction: ltr;'>" + Convert.ToDateTime(ContractDetails.detail[z].DOB).ToString("dd MMM yyyy") + "</td>");
    //                            sbDependants.Append(" </tr>");
    //                        }
    //                        sbDependants.Append("</table> <br /><br /><br />");
    //                        BodyText.Replace("<!--DependantDetailsAdditional-->", sbDependants.ToString());
    //                    }
    //                }


    //                #endregion

    //            }
    //            catch (Exception ex)
    //            {
    //                throw ex;
    //            }
    //        #endregion
    //        }
    //        #endregion


    //        if (CreateHTMLFile == true)
    //        {
    //            int q = 1;
    //        CheckFile:
    //            if (File.Exists(HTMLFileCreationPath))
    //            {
    //                HTMLFileName = HTMLFileName.Substring(0, HTMLFileName.Length - 4) + "_" + (q) + ".htm";
    //                HTMLFileCreationPath = Applicationpath + "\\Visitor\\" + HTMLFileName;
    //                HTMLReadURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Visitor/" + HTMLFileName;
    //                q++;
    //                goto CheckFile;
    //            }

    //        }

    //        #region make all the Images as Public
    //        //BodyText.Replace("<--CompanySeal-->", HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Images/CompanySeal.jpg");
    //        //BodyText.Replace("<--ManagerSeal-->", HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Images/ManagerSeal.jpg");

    //        #endregion



    //        #region make all the Images as Public
    //        //BodyText.Replace("Jet-Airways-Logo.gif",ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString()+ "/PNR/Jet-Airways-Logo.gif");
    //        //BodyText.Replace("logo_jet.png", ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString() + "/PNR/logo_jet.png");
    //        //BodyText.Replace("Baggage_DimensionsETI.jpg", ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString() + "/PNR/Baggage_DimensionsETI.jpg");
    //        //BodyText.Replace("logo_JetKonnect.gif", ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString() + "/PNR/logo_JetKonnect.gif");
    //        //BodyText.Replace("AdvertisementImage.jpg", ConfigurationManager.AppSettings["STMS-Access-WebURL"].ToString() + "/PNR/AdvertisementImage.jpg");

    //        #endregion

    //        if (CreateHTMLFile == true)
    //        {
    //            System.IO.File.WriteAllText(HTMLFileCreationPath, BodyText.ToString(), Encoding.UTF8);
    //        }

    //         Response.Redirect(HTMLReadURL, false);
			 
    //       /* #region code for creating pdf commented this block because it was creating pdf with old images
    //        //System.IO.File.ReadAllLines(@"C:\WriteText.htm");

    //        if (XSettings.InstallLicense(ConfigurationManager.AppSettings["ABDPDFLicense"].ToString()))
    //        {
    //            string licenseResponse = XSettings.LicenseDescription;
    //        }
    //        else
    //        {

    //        }


    //        Doc theDoc = new Doc();

    //        theDoc.Rect.Position(10, 10);
    //        theDoc.Rect.Height = 710;
    //        theDoc.Rect.Width = 690;
    //        // theDoc.Rect.Height = 740;
    //        //theDoc.Rect.Position(0, 40);
    //        theDoc.HtmlOptions.AddLinks = true;
    //        theDoc.HtmlOptions.PageCacheClear();
    //        theDoc.HtmlOptions.PageCacheEnabled = false;

    //        //Thread.Sleep(5000);
    //        theDoc.Encryption.Type = 2;
    //        theDoc.Encryption.CanCopy = false;
    //        //theDoc.Encryption.OwnerPassword = "JetAirwaysPDF";
    //        theDoc.Rect.Inset(10, 10);
    //        XColor xcolour = new XColor();
    //        xcolour.Color = System.Drawing.Color.White;
    //        //theDoc.Color.SetColor(xcolour);// = xcolour; //for 8.1 version
    //        theDoc.Color.Color = System.Drawing.Color.White;  //for 6.1 version  

    //        //int pagecount = theDoc.AddImageHtml(BodyText.ToString(), true, 800, true);
    //        int pagecount = 0;
    //        if (CreateHTMLFile == true)
    //        {
    //            pagecount = theDoc.AddImageUrl(HTMLReadURL + "?Dummy" + DateTime.Now.ToString("ddMMyyyyhhmmss"), true, 1000, true);
    //        }
    //        else
    //        {
    //            pagecount = theDoc.AddImageHtml(BodyText.ToString());
    //        }
    //        //int pagecount = theDoc.AddImageUrl(HTMLReadURL);
    //        bool isfirstpage = true;
    //        while (true)
    //        {

    //            theDoc.FrameRect();
    //            if (!theDoc.Chainable(pagecount))
    //            {
    //                // theDoc.Page = theDoc.AddPage();
    //                break;
    //            }
    //            //if (isfirstpage)
    //            //{
    //            //    theDoc.Rect.Height = 790;
    //            //    isfirstpage = false;
    //            //}
    //            //else
    //            //{
    //            //    theDoc.Rect.Height = 710;

    //            //}
    //            theDoc.Page = theDoc.AddPage();
    //            pagecount = theDoc.AddImageToChain(pagecount);
    //        }
    //        if (CreateHeader == true)
    //        {
    //            #region Header & Footer

    //            int theCount = theDoc.PageCount;

    //            if (FileType == "English")
    //            {
    //                for (int i = 2; i <= theCount; i++)
    //                {
    //                    theDoc.Rect.String = "20 10 500 50";
    //                    theDoc.Rect.Position(30, 710);

    //                    //System.Drawing.Color c = System.Drawing.ColorTranslator.FromHtml("#468DCB");
    //                    //theDoc.Color.Color = c;
    //                    //theDoc.PageNumber = i;
    //                    //theDoc.FillRect();
    //                    System.Drawing.Color cText = System.Drawing.ColorTranslator.FromHtml("#468DCB");
    //                    theDoc.Color.Color = cText;
    //                    string theFont = "Arial";
    //                    theDoc.Font = theDoc.AddFont(theFont);
    //                    theDoc.FontSize = 10;
    //                    theDoc.PageNumber = i;
    //                    theDoc.AddText("Bupa Inbound Travel Health Insurance Product Policy (to KSA)");

    //                    //the below string values are as follows.First one is left position inside rectangle second is margin from top
    //                    //third is right position and fourth is height
    //                    theDoc.Rect.String = "650 0 700 60";  // last parameter is height 
    //                    //the below parametrs are rectangle posistions within the above range
    //                    theDoc.Rect.Position(510, 710); //first parameter is the rect position from left and second is the height position from bottom                               
    //                    string imagefilePath = HttpContext.Current.Server.MapPath("Bupalogo.jpg");
    //                    Bitmap myBmp = (Bitmap)Bitmap.FromFile(imagefilePath);
    //                    theDoc.AddImage(myBmp);

    //                    #region Footer
    //                    theDoc.Rect.String = "30 15 300 50";
    //                    theDoc.Rect.Position(30, 15);
    //                    theDoc.Color.Color = System.Drawing.Color.DarkGray;
    //                    theDoc.Font = theDoc.AddFont(theFont);
    //                    theDoc.FontSize = 10;
    //                    theDoc.AddText("Bupa Arabia");

    //                    theDoc.Rect.String = "20 15 670 50";
    //                    theDoc.Rect.Position(500, 15);
    //                    theDoc.Color.Color = System.Drawing.Color.DarkGray;
    //                    theDoc.Font = theDoc.AddFont(theFont);
    //                    theDoc.FontSize = 10;
    //                    theDoc.AddText(i.ToString());
    //                    #endregion

    //                }
    //            }
    //            else
    //            {
    //                for (int i = 2; i <= theCount; i++)
    //                {
    //                    theDoc.Rect.String = "20 0 500 50";
    //                    theDoc.Rect.Position(270, 710);

    //                    //System.Drawing.Color c = System.Drawing.ColorTranslator.FromHtml("#468DCB");
    //                    //theDoc.Color.Color = c;
    //                    //theDoc.PageNumber = i;
    //                    //theDoc.FillRect();
    //                    System.Drawing.Color cText = System.Drawing.ColorTranslator.FromHtml("#468DCB");
    //                    theDoc.Color.Color = cText;
    //                    string theFont = "Arial";
    //                    theDoc.Font = theDoc.AddFont(theFont);
    //                    theDoc.FontSize = 10;
    //                    theDoc.PageNumber = i;
    //                    //theDoc.AddText("وثيقة بوبا العربية التأمينية على الزائرين القادمين للمملكة العربية السعودية ");

    //                    theDoc.Rect.String = "30 0 80 50";  // last parameter is height 
    //                    //the below parametrs are rectangle posistions within the above range
    //                    theDoc.Rect.Position(30, 710); //first parameter is the rect position from left and second is the height position from bottom                               
    //                    string imagefilePath = HttpContext.Current.Server.MapPath("Bupalogo.jpg");
    //                    Bitmap myBmp = (Bitmap)Bitmap.FromFile(imagefilePath);
    //                    theDoc.AddImage(myBmp);

    //                    theDoc.Rect.String = "80 0 200 50";  // last parameter is height 
    //                    theDoc.Rect.Position(200, 710); //first parameter is the rect position from left and second is the height position from bottom                               

    //                    theDoc.FontSize = 10;
    //                    theDoc.AddText("");
    //                    //the below string values are as follows.First one is left position inside rectangle second is margin from top
    //                    //third is right position and fourth is height

    //                    theDoc.Rect.String = "300 0 585 50";  // last parameter is height 
    //                    //the below parametrs are rectangle posistions within the above range
    //                    theDoc.Rect.Position(300, 705); //first parameter is the rect position from left and second is the height position from bottom                               
    //                    //imagefilePath = HttpContext.Current.Server.MapPath("PolicyHeaderArabic.png");
    //                    //myBmp = (Bitmap)Bitmap.FromFile(imagefilePath);
    //                    //theDoc.AddImage(myBmp);
    //                    StringBuilder sb = new StringBuilder();

    //                    sb.Append("<html lang='ar'>");
    //                    sb.Append("<body>");
    //                    sb.Append("<table style='font-family: Arial (Body CS); color: #0070c0; font-size: 30px; width: 800px; direction: rtl;'>");
    //                    sb.Append("<tr>");
    //                    sb.Append("<td>وثيقة بوبا العربية التأمينية على الزائرين القادمين للمملكة العربية السعودية </td>");
    //                    sb.Append("</tr>");
    //                    sb.Append("</table>");
    //                    sb.Append("</body>");
    //                    sb.Append("</html>");
    //                    theDoc.AddImageHtml(sb.ToString());


    //                    //foooter
    //                    theDoc.Rect.String = "20 15 200 40";
    //                    theDoc.Rect.Position(50, 15);
    //                    theDoc.Color.Color = System.Drawing.Color.DarkGray;
    //                    theDoc.Font = theDoc.AddFont(theFont);
    //                    theDoc.FontSize = 9;
    //                    theDoc.AddText(i.ToString());

    //                    theDoc.Rect.String = "400 15 680 50";
    //                    theDoc.Rect.Position(400, 15);
    //                    // imagefilePath = HttpContext.Current.Server.MapPath("PolicyFooterArabic.png");
    //                    // myBmp = (Bitmap)Bitmap.FromFile(imagefilePath);
    //                    // theDoc.AddImage(myBmp);
    //                    sb = new StringBuilder();

    //                    sb.Append("<html lang='ar'>");
    //                    sb.Append("<body>");
    //                    sb.Append("<table style='font-family: Arial (Body CS); color: Gray; font-size: 30px; width: 500px; direction: rtl;'>");
    //                    sb.Append("<tr>");
    //                    sb.Append("<td>بوبا العربية </td>");
    //                    sb.Append("</tr>");
    //                    sb.Append("</table>");
    //                    sb.Append("</body>");
    //                    sb.Append("</html>");
    //                    theDoc.AddImageHtml(sb.ToString());
    //                    //theDoc.Color.Color = System.Drawing.Color.Black;
    //                    //theDoc.Font = theDoc.AddFont(theFont);
    //                    //theDoc.Font = theDoc.EmbedFont("Times-Roman", LanguageType.Unicode, false, true);
    //                    //theDoc.FontSize = 11;
    //                    //theDoc.AddText("بوبا العربية"); 
    //                }
    //            }
    //            #endregion
    //        }
    //        for (int i = 1; i <= theDoc.PageCount; i++)
    //        {
    //            theDoc.PageNumber = i;
    //            theDoc.Flatten();
    //        }
    //        //reset back to page 1 so the pdf starts displaying there
    //        if (theDoc.PageCount > 0)
    //            theDoc.PageNumber = 1;
    //        theDoc.SetInfo(theDoc.Root, "/OpenAction", "[ " + theDoc.Page.ToString() + " 0 R /XYZ null null 1 ]");
    //        // theDoc1.Append(theDoc);
    //        theDoc.Save(PDFFileSavePath);
    //        // ErrorInfo.LogError("ABC PDF", PDFFilePath + " has been generated", "New File", "", "", strStaffid);
    //        theDoc.Clear();
    //        #endregion
    //        ////Response.Redirect(PDFReadURL, false);*/
    //    }
    //    catch (ExecutionEngineException ex)
    //    {
    //        //Response.Write(ex.Message + ex.StackTrace);
    //        throw ex;
    //        //runcount++;
    //        //ErrorInfo.LogError("ABC PDF Error", ex.Message, "EngineException", "", ex.StackTrace, strStaffid);
    //        //if (runcount < 4)
    //        //{
    //        //    goto runloop;
    //        //}
    //        //else
    //        //{
    //        //    throw ex;
    //        //}
    //        //CommonClass.LogError("CreateAgreement1", ex.Message, ex.InnerException, ex.StackTrace);
    //    }
    //    catch (Exception ex)
    //    {
    //        // Response.Write(ex.Message + ex.StackTrace);
    //        throw ex;
    //        //runcount++;
    //        //ErrorInfo.LogError("ABC PDF Error", ex.Message, "PDF", "", ex.StackTrace, strStaffid);
    //        //if (runcount < 4)
    //        //{
    //        //    goto runloop;
    //        //}
    //        //else
    //        //{
    //        //    throw ex;
    //        //}
    //        //CommonClass.LogError("CreateAgreement2", ex.Message, ex.InnerException, ex.StackTrace);
    //    }
    //    return IsAgreementCreated;
    //}


    private void DeleteTempFiles()
    {

        string[] filePaths = Directory.GetFiles(Server.MapPath(""));
        foreach (string filePath in filePaths)
        {
            string FileName = filePath.Substring(Server.MapPath("").Length + 1);
            if (FileName.StartsWith("1") || FileName.StartsWith("7") || FileName.StartsWith("8"))
            {
                System.IO.FileInfo fi = null;
                try
                {
                    fi = new System.IO.FileInfo(filePath);
                    if (DateTime.Now > fi.CreationTime.AddMinutes(5))
                    {
                        File.Delete(filePath);
                    }
                }
                catch (Exception ex)
                {
                    CommonClass.LogError("File Delete", ex.Message, ex.InnerException, ex.StackTrace, "");
                }

            }
            //else
            //{
            //    CommonClass.LogError("File Info", FileName, null,"");
            //}
        }
    }
}