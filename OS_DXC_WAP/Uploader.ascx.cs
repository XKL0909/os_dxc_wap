using System;
using System.Collections;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using log4net;
using Bupa.Core.Logging;

using Bupa.OSWeb.Helper;
using Utility.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using OS_DXC_WAP.CaesarWS;
using Bupa.Core.Logging;
using Utility.Data;

public partial class Uploader : System.Web.UI.UserControl
{
    private ILog _log = LogManager.GetLogger(typeof(Uploader));

    private UploadManager _uploadManager;
    private DataSet _uploadedFileSet = new DataSet();

    private string _sessionID = string.Empty;
    private string _userID = string.Empty;
    private string _uploadCategory = string.Empty;
    private string _allowedExtensions = string.Empty;

    private bool _fileSizeDefined = false;
    private long _maxFileSize;
    private string _folder;
    private bool _fileSetSizeDefined = false;
    private long _maxFileSetByteSize;

    long fileSizeLimit;
    long fileSize;

    public int FilesCount { get { return dvFiles.Rows.Count; } }

    public string SessionID
    {
        get { return _sessionID; }
        set { _sessionID = value; }
    }

    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }

    public string UploadCategory
    {
        get { return _uploadCategory; }
        set { _uploadCategory = value; }
    }

    public string AllowedExtensions
    {
        get { return _allowedExtensions; }
        set { _allowedExtensions = value; }
    }

    public long MaxFileSize
    {
        get { return _maxFileSize; }
        set 
        { 
            _maxFileSize = value;
            _fileSizeDefined = true;
        }
    }

    public long MaxFileSetSize
    {
        get { return _maxFileSetByteSize; }
        set 
        { 
            _maxFileSetByteSize = value;
            _fileSetSizeDefined = true;
        }

    }

    public Label LblUpload
    {
        get { return lblUpload; }
    }

    public DataSet UploadedFileSet
    {
        get
        {
            if (_uploadedFileSet.Tables.Count == 0)
            {
                // Reload the fileset
                _uploadedFileSet = ShowUploadedList();
            }
            return _uploadedFileSet;
        }
    }

    public Uploader()
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {

        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }
        Logger.Tracer.WriteMemberExit();
    }


  
    protected void Page_Load(object sender, EventArgs e)
    {
        //HttpContext.Current.Response.Write("1" + SessionID);
        Logger.Tracer.WriteMemberEntry();
        try
        {
            // Stop Caching in IE 
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

            // Stop Caching in Firefox 
            Response.Cache.SetNoStore();

            _folder = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHFOLDER_PATH);

            if (!_fileSetSizeDefined)
            {
                _maxFileSetByteSize = long.Parse(CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSize));
            }
            ////Tracer.WriteLine(ref _log, "_maxFileSetByteSize is: " + _maxFileSetByteSize);

            if (!_fileSizeDefined)
            {
                // Set the max file size that can be uploaded to be 5MB
                _maxFileSize = 5242880;
            }
            //if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["optiontype"])) && Convert.ToString(Request.QueryString["optiontype"]).ToLower() == "class")
            //{
            //    _uploadCategory = Bupa.OSWeb.Helper.UploadCategory.MEMBER_CHGCLASS;
            //}
            // Set the allowed extensions to be uploaded - note that this can be overriden by the host
            if (Request["UploadCategory"] == "Submit Reimbursement" || Request["UploadCategory"] == "Submit eClaims")
            {
                _allowedExtensions = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensionseClaims);
            }
            else
            {
                _allowedExtensions = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
            }
            

            // Set the default values for the upload manager = note that this can also be overriden by the host
            _uploadManager = new UploadManager(_maxFileSetByteSize, _maxFileSize);

            lblUpload.Text = string.Empty;
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }
        Logger.Tracer.WriteMemberExit();
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
			bool isvalidFile = false;
            lblUpload.Text = "";
            if (uploader.HasFile)
            {

                if (Request["UploadCategory"] == "Submit Reimbursement" || Request["UploadCategory"] == "Submit eClaims")
                {
                    CheckMimeTypes validExcelfile = new CheckMimeTypes();
                    if (validExcelfile.ValidateExcelFile(uploader.PostedFile))
                    {
                        isvalidFile = true;
                    }
                    else
                    {
                        lblUpload.Text = "Please select a valid file";
                        return;
                    }
                }
                else
                {
                    Boolean fileOK = false;
                    Boolean mimeOK = false;
                    System.Web.HttpPostedFile file = uploader.PostedFile;
                    CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
                    string mime = findMimeFromDate.CheckFindMimeFromData(file);
                    String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
                    String fileExtension = System.IO.Path.GetExtension(uploader.FileName).ToLower();
                    String[] allowedExtensions = _allowedExtensions.Split(',');
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                            break;
                        }
                    }
                    for (int i = 0; i < allowedMime.Length; i++)
                    {
                        if (mime == allowedMime[i])
                        {
                            mimeOK = true;
                            break;
                        }
                    }
                    if (fileOK && mimeOK)
                    {
                        isvalidFile = true;
                    }
                    else
                    {
                        lblUpload.Text = "Please select a valid file";
                        return;
                    }

                    fileSizeLimit = Convert.ToInt64(CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSize));
                    fileSize = uploader.FileContent.Length;

                    if (fileSize > fileSizeLimit)
                    {

                        string fileSizeMB = (fileSize / 1048576).ToString();
                        string fileSizeLimitMB = (fileSizeLimit / 1048576).ToString();
                        lblUpload.Text = "This file exceeds the total file size allowed to be uploaded. Total allowed size is: " + fileSizeLimitMB + "MB. File size is: ";

                        return;
                    }
                    else
                    {
                        isvalidFile = true;
                    }


                }

                if (isvalidFile)
                {


                    string fileName = uploader.FileName;
                    ////Tracer.WriteLine(ref _log, "fileName is: " + fileName);

                    bool grantSave = IsAllowedExtension(fileName);
                    ////Tracer.WriteLine(ref _log, "grantSave is: " + grantSave);
                    if (!grantSave)
                    {
                        // Extension not allowed for upload
                        lblUpload.Text = "Please ensure you upload the correct file type (allowed extensions are: " + _allowedExtensions + ")";
                        return;
                    }

                    // Proceed with saving the file
                    UploadResult uploaded = new UploadResult();
                    if (Session["peauthupload"] != null && Session["peauthupload"].ToString() == "Yes")
                    {
                        uploaded = SaveFileToFolder(fileName, _folder);
                    }
                    else
                    {
                        uploaded = SaveFile(fileName);
                    }

                    ////Tracer.WriteLine(ref _log, "uploaded is: " + uploaded.Result);

                    // Check if the upload was successful
                    if (!uploaded.Result)
                    {
                        // Parse the errors
                        string errors = "Cannot upload this file: ";
                        foreach (string errorMessage in uploaded.ErrorReason)
                        {
                            errors = errors + errorMessage;
                        }

                        // File size has been exceeded
                        lblUpload.Text = errors;
                    }
                }
                else
                {
                    lblUpload.Text = "Please select a valid file";
                }
            }
            else
            {
                lblUpload.Text = "Please select a valid file";
            }
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }
        Logger.Tracer.WriteMemberExit();
    }

    protected void dvFiles_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            string currentCommand = e.CommandName;
            if (e.CommandName == "Delete")
            {
                ////Tracer.WriteLine(ref _log, "Deleting....");
                DataSet d = ShowUploadedList();

                if (d != null)
                {
                    // Two items need to be deleted.
                    // The first is the record in the database and the second is the actual file itself
                    int currentRowIndex = Int32.Parse(e.CommandArgument.ToString());
                    ////int uploadedID = Int32.Parse(dvFiles.DataKeys[currentRowIndex].Value.ToString());
					int uploadedID = Int32.Parse(dvFiles.DataKeys[currentRowIndex][0].ToString());
                    ////Tracer.WriteLine(ref _log, "uploadedID is: " + uploadedID);

                    // Get the file path to be deleted
                    DataTable t = d.Tables[0];
                    ////Tracer.WriteLine(ref _log, "Fetched datatable for upload deletion");

                    DataRow[] dr = t.Select("UploadedID = " + uploadedID);

                    string deletedFilePath = string.Empty;
                    foreach (DataRow r in dr)
                    {
                        deletedFilePath = r["FilePath"].ToString();

                        ////Tracer.WriteLine(ref _log, "deletedFilePath is: " + deletedFilePath);
                        _uploadManager.Remove(uploadedID, deletedFilePath);
                        break;
                    }

                    //if (deletedFilePath.Length == 0)
                        ////Tracer.WriteLine(ref _log, "Unable to delete uploaded file - uploadedID is: " + uploadedID);
                }
                else
                {
                    lblUpload.Text = "Error retrieving filelist before deletion - please contact support";
                }
            }
            ShowUploadedList();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
    }

    protected void dvFiles_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    public void SetupUploader(string functionName, string userID, string allowedExtensions, long maxFileSize)
    {
        this.UserID = userID;
        this.UploadCategory = functionName;

        this.AllowedExtensions = allowedExtensions;
        this.MaxFileSize = maxFileSize;

        // Require the upload manager to deal with uploads
        _uploadManager = new UploadManager(_maxFileSetByteSize, maxFileSize);

        if (Page.IsPostBack && ViewState[UploadPublication.Session_RequestID] != null)
        {
            // Retrieve the requestID for uploads to remember what was uploaded
            
            ////Tracer.WriteLine(ref _log, "Retrieve the requestID for uploads to remember what was uploaded");

            if (!string.IsNullOrEmpty(Convert.ToString(Request["sess"])))
            {
                this.SessionID = Convert.ToString(Request["sess"]).ToString();
                ////Tracer.WriteLine(ref _log, "Retrieved this.SessionID is: " + this.SessionID);
            }
            else
            {
                if (ViewState[UploadPublication.Session_RequestID] != null)
                {
                    this.SessionID = ViewState[UploadPublication.Session_RequestID].ToString();
                    ////Tracer.WriteLine(ref _log, "Retrieved this.SessionID is: " + this.SessionID);
                }
            }
        }
        else
        {
            // Generate the request ID for the upload
            if (!string.IsNullOrEmpty(Convert.ToString(Request["sess"])))
            {
                this.SessionID = Convert.ToString(Request["sess"]);
            }
            else
            {
                this.SessionID = UploadManager.GetNextRequest();
            }
            
            
            
            
            ////Tracer.WriteLine(ref _log, "this.SessionID for GetNextRequest is: " + this.SessionID);

            // Store in the ViewState to retrieve on postbacks
            ViewState[UploadPublication.Session_RequestID] = this.SessionID;
        }
    }

    public void Notify(string uploadSessionID, string function, string referenceID, string[] addressees, string fullName, string username)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            // Send an email informing the team about a pre-auth request
            string fromEmail = "Membership_online_doc@bupa.com.sa";

            string subject = function + " reference:" + referenceID + " - attachments added by " + fullName + " (" + username + ")";
            string content = "Please view the attached uploaded documents added by " + fullName + " (" + username + ") for " + function + " reference: " + referenceID;

            // Get the list of attachments for this user
            Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
            DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);

            StringCollection attachments = new StringCollection();
            if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow r in d.Tables[0].Rows)
                {
                    string attachPath = r["VirtualPath"].ToString();
                    attachments.Add(attachPath);
                }
            }
            else
            {
                // No attachments and therefore, nothing to email
                return;
            }

            // Get the addressees
            foreach (string toEmail in addressees)
            {
                SendEmail(fromEmail, toEmail, subject, content, attachments);
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        Logger.Tracer.WriteMemberExit();
    }

    private UploadResult SaveFile(string fileName)
    {
        Logger.Tracer.WriteMemberEntry();

        UploadResult uploadResult = new UploadResult();
        uploadResult.Result = false;
        string virtualPath = "";
        string serverPath = string.Empty;
       

        try
        {
            // Fetch the filename and save to the server
            ////Tracer.WriteLine(ref _log, "fileName is: " + fileName);

            // Get a random name and append to the file
            int seed = DateTime.Now.Millisecond;
            ////Tracer.WriteLine(ref _log, "seed is: " + seed);

            string uniquePrefix = WebPublication.GenerateUniqueID;
            ////Tracer.WriteLine(ref _log, "uniquePrefix is: " + uniquePrefix);
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            // Go ahead and save to the server for the moment
            if (!string.IsNullOrEmpty(Convert.ToString(Session["PreauthUpload"])))
            {
                string strPreauth = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHFOLDER_PATH);
                virtualPath = "~/" + strPreauth + "/" + uniquePrefix + "_" + fileName;
            }
            else if (url.ToLower().Contains("reimbursementform"))
            {
                //Response.Write("Step 1 : Inside reimbursementform <br/>");

                

                ////serverPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentServerPath);
                serverPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentServerPathCommon);
                //////string strPreauth = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PREAUTHFOLDER_PATH);
                //////virtualPath = "~/ClaimsUpload/" + uniquePrefix + "_" + fileName;
                //Logger.Current.Information("getting config in" + serverPath);
                //Response.Write("Step 2: getting config in" + serverPath + "<br/>");

            }
            else
            {
                virtualPath = "~/Uploads/" + uniquePrefix + "_" + fileName;
                ////Logger.Current.Information("not in reimbursement" + serverPath);
                //Response.Write("Step 3: not in reimbursement" + serverPath + "<br/>");
            }

            string destinationPath = string.Empty;

            if (url.ToLower().Contains("reimbursementform"))
            {
                ////network is not working
                ////old code start
                /* using (NetworkShareAccesser.Access(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServer"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServerDomain"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServerUser"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServerPassword"])))
                 ////using (NetworkShareAccesser.Access(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPClaimServer"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPServerDomain"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPClaimUser"]), Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FTPClaimPassword"])))
                 {
                     destinationPath = serverPath + uniquePrefix + "_" + fileName;
                     uploader.SaveAs(serverPath + uniquePrefix + "_" + fileName);
                     virtualPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentVirtualPath) + "/" + uniquePrefix + "_" + fileName;

                 }*/
                ////old code end
                //// new code start

                /// Logger.Current.Information("getting file config");
                //Response.Write("Step 4 : getting file config <br/>");

                var domainName = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentDomain);
                var userName = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentUserName);
                var userCredential = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentUserCredential);
                ImpersonationHelper.Impersonate(domainName, userName, userCredential, delegate
                {
                    ///string extension = Path.GetExtension(fileName);
                    ///string newfileName = TransactionManager.TransactionID().ToString() + "_" + Session["ClientID"] + extension;
                    string newfileName = uniquePrefix + "_" + fileName;
                    string vPath = serverPath + newfileName;
                    destinationPath = vPath;
                    uploader.SaveAs(vPath);
                    virtualPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentVirtualPath) + "/" + uniquePrefix + "_" + fileName;

                    ////Logger.Current.Information(virtualPath);
                    ////Logger.Current.Information("file saved in server");

                    //Response.Write("Step 5 :"+ virtualPath + "<br/>");
                    //Response.Write("Step 6 : file saved in server <br/>");
                });

                //// new code end



                //if(string.IsNullOrEmpty(_uploadCategory.Trim()))
                //// if SessionID is null, setting reimbursement session id by sakthi on  29-Mar-2016
                ////Start
                _uploadCategory = string.IsNullOrEmpty(_uploadCategory.Trim()) ? Bupa.OSWeb.Helper.UploadCategory.Submit_Reimbursement : _uploadCategory;

                //Logger.Current.Information("geting files from session");
                //Response.Write("Step 7: geting files from session <br/>");
                if (string.IsNullOrEmpty(_sessionID.Trim()))
                {
                    //Logger.Current.Information("geting files from session is not null"); 
                    //Response.Write("Step 8: geting files from session is not null <br/>");
                    if (dvFiles != null && dvFiles.Rows.Count > 0)
                    {
                        ////Logger.Current.Information("geting files from file count is not null");
                        //Response.Write("Step 9 geting files from file count is not null <br/>");
                        foreach (GridViewRow row in dvFiles.Rows)
                        {
                            string theDataKey = dvFiles.DataKeys[row.RowIndex][1].ToString();
                            if (!string.IsNullOrEmpty(theDataKey.Trim()))
                            {
                                this.SessionID = theDataKey;
                                _sessionID = this.SessionID;
                                break;
                            }
                        }
                        _sessionID = string.IsNullOrEmpty(_sessionID.Trim()) ? UploadManager.GetNextRequest() : _sessionID;
                    }
                    else
                    {
                        this.SessionID = UploadManager.GetNextRequest();
                        _sessionID = this.SessionID;

                        ////Logger.Current.Information("geting files from file count is null");
                        //Response.Write("Step 10 geting files from file count is null <br/>");
                    }
                    ViewState[UploadPublication.Session_RequestID] = this.SessionID;
                }
               
                ////End
            }
            else
            {
                ////Tracer.WriteLine(ref _log, "virtualPath is: " + virtualPath);
                destinationPath = Server.MapPath(virtualPath);
                ////Tracer.WriteLine(ref _log, "destinationPath is: " + destinationPath);

                // Save the file & fetch the session ID as this will be used as part of the upload key
                uploader.SaveAs(destinationPath);
                ////Tracer.WriteLine(ref _log, "_sessionID is: " + _sessionID);
            }

            ////string destinationPath = Server.MapPath(virtualPath);
            ////uploader.SaveAs(destinationPath);
            //if(string.IsNullOrEmpty(_uploadCategory.Trim()))
            //// if SessionID is null, setting reimbursement session id by sakthi on  29-Mar-2016
            ////Start
            //if (string.IsNullOrEmpty(Convert.ToString(Session["UploadSessionId"])))
            //{
            //    if (dvFiles != null && dvFiles.Rows.Count > 0)
            //    {
            //        foreach (GridViewRow row in dvFiles.Rows)
            //        {
            //            string theDataKey = dvFiles.DataKeys[row.RowIndex][1].ToString();
            //            if (!string.IsNullOrEmpty(theDataKey.Trim()))
            //            {
            //                this.SessionID = theDataKey;
            //                _sessionID = this.SessionID;
            //                break;
            //            }
            //        }
            //        _sessionID = string.IsNullOrEmpty(_sessionID.Trim()) ? UploadManager.GetNextRequest() : _sessionID;
            //        this.SessionID = _sessionID;
            //    }
            //    else
            //    {
            //        this.SessionID = UploadManager.GetNextRequest();
            //        _sessionID = this.SessionID;
            //    }
            //    ViewState[UploadPublication.Session_RequestID] = this.SessionID;
            //    Session["UploadSessionId"] = this.SessionID;
            //}
            //else{
            //    _sessionID = Convert.ToString(Session["UploadSessionId"]);
            //}

            ////End
            //Response.Write("Step 11 before file to db" + "session : " + Convert.ToString(_sessionID) + "_uploadCategory : " + Convert.ToString(_uploadCategory) + "Destination Path : " + Convert.ToString(destinationPath) + "virtualPath : " + virtualPath + "fileName : " + fileName + "_userID : " + _userID + "<br/>");

            if (_uploadCategory == "Submit Reimbursement")
            {
                try
                {
                    //Response.Write("Step 44 : File Saved DB Status" + Convert.ToString(uploadResult) + "<br/>");
                    //Response.Write("Step 12 afer file to db" + "session : " + Convert.ToString(_sessionID) + "_uploadCategory : " + Convert.ToString(_uploadCategory) + "Destination Path : " + Convert.ToString(destinationPath) + "virtualPath : " + virtualPath + "fileName : " + fileName + "_userID : " + _userID + "<br/>");
                    string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
                    int _dbTimeout = WebPublication.DBConnectionTimeout();
                    DataParameter dpSessionID = new DataParameter(System.Data.SqlDbType.VarChar, "sessionID", _sessionID, System.Data.ParameterDirection.Input);
                    DataParameter dpUploadedSourceID = new DataParameter(System.Data.SqlDbType.VarChar, "uploadedSourceID", _uploadCategory, System.Data.ParameterDirection.Input);
                    DataParameter dpDocName = new DataParameter(System.Data.SqlDbType.NVarChar, "docName", _sessionID + "_" + fileName, System.Data.ParameterDirection.Input);
                    DataParameter dpFriendlyDocName = new DataParameter(System.Data.SqlDbType.NVarChar, "friendlyDocName", fileName, System.Data.ParameterDirection.Input);
                    DataParameter dpFilePath = new DataParameter(System.Data.SqlDbType.NVarChar, "filePath", destinationPath, System.Data.ParameterDirection.Input);
                    DataParameter dpVirtualPath = new DataParameter(System.Data.SqlDbType.NVarChar, "virtualPath", virtualPath, System.Data.ParameterDirection.Input);
                    DataParameter dpFileSize = new DataParameter(System.Data.SqlDbType.Decimal, "fileSize", fileSize, System.Data.ParameterDirection.Input);
                    DataParameter dpUserID = new DataParameter(System.Data.SqlDbType.VarChar, "userID", _userID, System.Data.ParameterDirection.Input);
                    DataParameter[] dpBasket = new DataParameter[] { dpSessionID, dpUploadedSourceID, dpDocName, dpFriendlyDocName, dpFilePath, dpVirtualPath, dpFileSize, dpUserID };
                    DataManager.ExecuteStoredProcedureNonQuery(_connection, UploadPublication.spBupa_OSInsertUpload, ref dpBasket, _dbTimeout);
                    uploadResult.Result = true;

                    //Response.Write("File Saved" + Convert.ToString(uploadResult.Result) + "<br/>");

                }
                catch (Exception)
                {
                    uploadResult.Result = false;
                }
               
            }
            else
            {
                uploadResult = _uploadManager.Add(_sessionID, _uploadCategory, destinationPath, virtualPath, fileName, _userID);
            }

           
            

           

          

            //Logger.Current.Information("adding file to db");

            //Logger.Current.Information(Convert.ToString(_sessionID));

            ////Tracer.WriteLine(ref _log, "uploadResult is: " + uploadResult.Result);
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }

        Logger.Tracer.WriteMemberExit();
        return uploadResult;
    }


    private UploadResult SaveFileToFolder(string fileName, string Folder)
    {
        Logger.Tracer.WriteMemberEntry();
        fileName = fileName.Trim();
        Folder = Folder.Trim();
        UploadResult uploadResult = new UploadResult();
        uploadResult.Result = false;

        try
        {
            // Fetch the filename and save to the server
            ////Tracer.WriteLine(ref _log, "fileName is: " + fileName);

            // Get a random name and append to the file
            int seed = DateTime.Now.Millisecond;
            ////Tracer.WriteLine(ref _log, "seed is: " + seed);

            string uniquePrefix = WebPublication.GenerateUniqueID;
            ////Tracer.WriteLine(ref _log, "uniquePrefix is: " + uniquePrefix);

            // Go ahead and save to the server for the moment
            string virtualPath = "~/" + Folder + "/" + uniquePrefix + "_" + fileName;
            ////Tracer.WriteLine(ref _log, "virtualPath is: " + virtualPath);

            string destinationPath = Server.MapPath(virtualPath);
            ////Tracer.WriteLine(ref _log, "destinationPath is: " + destinationPath);

            // Save the file & fetch the session ID as this will be used as part of the upload key
            uploader.SaveAs(destinationPath);
            ////Tracer.WriteLine(ref _log, "_sessionID is: " + _sessionID);
            uploadResult = _uploadManager.Add(_sessionID, _uploadCategory, destinationPath, virtualPath, fileName, _userID);
            ////Tracer.WriteLine(ref _log, "uploadResult is: " + uploadResult.Result);
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }

        Logger.Tracer.WriteMemberExit();
        return uploadResult;
    }

    private DataSet ShowUploadedList()
    {
        Logger.Tracer.WriteMemberEntry();

        DataSet ds = null;
        dvFiles.DataSource = null;
        try
        {

            //Logger.Current.Information("inside show upload");

            //Response.Write("Step 13 : inside show upload <br/>");
            // Get the table with the latest uploaded fileset
            ds = _uploadManager.GetUploadedFileSet(_sessionID, _uploadCategory);


            ////Logger.Current.Information(Convert.ToString(_sessionID) + "sdghdghsd" + Convert.ToString(_uploadCategory) );

            ////Logger.Current.Information(Convert.ToString(ds.Tables[0]) );

            ////Logger.Current.Information(Convert.ToString(ds.Tables[0].Rows.Count));

            //Response.Write("Step 14 : Session : " + Convert.ToString(_sessionID) + "Upload Category : " + Convert.ToString(_uploadCategory) + "<br/>");

            //Response.Write("Step 15 : DataSet : " + Convert.ToString(ds.Tables.Count)  + "<br/>");

            //Response.Write("Step 16 :Table : " + Convert.ToString(ds.Tables[0].Rows.Count)  + "<br/>");



            ////dvFiles.DataSource = ds;
            dvFiles.DataSource = ds.Tables[0];
            dvFiles.DataBind();

            dvFiles.Visible = true;
            _uploadedFileSet = ds;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }

        Logger.Tracer.WriteMemberExit();
        return ds;

    }

    private bool IsAllowedExtension(string file)
    {
        Logger.Tracer.WriteMemberEntry();

        bool allowed = false;
        try
        {
            // Allowed extensions are a comma separated list
            string fileExtension = file.Substring(file.LastIndexOf(".") + 1);
            ////Tracer.WriteLine(ref _log, "fileExtension is: " + fileExtension);

            if (_allowedExtensions.ToLower().Contains(fileExtension.ToLower()))
                allowed = true;
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            //throw;
        }
        ////Tracer.WriteLine(ref _log, "allowed is: " + allowed);

        Logger.Tracer.WriteMemberExit();
        return allowed;
    }

    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        try
        {
            // Setup the mail message
            MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

            // Deal with the attachments
            if (attachmentPaths != null)
            {
                foreach (string path in attachmentPaths)
                {
                    // Create the attachment
                    try
                    {
                        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(path));

                        // Add the attachment
                        mail.Attachments.Add(attachment);
                    }
                    catch (Exception ex) { }////Tracer.WriteLine(ref _log, "error attachments  is: " + ex.Message); }
                }
            }

            string smtpHost = WebPublication.EmailRelayAddress();
            int smtpPort = WebPublication.EmailRelayPort();

            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
            mail.IsBodyHtml = true;
            smtp.Send(mail);
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
    }


   

    public string[] SupportDocuments(string uploadSessionID, string function)
    {
        string[] _supportDocuments;
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);
        int _count = 0;
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            _supportDocuments = new string[d.Tables[0].Rows.Count];
            WebPublication _webPub = new WebPublication();
            foreach (DataRow r in d.Tables[0].Rows)
            {

                string attachPath = r["VirtualPath"].ToString();
                attachPath = attachPath.Replace("/", "\\");
                attachPath = attachPath.Replace("~", "");
                attachPath = attachPath.Replace("\\Uploads\\", "\\");

                _supportDocuments[_count] = _webPub.DocLocation()  +  attachPath;
                _count++;
            }
            return _supportDocuments;
        }
        else
        {
            return new string[0];
        }

    }


    public PreauthDoc_DN[] preauthSupportDocuments(string uploadSessionID, string function, long Transaction)
    {
        
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(uploadSessionID, function);
        OS_DXC_WAP.CaesarWS.PreauthDoc_DN[] _supportDocuments = new OS_DXC_WAP.CaesarWS.PreauthDoc_DN[0];
        int _count = 0;
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            _supportDocuments = new PreauthDoc_DN[d.Tables[0].Rows.Count];
            WebPublication _webPub = new WebPublication();
            foreach (DataRow r in d.Tables[0].Rows)
            {

                string attachPath = r["VirtualPath"].ToString();
                attachPath = attachPath.Replace("/", "\\");
                attachPath = attachPath.Replace("~", "");
                attachPath = attachPath.Replace("\\preauthUpload\\", "\\");
                _supportDocuments[_count] = new PreauthDoc_DN();
                _supportDocuments[_count].DOC_PATH = _webPub.DocLocation() + attachPath;
                _supportDocuments[_count].sql_type = "CSR.PREA_REQ_DOC01";
                _supportDocuments[_count].TXN_ID = Transaction;
                _count++;
            }
            return _supportDocuments;
        }
        else
        {
            return null;
        }

    }

    public void ClaerUploader()
    {
        try
        {
            dvFiles.DataSource = null;
            dvFiles.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }
}