<%@ Page language="c#" Inherits="AsynchronousProcessing.ProcessingMessage" Codebehind="MemberVerify.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Submit Long Process Page</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<font style="font-family:Arial,Verdana;font-size:9pt;font-weight:bold;">Enter the length of time in seconds for a hypothetical process.<br />
            </font>&nbsp;<asp:TextBox CssClass="textbox" ID="txtTrans" runat="server"></asp:TextBox>
            <br>
			
        <table style="width: 579px"><tr><td style="width: 350px"> 
        <asp:TextBox CssClass="textbox" ID="txtMembershipNo"  runat="server"  ValidationGroup="Membersearch" Width="273px" >4436187</asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMembershipNo"
                Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" Font-Names="Verdana"
                Font-Size="Small" ValidationGroup="Membersearch">Please type in the membership No.</asp:RequiredFieldValidator></td>
        <td style="width: 94px">
            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlReferalLetter" runat="server">
                <asp:ListItem Value="0">-Select-</asp:ListItem>
                <asp:ListItem Value="Y">Yes</asp:ListItem>
                <asp:ListItem Selected="True" Value="N">No</asp:ListItem>
            </asp:DropDownList></div></td>
        <td>
            &nbsp;</td></tr>
            <tr>
                <td colspan="2">
                    &nbsp;<div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlDepartment" runat="server"              Width="422px">
                        <asp:ListItem>-Select-</asp:ListItem>
                        <asp:ListItem Value="CVC">Cardio-Vascular Clinic</asp:ListItem>
                        <asp:ListItem Value="DEN">Dental Clinic</asp:ListItem>
                        <asp:ListItem Value="DVC">Derma &amp; Venereal Clinic</asp:ListItem>
                        <asp:ListItem Value="DEC">Diabetes &amp; Endocrinology Clinic</asp:ListItem>
                        <asp:ListItem Value="ENT">E.N.T. Clinic</asp:ListItem>
                        <asp:ListItem Value="GFM">General Practitioner/Family Medicine Clinic</asp:ListItem>
                        <asp:ListItem Value="GEC">G.I &amp; Endoscopies Clinic</asp:ListItem>
                        <asp:ListItem Value="IMC">Internal Medicine Clinic</asp:ListItem>
                        <asp:ListItem Value="NPC">Neuro &amp; Psychiatric Clinic</asp:ListItem>
                        <asp:ListItem Value="NSC">Neuro-Surgery Clinic</asp:ListItem>
                        <asp:ListItem Value="OBG">Obs &amp; Gynae Clinic</asp:ListItem>
                        <asp:ListItem Value="OGP">Obs &amp; Gynae Clinic &amp; Pregnancy related</asp:ListItem>
                        <asp:ListItem Value="OPH">Ophthalmic Clinic</asp:ListItem>
                        <asp:ListItem Value="OER">Ophthalmic Clinic &amp; Error of Refraction</asp:ListItem>
                        <asp:ListItem Value="ORT">Orthopedics Clinic</asp:ListItem>
                        <asp:ListItem Value="PUL">Pulmonary Clinic</asp:ListItem>
                        <asp:ListItem Value="PED">Pediatric Clinic</asp:ListItem>
                        <asp:ListItem Value="PHA">Pharmacy / medication</asp:ListItem>
                        <asp:ListItem Value="SUR">Surgery Clinic</asp:ListItem>
                        <asp:ListItem Value="URO">Urology Clinic</asp:ListItem>
                    </asp:DropDownList></div><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDepartment"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" Font-Names="Verdana"
                        Font-Size="Small" ValidationGroup="Membersearch">Department is Mandatory</asp:RequiredFieldValidator></td>
                <td>
                    </td>
            </tr>
            <tr>
                <td style="width: 350px">
                
        
                
                </td>
                <td style="width: 94px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 350px">
                </td>
                <td style="width: 94px">
                </td>
                <td>
                    </td>
            </tr>
        </table><br />

			
			
			
			
			<asp:TextBox CssClass="textbox" id="txtProcessLength" runat="server" Width="30px"></asp:TextBox>
			<asp:Button CssClass="submitButton" id="SubmitButton" runat="server" Text="Start Long Process" onclick="SubmitButton_Click"></asp:Button>
		</form>
	</body>
</HTML>
