﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Threading;

using Bupa.OSWeb.Helper;

using log4net;
using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;



using DevExpress.Web.ASPxGridView;

using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Diagnostics;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraGrid;
using Microsoft.Office.Interop.Excel;







public partial class eclaims : System.Web.UI.Page
{
    #region Fields


    public string _UploadCategory = UploadCategory.Submit_eClaims;
    public string _Username = "Unknown Sender";
    public string _InitialRequestID = string.Empty;
    string strClientID;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;

    public string _urlValues = string.Empty;
    public string _containerValue;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (Session["_InitialRequestID"] != null)
                _InitialRequestID = Convert.ToString(Session["_InitialRequestID"]);
        }
        else
        {

            _InitialRequestID = WebPublication.GenerateUniqueID;
            Session["_InitialRequestID"] = _InitialRequestID;
        }
        
    }
    
}