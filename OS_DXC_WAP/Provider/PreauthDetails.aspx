﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Provider_PreauthDetails" Codebehind="PreauthDetails.aspx.cs" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
   <title>Untitled Page</title>
     <script type="text/javascript" src="scripts/jquery.js"></script>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
    <script type="text/javascript"  src="js/jquery.dataTables.js"></script>


   <style type="text/css">
        @media print {
        .noPrint { display:none;}
        .Hidden { display:inline;}
        }
        .Hidden { display:none;}
    </style>
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print" />
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen" />
    <style type="text/css" media="print">
        body { font: 16px Arial; }
    </style>
    <style type="text/css" title="currentStyle">
		.style1
        {
            font-weight: normal;
        }
        .style3
        {
            font-weight: normal;
            font-size:12pt;
            font-family: Arial, sans-serif;
            text-align: right;
            direction:rtl;
        }
        .style5
        {
            font-weight: normal;
            font-size: 12pt;
            font-family: Arial, sans-serif;
            text-align: right;
            direction: rtl;
            width: 6px;
        }
    </style>

<script type='text/JavaScript'>

    function newWindow(url) {
        popupWindow = window.open(url,
                                  'popUpWindow',
                                  'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');
    }

    function modalWin(url) {

        if (window.showModalDialog) {
            window.showModalDialog(url, "name", "dialogWidth:900px;dialogHeight:600px");
        }
    }

    var popupWindow = null;

    function child_openurl(url) {

        popupWindow = window.open(url, "_blank", 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');

    }

    function Refresh() {
        setTimeout("location.reload(true);", 1500);

    }


    function parent_disable() {
        if (popupWindow && !popupWindow.closed)
            popupWindow.focus();
    }


    // <![CDATA[
    function ShowLoginWindow() {
        pcLogin.Show();
    }
    function ShowCreateAccountWindow() {
        pcCreateAccount.Show();
        tbUsername.Focus();
    }
    </script> 
    
    
</head>
<body id="test1" style="width:99%;">
  <div class="adbanner" align="right" style="background-color:white;">
 
         <asp:Image runat="server" ID="imgLogo" ImageUrl="~/images/logo-new.jpg" width="100" height="100"/>      
        <br>
        <span style="font-size: 8pt; font-family: Arial">Customer Service Tel No. 800 244 0307 </span>
        <hr>
    </div>
    
   <form id="form1" runat="server">     
  <div class="noPrint" align="left">
        <table width=97% ><tr><td align=left>
            <span style="font-size: 12px;font-family: Arial">
            <table width="730px">
            <tr>
            <td><span style="font-family: Arial"><span style="font-size: 11pt">
            <label ID="Label1" runat="server"  ><font size=4>Pre-Authorisation Reply Form</font></label>  
            </span></span></td>
            <td style="direction: rtl" align="right" ><span style="font-family: Arial"><span style="font-size: 11pt">
            <label ID="Label3" ><font size=4 style="text-align: right">نموذ ج الرد على طلب موافقة </font></label>  
            </span></span></td>
            </tr>
            </table>
            
            
            </span>
        </td>
            <td><a href="#" onclick="window.print()">
                <img class="noPrint" border="0" src="images/printer.gif" alt="Print" width="16" height="16" ></a>
                
<%--        <a href="#" onclick="printSelection(document.getElementById('form1'));return false;">print</a>
--%>

                
                <%-- <img alt="Go back" visible="false"  style="cursor:pointer;" src="../icons/back4.png" onClick="history.go(-1)"  />--%>
          
                   <asp:HyperLink ID="lnkBackOption" runat="server"
            Font-Size="Large" NavigateUrl="javascript:history.go(-1)" Visible="true">Go-Back</asp:HyperLink>

            </td></tr></table>
 






  
  
  <table  border="0">
    <tr>
        <td align="left"><asp:Button ID="btnFollowUpRequest" runat="server"  Text="Follow-Up Request" Width="130"  OnClientClick="return confirm('Are you sure you like to follow-up this request?');" OnClick="btnFollowUpRequest_Click" /></td>
        <td align="left"><asp:Button ID="btnCancelRequest" runat="server"  Text="Cancellation Request" Width="130" OnClick="lnkCancelRequest_Click"  OnClientClick="return confirm('Are you sure you want to cancel this request?');" /></td>
        <td align="left"><input type="button" id="txtRenew"  runat="server" onfocus="this.blur();"  name="txtTeethMap"  value="Renewal Request "  Width="130"   onclick="toggleDiv();" style="width: 146px" /></td>
        <td align="left"><input type="button" id="btnDispensed" style="<%= _View %>" value="Dispensed" onclick="child_openurl('dispensed.aspx?PAID=<%=_PAID %>&UserID=<%=_Username%>&PID=<%= _PROID %>');"/></td>
    </tr>
  </table>



  <div id="myDivBox" style="display: none; position: static; left: 152px; background-color: #e6f5ff;"> 
  

<table border=0><tr><td style="width:261px;"><nobr><font size=2><b>Please input new treatment date for renewal</b></font></nobr></td><td  style="width:201px; text-align: left;">
<uc1:Date2 ID="txtRenewalDate" runat="server" />
     </td><td  style="width:102px;">  <asp:Button ID="btnRenewRequest" runat="server"  Text="OK" OnClick="btnRenewRequest_Click"  OnClientClick="toggleDiv();" />
<input id="1" type="button" value="Cancel"   onclick="toggleDiv();" /></td></tr></table>
</div>
<div id="CancelList"  visible="false" style=" font-size:10px; font-family:Verdana; height:auto; width: 100%; overflow:hidden;" runat="server" >   </div>
 

</div>

 

        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        
  <%--       <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <%=DateTime.Now.ToString() %>
            <asp:Panel ID="Panel2" runat="server">
                <asp:Image ID="Image2" runat="server" ImageUrl="spinner.gif" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
<asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel2"  ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <img src="../images/spinner.gif" width="33" height="33" />
                Updating Page ......
            </ProgressTemplate>
        </asp:UpdateProgress>
     
     
     
         <asp:UpdatePanel ID="UpdatePanel2"  runat="server"   Visible="true" >
        <ContentTemplate >
                    <font size="3">

<div id="reportborder" runat="server" style="border:1px;">
   
            <fieldset style="border-right: medium none; border-top: medium none; border-left: medium none; width: 95%;
                            border-bottom: medium none; border-style: solid; border-width: 1px; font-size:13px;">
                            <table border="0" cellpadding="2" cellspacing="2" style="font-size: small; font-family: Verdana; " width="100%">
                                <tr>
                                    <td align="left" class="th">
                                        Provider Name:<br />اسم مزود الخدمة:</td>
                                    <td align="left">
                                        <asp:Label ID="lblProviderName" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Insured Name:<br />اسم المؤمن عليه:</td>
                                    <td align="left" colspan="3">
                                        <asp:Label ID="lblInsuredName" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        ID. Card:<br />رقم الهوية الوطنية / الاقامة</td>
                                    <td align="left">
                                        <asp:Label ID="lblIDCard" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="th">
                                        Insurance Co:<br />شركة التأمين:</td>
                                    <td align="left">
                                        <asp:Label ID="lblInsurance_Co" runat="server" Text=" "></asp:Label><br /> بوبا العربية</td>
                                    <td align="left" class="th">
                                        Age:<br />العمر:</td>
                                    <td align="left">
                                        <asp:Label ID="lbAge" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Sex:<br />الجنس:</td>
                                    <td align="left">
                                        <asp:Label ID="lblSex" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        Policy Holder:<br />صاحب الوثيقة:</td>
                                    <td align="left">
                                        <asp:Label ID="lblPolicyHolder" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="th">
                                        Patient File No.:<br />رقم ملف المريض:</td>
                                    <td align="left">
                                        <asp:Label ID="lblPatientFileNo" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Dept:<br />القسم:</td>
                                    <td align="left">
                                        <asp:Label ID="lblDept" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Policy No.:<br />رقم العقد:</td>
                                    <td align="left">
                                        <asp:Label ID="lblPolicyNo" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        Class:<br />درجة التغطية:</td>
                                    <td align="left">
                                        <asp:Label ID="lblClass" runat="server" Text=" "></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="th">
                                        Provider Fax No.:<br />رقم فاكس مزود الخدمة:</td>
                                    <td align="left">
                                        <asp:Label ID="lblProviderFaxNo" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Date of Visit:<br />تاريخ الزيارة:</td>
                                    <td align="left">
                                        <asp:Label ID="lblDateOfVisit" runat="server" Text=" "></asp:Label></td>
                                    <td align="left" class="th">
                                        Expiry Date:<br />تاريخ انتهاء التغطية:</td>
                                    <td align="left">
                                        <asp:Label ID="lblExpiryDate" runat="server" Text=" "></asp:Label></td>
                                </tr>
                            </table>
                        </fieldset><br />
                   
            <fieldset   style="font-size: meduim; width: 95%; font-family: Verdana; border: 1px solid #000000;">
                            <table cellpadding="0" cellspacing="0" height="100%">
                                <tr>
                                    <td style="width:50%; height: 118px;"  valign="top">
                                                <font style="font-size: 10px"><span style="font-size: 9pt; font-family: Verdana" 
                                                        class="style1">
                                           Reference to your pre-authorisation request (page 1) for our member (details listed above). We, Bupa Arabia replying on the membership and limited medical information supplied provided in your request took the decision mentioned below: </span></font>
                                                </td>

                                                <td valign="top" class="style3"  style=" padding-right:5px;  height: 118px">
                                               <div style="border: solid 0 #000; border-left-width:1px; height:100%; padding-left:0.5ex">
    
                                                إشارة إلى طلب الموافقة 
                                                    الخاص بكم (صفحة 1) لعميلنا صاحب التفاصيل المذكورة أعلاه، وبناءاً على المعلومات 
                                                    الطبية المحدودة والمقدمة من قبلكم، قررنا نحن بوبا العربية إتخاذ القرار المذكور 
                                                    أدناه</span>
                                                    </div>
                                                    </td>
                                </tr>
                            </table>
                        </fieldset><br />
                  
            <fieldset style="font-size: 13px;  width: 95%; font-family: Verdana; border: 1px solid #000000;">
                            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                <tr>
                                    <td align="left" colspan="3" valign="top">
                                        &nbsp; 
                                        
                                        <table style="width:100%;">
                                            <tr>
                                               <td valign="top"><b>Pre-authorisation Status: <br />حالة طلب الموافقة:</b></td>
                                                   
                                                <td  valign="top">
                                                    <asp:Label ID="lblPreauthorisationStatus" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th"  valign="top">
                                      <asp:Label ID="lbl_approvalNo" runat="server">Approval No.:<br />رقم الموافقة:</asp:Label></td>
                                    <td align="left"  valign="top">
                                        <asp:Label ID="lblApprovalNo" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td  valign="top"><u><b>Comments:<br />ملاحظات:</b></u>
                                    </td>
                                    <td  valign="top">
                                    <asp:Label ID="lblComments" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th" width="17%"  valign="top">
                                        Approval Validity:<br />مدة صلاحية الموافقة:</td>
                                    <td align="left"  valign="top">
                                        <asp:Label ID="lblApprovalValidity" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" class="th">
                                        Room Type:<br />نوع الغرفة:</td>
                                    <td align="left">
                                        <asp:Label ID="lblRoomType" runat="server"></asp:Label></td>
                                </tr>
                                </table>
                                </fieldset>
                                
             <br />
                               
                               
            <%--<fieldset style="font-size: 13px;  width: 95%; font-family: Verdana; border: 0px solid #000000; height:auto;">
                            <table border="1" cellpadding="2" cellspacing="2" 
                                        style="font-size: 10px; width: 100%; font-family: Verdana; border: 0px solid #000000;">
                                <tr>
                                    <td align="left" class="th" colspan="6" style=' font-size:13px; ' bordercolor=black>
                                        </td>
                                </tr>
                                
                                </table>
                                </fieldset>--%>
                                
           
            
            <fieldset style=" width:95%; border:black; border-width:thin; " >
           
            <table width="100%" border="0" style="border:solid 1px black; border-style: inset ;"><tr><td > 
             <u>Service Required:</u>
            </td></tr>
         <tr>
                                    <td   >
                                <div id="ServiceListReport"  visible="true" style=" font-family:Verdana; ; height:auto;  border:0px; width:100%; " runat="server" >   </div>
                                        <br />
                                    </td>
                                </tr>           
            
                <tr>
                    <td>
                       
                    </td>
                </tr>
            
            </table>
             </fieldset>
            <br />
           
            
            <fieldset   style="font-size:13px;width: 95%; font-family: Verdana; border: 1px solid #000000;">
                            <table border="0" width="100%"  cellpadding="2" cellspacing="2" >
                                <tr>
                                    <td align="left">
                                        <b><u>Additional Comments<br />ملاحظات اضافية</u></b> &nbsp;
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblAdditionalComments" runat="server" Width="352px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        
                                        <table
                                            style="width: 100%;">
                                            <tr>
                                                
                                                <td>
                                                   <b>Insurance Officer<br />مسؤول التامين</b>
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblInsuranceOfficer" runat="server" Width="152px"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                    <td >
                                        
                                        
                                        <table
                                            style="width: 400px;">
                                            <tr>
                                                
                                                <td><b>Date &amp; Time<br />لوقت والتاريخ:</b> </td>
                                                <td><asp:Label ID="lblDateTime" runat="server" Width="152px"></asp:Label></td>
                                                </tr>
                                                </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
           <br />
            <br />  

           <br />
            <fieldset   style="font-size:13px;width: 95%; font-family: Verdana; border: 1px solid #000000;">
                            <table border="0" width="100%"  cellpadding="2" cellspacing="2" >
                                <tr>
                                    <td align="left">
                                        <b><u>Above decision based on the information received:<br /></u></b> &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td colspan="6">
                                                   <b> Member Detail </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Member Name
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblMemberName" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Card Issue No:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCardIssueNo" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Mobile No:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMobileNo" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Membership No:
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblMembershipNo" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                   Member Id/Iqama
                                                </td>
                                                <td>
                                                   <asp:Label ID="lbiIdIqamaNo" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Patient File No:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPatientFileNo2" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Contract No:
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblContractNo" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                   
                                                </td>
                                                <td>
                                                   
                                                </td>
                                                <td>
                                                     Verification ID:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblVerificationId" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6"><br /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                   <b> Provider Detail </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Provider Code
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblProviderCode" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Physician Name:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPhysicianName" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Fax No:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFaxNo" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6"><br /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                   <b> Treatment Detail </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Chief Complaints and main symptoms
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblMainSymptoms" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Diagnosis Code:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDiagnosisCode" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Diagnosis Desc:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDiagnosisDesc" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Treatment Type
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblTreatmentType" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Date of Admission:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDateOfAdmission" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Quantity:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblQuantity" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Department Type
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblDepartmentType" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Length of Stay:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLengthOfStay" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Estimated Amount:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEstimatedAmount" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>

                                          <tr>
                                                <td>
                                                   Last Menstrual Period in Hijri
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblLastMenstrualPeriodHijri" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Expected Delivery in Hijri
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblExpectedDeliveryHijri" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                          <tr>
                                                <td>
                                                   Last Menstrual Period in Gregorian
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblLastMenstrualPeriodGregorian" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Expected Delivery in Gregorian
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblExpectedDeliveryGregorian" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6"><br /></td>
                                            <tr>
                                                <td colspan="6">
                                                   <b> Maternity Detail </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Exemptions
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblExemptions" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Referral:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblReferral" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Chronic:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblChronic" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   RTA
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblRTA" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Infertility:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblInfertility" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Work Related:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblWorkRelated" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Gravida
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblGravida" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Para:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPara" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Live:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLive" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                   Possible line of treatment
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblPossibleLineOfTreatment" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                     Other Conditions:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblOtherConditions" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                   Check-up
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblCheckUp" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Congenital:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCongenital" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Psychiatric:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPsychiatric" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Vaccination
                                                </td>
                                                <td>
                                                   <asp:Label ID="lblVaccination" runat="server" ></asp:Label>
                                                </td>
                                                <td>
                                                    Blood Pressure:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBloodPressure" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Pulse:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPulse" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Maternity Type
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMaternityType" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    Abortion:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblAbortion" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Death:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDeath" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                                <tr>
                                                <td>
                                                   
                                                </td>
                                                <td>

                                                </td>
                                                <td>
                                                    Temperature:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTemperature" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                     Duration of Illness:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDurationOfIllness" runat="server" Text=""></asp:Label>
                                                </td>

                                             </tr>

                                            <tr>
                                                <td colspan="6"><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    
                                                    <asp:GridView ID="gvServices" runat="server" BackColor="White" 
                                                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
                                                        CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="800px">
                                                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                                        <RowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                        <SortedDescendingHeaderStyle BackColor="#242121" />
                                                    </asp:GridView>

                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
           <br />
            <br />  

            <fieldset style="font-size: 10px; width: 95%; font-family: Verdana; border: 1px solid #000000; height: 100%;">
                            <table   border="0" cellpadding="2" cellspacing="2" width="100%" height="100%">
                                <tr>
                                    <td valign="top" style="width:45%">
                                        <div align="left">
                                        <br />
                                <font style="font-size: 10px"><b><span style="font-family: Verdana">Kindly note:&nbsp; This approval is subject to the terms and conditions of the signed agreement
                                 including agreed package prices and price list and the customer policy limits and exclusions. Further, BUPA Arabia confirms
                                  cover for the member's treatment as specified in the coverage details field based on the limited information
                                   supplied to us during pre-authorisation. BUPA Arabia reserves the right to fully or partially deny the payment
                                    for any of the above treatment during the claim processing stage in case one of the following reasons (which
                                     does not constitute a numerous clauses of events) becomes apparent: 
                                </span></b>
                                    <ol type="1">
                                        <li><span style="font-family: Verdana">If the diagnosis, treatment or any other material fact alters from those disclosed
                                            during pre-authorisation </span></li>
                                        <li><span style="font-family: Verdana">If the line of treatment is not according to internationally recognised medical
                                            standards and in line with the M.O.H approved practices </span></li>
                                        <li><span style="font-family: Verdana">In case of forgery </span></li>
                                    </ol>
                                </font>
                            </div>
                                        </td>
                                    <td style="direction: rtl; vertical-align:top" class="style5" >
                                        &nbsp;</td>
                                   
                                    <td class="style3" style="direction: rtl; vertical-align:top; height: 100%;" height="100%">
                                    <div style="border: solid 0 #000; border-left-width: 1px; height: 100%; padding-right: 0.5ex; padding-left:0.5ex">
                                        يرجى الملاحظة أن هذه الموافقة تخضع لشروط وأحكام الاتفاقية الموقعة بين الطرفين بما في ذلك أسعار 
                                        الاتفاقية الشاملة المتفق عليها ، وقوائم الأسعار بما في ذالك حدود تغطية العملاء 
                                        والاستثناءات.كذلك، تؤكد بوبا العربية تغطيتها لعلاج العضو بما تم ادراجه في خانة 
                                        الحالات المدرجة ضمن التغطية إستناداً الى المعلومات المحدودة والتي تم إفادتنا بها 
                                        خلال طلب الموافقة على الخدمات الطبية.تحتفظ بوبا العربية بحق الرفض الكلي او 
                                        الجزئي بخصوص سداد تكاليف الخدمات الطبية عند المطالبة بها اذا ما توفر أحد الاسباب التالية 
                                        ( التي لا تشكل بنود عديدة من الأحداث ) :<br />
                                        <ul type="1">
                                            <li>اذا كان التشخيص او العلاج او اي حقائق اخرى مخالفة لما ورد في طلب الموافقة.</li><li>عدم تطابق أو توافق العلاج او الخدمة الطبية الممنوحة مع المعايير المتعارف عليها 
                                                دوليا او مخالفته لما نصت عليه لوائح و قوانين وزراة الصحة السعودية.</li><li class="style3">في حالة الاحتيال او التزوير.</li></ul>
                                        </div>
                                    </td>
                                   
                                </tr>
                            </table>
                        </fieldset>
                        
                        
                        
                        <div align="center">
                            <h6>
                                <span style="font-size: 8pt; font-family: Verdana">
                                Saudi Medical Insurance Standardization - United Claim &amp; Approval Form (SMIS-UCAF
                                1.0)</span></h6>
                        </div>
                               
                               </div>
</font>

        </ContentTemplate>
        </asp:UpdatePanel>
                
        
        <asp:Label ID="Message1" runat="server" Font-Size="Small" Height="15px" Text="" Width="527px"></asp:Label><br />

       <br />
       
         
            <asp:TextBox CssClass="textbox" ID="txtTxnID" runat="server" Visible="False"></asp:TextBox>
            &nbsp; &nbsp;
            <asp:TextBox CssClass="textbox"   ID="txtMbrShp" runat="server" Visible="False"></asp:TextBox>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            <br />
            <br />
           
 
            &nbsp;<br />
          
       
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
               <asp:UpdatePanel runat="server">
                   <ContentTemplate>
                       <asp:LinkButton ID="lnkSession" runat="server" />
                       <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                           OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                       </ajaxToolkit:ModalPopupExtender>
                       <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                           <div class="header">
                               <table style="width: 100%" cellpadding="0" cellspacing="0">
                                   <tr>
                                       <td style="text-align: left">
                                           <strong style="font-size: larger">Session Expiring! </strong>
                                       </td>
                                       <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                   </tr>
                               </table>
                           </div>

                           <div class="body">
                               <table cellpadding="0" cellspacing="0" style="width: 100%">
                                   <tr>
                                       <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                   </tr>
                               </table>
                           </div>

                           <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                       </asp:Panel>
                       <div id="mainContent">
                       </div>
                   </ContentTemplate>
               </asp:UpdatePanel>


    </form>
    
    
        <script type="text/javascript" language="javascript">
//            var firstFlag = true;
//            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
//            function pageLoad(){
//                if(firstFlag)  __doPostBack("UpdatePanel2","");
//            }
//            function BeginRequestHandler(sender, args){
//                firstFlag = false;
//            }
//            
            
            
    function toggleDiv()   
    {   
      if (document.getElementById("myDivBox").style.display == "block")   
      {   
        document.getElementById("myDivBox").style.display = "none";   
      }   
      else  
      {   
        document.getElementById("myDivBox").style.display = "block";   
      }   
    }   

            
            
            
    </script>
    
 <script type="text/javascript">

function printSelection(node){

  var content=node.innerHTML
  var pwin=window.open('','print_content','width=1px,height=1px');
alert(content);
  pwin.document.open();
  pwin.document.write('<html><body width="100%" onload="window.print();">'+content+'</body></html>');
  pwin.document.close();
 
  setTimeout(function(){pwin.close();},1000);

}
</script>
    
</body>
</html>
