﻿using Bupa.Core.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Provider_BatchStatment : System.Web.UI.Page
{
    long statementID = 0;
    string statementValue = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            lblMeesage.Text = string.Empty;
            bool isVlaidId = false;
            statementValue = Convert.ToString(Request.QueryString["val"]);
            statementValue = Cryption.Decrypt(statementValue);
            if (!string.IsNullOrEmpty(statementValue))
            {
                if (statementValue.All(char.IsDigit))
                {
                    statementID = Convert.ToInt64(statementValue.Trim());
                    isVlaidId = true;
                   
                }
            }
            if(isVlaidId)
                GetStatment();
            else
                lblMeesage.Text = "Invalid statement Id";
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            lblMeesage.Text = "Unable to view provider statement";
        }
      
       
    }

    private void GetStatment()
    {
        try
        {
            OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
            OS_DXC_WAP.CaesarWS.UpdStmViewRequest_DN request;
            OS_DXC_WAP.CaesarWS.UpdStmViewResponse_DN response;
            request = new OS_DXC_WAP.CaesarWS.UpdStmViewRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            long ls_txnid = TransactionManager.TransactionID();
            request.transactionID = ls_txnid;
            request.stmID = statementID;///statementID;
            response = ws.UpdStmView(request);

            if (response != null)
            {
                if (response.status == "0")
                {
                    if (!string.IsNullOrEmpty(response.stmPath))
                    {
                       var input = response.stmPath;
                        string fileName = string.Empty;
                        foreach (var item in Regex.Split(input, @"(?<=CL\\(?=[^\\]+))"))
                        {
                            fileName = item;
                        }

                        if(fileName.Length > 0)
                            Response.Redirect(Convert.ToString(ConfigurationManager.AppSettings["ProviderVirtualPath"]) +  fileName, false);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            
            throw;
        }
    }
}