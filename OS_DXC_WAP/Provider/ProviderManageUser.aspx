﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Templates/Inner.Master" Inherits="Provider_CreateManageUser" Codebehind="ProviderManageUser.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server">
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() == null) {
                if ($get("lblUserError") != null) {
                    var value = $get("lblUserError").value;
                    if ($get("lblUserManageError").innerText == "") {
                        $get("lblUserManageError").innerText = value;
                        $get("lblUserManageError").style.display = "block";
                    }

                    else {
                        $get("lblUserManageError").style.display = "none";
                        $get("lblUserManageError").innerText = "";
                        $get("lblUserError").value = "";
                    }
                    if ($get("txtimgcode").value == "" && $get("lblChkEmail").value != "" && $get("lblChkUser").value != "") {
                        $get("WrongCaptcha").style.visibility = "visible";
                    }
                    else {
                        $get("WrongCaptcha").style.visibility = "hidden";
                    }
                }
            }

        }
   </script>
    <div id="userWrapper" runat="server">
    <h2>Create New User
    </h2>
    <div class="memberLoginComp">
            <table style="width: 100%; border: 0">
                <tr>
                    <td colspan="2">
                        <table style="width: 100%; border: 0">
                            <tr>
                                <td style="text-align: center">
                                    <table style="width: 100%; border: 0">
                                        <tr>
                                            <td colspan="4">
                                                <asp:Label ID="lblUserManageError" runat="server" Text="" ForeColor="Red" Font-Bold="true" Font-Size="Larger"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:UpdatePanel runat="server" ID="updatePanel1">
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="txtUserName" />
                                                    </Triggers>
                                                    <ContentTemplate>
                                                        <table width="100%" style="border: 0" cellpadding="0" cellspacing="0">
                                                            <tr style="padding-left: 55px; float: left">
                                                                <td valign="top"><span style="color: #ff3300"><strong>*</strong></span>User Name:</td>
                                                                <td valign="top">
                                                <asp:TextBox ID="txtUserName" runat="server" MaxLength="20" AutoPostBack="true" OnTextChanged="txtUserName_TextChanged"></asp:TextBox>
                                                <asp:Label ID="lblChkUser" runat="server" Style="color: red" Visible="false"></asp:Label>
                                                <asp:RequiredFieldValidator ID="ReqUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="Field is Required" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regexpName" runat="server" ErrorMessage="Invalid UserName. User Name must be min 6 characters long and only following special characters are allowed - _" ControlToValidate="txtUserName" ValidationExpression="^[A-Za-z0-9_-]{6,20}$" 
                                                    Display="Dynamic" ValidationGroup="Submit"/>
                                            </td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            
                                        </tr>
                                        <tr style="padding-left: 51px; float: left;">
                                            <td valign="top" style="text-align: right"><span style="color: #ff3300"><strong>*</strong></span>First Name:</td>
                                            <td valign="top" style="text-align: left;">
                                                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td valign="top" style="width: 154px; text-align: left;">
                                                <asp:RequiredFieldValidator ID="ReqFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="Field is Required" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                            <td valign="top" style="text-align: right"><span style="color: #ff3300"><strong>*</strong></span>Last Name :</td>
                                            <td valign="top" style="text-align: left;">
                                                <asp:TextBox ID="txtLastName" runat="server" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td valign="top">
                                                <asp:RequiredFieldValidator ID="ReqLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Field is Required" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr style="padding-left: 12px; float: left;">
                                            <td valign="top" colspan="2">
                                                <asp:UpdatePanel runat="server" ID="updatePanel3">
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="txtEmailId" />
                                                    </Triggers>
                                                    <ContentTemplate>
                                                        <table width="100%" style="border: 0" cellpadding="0" cellspacing="0">
                                                            <tr style="padding-left: 55px; float: left">
                                            <td valign="top" style="text-align: right"><span style="color: #ff3300"><strong>*</strong></span>Email Id :</td>
                                            <td valign="top" style="text-align: left;">
                                                <asp:TextBox ID="txtEmailId" runat="server" MaxLength="50" AutoPostBack="true" OnTextChanged="txtEmailId_TextChanged"></asp:TextBox>
                                                                </td>
                                                                <td valign="top" style="width: 132px; text-align: left;padding-left:4px;">
                                                <asp:Label ID="lblChkEmail" runat="server" Style="color: red;" Visible="false"></asp:Label>
                                                <asp:RequiredFieldValidator ID="ReqEmailId" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Field is Required" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegExpEmailId" runat="server" ErrorMessage="Not a valid email address" ControlToValidate="txtEmailId" Display="Dynamic" ValidationGroup="Submit" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="true"></asp:RegularExpressionValidator>
                                            </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td valign="top" style="text-align: right"><span style="color: #ff3300"><strong>*</strong></span>Mobile Number :</td>
                                            <td valign="top" style="text-align: left;">
                                                <asp:TextBox ID="txtMobileNumber" runat="server" MaxLength="10"></asp:TextBox><br />
                                                <span style="color: #ff3300"><strong>05xxxxxxxx</strong></span><br />
                                            </td>
                                            <td valign="top">
                                                <asp:RequiredFieldValidator ID="ReqMobNumber" runat="server" ControlToValidate="txtMobileNumber" ErrorMessage="Field is Required" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator><br />
                                                <asp:RegularExpressionValidator ID="RegMobNumber" runat="server" ValidationGroup="Submit" ControlToValidate="txtMobileNumber" Display="Dynamic" ErrorMessage="Invalid phone number" ValidationExpression="[0][5][0-9]{8}" />
                                            </td>
                                            <td>
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width: 100%; border: 0">
                            <tr runat="server" id="uploaderCtl">
                                <td valign="top">
                                    <table style="width: 100%; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
                                        runat="server" id="tblUploader">
                                        <tr>
                                            <td align="left">
                                                <span style="color: #ff3300"><strong>*</strong></span><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:FileUpload ID="FileUpload1" runat="server" EnableViewState="true" ViewStateMode="Enabled" Style="width: 50%" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button CssClass="submitButton" ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" /><br />
                                                <span>Image Files Only (.jpg, .bmp, .png, .gif, .pdf) 5MB limit per file</span><br />
                                                <asp:RequiredFieldValidator ID="rfvFileUpload1" runat="server"
                                                    ErrorMessage="Please selcet file!" ControlToValidate="FileUpload1" ValidationGroup="Submit"></asp:RequiredFieldValidator><br />
                                                <asp:RegularExpressionValidator runat="server" ID="revFileUpload1" ControlToValidate="FileUpload1"
                                                    ErrorMessage="Image Files Only (.jpg, .bmp, .png, .gif, .pdf)"
                                                    ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.jpeg|.JPEG|.bmp|.BMP|.png|.PNG|.pdf)$" ValidationGroup="Submit" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style4">
                                                <strong>Please attach ID
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="uploadGrid" visible="false">
                                            <td>
                                                <table style="width: 100%; border: 0" cellpadding="0" cellspacing="0">
                                                    <tr style="background-color: #5D7B9D; font-weight: bold; color: #333333; line-height: 28px">
                                                        <td>Document Name
                                                        </td>
                                                        <td colspan="3">Uploaded
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDocumentname" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblUploadDate" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblfileDestination" runat="server" Visible="false"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnDelete" runat="server" Text="Delete?" OnClick="btnDelete_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <asp:UpdatePanel runat="server" ID="updatePanel">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="CaptchaValidate" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <table width="100%" style="border: 0">
                                                <tr>
                                                    <td>
                                                        <div id="divCaptchaImage" runat="server" width="100%">
                                                            <table width="0" style="border: 0">
                                                                <tr>
                                                                    <td valign="top" style="text-align: left; padding-left: 37px;"><span style="color: #ff3300"><strong>*</strong></span>Insert Below Text:</t>
                                                                    <td valign="top" style="text-align: left;">
                                                                        <asp:TextBox ID="txtimgcode" runat="server" onchange="HideCaptchaMessage()"></asp:TextBox><br />
                                                                        <asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" ControlToValidate="txtimgcode" ErrorMessage="Field is Required" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                        <span id="WrongCaptcha" runat="server" style="color: #ff3300; visibility: hidden"><strong>Wrong entry try again</strong></span>
                                                                    </td>
                                                                </tr>
                                                                <table style="border: 0" width="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div id="CaptchaText" runat="server" style="padding-top: 15px; width: 302px; height: 60px; background-image: url('../images/CaptchaBackground.png'); text-align: center; font-size: 40px; color: #61C0E0; font-weight: bold; letter-spacing: 3px; vertical-align: middle; font-family: 'GenericSansSerif'; font-style: italic;">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="CaptchaValidate" runat="server" ImageUrl="../images/icons/reCaptchaImg.jpg" OnClick="CaptchaValidate_Click" Style="width: 81px" />
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="popUpSuccess" runat="server" style="display: none; top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999;" visible="false">
                                                <div style="left: 30%; top: 10%; position: absolute;">
                                                    <table style="width: 100%; background-color: white;">
                                                        <tr style="background-color: #0886D6; line-height: 30px">
                                                            <td colspan="2" style="color: #fff; text-align: left; padding-right: 8px; border-bottom-color: cyan; border-bottom-width: medium; border-bottom-style: outset;">
                                                                <strong style="padding-left: 5px; font-size: larger">BUPA - Online Services</strong>
                                                                <span style="float: right;">
                                                                    <strong style="font-size: x-large">بوبا - خدمات الإلكترونية</strong>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table style="width: 500px; height: 130px; background-color: white; padding: 25px" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td colspan="2" style="font-size: 16px">
                                                                            New user details has been successfully registered. An email has been sent to your registered email and OTP on your mobile. Please check your email and click on the link to verify your email and follow the instructions to activate your account.
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 390px;"></td>
                                                                        <td style="background-color: #0886D6; text-align: center; line-height: 35px; padding-right: 17px; padding-left: 17px;">
                                                                            <a href="../UserManagment.aspx" style="text-decoration: none;color:white">Ok</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table style="width: 100%; border: 0">
                                        <tr>
                                            <td colspan="2">
                                                <div class="Addwidth" id="TermsAndLinks">
                                                    <table style="width: 100%; border: 0">
                                                       <tr>
                                                            <td style="text-align: left; font-weight: bold">Terms and Condition use by health care providers of Bupa Arabia’s electronic services and Cyber Security:
                                                           </td>
                                                            <td style="text-align: right; font-weight: bold">شروط وأحكام استخدام مقدمي الخدمات الصحية  لخدمات بوبا العربية الالكترونية و الامن السيبراني:
                                                           </td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">This Appendix sets forth the terms and conditions of the use of Bupa Arabia's web services to its health care providers in accordance with the regulations of the Saudi Arabian Monetary Agency and the Council of Cooperative Health Insurance. Bupa Arabia has formulated these terms and conditions to protect the clients’ information and to confirm Bupa Arabia's commitment to the privacy of this information when using Bupa web services, this Appendix shall form an integral part of the insurance policy.
                                                           </td>
                                                            <td style="text-align: right">يحدد هذا الملحق شروط و أحكام استخدام خدمات بوبا العربية الالكترونية والتي تقدمها شركة بوبا العربية لمقدمي الخدمات الصحية وفقاً لأنظمة ولوائح مؤسسة النقد العربي السعودي ومجلس الضمان الصحي التعاوني، وقد قامت بوبا العربية بصياغة هذه الشروط والأحكام لحماية معلومات عملاءها وتأكيد التزام بوبا العربية بخصوصية هذه المعلومات عند استخدام خدمات بوبا العربية الالكترونية، و يعد هذا المحلق جزءا لا يتجزأ من وثيقة التأمين
                                                           </td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left; font-weight: bold">1. Definitions:
                                                           </td>
                                                            <td style="text-align: right; font-weight: bold">التعاريف: 
                                                           </td>
                                                            <td style="vertical-align: top">.1</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">1.1. Bupa Online Services: A 24/7 service that allows clients and health care providers to access Bupa Arabia's online services.
                                                           </td>
                                                            <td style="text-align: right">خدمات بوبا الإلكترونية: هي خدمة تقدمها بوبا العربية على مدار الساعة وخلال أيام الأسبوع لعملائها ومقدمي الخدمات الصحية ليتسنى لهم المتابعة والإستفسار عن العمليات التي تمت مسبقاً.
                                                           </td>
                                                            <td style="vertical-align: top">.1.1</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">1.2. The Relevant Regulatory Parties intended here as:<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	Saudi Arabian Monetary Agency. (SAMA)<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	Counsel of Cooperative Health Insurance (CCHI)

                                                           </td>
                                                            <td style="text-align: right">الجهات الرقابية ذات الصلة:<br />
                                                             &nbsp;&nbsp;&nbsp;&nbsp;•	مؤسسة النقد العربي السعودي<br />
                                                             &nbsp;&nbsp;&nbsp;&nbsp;•	مجلس الضمان الصحي التعاوني

                                                           </td>
                                                            <td style="vertical-align: top">.2.1</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">1.3. The Authorized Person: A person authorized to access the online service.
                                                           </td>
                                                            <td style="text-align: right">الشخص المخول: هو الشخص المخول له بالدخول إلى الموقع.
                                                           </td>
                                                            <td style="vertical-align: top">.1.3</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">1.4. Using the Online Service: The usage of the online services is strictly limited to those authorized by the health care provider.
                                                           </td>
                                                            <td style="text-align: right">استخدام الخدمات الإلكترونية: يقتصر استخدام هذه الخدمات على الشخص أو الأشخاص المخولين من قبل مقدم الخدمة الصحية المعتمد.
                                                           </td>
                                                            <td style="vertical-align: top">.1.4</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">1.5. User Account: A unique username and password is issued in benefit of the health care provider.
                                                           </td>
                                                            <td style="text-align: right">حساب المستخدم: سيتم إصدار اسم مستخدم وكلمة سر خاصة لحساب مقدم الخدمة الصحية المعتمد.
                                                           </td>
                                                            <td style="vertical-align: top">.1.5</td>
                                                       </tr>
                                                        <tr>
                                                            <td style="text-align: left">1.6. Services :<br />
                                                            consists of – but not limited to-:<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	Verification: Verify members eligibility and benefits<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	TOB: Checking the table of benefits of  our latest contract<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	Request: New and existing preauthorization request<br />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;•	Notifications: Additional services added in the agreement
                                                                <br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	Statements: Health care provider’s monthly statements<br />

                                                           </td>
                                                            <td style="text-align: right">الخدمات:
                                                                <br />
                                                            تشتمل – عل سبيل المثال لا الحصر- على:<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	التحقق: التحقق من أهلية الاعضاء و المزايا الخاصة بهم.<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	جدول المنافع: التحقق من المزايا المنصوص عليها خلال سريان العقد<br />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;•	الطلب: طلب موافقة مسبقة جديدة او حالية
                                                                <br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	الاخطارات: الخدمات الاضافية المضافة في العقد<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;•	البيانات: البيانات الشهرية لمزودي الخدمه الصحية المعتمدين
                                                           </td>
                                                            <td style="vertical-align: top">.1.6</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">2. Bupa Arabia is not responsible for economic losses or other damages due to technical problems, network Load or service breaks caused by administrative action. Bupa Arabia will not notify the customers about any service interruptions of substantial duration required by maintenance.
                                                           </td>
                                                            <td style="text-align: right">بوبا العربية ليست مسؤولة عن الخسائر الاقتصادية أو أي أضرار أخرى ناتجة عن مشاكل فنية قد تحدث بسبب انقطاع الخدمة، لن تقوم بوبا العربية بإبلاغ المستخدمين في حال تعرضت الخدمة إلى انقطاع بغض النظر عن مسببات هذا الانقطاع وإلى الوقت الذي تحتاجه بوبا العربية لإعادة تنشيط الخدمة مرة أخرى.
                                                           </td>
                                                            <td style="vertical-align: top">.2</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">3. Bupa Arabia will issue and send to its health care provider's designated user-IDs and passwords which will enable to access their web accounts.
                                                           </td>
                                                            <td style="text-align: right">تقوم بوبا العربية بإصدار اسم مستخدم وكلمة مرور خاصة لكل مقدم الخدمة الصحية المعتمد وارسالها لهم مما سيتيح لهم فرصة لاستخدام خدمات بوبا العربية الالكترونية.
                                                           </td>
                                                            <td style="vertical-align: top">.3</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">4. Health care provider undertakes to maintain the Passwords in strict confidence and not to provide the Passwords to any third party and to be responsible for the security, integrity and appropriate authorized use of Bupa Arabia’s web services.
                                                           </td>
                                                            <td style="text-align: right">يقر مقدم الخدمة الصحية المعتمد على الحفاظ على اسم المستخدم وكلمة المرور في مكان آمن وعدم إعلام أي طرف ثالث غير مصرح له وأن يكون مسؤولاً عن الأمن والنزاهة والاستخدام المناسب المصرح به لخدمات بوبا العربية الالكترونية.
                                                           </td>
                                                            <td style="vertical-align: top">.4</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">5. Health care provider shall notify Bupa Arabia promptly if there is a loss or compromise of any Passwords or username and the health care provider will be solely responsible for all legal actions and financial liabilities incurred as a result of such incident resulting from the Customer's negligence or intentional misconduct and when the health care provider discovers a Cyber security Incident or encounters Suspicious Activities that relate to Bupa Arabia’s web services or health care provider systems that interact at any time with a Bupa Arabia system.
                                                           </td>
                                                            <td style="text-align: right">على مقدم الخدمة الصحية المعتمد إبلاغ بوبا العربية فوراً في حال تم فقدان اسم المستخدم وكلمة المرور نتيجة الاهمال أو في حال تم تسريبه لطرف ثالث غير مصرح له أو حادثة الأمن السيبراني أو واجه أنشطة مشبوهة تتعلق بخدمات بوبا العربية الالكترونية أو 
أنظمة مقدم الخدمة الصحية المعتمد التي تتفاعل في أي وقت مع نظام بوبا العربية ويتحمل مقدم الخدمة الصحية المعتمد مسؤولية العمليات التي تمت على حسابه جراء ذلك وما يترتب عليها من امور مالية وقانونية.
                                                           </td>
                                                            <td style="vertical-align: top">.5</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">6. Health care provider shall notify Bupa Arabia promptly if any of the authorized users resigned or was terminated from the health care providers’ organization in order to secure a new username and password. Health care provider will be solely responsible for all legal actions and financial liabilities incurred as a result of such incident resulting from the health care provider’s negligence or intentional fraud.
                                                           </td>
                                                            <td style="text-align: right">على مقدم الخدمة الصحية المعتمد إبلاغ بوبا العربية فوراً في حال استقال الموظف المخول بإجراء العمليات أو في حال إنهاء خدماتة من الشركة أو المؤسسة حتى يتم إلغاء اسم المستخدم الخاص به وإصدار اسم مستخدم جديد وكلمة مرور جديدة، ويتحمل العميل المسؤولية الكاملة جراء اهمالة أو اساءة استعماله المتعمده وما يترتب عليها من أمور مالية و/أو قانونية.
                                                           </td>
                                                            <td style="vertical-align: top">.6</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">7. Any unauthorized use of user IDs or Passwords by health care provider will constitute a material breach of this terms and conditions.
                                                           </td>
                                                            <td style="text-align: right">أي استخدام غير مصرح به لاسم المستخدم أو كلمة المرور من قبل مقدم الخدمة الصحية المعتمد أو الموظف المخول يشكل خرقاً مادياً لهذا الملحق.
                                                           </td>
                                                            <td style="vertical-align: top">.7</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">8. Health care provider agrees and fully understands that any amendments requested through Bupa Arabia's web services will be reflected on their invoices once the process is finalized and approved.
                                                           </td>
                                                            <td style="text-align: right">يوافق مقدم الخدمة الصحية المعتمد ويعي أن أي عملية تتم من خلال الخدمات الإلكترونية ستظهر على الفواتير التي تصدر بشكل شهري متى ما تمت العملية بنجاح.
                                                           </td>
                                                            <td style="vertical-align: top">.8</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">9. Fixing fault situations will mainly be done during office hours. The service does not include correcting problems caused by hardware and/or software of the user.
                                                           </td>
                                                            <td style="text-align: right">تعديل الأخطاء سيتم خلال أوقات الدوام الرسمية فقط وهذا البند لايشمل المشاكل الناتجة عن الأجهزة و/أو الشبكة الخاصة بالمستخدم.
                                                           </td>
                                                            <td style="vertical-align: top">.9</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">10. Health care provider is fully responsible for any consequences such as delay due to any wrong data uploaded on Bupa's website.
                                                           </td>
                                                            <td style="text-align: right">يتحمل مقدم الخدمة الصحية المعتمد المسؤولية الكاملة عن المعلومات التي يتم تسجيلها في الموقع ويتحمل مسؤولية التأخير في حال كانت المعلومات المدخلة غير صحيحة.
                                                           </td>
                                                            <td style="vertical-align: top">.10</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">11. Bupa Arabia reserves the right to modify or change the foregoing Terms & Conditions as it deems necessary and in accordance to SAMA and CCHI regulations without reference and/or inform the Health care provider.
                                                           </td>
                                                            <td style="text-align: right">تحتفظ بوبا العربية بحق تعديل أو تغيير الشروط والأحكام حسب ما تراه ضرورياً وطبقاً لقرارات وتعاميم مؤسسة النقد العربي السعودي ومجلس الضمان الصحي التعاوني دون الرجوع و/أو إبلاغ مقدم الخدمة الصحية المعتمد.
                                                           </td>
                                                            <td style="vertical-align: top">.11</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">12. The health care provider shall provide Bupa Arabia of a clear copy of Saudi ID or Iqama for those whom will be authorized to accessing the online service and /or any documents that may be required for registration and auditing in relation to services.
                                                           </td>
                                                            <td style="text-align: right">يلتزم مقدم الخدمة الصحية المعتمد بتزويد بوبا العربية صورة واضحة من بطاقة الهوية الوطنية أو الإقامة للأشخاص المخولين باستخدام الخدمات الإلكترونية. و /أو أي مستندات أخرى والتي قد تطلب لاحقاً بغرض التسجيل او التدقيق في العمليات التي تمت مسبقاً.
                                                           </td>
                                                            <td style="vertical-align: top">.12</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">13. Bupa Arabia  reserves the right to conduct a Cyber security Assessment once per calendar year or more frequently upon the occurrence of any of the following:<br />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;a)	Suspicious Activities;
                                                                <br />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;b)	a Cyber security Incident in respect of a health care provider client’s system; and
                                                                <br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;c)	a Cyber security Incident in respect of a Bupa Arabia system.
                                                           </td>
                                                            <td style="text-align: right">تحتفظ بوبا العربية بحقها في إجراء تقييم للأمن السيبراني مرة في كل سنة تقويمية أو أكثر عند حدوث أي مما يلي:<br />
                                                            ‌أ.	‌الأنشطة المشبوهة ؛<br />
                                                            ‌ب.	حادثة الأمن السيبراني فيما يتعلق بنظام العميل ؛ و<br />
                                                            ‌ج.	حادثة الأمن السيبراني فيما يتعلق بأنظمة بوبا العربية
                                                           </td>
                                                            <td style="vertical-align: top">.13</td>
                                                       </tr>
                                                       <tr>
                                                            <td style="text-align: left">14. Bupa Arabia reserves the right, exercisable at its sole discretion, to cancel the access of health care provider or any of its individual personnel or agents to any Bupa Arabia system and disable the connections of health care provider’s computer systems to Bupa Arabia’s systems.  Such action shall be in addition to and not in substitution of any right available to Bupa Arabia under this terms and conditions or at law and shall not relieve health care provider of any obligation to perform under this terms and conditions.  
                                                           </td>
                                                            <td style="text-align: right">تحتفظ شركة بوبا العربية بحقها ، وفقاً لتقديرها الخاص ، في إلغاء وصول مقدم الخدمة الصحية المعتمد أو أي من موظفيها أو وكلائها إلى أي نظام الكتروني تابع لبوبا العربية وتعطيل اتصالات أنظمة الكمبيوتر الخاصة بمقدم الخدمة الصحية المعتمد إلى أنظمة بوبا العربية. يجب أن يكون هذا الإجراء إضافة إلى أي حق متاح لـ "بوبا العربية" ولا يحل محله بموجب هذه الاحكام والشروط او القانون ، ويجب أن لا يعفي العميل من أي التزام لتنفيذه بموجب هذه الاحكام والشروط.
                                                           </td>
                                                            <td style="vertical-align: top">.14</td>
                                                       </tr>
                                                   </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; padding-left: 177px;">
                                                <div><a href="javascript:LinktoggleFooter();" id="divReadMore" style="text-decoration: none">Read More...</a></div>
                                            </td>
                                            <td style="text-align: right; padding-right: 28px;">
                                                <div><a href="javascript:LinktoggleFooter();" id="divArReadMore" style="text-decoration: none">أقرا المزيد</a></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:UpdatePanel runat="server" ID="updatePanel2">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnSubmit" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <table style="width: 100%; border: 0">
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="Iagree" runat="server" Text="I agree with the terms and condition as according to Bupa../ أوافق على الشروط والأحكام وفقا لبوبا العربية" AutoPostBack="true" OnCheckedChanged="Iagree_CheckedChanged" />
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <asp:Button CssClass="submitButton" ID="btnSubmit" style="color: darkgray; background-image: none;" runat="server" OnClick="btnSubmit_Click" Text="Submit" CausesValidation="true" ValidationGroup="Submit" Enabled="false"></asp:Button>
                                                    </td>
                                                    <td colspan="2" style="text-align: left">
                                                    <asp:Textbox  ID="lblUserError" runat="server" Text="" style="color:none;display:none" ></asp:Textbox>
                                                </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: left">
                                    <%--<asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" Font-Size="Larger"></asp:Label>--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
       
</asp:Content>
