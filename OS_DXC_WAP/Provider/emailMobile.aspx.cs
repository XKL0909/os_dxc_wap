﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Net.Mail;

public partial class Provider_emailMobile : System.Web.UI.Page
{
    


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }
    }

    protected void btnsend_Click(object sender, EventArgs e)
    {
        if (Request["action"] == "pre")
        {

            MailMessage mailMsg = new MailMessage();
            mailMsg.Attachments.Add(new Attachment(Server.MapPath("../mobile/PreAuth" + Request["PID"] + ".htm")));
            mailMsg.To.Add(txtFax.Text);

            mailMsg.From = new MailAddress("webmaster@bupame.com");
            mailMsg.Subject = "Pre-authorisation Statement - Bupa Arabia";
            mailMsg.Body = "Dear Sir/Madame,<br><br>Please find attached a copy of the Pre-authorisation Statement.<br><br>Kind Regards,<br>Bupa Arabia.";
            mailMsg.IsBodyHtml = true;
            
            
            

            SmtpClient smptClient = new System.Net.Mail.SmtpClient();// .SmtpMail.Send("admin@bupame.com", );
            try
            {

                smptClient.Send(mailMsg);
                mailMsg.Dispose();
                lblMsg.Text = "PreAuth Document has been successfully send .";
                pnlEmailForm.Visible = false;
                System.IO.File.Delete(Server.MapPath("/mobile/PreAuth" + Request["PID"] + ".htm"));
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message + ". Please contact the administrator for further details.";
            }


            
        }
        else
        {

            MailMessage mailMsg = new MailMessage();
            mailMsg.Attachments.Add(new Attachment(Server.MapPath("../mobile/Mypage" + Request["MembershipNo"] + ".htm")));
            mailMsg.To.Add(txtFax.Text);
            mailMsg.From = new MailAddress("webmaster@bupame.com");
            mailMsg.Subject = "Member Certificate - Bupa Arabia";
//            mailMsg.Body = "Dear ,<br>Check the attached Member Certificate<br>Regards,<br>BUPA ARABIA.";
            mailMsg.Body = "Dear Sir/Madame,<br><br>Please find attached a copy of the Pre-authorisation Statement.<br><br>Kind Regards,<br>Bupa Arabia.";

            mailMsg.IsBodyHtml = true;




            SmtpClient smptClient = new SmtpClient();// .SmtpMail.Send("admin@bupame.com", );
            try
            {

                smptClient.Send(mailMsg);
                mailMsg.Dispose();
                lblMsg.Text = "Member Certificate has been successfully send .";
                pnlEmailForm.Visible = false;
                System.IO.File.Delete(Server.MapPath("/mobile/Mypage" + Request["MembershipNo"] + ".htm"));
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message + ". Please contact the administrator for further details.";
            }

            
        }
     
    }

    private static void Upload(string ftpServer, string userName, string password, string filename)
    {
        using (System.Net.WebClient client = new System.Net.WebClient())
        {
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.UploadFile(ftpServer + "/" + new FileInfo(filename).Name,"STOR",filename);
        }
    } 


}