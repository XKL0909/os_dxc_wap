using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class tabsample : BasicTabPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        ////////        TabFlag = "M";

        DisplayMemberCertResult("3221965");
        //the below would be replace with vlaue from session var for the user
        //tabMemberName.HeaderText = response.memberName;


    }

    public override void DisplayMemberCertResult(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            request.membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.EnqMbrDetInfo(request);

            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.detail[0].memberName != null)
            {
                ((BasicTabPage)Page).DisplayMbrDetInfo(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;


                if (((AjaxControlToolkit.TabPanel)this.FindControl("tabMemberName")).HeaderText == "Member")
                    ((AjaxControlToolkit.TabPanel)this.FindControl("tabMemberName")).HeaderText = response.detail[0].memberName;


            }
            //else
            //Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            //Message1.Text = ex.Message;
        }

    }

    // function used for Tab System to retrieve family list
    public override void DisplayMemberListResult(OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;

        ////////////string[,] ResponseDetails;

        ////////////ResponseDetails[0,0] = "test";
        ////////////ResponseDetails[0,1] = "test";
        ////////////ResponseDetails[0,2] = "test";


        ////////string arr(2,4);
        //////// string ar()() = new String({New String() {"", "", ""}, New String() {"", "", ""}};

        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.status == "0")
        {
            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px; border:solid thin ;'  class='display' id='example'><thead><tr><th>Provider Name</th>   <th>Address 1</th> <th>Address 2</th> <th>Address 3</th> <th>District</th> <th>Tel No 1</th> <th>Tel No 2</th>  	</tr>	</thead><tbody> ");
            ((DropDownList)this.FindControl("DDLDependentList")).Items.Clear();

            foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
            {

                //DDLDependentList.Items.Add( New  ListItem("Item 1", "1");
                //http://authors.aspalliance.com/stevesmith/articles/dotnetlistbox2.asp


                // DDLDependentList.Items.Add(new ListItem(dtl.memberName + " -- " + dtl.relationship, dtl.membershipNo));

                //DDLDependentList.DataTextField =dtl.memberName + " -- " + dtl.relationship;
                //DDLDependentList.DataValueField = dtl.membershipNo.ToString();
                //DDLDependentList.Items.Insert(.DataBind();



                ListItem DepList = new ListItem();
                DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                DepList.Value = dtl.membershipNo.ToString();
                ((DropDownList)this.FindControl("DDLDependentList")).Items.Add(DepList);

            }
        }
        else
        {
            //msge = new StringBuilder(100);
            //foreach (String s in response.errorMessage)
            //{
            //    msge.Append(s).Append("/n");
            //}
            //Message1.Text = msge.ToString();
        }

        //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    }

    public override void DisplayMbrDetInfo(OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response)
    {


        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {
            //sbResponse.Append("Client TransactionID: ").Append(response.TransactionID).Append("\n");
            lblMemberName.Text = response.detail[0].memberName;
            //lblMembershipNo.Text = response.detail[0].membershipNo.ToString();
            //lblCompanyName.Text = response.detail[0].companyName;
            //lblEmployeeID.Text = response.detail[0].employeeNo;
            //lblEndDate.Text = response.detail[0].endDate.ToString();
            //lblGender.Text = response.detail[0].gender;
            //lblDateOfBirth.Text = response.detail[0].memberDOB.ToString();
            //lblEmployeeNo.Text = response.detail[0].memberID;
            //lblType.Text = response.detail[0].memberTypeDescription;
            //lblStartDate.Text = response.detail[0].startDate.ToString();
            //lblSponsorId.Text = response.detail[0].sponsorID;

            //lblClass.Text = response.className;
            //lblOptions.Text = response.option;
            //lblIssueDate.Text = DateTime.Now.ToShortDateString();
            ////response.
            //// lblSponsorId.Text = response.MemberName;   /// lblSponsorId ??
            //lblEffectiveFrom.Text = response.effFrom.ToString();
            //lblCustomerName.Text = response.customerName;
            //lblDateOfBirth.Text = response.DOB;
            //lblStaffNo.Text = response.staffNo;
            //lblContribution.Text = response.deductible;
            // lblEmployeeNo.Text = response.staffNo;
            //lblEffectiveTo.Text = response.effTo.ToString();
            //lblGender.Text = response.MemberName;  // lblGender
            // lblStatus.Text = response.status;
            //lblRoomType.Text = response.roomType;
            UpdatePanel2.Visible = true;

            //sbResponse.Append("Client Status: ").Append(response.Status).Append("\n");
            //sbResponse.Append("Client CustomerNo: ").Append(response.CustomerNo.ToString()).Append("\n");
            //sbResponse.Append("Client ClassID: ").Append(response.ClassID.ToString()).Append("\n");
            //sbResponse.Append("Client BranchCode: ").Append(response.BranchCode.Trim()).Append("\n");
            //sbResponse.Append("Client MemberName: ").Append(response.MemberName);
        }
        else
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);

        //Message1.Text = sbResponse.ToString(); //MessageBox.Show(sbResponse.ToString());


    }
}
