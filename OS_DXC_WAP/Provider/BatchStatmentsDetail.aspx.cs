using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Linq;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Bupa.Core.Logging;

public partial class BatchStatmentsDetail : System.Web.UI.Page
{

    #region Variable Declaration
    string strProviderID;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    public string UrlValue = string.Empty;
    private List<BatchStatmentInfo> lstBatchStatment;
    #endregion

    #region Page Event
    void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UrlValue = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
            UrlValue = UrlValue.Replace("batchstatmentsdetail.aspx", "BatchStatment.aspx?val=");
            strProviderID = Convert.ToString(Session["ProviderID"]);
            if (string.IsNullOrEmpty(strProviderID))
                Response.Redirect("../default.aspx");

            lblMeesage.Text = string.Empty;// +"Unable to get provider statement";

            if (!Page.IsPostBack)
            {
                lblProviderDetails.Text = Convert.ToString(Session["ProviderID"]) + " / " + Convert.ToString(Session["ProviderName"]);
                GetBatchStatement();
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            lblMeesage.Text = "Unable to get provider statement";
           
        }
       

    }
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            ///System.Threading.Thread.Sleep(1000);
            Clear();
            GetBatchStatement();
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            lblMeesage.Text = "Unable to refresh provider statement";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            ////System.Threading.Thread.Sleep(1000);

            lstBatchStatment = new List<BatchStatmentInfo>();
            if (Session["ProviderSatatements"] == null)
                GetBatchStatement();
            lstBatchStatment = (List<BatchStatmentInfo>)Session["ProviderSatatements"];
            if (lstBatchStatment != null && lstBatchStatment.Count > 0)
            {
                //Default selection
                var lstBatchFilter = from item in lstBatchStatment
                                     select item;
                //Batch Id filter condition
                if (!string.IsNullOrEmpty(txtBatchID.Text.Trim()))
                    lstBatchFilter = lstBatchFilter.Where(item => item.Id.Trim().Contains(txtBatchID.Text.Trim()));
                //Year & month filter condition
                string yearMonth = string.Empty;
                if (!string.IsNullOrEmpty(txtYear.Text.Trim()) && !string.IsNullOrEmpty(txtMonth.Text.Trim()))
                    yearMonth = txtYear.Text.Trim() + txtMonth.Text.Trim();
                else if (!string.IsNullOrEmpty(txtYear.Text.Trim()))
                    yearMonth = txtYear.Text.Trim();
                else if (!string.IsNullOrEmpty(txtMonth.Text.Trim()))
                    yearMonth = txtMonth.Text.Trim();
                if (!string.IsNullOrEmpty(yearMonth))
                    lstBatchFilter = lstBatchFilter.Where(item => item.Period.ToLower().Contains(yearMonth.ToLower()));

                //Statement Id filter condition
                if (!string.IsNullOrEmpty(txtStatmentID.Text.Trim()))
                    lstBatchFilter = lstBatchFilter.Where(item => item.StatementId.Contains(txtStatmentID.Text.Trim()));

                //Claim type filter condition

                string claimId = string.Empty;
                claimId = ddlClaimType.SelectedValue == "0" ? string.Empty : ddlClaimType.SelectedValue;
                if (!string.IsNullOrEmpty(claimId))
                    lstBatchFilter = lstBatchFilter.Where(item => item.ClaimId.Contains(claimId));



                if (lstBatchFilter == null && lstBatchFilter.Count() == 0)
                    lstBatchFilter = null;

                gvProviderStatement.DataSource = lstBatchFilter;
                gvProviderStatement.DataBind();
            }


        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            lblMeesage.Text = "Unable to search provider statement";
        }

    }
    protected void gvProviderStatement_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
               HtmlAnchor aStatment = (HtmlAnchor)e.Row.FindControl("aStatment");
               if (e.Row.Cells[3].Text.Trim().ToLower() == "statement" || e.Row.Cells[3].Text.Trim().ToLower() == "paid")                  
                   aStatment.Attributes.Add("style", "display:block");
               else
                   aStatment.Attributes.Add("style", "display:none");

                string statmentId = gvProviderStatement.DataKeys[e.Row.RowIndex].Values[1].ToString();
                aStatment.HRef = UrlValue + Cryption.Encrypt(statmentId);
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            lblMeesage.Text = "Unable to bind provider statement";
           
        }
    }
    #endregion

    #region Page Function
    private void GetBatchStatement()
    {
        try
        {
            OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
            OS_DXC_WAP.CaesarWS.EnqBatchStmHistRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqBatchStmHistResponse_DN response;
            request = new OS_DXC_WAP.CaesarWS.EnqBatchStmHistRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            long ls_txnid = TransactionManager.TransactionID();
            request.transactionID = ls_txnid;
            request.provCode = strProviderID;
            response = ws.EnqBatchStmHist(request);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqBatchStmHistRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqBatchStmHistRequest_DN>();
            //XmlReq.Request(request, "EnqBatchStmHistRequest_DN");

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqBatchStmHistResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqBatchStmHistResponse_DN>();
            //XmlResp.Response(response, "EnqBatchStmHistResponse_DN");

            if (response != null)
            {
                if (response.status == "0")
                {
                    GetBatchStatements(response);
                }

            }



        }
        catch (Exception)
        {

            throw;
        }
    }
    private void GetBatchStatements(OS_DXC_WAP.CaesarWS.EnqBatchStmHistResponse_DN response)
    {
        try
        {
            if (response.detail != null)
            {
                lstBatchStatment = new List<BatchStatmentInfo>();
                foreach (var item in response.detail)
                {
                    BatchStatmentInfo batchStatment = new BatchStatmentInfo();
                    batchStatment.Id = Convert.ToString(item.batchID);
                    batchStatment.Period = item.batchPeriod;
                    batchStatment.Status = item.status.ToUpper() == "I" ? "Received" : item.status.ToUpper() == "S" ? "Statement" : item.status.ToUpper() == "P" ? "Paid" : string.Empty;
                    batchStatment.ProviderCode = item.provCode;
                    batchStatment.ProviderName = item.provName;                   
                    batchStatment.StatementId = Convert.ToString(item.stmID);                    
                    batchStatment.StatementIndicator = item.stmInd;
                    batchStatment.ClaimId = item.clmType;
                    batchStatment.ClaimType = item.clmType.ToUpper() == "I" ? "In Patient" : item.clmType.ToUpper() == "O" ? "Out Patient" : item.clmType.ToUpper() == "B" ? "In/Out Patient" : string.Empty;
                    lstBatchStatment.Add(batchStatment);
                }
                Session["ProviderSatatements"] = lstBatchStatment;
                gvProviderStatement.DataSource = lstBatchStatment;
                gvProviderStatement.DataBind();
            }
            else
            {
                gvProviderStatement.DataSource = null;
                gvProviderStatement.DataBind();
            }
        }
        catch (Exception)
        {
            
            throw;
        }
       
    }
    private void Clear()
    {
        txtYear.Text = string.Empty;
        txtMonth.Text = string.Empty;
        txtBatchID.Text = string.Empty;
        txtStatmentID.Text = string.Empty;
        ddlClaimType.ClearSelection();
    }
    #endregion


}
[Serializable]
public class BatchStatmentInfo
{
    public string Id { get; set; }
    public string Period { get; set; }
    public string ClaimId { get; set; }
    public string ClaimType { get; set; }
    public string Status { get; set; }
    public string StatementId { get; set; }
    public string StatementIndicator { get; set; }
    public string ProviderCode { get; set; }
    public string ProviderName { get; set; }
}