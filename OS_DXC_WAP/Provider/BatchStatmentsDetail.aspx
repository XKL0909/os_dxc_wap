<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Templates/Inner.Master" Inherits="BatchStatmentsDetail" Debug="true" Codebehind="BatchStatmentsDetail.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register Src="../Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <link href="css/basic.css" rel="stylesheet" type="text/css" />
    <link href="css/basic_ie.css" rel="stylesheet" type="text/css" />
    <%--<asp:ScriptManager runat="server" ID="scriptManager" />--%>
    <link rel="stylesheet" href="../styles/stylejquery.css" />

    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $("input").keydown(function (e) {
                // Allow: backspace, delete, tab, escape and enter
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
      

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
            });

            prm.add_endRequest(function () {
                $('#entryForm').unblock();
            });

            // Progress indicator preloading.
            var preload = document.createElement('img');
            preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
            delete preload;



    </script>



    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:AsyncPostBackTrigger ControlID="btnRefresh" />
        </Triggers>
        <ContentTemplate>
            <div id="entryForm">
                <table width="100%" border="0">
                    <tr>
                        <td>
                            <div>
                                <asp:Label ID="lblMeesage" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </div>
                            <div>
                                <fieldset>
                                    <legend>
                                        <h1>
                                            <asp:Label ID="lblPageTitle" runat="server" Text="Search Batch Statements"></asp:Label></h1>
                                    </legend>

                                    <table style="padding: 15px 2px 15px 2px !important;">
                                        <tr>
                                            <td colspan="4" style="width: 100%">Provider ID / Name : 
                                                                <asp:Label runat="server" ID="lblProviderDetails" Text="" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%">Batch Period(YYYY/MM)</td>
                                            <td>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox CssClass="textbox" runat="server" MaxLength="4" Width="80px" ID="txtYear"></asp:TextBox></td>
                                                        <td>/ </td>
                                                        <td>
                                                            <asp:TextBox CssClass="textbox" runat="server" MaxLength="2" Width="40px" ID="txtMonth"></asp:TextBox></td>


                                                    </tr>


                                                </table>
                                            </td>
                                            <td style="width: 30%">Batch ID</td>
                                            <td>
                                                <asp:TextBox CssClass="textbox" runat="server" Width="120px" MaxLength="8" ID="txtBatchID" />

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Claim Type</td>
                                            <td>
                                                <asp:DropDownList CssClass="DropDownListCssClass" runat="server" ID="ddlClaimType" Width="120px">
                                                    <asp:ListItem Text="---Select---" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="In patient" Value="I"></asp:ListItem>
                                                    <asp:ListItem Text="Out patient" Value="O"></asp:ListItem>
                                                    <asp:ListItem Text="In/Out patient" Value="B"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>Statment ID</td>
                                            <td>
                                                <asp:TextBox CssClass="textbox" runat="server" Width="120px" MaxLength="10" ID="txtStatmentID"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:RegularExpressionValidator ID="rfvtxtYear" runat="server" ControlToValidate="txtYear"
                                                    ErrorMessage="Year should be numbers only" ForeColor="Red" ValidationExpression="^\d+$" Display="Dynamic">        </asp:RegularExpressionValidator><br />
                                                <asp:RegularExpressionValidator ID="rfvtxtMonth" runat="server" ControlToValidate="txtMonth"
                                                    ErrorMessage="Month should be numbers only" ForeColor="Red" ValidationExpression="^\d+$" Display="Dynamic">        </asp:RegularExpressionValidator><br />
                                                <asp:RegularExpressionValidator ID="rfvtxtBatchID" runat="server" ControlToValidate="txtBatchID"
                                                    ErrorMessage="Batch ID should be numbers only" ForeColor="Red" ValidationExpression="^\d+$" Display="Dynamic">        </asp:RegularExpressionValidator><br />
                                                <asp:RegularExpressionValidator ID="rfvtxtStatmentID" runat="server" ControlToValidate="txtStatmentID"
                                                    ErrorMessage="Statment ID should be numbers only" ForeColor="Red" ValidationExpression="^\d+$" Display="Dynamic">        </asp:RegularExpressionValidator><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <asp:Button CssClass="submitButton" ID="btnRefresh" runat="server" Text="Refresh List" Width="100px" OnClick="btnRefresh_Click" /></td>
                                            <td>
                                                <asp:Button CssClass="submitButton" ID="btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" />

                                            </td>
                                        </tr>
                                    </table>


                                </fieldset>

                                <br />
                                <div style="margin-top: 20px">
                                    <asp:GridView runat="server" ID="gvProviderStatement" Visible="true" AutoGenerateColumns="false" Width="100%" Style="margin-top: 30px;"
                                        AutoGenerateDeleteButton="false" AutoGenerateEditButton="false" OnRowDataBound="gvProviderStatement_RowDataBound"
                                        ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" DataKeyNames="Id, StatementId">
                                        <HeaderStyle BackColor="#0072c6" ForeColor="White" Font-Bold="true" BorderStyle="None" Height="30px" Font-Size="14px" />
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" BorderStyle="None" Height="30px" Font-Size="12px" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Batch ID" DataField="Id" />
                                            <asp:BoundField HeaderText="Batch Period" DataField="Period" />
                                            <asp:BoundField HeaderText="Claim Type" DataField="ClaimType" />
                                            <asp:BoundField HeaderText="Batch Status" DataField="Status" />
                                            <asp:BoundField HeaderText="Statment ID" DataField="StatementId" />
                                            <asp:TemplateField HeaderText="View Statment">
                                                <ItemTemplate>
                                                    <a href="#" id="aStatment" runat="server">
                                                        <img alt="View statement" id='img<%# Eval("Id")%>' src="images/pdf.jpg" height="24px" width="24px"  /></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>


                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <uc2:OSNav ID="OSNav1" runat="server" />
            <div id='content'>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        View and print your claim statement.
    </p>
</asp:Content>
