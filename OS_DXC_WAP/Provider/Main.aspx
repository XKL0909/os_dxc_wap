﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/DashBoard.master" AutoEventWireup="true" Inherits="Provider_Main" Codebehind="Main.aspx.cs" %>

<%@ Register Src="../UserControls/DashBoardWelcome.ascx" TagName="DashBoardWelcome"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/DashBoardOSNav.ascx" TagName="DashBoardOSNav" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/DashBoardTOB.ascx" TagName="DashBoardTOB" TagPrefix="uc3" %>
<%@ Register Src="../UserControls/Tips.ascx" TagName="Tips" TagPrefix="uc4" %>
<%@ Register Src="../UserControls/MyInbox.ascx" TagName="MyInbox" TagPrefix="uc5" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderLeftNav" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
    <style type="text/css">
        .btnCss {
            width: auto !important;
            background-color: #0886D6;
            cursor: pointer;
            color: #fff !important;
            line-height: 30px !important;
            outline: none;
            border: none;
            font-weight:bold;
            font-size:13px !important;
            
        }

            .btnCss:hover {
                background-color: #00a1e4;
            }
    </style>
    
     <style type="text/css">
        .linkcss a:visited
        {
            color: Red;
        }
        .linkcss a:link
        {
            color: Red;
        }
        
        #page-load-modal-content .window {
  position:fixed;
  left:0;
  top:0;
  width:440px;
  height:200px;
  display:none;
  z-index:9999;
  padding:20px;
}
        div.botright
        {
            display: block;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 500px;
            height: 350px;
            padding: 25px 15px;
            margin-top: -9em; /*set to a negative number 1/2 of your height*/
            margin-left: -15em; /*set to a negative number 1/2 of your width*/ /*give it some background and border*/
            background: #00A1E4;
            border: 1px solid #7d5912;
            color: #FFFFFF;
            font-family: Arial;
            font-size: 15px;
        }
    </style>



    <script type="text/javascript">

        function close() {

        }
        function goto(x) {
            window.location = x;
        }
        function doshow() {
            debugger;
            var currentTime = new Date()
            var endDate = '02/10/2014';
            if (new Date(endDate) > new Date(currentTime)) {
                document.getElementById('basic-modal-content').style.display = 'block';

            }
        }

    </script>
    <link href="css/css-notification-box.css" rel="stylesheet" type="text/css" />
    <link href="css/global.css" rel="stylesheet" type="text/css" />


    <link type='text/css' href='css/basic.css' rel='stylesheet' media='screen' />
   
    <td>
        <uc1:DashBoardWelcome ID="DashBoardWelcome1" runat="server" />
        <uc5:MyInbox ID="MyInbox1" runat="server" />
        <uc2:DashBoardOSNav ID="DashBoardOSNav1" runat="server" />
        <!--<uc4:Tips ID="Tips1" runat="server" />-->
        <uc3:DashBoardTOB ID="DashBoardTOB1" runat="server" />


        <div id='container'>
	
	
		
		 <div id='content'>
		<!-- modal content -->



		<div id="basic-modal-content">
			
           <p>We would like to inform you that we have applied a new enhancements on Maternity automation.</p>
            <p style="text-align:right;">نود اعلامكم بأنه قد تم اضافة تحسينات جديدة لعمليات أتمتة طلبات الأمومة</p>
            <div style="vertical-align:central;"><p><a href="#" style="color:#fff;text-align:left;" onclick="goto('../Provider/Preauth15.aspx');" >Close</a> 
            <a href="#" style="color:#fff;text-align:right;" onclick="goto('../Provider/Preauth15.aspx');" >إغلاق</a></p></div>
            </div>

            <div id="basic-modal-content1">
			
           <p>We would like to inform you that we have applied a new enhancements on Maternity automation.</p>
            <p style="text-align:right;">نود اعلامكم بأنه قد تم اضافة تحسينات جديدة لعمليات أتمتة طلبات الأمومة</p>
            <div style="vertical-align:central;"><p><a href="#" style="color:#fff;text-align:left;" onclick="goto('../Provider/ExistingRequestSearch.aspx');" >Close</a> 
               <a href="#" style="color:#fff;text-align:right;" onclick="goto('../Provider/ExistingRequestSearch.aspx');" >إغلاق</a></p></div>
            </div>
        
                </div>

          </div> 
     
    </td>
    <td>
            <div id="popupPasswordExpiry" runat="server" style="top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999;">
                <div style="left: 40%; top: 40%; position: fixed;">
                    <table style="background-color: #fff; width: 100%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                        <tr style="background-color: #f44336; line-height: 30px">
                            <td colspan="3" style="color: #fff; text-align: left">
                                <strong style="padding-left: 5px; font-size: larger">Password Expiry</strong>
                                <span style="float: right;">
                                    <strong style="font-size: larger">انتهاء صلاحية كلمة المرور&nbsp;</strong>
                                    <%--<a onclick="closePWDNotification('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size: larger">X</a>--%>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="margin: auto" colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 60%">
                                <div style="padding-left: 10px; padding-right: 10px;">
                                    <asp:Panel ID="Panel1" runat="server">
                                        <div style="margin-top: 0; margin-bottom: 0;">Dear Client, </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="Panel2">
                                        <div id="ExpMessage"  runat="server" style="margin-top: 0; margin-bottom: 0;">
                                        </div>
                                    </asp:Panel>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right">
                                <div>
                                    <asp:Panel ID="Panel4" runat="server">
                                        <div style="margin-top: 0; margin-bottom: 0; padding-right: 10px;">،عزيزي العميل</div>
                                    </asp:Panel>
                                    <div  id="ExpMessagear"  runat="server" style="padding-right: 10px; text-align:right; padding-left: 10px;">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                                <input id="btnResetPassword" type="button" onclick="redirectChangePassword()" class="btnCss" value="Reset" />
                                <input id="btnLater" runat="server" type="button" onclick="ClosePopUp(1)" class="btnCss" value="Later" />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        <div id="PopUpIsSuperUser" runat="server" style="display:none;top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999999999999;">
                <div style="left: 25%; top: 50%; position: fixed;">
                    <table style="background-color: #E5F7FF; width: 50%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                        <tr style="background-color: #0886D6; line-height: 30px">
                            <td colspan="3" style="color: #fff; text-align: left">
                                <strong style="padding-left: 5px; font-size: larger">BUPA - Online Services</strong>
                                <span style="float: right;">
                                    <strong style="font-size: x-large">بوبا - خدمات الإلكترونية&nbsp;</strong>
                                    <a onclick="PopUpIsSuperUser('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size: larger"><img alt="Procced" src="../images/failure.png" style="width:20px; height:20px" /></a>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="margin: auto" colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 60%">
                                <div style="padding-left: 10px; padding-right: 10px;">
                                    <asp:Panel ID="Panel3" runat="server">
                                        <div style="margin-top: 0; margin-bottom: 0;">
                                            A Concurrent user will not be able to access Bupa Arabia online services as of 23 May 2018 at 10:00 AM to share same user established to multiple users. Kindly use the option (User Management) to create additional user.
                                        </div>
                                    </asp:Panel>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right">
                                <div style="padding-right: 10px; padding-left: 10px;">
                                    <div style="padding-right: 10px; padding-left: 10px;">
                                        <asp:Panel ID="Panel6" runat="server">
                                           بحلول 23 مايو 2018 الساعة 10:00 صباحا لن يتمكن المستخدم المتزامن (مشاركة اسم المستخدم نفسه بين عدة مستخدمين) من الوصول إلى خدمات بوبا العربية الإكترونية. يرجى استخدام خيار (إدارة المستخدمين) لإنشاء مستخدم إضافي.
                                        </asp:Panel>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                                <%-- <input type="button" onclick="ClosePopUp('2')" class="btnCss" value="Click here for more details | انقر هنا لمزيد من التفاصيل" />--%>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        <div id="PopUpTOB" runat="server" style="display:none;top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999999999999;">
                <div style="left: 25%; top: 50%; position: fixed;">
                    <table style="background-color: #E5F7FF; width: 80%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                        <tr style="background-color: #0886D6; line-height: 30px">
                            <td colspan="3" style="color: #fff; text-align: left">
                                <strong style="padding-left: 5px; font-size: larger">BUPA - Online Services</strong>
                                <span style="float: right;">
                                    <strong style="font-size: x-large">بوبا - خدمات الإلكترونية&nbsp;</strong>
                                    <a onclick="PopUpTOB('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size: larger"><img alt="Procced" src="../images/failure.png" style="width:20px; height:20px" /></a>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="margin: auto" colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 50%">
                                <div style="padding-left: 10px; padding-right: 10px;">
                                    <asp:Panel ID="pnlTobEng" runat="server">
                                        <div style="margin-top: 0; margin-bottom: 0;">
                                            Bupa Arabia  have launched the new updated TOB describing the changes related to different level of Providers and benefits effective as of July 1, 2018
                                        </div>
                                    </asp:Panel>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right">
                                <div style="padding-right: 10px; padding-left: 10px;">
                                    <div style="padding-right: 10px; padding-left: 10px;">
                                        <asp:Panel ID="pnlTobArb" runat="server">
                                           قامت بوبا العربية بتحديث جدول المنافع على موقعها الإلكتروني بحيث تعكس التغيرات الجديدة المرتبطة بكل مقدم خدمة حسب مستوى التغطية والمزايا المتوفرة له وذلك إعتبارآ من 1 يوليو 2018
                                        </asp:Panel>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                                <%-- <input type="button" onclick="ClosePopUp('2')" class="btnCss" value="Click here for more details | انقر هنا لمزيد من التفاصيل" />--%>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>        
        


        </td>
<script type='text/javascript' src="js/box/jquery.js"></script>
<script type='text/javascript' src='js/jquery.simplemodal.js'></script>
<script type='text/javascript' src='js/basic.js'></script>
    <script type="text/javascript">
        function ClosePopUp(value) {
            var ctrl = document.getElementById("<%=popupPasswordExpiry.ClientID%>");
           ctrl.style.display = "none";
        }
        function PopUpIsSuperUser() {
            var ctrl = document.getElementById("<%=PopUpIsSuperUser.ClientID%>");
            ctrl.style.display = "none";
        }
        function redirectChangePassword(value) {
            window.location("../default.aspx");
            var ctrl = document.getElementById("<%=popupPasswordExpiry.ClientID%>");
            ctrl.style.display = "none";
        }
     function PopUpTOB() {
            var ctrl = document.getElementById("<%=PopUpTOB.ClientID%>");
            ctrl.style.display = "none";
        }
    </script>
</asp:Content>
