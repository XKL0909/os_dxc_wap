﻿<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    Trace="false" EnableEventValidation="false" Inherits="PreauthAutomation" Codebehind="PreAuthAutomation.aspx.cs" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/Uploader.ascx" TagPrefix="uc1" TagName="Uploader" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">


    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <link href="css/basic.css" rel="stylesheet" type="text/css" />
    <link href="css/basic_ie.css" rel="stylesheet" type="text/css" />
   <%-- <asp:ScriptManager runat="server" ID="scriptManager" />--%>
    <link rel="stylesheet" href="../styles/stylejquery.css" />
    <style type="text/css">
        #myDivBox {
            width: 100px;
            height: 100px;
            background-color: #567;
            position: fixed;
            left: 50%;
            top: 50%;
            margin-left: -50px;
            margin-top: -50px;
        }
    </style>
    <style type="text/css">
        #idletimeout {
            background: red;
            border: 3px solid #FF6500;
            color: blue;
            border-color: lime;
            border-width: 10px;
            font-family: arial, sans-serif;
            text-align: center;
            font-size: 27px;
            padding: 10px;
            position: relative;
            top: 0px;
            left: 0;
            right: 0;
            z-index: 100000;
            display: none;
        }

            #idletimeout a {
                color: #fff;
                font-weight: bold;
                font-size: 17px;
            }

            #idletimeout span {
                font-weight: bold;
                font-size: 33px;
            }

        .style2 {
            width: 354px;
        }

        .style3 {
            width: 121px;
        }
    </style>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.8.2.custom.min.js"></script>
    <link type="text/css" rel="Stylesheet" href="../Scripts/jquery-ui-1.8.5.custom.css" />
    <script type="text/javascript" src="js/preauthfilldata.js"></script>

    <script type="text/javascript">



        function child_openurl(url) {

            popupWindow = window.open(url, "_blank", 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');

        }

        String.Format = function () {
            var s = arguments[0];
            for (var i = 0; i < arguments.length - 1; i++) {
                var reg = new RegExp("\\{" + i + "\\}", "gm");
                s = s.replace(reg, arguments[i + 1]);
            }
            return s;
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function JsonServiceCategoryList() {
            $.ajax({
                type: 'GET',
                url: "http://localhost:61448/onlineServicesData.asmx/ServiceCategoryList",
                // data: "{myparm1:105,myparm2:23}",
                contentType: 'application/json; charset=UTF-8',
                dataType: 'json',
                async: false,
                success: function (msg) {
                    alert(msg.d);
                },
                error: function (msg) {
                    alert('failure');
                    alert(msg);
                }
            });
        }

        function OnClientDropDownClosedHandler(sender, eventArgs) {


            $("#txtServiceDesc").val(sender.get_text());
            $('#ddlCategory').val(sender.get_text()).change();

        }
        function txtServiceDescChanged() {

            $('#dvCatOptionsForm').css("display", "none");

        }
        function OnMenuSelection(sCode) {

            $("#txtServiceDesc").val(sCode);
            $("#lblServiceDesc").val(sCode);
            $("#dvlblServiceDesc").innerHTML = sCode;
            $('#ddlCategory').val(sCode).change();

            //$("#txtServiceDesc").css("display", "none");
            //$("#txtServiceDesc:contains('OTH')").css("display", "block");
        }

        function diagAuto() {

            // JsonServiceCategoryList();

            $('#dialog-Automation').dialog
                   ({
                       height: 610,
                       width: 950,
                       modal: true,
                       resizable: false,
                       draggable: false,

                       buttons:
                       {
                           'Save': function () {
                               filldata();
                               $(this).dialog('close');
                               dialogConfirmed = true;
                               document.getElementById('val').value = "Y";
                               if (obj) obj.click();
                           },
                           'Cancel': function () {
                               $(this).dialog('close');
                               dialogConfirmed = true;
                               document.getElementById('val').value = "N";
                               if (obj) obj.click();
                           }
                       }
                   });
        }

        function OnClientTextChange(sender, eventArgs) {
            //alert("You1 typed " + sender.get_text());
            $("#txtServiceDesc").val(sender.get_text());


        }
        function OnClientSelectedIndexChanged(sender, eventArgs) {
            var item = eventArgs.get_item();
            $("#txtServiceDesc").val(item.get_text());

            //sender.set_text("You2 selected " + item.get_text());
            //$("#radComboCategory").val(item.get_text());
        }

        var dialogConfirmed = false;
        function ConfirmDialogAutomation(obj, ctl) {
            var _mVal = document.getElementById(ctl).value;
            $('#dialog-Automation').dialog
                ({
                    height: 310,
                    width: 650,
                    modal: true,
                    resizable: false,
                    draggable: false,

                    buttons:
                    {
                        'Yes': function () {
                            $(this).dialog('close');
                            dialogConfirmed = true;
                            document.getElementById('val').value = "Y";
                            if (obj) obj.click();
                        },
                        'No': function () {
                            $(this).dialog('close');
                            dialogConfirmed = true;
                            document.getElementById('val').value = "N";
                            if (obj) obj.click();
                        }
                    }
                });

            return dialogConfirmed;
        }
        function ConfirmDialog(obj, ctl) {
            //debugger;
            var _mVal = document.getElementById(ctl).value;
            if (_mVal == 'DEN' || _mVal == 'OER' || _mVal == 'OGP' || _mVal == 'VAC' || _mVal == 'PHA') {
                document.getElementById('val').value = "";
                return true;
            } else {
                if (!dialogConfirmed) {


                    $('#dialog-confirm').dialog
                ({
                    height: 310,
                    width: 650,
                    modal: true,
                    resizable: false,
                    draggable: false,

                    buttons:
                    {
                        'Yes': function () {
                            $(this).dialog('close');
                            dialogConfirmed = true;
                            document.getElementById('val').value = "Y";
                            if (obj) obj.click();
                        },
                        'No': function () {
                            $(this).dialog('close');
                            dialogConfirmed = true;
                            document.getElementById('val').value = "N";
                            if (obj) obj.click();
                        }
                    }
                });
                }

                return dialogConfirmed;
            }
        }

        function ConfirmDialogReloadd(ctl) {
            //debugger;
            var _mVal = document.getElementById(ctl).value;
            if (_mVal == 'DEN' || _mVal == 'OER' || _mVal == 'OGP' || _mVal == 'VAC' || _mVal == 'PHA') {
                document.getElementById('val').value = "";
                return true;
            } else {
                if (!dialogConfirmed) {


                    $('#dialog-confirm').dialog
                ({
                    height: 310,
                    width: 650,
                    modal: true,
                    resizable: false,
                    draggable: false,

                    buttons:
                    {
                        'Yes': function () {
                            $(this).dialog('close');
                            dialogConfirmed = true;
                            document.getElementById('val').value = "Y";
                            document.getElementById('lblvalTxt').value = "Y";
                            //if (obj) obj.click();
                        },
                        'No': function () {
                            $(this).dialog('close');
                            dialogConfirmed = true;
                            document.getElementById('val').value = "N";
                            document.getElementById('lblvalTxt').value = "N";
                            //if (obj) obj.click();
                        }
                    }
                });
                }

                return dialogConfirmed;
            }
        }

        // Parse URL Queries
        function url_query(query) {
            query = query.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var expr = "[\\?&]" + query + "=([^&#]*)";
            var regex = new RegExp(expr);
            var results = regex.exec(window.location.href);
            if (results !== null) {
                return results[1];
            } else {
                return false;
            }
        }
        //debugger;
        $(window).load(function () {
            var isReload = isPostBack();
            var url_param = url_query('PID');
            if (isReload != "True") {
                if (url_param) {
                   // ConfirmDialogReloadd('ddlDepartment');
                }
            }
        });



        function GetValueFromChild(myVal) {

            document.getElementById('uploadSession').value = myVal;

        }

        function isPostBack() {
            //function to check if page is a postback-ed one
            // debugger;    
            return document.getElementById('_ispostback').value;
        }

    </script>



    <script type="text/javascript">

        function ShowHideDiv() {
            //Get dropdown selected value
            //debugger;
            var SelectedValue = $('#<%= ddlPharmacyDiag.ClientID %> option:selected').val();
            //alert(SelectedValue);
            if (SelectedValue == 0) {

                $('#maternityDiv').css("display", "none");

                if ($('#<%=(rfvMaternity.ClientID)%>').length > 0) {
                ValidatorEnable(document.getElementById('rfvMaternity'), false);
                ValidatorEnable(document.getElementById('rfvGravida'), false);
                ValidatorEnable(document.getElementById('rfvPara'), false);
                ValidatorEnable(document.getElementById('rfvLive'), false);
                ValidatorEnable(document.getElementById('rfvAbortion'), false);
                ValidatorEnable(document.getElementById('rfvDeath'), false);
                ValidatorEnable(document.getElementById('rfvHijriGregorian'), false);
                ValidatorEnable(document.getElementById('rfvDay'), false);
                ValidatorEnable(document.getElementById('rfvMonth'), false);
                ValidatorEnable(document.getElementById('rfvYear'), false);
                ValidatorEnable(document.getElementById('rfvExpectedDeliveryGreg'), false);
            }
        }

        else if (SelectedValue == 'Z39' || SelectedValue == 'Z41.2' || SelectedValue == 'Z41.3') {
            //If cash is selected then hide the Div
            $('#maternityDiv').css("display", "none");

            if ($('#<%=(rfvMaternity.ClientID)%>').length > 0) {
                    ValidatorEnable(document.getElementById('rfvMaternity'), false);
                    ValidatorEnable(document.getElementById('rfvGravida'), false);
                    ValidatorEnable(document.getElementById('rfvPara'), false);
                    ValidatorEnable(document.getElementById('rfvLive'), false);
                    ValidatorEnable(document.getElementById('rfvAbortion'), false);
                    ValidatorEnable(document.getElementById('rfvDeath'), false);
                    ValidatorEnable(document.getElementById('rfvHijriGregorian'), false);
                    ValidatorEnable(document.getElementById('rfvDay'), false);
                    ValidatorEnable(document.getElementById('rfvMonth'), false);
                    ValidatorEnable(document.getElementById('rfvYear'), false);
                    ValidatorEnable(document.getElementById('rfvExpectedDeliveryGreg'), false);
                }
                //or you can simply use jQuery hide method to hide the Div as below:
                //$('#dvShowHide').hide();           
            }
            else {

                //If Cheque is selected then show the Div
                //$('#maternityDiv').css("display", "block");
                //or you can simply use jQuery show method to show the Div as below:
                $('#maternityDiv').show();
                //Clear textboxes

                //yahya1
                if (SelectedValue == 'Z34' || SelectedValue == 'O80' || SelectedValue == 'O82' ||
                    SelectedValue == 'O20' || SelectedValue == 'O04' || SelectedValue == 'O34.3' ||
                    SelectedValue == 'O15.0' || SelectedValue == 'O42') {

                   // $.growlUI("<h3 style='color:#00AEF7;background-color:white'>New Maternity Automation<h3>", '<h4>We would like to inform you that we have applied a new enhancements on Maternity automation.<br /><br />نود اعلامكم بأنه قد تم اضافة تحسينات جديدة لعمليات أتمتة طلبات الأمومة</h4>', 7000);




                    //$("#dialog").dialog();
                }

                //Set focus in bank name textbox
                if ($('#<%=(rfvMaternity.ClientID)%>').length > 0) {

                    document.getElementById('rfvMaternity').enabled = true;
                    document.getElementById('rfvGravida').enabled = true;
                    document.getElementById('rfvPara').enabled = true;
                    document.getElementById('rfvLive').enabled = true;
                    document.getElementById('rfvAbortion').enabled = true;
                    document.getElementById('rfvDeath').enabled = true;
                    document.getElementById('rfvHijriGregorian').enabled = true;
                    document.getElementById('rfvDay').enabled = true;
                    document.getElementById('rfvMonth').enabled = true;
                    document.getElementById('rfvYear').enabled = true;
                    document.getElementById('rfvExpectedDeliveryGreg').enabled = true;

                    document.getElementById('rfvMaternity').isvalid = true;
                    document.getElementById('rfvGravida').isvalid = true;
                    document.getElementById('rfvPara').isvalid = true;
                    document.getElementById('rfvLive').isvalid = true;
                    document.getElementById('rfvAbortion').isvalid = true;
                    document.getElementById('rfvDeath').isvalid = true;
                    document.getElementById('rfvHijriGregorian').isvalid = true;
                    document.getElementById('rfvDay').isvalid = true;
                    document.getElementById('rfvMonth').isvalid = true;
                    document.getElementById('rfvYear').isvalid = true;
                    document.getElementById('rfvExpectedDeliveryGreg').isvalid = true;

                }
        }

            addOptions(SelectedValue, '');
            
    }

    function rblSelectedValue(rbl) {
        var selectedvalue = $("#" + rbl.id + " input:radio:checked").val();
        if (selectedvalue == 0 || selectedvalue == 1) {
            document.getElementById('txtDay').disabled = false;
            document.getElementById('txtMonth').disabled = false;
            document.getElementById('txtYear').disabled = false;
            document.getElementById('txtDay').focus();
        }
    }

    function rbnMaternitySelectedValue(rbl) {
        var prevIVF = document.getElementById('<%= hidPrevPreg.ClientID%>').value;
        //alert(prevIVF);
        var selectedvalue = $("#" + rbl.id + " input:radio:checked").val();
        if (selectedvalue == 1 && prevIVF.trim() == "Y") {
            window.confirm("Infertility/IVF/Induced pregnancy/Undeclared pregnancy was ticked on previous request. Please be accurate on this information.");
        }
        //else {
        //    window.confirm("Not selected value =" + selectedvalue + " prevIVF = " + prevIVF);
        //alert("Infertility/IVF/Induced pregnancy/Undeclared pregnancy was ticked on previous request. Please be accurate on this information.222");
        //}
    }

    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.]")
        if (key == 8) {
            keychar = String.fromCharCode(key);
        }
        if (key == 11) {
            key = 8;
            keychar = String.fromCharCode(key);
        }
        return reg.test(keychar);
    }

    function calculateDate(arg) {
        d = parseInt(arg.txtDay.value)
        m = parseInt(arg.txtMonth.value)
        y = parseInt(arg.txtYear.value)

        if (isNaN(d) || isNaN(m) || isNaN(y)) {
            alert("Please select date type and fill day, month, year");
            return false;
        }

        if ($("#RadioDiv input:radio:checked").val() == 0) {
            convertToGreg(arg);
        }
        else if ($("#RadioDiv input:radio:checked").val() == 1) {
            convertToHijri(arg);
        }

        ValidatorEnable(document.getElementById('rfvExpectedDeliveryGreg'), false);
    }

    function convertToHijri(arg) {
        d = parseInt(arg.txtDay.value)
        m = parseInt(arg.txtMonth.value)
        y = parseInt(arg.txtYear.value)

        arg.txtLastPeriodGreg.value = d + '/' + m + '/' + y;
        arg.txtLastPeriodHijri.value = calculateHijri(d, m, y);

        dateEx = new Date(y, m, d);
        dateEx.setDate(dateEx.getDate() + parseInt(239));

        arg.txtExpectedDeliveryGreg.value = dateEx.getDate() + '/' + (dateEx.getMonth() + 1) + '/' + dateEx.getFullYear();
        arg.txtExpectedDeliveryHijri.value = calculateHijri(dateEx.getDate(), (dateEx.getMonth() + 1), dateEx.getFullYear())
    }

    function calculateHijri(d, m, y) {
        if ((y > 1582) || ((y == 1582) && (m > 10)) || ((y == 1582) && (m == 10) && (d > 14))) {
            jd = intPart((1461 * (y + 4800 + intPart((m - 14) / 12))) / 4) + intPart((367 * (m - 2 - 12 * (intPart((m - 14) / 12)))) / 12) -
intPart((3 * (intPart((y + 4900 + intPart((m - 14) / 12)) / 100))) / 4) + d - 32075
        }
        else {
            jd = 367 * y - intPart((7 * (y + 5001 + intPart((m - 9) / 7))) / 4) + intPart((275 * m) / 9) + d + 1729777
        }
        l = jd - 1948440 + 10632
        n = intPart((l - 1) / 10631)
        l = l - 10631 * n + 354
        j = (intPart((10985 - l) / 5316)) * (intPart((50 * l) / 17719)) + (intPart(l / 5670)) * (intPart((43 * l) / 15238))
        l = l - (intPart((30 - j) / 15)) * (intPart((17719 * j) / 50)) - (intPart(j / 16)) * (intPart((15238 * j) / 43)) + 29
        m = intPart((24 * l) / 709)
        d = l - intPart((709 * m) / 24)
        y = 30 * n + j - 30
        return d + '/' + m + '/' + y;
    }

    function intPart(floatNum) {
        if (floatNum < -0.0000001) {
            return Math.ceil(floatNum - 0.0000001)
        }
        return Math.floor(floatNum + 0.0000001)
    }

    function convertToGreg(arg) {
        d = parseInt(arg.txtDay.value)
        m = parseInt(arg.txtMonth.value)
        y = parseInt(arg.txtYear.value)

        arg.txtLastPeriodHijri.value = d + '/' + m + '/' + y;

        jd = intPart((11 * y + 3) / 30) + 354 * y + 30 * m - intPart((m - 1) / 2) + d + 1948440 - 385

        if (jd > 2299160) {
            l = jd + 68569
            n = intPart((4 * l) / 146097)
            l = l - intPart((146097 * n + 3) / 4)
            i = intPart((4000 * (l + 1)) / 1461001)
            l = l - intPart((1461 * i) / 4) + 31
            j = intPart((80 * l) / 2447)
            d = l - intPart((2447 * j) / 80)
            l = intPart(j / 11)
            m = j + 2 - 12 * l
            y = 100 * (n - 49) + i + l
        }
        else {
            j = jd + 1402
            k = intPart((j - 1) / 1461)
            l = j - 1461 * k
            n = intPart((l - 1) / 365) - intPart(l / 1461)
            i = l - 365 * n + 30
            j = intPart((80 * i) / 2447)
            d = i - intPart((2447 * j) / 80)
            i = intPart(j / 11)
            m = j + 2 - 12 * i
            y = 4 * k + n + i - 4716
        }

        arg.txtLastPeriodGreg.value = d + '/' + m + '/' + y;

        dateEx = new Date(y, m, d);
        dateEx.setDate(dateEx.getDate() + parseInt(239));

        arg.txtExpectedDeliveryGreg.value = dateEx.getDate() + '/' + (dateEx.getMonth() + 1) + '/' + dateEx.getFullYear();
        arg.txtExpectedDeliveryHijri.value = calculateHijri(dateEx.getDate(), (dateEx.getMonth() + 1), dateEx.getFullYear())

    }

    function calculateGreg(d, m, y) {

        jd = intPart((11 * y + 3) / 30) + 354 * y + 30 * m - intPart((m - 1) / 2) + d + 1948440 - 385

        if (jd > 2299160) {
            l = jd + 68569
            n = intPart((4 * l) / 146097)
            l = l - intPart((146097 * n + 3) / 4)
            i = intPart((4000 * (l + 1)) / 1461001)
            l = l - intPart((1461 * i) / 4) + 31
            j = intPart((80 * l) / 2447)
            d = l - intPart((2447 * j) / 80)
            l = intPart(j / 11)
            m = j + 2 - 12 * l
            y = 100 * (n - 49) + i + l
        }
        else {
            j = jd + 1402
            k = intPart((j - 1) / 1461)
            l = j - 1461 * k
            n = intPart((l - 1) / 365) - intPart(l / 1461)
            i = l - 365 * n + 30
            j = intPart((80 * i) / 2447)
            d = i - intPart((2447 * j) / 80)
            i = intPart(j / 11)
            m = j + 2 - 12 * i
            y = 4 * k + n + i - 4716
        }

        return d + '/' + m + '/' + y;
    }
    </script>

    <div id="dialog-confirm" title="Bupa Confirmation" style="display: none; width: 650px">
        <input type="hidden" id="_ispostback" value="<%=Page.IsPostBack.ToString()%>" />
        <table style="width: 550px; font-size: 16px;">
            <tr>
                <td colspan="3">
                    <h2>Does your request contain any of the following:</h2>

                </td>
            </tr>
            <tr>
                <td>MRI Scan<br />
                    CT Scan<br />
                    Physiotherapy<br />
                    Day case
                    <br />
                    Surgeries
                </td>
                <td>Hearing Aid<br />
                    Optical Cases<br />
                    Dialysis<br />
                    Medication for more than 1 month
                </td>
                <td>Endoscopy<br />
                    Dental<br />
                    Maternity<br />
                </td>
            </tr>
        </table>
    </div>


    <script type="text/javascript">

        function confirmAction(ctl) {

            var _mVal = ctl.value;
            if (_mVal == 'DEN' || _mVal == 'OER' || _mVal == 'OGP' || _mVal == 'VAC' || _mVal == 'PHA') {
                document.getElementById('val').value = "No";

                return true;

            } else {
                if ($.prompt('', { buttons: { Ok: true, Cancel: false }, focus: 1 })) {

                    document.getElementById('val').value = "Yes";

                    return true;
                }
                else {

                    document.getElementById('val').value = "No";

                    return true;
                }
            }

        }

    </script>
    <script language="javascript" type="text/javascript">
        function Confirm() {
            document.getElementById('columnlist').style.display = 'block';
            document.getElementById('main').style.display = 'block';
        }
    </script>
    <script type='text/JavaScript' src='scw.js'></script>
    <script type='text/JavaScript'>

        function newWindow(url) {
            popupWindow = window.open(url,
                                  'popUpWindow',
                                  'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');
        }
        $(function () {


            $('#<%= btnSubmit.ClientID %>').click(function () {
                $.blockUI({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
            });

        });

        function toggleDiv1() {

            if (document.getElementById("myDivBox").style.visibility == "visible") {
                document.getElementById("myDivBox").style.display = "none";
                document.getElementById("myDivBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("myDivBox").style.display = "block";
                document.getElementById("myDivBox").style.visibility = "visible";
            }
        }


        function child_openurl(url) {

            popupWindow = window.open(url, "_blank", 'height=400,width=800,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');

        }
    </script>
    <script type="text/javascript">

        var Page;

        function pageLoad() {

            Page = Sys.WebForms.PageRequestManager.getInstance();

            Page.add_initializeRequest(OnInitializeRequest);

        }

        function OnInitializeRequest(sender, args) {

            var postBackElement = args.get_postBackElement();

            if (Page.get_isInAsyncPostBack()) {



                args.set_cancel(true);

            }

        }

    </script>
    <script src="../javascript/jquery.idletimer.js" type="text/javascript"></script>
    <script src="../javascript/jquery.idletimeout.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $.idleTimeout('#idletimeout', '#idletimeout a', {
                idleAfter: 3510, // 3600sec minus 90sec = 3510 sec 
                pollingInterval: 2,
                keepAliveURL: 'keepalive.php',
                serverResponseEquals: 'OK',
                onTimeout: function () {
                    $(this).slideUp();
                    window.location = "../SessionExpired.aspx";
                },
                onIdle: function () {
                    $(this).slideDown(); // show the warning bar
                },
                onCountdown: function (counter) {
                    $(this).find("span").html(counter); // update the counter
                },
                onResume: function () {
                    $(this).slideUp(); // hide the warning bar
                }
            });
        });
    </script>
    <script language="javascript" type="text/javascript">

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
        });

        prm.add_endRequest(function () {
            $('#entryForm').unblock();
        });

        // Progress indicator preloading.
        var preload = document.createElement('img');
        preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
        delete preload;

        $(document).ready(function () {
            $(window).load(function () { $.unblockUI(); }); //load everything including images.  
            //--or--  
            // $.unblockUI(); //only make sure the document is full loaded, including scripts.  
        });

    </script>

    <div id="entryForm">
        <%--<div id="dialog" title="Basic dialog">
  <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>--%>

        <asp:UpdatePanel runat="server" ID="updatePanel1" UpdateMode="Conditional">

            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnProceed" />
                <asp:AsyncPostBackTrigger ControlID="btnAdd" />
                <asp:AsyncPostBackTrigger ControlID="btnUpdate" />
                <asp:AsyncPostBackTrigger ControlID="btnSubmit" />
                <asp:AsyncPostBackTrigger ControlID="radComboCategory" />
                <asp:AsyncPostBackTrigger ControlID="ddlCategory" />
                


            </Triggers>
            <ContentTemplate>
     <div id="dialog-Automation" title="Bupa Automation Confirmation" style="display: none; width: 950px">
        <input type="hidden" id="_ispostback" value="<%=Page.IsPostBack.ToString()%>" />
  

         <%--<uc1:ServiceCategoryOptions runat="server" ID="ServiceCategoryOptions" />--%>

          <div id="dvServiceCategoryOptions" runat="server">    </div>
    </div>

                <script type="text/javascript">
                    Sys.Application.add_load(ShowHideDiv);
                </script>
                <h1>
                    <asp:Label ID="lblAdd_Employee" runat="server" Text="Pre-Authorisation New Request"
                        Font-Size="Large" Font-Names="Arial"></asp:Label>
                </h1>
                <asp:Panel ID="PnlVerify" runat="server" BorderColor="Blue" BorderWidth="0px" BackColor="White">


                    <asp:TextBox CssClass="textbox" ID="txtVerificationID" MaxLength="10" BackColor="white"
                        onmouseout="if(document.getElementById('txtVerificationID').value == '')  document.getElementById('btnProceed').disabled =false; "
                        ValidationGroup="VGroup" runat="server"></asp:TextBox>
                    <asp:Button CssClass="submitButton" ID="btnVerificationID" runat="server" ValidationGroup="VGroup"
                        Text="Verify" OnClick="btnVerificationID_Click" BackColor="Control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="VGroup"
                        runat="server" ControlToValidate="txtVerificationID" Display="Dynamic" ErrorMessage="Please type in the Verification ID."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                        ControlToValidate="txtVerificationID" Display="Dynamic" ErrorMessage="Incorrect Verification ID"
                        ValidationExpression="[1-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]" ValidationGroup="VGroup"
                        Width="131px"></asp:RegularExpressionValidator>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" BorderColor="White" Height="290px" BorderWidth="0px"
                    BackColor="White">
                    <fieldset>
                        <legend>
                            <h1>
                                <asp:Label ID="lblMemberDetails" runat="server" Text="Member Details"></asp:Label></h1>
                        </legend>
                        <table style="width: 458px; font-size: small;">
                            <tr>
                                <td style="width: 117px">
                                    <asp:Label ID="lblMembershipNo" runat="server" Text="<font color=red>*</font>Membership No"
                                        Width="101px"></asp:Label>

                                    <asp:TextBox type="text" value="" ID="uploadSession" runat="server" Visible="false"></asp:TextBox>
                                </td>
                                <td style="width: 242px">
                                    <asp:TextBox CssClass="textbox" ID="txtMembershipNo" runat="server" ValidationGroup="MbrDtlGroup"
                                        MaxLength="8"></asp:TextBox><br />
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMembershipNo"
                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[3-9]\d\d\d\d\d\d"
                                        ValidationGroup="MbrDtlGroup1">Field data invalid</asp:RegularExpressionValidator>--%>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtMembershipNo"
                                        ErrorMessage="RequiredFieldValidator" Display="Dynamic" ValidationGroup="MbrDtlGroup1">Field is Mandatory</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 301px">&nbsp;
                                </td>
                                <td style="width: 280px">
                                    <asp:Button CssClass="submitButton" ID="btnFetchDetails" runat="server" Visible="true"
                                        OnClick="btnFetchDetails_Click" Text="Get Member Details" Width="100%" BackColor="Control"
                                        ValidationGroup="MbrDtlGroup1" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3" style="width: 100%;">
                                    <asp:Label ID="lblMemberName" runat="server" Text="" Width="300px"></asp:Label>
                                    <p style="display: none;">
                                        <asp:Label ID="txtAgeInMonths" runat="server" Text=""></asp:Label>
                                        <asp:TextBox CssClass="textbox" ID="txtMemberName" runat="server" ValidationGroup="MbrDtlGroup"
                                            MaxLength="43"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtMemberName"
                                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="MbrDtlGroup">Field is Mandatory</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtMemberName"
                                            Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                                            Font-Bold="False" ValidationExpression="[a-zA-Z ]+" ValidationGroup="MbrDtlGroup"></asp:RegularExpressionValidator>
                                        <span style="color: #ff0066">*</span><asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label>
                                        <asp:TextBox CssClass="textbox" ID="txtAge" runat="server" ValidationGroup="MbrDtlGroup"
                                            MaxLength="3"></asp:TextBox><br />
                                        <%-- <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtAge"
                                ErrorMessage="RangeValidator" MaximumValue="120" MinimumValue="1" Type="Integer" Display="Dynamic" ValidationGroup="MbrDtlGroup">Age should be numeric</asp:RangeValidator>
                                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtAge" Display="Dynamic" ValidationGroup="MbrDtlGroup">Field is Mandatory</asp:RequiredFieldValidator>--%>
                                        <span style="color: #ff0066">*</span><asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                        <asp:TextBox CssClass="textbox" ID="ddlGender" runat="server" ValidationGroup="MbrDtlGroup"
                                            MaxLength="3"></asp:TextBox>
                                        <%--<div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlGender" runat="server" BackColor="#eff6fc" ValidationGroup="MbrDtlGroup">
                                <asp:ListItem Value="0">-Select-</asp:ListItem>
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList></div><br />
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlGender"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="MbrDtlGroup">Field is Mandatory</asp:RequiredFieldValidator>--%>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 117px">
                                    <asp:Label ID="lblMemberID" runat="server" Text="&nbsp;Member ID/Iqama ID" Width="154px"></asp:Label>
                                </td>
                                <td style="width: 242px">
                                    <asp:TextBox CssClass="textbox" ID="txtMemberID" MaxLength="10" runat="server" ValidationGroup="MbrDtlGroup"
                                        BackColor="White"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                        ControlToValidate="txtMemberID" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                                        ValidationExpression="\d{10}" Width="152px">Incorrect Saudi/Iqama ID</asp:RegularExpressionValidator>&nbsp;
                                </td>
                                <td style="width: 301px">
                                    <asp:Label ID="lblCardIssueNo" runat="server" Text="&nbsp;Card Issue No" Width="108px"></asp:Label>
                                </td>
                                <td style="width: 280px">
                                    <asp:TextBox CssClass="textbox" ID="txtCardIssueNo" runat="server" ValidationGroup="MbrDtlGroup"
                                        BackColor="White" MaxLength="3"></asp:TextBox>
                                    <asp:RangeValidator ID="RangeValidator2" runat="server" Type="Integer" ControlToValidate="txtCardIssueNo"
                                        Display="Dynamic" ErrorMessage="RangeValidator" MaximumValue="1000" MinimumValue="1">Field should be numeric</asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 117px">
                                    <asp:Label ID="lblMobileNo" runat="server" Text="&nbsp;Mobile No" Width="137px"></asp:Label>
                                </td>
                                <td style="width: 242px">
                                    <asp:TextBox CssClass="textbox" ID="txtMobileNo" runat="server" ValidationGroup="MbrDtlGroup"
                                        MaxLength="12" BackColor="White"></asp:TextBox>
                                </td>
                                <td style="width: 301px">
                                    <asp:Label ID="lblPolicyNo" runat="server" Text="&nbsp;Policy No"></asp:Label>
                                </td>
                                <td style="width: 280px">
                                    <asp:TextBox CssClass="textbox" ID="txtPolicyNo" runat="server" ValidationGroup="MbrDtlGroup"
                                        MaxLength="7" BackColor="White"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                <td style="width: 117px" valign="top">
                                    <span style="color: #ff0066">*</span><asp:Label ID="lblDepartment" runat="server"
                                        Text="Department"></asp:Label>
                                </td>
                                <td style="width: 242px" valign="top">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlDepartment" BackColor="#eff6fc" AutoPostBack="true"
                                            runat="server" Width="256px" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                                            onchange="ChangeToOutpatient(this.value); " ValidationGroup="MbrDtlGroup">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="CVC">Cardio-Vascular Clinic</asp:ListItem>
                                            <asp:ListItem Value="DEN">Dental Clinic</asp:ListItem>
                                            <asp:ListItem Value="DVC">Derma &amp; Venereal Clinic</asp:ListItem>
                                            <asp:ListItem Value="DEC">Diabetes &amp; Endocrinology Clinic</asp:ListItem>
                                            <asp:ListItem Value="ENT">E.N.T. Clinic</asp:ListItem>
                                            <asp:ListItem Value="GFM">General Practitioner/Family Medicine Clinic</asp:ListItem>
                                            <asp:ListItem Value="GEC">G.I &amp; Endoscopies Clinic</asp:ListItem>
                                            <asp:ListItem Value="IMC">Internal Medicine Clinic</asp:ListItem>
                                            <asp:ListItem Value="NPC">Neuro &amp; Psychiatric Clinic</asp:ListItem>
                                            <asp:ListItem Value="NSC">Neuro-Surgery Clinic</asp:ListItem>
                                            <asp:ListItem Value="OBG">Gynae Clinic</asp:ListItem>
                                            <asp:ListItem Value="OGP">Obstetrics Pregnancy Related (Automation)</asp:ListItem>
                                            <asp:ListItem Value="OPH">Ophthalmic Clinic</asp:ListItem>
                                            <asp:ListItem Value="OER">Ophthalmic Clinic &amp; Error of Refraction</asp:ListItem>
                                            <asp:ListItem Value="ORT">Orthopedics Clinic</asp:ListItem>
                                            <asp:ListItem Value="PUL">Pulmonary Clinic</asp:ListItem>
                                            <asp:ListItem Value="PED">Pediatric Clinic</asp:ListItem>
                                            <asp:ListItem Value="PHA">Pharmacy / medication</asp:ListItem>
                                            <asp:ListItem Value="SUR">Surgery Clinic</asp:ListItem>
                                            <asp:ListItem Value="URO">Urology Clinic</asp:ListItem>
                                            <asp:ListItem Value="VAC">Vaccination</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlDepartment"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0">Field is Mandatory</asp:RequiredFieldValidator>
                                </td>



                                <td style="width: 301px" valign="top">
                                    <span style="color: #ff0066">*</span><asp:Label ID="lblPatientFileNo" runat="server"
                                        Text="Patient File No"></asp:Label>
                                </td>
                                <td style="width: 280px" valign="top">
                                    <asp:TextBox CssClass="textbox" ID="txtPatientFileNo" runat="server" ValidationGroup="MbrDtlGroup"
                                        MaxLength="30"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPatientFileNo"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator">Field is Mandatory</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 117px" valign="top">
                                    <span style="color: #ff0066">*</span><asp:Label ID="lblNameOfPhysician" runat="server"
                                        Text="Physician Name" Width="105px"></asp:Label>
                                </td>
                                <td style="width: 242px" runat="server" id="tdPhysicianTextBox">
                                    <asp:TextBox CssClass="textbox" ID="txtNameOfPhysician" runat="server" ValidationGroup="MbrDtlGroup"
                                        MaxLength="40"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtNameOfPhysician"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator">Field is Mandatory</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                        ControlToValidate="txtNameOfPhysician" ErrorMessage="Incorrect name format, please use only alphabets"
                                        ValidationExpression="[a-zA-Z ]+"></asp:RegularExpressionValidator>
                                </td>
                                <td runat="server" id="tdPhysicianDropDown" visible="false">
                                    <asp:DropDownList CssClass="DropDownListCssClass" runat="server" ID="ddlPhysician" BackColor="#eff6fc" AutoPostBack="true" OnSelectedIndexChanged="ddlPhysician_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlPhysician"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0">Field is Mandatory</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 301px" valign="top">
                                    <asp:Label ID="lblTreatmentType" runat="server" Text="<font color=red>*</font>Treatment Type"></asp:Label>
                                </td>
                                <td style="width: 280px" valign="top">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlTreatmentType" BackColor="#eff6fc"
                                            name="ddlTreatmentType" onchange="ChangeToOutpatient(document.getElementById('ddlDepartment').value);"
                                            runat="server" ValidationGroup="MbrDtlGroup">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="I">In Patient</asp:ListItem>
                                            <asp:ListItem Value="O">Out Patient</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlTreatmentType"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0">Field is Mandatory</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr runat="server" visible="false" id="trOtherPhysicianName">
                                <td style="width: 117px" valign="top">
                                    <span style="color: #ff0066">*</span><asp:Label ID="Label1" runat="server"
                                        Text="Other Physician Name" Width="105px"></asp:Label>
                                </td>
                                <td style="width: 242px" runat="server" id="td1">
                                    <asp:TextBox CssClass="textbox" ID="txtOtherPhysicianName" runat="server" ValidationGroup="MbrDtlGroup"
                                        MaxLength="40"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtOtherPhysicianName"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator">Field is Mandatory</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                        ControlToValidate="txtOtherPhysicianName" ErrorMessage="Incorrect name format, please use only alphabets"
                                        ValidationExpression="[a-zA-Z ]+"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                              <tr><td colspan="4"><div id="dvPreAuthOpt" style="background-color: lightskyblue;" runat="server"></div></td>
                                </tr>
                            <tr>
                                <td style="width: 117px"></td>
                                <td style="width: 242px">
                                    <input type="hidden" id="val" runat="server" clientidmode="Static" />
                                    <asp:Label ID="lblval" ClientIDMode="Static" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:TextBox ID="lblvalTxt" ClientIDMode="Static" runat="server" Text="" Visible="false"></asp:TextBox>
                                    <asp:Label ID="lbldepartment1" ClientIDMode="Static" runat="server" Text="" Visible="false"></asp:Label>
                                </td>
                                <td style="width: 301px"></td>
                                <td style="width: 280px">
                                    <asp:Button CssClass="submitButton" ID="btnProceed" runat="server" OnClick="btnProceed_Click"
                                        Text="Proceed" BackColor="Control" Enabled="False" OnClientClick="return ConfirmDialog(this,'ddlDepartment');" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <br />
                <br />
                <br />
                <asp:Panel ID="UPnlDiagnosis" runat="server" Visible="false">
                    <fieldset>
                        <legend>
                            <h1>
                                <asp:Label ID="lbl_DiagnosisDetails" runat="server" Text="Diagnosis details"></asp:Label></h1>
                        </legend>
                        <asp:Label ID="lbl_PleaseTick" runat="server" Font-Size="Small" Text="Please Tick (x) where appropriate"></asp:Label><br />
                        <asp:Panel ID="PnlDentalStandardCheckList" runat="server">
                            <table style="font-size: small; width: 540px">
                                <tr>
                                    <td style="width: 25%; height: 29px;">
                                        <asp:CheckBox ID="chkCongenital" runat="server" Width="84px" Text="Congenital/Developmental" />
                                    </td>
                                    <td style="width: 25%; height: 29px;">
                                        <asp:CheckBox ID="chkRTA" runat="server" Width="116px" Text="RTA" />
                                    </td>
                                    <td style="width: 25%; height: 29px;">
                                        <asp:CheckBox ID="chkCheckup" runat="server" Width="77px" Text="Checkup" />
                                    </td>
                                    <td style="width: 25%; height: 29px;">&nbsp;<asp:CheckBox ID="chkWorkRelated" runat="server" Width="138px" Text="Work related accident" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="PnlStandardCheckList" runat="server">
                            <table style="font-size: small; width: 540px">
                                <tr>
                                    <td style="width: 25%; height: 22px">
                                        <asp:CheckBox ID="chkChronic" runat="server" Width="76px" Text="Chronic" />
                                    </td>
                                    <td style="width: 25%; height: 22px">
                                        <asp:CheckBox ID="chkVaccination" runat="server" Width="87px" Text="Vaccination" />
                                    </td>
                                    <td style="width: 25%; height: 22px">
                                        <asp:CheckBox ID="chkPsychiatric" runat="server" Width="85px" Text="Psychiatric" />
                                    </td>
                                    <td style="width: 25%; height: 22px">&nbsp;<asp:CheckBox ID="chkInfertility" runat="server" Width="78px" Text="Infertility" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="PnlDentalCheckList" runat="server">
                            <table style="font-size: small; width: 540px">
                                <tr>
                                    <td style="width: 25%">
                                        <asp:CheckBox ID="chkCleaning" runat="server" Width="85px" Text="Cleaning" />
                                    </td>
                                    <td style="width: 25%">
                                        <asp:CheckBox ID="chkOrthodontic" runat="server" Width="126px" Text="Orthodontics/Aesthetics" />
                                    </td>
                                    <td style="width: 25%"></td>
                                    <td style="width: 25%">&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </asp:Panel>
                        &nbsp;
                        <asp:Panel ID="PnlPharmacy" runat="server">
                            <table style="width: 700px">
                                <tr>
                                    <td style="width: 79px" colspan="2">
                                        <asp:Label ID="lblPharNotes" Visible="false" runat="server" Text="Note:<br>Press 'Request Medication' if the request is to refill existing medication of our member. <br>If member does not have existing medication, GO TO TREATMENT DETAILS BELOW."
                                            Width="510px"></asp:Label>
                                    </td>
                                    <td style="width: 439px"></td>
                                </tr>
                                <tr>
                                    <td style="width: 79px; vertical-align: top;">
                                        <asp:Label ID="Label5" runat="server" Text="<font color=red>*</font>Diagnosis Description"
                                            Width="210px"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ClientIDMode="Static" ID="ddlPharmacyDiag"
                                                BackColor="#eff6fc" runat="server" Width="380px" onChange="ShowHideDiv();addOptions(this.value, '');">
                                            </asp:DropDownList>
                                        </div>


                                        <%--<input  id="click" type="button" title="Click Me!"></input>--%>

                                        <asp:Panel runat="server" ID="pnlMaternity" Visible="false">

                                            <div id="maternityDiv" style="text-align: left; display:none;">
                                                <br />
                                                <table>
                                                    <%--                                        <tr>
                                            <td >
                                                <asp:Image ID="Image1" runat="server"   ImageUrl="~/images/new yellow.png" Width="55" Height="55" />
                                            </td>
                                        </tr>--%>
                                                    <tr>
                                                        <td>

                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/new.gif" Width="35px" Height="35px" />
                                                                        <strong>Maternity Type</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButtonList ID="rdnMaternity" runat="server" OnClick="rbnMaternitySelectedValue(this)">
                                                                            <asp:ListItem Value="0">Infertility/IVF/Induced/ Undeclared pregnancy</asp:ListItem>
                                                                            <asp:ListItem Value="1">Normal Pregnancy</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                        <asp:HiddenField runat="server" ID="hidPrevPreg" />
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvMaternity" runat="server" ErrorMessage="Please select maternity type"
                                                                            ControlToValidate="rdnMaternity" ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left;">

                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/new.gif" Width="35px" Height="35px" />
                                                                        <strong>Maternity History</strong>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gravida*
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtGravida" runat="server" onkeypress="javascript:return allownumbers(event);" MaxLength="2" Width="40"></asp:TextBox>

                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvGravida" runat="server" ErrorMessage="Please fill Gravida Number"
                                                                            ControlToValidate="txtGravida" ValidationGroup="1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Para*
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPara" runat="server" onkeypress="javascript:return allownumbers(event);" MaxLength="2" Width="40"></asp:TextBox>

                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvPara" runat="server" ErrorMessage="Please fill Para Number"
                                                                            ControlToValidate="txtPara" ValidationGroup="1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Live*
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLive" runat="server" onkeypress="javascript:return allownumbers(event);" MaxLength="2" Width="40"></asp:TextBox>

                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvLive" runat="server" ErrorMessage="Please fill Live number"
                                                                            ControlToValidate="txtLive" ValidationGroup="1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Abortion*
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAbortion" runat="server" onkeypress="javascript:return allownumbers(event);" MaxLength="2" Width="40"></asp:TextBox>

                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvAbortion" runat="server" ErrorMessage="Please fill Abortion number"
                                                                            ControlToValidate="txtAbortion" ValidationGroup="1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Death*
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDeath" runat="server" onkeypress="javascript:return allownumbers(event);" MaxLength="2" Width="40"></asp:TextBox>

                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvDeath" runat="server" ErrorMessage="Please fill Death number"
                                                                            ControlToValidate="txtDeath" ValidationGroup="1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>

                                                            </table>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>

                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/images/new.gif" Width="35px" Height="35px" />
                                                                        <strong>Maternity Details</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">Last Menstrual Period
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div id="RadioDiv">
                                                                            <asp:RadioButtonList ID="rdnHijriGregorian" runat="server" RepeatDirection="Horizontal" OnClick="rblSelectedValue(this)">
                                                                                <asp:ListItem Value="0">Hijri</asp:ListItem>
                                                                                <asp:ListItem Value="1">Gregorian</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvHijriGregorian" runat="server" ErrorMessage="Please select Date type"
                                                                            ControlToValidate="rdnHijriGregorian" ValidationGroup="1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtDay" runat="server" onkeypress="javascript:return allownumbers(event);"
                                                                                        MaxLength="2" Width="40" Enabled="false" EnableTheming="True"></asp:TextBox>
                                                                                </td>
                                                                                <td>/
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtMonth" runat="server" onkeypress="javascript:return allownumbers(event);"
                                                                                        MaxLength="2" Width="40" Enabled="false"></asp:TextBox>
                                                                                </td>
                                                                                <td>/
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtYear" runat="server" onkeypress="javascript:return allownumbers(event);"
                                                                                        MaxLength="4" Width="80" Enabled="false"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <input id="btnCalculateDate" type="button" value="Calculate" onclick="calculateDate(this.form)" /><br />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" style="font-size: xx-small; color: darkseagreen; font-weight: bold; font-style: italic;">Day
                                <asp:RequiredFieldValidator ID="rfvDay" runat="server" ErrorMessage="Required" Text="*" SetFocusOnError="True"
                                    ControlToValidate="txtDay" ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                </td>

                                                                                <td colspan="2" style="font-size: xx-small; color: darkseagreen; font-weight: bold; font-style: italic;">Month
                                <asp:RequiredFieldValidator ID="rfvMonth" runat="server" ErrorMessage="Required" Text="*" SetFocusOnError="True"
                                    ControlToValidate="txtMonth" ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                </td>

                                                                                <td style="font-size: xx-small; color: darkseagreen; font-weight: bold; font-style: italic;">Year
                                <asp:RequiredFieldValidator ID="rfvYear" runat="server" ErrorMessage="Required" Text="*" SetFocusOnError="True"
                                    ControlToValidate="txtYear" ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="6">
                                                                                    <asp:RequiredFieldValidator ID="rfvExpectedDeliveryGreg" runat="server" ErrorMessage="Please fill day, month, year and press calculate" SetFocusOnError="True"
                                                                                        ControlToValidate="txtExpectedDeliveryGreg" ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblLastPeriodHijri" runat="server" Text="Last Menstrual Period in Hijri"></asp:Label>

                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLastPeriodHijri" runat="server" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lbExpectedDeliveryHijril" runat="server" Text="Expected Delivery in Hijri"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtExpectedDeliveryHijri" runat="server" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblLastPeriodGregorian" runat="server" Text="Last Menstrual Period in Gregorian"></asp:Label><br />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLastPeriodGreg" runat="server" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lbExpectedDeliveryGregorian" runat="server" Text="Expected Delivery in Gregorian"></asp:Label><br />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtExpectedDeliveryGreg" runat="server" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                            </div>
                                        </asp:Panel>
                                        <asp:TextBox CssClass="textbox" ID="txtReqMed" Visible="true" runat="server" Text="false"
                                            Height="0px" BorderColor="transparent" BackColor="transparent" Width="0px"></asp:TextBox>
                                        <asp:Button CssClass="submitButton" ID="btnRequestMedication" runat="server" BackColor="Control"
                                            Text="Request Medication" Width="134px" OnClick="btnRequestMedication_Click" />
                                        <br />
                                        <label id="lblReqMedMessage" runat="server" visible="false" style="font-size: smaller; background-color: LightPink;">
                                            <b>Please Note</b>: Before pressing submit, please review and remove the <font size="3"
                                                face="Wingdings">þ</font>check mark on medications you don’t want to be included
                                            on this request.
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div id="others" style="display: none; position: static; background-color: white;"
                                            runat="server">
                                            <nobr><asp:Label id="lblOthers" runat="server" Text="Other" Width="210px"></asp:Label> 
                            <asp:TextBox CssClass="textbox" id="txtOthers" runat="server" BackColor="#eff6fc" Width="380px"  MaxLength="500" ></asp:TextBox> </nobr>
                                            <asp:RequiredFieldValidator ID="vldtrRFtxtOthers" runat="server" ControlToValidate="txtOthers"
                                                Enabled="false" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1">Field is Mandatory</asp:RequiredFieldValidator>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <asp:Panel ID="PnlDiagnosisCommon" runat="server">

                            <table style="width: 699px">
                                <tr>
                                    <td style="width: 79px">
                                        <asp:Label ID="lblChiefComplaintsAndMainSymptoms" runat="server" Text="Chief complaints and main symptoms"
                                            Width="199px"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtChiefComplaintsAndMainSymptoms" runat="server"
                                            TextMode="MultiLine" Width="380px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 220px">
                                                    <asp:Label ID="lblDiagnosisDescription" runat="server" Text="<font color=red>*</font>Diagnosis description"
                                                        Width="142px"></asp:Label>
                                                </td>
                                                <td class="style2">
                                                    <asp:TextBox CssClass="textbox" ID="txtDiagnosisDescription" BackColor="#eff6fc"
                                                        runat="server" Width="358px" ValidationGroup="1" ClientIDMode="Static" MaxLength="500"
                                                        Height="43px"></asp:TextBox><br />
                                                    <asp:RegularExpressionValidator ID="valtxtProjectDescription" runat="server" ControlToValidate="txtDiagnosisDescription"
                                                        Display="None" ErrorMessage="Diagnosis Description Should be less than or equale 500 Characters"
                                                        SetFocusOnError="True" ValidationExpression="^([\S\s]{0,500})$"></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ControlToValidate="txtDiagnosisDescription"
                                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1">Field is Mandatory</asp:RequiredFieldValidator>
                                                </td>
                                                <td class="style3" valign="top">
                                                    <asp:TextBox ID="txtCIDCode" Enabled="false" runat="server" Width="69px" ClientIDMode="Static" Visible="false"></asp:TextBox>
                                                </td>
                                                <td valign="top">
                                                    <input type="button" id="btnICD" runat="server" class="submitButton" value="ICD 10 Code"
                                                        onclick="child_openurl('icd.aspx');" visible="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 79px">
                                        <asp:Label ID="lblDiagCode" Visible="false" runat="server" Text="Diagnosis Code"
                                            Width="172px"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtDiagCode" BackColor="white" Visible="false"
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 79px">
                                        <asp:Label ID="lblDateOfVisit" runat="server" Text="<font color=red>*</font>Date of visit / admission"
                                            Width="144px"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtDateOfVisit" runat="server" Width="144px"
                                            onclick="scwShow(this,event);" onfocus="this.blur();" Style="cursor: default;"
                                            ValidationGroup="1"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDateOfVisit"
                                            ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="1"
                                            Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="176px"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="PnlStandard" runat="server">
                            <table style="width: 701px">
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblPulse" runat="server" Text="Pulse"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtPulse" BackColor="white" runat="server"></asp:TextBox>
                                        <asp:RangeValidator ID="RangeValidator3" runat="server" Type="Integer" ControlToValidate="txtPulse"
                                            Display="Dynamic" ErrorMessage="RangeValidator" MaximumValue="1000" MinimumValue="1"
                                            ValidationGroup="MbrDtlGroup">Field should be numeric</asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblTemperature" runat="server" Text="Temperature"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtTemperature" BackColor="white" runat="server"></asp:TextBox>
                                        <asp:RangeValidator ID="RangeValidator4" runat="server" Type="Double" ControlToValidate="txtTemperature"
                                            Display="Dynamic" ErrorMessage="RangeValidator" MinimumValue="0" MaximumValue="100"
                                            ValidationGroup="MbrDtlGroup">Field should be a decimal</asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblBloodPressure" runat="server" Text="Blood pressure"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtBloodPressure" runat="server" BackColor="white"
                                            MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblLastMenstruationPeriod" runat="server" Text="Last menstruation period"
                                            Width="184px"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtLastMenstruationPeriod" runat="server" onclick="scwShow(this,event);"
                                            onfocus="this.blur();" BackColor="white"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblPossibleLineOfTreatment" runat="server" Text="Possible line of treatment"
                                            Width="190px"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtPossibleLineOfTreatment" runat="server" TextMode="MultiLine"
                                            BackColor="white" Width="370px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblOtherConditions" runat="server" Text="Other conditions" Width="177px"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtOtherConditions" runat="server" TextMode="MultiLine"
                                            BackColor="white"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblDurationOfIllness" runat="server" Text="Duration of illness" Width="171px"></asp:Label>
                                    </td>
                                    <td style="width: 439px">
                                        <asp:TextBox CssClass="textbox" ID="txtDurationOfIllness" runat="server" BackColor="white"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <table style="width: 701px">
                            <tr>
                                <td style="width: 193px">
                                    <asp:Label ID="lblLengthOfStay" runat="server" Text="<font color=red>*</font>Length of stay"
                                        Visible="false" Width="112px"></asp:Label>
                                </td>
                                <td style="width: 439px">
                                    <asp:TextBox CssClass="textbox" ID="txtLengthOfStay" Visible="false" runat="server"
                                        ValidationGroup="1"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFVldtrLengthofStay" runat="server" Enabled="false"
                                        ControlToValidate="txtLengthOfStay" ErrorMessage="RequiredFieldValidator" ValidationGroup="1">Field is Mandatory</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareVldtrLengthofStay" runat="server" ValidationGroup="TreatmentGroup1"
                                        ErrorMessage="CompareValidator" Display="Dynamic" ControlToValidate="txtLengthOfStay"
                                        Type="Integer" ValueToCompare="0" Operator="GreaterThanEqual">Numeric value required</asp:CompareValidator>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Panel ID="PnlOptical" runat="server" Width="540px">
                            <table style="font-size: x-small; width: 540px; font-family: Arial">
                                <tr>
                                    <td style="width: 77px; height: 11px"></td>
                                    <td align="center" colspan="6" style="font-weight: bold; font-size: 12px; height: 11px; background-color: #ccffff"
                                        valign="bottom">
                                        <strong>Left Eye</strong>
                                    </td>
                                    <td style="width: 77px; height: 11px">
                                        <nobr>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;</nobr>
                                    </td>
                                    <td align="center" colspan="6" style="font-weight: bold; font-size: 12px; height: 11px; background-color: #ccffff"
                                        valign="bottom">
                                        <strong>Right Eye</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 36px; height: 22px"></td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">Sphere
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">Cylinder
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">Axis
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">Prism
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">V/A
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">PD
                                    </td>
                                    <td style="font-weight: bold; width: 33px; text-align: center"></td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">Sphere
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">Cylinder
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">Axis
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">Prism
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;">V/A
                                    </td>
                                    <td style="width: 33px; text-align: center; font-weight: bold;"></td>
                                </tr>
                                <tr>
                                    <td style="font-size: small; width: 36px">Distance
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyedistancesphere" type="text" runat="server" name="lefteyedistancesphere"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyedistancecylinder" type="text" runat="server" name="lefteyedistancecylinder"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyedistanceaxis" type="text" runat="server" name="lefteyedistanceaxis"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyedistanceprism" type="text" runat="server" name="lefteyedistanceprism"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyedistanceva" type="text" runat="server" name="lefteyedistanceva"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyedistancepd" type="text" runat="server" name="lefteyedistancepd"
                                            class="midbox" onblur="checknumber(this);" onkeyup="checkLength(this,10);" style="width: 33px" />
                                    </td>
                                    <td style="width: 77px"></td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyedistancesphere" type="text" runat="server" name="righteyedistancesphere"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyedistancecylinder" type="text" runat="server" name="righteyedistancecylinder"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyedistanceaxis" type="text" runat="server" name="righteyedistanceaxis"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyedistanceprism" type="text" runat="server" name="righteyedistanceprism"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyedistanceva" type="text" runat="server" name="righteyedistanceva"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center"></td>
                                </tr>
                                <tr>
                                    <td style="font-size: small; width: 36px">Near
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyenearsphere" type="text" runat="server" name="lefteyenearsphere"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyenearcylinder" type="text" runat="server" name="lefteyenearcylinder"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyenearaxis" type="text" runat="server" name="lefteyenearaxis" class="midbox"
                                            style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyenearprism" type="text" runat="server" name="lefteyenearprism" class="midbox"
                                            style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyenearva" type="text" runat="server" name="lefteyenearva" class="midbox"
                                            style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="lefteyenearpd" type="text" runat="server" name="lefteyenearpd" class="midbox"
                                            style="width: 33px" />
                                    </td>
                                    <td style="width: 77px"></td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyenearsphere" type="text" runat="server" name="righteyenearsphere"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyenearcylinder" type="text" runat="server" name="righteyenearcylinder"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyenearaxis" type="text" runat="server" name="righteyenearaxis" class="midbox"
                                            style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyenearprism" type="text" runat="server" name="righteyenearprism"
                                            class="midbox" style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center">
                                        <input id="righteyenearva" type="text" runat="server" name="righteyenearva" class="midbox"
                                            style="width: 33px" />
                                    </td>
                                    <td style="width: 33px; text-align: center"></td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </fieldset>
                </asp:Panel>
                <br />
                <asp:Panel ID="updatePanel" runat="server" Visible="false" UpdateMode="Conditional">
                    <fieldset>
                        <legend>
                            <h1>Services List</h1>
                        </legend>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Font-Names="Arial"
                            OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="GridView1_RowDataBound"
                            Width="90%" OnRowDeleting="GridView1_RowDeleting" EmptyDataText="There is No Data">
                            <Columns>
                                <asp:BoundField DataField="Benefit" HeaderText="Benefit" Visible="False">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ServiceCode" HeaderText="Service Code">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ServiceDesc" HeaderText="Service Description">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="period" HeaderText="Supply Period" Visible="False">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DateFrom" HeaderText="Supply From" Visible="False">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DateTo" DataFormatString="{0:d MMM yyyy}" HeaderText="Supply To"
                                    HtmlEncode="False" Visible="False">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" Wrap="false" />
                                </asp:BoundField>
                                <%--<asp:BoundField DataField="Quantity" HeaderText="Quantity">
                        <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                        <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />                    </asp:BoundField>
                    <asp:BoundField DataField="Cost" HeaderText="Estimated Cost">
                         <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                        <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                    </asp:BoundField>
                                --%>
                                <asp:TemplateField HeaderText="Quantity">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <EditItemTemplate>
                                        <asp:TextBox CssClass="textbox" ID="TextBox1" BackColor="AliceBlue" runat="server"
                                            Text='<%# Bind("Quantity") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%# GetQuantity( int.Parse( Eval( "Quantity" ).ToString( ) ) ).ToString( "N2" )%>
                                        <%--        <asp:Label ID="Label1" runat="server"  OnLoad="GetUnitPrice()" Text='<%# Bind("Quantity") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox CssClass="textbox" ID="TextBox1234" BackColor="Brown" Visible="true"
                                            runat="server" Text='<%# GetTotalQuantity( ).ToString( "N2" )%>'></asp:TextBox>
                                        <%# GetTotalQuantity( ).ToString( "N2" )%>
                                    </FooterTemplate>
                                    <ItemStyle HorizontalAlign="Center" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle Font-Size="X-Small" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Estimated Cost">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <EditItemTemplate>
                                        <asp:TextBox CssClass="textbox" ID="TextBox2" runat="server" BackColor="AliceBlue"
                                            Text='<%# Bind("Cost") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%# GetCost( decimal.Parse( Eval( "Cost" ).ToString( ) ) ).ToString( "N" )%>
                                        <%-- <asp:Label ID="Label2" runat="server" Text='<%# Bind("Cost") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <%# GetTotalCost( ).ToString( "N" )%>
                                    </FooterTemplate>
                                    <ItemStyle HorizontalAlign="Center" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle Font-Size="X-Small" ForeColor="Navy" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <%--  -----%>
                                <asp:BoundField DataField="teethMap" HeaderText="Tooth No" Visible="False">
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Request Refill">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="Chec" runat="server" Enabled="true" EnableViewState="true" Checked="true" />
                                    </ItemTemplate>
                                    <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ShowSelectButton="True" SelectText="Edit">
                                    <HeaderStyle BackColor="White" />
                                    <ItemStyle BackColor="White" BorderColor="White" />
                                </asp:CommandField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                            CommandName="Delete" Text="Delete"></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle BackColor="White" />
                                    <ItemStyle BackColor="White" BorderColor="White" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="index" ShowHeader="False" Visible="False" />
                            </Columns>
                            <SelectedRowStyle BorderStyle="None" />
                            <HeaderStyle Font-Names="Arial" Font-Size="9pt" />
                        </asp:GridView>
                        <p style="text-align: right;">
                            <table>
                                <tr>
                                    <td align="center">Total Quantity
                                    </td>
                                    <td align="center">Total Cost
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox CssClass="textbox" ID="txtSumQuantity" runat="server" Text="0" Style="text-align: center"
                                            onfocus="this.blur();" Width="96px" Visible="False"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="textbox" ID="txtSumCost" runat="server" onfocus="this.blur();"
                                            Style="text-align: center" Text="0.0" Width="96px" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:TextBox CssClass="textbox" ID="txtTempQuantity" Visible="false" Text="0" runat="server"
                                BackColor="#C0FFC0"></asp:TextBox>
                            <asp:TextBox CssClass="textbox" ID="txtTempCost" Text="0.0" Visible="false" runat="server"
                                BackColor="#C0FFC0"></asp:TextBox>
                        </p>
                        <hr />
                        <asp:TextBox CssClass="textbox" ID="txtNeedEyeTable" runat="server" BackColor="#C0C0FF"
                            Visible="False"></asp:TextBox>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="UPnlTreatmentDets" Visible="false" runat="server">
                    <fieldset>
                        <legend>
                            <h1>Treatment Information</h1>
                        </legend>
                       <%-- <center>--%>
                        <table style="width: 850px;">
                            <tr>
                                <td style="width: 130px">
                                    <span style="color: #ff0066">*</span><asp:Label ID="lblServiceDesctxt" runat="server"
                                        Text="Service Description" Width="120px"></asp:Label>
                                </td>
                                <td colspan="3" style="width: 404px">

                                     <div class="dvMultilevelddl" id="dvMultilevelddl" runat="server"></div>
                                    &nbsp; &nbsp;  <asp:Label ID="lblServiceDesc" runat="server" Text=""></asp:Label>
                                    <div id="dvlblServiceDesc" runat="server"></div>

                                    <asp:TextBox CssClass="textbox" ID="txtServiceDesc" runat="server"   Width="441px"
                                        Style="display: block;" MaxLength="150"></asp:TextBox>
                                     <asp:TextBox CssClass="textbox" ID="txtHiddenOptions" runat="server" 
                                        Style="display: none;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vldtrRFtxtServDesc" runat="server" Enabled="false"
                                        ControlToValidate="txtServiceDesc" Display="Dynamic" ErrorMessage="RequiredFieldValidator"
                                        ValidationGroup="TreatmentGroup1" Text="Field is Manadatory - text" InitialValue=""></asp:RequiredFieldValidator>
                                    <!-- Test Area -->

                                  
                                
                                    <telerik:radcombobox runat="server" id="radComboCategory" datatextfield="Category" style="display:none;"
                                        enableloadondemand="True" onitemsrequested="RadComboBox2_ItemsRequested" height="200px" width="445px"
                                        onselectedindexchanged="RadComboBox2_SelectedIndexChanged"  ontextchanged="RadComboBox1_TextChanged" OnClientDropDownClosed="OnClientDropDownClosedHandler" OnClientClick="return ConfirmDialogAutomation(this,'RadComboBox2');"   onclientselectedindexchanged="OnClientSelectedIndexChanged" onclienttextchange="OnClientTextChange" >
                                     </telerik:radcombobox>

                                     <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlCategory" runat="server" 
                                            BackColor="#eff6fc" Width="410px" Style="display: none;" onChange=""
                                            ValidationGroup="" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="True">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                           
                                        </asp:DropDownList>
                                    </div>
                                    <!-- New Multilevel dropdownlist-->
                                    <style>
                                        #nav, #nav ul{
                                                         margin:0;
                                                         
                                                         list-style-type:none;
                                                         list-style-position:outside;
                                                         position:relative;
                                                         line-height:2.5em;
                                                     }
                                        
                                         #nav a:link, #nav a:active, #nav a:visited{
                                            display:block;
                                            padding:1.5px 1px;
                                            border:1px solid #333;
                                            color:#333;
                                            text-decoration:none;
                                            background-color:#EFF6FC;
                                         }

                                        #nav a:hover{
                                            background-color:#01AEF0;
                                            /*color:#333;*/
                                            color:white;
                                        }
                                        #nav li{
                                            float:left;
                                            position:relative;
                                        }
                                        #nav ul {
                                            position:absolute;
                                            width:12em;
                                            top:1.5em;
                                            display:none;
                                        }
                                        #nav li ul a{
                                            width:21.5em;
                                            float:left;
                                        }
                                        #nav ul ul{
	                                        top:auto;
	                                        }

                                        #nav li ul ul {
                                            left:21.2em;
                                            margin:0px 0 0 10px;
                                            }

                                        #nav li:hover ul ul, #nav li:hover ul ul ul, #nav li:hover ul ul ul ul{
                                            display:none;
                                            }
                                        #nav li:hover ul, #nav li li:hover ul, #nav li li li:hover ul, #nav li li li li:hover ul{
                                            display:block;
                                            }
                                        li ul a {
                                            background-color: buttonface;
                                            -webkit-border-radius: 3px;
                                            -moz-border-radius: 3px;
                                            -border-radius: 3px;
                                            border-radius: 3px;
                                        }
                                    </style>
                                    <script>

                                        function mainmenu() {
                                            $(" #nav ul ").css({ display: "none" }); // Opera Fix
                                            $(" #nav li").hover(function () {
                                                $(this).find('ul:first').css({ visibility: "visible", display: "none" }).show(400);
                                            }, function () {
                                                $(this).find('ul:first').css({ visibility: "hidden" });
                                            });
                                        }

                                        $(document).ready(function () {
                                            mainmenu();
                                        });

                                        $(" #nav ul ").css({ display: "none" }); // Opera Fix

                                        $(" #nav li").hover(function () { // here goes mouse over effect
                                        }, function () { // here goes mouse out effect
                                        });
                                        $(this).find('ul:first:hidden').css({ visibility: "visible", display: "none" }).show(400);

                                        $(this).find('ul:first').css({ visibility: "hidden" });

                                        $(document).ready(function () {
                                            mainmenu();
                                        });

                                    </script>
                                    <br/>
                                    

                                     <!-- End New Multilevel dropdownlist-->
                               <%-- <br/>
                                                <div id="dvCatOptionsForm" runat="server" style="border: 1px solid #333333; width:700px;overflow:auto;background-color: lightskyblue;" visible="false">    </div>
    <div id="dvScript" runat="server" visible="true"></div>--%>
                                    <!-- Test Area -->

                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlServiceDesc" runat="server"
                                            BackColor="#eff6fc" Width="410px" Style="display: block;" onChange="redirect(this.options.selectedIndex, this.value);calcCost();"
                                            ValidationGroup="TreatmentGroup1" OnDataBound="ddlServiceDesc_DataBound">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="O1" Text="Consultation"></asp:ListItem>
                                            <asp:ListItem Value="O5" Text="Contact Lenses"></asp:ListItem>
                                            <asp:ListItem Value="O2" Text="Eye Test"></asp:ListItem>
                                            <asp:ListItem Value="O6" Text="Frame"></asp:ListItem>
                                            <asp:ListItem Value="O3" Text="Regular Lenses"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlServiceDesc1" OnDataBound="ddlServiceDesc1_DataBound"
                                            Visible="false" runat="server" BackColor="#eff6fc" Width="410px" Style="display: block;"
                                            onChange="redirect(this.options.selectedIndex, this.value);" ValidationGroup="TreatmentGroup1">
                                        </asp:DropDownList>
                                    </div>
                                    <asp:XmlDataSource ID="XmlDataSourceDental" runat="server" DataFile="~/Provider/XMLFiles/ServiceDescriptionDental.xml"></asp:XmlDataSource>
                                    <asp:XmlDataSource ID="XmlDataSourceOptical" runat="server" DataFile="~/Provider/XMLFiles/ServiceDescriptionOptical.xml"></asp:XmlDataSource>
                                    <asp:XmlDataSource ID="XmlDataSourcePharmcy" runat="server" DataFile="~/Provider/XMLFiles/ServiceDescriptionPharmcy.xml"></asp:XmlDataSource>
                                    <asp:RequiredFieldValidator ID="vldtrRFddlServDesc" runat="server" ControlToValidate="ddlServiceDesc"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="TreatmentGroup1"
                                        Text="Field is Mandatory" InitialValue="0"></asp:RequiredFieldValidator>




                                </td>
                                <td> 
                                  

                                </td>
                            </tr>
                            <tr>
                                <td style="width: 130px">
                                    <asp:Label ID="lblQuantity" runat="server" Text="<font color=red>*</font>Quantity"></asp:Label>
                                </td>
                                <td style="width: 150px">
                                    <asp:TextBox CssClass="textbox" ID="txtQuantity" runat="server" Width="146px" ValidationGroup="TreatmentGroup1"
                                        onblur="Javascript:calcCost();"></asp:TextBox><br /><asp:RequiredFieldValidator ID="vldtrRQuantity"
                                            runat="server" ControlToValidate="txtQuantity" Display="Dynamic" ErrorMessage="RequiredFieldValidator"
                                            ValidationGroup="TreatmentGroup1">Please enter the quantity</asp:RequiredFieldValidator><asp:CompareValidator
                                                ID="vldtrCompareQuantity" runat="server" ControlToValidate="txtQuantity" Display="Dynamic"
                                                ErrorMessage="CompareValidator" Operator="GreaterThanEqual" Type="Integer" ValidationGroup="TreatmentGroup1"
                                                ValueToCompare="0">Numeric value required</asp:CompareValidator>
                                </td>
                                <td style="width: 50px"></td>
                                <td style="width: 50px"></td>
                                <td style="width: 150px"></td>
                            </tr>
                            <tr>
                                <td style="width: 130px">
                                    <span style="color: #ff0066">*</span>
                                    <asp:Label ID="lblCost" runat="server" Text="Estimated Cost"></asp:Label>
                                </td>
                                <td style="width: 150px">
                                    <asp:TextBox CssClass="textbox" ID="txtCost" ClientIDMode="Static" runat="server"
                                        Width="146px" ValidationGroup="TreatmentGroup1" onblur="Javascript:calcCost();"></asp:TextBox>
                                    <br /><asp:RequiredFieldValidator ID="vldtrRFCost" runat="server" ControlToValidate="txtCost"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="TreatmentGroup1">Please enter the estimated cost</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="vldtrCompareCost" runat="server" ControlToValidate="txtCost"
                                        Display="Dynamic" ErrorMessage="CompareValidator" Operator="GreaterThanEqual"
                                        Type="Currency" ValidationGroup="TreatmentGroup1" ValueToCompare="0">Numeric value required</asp:CompareValidator>
                                </td>
                                <td style="width: 50px"></td>
                                <td style="width: 50px"></td>
                                <td style="width: 150px"></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <asp:CheckBox ID="chk" runat="server" Text="Is this\there a referral?" />
                                </td>
                            </tr>
                            <tr><td colspan="5">

                                 <br/>
                                    <div id="dvCatOptionsForm" runat="server" style="border: 1px solid #333333; width:850px;overflow:auto;background-color:#01AEF0;" visible="false">    </div>
    <div id="dvScript" runat="server" visible="true"></div>
                                  <br/>
                                </td></tr>
                        </table>
                       <%-- </center>--%>
                        <asp:Panel runat="server" ID="PanelPharmacy" Visible="false" Width="540px">
                            <table style="width: 617px">
                                <tr>
                                    <td style="width: 136px; height: 34px;">
                                        <asp:Label ID="lblDateFrom" runat="server" Text="Supply Period From" Width="166px"></asp:Label>
                                    </td>
                                    <td style="width: 100px; height: 34px;">
                                        <asp:TextBox CssClass="textbox" ID="DateFrom" runat="server" Width="144px" onfocus="this.blur();"
                                            onclick="scwShow(this,event);" Style="cursor: default;" onchange="AddMonths();"
                                            onmouseout="AddMonths();" onblur="AddMonths();" ValidationGroup="TreatmentGroup"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ControlToValidate="DateFrom"
                                            ErrorMessage="RequiredFieldValidator" Font-Names="Verdana" ValidationGroup="TreatmentGroup"
                                            Display="Dynamic" Font-Size="Small" Text="Please fill in the Date" Width="176px"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="width: 10px; height: 34px;"></td>
                                    <td style="width: 160px; height: 34px;">&nbsp;
                                    </td>
                                    <td style="width: 20px; height: 34px;"></td>
                                    <td style="width: 100px; text-align: right; height: 34px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 136px; height: 13px;">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Supply Period" Width="100px"></asp:Label>
                                    </td>
                                    <td style="width: 100px; height: 13px;">
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlPeriod" runat="server" BackColor="#eff6fc"
                                                ValidationGroup="TreatmentGroup" Width="150px" onchange="AddMonths();">
                                                <asp:ListItem Value="0">Select</asp:ListItem>
                                                <asp:ListItem Value="1">1 Month</asp:ListItem>
                                                <asp:ListItem Value="2">2 Month</asp:ListItem>
                                                <asp:ListItem Value="3">3 Month</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlPeriod"
                                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="TreatmentGroup1"
                                            Text="Field is Mandatory" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="width: 10px; height: 13px;"></td>
                                    <td style="width: 160px; height: 13px;"></td>
                                    <td style="width: 20px; height: 13px;"></td>
                                    <td style="width: 100px; text-align: right; height: 13px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 136px; height: 13px;">
                                        <asp:Label ID="lblDateTo" runat="server" Text="Supply Period To" Width="100px"></asp:Label>
                                    </td>
                                    <td style="width: 100px; height: 13px;">
                                        <asp:TextBox CssClass="textbox" ID="txtDateTo" runat="server" onMouseover="AddMonths();"
                                            onfocus="this.blur();" Width="146px" ValidationGroup="TreatmentGroup"></asp:TextBox>
                                    </td>
                                    <td style="width: 10px; height: 13px;"></td>
                                    <td style="width: 160px; height: 13px;"></td>
                                    <td style="width: 20px; height: 13px;"></td>
                                    <td style="width: 100px; text-align: right; height: 13px;"></td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="PanelOptical" Visible="false" Width="540px">
                            <%--<input id="chkTest" value="Test" title="Test1" runat="server" type="checkbox"/>--%>
                            <table style="width: 694px">
                                <tr>
                                    <td style="width: 123px">
                                        <asp:Label ID="lblLensType" runat="server" Text="Lens Type" Width="120px"></asp:Label>
                                    </td>
                                    <td style="width: 150px">
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlLensType" Width="90px" onchange=" document.getElementById('txtLenseType').value = this.value; "
                                                runat="server" ValidationGroup="TreatmentGroup">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:TextBox CssClass="textbox" ID="txtLenseType" Style="display: none;" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="vldtrRFddlLenseType" runat="server" ControlToValidate="ddlLensType"
                                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="TreatmentGroup1"
                                            Text="Field is Mandatory" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label ID="lblServiceCode" runat="server" Text="Service Code" Width="89px"></asp:Label>
                                    </td>
                                    <td style="width: 150px">
                                        <asp:TextBox CssClass="textbox" ID="txtServiceCode" runat="server" Width="150px"
                                            ValidationGroup="TreatmentGroup"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 123px"></td>
                                    <td style="width: 150px"></td>
                                    <td style="width: 3px"></td>
                                    <td style="width: 100px"></td>
                                    <td style="width: 150px"></td>
                                </tr>
                            </table>
                            <asp:Panel runat="server" ID="Pane1" Width="531px" Enabled="false">
                                <input id="chkMulticoated" name="OpticalOptions" type="checkbox" value="Multicoated"
                                    runat="server" title="Text1" />
                                <asp:Label ID="LMulticoated" runat="server" Text="Multicoated" Width="145px"></asp:Label>
                                <input id="chkMedium" name="OpticalOptions" type="checkbox" value="Medium" runat="server"
                                    title="Text1" />
                                <asp:Label ID="lMedium" runat="server" Text="Medium" Width="145px"></asp:Label>
                                <input id="chkAntireflectingcoating" name="OpticalOptions" type="checkbox" value="Antireflectingcoating"
                                    runat="server" title="Text1" />
                                <asp:Label ID="lAntiReflectingCoating" runat="server" Text="AntiReflectingCoating"
                                    Width="145px"></asp:Label>
                                <input id="chkVarilux" name="OpticalOptions" type="checkbox" value="Varilux" runat="server"
                                    title="Text1" />
                                <asp:Label ID="lVarilux" runat="server" Text="Varilux" Width="145px"></asp:Label>
                                <input id="chkLenticular" name="OpticalOptions" type="checkbox" value="Lenticular"
                                    runat="server" title="Text1" />
                                <asp:Label ID="lLenticular" runat="server" Text="Lenticular" Width="145px"></asp:Label>
                                <input id="chkPhotosensitive" name="OpticalOptions" type="checkbox" value="Photosensitive"
                                    runat="server" title="Text1" />
                                <asp:Label ID="lPhotosensitive" runat="server" Text="Photosensitive" Width="145px"></asp:Label>
                                <input id="chklight" name="OpticalOptions" type="checkbox" value="light" runat="server"
                                    title="Text1" />
                                <asp:Label ID="lLight" runat="server" Text="Light" Width="145px"></asp:Label>
                                <input id="chkAntiscratch" name="OpticalOptions" type="checkbox" value="Antiscratch"
                                    runat="server" title="Text1" />
                                <asp:Label ID="lAntiscratch" runat="server" Text="AntiScratch" Width="145px"></asp:Label>
                                <input id="chkHighindex" name="OpticalOptions" type="checkbox" value="Highindex"
                                    runat="server" title="Text1" />
                                <asp:Label ID="lHighIndex" runat="server" Text="HighIndex" Width="145px"></asp:Label>
                                <input id="chkAspheric" name="OpticalOptions" type="checkbox" value="Aspheric" runat="server"
                                    title="Text1" />
                                <asp:Label ID="lAspheric" runat="server" Text="Aspheric" Width="145px"></asp:Label>
                                <input id="chkDark" name="OpticalOptions" type="checkbox" value="Dark" runat="server"
                                    title="Text1" />
                                <asp:Label ID="lDark" runat="server" Text="Dark" Width="145px"></asp:Label>
                                <input id="chkColored" name="OpticalOptions" type="checkbox" value="Colored" runat="server"
                                    title="Text1" />
                                <asp:Label ID="lColored" runat="server" Text="Colored" Width="145px"></asp:Label>
                                <input id="chkBifocal" name="OpticalOptions" type="checkbox" value="Bifocal" runat="server"
                                    title="Text1" onclick="CheckBiofocal_Single();" />
                                <asp:Label ID="lBifocal" runat="server" Text="Bifocal " Width="145px"></asp:Label>
                                <input id="chkSafetythickness" name="OpticalOptions" type="checkbox" value="Safetythickness"
                                    runat="server" title="Text1" />
                                <asp:Label ID="lSafetyThickness" runat="server" Text="SafetyThickness" Width="145px"></asp:Label>
                                <input id="chkSinglevision" name="OpticalOptions" type="checkbox" value="Singlevision"
                                    runat="server" onclick="CheckBiofocal_Single();" title="Text1" />
                                <asp:Label ID="lSingleVision" runat="server" Text="SingleVision" Width="145px"></asp:Label>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="PanelDental" Visible="false" Width="540px" Style="display: none; position: static; left: 152px; background-color: white;">
                            <%--<input id="chkTest" value="Test" title="Test1" runat="server" type="checkbox"/>--%>
                            <a name="DETAIL"></a>
                            <div id="myDivBox" style="display: none;">
                                <img src="TeethMap.jpg" width="337" height="342" usemap="#realtors" alt="" border="0"
                                    onclick="toggleDiv1();">
                            </div>
                            <table style="width: 356px;">
                                <tr>
                                    <td style="width: 130px">
                                        <asp:Label ID="lblTeethMap" runat="server" Text="Teeth Map" Width="160px"></asp:Label>
                                    </td>
                                    <td style="width: 306px">&nbsp;<input type="text" id="txtTeethMap" runat="server" onfocus="this.blur();" name="txtTeethMap"
                                        onclick="toggleDiv1();" style="width: 146px" />
                                        <asp:RequiredFieldValidator ID="RFVldtrTeethmap" runat="server" ControlToValidate="txtTeethMap"
                                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="TreatmentGroup1"
                                            Enabled="False">Field is Mandatory</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <div id="dvErrorMessage"  align="left" style="border: 1px solid black;color: red; width:850px;overflow:auto;font-size:14px;padding: 2px 2px 2px 2px;display:none;">

                        </div><br/>
                        <div align="center">
                            <asp:Button CssClass="submitButton" ID="btnAdd" runat="server" OnClick="Button1_Click"
                                Text="Add" OnClientClick="PopulateDRopdownForStandardOthers();DisableAddButton();return filldata() ;  "
                                ValidationGroup="TreatmentGroup1" onmouseover="return anyCheck1();" onfocus="return anyCheck1();   "
                                onKeyDown="if(event.keyCode==13) return anyCheck1();" Width="133px" BackColor="Control" />
                            <asp:Button CssClass="submitButton" ID="btnUpdate" runat="server" Text="Update" OnClick="Button3_Click"
                                ValidationGroup="TreatmentGroup1" Visible="False" OnClientClick="PopulateDRopdownForStandardOthers(); return filldata() ; "
                                BackColor="Control" />
                            <asp:Button CssClass="submitButton" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                Text="Cancel" Visible="false" BackColor="Control" />
                        </div>
                        <div align="right" visible="false">
                            <a href="#" style="display: none" onclick="JavaScript:newWindow('../SimpleUpload.aspx?UploadCategory=<%=Bupa.OSWeb.Helper.UploadCategory.PREAUTH_ADD %>&UserID=<%=Session["ProviderUserName"]%>&RequestID=<%=_InitialRequestID %>');">Click to upload pre-auth documents</a>
                        </div>
                        <table style="width: 100%">
                            <tr>
                                <td align="left">Click to upload pre-auth documents , Click <a href="#" onclick="child_openurl('uploadDocuments.aspx?sess=<%=strUploadSession%>')">here</a>
                                 
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSubmit" runat="server" BackColor="Control" CssClass="submitButton" OnClick="btnSubmit_Click" OnClientClick="DisableButton();" onmouseover="calculateChecked();" Text="Submit Request" ValidationGroup="1" Visible="false" Width="150px" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lblNoTreatmentDetError" runat="server" ForeColor="Red" Visible="False"
                            Width="402px"></asp:Label>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        <map name="realtors" id="realtors1">
                            <area shape="circle" coords="30,194,12" href="#DETAIL" onclick="showLocation('32');hideMyDiv();">
                            <area shape="circle" coords="76,128,6" href="#DETAIL" onclick="showLocation('B');hideMyDiv();">
                            <area shape="circle" coords="144,210,8" href="#DETAIL" onclick="showLocation('L');hideMyDiv();">
                            <area shape="circle" coords="184,213,12" href="#DETAIL" onclick="showLocation('18');hideMyDiv();">
                            <area shape="circle" coords="82,224,9" href="#DETAIL" onclick="showLocation('R');hideMyDiv();">
                            <area shape="circle" coords="90,234,8" href="#DETAIL" onclick="showLocation('Q');hideMyDiv();">
                            <area shape="circle" coords="104,241,8" href="#DETAIL" onclick="showLocation('P');hideMyDiv();">
                            <area shape="circle" coords="73,144,8" href="#DETAIL" onclick="showLocation('A');hideMyDiv();">
                            <area shape="circle" coords="73,194,10" href="#DETAIL" onclick="showLocation('T');hideMyDiv();">
                            <area shape="circle" coords="35,106,10" href="#DETAIL" onclick="showLocation('3');hideMyDiv();">
                            <area shape="circle" coords="64,60,13" href="#DETAIL" onclick="showLocation('6');hideMyDiv();">
                            <area shape="circle" coords="51,75,13" href="#DETAIL" onclick="showLocation('5');hideMyDiv();">
                            <area shape="circle" coords="81,114,7" href="#DETAIL" onclick="showLocation('C');hideMyDiv();">
                            <area shape="circle" coords="53,263,11" href="#DETAIL" onclick="showLocation('28');hideMyDiv();">
                            <area shape="circle" coords="118,294,13" href="#DETAIL" onclick="showLocation('24');hideMyDiv();">
                            <area shape="circle" coords="176,248,12" href="#DETAIL" onclick="showLocation('20');hideMyDiv();">
                            <area shape="circle" coords="139,113,8" href="#DETAIL" onclick="showLocation('H');hideMyDiv();">
                            <area shape="circle" coords="67,278,13" href="#DETAIL" onclick="showLocation('27');hideMyDiv();">
                            <area shape="circle" coords="144,142,9" href="#DETAIL" onclick="showLocation('J');hideMyDiv();">
                            <area shape="circle" coords="129,102,9" href="#DETAIL" onclick="showLocation('G');hideMyDiv();">
                            <area shape="circle" coords="134,288,14" href="#DETAIL" onclick="showLocation('23');hideMyDiv();">
                            <area shape="circle" coords="31,214,11" href="#DETAIL" onclick="showLocation('31');hideMyDiv();">
                            <area shape="circle" coords="30,141,14" href="#DETAIL" onclick="showLocation('1');hideMyDiv();">
                            <area shape="circle" coords="149,278,12" href="#DETAIL" onclick="showLocation('22');hideMyDiv();">
                            <area shape="circle" coords="185,196,12" href="#DETAIL" onclick="showLocation('17');hideMyDiv();">
                            <area shape="circle" coords="88,103,8" href="#DETAIL" onclick="showLocation('D');hideMyDiv();">
                            <area shape="circle" coords="101,94,7" href="#DETAIL" onclick="showLocation('E');hideMyDiv();">
                            <area shape="circle" coords="185,138,12" href="#DETAIL" onclick="showLocation('16');hideMyDiv();">
                            <area shape="circle" coords="115,95,9" href="#DETAIL" onclick="showLocation('F');hideMyDiv();">
                            <area shape="circle" coords="142,127,8" href="#DETAIL" onclick="showLocation('I');hideMyDiv();">
                            <area shape="circle" coords="134,49,11" href="#DETAIL" onclick="showLocation('10');hideMyDiv();">
                            <area shape="circle" coords="116,44,10" href="#DETAIL" onclick="showLocation('9');hideMyDiv();">
                            <area shape="circle" coords="150,57,11" href="#DETAIL" onclick="showLocation('11');hideMyDiv();">
                            <area shape="circle" coords="31,166,14" href="#DETAIL" onclick="showLocation('X');hideMyDiv();">
                            <area shape="circle" coords="98,42,10" href="#DETAIL" onclick="showLocation('8');hideMyDiv();">
                            <area shape="circle" coords="41,247,12" href="#DETAIL" onclick="showLocation('29');hideMyDiv();">
                            <area shape="circle" coords="99,295,13" href="#DETAIL" onclick="showLocation('25');hideMyDiv();">
                            <area shape="circle" coords="34,231,12" href="#DETAIL" onclick="showLocation('30');hideMyDiv();">
                            <area shape="circle" coords="31,122,13" href="#DETAIL" onclick="showLocation('2');hideMyDiv();">
                            <area shape="circle" coords="183,168,14" href="#DETAIL" onclick="showLocation('X');hideMyDiv();">
                            <area shape="circle" coords="120,242,9" href="#DETAIL" onclick="showLocation('O');hideMyDiv();">
                            <area shape="circle" coords="175,88,10" href="#DETAIL" onclick="showLocation('13');hideMyDiv();">
                            <area shape="circle" coords="165,263,13" href="#DETAIL" onclick="showLocation('21');hideMyDiv();">
                            <area shape="circle" coords="133,234,8" href="#DETAIL" onclick="showLocation('N');hideMyDiv();">
                            <area shape="circle" coords="81,286,11" href="#DETAIL" onclick="showLocation('26');hideMyDiv();">
                            <area shape="circle" coords="183,121,13" href="#DETAIL" onclick="showLocation('15');hideMyDiv();">
                            <area shape="circle" coords="40,90,12" href="#DETAIL" onclick="showLocation('4');hideMyDiv();">
                            <area shape="circle" coords="164,72,10" href="#DETAIL" onclick="showLocation('12');hideMyDiv();">
                            <area shape="circle" coords="141,222,9" href="#DETAIL" onclick="showLocation('M');hideMyDiv();">
                            <area shape="circle" coords="81,51,12" href="#DETAIL" onclick="showLocation('7');hideMyDiv();">
                            <area shape="circle" coords="145,196,10" href="#DETAIL" onclick="showLocation('K');hideMyDiv();">
                            <area shape="circle" coords="181,232,11" href="#DETAIL" onclick="showLocation('19');hideMyDiv();">
                            <area shape="circle" coords="76,209,7" href="#DETAIL" onclick="showLocation('S');hideMyDiv();">
                            <area shape="circle" coords="181,105,12" href="#DETAIL" onclick="showLocation('14');hideMyDiv();">
                        </map>
                    </fieldset>
                </asp:Panel>
                <asp:Label ID="Message1" ForeColor="Red" runat="server" Font-Size="12pt"></asp:Label>
                <asp:SqlDataSource ID="SqlDatSourceDental1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
                    SelectCommand="SELECT LTRIM(RTRIM(Value)) + '__' + LTRIM(RTRIM(cost)) AS value, LTRIM(RTRIM(text)) AS text FROM DrogCode WHERE (section = '3')"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSourcechronic" runat="server" ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
                    SelectCommand="SELECT LTRIM(RTRIM(Value)) + '__' + LTRIM(RTRIM(cost)) AS value, LTRIM(RTRIM(text)) AS text FROM DrogCode WHERE (section = '1')"></asp:SqlDataSource>
                <br />
                <br />

                <asp:TextBox CssClass="textbox" ID="txtMinor" runat="server" Style="display: none;"></asp:TextBox>
                <script type="text/jscript" language="javascript" src="newreqpreauthrules1.js"></script>

                <div id='content'>
                    <div id="basic-modal-content">
                        <p>
                            This contract has been extended to <b>
                                <asp:Literal runat="server" ID="litExtendedDate"></asp:Literal></b>
                            <br />
                            <br />
                            Please provide services to member in line with Table of Benefits.
                            <br />
                            <br />
                            You can print the Table of Benefits with Extension date.<br />
                        </p>
                        <div style="vertical-align: central;">
                            <p>
                                <a runat="server" id="aCloseOverlay" href="#" style="color: #fff; text-align: left;">Close</a>
                            </p>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

        <br />

        <br />
        <uc1:OSNav ID="OSNav2" runat="server" />
        <asp:TextBox CssClass="textbox" ID="TextBox3" runat="server" Style="display: none;"></asp:TextBox>

    </div>

    <script type="text/javascript">
        function goto(x) {
            window.location = x;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        You may submit your pre-authorisation request online and get an instant reply.
    </p>
</asp:Content>
