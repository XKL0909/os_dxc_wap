﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Provider_icd" Codebehind="icd.aspx.cs" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../functions/Login/jquery.js"></script>
    <script type="text/javascript" src="../functions/Login/jquery.anythingslider.min.js"></script>
    <script type="text/javascript" src="../functions/Login/cufon.js"></script>
    <script type="text/javascript" src="../functions/Login/Gotham_Book_400.font.js"></script>
    <script type="text/javascript" src="../functions/Login/cufon.init.js"></script>
    <script type="text/javascript" src="../functions/Login/functions.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.blockUI.js"></script>
     <script type="text/javascript" src="../Scripts/jquery.validate.min.js"></script>
   
     <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
    <script type="text/javascript"  src="js/jquery.dataTables.js"></script>


    <script language="javascript" type="text/javascript">
        function CloseWindow() {
            window.close();
        }
        
        function OnCellClick(s, row_index, col_index) {
            var rowVisibleIndex = row_index;
            var fName = s.GetColumn(col_index).fieldName;
        }

    </script>
    <style type="text/css">
        .style1
        {
            width: 118px;
        }
        .style2
        {
            width: 6%;
        }
        
    </style>

     <script type='text/JavaScript'>
         $(function () {
             $('#<%# btnfilter.ClientID %>').click(function () {
                 $.blockUI({ message: '<br/><img src="<%#Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
             });
             $('#<%# GridView1.ClientID %>').change(function () {
                 $.blockUI({ message: '<br/><img src="<%#Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
             });
         }); 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%" border="0">
        <tr>
            <td class="style1">Filter By </td>
            <td style="width:250px">
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                    RepeatDirection="Horizontal" 
                    onselectedindexchanged="RadioButtonList1_SelectedIndexChanged" 
                    AutoPostBack="True">
                    <asp:ListItem Value="0">Diagnosis Code</asp:ListItem>
                    <asp:ListItem Value="1">Description</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td style="width:1%">
                <asp:TextBox ID="txtICD" runat="server" ></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnfilter" runat="server" Text="  Get List  " 
                    onclick="btnfilter_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" 
            GridLines="None" onselectedindexchanged="GridView1_SelectedIndexChanged" 
            PageSize="50" Width="100%" AllowPaging="True" 
                    onpageindexchanging="GridView1_PageIndexChanging">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="ICD_Code" HeaderText="ICD_Code" 
                    SortExpression="ICD_Code" />
                <asp:BoundField DataField="ICD_Desc" HeaderText="ICD_Desc" 
                    SortExpression="ICD_Desc" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView></td>
        </tr>
    </table>
        <br />
    
        
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>" 
            SelectCommand="SELECT ICD_Code, ICD_Desc FROM ICDCode ORDER BY ICD_Code">
            <FilterParameters>
    <asp:ControlParameter 
     Name="ICD_Code" 
     ControlID="txtICD" 
     PropertyName="Text" />
  </FilterParameters>

        </asp:SqlDataSource>
    
    </div>
         <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="lnkSession" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                    <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                   <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                </asp:Panel>
                <div id="mainContent">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
