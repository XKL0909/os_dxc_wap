﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Configuration;


public partial class Provider_dispensed : System.Web.UI.Page
{
    DataSet ds = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
        }
        catch
        {

        }
        
        
        if(!string.IsNullOrEmpty(Convert.ToString(Session["tablePA"])))
        {
            if (!IsPostBack)
            {

                GridViewDataTextColumn column5 = new GridViewDataTextColumn();
                column5.FieldName = "ItemID";
                column5.VisibleIndex = 1;
                column5.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                column5.ReadOnly = true;
                column5.ReadOnly = true;

                GridViewDataTextColumn column1 = new GridViewDataTextColumn();
                column1.FieldName = "ProviderID";
                column1.VisibleIndex = 2;
                column1.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                column1.ReadOnly = true;
                column1.ReadOnly = true;

                GridViewDataTextColumn column2 = new GridViewDataTextColumn();
                column2.FieldName = "PAID";
                column2.VisibleIndex = 3;
                column2.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                column2.Caption = "PreAuth ID";
                column2.ReadOnly = true;
                column2.Visible = true;

                GridViewDataTextColumn column3 = new GridViewDataTextColumn();
                column3.FieldName = "ServicesCode";
                column3.VisibleIndex = 4;
                column3.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                column3.Caption = "Services Code";
                column3.ReadOnly = true;
                column3.Visible = true;

                GridViewDataCheckColumn column4 = new GridViewDataCheckColumn();
                column4.FieldName = "Dispensed";
                column4.VisibleIndex = 6;
                column4.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                column4.Caption = "Dispensed";
                column4.Visible = true;


                GridViewDataTextColumn column6 = new GridViewDataTextColumn();
                column6.FieldName = "Services Description";
                column6.VisibleIndex = 5;
                column6.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                column6.Caption = "Services Description";
                column6.Visible = true;

                gvData.Columns.Add(column5);
                gvData.Columns.Add(column1);
                gvData.Columns.Add(column2);
                gvData.Columns.Add(column3);
                gvData.Columns.Add(column4);
                gvData.Columns.Add(column6);


                ds = (DataSet)Session["tablePA"];
                gvData.DataSource = ds;
                gvData.DataBind();
                
            }

            else
            {
                ds = (DataSet)Session["tablePA"];
                gvData.DataSource = ds;
                gvData.DataBind();
            }
            
        }
  
    }

    protected void gvData_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ds = (DataSet)Session["tablePA"];
        gvData.DataSource = ds.Tables[0];
        gvData.DataBind();
    }
    protected void gvData_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {

        

    }

    protected void gvData_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        
        ASPxGridView gridView = (ASPxGridView)sender;
        ds = (DataSet)Session["tablePA"];
        System.Data.DataTable dataTable = ds.Tables[0];
        DataRow row = dataTable.Rows.Find(e.Keys[0]);
        IDictionaryEnumerator enumerator = e.NewValues.GetEnumerator();

        enumerator.Reset();
        string ProID = "";
        string ItemID = "";
        string PAID = "";
        string serverCode = "";
        bool __disp = false;
        int _count = 0;

        while (enumerator.MoveNext())
        {
            switch (_count)
            {
                case 0:
                    ItemID = enumerator.Value.ToString();
                    break;
                case 1:
                    ProID = enumerator.Value.ToString();
                    break;
                case 2:
                    if(string.IsNullOrEmpty(Convert.ToString(enumerator.Value)))
                    {
                        PAID = "";
                    }else{
                        PAID = enumerator.Value.ToString();
                    }
                    
                    break;
                case 3:
                    if (string.IsNullOrEmpty(Convert.ToString(enumerator.Value)))
                    {
                        serverCode = "";
                    }
                    else
                    {
                        serverCode = enumerator.Value.ToString();
                    }
                    
                    
                    break;
                case 5:
                    __disp = Convert.ToBoolean(enumerator.Value.ToString());
                    break;
            }
            _count = _count + 1;
            if (string.IsNullOrEmpty(Convert.ToString(enumerator.Value)))
            {
                row[enumerator.Key.ToString()] = "";
            }
            else
            {
                row[enumerator.Key.ToString()] = enumerator.Value;
            }
            
        }
        gridView.CancelEdit();
        e.Cancel = true;
        UpdateRecord(ItemID,ProID, PAID, serverCode, __disp, Convert.ToString(Session["ProviderUserName"]));



    }

    public void UpdateRecord(string _itemid,string _ProviderID, string _PAID, string _serviceCode, bool _dispensed, string _username)
    {

        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        }
        catch { }
        SqlParameter[] arParms = new SqlParameter[6];

        arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
        arParms[0].Value = Convert.ToInt32(_itemid);

        arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
        arParms[1].Value = _ProviderID;

        arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
        arParms[2].Value = _PAID;

        arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
        arParms[3].Value = _serviceCode;

        arParms[4] = new SqlParameter("@Dispensed", SqlDbType.Bit);
        arParms[4].Value = _dispensed;

        arParms[5] = new SqlParameter("@UserName", SqlDbType.NVarChar, 50);
        arParms[5].Value = _username;

        


        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Update_Dispensed", arParms);
        if (connection.State == ConnectionState.Open)
        {
            connection.Close();
        }
    }

    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }

    protected void gvData_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        try
        {
            if (e.Value.ToString().Contains("/"))
            {
               
            }
        }
        catch { }
    }


    protected void gvData_HtmlCommandCellPrepared(object sender, ASPxGridViewTableCommandCellEventArgs e)
    {
        try
        {
            if ((bool)gvData.GetRowValues(e.VisibleIndex, "Dispensed"))
            {

                e.Cell.Controls[0].Visible = false;
                e.Cell.Controls.Add(new LiteralControl("&nbsp"));
            }
        }
        catch { }

    }
    protected void lbtnClose_Click(object sender, EventArgs e)
    {
        this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.opener.Refresh(); window.close()", true);
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
}