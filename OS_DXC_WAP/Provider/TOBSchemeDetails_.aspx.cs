using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class TOBSchemeDetails_ : System.Web.UI.Page
{
    string strSchemeID;
    string strProviderCode;
    string strCustomerID;
    string strContractDuration;
    string strCYM;
   
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        SessionManager.CheckSessionTimeout();

        //strSchemeID = Request.QueryString["SID"];
        //strCustomerID = Request.QueryString["CID"];
        //strProviderCode = Request.QueryString["PC"];
        //strContractDuration = Request.QueryString["CD"];
        //string strCustomerID;
        
        //string strCustomerID;

        //if(Request.QueryString["CustomerID"] != "")
        //    strCustomerID = Request.QueryString["CustomerID"];

        //if (Request.QueryString["CYM"] != "")
        //    strContractDuration = Request.QueryString["CYM"];
        //if (Request.QueryString["SchemeID"] != "")
        //    strSchemeID = Request.QueryString["SchemeID"];

        //strProviderCode = "20087"; ;

        //if (strCustomerID != "" && strContractDuration != "" && strSchemeID != "")
        //    DisplayTOBScheme();

        //strSchemeID = "1";
        //strCustomerID = "10531300";
        //strProviderCode = "20087"; 
        //strContractDuration = "200901";;

     //   DisplayTOBSchemen(Request.QueryString["PID"], Request.QueryString["PC"]);

        //strProviderCode =  "20087";
        strProviderCode = Session["ProviderID"].ToString();


        if (Request.QueryString["Type"] == "Plan")
        {
            DisplayTOBSchemByPlan(Request.QueryString["PID"], Request.QueryString["PC"]);
        }
        else
        {
            if (Request.QueryString["CustomerID"] != "")
                strCustomerID = Request.QueryString["CustomerID"];

            if (Request.QueryString["CYM"] != "")
                strContractDuration = Request.QueryString["CYM"];
            if (Request.QueryString["SchemeID"] != "")
                strSchemeID = Request.QueryString["SchemeID"];

            if (strCustomerID != "" && strContractDuration != "" && strSchemeID != "")
                DisplayTOBScheme();
        }
    }


    private void DisplayTOBSchemByPlan(string PlanID, string ContractID)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsResponse_DN response;


        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            //request = new OS_DXC_WAP.CaesarWS.ReqTOBBySchemeRequest_DN();

            request.planID = PlanID;
            request.providerCode = strProviderCode;
            //request.membershipNo = MembershipNo;
            request.contractNo = ContractID;

            //request.schemeID = strSchemeID; //long.Parse(EpisodeNo);
            //request.customerID = strCustomerID; // long.Parse(PreauthID);
            //request.providerCode = strProviderCode;
            //request.contYYMM = strContractDuration;

            // .membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            //    ExceptionManager.TrackWSPerformance("TOBSchemeDetails", "ReqTOB", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), strProviderCode, "", "");
            response = ws.ReqTOB(request);// ws.RequestPreauthByID(request);
        //    ExceptionManager.TrackWSPerformance("TOBSchemeDetails", "ReqTOB", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), strProviderCode, "", "");


            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.status == "0")
            {
                //response.unified_Pol_Comply == "Y" then regulated

                lblHospitalAccommodation.Text = response.bedType;
                lblCustomerID.Text = response.contractNo_CCHI;
                lblCompanyName.Text = response.customerName;
                lblIPDDA.Text = response.dependentDeductibleAmountForInPatient;
                lblOPDDA.Text = response.dependentDeductibleAmountForOutPatient;
                lblIPDD.Text = response.dependentDeductiblePercentageForInPatient + " " + response.dependentDeductibleLimitForInPatient;
                lblOPDD.Text = response.dependentDeductiblePercentageForOutPatient + " Upto: " + response.dependentDeductibleMAXLimitForOutPatient;

                lblIPDM.Text = response.mainMemberDeductiblePercentageForInPatient;
                lblOPDM.Text = response.mainMemberDeductiblePercentageForOutPatient + " Upto: " + response.mainMemberDeductibleMAXLimitForOutPatient;
                lblOPDMA.Text = response.mainMemberDeductibleAmountForOutPatient;
                lblIPDMA.Text = response.mainMemberDeductibleAmountForInPatient;

                lblRLRMA.Text = "Main Member:" + response.mainMemberMandatoryReferralIndicator + "</br>Dependent: " + response.dependentMandatoryReferralIndicator;
lblEffectiveDate.Text =  String.Format("{0:d}", response.contractEffectiveDate)  + " to " +  String.Format("{0:d}", response.contractTerminationDate) ;

                //  response.mainMemberDeductibleLimitForInPatient;


                //   ;
                lblOverAllLimit.Text = response.overallLimit;
                lblPreauthorisationLimit.Text = response.preauthLimit;
                lblSchemeName.Text = response.planDescription;
                lblTOBSpecialInstruction.Text = response.special_Instruction;
                lblTreatmentType.Text = response.treatmentType;


                // response.unifiedPolicyComply;

                StringBuilder sbResponse = new StringBuilder(2200);
                sbResponse.Append("<table width=100% border=1  style=' border-style:none ; border-left-width: thin; border-left-color: silver; border-bottom-width: thin; border-bottom-color: silver; border-top-color: silver; border-right-width: thin; border-right-color: silver;'><tr><td width=20%><b>Benefit Type</td><td width='52%'><b>Treatment Covered</td><td width='14%'><b>Sub Limit</b></td><td width=14%><b>Deductable</td></tr>");

                for (int i = 0; i < response.benefitDescription.Length; i++)
                {
                    sbResponse.Append("<tr><td><b>" + response.benefitDescription[i] + "</b></td><td>" + response.benefitHead[i] + " &nbsp;</td><td>" + response.benefitTypeLimit[i] + "</td>");
                    if (response.benefitDescription[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                    sbResponse.Append("<td>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100 </td></tr>");  // response.service_Code
                    else
                        sbResponse.Append("<td>" + response.deductibleAmountOnBenefitTypes[i] + " &nbsp; </td></tr>");  // response.service_Code

                }

                sbResponse.Append("</table>");
                ServiceListReport.Visible = true;
                ServiceListReport.InnerHtml = sbResponse.ToString();


                if (response.unified_Pol_Comply.Trim() == "Y")
                {
                    Panel4.Visible = true;
                    Panel5.Visible = false;
                }
                else
                {
                    Panel5.Visible = true;
                    Panel4.Visible = false;
                }

                //lblNotes.Text = response.notes.ToString();
                //lblServiceCode.Text = response.service_Code.Length;
                //lblServiceDescription.Text = response.service_Desc.ToString();




                //response.
                // lblClassName.Text = response.className;
                // lblCustomerName.Text = response.companyName;

                // DisplayPreauthHistoryResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                //if (tabMemberName.HeaderText == "Member")
                //    tabMemberName.HeaderText = request.membershipNo;



            }
            else
                Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }





    private void DisplayTOBScheme()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        //}
        //catch (Exception ex)
        //{
        //    Message1.Text = ex.Message + " Please close the program and re-check the Web Service connection";
        //}


        OS_DXC_WAP.CaesarWS.ReqTOBBySchemeRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqTOBBySchemeResponse_DN response;
        //////if (txtMbrShp.Text.Trim().Length == 0 || txtTxnID.Text.Trim().Length == 0)
        //////{
        //////    Message1.Text = "Please insert all the fields before using the web service";
        //////    return;
        //////}
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqTOBBySchemeRequest_DN();
            request.schemeID = strSchemeID; //long.Parse(EpisodeNo);
            request.customerID = strCustomerID; // long.Parse(PreauthID);
            request.providerCode = strProviderCode;
            request.contYYMM = strContractDuration;
            
               // .membershipNo = MemberNo;
            //request.transactionID  = Int32.Parse(txtTxnID.Text.Trim());
            //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            //request.IqamaNo =  "5523234443";
            //request.TotalNoOfFamily = "3";
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            //      ExceptionManager.TrackWSPerformance("TOBSchemeDetails", "ReqTOBByScheme", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), strProviderCode, "", "");
            response = ws.ReqTOBByScheme(request);// ws.RequestPreauthByID(request);
      //      ExceptionManager.TrackWSPerformance("TOBSchemeDetails", "ReqTOBByScheme", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), strProviderCode, "", "");

            //response = ws.EnqMbrListInfo(request);
            // Cursor.Current = Cursors.Default;


            if (response.status == "0")
            {


                lblHospitalAccommodation.Text = response.bedType;
                lblCustomerID.Text = response.contractNo_CCHI;
                lblCompanyName.Text = response.customerName;
                lblIPDDA.Text = response.dependentDeductibleAmountForInPatient;
                lblOPDDA.Text = response.dependentDeductibleAmountForOutPatient;
                lblIPDD.Text = response.dependentDeductiblePercentageForInPatient + " " + response.dependentDeductibleLimitForInPatient;
                lblOPDD.Text = response.dependentDeductiblePercentageForOutPatient + " Upto: " + response.dependentDeductibleMAXLimitForOutPatient;

                lblIPDM.Text = response.mainMemberDeductiblePercentageForInPatient;
                lblOPDM.Text = response.mainMemberDeductiblePercentageForOutPatient + " Upto: " + response.mainMemberDeductibleMAXLimitForOutPatient;
                lblOPDMA.Text = response.mainMemberDeductibleAmountForOutPatient;
                lblIPDMA.Text = response.mainMemberDeductibleAmountForInPatient;

                lblRLRMA.Text = "Main Member:" + response.mainMemberMandatoryReferralIndicator + "</br>Dependent: " + response.dependentMandatoryReferralIndicator;
          //      lblEffectiveDate.Text = response.effectiveDate.ToString() + " to " + response.effectiveEndDate.ToString();
                lblEffectiveDate.Text = String.Format("{0:d}", response.effectiveDate) + " to " + String.Format("{0:d}", response.effectiveEndDate);

                //  response.mainMemberDeductibleLimitForInPatient;


                //   ;
                lblOverAllLimit.Text = response.overallLimit;
                lblPreauthorisationLimit.Text = response.preauthLimit;
                lblSchemeName.Text = response.schemeName;
                lblTOBSpecialInstruction.Text = response.specialInstruction;
                lblTreatmentType.Text = response.treatmentType;



                StringBuilder sbResponse = new StringBuilder(2200);
                sbResponse.Append("<table width=100% border=1  style=' border-style:none ; border-left-width: thin; border-left-color: silver; border-bottom-width: thin; border-bottom-color: silver; border-top-color: silver; border-right-width: thin; border-right-color: silver;'><tr><td width=20%><b>Benefit Type</td><td width='52%'><b>Treatment Covered</td><td width='14%'><b>Sub Limit</b></td><td width=14%><b>Deductable</td></tr>");

                for (int i = 0; i < response.benefitDesc.Length; i++)
                {
                    sbResponse.Append("<tr><td><b>" + response.benefitDesc[i] + "</b></td><td>" + response.benefitHead[i] + " &nbsp;</td><td>" + response.benefitTypeLmt[i] + "</td><td>");
                    if (response.deductiblePercentageOnBenefitTypes[i] != null || (response.deductiblePercentageOnBenefitTypes[i] != null && response.maxDeductibleLimitOnBenefitTypes[i] != null))
                        sbResponse.Append(response.deductiblePercentageOnBenefitTypes[i].ToString() + " Upto: " + response.maxDeductibleLimitOnBenefitTypes[i].ToString());
                    else
                        sbResponse.Append( response.deductibleAmountOnBenefitTypes[i] );
                    
                    sbResponse.Append(" &nbsp; </td></tr>");  // response.service_Code
                }

                sbResponse.Append("</table>");
                ServiceListReport.Visible = true;
                ServiceListReport.InnerHtml = sbResponse.ToString();

                Panel1.Visible = true;
                Panel3.Visible = true;
                if (response.unifiedPolicyComply.Trim() == "Y")
                {
                    Panel4.Visible = true;
                    Panel5.Visible = false;
                }
                else
                {
                    Panel5.Visible = true;
                    Panel4.Visible = false; 
                }
                //lblNotes.Text = response.notes.ToString();
                //lblServiceCode.Text = response.service_Code.Length;
                //lblServiceDescription.Text = response.service_Desc.ToString();




                //response.
                // lblClassName.Text = response.className;
                // lblCustomerName.Text = response.companyName;

                // DisplayPreauthHistoryResult(response);
                //dataGridView1.DataSource = WebServicesManager.WebServiceWrapper.GetArrayList(response.detail);
                //dataGridView1.DataBindingComplete = true;

                //if (tabMemberName.HeaderText == "Member")
                //    tabMemberName.HeaderText = request.membershipNo;



            }
            else
            {
                Message1.Text = response.errorMessage;
                Panel1.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                ServiceListReport.Visible = false;
            }  // Message1.Text = "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }






    //OS_DXC_WAP.CaesarWS.
    //private void DisplayPreauthHistoryResult(OS_DXC_WAP.CaesarWS.ReqTOBBySchemeResponse_DN response)
    //{
    //    ListItem itm;
    //    StringBuilder msge;


    //    string str1;
    //    string str2;
    //    str2 = "";
    //    str1 = "";
    //    StringBuilder sbResponse = new StringBuilder(2200);

    //    if (response.status == "0")
    //    {

    //        //dtl.proratedOverallAnnualLimit = ;


    //        sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Verdana;font-size:10px;  ;'><thead><tr><th>Pre-auth No</th>   <th>Episode No</th><th>Provider Name</th>   <th>Received Date/Time</th><th>Response Date/Time</th>   <th>Pre-auth Status</th></tr>	</thead><tbody> ");

    //        foreach (OS_DXC_WAP.CaesarWS.PreHisDetail_DN dtl in response.detail)
    //        {
    //            sbResponse.Append("<a href='PreauthDetails.aspx?PID=" + dtl.preAuthNo + "&PC=" + dtl.providerCode + "&EN=" + dtl.episodeID + "' style='cursor:hand;'><tr ><td>" + dtl.preAuthNo + "</td><td>" + dtl.episodeID + "</td><td>" + dtl.providerName + "</td><td>" + dtl.receivedDateTime.ToString() + "</td><td>" + dtl.submitDateTime.ToString() + "</td><td>" + dtl.preAuthStatus + "</td></tr></a>");

    //            //if (dtl.detail_ProratedLimit != -1)
    //            //    sbResponse.Append("<td>" + dtl.detail_ProratedLimit + ".00</td></tr>");
    //            //else
    //            //    sbResponse.Append("<td>Not Covered</td></tr>");


    //        }
    //        sbResponse.Append("</table>");
    //        CoverageListReport.Visible = true;
    //        CoverageListReport.InnerHtml = sbResponse.ToString();


    //    }
    //    else
    //    {
    //        //msge = new StringBuilder(100);
    //        //foreach (String s in response.errorMessage)
    //        //{
    //        //    msge.Append(s).Append("/n");
    //        //}
    //        //Message1.Text = msge.ToString();
    //    }

    //    //     BulletedList1.Items.Add(ResponseDetails[0,1].ToString());


    //}




}
