using System;
using System.Text;
using System.Web.UI;

public partial class ExistingRequestSearch : System.Web.UI.Page
{
    string strProviderID;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;

    void Page_Load(object sender, EventArgs e)
    {
        SessionManager.CheckSessionTimeout();
        if (string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            Response.Redirect("../default.aspx");
        strProviderID = Session["ProviderID"].ToString();
    }
    
    private void ReqPreauthListByMbrNoNDate(string MemberNo, string SearchOption)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.RequestPreauthByMembershipNoRequest_DN request;
        OS_DXC_WAP.CaesarWS.RequestPreauthByMembershipNoResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestPreauthByMembershipNoRequest_DN();
            if (SearchOption == "M")
            {

                request.membershipNo = MemberNo;
            }
            else
                request.type = ddlPreauth.SelectedValue;
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));

            request.providerCode = strProviderID;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.RequestPreauthByMbrshpNo(request);
            StringBuilder sbResponse = new StringBuilder(200);
            StringBuilder sb1 = new StringBuilder(200);
            if (response.status == "0")
            {
                sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Arial;border-collapse:collapse;font-size:11px; width:90%  ;'><thead bgcolor='#e6f5ff'><tr><th>Reference No</th><th>Episode Number</th><th>Episode Status</th><th>Service Description</th><th>Approval Date</th><th>Member Name</th><th>Patient File No</th></tr>	</thead><tbody> ");
                for (int i = 0; i < response.service_Desc.Length; i++)
                {
                    var queryString = "PID=" + response.preauthorisationID[i] + "&EN=" + response.episodeNo[i];
                    queryString = Cryption.Encrypt(queryString);
                    sbResponse.Append("<tr><td><a href=\"PreauthDetails.aspx?val=" + queryString + "\">" + response.preauthorisationID[i] + "</a></td><td>" + response.episodeNo[i] + "</td><td>" + response.episodeStatus[i] + "</td><td>" + response.service_Desc[i] + "</td><td>" + response.approvalDate[i] + "</td><td>" + response.memberName[i] + "</td><td>" + response.patientFileNumber[i] + "</td></tr>");
                    ////sbResponse.Append("<tr><td><a href=\"PreauthDetails.aspx?PID=" + response.preauthorisationID[i] + "&EN=" + response.episodeNo[i] + "\">" + response.preauthorisationID[i] + "</a></td><td>" + response.episodeNo[i] + "</td><td>" + response.episodeStatus[i] + "</td><td>" + response.service_Desc[i] + "</td><td>" + response.approvalDate[i] + "</td><td>" + response.memberName[i] + "</td><td>" + response.patientFileNumber[i] + "</td></tr>");
                }

                sbResponse.Append("</table>");
                CoverageListReport.Visible = true;
                CoverageListReport.Text = sbResponse.ToString();
                Message1.Visible = false;
            }
            else
            {
                Message1.Text = response.errorMessage;
                CoverageListReport.Visible = false;
                Message1.Text += ""; 
                Message1.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }
    }

    private void ReqVerificationByMbrNoNDate(string MemberNo, string SearchOption)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqMbrVerifyByMbrshpNoRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqMbrVerifyByMbrshpNoResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqMbrVerifyByMbrshpNoRequest_DN();
            if (SearchOption == "M")
            {
                request.membershipNo = MemberNo;
            }
            else
            {
                if (ddlVerify.SelectedValue == "T") request.searchDate = System.DateTime.Today; 
                if (ddlVerify.SelectedValue == "Y") request.searchDate = System.DateTime.Today.AddDays(-1); 
            }
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.provCode = strProviderID;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ReqMbrVerificationByMbrshpNo(request);
            StringBuilder sbResponse = new StringBuilder(200);
            StringBuilder sb1 = new StringBuilder(200);
            if (response.status == "0")
            {
                sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='font:Arial;border-collapse:collapse;font-size:11px; width:90%  ;'><thead bgcolor='#e6f5ff'><tr><th>Verification ID</th><th>Membership No</th><th>Member Name</th><th>Department</th><th>Verification Status</th><th>Transaction Date</th></tr>	</thead><tbody> ");
                for (int i = 0; i < response.department.Length; i++)
                {
                    sbResponse.Append("<tr ><td><a href='TOB.aspx?VID=" + response.verificationID[i] + "&MN=" + response.membershipNo[i] + "&PC=" 
                        + strProviderID + "&Type=Search&D=" + response.deptCode[i] + "&Status=" + response.verificationStatus[i] + 
                        "'  style='cursor:hand;'>" + response.verificationID[i] + "</a></td><td>" + response.membershipNo[i] + 
                        "</td><td>" + response.memberName[i] + "</td><td>" + response.department[i] + "</td>");
                    if (response.verificationStatus[i] == "0")
                        sbResponse.Append("<td>Incomplete Transaction</td>");
                    if (response.verificationStatus[i] == "A")
                        sbResponse.Append("<td>Approved</td>");
                    if (response.verificationStatus[i] == "R")
                        sbResponse.Append("<td>Rejected</td>");
                    sbResponse.Append("<td>" + response.transactionDate[i] + "</td></tr>");
                }
                sbResponse.Append("</table>");
                CoverageListReport.Visible = true;
                CoverageListReport.Text = sbResponse.ToString();
            }
            else
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb1.Append(response.errorMessage[i]).Append("<br/> ");
                Message1.Text = sb1.ToString();
                Message1.Text += "."; 
                CoverageListReport.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }

    private void ReqListByRefNo(string RefNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN request;
        OS_DXC_WAP.CaesarWS.RequestPreauthByIDResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN();
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.providerCode = strProviderID;
            request.preAuthorisationID = Int64.Parse(RefNo.Trim());
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.RequestPreauthByID(request);
            StringBuilder sb1 = new StringBuilder(200);

            if (response.status == "0")
            {
                Message1.Text = "";
                var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=" + request.episodeNo.ToString();
                queryString = Cryption.Encrypt(queryString);
                Response.Redirect("PreauthDetails.aspx?val=" + queryString);
                ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=" + request.episodeNo.ToString());
            }
            else
            {
                Message1.Visible = true;
                Message1.Text = response.errorMessage; 
                CoverageListReport.Visible = false; 
                Message1.Text += ".";
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }

    private void ReqListByVerificationNo(string VerificationNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDRequest_DN();
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.provCode = strProviderID;
            request.verificationID = Int64.Parse(VerificationNo.Trim());
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ReqMbrVerificationByID(request);
            StringBuilder sb1 = new StringBuilder(200);

            if (response.status == "0")
            {
                if (response.extIndicator == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "TOB.aspx?VID=" + response.verificationID.ToString() + "&MN=" + response.membershipNo.ToString() + "&PC=" + strProviderID + "&Type=Search&D=" + response.deptCode);
                    litExtendedDate.Text = ((DateTime)response.extDate).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }
                else
                {
                    Message1.Text = "";
                    Response.Redirect("TOB.aspx?VID=" + response.verificationID.ToString() + "&MN=" + response.membershipNo.ToString() + "&PC=" + strProviderID + "&Type=Search&D=" + response.deptCode);//'  style='cursor:hand;'><tr ><td>" + response.verificationID[i] + "</td><td>" + response.membershipNo[i] + "</td><td>" + response.memberName[i] + "</td><td>" + response.department[i] + "</td><td>" + response.verificationStatus[i] + "</td><td>" + response.transactionDate[i] + "</td></tr></a>");
                }
            }
            else
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb1.Append(response.errorMessage[i]).Append("<br/> ");

                Message1.Text = sb1.ToString();
                Message1.Text += ".";
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }
    }

    protected void btnReferenceNo_Click(object sender, EventArgs e)
    {
        ReqListByRefNo(txtReferenceNo.Text);
    }
    
    protected void btnMembershipNoPre_Click(object sender, EventArgs e)
    {
        ReqPreauthListByMbrNoNDate(txtMembershipNoPre.Text, "M");
    }

    protected void btnVerificationID_Click(object sender, EventArgs e)
    {
        ReqListByVerificationNo(txtVerificationID.Text);
    }

    protected void btnMembershipNoVer_Click(object sender, EventArgs e)
    {
        ReqVerificationByMbrNoNDate(txtMembershipNoVer.Text, "M");
    }
    
    protected void btnSearchByVer_Click(object sender, EventArgs e)
    {
        ReqVerificationByMbrNoNDate(ddlVerify.SelectedValue, "D");
    }
    
    protected void btnSearchByPre_Click(object sender, EventArgs e)
    {
        ReqPreauthListByMbrNoNDate(ddlPreauth.SelectedValue, "D");
    }
    
    protected void btnVerificationID_Click1(object sender, EventArgs e)
    {
        ReqListByVerificationNo(txtVerificationID.Text);
    }
}