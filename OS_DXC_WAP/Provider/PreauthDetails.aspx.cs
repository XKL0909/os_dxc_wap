﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Configuration;

public partial class Provider_PreauthDetails : System.Web.UI.Page
{
    string PreauthID;
    string ProviderCode;
    string EpisodeNo;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    public string _Username = "";
    public string _PAID = "";
    public string _PROID = "";
    public string _View = "display:none";
    private Hashtable hasQueryValue;
    private string RequestType, OptionType, Rtype, Department, TransactionID, ServiceError;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }
            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Session["Reset"] = true;
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);

            //Session Timeout Warning Dialog
            CheckSessionExpired();

            SessionManager.CheckSessionTimeout();

            hasQueryValue = new Hashtable();
            string queryStringValues = string.Empty;
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    var val = Request.QueryString["val"];
                    queryStringValues = Cryption.Decrypt(val);
                    hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                    ////Session["hashQueryPDetails"] = hasQueryValue;
                    PreauthID = hasQueryValue.ContainsKey("PID") ? Convert.ToString(hasQueryValue["PID"]) : string.Empty;
                    ProviderCode = hasQueryValue.ContainsKey("PC") ? Convert.ToString(hasQueryValue["PC"]) : string.Empty;
                    EpisodeNo = hasQueryValue.ContainsKey("EN") ? Convert.ToString(hasQueryValue["EN"]) : string.Empty;
                    Rtype = hasQueryValue.ContainsKey("RType") ? Convert.ToString(hasQueryValue["RType"]) : string.Empty;
                    RequestType = hasQueryValue.ContainsKey("ReqType") ? Convert.ToString(hasQueryValue["ReqType"]) : string.Empty;
                    Department = hasQueryValue.ContainsKey("DEPT") ? Convert.ToString(hasQueryValue["DEPT"]) : string.Empty;
                    TransactionID = hasQueryValue.ContainsKey("txnid") ? Convert.ToString(hasQueryValue["txnid"]) : string.Empty;
                    ServiceError = hasQueryValue.ContainsKey("sError") ? Convert.ToString(hasQueryValue["sError"]) : string.Empty;
                    OptionType = hasQueryValue.ContainsKey("OptionType") ? Convert.ToString(hasQueryValue["OptionType"]) : string.Empty;
                }
                catch (Exception ex)
                {
                    Response.Write("Invalid rquest!");
                    return;
                }

            }
            else
            {
                Response.Write("Invalid rquest!");
                return;
            }



            // ProviderCode = "20087";//

            ////if (Request.QueryString["ReqType"] == "M" || Request.QueryString["ReqType"] == "C")
            if (RequestType == "M" || RequestType == "C")
            {
                btnFollowUpRequest.Visible = false;
                btnCancelRequest.Visible = false;
                txtRenew.Visible = false;
            }
            else
            {

                ProviderCode = Session["ProviderID"].ToString();  // Request.QueryString["PC"];
            }

            //PreauthID = Request.QueryString["PID"];
            //string OptionType = Request.QueryString["OptionType"];
            //string RType = Request.QueryString["RType"];
            //EpisodeNo = Request.QueryString["EN"];
            //string Dept =  Request.QueryString["DEPT"];

            try
            {
                if (PreauthID == "0")
                {
                    string txtID = Request.QueryString["txnid"];
                    string sError = Request.QueryString["sError"];
                    sError = "<table style=\"background-color: lightyellow;border: solid;\" background=red><tr><td width=\"150px;\">Transaction ID :</td><td> " + txtID + " </td></tr><tr><td>Error :</td><td> " + sError + "</td></tr></Table>";
                    //Response.Write(txtID + "<br>" +sError);
                    Response.Write(sError);
                }
            }
            catch (Exception ex)
            {

            }

            _PROID = ProviderCode;
            _PAID = PreauthID;
            _Username = Convert.ToString(Session["ProviderUserName"]);

            if (OptionType == "Cancel")
            {
                ////EpisodeNo = Request.QueryString["EN"];
                CancelPreAuth(PreauthID, EpisodeNo);
                return;
            }


            if (!Page.IsPostBack)
            {

                DisplayPreAuthHist();
            }
            //if (RType == "NR")
            if (Rtype == "NR")
            {
                //if (Dept == "VAC")
                if (Department == "VAC")
                {
                    btnFollowUpRequest.Enabled = false;
                    txtRenew.Disabled = true;
                }

                lnkBackOption.Text = "Submit New Request";
                lnkBackOption.NavigateUrl = "~/Provider/Preauth15.aspx";
            }
            Response.Write(GetPostBackControl());
        }

        catch (Exception ex)
        {
            // ExceptionManager.LogException(ex);
        }
    }

    protected void OnSaveClick(object sender, EventArgs e)
    {
        string parameter = Request["__EVENTARGUMENT"];
    }

    private void DisplayPreAuthHist()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN request;
        OS_DXC_WAP.CaesarWS.RequestPreauthByIDResponse_DN response;

        DataTable tablePA = new DataTable();
        tablePA.Columns.Add("ItemID", typeof(string));
        tablePA.Columns.Add("ProviderID", typeof(string));
        tablePA.Columns.Add("PAID", typeof(string));
        tablePA.Columns.Add("ServicesCode", typeof(string));
        tablePA.Columns.Add("Services Description", typeof(string));
        tablePA.Columns.Add("Dispensed", typeof(bool));

        DataColumn[] keyColumns = new DataColumn[1];
        keyColumns[0] = tablePA.Columns["ItemID"];
        tablePA.PrimaryKey = keyColumns;

        string _Dispends = "";
        string username = "";
        string _date = "";
        DateTime _dt;
        bool _addcheck = true;
        string _scode = "";
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN();
            if (EpisodeNo != null && EpisodeNo != "")
                request.episodeNo = long.Parse(EpisodeNo);
            else
                request.episodeNo = 0;
            request.preAuthorisationID = long.Parse(PreauthID);
            request.providerCode = ProviderCode;
            request.transactionID = TransactionManager.TransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.RequestPreauthByID(request);// ws.RequestPreauthByID(request);

            if (response.status.Trim() == "0")
            {
                if (response.preauthorisationStatus != null && response.preauthorisationStatus.Trim() == "WIP")
                {
                    UpdatePanel2.Visible = false;
                    btnCancelRequest.Visible = false;
                    btnRenewRequest.Visible = false;
                    btnFollowUpRequest.Visible = false;
                    txtRenew.Visible = false;
                    Label1.Visible = false;
                    ////if (Request.QueryString["RType"] != null && Request.QueryString["RType"].ToString() == "NR")
                    if (Rtype == "NR")
                        Message1.Text = "Reference No: <b>" + response.preAuthorizationID.ToString() + "</b>  <a href='CheckNewReply.aspx'>Check Reply</a>";
                    else
                        Message1.Text = "This preauthorisation is currently being processed by Bupa. Please check later.";// +response.preAuthorizationID.ToString() + "</b>  <a href='CheckNewReply.aspx'>Check Reply</a>";
                }
                else
                {
                    lblClass.Text = response.memberClass;
                    lbAge.Text = response.age.ToString();
                    lblDateOfVisit.Text = String.Format("{0:d}", response.dateOfAdmission);

                    lblDept.Text = response.prov_Dept_Name;
                    lblExpiryDate.Text = String.Format("{0:d}", response.expiryDate); //response.expiryDate.ToString();
                    lblIDCard.Text = response.IDCardNo;
                    lblInsurance_Co.Text = response.insurance_Company;
                    lblInsuredName.Text = response.memberName;
                    lblPatientFileNo.Text = response.patientFileNo;
                    lblDateTime.Text = response.date_Time.ToString();
                    lblPolicyHolder.Text = response.policyHolder;
                    lblPolicyNo.Text = response.membershipNo;
                    //need to check///lblPolicyNo.Text = response.
                    lblProviderFaxNo.Text = response.providerFaxNo;
                    lblProviderName.Text = response.provider_Name;
                    lblDateTime.Text = response.date_Time.ToString();
                    lblSex.Text = response.gender;
                    lblAdditionalComments.Text = response.add_Comments;
                    ////// need to check       lblApprovalNo.Text = response.
                    lblApprovalValidity.Text = String.Format("{0:d}", response.app_Validity); //response.app_Validity.ToString();
                    lblInsuranceOfficer.Text = response.insurance_Officer;
                    lblApprovalNo.Text = PreauthID;

                    //Added by Yahya
                    lblMemberName.Text = response.memberName;

                    lblCardIssueNo.Text = response.web_CardIssueNo;
                    lbiIdIqamaNo.Text = response.web_IDCard_No;

                    lblMobileNo.Text = response.web_Mobile_No;
                    lblMembershipNo.Text = response.membershipNo;
                    lblPatientFileNo2.Text = response.patientFileNo;
                    lblContractNo.Text = response.web_Contract_No;
                    lblVerificationId.Text = response.web_Verification_Id;
                    lblProviderCode.Text = response.web_Provider_Code;
                    lblPhysicianName.Text = response.physicians_Name;
                    lblFaxNo.Text = response.providerFaxNo;
                    lblMainSymptoms.Text = response.web_Main_Symptoms;
                    lblDiagnosisCode.Text = response.web_Diagnosis_Code;
                    lblDiagnosisDesc.Text = response.web_Diagnosis_Desc;
                    lblTreatmentType.Text = response.web_Treatment_Type;
                    lblDateOfAdmission.Text = String.Format("{0:d}", response.dateOfAdmission);

                    lblDepartmentType.Text = response.web_Dept_Type;
                    lblLengthOfStay.Text = response.web_Length_Of_Stay;


                    lblExemptions.Text = response.web_Exempted_Ind;
                    lblReferral.Text = response.web_Referral_Ind;
                    lblChronic.Text = response.chronic_Ind;
                    lblRTA.Text = response.web_Rta_Ind;
                    lblInfertility.Text = response.web_Infert_Ind;
                    lblWorkRelated.Text = response.web_Wrk_Related_Ind;
                    lblGravida.Text = response.web_Gravida;
                    lblPara.Text = response.web_Para;
                    lblLive.Text = response.web_Live;
                    lblPossibleLineOfTreatment.Text = response.web_Line_Of_Treatment;
                    lblOtherConditions.Text = response.web_Other_Condition;
                    lblCheckUp.Text = response.web_Check_up_Ind;
                    lblCongenital.Text = response.web_Congen_Ind;
                    lblPsychiatric.Text = response.web_Psyc_Ind;
                    lblVaccination.Text = response.web_Vacc_Ind;
                    lblBloodPressure.Text = response.web_Blood_Pressure;
                    lblPulse.Text = response.web_Pulse;
                    lblAbortion.Text = response.web_Abortion;

                    if (response.web_IVF_Pregnancy == "N" && response.web_Normal_Pregnancy == "Y")
                        lblMaternityType.Text = "Normal Pregnancy";
                    else if (response.web_IVF_Pregnancy == "Y" && response.web_Normal_Pregnancy == "N")
                        lblMaternityType.Text = "Infertility/IVF/Induced/Undeclared Pregnancy";

                    lblDeath.Text = response.web_Death;
                    lblTemperature.Text = response.web_Temperature;
                    lblDurationOfIllness.Text = response.web_Illness;
                    lblLastMenstrualPeriodHijri.Text = String.Format("{0:d}", response.last_Menstrual_Period_Hijri);
                    lblLastMenstrualPeriodGregorian.Text = String.Format("{0:d}", response.last_Menstrual_Period_Gregorian);
                    lblExpectedDeliveryHijri.Text = String.Format("{0:d}", response.expected_Delivery_Hijri);
                    lblExpectedDeliveryGregorian.Text = String.Format("{0:d}", response.expected_Delivery_Gregorian);

                    //End Added by Yahya
                    decimal decAmount = 0;
                    int intQty = 0;
                    if (response.web_Serv_Code != null)
                    {
                        DataTable dt = new DataTable();
                        dt.Clear();
                        dt.Columns.Add("ServiceCode");
                        dt.Columns.Add("SupplyPeriod");
                        dt.Columns.Add("SupplyFrom");
                        dt.Columns.Add("SupplyTo");
                        dt.Columns.Add("ServiceDescription");
                        dt.Columns.Add("Qty");
                        dt.Columns.Add("Amount");

                        for (int i = 0; i < response.web_Serv_Code.Length; i++)
                        {
                            DataRow dr = dt.NewRow();
                            dr["ServiceCode"] = response.web_Serv_Code[i];
                            dr["SupplyPeriod"] = response.web_Supp_Period[i];
                            if (response.web_Supp_From[i] != null)
                                dr["SupplyFrom"] = String.Format("{0:d}", response.web_Supp_From[i].ToString()).Substring(0, 10);
                            if (response.web_Supp_To[i] != null)
                                dr["SupplyTo"] = String.Format("{0:d}", response.web_Supp_To[i].ToString()).Substring(0, 10);
                            dr["ServiceDescription"] = response.web_Serv_Desc[i];
                            dr["Qty"] = response.web_Item_Qty[i];
                            dr["Amount"] = response.web_Line_Amt[i];
                            dt.Rows.Add(dr);
                            decAmount = decAmount + Convert.ToDecimal(response.web_Line_Amt[i]);
                            intQty = intQty + int.Parse(response.web_Item_Qty[i]);
                        }
                        lblEstimatedAmount.Text = decAmount.ToString();
                        lblQuantity.Text = intQty.ToString(); //need to be added from COL
                        gvServices.DataSource = dt;
                        gvServices.DataBind();
                    }

                    if (response.chronic_Ind.Trim() != "N" || response.department == "DEN" || response.department == "OER")
                        btnFollowUpRequest.Visible = false;

                    if (response.department == "VAC")
                    {
                        btnFollowUpRequest.Visible = false;
                        btnCancelRequest.Visible = false;
                    }

                    lblComments.Text = response.comments;

                    lblPreauthorisationStatus.Text = response.preauthorisationStatus;
                    lblRoomType.Text = response.roomType;

                    StringBuilder sbResponse = new StringBuilder(2200);
                    if (response.chronic_Ind.Trim() == "Y" || response.supply_Period.Length > 0)
                        sbResponse.Append("<table   style='font-size:13px;width:100% ;' border=0><tr ><td width='5%'><b>Service Code<br>رمز الخدمة</td><td width='10%'><b>Supply Period<br>مدة الخدمة</td><td width='10%'><b>Supply From<br>من تاريخ</td><td width='10%'><b>Supply To<br>الى تاريخ</td><td width='40%' valign=top><b>Service Description<br>وصف الخدمة</td><td width='30%' valign=top><b>Notes<br>ملاحظات</b></td></tr>");
                    else
                        sbResponse.Append("<table  style='font-size:13px;width:100% ;' border=0 ><tr><td width='15px'><b>Service Code</td><td valign=top><b>Service Description</td><td valign=top><b>Notes</b></td></tr>");

                    for (int i = 0; i < response.service_Code.Length; i++)
                    {
                        sbResponse.Append("<tr>");
                        if (response.chronic_Ind.Trim() == "Y" || response.supply_Period.Length > 0)
                        {
                            sbResponse.Append("<td>");
                            if (response.service_Code[i] != null) sbResponse.Append(response.service_Code[i]);
                            sbResponse.Append("</td><td>");
                            if (response.supply_Period[i] != null) sbResponse.Append(response.supply_Period[i] + " Month/s");
                            sbResponse.Append("</td><td>");
                            if (response.supply_From[i] != null) sbResponse.Append(DateTime.ParseExact(String.Format("{0:d}", response.supply_From[i].Replace(" 00:00:00.0", "")), "yyyy-MM-dd", null).ToShortDateString());
                            sbResponse.Append("</td><td>");
                            if (response.supply_To[i] != null) sbResponse.Append(DateTime.ParseExact(String.Format("{0:d}", response.supply_To[i].Replace(" 00:00:00.0", "")), "yyyy-MM-dd", null).ToShortDateString());
                            sbResponse.Append("</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td>");
                            if (!response.notes[i].ToString().Contains("Rejected"))
                            {
                                _View = "display:block";
                                _Dispends = SelectRecord(i, ProviderCode, PreauthID, response.service_Code[i]);
                                username = SelectRecordUser(i, ProviderCode, PreauthID, response.service_Code[i]);
                                _date = SelectRecordDate(i, ProviderCode, PreauthID, response.service_Code[i]);
                                _date = String.Format("{0:d/M/yyyy HH:mm:ss}", _date);
                                if (_Dispends == "")
                                {
                                    _Dispends = "false";
                                    if (string.IsNullOrEmpty(Convert.ToString(response.service_Code[i])))
                                    {
                                        InsertRecord(i, ProviderCode, PreauthID, i.ToString(), response.service_Desc[i]);
                                    }
                                    else
                                    {
                                        InsertRecord(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i]);
                                    }
                                    if (string.IsNullOrEmpty(response.service_Code[i]))
                                    {
                                        tablePA.Rows.Add(i, ProviderCode, PreauthID, "", response.service_Desc[i], false);
                                        _addcheck = true;
                                    }
                                    else
                                    {
                                        _addcheck = true;
                                        tablePA.Rows.Add(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i], false);
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(Convert.ToString(response.service_Code[i])))
                                    {
                                        _scode = "";
                                    }
                                    else
                                    {
                                        _scode = response.service_Code[i];
                                    }
                                    tablePA.Rows.Add(i, ProviderCode, PreauthID, _scode, response.service_Desc[i], Convert.ToBoolean(_Dispends));
                                }
                                if (_addcheck)
                                {
                                    if (Convert.ToBoolean(_Dispends))
                                    {
                                        sbResponse.Append("<td><input id='" + response.service_Code[i] + "' type='checkbox' name='" + response.service_Code[i] + "' disabled='disabled' checked='checked' /></td><td>" + username + "</td><td>" + _date + "</td></tr>");
                                    }
                                    else
                                    {
                                        sbResponse.Append("<td><input id='" + response.service_Code[i] + "' type='checkbox' name='" + response.service_Code[i] + "' disabled='disabled' /></td><td>" + username + "</td><td>" + _date + "</td></tr>");
                                    }
                                }
                            }
                            else
                            {
                                sbResponse.Append("</tr>");
                            }
                        }

                        else
                        {
                            sbResponse.Append("<td>" + response.service_Code[i] + "</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td>");  // response.service_Code
                            if (!response.notes[i].ToString().Contains("Rejected"))
                            {
                                _View = "display:block";
                                _Dispends = SelectRecord(i, ProviderCode, PreauthID, response.service_Code[i]);
                                username = SelectRecordUser(i, ProviderCode, PreauthID, response.service_Code[i]);
                                _date = SelectRecordDate(i, ProviderCode, PreauthID, response.service_Code[i]);
                                _date = String.Format("{0:d/M/yyyy HH:mm:ss}", _date);
                                if (_Dispends == "")
                                {
                                    _Dispends = "false";
                                    if (string.IsNullOrEmpty(response.service_Code[i]))
                                    {
                                        InsertRecord(i, ProviderCode, PreauthID, i.ToString(), response.service_Desc[i]);
                                        _addcheck = false;
                                    }
                                    else
                                    {
                                        _addcheck = true;
                                        InsertRecord(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i]);
                                        tablePA.Rows.Add(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i], false);
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(response.service_Code[i]))
                                    {
                                        //tablePA.Rows.Add(ProviderCode, PreauthID, "", false);
                                    }
                                    else
                                    {
                                        tablePA.Rows.Add(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i], Convert.ToBoolean(_Dispends));
                                    }

                                }

                                if (_addcheck)
                                {
                                    if (Convert.ToBoolean(_Dispends))
                                    {
                                        sbResponse.Append("<td><input id='" + response.service_Code[i] + "' type='checkbox' name='" + response.service_Code[i] + "' disabled='disabled' checked='checked' /></td><td>" + username + "</td><td>" + _date + "</td></tr>");
                                    }
                                    else
                                    {
                                        sbResponse.Append("<td><input id='" + response.service_Code[i] + "' type='checkbox' name='" + response.service_Code[i] + "' disabled='disabled' /></td><td>" + username + "</td><td>" + _date + "</td></tr>");
                                    }
                                }

                            }
                            else
                            {
                                sbResponse.Append("</tr>");
                            }
                        }
                    }
                    ServiceListReport.Visible = true;
                    ServiceListReport.InnerHtml = sbResponse.ToString() + "</table>";
                    DataSet ds = new DataSet();
                    ds.Tables.Add(tablePA);
                    Session["tablePA"] = ds;
                }
            }
            else
            {
                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                txtRenew.Visible = false;
                Message1.Text = response.errorMessage;// "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
            //ExceptionManager.LogException(ex);
        }


    }


    private string GetPostBackControl()
    {
        string Outupt = "";

        //get the __EVENTTARGET of the Control that cased a PostBack(except Buttons and ImageButtons)
        string targetCtrl = Page.Request.Params.Get("__EVENTTARGET");
        if (targetCtrl != null && targetCtrl != string.Empty)
        {
            Outupt = targetCtrl + " button make Postback";
        }
        else
        {
            //if button is cased a postback
            foreach (string str in Request.Form)
            {
                Control Ctrl = Page.FindControl(str);
                if (Ctrl is Button)
                {
                    Outupt = Ctrl.ID;
                    break;
                }
            }
        }
        if (Outupt == "btnCancelRequest")
        { Outupt = ""; }

        return Outupt;
    }

    protected void Check(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;
        Response.Write(chk.Checked.ToString());
    }

    private void CancelPreAuth(string Preauth, string Episode)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.SendPreauthCancellationRequest_DN request;
        OS_DXC_WAP.CaesarWS.SendPreauthCancellationResponse_DN response;

        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.SendPreauthCancellationRequest_DN();

            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.preAuthorisationID = Int64.Parse(Preauth.Trim());
            request.episodeNo = Int64.Parse(Episode.Trim());

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            //  ExceptionManager.TrackWSPerformance("PreauthDetails", "CancelInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), ProviderCode, "", "");
            response = ws.CancelInfo(request);
            ///ExceptionManager.TrackWSPerformance("PreauthDetails", "CancelInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), ProviderCode, "", "");

            StringBuilder sbResponse = new StringBuilder(200);
            StringBuilder sb1 = new StringBuilder(200);

            if (response.status == "0")
            {
                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                txtRenew.Visible = false;

                Message1.Text = "Your request for Cancellation will be processed.";// response.errorMessage.Trim();

            }
            else
            {

                reportborder.Visible = false;
                string txtID = Request.QueryString["txnid"];
                string sError = "<table style=\"background-color: lightyellow;border: solid;text-align:center;\" width='100%' background=red><tr><td> " + response.errorMessage + "</td></tr></Table>";
                //Response.Write(txtID + "<br>" +sError);
                Response.Write(sError);
                //Message1.Text = response.errorMessage;// sb1.ToString(); // "Invalid information reported. Please type in the exact information.";
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
            // ExceptionManager.LogException(ex);
        }

    }

    protected void lnkCancelRequest_Click(object sender, EventArgs e)
    {

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.RequestCancellationStatusRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestCancellationStatusRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestCancellationStatusResponse_DN response;

        request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        request.preAuthorisationID = Int64.Parse(PreauthID.Trim());
        request.providerCode = ProviderCode;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;





        try
        {

            StringBuilder sb = new StringBuilder(200);

            // ExceptionManager.TrackWSPerformance("PreauthDetails", "RequestCancelStatus", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), ProviderCode, "", "");
            response = ws.RequestCancelStatus(request);
            //ExceptionManager.TrackWSPerformance("PreauthDetails", "RequestCancelStatus", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), ProviderCode, "", "");


            if (response.status.Trim() != "0")
            {
                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                txtRenew.Visible = false;
                Message1.Text = response.errorMessage.Trim();
            }
            else
            {

                StringBuilder sbResponse = new StringBuilder(2200);

                sbResponse.Append("<table border=1  width=100%><tr><td width=120px;><b>Preauth ID</td><td><b>Episode Number</td><td><b>Episode Status</b></td><td><b>Service Description</b></td><td><b>Service Status</b></td><td><b>Approval Date</b></td><td><b>Member Name</b></td><td><b>Patient File No</b></td><td><b>Action</b></td></tr>");

                for (int i = 0; i < response.episodeNo.Length; i++)
                {

                    ////sbResponse.Append("<tr><td>" + response.preauthorisationID[i].ToString() + "</td><td>" + response.episodeNo[i].ToString() + "</td><td>" + response.episodeStatus[i] + "</td><td>" + response.serviceDescription[i] + "</td><td>" + response.lineItemStatus[i] + "</td><td>" + String.Format("{0:d}", response.approvalDate[i]) + "</td><td>" + response.memberName[i] + "</td><td>" + response.patientFileNumber[i] + "</td><td>" + "<a href='PreauthDetails.aspx?PID=" + response.preauthorisationID[i].ToString() + "&EN=" + response.episodeNo[i].ToString() + "&OptionType=Cancel'  style='cursor:hand;background:#BFEBFB;'>Cancel</a>" + "</td>");  // response.service_Code
                    var queryString = "PID=" + response.preauthorisationID[i].ToString() + "&EN=" + response.episodeNo[i].ToString() + "&OptionType=Cancel";
                    queryString = Cryption.Encrypt(queryString);
                    sbResponse.Append("<tr><td>" + response.preauthorisationID[i].ToString() + "</td><td>" + response.episodeNo[i].ToString() + "</td><td>" + response.episodeStatus[i] + "</td><td>" + response.serviceDescription[i] + "</td><td>" + response.lineItemStatus[i] + "</td><td>" + String.Format("{0:d}", response.approvalDate[i]) + "</td><td>" + response.memberName[i] + "</td><td>" + response.patientFileNumber[i] + "</td><td>" + "<a href='PreauthDetails.aspx?val=" + queryString + "'  style='cursor:hand;background:#BFEBFB;'>Cancel</a>" + "</td>");

                }
                sbResponse.Append("<tr><td colspan=9><center>Please confirm the above details and Click Cancel</center></td></tr>");
                sbResponse.Append("</table>");



                CancelList.Visible = true;
                CancelList.InnerHtml = sbResponse.ToString();



                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                txtRenew.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
            //ExceptionManager.LogException(ex);
        }


    }

    protected void btnRenewRequest_Click(object sender, EventArgs e)
    {

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.SendRenewInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.SendRenewInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.SendRenewInfoResponse_DN response;


        request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
        request.transactionID = TransactionManager.TransactionID();// 
        request.preAuthorisationID = Int64.Parse(PreauthID.Trim());
        request.treatmentDate = txtRenewalDate.CalendarDate;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.SendRenewInfo(request);
            if (response.status != "0")
            {
                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                btnRenewRequest.Visible = false;
                txtRenew.Visible = false;
                Message1.Text = response.errorMessage;
            }
            else
            {
                Message1.Text = "Request Successfully send for Renewal";
                lblApprovalNo.Font.Bold = true;
                lbl_approvalNo.Text = "New Approval No:";
                lblDateOfVisit.Text = String.Format("{0:d}", response.dateOfAdmission);
                lblApprovalValidity.Text = String.Format("{0:d}", response.app_Validity);
                lblDateTime.Text = response.date_Time.ToString();
                lblPreauthorisationStatus.Text = response.preauthorisationStatus.Trim();

                StringBuilder sbResponse = new StringBuilder(2200);
                sbResponse.Append("<table width=900px><tr><td width=20%><b>Service Code</td><td><b>Service Description</td><td width='12%'><b>Notes</b></td></tr>");


                for (int i = 0; i < response.service_Code.Length; i++)
                {
                    sbResponse.Append("<tr>");
                    sbResponse.Append("<td>" + response.service_Code[i] + "</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td>");  // response.service_Code


                }
                ServiceListReport.Visible = true;
                ServiceListReport.InnerHtml = sbResponse.ToString();

                lblApprovalNo.Text = response.preAuthorizationID.ToString();// request.preAuthorisationID.ToString();

            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message + ".<br>Please try again later";
            //ExceptionManager.LogException(ex);
        }

    }

    protected void btnFollowUpRequest_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("Preauth15.aspx?val=" + Cryption.Encrypt(PreauthID), false);// response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1");
        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }

    }

    protected void txtRenew_ServerClick(object sender, EventArgs e)
    {

    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        foreach (Control control in Controls)
        {
            CheckBox chkBox = control as CheckBox;
            if (chkBox != null)
            {
                chkBox.Checked = true;
            }
        }
    }




    public void InsertRecord(int itemID, string _ProviderID, string _PAID, string _serviceCode, string Desc)
    {

        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            SqlParameter[] arParms = new SqlParameter[5];



            arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
            arParms[0].Value = itemID;

            arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
            arParms[1].Value = _ProviderID;

            arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
            arParms[2].Value = _PAID;

            arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
            arParms[3].Value = _serviceCode;

            arParms[4] = new SqlParameter("@Description", SqlDbType.NText);
            arParms[4].Value = Desc;


            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Insert_Dispensed", arParms);
        }
        catch { }
        finally
        {
            if (connection.State == ConnectionState.Open)
                connection.Close();
        }

    }

    public string SelectRecord(int itemID, string _ProviderID, string _PAID, string _serviceCode)
    {
        string _Value = "";
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            SqlParameter[] arParms = new SqlParameter[5];

            arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
            arParms[0].Value = itemID;


            arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
            arParms[1].Value = _ProviderID;

            arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
            arParms[2].Value = _PAID;

            arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
            arParms[3].Value = _serviceCode;

            arParms[4] = new SqlParameter("@OutPut", SqlDbType.Bit);
            arParms[4].Direction = ParameterDirection.Output;
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Select_Dispensed", arParms);
            if (string.IsNullOrEmpty(arParms[4].Value.ToString()))
            {
                _Value = "";
            }
            else
            {
                _Value = arParms[4].Value.ToString();
            }
        }
        catch { }
        finally
        {
            if (connection.State == ConnectionState.Open)
                connection.Close();
        }

        return _Value;
    }

    public string SelectRecordUser(int itemID, string _ProviderID, string _PAID, string _serviceCode)
    {
        string _Value = "";
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            SqlParameter[] arParms = new SqlParameter[5];

            arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
            arParms[0].Value = itemID;


            arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
            arParms[1].Value = _ProviderID;

            arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
            arParms[2].Value = _PAID;

            arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
            arParms[3].Value = _serviceCode;

            arParms[4] = new SqlParameter("@OutPut", SqlDbType.NVarChar, 50);
            arParms[4].Direction = ParameterDirection.Output;
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Select_Dispensed_user", arParms);
            if (string.IsNullOrEmpty(arParms[4].Value.ToString()))
            {
                _Value = "";
            }
            else
            {
                _Value = arParms[4].Value.ToString();
            }
        }
        catch { }
        finally
        {
            if (connection.State == ConnectionState.Open)
                connection.Close();
        }

        return _Value;
    }



    public string SelectRecordDate(int itemID, string _ProviderID, string _PAID, string _serviceCode)
    {
        string _Value = "";
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            SqlParameter[] arParms = new SqlParameter[5];

            arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
            arParms[0].Value = itemID;


            arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
            arParms[1].Value = _ProviderID;

            arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
            arParms[2].Value = _PAID;

            arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
            arParms[3].Value = _serviceCode;

            arParms[4] = new SqlParameter("@OutPut", SqlDbType.NVarChar, 20);
            arParms[4].Direction = ParameterDirection.Output;
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Select_Dispensed_date", arParms);
            if (string.IsNullOrEmpty(arParms[4].Value.ToString()))
            {
                _Value = "";
            }
            else
            {
                _Value = arParms[4].Value.ToString();
            }
        }
        catch { }
        finally
        {
            if (connection.State == ConnectionState.Open)
                connection.Close();
        }

        return _Value;
    }



    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        // ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

}
