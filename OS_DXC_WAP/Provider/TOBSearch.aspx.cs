
using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;

public partial class TOBSearch : System.Web.UI.Page
{
    string  strProviderID ;   
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    private string _url = "";

    void Page_Load(object sender, EventArgs e)
    {
        rdnSearchBy.Items[0].Attributes.CssStyle.Add("margin-right", "5px");
        rdnSearchBy.Items[1].Attributes.CssStyle.Add("margin-left", "15px");

        rdnSearchBy.Items[1].Attributes.CssStyle.Add("margin-right", "5px");
        rdnSearchBy.Items[2].Attributes.CssStyle.Add("margin-left", "15px");

	    SessionManager.CheckSessionTimeout();
        if (string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
        {
            Response.Redirect("../default.aspx");
        }
        else
        {
            strProviderID = Session["ProviderID"].ToString();
        }
        if (!IsPostBack)
        {
           
            ReqConDltList(strProviderID);
        }
    }

    private void RequestPolicySchemeList(string PolicyNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqSchemeListByPolicyRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqSchemeListByPolicyResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqSchemeListByPolicyRequest_DN();

            request.transactionID = TransactionManager.TransactionID();
            request.policyNo = PolicyNo;
            request.provCode = strProviderID;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            //request.policyYrMth = "201406";
            if (!string.IsNullOrEmpty(txtSearchDate.Text.Trim()))
            {
                DateTime dtTemp = DateTime.ParseExact(txtSearchDate.Text, "dd/MM/yyyy", null);
                request.policyYrMth = dtTemp.ToString("yyyy") + dtTemp.ToString("MM");
            }
            else
            {
                DateTime dtTemp = DateTime.ParseExact(System.DateTime.Today.ToShortDateString(), "dd/MM/yyyy", null);
                request.policyYrMth = dtTemp.ToString("yyyy") + dtTemp.ToString("MM");
            }

            response = ws.ReqSchemeListByPolicy(request);

            StringBuilder sb1 = new StringBuilder(2000);
            if (response.detail != null)
            {
                cbNetworkList1.Visible = true;
                cbNetworkList1.DataSource = response.detail;
                cbNetworkList1.TextField = "schemeName";
                cbNetworkList1.ValueField = "classID";
                cbNetworkList1.DataBind();

                cbNetworkList1.Items.Insert(0,  new  DevExpress.Web.ASPxEditors.ListEditItem ("-- ALL --", "ALL"));
                cbNetworkList1.SelectedIndex = 0;
                Message1.Text = "";
            }
            else
            {
                cbNetworkList1.DataBind();
                cbNetworkList1.Items.Clear();
                gvResult.Visible = false;
				////Disabled by Sakthi on 11-Oct-2016 as per business request.
                ////Message1.Text = "The Selected Policy does not have data. Please make sure the policy number and effective date are correct";
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }
    }

    private void ReqSchemeList(string MemberNo, string SearchOption)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.EnqPlanListByProvRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqPlanListByProvResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqPlanListByProvRequest_DN();
            if (rdnSearchBy.SelectedValue == "2")
            {
                request.mbrshp_No = MemberNo;
            }
            else if (rdnSearchBy.SelectedValue == "1")
            {
                //// New CCHI CR Need to passs Policy Number instead of Contract Number
                ////Start
                ////request.cont_No = txtPolicy.Text;
                if (txtPolicy.Text.Trim().Length > 8)
                    request.policyNo = txtPolicy.Text.Trim();
                else
                    request.cont_No = txtPolicy.Text.Trim();
                ////End
            }
            else if (rdnSearchBy.SelectedValue == "3")
            {
                if (!string.IsNullOrEmpty(txtSearchDate.Text.Trim()))
                {
                    request.eff_Date = DateTime.ParseExact(txtSearchDate.Text, "dd/MM/yyyy", null);
                }
                else
                {
                    request.eff_Date = DateTime.ParseExact(System.DateTime.Today.ToShortDateString(), "dd/MM/yyyy", null);
                }
            }

            request.transactionID = TransactionManager.TransactionID();
            request.prov_Code = strProviderID;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqPlanListByProv(request);
            StringBuilder sb1 = new StringBuilder(2000);
            if (response.detail != null)
            {
                gvResult.Visible = true;
                DisplaySchemeList(response);
                Message1.Text = "";
            }
            else
            {
                gvResult.DataSource = null;
                gvResult.DataBind();
                gvResult.Visible = false;
                Message1.Text = response.errorMessage;//sb1.ToString(); // "Invalid information reported. Please type in the exact information.";
                Message1.Text += "."; // Branch changed successfully. Your reference number is " + response.ReferenceNo.ToString() + ".";
            }
        }
       catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }
    }

    private void ReqConDltList(string ProviderID)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.RequestPlanContractInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestPlanContractInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestPlanContractInfoResponse_DN response;

        request.TransactionID = TransactionManager.TransactionID();//  request.TransactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        request.ProviderCode = ProviderID;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

       try
        {
            response = null;
            response = ws.ReqConDtl(request);

            StringBuilder sb1 = new StringBuilder(200);

            if (response.Status == "0")
            {
                DisplayContractListInfo(response);
            }
            else
            {
                sb1.Append(response.ErrorMessage).Append("<br/> ");
                Message1.Text = sb1.ToString(); // "Invalid information reported. Please type in the exact information.";
                Message1.Text += "."; // Branch changed successfully. Your reference number is " + response.ReferenceNo.ToString() + ".";
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }
    }

    private void DisplayContractListInfo(OS_DXC_WAP.CaesarWS.RequestPlanContractInfoResponse_DN response)
    {
        StringBuilder sbResponse = new StringBuilder(2200);
        DataTable ResultList = new DataTable();
        ResultList.Columns.Add("Policy Number");
        ResultList.Columns.Add("Customer Name");
        ResultList.Columns.Add("Details");

        if (response.Status == "0")
        {
            
            int totalCount = response.plan_ID.Length;
            int i;
            for (i = 0; i < totalCount; i++)
            {
                ////sbResponse.Append("<a style='cursor:hand;'  href='TOBSchemeDetails.aspx?PID=" + response.plan_ID[i] + "&PC=" + response.contract_No[i] + "&Type=Plan'><tr ><td>" + response.customer_Name[i] + "</td><td>" + response.plan_Desc[i] + "</td><td>" + response.plan_Eff_Date[i].ToShortDateString() + "</td><td>" + response.contract_No[i] + "</td></tr></a>");

                var queryString = "PID=" + response.plan_ID[i] + "&PC=" + response.contract_No[i] + "&Type=Plan";
                queryString = Cryption.Encrypt(queryString);
                sbResponse.Append("<a style='cursor:hand;'  href='TOBSchemeDetails.aspx?val=" + queryString + "'><tr ><td>" + response.customer_Name[i] + "</td><td>" + response.plan_Desc[i] + "</td><td>" + response.plan_Eff_Date[i].ToShortDateString() + "</td><td>" + response.contract_No[i] + "</td></tr></a>");
                DataRow row = ResultList.NewRow();
                row["Policy Number"] = response.contract_No[i];
                row["Customer Name"] = response.customer_Name[i];

                if (i > 0)
                {
                    if (response.contract_No[i] == response.contract_No[i - 1])
                    {
                        row["Details"] = response.plan_ID[i - 1] + "," + response.contractYearAndMonth[i];
                    }
                    else
                    {
                        row["Details"] = response.plan_ID[i] + "," + response.contractYearAndMonth[i];
                    }
                }
                else
                {
                    row["Details"] = response.plan_ID[i] + "," + response.contractYearAndMonth[i];
                }
                
                ResultList.Rows.Add(row);
            }

            DataSet dsProviders = new DataSet();
            dsProviders.Tables.Add(ResultList);
            DataTable distinctValues = dsProviders.Tables[0].DefaultView.ToTable(true, new string[] { "Policy Number", "Customer Name", "Details" });

            DataSet dsProvidersDistinct = new DataSet();

            dsProvidersDistinct.Tables.Add(distinctValues);
            gvResult.DataSource = dsProvidersDistinct.Tables[0].DefaultView;
            gvResult.DataBind();
            gvResult.Visible = true;
        }
    }

    private void DisplaySchemeList(OS_DXC_WAP.CaesarWS.EnqPlanListByProvResponse_DN response)
    {
        DataTable ResultList = new DataTable();
        ResultList.Columns.Add("Policy Number");
        ResultList.Columns.Add("Customer Name");
        ResultList.Columns.Add("Details");
        int i = 0;
        string strCon = "";
        string planID = "";
        if (response.status == "0")
        {
            foreach (OS_DXC_WAP.CaesarWS.EnqPlanListDetail_DN dtl in response.detail)
            {
			 if (!string.IsNullOrWhiteSpace(dtl.ContNo.Trim()))
             {
					DataRow row = ResultList.NewRow();
					if (!string.IsNullOrEmpty(strCon))
					{
						if (dtl.ContNo != strCon)
						{
							strCon = dtl.ContNo;
							planID = dtl.PlanID;
							row["Details"] = dtl.PlanID + "," + dtl.ContYYMM;
						}
						else
						{
							row["Details"] = planID + "," + dtl.ContYYMM;
						}
					}
					else
					{
						strCon = dtl.ContNo;
						planID = dtl.PlanID;
						row["Details"] = dtl.PlanID + "," + dtl.ContYYMM;
					}

					row["Policy Number"] = dtl.ContNo;
					row["Customer Name"] = dtl.CustName;
					i = i + 1;
					ResultList.Rows.Add(row);
				}
            }
           
            DataSet dsProviders = new DataSet();
            dsProviders.Tables.Add(ResultList);
            DataTable distinctValues = dsProviders.Tables[0].DefaultView.ToTable(true, new string[] { "Policy Number", "Customer Name", "Details" });

            DataSet dsProvidersDistinct = new DataSet();

            dsProvidersDistinct.Tables.Add(distinctValues);
            gvResult.DataSource = dsProvidersDistinct.Tables[0].DefaultView;
            gvResult.DataBind();
            gvResult.Visible = true;
        }
    }

    private string CheckMemberExist(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ChkMbrExistRequest_DN request;
        OS_DXC_WAP.CaesarWS.ChkMbrExistResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ChkMbrExistRequest_DN();
            request.membershipNo = MemberNo;
            request.iqamaNo = "";// IqamaNo;
            request.totalNoOfFamily = ""; // DependentNo;
            request.transactionID = TransactionManager.TransactionID();//   request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ChkMbrExist(request);
            
            if (response.memberName != null)
            {
                return "Exist";
            }
            else
            {
                return response.errorMessage[0];
            }
        }
        catch (Exception ex)
        {
            return "Error"; 
        }
    }

    protected void btnSearchByDate_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtPolicy.Text.Trim()))
        {
            ReqSchemeList(txtPolicy.Text, "policy");

            if (cbNetworkList1.SelectedIndex != -1 && cbNetworkList1.SelectedItem.Value.ToString() !="ALL")
            {
                Session["Class_ID"] = cbNetworkList1.SelectedItem.Value.ToString();
            }
        }
        else if (!string.IsNullOrEmpty(txtMemberSearch.Text.Trim()))
        {
            ReqSchemeList(txtMemberSearch.Text, "MembershipNo");
        }
        else
        {
            ReqSchemeList(txtMemberSearch.Text, "Date");   
        }
    }

    protected void gvResult_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        string[] strArray;
        if (e.DataColumn.FieldName == "Policy Number")
        {
            //_url = "Statement.aspx?CustomerID=" + e.CellValue.ToString();
            _url = "CustomerID=" + e.CellValue.ToString();
            e.DataColumn.Visible = false;
        }

        if (e.DataColumn.FieldName == "Details")
        {
            strArray = e.CellValue.ToString().Split(',');
            try
            {
                ///_url = _url + "&CYM=" + strArray[1] + "&SchemeID=" + strArray[0];
                _url = _url + "&CYM=" + strArray[1] + "&SchemeID=" + strArray[0];
            }
            catch { }

            var queryString = Cryption.Encrypt(_url);
            e.Cell.HorizontalAlign = HorizontalAlign.Center;
            HyperLink hl = new HyperLink();
            hl.NavigateUrl = "Statement.aspx?val="+ queryString;
            hl.Text = "Details";
            hl.Target = "_blank";
            e.Cell.Text = "Details";
            e.Cell.Controls.Add(hl);
        }
    }
   
    protected void txtSearchDate_DateChanged(object sender, EventArgs e)
    {
        //txtPolicy.Text = "";
        //txtMemberSearch.Text = "";
    }

    protected void btnLoadPolicyDetails_Click(object sender, EventArgs e)
    {
        cbNetworkList1.Items.Clear();

        RequestPolicySchemeList(txtPolicy.Text);

    }
}
