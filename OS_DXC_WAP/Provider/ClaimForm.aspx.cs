using Netways.QRCode.Common;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web.Configuration;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Collections;

public partial class ClaimForm : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string Dept;
    string VerificationID;
    string VerificationStatus;
    string RejectionReason;
    string ProviderCode;
    string PlanID;
    string ContractNo;
    string TransactionDate;
    string strProviderName;
    string MembershipNo;
    bool blnSwipeEnabled;

    private Hashtable hasQueryValue;


    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {

            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();           
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
        }
        catch
        {

        }

        VerificationStatus = "A";
        RejectionReason = "No error";
        ProviderCode = (string)Session["ProviderID"];
        strProviderName = (string)Session["ProviderName"];
        lblProviderName.Text = strProviderName;



        hasQueryValue = new Hashtable();
        string queryStringValues = string.Empty;

        if (Request.QueryString.Count > 0)
        {
            try
            {
                var val = Request.QueryString["val"];
                queryStringValues = Cryption.Decrypt(val);
                hasQueryValue = Cryption.GetQueryValue(queryStringValues);

                Dept = hasQueryValue.ContainsKey("Dept") ? Convert.ToString(hasQueryValue["Dept"]) : string.Empty;
                VerificationID = hasQueryValue.ContainsKey("VID") ? Convert.ToString(hasQueryValue["VID"]) : string.Empty;   
                MembershipNo = hasQueryValue.ContainsKey("MN") ? Convert.ToString(hasQueryValue["MN"]) : string.Empty;
            }
            catch (Exception ex)
            {
                Response.Write("Invalid rquest!");
                return;
            }

        }
        else
        {
            Response.Write("Invalid rquest!");
            return;
        }

        //QR Code
        if (!string.IsNullOrEmpty(Convert.ToString(Session["SwipCardEnabled"])))
        {
            blnSwipeEnabled = Convert.ToBoolean(Session["SwipCardEnabled"]);
        }
        else
        {
            blnSwipeEnabled = false;
        }

        if (blnSwipeEnabled == true)
        {
            imgQRCODE.Visible = true;
            imgQRCODE.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String((byte[])GetQrCode(VerificationID));
        }
        else
        {
            imgQRCODE.Visible = false;
        }

        //End QR Code


        ////if (Request.QueryString["Dept"] == "OER")
        if (Dept == "OER")
        {
            PanelUCAF.Visible = false;
            PanelOCAF.Visible = true;
            ////lblPageTitle.Text = "OCAF";
            lblPageTitle.Text = lblPageTitle.Text.Replace("UCAF", "OCAF");
            DisplayTOBReport();

        }
        ////else if (Request.QueryString["Dept"] == "DEN")
        else if (Dept == "DEN")
        {
            PanelUCAF.Visible = false;
            PanelDCAF.Visible = true;
            ////lblPageTitle.Text = "DCAF";
            lblPageTitle.Text = lblPageTitle.Text.Replace("UCAF", "DCAF").Replace("1.0","2.0");
            lblDepartment.Text = "Dental Clinic";
            DisplayTOBReport();
        }
        else
        {
            DisplayTOBReport();
        }
        lblInsuranceCo.Text = "Bupa Arabia";
        lblStatus.Text = "Approved";
    }

    private void DisplayTOBReport()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDResponse_DN response;
        response = null;
        request = new OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDRequest_DN();
        request.verificationID = long.Parse(VerificationID);
        request.provCode = ProviderCode;
        request.transactionID = TransactionManager.TransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        response = ws.ReqMbrVerificationByID(request);
        if (response.status == "0")
        {
            PlanID = response.planID.Trim();
            ContractNo = response.contractNo;
            TransactionDate = response.transactionDate;
            VerificationStatus = response.verificationStatus;
            lblAge.Text = response.age.ToString();
            Dept = response.deptCode;
            lblDepartment.Text = response.department;
            lblGender.Text = response.gender;
            lblDOB.Text = response.memberDOB;
            lblMemberName.Text = response.memberName;
            lblBupaCardNo.Text = response.membershipNo;
            lblVerificationID.Text = response.verificationID.ToString();
            lblProviderName.Text = strProviderName;
            lblDateOfVisit.Text = DateTime.Now.ToString();
            if (VerificationStatus == "A")
            {
                if (Dept == "OGP" || Dept == "DEN" || Dept == "OER")
                    DisplayTOBDOMReport("Y");
                else
                    DisplayTOBStdReport("Y");
            }
        }
    }

    private void DisplayTOBStdReport(string SearchMode)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqTOBStandardRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqTOBStandardResponse_DN response;
        response = null;
        request = new OS_DXC_WAP.CaesarWS.ReqTOBStandardRequest_DN();
        request.transactionID = TransactionManager.TransactionID();
        request.planID = PlanID;
        request.contNo = ContractNo;
        request.mbrshipNo = MembershipNo;
        request.deptNameCode = Dept;
        request.providerCode = ProviderCode;
        request.verificationID = Int64.Parse(VerificationID.Trim());
        request.verificationStatus = VerificationStatus;

        ////Commented by sakthi on 29-Mar-2018 due to BasePage inheritance, later need to investigate and pass the transaction date
        ///Start 
        ////request.txnDate = DateTime.Parse(TransactionDate);
        request.txnDate = null;
        ////End 

        request.searchMode = SearchMode;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        response = ws.ReqTOBStandard(request);

        ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBStandardRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBStandardRequest_DN>();
        ////XmlReq.Request(request, "ReqTOBStandardRequest_DN");

        ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBStandardResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBStandardResponse_DN>();
        ////XmlResp.Response(response, "ReqTOBStandardResponse_DN");


        if (response.status == "0")
        {
            lblEffectiveTo.Text = response.effectiveEndDate;
            //lblPolicyNo.Text = response.contractNo_CCHI;
            //based on Franco request
            ////CR Contract No, customer name and limit changes
            ////Start
            ////lblPolicyNo.Text = ContractNo; 
            ////lblPolicyHolder.Text = response.customerName; 
            lblPolicyNo.Text = response.policyNo;           
            lblPolicyHolder.Text = response.policyHolder;
            ////End  
            lblScheme.Text = response.schemeName;
            lblReferralLetterD.Text = response.dependentMandatoryReferralIndicator;
            lblReferralLetterM.Text = response.mainMemberMandatoryReferralIndicator;
            lblVerificationID.Text = VerificationID;
            lblDateOfVisit.Text = DateTime.Now.ToString();
            lblBupaCardNo.Text = MembershipNo;
            lblHospitalAccomodation.Text = response.bedType;
            lblInPatientDeductibleDA.Text = response.dependentDeductibleAmountForInPatient;
            lblOutPatientDeductibleDA.Text = response.dependentDeductibleAmountForOutPatient;
            lblInPatientDeductibleDP.Text = response.dependentDeductiblePercentageForInPatient + " " + response.dependentDeductibleAmountForInPatient;
            
            lblInPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForInPatient;
            
            lblOutPatientDeductibleMA.Text = response.mainMemberDeductibleAmountForOutPatient;
            lblInPatientDeductibleMA.Text = response.mainMemberDeductibleAmountForInPatient;
            lblEffectiveFrom.Text = response.effectiveDate.ToString();
            lblOverallAnnualLimit.Text = response.overallLimited;
            lblPreauthorisationLimit.Text = response.preauthLimited;
            lblScheme.Text = response.schemeName;
            lblTobSpecialInstructionsMA.Text = response.specialInstruction;
            lblTreatmentType.Text = response.treatmentType;

            //// CR414 TOB Changes
            ////Start
            ////lblOutPatientDeductibleDP.Text = response.dependentDeductiblePercentageForOutPatient;
            ////lblOutPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForOutPatient;
            if (response.TOBTemplate == "Y")
            {
                lblOutPatientDeductibleMP.Text = "*H - " + response.memberContributionForOutPatientTreatmentHospital + "</br>*P - " + response.memberContributionForOutPatientTreatmentPolyclinic + "</br>*M - " + response.memberContributionForOutPatientTreatmentMPN;
                lblOutPatientDeductibleDP.Text = "*H - " + response.memberContributionForOutPatientTreatmentHospital + "</br>*P - " + response.memberContributionForOutPatientTreatmentPolyclinic + "</br>*M - " + response.memberContributionForOutPatientTreatmentMPN;
                trNewTOB.Visible = true;
            }
            else
            {
                lblOutPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForOutPatient;
                lblOutPatientDeductibleDP.Text = response.dependentDeductiblePercentageForOutPatient;
                trNewTOB.Visible = false;
            }
            ////End

            StringBuilder sbResponse = new StringBuilder();
            if (response.benefitDesc.Length == 1)
            {
                sbResponse.Append("<table cellpadding='0' cellspacing='0'    style=' font-family:Arial; font-size:10px; mso-table-layout-alt: fixed; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 480; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid windowtext; mso-border-insidev: .5pt solid windowtext; width: 100%;' >");
                sbResponse.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                sbResponse.Append("<th colspan='4' class='style13'  style='border-top: 1.0pt solid windowtext;' ><B>TABLE OF BENEFITS</B></th></tr>");
                sbResponse.Append("<tr  style='background-color: #06AFF2;'>");
                sbResponse.Append("<th  style='width:20%;' class='style13'><b>Benefit Type</th><th style='width:55%;' class='style13'><b>Treatment Covered</th><th  style='width:15%;' class='style13'><b>Sub Limit</b></th><th style='width:10%;' class='style13'><b>Deductible</th>");
                sbResponse.Append("</tr>");
                for (int i = 0; i < response.benefitDesc.Length; i++)
                {
                    sbResponse.Append("<tr>");
					////sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'><b>" + response.benefitDesc[i].Trim() + "</b></td><td   class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td   class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitTypeLimit[i].Trim() + "</td>");
                    response.benefitTypeLimit[i] = !string.IsNullOrWhiteSpace(response.benefitTypeLimit[i]) ? response.benefitTypeLimit[i].Contains("99999999.99") ? "MAX Annual Limit" : response.benefitTypeLimit[i] : response.benefitTypeLimit[i];
                    sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'><b>" + response.benefitDesc[i].Trim() + "</b></td><td   class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td   class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitTypeLimit[i].Trim() + "</td>");
                    if (response.benefitDesc[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                        sbResponse.Append("<td  class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100</td></tr>");
                    else
                        sbResponse.Append("<td  class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>" + response.maxDeductibleLimitOnBenefitTypes[i] + "</td>");

                    sbResponse.Append("</tr>");
                }
            }
            else
            {
                sbResponse.Append("<table   cellpadding='0' cellspacing='0'   class='style16' style=' font-family:Arial; font-size:10px; mso-table-layout-alt: fixed; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 480; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid windowtext; mso-border-insidev: .5pt solid windowtext; width: 100%;' >");
                sbResponse.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                sbResponse.Append("<th colspan='9' class='style13' style='border-top: 1.0pt solid windowtext;' ><B>TABLE OF BENEFITS</B></th></tr>");
                sbResponse.Append("<tr  style='background-color: #06AFF2;'>");
                sbResponse.Append("<th  style='width:15%;' class='style13'><b>Benefit Type</th><th style='width:15%;' class='style13'><b>Treatment Covered</th><th  style='width:12%;' class='style13'><b>Sub Limit</b></th><th style='width:8%;' class='style13'><b>Deductible</th>");
                sbResponse.Append("<th style='width:1px;' class='style13EmplyColumn'></th>");
                sbResponse.Append("<th style='width:15%;' class='style13'><b>Benefit Type</th><th  style='width:15%;' class='style13'><b>Treatment Covered</th><th style='width:12%;' class='style13'><b>Sub Limit</b></th><th  style='width:8%;' class='style13'><b>Deductible</th>");
                sbResponse.Append("</tr>");
                for (int i = 0; i < response.benefitDesc.Length; i++)
                {
                    sbResponse.Append("<tr>");
					////sbResponse.Append("<td  class='styletdBorder'  style='vertical-align:top;padding-left:2px;'><b>" + response.benefitDesc[i].Trim() + "</b></td><td  class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td  class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>" + response.benefitTypeLimit[i].Trim() + "</td>");
                    response.benefitTypeLimit[i] = !string.IsNullOrWhiteSpace(response.benefitTypeLimit[i]) ? response.benefitTypeLimit[i].Contains("99999999.99") ? "MAX Annual Limit" : response.benefitTypeLimit[i] : response.benefitTypeLimit[i];
                    sbResponse.Append("<td  class='styletdBorder'  style='vertical-align:top;padding-left:2px;'><b>" + response.benefitDesc[i].Trim() + "</b></td><td  class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td  class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>" + response.benefitTypeLimit[i].Trim() + "</td>");
                    if (response.benefitDesc[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100</td></tr>");
                    else
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.maxDeductibleLimitOnBenefitTypes[i] + "</td>");

                    sbResponse.Append("<td  class='styletdBorder'    style='vertical-align:top;'></td>");
                    i++;
                    if (i < response.benefitDesc.Length)
                    {
						////sbResponse.Append("<td   class='styletdBorder'  style='vertical-align:top;padding-left:2px;'><b>" + response.benefitDesc[i].Trim() + "</b></td><td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitTypeLimit[i].Trim() + "</td>");
                        response.benefitTypeLimit[i] = !string.IsNullOrWhiteSpace(response.benefitTypeLimit[i]) ? response.benefitTypeLimit[i].Contains("99999999.99") ? "MAX Annual Limit" : response.benefitTypeLimit[i] : response.benefitTypeLimit[i];
                        sbResponse.Append("<td   class='styletdBorder'  style='vertical-align:top;padding-left:2px;'><b>" + response.benefitDesc[i].Trim() + "</b></td><td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitTypeLimit[i].Trim() + "</td>");
                        if (response.benefitDesc[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                            sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100</td></tr>");
                        else
                            sbResponse.Append("<td class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>" + response.maxDeductibleLimitOnBenefitTypes[i] + " </td>");
                    }
                    else
                    {
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>&nbsp;</td>");
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>&nbsp;</td>");
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>&nbsp;</td>");
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>&nbsp;</td>");
                    }
                    sbResponse.Append("</tr>");
                }
            }
            sbResponse.Append("</table>");
            ServiceListReport.Visible = true;
            ServiceListReport.InnerHtml = sbResponse.ToString();
        }
    }

    private void DisplayTOBDOMReport(string SearchMode)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqTOBDOMRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqTOBDOMResponse_DN response;
        response = null;
        request = new OS_DXC_WAP.CaesarWS.ReqTOBDOMRequest_DN();
        request.transactionID = TransactionManager.TransactionID();
        request.planID = PlanID;
        request.contNo = ContractNo;
        request.mbrshipNo = MembershipNo;
        request.deptNameCode = Dept;
        request.providerCode = ProviderCode;
        request.verificationID = Int64.Parse(VerificationID.Trim());
        request.verificationStatus = VerificationStatus;
        request.txnDate = DateTime.Today;
        request.failReason = RejectionReason;
        request.searchMode = SearchMode;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        response = ws.ReqTOBDOM(request);

        ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBDOMRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBDOMRequest_DN>();
        ////XmlReq.Request(request, "ReqTOBDOMRequest_DN");

        ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBDOMResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBDOMResponse_DN>();
        ////XmlResp.Response(response, "ReqTOBDOMResponse_DN");

        if (response.status == "0")
        {
            lblVerificationID.Text = VerificationID;
            lblDateOfVisit.Text = DateTime.Now.ToString();
            lblEffectiveTo.Text = response.effectiveEndDate;
            ////CR Contract No, customer name and limit changes
            ////Start
            ////lblPolicyNo.Text = response.contractNo_CCHI;
            ////lblPolicyHolder.Text = response.customerName;
            lblPolicyNo.Text = response.policyNo;
            lblPolicyHolder.Text = response.policyHolder;
            ////End  
            lblScheme.Text = response.schemeName;
            lblBupaCardNo.Text = MembershipNo;
            lblHospitalAccomodation.Text = response.bedType;
            lblInPatientDeductibleDA.Text = response.dependentDeductibleAmountForInPatient;
            lblOutPatientDeductibleDA.Text = response.dependentDeductibleAmountForOutPatient;
            lblInPatientDeductibleDP.Text = response.dependentDeductiblePercentageForInPatient + " " + response.dependentDeductibleAmountForInPatient;
            lblOutPatientDeductibleDP.Text = response.dependentDeductiblePercentageForOutPatient + " Upto: ";
            lblInPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForInPatient;
            
            lblOutPatientDeductibleMA.Text = response.mainMemberDeductibleAmountForOutPatient;
            lblInPatientDeductibleMA.Text = response.mainMemberDeductibleAmountForInPatient;
            lblReferralLetterD.Text = response.dependentMandatoryReferralIndicator;
            lblReferralLetterM.Text = response.mainMemberMandatoryReferralIndicator;
            lblEffectiveFrom.Text = response.effectiveDate.ToString();
            lblOverallAnnualLimit.Text = response.overallLimited;
            lblPreauthorisationLimit.Text = response.preauthLimited;
            lblScheme.Text = response.schemeName;
            lblTobSpecialInstructionsMA.Text = response.specialInstruction;
            lblTreatmentType.Text = response.treatmentType;

            //// CR414 TOB Changes
            ////Start
            ////lblOutPatientDeductibleDP.Text = response.dependentDeductiblePercentageForOutPatient + " Upto: ";
            ////
            if (response.TOBTemplate == "Y")
            {
                lblOutPatientDeductibleMP.Text = "*H - " + response.memberContributionForOutPatientTreatmentHospital + "</br>*P - " + response.memberContributionForOutPatientTreatmentPolyclinic + "</br>*M - " + response.memberContributionForOutPatientTreatmentMPN;
                lblOutPatientDeductibleDP.Text = "*H - " + response.memberContributionForOutPatientTreatmentHospital + "</br>*P - " + response.memberContributionForOutPatientTreatmentPolyclinic + "</br>*M - " + response.memberContributionForOutPatientTreatmentMPN;
                trNewTOB.Visible = true;
            }
            else
            {
                lblOutPatientDeductibleDP.Text = response.dependentDeductiblePercentageForOutPatient;  ///+ "Upto: ";
                lblOutPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForOutPatient; // + "Upto: ";
                trNewTOB.Visible = false;
            }
            ////End

            StringBuilder sbResponse = new StringBuilder();
            if (response.benefitDesc.Length == 1)
            {
                sbResponse.Append("<table   cellpadding='0' cellspacing='0'   class='style16' style=' font-family:Arial; font-size:10px; mso-table-layout-alt: fixed; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 480; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid windowtext; mso-border-insidev: .5pt solid windowtext; width: 100%;' >");
                sbResponse.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                sbResponse.Append("<th colspan='4' class='style13' style='border-top: 1.0pt solid windowtext;' ><B>TABLE OF BENEFITS</B></th></tr>");
                sbResponse.Append("<tr  style='background-color: #06AFF2;'>");
                sbResponse.Append("<th  style='width:20%;' class='style13'><b>Benefit Type</th><th style='width:55%;' class='style13'><b>Treatment Covered</th><th  style='width:15%;' class='style13'><b>Sub Limit</b></th><th style='width:10%;' class='style13'><b>Deductible</th>");
                sbResponse.Append("</tr>");
                for (int i = 0; i < response.benefitDesc.Length; i++)
                {
                    sbResponse.Append("<tr>");
					////sbResponse.Append("<td class='styletdBorder' style='vertical-align:top;padding-left:2px;' ><b>" + response.benefitDesc[i].Trim() + "</b></td><td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitHeadSubLimit[i].Trim() + "</td>");
                    response.benefitHeadSubLimit[i] = !string.IsNullOrWhiteSpace(response.benefitHeadSubLimit[i]) ? response.benefitHeadSubLimit[i].Contains("99999999.99") ? "MAX Annual Limit" : response.benefitHeadSubLimit[i] : response.benefitHeadSubLimit[i];                   
                    sbResponse.Append("<td class='styletdBorder' style='vertical-align:top;padding-left:2px;' ><b>" + response.benefitDesc[i].Trim() + "</b></td><td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitHeadSubLimit[i].Trim() + "</td>");
                    if (response.benefitDesc[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                        sbResponse.Append("<td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100 </td></tr>");  // response.service_Code
                    else
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.maxDeductibleLimitOnBenefitTypes[i] + "</td>");


                    sbResponse.Append("</tr>");  // response.service_Code
                }
            }
            else
            {
                sbResponse.Append("<table     cellpadding='0' cellspacing='0'   class='style16' style=' font-family:Arial; font-size:10px; mso-table-layout-alt: fixed; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 480; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid windowtext; mso-border-insidev: .5pt solid windowtext; width: 100%;' >");
                sbResponse.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                sbResponse.Append("<th colspan='9' class='style13'  style='border-top: 1.0pt solid windowtext;' ><B>TABLE OF BENEFITS</B></th></tr>");
                sbResponse.Append("<tr  style='background-color: #06AFF2;'>");
                sbResponse.Append("<th  style='width:15%;' class='style13'><b>Benefit Type</th><th style='width:15%;' class='style13'><b>Treatment Covered</th><th  style='width:12%;' class='style13'><b>Sub Limit</b></th><th style='width:8%;' class='style13'><b>Deductible</th>");
                sbResponse.Append("<th style='width:1px;' class='style13EmplyColumn'></th>");
                sbResponse.Append("<th style='width:15%;' class='style13'><b>Benefit Type</th><th  style='width:15%;' class='style13'><b>Treatment Covered</th><th style='width:12%;' class='style13'><b>Sub Limit</b></th><th  style='width:8%;' class='style13'><b>Deductible</th>");
                sbResponse.Append("</tr>");
                for (int i = 0; i < response.benefitDesc.Length; i++)
                {
                    sbResponse.Append("<tr>");
					////sbResponse.Append("<td class='styletdBorder' style='vertical-align:top;padding-left:2px;' ><b>" + response.benefitDesc[i].Trim() + "</b></td><td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitHeadSubLimit[i].Trim() + "</td>");
                    response.benefitHeadSubLimit[i] = !string.IsNullOrWhiteSpace(response.benefitHeadSubLimit[i]) ? response.benefitHeadSubLimit[i].Contains("99999999.99") ? "MAX Annual Limit" : response.benefitHeadSubLimit[i] : response.benefitHeadSubLimit[i];
                    sbResponse.Append("<td class='styletdBorder' style='vertical-align:top;padding-left:2px;' ><b>" + response.benefitDesc[i].Trim() + "</b></td><td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitHeadSubLimit[i].Trim() + "</td>");
                    if (response.benefitDesc[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100 </td></tr>");  // response.service_Code
                    else
                        sbResponse.Append("<td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.maxDeductibleLimitOnBenefitTypes[i] + "</td>");

                    sbResponse.Append("<td  class='styletdBorder'    style='vertical-align:top;'></td>");
                    i++;
                    if (i < response.benefitDesc.Length)
                    {
						////sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'><b>" + response.benefitDesc[i].Trim() + "</b></td><td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitHeadSubLimit[i].Trim() + "</td>");
                        response.benefitHeadSubLimit[i] = !string.IsNullOrWhiteSpace(response.benefitHeadSubLimit[i]) ? response.benefitHeadSubLimit[i].Contains("99999999.99") ? "MAX Annual Limit" : response.benefitHeadSubLimit[i] : response.benefitHeadSubLimit[i];
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'><b>" + response.benefitDesc[i].Trim() + "</b></td><td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.treatmentCoverd[i].Trim() + "</td><td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.benefitHeadSubLimit[i].Trim() + "</td>");
                        if (response.benefitDesc[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                            sbResponse.Append("<td class='styletdBorder' style='vertical-align:top;padding-left:2px;'>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100 </td></tr>");  // response.service_Code
                        else
                            sbResponse.Append("<td class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>" + response.maxDeductibleLimitOnBenefitTypes[i] + " </td>");

                    }
                    else
                    {
                        sbResponse.Append("<td class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>&nbsp;</td>");
                        sbResponse.Append("<td  class='styletdBorder' style='vertical-align:top;padding-left:2px;'>&nbsp;</td>");
                        sbResponse.Append("<td class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>&nbsp;</td>");
                        sbResponse.Append("<td class='styletdBorder'  style='vertical-align:top;padding-left:2px;'>&nbsp;</td>");

                    }
                    sbResponse.Append("</tr>");  // response.service_Code
                }
            }
            sbResponse.Append("</table>");
            ServiceListReport.Visible = true;
            ServiceListReport.InnerHtml = sbResponse.ToString();
        }
    }

    #region "QR Code"
    public Image byteArrayToImage(byte[] byteArrayIn)
    {
        MemoryStream ms = new MemoryStream(GetQrCode(VerificationID));
        Image returnImage = Image.FromStream(ms);
        return returnImage;
    }

    public static Image GenerateQrCode(string content, bool isRotate)
    {
        QrEncoder qrEncoder = new QrEncoder();
        Bitmap qrBitmap = null;

        if (isRotate)
            qrBitmap = RotateImage(qrEncoder.EncodeToBitMapFile(content), 45);
        else
            qrBitmap = qrEncoder.EncodeToBitMapFile(content);
        return qrBitmap;
    }

    private byte[] GetQrCode(string referenceNo)
    {
        Image bmp = GenerateQrCode(referenceNo, true);
        MemoryStream ms = new MemoryStream();
        bmp.Save(ms, ImageFormat.Png);
        byte[] bmpBytes = ms.GetBuffer();
        bmp.Dispose();
        ms.Close();
        return bmpBytes;
    }

    private byte[] _qrImage;

    public byte[] QrImage
    {
        get { return _qrImage; }
        set { _qrImage = value; }
    }

    private static Bitmap RotateImage(Bitmap bmpSrc, float theta)
    {
        Matrix mRotate = new Matrix();
        mRotate.Translate(bmpSrc.Width / -2, bmpSrc.Height / -2, MatrixOrder.Append);
        mRotate.RotateAt(theta, new System.Drawing.Point(0, 0), MatrixOrder.Append);
        using (GraphicsPath gp = new GraphicsPath())
        {  // transform image points by rotation matrix
            gp.AddPolygon(new System.Drawing.Point[] { new System.Drawing.Point(0, 0), new System.Drawing.Point(bmpSrc.Width, 0), new System.Drawing.Point(0, bmpSrc.Height) });
            gp.Transform(mRotate);
            System.Drawing.PointF[] pts = gp.PathPoints;

            // create destination bitmap sized to contain rotated source image
            Rectangle bbox = boundingBox(bmpSrc, mRotate);
            Bitmap bmpDest = new Bitmap(bbox.Width, bbox.Height);
            Color bgColor = Color.Transparent;
            SolidBrush brush = new SolidBrush(bgColor);
            using (Graphics gDest = Graphics.FromImage(bmpDest))
            {  // draw source into dest
                gDest.FillRectangle(brush, 0, 0, bmpSrc.Width, bmpSrc.Height);
                Matrix mDest = new Matrix();
                mDest.Translate(bmpDest.Width / 2, bmpDest.Height / 2, MatrixOrder.Append);
                gDest.Transform = mDest;
                gDest.DrawImage(bmpSrc, pts);
                return bmpDest;
            }
        }
    }

    private static Rectangle boundingBox(System.Drawing.Image img, Matrix matrix)
    {
        GraphicsUnit gu = new GraphicsUnit();
        Rectangle rImg = Rectangle.Round(img.GetBounds(ref gu));
        Color bgColor = Color.Transparent;
        SolidBrush brush = new SolidBrush(bgColor);

        // Transform the four points of the image, to get the resized bounding box.
        System.Drawing.Point topLeft = new System.Drawing.Point(rImg.Left, rImg.Top);
        System.Drawing.Point topRight = new System.Drawing.Point(rImg.Right, rImg.Top);
        System.Drawing.Point bottomRight = new System.Drawing.Point(rImg.Right, rImg.Bottom);
        System.Drawing.Point bottomLeft = new System.Drawing.Point(rImg.Left, rImg.Bottom);
        System.Drawing.Point[] points = new System.Drawing.Point[] { topLeft, topRight, bottomRight, bottomLeft };
        GraphicsPath gp = new GraphicsPath(points,
        new byte[] { (byte)PathPointType.Start, (byte)PathPointType.Line, (byte)PathPointType.Line, (byte)PathPointType.Line });
        gp.Transform(matrix);
        return Rectangle.Round(gp.GetBounds());
    }

    #endregion

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientID"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["ProCurrentLogID"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }
}

//public class ClassXMLGeneration<T>
//{
//    public void Request(T classType, string req)
//    {
//        System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
//        System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\CaesarXML\\" + req + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
//        xmlSrQ.Serialize(file, classType);
//        file.Close();


//    }
//    public void Response(T classType, string resp)
//    {
//        System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
//        System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\CaesarXML\\" + resp + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
//        xmlSr.Serialize(file, classType);
//        file.Close();
//    }
//}