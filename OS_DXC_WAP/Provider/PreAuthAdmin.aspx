﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Provider_PreAuthAdmin" Codebehind="PreAuthAdmin.aspx.cs" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <p>
        Pre-Auth Automation - Administration
    </p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="Server">
    <%--<asp:ScriptManager runat="server" ID="scriptManager" />--%>
  
    <div>
        <h3>Pre-Auth Automation - Service Categories</h3>
        <telerik:radgrid id="RadGrid2" runat="server" allowpaging="True" pagesize="10"
            datasourceid="SqlDataSource2" allowautomaticinserts="True" allowautomaticupdates="True"
            allowautomaticdeletes="True">
              <MasterTableView EditMode="InPlace" CommandItemDisplay="Bottom" DataSourceID="SqlDataSource2"  DataKeyNames="CategoryID" AutoGenerateColumns="True">
                <EditFormSettings>
                  <EditColumn UniqueName="EditCommandColumn1" />
                  <PopUpSettings ScrollBars="None" />
                </EditFormSettings>
                <Columns>
                  <telerik:GridEditCommandColumn />
                <%--  <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" />
                  <telerik:GridBoundColumn DataField="CategoryID" HeaderText="CategoryID" ReadOnly="True"
                    UniqueName="QuestionId" Display="False" />
                  <telerik:GridBoundColumn DataField="CategoryID" HeaderText="CategoryID" UniqueName="CategoryID" />
                  <telerik:GridBoundColumn DataField="CategoryParentID" HeaderText="CategoryParentID" UniqueName="CategoryParentID">   </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="CategoryCode" HeaderText="CategoryCode" UniqueName="CategoryCode" />
                    <telerik:GridBoundColumn DataField="Category" HeaderText="Category" UniqueName="Category" />--%>
                </Columns>
              </MasterTableView>
</telerik:radgrid>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
            SelectCommand="select CategoryID,CategoryParentID,CategoryCode,Category from PreAuthCategory order by Categorycode"
          DeleteCommand="DELETE FROM [PreAuthCategory] WHERE [CategoryID] = @CategoryID"
            InsertCommand="INSERT INTO [PreAuthCategory] ([CategoryID], [CategoryParentID], [CategoryCode], [Category]) VALUES (@CategoryID, @CategoryParentID,@CategoryCode, @Category)"
            UpdateCommand="UPDATE [PreAuthCategory] SET [CategoryParentID] = @CategoryParentID, [CategoryCode] = @CategoryCode, [Category] = @Category WHERE [CategoryID] = @CategoryID">
            <DeleteParameters>
                <asp:Parameter Name="CategoryID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Question" Type="String" />
                <asp:Parameter Name="IsMandatory" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Question" Type="String" />
                <asp:Parameter Name="IsMandatory" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>

    <div>
        <h3>Pre-Auth Automation - Service Category Options</h3>
        <telerik:radgrid id="RadGrid3" runat="server" allowpaging="True" pagesize="10"
            datasourceid="SqlDataSource3" allowautomaticinserts="True" allowautomaticupdates="True"
            allowautomaticdeletes="True">
  <MasterTableView EditMode="InPlace" CommandItemDisplay="Bottom" DataSourceID="SqlDataSource3" DataKeyNames="OptionCode" AutoGenerateColumns="True">
    <EditFormSettings>
      <EditColumn UniqueName="EditCommandColumn1" />
      <PopUpSettings ScrollBars="None" />
    </EditFormSettings>
    <Columns>
      <telerik:GridEditCommandColumn />
    <%-- <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" />
       <telerik:GridBoundColumn DataField="OptionCode" HeaderText="OptionCode" ReadOnly="True" UniqueName="OptionCode" Display="False" />
      <telerik:GridBoundColumn DataField="OptionDescription" HeaderText="OptionDescription" UniqueName="OptionDescription" />
      <telerik:GridBoundColumn DataField="OptionInputType" HeaderText="OptionInputType" UniqueName="OptionInputType">   </telerik:GridBoundColumn>
      <telerik:GridBoundColumn DataField="QID" HeaderText="QID" UniqueName="QID" />--%>
    </Columns>
  </MasterTableView>
</telerik:radgrid>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
            SelectCommand="select OptionCode,OptionDescription, OptionInputType,QID from dbo.PreAuthOption order by OptionCode"
         DeleteCommand="DELETE FROM [PreAuthOption] WHERE [OptionCode] = @OptionCode"
            InsertCommand="INSERT INTO [PreAuthOption] ([OptionCode], [OptionDescription], [OptionInputType], [QID]) VALUES (@OptionCode, @OptionDescription,@OptionInputType,@QID)"
            UpdateCommand="UPDATE [PreAuthOption] SET [OptionDescription] = @OptionDescription, [OptionInputType] = @OptionInputType, [QID] = @QID WHERE [OptionCode] = @OptionCode">
            <DeleteParameters>
                <asp:Parameter Name="OptionCode" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="OptionCode" Type="String" />
                <asp:Parameter Name="OptionDescription" Type="String" />
                <asp:Parameter Name="OptionInputType" Type="String" />
                <asp:Parameter Name="QID" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="OptionCode" Type="String" />
                <asp:Parameter Name="OptionDescription" Type="String" />
                <asp:Parameter Name="OptionInputType" Type="String" />
                <asp:Parameter Name="QID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>

      <div>
        <h3>Pre-Auth Automation - Questions</h3>
        <telerik:radgrid id="RadGrid1" runat="server" allowpaging="True" pagesize="10"
            datasourceid="SqlDataSource1" allowautomaticinserts="True" allowautomaticupdates="True"
            allowautomaticdeletes="True">
              <MasterTableView EditMode="InPlace" CommandItemDisplay="Bottom" DataSourceID="SqlDataSource1" DataKeyNames="QuestionId" AutoGenerateColumns="True">
                <EditFormSettings>
                  <EditColumn UniqueName="EditCommandColumn1" />
                  <PopUpSettings ScrollBars="None" />
                </EditFormSettings>
                <Columns>
                  <telerik:GridEditCommandColumn />
                 <%-- <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" />
                  <telerik:GridBoundColumn DataField="QuestionId" HeaderText="QuestionId" ReadOnly="True"  UniqueName="QuestionId" Display="False" />
                  <telerik:GridBoundColumn DataField="QuestionId" HeaderText="QuestionId" UniqueName="QuestionId" />
                  <telerik:GridBoundColumn DataField="Question" HeaderText="Question" UniqueName="Question">   </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="IsMandatory" HeaderText="IsMandatory" UniqueName="IsMandatory" />--%>
                </Columns>
              </MasterTableView>
</telerik:radgrid>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
            SelectCommand="select QuestionId, Question,IsMandatory from PreAuthDiagnosisQuestions  order by Question"
            DeleteCommand="DELETE FROM [PreAuthDiagnosisQuestions] WHERE [QuestionId] = @QuestionId"
            InsertCommand="INSERT INTO [PreAuthDiagnosisQuestions] ([Question], [IsMandatory]) VALUES (@Question, @IsMandatory)"
            UpdateCommand="UPDATE [PreAuthDiagnosisQuestions] SET  [Question] = @Question, [IsMandatory] = @IsMandatory WHERE [QuestionId] = @QuestionId">
            <DeleteParameters>
                <asp:Parameter Name="QuestionId" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Question" Type="String" />
                <asp:Parameter Name="IsMandatory" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Question" Type="String" />
                <asp:Parameter Name="IsMandatory" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>

</asp:Content>

