﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Print.master" AutoEventWireup="true" Inherits="Provider_details" Codebehind="details.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">
<h2>Request Detials</h2>
                <br />

                <table width="100%" align="left">
                    <tr>
                        <td class="style1" align="left" colspan="2">
                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" align="left" colspan="2">
                            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1" colspan="2"><div style="noprint">Print processed notifictation form,to affix provider stamp & sign-off by authorized signatory & submit the processed notificsation form again </div></td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1" colspan="2"> <dx:ASPxUploadControl ID="uploadfiles" runat="server" Width="100%" 
        onfileuploadcomplete="uploadfiles_FileUploadComplete"
        ShowProgressPanel="True" ShowUploadButton="True" Visible="<%# _show %>">
        <ValidationSettings AllowedFileExtensions=".pdf,.jpg,.jepg,.png">
        </ValidationSettings>
    </dx:ASPxUploadControl></td>
                    </tr>
                    <tr>
                        <td class="style1" colspan="2"> <p align="center"><asp:Label ID="lblMsg" runat="server" style="font-weight: 700" ></asp:Label></p></td>
                    </tr>
                    <tr>
                        <td class="style1" colspan="2">

                <dx:ASPxGridView ID="detailGrid" runat="server" DataSourceID="sqldsDeatils"
                    Width="100%" 
                    AutoGenerateColumns="False">
                    <Columns>
                         <dx:GridViewDataTextColumn FieldName="Service_Code" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="1">
                            
                        </dx:GridViewDataTextColumn>
                       
                        <dx:GridViewDataTextColumn FieldName="Price" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Service_Type" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Remark" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                       
                    </Columns>
                    <Settings ShowFooter="True" />
                   
                </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td align="right">
                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" 
                        onclick="btnSubmit_Click" Theme="Office2010Blue">
                    </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">

            <asp:SqlDataSource ID="sqldsDeatils" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>" 
                SelectCommand="SELECT Service_Code, Description, Price, Service_Type, Status, Remark FROM ServicesCodeItems WHERE (RefNo = @sRefNo)">
                <SelectParameters>
                    <asp:QueryStringParameter Name="sRefNo" QueryStringField="refno" 
                        Type="String" />
        </SelectParameters>
            </asp:SqlDataSource>
                        </td>
                        <td align="right">
                            &nbsp;</td>
                    </tr>
                </table>
                

               
    <br />
   
    <br />

                <br />
                <p align="center">
                    &nbsp;</p>

            </asp:Content>

