﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="TOB" Codebehind="TOB.aspx.cs" %>
<%@ Register Src="~/Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
     <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
    <script type="text/javascript"  src="js/jquery.dataTables.js"></script>

    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="styles.css" type="text/css" />


    <script type="text/javascript" src="../functions/Login/jquery.js"></script>
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <link href="css/basic.css" rel="stylesheet" type="text/css" />
    <link href="css/basic_ie.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family:Arial;">
<form id="Form1" runat="server">

<div class="adbanner" align="right" >
        <img src="../images/logo2014.jpg" width="100px" height="100px">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    <div class="noPrint" align="right">
      
        <table width=100% ><tr><td align=left>
            <label ID="Label1" ><font size=4>Table of Benefits</font></label>  </td>
            <td><a href="#" onclick="window.print()"><img border="0" src="images/printer.gif" alt="Print" width="16" height="16" ></a>
             <asp:HyperLink ID="lnkBackOption" runat="server"
            Font-Size="Small" NavigateUrl="~/Provider/ExistingRequestSearch.aspx" Visible="true"><img alt="Go back"  style=" border:0px; cursor:pointer;" src="../icons/back4.png" /></asp:HyperLink>
             
            <%--<img alt="Go back" style="cursor:pointer;" src="../icons/back4.png" onClick="history.go(-1)" height="33px" />--%>
            </td></tr></table>
 
             <br />
            <asp:LinkButton ID="lnkClaim" runat="server" Font-Size="Small" OnClick="lnkPrintClaimForm_Click">Claim Form</asp:LinkButton><br />
            <br />

    </div>

      <div>
      

      

            <asp:Panel ID="Panel1" runat="server" >
                <table style="width: 608px; font-size: small; font-family: Arial;" border="1px" bordercolor="silver">
                    <tr>
                        <td style="width: 427px; border-top: black 1pt solid; border-left: black 1pt solid; border-bottom: black 1pt solid; border-right-width: 1pt; border-right-color: black;" >
                            <span style="text-align:center;">Verification ID<?xml namespace="" prefix="O" ?></span>
                        </td>
                        <td colspan="2" style="border-top: black 1pt solid; border-left-width: 1pt; border-left-color: black;
                            border-bottom: black 1pt solid; border-right-width: 1pt; border-right-color: black">
                            <span>
                                <asp:Label ID="lblVerificationID" runat="server" Width="166px" Font-Bold="True"></asp:Label></span>
                            <span></span>
                        </td>
                        <td style="width: 115px; border-top: black 1pt solid; border-left-width: 1pt; border-left-color: black; border-bottom: black 1pt solid; border-right-width: 1pt; border-right-color: black;" >
                            <span>Date</span>
                        </td>
                        <td style="width: 115px; border-right: black 1pt solid; border-top: black 1pt solid; border-left-width: 1pt; border-left-color: black; border-bottom: black 1pt solid;">
                            <asp:Label ID="lblDateOfVisit" runat="server" Width="120px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <span>Verification is valid only on the date specified above</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 427px">
                            <span></span>
                        </td>
                        <td style="width: 175px">
                            <span></span>
                        </td>
                        <td style="width: 356px">
                            <span></span>
                        </td>
                        <td style="width: 30px">
                            <span></span>
                        </td>
                        <td style="width: 125px">
                            <span></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#06aff2">
                            <span>MEMBER DETAILS</span>
                        </td>
                        <td colspan="3" bgcolor="#06aff2">
                            <span>POLICY DETAILS</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Member No</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblBupaCardNo" runat="server" Width="175px"></asp:Label></td>
                        <td style="width: 356px" >
                            <span>Customer No:</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblCustomerNo" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Name</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblMemberName" runat="server" Width="175px"></asp:Label></td>
                        <td style="width: 356px" >
                            <span>Customer Name:</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblCustomerName" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>ID No</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblIDNo" runat="server" Width="175px"></asp:Label></td>
                        <td style="width: 356px" >
                            <span>Scheme Name:</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblScheme" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Date of Birth</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblDOB" runat="server" Width="175px"></asp:Label></td>
                        <td style="width: 356px" >
                            <span>Effective Date:</span>
                        </td>
                        <td colspan="2">
                            </td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Member type</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblMemberType" runat="server" Width="175px"></asp:Label></td>
                        <td style="width: 356px" >
                            <span>From</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblEffectiveFrom" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Age</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblAge" runat="server" Width="175px"></asp:Label></td>
                        <td style="width: 356px" >
                            <span>To</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblEffectiveTo" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Gender</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblGender" runat="server" Width="175px"></asp:Label></td>
                        <td style="width: 356px">
                            <span></span>
                        </td>
                        <td style="width: 30px">
                            <span></span>
                        </td>
                        <td style="width: 125px">
                            <span></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 427px; height: 12px;">
                        </td>
                        <td style="width: 175px; height: 12px;">
                        </td>
                        <td style="width: 356px; height: 12px;">
                        </td>
                        <td style="width: 30px; height: 12px;">
                        </td>
                        <td style="width: 125px; height: 12px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" bgcolor="#06aff2">
                            <span>PROVIDER DETAILS</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Provider</span>
                        </td>
                        <td colspan="4">
                            <span></span>
                            <asp:Label ID="lblProviderName" runat="server" Width="320px"></asp:Label>
                            <span></span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Department</span>
                        </td>
                        <td colspan="4">
                            <span>
                                <asp:Label ID="lblDepartment" runat="server" Width="320px"></asp:Label></span>
                            <span></span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Verification result</span>
                        </td>
                        <td colspan="4">
                            <asp:Label ID="lblVerificationResult" runat="server" Width="320px" Font-Bold="True" ForeColor="Red"></asp:Label>
                            <span></span><span></span><span></span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 427px" >
                            <span>Remarks:</span>
                        </td>
                        <td colspan="4">
                            <asp:Label ID="lblRemarks" runat="server" Width="320px"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div style="font-size: small; font-family: Arial; text-align: left" align="center">
            <asp:Panel ID="Panel5" runat="server" Width="608px">
             <div id="ServiceListReport"  visible="true" style=" font-family:Arial; ; height:auto; width:100%; " runat="server" >   </div>

            
                <table style="width: 608px; font-size: small; font-family: Arial;" border="1px">
                    
                    
                   
                    <tr>
                        <td style="width: 240px; height: 21px">
                        </td>
                        <td style="width: 164px; height: 21px">
                        </td>
                        <td style="width: 100px; height: 21px">
                        </td>
                        <td style="width: 100px; height: 21px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 240px" >
                            Annual limit</td>
                        <td colspan="1" style="width: 164px">
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblOverallAnnualLimit" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <em>* This reflects member annual limit not remaining limit<?xml namespace="" prefix="O" ?><?xml
                                namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><?xml
                                    namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><?xml
                                        namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><o:p></o:p></em></td>
                    </tr>
                    <tr>
                        <td style="width: 240px" >
                            <p>
                                <span style="font-size: 10pt; font-family: Arial">Treatment type</span></p>
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lblTreatmentType" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 240px" >
                            <p>
                                <span style="font-size: 10pt; font-family: Arial">Hospital Accomodation</span></p>
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lblHospitalAccomodation" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 240px" >
                            <span style="font-size: 10pt; font-family: Arial">Pre-authorisation limit</span></td>
                        <td colspan="3">
                            <asp:Label ID="lblPreauthorisationLimit" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 240px" >
                            <p>
                                <span>Referral letter required?</span></p>
                        </td>
                        <td colspan="3">
                            Main member: 
                            <asp:Label ID="lblReferralLetterM" runat="server" Width="100px"></asp:Label>
                            <br />
                            Dependent:
                            <asp:Label ID="lblReferralLetterD" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <asp:Panel ID="Panel2" runat="server" Width="540px">
                <table style="width: 608px; border-bottom-style:outset;" border="1px">
                    <tr>
                        <td style="width: 140px" bgcolor="#06aff2">
                        </td>
                        <td style="width: 75px; text-align: center" bgcolor="#06aff2">
                            <span style="font-size: 10pt; font-family: Arial"><strong>Main member (amount)</strong></span></td>
                        <td style="width: 75px; text-align: center" bgcolor="#06aff2">
                            <span style="font-size: 10pt; font-family: Arial"><strong>Main member (%)</strong></span></td>
                        <td style="width: 75px; text-align: center" bgcolor="#06aff2">
                            <span style="font-size: 10pt; font-family: Arial"><strong>Dependent (amount)</strong></span></td>
                        <td style="width: 75px; text-align: center" bgcolor="#06aff2">
                            <span style="font-size: 10pt; font-family: Arial"><strong>Dependent (%)</strong></span></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <span style="font-size: 10pt; font-family: Arial"><strong>Out Patient Deductible</strong></span></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblOutPatientDeductibleMA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblOutPatientDeductibleMP" runat="server" Width="105px" style="margin-left:2px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblOutPatientDeductibleDA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblOutPatientDeductibleDP" runat="server" Width="105px" style="margin-left:2px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <span style="font-size: 10pt; font-family: Arial"><strong>In Patient Deductible</strong></span></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblInPatientDeductibleMA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblInPatientDeductibleMP" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblInPatientDeductibleDA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblInPatientDeductibleDP" runat="server" Width="75px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <span style="font-size: 10pt; font-family: Arial"><strong>TOB special instructions</strong></span></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblTobSpecialInstructionsMA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblTobSpecialInstructionsMP" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblTobSpecialInstructionsDA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblTobSpecialInstructionsDP" runat="server" Width="75px"></asp:Label></td>
                    </tr>
                    <tr runat="server" id="trNewTOB">
                        <td colspan="5">
                            <span runat="server" id="spanNewTOB" style="font-size: 7pt; font-family: Arial; font-weight: 900"><strong>* H - Hospital &nbsp; &nbsp; P - Polyclinic &nbsp;&nbsp; M - MPN </strong></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            &nbsp;
            <br />
            <asp:Panel ID="Panel3" BorderStyle="Outset" runat="server" Height="50px" Width="540px">
                <p align="center" class="MsoNormal" style="text-align: center">
                    <b><u><span style="font-size: 10pt; font-family: 'Arial','sans-serif'">Pre-authorization
                        (Please refer to the Benefit Table Pre-<span class="SpellE">authorisation</span>
                        section)</span></u></b><span style="font-size: 10pt; font-family: 'Arial','sans-serif'"><?xml
                            namespace="" prefix="O" ?><o:p></o:p></span></p>
                <p class="MsoNormal">
                    <span style="font-size: 10pt; font-family: 'Arial','sans-serif'">Unless otherwise specified
                        in the Benefit Table Pre-authorization required for MRI, CT-Scan and if treatment
                        exceeds the SR1000 and for All Inpatients and Daycare, except emergencies, which
                        must be notified within 24 hours of admission. Ambulance is covered for all plans
                        based on medical necessity to and from hospital.
                        <o:p></o:p>
                    </span>
                </p>
                <p align="center" style="text-align: center">
                    <b><u><span>General Exclusions (Unless otherwise stated in above Benefit Table)</span></u></b><span
                        style="font-size: 10pt; font-family: 'Arial','sans-serif'"></span></p>
                <ol style="margin-top: 0in" type="1">
                    <li><span>Pregnancy and childbirth </span></li>
                    <li><span style="font-size: 10pt; font-family: 'Arial','sans-serif';">Contraception,
                        abortion, sterilization, sexual problems (including impotence), sex changes, assisted
                        reproduction (for example IVF treatment), termination of pregnancy </span></li>
                    <li><span>Dialysis </span></li>
                    <li><span>Naturally occurring conditions </span></li>
                    <li><span>AIDS, HIV, and sexually transmitted diseases </span></li>
                    <li><span>Cosmetic treatment unless it is a surgical operation to restore a person´s
                        appearance after an accident or as a result for surgery for cancer </span></li>
                    <li><span>Contamination, war, riots and natural disasters </span></li>
                    <li><span>Health hydrous, spa baths, nature cure clinics or similar establishments </span>
                    </li>
                    <li><span>Psychiatric Treatment, Mental and addictive conditions </span></li>
                    <li><span>Problems present at birth  congenital </span></li>
                    <li><span>Neo-natal problems </span></li>
                    <li><span>Vaccinations </span></li>
                    <li><span>Nursing at home </span></li>
                    <li><span>Work related accidents to the extent that the cost of treatment is payable
                        by GOSI </span></li>
                    <li><span>Artificial body parts, surgical aids and appliances unless implanted into
                        the body as an integral part of a surgical procedure </span></li>
                    <li><span>Weight control </span></li>
                    <li><span>Routine Dental treatment </span></li>
                    <li><span>Road traffic accidents to the extent that the cost of treatment is payable
                        by GOSI </span></li>
                    <li><span>Circumcision </span></li>
                    <li><span>Body piercing </span></li>
                    <li><span>Hazardous sports </span></li>
                    <li><span>Treatment not related to a medical condition </span></li>
                    <li><span>Un-prescribed drugs </span></li>
                    <li><span>Military service </span></li>
                    <li><span>Physiotherapy unless as part of post-surgical treatment authorized by a licensed
                        medical practitioner </span></li>
                    <li><span>Acupuncture, osteopathy, chiropractic and homeopathy </span></li>
                    <li><span>Self-inflicted </span></li>
                    <li><span>Associated non-medical and personal expenses<o:p></o:p></span> </li>
                </ol>
            </asp:Panel>
            <br />
            <asp:Label ID="Message1" runat="server" Text=""></asp:Label>
            <br />
        </div>
    <div id='content'>
        <div id="basic-modal-content">
            <p>This contract has been extended to <b><asp:Literal runat="server" ID="litExtendedDate"></asp:Literal></b>
                <br /><br />
                Please provide services to member in line with Table of Benefits. <br /><br />
                You can print the Table of Benefits with Extension date.<br />
            </p>
            <div style="vertical-align:central;"><p><a runat="server" id="aCloseOverlay" href="#" style="color:#fff;text-align:left;background:none;" >Close</a> 
            </p></div>
        </div>
    </div>
     <script type="text/javascript">
         function goto(x) {
             window.location = x;
         }
    </script>

    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

    </form>
</body>
</html>
