using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Collections;

public partial class Provider_Statement : System.Web.UI.Page
{
    private ReportDocument doc = null;
    private DataSet ds = null;
    String InvID = null;
    string clientId, cym, schemeId, pr;


    private String ls_cont_no, ls_cont_yymm;
    private ReportDocument crDoc;
    private DataSet rptDS = new DataSet();
    private DataTable rptDT = new DataTable();
    private Hashtable hasQueryValue;


    protected void Page_Init(object sender, EventArgs e)
    {
        
      

    }

    public void BindReport()
    {
        try
        {
            OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN response;
            response = GetIndicator();
            if (response != null && response.status == "0")
            {
                //InvID = Request.QueryString["ID"];  

                if (response.detail[0].CCHICatPolicyInd == "N")
                {
                    doc.Load(MapPath("MRCUSTTOB01.rpt"));
                    GetDataNewCCHIReport(response);
                }
                else
                {
                    doc.Load(MapPath("MRCUSTTOB02.rpt"));
                    GetDataOldCCHIReport(response);
                   
                }

                ////GetData();
            }
            else
            {
                Response.Write(response.errorMessage + "<br>");
            }
           
        }
        catch(Exception ex)
        {
            Response.Write(ex.Message + "<br>");
            Response.Write(ex.InnerException + "<br>");
        }
    }





    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }

            hasQueryValue = new Hashtable();
            string queryStringValues = string.Empty;

            ////clientId = Request["CustomerID"];

            if (Request.QueryString.Count > 0)
            {
                try
                {
                    var val = Request.QueryString["val"];
                    queryStringValues = Cryption.Decrypt(val);
                    hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                    ////Session["hashQueryPDetails"] = hasQueryValue;
                    clientId = hasQueryValue.ContainsKey("CustomerID") ? Convert.ToString(hasQueryValue["CustomerID"]) : string.Empty;
                    InvID = hasQueryValue.ContainsKey("ID") ? Convert.ToString(hasQueryValue["ID"]) : string.Empty;
                    cym = hasQueryValue.ContainsKey("CYM") ? Convert.ToString(hasQueryValue["CYM"]) : string.Empty;
                    schemeId = hasQueryValue.ContainsKey("SchemeID") ? Convert.ToString(hasQueryValue["SchemeID"]) : string.Empty;
                    pr = hasQueryValue.ContainsKey("Pr") ? Convert.ToString(hasQueryValue["Pr"]) : string.Empty;

                    String Par1 = pr;
                    doc = new ReportDocument();
                    if ((ViewState["ParametersShown"] != null) && (ViewState["ParametersShown"].ToString() == "True"))
                    {

                        BindReport();

                    }
                    else
                    {
                        BindReport();
                        ViewState["ParametersShown"] = "True";
                    }



                }
                catch (Exception ex)
                {
                    Response.Write("Invalid rquest!");
                    return;
                }

            }
            else
            {
                Response.Write("Invalid rquest!");
                return;
            }
        }
        catch
        {

        }

      

    }


    private DataTable BuildTable()
    {
        DataTable dt;
        DataColumn col;

        dt = new DataTable();
        col = new DataColumn();
        col.ColumnName = "Seqno";
        col.DataType = Type.GetType("System.String");

        dt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "Rectype";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);
        col = new DataColumn();

        col.ColumnName = "Contno";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);
        col = new DataColumn();

        col.ColumnName = "Custname";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);
        col = new DataColumn();

        col.ColumnName = "Preauthopta";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);
        col = new DataColumn();


        col.ColumnName = "Effdate";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);
        col = new DataColumn();

        col.ColumnName = "Termdate";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Schemenamea";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Deductamta";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Deductpcta";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Maxdeductlmta";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);


        col = new DataColumn();
        col.ColumnName = "Bedtypea";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Maternitya";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Dentallimita";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Opticala";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Opticallimita";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);


        col = new DataColumn();
        col.ColumnName = "Schemenameb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Deductamtb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Deductpctb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Maxdeductlmtb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Bedtypeb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Maternityb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Dentallimitb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Opticalb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Opticallimitb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Preauthoptb";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);



        col = new DataColumn();
        col.ColumnName = "Schemenamec";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Deductamtc";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Deductpctc";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Maxdeductlmtc";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Bedtypec";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Maternityc";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Dentallimitc";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Opticalc";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Opticallimitc";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Preauthoptc";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Schemenamed";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Deductamtd";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Deductpctd";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Maxdeductlmtd";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Bedtyped";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Maternityd";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Dentallimitd";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Opticald";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Opticallimitd";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Preauthoptd";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Schemename";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Spinstruct";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Firstinstructrec";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Lastinstructrec";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Crtuser";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Crtdate";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Spinstruct4";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

        col = new DataColumn();
        col.ColumnName = "Spinstruct5";
        col.DataType = Type.GetType("System.String");
        dt.Columns.Add(col);

       

        return dt;
    }


    private void GetData()
    {
        DataSet ds;
        DataTable dt;
        DataRow row;
       
       OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
       OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN datum;
       OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN response;
       datum = new OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN();
      
       //// New CCHI CR Need to passs Policy Number instead of Contract Number
        ////Start
        ////datum.contNo = strClientID; // "10319400";//10319400
		datum.policyNo = clientId;
        ///if (strClientID.Trim().Length > 8)
        //datum.policyNo = strClientID; // "103194001";//103194001      
        //else
        //datum.contNo = strClientID; // "10319400";//10319400
        ////End



        /// string str = Request["CYM"];
        string str = pr;
        // string[] strArray = str.Split('/');
        //str = DateTime.ParseExact(str, "YYYY/MM/DD", null).ToLongDateString();
        datum.contYymm = str;// strArray[2].Substring(0, 4) + strArray[1];
       datum.reportGroup = "AL";
       datum.provCode = Convert.ToString(Session["ProviderID"]);

       if (Session["Class_ID"] != null)
       {
           datum.classID = Session["Class_ID"].ToString();
       }


        datum.Username = WebPublication.CaesarSvcUsername;
        datum.Password = WebPublication.CaesarSvcPassword;
        datum.transactionID = TransactionManager.TransactionID();
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        response = ws.EnqTOBRpt(datum);

        //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN>();
        //XmlReq.Request(datum, "EnqTOBRptRequest_DN");

        //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN>();
        //XmlResp.Response(response, "EnqTOBRptResponse_DN");
        if (response.status == "0")
        {
            DataColumn rptCol;
            DataRow rptRow;

            //rptDT.Columns.Add("SEQ_NO", typeof(long));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SEQ_NO";
            rptCol.DataType = Type.GetType("System.Int64");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("REC_TYPE", typeof(long));
            rptCol = new DataColumn();
            rptCol.ColumnName = "REC_TYPE";
            rptCol.DataType = Type.GetType("System.Int64");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("CONT_NO", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "CONT_NO";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 9;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("CUST_NAME", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "CUST_NAME";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 70;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("EFF_DATE", typeof(DateTime));
            rptCol = new DataColumn();
            rptCol.ColumnName = "EFF_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("TERM_DATE", typeof(DateTime));
            rptCol = new DataColumn();
            rptCol.ColumnName = "TERM_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);

            //Extension Term Date
            rptCol = new DataColumn();
            rptCol.ColumnName = "EXT_TERM_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "CONT_TYPE";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("SCHEME_NAME_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_AMT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_PCT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MAX_DEDUCT_LMT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("BED_TYPE_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MATERNITY_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DENTAL_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            //rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("PRE_AUTH_OPT_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //2014-06-04 REF-1482 CR218 Nathan Start
            //rptDT.Columns.Add("HEART_VALVE_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("HEARING_AID_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ALZHEIMER_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("AUTISM_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DISABILITY_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);
            //2014-06-04 REF-1482 CR218 Nathan End


            //2014-06-17 REF-1493 CR222 Eric Shek, start
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            //2014-06-17 REF-1493 CR222 Eric Shek, End

            //rptDT.Columns.Add("SCHEME_NAME_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_AMT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_PCT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MAX_DEDUCT_LMT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("BED_TYPE_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MATERNITY_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DENTAL_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            //rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("PRE_AUTH_OPT_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //2014-06-04 REF-1482 CR218 Nathan Start
            //rptDT.Columns.Add("HEART_VALVE_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("HEARING_AID_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ALZHEIMER_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("AUTISM_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DISABILITY_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);
            //2014-06-04 REF-1482 CR218 Nathan End

            //rptDT.Columns.Add("SCHEME_NAME_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_AMT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_PCT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MAX_DEDUCT_LMT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("BED_TYPE_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MATERNITY_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DENTAL_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            //rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("PRE_AUTH_OPT_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //2014-06-04 REF-1482 CR218 Nathan Start
            //rptDT.Columns.Add("HEART_VALVE_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("HEARING_AID_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ALZHEIMER_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("AUTISM_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DISABILITY_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);
            //2014-06-04 REF-1482 CR218 Nathan End

            //rptDT.Columns.Add("SCHEME_NAME_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_AMT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_PCT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MAX_DEDUCT_LMT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("BED_TYPE_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MATERNITY_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DENTAL_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            //rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("PRE_AUTH_OPT_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //2014-06-04 REF-1482 CR218 Nathan Start
            //rptDT.Columns.Add("HEART_VALVE_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("HEARING_AID_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ALZHEIMER_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("AUTISM_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DISABILITY_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEW_CCHI_POLICY_IND", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEW_CCHI_POLICY_IND";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);
            //2014-06-04 REF-1482 CR218 Nathan End

            //rptDT.Columns.Add("sabic_cont_ind", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SABIC_CONT_IND";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("SCHEME_NAME", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("SP_INSTRUCT", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT1";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT2";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT3";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("FIRST_INSTRUCT_REC", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "FIRST_INSTRUCT_REC";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("LAST_INSTRUCT_REC", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "LAST_INSTRUCT_REC";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
 			rptDT.Columns.Add(rptCol);


			/// New CCHI CR Need to passs Policy Number instead of Contract Number
            ////Start
            rptCol = new DataColumn();
            rptCol.ColumnName = "POLICY_HOLDER";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 300;
            rptDT.Columns.Add(rptCol);
            ////End


            /// New Aramco description changes on 05-Feb-2018
            ////Start
            rptCol = new DataColumn();
            rptCol.ColumnName = "Spinstruct4";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "Spinstruct5";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10000;
            rptDT.Columns.Add(rptCol);
            ////End


         

            for (int i = 0; i < response.detail.Length; i++)
            {
                rptRow = rptDT.NewRow();
                rptRow["SEQ_NO"] = response.detail[i].Seqno;
                rptRow["REC_TYPE"] = response.detail[i].Rectype;
                rptRow["CONT_NO"] = response.detail[i].Contno;
                rptRow["CUST_NAME"] = response.detail[i].Custname;
                rptRow["EFF_DATE"] = response.detail[i].Effdate;
                rptRow["TERM_DATE"] = response.detail[i].Termdate;
                if (response.detail[i].ExtDate != null)
                {
                    rptRow["EXT_TERM_DATE"] = response.detail[i].ExtDate;
                }
                rptRow["SCHEME_NAME_A"] = response.detail[i].Schemenamea;
                rptRow["CONT_TYPE"] = response.detail[i].Conttype;
                rptRow["DEDUCT_AMT_A"] = response.detail[i].Deductamta;
                rptRow["DEDUCT_PCT_A"] = response.detail[i].Deductpcta;
                rptRow["MAX_DEDUCT_LMT_A"] = response.detail[i].Maxdeductlmta;
                rptRow["BED_TYPE_A"] = response.detail[i].Bedtypea;
                rptRow["MATERNITY_A"] = response.detail[i].Maternitya;
                rptRow["DENTAL_LIMIT_A"] = response.detail[i].Dentallimita;
                rptRow["OPTICAL_A"] = response.detail[i].Opticala;
                rptRow["OPTICAL_LIMIT_A"] = response.detail[i].Opticallimita;
                rptRow["PRE_AUTH_OPT_A"] = response.detail[i].Preauthopta;
                //2014-06-04 REF-1482 CR218 Nathan Start
                rptRow["HEART_VALVE_LIMIT_A"] = response.detail[i].HeartValvea;
                rptRow["ORGAN_TRANS_LIMIT_A"] = response.detail[i].OrganTransa;
                rptRow["HEARING_AID_LIMIT_A"] = response.detail[i].HearingAida;
                rptRow["NEWBORN_DIAG_LIMIT_A"] = response.detail[i].NewbornDiaga;
                rptRow["ALZHEIMER_LIMIT_A"] = response.detail[i].Alzheimera;
                rptRow["AUTISM_LIMIT_A"] = response.detail[i].Autisma;
                rptRow["DISABILITY_LIMIT_A"] = response.detail[i].Disabilitya;
                //2014-06-04 REF-1482 CR218 Nathan End


                rptRow["SCHEME_NAME_B"] = response.detail[i].Schemenameb;
                rptRow["DEDUCT_AMT_B"] = response.detail[i].Deductamtb;
                rptRow["DEDUCT_PCT_B"] = response.detail[i].Deductpctb;
                rptRow["MAX_DEDUCT_LMT_B"] = response.detail[i].Maxdeductlmtb;
                rptRow["BED_TYPE_B"] = response.detail[i].Bedtypeb;
                rptRow["MATERNITY_B"] = response.detail[i].Maternityb;
                rptRow["DENTAL_LIMIT_B"] = response.detail[i].Dentallimitb;
                rptRow["OPTICAL_B"] = response.detail[i].Opticalb;
                rptRow["OPTICAL_LIMIT_B"] = response.detail[i].Opticallimitb;
                rptRow["PRE_AUTH_OPT_B"] = response.detail[i].Preauthoptb;
                //2014-06-04 REF-1482 CR218 Nathan Start
                rptRow["HEART_VALVE_LIMIT_B"] = response.detail[i].HeartValveb;
                rptRow["ORGAN_TRANS_LIMIT_B"] = response.detail[i].OrganTransb;
                rptRow["HEARING_AID_LIMIT_B"] = response.detail[i].HearingAidb;
                rptRow["NEWBORN_DIAG_LIMIT_B"] = response.detail[i].NewbornDiagb;
                rptRow["ALZHEIMER_LIMIT_B"] = response.detail[i].Alzheimerb;
                rptRow["AUTISM_LIMIT_B"] = response.detail[i].Autismb;
                rptRow["DISABILITY_LIMIT_B"] = response.detail[i].Disabilityb;
                //2014-06-04 REF-1482 CR218 Nathan End
                rptRow["SCHEME_NAME_C"] = response.detail[i].Schemenamec;
                rptRow["DEDUCT_AMT_C"] = response.detail[i].Deductamtc;
                rptRow["DEDUCT_PCT_C"] = response.detail[i].Deductpctc;
                rptRow["MAX_DEDUCT_LMT_C"] = response.detail[i].Maxdeductlmtc;
                rptRow["BED_TYPE_C"] = response.detail[i].Bedtypec;
                rptRow["MATERNITY_C"] = response.detail[i].Maternityc;
                rptRow["DENTAL_LIMIT_C"] = response.detail[i].Dentallimitc;
                rptRow["OPTICAL_C"] = response.detail[i].Opticalc;
                rptRow["OPTICAL_LIMIT_C"] = response.detail[i].Opticallimitc;
                rptRow["PRE_AUTH_OPT_C"] = response.detail[i].Preauthoptc;
                //2014-06-04 REF-1482 CR218 Nathan Start
                rptRow["HEART_VALVE_LIMIT_C"] = response.detail[i].HeartValvec;
                rptRow["ORGAN_TRANS_LIMIT_C"] = response.detail[i].OrganTransc;
                rptRow["HEARING_AID_LIMIT_C"] = response.detail[i].HearingAidc;
                rptRow["NEWBORN_DIAG_LIMIT_C"] = response.detail[i].NewbornDiagc;
                rptRow["ALZHEIMER_LIMIT_C"] = response.detail[i].Alzheimerc;
                rptRow["AUTISM_LIMIT_C"] = response.detail[i].Autismc;
                rptRow["DISABILITY_LIMIT_C"] = response.detail[i].Disabilityc;
                //2014-06-04 REF-1482 CR218 Nathan End
                rptRow["SCHEME_NAME_D"] = response.detail[i].Schemenamed;
                rptRow["DEDUCT_AMT_D"] = response.detail[i].Deductamtd;
                rptRow["DEDUCT_PCT_D"] = response.detail[i].Deductpctd;
                rptRow["MAX_DEDUCT_LMT_D"] = response.detail[i].Maxdeductlmtd;
                rptRow["BED_TYPE_D"] = response.detail[i].Bedtyped;
                rptRow["MATERNITY_D"] = response.detail[i].Maternityd;
                rptRow["DENTAL_LIMIT_D"] = response.detail[i].Dentallimitd;
                rptRow["OPTICAL_D"] = response.detail[i].Opticald;
                rptRow["OPTICAL_LIMIT_D"] = response.detail[i].Opticallimitd;
                rptRow["PRE_AUTH_OPT_D"] = response.detail[i].Preauthoptd;
                //2014-06-04 REF-1482 CR218 Nathan Start
                rptRow["HEART_VALVE_LIMIT_D"] = response.detail[i].HeartValved;
                rptRow["ORGAN_TRANS_LIMIT_D"] = response.detail[i].OrganTransd;
                rptRow["HEARING_AID_LIMIT_D"] = response.detail[i].HearingAidd;
                rptRow["NEWBORN_DIAG_LIMIT_D"] = response.detail[i].NewbornDiagd;
                rptRow["ALZHEIMER_LIMIT_D"] = response.detail[i].Alzheimerd;
                rptRow["AUTISM_LIMIT_D"] = response.detail[i].Autismd;
                rptRow["DISABILITY_LIMIT_D"] = response.detail[i].Disabilityd;
                rptRow["NEW_CCHI_POLICY_IND"] = response.detail[i].NewCCHIPolicyInd;
                rptRow["SABIC_CONT_IND"] = response.detail[i].SABICContInd;
                //2014-06-04 REF-1482 CR218 Nathan End
                rptRow["SCHEME_NAME"] = response.detail[i].Schemename;
                rptRow["SP_INSTRUCT1"] = response.detail[i].Spinstruct1;
                rptRow["SP_INSTRUCT2"] = response.detail[i].Spinstruct2;
                rptRow["SP_INSTRUCT3"] = response.detail[i].Spinstruct3;
                rptRow["FIRST_INSTRUCT_REC"] = response.detail[i].Firstinstructrec;
                rptRow["LAST_INSTRUCT_REC"] = response.detail[i].Lastinstructrec;


                //2014-06-18 REF-1493 CR222 Eric Shek, Start
                rptRow["SCHEME_DISPLAY_IND_A"] = response.detail[i].SchemeDisplayInda;
                rptRow["MATERNITY_SPI_A"] = response.detail[i].MaternitySPIa;
                rptRow["DENTAL_SPI_A"] = response.detail[i].DentalSPIa;
                rptRow["OPTICAL_SPI_A"] = response.detail[i].OpticalSPIa;
                rptRow["HEART_VALVE_SPI_A"] = response.detail[i].HeartValveSPIa;
                rptRow["ORGAN_TRANS_SPI_A"] = response.detail[i].OrganTransSPIa;
                rptRow["HEARING_AID_SPI_A"] = response.detail[i].HearingAidSPIa;
                rptRow["NEWBORN_DIAG_SPI_A"] = response.detail[i].NewbornDiagSPIa;
                rptRow["ALZHEIMER_SPI_A"] = response.detail[i].AlzheimerSPIa;
                rptRow["AUTISM_SPI_A"] = response.detail[i].AutismSPIa;
                rptRow["DISABILITY_SPI_A"] = response.detail[i].DisabilitySPIa;
                rptRow["SCHEME_DISPLAY_IND_B"] = response.detail[i].SchemeDisplayIndb;
                rptRow["MATERNITY_SPI_B"] = response.detail[i].MaternitySPIb;
                rptRow["DENTAL_SPI_B"] = response.detail[i].DentalSPIb;
                rptRow["OPTICAL_SPI_B"] = response.detail[i].OpticalSPIb;
                rptRow["HEART_VALVE_SPI_B"] = response.detail[i].HeartValveSPIb;
                rptRow["ORGAN_TRANS_SPI_B"] = response.detail[i].OrganTransSPIb;
                rptRow["HEARING_AID_SPI_B"] = response.detail[i].HearingAidSPIb;
                rptRow["NEWBORN_DIAG_SPI_B"] = response.detail[i].NewbornDiagSPIb;
                rptRow["ALZHEIMER_SPI_B"] = response.detail[i].AlzheimerSPIb;
                rptRow["AUTISM_SPI_B"] = response.detail[i].AutismSPIb;
                rptRow["DISABILITY_SPI_B"] = response.detail[i].DisabilitySPIb;
                rptRow["SCHEME_DISPLAY_IND_C"] = response.detail[i].SchemeDisplayIndc;
                rptRow["MATERNITY_SPI_C"] = response.detail[i].MaternitySPIc;
                rptRow["DENTAL_SPI_C"] = response.detail[i].DentalSPIc;
                rptRow["OPTICAL_SPI_C"] = response.detail[i].OpticalSPIc;
                rptRow["HEART_VALVE_SPI_C"] = response.detail[i].HeartValveSPIc;
                rptRow["ORGAN_TRANS_SPI_C"] = response.detail[i].OrganTransSPIc;
                rptRow["HEARING_AID_SPI_C"] = response.detail[i].HearingAidSPIc;
                rptRow["NEWBORN_DIAG_SPI_C"] = response.detail[i].NewbornDiagSPIc;
                rptRow["ALZHEIMER_SPI_C"] = response.detail[i].AlzheimerSPIc;
                rptRow["AUTISM_SPI_C"] = response.detail[i].AutismSPIc;
                rptRow["DISABILITY_SPI_C"] = response.detail[i].DisabilitySPIc;
                rptRow["SCHEME_DISPLAY_IND_D"] = response.detail[i].SchemeDisplayIndd;
                rptRow["MATERNITY_SPI_D"] = response.detail[i].MaternitySPId;
                rptRow["DENTAL_SPI_D"] = response.detail[i].DentalSPId;
                rptRow["OPTICAL_SPI_D"] = response.detail[i].OpticalSPId;
                rptRow["HEART_VALVE_SPI_D"] = response.detail[i].HeartValveSPId;
                rptRow["ORGAN_TRANS_SPI_D"] = response.detail[i].OrganTransSPId;
                rptRow["HEARING_AID_SPI_D"] = response.detail[i].HearingAidSPId;
                rptRow["NEWBORN_DIAG_SPI_D"] = response.detail[i].NewbornDiagSPId;
                rptRow["ALZHEIMER_SPI_D"] = response.detail[i].AlzheimerSPId;
                rptRow["AUTISM_SPI_D"] = response.detail[i].AutismSPId;
                rptRow["DISABILITY_SPI_D"] = response.detail[i].DisabilitySPId;
                //2014-06-18 REF-1493 CR222 Eric Shek, End
 				
				/// New CCHI CR Need to passs Policy Number instead of Contract Number
                ////Start
                rptRow["POLICY_HOLDER"] = response.detail[i].PolicyHolder;
                ////End

                rptDT.Rows.Add(rptRow);

            }
            rptDS.Tables.Add(rptDT);

            //Response.Write(rptDT.Rows[0]["CONT_NO"].ToString() + "AAA" + "AAA");

            crDoc = new ReportDocument();
            crDoc.Load(MapPath(@"~\provider\MRCUSTTOB02.rpt"));
            crDoc.SetDataSource(rptDT);

            crDoc.SetParameterValue("pc_report_id", "MRCUSTTOB02");
            crDoc.SetParameterValue("pc_report_group", "AL");
            crDoc.SetParameterValue("pv_company_name", "dd");
            crDoc.SetParameterValue("pd_report_date", "29/01/2013");
            crDoc.SetParameterValue("pv_title1", "ddd");
            crDoc.SetParameterValue("pc_rpt_grp", "AL");
            crDoc.SetParameterValue("pc_cont_no", ls_cont_no);
            crDoc.SetParameterValue("pc_cont_yymm", ls_cont_yymm);

            CrystalReportViewer1.ReportSource = crDoc;
            Session["rptDoc5"] = crDoc;
            //crViewer.RefreshReport();
        }
        else
        {
            Response.Write(response.errorMessage + "<br>");
        }
    }

    private OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN GetIndicator()
    {
        
        OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
        OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN datum;
        OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN response;
        try
        {
            datum = new OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN();
            datum.policyNo = clientId;
            string str =cym;
            datum.contYymm = str;
            datum.reportGroup = "AL";
            datum.provCode = Convert.ToString(Session["ProviderID"]);

            if (Session["Class_ID"] != null)
            {
                datum.classID = Session["Class_ID"].ToString();
            }


            datum.Username = WebPublication.CaesarSvcUsername;
            datum.Password = WebPublication.CaesarSvcPassword;
            datum.transactionID = TransactionManager.TransactionID();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            return response = ws.EnqTOBRpt(datum);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN>();
            //XmlReq.Request(datum, "EnqTOBRptResponse_DN");

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN>();
            //XmlResp.Response(response, "EnqTOBRptResponse_DN");


        }
        catch (Exception)
        {
            throw;
           
        }
    }

    private void GetDataOldCCHIReport(OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN response)
    {
        try
        {
          
                DataColumn rptCol;
                DataRow rptRow;

                //rptDT.Columns.Add("SEQ_NO", typeof(long));
                rptCol = new DataColumn();
                rptCol.ColumnName = "SEQ_NO";
                rptCol.DataType = Type.GetType("System.Int64");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("REC_TYPE", typeof(long));
                rptCol = new DataColumn();
                rptCol.ColumnName = "REC_TYPE";
                rptCol.DataType = Type.GetType("System.Int64");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("CONT_NO", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "CONT_NO";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 9;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("CUST_NAME", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "CUST_NAME";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 70;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("EFF_DATE", typeof(DateTime));
                rptCol = new DataColumn();
                rptCol.ColumnName = "EFF_DATE";
                rptCol.DataType = Type.GetType("System.DateTime");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("TERM_DATE", typeof(DateTime));
                rptCol = new DataColumn();
                rptCol.ColumnName = "TERM_DATE";
                rptCol.DataType = Type.GetType("System.DateTime");
                rptDT.Columns.Add(rptCol);

                //Extension Term Date
                rptCol = new DataColumn();
                rptCol.ColumnName = "EXT_TERM_DATE";
                rptCol.DataType = Type.GetType("System.DateTime");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "CONT_TYPE";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 10;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("SCHEME_NAME_A", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_NAME_A";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 100;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DEDUCT_AMT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DEDUCT_AMT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DEDUCT_PCT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DEDUCT_PCT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("MAX_DEDUCT_LMT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "MAX_DEDUCT_LMT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("BED_TYPE_A", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "BED_TYPE_A";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 200;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("MATERNITY_A", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "MATERNITY_A";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 20;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DENTAL_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DENTAL_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("OPTICAL_A", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                //rptCol.MaxLength = 20;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("OPTICAL_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("PRE_AUTH_OPT_A", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "PRE_AUTH_OPT_A";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 200;
                rptDT.Columns.Add(rptCol);

                //2014-06-04 REF-1482 CR218 Nathan Start
                //rptDT.Columns.Add("HEART_VALVE_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "HEART_VALVE_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "ORGAN_TRANS_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("HEARING_AID_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "HEARING_AID_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("ALZHEIMER_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "ALZHEIMER_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("AUTISM_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "AUTISM_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DISABILITY_LIMIT_A", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DISABILITY_LIMIT_A";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);
                //2014-06-04 REF-1482 CR218 Nathan End


                //2014-06-17 REF-1493 CR222 Eric Shek, start
                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_DISPLAY_IND_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);


                rptCol = new DataColumn();
                rptCol.ColumnName = "MATERNITY_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "DENTAL_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "HEART_VALVE_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "ORGAN_TRANS_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "HEARING_AID_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "NEWBORN_DIAG_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "ALZHEIMER_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "AUTISM_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "DISABILITY_SPI_A";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);


                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_DISPLAY_IND_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "MATERNITY_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "DENTAL_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);


                rptCol = new DataColumn();
                rptCol.ColumnName = "HEART_VALVE_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "ORGAN_TRANS_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "HEARING_AID_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "NEWBORN_DIAG_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "ALZHEIMER_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "AUTISM_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "DISABILITY_SPI_B";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_DISPLAY_IND_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);


                rptCol = new DataColumn();
                rptCol.ColumnName = "MATERNITY_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "DENTAL_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);


                rptCol = new DataColumn();
                rptCol.ColumnName = "HEART_VALVE_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);


                rptCol = new DataColumn();
                rptCol.ColumnName = "ORGAN_TRANS_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "HEARING_AID_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);


                rptCol = new DataColumn();
                rptCol.ColumnName = "NEWBORN_DIAG_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "ALZHEIMER_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "AUTISM_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "DISABILITY_SPI_C";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_DISPLAY_IND_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "MATERNITY_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "DENTAL_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "HEART_VALVE_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "ORGAN_TRANS_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "HEARING_AID_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "NEWBORN_DIAG_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "ALZHEIMER_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "AUTISM_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "DISABILITY_SPI_D";
                rptCol.DataType = Type.GetType("System.String");
                rptDT.Columns.Add(rptCol);

                //2014-06-17 REF-1493 CR222 Eric Shek, End

                //rptDT.Columns.Add("SCHEME_NAME_B", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_NAME_B";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 100;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DEDUCT_AMT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DEDUCT_AMT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DEDUCT_PCT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DEDUCT_PCT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("MAX_DEDUCT_LMT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "MAX_DEDUCT_LMT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("BED_TYPE_B", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "BED_TYPE_B";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 200;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("MATERNITY_B", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "MATERNITY_B";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 20;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DENTAL_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DENTAL_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("OPTICAL_B", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                //rptCol.MaxLength = 20;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("OPTICAL_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("PRE_AUTH_OPT_B", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "PRE_AUTH_OPT_B";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 200;
                rptDT.Columns.Add(rptCol);

                //2014-06-04 REF-1482 CR218 Nathan Start
                //rptDT.Columns.Add("HEART_VALVE_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "HEART_VALVE_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "ORGAN_TRANS_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("HEARING_AID_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "HEARING_AID_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("ALZHEIMER_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "ALZHEIMER_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("AUTISM_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "AUTISM_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DISABILITY_LIMIT_B", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DISABILITY_LIMIT_B";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);
                //2014-06-04 REF-1482 CR218 Nathan End

                //rptDT.Columns.Add("SCHEME_NAME_C", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_NAME_C";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 100;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DEDUCT_AMT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DEDUCT_AMT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DEDUCT_PCT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DEDUCT_PCT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("MAX_DEDUCT_LMT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "MAX_DEDUCT_LMT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("BED_TYPE_C", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "BED_TYPE_C";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 200;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("MATERNITY_C", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "MATERNITY_C";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 20;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DENTAL_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DENTAL_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("OPTICAL_C", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                //rptCol.MaxLength = 20;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("OPTICAL_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("PRE_AUTH_OPT_C", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "PRE_AUTH_OPT_C";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 200;
                rptDT.Columns.Add(rptCol);

                //2014-06-04 REF-1482 CR218 Nathan Start
                //rptDT.Columns.Add("HEART_VALVE_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "HEART_VALVE_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "ORGAN_TRANS_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("HEARING_AID_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "HEARING_AID_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("ALZHEIMER_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "ALZHEIMER_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("AUTISM_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "AUTISM_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DISABILITY_LIMIT_C", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DISABILITY_LIMIT_C";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);
                //2014-06-04 REF-1482 CR218 Nathan End

                //rptDT.Columns.Add("SCHEME_NAME_D", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_NAME_D";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 100;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DEDUCT_AMT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DEDUCT_AMT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DEDUCT_PCT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DEDUCT_PCT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("MAX_DEDUCT_LMT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "MAX_DEDUCT_LMT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("BED_TYPE_D", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "BED_TYPE_D";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 200;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("MATERNITY_D", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "MATERNITY_D";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 20;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DENTAL_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DENTAL_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("OPTICAL_D", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                //rptCol.MaxLength = 20;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("OPTICAL_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "OPTICAL_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("PRE_AUTH_OPT_D", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "PRE_AUTH_OPT_D";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 200;
                rptDT.Columns.Add(rptCol);

                //2014-06-04 REF-1482 CR218 Nathan Start
                //rptDT.Columns.Add("HEART_VALVE_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "HEART_VALVE_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "ORGAN_TRANS_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("HEARING_AID_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "HEARING_AID_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("ALZHEIMER_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "ALZHEIMER_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("AUTISM_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "AUTISM_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("DISABILITY_LIMIT_D", typeof(double));
                rptCol = new DataColumn();
                rptCol.ColumnName = "DISABILITY_LIMIT_D";
                rptCol.DataType = Type.GetType("System.Decimal");
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("NEW_CCHI_POLICY_IND", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "NEW_CCHI_POLICY_IND";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 1;
                rptDT.Columns.Add(rptCol);
                //2014-06-04 REF-1482 CR218 Nathan End

                //rptDT.Columns.Add("sabic_cont_ind", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "SABIC_CONT_IND";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 1;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("SCHEME_NAME", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "SCHEME_NAME";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 100;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("SP_INSTRUCT", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "SP_INSTRUCT1";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 4000;
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "SP_INSTRUCT2";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 4000;
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "SP_INSTRUCT3";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 4000;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("FIRST_INSTRUCT_REC", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "FIRST_INSTRUCT_REC";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 1;
                rptDT.Columns.Add(rptCol);

                //rptDT.Columns.Add("LAST_INSTRUCT_REC", typeof(String));
                rptCol = new DataColumn();
                rptCol.ColumnName = "LAST_INSTRUCT_REC";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 1;
                rptDT.Columns.Add(rptCol);


                /// New CCHI CR Need to passs Policy Number instead of Contract Number
                ////Start
                rptCol = new DataColumn();
                rptCol.ColumnName = "POLICY_HOLDER";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 300;
                rptDT.Columns.Add(rptCol);
                ////End


                /// New Aramco description changes on 05-Feb-2018
                ////Start
                rptCol = new DataColumn();
                rptCol.ColumnName = "Spinstruct4";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 10000;
                rptDT.Columns.Add(rptCol);

                rptCol = new DataColumn();
                rptCol.ColumnName = "Spinstruct5";
                rptCol.DataType = Type.GetType("System.String");
                rptCol.MaxLength = 10000;
                rptDT.Columns.Add(rptCol);
                ////End




                for (int i = 0; i < response.detail.Length; i++)
                {
                    rptRow = rptDT.NewRow();
                    rptRow["SEQ_NO"] = response.detail[i].Seqno;
                    rptRow["REC_TYPE"] = response.detail[i].Rectype;
                    rptRow["CONT_NO"] = response.detail[i].Contno;
                    rptRow["CUST_NAME"] = response.detail[i].Custname;
                    rptRow["EFF_DATE"] = response.detail[i].Effdate;
                    rptRow["TERM_DATE"] = response.detail[i].Termdate;
                    if (response.detail[i].ExtDate != null)
                    {
                        rptRow["EXT_TERM_DATE"] = response.detail[i].ExtDate;
                    }
                    rptRow["SCHEME_NAME_A"] = response.detail[i].Schemenamea;
                    rptRow["CONT_TYPE"] = response.detail[i].Conttype;
                    rptRow["DEDUCT_AMT_A"] = response.detail[i].Deductamta;
                    rptRow["DEDUCT_PCT_A"] = response.detail[i].Deductpcta;
                    rptRow["MAX_DEDUCT_LMT_A"] = response.detail[i].Maxdeductlmta;
                    rptRow["BED_TYPE_A"] = response.detail[i].Bedtypea;
                    rptRow["MATERNITY_A"] = response.detail[i].Maternitya;
                    rptRow["DENTAL_LIMIT_A"] = response.detail[i].Dentallimita;
                    rptRow["OPTICAL_A"] = response.detail[i].Opticala;
                    rptRow["OPTICAL_LIMIT_A"] = response.detail[i].Opticallimita;
                    rptRow["PRE_AUTH_OPT_A"] = response.detail[i].Preauthopta;
                    //2014-06-04 REF-1482 CR218 Nathan Start
                    rptRow["HEART_VALVE_LIMIT_A"] = response.detail[i].HeartValvea;
                    rptRow["ORGAN_TRANS_LIMIT_A"] = response.detail[i].OrganTransa;
                    rptRow["HEARING_AID_LIMIT_A"] = response.detail[i].HearingAida;
                    rptRow["NEWBORN_DIAG_LIMIT_A"] = response.detail[i].NewbornDiaga;
                    rptRow["ALZHEIMER_LIMIT_A"] = response.detail[i].Alzheimera;
                    rptRow["AUTISM_LIMIT_A"] = response.detail[i].Autisma;
                    rptRow["DISABILITY_LIMIT_A"] = response.detail[i].Disabilitya;
                    //2014-06-04 REF-1482 CR218 Nathan End


                    rptRow["SCHEME_NAME_B"] = response.detail[i].Schemenameb;
                    rptRow["DEDUCT_AMT_B"] = response.detail[i].Deductamtb;
                    rptRow["DEDUCT_PCT_B"] = response.detail[i].Deductpctb;
                    rptRow["MAX_DEDUCT_LMT_B"] = response.detail[i].Maxdeductlmtb;
                    rptRow["BED_TYPE_B"] = response.detail[i].Bedtypeb;
                    rptRow["MATERNITY_B"] = response.detail[i].Maternityb;
                    rptRow["DENTAL_LIMIT_B"] = response.detail[i].Dentallimitb;
                    rptRow["OPTICAL_B"] = response.detail[i].Opticalb;
                    rptRow["OPTICAL_LIMIT_B"] = response.detail[i].Opticallimitb;
                    rptRow["PRE_AUTH_OPT_B"] = response.detail[i].Preauthoptb;
                    //2014-06-04 REF-1482 CR218 Nathan Start
                    rptRow["HEART_VALVE_LIMIT_B"] = response.detail[i].HeartValveb;
                    rptRow["ORGAN_TRANS_LIMIT_B"] = response.detail[i].OrganTransb;
                    rptRow["HEARING_AID_LIMIT_B"] = response.detail[i].HearingAidb;
                    rptRow["NEWBORN_DIAG_LIMIT_B"] = response.detail[i].NewbornDiagb;
                    rptRow["ALZHEIMER_LIMIT_B"] = response.detail[i].Alzheimerb;
                    rptRow["AUTISM_LIMIT_B"] = response.detail[i].Autismb;
                    rptRow["DISABILITY_LIMIT_B"] = response.detail[i].Disabilityb;
                    //2014-06-04 REF-1482 CR218 Nathan End
                    rptRow["SCHEME_NAME_C"] = response.detail[i].Schemenamec;
                    rptRow["DEDUCT_AMT_C"] = response.detail[i].Deductamtc;
                    rptRow["DEDUCT_PCT_C"] = response.detail[i].Deductpctc;
                    rptRow["MAX_DEDUCT_LMT_C"] = response.detail[i].Maxdeductlmtc;
                    rptRow["BED_TYPE_C"] = response.detail[i].Bedtypec;
                    rptRow["MATERNITY_C"] = response.detail[i].Maternityc;
                    rptRow["DENTAL_LIMIT_C"] = response.detail[i].Dentallimitc;
                    rptRow["OPTICAL_C"] = response.detail[i].Opticalc;
                    rptRow["OPTICAL_LIMIT_C"] = response.detail[i].Opticallimitc;
                    rptRow["PRE_AUTH_OPT_C"] = response.detail[i].Preauthoptc;
                    //2014-06-04 REF-1482 CR218 Nathan Start
                    rptRow["HEART_VALVE_LIMIT_C"] = response.detail[i].HeartValvec;
                    rptRow["ORGAN_TRANS_LIMIT_C"] = response.detail[i].OrganTransc;
                    rptRow["HEARING_AID_LIMIT_C"] = response.detail[i].HearingAidc;
                    rptRow["NEWBORN_DIAG_LIMIT_C"] = response.detail[i].NewbornDiagc;
                    rptRow["ALZHEIMER_LIMIT_C"] = response.detail[i].Alzheimerc;
                    rptRow["AUTISM_LIMIT_C"] = response.detail[i].Autismc;
                    rptRow["DISABILITY_LIMIT_C"] = response.detail[i].Disabilityc;
                    //2014-06-04 REF-1482 CR218 Nathan End
                    rptRow["SCHEME_NAME_D"] = response.detail[i].Schemenamed;
                    rptRow["DEDUCT_AMT_D"] = response.detail[i].Deductamtd;
                    rptRow["DEDUCT_PCT_D"] = response.detail[i].Deductpctd;
                    rptRow["MAX_DEDUCT_LMT_D"] = response.detail[i].Maxdeductlmtd;
                    rptRow["BED_TYPE_D"] = response.detail[i].Bedtyped;
                    rptRow["MATERNITY_D"] = response.detail[i].Maternityd;
                    rptRow["DENTAL_LIMIT_D"] = response.detail[i].Dentallimitd;
                    rptRow["OPTICAL_D"] = response.detail[i].Opticald;
                    rptRow["OPTICAL_LIMIT_D"] = response.detail[i].Opticallimitd;
                    rptRow["PRE_AUTH_OPT_D"] = response.detail[i].Preauthoptd;
                    //2014-06-04 REF-1482 CR218 Nathan Start
                    rptRow["HEART_VALVE_LIMIT_D"] = response.detail[i].HeartValved;
                    rptRow["ORGAN_TRANS_LIMIT_D"] = response.detail[i].OrganTransd;
                    rptRow["HEARING_AID_LIMIT_D"] = response.detail[i].HearingAidd;
                    rptRow["NEWBORN_DIAG_LIMIT_D"] = response.detail[i].NewbornDiagd;
                    rptRow["ALZHEIMER_LIMIT_D"] = response.detail[i].Alzheimerd;
                    rptRow["AUTISM_LIMIT_D"] = response.detail[i].Autismd;
                    rptRow["DISABILITY_LIMIT_D"] = response.detail[i].Disabilityd;
                    rptRow["NEW_CCHI_POLICY_IND"] = response.detail[i].NewCCHIPolicyInd;
                    rptRow["SABIC_CONT_IND"] = response.detail[i].SABICContInd;
                    //2014-06-04 REF-1482 CR218 Nathan End
                    rptRow["SCHEME_NAME"] = response.detail[i].Schemename;
                    rptRow["SP_INSTRUCT1"] = response.detail[i].Spinstruct1;
                    rptRow["SP_INSTRUCT2"] = response.detail[i].Spinstruct2;
                    rptRow["SP_INSTRUCT3"] = response.detail[i].Spinstruct3;
                    rptRow["FIRST_INSTRUCT_REC"] = response.detail[i].Firstinstructrec;
                    rptRow["LAST_INSTRUCT_REC"] = response.detail[i].Lastinstructrec;


                    //2014-06-18 REF-1493 CR222 Eric Shek, Start
                    rptRow["SCHEME_DISPLAY_IND_A"] = response.detail[i].SchemeDisplayInda;
                    rptRow["MATERNITY_SPI_A"] = response.detail[i].MaternitySPIa;
                    rptRow["DENTAL_SPI_A"] = response.detail[i].DentalSPIa;
                    rptRow["OPTICAL_SPI_A"] = response.detail[i].OpticalSPIa;
                    rptRow["HEART_VALVE_SPI_A"] = response.detail[i].HeartValveSPIa;
                    rptRow["ORGAN_TRANS_SPI_A"] = response.detail[i].OrganTransSPIa;
                    rptRow["HEARING_AID_SPI_A"] = response.detail[i].HearingAidSPIa;
                    rptRow["NEWBORN_DIAG_SPI_A"] = response.detail[i].NewbornDiagSPIa;
                    rptRow["ALZHEIMER_SPI_A"] = response.detail[i].AlzheimerSPIa;
                    rptRow["AUTISM_SPI_A"] = response.detail[i].AutismSPIa;
                    rptRow["DISABILITY_SPI_A"] = response.detail[i].DisabilitySPIa;
                    rptRow["SCHEME_DISPLAY_IND_B"] = response.detail[i].SchemeDisplayIndb;
                    rptRow["MATERNITY_SPI_B"] = response.detail[i].MaternitySPIb;
                    rptRow["DENTAL_SPI_B"] = response.detail[i].DentalSPIb;
                    rptRow["OPTICAL_SPI_B"] = response.detail[i].OpticalSPIb;
                    rptRow["HEART_VALVE_SPI_B"] = response.detail[i].HeartValveSPIb;
                    rptRow["ORGAN_TRANS_SPI_B"] = response.detail[i].OrganTransSPIb;
                    rptRow["HEARING_AID_SPI_B"] = response.detail[i].HearingAidSPIb;
                    rptRow["NEWBORN_DIAG_SPI_B"] = response.detail[i].NewbornDiagSPIb;
                    rptRow["ALZHEIMER_SPI_B"] = response.detail[i].AlzheimerSPIb;
                    rptRow["AUTISM_SPI_B"] = response.detail[i].AutismSPIb;
                    rptRow["DISABILITY_SPI_B"] = response.detail[i].DisabilitySPIb;
                    rptRow["SCHEME_DISPLAY_IND_C"] = response.detail[i].SchemeDisplayIndc;
                    rptRow["MATERNITY_SPI_C"] = response.detail[i].MaternitySPIc;
                    rptRow["DENTAL_SPI_C"] = response.detail[i].DentalSPIc;
                    rptRow["OPTICAL_SPI_C"] = response.detail[i].OpticalSPIc;
                    rptRow["HEART_VALVE_SPI_C"] = response.detail[i].HeartValveSPIc;
                    rptRow["ORGAN_TRANS_SPI_C"] = response.detail[i].OrganTransSPIc;
                    rptRow["HEARING_AID_SPI_C"] = response.detail[i].HearingAidSPIc;
                    rptRow["NEWBORN_DIAG_SPI_C"] = response.detail[i].NewbornDiagSPIc;
                    rptRow["ALZHEIMER_SPI_C"] = response.detail[i].AlzheimerSPIc;
                    rptRow["AUTISM_SPI_C"] = response.detail[i].AutismSPIc;
                    rptRow["DISABILITY_SPI_C"] = response.detail[i].DisabilitySPIc;
                    rptRow["SCHEME_DISPLAY_IND_D"] = response.detail[i].SchemeDisplayIndd;
                    rptRow["MATERNITY_SPI_D"] = response.detail[i].MaternitySPId;
                    rptRow["DENTAL_SPI_D"] = response.detail[i].DentalSPId;
                    rptRow["OPTICAL_SPI_D"] = response.detail[i].OpticalSPId;
                    rptRow["HEART_VALVE_SPI_D"] = response.detail[i].HeartValveSPId;
                    rptRow["ORGAN_TRANS_SPI_D"] = response.detail[i].OrganTransSPId;
                    rptRow["HEARING_AID_SPI_D"] = response.detail[i].HearingAidSPId;
                    rptRow["NEWBORN_DIAG_SPI_D"] = response.detail[i].NewbornDiagSPId;
                    rptRow["ALZHEIMER_SPI_D"] = response.detail[i].AlzheimerSPId;
                    rptRow["AUTISM_SPI_D"] = response.detail[i].AutismSPId;
                    rptRow["DISABILITY_SPI_D"] = response.detail[i].DisabilitySPId;
                    //2014-06-18 REF-1493 CR222 Eric Shek, End

                    /// New CCHI CR Need to passs Policy Number instead of Contract Number
                    ////Start
                    rptRow["POLICY_HOLDER"] = response.detail[i].PolicyHolder;
                    ////End

                    rptDT.Rows.Add(rptRow);

                }
                rptDS.Tables.Add(rptDT);

                //Response.Write(rptDT.Rows[0]["CONT_NO"].ToString() + "AAA" + "AAA");

                crDoc = new ReportDocument();
                crDoc.Load(MapPath(@"~\provider\MRCUSTTOB02.rpt"));
                crDoc.SetDataSource(rptDT);

                crDoc.SetParameterValue("pc_report_id", "MRCUSTTOB02");
                crDoc.SetParameterValue("pc_report_group", "AL");
                crDoc.SetParameterValue("pv_company_name", "dd");
                crDoc.SetParameterValue("pd_report_date", "29/01/2013");
                crDoc.SetParameterValue("pv_title1", "ddd");
                crDoc.SetParameterValue("pc_rpt_grp", "AL");
                crDoc.SetParameterValue("pc_cont_no", ls_cont_no);
                crDoc.SetParameterValue("pc_cont_yymm", ls_cont_yymm);

                CrystalReportViewer1.ReportSource = crDoc;
                Session["rptDoc5"] = crDoc;
               
           

        }
        catch (Exception)
        {

            throw;
        }
    }

    private void GetDataNewCCHIReport(OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN response)
    {
        try
        {

            DataColumn rptCol;
            DataRow rptRow;

            rptCol = new DataColumn();
            rptCol.ColumnName = "SEQ_NO";
            rptCol.DataType = Type.GetType("System.Int64");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "REC_TYPE";
            rptCol.DataType = Type.GetType("System.Int64");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "CONT_NO";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 9;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "CUST_NAME";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 70;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "EFF_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "TERM_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "EXT_TERM_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "CONT_TYPE";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEW_CCHI_POLICY_IND";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SABIC_CONT_IND";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT1";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT2";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT3";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "FIRST_INSTRUCT_REC";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "LAST_INSTRUCT_REC";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "POLICY_HOLDER";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 300;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "Spinstruct4";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "Spinstruct5";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10000;
            rptDT.Columns.Add(rptCol);

            ////CR 414 TOB Report Field Satrt by Sakthi on 10-Jun-2018
            ////Satrt
            rptCol = new DataColumn();
            rptCol.ColumnName = "CCHI_CAT_POLICY_IND";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OVERALL_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MPN_DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MPN_DEDUCT_PCT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_MPN_DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HOSP_DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HOSP_DEDUCT_PCT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_HOSP_DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "POLYCLIN_DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "POLYCLIN_DEDUCT_PCT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_POLYCLIN_DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NET_TYPE_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "CIRCUM_MALE_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");           
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "EAR_PIRE_NB_F_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "PREMAT_BABY_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "VACC_MOH_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "DIALYSIS_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "PSYCHIATRIC_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "NON_ACUTE_PSY_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "META_SCREEN_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "GS_BMI_OVER_45_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_LMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OVERALL_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MPN_DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MPN_DEDUCT_PCT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_MPN_DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HOSP_DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HOSP_DEDUCT_PCT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_HOSP_DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "POLYCLIN_DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "POLYCLIN_DEDUCT_PCT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_POLYCLIN_DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NET_TYPE_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "CIRCUM_MALE_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "EAR_PIRE_NB_F_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "PREMAT_BABY_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "VACC_MOH_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DIALYSIS_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "PSYCHIATRIC_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NON_ACUTE_PSY_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "META_SCREEN_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "GS_BMI_OVER_45_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_LMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OVERALL_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MPN_DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MPN_DEDUCT_PCT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_MPN_DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HOSP_DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HOSP_DEDUCT_PCT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_HOSP_DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "POLYCLIN_DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "POLYCLIN_DEDUCT_PCT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_POLYCLIN_DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "NET_TYPE_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "CIRCUM_MALE_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "EAR_PIRE_NB_F_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "PREMAT_BABY_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "VACC_MOH_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DIALYSIS_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "PSYCHIATRIC_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NON_ACUTE_PSY_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "META_SCREEN_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "GS_BMI_OVER_45_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_LMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OVERALL_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MPN_DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MPN_DEDUCT_PCT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_MPN_DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HOSP_DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HOSP_DEDUCT_PCT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_HOSP_DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "POLYCLIN_DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "POLYCLIN_DEDUCT_PCT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_POLYCLIN_DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NET_TYPE_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "CIRCUM_MALE_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "EAR_PIRE_NB_F_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "PREMAT_BABY_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "VACC_MOH_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DIALYSIS_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "PSYCHIATRIC_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NON_ACUTE_PSY_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "META_SCREEN_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "GS_BMI_OVER_45_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_LMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            ////End
            for (int i = 0; i < response.detail.Length; i++)
            {
                rptRow = rptDT.NewRow();

                rptRow["SEQ_NO"] = response.detail[i].Seqno;
                rptRow["REC_TYPE"] = response.detail[i].Rectype;
                rptRow["CONT_NO"] = response.detail[i].Contno;
                rptRow["CUST_NAME"] = response.detail[i].Custname;
                rptRow["EFF_DATE"] = response.detail[i].Effdate;
                rptRow["TERM_DATE"] = response.detail[i].Termdate;
                if (response.detail[i].ExtDate != null)
                {
                    rptRow["EXT_TERM_DATE"] = response.detail[i].ExtDate;
                }
                rptRow["SCHEME_NAME_A"] = response.detail[i].Schemenamea;
                rptRow["CONT_TYPE"] = response.detail[i].Conttype;
                rptRow["DEDUCT_AMT_A"] = response.detail[i].Deductamta;
                rptRow["DEDUCT_PCT_A"] = response.detail[i].Deductpcta;
                rptRow["MAX_DEDUCT_LMT_A"] = response.detail[i].Maxdeductlmta;
                rptRow["BED_TYPE_A"] = response.detail[i].Bedtypea;
                rptRow["MATERNITY_A"] = response.detail[i].Maternitya;
                rptRow["DENTAL_LIMIT_A"] = response.detail[i].Dentallimita;
                rptRow["OPTICAL_A"] = response.detail[i].Opticala;
                rptRow["OPTICAL_LIMIT_A"] = response.detail[i].Opticallimita;
                rptRow["PRE_AUTH_OPT_A"] = response.detail[i].Preauthopta;               
                rptRow["HEART_VALVE_LIMIT_A"] = response.detail[i].HeartValvea;
                rptRow["ORGAN_TRANS_LIMIT_A"] = response.detail[i].OrganTransa;
                rptRow["HEARING_AID_LIMIT_A"] = response.detail[i].HearingAida;
                rptRow["NEWBORN_DIAG_LIMIT_A"] = response.detail[i].NewbornDiaga;
                rptRow["ALZHEIMER_LIMIT_A"] = response.detail[i].Alzheimera;
                rptRow["AUTISM_LIMIT_A"] = response.detail[i].Autisma;
                rptRow["DISABILITY_LIMIT_A"] = response.detail[i].Disabilitya;
                rptRow["SCHEME_NAME_B"] = response.detail[i].Schemenameb;
                rptRow["DEDUCT_AMT_B"] = response.detail[i].Deductamtb;
                rptRow["DEDUCT_PCT_B"] = response.detail[i].Deductpctb;
                rptRow["MAX_DEDUCT_LMT_B"] = response.detail[i].Maxdeductlmtb;
                rptRow["BED_TYPE_B"] = response.detail[i].Bedtypeb;
                rptRow["MATERNITY_B"] = response.detail[i].Maternityb;
                rptRow["DENTAL_LIMIT_B"] = response.detail[i].Dentallimitb;
                rptRow["OPTICAL_B"] = response.detail[i].Opticalb;
                rptRow["OPTICAL_LIMIT_B"] = response.detail[i].Opticallimitb;
                rptRow["PRE_AUTH_OPT_B"] = response.detail[i].Preauthoptb;               
                rptRow["HEART_VALVE_LIMIT_B"] = response.detail[i].HeartValveb;
                rptRow["ORGAN_TRANS_LIMIT_B"] = response.detail[i].OrganTransb;
                rptRow["HEARING_AID_LIMIT_B"] = response.detail[i].HearingAidb;
                rptRow["NEWBORN_DIAG_LIMIT_B"] = response.detail[i].NewbornDiagb;
                rptRow["ALZHEIMER_LIMIT_B"] = response.detail[i].Alzheimerb;
                rptRow["AUTISM_LIMIT_B"] = response.detail[i].Autismb;
                rptRow["DISABILITY_LIMIT_B"] = response.detail[i].Disabilityb;              
                rptRow["SCHEME_NAME_C"] = response.detail[i].Schemenamec;
                rptRow["DEDUCT_AMT_C"] = response.detail[i].Deductamtc;
                rptRow["DEDUCT_PCT_C"] = response.detail[i].Deductpctc;
                rptRow["MAX_DEDUCT_LMT_C"] = response.detail[i].Maxdeductlmtc;
                rptRow["BED_TYPE_C"] = response.detail[i].Bedtypec;
                rptRow["MATERNITY_C"] = response.detail[i].Maternityc;
                rptRow["DENTAL_LIMIT_C"] = response.detail[i].Dentallimitc;
                rptRow["OPTICAL_C"] = response.detail[i].Opticalc;
                rptRow["OPTICAL_LIMIT_C"] = response.detail[i].Opticallimitc;
                rptRow["PRE_AUTH_OPT_C"] = response.detail[i].Preauthoptc;                
                rptRow["HEART_VALVE_LIMIT_C"] = response.detail[i].HeartValvec;
                rptRow["ORGAN_TRANS_LIMIT_C"] = response.detail[i].OrganTransc;
                rptRow["HEARING_AID_LIMIT_C"] = response.detail[i].HearingAidc;
                rptRow["NEWBORN_DIAG_LIMIT_C"] = response.detail[i].NewbornDiagc;
                rptRow["ALZHEIMER_LIMIT_C"] = response.detail[i].Alzheimerc;
                rptRow["AUTISM_LIMIT_C"] = response.detail[i].Autismc;
                rptRow["DISABILITY_LIMIT_C"] = response.detail[i].Disabilityc;             
                rptRow["SCHEME_NAME_D"] = response.detail[i].Schemenamed;
                rptRow["DEDUCT_AMT_D"] = response.detail[i].Deductamtd;
                rptRow["DEDUCT_PCT_D"] = response.detail[i].Deductpctd;
                rptRow["MAX_DEDUCT_LMT_D"] = response.detail[i].Maxdeductlmtd;
                rptRow["BED_TYPE_D"] = response.detail[i].Bedtyped;
                rptRow["MATERNITY_D"] = response.detail[i].Maternityd;
                rptRow["DENTAL_LIMIT_D"] = response.detail[i].Dentallimitd;
                rptRow["OPTICAL_D"] = response.detail[i].Opticald;
                rptRow["OPTICAL_LIMIT_D"] = response.detail[i].Opticallimitd;
                rptRow["PRE_AUTH_OPT_D"] = response.detail[i].Preauthoptd;               
                rptRow["HEART_VALVE_LIMIT_D"] = response.detail[i].HeartValved;
                rptRow["ORGAN_TRANS_LIMIT_D"] = response.detail[i].OrganTransd;
                rptRow["HEARING_AID_LIMIT_D"] = response.detail[i].HearingAidd;
                rptRow["NEWBORN_DIAG_LIMIT_D"] = response.detail[i].NewbornDiagd;
                rptRow["ALZHEIMER_LIMIT_D"] = response.detail[i].Alzheimerd;
                rptRow["AUTISM_LIMIT_D"] = response.detail[i].Autismd;
                rptRow["DISABILITY_LIMIT_D"] = response.detail[i].Disabilityd;
                rptRow["NEW_CCHI_POLICY_IND"] = response.detail[i].NewCCHIPolicyInd;
                rptRow["SABIC_CONT_IND"] = response.detail[i].SABICContInd;                
                rptRow["SCHEME_NAME"] = response.detail[i].Schemename;
                rptRow["SP_INSTRUCT1"] = response.detail[i].Spinstruct1;
                rptRow["SP_INSTRUCT2"] = response.detail[i].Spinstruct2;
                rptRow["SP_INSTRUCT3"] = response.detail[i].Spinstruct3;
                rptRow["FIRST_INSTRUCT_REC"] = response.detail[i].Firstinstructrec;
                rptRow["LAST_INSTRUCT_REC"] = response.detail[i].Lastinstructrec;               
                rptRow["SCHEME_DISPLAY_IND_A"] = response.detail[i].SchemeDisplayInda;
                rptRow["MATERNITY_SPI_A"] = response.detail[i].MaternitySPIa;
                rptRow["DENTAL_SPI_A"] = response.detail[i].DentalSPIa;
                rptRow["OPTICAL_SPI_A"] = response.detail[i].OpticalSPIa;
                rptRow["HEART_VALVE_SPI_A"] = response.detail[i].HeartValveSPIa;
                rptRow["ORGAN_TRANS_SPI_A"] = response.detail[i].OrganTransSPIa;
                rptRow["HEARING_AID_SPI_A"] = response.detail[i].HearingAidSPIa;
                rptRow["NEWBORN_DIAG_SPI_A"] = response.detail[i].NewbornDiagSPIa;
                rptRow["ALZHEIMER_SPI_A"] = response.detail[i].AlzheimerSPIa;
                rptRow["AUTISM_SPI_A"] = response.detail[i].AutismSPIa;
                rptRow["DISABILITY_SPI_A"] = response.detail[i].DisabilitySPIa;
                rptRow["SCHEME_DISPLAY_IND_B"] = response.detail[i].SchemeDisplayIndb;
                rptRow["MATERNITY_SPI_B"] = response.detail[i].MaternitySPIb;
                rptRow["DENTAL_SPI_B"] = response.detail[i].DentalSPIb;
                rptRow["OPTICAL_SPI_B"] = response.detail[i].OpticalSPIb;
                rptRow["HEART_VALVE_SPI_B"] = response.detail[i].HeartValveSPIb;
                rptRow["ORGAN_TRANS_SPI_B"] = response.detail[i].OrganTransSPIb;
                rptRow["HEARING_AID_SPI_B"] = response.detail[i].HearingAidSPIb;
                rptRow["NEWBORN_DIAG_SPI_B"] = response.detail[i].NewbornDiagSPIb;
                rptRow["ALZHEIMER_SPI_B"] = response.detail[i].AlzheimerSPIb;
                rptRow["AUTISM_SPI_B"] = response.detail[i].AutismSPIb;
                rptRow["DISABILITY_SPI_B"] = response.detail[i].DisabilitySPIb;
                rptRow["SCHEME_DISPLAY_IND_C"] = response.detail[i].SchemeDisplayIndc;
                rptRow["MATERNITY_SPI_C"] = response.detail[i].MaternitySPIc;
                rptRow["DENTAL_SPI_C"] = response.detail[i].DentalSPIc;
                rptRow["OPTICAL_SPI_C"] = response.detail[i].OpticalSPIc;
                rptRow["HEART_VALVE_SPI_C"] = response.detail[i].HeartValveSPIc;
                rptRow["ORGAN_TRANS_SPI_C"] = response.detail[i].OrganTransSPIc;
                rptRow["HEARING_AID_SPI_C"] = response.detail[i].HearingAidSPIc;
                rptRow["NEWBORN_DIAG_SPI_C"] = response.detail[i].NewbornDiagSPIc;
                rptRow["ALZHEIMER_SPI_C"] = response.detail[i].AlzheimerSPIc;
                rptRow["AUTISM_SPI_C"] = response.detail[i].AutismSPIc;
                rptRow["DISABILITY_SPI_C"] = response.detail[i].DisabilitySPIc;
                rptRow["SCHEME_DISPLAY_IND_D"] = response.detail[i].SchemeDisplayIndd;
                rptRow["MATERNITY_SPI_D"] = response.detail[i].MaternitySPId;
                rptRow["DENTAL_SPI_D"] = response.detail[i].DentalSPId;
                rptRow["OPTICAL_SPI_D"] = response.detail[i].OpticalSPId;
                rptRow["HEART_VALVE_SPI_D"] = response.detail[i].HeartValveSPId;
                rptRow["ORGAN_TRANS_SPI_D"] = response.detail[i].OrganTransSPId;
                rptRow["HEARING_AID_SPI_D"] = response.detail[i].HearingAidSPId;
                rptRow["NEWBORN_DIAG_SPI_D"] = response.detail[i].NewbornDiagSPId;
                rptRow["ALZHEIMER_SPI_D"] = response.detail[i].AlzheimerSPId;
                rptRow["AUTISM_SPI_D"] = response.detail[i].AutismSPId;
                rptRow["DISABILITY_SPI_D"] = response.detail[i].DisabilitySPId;
               
                rptRow["POLICY_HOLDER"] = response.detail[i].PolicyHolder;
               

                ////CR 414 TOB Report Field Satrt by Sakthi on 10-Jun-2018
                ////Satrt
                rptRow["CCHI_CAT_POLICY_IND"] = response.detail[i].CCHICatPolicyInd;
                rptRow["OVERALL_LIMIT_A"] = response.detail[i].OverallLimitA;
                rptRow["MPN_DEDUCT_AMT_A"] = response.detail[i].MPNDeductAmtA;
                rptRow["MPN_DEDUCT_PCT_A"] = response.detail[i].MPNDeductPctA;
                rptRow["MAX_MPN_DEDUCT_AMT_A"] = response.detail[i].MaxMPNDeductAmtA;
                rptRow["HOSP_DEDUCT_AMT_A"] = response.detail[i].HOSPDeductAmtA;
                rptRow["HOSP_DEDUCT_PCT_A"] = response.detail[i].HOSPDeductPctA;
                rptRow["MAX_HOSP_DEDUCT_AMT_A"] = response.detail[i].MaxHOSPDeductAmtA;
                rptRow["POLYCLIN_DEDUCT_AMT_A"] = response.detail[i].PolyclinDeductAmtA;
                rptRow["POLYCLIN_DEDUCT_PCT_A"] = response.detail[i].PolyclinDeductPctA;
                rptRow["MAX_POLYCLIN_DEDUCT_AMT_A"] = response.detail[i].MaxPolyclinDeductAmtA;
                rptRow["NET_TYPE_A"] = response.detail[i].NetTypeA;
                rptRow["CIRCUM_MALE_LIMIT_A"] = response.detail[i].CircumMaleLimitA;
                rptRow["EAR_PIRE_NB_F_LIMIT_A"] = response.detail[i].EarPierNBFLimitA;
                rptRow["PREMAT_BABY_LIMIT_A"] = response.detail[i].PrematBabyLimitA;
                rptRow["VACC_MOH_LIMIT_A"] = response.detail[i].VaccMOHLimitA;
                rptRow["DIALYSIS_LIMIT_A"] = response.detail[i].DialysisLimitA;
                rptRow["PSYCHIATRIC_LIMIT_A"] = response.detail[i].PsychiatricLimitA;
                rptRow["NON_ACUTE_PSY_LIMIT_A"] = response.detail[i].NonAcutePsyLimitA;
                rptRow["META_SCREEN_LIMIT_A"] = response.detail[i].MetaScreenLimitA;
                rptRow["GS_BMI_OVER_45_LIMIT_A"] = response.detail[i].GSBMIOver45LimitA;
                rptRow["MATERNITY_LMT_A"] = response.detail[i].MaternityLimitA;

                rptRow["OVERALL_LIMIT_B"] = response.detail[i].OverallLimitB;
                rptRow["MPN_DEDUCT_AMT_B"] = response.detail[i].MPNDeductAmtB;
                rptRow["MPN_DEDUCT_PCT_B"] = response.detail[i].MPNDeductPctB;
                rptRow["MAX_MPN_DEDUCT_AMT_B"] = response.detail[i].MaxMPNDeductAmtB;
                rptRow["HOSP_DEDUCT_AMT_B"] = response.detail[i].HOSPDeductAmtB;
                rptRow["HOSP_DEDUCT_PCT_B"] = response.detail[i].HOSPDeductPctB;
                rptRow["MAX_HOSP_DEDUCT_AMT_B"] = response.detail[i].MaxHOSPDeductAmtB;
                rptRow["POLYCLIN_DEDUCT_AMT_B"] = response.detail[i].PolyclinDeductAmtB;
                rptRow["POLYCLIN_DEDUCT_PCT_B"] = response.detail[i].PolyclinDeductPctB;
                rptRow["MAX_POLYCLIN_DEDUCT_AMT_B"] = response.detail[i].MaxPolyclinDeductAmtB;
                rptRow["NET_TYPE_B"] = response.detail[i].NetTypeB;
                rptRow["CIRCUM_MALE_LIMIT_B"] = response.detail[i].CircumMaleLimitB;
                rptRow["EAR_PIRE_NB_F_LIMIT_B"] = response.detail[i].EarPierNBFLimitB;
                rptRow["PREMAT_BABY_LIMIT_B"] = response.detail[i].PrematBabyLimitB;
                rptRow["VACC_MOH_LIMIT_B"] = response.detail[i].VaccMOHLimitB;
                rptRow["DIALYSIS_LIMIT_B"] = response.detail[i].DialysisLimitB;
                rptRow["PSYCHIATRIC_LIMIT_B"] = response.detail[i].PsychiatricLimitB;
                rptRow["NON_ACUTE_PSY_LIMIT_B"] = response.detail[i].NonAcutePsyLimitB;
                rptRow["META_SCREEN_LIMIT_B"] = response.detail[i].MetaScreenLimitB;
                rptRow["GS_BMI_OVER_45_LIMIT_B"] = response.detail[i].GSBMIOver45LimitB;
                rptRow["MATERNITY_LMT_B"] = response.detail[i].MaternityLimitB;

                rptRow["OVERALL_LIMIT_C"] = response.detail[i].OverallLimitC;
                rptRow["MPN_DEDUCT_AMT_C"] = response.detail[i].MPNDeductAmtC;
                rptRow["MPN_DEDUCT_PCT_C"] = response.detail[i].MPNDeductPctC;
                rptRow["MAX_MPN_DEDUCT_AMT_C"] = response.detail[i].MaxMPNDeductAmtC;
                rptRow["HOSP_DEDUCT_AMT_C"] = response.detail[i].HOSPDeductAmtC;
                rptRow["HOSP_DEDUCT_PCT_C"] = response.detail[i].HOSPDeductPctC;
                rptRow["MAX_HOSP_DEDUCT_AMT_C"] = response.detail[i].MaxHOSPDeductAmtC;
                rptRow["POLYCLIN_DEDUCT_AMT_C"] = response.detail[i].PolyclinDeductAmtC;
                rptRow["POLYCLIN_DEDUCT_PCT_C"] = response.detail[i].PolyclinDeductPctC;
                rptRow["MAX_POLYCLIN_DEDUCT_AMT_C"] = response.detail[i].MaxPolyclinDeductAmtC;
                rptRow["NET_TYPE_C"] = response.detail[i].NetTypeC;
                rptRow["CIRCUM_MALE_LIMIT_C"] = response.detail[i].CircumMaleLimitC;
                rptRow["EAR_PIRE_NB_F_LIMIT_C"] = response.detail[i].EarPierNBFLimitC;
                rptRow["PREMAT_BABY_LIMIT_C"] = response.detail[i].PrematBabyLimitC;
                rptRow["VACC_MOH_LIMIT_C"] = response.detail[i].VaccMOHLimitC;
                rptRow["DIALYSIS_LIMIT_C"] = response.detail[i].DialysisLimitC;
                rptRow["PSYCHIATRIC_LIMIT_C"] = response.detail[i].PsychiatricLimitC;
                rptRow["NON_ACUTE_PSY_LIMIT_C"] = response.detail[i].NonAcutePsyLimitC;
                rptRow["META_SCREEN_LIMIT_C"] = response.detail[i].MetaScreenLimitC;
                rptRow["GS_BMI_OVER_45_LIMIT_C"] = response.detail[i].GSBMIOver45LimitC;
                rptRow["MATERNITY_LMT_C"] = response.detail[i].MaternityLimitC;

                rptRow["OVERALL_LIMIT_D"] = response.detail[i].OverallLimitD;
                rptRow["MPN_DEDUCT_AMT_D"] = response.detail[i].MPNDeductAmtD;
                rptRow["MPN_DEDUCT_PCT_D"] = response.detail[i].MPNDeductPctD;
                rptRow["MAX_MPN_DEDUCT_AMT_D"] = response.detail[i].MaxMPNDeductAmtD;
                rptRow["HOSP_DEDUCT_AMT_D"] = response.detail[i].HOSPDeductAmtD;
                rptRow["HOSP_DEDUCT_PCT_D"] = response.detail[i].HOSPDeductPctD;
                rptRow["MAX_HOSP_DEDUCT_AMT_D"] = response.detail[i].MaxHOSPDeductAmtD;
                rptRow["POLYCLIN_DEDUCT_AMT_D"] = response.detail[i].PolyclinDeductAmtD;
                rptRow["POLYCLIN_DEDUCT_PCT_D"] = response.detail[i].PolyclinDeductPctD;
                rptRow["MAX_POLYCLIN_DEDUCT_AMT_D"] = response.detail[i].MaxPolyclinDeductAmtD;
                rptRow["NET_TYPE_D"] = response.detail[i].NetTypeD;
                rptRow["CIRCUM_MALE_LIMIT_D"] = response.detail[i].CircumMaleLimitD;
                rptRow["EAR_PIRE_NB_F_LIMIT_D"] = response.detail[i].EarPierNBFLimitD;
                rptRow["PREMAT_BABY_LIMIT_D"] = response.detail[i].PrematBabyLimitD;
                rptRow["VACC_MOH_LIMIT_D"] = response.detail[i].VaccMOHLimitD;
                rptRow["DIALYSIS_LIMIT_D"] = response.detail[i].DialysisLimitD;
                rptRow["PSYCHIATRIC_LIMIT_D"] = response.detail[i].PsychiatricLimitD;
                rptRow["NON_ACUTE_PSY_LIMIT_D"] = response.detail[i].NonAcutePsyLimitD;
                rptRow["META_SCREEN_LIMIT_D"] = response.detail[i].MetaScreenLimitD;
                rptRow["GS_BMI_OVER_45_LIMIT_D"] = response.detail[i].GSBMIOver45LimitD;
                rptRow["MATERNITY_LMT_D"] = response.detail[i].MaternityLimitD;


                ////End


              

                
                

                rptDT.Rows.Add(rptRow);

            }
            rptDS.Tables.Add(rptDT);

           

            crDoc = new ReportDocument();
            crDoc.Load(MapPath(@"~\provider\MRCUSTTOB01.rpt"));
            crDoc.SetDataSource(rptDT);

            crDoc.SetParameterValue("pc_report_id", "MRCUSTTOB01");
            crDoc.SetParameterValue("pc_report_group", "AL");
            crDoc.SetParameterValue("pv_company_name", "dd");
            crDoc.SetParameterValue("pd_report_date", "29/01/2013");
            crDoc.SetParameterValue("pv_title1", "ddd");
            crDoc.SetParameterValue("pc_rpt_grp", "AL");
            crDoc.SetParameterValue("pc_cont_no", ls_cont_no);
            crDoc.SetParameterValue("pc_cont_yymm", ls_cont_yymm);

            CrystalReportViewer1.ReportSource = crDoc;
            Session["rptDoc5"] = crDoc;


        }
        catch (Exception)
        {

            throw;
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
       
        doc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "Statement");//-" + DateTime.Today.ToShortDateString());

    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        this.CrystalReportViewer1.ShowNextPage();
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["ProCurrentLogID"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }
}

//public class ClassXMLGeneration<T>
//{
//    public void Request(T classType, string req)
//    {
//        System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
//        System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\CaesarXML\\" + req + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
//        xmlSrQ.Serialize(file, classType);
//        file.Close();


//    }
//    public void Response(T classType, string resp)
//    {
//        System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
//        System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\CaesarXML\\" + resp + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
//        xmlSr.Serialize(file, classType);
//        file.Close();
//    }
//}
