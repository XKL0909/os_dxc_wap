﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="MembershipCert2_Default" Codebehind="MembershipCert2.aspx.cs" %>
<%@ Register Src="../Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
   
    

    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

    <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

.style1 {
	text-align: center;
}
.style2 {
	font-weight: bold;
	text-align: right;
}
.style3 {
	text-align: right;
}

.style5 {
	border-right: 1px solid #808080;
	border-bottom: 1px solid #808080;
	border-style: none none solid none;
	border-width: 0px 0px 1px 0px;
	text-align: center;
}
.style6 {
	border-right: 1px none #808080;
	border-bottom: 1px solid #808080;
	border-width: 0px 0px 1px 0px;
	text-align: right;
	border-left-style: none;
	border-top-style: none;
	border-bottom-color: #808080;
}
.style8 {
	font-family: Arial, sans-serif;
	font-size: 9pt;
}

.style9 {
	border-right: 0px none #808080;
	border-bottom: 1px solid #808080;
	text-align: left;
	font-size: 9pt;
	font-family: Arial, sans-serif;
	border-left-style: none;
	border-left-width: 0px;
	border-top-style: none;
	border-top-width: 0px;
}

.style10 {
	border-right: 0px none #808080;
	border-bottom: 1px solid #808080;
	text-align: center;
	font-family: Arial, sans-serif;
	font-size: 9pt;
	border-left-style: none;
	border-left-width: 0px;
	border-top-style: none;
	border-top-width: 0px;
	width:40%;
}

.style11 {
	font-family: Arial, sans-serif;
	font-weight: bold;
}
.style12 {
	font-family: Arial, sans-serif;
}

.style13 {
	font-size: medium;
}

.style14 {
	text-align: left;
}

</style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="styles.css" type="text/css" />
    


    <SCRIPT src="datepicker/firebug.js" 
type=text/javascript></SCRIPT>
<!-- jQuery -->
<SCRIPT 
src="datepicker/jquery-1.2.6.min.js" 
type=text/javascript></SCRIPT>
<!-- required plugins -->
<SCRIPT src="datepicker/date.js" 
type=text/javascript></SCRIPT>
<!--[if IE]>
<SCRIPT 
src="datepicker/jquery.bgiframe.min.js" 
type=text/javascript></SCRIPT>
<![endif]--><!-- jquery.datePicker.js -->
<SCRIPT 
src="datepicker/jquery.datePicker.js" 
type=text/javascript></SCRIPT>
<!-- datePicker required styles --><LINK media=screen 
href="datepicker/datePicker.css" 
type=text/css rel=stylesheet><!-- page specific styles --><LINK media=screen 
href="datepicker/demo.css" 
type=text/css rel=stylesheet><!-- page specific scripts -->
<SCRIPT type=text/javascript charset=utf-8>
            $(function()
            {
				$('.date-pick').datePicker().val(new Date().asString()).trigger('change');
            });
		</SCRIPT>

    
    
</head>
<body topmargin="0px" style="background-color:White;">
    <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Middle
            East Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    <div class="noPrint" align="right">
      
        <table width=100% ><tr><td align=left>
            <label ID="Label1" style="width: 273px" ><font size=4>Certificate of Membership</font></label>  </td>
            <td><a href="#" onclick="window.print()">
                <img border="0" src="images/printer.jpg" alt="Print" style="height: 33px">
            </a> </td></tr></table>
            
            
       
  
    </div>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
       
        <%--       <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <%=DateTime.Now.ToString() %>
            <asp:Panel ID="Panel2" runat="server">
                <asp:Image ID="Image2" runat="server" ImageUrl="spinner.gif" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
 <div class="noPrint" >
        <table><tr><td valign="baseline"><asp:Label  ID="Label1" runat="server" Text="Membership No."></asp:Label><br /><asp:TextBox CssClass="textbox" ID="txtSearch" runat="server"></asp:TextBox></td>
        <td valign="baseline"><asp:Label ID="Label2" runat="server" Text="Valid Date"></asp:Label>
            <uc1:Date2 ID="Date2"  runat="server" />
            <br /> 
  
            
        </td>
        <td valign="baseline"><br /><asp:Button CssClass="submitButton" ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
</td></tr></table>
</div>


        &nbsp;
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSearch"
            ErrorMessage="RequiredFieldValidator" Font-Bold="False" Font-Names="Verdana"
            Font-Size="Small">Please type in the membership No./ Staff No. to search</asp:RequiredFieldValidator>
        <br />
        
        
        <div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
        
        
        
        
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" Visible="false" >
            <ContentTemplate>
                <br />
                <br />
                <table style="width: 100%; padding-left:0px; padding-right:0px;" cellspacing="0"   >
                    <tr >
                        <td style="width: 499px" class="style13">
                            <b class="style11">Attention all BUPA Middle East Network</b></td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style2" style="width: 40%">
                            السادة / شبكة مستشفيات بوبا الشرق الأوسط
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 499px; height: 30px;">
                        </td>
                        <td class="style1" style="width: 127px; height: 30px;">
                        </td>
                        <td class="style3" style="height: 30px">
                        </td>
                    </tr>
                    <tr>
                        <td class="style11">
                            TO WHOM IT MAY CONCERN</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            <b>لمن يهمه الأمر</b></td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            &nbsp;</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            <span class="style12">Subject: </span><span class="style11">Certificate of Membership</span></td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            الموضوع: شهادة عضوية</td>
                    </tr>
                    <tr>
                        <td style="width: 499px">
                            &nbsp;</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 499px" class="style8">
                            Certificate Issue Date</td>
                        <td class="style14" style="width: 127px">
                            <strong class="style10">
                                <asp:Label ID="lblIssueDate" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td class="style3">
                            تاريخ الاصدار
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 499px" class="style8">
                            This is to confirm that the below mentioned member is medically covered under the
                            BUPA Middle East Health Care scheme with the following information:</td>
                        <td class="style1" style="width: 127px">
                            &nbsp;</td>
                        <td class="style3">
                            إن هذه الوثيقة تؤكد ان المذكور مغطى طبيا
                            <br />
                            تحت برنامج بوبا الشرق الاوسط للرعاية الصحية وفقا للبيانات التالية
                        </td>
                    </tr>
                </table>
                <br>
                <table style="width: 100%">
                    <tr>
                        <td align="left" class="style9" style="width: 30%">
                            Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblMemberName" runat="server"></asp:Label></strong></td>
                        <td align="right" class="style6" style="width: 30%">
                            الاسم</td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Class Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblClass" runat="server"></asp:Label></strong></td>
                        <td align="right" class="style6">
                            درجة التغطية
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Membership No:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblMembershipNo" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            رقم العضوية
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Option*:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblOptions" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            قائمة المزايا
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Room:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblRoomType" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            درجة الإقامة
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Year of Birth:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblDateOfBirth" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            تاريخ الميلاد
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Effective From:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblEffectiveFrom" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            صالحة من تاريخ&nbsp;
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Effective To:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblEffectiveTo" runat="server"></asp:Label>
                            </strong>
                        </td>
                        <td align="right" class="style6">
                            صالحة الى تاريخ
                        </td>
                    </tr>
                    <tr class="style5">
                        <td align="left" class="style9" style="width: 320px">
                            Contribution:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblContribution" runat="server" Text="Label"></asp:Label></strong></td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">نسبة المشاركة</span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="style9" style="width: 320px">
                            Staff No.</td>
                        <td class="style10">
                            <asp:Label ID="lblStaffNo" runat="server"></asp:Label>&nbsp;</td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">الرقم الوظيفي </span>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" class="style9" style="width: 320px">
                            Customer Name:</td>
                        <td class="style10">
                            <strong>
                                <asp:Label ID="lblCustomerName" runat="server"></asp:Label></strong></td>
                        <td class="style6">
                            <span lang="AR-SA" style="font-family: 'Arial','sans-serif'">أسم العميل</span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 320px">
                            &nbsp;</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 320px" class="style8">
                            All Other terms and conditions are as per policy</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            جميع شروط الغطاء تخضع لبنود التعاقد</td>
                    </tr>
                    <tr>
                        <td style="width: 320px">
                            &nbsp;</td>
                        <td class="style1" style="width: 239px">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                    </tr>
                </table>
                <p>
                </p>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 40%" class="style8" valign="top">
                            This certificate is issued as a temporary cover for this member. Should this member
                            need any treatment that is covered under this scheme, please provide it, and charge
                            BUPA Middle East.</td>
                        <td>
                            &nbsp;</td>
                        <td align="right" class="th" colspan="3" style="width: 40%" valign="top">
                            إن هذه الشهادة اصدرت مؤقتاً للعضو وفي حالة حاجته الى اية خدمات علاجية مدرجة تحت
                            بنود الغطاء التأميني الخاص به الرجاء تقديم الخدمة العلاجية و ارسال الفواتير الى
                            بوبا الشرق الاوسط
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 437px" class="style8" valign="top">
                            Should you have any queries, please contact one of our advisers on the BUPA Middle
                            East dedicated customer service help lines 800 244 0307. This help
                            line is open 24hrs seven days a week.</td>
                        <td style="width: 55px">
                            &nbsp;</td>
                        <td align="right" class="th" colspan="3" valign="top">
                            لمزيد من المعلومات الرجاء الاتصال بقسم خدمات العملاء هاتف او الهاتف المجاني
                            0307 244 800 على مدار الساعة طوال ايام الاسبوع
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 437px" class="style8" valign="top">
                            This certificate is valid only for the named insured person when accompanied with
                            ID documents.
                        </td>
                        <td style="width: 55px">
                            &nbsp;</td>
                        <td align="right" class="th" colspan="3" valign="top">
                            إن هذه الشهادة صالحة فقط للعضو المذكور اعلاه شرط توفر وثيقة اثبات رسمية
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td>
                            <b><i class="style8">This is only a definition for the codes shown under the option
                                field. Do not use for coverage checking.</i></b></td>
                    </tr>
                    <tr>
                        <td class="style8">
                            * A: Dialysis, C: Chronic, D: Dental, E: Neo-natal care, H: Other Conditions, K:
                            Cosmetic, L: Congenital, M: Maternity, N: Natural Changes, O: Optical, P: Developmental,
                            S: Standard Benefit, T: Screening, V: Vaccinations, Y: Physiotherapy, Z: Hazardous
                            Sports Injury
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
        <br />
        <div>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
            <br />
            <%--          <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtenderLogin">
           
           </ajaxToolkit:ModalPopupExtender>--%>
            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label><asp:TextBox CssClass="textbox" ID="txtBox_output"
                Visible="false" runat="server"></asp:TextBox>
                        
           
       
        </div>
         
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                       <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

    </form>
    <%--   <script type="text/javascript" language="javascript">
            var firstFlag = true;
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            function pageLoad(){
                if(firstFlag)  __doPostBack("UpdatePanel2","");
            }
            function BeginRequestHandler(sender, args){
                firstFlag = false;
            }
    </script>--%>
    
    
    


</body>
</html>
