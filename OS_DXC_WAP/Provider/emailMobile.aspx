﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Templates/Mobile.Master"   EnableEventValidation="false" Inherits="Provider_emailMobile" Codebehind="emailMobile.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
 <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

  
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

    </style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
    <div>
    
      
    <center><table style="width:100%;">
        <tr>
            <td colspan="2" class="style3">
                &nbsp;</td>
            <td colspan="2" class="style3">
                
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h2 style="text-align: center">
                    Send Email</h2></td>
        </tr>
       </table> 
    
    <asp:Panel ID="pnlEmailForm"  runat="server" >  
       <table style="width:100%;">
           <caption>
               <tr>
                   <td align="left">
                       <strong>E-mail Address</strong></td>
                   <td class="style1" colspan="2" style="text-align: left">
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                           ControlToValidate="txtFax" ErrorMessage="*"></asp:RequiredFieldValidator>
                   </td>
                   <td style="text-align: left">
                       <asp:TextBox ID="txtFax" runat="server"></asp:TextBox>
                   </td>
               </tr>
               <tr>
                   <td colspan="4">
                       <asp:Button ID="btnsend" runat="server" onclick="btnsend_Click" Text="Send" />
                   </td>
               </tr>
               <tr>
                   <td align="center" colspan="4">
                       &nbsp;</td>
               </tr>
           </caption>
    </table>
    </asp:Panel> 
   </center> 
     </div>
   <center> <asp:Label ID="lblMsg" runat="server" Text="" style="color:Green"></asp:Label></center>
   
   </asp:Content>