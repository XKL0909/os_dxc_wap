<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Templates/Inner.Master" Inherits="ExistingRequestSearch" Debug="true" Codebehind="ExistingRequestSearch.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register Src="../Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <link href="css/basic.css" rel="stylesheet" type="text/css" />
    <link href="css/basic_ie.css" rel="stylesheet" type="text/css" />
   <%-- <asp:ScriptManager runat="server" ID="scriptManager" />--%>
    <link rel="stylesheet" href="../styles/stylejquery.css" />
    <script type='text/JavaScript'>
        function newWindow(url) {
            popupWindow = window.open(url,
                                 'popUpWindow',
                                 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');
        }

        function toggleDiv() {
            if (document.getElementById("myDivBox").style.display == "block") {
                document.getElementById("myDivBox").style.display = "none";
            }
            else {
                document.getElementById("myDivBox").style.display = "block";
            }
        }
    </script>
    <script type="text/javascript">
        function goto(x) {
            window.location = x;
        }
    </script>
    <script language="javascript" type="text/javascript">
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
        });

        prm.add_endRequest(function () {
            $('#entryForm').unblock();
        });

        // Progress indicator preloading.
        var preload = document.createElement('img');
        preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
        delete preload;

    </script>
    <script language="javascript" type="text/javascript">

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
        });

        prm.add_endRequest(function () {
            $('#entryForm').unblock();
        });

        // Progress indicator preloading.
        var preload = document.createElement('img');
        preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
        delete preload;

    </script>
    <style type="text/css" media="screen">
        .DatePicker {
            BACKGROUND: url(images/cal.gif) no-repeat right center;
        }

        DIV.DatePicker-Container {
            font-size: 9px;
        }

        DIV.DatePicker-Wrapper {
            BORDER-RIGHT: #e0e0e0 1px solid;
            BORDER-TOP: #e0e0e0 1px solid;
            BORDER-LEFT: #e0e0e0 1px solid;
            BORDER-BOTTOM: #e0e0e0 1px solid;
            BACKGROUND-COLOR: skyblue;
        }

            DIV.DatePicker-Wrapper TABLE {
            }

            DIV.DatePicker-Wrapper TD {
                PADDING-RIGHT: 0px;
                PADDING-LEFT: 0px;
                PADDING-BOTTOM: 0px;
                PADDING-TOP: 0px;
            }

                DIV.DatePicker-Wrapper TD.nav {
                }

                DIV.DatePicker-Wrapper TD.dayName {
                    BORDER-RIGHT: #e0e0e0 1px solid;
                    FONT-WEIGHT: bold;
                    BORDER-BOTTOM: #e0e0e0 1px solid;
                    BACKGROUND-COLOR: #eeeeee;
                }

                DIV.DatePicker-Wrapper TD.day {
                    BORDER-RIGHT: #e0e0e0 1px solid;
                    BORDER-BOTTOM: #e0e0e0 1px solid;
                }

                DIV.DatePicker-Wrapper TD.empty {
                    BORDER-RIGHT: #e0e0e0 1px solid;
                    BORDER-BOTTOM: #e0e0e0 1px solid;
                }

                DIV.DatePicker-Wrapper TD.current {
                    COLOR: #ffffff;
                    BACKGROUND-COLOR: #666666;
                }

                DIV.DatePicker-Wrapper TD.dp_roll {
                    BACKGROUND-COLOR: #e0e0e0;
                }
    </style>
    <style type="text/css">
        #idletimeout {
            background: red;
            border: 3px solid #FF6500;
            color: blue;
            border-color: lime;
            border-width: 10px;
            font-family: arial, sans-serif;
            text-align: center;
            font-size: 27px;
            padding: 10px;
            position: relative;
            top: 0px;
            left: 0;
            right: 0;
            z-index: 100000;
            display: none;
        }

            #idletimeout a {
                color: #fff;
                font-weight: bold;
                font-size: 17px;
            }

            #idletimeout span {
                font-weight: bold;
                font-size: 33px;
            }
    </style>
    <script type='text/JavaScript'>
        function newWindow(url) {
            popupWindow = window.open(url,
                                      'popUpWindow',
                                      'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');
        }
    </script>

    <div id="Div1">
        <asp:UpdatePanel runat="server" ID="updatePanel1">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnReferenceNo" />
                <asp:AsyncPostBackTrigger ControlID="btnMembershipNoPre" />
                <asp:AsyncPostBackTrigger ControlID="btnSearchByPre" />
                <asp:AsyncPostBackTrigger ControlID="btnVerificationID" />
                <asp:AsyncPostBackTrigger ControlID="btnMembershipNoVer" />
                <asp:AsyncPostBackTrigger ControlID="btnSearchByVer" />
            </Triggers>
            <ContentTemplate>
                <table width="100%" border="0">
                    <tr>
                        <td>
                            <div id="idletimeout">
                                You will be logged off in
                                <br />
                                <span>
                                </span>&nbsp;<br />
                                seconds due to inactivity.<br />
                            </div>
                            <div>
                                <fieldset>
                                    <legend>
                                        <h1>
                                            <asp:Label ID="lblPageTitle" runat="server" Text="Existing Request"></asp:Label></h1>
                                    </legend>
                                    <div id="entryForm">
                                        <table style="font-size: small; width: 100%; font-family: Arial;" cellspacing="0">
                                            <tr>
                                                <td style="width: 300px;">
                                                    <table>
                                                        <tr>
                                                            <td colspan="3" style="height: 22px; width: 314px; background-color: white;">
                                                                <strong><span style="font-size: 10pt; background-color: white; color: #0099ff;">Search for pre-authorisation</span></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="width: 314px;">
                                                                <asp:Panel ID="Panel1_1" DefaultButton="btnReferenceNo" runat="server">

                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 108px;">Reference Number</td>
                                                                            <td style="width: 110px;">
                                                                                <asp:TextBox CssClass="textbox" ID="txtReferenceNo" runat="server" Width="110px" ValidationGroup="1"></asp:TextBox></td>
                                                                            <td style="width: 75px;" valign="top">
                                                                                <asp:Button CssClass="submitButton" ID="btnReferenceNo" runat="server" Text="Search" OnClick="btnReferenceNo_Click" ValidationGroup="1" Width="75px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3" valign="top">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReferenceNo"
                                                                                    ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="203px" Display="Dynamic">Please type in the value to search</asp:RequiredFieldValidator></td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="width: 314px;">
                                                                <asp:Panel ID="Panel1" DefaultButton="btnMembershipNoPre" runat="server">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 108px">Membership No</td>
                                                                            <td style="width: 110px">
                                                                                <asp:TextBox CssClass="textbox" ID="txtMembershipNoPre" runat="server" Width="110px" ValidationGroup="2"></asp:TextBox></td>
                                                                            <td style="width: 75px" valign="top">
                                                                                <asp:Button CssClass="submitButton" ID="btnMembershipNoPre" runat="server" Text="Search" OnClick="btnMembershipNoPre_Click" ValidationGroup="2" Width="75px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMembershipNoPre"
                                                                                    ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="202px" Display="Dynamic">Please type in the value to search</asp:RequiredFieldValidator></td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="width: 314px;">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 108px;">
                                                                            <asp:Label ID="Label1" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                                Text="Search by" Width="108px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px;">
                                                                            <div class="styled-select">
                                                                                <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlPreauth" runat="server" ValidationGroup="3"
                                                                                    Width="110px">
                                                                                    <asp:ListItem Value="T">Today</asp:ListItem>
                                                                                    <asp:ListItem Value="Y">Yesterday</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 75px;">
                                                                            <asp:Button CssClass="submitButton" ID="btnSearchByPre" runat="server"
                                                                                Text="Search" OnClick="btnSearchByPre_Click" ValidationGroup="3"
                                                                                Width="75px" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                                                                ControlToValidate="ddlPreauth" Display="Dynamic"
                                                                                ErrorMessage="RequiredFieldValidator" ValidationGroup="3">Please type in the value to search</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                &nbsp;&nbsp; &nbsp; &nbsp;<br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 20px" bgcolor="#ffffff"></td>
                                                <td style="width: 310px; background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td colspan="3" style="width: 300px; height: 22px; background-color: white">
                                                                <strong style="background-color: white"><span style="font-size: 10pt; color: #0099ff;">Search for member verification</span></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="width: 300px;">
                                                                <asp:Panel ID="Panel2" DefaultButton="btnVerificationID" runat="server">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 100px">Verification ID</td>
                                                                            <td style="width: 110px">
                                                                                <asp:TextBox CssClass="textbox" ID="txtVerificationID" runat="server" Width="110px" ValidationGroup="4"></asp:TextBox></td>
                                                                            <td style="width: 62px" valign="top">
                                                                                <asp:Button CssClass="submitButton" ID="btnVerificationID" runat="server" Text="Search" OnClick="btnVerificationID_Click1" ValidationGroup="4" Width="75px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtVerificationID"
                                                                                    ErrorMessage="RequiredFieldValidator" ValidationGroup="4" Width="222px" Display="Dynamic">Please type in the value to search</asp:RequiredFieldValidator></td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="width: 300px;">
                                                                <asp:Panel ID="Panel3" DefaultButton="btnMembershipNoVer" runat="server">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 100px">Membership No</td>
                                                                            <td style="width: 110px">
                                                                                <asp:TextBox CssClass="textbox" ID="txtMembershipNoVer" runat="server" Width="110px" ValidationGroup="5"></asp:TextBox></td>
                                                                            <td style="width: 62px" valign="top">
                                                                                <asp:Button CssClass="submitButton" ID="btnMembershipNoVer" OnClick="btnMembershipNoVer_Click" runat="server" Text="Search" ValidationGroup="5" Width="75px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMembershipNoVer"
                                                                                    ErrorMessage="RequiredFieldValidator" ValidationGroup="5" Display="Dynamic">Please type in the value to search</asp:RequiredFieldValidator></td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="width: 300px;">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 100px">
                                                                            <asp:Label ID="Label2" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                                Text="Search by" Width="100px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <div class="styled-select">
                                                                                <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlVerify" runat="server" ValidationGroup="6"
                                                                                    Width="110px">
                                                                                    <asp:ListItem Value="T">Today</asp:ListItem>
                                                                                    <asp:ListItem Value="Y">Yesterday</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </td>
                                                                        <td style="width: 62px" valign="top">
                                                                            <asp:Button CssClass="submitButton" ID="btnSearchByVer"
                                                                                OnClick="btnSearchByVer_Click" runat="server" Text="Search" ValidationGroup="6"
                                                                                Width="75px" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                                                                ControlToValidate="ddlVerify" Display="Dynamic"
                                                                                ErrorMessage="RequiredFieldValidator" ValidationGroup="6" Width="218px">Please type in the value to search</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <table width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:Literal runat="server" ID="CoverageListReport" Visible="false"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Message1" ForeColor="Red" runat="server" Font-Size="Small"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <br />
                                <uc2:OSNav ID="OSNav1" runat="server" />
                            </div>
                        </td>
                    </tr>
                </table>
                <div id='content'>
                    <div id="basic-modal-content">
                        <p>
                            This contract has been extended to <b>
                                <asp:Literal runat="server" ID="litExtendedDate"></asp:Literal></b>
                            <br />
                            <br />
                            Please provide services to member in line with Table of Benefits.
                            <br />
                            <br />
                            You can print the Table of Benefits with Extension date.<br />
                        </p>
                        <div style="vertical-align: central;">
                            <p>
                                <a runat="server" id="aCloseOverlay" href="#" style="color: #fff; text-align: left;">Close</a>
                            </p>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

<asp:Content ID="Content3" runat="server"
    ContentPlaceHolderID="ContentPlaceHolder1">
    <p>
        Search and print existing pre-authorisations issued to our providers
    </p>
</asp:Content>