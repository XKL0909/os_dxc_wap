﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Configuration;
using Utility.Data;
using Bupa.OSWeb.Helper;
using System.IO;
using System.Web.UI.WebControls;


public partial class Provider_CreateManageUser : System.Web.UI.Page
{
    private static readonly string connectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
    private string _allowedExtensions = ConfigurationManager.AppSettings["UploadAllowedExtensions"].ToString();
    private string _folder = ConfigurationManager.AppSettings["UserManagement_UploadFile"].ToString();
    private string ManageUserActivationUrl = ConfigurationManager.AppSettings["ManageUserActivationUrl"].ToString();
    private string UploadFileUSMUserName = ConfigurationManager.AppSettings["UploadFileUSMUserName"].ToString();
    private string UploadFileUSMPassword = ConfigurationManager.AppSettings["UploadFileUSMPassword"].ToString();
    private string FTPServerDomain = ConfigurationManager.AppSettings["FTPServerDomain"].ToString();
    private UploadManager _uploadManager;
    private DataSet _uploadedFileSet = new DataSet();
    private long _maxFileSetByteSize;
    private long _maxFileSize = 5242880;
    private string _sessionId;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["SuperUser"])) && string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            Response.Redirect("~/CommClear.aspx", true);

        lblUserError.Text = "";
        lblUserManageError.Text = "";
        WrongCaptcha.Style.Add("visibility", "hidden");
        if (uploadGrid.Visible == false)
        {
            rfvFileUpload1.Enabled = true;
            revFileUpload1.Enabled = true;
        }
        Master.FindControl("aa").Visible = false;
        if (!IsPostBack)
        { 
            Session["CaptchaImageText"] = GenerateRandomCode();
            CaptchaText.InnerHtml = Session["CaptchaImageText"].ToString();
            Session["_sessionId"] = "";
        }
       
    }
    private string GenerateRandomCode()
    {
        Random r = new Random();
        string s = "";
        for (int j = 0; j < 5; j++)
        {
            int i = r.Next(3);
            int ch;
            switch (i)
            {
                case 1:
                    ch = r.Next(0, 9);
                    s = s + ch.ToString();
                    break;
                case 2:
                    ch = r.Next(65, 90);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
                case 3:
                    ch = r.Next(97, 122);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
                default:
                    ch = r.Next(97, 122);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
            }
            r.NextDouble();
            r.Next(100, 1999);
        }
        return s;
    }

    protected string AddUserToDataBase()
    {
        string _result;
        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        bool isTerms=Iagree.Checked;
        try
        {

            DataParameter pRefNo = new DataParameter(SqlDbType.Int, "@pRefNo", Convert.ToInt32(Session["pRefNo"].ToString()), ParameterDirection.Input);
            DataParameter proID = new DataParameter(SqlDbType.NVarChar, "@proID", Convert.ToString(Session["ProviderID"]), ParameterDirection.Input);
            DataParameter proUsername = new DataParameter(SqlDbType.NVarChar, "@proUsername ", txtUserName.Text.Trim(), ParameterDirection.Input);
            DataParameter proPassword = new DataParameter(SqlDbType.NVarChar, "@proPassword ", "", ParameterDirection.Input);
            DataParameter ProName = new DataParameter(SqlDbType.NVarChar, "@ProName ", txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim(), ParameterDirection.Input);
            DataParameter proEmail = new DataParameter(SqlDbType.NVarChar, "@proEmail ", txtEmailId.Text.Trim(), ParameterDirection.Input);
            DataParameter proFaxNo = new DataParameter(SqlDbType.NVarChar, "@proFaxNo ", "", ParameterDirection.Input);
            DataParameter proNotes = new DataParameter(SqlDbType.NVarChar, "@proNotes", "", ParameterDirection.Input);
            DataParameter Salt = new DataParameter(SqlDbType.NVarChar, "@Salt ", "", ParameterDirection.Input);
            DataParameter Active = new DataParameter(SqlDbType.Bit, "@proActive", false, ParameterDirection.Input);
            DataParameter CreatedDate = new DataParameter(SqlDbType.DateTime, "@CreatedDate", DateTime.Now, ParameterDirection.Input);
            DataParameter CreatedBy = new DataParameter(SqlDbType.Int, "@CreatedBy", Convert.ToInt32(Session["ProviderID"].ToString()), ParameterDirection.Input);
            DataParameter isTermsAndConditionChecked = new DataParameter(SqlDbType.Bit, "@isTermsAndConditionChecked", isTerms, ParameterDirection.Input);
            DataParameter TermsConditionCheckedDate = new DataParameter(SqlDbType.DateTime, "@TermsConditionCheckedDate", DateTime.Now, ParameterDirection.Input);
            DataParameter Mobile = new DataParameter(SqlDbType.VarChar, "@Mobile", txtMobileNumber.Text.Trim(), ParameterDirection.Input);
            DataParameter isSuperUser = new DataParameter(SqlDbType.Bit, "@isSuperUser", false, ParameterDirection.Input);
            
            DataParameter result = new DataParameter(SqlDbType.Int, "@Result", "", ParameterDirection.Output);

            DataParameter[] dpBasket = new DataParameter[] { pRefNo, proID, proUsername, proPassword, ProName, proEmail, proFaxNo, proNotes, Salt, Active, CreatedDate, CreatedBy, isTermsAndConditionChecked, TermsConditionCheckedDate, Mobile, isSuperUser, result };

            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.ManageUserInsertProvider", ref dpBasket, 60000);
            _result = dpBasket[16].ParamValue.ToString();
        }
        catch (Exception ex)
        {
            throw;
        }

        return _result;
    }


    private bool checkUsernameAlreadyExist()
    {
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        try
        {
            cmd.Connection = conn;
            cmd.CommandText = "CheckManageUserExist";
            cmd.Parameters.Add(new SqlParameter("@cliUsername", txtUserName.Text.Trim()));
            cmd.Parameters.Add(new SqlParameter("@ClientType", "P"));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            var result = cmd.ExecuteScalar();

            if (result.ToString() != "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conn.Close();
        }

    }

    
    private bool CheckEmailAlreadyExist()
    {
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        try
        {
            cmd.Connection = conn;
            cmd.CommandText = "CheckManageUserEmailExist";
            cmd.Parameters.Add(new SqlParameter("@emailID", txtEmailId.Text.Trim()));
            cmd.Parameters.Add(new SqlParameter("@ClientType", "P"));
            cmd.Parameters.Add(new SqlParameter("@ContractNumber", ""));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            var result = cmd.ExecuteScalar();

            if (result.ToString() != "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conn.Close();
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {

            lblUserError.Text = "";
            lblUserManageError.Text = "";
            if (checkUsernameAlreadyExist())
            {
                if (lblChkUser.Text == "")
                    lblUserError.Text = "User name is already in use.";
                Iagree.Checked = false;
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                WrongCaptcha.Style.Add("visibility", "hidden");
                return;
            }
            if(CheckEmailAlreadyExist())
            {
                if(lblChkEmail.Text =="")
                    lblUserError.Text = "Email already in use.";
                Iagree.Checked = false;
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                WrongCaptcha.Style.Add("visibility", "hidden");
                return;
            }
            if (txtimgcode.Text == Session["CaptchaImageText"].ToString())
            {
                divCaptchaImage.Visible = false;
                txtimgcode.Text = "";
                WrongCaptcha.Style.Add("visibility", "hidden");

            }
            else
            {
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                WrongCaptcha.Style.Add("visibility", "visible");
                Session["CaptchaImageText"] = GenerateRandomCode();
                CaptchaText.InnerText = Session["CaptchaImageText"].ToString();
                Iagree.Checked = false;
                return;
            }

            if (Iagree.Checked == false)
            {
                lblUserError.Text = "Please select terms and conditions";
                Iagree.Focus();
                return;
            }
            string result=AddUserToDataBase();
            if (!string.IsNullOrEmpty(result))
            {

                bool IsSuccess = SendEmail(txtEmailId.Text, txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim(), result, txtUserName.Text);
                if (IsSuccess)
                {
                    string smsMessage = "Dear " + txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim() + ", please use the following OTP " + Session["ProviderOTP"].ToString() + " to activate the user." +
                         " Yours sincerely, Customer Services, Bupa Arabia";
                    BLCommon.InsertSMS(txtMobileNumber.Text.Trim(), smsMessage, ConfigurationManager.AppSettings["OSProviderSMSSource"]);
                    ClearControls();
                    btnSubmit.Enabled = false;
                    popUpSuccess.Visible = true;
                    popUpSuccess.Style.Add("display","block");
                    //lblWarningMsg.Text = "User successully added, a Mail will be send to the registered email address with link to change password";
                    //divWarningAlert.Visible = true;

                }
                else
                {
                    lblUserError.Text = "Unable to insert OTP details";
                    Iagree.Checked = false;
                }
            }
        }
        catch (Exception ex)
        {
            txtimgcode.Text = "";
            divCaptchaImage.Visible = true;
            WrongCaptcha.Style.Add("visibility", "hidden");
            Iagree.Checked = false;
        }
    }
    private void ClearControls()
    {
        txtUserName.Text = "";
        txtMobileNumber.Text = "";
        txtLastName.Text = "";
        txtimgcode.Text = "";
        txtFirstName.Text = "";
        txtEmailId.Text = "";
        Iagree.Checked = false;
        Session["_sessionId"] = "";

        try
        {
            Session["CaptchaImageText"] = GenerateRandomCode();
            CaptchaText.InnerText = Session["CaptchaImageText"].ToString();
            if (txtimgcode.Text == "" || txtimgcode.Text != "")
            {
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                return;
            }
        }
        catch (Exception ex) { }
    }
    private string InsertOTPIntoDataBase(string OTPValue, string ClientRefno, string ClientType)
    {
        string _result;
        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        bool isTerms = Iagree.Checked;
        try
        {
            DataParameter RefNo = new DataParameter(SqlDbType.Int, "@RefNo", ClientRefno, ParameterDirection.Input);
            DataParameter UserType = new DataParameter(SqlDbType.Char, "@UserType", ClientType, ParameterDirection.Input);
            DataParameter OTP = new DataParameter(SqlDbType.NVarChar, "@OTP ", OTPValue, ParameterDirection.Input);
            DataParameter Created = new DataParameter(SqlDbType.DateTime, "@Created", DateTime.Now, ParameterDirection.Input);
            DataParameter IsValid = new DataParameter(SqlDbType.Bit, "@IsValid ", true, ParameterDirection.Input);

            DataParameter result = new DataParameter(SqlDbType.Int, "@Result", "", ParameterDirection.Output);
            DataParameter[] dpBasket = new DataParameter[] { RefNo, UserType, OTP, Created, IsValid, result };

            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.InsertManageUserOTP", ref dpBasket, 60000);
            _result = dpBasket[5].ParamValue.ToString();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

        }
        return _result;
    }
    private bool SendEmail(string Email, string FullName,string ClientRefno,string UserName)
    {
        String OTP = "";

        int pinCode = GeneralHelper.GenetareVerificationPin(100001, 999999);
        while (pinCode < 0)
        {
            pinCode = GeneralHelper.GenetareVerificationPin(100001, 999999);
        }
        OTP = pinCode.ToString();
        Session["ProviderOTP"] = OTP;
        string result = InsertOTPIntoDataBase(OTP, ClientRefno, "p");
        if (result == "1")
        {
            var keys = "referenceNumber=" + ClientRefno  + "&UserName=" + UserName + "&provider=p" + "&otp=" + OTP;
            var encryptedquerystringvalue = Cryption.Encrypt(keys);
            var finalUrl = ManageUserActivationUrl + "/UserActivation.aspx?val=" + encryptedquerystringvalue;
            MailMessage mailMsg = new MailMessage();
            mailMsg.From = new MailAddress("donotreply@bupa.com.sa");
            mailMsg.Subject = "Bupa Onlineservice New User";
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("../Templates/mail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Url}", finalUrl);
            body = body.Replace("{UserName}", UserName);
            mailMsg.Body = body;
            mailMsg.To.Add(Email);
            mailMsg.IsBodyHtml = true;
            SmtpClient smptClient = new System.Net.Mail.SmtpClient();
            smptClient.Send(mailMsg);
            mailMsg.Dispose();
            return true;
        }
        else
        {
            return false;
        }
    }


    protected void CaptchaValidate_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Session["CaptchaImageText"] = GenerateRandomCode();
            CaptchaText.InnerText = Session["CaptchaImageText"].ToString();
            if (txtimgcode.Text == "" || txtimgcode.Text != "")
            {
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                return;
            }
        }
        catch (Exception ex) { }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            lblUserManageError.Text = "";
            if (uploadGrid.Visible == false)
            {
                if (FileUpload1.PostedFile.ContentLength > _maxFileSize)
                {
                    lblUserManageError.Text = "Selected files size exceed the limit it should be 5MB";
                    Iagree.Checked = false;
                    return;
                }
            bool isvalidFile = false;
            if (FileUpload1.HasFile)
            {
                    Boolean fileOK = false;
                    Boolean mimeOK = false;
                    System.Web.HttpPostedFile file = FileUpload1.PostedFile;
                    CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
                    string mime = findMimeFromDate.CheckFindMimeFromData(file);
                    String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
                    String fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                    String[] allowedExtensions = _allowedExtensions.Split(',');
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                            break;
                        }
                    }
                    for (int i = 0; i < allowedMime.Length; i++)
                    {
                        if (mime == allowedMime[i])
                        {
                            mimeOK = true;
                            break;
                        }
                    }
                    if (fileOK && mimeOK)
                    {
                        isvalidFile = true;
                    }
                    else
                    {
                        lblUserManageError.Text = "Please select a valid file";
                        Iagree.Checked = false;
                        return;
                    }
                

                if (isvalidFile)
                {

                    // Proceed with saving the file
                        bool uploaded = false;
                    uploaded = SaveFileToFolder(FileUpload1.FileName, _folder);
                    }
                    else
                    {
                        lblUserManageError.Text = "Please select a valid file";
                        Iagree.Checked = false;
                        }
                    }

                else
                {
                    lblUserManageError.Text = "Please select a valid file";
                    Iagree.Checked = false;
                }
            }
            else
            {
                lblUserManageError.Text = "Only one file can be uploaded inorder to upload new file, delete the existing one.";
                Iagree.Checked = false;
            }
        }
        catch (Exception ex)
        {
            Iagree.Checked = false;
        }
       
    }

    private bool SaveFileToFolder(string fileName, string Folder)
    {
        fileName = fileName.Trim();
        Folder = Folder.Trim();
        UploadResult uploadResult = new UploadResult();
        uploadResult.Result = false;

        try
        {
            ImpersonationHelper.Impersonate(FTPServerDomain, UploadFileUSMUserName, UploadFileUSMPassword, delegate
            {
                string extension = Path.GetExtension(fileName);
                string newfileName = TransactionManager.TransactionID().ToString() + "_" + Session["ClientID"] + extension;
                string virtualPath = Folder + newfileName;

                FileUpload1.SaveAs(virtualPath);
                ShowUploadedList(newfileName, virtualPath);
            });

        }
        catch (Exception ex)
        {
            return false;
            uploadGrid.Visible = true;
        }

        return true;
    }


    private void ShowUploadedList(string filename, string destination)
    {
        lblfileDestination.Text = destination;
        lblDocumentname.Text = filename;
        lblUploadDate.Text = DateTime.Now.ToString();
        uploadGrid.Visible = true;
        rfvFileUpload1.Enabled = false;
        revFileUpload1.Enabled = false;
    }
    
    protected void Iagree_CheckedChanged(object sender, EventArgs e)
    {
        if (Iagree.Checked == true)
        {
            if (txtEmailId.Text != "" && txtFirstName.Text != "" && txtimgcode.Text != "" && txtLastName.Text != "" && txtMobileNumber.Text != "" && txtUserName.Text != "")
            { 
                btnSubmit.Enabled = true;
                btnSubmit.Style.Add("color", "none");
                string background = "../images/buttons-back.gif";
                btnSubmit.Style.Add("background-image", background);
            }
            else
                Iagree.Checked = false;
        }
        else
        {
            btnSubmit.Enabled = false;
            btnSubmit.Style.Add("color", "darkgray");
            btnSubmit.Style.Add("background-image", "none");
            Iagree.Checked = false;
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImpersonationHelper.Impersonate(FTPServerDomain, UploadFileUSMUserName, UploadFileUSMPassword, delegate
           {
               File.Delete(lblfileDestination.Text);
               uploadGrid.Visible = false;
           });
        }
        catch (Exception ex)
        {
            uploadGrid.Visible = true;
        }
    }
    protected void txtUserName_TextChanged(object sender, EventArgs e)
    {
        bool Isexist = false;
        
        try
        {
            Isexist = checkUsernameAlreadyExist();
            if (Isexist)
            {
                lblChkUser.Visible = true;
                lblChkUser.Text = "User name is already in use.";
                Iagree.Checked = false;
                WrongCaptcha.Style.Add("visibility", "hidden");
                txtUserName.Focus();
                return;
            }
            else
            {
                lblChkUser.Visible = false;
                lblChkUser.Text = "";
            }

        }
        catch (Exception ex)
        {
            txtUserName.Focus();
            lblChkUser.Text = "";
            lblChkUser.Visible = false;
        }
    }

    protected void txtEmailId_TextChanged(object sender, EventArgs e)
    {
        bool Isexist = false;
        
        try
        {
            Isexist = CheckEmailAlreadyExist();
            if (Isexist)
                {
                lblChkEmail.Visible = true;
                lblChkEmail.Text = "Email is already in use.";
                Iagree.Checked = false;
                WrongCaptcha.Style.Add("visibility", "hidden");
                txtEmailId.Focus();
                return;
            }
            else
            {
                lblChkEmail.Text = "";
                lblChkEmail.Visible = false;
            }
                
        }
        catch (Exception ex)
        {
            txtEmailId.Focus();
            txtEmailId.Text = string.Empty;
            lblChkEmail.Text = "";
            lblChkEmail.Visible = false;
    }
    }

    protected void btnOk_Click(object sender, EventArgs e)
        {
        Response.Redirect("ProviderManageUser.aspx", true);
    }
}