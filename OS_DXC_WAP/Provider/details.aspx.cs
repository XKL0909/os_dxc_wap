﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

public partial class Provider_details : System.Web.UI.Page
{

    public bool _show = false;
    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        SqlConnection connection = null;


        
        string fullname = "";
        try
        {
            try
            {
                connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
               
            }
            // Set up parameters (2 input and 3 output) 
            SqlParameter[] arParms = new SqlParameter[2];
            arParms[0] = new SqlParameter("@RefNo", SqlDbType.BigInt);
            arParms[0].Value = Request["refno"];

            // @QtyPerUnit Output Parameter
            arParms[1] = new SqlParameter("@Status", SqlDbType.NVarChar, 50);
            arParms[1].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "SelectServiceCode", arParms);

            if (!string.IsNullOrEmpty(Convert.ToString(arParms[1].Value)))
            {
                _show = true;
            }
           
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

           
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        Label1.Text = "<b>Provider Name</b> :" + Convert.ToString(Session["ProviderName"]);
        Label2.Text = "<b>Provider Code</b> :" + Convert.ToString(Session["ProviderID"]);
    }
    protected void uploadfiles_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        if (uploadfiles.IsValid)
        {
            string filename = uploadfiles.PostedFile.FileName;
            int lastSlash = filename.LastIndexOf("\\");
            string trailingPath = filename.Substring(lastSlash + 1);
            string fullPath = Server.MapPath("../") + "\\backend\\uploads\\" + trailingPath;
            Session["_path"] = fullPath;
            uploadfiles.PostedFile.SaveAs(fullPath);

            SqlConnection connection = null;
            try
            {
                connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
               // lblError.Text = "The connection with the database can´t be established. Application Error";

            }


            SqlParameter[] arParms = new SqlParameter[2];

            // @ProductID Input Parameter 
            // assign value = 1
            arParms[0] = new SqlParameter("@RefNo", SqlDbType.BigInt);
            arParms[0].Value = Convert.ToInt64(Request["refno"]);

            arParms[1] = new SqlParameter("@DocPath", SqlDbType.NVarChar, 400);
            arParms[1].Value = trailingPath;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "[Insert_ServiceDoc]", arParms);
            lblMsg.Text = "File uploaded successfully";
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        }
        catch
        {


        }
        SqlParameter[] arParms = new SqlParameter[3];
        arParms[0] = new SqlParameter("@RefNo", SqlDbType.BigInt);
        arParms[0].Value = Convert.ToInt64(Request["refno"]);

        arParms[1] = new SqlParameter("@Status", SqlDbType.NVarChar, 50);
        arParms[1].Value = "For Confirmation";

        arParms[2] = new SqlParameter("@BupaUser", SqlDbType.NVarChar, 10);
        arParms[2].Value = Convert.ToString(Session["user"]);

        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Update_ServiceCodeStatus", arParms);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "myCloseScript", "window.close()", true);
        if (connection.State == ConnectionState.Open)
        {
            connection.Close();
        }
    }
}