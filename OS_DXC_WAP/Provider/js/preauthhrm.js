﻿function anyCheck1() {

    try {
        var e = document.getElementById("ddlServiceDesc");
        var val = document.getElementById("tblServPA").rows[e.selectedIndex + 1].cells[4].innerHTML;
        if (document.getElementById('ddlDepartment').value == 'OER' && val == 'O3') {
            var total = 0;
            var list = document.getElementsByTagName("input");
            for (var i in list) {
                if (list[i].type == "checkbox" && list[i].checked)
                    total = total + 1;
            }
            if (total > 0)
            { return true; }
            else
            {
                alert("Please select specification from the list.");
                return false;
            }
        }
    } catch (err) { }
}
function ConfirmDialogFollowUpRequest(obj, ctl) {            
    var _mVal = document.getElementById(ctl).value;
    if (_mVal == 'DEN' || _mVal == 'OER' || _mVal == 'OGP' || _mVal == 'VAC' || _mVal == 'PHA') {
        document.getElementById('val').value = "";
        return true;
    } else {
        if (!dialogConfirmed) {


            $('#dialog-confirm').dialog
        ({
            height: 310,
            width: 650,
            modal: true,
            resizable: false,
            draggable: false,

            buttons:
            {
                'Yes': function () {
                    $(this).dialog('close');
                    dialogConfirmed = true;
                    document.getElementById('val').value = "Y"; 
                    if (obj) obj.click();
                },
                'No': function () {
                    $(this).dialog('close');
                    dialogConfirmed = true;
                    document.getElementById('val').value = "N"; 
                    if (obj) obj.click();
                }
            }
        });
        }

        return dialogConfirmed;
    }
}

var app = angular.module('myHRMApp', []);
app.controller('myHRMCtrl', function($scope) {
    // $scope.txtQuantity = "1";
    //$scope.txtCost = "1";
    $scope.txtCost =  $("#txtServiceDesc").val();
});
          
app.controller('BupaCodeCtrl', function($scope, $http) {
    //$http.get("http://www.w3schools.com/angular/customers.php")
           
});

function loadHRMServices(){
    document.getElementById("ddlServiceDesc").selectedIndex=0;
    $("#txtServiceDesc").val("");
    if( $("#tblBupaCodeList").html().length>10){
        $('#txtPASearch').val('');
        fnPASearch();
        $('#tblBupaCodeList').css("display", "block");
        $('#txtPASearch').focus();
    }else{
        ValidateToothSite('');
        $('#txtPASearch').val('');
        $('#txtPASearch').focus();
    }
    //$("#txtServiceDesc").val("");
    //$("#txtServiceDesc").css('display','block');
    //$("ddlServiceDesc").css('display','none');
    //
}

function BupaCodeList(result){
    //alert(JSON.stringify(result.d));
    var htmlContent="";
    var obj = result.d;
    htmlContent="<input type='text' id='txtPASearch' onkeyup='fnPASearch()'  size='48'><input type='button' value='X' id='btnClose' onclick='HideBupaCodeList()'><table id='tblServPA' class='display' cellspacing='0' width='100%'><thead style='text-align:left;background-color: skyblue;'><tr><th>Service Description</th><th>Service Code</th><th>Amount</th><th style='display:none;'>ICD Code</th><th style='display:none;'>Benefit Head</th><th style='display:none;'>EyeTest Indicator</th><th style='display:none;'>ToothSite Indicator</th><th style='display:none;'>Supply Indicator</th></tr></thead>";
    htmlContent+="<tfoot  style='text-align:left;background-color: skyblue;'><tr><th>Service Description</th><th>Service Code</th><th>Amount</th><th style='display:none;'>ICD Code</th><th style='display:none;'>Benefit Head</th><th style='display:none;'>EyeTest Indicator</th><th style='display:none;'>ToothSite Indicator</th><th style='display:none;'>Supply Indicator</th></tr></tfoot><tbody>";
    $.each(obj, function(i, item) {
        htmlContent+="<tr onmouseover='ChangeBackgroundColor(this)' onclick='ActionBupaCode(this)' onmouseout='RestoreBackgroundColor(this)'><td>"+item.ServiceDesc+"</td><td>"+item.ServiceCode+"</td><td>"+item.Amount+"</td><td style='display:none;'>"+item.ICDCode+"</td><td style='display:none;'>"+item.BenefitHead+"</td><td style='display:none;'>"+item.EyeTestIndicator+"</td><td style='display:none;'>"+item.ToothSiteIndicator+"</td><td style='display:none;'>"+item.SupplyIndicator+"</td></tr>";
                 
    });
    htmlContent+=" </tbody></table>";
        
    $("#tblBupaCodeList").html(htmlContent);
    $('#tblBupaCodeList').css("display", "block");
            
    //$('#ddlServiceDesc').popModal({ html : $('#tblBupaCodeList').html(), placement : 'bottomLeft', showCloseBut : true, onDocumentClickClose : true, onOkBut : function(){ }, onCancelBut : function(){ }, onLoad : function(){ }, onClose : function(){ } });
             


}
var TableBackgroundNormalColor = "white";
var TableBackgroundMouseoverColor = "skyblue";
      
        
      
// These functions need no customization.
function HideBupaCodeList()
{  
    $('#tblBupaCodeList').css("display", "none");
    if($("#ddlTreatmentType").val()=="I")
    {
        $("#txtCost").val('');
        $("#txtQuantity").val('1')
    }else{
        $("#txtCost").val('');
        $("#txtQuantity").val('')
    }
    var myVal = document.getElementById('RFVldtrTeethmap');
    myVal.enabled = false;
    document.getElementById("PanelDental").style.display = "none";
    document.getElementById('txtTeethMap').value = '';
}
function fnPASearch() {
    var x = document.getElementById("txtPASearch");
    //x.value = x.value.toUpperCase();
    var letter = x.value;
    //document.getElementById("tblServPA").rows[e.selectedIndex+1].cells[6]
    //$('#tblServPA td').css("display", "none");
    /*
    $( "#tblServPA tr" ).css( "background-color", "white" );
    $( "#tblServPA tr:contains('"+ x.value.toUpperCase() +"')" ).css( "background-color", "skyblue" );
    */
    if(x.value.trim()=="" || x.value.trim().length==0){
        $( "#tblServPA tr" ).show();
    }else{
        $( "#tblServPA tr" ).hide();
        $( "#tblServPA tr:icontains('"+ x.value.toUpperCase() +"')" ).show();
    }
    var e = document.getElementById("ddlServiceDesc");
    e.options[0].text="-- Select --";
    e.selectedIndex=0;
    $("#txtServiceDesc").val("");
}
jQuery.expr[':'].icontains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};
function ActionBupaCode(row) { 
    $('#tblBupaCodeList').css("display", "none");
    var e = document.getElementById("ddlServiceDesc");
    var selectedVal=row.cells[0].innerHTML+"-"+row.cells[1].innerHTML+"-"+row.cells[3].innerHTML;
    //e.options[e.selectedIndex].text=selectedVal;
    if(e.options[0].text.includes("Select"))
        e.selectedIndex=row.rowIndex;
    else
        e.selectedIndex=row.rowIndex-1;

    //alert(row.rowIndex +"---" + e.selectedIndex +"---" +e.options[e.selectedIndex].text+"---" +e.options[e.selectedIndex].value);
    e.options[e.selectedIndex].value=row.cells[1].innerHTML;
    //alert(e.selectedIndex +"---" +e.options[e.selectedIndex].text+"---" +e.options[e.selectedIndex].value);
    $("#txtServiceDesc").val(e.options[e.selectedIndex].text);
    $("#txtServiceDesc").css('display','none');
    //$("ddlServiceDesc").css('display','none');
    calcPrice();
    //service description=0,service code=1,amount=2,icdcode=3,BenefitHead=4,EyeTestIndicator=5,ToothSiteIndicator=6,SupplyIndicator=7
    if($("#ddlDepartment").val()=='DEN')
    {
        if(row.cells[6].innerHTML=='Y'){
                      
            document.getElementById("PanelDental").style.display = "block";
            document.getElementById('txtTeethMap').value = '';
            var myVal = document.getElementById('RFVldtrTeethmap');
            myVal.enabled = true;
        }else {
                      
            var myVal = document.getElementById('RFVldtrTeethmap');
            myVal.enabled = false;
            document.getElementById("PanelDental").style.display = "none";
            document.getElementById('txtTeethMap').value = '';
        }

    }
    if($("#ddlDepartment").val()=='OER')
    {    
        var myVal = document.getElementById('vldtrRFddlLenseType');
        var rfValLeftEye = document.getElementById('rfvddlEyeTestReadingLeft');
        var rfValRightEye = document.getElementById('rfvddlEyeTestReadingRight');
        rfValLeftEye.enabled = false;
        rfValRightEye.enabled = false;
        myVal.enabled = false;
        //vldtrRFddlLenseType
        $("#Pane1").css('display','none');
        var val = row.cells[4].innerHTML;
        var EyeTestReading = row.cells[5].innerHTML;
        //alert(EyeTestReading);
                     
                         
        if (val == 'O5') {
            document.getElementById("Pane1").disabled = true;
            $("#Pane1").css('display','none');
            var select = document.getElementById('ddlLensType');
            select.options.length = 0;
            select.options[select.options.length] = new Option("Select", "0");
            select.options[select.options.length] = new Option("Disposable", "D");
            select.options[select.options.length] = new Option("Permanent", "P");
            myVal.enabled = true;
        }
        if (val == 'O3') {
            document.getElementById("Pane1").disabled = false;
            $("#Pane1").css('display','block');
            var select = document.getElementById('ddlLensType');
            select.options.length = 0;
            select.options[select.options.length] = new Option("Select", "0");
            select.options[select.options.length] = new Option("None", "N");
            select.options[select.options.length] = new Option("Glass", "G");
            select.options[select.options.length] = new Option("Plastic", "P");
            myVal.enabled = true;

        }
        if(row.cells[5].innerHTML=='Y')
        {
            rfValLeftEye.enabled = true;
            rfValRightEye.enabled = true;
            $("#rfValLeftEye").css('display','block');
            $("#rfValRightEye").css('display','block');
            $("#PnlOpticalHRM").css('display','block');
            $("#ddlEyeTestReadingLeft")[0].selectedIndex = 0;
            $("#ddlEyeTestReadingRight")[0].selectedIndex = 0;
                             
        }
        else
        {
            rfValLeftEye.enabled = false;
            rfValRightEye.enabled = false;
            $("#rfValLeftEye").css('display','none');
            $("#rfValRightEye").css('display','none');
            $("#PnlOpticalHRM").css('display','none');
                          
            document.getElementById("Pane1").disabled = true;
            $("#Pane1").css('display','none');
        }
                     
                    
        if (val == 'O1' || val == 'O2' || val == 'O6') {
            // document.getElementById("Pane1").disabled = false;
            var select = document.getElementById('ddlLensType');
            select.options.length = 0;
            select.options[select.options.length] = new Option("Select", "");

            //var myVal = document.getElementById('vldtrRFddlLenseType');
            myVal.enabled = false;

            //alert("regular");
        }


    }
              

}
function calcPrice()
{

    try {
        var TotalCost = 0;
        var e = document.getElementById("ddlServiceDesc");
        //alert('hello');
        var strText = e.options[e.selectedIndex].text;
        document.getElementById("txtCost").value = strText.split("-")[2];
        if(document.getElementById("txtQuantity").value=="")
            document.getElementById("txtQuantity").value=1;
        TotalCost =  parseInt(document.getElementById("txtQuantity").value) *  parseInt(strText.split("-")[2]);
        document.getElementById("txtCost").value =TotalCost;
                  
    }
    catch(err) {
        alert(err.message);
    }
}
function ChangeBackgroundColor(row) { row.style.backgroundColor = TableBackgroundMouseoverColor; }
function RestoreBackgroundColor(row) { row.style.backgroundColor = TableBackgroundNormalColor; }
function calcCostHRM()
{
    calcPrice();
}
function ValidateToothSite(BupaCode) {
    var pId=$("#hidProvider").val();
    var sDept =$("#ddlDepartment").val();
            
    var status=0;
    var codesB="";
    var obj;
    if(sDept=='OER')
        sDept='OPT';
    //alert(pId + "--" + sDept);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "BupaCaesar.asmx/GetBupaCodeObject",
        data: "{ 'sProviderID': '"+ pId +"','sDepartment':'"+ sDept +"'}",
        dataType: "json",
        success: function (result) {
            obj = result.d;
            $.each(obj, function(i, item) {
                //codesB+=","+item.ServiceCode+"="+BupaCode;
                //$("#txtLogs").val($("#txtLogs").val()+'-'+item.ServiceCode)
                if(BupaCode==item.ServiceCode){
                    $("#txtLogs").val(item.ServiceCode);
                    status=1;
                }
                         
            });
            BupaCodeList(result);
            //alert(JSON.stringify(obj));
        },
        error: function (result) {
            alert("Error"+result);
        },
        async: false
    });
            
    return status;
}
function hrmBupaCodeValues(x, val) {
              
    try {
        var e = document.getElementById("ddlServiceDesc");
        var strText = e.options[e.selectedIndex].text;
        if(!isNaN(strText.split("-")[2])){
            document.getElementById("txtCost").value = strText.split("-")[2];
        }
        document.getElementById("txtQuantity").value = '1';
        var toothInd='N';
        try{
            if( $("#tblBupaCodeList").html().length>10){
                $('#txtPASearch').val('');
                //fnPASearch();
                $('#tblBupaCodeList').css("display", "none");
            }else{
                ValidateToothSite('');
                $('#tblBupaCodeList').css("display", "none");
            }
                  
        }catch(err){
            //ValidateToothSite('');
        }

        if (document.getElementById('ddlDepartment').value == 'DEN') {
            toothInd=document.getElementById("tblServPA").rows[e.selectedIndex+1].cells[6].innerHTML;
            if(toothInd=='Y')
            {
                document.getElementById("PanelDental").style.display = "block";
                document.getElementById('txtTeethMap').value = '';
                var myVal = document.getElementById('RFVldtrTeethmap');
                myVal.enabled = true;
            }else {
                var myVal = document.getElementById('RFVldtrTeethmap');
                myVal.enabled = false;
                document.getElementById("PanelDental").style.display = "none";
                document.getElementById('txtTeethMap').value = '';
            }
        }

        //Eye Validation
        if($("#ddlDepartment").val()=='OER')
        {    
            var myVal = document.getElementById('vldtrRFddlLenseType');
            var rfValLeftEye = document.getElementById('rfvddlEyeTestReadingLeft');
            var rfValRightEye = document.getElementById('rfvddlEyeTestReadingRight');
            rfValLeftEye.enabled = false;
            rfValRightEye.enabled = false;
            myVal.enabled = false;
            //vldtrRFddlLenseType
            $("#Pane1").css('display','none');
            var val = document.getElementById("tblServPA").rows[e.selectedIndex+1].cells[4].innerHTML;;
            var EyeTestReading = document.getElementById("tblServPA").rows[e.selectedIndex+1].cells[5].innerHTML;;
            //alert(val);

            if (val == 'O5') {
                document.getElementById("Pane1").disabled = true;
                $("#Pane1").css('display','none');
                var select = document.getElementById('ddlLensType');
                select.options.length = 0;
                select.options[select.options.length] = new Option("Select", "0");
                select.options[select.options.length] = new Option("Disposable", "D");
                select.options[select.options.length] = new Option("Permanent", "P");
                myVal.enabled = true;
            }
            if (val == 'O3') {
                document.getElementById("Pane1").disabled = false;
                $("#Pane1").css('display','block');
                var select = document.getElementById('ddlLensType');
                select.options.length = 0;
                select.options[select.options.length] = new Option("Select", "0");
                select.options[select.options.length] = new Option("None", "N");
                select.options[select.options.length] = new Option("Glass", "G");
                select.options[select.options.length] = new Option("Plastic", "P");
                myVal.enabled = true;

            }
            if(document.getElementById("tblServPA").rows[e.selectedIndex+1].cells[5].innerHTML=='Y')
            {
                rfValLeftEye.enabled = true;
                rfValRightEye.enabled = true;
                $("#rfValLeftEye").css('display','block');
                $("#rfValRightEye").css('display','block');
                $("#PnlOpticalHRM").css('display','block');
                $("#ddlEyeTestReadingLeft")[0].selectedIndex = 0;
                $("#ddlEyeTestReadingRight")[0].selectedIndex = 0;
                             
            }
            else
            {
                rfValLeftEye.enabled = false;
                rfValRightEye.enabled = false;
                $("#rfValLeftEye").css('display','none');
                $("#rfValRightEye").css('display','none');
                $("#PnlOpticalHRM").css('display','none');
                          
                document.getElementById("Pane1").disabled = true;
                $("#Pane1").css('display','none');
            }
                     
                    
            if (val == 'O1' || val == 'O2' || val == 'O6') {
                // document.getElementById("Pane1").disabled = false;
                var select = document.getElementById('ddlLensType');
                select.options.length = 0;
                select.options[select.options.length] = new Option("Select", "");

                //var myVal = document.getElementById('vldtrRFddlLenseType');
                myVal.enabled = false;

            }


        }


    }
    catch(err) {
        alert(err.message);
    }
}


$(document).ready(function () {
    alert('ready');
    $(window).load(function () {
        alert('load');
        if($("#hidHRM").val()==1){
            if($("#ddlDepartment").val()=='DEN' || $("#ddlDepartment").val()=='OER' ){
                $('#btnSelectService').css("display", 'block');
                $("#rfValLeftEye").css('display','none');
                $("#rfValRightEye").css('display','none');
                $("#txtServiceDesc").css('display','none');
            }
        } } ); //load everything including images.  
});
        

