﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="eclaims" Codebehind="eclaims.aspx.cs" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register src="../Uploader.ascx" tagname="Uploader" tagprefix="uc1" %>

<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">

<script type="text/javascript">
    function child_openurl(url) {

        popupWindow = window.open(url, "_blank", 'height=600,width=900,left=50,top=50,resizable=yes,scrollbars=1,toolbar=no,menubar=no,location=no,directories=no,status=no');

    }

</script>
<fieldset>
                <legend><h1>Submit e-Claims</h1></legend>                 
    <table >
       
        <tr>
            <td>
                <a href="../Docs/Sample%20eClaims.xls">Download e-Claim Sample</a></td>
        </tr>
        <tr>
            <td>
                When submitting the e-Claim, please ensure e-Claim sheet is completely filled with 
                accurate information.</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td  align="right">&nbsp;</td>
                        <td align="left">
                            &nbsp;</td>
                        <td align="left">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" runat="server" id="Uploaderlink"  align="center">
                            <%--<a href="#" onclick="child_openurl('../SimpleUpload.aspx?UploadCategory=<%=_UploadCategory %>&UserID=<%=_Username%>&RequestID=<%=_InitialRequestID %>&t=pro');">Please Click here to upload Documents</a>--%>
                            <%
                                _urlValues = "UploadCategory=" + _UploadCategory + "&UserID="+_Username+"&RequestID="+_InitialRequestID+"&t=pro";
                                _urlValues = Cryption.Encrypt(_urlValues);

                             %>
                            <a href="#" onclick="child_openurl('../UploadRevamp.aspx?val=<%=_urlValues%>');">Please Click here to upload Documents</a>

                        </td>
                    </tr>
                    <tr>
                        <td style="width: 156px">&nbsp;</td>
                        <td align="right">
                            &nbsp;</td>
                        <td align="right">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center">
                        
                <asp:Label ID="lblMsg" runat="server" Text="" 
                    style="color: #FF0000; font-weight: 700"></asp:Label>
           
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <uc2:OSNav ID="OSNav1" runat="server" />
                        </td>
                    </tr>
                </table></td>
        </tr>
        </table>
        </fieldset>
</asp:Content>


