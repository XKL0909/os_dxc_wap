﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ClaimForm" Codebehind="ClaimForm.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta content="en-gb" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>UCAF</title>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>

    <style>
        @media print {
            .noPrint {
                display: none;
            }

            .Hidden {
                display: inline;
            }
        }

        .Hidden {
            display: none;
        }

        @page {
            size: auto; /* auto is the current printer page size */
            margin: 10mm; /* this affects the margin in the printer settings */
            margin-top: 8mm;
            margin-right: 10mm;
        }

        body #main {padding: 0; position: static;}
    </style>
<%--    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">--%>
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="styles.css" type="text/css" />


    <style type="text/css">
        input.checkbox {
            width: 0.5em;
            height: 0.5em;
            padding: 0px;
            margin: 0px;
        }
    </style> 

<style type="text/css">
    .style1 {
        font-size: xx-large;
        font-family: Arial;
        text-align: center;
    }

    p.MsoNormal {
        margin-bottom: .0001pt;
        font-size: 10.0pt;
        font-family: Arial;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
    }

    h5 {
        margin-bottom: .0001pt;
        text-indent: 36.0pt;
        page-break-after: avoid;
        font-size: 9.0pt;
        font-family: Arial;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
    }

    .style2 {
        border: 1px solid #000000;
    }

    h3 {
        margin-bottom: .0001pt;
        page-break-after: avoid;
        font-size: 8.5pt;
        font-family: Arial;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
    }

    .style3 {
        border: 1px solid #808080;
    }

    .style4 {
        text-align: center;
        font-family: Arial;
        font-size: 11px;
        border: 1px solid #808080;
    }

    h4 {
        margin-bottom: .0001pt;
        page-break-after: avoid;
        font-size: 8.0pt;
        font-family: Arial;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
    }

    .style5 {
        text-align: justify;
        font-size: 9.0pt;
        font-family: Arial;
        font-weight: bold;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
        margin-bottom: .0001pt;
    }

    .style6 {
        text-align: justify;
        font-size: 9.0pt;
        font-family: "Arial";
        font-style: italic;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
        margin-bottom: .0001pt;
    }

    .style7 {
        font-size: 9.0pt;
        font-family: Arial;
        font-style: italic;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
        margin-bottom: .0001pt;
    }

    .style9 {
        text-align: center;
        font-size: 12.0pt;
        font-family: "Times New Roman";
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
        margin-bottom: .0001pt;
    }

    .style10 {
        font-size: 12.0pt;
        font-family: Arial;
        margin-left: 0cm;
        margin-right: 0cm;
        margin-top: 0cm;
        margin-bottom: .0001pt;
    }

    .style12 {
        border-collapse: collapse;
        font-size: 11px;
        font-family: Arial;
        border-style: none;
        border-color: inherit;
        border-width: medium;
        margin-left: .9pt;
    }

    .style13 {
        font-weight: bold;
        background-color: #06AFF2;
        padding-top: 2px;
        padding-bottom: 2px;
        font-size: 7.5pt;
        font-family: Arial;
        width:90.0pt;border:solid windowtext 1.0pt;
         border-top:none;
         padding:0cm 3.4pt 0cm 3.4pt ;
    }

     .style13EmplyColumn {
        font-weight: bold;
        background-color: #06AFF2;
        padding-top: 2px;
        padding-bottom: 2px;
        font-size: 7.5pt;
        font-family: Arial;
        width:90.0pt;border:solid windowtext 1.0pt;
         border-top:none; 
    }
    .styletdBorder{
          width:99.0pt;border:solid windowtext 1.0pt;
         border-top:none; 
         vertical-align:top;
          padding:0cm 3.0pt 0cm 3.0pt ;
    }
    .style14 {
        border: 1px solid #000000;
        text-align: center;
        background-color: #06AFF2;
    }

    .style16 {
        border-collapse: collapse;
        font-size: 9.0pt;
        font-family: Arial;
        border-style: none;
        border-color: inherit;
        border-width: medium;
        margin-left: -.6pt;
        padding-top: 2px;
        padding-bottom: 2px;
        padding-right: 2px;
    }

    .style17 {
        border: 1px solid #808080;
        border-collapse: collapse;
    }
</style>
    <link href="../css/PrintForm.css" rel="stylesheet" media="print" type="text/css" />
</head>

<body>
    <div class="noPrint" style="width:850px;" >
      
        <table width="800px" ><tr><td align="left">
            <label ID="Label1" ><font size=4>Claim Form</font></label>  </td>
            <td><a href="#" onclick="window.print()"><img border="0" src="images/printer.gif" alt="Print" width="16" height="16" >
            </a><img alt="Go back" style="cursor:pointer;" src="../icons/back4.png" onClick="history.go(-1)"  /></td></tr></table>
    </div>
    <div style="width:850px;" >
       
<table style="width: 850px;">
    <tr>
		<td  style="text-align:center;font-size:xx-large;width:90%;">
            
		<strong >
            <asp:Label ID="lblPageTitle" runat="server" Text="UCAF 1.0"></asp:Label></strong> </td>
        <td style="text-align:right;">
            <img src="http://onlineservices.bupa.com.sa/images/logo-new.jpg" width="50" height="50">
        </td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:right">
              <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Arabia Customer Service Toll Free No. 800 244 0307</span>
        </td>
    </tr>
</table>
          
    </div>
            
       
  

 <form id="form1" runat="server" >
      <div style="width:850px;" >
<table style="width: 850px;font-family: Arial;font-size: 8.0pt;">
	<tr>
        <td colspan="3" style="padding-left:2px;">
            <table style="width: 850px;">
	<tr>
		<td class="style2" style="width: 310px; vertical-align:top;">
        
            <table width="310px" >
                <tr>
                    <td colspan="2">
                          <b>  <u>
		<p class="MsoNormal"  >
		<i style="mso-bidi-font-style: normal"><b>
		<span lang="EN-US" style="font-size: 9.0pt; font-family: Arial; color: black; mso-bidi-font-weight: bold">
		To be Completed by Reception/Nurse </span></b></i> </p></u></b>
                    </td>
                </tr>
                <tr>
                    <td width="80px;" style="vertical-align:top;">
 
		Provider Name
                    </td>
                    <td width="215px">
                        <asp:Label ID="lblProviderName" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Insurance Co.
                    </td>
                    <td>
                        <asp:Label ID="lblInsuranceCo"
                runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td>
Department
                    </td>
                    <td>
                         <asp:Label ID="lblDepartment" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td>
                       Patient File No
                    </td>
                    <td>
                           <asp:Label ID="lblPatientFileNo" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td>
                        Visit
                    </td>
                    <td>
                         <asp:Label ID="lblDateOfVisit" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Martial Status
                    </td>
                    <td>
                           Single &#124;  Married           
	
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top;">Visit Type (&#10004;)</td>
                    <td>
                        New Visit &#124; Follow Up/Refill &#124; Walk-in &#124; Referral
                        
                    </td>
                </tr>
              
            </table>
		 
	</td>
		<td style="width: 2px;"></td>
		<td class="style2" style="width: 440px; vertical-align:top;" >
            <table  width="440px">
               
                        <tr>
                            <td width="80px">
		        Member Name
                            </td>
                              <td width="280px" colspan="3">
  <asp:Label ID="lblMemberName" runat="server" Font-Bold="True" ></asp:Label>
                        </td>
                            <td width="60px" rowspan="4">
                                 <asp:Image ID="imgQRCODE" runat="server" Height="60px" Width="60px"  />   
                            </td>
                        </tr>
                        <tr>
                            <td> Bupa Card No.</td>
                            <td width="80px"><asp:Label ID="lblBupaCardNo" runat="server" Font-Bold="True" ></asp:Label></td>
                            <td width="80px">Member type </td>
                            <td width="120px">
                                 <asp:Label ID="lblMemberType" runat="server" Font-Bold="True"></asp:Label>
		        </td>
                </tr>
                         <tr>
                            <td>  Effective from</td>
                            <td> <asp:Label ID="lblEffectiveFrom" runat="server" Font-Bold="True"></asp:Label></td>
                            <td>  Effective To </td>
                            <td>
                                 <asp:Label ID="lblEffectiveTo" runat="server" Font-Bold="True"></asp:Label>
         </td>
	</tr>
	<tr>
                            <td>DOB</td>
                            <td>  <asp:Label
                    ID="lblDOB" runat="server" Font-Bold="True"></asp:Label></td>
                            <td> Age</td>
                            <td>
                                   <asp:Label ID="lblAge" runat="server" Font-Bold="True"></asp:Label>
		</td>
	</tr>
   <tr>
                            <td style="vertical-align:top;">  Policy No </td>
                            <td style="vertical-align:top;"><asp:Label ID="lblPolicyNo" runat="server" Font-Bold="True"></asp:Label></td>
                            <td style="vertical-align:top;"> Policy Holder </td>
                            <td colspan="2">
                                 <asp:Label ID="lblPolicyHolder" runat="server" Font-Bold="True"></asp:Label>
		</td>
	</tr>
    <tr>
                           
                            <td style="vertical-align:top;">Sex</td>
                            <td style="vertical-align:top;">
                                <asp:Label
                        ID="lblGender" runat="server" Font-Bold="True"></asp:Label>
        </td>
         <td style="vertical-align:top;">Scheme</td>
                            <td colspan="2"><asp:Label ID="lblScheme" runat="server" Font-Bold="True"></asp:Label></td>
                        </tr>
                         <tr>
                            <td>Verification ID </td>
                            <td>    <asp:Label ID="lblVerificationID" runat="server" Font-Bold="True" ></asp:Label></td>
                            <td> Status </td>
        <td>
                                 <asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;">  Remarks </td>
                            <td colspan="3"><asp:Label ID="lblRemarks" runat="server" Font-Bold="True"></asp:Label> </td>
                        </tr>
                    
           </table>
         </td>
	</tr>	
            </table>
        </td>
	</tr>

   
    <tr>
        <td colspan="3" style="padding-left:2px;">
            <table style="width: 850px;">
                <tr>
                    <td>

        <div id="ServiceListReport"  visible="true" style=" font-family:Arial; font-size:small ; height:auto; width:846px; " runat="server" >   </div>
                    </td>
                </tr>
            </table>
        </td>
            
        
    </tr>
	<tr>
		<td  colspan="3" >
            <table  style="width:850px;">
                <tr>
                    <td style="width:340px;vertical-align:top;">
		<table cellpadding="0" cellspacing="0" width="340px">
			<tr >
				<th style="background-position: 0% 0%;padding-top:3px;padding-bottom:3px; width: 90.0pt; border: solid windowtext 1.0pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; background-image: none; background-repeat: repeat; background-attachment: scroll;" valign="top" width="150" class="style13" >
				<p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span lang="EN-US" style="font-size:12px;font-family:Arial;mso-no-proof:yes">
				Other Benefits<?xml namespace="" prefix="O" ?><o:p></o:p></span></p>
				</th>
				<th style="border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; background-position: 0% 0%;padding-top:3px;padding-bottom:3px; width: 154.5pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; background-image: none; background-repeat: repeat; background-attachment: scroll;" valign="top"  class="style13">
				<p align="center" class="style9" style="tab-stops: center 207.65pt right 415.3pt">
				<span lang="EN-US" style="font-size:12px;font-family:Arial;mso-no-proof:yes">
				Details<o:p></o:p></span></p>
				</th>
			</tr>
			<tr style="mso-yfti-irow:1">
				<td style="width:90.0pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt" valign="top" >
				<p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span lang="EN-US" style="font-size:11px;font-family:Arial;mso-no-proof:yes">
				Overall annual limit<o:p></o:p></span></p>
				</td>
				<td style="width:154.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top" >
                    <asp:Label ID="lblOverallAnnualLimit" runat="server"></asp:Label></td>
			</tr>
			<tr style="mso-yfti-irow:2; font-size:11px; font-family: Arial;">
				<td style="width:90.0pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt" valign="top" >
				<p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span lang="EN-US" style="font-size:11px; font-family: Arial; mso-no-proof:yes">
				Treatment type<o:p></o:p></span></p>
				</td>
				<td style="width:154.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top" >
                    <asp:Label ID="lblTreatmentType" runat="server"></asp:Label></td>
			</tr>
			<tr style="mso-yfti-irow:3">
				<td style="width:90.0pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt; font-family: Arial;" valign="top">
				<p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span lang="EN-US" style="font-size:11px;font-family:Arial;mso-no-proof:yes">
				Hospital accomodation<o:p></o:p></span></p>
				</td>
				<td style="width:154.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top"  >
                    <asp:Label ID="lblHospitalAccomodation" runat="server"></asp:Label></td>
			</tr>
			<tr style="mso-yfti-irow:4">
				<td style="width:90.0pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt" valign="top">
				<p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span lang="EN-US" style="font-size:11px;font-family:Arial;mso-no-proof:yes">
				Referral letter<o:p></o:p></span></p>
				</td>
				<td style="width:154.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top" >
                    Main member:
                    <asp:Label ID="lblReferralLetterM" runat="server"></asp:Label>&nbsp;/&nbsp;
                    Dependent: 
                    <asp:Label ID="lblReferralLetterD" runat="server"></asp:Label>
                </td>
			</tr>
			<tr style="mso-yfti-irow:5">
				<td style="width:90.0pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt" valign="top" >
				<p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span lang="EN-US" style="font-size:11px;font-family:Arial;mso-no-proof:yes">
				Pre-authorisation limit<o:p></o:p></span></p>
				</td>
				<td style="width:154.5pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt" valign="top" >
                    <asp:Label ID="lblPreauthorisationLimit" runat="server"></asp:Label></td>
			</tr>
		
			<tr style="mso-yfti-irow:5">
				<td 
                    style="border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext;   mso-border-top-alt:solid windowtext .5pt; mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt; border-top-style: none; border-top-color: inherit; border-top-width: medium;" 
                    valign="top" colspan="2">
				    <p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span lang="EN-US" style="font-size:11px;font-family:Arial;fonmso-no-proof:yes;font-weight:bold;">
				Approval<o:p></o:p></span></p></td>
			</tr>
		
		</table>
       
        </td>
                    <td style="vertical-align:top;width:502px;">
		<table  cellpadding="0" cellspacing="0" width="502px" >
			<tr>
				<td class="style13" style="background-position: 0% 0%; width: 72.0pt; border: solid windowtext 1.0pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 13.5pt; background-image: none; background-repeat: repeat; background-attachment: scroll;" valign="top" width="96">
				<p align="center" class="style9" style="tab-stops: center 207.65pt right 415.3pt">
				<span style="font-size:8.0pt;font-family:Arial;mso-no-proof:yes">
				Deductible</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-ansi-language:EN-US;
  mso-fareast-language:EN-US;mso-no-proof:yes"><o:p></o:p></span></p>
				</td>
				<td class="style13" style="border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; background-position: 0% 0%; width: 119.25pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 13.5pt; background-image: none; background-repeat: repeat; background-attachment: scroll;" valign="top" width="159">
				<p align="center" class="style9" style="tab-stops: center 207.65pt right 415.3pt">
				<span style="font-size:8.0pt;font-family:Arial;mso-no-proof:yes">
				Main Member (amount)</span><span lang="EN-US" style="font-size:8.0pt;font-family:
  Arial;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-no-proof:yes"><o:p></o:p></span></p>
				</td>
				<td class="style13" style="border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; background-position: 0% 0%; width: 119.25pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 13.5pt; background-image: none; background-repeat: repeat; background-attachment: scroll;" valign="top" width="159">
				<p align="center" class="style9" style="tab-stops: center 207.65pt right 415.3pt">
				<span style="font-size:8.0pt;font-family:Arial;mso-no-proof:yes">
				Main member (%)</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-ansi-language:EN-US;
  mso-fareast-language:EN-US;mso-no-proof:yes"><o:p></o:p></span></p>
				</td>
				<td class="style13" style="border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; background-position: 0% 0%; width: 119.25pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 13.5pt; background-image: none; background-repeat: repeat; background-attachment: scroll;" valign="top" width="159">
				<p align="center" class="style9" style="tab-stops: center 207.65pt right 415.3pt">
				<span style="font-size:8.0pt;font-family:Arial;mso-no-proof:yes">
				Dependent (amount)</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-ansi-language:EN-US;
  mso-fareast-language:EN-US;mso-no-proof:yes"><o:p></o:p></span></p>
				</td>
				<td class="style13" style="border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; background-position: 0% 0%; width: 119.25pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 13.5pt; background-image: none; background-repeat: repeat; background-attachment: scroll;" valign="top" width="159">
				<p align="center" class="style9" style="tab-stops: center 207.65pt right 415.3pt">
				<span style="font-size:8.0pt;font-family:Arial;mso-no-proof:yes">
				Dependent (%)</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-ansi-language:EN-US;
  mso-fareast-language:EN-US;mso-no-proof:yes"><o:p></o:p></span></p>
				</td>
			</tr>
			<tr style="mso-yfti-irow:1;height:10.9pt">
				<td style="width:72.0pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:10.9pt" valign="top" width="96">
				<p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span style="font-size:8.0pt;font-family:Arial;mso-no-proof:yes">
				Out Patient</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-ansi-language:EN-US;
  mso-fareast-language:EN-US;mso-no-proof:yes"><o:p></o:p></span></p>
				</td>
				<td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.9pt" valign="top" width="159">
                    <asp:Label ID="lblOutPatientDeductibleMA" runat="server"></asp:Label></td>
				<td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.9pt" valign="top" width="159">
                    <asp:Label ID="lblOutPatientDeductibleMP" runat="server"></asp:Label></td>
				<td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.9pt" valign="top" width="159">
                    <asp:Label ID="lblOutPatientDeductibleDA" runat="server"></asp:Label></td>
				<td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.9pt" valign="top" width="159">
                    <asp:Label ID="lblOutPatientDeductibleDP" runat="server"></asp:Label></td>
			</tr>
			<tr style="mso-yfti-irow:2;height:10.85pt">
				<td style="width:72.0pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:10.85pt" valign="top" width="96">
				<p class="style10" style="tab-stops: center 207.65pt right 415.3pt">
				<span style="font-size:8.0pt;font-family:Arial;mso-no-proof:yes">
				In Patient</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-ansi-language:EN-US;
  mso-fareast-language:EN-US;mso-no-proof:yes"><o:p></o:p></span></p>
				</td>
				<td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.85pt" valign="top" width="159">
                    <asp:Label ID="lblInPatientDeductibleMA" runat="server"></asp:Label></td>
				<td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.85pt" valign="top" width="159">
                    <asp:Label ID="lblInPatientDeductibleMP" runat="server"></asp:Label></td>
				<td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.85pt" valign="top" width="159">
                    <asp:Label ID="lblInPatientDeductibleDA" runat="server"></asp:Label></td>
				<td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.85pt" valign="top" width="159">
                    <asp:Label ID="lblInPatientDeductibleDP" runat="server"></asp:Label></td>
			</tr>
			<tr style="mso-yfti-irow:3;mso-yfti-lastrow:yes;height:20pt">
				<td style="width:72.0pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;" valign="top" colspan="2">
				<p class="style10" >
				<span style="font-size:8.0pt;font-family:Arial;mso-no-proof:yes">
				TOB<span style="mso-spacerun:yes">&nbsp; </span>Special 
				Instruction</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
  EN-US;mso-no-proof:yes"><o:p></o:p></span></p>
				</td>
				<td colspan="3" style="border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;" valign="top" >
				<asp:Label ID="lblTobSpecialInstructionsMA" runat="server" Width="95%"></asp:Label>
			
				</td>
			</tr>
            <tr runat="server" id="trNewTOB">
                <td colspan="5">
                    <span runat="server" id="spanNewTOB" style="font-size: 7pt; font-family: Arial; font-weight: 900"><strong>* H - Hospital &nbsp; &nbsp; P - Polyclinic &nbsp;&nbsp; M - MPN </strong></span>
                </td>
            </tr>
		</table>
                    </td>
                </tr>
            </table>
		
		</td>
	</tr>
	<tr>
		<td   colspan="3" style="padding-left:3.5px;">
		<table  class="style2"  style="width: 846px;">
			<tr>
				<td style=" text-align: center;" colspan="3">
				<asp:Panel ID="PanelUCAF" runat="server">
				<h3 style="width: 790px; text-align: left;"><i style="mso-bidi-font-style:normal"><b>
				<span lang="EN-US" style="font-size:8.0pt;
font-family:Arial;">To be 
				Completed by Attending </span></b></i><u>
				<span lang="EN-US" style="font-size:8.0pt;font-family:
Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">PHYSICIAN:</span></u><span lang="EN-US" style="font-size:8.0pt"><span style="mso-spacerun:yes">&nbsp; 
				&nbsp;&nbsp;</span></span><span lang="EN-US" style="font-size:
8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;"><span style="mso-spacerun:yes">&nbsp;</span>(</span><i style="mso-bidi-font-style:normal"><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
font-weight:normal;mso-bidi-font-weight:bold"> please tick &#10004; ) </span></i>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
				<span style="mso-spacerun:yes">&nbsp;</span></span><span lang="EN-US" style="font-size:8.0pt;
font-family:Symbol;mso-ascii-font-family:Arial;mso-hansi-font-family:Arial;
mso-char-type:symbol;mso-symbol-font-family:Symbol"><span style="mso-char-type:
symbol;mso-symbol-font-family:Symbol"></span></span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;"> 
				Inpatient<span style="mso-spacerun:yes">&nbsp;&nbsp; </span>
				</span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Symbol;mso-ascii-font-family:Arial;
mso-hansi-font-family:Arial;mso-char-type:symbol;mso-symbol-font-family:Symbol">
				<span style="mso-char-type:symbol;mso-symbol-font-family:Symbol">
				</span></span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;"> 
				Outpatient<span style="mso-spacerun:yes">&nbsp; </span>\<span style="mso-spacerun:yes">&nbsp;
				</span><span style="color:red">Emergency Case ?<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;
				</span></span></span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Symbol;mso-ascii-font-family:Arial;
mso-hansi-font-family:Arial;color:red;mso-char-type:symbol;mso-symbol-font-family:
Symbol"><span style="mso-char-type:symbol;mso-symbol-font-family:Symbol"></span></span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:red"> Yes<span style="mso-spacerun:yes">&nbsp;&nbsp; </span></span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Symbol;mso-ascii-font-family:
Arial;mso-hansi-font-family:Arial;color:red;mso-char-type:symbol;mso-symbol-font-family:
Symbol"><span style="mso-char-type:symbol;mso-symbol-font-family:Symbol"></span></span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:red"> NO<span style="mso-spacerun:yes">&nbsp; </span></span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p>
				</span></h3>
				<h3 style="width: 796px; text-align: left;">
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
				BP:</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
font-weight:normal;mso-bidi-font-weight:bold">_____________ </span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
				Pulse:</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
font-weight:normal;mso-bidi-font-weight:bold"> _____________</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:blue"> </span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;
mso-bidi-font-family:&quot;Times New Roman&quot;">Temp:</span><span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
font-weight:normal;mso-bidi-font-weight:bold"> _____________<span style="color:blue"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span></span></span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
				Duration of illness: ________________________<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				&nbsp;&nbsp;&nbsp;&nbsp;</span><o:p></o:p></span></h3>
				<p class="MsoNormal" style="width: 773px; text-align: left;"><b style="mso-bidi-font-weight:normal">
				<span lang="EN-US" style="font-size:8.0pt;
font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">Chief 
				Complaint<span style="mso-spacerun:yes">&nbsp; </span>&amp; Main 
				Symptoms: </span></b>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:blue"><span style="mso-tab-count:1 dotted">
				...............................................................................................................................................................................................
				</span><o:p></o:p></span></p>
				<p class="MsoNormal" style="width: 769px; text-align: left;">
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:blue"><span style="mso-tab-count:1 dotted">
				..................................................................................................................................................................................................................................................................
				</span><o:p></o:p></span></p>
				<h3 style="width: 787px; text-align: left; ">
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
				Significant Signs: </span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:blue;font-weight:normal;
mso-bidi-font-weight:bold"><span style="mso-tab-count:1 dotted">
				.................................................................................................................................................................................................................................
				</span></span>
			</h3>
				<h3 style="width: 793px; text-align: left; ">
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
				Other Conditions </span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:blue;font-weight:normal;
mso-bidi-font-weight:bold"><span style="mso-tab-count:1 dotted">
				.................................................................................................................................................................................................................................
				</span></span><i style="mso-bidi-font-style:normal">
				<span lang="EN-US" style="font-size:8.0pt;
font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p>
				</span></i></h3>
				<h3 style="width: 786px; text-align: left; ">
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
				Diagnosis: 
				</span>
				<span lang="EN-US" style="font-size:
8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;color:blue;
font-weight:normal;mso-bidi-font-weight:bold">
				<span style="mso-tab-count:2 dotted">
				.............................................................................................................................................................................................................................................
				</span></span></h3>
			
				<p class="MsoNormal" style="width: 761px; text-align: left;">
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:red">Principal Code:<span style="mso-spacerun:yes">&nbsp; </span>
				___________<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span>2<sup>nd</sup> Code:<span style="mso-spacerun:yes">&nbsp;
				</span>___________<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span>3<sup>rd</sup> Code:<span style="mso-spacerun:yes">&nbsp;
				</span>___________<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span>4<sup>th</sup><span style="mso-spacerun:yes">&nbsp;
				</span>Code:<span style="mso-spacerun:yes">&nbsp; </span>
				___________<o:p></o:p></span></p>
			
                    <table>
                       
                        <tr>
                            <td  style="text-align:left; font-weight:bold;">
                                	 <i style="mso-bidi-font-style:normal">
				<span lang="EN-US" style="font-size:8.0pt;
font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">Please tick 
				( &#10004; ) where appropriate:</span></i>
                            </td>
                        </tr><tr><td style="text-align:left;">
               <asp:CheckBoxList RepeatColumns="9" CssClass="checkbox" runat="server" ID="chkListUCAF" RepeatDirection="Horizontal"  Font-Size="10px"   >
                   <asp:ListItem>Chronic</asp:ListItem>
                    <asp:ListItem>Congenital</asp:ListItem>
                    <asp:ListItem>RTA</asp:ListItem>
                    <asp:ListItem>Work Related</asp:ListItem>
                    <asp:ListItem >Vaccination</asp:ListItem>
                    <asp:ListItem>Check-Up</asp:ListItem>
                    <asp:ListItem>Psychiatric</asp:ListItem>
                    <asp:ListItem>Infertility</asp:ListItem>
                  <asp:ListItem>Pregnancy / Indicate LMP: ­­­­­­­­­­­_____________</asp:ListItem>
               </asp:CheckBoxList>
                        </td></tr></table>
				<p class="MsoNormal" style="width: 702px; text-align: left;">
				<i style="mso-bidi-font-style:normal">
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
				<span style="mso-spacerun:yes">&nbsp;&nbsp; </span>Suggestive 
				line(s) of management: Kindly, enumerate the recommended 
				investigations, and/or procedures for <u>Outpatient approvals 
				only</u>:<o:p></o:p></span></i></p>
                 
				<table align="center" class="style17" style="width: 93%; ">
                    
					<tr>
						<th class="style4" style="width: 83px;height:15px;"">Code</th>
						<th class="style4" style="width: 1277px">Description of 
						Service</th>
						<th class="style4" style="width: 270px">Quantity</th>
						<th class="style4" style="width: 232px">Type</th>
						<th class="style4" style="width: 187px">Cost*</th>
					</tr>
					<tr >
						<td class="style3" style="width: 83px;height:15px;"></td>
						<td class="style3" style="width: 1277px"></td>
						<td class="style3" style="width: 270px"></td>
						<td class="style3" style="width: 232px"></td>
						<td class="style3" style="width: 187px"></td>
					</tr>
					<tr>
						<td class="style3" style="width: 83px;height:15px;"></td>
						<td class="style3" style="width: 1277px"></td>
						<td class="style3" style="width: 270px"></td>
						<td class="style3" style="width: 232px"></td>
						<td class="style3" style="width: 187px"></td>
					</tr>
					<tr>
						<td class="style3" style="width: 83px;height:15px;"></td>
						<td class="style3" style="width: 1277px"></td>
						<td class="style3" style="width: 270px"></td>
						<td class="style3" style="width: 232px"></td>
						<td class="style3" style="width: 187px"></td>
					</tr>
				
					<tr>
						<td class="style3" style="width: 83px;height:15px;"></td>
						<td class="style3" style="width: 1277px"></td>
						<td class="style3" style="width: 270px"></td>
						<td class="style3" style="width: 232px"></td>
						<td class="style3" style="width: 187px"></td>
					</tr>
					<tr>
						<td class="style3" style="width: 83px;height:15px;"></td>
						<td class="style3" style="width: 1277px"></td>
						<td class="style3" style="width: 270px"></td>
						<td class="style3" style="width: 232px"></td>
						<td class="style3" style="width: 187px"></td>
					</tr>
					<tr>
						<td colspan="2" style="height:18px;"><b style="mso-bidi-font-weight:normal">
						<i style="mso-bidi-font-style:normal">
						<span lang="EN-US" style="font-size:8.0pt;
font-family:&quot;Times New Roman&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
color:black;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:
AR-SA">-<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp; </span>Is Case Management Form 
						(CMF1.0) included<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;
						</span></span></i>
						<span lang="EN-US" style="font-size:8.0pt;font-family:Symbol;mso-ascii-font-family:Arial;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Arial;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;
mso-fareast-language:EN-US;mso-bidi-language:AR-SA;mso-char-type:symbol;
mso-symbol-font-family:Symbol">
						<span style="mso-char-type:symbol;mso-symbol-font-family:
Symbol"></span></span><span lang="EN-US" style="font-size:8.0pt;font-family:
Arial;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:
AR-SA"> Yes<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>
						<span lang="EN-US" style="font-size:8.0pt;font-family:Symbol;mso-ascii-font-family:
Arial;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Arial;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;
mso-fareast-language:EN-US;mso-bidi-language:AR-SA;mso-char-type:symbol;
mso-symbol-font-family:Symbol">
						<span style="mso-char-type:symbol;mso-symbol-font-family:
Symbol"></span></span><span lang="EN-US" style="font-size:8.0pt;font-family:
Arial;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:
AR-SA"> No</span><i style="mso-bidi-font-style:normal"><span lang="EN-US" style="font-size:8.0pt;font-family:&quot;Times New Roman&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA"> </span></i></b></td>
						<td class="style2" style="width: 270px">&nbsp;</td>
						<td class="style2" style="width: 232px">&nbsp;</td>
						<td class="style2" style="width: 187px">&nbsp;</td>
					</tr>
                    
				</table>
				 
                    <table><tr>
                        <td  style="text-align:left;">
<nobr>	Please specify possible line of management when applicable:		</nobr> .............................................................................................................................................................<br/>
                    <nobr>
                    	Estimated Length of Stay: …….. days       &nbsp;&nbsp;&nbsp;&nbsp;                           Expected Date of Admission:          ……  /   ……     /  …………
		</nobr>
                        </td>
                    </tr>
                        </table>
				    </asp:Panel>
                 
				<div ID="PanelDCAF" runat="server" Visible="False">

                    <table style="width: 800px; ">
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td style="text-align: left; width: 610px;" valign="top">
				 	         <span style=" font-family: Arial; font-size: 8pt; text-decoration: underline;" ><em>
				            <b>To be Completed by Dentist</b>
                                   </em></span>	
                  
				            </td>
                                    </tr>
                                    <tr>
                         <td style="text-align:left;">
                             <span style="font-family: Arial; font-size: 8.25pt;">                   
				Duration of illness:……………(Day)</span>	
                         </td>
                     </tr>
                                    <tr>
                            <td style="text-align:left;">                         
                        <span style="font-family: Arial; font-size: 8.25pt;">Chief Complaint and Main Symptoms : <br/>
                                ……………………………………………………………………………………………………………………<br />
                            ……………………………………………………………………………………………………………………                        
                        </span>                    
                            </td>
                        </tr>
                                    <tr>
                            <td style="text-align:left;">                         
                        <span style="font-family: Arial; font-size: 8.25pt;">Significant Signs …………………………………………………………………………………………………
                        </span>                    
                            </td>
                        </tr>
                                    <tr>
                            <td style="text-align:left;">                         
                        <span style="font-family: Arial; font-size: 8.25pt;">Diagnoses  (ICD10) <br />
                            Primary ……………………………………………………………………………………………………………<br />
                            Secondary ………………………………………………………………………………………………………
                        </span>                    
                            </td>
                        </tr>
                                    <tr>
                            <td style="text-align:left;"> 
                      
                        <span style="font-family: Arial; font-size: 8.25pt;">
                            Other Conditions …………………………………………………………………………………………………                           

                        </span>
                    
                            </td>
                        </tr>
                                    <tr>
                            <td   style="text-align:left; font-weight:bold;">
                                	 <i  >
			Please tick ( &#10004;) where appropriate: </i>
                            </td>
                        </tr>
                                    <tr>
                            <td   style="text-align:left;">
                                ( ) Regular Dental Treatment	
                            </td>
                        </tr>
                                    <tr>
                            <td   style="text-align:left;">
                                ( ) Dental Cleaning	
                            </td>
                        </tr>
                                    <tr>
                            <td   style="text-align:left;">
                                ( ) Trauma Treatment Specify: ( ) RTA  ( ) Work Related  ( ) Other
                            </td>
                        </tr>
                                    <tr>
                            <td style="text-align:left;"> 
                      
                        <span style="font-family: Arial; font-size: 8.25pt;">
                            How : ……………………………………………………………………………………………………………
                           

                        </span>
                    
                            </td>
                        </tr>
                                    <tr>
                            <td style="text-align:left;"> 
                      
                        <span style="font-family: Arial; font-size: 8.25pt;">
                            When : ……………………………………………  Where : ……………………………………………………
                           

                        </span>
                    
                            </td>
                        </tr>
                                </table>
                            </td>
                            <td><table>
                                <tr>
                                    <td><img height="330" src="FDIDentalNotations.png" width="240" alt=""  /></td>
                                </tr>
                                </table></td>
                        </tr>
                     </table>                    
                    <table>
                        <tr>
                            <td style="text-align: left;">
                                <span>Specify the recommended procedures using the tooth number as shown on the Teeth Map above</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <table align="left" class="style16" style="width: 97%;">
					<tr>
						<th class="style4" style="width: 83px;height:18px;">Code(*)</th>
						<th class="style4" style="width: 594px">Description of 
						Service</th>
						<th class="style4" style="width: 140px">Tooth No./letter</th>
						<th class="style4" style="width: 207px">Cost*</th>
					</tr>
					
				
					<tr>
						<td class="style3" style="width: 83px; height:45px;"> </td>
						<td class="style3" style="width: 594px; " > </td>
						<td class="style3" style="width: 140px; "> </td>
						<td class="style3" style="width: 207px; "> </td>
					</tr>
					<tr>
						<td style="width: 83px;height:18px;"> </td>
						<td style="width: 594px"> </td>
						<td class="style20" style="width: 140px">
                            <span style="font-family: Arial">Total</span></td>
						<td class="style2" style="width: 207px"> </td>
					</tr>
				</table> 
                                </td>
                        </tr>
                    </table>
                    <table>
                       
                        <tr>
                            <td>
                            <table align="left" class="style16" style="width: 97%;">
					<tr>
						<th class="style4" style="width: 450px;height:18px;">Medication Name (Generic Name)</th>						
						<th class="style4" style="width: 450px">Type</th>
						<th class="style4" style="width: 150px">Quantity</th>
					</tr>
					
				
					<tr>
						<td class="style3" style="width: 450px; height:45px;"> </td>
						<td class="style3" style="width: 450px; " > </td>
						<td class="style3" style="width: 150px; "> </td>
						
					</tr>
					
				</table> 
                                </td>
                        </tr>
                    </table>
                   

				<%--<table style="width: 800px;  ">
			<tr>
				<td style="text-align: left; width: 610px;" valign="top">
				 
	 
                                <span style=" font-family: Arial; font-size: 8pt; text-decoration: underline;" ><em>
				<b>To be Completed by DENTIST 
                </em></span>	
                  
				</td>
				<td rowspan="8"><img height="200" src="TeethMap.png" width="210"  /></td>
			</tr>
                     <tr>
                         <td style="text-align:left;">
                             <span style="font-family: Arial; font-size: 8.25pt;">                   
				Duration of illness:…………………………………………………………………………………………………………………</span>	
                         </td>
                     </tr>
                         <tr>
                            <td style="text-align:left;"> 
                        
                        <span style="font-family: Arial; font-size: 8.25pt;">Chief Complaint &amp; Main Symptoms : ……………………………………………………………………………………………
                        
                        </span>
                    
                            </td>
                        </tr>
                         <tr>
                            <td style="text-align:left;">
                               
                       
				 <span style="font-family: Arial; font-size: 8.25pt;">Diagnosis: ………………………………………………………………………………………………………………………… 
                    </span>
				 
                            </td>
                        </tr>
                         <tr>
                            <td style="text-align:left;"> <span style="font-family: Arial; font-size: 8.25pt;">
                       
				Principal Code: 
				___________ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				2<sup>nd</sup> Code: ___________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 3<sup>rd</sup> Code: 
			__________  </span></td>
                        </tr>
                         <tr>
                            <td style="text-align:left;">
                               
				 
                    <span style="font-family: Arial">Other Conditions : …………………………………………………………………………………………………………………</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">
                                  <table>
                        
                        <tr>
                            <td colspan="4"  style="text-align:left; font-weight:bold;">
                                	 <i  >
			Please tick ( &#10004;) where appropriate: </i>
                            </td>
                        </tr><tr><td colspan="4" style="text-align:left;">
               <asp:CheckBoxList RepeatColumns="4" runat="server" ID="ChkListDCAF" RepeatDirection="Horizontal"   >
                    <asp:ListItem>RTA</asp:ListItem>
                   <asp:ListItem>Work Related Accident</asp:ListItem>
                   <asp:ListItem>Sports Related</asp:ListItem>
                    <asp:ListItem >Orthodontics\Esthetics</asp:ListItem>
                    <asp:ListItem>Congenital\Developmental</asp:ListItem>
                    <asp:ListItem>Check-Up</asp:ListItem>
                    <asp:ListItem>Cleaning</asp:ListItem> 
               </asp:CheckBoxList>
	 
                        </td></tr></table>
			
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;"> 
                        <i style="mso-bidi-font-style: normal"><span style="font-size: 8pt; color: black;
                            font-family: Arial;"></span>
				<span lang="EN-US" style="font-size:8.0pt;font-family:Arial;">Specify the recommended investigations, and/or procedures using the tooth number as shown on the Teeth Map above</span></i> 
                            </td>
                        </tr>
			<tr>
				<td colspan="2">
				<table align="right" class="style16" style="width: 97%;">
					<tr>
						<th class="style4" style="width: 83px;height:18px;">Code</th>
						<th class="style4" style="width: 594px">Description of 
						Service</th>
						<th class="style4" style="width: 140px">Tooth No./letter</th>
						<th class="style4" style="width: 207px">Cost*</th>
					</tr>
					<tr>
						<td class="style3" style="width: 83px;height:18px;"> </td>
						<td class="style3" style="width: 594px"> </td>
						<td class="style3" style="width: 140px"> </td>
						<td class="style3" style="width: 207px"> </td>
					</tr>
					<tr>
						<td class="style3" style="width: 83px;height:18px;"> </td>
						<td class="style3" style="width: 594px"> </td>
						<td class="style3" style="width: 140px"> </td>
						<td class="style3" style="width: 207px"> </td>
					</tr>
					<tr>
						<td class="style3" style="width: 83px;height:18px;"> </td>
						<td class="style3" style="width: 594px"> </td>
						<td class="style3" style="width: 140px"> </td>
						<td class="style3" style="width: 207px"> </td>
					</tr>
					<tr>
						<td class="style3" style="width: 83px;height:18px;"> </td>
						<td class="style3" style="width: 594px"> </td>
						<td class="style3" style="width: 140px"> </td>
						<td class="style3" style="width: 207px"> </td>
					</tr>
				
					<tr>
						<td class="style3" style="width: 83px; height:18px;"> </td>
						<td class="style3" style="width: 594px; " > </td>
						<td class="style3" style="width: 140px; "> </td>
						<td class="style3" style="width: 207px; "> </td>
					</tr>
					<tr>
						<td style="width: 83px;height:18px;"> </td>
						<td style="width: 594px"> </td>
						<td class="style20" style="width: 140px">
                            <span style="font-family: Arial">Total</span></td>
						<td class="style2" style="width: 207px"> </td>
					</tr>
				</table> 
				</td>
			</tr>
                        <tr>
                            <td colspan="5" style="text-align:left;">
                                	<nobr>	Please specify possible line of management when applicable:	</nobr>		.........................................................................................................................................................<br/>
                    <nobr>
                    	Estimated Length of Stay: …….... days   &nbsp; &nbsp;&nbsp; &nbsp;                                      Expected Date of Admission:           ……  /   ……     /  …………
		</nobr>
                            </td>
                        </tr>
		<//ta--%>
	 
				</div>
				
				<div ID="PanelOCAF" runat="server" Visible="False">
                    <asp:Image ID="Image1" runat="server" ImageUrl="image3.png" /></div>

                     <table style="width: 810px;">
                <tr>

		<td class="style2" style="width: 410px;vertical-align:top;">
		<p class="style5" style="font-weight: normal;" >
		<span lang="EN-US" style="font-size:8.0pt;font-family:arial;">
		I hereby certify that ALL information mentioned are correct and that the 
		medical services shown on this form were medically indicated and necessary for the management of this case.
		<br />
           
            Dentist Signature/Stamp &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date……/……/………
		</span></p>
	
		</td>
		<td width="2px;"></td>
		<td class="style2" width="410px">
		<p class="style6" style="font-weight: normal; font-style: normal;"><span lang="EN-US" style="font-size:8.0pt;font-family:arial;">I hereby certify that all 
		statements and information provided concerning
		patient identification and the present illness or injury are TRUE</span></p>           
            <p class="style6" style="font-weight: normal; font-style: normal; text-align: left;">
           <span  lang="EN-US" style="font-size:8.0pt;font-family:arial;">Name and Relationship (if guardian) ………………………………………………</span> </p>                  
             <p class="style6" style="font-weight: normal; font-style: normal; text-align: left;">
                  <span  lang="EN-US" style="font-size:8.0pt;font-family:arial;">Signature: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date……/……/………</span>
                 </p>
		</td>
                    
                </tr>
                <tr>
		<td class="style2" colspan="3" style="text-align: left;">	
		<span style="font-weight: normal; font-style: normal; text-align: left;">(*) Provider’s Approval/Coding Staff must 
		review/code the recommended service (s) and allocate cost and complete the following:</span>
	<table style="width:100%"><tr>
        <td style="width:60%">Total cost SAR………………………………
    </td>
        <td style="width:40%">As estimated/ Package Deal</td>
	       </tr></table>
            <table style="width:100%"><tr>
                <td style="width:40%">Completed/Code By………………………………</td>
                <td style="width:20%">Signature</td>
                <td style="width:20%">Date……/……/………</td>
                </tr>
                </table>
            <table style="width:100%; font-weight: bold;"><tr>
                <td colspan="3">For Insurance Company Use Only:</td>               
                </tr>
                <tr>
                <td style="width:20%">Approved( )</td>
                <td style="width:20%">Not Approved( )</td>
                <td style="width:30%">Approval No:………………………………</td>
                <td style="width:30%">Approval Validity………………………………</td>
                </tr>
                </table>
             <table style="width:100%; font-weight: bold;"><tr>
                <td>Comments (include approved days/services if different from the requested)……………………………………………………………………………………</td>  
                </tr>
                </table>		
		</td>
	</tr>
            </table>
                    
				</td>


				
			</tr>			
			
		</table>
		</td>
	</tr>
	<%--<tr>
        <td colspan="3" style="padding-left:2px;">
            <table style="width: 850px;">
                <tr>

		<td class="style2" style="width: 420px;vertical-align:top;">
		<p class="style5" ><i style="mso-bidi-font-style:normal">
		<span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:9.0pt;font-family:&quot;Times New Roman&quot;">
		I hereby certify that ALL information mentioned are correct &amp; that the 
		medical services shown on this form were medically indicatedstyle&nbsp;&amp; necessary for the management of this case.
		</span><o:p></o:p> </i></p>
		<p class="MsoNormal"><b style="mso-bidi-font-weight:normal">
		<i style="mso-bidi-font-style:
normal"><span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:12.0pt"><o:p>
		&nbsp;</o:p>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></i></b>&nbsp;</p>
		<h4><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;">
		Physician's Signature &amp; Stamp:<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</span>______________________ <o:p></o:p></span></h4>
		</td>
		<td width="2px;"></td>
		<td class="style2" width="425px">
		<p class="style6"><b style="mso-bidi-font-weight:
normal"><span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:9.0pt;
font-style:normal;mso-bidi-font-style:italic">I hereby certify that all 
		statements &amp; information provided concerning<span style="mso-spacerun:yes">&nbsp;
		</span>patient<span style="mso-spacerun:yes">&nbsp; </span>
		identification &amp; the present illness or injury are TRUE<o:p></o:p></span></b></p>
		<p class="style6"><b style="mso-bidi-font-weight:
normal"><span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:9.0pt;
font-style:normal;mso-bidi-font-style:italic"><o:p>&nbsp;</o:p>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></b>&nbsp;</p>
		<p class="style7"><b style="mso-bidi-font-weight:normal">
		<span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:9.0pt;font-style:normal;mso-bidi-font-style:
italic">______________________________<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</span>_____________    <span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</span><o:p></o:p></span></b></p>
		<p class="style7"><b style="mso-bidi-font-weight:normal">
		<span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:9.0pt;font-style:normal;mso-bidi-font-style:
italic">Name &amp; Relationship (if guardian)<span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		</span><span style="mso-spacerun:yes">&nbsp;&nbsp; </span>Signature<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		</span>Date:  …/…/………<o:p></o:p></span></b></p>
		</td>
                    
                </tr>
            </table>

        </td>
       
	</tr>
    <tr>
        <td colspan="3" style="padding-left:2px;">
            <table width="850px;">
                <tr>
		<td class="style2" colspan="3">	
		<span style="font-size:8.0pt;">* Provider’s Approval/Coding Staff must 
		review / code the recommended service (s) &amp; allocate costand complete the following:</span>
	<table><tr><td height="1px;"></td></tr></table>
		<p class="MsoNormal">
		<span style="font-size:8.0pt;">Completed/Coded by:<b style="mso-bidi-font-weight:
normal">……………………………………..</b>&nbsp;&nbsp;Signature<b style="mso-bidi-font-weight:normal">…………………………………………………..</b>&nbsp;&nbsp;Date: 
		 ……  /   ……     /  …………</span>
		 </p>
		</td>
	</tr>
            </table>
        </td>
    </tr>
	
    <tr>
        <td colspan="3">


<p align="center" class="style9" style="tab-stops: center 207.65pt right 415.3pt;">
<span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:12.0pt">Saudi 
Medical Insurance Standardization<span style="mso-spacerun:yes"> </span></span>
<span style="mso-spacerun:yes">
<span style="font-size:8.0pt;mso-bidi-font-size:12.0pt">- Unified</span></span><span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:12.0pt"> 
Claim &amp; Approval Form ( SMIS-</span><span style="font-size:8.0pt;mso-bidi-font-size:12.0pt">U</span><span lang="EN-US" style="font-size:8.0pt;mso-bidi-font-size:12.0pt">CAF 
1.0 )<o:p></o:p></span></p>
        </td>
    <--%>
</table>
</div>
     <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
             <asp:UpdatePanel runat="server">
                 <ContentTemplate>
                     <asp:LinkButton ID="lnkSession" runat="server" />
                     <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                         OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                     </ajaxToolkit:ModalPopupExtender>
                     <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                         <div class="header">
                             <table style="width: 100%" cellpadding="0" cellspacing="0">
                                 <tr>
                                     <td style="text-align: left">
                                         <strong style="font-size: larger">Session Expiring! </strong>
                                     </td>
                                     <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                 </tr>
                             </table>
                         </div>

                         <div class="body">
                             <table cellpadding="0" cellspacing="0" style="width: 100%">
                                 <tr>
                                     <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                 </tr>
                             </table>
                         </div>

                         <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                     </asp:Panel>
                     <div id="mainContent">
                     </div>
                 </ContentTemplate>
             </asp:UpdatePanel>

</form>
</body>

</html>
