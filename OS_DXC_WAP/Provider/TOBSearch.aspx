<%@ Page Language="C#"  MasterPageFile="~/Templates/Inner.Master"  AutoEventWireup="true" Inherits="TOBSearch" Debug="true" Codebehind="TOBSearch.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register Src="../Date2.ascx" TagName="Date2" TagPrefix="uc1" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1.Export, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">

    <style type="text/css">
        .FloatingDiv{position:fixed;top:0;left:0;text-align:center;padding:200px 0 0 0;z-index:999;width:100%;height:100%;background:url('~/images/Floatingimg.png') repeat top left;}
        .Floatimage{display:block;width:100%;height:100%;vertical-align:middle;}     
    </style>

		
<STYLE type=text/css media=screen>
		
		
.DatePicker {
	BACKGROUND: url(images/cal.gif) no-repeat right center
}
DIV.DatePicker-Container { font-size:9px;
	
}
DIV.DatePicker-Wrapper {
	BORDER-RIGHT: #e0e0e0 1px solid; BORDER-TOP: #e0e0e0 1px solid; BORDER-LEFT: #e0e0e0 1px solid; BORDER-BOTTOM: #e0e0e0 1px solid; BACKGROUND-COLOR: skyblue;
}
DIV.DatePicker-Wrapper TABLE {
	
}
DIV.DatePicker-Wrapper TD {
	PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px
}
DIV.DatePicker-Wrapper TD.nav {
	
}
DIV.DatePicker-Wrapper TD.dayName {
	BORDER-RIGHT: #e0e0e0 1px solid; FONT-WEIGHT: bold; BORDER-BOTTOM: #e0e0e0 1px solid; BACKGROUND-COLOR: #eeeeee
}
DIV.DatePicker-Wrapper TD.day {
	BORDER-RIGHT: #e0e0e0 1px solid; BORDER-BOTTOM: #e0e0e0 1px solid
}
DIV.DatePicker-Wrapper TD.empty {
	BORDER-RIGHT: #e0e0e0 1px solid; BORDER-BOTTOM: #e0e0e0 1px solid
}
DIV.DatePicker-Wrapper TD.current {
	 COLOR: #ffffff; BACKGROUND-COLOR: #666666; 
}
DIV.DatePicker-Wrapper TD.dp_roll {
	BACKGROUND-COLOR: #e0e0e0
}
</STYLE>


<style type="text/css">@import "jQueryDate/jquery.datepick.css";</style>


        <script language="javascript" type="text/javascript">
            $(document).ready(function () {

                if ($("#RadioDiv input:radio:checked").val() == 2) {
                    document.getElementById('row_network').style.display = "none";
                    document.getElementById('rowPolicyNo').style.display = "none";
                    document.getElementById('rowMemberShip').style.display = "";
                    document.getElementById('roeEffectedDate').style.display = "none";
                    
                    var validator = document.getElementById('rfvMemberSearch');
                    ValidatorEnable(validator, true);

                    var validator = document.getElementById('rfvPolicyDetailsSearch');
                    ValidatorEnable(validator, false);
                }
                else if ($("#RadioDiv input:radio:checked").val() == 1) {
                    document.getElementById('row_network').style.display = "";
                    document.getElementById('rowPolicyNo').style.display = "";
                    document.getElementById('rowMemberShip').style.display = "none";
                    document.getElementById('roeEffectedDate').style.display = "";
                    var validator = document.getElementById('rfvMemberSearch');
                    ValidatorEnable(validator, false);

                    var validator = document.getElementById('rfvPolicyDetailsSearch');
                    ValidatorEnable(validator, true);
                }
                else if ($("#RadioDiv input:radio:checked").val() == 3) {
                    document.getElementById('row_network').style.display = "none";
                    document.getElementById('rowPolicyNo').style.display = "none";
                    document.getElementById('rowMemberShip').style.display = "none";
                    document.getElementById('roeEffectedDate').style.display = "";

                    var validator = document.getElementById('rfvMemberSearch');
                    ValidatorEnable(validator, false);

                    var validator = document.getElementById('rfvPolicyDetailsSearch');
                    ValidatorEnable(validator, false);


                }
                else
                {
                    document.getElementById('row_network').style.display = "none";
                    document.getElementById('rowPolicyNo').style.display = "none";
                    document.getElementById('rowMemberShip').style.display = "none";

                    var validator = document.getElementById('rfvMemberSearch');
                    ValidatorEnable(validator, false);

                    var validator = document.getElementById('rfvPolicyDetailsSearch');
                    ValidatorEnable(validator, false);
                }
            });

            $(document).ready(function () {
                $('#RadioDiv input').click(function () {
                    $("#info").text('Selected Value: ' + $("#RadioDiv input:radio:checked").val());
                    if ($("#RadioDiv input:radio:checked").val() == 2) {
                        document.getElementById('row_network').style.display = "none";
                        document.getElementById('rowPolicyNo').style.display = "none";
                        document.getElementById('rowMemberShip').style.display = "";
                        document.getElementById('rowSearchButton').style.display = "";
                        document.getElementById('roeEffectedDate').style.display = "none";

                        var validator = document.getElementById('rfvMemberSearch');
                        ValidatorEnable(validator, true);

                        var validator = document.getElementById('rfvPolicyDetailsSearch');
                        ValidatorEnable(validator, false);

                        $("#<%=txtPolicy.ClientID%>").val('');
                        
                    }
                    else if ($("#RadioDiv input:radio:checked").val() == 1) {
                        document.getElementById('row_network').style.display = "none";
                        document.getElementById('rowPolicyNo').style.display = "";
                        document.getElementById('rowMemberShip').style.display = "none";
                        document.getElementById('rowSearchButton').style.display = "none";
                        document.getElementById('roeEffectedDate').style.display = "";
                        var validator = document.getElementById('rfvMemberSearch');
                        ValidatorEnable(validator, false);

                        var validator = document.getElementById('rfvPolicyDetailsSearch');
                        ValidatorEnable(validator, true);

                        $("#<%=txtMemberSearch.ClientID%>").val('');
                    }
                    else if ($("#RadioDiv input:radio:checked").val() == 3) {
                        document.getElementById('row_network').style.display = "none";
                        document.getElementById('rowPolicyNo').style.display = "none";
                        document.getElementById('rowMemberShip').style.display = "none";
                        document.getElementById('rowSearchButton').style.display = "";
                        document.getElementById('roeEffectedDate').style.display = "";
                        var validator = document.getElementById('rfvMemberSearch');
                        ValidatorEnable(validator, false);

                        var validator = document.getElementById('rfvPolicyDetailsSearch');
                        ValidatorEnable(validator, false);

                        var s = document.getElementById('ctl00$ContentPlaceHolderLeft$txtSearchDate');
                        var currentDate = s.GetDate();
                        if (!currentDate) {
                            s.SetDate(new Date());
                        }
                    }
                });
            });

            //function ChangeGroupName() {
            //    var groupName = $("#ctl00$ContentPlaceHolderLeft$btnSearchByDate").val();
            //    if (groupName != '') {
            //        $("#ctl00$ContentPlaceHolderLeft$btnSearchByDate").attr('validationGroup', 'PolicyDetails');
            //    }
            //};
    </script>

   <script type="text/javascript">
                        function OnDropDown(s, e) {
                        var currentDate = s.GetDate();
                        if (!currentDate) {
                            s.SetDate(new Date());
                        }
                    }
    </script>



<style type="text/css">
#idletimeout { background:red; border:3px solid #FF6500; color:blue;  border-color:lime; border-width:10px;   font-family:arial, sans-serif; text-align:center; font-size:27px; padding:10px; position:relative; top:0px; left:0; right:0; z-index:100000; display:none; }
#idletimeout a { color:#fff; font-weight:bold; font-size:17px; }
#idletimeout span { font-weight:bold; font-size:33px; }
    .style1
    {
        width: 97px;
    }
</style>

<link rel="stylesheet" type="text/css" href="../javascript/examples1.css" />

<div id="idletimeout">
	You will be logged off in <br /> <span><!-- countdown place holder --></span>&nbsp;<br />seconds due to inactivity.<br /> 
	<%--<a id="idletimeout-resume" href="#">Click here to continue using this web page</a>.--%>
</div>


       <div> 

       <%-- <asp:ScriptManager runat="server"></asp:ScriptManager>--%>
        
       <table border=0   style=" background-repeat:no-repeat; background-position:right;" width="100%" >
       <tr>
        <td>
            <h3>Document Center</h3></td>
       </tr>
       <tr>
        <td align="left">
            <table>
            <tr>
                <td> <asp:ImageButton ID="imgClose" ImageUrl="~/images/new yellow.png"  
                    runat="server"  width="24" height="24" style="border:0px"  Visible="true"
                   /></td>
                <td><a href="../Docs/Improved Card and New ToB Provider Communication V8.pdf">Announcement - Improved Bupa Membership Card&nbsp; and ToB</a></td>
                 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td> 
                    <asp:ImageButton ID="imgClose0" ImageUrl="~/images/new yellow.png"  
                    runat="server"  width="24" height="24" style="border:0px"  Visible="true"
                   /></td>
                <td><a href="../Docs/New ToB Instruction.pdf">New ToB Instruction</a></td>

                  <td>&nbsp;</td>
                <td> &nbsp; </td>
            </tr>
            <tr>
                <td colspan="4"> <h3>Table of Benefits</h3></td>
            </tr>
            <tr>
                <td> 
                    <asp:ImageButton ID="imgClose1" ImageUrl="~/images/new yellow.png"  
                    runat="server"  width="24" height="24" style="border:0px"  Visible="true"
                   /></td>
                <td><a href="../Docs/BupaStandardToB.pdf">Bupa Standard ToB</a></td>

                  <td> <asp:ImageButton ID="ImageButton5" ImageUrl="~/images/new yellow.png"  runat="server"  width="24" height="24" style="border:0px"  Visible="true" /></td>
                <td><a href="../Docs/BAMaternityProductToB.pdf">BA - Maternity Product</a></td>
            </tr>
            <tr>
                <td> 
                    <asp:ImageButton ID="imgClose2" ImageUrl="~/images/new yellow.png"  
                    runat="server"  width="24" height="24" style="border:0px"  Visible="true"
                   /></td>
                <td><a href="../Docs/BupaFamilyTableofBenefits.pdf">Bupa Family ToB</a></td>

                  <td> <asp:ImageButton ID="ImageButton6" ImageUrl="~/images/new yellow.png"  runat="server"  width="24" height="24" style="border:0px"  Visible="true" /></td>
                <td><a href="../Docs/BAMaternityPlusProductToB.pdf">BA - Maternity Plus Product</a></td>
            </tr>
                <tr>
                <td> 
                    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/new yellow.png"  
                    runat="server"  width="24" height="24" style="border:0px"  Visible="true"
                   /></td>
                <td><a href="../Docs/BupaFamilyIn-patientTOB.pdf">Bupa Family In-Patient ToB</a></td>

                      <td> <asp:ImageButton ID="ImageButton7" ImageUrl="~/images/new yellow.png"  runat="server"  width="24" height="24" style="border:0px"  Visible="true" /></td>
                <td><a href="../Docs/BASaudiIndividualToB.pdf">BA - Saudi Individual</a></td>
            </tr>
                <tr>
                <td> 
                    <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/new yellow.png"  
                    runat="server"  width="24" height="24" style="border:0px"  Visible="true"
                   /></td>
                <td><a href="../Docs/BupaFamilyNewlywedsToB.pdf">Bupa Family Newly Weds</a></td>

                <td>&nbsp;</td>
                <td> &nbsp; </td>
            </tr>			
            </table></td>
       </tr>
       <tr>
        <td>
            
      <fieldset>
                <legend><h1><asp:Label ID="lblAdd_Employee" runat="server"  Text="Table of Benefits" Font-Size=Large Font-Names="Arial" ></asp:Label></h1></legend>                 
                               <table border=0   style=" background-repeat:no-repeat; background-position:right;" width="100%" >
        <tr align="right"><td align="left">
           <asp:Label  ID="lblPageTitle" runat="server"></asp:Label>  
            </td>
            <td valign="bottom" width="210px" style="height:53px;" >
            </td>
            </tr>
            </table>
                       
                       
        <table style=" background-repeat:no-repeat; background-position:right;" width="100%">
            <tr>
                <td>
        <table style="width:100%">
        <tr>
            <td colspan="2" style="width:20%">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Search by" Width="100px"></asp:Label>
                        </td>
                        <td>
                            <div id="RadioDiv">
                                <asp:RadioButtonList ID="rdnSearchBy" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CellSpacing="50" Height="30px" TabIndex="1">
                                   <asp:ListItem Value="3">Effective Date</asp:ListItem>
                                     <asp:ListItem Value="1">Policy No</asp:ListItem>
                                    <asp:ListItem Value="2">Membership No</asp:ListItem>
                                    
                                </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                </table>          
            </td>
            <td align="left" style="width:47%">
                <asp:RequiredFieldValidator   
                    ID="ReqiredFieldValidator1"  
                    runat="server"  
                    ControlToValidate="rdnSearchBy"  
                    ErrorMessage="Select search by!"
                    ValidationGroup="Datesearch"
                    SetFocusOnError="True">  
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="rowPolicyNo" >
            <td style="width:20%">
                <span style="color: #ff0000">*</span>Policy No
            </td>
            <td align="left">
                <asp:TextBox  MaxLength="9" ID="txtPolicy"  
                    runat="server"  ValidationGroup="Datesearch" 
                     ControlToValidate="txtPolicy" Width="230px" Height="20px" TabIndex="2"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtPolicyNo" runat="server" ValidationGroup="PolicyDetails" ErrorMessage="Required" ControlToValidate="txtPolicy" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="rfvPolicyDetailsSearch" runat="server" ValidationGroup="Datesearch"  ErrorMessage="Required" Text="Mandatory Field" ControlToValidate="txtPolicy" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </td>
            <td align="left" style="width:50%">

                <asp:Button  ID="btnLoadPolicyDetails" runat="server"  CausesValidation="True"  ValidationGroup="PolicyDetails"  Text="Get Policy Details" OnClick="btnLoadPolicyDetails_Click" Font-Bold="False" Font-Size="8pt" Width="100px" Height="20px" TabIndex="3"   />
            </td>
        </tr>
        <tr id="rowMemberShip">
            <td style="width:20%">
                <span style="color: #ff0000">*</span>Membership No
            </td>
            <td align="left">
                <asp:TextBox  MaxLength="8" ID="txtMemberSearch"  
                    runat="server"  ValidationGroup="Datesearch"  Width="230px" Height="20px" TabIndex="4"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvMemberSearch" runat="server" ValidationGroup="Datesearch"  ErrorMessage="Required" Text="*" ControlToValidate="txtMemberSearch" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </td>
            <td align="left" style="width:47%">
                &nbsp;</td>
        </tr>
        <tr>

            <td colspan="3">



            </td>
        </tr>   
        <tr id="roeEffectedDate">
            <td style="width:20%">
                <span style="color: #ff0000">*</span><asp:Label ID="Label1" runat="server" Text="Effective Date" Width="100px"></asp:Label>
            </td>    
            <td align="left" style="text-align:left;">
                <dx:ASPxDateEdit ID="txtSearchDate" runat="server" 
                            EditFormat="Date" UseMaskBehavior="true" EditFormatString="dd/MM/yyyy" 
                            EnableViewState="true"  Width="230px" TabIndex="5">
                    <ClientSideEvents DropDown="OnDropDown" />
                </dx:ASPxDateEdit>
				<asp:RequiredFieldValidator ID="ReqValSearchDate" runat="server" ValidationGroup="Datesearch" Display="Dynamic"  ErrorMessage="Please select the effective date"  ControlToValidate="txtSearchDate" SetFocusOnError="True" Enabled="false"></asp:RequiredFieldValidator>
            </td>
             
            <td style="width:47%">
              

            </td>
        </tr>
         <tr>

            <td colspan="3"></td>
        </tr>   

        <tr id="row_network">
            <td style="width:20%">
                <asp:Label ID="Label2" runat="server" Text="Network" Width="100px"></asp:Label></td>
            <td align="left" >
                <dx:ASPxComboBox ID="cbNetworkList1" runat="server" Width="230px" TabIndex="6" >
                </dx:ASPxComboBox>
            </td>
            <td align="left" style="width:47%"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>   

        <tr id= "rowSearchButton">
            <td style="width:20%">
                &nbsp;
            </td>
            <td >
                <asp:Button CssClass="submitButton" ID="btnSearchByDate" runat="server"  
                CausesValidation="true"  ValidationGroup="Datesearch"  Text="Search" OnClick="btnSearchByDate_Click" TabIndex="7"  />
            </td>
            <td>
            </td>
        </tr>
        </table>
                    </td>
                </tr>
            <tr >
                <td colspan="3" style="margin-left: 40px">
                    <table style="width:100%">
                        <tr>
                            <td>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator22"  runat="server" ControlToValidate="txtSearchDate" 
            ErrorMessage="RequiredFieldValidator" Font-Names="Verdana"   ValidationGroup="Datesearch" 
             Display="Dynamic"  Font-Size="Small"  Text="Please fill in the Date to be searched for"> </asp:RequiredFieldValidator>--%>

                            </td>
                        </tr>
                        <tr>
                            <td><%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"  runat="server" ControlToValidate="txtSearch"
            ErrorMessage="RequiredFieldValidator" Font-Bold="False" Font-Names="Verdana" ValidationGroup="Membersearch"
            Font-Size="Small" Display="Dynamic"><br />Please type in the membership No. to search</asp:RequiredFieldValidator>--%></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Message1" runat="server" Width="" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td><asp:Literal ID="txtMsg" Text="" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Label18" runat="server" Font-Size="Small"  Text="List below are the most recent groups which have joined or renewed with BUPA. <br/>  Click on the plan / contract name to view or print the group's Table of Benefits." /> </asp:Label></td></tr><tr>
                            <td>
                            <dx:aspxgridview ID="gvResult" runat="server" 
         KeyFieldName="Customer ID"
                Width="100%" onhtmldatacellprepared="gvResult_HtmlDataCellPrepared" Visible="False">
        <SettingsPager PageSize="50">
        </SettingsPager>
                
            </dx:aspxgridview>
                                <br />
                            </td>
                        </tr>
                    </table>
                    </td>
            </tr>
        </table><br />
        
 
      </fieldset>
            </td></tr><tr>
        <td><uc2:OSNav ID="OSNav1" runat="server" /></td>
       </tr>
       </table>

</div>


                </table>
   

                </table>

   

</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
    <p>
        View and print our members&#39; Table of Benefits</p></asp:Content>