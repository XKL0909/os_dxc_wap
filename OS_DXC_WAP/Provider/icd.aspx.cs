﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using System.Configuration;
using System.Web.Configuration;




public partial class Provider_icd : System.Web.UI.Page
{
    string _strFilter = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();
        }
        catch
        {

        }
        
        if(string.IsNullOrEmpty(Convert.ToString(Session["filter"])))
        {
            Session["filter"] = "ICD_Code  LIKE '%{0}%'";
        }
    
    }



    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.opener.document.getElementById('txtDiagnosisDescription').value ='" + GridView1.SelectedRow.Cells[2].Text + "';window.opener.document.getElementById('txtCIDCode').value ='" + GridView1.SelectedRow.Cells[1].Text + " ';window.close();", true);
    }
    protected void btnfilter_Click(object sender, EventArgs e)
    {
        SqlDataSource1.FilterExpression = Convert.ToString(Session["filter"]);
        GridView1.DataBind();
    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedValue == "0")
        {
            Session["filter"] = "ICD_Code  LIKE '%{0}%'";
        }
        else
        {
            Session["filter"] = "ICD_Desc  LIKE '%{0}%'";
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        SqlDataSource1.FilterExpression = Convert.ToString(Session["filter"]);
        GridView1.DataBind();
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
}