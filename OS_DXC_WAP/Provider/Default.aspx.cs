﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Utility.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class Provider_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionManager.CheckSessionTimeout();

		if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/Default.aspx", true);
        }
        else
        {
        DataSet _ProPer = GetProviderPermission(Convert.ToString(Session["ProviderUserName"]));
        foreach (DataRow dr in _ProPer.Tables[0].Rows)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(dr["NewRequest"])))
                Option1.Visible = Convert.ToBoolean(dr["NewRequest"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ExistingRequest"])))
                Option2.Visible = Convert.ToBoolean(dr["ExistingRequest"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["CheckNewReply"])))
                Option3.Visible = Convert.ToBoolean(dr["CheckNewReply"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["TableOfBenefits"])))
                Option4.Visible = Convert.ToBoolean(dr["TableOfBenefits"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["EClaimsSubmission"])))
                Option5.Visible = Convert.ToBoolean(dr["EClaimsSubmission"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["Verification"])))
                Option6.Visible = Convert.ToBoolean(dr["Verification"]);
        }
    }
	}

    protected DataSet GetProviderPermission(string userName)
    {
        DataSet ds = new DataSet();


        try
        {



            /*DataParameter UserName = new DataParameter(SqlDbType.NVarChar, "@proUsername", userName, ParameterDirection.Input, 30);

            DataParameter[] dpBasket = new DataParameter[] { UserName };
            
            ds = DataManager.ExecuteStoredProcedureCachedReturn(WebPublication.Key_ConnBupa_OS, "spGetProviderPermission", ref dpBasket, 60000, true);
            */
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            SqlCommand cmd = new SqlCommand("spGetProviderPermission", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@proUsername",userName);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

        }
        catch (Exception ex)
        {

            throw;
        }

        return ds;
    }
}