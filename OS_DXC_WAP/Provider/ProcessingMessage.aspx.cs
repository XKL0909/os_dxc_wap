using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Configuration;

namespace AsynchronousProcessing
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{

            try
            {
                if (Request.UrlReferrer == null)
                {
                    Response.Redirect("~/CommClear.aspx", true);
                }

                //Session Timeout Warning Dialog
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Session["Reset"] = true;
                    Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                    SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                    int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
                
                CheckSessionExpired();
            }
            catch
            {

            }
            
            
            string redirectPage = "processing.gif";// Request.QueryString["redirectPage"];
            string processSessionName = "process";// Request.QueryString["ProcessSessionName"];
			if (Session["ProcessTime"] == null)
			{
				StatusMessage.Text = "Process has been running for 0 seconds";
				Session["ProcessTime"] = 0;
			}
			else
			{
				Session["ProcessTime"] = Convert.ToInt32(Session["ProcessTime"]) + 1;
				StatusMessage.Text = "Process has been running for " + Session["ProcessTime"].ToString() + " seconds";
			}
	
			if ((bool)Session[processSessionName] == true)
			{
				Session[processSessionName] = null;
				Session["ProcessTime"] = null;
                Response.Redirect(redirectPage + "?dat=" + Session["data"]);
			}			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

        
        protected void aYes_Click(object sender, EventArgs e)
        {
            //  Response.Redirect(Request.RawUrl);

            // Session["SessionExpire"] = false;
        }
        private void CheckSessionExpired()
        {
            int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
            int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

            ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
            //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

            string url = ConfigurationManager.AppSettings["SessionPath"];

            string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

        }

	}
}
