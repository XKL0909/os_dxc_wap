﻿using Bupa.OSWeb.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Configuration;
using Bupa.Core.Logging;
using System.Web.Configuration;
using System.Configuration;

public partial class Provider_UploadDocuments : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         try
        {

            try
            {
                
                    //Session Timeout Warning Dialog
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Session["Reset"] = true;
                    Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                    SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                    int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
               
                CheckSessionExpired();
            }
            catch
            {

            }
             
             SessionManager.CheckSessionTimeoutForMemberVerify();

            if (string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            {
                Response.Redirect("../default.aspx", true);
            }
            else
            {
                SetupUploader();
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
    }

    private void SetupUploader()
    {
        // By default, unless a user has uploaded supporting documents,
        // the Submit button will be disabled

        EnableUploader(true);

        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(UploadCategory.PREAUTH_UPLOAD, username, allowedExtent, fileByteLimit);

        // Bind to allow resolving of # tags in mark-up
        //DataBind();

        //DataSet ds = (DataSet)Session["DataSet"];
        //foreach (DataRow dr in ds.Tables[0].Rows)
        //{
        //    if (dr["SEQ"].ToString() == Convert.ToString(Convert.ToInt32(Request["SEQ"]) + 1))
        //    {

        //        dr["SupportDocs"] = "";
        //    }
        //}

        sessionID.Text =Convert.ToString(Session["sessionID"]);
    }

    private void EnableUploader(bool optionalUploads)
    {
        UploadManager _uploadmanger = new UploadManager(Int32.MinValue, Int32.MinValue);
    }


    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
}