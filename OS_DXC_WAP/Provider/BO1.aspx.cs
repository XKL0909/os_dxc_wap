using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;


public partial class BO1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        string strVerificationID;
        string strVerificationStatus;
        string strRejectionReason;


        if (Session["ProcessTime"] == null)
        {
            StatusMessage.Text = "Process has been running for 0 seconds";
            Session["ProcessTime"] = 0;
        }
        else
        {
            Session["ProcessTime"] = Convert.ToInt32(Session["ProcessTime"]) + 1;
            StatusMessage.Text = "Process has been running for " + Session["ProcessTime"].ToString() + " seconds";
        }


        SqlConnection mySqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);

        SqlCommand mySqlCommand = mySqlConnection.CreateCommand();

        mySqlCommand.CommandText = "SELECT top 1 VerificationID, VerificationStatus, RejectionReason FROM BiometricVerification WHERE MembershipNo =" + Session["MembershipNo"].ToString() + "  and TransactionNo = " + Session["TransNo"].ToString() + " and   VerificationStatus in ('A' , 'R',  '0')  and  RejectionReason <> '' ";// (DateTimeSent between  DATEADD(second, -5, getdate() ) and getdate()) order by transactionno desc  ";


        mySqlConnection.Open();
        SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader(CommandBehavior.SingleRow);

        if (mySqlDataReader.HasRows == true)
        {
            // while (mySqlDataReader.Read())
            //{

           
            mySqlDataReader.Read();
        strVerificationID = mySqlDataReader[0].ToString();
        strVerificationStatus = mySqlDataReader[1].ToString();
        strRejectionReason = mySqlDataReader[2].ToString();
        Session["RejectionReason"] = strRejectionReason;




        System.Data.SqlClient.SqlConnection sqlConnection1 = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "select max(TransactionNo)+1 from BiometricVerification";
        cmd.Connection = sqlConnection1;


        sqlConnection1.Open();

        string txtTrans = cmd.ExecuteScalar().ToString();
        sqlConnection1.Close();


        long MbrVrfID;
        MbrVrfID = long.Parse(Session["TransNo"].ToString()) + 1999999085;// +1900021549;// 999818405;

            Response.Redirect("TOB.aspx?VID=" + MbrVrfID.ToString() + "&Type=In&MN=" + Session["MembershipNo"].ToString() + "&D=" + Session["Dept"].ToString() + "&Status=" + strVerificationStatus);
            //}
        }
        mySqlDataReader.Close();
        mySqlConnection.Close();

    }

    public void ProcessRequests1(string prefixes)
    {
        if (!System.Net.HttpListener.IsSupported)
        {
            Console.WriteLine(
                "Windows XP SP2, Server 2003, or higher is required to " +
                "use the HttpListener class.");
            return;
        }
        // URI prefixes are required,
        if (prefixes == null || prefixes.Length == 0)
            throw new ArgumentException("prefixes");

        // Create a listener and add the prefixes.
        System.Net.HttpListener listener = new System.Net.HttpListener();
        //foreach (string s in prefixes)
        //{
        listener.Prefixes.Add(prefixes);
        // }

        try
        {
            // Start the listener to begin listening for requests.
            listener.Start();
            txt1.Text = "Listening...";

            // Set the number of requests this application will handle.
            int numRequestsToBeHandled = 10;

            //for (int i = 0; i < numRequestsToBeHandled; i++)
            //{
            HttpListenerResponse response = null;
            try
            {
                // Note: GetContext blocks while waiting for a request.
                HttpListenerContext context = listener.GetContext();
                txt1.Text += context.Request.Url.Query + "------" + context.Request.Url.Query.Substring(6) + "=======<br/>" + DateTime.Now.ToString(DateTimeFormatInfo.CurrentInfo) + "*********";

                if (context.Request.Url.Query != "")
                {

                
                }


                // Create the response.
                response = context.Response;
                //  context.Request.Url.Query;

                string responseString =
                    "<HTML><BODY>The time is currently " +
                    DateTime.Now.ToString(
                    DateTimeFormatInfo.CurrentInfo) +
                    "</BODY></HTML>";
                byte[] buffer =
                    System.Text.Encoding.UTF8.GetBytes(responseString);
                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
            }
            catch (HttpListenerException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (response != null)
                    response.Close();
            }

          // }
        }
        catch (HttpListenerException ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            // Stop listening for requests.
            listener.Close();
            txt1.Text += "Done Listening.";
        }
    }


}
