﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ExistingRequest" Codebehind="ExistingRequest.aspx.cs" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
    <script type="text/javascript"  src="js/jquery.dataTables.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;<strong><span style="font-size: 10pt">Search for pre-authorisation</span></strong><br />
            <table>
                <tr>
                    <td style="width: 100px">
                        Reference Number</td>
                    <td style="width: 100px">
                        <asp:TextBox CssClass="textbox" ID="txtRefernceNo" runat="server" ></asp:TextBox></td>
                    <td style="width: 100px">
                        <asp:Button CssClass="submitButton" ID="btnRefernceNo" runat="server" Text="Search" /></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Membership Number</td>
                    <td style="width: 100px">
                        <asp:TextBox CssClass="textbox" ID="txtMembershipNoPre" runat="server"></asp:TextBox></td>
                    <td style="width: 100px">
                        <asp:Button CssClass="submitButton" ID="btnMembershipNoPre" runat="server" Text="Search" /></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Search by</td>
                    <td style="width: 100px">
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlSearchByPre" runat="server" Width="92px">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="T">Today</asp:ListItem>
                            <asp:ListItem Value="Y">Yesterday</asp:ListItem>
                        </asp:DropDownList></div></td>
                    <td style="width: 100px">
                        <asp:Button CssClass="submitButton" ID="btnSearchByPre" runat="server" Text="Search" /></td>
                </tr>
            </table>
            <br />
            &nbsp;<strong><span style="font-size: 10pt">Search for member verification</span> </strong>
            <br />
            <table>
                <tr>
                    <td style="width: 100px">
                        Verification ID</td>
                    <td style="width: 100px">
                        <asp:TextBox CssClass="textbox" ID="txtVerificationID" runat="server"></asp:TextBox></td>
                    <td style="width: 100px">
                        <asp:Button CssClass="submitButton" ID="btnVerificationID" runat="server" Text="Search" /></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Membership Number</td>
                    <td style="width: 100px">
                        <asp:TextBox CssClass="textbox" ID="txtMembershipNoVer" runat="server"></asp:TextBox></td>
                    <td style="width: 100px">
                        <asp:Button CssClass="submitButton" ID="btnMembershipNoVer" runat="server" Text="Search" /></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Search by</td>
                    <td style="width: 100px">
                        <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlSearchByVer" runat="server" Width="95px">
                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                            <asp:ListItem Value="T">Today</asp:ListItem>
                            <asp:ListItem Value="Y">Yesterday</asp:ListItem>
                        </asp:DropDownList></div></td>
                    <td style="width: 100px">
                        <asp:Button CssClass="submitButton" ID="btnSearchByVer" runat="server" Text="Search" /></td>
                </tr>
            </table>
        </div>
        <uc1:OSNav ID="OSNav1" runat="server" />

         <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                       <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
    </form>
</body>
</html>
