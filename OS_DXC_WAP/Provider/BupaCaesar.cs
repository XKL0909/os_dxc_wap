﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections;
//using OS_DXC_WAP.CaesarWS;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using OS_DXC_WAP.CaesarWS;

/// <summary>
/// Summary description for BupaCaesar
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
[System.ComponentModel.ToolboxItem(false)]
public class BupaCaesar : System.Web.Services.WebService {

    public BupaCaesar () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod(EnableSession = true)]
    public ArrayList GetBupaCodeList(string sProviderID, string sDepartment)
    {
        ArrayList bupaCodeList = new ArrayList();
        List<BupaCodeEntity> BupaCodeEntitylist = new List<BupaCodeEntity>();
        List<BupaCodeListDetail_DN> BupaCodeListDetail = new List<BupaCodeListDetail_DN>();

        if (sProviderID != string.Empty && sDepartment != string.Empty)
        {
              DataTable dt = LoadHRMServices(sProviderID, sDepartment);
              BupaCodeListDetail =  LoadHRMServicesList (sProviderID, sDepartment);
              foreach (BupaCodeListDetail_DN bcl in BupaCodeListDetail)
              {
                  bupaCodeList.Add(bcl);
              }  
        }
        else
        {
            bupaCodeList.Add("No Records Found");
        }
                  

        return bupaCodeList;
    }
    [WebMethod(EnableSession = true)]
    public List<BupaCodeEntity> GetBupaCodeObject(string sProviderID, string sDepartment)
    {
        List<BupaCodeEntity> BupaCodeEntitylist = new List<BupaCodeEntity>();
        List<BupaCodeListDetail_DN> BupaCodeListDetail = new List<BupaCodeListDetail_DN>();

        if (sProviderID != string.Empty && sDepartment != string.Empty)
        {
            //DataTable dt = LoadHRMServices(sProviderID, sDepartment);
            BupaCodeListDetail = LoadHRMServicesList(sProviderID, sDepartment);
            ////int i= 0;
            foreach (BupaCodeListDetail_DN bcl in BupaCodeListDetail)
            {
                ////i++;
                //BupaCodeEntitylist.Add(new BupaCodeEntity { ServiceCode = bcl.servCode, ServiceDesc = bcl.servDesc, Amount = bcl.amount, ServiceDisplay = bcl.servDesc + " - " + bcl.servCode + " - " + bcl.amount });
                BupaCodeEntitylist.Add(new BupaCodeEntity {
                    ////ServiceCode = bcl.servCode,
                    //ServiceDesc = bcl.servDesc.Split('-')[0].Replace('/', ' '),
                    //ServiceDesc = bcl.servDesc.Split('-')[0],
                    ////ServiceDesc = bcl.servDesc, 
                    ///ServiceCode = bcl.servCode,
                    
                    ////ServiceCode = i.ToString(),
                    ServiceCode = bcl.servCode,
                    ServiceDesc = bcl.servDesc,
                    ICDCode = bcl.ICDCode, 
                    Amount = bcl.amount,
                    //ServiceDisplay = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc.Split('-')[0].Replace('/', ' '), bcl.servCode, bcl.amount),
                    //ServiceDisplay = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc.Split('-')[0], bcl.servCode, bcl.amount),
                    ////ServiceDisplay = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc, bcl.servCode, bcl.amount),
                    ServiceDisplay = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc, bcl.servCode, bcl.amount),
                    BenefitHead = bcl.benHead,
                    EyeTestIndicator= bcl.eyeTestInd,
                    SupplyIndicator= bcl.supplyInd,
                    ToothSiteIndicator= bcl.toothSiteInd
                });
                
            }
        }
        else
        {
            BupaCodeEntitylist.Add(new BupaCodeEntity {});
        }

        //List<BupaCodeEntity> SortedList = BupaCodeEntitylist.OrderBy(o => o.ServiceDesc).ToList();
        //return SortedList;
        return BupaCodeEntitylist;
    }
    [WebMethod(EnableSession = true)]
    public List<BupaCodeEntity> GetBupaCodeObjectFilter(string sProviderID, string sDepartment, string sServiceDesc, string sServiceCode)
    {
        List<BupaCodeEntity> BupaCodeEntitylist = new List<BupaCodeEntity>();
        List<BupaCodeListDetail_DN> BupaCodeListDetail = new List<BupaCodeListDetail_DN>();

        if (sProviderID != string.Empty && sDepartment != string.Empty)
        {
            //DataTable dt = LoadHRMServices(sProviderID, sDepartment);
            BupaCodeListDetail = LoadHRMServicesList(sProviderID, sDepartment, sServiceDesc, sServiceCode);
            foreach (BupaCodeListDetail_DN bcl in BupaCodeListDetail)
            {
                
                BupaCodeEntitylist.Add(new BupaCodeEntity
                {
                    ////ServiceCode = bcl.servCode,
                    ServiceCode = bcl.servCode,
                    ServiceDesc = bcl.servDesc,
                    //ServiceDesc = bcl.servDesc.Split('-')[0].Replace('/', ' '),
                    //ServiceDesc = bcl.servDesc.Split('-')[0],
                    /////ServiceDesc = bcl.servDesc,
                    ICDCode = bcl.ICDCode,
                    Amount = bcl.amount,
                    //ServiceDisplay = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc.Split('-')[0].Replace('/', ' '), bcl.servCode, bcl.amount),
                    //ServiceDisplay = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc.Split('-')[0], bcl.servCode, bcl.amount),
                    /////ServiceDisplay = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc, bcl.servCode, bcl.amount),
                    ServiceDisplay = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc, bcl.servCode, bcl.amount),
                    BenefitHead = bcl.benHead,
                    EyeTestIndicator = bcl.eyeTestInd,
                    SupplyIndicator = bcl.supplyInd,
                    ToothSiteIndicator = bcl.toothSiteInd
                });

            }
        }
        else
        {
            BupaCodeEntitylist.Add(new BupaCodeEntity { });
        }

        //List<BupaCodeEntity> SortedList = BupaCodeEntitylist.OrderBy(o => o.ServiceDesc).ToList();
        //return SortedList;
        return BupaCodeEntitylist;
    }
    public List<BupaCodeListDetail_DN> LoadHRMServicesList(string sProviderID, string sDepartment)
    {

        OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListResponse_DN response;
        BupaCodeListDetail_DN[] BupaCodeList;
        List<BupaCodeListDetail_DN> BupaCodeListDetail = new List<BupaCodeListDetail_DN>();
        try
        {
            try
            {
                if (BLCommon.BupaCodeListDetail != null)
                {
                    BupaCodeListDetail = (List<BupaCodeListDetail_DN>)BLCommon.BupaCodeListDetail;
                }
            }
            catch(Exception ex){
                BLCommon.BupaCodeListDetail = null;
            }
            if (BLCommon.BupaCodeListDetail != null)
            {
                BupaCodeListDetail = (List<BupaCodeListDetail_DN>)BLCommon.BupaCodeListDetail;
            }
            else
            {
                response = null;
                request = new OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN();
                request.transactionID = TransactionManager.TransactionID();
                request.provCode = sProviderID;
                request.PAAutomation = sDepartment;
                //ADC    24 Hrs Admission for Child
                //CLS    Colonoscopy
                //CRN    Chronic Medication
                //DEN    Dental
                //EDO    UGI Endoscopy
                //MAT    Maternity
                //MRK    MRI Knee
                //MRS    MRI Shoulder
                //MSP    MRI Spine
                //OPT    Optical
                //PNS    CT PNS
                //VAC    Vaccination
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;
                using (ServiceDepot_DNService ws = new ServiceDepot_DNService())
                {
                    /////mmmmmmmmmmmmmmmmm

                    response = ws.ReqBupaCodeList(request);

                    ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN>();
                    ////XmlReq.Request(request, "ReqBupaCodeListRequest_DNmmmmmmmmmmmmmmmmm");

                    ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqBupaCodeListResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqBupaCodeListResponse_DN>();
                    ////XmlResp.Response(response, "ReqBupaCodeListResponse_DNmmmmmmmmmmmmmmmmm");

                }
                if (response.status == "0")
                {

                    BupaCodeList = response.detail;
                    int i=0;
                    foreach (BupaCodeListDetail_DN bcl in response.detail)
                    {
                        BupaCodeListDetail.Add(bcl);
                        i++;
                        if (i > 200)
                            break;
                    }
                    //response.errorMessage;  
                    //response.errorID;
                    //response.transactionID;
                    BLCommon.BupaCodeListDetail = BupaCodeListDetail;
                }
            }
           
            

        }
        catch (Exception ex)
        {

        }
        return BupaCodeListDetail;
    }
    public List<BupaCodeListDetail_DN> LoadHRMServicesList(string sProviderID, string sDepartment, string sServiceDesc, string sServiceCode)
    {

        OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListResponse_DN response;
        BupaCodeListDetail_DN[] BupaCodeList;
        List<BupaCodeListDetail_DN> BupaCodeListDetail = new List<BupaCodeListDetail_DN>();
        try
        {
            if (BLCommon.BupaCodeListDetail != null && string.IsNullOrEmpty(sServiceCode) && string.IsNullOrEmpty(sServiceDesc))
            {
                BupaCodeListDetail = (List<BupaCodeListDetail_DN>)BLCommon.BupaCodeListDetail;
            }
            else
            {
                response = null;
                request = new OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN();
                request.transactionID = TransactionManager.TransactionID();
                request.provCode = sProviderID;
                request.PAAutomation = sDepartment;
                request.servCode = sServiceCode;
                request.servDesc = sServiceDesc.ToUpper();
                request.PAAutomation = sDepartment;
                //ADC    24 Hrs Admission for Child
                //CLS    Colonoscopy
                //CRN    Chronic Medication
                //DEN    Dental
                //EDO    UGI Endoscopy
                //MAT    Maternity
                //MRK    MRI Knee
                //MRS    MRI Shoulder
                //MSP    MRI Spine
                //OPT    Optical
                //PNS    CT PNS
                //VAC    Vaccination
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;
                using (ServiceDepot_DNService ws = new ServiceDepot_DNService())
                {
                    response = ws.ReqBupaCodeList(request);
                }
                if (response.status == "0")
                {

                    BupaCodeList = response.detail;
                    int i = 0;
                    foreach (BupaCodeListDetail_DN bcl in response.detail)
                    {
                        BupaCodeListDetail.Add(bcl);
                        i++;
                        if (i > 200)
                            break;
                    }
                    //response.errorMessage;  
                    //response.errorID;
                    //response.transactionID;
                    BLCommon.BupaCodeListDetail = BupaCodeListDetail;
                }
            }



        }
        catch (Exception ex)
        {

        }
        return BupaCodeListDetail;
    }
    public DataTable LoadHRMServices(string sProviderID,string sDepartment)
    {
        
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListResponse_DN response;
        BupaCodeListDetail_DN[] BupaCodeList;
        DataTable dt = new DataTable();
        dt.TableName = "BupaHRMCode";

        dt.Columns.Add("servCode");
        dt.Columns.Add("servDesc");
        dt.Columns.Add("amount");
        dt.Columns.Add("ICDCode");
        dt.Columns.Add("servDisplay");
        dt.Columns.Add("BenefitHead");
        dt.Columns.Add("EyeTestIndicator");
        dt.Columns.Add("SupplyIndicator");
        dt.Columns.Add("ToothSiteIndicator");

        try
        {
            if (BLCommon.BupaCodeLists != null)
            {
                dt = (DataTable)BLCommon.BupaCodeLists;
            }
            else
            {
                response = null;
                request = new OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN();
                request.transactionID = TransactionManager.TransactionID();
                request.provCode = sProviderID;
                request.PAAutomation = sDepartment;
                //ADC    24 Hrs Admission for Child
                //CLS    Colonoscopy
                //CRN    Chronic Medication
                //DEN    Dental
                //EDO    UGI Endoscopy
                //MAT    Maternity
                //MRK    MRI Knee
                //MRS    MRI Shoulder
                //MSP    MRI Spine
                //OPT    Optical
                //PNS    CT PNS
                //VAC    Vaccination
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;
                using (ServiceDepot_DNService ws = new ServiceDepot_DNService())
                {
                    response = ws.ReqBupaCodeList(request);
                 
                }
                if (response.status == "0")
                {
                    int i = 0;
                    BupaCodeList = response.detail;
                    foreach (BupaCodeListDetail_DN bcl in response.detail)
                    {
                        DataRow dr = dt.NewRow();

                        dr[0] = bcl.servCode;
                        //dr[1] = bcl.servDesc.Split('-')[0].Replace('/', ' ');
                        //dr[1] = bcl.servDesc.Split('-')[0];
                        dr[1] = bcl.servDesc;
                        dr[2] = bcl.amount;
                        dr[3] = bcl.ICDCode;
                        //dr[4] = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc.Split('-')[0].Replace('/', ' '), bcl.servCode, bcl.amount);
                        //dr[4] = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc.Split('-')[0], bcl.servCode, bcl.amount);
                        dr[4] = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc, bcl.servCode, bcl.amount);
                        dr[5] = bcl.benHead;
                        dr[6] = bcl.eyeTestInd;
                        dr[7] = bcl.supplyInd;
                        dr[8] = bcl.toothSiteInd;
                        dt.Rows.Add(dr);
                        i++;
                        if (i > 200)
                            break;
                    }
                    //response.errorMessage;  
                    //response.errorID;
                    //response.transactionID;
                    BLCommon.BupaCodeLists=dt;
                }
            }

        }
        catch (Exception ex)
        {

        }
        //dt.DefaultView.Sort = "servDesc asc";
        //dt = dt.DefaultView.ToTable(true);
        return dt;
    }
    public DataTable LoadHRMServices(string sProviderID, string sDepartment, string sServiceDesc, string sServiceCode)
    {

        OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListResponse_DN response;
        BupaCodeListDetail_DN[] BupaCodeList;
        DataTable dt = new DataTable();
        dt.TableName = "BupaHRMCode";

        dt.Columns.Add("servCode");
        dt.Columns.Add("servDesc");
        dt.Columns.Add("amount");
        dt.Columns.Add("ICDCode");
        dt.Columns.Add("servDisplay");
        dt.Columns.Add("BenefitHead");
        dt.Columns.Add("EyeTestIndicator");
        dt.Columns.Add("SupplyIndicator");
        dt.Columns.Add("ToothSiteIndicator");

        try
        {
            if (BLCommon.BupaCodeLists != null  && string.IsNullOrEmpty(sServiceCode) && string.IsNullOrEmpty(sServiceDesc))
            {
                dt = (DataTable)BLCommon.BupaCodeLists;
            }
            else
            {
                response = null;
                request = new OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN();
                request.transactionID = TransactionManager.TransactionID();
                request.provCode = sProviderID;
                request.PAAutomation = sDepartment;
                request.servCode = sServiceCode;
                request.servDesc = sServiceDesc;
                //ADC    24 Hrs Admission for Child
                //CLS    Colonoscopy
                //CRN    Chronic Medication
                //DEN    Dental
                //EDO    UGI Endoscopy
                //MAT    Maternity
                //MRK    MRI Knee
                //MRS    MRI Shoulder
                //MSP    MRI Spine
                //OPT    Optical
                //PNS    CT PNS
                //VAC    Vaccination
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;
                using (ServiceDepot_DNService ws = new ServiceDepot_DNService())
                {
                    response = ws.ReqBupaCodeList(request);
                }
                if (response.status == "0")
                {
                    int i = 0;
                    BupaCodeList = response.detail;
                    foreach (BupaCodeListDetail_DN bcl in response.detail)
                    {
                        DataRow dr = dt.NewRow();
                      
                        ////dr[0] = bcl.servCode;
                        dr[0] = bcl.servCode;
                        //dr[1] = bcl.servDesc.Split('-')[0].Replace('/', ' ');
                        //dr[1] = bcl.servDesc.Split('-')[0];
                        ////dr[1] = bcl.servDesc;
                        dr[1] = bcl.servDesc;
                        dr[2] = bcl.amount;
                        dr[3] = bcl.ICDCode;
                        //dr[4] = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc.Split('-')[0].Replace('/', ' '), bcl.servCode, bcl.amount);
                        //dr[4] = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc.Split('-')[0], bcl.servCode, bcl.amount);
                        ////dr[4] = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc, bcl.servCode, bcl.amount);
                        dr[4] = string.Format("{0,-50}-{1,-12}-{2,5}", bcl.servDesc, bcl.servCode, bcl.amount);
                        dr[5] = bcl.benHead;
                        dr[6] = bcl.eyeTestInd;
                        dr[7] = bcl.supplyInd;
                        dr[8] = bcl.toothSiteInd;
                        dt.Rows.Add(dr);
                        i++;
                        if (i > 11000)
                            break;
                    }
                    //response.errorMessage;  
                    //response.errorID;
                    //response.transactionID;
                    BLCommon.BupaCodeLists = dt;
                }
            }

        }
        catch (Exception ex)
        {

        }
        //dt.DefaultView.Sort = "servDesc asc";
        //dt = dt.DefaultView.ToTable(true);
        return dt;
    }
}
public class BupaCodeEntity
{
    public string ServiceCode { get; set; }
    public string ServiceDesc { get; set; }
    public double Amount { get; set; }
    public string ICDCode { get; set; }
    public string ServiceDisplay { get; set; }
    public string BenefitHead { get; set; }
    public string EyeTestIndicator { get; set; }
    public string SupplyIndicator { get; set; }
    public string ToothSiteIndicator { get; set; }

    
}

////public class ClassXMLGeneration<T>
////{
////    public void Request(T classType, string req)
////    {
////        System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
////        System.IO.StreamWriter file = new StreamWriter("c:\\CaesarXML\\" + req + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
////        xmlSrQ.Serialize(file, classType);
////        file.Close();


////    }
////    public void Response(T classType, string resp)
////    {
////        System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
////        System.IO.StreamWriter file = new StreamWriter("c:\\CaesarXML\\" + resp + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
////        xmlSr.Serialize(file, classType);
////        file.Close();
////    }
////}