<%@ Page Language="C#" MasterPageFile="~/Templates/Inner.Master" AutoEventWireup="true" EnableSessionState="True" Inherits="MemberVerification1" Debug="true" Codebehind="MemberVerification1.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register Src="../UserControls/OSNav.ascx" TagName="OSNav" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <link rel="stylesheet" type="text/css" href="../javascript/examples1.css" />
    <script src="../javascript/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">
        .style2
        {
            height: 24px;
        }

        .style3
        {
            width: 155px;
        }

        .left-div
        {
            float: left;
            width: 35px;
            height: 35px;
        }

        .right-div
        {
            vertical-align: middle;
            margin-top: 15px;
            margin-left: 35px;
        }

        .modalBackground
        {
            background-color: black;       
            opacity: 0.7;       
        }
         #grd
         {
             margin:25px;
         }
         .mybtn
        {
            border:2px solid Red;
            font-weight:bold;
        }
    </style>
    <p>
        Verify members eligibility and benefits
    </p>
   <script type="text/javascript">
       var startdatetime;
       var enddatetime;

       function changetext(reqName) {
           if (reqName == "rfvCardReader") {
               ValidatorEnable(document.getElementById('<%= rfvCardReader.ClientID %>'), false);
               if (document.getElementById('txtAramcoBadgeNumber').value == "")
                   ValidatorEnable(document.getElementById('<%= rfvCardReader.ClientID %>'), true);
           }
       }
           <%--else if(reqName == "reqFieldValAramcoBadgeNumber")
               ValidatorEnable(document.getElementById('<%= reqFieldValAramcoBadgeNumber.ClientID %>'), false);
           }--%>



       function allownumbers(e) {
           //to make sure the entry happened from swipe card i am checking the time for populating the text
           if (document.getElementById('<%= txtCardReader.ClientID %>').value.length == 1) {
               startdatetime = new Date();
           }
           else if (document.getElementById('<%= txtCardReader.ClientID %>').value.length == 8 || document.getElementById('<%= txtCardReader.ClientID %>').value.length == 7) {
               enddatetime = new Date();
               var difference_ms = enddatetime - startdatetime;
               if (difference_ms > 100) {
                   clear_text();
                   return false;
               }
               else {
                   document.getElementById('<%= txtMembershipNo.ClientID %>').value = document.getElementById('<%= txtCardReader.ClientID %>').value;
                   document.getElementById('<%= txtMembershipNo.ClientID %>').focus();
                   document.getElementById('<%= hidoverride.ClientID %>').value = "0";
                   document.getElementById('<%= txtMembershipManually.ClientID %>').value = "";
                   document.getElementById('<%= txtPatientMobile.ClientID %>').value = "";
               }
           }

            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);
            }
            if (key == 11) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }

        function clear_text() {
            document.getElementById('<%= txtMembershipNo.ClientID %>').value = "";
            document.getElementById('<%= txtCardReader.ClientID %>').value = "";
            document.getElementById('<%= txtMembershipManually.ClientID %>').value = "";
            document.getElementById('<%= txtPatientMobile.ClientID %>').value = "";
            document.getElementById("<%=txtBadgeNumber.ClientID%>").value = "";
            document.getElementById("<%=txtAramcoBadgeNumber.ClientID%>").value = "";
            document.getElementById('<%= lblError.ClientID %>').innerHTML = "";
            document.getElementById('<%= txtCardReader.ClientID %>').focus();

        }

       function Hidepopup() {

           $find("ModalPopup").hide();
           clear_text();
           //document.getElementById('<%= txtCardReader.ClientID %>').focus();
           return false;
       }

       function SavePopUpValues() {
           document.getElementById('<%= lblError.ClientID %>').innerHTML = "";
           if (document.getElementById('<%= txtMembershipManually.ClientID %>').value == '') {
               if (document.getElementById('<%= txtBadgeNumber.ClientID %>').value != undefined && document.getElementById('<%= txtBadgeNumber.ClientID %>').value != '') {
                   ValidatorEnable(document.getElementById("RequiredFieldValidator4"), false);
                  // ValidatorEnable(document.getElementById("RequiredFieldValidator3"), false);
                   ValidatorEnable(document.getElementById("ReqBadgeNumber"), true);
                   PageMethods.GetMembership(document.getElementById("<%=txtBadgeNumber.ClientID%>").value, '<%= Session["ProviderID"] %>', OnSuccess);
               }
               else {
                   ValidatorEnable(document.getElementById("RequiredFieldValidator4"), true);
                   //ValidatorEnable(document.getElementById("RequiredFieldValidator3"), true);
                   ValidatorEnable(document.getElementById("ReqBadgeNumber"), true);
               }

           }
           else 
               ValidatorEnable(document.getElementById("ReqBadgeNumber"), false);
               
               

           var validated = Page_ClientValidate('override');
           if (validated) {
               document.getElementById('<%= txtMembershipNo.ClientID %>').value = document.getElementById('<%= txtMembershipManually.ClientID %>').value;
               document.getElementById('<%= txtCardReader.ClientID %>').value = document.getElementById('<%= txtMembershipManually.ClientID %>').value;
               document.getElementById('<%= hidoverride.ClientID %>').value = "1";
               //ValidatorEnable(document.getElementById('reqFieldValAramcoBadgeNumber'), false);
               $find("ModalPopup").hide();
               document.getElementById('<%= txtCardReader.ClientID %>').focus();
               return false;
           }
           //else
           //    //ValidatorEnable(document.getElementById('reqFieldValAramcoBadgeNumber'), false);

           function OnSuccess(response) {
              
               if (response == '') {
                   document.getElementById('<%= lblError.ClientID %>').innerText = 'Membership number not found';
                   <%--ValidatorEnable(document.getElementById('<%= reqFieldValAramcoBadgeNumber.ClientID %>'), true);--%>
               }
               else {
                   if (response.indexOf(':') > 0 && response.split(":")[0].trim() == "Error") {
                       document.getElementById('<%= lblError.ClientID %>').innerText = response.split(":")[1];
                       <%--ValidatorEnable(document.getElementById('<%= reqFieldValAramcoBadgeNumber.ClientID %>'), true);--%>
                   }
                   else {
                       document.getElementById('<%= txtMembershipNo.ClientID %>').innerText = response;
                       document.getElementById('<%= txtCardReader.ClientID %>').innerText = response;
                       document.getElementById('<%= txtMembershipNo.ClientID %>').value = response;
                       document.getElementById('<%= txtCardReader.ClientID %>').value = response;
                       <%--if (document.getElementById('<%= txtMembershipNo.ClientID %>').innerText !="" || document.getElementById('<%= txtCardReader.ClientID %>').value != "")
                           ValidatorEnable(document.getElementById('reqFieldValAramcoBadgeNumber'), false);--%>
                   }
               }
           }
       }
       
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="server" ClientIDMode="Static">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>--%>
    <div>
        <fieldset>
            <legend>
                <asp:Label ID="lblPageTitle" runat="server" Text="Member Verification" Font-Size="Large" Font-Names="Arial"></asp:Label>
                &nbsp;
                <asp:Label ID="lblPageTitle1" runat="server" Font-Bold="True"></asp:Label>
            </legend>
            <table style="width: 610px">
                <tr>
                    <td>
                        <table style="width: 579px">
                            <tr>
                                <td class="style3">
                                    <div id="imgDiv" runat="server" style="float: left;" class="left-div">
                                        <asp:Image ID="imgNew" runat="server" ImageUrl="~/images/new.gif" Width="35px" Height="35px" />
                                    </div>
                                    <div id="imgRightDiv" runat="server" class="right-div">
                                        <span style="color: #ff0000">*</span><asp:Label ID="Label1" runat="server" Text="Membership Number" Width="115px"></asp:Label>
                                    </div>
                                </td>
                                <td>
                                    <asp:TextBox CssClass="textbox" ID="txtMembershipNo" runat="server" ValidationGroup="1" Width="100%" 
                                        MaxLength="8" onkeypress="javascript:return allownumbers(event);" Enabled="False"></asp:TextBox>
                                </td>
                                <td style="width:100px; text-align:center;">
                                    <input id="btnClear" type="button" value="Clear" onclick="clear_text()" runat="server" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCardReader" runat="server" onkeypress="javascript:return allownumbers(event);" MaxLength="8" 
                                        Height="1px" BorderStyle="Solid"
                                        Width="1px" TextMode="Password" ForeColor="White" BorderWidth="0" BorderColor="White" BackColor="White">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label Style="padding-left: 5px;" ID="lblPleaseSwipeCard" runat="server" Text="(Please Swipe the Card)"
                                    ForeColor="DarkBlue" Font-Size="X-Small" Font-Names="Verdana"></asp:Label>
                                </td>
                                <td style="width: 50px;" colspan="2">
                                    <asp:RequiredFieldValidator ID="rfvCardReader" runat="server" ControlToValidate="txtMembershipNo"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" Font-Names="Verdana"
                                        Font-Size="Small" ValidationGroup="1" SetFocusOnError="true">Membership No. Required field</asp:RequiredFieldValidator>
                                </td>
                                <td></td>
                            </tr>
                            <%--Start Aramco Addition For Batch Number--%>
                            <tr>
                                <td class="style3">
                                    <div id="Div2" runat="server" class="right-div">
                                        <span style="color: #ff0000"></span><asp:Label ID="Label4" runat="server" Text="Badge Number" Width="115px"></asp:Label>
                                    </div>
                                </td>
                                <td>
                                    <asp:TextBox CssClass="textbox" ID="txtAramcoBadgeNumber" runat="server" ValidationGroup="1" Width="100%" onchange="changetext('rfvCardReader')"
                                        MaxLength="11"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label Style="padding-left: 31px; color:red;" ID="Label5" runat="server" Text="(Only for Aramco)" ForeColor="DarkBlue" Font-Size="X-Small" Font-Names="Verdana"></asp:Label>
                                </td>
                                <td style="width: 50px;" colspan="2">
                                    <%--<asp:RequiredFieldValidator ID="reqFieldValAramcoBadgeNumber" runat="server" ControlToValidate="txtAramcoBadgeNumber"
                                        Display="Dynamic" ErrorMessage="Badge No. Required field(Only for Aramco) " Font-Bold="False" Font-Names="Verdana"
                                        Font-Size="Small" ValidationGroup="1" SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="1" ControlToValidate="txtAramcoBadgeNumber" Display="Dynamic" ErrorMessage="Only numbers with hyphen.."
                                                    ValidationExpression="\d*-?\d*" />
                                </td>
                                <td></td>
                            </tr>
                            <%--End Aramco Addition--%>
                           
                            <tr>
                                <td class="style3">
                                    <asp:Label ID="Label2" runat="server" Height="24px" Text="Referral letter ?" Width="145px"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlReferalLetter" runat="server" Width="350px">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    <span style="color: #ff0000">*</span><asp:Label ID="Label3" runat="server" Text="Department" Width="121px"></asp:Label>
                                    <asp:XmlDataSource ID="XmlDataSourceDepartment" runat="server" DataFile="xmlfiles/Department.xml"></asp:XmlDataSource>
                                </td>
                                <td colspan="3">
                                    <div class="styled-select">
                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlDepartment" runat="server" Width="350px"
                                            DataSourceID="XmlDataSourceDepartment" DataTextField="Text" DataValueField="Value"
                                            ValidationGroup="1">
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDepartment"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" Font-Names="Verdana"
                                        Font-Size="Small" ValidationGroup="1" InitialValue="0">Department is Mandatory</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:TextBox CssClass="textbox" Enabled="true" ID="txtTrans" runat="server" Visible="false" BorderColor="White" Width="92px"></asp:TextBox>&nbsp;<asp:TextBox CssClass="textbox"
                                        Enabled="true" ID="txtNVerify" Height="0px" runat="server" BorderStyle="None"
                                        BorderColor="White" Width="0px"></asp:TextBox><br />
                                </td>
                            </tr>
                            <tr>

                                <td class="style2"></td>
                                <td style="width: 89px; height: 24px;">
                                    <asp:Button CssClass="submitButton" ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="1" /></td>
                                <td style="height: 24px"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="style2" colspan="4">
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align:top;  padding-top:10px;">
                        <asp:HiddenField ID="hidoverride" runat="server" />
                        <asp:Button ID="btnShow" Text="Override" Visible="true" runat="server" class="mybtn"/>
                        <ajax:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="btnShow"
                            BackgroundCssClass="modalBackground" PopupControlID="PnlShow">
                        </ajax:ModalPopupExtender>
                        <div id="PnlShow" runat="server" style="display: none; background-color: white;">
                            <div style="margin: 25px;">
                                <table>
                                    <tr>
                                        <td colspan="2"><strong>Please use this option ONLY if after swiping the membership card an  <br />
                                                                error is received or if there is no magnetic strip/data on the card:</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #ff0000">*</span>Membership Number

                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMembershipManually" runat="server" MaxLength="8" onkeypress="javascript:return allownumbers(event);" />

                                        </td>
                                        <td>
                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMembershipManually"
                                                Display="Dynamic" ErrorMessage="Please provide either membership or badge number" ValidationGroup="override" SetFocusOnError="true" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <span style="color: #ff0000">*</span>Badge Number <br /><span style="color:red; font-size:x-small">(Only for Aramco)</span>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtBadgeNumber" runat="server" MaxLength="11" />

                                        </td>
                                        <td>
                                               <asp:RequiredFieldValidator ID="ReqBadgeNumber" runat="server" ControlToValidate="txtBadgeNumber"
                                                Display="Dynamic" ErrorMessage="Please provide either membership or badge number" ValidationGroup="override" SetFocusOnError="true" />
                                               <asp:RegularExpressionValidator ID="RegularExpressByBadgeNumber" runat="server" ValidationGroup="override" ControlToValidate="txtBadgeNumber" Display="Dynamic" ErrorMessage="Only numbers with hyphen.."
                                                    ValidationExpression="\d*-?\d*" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #ff0000">*</span>Patient Mobile No</td>
                                        <td>
                                            <asp:TextBox ID="txtPatientMobile" runat="server" MaxLength="10" onkeypress="javascript:return allownumbers(event);" />
                                        </td>
                                        <td >
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPatientMobile"
                                                Display="Dynamic" ErrorMessage="Required Field" ValidationGroup="override" SetFocusOnError="true" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ValidationGroup="override"
                                                    ControlToValidate="txtPatientMobile" Display="Dynamic" ErrorMessage="Incorrect Mobile No."
                                                    ValidationExpression="[0][5][0-9]{8}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <asp:Button ID="btnUpdate" runat="server" Text="Proceed" OnClientClick="return SavePopUpValues()" ValidationGroup="override"/>
                                            </td>
                                        <td>
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="return Hidepopup()" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
			<label style="color: #ffffff;">31</label>
            <br />
        </fieldset>
    </div>
    <uc1:OSNav ID="OSNav1" runat="server" />
</asp:Content>