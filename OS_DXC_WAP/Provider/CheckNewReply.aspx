﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Provider_CheckNewReply" Codebehind="CheckNewReply.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<%@ Register src="../UserControls/OSNav.ascx" tagname="OSNav" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
<link rel="stylesheet" type="text/css" href="../javascript/examples1.css" />
<script src="../javascript/jquery.idletimer.js" type="text/javascript"></script>
<script src="../javascript/jquery.idletimeout.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $.idleTimeout('#idletimeout', '#idletimeout a', {
            idleAfter: 3510, // 3600sec minus 90sec = 3510 sec 
            pollingInterval: 2,
            keepAliveURL: 'keepalive.php',
            serverResponseEquals: 'OK',
            onTimeout: function () {
                $(this).slideUp();
                window.location = "../SessionExpired.aspx";
            },
            onIdle: function () {
                $(this).slideDown(); // show the warning bar
            },
            onCountdown: function (counter) {
                $(this).find("span").html(counter); // update the counter
            },
            onResume: function () {
                $(this).slideUp(); // hide the warning bar
            }
        });
    });
</script>
<table width="100%" border="0">
<tr>
<td>



     <fieldset>
                <legend><h1><asp:Label ID="lblAdd_Employee" runat="server"  Text="Check New Reply" Font-Size=Large Font-Names="Arial" ></asp:Label></h1></legend>        
     
      
        <table width=100% ><tr align="right"><td align=left>
            <label ID="Label1" ><font size=4></font></label>  </td>
            <td></td></tr></table>
            
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>

       
  <div class="noPrint" >   
        
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size="1"> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
     
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
  <asp:TextBox CssClass="textbox" ID="txtBox_output"  Visible="false" runat="server"></asp:TextBox>
       
        
      <p style="left:auto;" >
       <asp:HyperLink ID="lnkRefresh" runat="server" Text="Disable Refresh" NavigateUrl='?Refresh=F'></asp:HyperLink>
       </p>
        <asp:Repeater ID="Repeater1"   runat="server" DataSourceID="SqlDataSource1" >
        <HeaderTemplate>
        <table border="1" style="font-size:11px; font-family:Arial; cursor:hand;">
        <tr  bgcolor='#e6f5ff'>
        <td>Membership No</td><td>Member Name</td> <td>Reference No</td><td>Episode No</td> <td>Status</td> <td>Date & Time Received</td> <td>Date & Time Answered</td>   
        </tr>
        
        
        
        </HeaderTemplate>
        <ItemTemplate >
              <%  UrlValues = "PID=" + Eval("PREAUTH_ID") + "&PC=" + Eval("Prov_Code") + "&EN=" + Eval("EPISODE_ORDER") + "&OptionType=CNR";
                                                        UrlValues = Cryption.Encrypt(UrlValues);
                                                    %>

              <a href="PreauthDetails.aspx?val=<%=UrlValues%>">
            
                    <%-- <a href="PreauthDetails.aspx?PID=<%# Eval("PREAUTH_ID") %>&PC=<%# Eval("Prov_Code")%>&EN=<%# Eval("EPISODE_ORDER") %>&OptionType=CNR" >--%>

        <tr>
        <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("MBRSHP_NO") %>' /></td>
        <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("NAME") %>' /></td>
        <td><asp:Label ID="lblPreathID" runat="server" Text='<%# Eval("PREAUTH_ID") %>' /></td>
        <td><asp:Label ID="lblEpisodeNo" runat="server" Text='<%# Eval("EPISODE_ORDER") %>' /></td>
        <td><asp:Label ID="Label7" runat="server" Text='<%# Eval("STATUS") %>' /></td>
       

        
        <td><asp:Label ID="Label8" runat="server" Text='<%# Eval("RECEIVED_DATE") %>' /></td>
        <td><asp:Label ID="Label9" runat="server" Text='<%# Eval("ANSWERED_DATE") %>' /></td>
       
       
        <tr />
       </a>
        </ItemTemplate>
        <FooterTemplate>
        </table></FooterTemplate>
        
      
        
        </asp:Repeater>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            SelectCommand="SELECT top 29 [TXN_ID], [PROV_CODE] = RTrim([Prov_Code]), [MBRSHP_NO], [NAME], [PREAUTH_ID], [EPISODE_ORDER], [STATUS], [RECEIVED_DATE], [ANSWERED_DATE], [CHECKED_IND] FROM [PREAUTH_ALERT] where Prov_code = '20087' and received_date like '%2009%' and CHECKED_IND = 'N' order by received_date desc ">
            <SelectParameters>
                <asp:Parameter DefaultValue="'N'" Name="CHECKED_IND" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
           
        
    <asp:DataList ID="DataList1" runat="server" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingItemStyle BackColor="White" />
        <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
    </asp:DataList>
   

   </td>
</tr>

</table>

</fieldset>
<br />
<uc1:OSNav ID="OSNav2" runat="server" />
</asp:Content>

