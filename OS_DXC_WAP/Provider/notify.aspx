﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Provider_notify" Codebehind="notify.aspx.cs" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">


<script type="text/javascript">
    var index = -1;

    function grid_RowClick(s, e) {
        if (grid.IsEditing() == true) {
            index = e.visibleIndex;

            s.UpdateEdit();
        }
        else {
            s.SetFocusedRowIndex(e.visibleIndex); // for better visual appearence                                
            s.StartEditRow(e.visibleIndex);
        }
    }

    function grid_EndCallback(s, e) {
        if (index != -1) {
            var _index = index;
            index = -1;

            s.SetFocusedRowIndex(_index); // for better visual appearence

            s.StartEditRow(_index);
        }
    }
    </script>



    <table width="100%" border="0">
     <tr>
                    <td style="text-align:left" colspan="5">
                    
                        <h2>Notification Service Code Requests</h2></td>
                </tr>
    <tr>
                    <td style="width: 102px;text-align:left">
                    
                        &nbsp;</td>
                    <td style="width: 157px;text-align:left">
                    
                        &nbsp;</td>
                    <td style="width: 98px">
                    
                        &nbsp;</td>
                    <td style="text-align:left">
                    
                        &nbsp;</td>
                    <td style="text-align:left">
                    
                        &nbsp;</td>
                </tr>
    <tr>
                    <td style="text-align:left" colspan="2">
                    
                        <b>Provider Name: </b>
                    
                        <dx:ASPxLabel ID="lblProName" runat="server" Text="">
                        </dx:ASPxLabel>
                    
                    </td>
                    <td colspan="2">
                    
                        <b>Provider Code: </b>
                    
                        <dx:ASPxLabel ID="lblProCode" runat="server" Text="">
                        </dx:ASPxLabel>
                    
                    </td>
                    <td align=right>
                    
            <dx:ASPxButton ID="btnNew" runat="server" Text="Add New Request" 
                Theme="Office2010Blue" onclick="btnNew_Click" Font-Bold="False" Font-Size="10pt" 
                            Width="211px">
            </dx:ASPxButton>
                    </td>
                </tr>
    <tr>
        <td colspan="5" align="right">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="5">
            <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" DataSourceID="SqlDataSource1"
        KeyFieldName="RefNo" Width="100%" AutoGenerateColumns="False" 
                Theme="Office2010Blue" on>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" 
                VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="RefNo" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ProviderCode" Visible="False" 
                VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="UpdateDate" VisibleIndex="4">
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn VisibleIndex="5" Caption="Details">
                <DataItemTemplate>                   
                    <a href='newservicecode.aspx?val=<%#Cryption.Encrypt("refno=" + Eval("RefNo") + "&mod=vw&s=" +Eval("Status"))%>' target="_blank" >View</a>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        
      
    </dx:ASPxGridView>




           
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">


 <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>" 
                DeleteCommand="DELETE FROM ServicesCode WHERE (Service_Code = @Service_Code)" 
                
                
                
                
                SelectCommand="SELECT id, RefNo, ProviderCode, Status, UpdateDate FROM ServicesCode WHERE (ProviderCode = @ProviderCode) AND (Status <> N'initi') ORDER BY RefNo DESC">
                <DeleteParameters>
                    <asp:Parameter Name="Service_Code" />

                </DeleteParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="ProviderCode" SessionField="ProviderID" />
                    
                </SelectParameters>
            </asp:SqlDataSource>
            <dx:ASPxLabel ID="lblError" runat="server" Text="">
            </dx:ASPxLabel>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
  </table>
</asp:Content>

