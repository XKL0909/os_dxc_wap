﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Provider_Main : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            {
                Response.Redirect("../default.aspx");
            }
            else
            {
                // if (Convert.ToString(Session["SuperUser"]) == "True")
                // {
                    // PopUpIsSuperUser.Attributes.Add("style", "display:block");
                // }
                // else
                // {
                    // PopUpIsSuperUser.Attributes.Add("style", "display:none");
                // }
                
                
                //Start Account lockout Password Expiry Popup
                if (ConfigurationManager.AppSettings["isProviderAccountLockedEnabled"].ToString().ToUpper() == "TRUE")
                {
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["PasswordExpiryDate"])))
                    {
                        DateTime dtPWDExp = Convert.ToDateTime(Session["PasswordExpiryDate"]);
                        int getDays = (int)(dtPWDExp - DateTime.Today).TotalDays;
                        TimeSpan totalvalue = dtPWDExp.Subtract(DateTime.Today);
                        string ExpDateEng = "";
                        string ExpDateAr = "";
                        int ExpDays = Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpMsgDays"]);
                        if (totalvalue.Ticks > 0)
                        {
                            DateTime daysLeft = DateTime.Today.AddDays(getDays);
                            ExpDateEng = daysLeft.ToString("d-MMMM-yyyy");
                            ExpDateAr = daysLeft.ToString("d-MMMM-yyyy");
                        }
                        if (getDays == 1)
                        {
                            btnLater.Visible = true;
                            popupPasswordExpiry.Attributes.Add("style", "display:inline");
                            ExpMessage.InnerHtml = "This is a system generated alert to inform you that your password is due to expire on " + ExpDateEng + ". <br />Please reset";
                            ExpMessagear.InnerHtml = "هذا تنبيه تم إنشاؤه بواسطة النظام لإعلامك بأن كلمة المرور الخاصة بك سوف تنتهي في" + "<br />" + ExpDateEng + "<br />يرجى إعادة تعيينها";
                        }
                        else if (getDays < 0)
                        {
                            btnLater.Visible = false;
                            popupPasswordExpiry.Attributes.Add("style", "display:inline");
                            ExpMessage.InnerHtml = "Your password is expired";
                            ExpMessagear.InnerHtml = "لقد تم انتهاء صلاحية كلمة المرور الخاصة بك";
                        }
                        else if (getDays <= ExpDays)
                        {
                            btnLater.Visible = true;
                            popupPasswordExpiry.Attributes.Add("style", "display:inline");
                            ExpMessage.InnerHtml = "This is a system generated alert to inform you that your password is due to expire on " + ExpDateEng + ". <br />Please reset";
                            ExpMessagear.InnerHtml = "هذا تنبيه تم إنشاؤه بواسطة النظام لإعلامك بأن كلمة المرور الخاصة بك سوف تنتهي في" + "<br />" + ExpDateEng + "<br />يرجى إعادة تعيينها";
                        }
                        else
                        {
                            popupPasswordExpiry.Attributes.Add("style", "display:none");
                        }
                        //if (dtPWDExp <= DateTime.Today)
                        //{
                        //    btnLater.Visible = false;
                        //    popupPasswordExpiry.Attributes.Add("style", "display:none");
                        //}
                        //else if (dtPWDExp > DateTime.Today)
                        //{
                        //    popupPasswordExpiry.Attributes.Add("style", "display:none");
                        //}
                        //else
                        //{
                        //    popupPasswordExpiry.Attributes.Add("style", "display:none");
                        //}

                    }
                    else
                    {
                        popupPasswordExpiry.Attributes.Add("style", "display:none");
                    }
                }
                else
                {
                    popupPasswordExpiry.Attributes.Add("style", "display:none");
                }


                if (Session["TOBDisable"] == null)
                {
                    if (DateTime.Now.Date > Convert.ToDateTime(ConfigurationManager.AppSettings["TOBPopUpStartDate"]) && DateTime.Now.Date < Convert.ToDateTime(ConfigurationManager.AppSettings["TOBPopUpEndDate"]))
                        PopUpTOB.Attributes.Add("style", "display:block");
                    else
                        PopUpTOB.Attributes.Add("style", "display:none");

                    Session["TOBDisable"] = "Yes";
                }
                else
                    PopUpTOB.Attributes.Add("style", "display:none");


                //End Account lockout Password Expiry Popup

                if (!IsPostBack)
                {
                    /*   if (System.DateTime.Today > Convert.ToDateTime("31/08/2013"))
                       {
                           NotifyBox.Visible = false;
                       }
                       else
                       {
                           NotifyBox.Visible = true;
                       }
                     * */

                }
            }
        }
        catch (Exception ex) {
            popupPasswordExpiry.Attributes.Add("style", "display:none");
            PopUpIsSuperUser.Attributes.Add("style", "display:none");
        }
    }
    
    
}