using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

public partial class Provider_ChangePass : System.Web.UI.Page
{
    string strClientID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ProviderID"] == null)
            Response.Write("<script>window.open('../providerdefault.aspx','_parent');</script>");
        else
        {
            strClientID = Session["ProviderID"].ToString();
        }
    }
    protected void JoinBtn_Click(object sender, EventArgs e)
    {
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        string result = "";
        try
        {
            SqlParameter[] arParms = new SqlParameter[7];
            arParms[0] = new SqlParameter("@UserID", SqlDbType.NVarChar, 30);
            arParms[0].Value = Session["ProviderID"].ToString();
            arParms[1] = new SqlParameter("@Username", SqlDbType.NVarChar, 30);
            arParms[1].Value = Session["ProviderUserName"].ToString();
            arParms[2] = new SqlParameter("@OldPassword", SqlDbType.NVarChar, 30);
            arParms[2].Value = txtOldPassword.Text;
            arParms[3] = new SqlParameter("@NewPassword", SqlDbType.NVarChar, 30);
            arParms[3].Value = txtPassword.Text;
            arParms[4] = new SqlParameter("@UserType", SqlDbType.NVarChar, 30);
            arParms[4].Value = "Provider";
            arParms[5] = new SqlParameter("@EncryptedPWD", SqlDbType.NVarChar, 64);
            arParms[5].Value = BUPA_EncryptWithSha256.EncryptionManager.Encrypt(txtPassword.Text);
            arParms[6] = new SqlParameter("@Result", SqlDbType.NVarChar, 30);
            arParms[6].Direction = ParameterDirection.Output;
            //SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spChangeUserPassword", arParms);//
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spChangeUserPassword_Hash", arParms);
            result = arParms[6].Value.ToString();
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }

        lblMessage.Visible = true;
        if (result == "Found")
        {
			Session["ProPasswordExpiryDate"] = "";
            PnlChangePass.Visible = false;
            lblMessage.ForeColor = System.Drawing.Color.Green; //.Black; 
            lblMessage.Text = "<BR>Password successfully changed.";
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "Incorrect Password. Please type in the correct 'Old Password'.";
        }
    }
}
