﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="TOBSchemeDetails" Codebehind="TOBSchemeDetails.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
    <script type="text/javascript"  src="js/jquery.dataTables.js"></script>

   <meta http-equiv="Page-Enter" content="progid:DXImageTransform.Microsoft.Wipe(
        GradientSize=1.0, wipeStyle=1, motion='forward', duration=5)" />
<meta http-equiv="Page-Exit" content="progid:DXImageTransform.Microsoft.Pixelate(duration=7)" />
<link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen" />
    
<style type="text/css">
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>     
    
    
</head>
<body style="font-family:Arial; font-style:normal; font-size:11px;">
  <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Customer Service Tel No. 800 244 0307</span>
        <hr>
    </div>
    
   <div class="noPrint">   
        <table width=100% ><tr><td align=left>
            <span style="font-family: Arial"><span style="font-size: 11pt">
            <font size=4></font>  
            </span></span>
        </td>
            <td style="width:185px; text-align:right;" ><a href="#" onclick="window.print()">
                <img class="noPrint" border="0" src="images/printer.gif" alt="Print" width="16" height="16" ></a>
                 <img alt="Go back" style="cursor:pointer;" src="../icons/back4.png" onClick="history.go(-1)"  />&nbsp;
            </td></tr></table>
         </div>   
            
            

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        
 <asp:UpdateProgress   ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <img src="spinner.gif" width="33" height="33" />
                Updating Page ......
            </ProgressTemplate>
        </asp:UpdateProgress>
     
     
     
     
      <div>
            <asp:Label ID="lblTableOfBenefit" runat="server" Text="Table Of Benefits" Font-Bold="True" Font-Names="Arial" Font-Size="Medium"></asp:Label><br />
       <br />
               <asp:Label ID="Message1" runat="server" Height="15px" Text="" Width="527px"></asp:Label><br />

       <asp:Panel ID="Panel1" runat="server" Height="50px" Width="100%">
        <table width="100%" border=1  style=" border-style:none ; border-left-width: thin; border-left-color: silver; border-bottom-width: thin; border-bottom-color: silver; border-top-color: silver; border-right-width: thin; border-right-color: silver;">
            <tr>
                <td width="25%">
                        <asp:Label ID="lblCompany_Name" runat="server" Text="Company Name" Font-Bold="True"></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblCustomer_ID" runat="server" Text="Customer ID" Font-Bold="True"></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblEffective_Date" runat="server" Text="Effective Date" Font-Bold="True"></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblScheme_Name" runat="server" Text="Scheme Name" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td width="25%">
                        <asp:Label ID="lblCompanyName" runat="server" Text=""></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblCustomerID" runat="server" Text=""></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblEffectiveDate" runat="server" Text=""></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblSchemeName" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td width="25%">
                        <asp:Label ID="lblTreatment_Type" runat="server" Text="Treatment Type" Font-Bold="True"></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblHospital_Accommodation" runat="server" Text="Hospital Accommodation" Font-Bold="True"></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblPreauthorisation_Limit" runat="server" Text="Preauthorisation Limit" Font-Bold="True"></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblOver_All_Limit" runat="server" Text="Over All Limit" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td width="25%">
                        <asp:Label ID="lblTreatmentType" runat="server" Text=""></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblHospitalAccommodation" runat="server" Text=""></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblPreauthorisationLimit" runat="server" Text=""></asp:Label></td>
                <td width="25%">
                        <asp:Label ID="lblOverAllLimit" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
        </asp:Panel>
          <br />
          <br />
          <br />
          <br />
 <div id="ServiceListReport"  visible="true" style=" font-family:Arial; ; height:auto; width:100%; " runat="server" >   </div>

        
        
         
        <br />
        <asp:Panel ID="Panel3" runat="server" Height="50px" Width="100%">
            <table  width="100%"  border="1"  style=" border-style:none ; border-left-width: thin; border-left-color: silver; border-bottom-width: thin; border-bottom-color: silver; border-top-color: silver; border-right-width: thin; border-right-color: silver;" >
            <thead>
                <tr>
                    <td style="width: 157px" > &nbsp;
                    </td>
                    <th style="width: 100px">
                        <asp:Label ID="lblMainMemberAmount" runat="server" Text="Main Member (amount"></asp:Label></th>
                    <th style="width: 100px">
                        <asp:Label ID="lblDependentAmount" runat="server" Text="Dependent (amount)"></asp:Label></th>
                    <th style="width: 100px">
                        <asp:Label ID="lblMainMember" runat="server" Text="Main Member (%)"></asp:Label></th>
                    <th style="width: 100px">
                        <asp:Label ID="lblDependent" runat="server" Text="Dependent (%)"></asp:Label></th>
                </tr>
                </thead>
                <tr>
                    <td style="width: 157px">
                        <asp:Label ID="lblOutPatientDeductible" runat="server" Text="Out Patient Deductible" Font-Bold="True"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:Label ID="lblOPDMA" runat="server"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:Label ID="lblOPDDA" runat="server"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:Label ID="lblOPDM" runat="server"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:Label ID="lblOPDD" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 157px">
                        <asp:Label ID="lblInPatientDeductible" runat="server" Text="In Patient Deductible" Font-Bold="True"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:Label ID="lblIPDMA" runat="server"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:Label ID="lblIPDDA" runat="server"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:Label ID="lblIPDM" runat="server"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:Label ID="lblIPDD" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 157px">
                        <asp:Label ID="lblReferralLetterRequired" runat="server" Text="Referral letter required" Font-Bold="True"></asp:Label></td>
                    <td colspan="4">
                        <asp:Label ID="lblRLRMA" runat="server"></asp:Label><asp:Label ID="lblRLRDA" runat="server"></asp:Label><asp:Label ID="lblRLRM" runat="server"></asp:Label><asp:Label ID="lblRLRD" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 157px">
                        &nbsp;
                        <asp:Label ID="lblTOBSpecialInstruction000" runat="server" Text="TOB Special Instruction" Font-Bold="True" Width="147px"></asp:Label></td>
                    <td colspan="4">
                        <asp:Label ID="lblTSIMA" runat="server"></asp:Label><asp:Label ID="lblTSIDA" runat="server"></asp:Label><asp:Label ID="lblTSIM" runat="server"></asp:Label><asp:Label ID="lblTSID" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblTOBSpecialInstruction" runat="server" Font-Bold="False" Text="TOB Special Instruction"></asp:Label></td>
                </tr>
            </table>
        </asp:Panel>
        <br /><br />
          <br />
          <br />
          <br />
        <asp:Panel ID="Panel4" runat="server" Visible="false" Height="50px" Width="100%">
           <br /> <fieldset>
                <div align="center" style="text-align: left">
                    <b>
                        
                            <u>
                                <br />
            General Exclusions (Unless otherwise stated in above Benefit Table)</u>
                    </b>
                </div>
                <div align="left">
                    <ul>
                        <li type="A">
                            <b>
                                This policy will not cover claims arising out of the following:</b>
                            <ul>
                                <li type="disc">Injury caused deliberately by the person. </li>
                                <li type="disc">Illnesses, caused by misuse of certain medicaments, stimulants, tranquilizers
                                    or by consumption of alcoholic drinks, drugs and the like. </li>
                                <li type="disc">Plastic surgery or treatment unless necessitated by an accidental bodily
                                    injury not excluded in this part. </li>
                                <li type="disc">Full checkups, vaccinations, drugs or preventive procedures that do
                                    not require any medical treatment stipulated in this policy (with the exception
                                    of the preventive procedures stated by the Ministry of
            Health such as, vaccinations
                                    and maternity &amp; childhood care). </li>
                                <li type="disc">Treatment related to pregnancy and delivery for a woman on a single
                                    status contract.</li><li type="disc">Free of charge treatment of an insured person. </li>
                                <li type="disc">Rest, general body health programs and treatment at social welfare houses.</li><li type="disc">Any illness or injury resulting directly from the profession of the
                                    insured. </li>
                                <li type="disc">Treatment of genital diseases and the medically recognized diseases
                                    usually communicated by sexual intercourse. ' </li>
                                <li type="disc">The treatment expenses for the period following the diagnosis or HIV
                                    or the diseases related to HIV including AIDS (Acquired Immunity Deficiency Syndrome)
                                    their derivatives, synonyms or other forms. </li>
                                <li type="disc">All costs related to teeth transplanting, dentures, bridgework fixed
                                    &amp; removab1e), orthodontics- excluding those caused by violent external actions.
                                </li>
                                <li type="disc">Tests for correction of sight &amp; hearing and audio - visual aids,
                                    unless ordered by a licensed physician. </li>
                                <li type="disc">Expenses of transportation of an insured by local or authorized ambulances
                                    or by ambulances belonging to the Saudi Red Crescent Association. </li>
                                <li type="disc">Hair falling, baldness or wigs. </li>
                                <li type="disc">Psychotherapy, mental or neurotic disorders excluding acute cases. </li>
                                <li type="disc">Allergy tests of whatever nature excluding those related to drugs, diagnosis
                                    or treatment. </li>
                                <li type="disc">Equipment, aids, drugs, procedures or treatment by hormones for birth control, inducing or preventing pregnancy, sterility, impotency, lack of fertility,
                                    tube fertilization or any other means of artificial insemination. </li>
                                <li type="disc">Any defects or congenital deformities existing before the effective
                                    date of policy and posing no threat to life. </li>
                                <li type="disc">Any additional costs or expenses incurred by the person escorting the
                                    insured during his hospitalization or stay in hospital with the exception of hospital
                                    room &amp; board costs for one escort per an insured, i.e. the accompanying of the
                                    mother of her child ? up to twelve years old or whenever this is medically necessary,
                                    all at the discretion of the treating doctor. </li>
                                <li type="disc">Treatment of acne or any other treatments relating to obesity or overweight.
                                </li>
                                <li type="disc">Transplant of organs taken from other person, bone marrow and artificial
                                    limbs replacing any organ in the body. </li>
                                
                            </ul><br />
                        </li>
                        <li type="A">
                            <b>
                                This policy will not cover the health benefits and repatriation of the remains to
                                country of origin if claims are directly arising from the following:</b>
                            <ul>
                                <li type="disc">War, invasion, foreign enemy actions, aggressive actions (whether war
                                    declared or not) and civil war. </li>
                                <li type="disc">Ionic radiation and contamination with radio active material resulting
                                    from nuclear fuel or any nuclear waste resulting from the burning of nuclear fuel.
                                </li>
                                <li type="disc">The radioactive, poisonous, explosive properties or any other hazardous
                                    properties of any nuclear materials stored or any of their components. </li>
                                <li type="disc">The insured involvement or participation in the service of the armed
                                    forces, police or in any of their operations. </li>
                                <li type="disc">Riots, strikes terrorism or any similar acts. </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </fieldset>
            
            </asp:Panel>
         
         
         
         
         
         
                  <asp:Panel ID="Panel5"  Visible="false"  runat="server" Height="50px" Width="100%">
              <br />
              <fieldset>
                  <div align="center" style="text-align: left">
                      <div align="center">
                          <h5>
                              <b><u>
                  Pre-authorisation (Please refer to the Benefit Table Pre-authorisation section)</u></b></h5>
                      </div>
                      <p align="left">
                  
Unless otherwise specified in the Benefit Table Pre-authorization required for MRI, CT-Scan and if treatment exceeds the SR1000 and for All Inpatients and Daycare, except emergencies, which must be notified within 24 hours of admission. Ambulance is covered for all plans based on medical necessity to and from hospital. 
                      </p>
                      <div align="center">
                          <b>
                              <h3>
                                  <u>
                          General Exclusions (Unless otherwise stated in above Benefit Table)</u></h3>
                          </b>
                      </div>
                      <div align="left">
                          <ul>
                              <li type="disc">Pregnancy and childbirth </li>
                              <li type="disc">Contraception, abortion, sterilization, sexual problems (including impotence),
                                  sex changes, assisted reproduction (for example IVF treatment), termination of pregnancy 
                              </li>
                              <li type="disc">Dialysis </li>
                              <li type="disc">Naturally occurring conditions </li>
                              <li type="disc">AIDS, HIV, and sexually transmitted diseases </li>
                              <li type="disc">Cosmetic treatment unless it is a surgical operation to restore a person´s
                                  appearance after an accident or as a result for surgery for cancer </li>
                              <li type="disc">Contamination, war, riots and natural disasters </li>
                              <li type="disc">Health hydrous, spa baths, nature cure clinics or similar establishments</li>
                              <li type="disc">Psychiatric Treatment, Mental and addictive conditions </li>
                              <li type="disc">Problems present at birth – congenital </li>
                              <li type="disc">Neo-natal problems </li>
                              <li type="disc">Vaccinations </li>
                              <li type="disc">Nursing at home </li>
                              <li type="disc">Work related accidents to the extent that the cost of treatment is payable
                                  by GOSI </li>
                              <li type="disc">Artificial body parts, surgical aids and appliances unless implanted
                                  into the body as an integral part of a surgical procedure </li>
                              <li type="disc">Weight control </li>
                              <li type="disc">Routine Dental treatment </li>
                              <li type="disc">Road traffic accidents to the extent that the cost of treatment is payable
                                  by a third party.</li>
                              <li type="disc">Circumcision </li>
                              <li type="disc">Body piercing </li>
                              <li type="disc">Hazardous sports </li>
                              <li type="disc">Treatment not related to a medical condition </li>
                              <li type="disc">Un-prescribed drugs </li>
                              <li type="disc">Military service </li>
                              <li type="disc">Physiotherapy unless as part of post-surgical treatment authorised by a licensed medical practitioner </li>
                              <li type="disc">Acupuncture, osteopathy, chiropractic and homeopathy </li>
                              <li type="disc">Self-inflicted </li>
                              <li type="disc">Associated non-medical and personal expenses </li>
                          </ul>
                      </div>
                  </div>
                                  <p>
                                      &nbsp;</p>
              </fieldset>
          </asp:Panel>
      </div> 
     
       <br />
        <div>
         
            <asp:TextBox CssClass="textbox" ID="txtTxnID" runat="server" Visible="False">88881051</asp:TextBox>
            &nbsp; &nbsp;
            <asp:TextBox CssClass="textbox"   ID="txtMbrShp" runat="server" Visible="False">3208733</asp:TextBox>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            <br />
            <br />
           
            &nbsp;<br />
     
        </div>

        
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

    </form>
    
    
  
</body>
</html>
