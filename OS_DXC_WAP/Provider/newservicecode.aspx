﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Print.master" AutoEventWireup="true" Inherits="Provider_newservicecode" Codebehind="newservicecode.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #mainContainer td.buttonCell {
            padding-top: 15px;
        }

        #mainContainer td.caption {
            padding-right: 5px;
            padding-top: 4px;
            vertical-align: top;
        }

        #mainContainer td.content {
            padding-bottom: 20px;
        }

        #mainContainer td.imagePreviewCell {
            border: solid 2px gray;
            width: 110px;
            height: 115px;
            /*if IE*/
            height: expression("110px");
            text-align: center;
        }

        #mainContainer td.note {
            text-align: left;
            padding-top: 1px;
        }

        @media print {
            .noPrint {
                display: none;
            }

            .PrintMe {
                display: block;
            }
        }

        .style1 {
            width: 124px;
        }
    </style>
    <script type="text/javascript">
        function PrintMe() {
            document.getElementById('tblFooter').style.display = 'block';
            window.print();
            window.close()
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" runat="Server">

    <table align="center" style="height: 100%; width: 800px">
        <tr>
            <td style="text-align: left" colspan="5" align="center">

                <center>
                    <h2>Notification Service Code Request Form</h2>
                </center>
            </td>
        </tr>
        <tr>
            <td style="width: 102px; text-align: left">&nbsp;</td>
            <td style="width: 157px; text-align: left">&nbsp;</td>
            <td style="width: 98px">&nbsp;</td>
            <td style="text-align: left">&nbsp;</td>
            <td style="text-align: right">
                <table align="right" class="noPrint">
                    <tr>
                        <td><a href="#" onclick="PrintMe();">Print</a> |</td>
                        <td><a href="#" onclick="javascript:window.close();">Close</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: left" colspan="5">
                <table width="100%">
                    <tr>
                        <td style="width: 50%"><strong>Bupa&nbsp; Arabia</strong></td>
                        <td style="width: 50%">

                            <b>Provider Name: </b>

                            <dx:ASPxLabel ID="lblProName" runat="server" Text="">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%">

                            <strong>PO: 23807</strong></td>
                        <td style="width: 70%">
                            <b>Provider Code: </b>

                            <dx:ASPxLabel ID="lblProCode" runat="server" Text="">
                            </dx:ASPxLabel>

                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%">

                            <strong>Location: Jeddah - 21436</strong></td>
                        <td style="width: 70%">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td style="text-align: right" colspan="5">

                <b>&nbsp;</b><b>&nbsp; </b>

            </td>
        </tr>

        <tr>
            <td style="width: 102px; text-align: left">&nbsp;</td>
            <td style="width: 157px; text-align: left">&nbsp;</td>
            <td style="width: 98px">&nbsp;</td>
            <td style="text-align: left">&nbsp;</td>
            <td style="text-align: left"></td>
        </tr>
        <tr runat="server" id="hr">
            <td style="width: 102px">

                <b>Service Code</b></td>
            <td style="width: 157px; text-align: left">

                <b>Description</b></td>
            <td style="width: 98px">

                <b>Price</b></td>
            <td align="left">

                <b>Service type</b></td>
            <td align="left">&nbsp;</td>
        </tr>
        <tr runat="server" id="hr1">
            <td style="width: 102px">

                <dx:ASPxTextBox ID="txtServiceCode" runat="server" Width="170px">
                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"></ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td style="width: 157px; text-align: left">

                <dx:ASPxTextBox ID="txtDesc" runat="server" Width="170px">
                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"></ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td style="width: 98px">

                <dx:ASPxTextBox ID="txtPrice" runat="server" Width="170px">
                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true">
                        <RegularExpression ValidationExpression="^[$]?([0-9][0-9]?([,][0-9]{3}){0,4}([.][0-9]{0,4})?)$|^[$]?([0-9]{1,14})?([.][0-9]{1,4})$|^[$]?[0-9]{1,14}$" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td align="left">


                <dx:ASPxComboBox ID="cmServiceType" runat="server" Theme="Office2010Blue"
                    SelectedIndex="0">
                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true">
                        <RequiredField IsRequired="True"></RequiredField>
                    </ValidationSettings>
                    <Items>
                        <dx:ListEditItem Selected="True" Text="-- Select --" Value="" />
                        <dx:ListEditItem Text="Other Services" Value="Other Services" />
                        <dx:ListEditItem Text="Package" Value="Package" />
                        <dx:ListEditItem Text="Consultation" Value="Consultation" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
            <td align="left">


                <dx:ASPxButton ID="btnAdd" runat="server" Text="Add"
                    Theme="Office2010Blue" Width="138px" OnClick="btnAdd_Click" Font-Bold="True"
                    Style="font-weight: 700">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr runat="server" id="hr2">
            <td colspan="5">

                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="5" align="center">

                <dx:ASPxLabel ID="lblError" runat="server" Text="" Style="font-weight: 700">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="right">

                <b>RefNo: </b>
                <dx:ASPxLabel ID="lblRefNo" runat="server" Text="" EncodeHtml="False">
                </dx:ASPxLabel>
                \  <b>Status: </b>
                <dx:ASPxLabel ID="lblstatus" runat="server" Text="" Visible="true">
                </dx:ASPxLabel>

            </td>
        </tr>
        <tr>
            <td colspan="5" style="height: 100%">

                <dx:ASPxGridView ID="detailGrid" runat="server" DataSourceID="sqldsDeatils"
                    Width="100%"
                    AutoGenerateColumns="False" Theme="Office2010Blue" KeyFieldName="id"
                    OnCustomCallback="detailGrid_CustomCallback">
                    <Columns>
                        <dx:GridViewCommandColumn Name="CommandColumn" VisibleIndex="0">
                            <EditButton Visible="True">
                            </EditButton>
                            <DeleteButton Visible="True">
                            </DeleteButton>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="Service_Code" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn FieldName="Price" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Service_Type" VisibleIndex="4">
                            <EditItemTemplate>
                                <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" SelectedIndex="0"
                                    Value='<%# Bind("Service_Type") %>'>
                                    <Items>


                                        <dx:ListEditItem Text="Other Services" Value="Other Services" />
                                        <dx:ListEditItem Text="Package" Value="Package" />
                                        <dx:ListEditItem Text="Consultation" Value="Consultation" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </EditItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="5" Name="StatusR">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Remark" VisibleIndex="6" Name="RemarkR">
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="7"
                            Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>

                    </Columns>
                    <SettingsBehavior ConfirmDelete="True" />
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowFooter="True" ShowFilterRow="True" />

                </dx:ASPxGridView>
            </td>
        </tr>
        <tr class="noPrint">
            <td colspan="5" align="left">


                <table border="0" cellpadding="0" cellspacing="0" id="mainContainer" width="100%">
                    <tr>
                        <td valign="top" align="center" class="content">
                            <table cellpadding="0" cellspacing="0" runat="server" id="tblUp" width="100%">
                                <tr>
                                    <td align="left" style="vertical-align: top;">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="caption" style="width: 124px">
                                                    <dx:ASPxLabel ID="lblSelectImage" runat="server" Text="Upload Document:" AssociatedControlID="uplImage">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td align="left">

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                        ErrorMessage="*" ControlToValidate="fileupload" ValidationGroup="Upload"></asp:RequiredFieldValidator>

                                                    <asp:FileUpload ID="fileupload" runat="server" />
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="style1"></td>
                                                <td class="note">
                                                    <asp:RegularExpressionValidator runat="server" ID="valUpTest" ControlToValidate="fileupload" ValidationGroup="Upload"
                                                        ErrorMessage="Files must be of the following types: ( pdf, jpg, jpeg, gif, png, bmp)"
                                                        ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.jpeg|.JPEG|.bmp|.BMP|.png|.PNG|.pdf)$" />

                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center" class="buttonCell">
                                                    <asp:Button ID="btnUpload" runat="server" Text="Upload" Width="100px" OnClick="btnUpload_Click" ValidationGroup="Upload" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center" class="buttonCell">&nbsp;</td>
                                            </tr>
                                        </table>
                                        <img src="../Content/ImagePreview.gif" id="previewImage" alt="" style="display: none" />
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                                            ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
                                            SelectCommand="SELECT LTRIM(RTRIM(Doc_path)) AS Doc_path FROM ServicesCode_Docs WHERE (RefNo = @RefNo)">
                                            <SelectParameters>
                                                <asp:QueryStringParameter Name="RefNo" QueryStringField="refno" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


                <dx:ASPxButton ID="btnSubmitDoc" runat="server" Text="Submit1"
                    Theme="Office2010Blue" Width="138px" CausesValidation="False"
                    Visible="false" OnClick="btnSubmitDoc_Click">
                </dx:ASPxButton>

                <dx:ASPxTreeList ID="ASPxTreeList1" runat="server" AutoGenerateColumns="False" Width="100%"
                    DataSourceID="SqlDataSource1" EnableTheming="True" Theme="Office2010Blue"
                    Caption="Uploaded Documents" OnCustomCallback="ASPxTreeList1_CustomCallback" ClientInstanceName="treeList">
                    <Columns>
                        <dx:TreeListTextColumn Caption="Documents" FieldName="Doc_path"
                            VisibleIndex="0">
                            <DataCellTemplate>
                                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" EnableTheming="True"
                                    NavigateUrl='<%# String.Format("../backend/Uploads/{0}", Eval("Doc_path")) %>'
                                    Text='<%#Eval("Doc_path")%>' Theme="Office2010Blue" Target="_blank" />
                            </DataCellTemplate>
                        </dx:TreeListTextColumn>
                    </Columns>
                    <Settings ShowColumnHeaders="False" />
                </dx:ASPxTreeList>

            </td>
        </tr>
        <tr class="noPrint">
            <td colspan="4" align="left">&nbsp;</td>
            <td align="right" valign="bottom">
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit"
                    Theme="Office2010Blue" Width="138px" CausesValidation="False"
                    OnClick="btnSubmit_Click">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td colspan="5" valign="bottom" style="height: 150px">

                <table width="100%" id="tblFooter" style="display: none; vertical-align: bottom;">
                    <tr>
                        <td style="width: 70%">&nbsp;</td>
                        <td style="width: 50%; text-align: left;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 70%">&nbsp;</td>
                        <td style="width: 50%; text-align: left;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 70%">&nbsp;</td>
                        <td style="width: 50%; text-align: left;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 70%"><strong>Signature</strong></td>
                        <td style="width: 50%; text-align: left;">

                            <b>

                                <strong>Signature</strong></b></td>
                    </tr>
                    <tr>
                        <td style="width: 70%">&nbsp;</td>
                        <td style="width: 50%; text-align: left;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 70%">

                            <strong>Name:</strong></td>
                        <td style="width: 70%; text-align: left;">
                            <strong>Name: Mohammad Al Saeed</strong></td>
                    </tr>
                    <tr>
                        <td style="width: 70%"><strong>Designation:</strong></td>
                        <td style="width: 50%; text-align: left;">

                            <strong>Designation: Provider Relations Dirctor</strong></td>
                    </tr>
                    <tr>
                        <td style="width: 70%">&nbsp;</td>
                        <td style="width: 50%; text-align: left;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 70%">

                            <strong>Date:</strong></td>
                        <td style="width: 70%; text-align: left;">

                            <strong>Date:</strong></td>
                    </tr>
                    <tr>
                        <td style="width: 70%">&nbsp;</td>
                        <td style="width: 70%; text-align: left;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 70%">

                            <strong>Provider Stamp: </strong></td>
                        <td style="width: 70%; text-align: left;"><strong><b>Bupa Stamp:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></td>
                    </tr>
                </table>

                <asp:SqlDataSource ID="sqldsDeatils" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
                    SelectCommand="SELECT [Service_Code], [Description], [Price], [Service_Type], [Status], [Remark], [id] FROM [ServicesCodeItems] WHERE ([RefNo] = @RefNo)"
                    ConflictDetection="CompareAllValues"
                    InsertCommand="INSERT INTO ServicesCodeItems(Service_Code, Description, Price, Service_Type, Status, Remark) VALUES (@Service_Code, @Description, @Price, @Service_Type, @Status, @Remark)"
                    OldValuesParameterFormatString="original_{0}"
                    UpdateCommand="UPDATE ServicesCodeItems SET Service_Code = @Service_Code, Description = @Description, Price = @Price, Service_Type = @Service_Type, Status = @Status, Remark = @Remark WHERE (id = @id)"
                    DeleteCommand="DELETE FROM ServicesCodeItems WHERE (id = @id)">

                    <DeleteParameters>
                        <asp:Parameter Name="id" />
                    </DeleteParameters>

                    <InsertParameters>
                        <asp:Parameter Name="Service_Code" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="Price" Type="String" />
                        <asp:Parameter Name="Service_Type" Type="String" />
                        <asp:Parameter Name="Status" Type="String" />
                        <asp:Parameter Name="Remark" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                       <%-- <asp:QueryStringParameter Name="RefNo" QueryStringField="refno" />--%>
                        <asp:SessionParameter Name="RefNo" SessionField="ReferenceNumber" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Service_Code" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="Price" Type="String" />
                        <asp:Parameter Name="Service_Type" Type="String" />
                        <asp:Parameter Name="Status" Type="String" />
                        <asp:Parameter Name="Remark" Type="String" />
                        <asp:Parameter Name="id" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </td>
        </tr>

    </table>
</asp:Content>

