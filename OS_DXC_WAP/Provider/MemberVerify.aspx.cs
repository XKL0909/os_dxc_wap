using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Threading;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Xml;
using System.Security.Cryptography;
using System.Web.SessionState;
using System.Threading;
using System.Web.Security;
using System.Net;
using System.Text;
using System.IO;
using System.Globalization;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;

namespace AsynchronousProcessing
{
	/// <summary>
	/// Summary description for ProcessingMessage.
	/// </summary>
	public partial class ProcessingMessage : System.Web.UI.Page
	{
		private const string PROCESS_NAME = "Process";
        private static byte[] _Salt = new byte[] { (byte)0xA9, (byte)0x9B, (byte)0xC8, (byte)0x32, (byte)0x56, (byte)0x35, (byte)0xE3, (byte)0x03 };

        string FullData = "data1";
        string DataValue;

		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void SubmitButton_Click(object sender, System.EventArgs e)
		{
            FullData += "--datafromsubmit";

          //  string Data1 = "MembershipNo=4436187;TransactionNo=5553333";// +txtTrans.Text;

            string Data1 = "MembershipNo=" + txtMembershipNo.Text + ";TransactionNo=5553" + txtTrans.Text;


            String EncryptedDataString = Encrypt(Data1, "test1");
            Data1 = HttpUtility.UrlEncode(EncryptedDataString);  // do we need to url-encode



			//Create and initialize new thread with the address of the StartLongProcess function
			Thread thread = new Thread(new ThreadStart(StartLongProcess));						
			


			//Start thread
			thread.Start();

			//Pass redirect page and session var name to the process wait (interum) page
			Response.Redirect("ProcessingMessage.aspx?Data=" + Data1);//?redirectPage=SubmitLongProcess.aspx&ProcessSessionName=" + PROCESS_NAME);			
		


        
        }

		private void StartLongProcess()
		{			
			//Initialize Session Variable
			Session[PROCESS_NAME] = false;
			
			//Replace the following line of code with your long process	
            ProcessRequests1("http://www9.bupame.com/Provider/swipecard/");
	//		Thread.Sleep(Convert.ToInt32(this.txtProcessLength.Text)* 1000);
            FullData += "--datafromthread";
			//Set session variable equal to true when process completes
			Session[PROCESS_NAME] = true;
            Session["data"] = DataValue;// FullData;// "testda";
		}

        public void ProcessRequests1(string prefixes)
        {
            if (!System.Net.HttpListener.IsSupported)
            {
                Console.WriteLine(
                    "Windows XP SP2, Server 2003, or higher is required to " +
                    "use the HttpListener class.");
                return;
            }
            // URI prefixes are required,
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            // Create a listener and add the prefixes.
            System.Net.HttpListener listener = new System.Net.HttpListener();
            //foreach (string s in prefixes)
            //{
            listener.Prefixes.Add(prefixes);
            // }

            try
            {
                // Start the listener to begin listening for requests.
                listener.Start();
                //txt1.Text = "Listening...";

                //Initialize Session Variable
                //Session["Result"] = false;

                // Set the number of requests this application will handle.
                int numRequestsToBeHandled = 10;

                //for (int i = 0; i < numRequestsToBeHandled; i++)
                //{
                HttpListenerResponse response = null;
                try
                {
                    // Note: GetContext blocks while waiting for a request.
                    HttpListenerContext context = listener.GetContext();
                    // txt1.Text += context.Request.Url.Query + "------" + context.Request.Url.Query.Substring(6) + "=======<br/>" + DateTime.Now.ToString(DateTimeFormatInfo.CurrentInfo) + "*********";
                    string theData = context.Request.Url.Query;// +"------" + context.Request.Url.Query.Substring(6) + "=======<br/>" + DateTime.Now.ToString(DateTimeFormatInfo.CurrentInfo) + "*********";
                    if (context.Request.Url.Query != "")
                    {

                        //Initialize Session Variable
                        // Session[PROCESS_NAME] = true;

                        String RawUrl = HttpUtility.UrlDecode(theData);
                        String EncryptedQueryString = RawUrl.Substring(RawUrl.ToUpper().IndexOf("?DATA=") + 6);


                        theData = Decrypt(EncryptedQueryString, "test1");

                        theData = theData.Replace("TransactionNo=", "");
                        theData = theData.Replace("VerificationStatus=", "");
                        theData = theData.Replace("MembershipNo=", "");
                        theData = theData.Replace("VerificationNo=", "");
                        theData = theData.Replace("ErrorMessage=", "");

                        DataValue = theData;
                        //Response.Redirect("BioR1.aspx?Value=" + theData);

                        //  Session["Result"] = true; // "success";
                        //return;

                        // Server.Execute("http://www9.bupame.com/Provider/teethmap.jpg?Dat=" + context.Request.Url.Query);



                        return;

                    }


                    // Create the response.
                    // response = context.Response;
                    //  context.Request.Url.Query;

                    //string responseString =
                    //    "<HTML><BODY>The time is currently " +
                    //    DateTime.Now.ToString(
                    //    DateTimeFormatInfo.CurrentInfo) +
                    //    "</BODY></HTML>";
                    //byte[] buffer =
                    //    System.Text.Encoding.UTF8.GetBytes(responseString);
                    //response.ContentLength64 = buffer.Length;
                    //System.IO.Stream output = response.OutputStream;
                    //output.Write(buffer, 0, buffer.Length);
                }
                catch (HttpListenerException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    if (response != null)
                        response.Close();
                }

                // }
            }
            catch (HttpListenerException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // Stop listening for requests.
                listener.Close();
                //txt1.Text += "Done Listening.";
            }
        }



        private static Encoding _Encoder
        {
            get { return Encoding.UTF8; }
        }
        public static String Encrypt(string message, string password)
        {
            PKCSKeyGenerator MyKey = new PKCSKeyGenerator();
            MyKey.Generate(password, _Salt, 19, 1);
            ICryptoTransform Crypt = MyKey.Encryptor;

            byte[] result = Crypt.TransformFinalBlock(_Encoder.GetBytes(message), 0, _Encoder.GetByteCount(message));
            return Convert.ToBase64String(result, Base64FormattingOptions.None);
        }
        public static String Decrypt(string message, string password)
        {
            PKCSKeyGenerator MyKey = new PKCSKeyGenerator();
            MyKey.Generate(password, _Salt, 19, 1);
            ICryptoTransform Decryptor = MyKey.Decryptor;

            byte[] ConvertedMessageBytes = Convert.FromBase64String(message);
            byte[] result = Decryptor.TransformFinalBlock(ConvertedMessageBytes, 0, ConvertedMessageBytes.Length);
            String DecodedString = _Encoder.GetString(result);
            return DecodedString;
        }
        private class PKCSKeyGenerator
        {
            byte[] key = new byte[8], iv = new byte[8];
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            public byte[] Key { get { return key; } }
            public byte[] IV { get { return IV; } }
            public ICryptoTransform Encryptor { get { return des.CreateEncryptor(key, iv); } }
            public ICryptoTransform Decryptor { get { return des.CreateDecryptor(key, iv); } }

            public PKCSKeyGenerator() { }
            public PKCSKeyGenerator(String keystring, byte[] salt, int md5iterations, int segments)
            {
                Generate(keystring, salt, md5iterations, segments);
            }

            public ICryptoTransform Generate(String keystring, byte[] salt, int md5iterations, int segments)
            {
                int HASHLENGTH = 16;    //MD5 bytes
                byte[] keymaterial = new byte[HASHLENGTH * segments];     //to store contatenated Mi hashed results

                // --- get secret password bytes ----
                byte[] psbytes;
                psbytes = Encoding.UTF8.GetBytes(keystring);

                // --- contatenate salt and pswd bytes into fixed data array ---
                byte[] data00 = new byte[psbytes.Length + salt.Length];
                Array.Copy(psbytes, data00, psbytes.Length);        //copy the pswd bytes
                Array.Copy(salt, 0, data00, psbytes.Length, salt.Length);    //concatenate the salt bytes

                // ---- do multi-hashing and contatenate results  D1, D2 ...  into keymaterial bytes ----
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] result = null;
                byte[] hashtarget = new byte[HASHLENGTH + data00.Length];   //fixed length initial hashtarget

                for (int j = 0; j < segments; j++)
                {
                    // ----  Now hash consecutively for md5iterations times ------
                    if (j == 0) result = data00;       //initialize
                    else
                    {
                        Array.Copy(result, hashtarget, result.Length);
                        Array.Copy(data00, 0, hashtarget, result.Length, data00.Length);
                        result = hashtarget;
                    }

                    for (int i = 0; i < md5iterations; i++)
                        result = md5.ComputeHash(result);

                    Array.Copy(result, 0, keymaterial, j * HASHLENGTH, result.Length);  //contatenate to keymaterial
                }

                Array.Copy(keymaterial, 0, key, 0, 8);
                Array.Copy(keymaterial, 8, iv, 0, 8);

                return Encryptor;
            }
        }



	}




}
