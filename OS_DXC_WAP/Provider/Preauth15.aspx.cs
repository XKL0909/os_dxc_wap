using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using OS_DXC_WAP.CaesarWS;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

public partial class Preauth15 : System.Web.UI.Page
{
    public string _InitialRequestID = string.Empty;

    DataTable DT;
    DataTable DTOptions;
    DataTable dtOpt;
    DataSet DS;
    int selectIndex;
    double SumCost = 0;
    decimal TotalCost = 0;
    int TotalQuantity = 0;
    double co = 0;
    double SumQuantity = 0;
    String Rcheck = "{";

    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strProviderFax;
    string strProviderID;
    string strProviderName;
    string strReqMed;
    string preAuthOptStatus = string.Empty;
    public string strUploadSession;

    private long ln_txnid;
    private int[] ItemNo;
    private String[] ServiceType;
    private String[] BenefitHead;
    private String[] ServiceCode;
    private String[] ServiceDescription;
    private long[] Quantity;
    private double[] EstimatedCost;
    private String[] SupplyPeriod;
    private DateTime[] SupplyDateFrom;
    private DateTime[] SupplyDateTo;
    private String[] ExemptCat; //2014-11-14 REF-1615 CR215 Nathan
    private DateTime supfrm;
    private DateTime supto;

    private string _uploadCategory = UploadCategory.PREAUTH_UPLOAD;
    String csName;
    Type csType;
    ClientScriptManager cs;

    private string PID = null;

    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }


    protected void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    PID = Convert.ToString(Request.QueryString["val"]);
                    PID = Cryption.Decrypt(PID);
                }
                catch (Exception)
                {

                    Message1.Text = "Invalid URL!";
                    return;
                }

            }
            else
                PID = null;

            btnOtherFrame.ToolTip = "Click if Frame is not available in the pricelist.\n1.Click �Others (Frame)� button.\n2.Input �Quantity�.\n3.Input �Estimated Cost�.\n4.Click �Add� button.\n";

            if (!IsPostBack)
            {
                Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                Response.Expires = -1500;
                Response.CacheControl = "no-cache";
                IsFollowUpRequest = 1;
            }
            else
            {
                IsFollowUpRequest = 0;
            }
            StringBuilder sb = new StringBuilder();
            Session["peauthupload"] = "Yes";

            // Define the name and type of the client script on the page.
            csName = "getList";
            csType = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            cs = Page.ClientScript;
            StringBuilder csText = new StringBuilder();
            // Check to see if the client script is already registered.
            if (!cs.IsClientScriptBlockRegistered(csType, csName))
            {

                csText.Append("<script type=\"text/javascript\"> function AddServiceDesc() {");
                csText.Append("var select1 = document.getElementById('ddlServiceDesc');");
            }

            if (!IsPostBack)
            {
                strUploadSession = TransactionManager.TransactionID().ToString();

                Session["UploadSession"] = strUploadSession;                
                uploadSession.Text = strUploadSession;

                if(DateTime.Now < new DateTime(2018, 7, 1))
                ddlDepartment.Items.RemoveAt(11);
            }
            else
            {
                strUploadSession = uploadSession.Text;
            }


            SqlConnection connection = null;
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);


            try
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                string sql = "select * from DrogCode where section = 2";
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                DataSet data = new DataSet();

                SqlDataReader dr;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    csText.Append("select1.options[select1.options.length] = new Option('" + Convert.ToString(dr["text"]).Trim() + "','" + Convert.ToString(dr["value"]).Trim() + "__" + Convert.ToString(dr["cost"]).Trim() + "');");
                }

            }
            catch (Exception ex)
            {
                Message1.Text += "\n" + ex.Message;
            }
            finally
            {
                connection.Close();
            }


            csText.Append("}</script>");
            cs.RegisterClientScriptBlock(csType, csName, csText.ToString());


            SessionManager.CheckSessionTimeout();

            Session["FixAJAXSysBug"] = true;
            strProviderID = "20087";// Session["ProviderID"].ToString(); 
            strProviderName = Session["ProviderName"].ToString();
            strProviderFax = Session["ProviderFax"].ToString();
            strProviderID = Session["ProviderID"].ToString();

            txtServiceDesc.Style["display"] = "block";

          

            if (Page.IsPostBack)
            {
                if (Session["_InitialRequestID"] != null)
                {
                    _InitialRequestID = Session["_InitialRequestID"].ToString();
                }

                if (ddlDepartment.SelectedValue != "OGP" && BLCommon.PreAuthAllowed && Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                {
                    LoadControl(ddlCategory.SelectedValue.ToString());
                    dvMultilevelddl.Visible = true;
                }
                else
                {
                    dvMultilevelddl.Visible = false;
                    txtServiceDesc.Visible = true;
                    txtServiceDesc.Style["display"] = "block";
                }
                if (ddlDepartment.SelectedValue == "OER" || ddlDepartment.SelectedValue == "DEN")
                {
                    hidProvider.Value = strProviderID;
                    if (BLCommon.HRMEnabled)
                    {
                       // btnSelectService.Text = ddlDepartment.SelectedText +" Service Details";
                        ((System.Web.UI.HtmlControls.HtmlInputControl)(btnSelectService)).Value = ddlDepartment.Items[ddlDepartment.SelectedIndex].Text + " Service Details";
                        btnSelectService.Visible = true;
                        PnlOpticalOld.Visible = false;
                        PnlOpticalHRM.Visible = true;

                        txtServiceDesc.Visible = false;
                        txtServiceDesc.Style["display"] = "none";
                        hidHRM.Value = "1";
                        tblBupaCodeList.Style["display"] = "block";
                        dvBupaCodeFilter.Style["display"] = "block";
                    }
                    else
                    {
                        btnSelectService.Visible = false;
                        PnlOpticalOld.Visible = true;
                        PnlOpticalHRM.Visible = false;
                        hidHRM.Value = "0";
                        tblBupaCodeList.Style["display"] = "none";
                        dvBupaCodeFilter.Style["display"] = "none";
                    }
                   
                }
                else
                {

                    tblBupaCodeList.Style["display"] = "none";
                    dvBupaCodeFilter.Style["display"] = "none";
                }
            }
            else
            {


                if (ddlDepartment.SelectedValue != "OGP" && BLCommon.PreAuthAllowed && Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                {
                    dvMultilevelddl.Visible = true;
                    string sql = "select categoryid, categoryparentid, categorycode, categorycode + ' - ' + category  as category,serviceCode from PreAuthCategory  where categoryparentid is not null  order by categorycode, category";
                    DataTable dtCategory = GetData(sql);
                    ddlCategory.DataSource = dtCategory;
                    ddlCategory.DataTextField = "category";
                    ddlCategory.DataValueField = "category";
                    ddlCategory.DataBind();
                    Session["dtServiceCategory"] = dtCategory;

                    LoadControlScript(dtCategory);

                    radComboCategory.DataSource = dtCategory;
                    radComboCategory.DataBind();
                    LoadCategoryDropdownMenu();
                }
                else
                {
                    dvMultilevelddl.Visible = false;
                    txtServiceDesc.Visible = true;
                    txtServiceDesc.Style["display"] = "block";
                }

                txtServiceDesc.Attributes.Add("OnKeyUp", "return txtServiceDescChanged()");

                // Get a unique request ID so that the attachment page recognises this request to be unique from others
                Random random = new Random(DateTime.Now.Millisecond);
                _InitialRequestID = random.Next().ToString();

                Session["_InitialRequestID"] = _InitialRequestID;

                //strProviderFax = "6651284";// ProviderFax = ""; // Session["ProviderFax"].ToString();
                txtDateOfVisit.Text = DateTime.Today.ToShortDateString();

                DS = new DataSet();
                //DS.ReadXmlSchema(Server.MapPath("DepdFile.xml"));
                DS.ReadXmlSchema(Server.MapPath("PreAuthRequest.xml"));
                Session["Dataset"] = DS;
                DT = DS.Tables[0];
                DTOptions = DS.Tables[1];
                for (int i = 0; i <= 0; i++)
                {
                    // LoadFirstRows(i);
                }
                Session["dt"] = DT;
                Session["dtOptions"] = DTOptions;

                GridView1.DataSource = DS;
                GridView1.DataBind();
                selectIndex = -1;
                Session["Index"] = selectIndex;


                PnlDentalCheckList.Visible = false;
                PnlDiagnosisCommon.Visible = false;
                PnlStandard.Visible = false;
                PnlStandardCheckList.Visible = false;
                lbl_DiagnosisDetails.Visible = false;
                lbl_PleaseTick.Visible = false;

               // if (Convert.ToBoolean(ConfigurationManager.AppSettings["HRMEnabled"]))
                hidProvider.Value = strProviderID;
                 if (BLCommon.HRMEnabled)
                {
                    if (ddlDepartment.SelectedValue == "OER" || ddlDepartment.SelectedValue == "DEN")
                    btnSelectService.Visible = true;
                    PnlOpticalOld.Visible = false;
                    PnlOpticalHRM.Visible = true;

                    txtServiceDesc.Visible = false;
                    txtServiceDesc.Style["display"] = "none";
                    hidHRM.Value = "1";
                    
                }
                else
                {
                    if (ddlDepartment.SelectedValue == "OER" || ddlDepartment.SelectedValue == "DEN")
                    btnSelectService.Visible = false;
                    PnlOpticalOld.Visible = true;
                    PnlOpticalHRM.Visible = false;
                    hidHRM.Value = "0";
                }

                PnlOptical.Visible = false;

                PnlPharmacy.Visible = false;
                PnlDentalStandardCheckList.Visible = false;

                ddlServiceDesc.Visible = false;
                PanelOptical.Visible = false;
                PanelDental.Visible = false;
                PanelPharmacy.Visible = false;

                ////if (Request.QueryString["PID"] != null)
                if (!string.IsNullOrEmpty(PID))
                {
                    Session["reload"] = "yes";
                    RequiredFieldValidator9.Enabled = false;
                    txtCIDCode.Visible = false;
                    btnICD.Visible = false;
                    ////ProcessFollowUp(Request.QueryString["PID"]);
                    ProcessFollowUp(PID);
                    UPnlDiagnosis.Visible = true;
                    GridView1.Columns[9].Visible = false;

                    if (ddlTreatmentType.SelectedValue == "I")
                    {
                        txtQuantity.Text = "0";
                        txtQuantity.Style["display"] = "none";
                        lblQuantity.Visible = false;

                        txtLengthOfStay.Visible = true;
                        lblLengthOfStay.Visible = true;
                        RFVldtrLengthofStay.Enabled = true;
                        CompareVldtrLengthofStay.Enabled = true;
                    }

                }
            }
            DS = (DataSet)Session["Dataset"];
            GridView1.DataSource = DS;
            lblval.Text = val.Value;

            //SetupUploader();
            int si = 0;
            if (Session["Index"] != null)
                int.TryParse(Session["Index"].ToString(), out si);
            if (si > 0)
                SetServiceDescriptionDropdownList(si);


            //try
            //{
            //    if (!Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
            //     {
            //        dvMultilevelddl.Visible = false;
            //        txtServiceDesc.Visible = true;
            //        txtServiceDesc.Style["display"] = "block";

            //    }

            //}
            //catch (Exception ex)
            //{
            //    // ExceptionManager.LogException(ex);
            //}

            if (BLCommon.HRMEnabled)
            {
               
                
                hidHRM.Value = "1";
                if (ddlDepartment.SelectedValue == "DEN" || ddlDepartment.SelectedValue == "OER")
                {
                    txtServiceDesc.Visible = false;
                    txtServiceDesc.Style["display"] = "none";
                    dvMultilevelddl.Visible = false;
                    ddlServiceDesc.Enabled = false;
                    txtCost.Enabled = false;
                    tblBupaCodeList.Style["display"] = "block";
                    dvBupaCodeFilter.Style["display"] = "block";
                }else
                {

                    tblBupaCodeList.Style["display"] = "none";
                    dvBupaCodeFilter.Style["display"] = "none";
                }

            }
            if (ddlDepartment.SelectedValue == "DEN" || ddlDepartment.SelectedValue == "OER")
            {
                tblBupaCodeList.Style["display"] = "block";
                btnSelectService.Style["display"] = "block";
               // dvBupaCodeFilter.Style["display"] = "block";
            }
            else
            {
                tblBupaCodeList.Style["display"] = "none";
                btnSelectService.Style["display"] = "none";
              //  dvBupaCodeFilter.Style["display"] = "none";
                
            }

        }
        catch (Exception ex)
        {
            // ExceptionManager.LogException(ex);
        }


    }
    public void LoadCategoryDropdownMenu()
    {


        try
        {
            string sInpatient = " and Category not like '%Admission%' ";
            string sql = "select categoryid, categoryparentid, categorycode, categorycode + ' - ' + category  as category from PreAuthCategory  where categoryparentid is not null ";
            string sqlParent = "select categoryid, categoryparentid, categorycode, category   from PreAuthCategory  where categoryparentid is null ";
            string sOrderby = "  order by categorycode, category ";

            if (ddlTreatmentType.SelectedValue == "O")
            {
                sql += sInpatient;
                sqlParent += sInpatient;
            }
            else
            {
                //sql += " and Category like '%Admission%' ";
                //sqlParent += " and Category like '%Admission%' ";

            }

            sql += sOrderby;
            sqlParent += sOrderby;


            DataTable dtCategory = GetData(sql);
            StringBuilder sbMenu = new StringBuilder("");
            DataView vwCategory = new DataView(dtCategory);
            //DataTable dtParentCategory = vwCategory.ToTable(true, "categoryparentid", "categorycode");
            DataTable dtChildCategory = vwCategory.ToTable(true, "categoryid", "categorycode", "Category");
            sbMenu.Append("<ul id=\"nav\">");

            DataTable dtParentCategory = GetData(sqlParent);



            sbMenu.Append("<li style=\"background-color: #01AEF0!important;-webkit-border-radius: 3px;-moz-border-radius: 10px;-border-radius: 10px;border-radius: 3px;top: -6.5px;background: buttonface;color:white; \"> &nbsp;&nbsp;&nbsp;&nbsp;Select from Automation List<ul>");
            for (int i = 0; i < dtParentCategory.Rows.Count; i++)
            {
                try
                {
                    //dtChildCategory = dtCategory.Select("categorycode = '" + dtParentCategory.Rows[i]["CategoryCode"].ToString() + "'").CopyToDataTable();
                    dtChildCategory = dtCategory.Select("categoryparentid = '" + dtParentCategory.Rows[i]["Categoryid"].ToString() + "'").CopyToDataTable();
                    sbMenu.Append("<li><a href=\"javascript:\">" + dtParentCategory.Rows[i]["Category"].ToString() + "</a>");

                    if (dtChildCategory.Rows.Count > 0)
                    {
                        sbMenu.Append("<ul>");
                    }
                    for (int j = 0; j < dtChildCategory.Rows.Count; j++)
                    {

                        //if (dtParentCategory.Rows[i]["CategoryCode"].ToString() == dtChildCategory.Rows[j]["CategoryCode"].ToString())
                        if (dtParentCategory.Rows[i]["Categoryid"].ToString() == dtChildCategory.Rows[j]["categoryparentid"].ToString())
                        {
                            sbMenu.Append("<li><a href=\"javascript:OnMenuSelection('" + dtChildCategory.Rows[j]["Category"].ToString() + "');\">" + dtChildCategory.Rows[j]["Category"].ToString() + "</a></li>");
                        }

                    }
                    if (dtChildCategory.Rows.Count > 0)
                    {
                        sbMenu.Append("</ul>");
                    }
                    sbMenu.Append("</li>");
                }
                catch (Exception ex) { }

            }
            sbMenu.Append("</li></ul>");
            sbMenu.Append("</ul>");
            dvMultilevelddl.InnerHtml = sbMenu.ToString();

        }
        catch (Exception ex) { }


    }
    public void LoadControl(string sServiceCode)
    {

        // Pre-Auth UserControl Populating
        StringBuilder sbHTML;
        StringBuilder csJScript;
        csType = this.GetType();
        cs = Page.ClientScript;

        //BLCommon.GenerateControl("CLS - Colonoscopy",out sbHTML, out csJScript);
        //BLCommon.GenerateControl("PNS - CT Scan PNS", out  sbHTML, out  csJScript);
        //BLCommon.GenerateControl("EDO - Endoscopy", out  sbHTML, out  csJScript);
        //BLCommon.GenerateControl("MRS - MRI Shoulder", out  sbHTML, out  csJScript);
        //BLCommon.GenerateControl("MRK - MRI Knee", out  sbHTML, out  csJScript);
        //BLCommon.GenerateControl("GEC - 1 Day Admission for GE Control", out  sbHTML, out  csJScript);
        //BLCommon.GenerateControl("MSP - MRI Scan Spine", out  sbHTML, out  csJScript);

        BLCommon.GenerateControl(sServiceCode, out sbHTML, out csJScript);

        csName = "PreAuthOptionFillData";
        if (!cs.IsClientScriptBlockRegistered(csType, csName))
        {
            cs.RegisterClientScriptBlock(csType, csName, csJScript.ToString());
            //ScriptManager.RegisterStartupScript(this, typeof(string), csName, csJScript.ToString(), true);
            // ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), csName, csJScript.ToString(), true);
        }



        //dvServiceCategoryOptions.InnerHtml = sbHTML.ToString();
        dvCatOptionsForm.InnerHtml = sbHTML.ToString();
        //dvScript.InnerHtml = csJScript.ToString();
        EditCatOptions();
        //System.IO.File.WriteAllText(Server.MapPath("js/preauthfilldata.js"), csJScript.ToString());

        try
        {
            updatePanel1.Update();
        }
        catch (Exception ex) { }

        // End Pre-Auth UserControl Populating

    }
    public void EditCatOptions()
    {
        string sTable = string.Empty;
        sTable = dvCatOptionsForm.InnerHtml.ToString();
        try
        {

            DT = (DataTable)Session["DT"];
            DTOptions = (DataTable)Session["dtOptions"];
            if (DT.Rows.Count >= 1)
            {
                if (DTOptions.Rows.Count >= 1)
                {
                    for (int i = 0; i < DTOptions.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(txtServiceDesc.Text) && txtServiceDesc.Text.Substring(0, 3) == DTOptions.Rows[i]["CategoryCode"].ToString())
                        {
                            sTable = dvCatOptionsForm.InnerHtml.ToString();
                            if (DTOptions.Rows[i]["OptionValue"].ToString().Trim() == "Y")
                            {

                                if (DTOptions.Rows[i]["OptionCode"].ToString().Contains("@"))
                                {
                                    string[] words = DTOptions.Rows[i]["OptionCode"].ToString().Split('@');

                                    for (int cnt = 0; cnt < words.Length; cnt++)
                                    {
                                        sTable = sTable.Replace("value='" + words[cnt].ToString() + "'", "value='" + words[cnt].ToString() + "' checked");
                                    }

                                }
                                else
                                {
                                    sTable = sTable.Replace("value='" + DTOptions.Rows[i]["OptionCode"].ToString() + "'", "value='" + DTOptions.Rows[i]["OptionCode"].ToString() + "' checked");
                                }

                            }
                            else
                            {
                                sTable = sTable.Replace("Text='" + DTOptions.Rows[i]["OptionCode"].ToString() + "'", "Text='" + DTOptions.Rows[i]["OptionCode"].ToString() + "' value='" + DTOptions.Rows[i]["OptionValue"].ToString() + "' ");

                            }

                            dvCatOptionsForm.InnerHtml = sTable;
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            dvCatOptionsForm.InnerHtml = sTable;
        }

    }
    public void LoadControlScript(DataTable dtCat)
    {
        try
        {

            // Pre-Auth UserControl Populating
            StringBuilder sbHTML;
            StringBuilder csJScript;
            StringBuilder allFunctions = new StringBuilder();
            csType = this.GetType();
            cs = Page.ClientScript;
            int i = 0;
            DataView dv = new DataView(dtCat);
            DataTable distinctCategorycode = dv.ToTable(true, "categorycode");
            string[] sCategoryCode = new string[distinctCategorycode.Rows.Count];
            foreach (DataRow dr in distinctCategorycode.Rows)
            {

                BLCommon.GenerateControlScript(dr["categorycode"].ToString().Trim(), out sbHTML, out csJScript);
                allFunctions.Append(csJScript);
                sCategoryCode[i++] = dr["categorycode"].ToString().Trim();

            }

            allFunctions.Append("function filldata() {");
            //allFunctions.Append("alert($(\"#lblServiceDesc\").text());");
            //allFunctions.Append("var strCategory = $(\"#txtServiceDesc\").val();");
            allFunctions.Append("var strCategory = $(\"#lblServiceDesc\").text();");
            allFunctions.Append("if (strCategory.substring(0, 3) == \"" + sCategoryCode[0] + "\") { " + sCategoryCode[0] + "(); }");
            for (i = 1; i < distinctCategorycode.Rows.Count; i++)
            {
                if (sCategoryCode[i].ToString().Trim().ToUpper() != "OTH")
                    allFunctions.Append("else if (strCategory.substring(0, 3) == \"" + sCategoryCode[i] + "\") { return  " + sCategoryCode[i] + "(); }");
            }


            allFunctions.Append("else { /*alert(\"Error Loading...Try after sometime.\"); */}");
            allFunctions.Append("}");

            //BLCommon.GenerateControl(sServiceCode, out sbHTML, out csJScript);
            System.IO.File.WriteAllText(Server.MapPath("js/preauthfilldata.js"), allFunctions.ToString());


            csName = "PreAuthOptionFillData";
            if (!cs.IsClientScriptBlockRegistered(csType, csName))
            {

                cs.RegisterClientScriptBlock(csType, csName, allFunctions.ToString(), true);
                //ScriptManager.RegisterStartupScript(this, typeof(string), csName, csJScript.ToString(), true);
                // ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), csName, csJScript.ToString(), true);
            }

        }
        catch (Exception ex) { }

    }
    protected void RadComboBox2_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e)
    {

        try
        {

            //string sql = "select * from PreAuthCategory where category like '%" + e.Text + "%'";
            //string sql = "select categoryid, categoryparentid, categorycode, categorycode + ' - ' + category  as category from PreAuthCategory where category like '%" + e.Text + "%'";
            string sql = "select categoryid, categoryparentid, categorycode, categorycode + ' - ' + category  as category from PreAuthCategory  where upper(category) like '%" + e.Text.ToUpper().Trim() + "%' or upper(categorycode) like '%" + e.Text.ToUpper().Trim() + "%' and  categoryparentid is not null  order by categorycode, category";


            DataTable dtCategory = GetData(sql);
            radComboCategory.DataSource = dtCategory;
            radComboCategory.DataBind();


        }
        catch (Exception ex)
        {
            // ExceptionManager.LogException(ex);
        }

    }
    protected void RadComboBox1_TextChanged(object sender, EventArgs e)
    {

        // LoadControl(radComboCategory.Text);
        //  GenerateControl(radComboCategory.Text.Substring(0,4));
    }
    protected void RadComboBox2_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        //txtServiceDesc.Text = "You selected " + e.Text + " item";
        //  LoadControl(e.Text);
    }

    public DataTable GetData(string query)
    {
        DataTable table = new DataTable();

        SqlConnection connection = null;
        DataSet dsCategories = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);

            dsCategories = SqlHelper.ExecuteDataset(connection, CommandType.Text, query);

            table = dsCategories.Tables[0];

            connection.Close();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            connection.Close();
        }


        return table;
    }
    //Updated Button Status when GridView1_SelectedIndexChanged
    protected void GridView1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {

            DiagnosisActions();
        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }

    }

    private void DiagnosisActions()
    {
        selectIndex = (int)Session["Index"];
        if (selectIndex == GridView1.SelectedIndex)
        {
            GridView1.SelectedIndex = -1;
            reset();
            Session["Index"] = -1;
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = false;
            GridView1.Enabled = true;
        }
        else
        {
            try
            {


                if (ddlServiceDesc.SelectedValue == "O3")
                    ResetChechBox();


                if (ddlServiceDesc.SelectedValue == "O1" || ddlServiceDesc.SelectedValue == "O2" || ddlServiceDesc.SelectedValue == "O6")
                {
                    vldtrRFddlLenseType.Enabled = false;
                }

                selectIndex = GridView1.SelectedIndex;
                Session["Index"] = selectIndex;
                FindData(selectIndex);

                btnAdd.Visible = false;
                btnUpdate.Visible = true;
                btnCancel.Visible = true;
                GridView1.Enabled = false;
                txtTempCost.Text = txtCost.Text;
                txtTempQuantity.Text = txtQuantity.Text;
                if ((ddlDepartment.SelectedValue == "OGP" || ddlDepartment.SelectedValue == "VAC" || ddlDepartment.SelectedValue == "PHA" || txtMinor.Text == "true") && (ddlPharmacyDiag != null && ddlPharmacyDiag.SelectedValue != "???.?"))
                {

                    DT = (DataTable)Session["DT"];
                    // txtServiceDesc.Text =  (String)DT.Rows[selectIndex][0];

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script language='javascript'>");
                    // sb.Append(@"addOptions('" + ddlPharmacyDiag.SelectedValue + "','" + (String)DT.Rows[selectIndex][6] + "')");
                    // CR108 line modified 

                    if (DT.Rows[selectIndex][9].ToString().Length == 9)
                        sb.Append(@"addOptions('" + ddlPharmacyDiag.SelectedValue + "','" + (String)DT.Rows[selectIndex][9] + "__" + (String)DT.Rows[selectIndex][7] + "')");

                    if (DT.Rows[selectIndex][9].ToString().Length == 6)
                        sb.Append(@"addOptions('" + ddlPharmacyDiag.SelectedValue + "','" + (String)DT.Rows[selectIndex][9] + "')");



                    sb.Append(@"</script>");

                    ScriptManager.RegisterStartupScript(btnAdd, this.GetType(), "JSCR", sb.ToString(), false);
                    ddlPharmacyDiag.Enabled = false; // ddlServiceDesc.SelectedIndex = 0;
                }


                if (ddlDepartment.SelectedValue == "OGP" || txtMinor.Text == "True")
                {

                }

                //// New code aded by sakthi on 22-Dec-2015
                //// Set dvMultilevelddl visible false during the edit
                //// Start
                switch (ddlDepartment.SelectedValue)
                {


                    //case "OGP":
                    case "PHA":
                    case "OER":
                    case "DEN":
                    case "VAC":
                        dvMultilevelddl.Visible = false;
                        break;
                    default:
                        break;
                }
                if (val.Value == "N")
                {
                    dvMultilevelddl.Visible = false;
                    txtServiceDesc.Visible = true;
                    txtServiceDesc.Style["display"] = "block";

                }

                btnSubmit.CausesValidation = true;

                ////End

                // Pre-Auth
                ddlCategory.SelectedValue = txtServiceDesc.Text;
                LoadCategoryOptions();
            }
            catch (Exception)
            {

            }

            //LoadControl(txtServiceDesc.Text);
            //ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "diagAuto()", true); 
            // End Pre-Auth
        }

        ////if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["PID"])))
        if (!string.IsNullOrEmpty(PID))
        {
            dvMultilevelddl.Visible = false;
            txtServiceDesc.Visible = true;
            txtServiceDesc.Style["display"] = "block";
        }
    }

    protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor; this.style.textDecoration='underline';this.style.backgroundColor='#FFCC66';";
                e.Row.Attributes["onmouseout"] = "this.style.textDecoration='none';this.style.backgroundColor = this.originalstyle;";

                if (Convert.ToString(e.Row.Cells[0].Text).Contains("##OF##"))
                    e.Row.Cells[0].Text = string.Empty;
                int iRow = 8;
                TableCell tc = e.Row.Cells[iRow];
                int iRowQ = 7;
                TableCell tcQ = e.Row.Cells[iRowQ];
                if (double.TryParse(tc.Text, out co) == true)
                {
                    co = Convert.ToDouble(tc.Text);
                    SumCost += co;

                }
                if (double.TryParse(tcQ.Text, out co) == true)
                {
                    co = Convert.ToDouble(tcQ.Text);
                    SumQuantity += co;
                }
            }
        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            lblNoTreatmentDetError.Text = "";
            int categoryID = e.RowIndex;
            DeleteRow(categoryID);
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = false;
            GridView1.Enabled = true;

            //need to work  better on the logic
            if (ddlPharmacyDiag.SelectedIndex > 0)
            {
                if (txtMinor.Text == "true" || ddlDepartment.SelectedValue == "OGP" || ddlDepartment.SelectedValue == "VAC")
                    DT = (DataTable)Session["DT"];
                if (DT.Rows.Count == 0)
                {
                    ddlPharmacyDiag.Enabled = true;
                    ddlPharmacyDiag.SelectedIndex = 0;
                    btnRequestMedication.Enabled = true;
                    txtReqMed.Text = "false";       //fixing for 'preauth vaccination cr' [the value was "" ] 
                }


            }

            ////if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["PID"])))
            if (!string.IsNullOrEmpty(PID))
            {
                dvMultilevelddl.Visible = false;
                txtServiceDesc.Visible = true;
                txtServiceDesc.Style["display"] = "block";
            }
        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }
    }

    public void DeleteRow(int RowIndex)
    {
        try
        {
            DT = (DataTable)Session["dt"];

            TotalCost = Convert.ToDecimal(txtSumCost.Text) - Convert.ToDecimal(DT.Rows[RowIndex][5]);
            txtSumCost.Text = Convert.ToString(TotalCost);
            TotalQuantity = Convert.ToInt32(txtSumQuantity.Text) - Convert.ToInt32(DT.Rows[RowIndex][4].ToString());
            txtSumQuantity.Text = Convert.ToString(TotalQuantity);
            txtServiceDesc.Text = Convert.ToString(DT.Rows[RowIndex]["ServiceDesc"].ToString());
            deleteSelectedOptions();
            DT.Rows[RowIndex].Delete();
            GridView1.SelectedIndex = -1;

            reset();


        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            lblNoTreatmentDetError.Text = "";
            GridView1.SelectedIndex = -1;
            reset();
            Session["Index"] = -1;
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = false;
            GridView1.Enabled = true;
            ddlCategory.SelectedIndex = 0;
            lblServiceDesc.Text = "";

            dvMultilevelddl.Visible = false;
            txtServiceDesc.Visible = false;
            txtServiceDesc.Style["display"] = "block";

            ////if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["PID"])))
            if (!string.IsNullOrEmpty(PID))
            {
                dvMultilevelddl.Visible = false;
                txtServiceDesc.Visible = true;
                txtServiceDesc.Style["display"] = "block";
            }
            if (BLCommon.HRMEnabled)
            {

                if (ddlDepartment.SelectedValue == "DEN" || ddlDepartment.SelectedValue == "OER")
                {
                    txtServiceDesc.Style["display"] = "none";
                    txtServiceDesc.Visible = false;
                    dvMultilevelddl.Visible = false;
                    PnlOpticalHRM.Style["display"] = "none";
                    ddlEyeTestReadingLeft.SelectedIndex = 0;
                    ddlEyeTestReadingRight.SelectedIndex = 0;
                }
                else
                {
                    txtServiceDesc.Visible = true;
                    txtServiceDesc.Style["display"] = "block";
                }

            }
        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }
    }

    public void FindData(int SI)
    {
        DT = (DataTable)Session["DT"];
        DTOptions = (DataTable)Session["dtOptions"];
        txtServiceDesc.Text = (String)DT.Rows[SI][0];
        try
        {
            if (ddlDepartment.SelectedValue == "DEN")
            {
                if (DT.Rows[SI][6].ToString().StartsWith("D"))
                    ddlServiceDesc.SelectedValue = (String)DT.Rows[SI][6];
                else
                    ddlServiceDesc.SelectedValue = (String)DT.Rows[SI][9]; // +"__" + (String)DT.Rows[SI][5];

                txtTeethMap.Value = (String)DT.Rows[SI][7];

                if (txtTeethMap.Value != "")
                    PanelDental.Style["display"] = "block";
                else
                    PanelDental.Style["display"] = "none";

            }


            if (ddlDepartment.SelectedValue == "PHA" && ddlPharmacyDiag.SelectedValue != "???.?")
            {
                //CR108 the below script modifed to concate the price 
                //string tempSingleCost = Convert.ToString( Convert.ToInt16((String)DT.Rows[SI][5]) / Convert.ToInt16((String)DT.Rows[SI][4])) ; 
                ddlServiceDesc.SelectedValue = (String)DT.Rows[SI][9] + "__" + (String)DT.Rows[SI][7];

            }

            if (ddlDepartment.SelectedValue == "OER")
            {
                int index = 0;
                int o = 0;
                foreach (ListItem item in ddlServiceDesc.Items)
                {
                    if (item.Text == (String)DT.Rows[SI][0])
                    {
                        index = o;
                    }
                    o += 1;
                }
                ddlServiceDesc.SelectedIndex = index;

                ddlLensType.Items.Clear();
                if (ddlServiceDesc.SelectedValue == "O5")
                {
                    ddlLensType.Items.Add(new ListItem("Select", "0"));
                    ddlLensType.Items.Add(new ListItem("Disposable", "D"));
                    ddlLensType.Items.Add(new ListItem("Permanent", "P"));
                }
                if (ddlServiceDesc.SelectedValue == "O3")
                {
                    ddlLensType.Items.Add(new ListItem("Select", "0"));
                    //  ddlLensType.Items.Add(new ListItem("None", "N"));
                    ddlLensType.Items.Add(new ListItem("Glass", "G"));
                    ddlLensType.Items.Add(new ListItem("Plastic", "P"));

                    Pane1.Enabled = true;
                }

                if (ddlServiceDesc.SelectedValue == "O1" || ddlServiceDesc.SelectedValue == "O2" || ddlServiceDesc.SelectedValue == "O6")
                {
                    ddlLensType.Items.Clear();
                    ddlLensType.Items.Add(new ListItem("Select", ""));
                    vldtrRFddlLenseType.Enabled = false;
                }
                if (ddlServiceDesc.SelectedValue == "O5" || ddlServiceDesc.SelectedValue == "O3")
                    ddlLensType.SelectedValue = (String)DT.Rows[SI][8];
            }
            else
            {
                if (ddlPharmacyDiag.SelectedValue == "???.?")
                {
                    ddlServiceDesc.Visible = false;
                    txtServiceDesc.Visible = true;

                    txtServiceDesc.Style["display"] = "block";
                    txtServiceDesc.Text = (String)DT.Rows[SI][0];

                }

            }
        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
            // ExceptionManager.LogException(ex);
        }

        ddlPeriod.SelectedValue = (String)DT.Rows[SI][1];
        DateFrom.Text = (String)DT.Rows[SI][2];
        txtDateTo.Text = (String)DT.Rows[SI][3];
        txtQuantity.Text = (String)DT.Rows[SI][4];
        txtCost.Text = (String)DT.Rows[SI][5];
        hidtxtQuantityUpdate.Value = txtQuantity.Text;
        hidtxtCostUpdate.Value = txtCost.Text;


        if (ddlServiceDesc.SelectedValue == "O3")
            GetCodeForCheckBoxs((String)DT.Rows[SI][6]);

        if (DT.Rows[SI][7].ToString() != "")
            txtTeethMap.Value = (String)DT.Rows[SI][7];

        if (ddlDepartment.SelectedValue == "OER" && (ddlLensType.SelectedValue == "O3" || ddlLensType.SelectedValue == "O5"))
            ddlLensType.SelectedValue = (String)DT.Rows[SI][8];

        if (txtMinor.Text == "false") txtServiceCode.Text = "";
        Session["dt"] = DT;
        Session["dtOptions"] = DTOptions;

        try { 
             if (ddlDepartment.SelectedValue == "OER" || ddlDepartment.SelectedValue == "DEN" )
             {
                 ddlPeriod.SelectedValue = (String)DT.Rows[SI][1];
                 DateFrom.Text = (String)DT.Rows[SI][2];
                 txtDateTo.Text = (String)DT.Rows[SI][3];
                 txtQuantity.Text = (String)DT.Rows[SI][4];
                 txtCost.Text = (String)DT.Rows[SI][5];
                 hidtxtQuantityUpdate.Value = txtQuantity.Text;
                 hidtxtCostUpdate.Value = txtCost.Text;
                
                  int index = 0;
                  int o = 0;
                  foreach (ListItem item in ddlServiceDesc.Items)
                  {

                      if (item.Text.Contains(DT.Rows[SI][0].ToString()))
                      {
                          index = o;
                      }
                      o += 1;
                  }
                  ddlServiceDesc.SelectedIndex = index;

             }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            // ExceptionManager.LogException(ex);
        }
    }

    public void SetServiceDescriptionDropdownList(int SI)
    {
        txtServiceDesc.Text = (String)DT.Rows[SI][0];
        try
        {
            if (ddlDepartment.SelectedValue == "DEN")
            {
                if (DT.Rows[SI][6].ToString().StartsWith("D"))
                    ddlServiceDesc.SelectedValue = (String)DT.Rows[SI][6];
                else
                    ddlServiceDesc.SelectedValue = (String)DT.Rows[SI][9]; // +"__" + (String)DT.Rows[SI][5];
                txtTeethMap.Value = (String)DT.Rows[SI][7];
                if (txtTeethMap.Value != "")
                    PanelDental.Style["display"] = "block";
                else
                    PanelDental.Style["display"] = "none";
            }
            if (ddlDepartment.SelectedValue == "PHA" && ddlPharmacyDiag.SelectedValue != "???.?")
            {
                //CR108 the below script modifed to concate the price 
                //string tempSingleCost = Convert.ToString( Convert.ToInt16((String)DT.Rows[SI][5]) / Convert.ToInt16((String)DT.Rows[SI][4])) ; 
                ddlServiceDesc.SelectedValue = (String)DT.Rows[SI][9] + "__" + (String)DT.Rows[SI][7];
            }
            else if (ddlDepartment.SelectedValue == "OER")
            {
                //ddlServiceDesc.Items.IndexOf(ddlServiceDesc.Items.FindByText((String)DT.Rows[SI][0]));
                int index = 0;
                int o = 0;
                foreach (ListItem item in ddlServiceDesc.Items)
                {
                    if (item.Text == (String)DT.Rows[SI][0])
                    {
                        index = o;
                    }
                    o += 1;
                }
                ddlServiceDesc.SelectedIndex = index;

                ddlLensType.Items.Clear();
                if (ddlServiceDesc.SelectedValue == "O5")
                {
                    ddlLensType.Items.Add(new ListItem("Select", "0"));
                    ddlLensType.Items.Add(new ListItem("Disposable", "D"));
                    ddlLensType.Items.Add(new ListItem("Permanent", "P"));
                }
                else if (ddlServiceDesc.SelectedValue == "O3")
                {
                    ddlLensType.Items.Add(new ListItem("Select", "0"));
                    //  ddlLensType.Items.Add(new ListItem("None", "N"));
                    ddlLensType.Items.Add(new ListItem("Glass", "G"));
                    ddlLensType.Items.Add(new ListItem("Plastic", "P"));

                    Pane1.Enabled = true;
                }

                else if (ddlServiceDesc.SelectedValue == "O1" || ddlServiceDesc.SelectedValue == "O2" || ddlServiceDesc.SelectedValue == "O6")
                {
                    ddlLensType.Items.Clear();
                    ddlLensType.Items.Add(new ListItem("Select", ""));
                    vldtrRFddlLenseType.Enabled = false;
                }
                else if (ddlServiceDesc.SelectedValue == "O5" || ddlServiceDesc.SelectedValue == "O3")
                    ddlLensType.SelectedValue = (String)DT.Rows[SI][8];
            }
            else
            {
                if (ddlPharmacyDiag.SelectedValue == "???.?")
                {
                    ddlServiceDesc.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        if (ddlServiceDesc.SelectedValue == "O3")
            GetCodeForCheckBoxs((String)DT.Rows[SI][6]);

    }
    public bool validateMandatory()
    {
        bool bStatus = false;

        try
        {

            if (lblServiceDesc.Text.Contains("OTH") || lblServiceDesc.Text.Contains("DLS") || BLCommon.PreAuthAllowed || (!Convert.ToBoolean(Session["PreAuthAllowed"].ToString())))
            {
                return true;
            }

            switch (ddlDepartment.SelectedValue)
            {
                case "DEN":
                case "PHA":
                case "OER":
                case "OGP":
                case "VAC":
                    dvMultilevelddl.Visible = false;
                    return true;
                    break;
                default:
                    break;
            }
            string sServiceCode = txtServiceDesc.Text.Substring(0, 3);
            string QuestionId = string.Empty;
            string OptionCode = string.Empty;
            int selectedOptionCount = 0;
            string sql = " select distinct QuestionID,Question,IsMandatory from dbo.PreAuthDiagnosisQuestions Dq,preauthoption Op  where OptionCode like '" + sServiceCode + "%'  and Dq.QuestionID=Op.QID order by QuestionId ";
            DataTable dtQuestions = GetData(sql);

            if (dtQuestions.Rows.Count == 0 || val.Value == "N")
            {
                return true;
            }
            if (txtServiceDesc.Text.Length > 3 && txtServiceDesc.Text.Contains("-"))
            {
                string[] QuestionAnswed = txtHiddenOptions.Text.Split('&');

                foreach (string val1 in QuestionAnswed)
                {
                    if (!String.IsNullOrEmpty(val1))
                    {
                        QuestionId = val1.Split('=')[0];
                        OptionCode = val1.Split('=')[1];

                        if (!String.IsNullOrEmpty(OptionCode.Trim()))
                        {
                            selectedOptionCount++;
                        }


                    }
                }

                int qCount = dtQuestions.Select("IsMandatory = True").Length;
                string sPreAuthStatus = Session["preAuthOptStatus"].ToString();
                if (sPreAuthStatus == "F" || sPreAuthStatus == "N")
                { qCount = 1; }

                if (qCount <= selectedOptionCount)
                { bStatus = true; }
                else
                {

                    Message1.Text = "Please select mandatory fields from '" + txtServiceDesc.Text + "' validation screen ";
                    Message1.Visible = true;


                }
            }

        }
        catch (Exception ex) { }

        return bStatus;

    }
    protected void Button1_Click(object sender, System.EventArgs e)
    {
        try
        {
            lblNoTreatmentDetError.Text = "";
            if (validateMandatory())
            {
                DT = (DataTable)Session["DT"];
                if (DT.Rows.Count == 1 && txtMinor.Text == "true" && ddlPharmacyDiag.SelectedValue != "???.?")
                {
                    //Response.Write("<script type='text//javascript'> alert('Only one service can be selected');</script>");
                    lblNoTreatmentDetError.Text = "You cannot request more than 1 day admission.";// ddlPharmacyDiag.Enabled = true;
                    lblNoTreatmentDetError.Visible = true;
                    reset();
                }

                else
                {

                    TotalCost = Convert.ToDecimal(txtSumCost.Text) + Convert.ToDecimal(txtCost.Text);
                    txtSumCost.Text = Convert.ToString(TotalCost);
                    if (txtQuantity.Text != "")
                    {
                        int tempQuantit = 0;
                        tempQuantit = txtQuantity.Text.Trim() == "0" ? 1 : Convert.ToInt32(txtQuantity.Text);
                        TotalQuantity = Convert.ToInt32(txtSumQuantity.Text) + tempQuantit;
                        txtSumQuantity.Text = Convert.ToString(TotalQuantity);
                    }
                    if (ddlServiceDesc.SelectedValue == "O3" || ddlServiceDesc.SelectedValue == "O5")
                        txtNeedEyeTable.Text = "true1";


                    addval(ddlDepartment.SelectedValue);

                    GridView1.DataSource = DT;
                    GridView1.DataBind();
                    GridView1.SelectedIndex = -1;

                    reset();

                    btnSubmit.Visible = true;
                    txtSumCost.Visible = true;
                    txtSumQuantity.Visible = true;

                }
                btnAdd.Style["display"] = "block";
                txtServiceDesc.Visible = false;
                txtServiceDesc.Style["display"] = "none";

                //if (!Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))

                //// fixing issue adding multiple record  for "VAC" by sakthi on 22-Dec-2015
                //// could not able to add a new record due to  txtServiceDesc visibility
                //// New code started

                if (ddlDepartment.SelectedValue == "OGP" || ddlDepartment.SelectedValue == "VAC" || BLCommon.PreAuthAllowed || !Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                {
                    txtServiceDesc.Visible = true;
                    txtServiceDesc.Style["display"] = "block";

                }
                if (ddlDepartment.SelectedValue == "DEN" || ddlDepartment.SelectedValue == "OER")
                {
                    if (BLCommon.HRMEnabled)
                    {
                        txtServiceDesc.Visible = false;
                        txtServiceDesc.Style["display"] = "none";
                    }
                }
                if (ddlDepartment.SelectedValue == "NEO")
                {
                    vldtrRFtxtServDesc.Enabled = false;
                    txtServiceDesc.Style["display"] = "none";
                    txtServiceDesc.Visible = false;
                }

                //// New code end
                //// old code started
                ////if (ddlDepartment.SelectedValue == "OGP" || !Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                ////{
                ////    txtServiceDesc.Visible = true;
                ////    txtServiceDesc.Style["display"] = "block";

                ////}
                //// old code end
                if (val.Value == "N")
                {
                    dvMultilevelddl.Visible = false;
                    txtServiceDesc.Visible = true;
                    txtServiceDesc.Style["display"] = "block";

                }

                ////if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["PID"])))
                if (!string.IsNullOrEmpty(PID))
                {
                    dvMultilevelddl.Visible = false;
                    txtServiceDesc.Visible = true;
                    txtServiceDesc.Style["display"] = "block";
                }

               
            }
        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }
    }

    public void updateVal(int index, string service)
    {
        try
        {

            DT = (DataTable)Session["dt"];
            DTOptions = (DataTable)Session["dtOptions"];
            //Husam 11/5/2009 Add Parameter to updateVal() depend on service

            ////Added by sakthi on 28-Dec-2015 regarding during the update service discription is not updating, it always keeping old description
            ////New  code Strart            
            ///txtServiceDesc.Text = !string.IsNullOrEmpty(lblServiceDesc.Text) ? Convert.ToString(lblServiceDesc.Text).Trim().ToLower().Equals(Convert.ToString(txtServiceDesc.Text).Trim().ToLower()) ? txtServiceDesc.Text : lblServiceDesc.Text : txtServiceDesc.Text;
            ////New code End


            DT.Rows[index][0] = lblServiceDesc.Text;
            if (service == "DEN" || service == "OER" || (service == "PHA" && ddlPharmacyDiag.SelectedValue != "???.?"))
            {
                //Husam 12/5/2009 Update Data (service desc /service code) into data table
                DT.Rows[index][0] = ddlServiceDesc.SelectedItem;

                //CR108 modification in order to remove the __ and the price list from the value
                if (ddlServiceDesc.SelectedValue.Contains("__") == true)
                {
                    DT.Rows[index][9] = ddlServiceDesc.SelectedValue; //.Remove(9);
                    DT.Rows[index][5] = ddlServiceDesc.SelectedValue.Remove(0, 11);
                    DT.Rows[index][7] = ddlServiceDesc.SelectedValue.Remove(0, 11); //ddlServiceDesc.SelectedValue.Remove(9);
                }
                else
                    DT.Rows[index][9] = ddlServiceDesc.SelectedValue;


                if (service == "PHA" && ddlServiceDesc.SelectedValue.Contains("__") == true)
                {
                    DT.Rows[index][9] = ddlServiceDesc.SelectedValue.Remove(9);
                    DT.Rows[index][5] = ddlServiceDesc.SelectedValue.Remove(0, 11);
                    DT.Rows[index][7] = ddlServiceDesc.SelectedValue.Remove(0, 11); //ddlServiceDesc.SelectedValue.Remove(9);
                }

                //if (service == "Oer")
                //{
                //    DT.Rows[index][9] = txtServiceCode.Text;
                //}
            }
            else
            {
                string other = txtServiceDesc.Text.Trim();
                string otherLable = lblServiceDesc.Text.Trim();
                if (!string.IsNullOrEmpty(otherLable))
                {
                    if (!string.IsNullOrEmpty(otherLable))
                    {
                        if (otherLable.ToLower().Trim().Equals("oth - others"))
                        {
                            DT.Rows[index][0] = other;
                            DT.Rows[index][9] = string.Empty;
                        }
                        else
                        {
                            DT.Rows[index][0] = otherLable;
                            DT.Rows[index][9] = otherLable.Length > 3 ? otherLable.Substring(0, 3) : otherLable;
                        }
                    }
                }
                else
                {
                    DT.Rows[index][0] = other;
                    DT.Rows[index][9] = Convert.ToString(DT.Rows[index][9]);

                }
                //else
                //{

                //}
                //DT.Rows[index][0] = txtServiceDesc.Text;
            }


            if ((service == "OGP" || service == "VAC") && ddlPharmacyDiag.SelectedValue != "???.?")
            {


                if (txtServiceDesc.Text.Contains("|") == true)
                {
                    string[] words = txtServiceDesc.Text.Split('|');

                    DT.Rows[index][0] = words[0]; // txtServiceDesc.Text;
                    DT.Rows[index][9] = words[1]; // txtServiceDesc.Text;
                    DT.Rows[index][6] = words[1];


                    if (DT.Rows[index][6].ToString().Contains("__") == true)
                        DT.Rows[index][9] = DT.Rows[index][6].ToString().Remove(9);

                }
            }

            DT.Rows[index][1] = ddlPeriod.SelectedValue;
            DT.Rows[index][2] = DateFrom.Text;
            DT.Rows[index][3] = txtDateTo.Text;
            DT.Rows[index][4] = txtQuantity.Text;
             if (ddlTreatmentType.SelectedValue == "I")
             {
                 txtQuantity.Text = "1";
             }
             try
             {
                 if (txtCost.Text == (Convert.ToDecimal(txtServiceDesc.Text.Split('-')[2]) * Convert.ToDecimal(txtQuantity.Text)).ToString())
                 { DT.Rows[index][5] = txtCost.Text; }
                 else
                 {
                     txtCost.Text = (Convert.ToDecimal(txtServiceDesc.Text.Split('-')[2]) * Convert.ToDecimal(txtQuantity.Text)).ToString();
                     //txtCost.Text = DT.Rows[index][5];
                     DT.Rows[index][5] = txtCost.Text;
                 }
             }
             catch (Exception ex)
             {
                 DT.Rows[index][5] = txtCost.Text;
                 //ExceptionManager.LogException(ex);
             }
         
            if (ddlServiceDesc.SelectedValue == "O3")
                DT.Rows[index][6] = PutCheckBox();

            if (service == "DEN")
                DT.Rows[index][7] = txtTeethMap.Value;

            DT.Rows[index][8] = ddlLensType.SelectedValue;

            if (txtMinor.Text == "true" && ddlPharmacyDiag.SelectedValue != "???.?")
            {
                string[] words1 = txtServiceDesc.Text.Split('|');

                DT.Rows[index][0] = words1[0];
                DT.Rows[index][9] = words1[1];
                DT.Rows[index][6] = words1[1];

            }

            ////if (Request.QueryString["PID"] != null)
            if (!string.IsNullOrEmpty(PID))
                DT.Rows[index][0] = txtServiceDesc.Text;


            updateOptions();
            if (BLCommon.HRMEnabled)
            {

                if (ddlDepartment.SelectedValue == "DEN" || ddlDepartment.SelectedValue == "OER")
                {
                    txtServiceDesc.Style["display"] = "none";
                    txtServiceDesc.Visible = false;
                    dvMultilevelddl.Visible = false;
                    PnlOpticalHRM.Style["display"] = "none";
                    ddlEyeTestReadingLeft.SelectedIndex = 0;
                    ddlEyeTestReadingRight.SelectedIndex = 0;
                }
                else
                {
                    txtServiceDesc.Visible = true;
                    txtServiceDesc.Style["display"] = "block";
                }

            }
        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }

    }
    public void deleteSelectedOptions()
    {

        try
        {
            DTOptions = (DataTable)Session["dtOptions"];

            for (int i = DTOptions.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = DTOptions.Rows[i];
                if (dr["CategoryCode"].ToString() == txtServiceDesc.Text.Substring(0, 3))
                    dr.Delete();
            }

            Session["dtOptions"] = DTOptions;

        }
        catch (Exception ex) { }

    }
    public void updateOptions()
    {

        try
        {
            DTOptions = (DataTable)Session["dtOptions"];

            if (txtServiceDesc.Text.Length > 3)
            {

                string[] QuestionAnswed = txtHiddenOptions.Text.Split('&');
                string QuestionId;
                string OptionCode;
                string ValueSelected = "Y";
                string QId;
                DataRow drOption;

                //string[] wordlist = new string[1];
                //wordlist[0] = txtServiceDesc.Text.Substring(0, 3);

                //DataRow[] rows = DTOptions.AsEnumerable().Where(x => !wordlist.Contains(x.Field<string>("CategoryCode"))).ToArray();
                //foreach (DataRow row in rows) DTOptions.Rows.Remove(row);
                //DTOptions.AcceptChanges();

                //foreach (DataRow dr in DTOptions.Rows)
                //{
                //    if (dr["CategoryCode"].ToString().Trim() == txtServiceDesc.Text.Substring(0, 3))
                //    {
                //        dr.Delete();
                //    }

                //}
                //DTOptions.AcceptChanges();

                for (int i = DTOptions.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = DTOptions.Rows[i];
                    if (dr["CategoryCode"].ToString() == txtServiceDesc.Text.Substring(0, 3))
                        dr.Delete();
                }


                foreach (string val in QuestionAnswed)
                {
                    if (!String.IsNullOrEmpty(val))
                    {
                        QuestionId = val.Split('=')[0];
                        OptionCode = val.Split('=')[1];
                        ValueSelected = "Y";
                        if (OptionCode.Trim() != "undefined")
                        {
                            drOption = DTOptions.NewRow();
                            drOption["CategoryCode"] = txtServiceDesc.Text.Substring(0, 3);
                            drOption["OptionCode"] = OptionCode.Trim();
                            drOption["OptionDescription"] = "";
                            drOption["OptionValue"] = ValueSelected.Trim();

                            if (QuestionId.Contains("@"))
                            {
                                QId = QuestionId.Split('@')[0];
                                ValueSelected = QuestionId.Split('@')[1];
                                drOption["OptionCode"] = ValueSelected.Trim();
                                drOption["OptionValue"] = OptionCode.Trim();

                            }


                            DTOptions.Rows.Add(drOption);
                        }
                    }
                }

                Session["dtOptions"] = DTOptions;
            }
        }
        catch (Exception ex) { }

    }

    //Husam 11/5/2009 Add Parameter to addval() depend on service
    public void addval(string service)
    {
        try
        {
            ///lblServiceDesc.Text = "";
            DT = (DataTable)Session["dt"];
            DTOptions = (DataTable)Session["dtOptions"];



            DataRow dr = DT.NewRow();
            int iCnt = 0;
            //Husam 11/5/2009 Add Parameter to addval() depend on service

            if (txtMinor.Text == "false")
                dr[0] = txtServiceDesc.Text;

            string temp = string.Empty;
            temp = lblServiceDesc.Text;
            string[] tempServiceDesc = new string[2000];
            tempServiceDesc = temp.Split('-');
            lblServiceDesc.Text = "";
            
            if (txtServiceDesc.Visible == false && txtServiceDesc.Text.Length > 3 && txtServiceDesc.Text.Contains("-"))
            {
                dr["ServiceCode"] = txtServiceDesc.Text.Substring(0, 3);
                dr["ServiceCategoryCode"] = txtServiceDesc.Text.Substring(0, 3);
                //dtOpt = GetData("select SUBSTRING(OptionCode,0,4) CategoryCode, OptionCode, OptionDescription,'N' OptionValue  from preAuthOption where OptionCode like  '%"+ txtServiceDesc.Text.Substring(0, 3) +"%'");
                //DTOptions.Merge(dtOpt);

                string[] QuestionAnswed = txtHiddenOptions.Text.Split('&');
                string QuestionId;
                string OptionCode;
                string ValueSelected = "Y";
                
                string QId;
                DataRow drOption;

                foreach (string val in QuestionAnswed)
                {
                    if (!String.IsNullOrEmpty(val))
                    {
                        QuestionId = val.Split('=')[0];
                        OptionCode = val.Split('=')[1];
                        ValueSelected = "Y";
                        if (OptionCode.Trim() != "undefined")
                        {
                            drOption = DTOptions.NewRow();
                            drOption["CategoryCode"] = txtServiceDesc.Text.Substring(0, 3);
                            drOption["OptionCode"] = OptionCode.Trim();
                            drOption["OptionDescription"] = "";
                            drOption["OptionValue"] = ValueSelected.Trim();

                            if (QuestionId.Contains("@"))
                            {
                                QId = QuestionId.Split('@')[0];
                                ValueSelected = QuestionId.Split('@')[1];
                                drOption["OptionCode"] = ValueSelected.Trim();
                                drOption["OptionValue"] = OptionCode.Trim();

                            }


                            DTOptions.Rows.Add(drOption);
                        }
                    }
                }

                Session["dtOptions"] = DTOptions;
            }

            if (txtMinor.Text == "true")
            {
                if (ddlPharmacyDiag.SelectedValue != "???.?")// || ddlPharmacyDiag.SelectedValue == "J45.0" || ddlPharmacyDiag.SelectedValue == "J35.0" || ddlPharmacyDiag.SelectedValue == "K29.7" || ddlPharmacyDiag.SelectedValue == "G03.9" || ddlPharmacyDiag.SelectedValue == "E11.1" || ddlPharmacyDiag.SelectedValue == "D57.0")
                    dr[10] = "S1"; // txtServiceDesc.Text;

                if (ddlPharmacyDiag.SelectedValue == "J45.9" || ddlPharmacyDiag.SelectedValue == "J45.0" || ddlPharmacyDiag.SelectedValue == "J35.0" || ddlPharmacyDiag.SelectedValue == "K29.7" || ddlPharmacyDiag.SelectedValue == "G03.9" || ddlPharmacyDiag.SelectedValue == "E11.1" || ddlPharmacyDiag.SelectedValue == "D57.0")
                    dr[10] = "C1"; // txtServiceDesc.Text;

                if (ddlPharmacyDiag.SelectedValue == "P24.9" || ddlPharmacyDiag.SelectedValue == "P76.9" || ddlPharmacyDiag.SelectedValue == "P22" || ddlPharmacyDiag.SelectedValue == "P59.9")
                    dr[10] = "E1"; //
            }


            if ((service == "OGP" || service == "VAC" || service == "PHA" || txtMinor.Text == "true") && ddlPharmacyDiag.SelectedValue == "???.?")
            {
                dr[0] = txtServiceDesc.Text;
            }

            if (service == "OGP")// ddlPharmacyDiag.SelectedValue == "???.?")
            {

                switch (ddlPharmacyDiag.SelectedValue)
                {
                    case "Z34":
                        dr[10] = "M1";
                        break;
                    case "Z39":
                        dr[10] = "M2";
                        break;
                    case "O80":
                        dr[10] = "M3";
                        break;
                    case "O82":
                        dr[10] = "M5";
                        break;
                    case "O20":
                        dr[10] = "M6";
                        break;
                    case "O04":
                        dr[10] = "M7";
                        break;
                    case "Z41.3":
                        dr[10] = "M8";
                        break;
                    case "Z41.2":
                        dr[10] = "M9";
                        break;
                    case "O34.3":
                        dr[10] = "M11";
                        break;
                    case "O15.0":
                        dr[10] = "M12";
                        break;
                    case "O42":
                        dr[10] = "M13";
                        break;
                    default:
                        dr[10] = "";
                        break;
                }
            }

            if ((service == "OGP" || service == "VAC" || service == "PHA" || txtMinor.Text == "true") && (ddlPharmacyDiag != null && ddlPharmacyDiag.SelectedValue != "???.?"))
            {
                if (txtServiceDesc.Text.Contains("|") == true)
                {
                    string[] words = txtServiceDesc.Text.Split('|');
                    dr[0] = words[0];
                    if (words[1].Contains("__") == true)
                    {
                        string[] words1 = words[1].Split('_');

                        dr[9] = words[1].Remove(9);
                        dr[7] = words1[2];
                    }
                    else
                        dr[9] = words[1];
                }
                else
                {
                    dr[0] = ddlServiceDesc.SelectedItem;
                    if (tempServiceDesc.Length > 1)
                    {
                        if (tempServiceDesc[0].Trim().ToUpper().Equals("OTH"))
                            dr[9] = string.Empty;
                    }
                    else
                        dr[9] = ddlServiceDesc.SelectedValue;

                    dr[6] = ddlServiceDesc.SelectedValue;
                }

                if (service == "PHA" && txtReqMed.Text == "true")
                {
                    if (ddlServiceDesc.SelectedValue.Contains("__") == true)
                    {
                        string[] words2 = ddlServiceDesc.SelectedValue.Split('_');
                        dr[9] = words2[0];
                        dr[7] = words2[2];
                    }
                }
            }

            if (service == "DEN" || service == "OER")//|| service == "PHA" )
            {
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.InnerHtml = hfhrmDesc.Value;
                hfhrmDesc.Value = div.InnerText;

                //Husam 12/5/2009 Add Data (service desc /service code) into data table
               ////if (ddlServiceDesc.SelectedIndex != 0)
                if (hfOtherFrmae.Value.ToUpper() == "##OF##")
                {
                    dr[0] = lblServiceDescOtherFrame.Text;
                    dr[8] = string.Empty;
                    dr[6] = "O6";//OS-EY-FR
                    dr[9] = "O6";//OS-EY-FR
                    dr[10] = "##OF##";
                    dr[12] = string.Empty;
                    dr[13] = string.Empty;
                }
                else
                {
                    if (ddlServiceDesc.SelectedIndex != 0 || hfhrmDesc.Value != "")
                    {
                        if (BLCommon.HRMEnabled)
                        {
                            //dr[0] = ddlServiceDesc.SelectedItem.ToString().Split('-')[0];
                            //dr[0] = ddlServiceDesc.SelectedItem.ToString();
                            if (ddlServiceDesc.SelectedIndex != 0)
                            {
                                iCnt = ddlServiceDesc.SelectedItem.ToString().Split('-').Length - 2;
                                for (int i = 0; i < iCnt; i++)
                                {
                                    dr[0] += ddlServiceDesc.SelectedItem.ToString().Split('-')[i] + " ";
                                }
                            }
                            else
                            {
                                iCnt = hfhrmDesc.Value.ToString().Split('-').Length - 2;
                                for (int i = 0; i < iCnt; i++)
                                {
                                    dr[0] += hfhrmDesc.Value.Split('-')[i] + " ";
                                }

                            }



                        }
                        else
                        {
                            dr[0] = ddlServiceDesc.SelectedItem.ToString();
                        }
                        //dr[0] = ddlServiceDesc.SelectedItem;
                        dr[9] = ddlServiceDesc.SelectedIndex == 0 ? hfhrmCode.Value : ddlServiceDesc.SelectedValue;
                        
                    }
                    if (service == "OER")
                    {
                        ////if (ddlServiceDesc.SelectedValue == "O3")
                        ////if (ddlServiceDesc.SelectedValue == "OS-EY-LE" || ddlServiceDesc.SelectedValue == "O3")
                        if (ddlServiceDesc.SelectedValue == "OS-EY-LE" || ddlServiceDesc.SelectedValue == "O3" || hfhrmBH.Value == "O3" || hfhrmBH.Value == "OS-EY-LE")
                            dr[6] = PutCheckBox();
                        else
                            dr[6] = "";

                        string codeValue = ddlServiceDesc.SelectedIndex == 0 ? hfhrmCode.Value : ddlServiceDesc.SelectedValue;
                        BupaCodeEntity objBupaCodeEntity = GetBupaCodeEntity(codeValue);
                        if (BLCommon.HRMEnabled)
                        {
                            if (ddlServiceDesc.SelectedIndex != 0)
                            {
                                if (objBupaCodeEntity.BenefitHead == "O3" || ddlServiceDesc.SelectedValue == "O3" || ddlServiceDesc.SelectedValue == "O5")
                                {
                                    //dr[9] = objBupaCodeEntity.BenefitHead;
                                    dr[6] = PutCheckBox();

                                }
                            }
                            else
                            {
                                if (objBupaCodeEntity.BenefitHead == "O3" || hfhrmBH.Value == "O3" || hfhrmBH.Value == "O5")
                                {
                                    //dr[9] = objBupaCodeEntity.BenefitHead;
                                    dr[6] = PutCheckBox();

                                }

                            }

                        }

                        if (!BLCommon.HRMEnabled)
                        {
                            dr[9] = "";
                            dr[9] = ddlServiceDesc.SelectedValue; //Comment thsi line 06/01/2017
                            dr[10] = ddlServiceDesc.SelectedValue;
                        }
                        else
                            dr[10] = hfhrmBH.Value;
                        ////if (ddlServiceDesc.SelectedValue == "O3" || ddlServiceDesc.SelectedValue == "O5")
                        //if (ddlServiceDesc.SelectedValue == "OS-EY-LE" || ddlServiceDesc.SelectedValue == "OS-EY-CL")


                        ////New code start
                        ///val == 'O1' || val == 'O2' || val == 'O6'////lence type empty
                        ///val == 'o3' || val == '05'
                        ////New Code Start Lens & Eye reading value not passing to CAESAR issue fixed.
                        if (BLCommon.HRMEnabled)
                        {
                            if (objBupaCodeEntity.BenefitHead == "O3" || objBupaCodeEntity.BenefitHead == "O5" || hfhrmBH.Value == "O3" || hfhrmBH.Value == "O5")
                            {
                                dr[8] = txtLenseType.Text; // ddlLensType.SelectedValue;
                                dr[12] = ddlEyeTestReadingLeft.SelectedValue;
                                dr[13] = ddlEyeTestReadingRight.SelectedValue;
                            }
                            else
                            {
                                dr[12] = string.Empty;
                                dr[13] = string.Empty;
                            }
                        }
                        else
                        {
                            if (ddlServiceDesc.SelectedValue.Length > 1)
                            {
                                dr[8] = txtLenseType.Text; // ddlLensType.SelectedValue;
                                dr[12] = ddlEyeTestReadingLeft.SelectedValue;
                                dr[13] = ddlEyeTestReadingRight.SelectedValue;

                            }
                            else
                            {
                                dr[12] = string.Empty;
                                dr[13] = string.Empty;
                            }
                        }
                        ////New code End

                        ////Old code Start
                        //if (ddlServiceDesc.SelectedValue.Length > 1)
                        //{
                        //    dr[8] = txtLenseType.Text; // ddlLensType.SelectedValue;
                        //    dr[12] = ddlEyeTestReadingLeft.SelectedValue;
                        //    dr[13] = ddlEyeTestReadingRight.SelectedValue;

                        //}
                        //else
                        //{
                        //    dr[12] = string.Empty;
                        //    dr[13] = string.Empty;
                        //}
                        ////Old code end


                    }
                }
            }
            if (DT != null && DT.Rows.Count == 0)
            {
                dr[1] = 0;
                dr[2] = System.DateTime.Today.ToShortDateString();
                dr[3] = DateTime.Today.AddDays(27).ToShortDateString();
            }
            else
            {
                dr[1] = ddlPeriod.SelectedValue;
                dr[2] = DateFrom.Text;
                dr[3] = txtDateTo.Text;
            }

            //CR108
            if (ddlTreatmentType.SelectedValue == "I")
                dr[4] = "1";
            else
                dr[4] = txtQuantity.Text;

            dr[5] = txtCost.Text;

            if (ddlDepartment.SelectedValue == "OGP" && ddlPharmacyDiag.SelectedValue == "")
            {
                dr[0] = txtServiceDesc.Text;
                dr[9] = "";
            }


            if (ddlDepartment.SelectedValue == "DEN")
            {
                //CR108
                if (ddlServiceDesc.SelectedValue.Contains("__") == true)
                    dr[6] = ddlServiceDesc.SelectedValue.Remove(9);
                else
                {
                    if (BLCommon.HRMEnabled)
                    {
                        dr[6] = ddlServiceDesc.SelectedIndex == 0 ? hfhrmCode.Value : ddlServiceDesc.SelectedValue;
                    }
                    else
                    dr[6] = ddlServiceDesc.SelectedValue;
                }

                dr[7] = txtTeethMap.Value;
            }
            if (ddlDepartment.SelectedValue == "CVC" || ddlDepartment.SelectedValue == "DVC" || ddlDepartment.SelectedValue == "DEC"
       || ddlDepartment.SelectedValue == "ENT" || ddlDepartment.SelectedValue == "GEC" || ddlDepartment.SelectedValue == "GFM"
       || ddlDepartment.SelectedValue == "OBG" || ddlDepartment.SelectedValue == "IMC" || ddlDepartment.SelectedValue == "NPC"
       || ddlDepartment.SelectedValue == "NSC" || ddlDepartment.SelectedValue == "OPH" || ddlDepartment.SelectedValue == "ORT"
       || ddlDepartment.SelectedValue == "PED" || ddlDepartment.SelectedValue == "PUL" || ddlDepartment.SelectedValue == "SUR"
       || ddlDepartment.SelectedValue == "URO")
            {
                if (dr[0].ToString() == "Select from Diagnosis List")
                {
                    dr[0] = txtServiceDesc.Text;
                    if (tempServiceDesc.Length > 1)
                    {
                        if (tempServiceDesc[0].Trim().ToUpper().Equals("OTH"))
                            dr[9] = string.Empty;
                        else
                        {
                            if (txtServiceDesc.Text.Split('-').Length > 1)
                            {
                                dr[9] = txtServiceDesc.Text.Split('-')[0].Trim();
                            }
                        }
                    }
                    else
                    {
                        if (txtServiceDesc.Text.Split('-').Length > 1)
                        {
                            dr[9] = txtServiceDesc.Text.Split('-')[0].Trim();
                        }
                    }
                }
            }

            if(ddlDepartment.SelectedValue == "NEO")
            {
                string[] result = ddlServiceDesc.SelectedValue.Split('~');
                if (result.Length >= 4)
                {                   
                    dr[9] = result[0];
                    dr[10] = result[1];
                    dr[6] = result[2];
                    dr[11] = result[3];
                }
                dr[0] = ddlServiceDesc.SelectedItem.Text;


            }

            ////if (Request.QueryString["PID"] != null)
            if (!string.IsNullOrEmpty(PID))
                dr[0] = txtServiceDesc.Text;

            if (dr[0].ToString() != "Select from Diagnosis List" && dr[0].ToString() != "")
            {
                DT.Rows.Add(dr);
            }


            reset();


        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }
    }
    public BupaCodeEntity GetBupaCodeEntity(string BupaServiceCode )
    {
        BupaCodeEntity listGetBupaCodeEntity = new BupaCodeEntity();
       
        try {
            DataTable dtBupaCode =  BLCommon.BupaCodeLists;
            dtBupaCode=dtBupaCode.Select("servCode ='"+BupaServiceCode+"'").CopyToDataTable();

                    listGetBupaCodeEntity.ServiceCode = dtBupaCode.Rows[0]["servCode"].ToString(); 
                    listGetBupaCodeEntity.ServiceDesc = dtBupaCode.Rows[0]["servDesc"].ToString(); 
                    listGetBupaCodeEntity.ICDCode = dtBupaCode.Rows[0]["ICDCode"].ToString(); 
                    listGetBupaCodeEntity.Amount = Convert.ToDouble(dtBupaCode.Rows[0]["amount"]);
                    listGetBupaCodeEntity.ServiceDisplay = dtBupaCode.Rows[0]["servDisplay"].ToString();
                    listGetBupaCodeEntity.BenefitHead = dtBupaCode.Rows[0]["BenefitHead"].ToString();
                    listGetBupaCodeEntity.EyeTestIndicator = dtBupaCode.Rows[0]["EyeTestIndicator"].ToString();
                    listGetBupaCodeEntity.SupplyIndicator = dtBupaCode.Rows[0]["SupplyIndicator"].ToString();
                    listGetBupaCodeEntity.ToothSiteIndicator = dtBupaCode.Rows[0]["ToothSiteIndicator"].ToString(); 

        }
        catch (Exception ex) { }
        return listGetBupaCodeEntity;
    }
    public void reset()
    {
        try
        {

            txtServiceDesc.Text = "";
            //Husam 11/5/2009 Reset service Description from Page
            if (ddlServiceDesc.SelectedIndex != -1)
                txtServiceDesc.Text = "";
            ddlPeriod.SelectedIndex = 0;
            if (ddlServiceDesc.SelectedIndex != -1) ddlServiceDesc.SelectedIndex = 0;
            DateFrom.Text = System.DateTime.Today.ToShortDateString();
            txtDateTo.Text = DateTime.Today.AddDays(27).ToShortDateString();
            //txtDateTo.Text = "";
            txtQuantity.Text = "";
            txtCost.Text = "";
            txtTeethMap.Value = "";
            // ddlLensType.SelectedIndex = -1;
            //Husam 11/5/2009 reset service code text field
            txtServiceCode.Text = "";
            ResetChechBox();
            Session["Index"] = -1;
            //radComboCategory.ClearSelection();

            hfhrmDesc.Value = string.Empty;
            hfhrmCode.Value = string.Empty;
            hfhrmBH.Value = string.Empty;
            ddlServiceDesc.SelectedIndex = ddlServiceDesc.Items.IndexOf(ddlServiceDesc.Items.FindByValue("0"));

            if (ddlPharmacyDiag.SelectedValue == "???.?")
            {
                ddlServiceDesc.Style["display"] = "none";
                txtServiceDesc.Style["display"] = "block";
                vldtrRFddlServDesc.Enabled = false;
                vldtrRFtxtServDesc.Enabled = true;
                others.Style["display"] = "block";
                vldtrRFtxtOthers.Enabled = true;

            }


            if (ddlPharmacyDiag.SelectedValue != "???.?")
            {
                ddlServiceDesc.Style["display"] = "block";
                txtServiceDesc.Style["display"] = "none";
                vldtrRFddlServDesc.Enabled = true;
                vldtrRFtxtServDesc.Enabled = false;
                others.Style["display"] = "none";
                vldtrRFtxtOthers.Enabled = false;

            }


            if (txtMinor.Text == "false")
            {
                ddlServiceDesc.Style["display"] = "none";
                txtServiceDesc.Style["display"] = "block";
                vldtrRFddlServDesc.Enabled = false;
                vldtrRFtxtServDesc.Enabled = true;
            }




            if (ddlDepartment.SelectedValue == "OER") ddlLensType.SelectedIndex = 0;

            if (((ddlDepartment.SelectedValue == "OGP" || ddlDepartment.SelectedValue == "VAC") && ddlPharmacyDiag.SelectedValue != "") || (txtMinor.Text == "true" && (ddlDepartment.SelectedValue != "PHA" || ddlDepartment.SelectedValue != "OER" || ddlDepartment.SelectedValue != "DEN")))
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script language='javascript'>");
                sb.Append(@"addOptions('" + ddlPharmacyDiag.SelectedValue + "','')");
                sb.Append(@"</script>");

                ScriptManager.RegisterStartupScript(btnAdd, this.GetType(), "JSCR", sb.ToString(), false);
                ddlPharmacyDiag.Enabled = false; // ddlServiceDesc.SelectedIndex = 0;
            }

            if (ddlTreatmentType.SelectedValue == "I")
            {
                txtQuantity.Text = "0";
                lblQuantity.Visible = false;
            }

            if (ddlDepartment.SelectedValue == "PHA" && ddlPharmacyDiag.SelectedValue != "0")
            {
                btnRequestMedication.Enabled = false;
                ddlPharmacyDiag.Enabled = false;
            }

            if (txtMinor.Text == "true" && ddlPharmacyDiag.SelectedValue == "0")
            {
                ddlPharmacyDiag.Enabled = true;
                ddlServiceDesc.Style["display"] = "none";
                txtServiceDesc.Style["display"] = "block";
                vldtrRFddlServDesc.Enabled = false;
                vldtrRFtxtServDesc.Enabled = true;
            }


            DT = (DataTable)Session["dt"];
            GridView1.DataSource = DT;
            GridView1.DataBind();
            txtServiceDesc.Style["display"] = "none";
            txtServiceDesc.Style["display"] = "block";
            dvCatOptionsForm.Visible = false;
            ddlCategory.SelectedIndex = -1;



            //// New code aded by sakthi on 22-Dec-2015
            //// Set dvMultilevelddl visible false during the edit
            //// Start
            switch (ddlDepartment.SelectedValue)
            {


                //case "OGP":
                case "PHA":
                case "OER":
                case "DEN":
                case "VAC":
                    dvMultilevelddl.Visible = false;
                    break;                               
                default:
                    break;
            }

            if (val.Value == "N")
            {
                dvMultilevelddl.Visible = false;
                txtServiceDesc.Visible = true;
                txtServiceDesc.Style["display"] = "block";

            }

            if (GridView1 != null && GridView1.Rows.Count > 0)
            {
                btnSubmit.CausesValidation = true;
            }


            ////End

            Message1.Text = "";
            Message1.Visible = false;
            if (BLCommon.HRMEnabled)
            {
               
                if (ddlDepartment.SelectedValue == "DEN" || ddlDepartment.SelectedValue == "OER")
                {
                    txtServiceDesc.Style["display"] = "none";
                    txtServiceDesc.Visible = false;
                    dvMultilevelddl.Visible = false;
                    PnlOpticalHRM.Style["display"] = "none";
                    ddlEyeTestReadingLeft.SelectedIndex = 0;
                    ddlEyeTestReadingRight.SelectedIndex = 0;
                    PnlOpticalHRM.Attributes.Add("style", "display:block");
                    lblServiceDescOtherFrame.Attributes.Add("style", "display:none");
                    hfOtherFrmae.Value = string.Empty;
                }
                else
                {
                    PnlOpticalHRM.Attributes.Add("style", "display:none");
                }
               
            }

            if(ddlDepartment.SelectedValue == "NEO")
            {
                dvMultilevelddl.Visible = false;
                txtServiceDesc.Visible = false;
                ddlServiceDesc.Visible = true;
                vldtrRFddlServDesc.Enabled = true;
                vldtrRFtxtServDesc.Enabled = false;
                txtServiceDesc.Style["display"] = "none";
                ddlServiceDesc.Style["display"] = "block";
            }


            

        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }

    }

    public void ResetChechBox()
    {
        chkMulticoated.Checked = false;
        chkVarilux.Checked = false;
        chklight.Checked = false;
        chkAspheric.Checked = false;
        chkBifocal.Checked = false;
        chkMedium.Checked = false;
        chkLenticular.Checked = false;
        chkSinglevision.Checked = false;
        chkDark.Checked = false;
        chkSafetythickness.Checked = false;
        chkAntireflectingcoating.Checked = false;
        chkPhotosensitive.Checked = false;
        chkHighindex.Checked = false;
        chkColored.Checked = false;
        chkAntiscratch.Checked = false;

    }

    public string PutCheckBox()
    {
        int Cno = Pane1.Controls.Count - 1;
        HtmlInputCheckBox ch;
        for (int n = 1; n < Cno; n += 4)
        {
            ch = (HtmlInputCheckBox)Pane1.Controls[n];

            switch (ch.Value)
            {
                case "Multicoated":
                    if (chkMulticoated.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkMulticoated);
                    }
                    break;

                case "Varilux":
                    if (chkVarilux.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkVarilux);
                    }
                    break;

                case "light":
                    if (chklight.Checked == true)
                    {
                        SetCodeForCheckBoxs(chklight);
                    }
                    break;
                case "Aspheric":
                    if (chkAspheric.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkAspheric);
                    }
                    break;
                case "Bifocal":
                    if (chkBifocal.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkBifocal);
                    }
                    break;
                case "Medium":
                    if (chkMedium.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkMedium);
                    }
                    break;
                case "Lenticular":
                    if (chkLenticular.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkLenticular);
                    }
                    break;
                case "Singlevision":
                    if (chkSinglevision.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkSinglevision);
                    }
                    break;
                case "Dark":
                    if (chkDark.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkDark);
                    }
                    break;
                case "Safetythickness":
                    if (chkSafetythickness.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkSafetythickness);
                    }
                    break;
                case "Antireflectingcoating":
                    if (chkAntireflectingcoating.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkAntireflectingcoating);
                    }
                    break;
                case "Photosensitive":
                    if (chkPhotosensitive.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkPhotosensitive);
                    }
                    break;
                case "Highindex":
                    if (chkHighindex.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkHighindex);
                    }
                    break;
                case "Colored":
                    if (chkColored.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkColored);
                    }
                    break;
                case "Antiscratch":
                    if (chkAntiscratch.Checked == true)
                    {
                        SetCodeForCheckBoxs(chkAntiscratch);
                    }
                    break;
                default:
                    Rcheck += "-";
                    break;

            }

        }
        //Husam 11/5/2009 Update Recheck [if checked and checkbox or not]
        if (Rcheck.Length == 1)
        {
            Rcheck = "";
        }
        else
        {
            Rcheck = Rcheck.Substring(0, Rcheck.Length - 1) + "}";
        }
        return Rcheck;
    }

    public void SetCodeForCheckBoxs(HtmlInputCheckBox chk)
    {
        if (chk.Checked == true)
        {
            Rcheck += chk.Value + ",";
        }
    }

    public void GetCodeForCheckBoxs(String Code)
    {
        if (Code.Contains("Multicoated"))
        {
            chkMulticoated.Checked = true;
        }
        if (Code.Contains("Varilux"))
        {
            chkVarilux.Checked = true;
        }
        if (Code.Contains("light"))
        {
            chklight.Checked = true;
        }
        if (Code.Contains("Aspheric"))
        {
            chkAspheric.Checked = true;
        }
        if (Code.Contains("Bifocal"))
        {
            chkBifocal.Checked = true;
        }
        if (Code.Contains("Medium"))
        {
            chkMedium.Checked = true;
        }
        if (Code.Contains("Lenticular"))
        {
            chkLenticular.Checked = true;
        }
        if (Code.Contains("Singlevision"))
        {
            chkSinglevision.Checked = true;
        }
        if (Code.Contains("Dark"))
        {
            chkDark.Checked = true;
        }
        if (Code.Contains("Safetythickness"))
        {
            chkSafetythickness.Checked = true;
        }
        if (Code.Contains("Antireflectingcoating"))
        {
            chkAntireflectingcoating.Checked = true;
        }
        if (Code.Contains("Photosensitive"))
        {
            chkPhotosensitive.Checked = true;
        }
        if (Code.Contains("Highindex"))
        {
            chkHighindex.Checked = true;
        }
        if (Code.Contains("Colored"))
        {
            chkColored.Checked = true;
        }
        if (Code.Contains("Antiscratch"))
        {
            chkAntiscratch.Checked = true;
        }
    }

    protected void btnSubmit_Click(object sender, System.EventArgs e)
    {
        try
        {
            //if (Page.IsValid)
            //{
                ///return;
                if (!string.IsNullOrEmpty(lblvalTxt.Text.Trim()))
                {
                    if (string.IsNullOrEmpty(lblval.Text))
                    {
                        lblval.Text = lblvalTxt.Text;
                    }
                }
                if (string.IsNullOrEmpty(lblval.Text))
                {
                    lblval.Text = val.Value;
                }


               
                MemberHelper memberRec = new MemberHelper(txtMembershipNo.Text, "");
                Member member = memberRec.GetMemberDetails();
                string strBody = "";

                try
                {
                    if (ddlTreatmentType.SelectedValue == "I")
                    {
                        txtQuantity.Text = "0";
                        txtQuantity.Style["display"] = "none";
                        lblQuantity.Visible = false;
                        vldtrRQuantity.Enabled = false;
                        txtQuantity.Text = "1";

                        string provider = Session["ProviderName"].ToString();
                        string username = Session["ProviderUserName"].ToString();


                        NotifyExecutive(provider, username, member);




                    }
                }
                catch (Exception ex)
                {
                    Message1.Text = ex.StackTrace;

                }


                //for sabic members
                if (member.ContractNumber == "32559700")
                {
                    if (!string.IsNullOrEmpty(member.MOBILE.Trim()))
                    {
                        switch (ddlDepartment.SelectedItem.Value.Trim())
                        {
                            case "CVC":
                            case "IMC":
                            case "DEC":
                                {

                                    strBody = "<table style='width:80%'><tr><td class='width: 207px;' colspan='2'>New " + ddlDepartment.SelectedItem.Text + " Case  request submitted </td></tr><tr><td class='width: 207px;'>Membership Number</td><td>" + member.MembershipNumber.Trim() + "</td></tr><tr><td class='width: 207px;'>Member Name</td><td>" + member.MemberName.Trim() + "</td></tr><tr><td class='width: 207px;'>Mobile Number</td><td>" + member.MOBILE.Trim() + "</td></tr><tr><td class='width: 207px;'>Provider Name</td><td>" + Convert.ToString(Session["ProviderName"]).Trim() + "</td></tr><tr><td class='width: 207px;'>Requested Department</td><td>" + ddlDepartment.SelectedItem.Text.Trim() + "</td></tr></table><br><br>" + txtDiagnosisDescription.Text.Trim();
                                    //SendEmail("webmaster@bupame.com", "BASabic.Hco@bupa.com.sa", ddlDepartment.SelectedItem.Text + " Member - Admission to the hospital request Request", strBody, null);

                                }
                                break;
                            case "OBG":
                                strBody = "<table style='width:80%'><tr><td class='width: 207px;' colspan='2'>New Maternity Case  request submitted </td></tr><tr><td class='width: 207px;'>Membership Number</td><td>" + member.MembershipNumber.Trim() + "</td></tr><tr><td class='width: 207px;'>Member Name</td><td>" + member.MemberName.Trim() + "</td></tr><tr><td class='width: 207px;'>Mobile Number</td><td>" + member.MOBILE.Trim() + "</td></tr><tr><td class='width: 207px;'>Provider Name</td><td>" + Convert.ToString(Session["ProviderName"]).Trim() + "</td></tr><tr><td class='width: 207px;'>Requested Department</td><td>" + ddlDepartment.SelectedItem.Text.Trim() + "</td></tr></table><br><br>" + txtDiagnosisDescription.Text.Trim();
                               // SendEmail("webmaster@bupame.com", "BASabic.Hco@bupa.com.sa", "Maternity Member - Admission to the hospital request Request", strBody, null);

                                break;
                        }
                    }
                }



                if (!string.IsNullOrEmpty(member.MOBILE.Trim()))
                {
                    switch (ddlDepartment.SelectedItem.Value.Trim())
                    {
                        case "CVC":
                        case "IMC":
                        case "DEC":
                            if (member.SETNAME.Contains("NW5") || member.SETNAME.Contains("NW6") || member.SETNAME.Contains("NW7"))
                            {

                                strBody = "<table style='width:80%'><tr><td class='width: 207px;' colspan='2'>New " + ddlDepartment.SelectedItem.Text + " Case  request submitted </td></tr><tr><td class='width: 207px;'>Membership Number</td><td>" + member.MembershipNumber.Trim() + "</td></tr><tr><td class='width: 207px;'>Member Name</td><td>" + member.MemberName.Trim() + "</td></tr><tr><td class='width: 207px;'>Mobile Number</td><td>" + member.MOBILE.Trim() + "</td></tr><tr><td class='width: 207px;'>Provider Name</td><td>" + Convert.ToString(Session["ProviderName"]).Trim() + "</td></tr><tr><td class='width: 207px;'>Requested Department</td><td>" + ddlDepartment.SelectedItem.Text.Trim() + "</td></tr></table><br><br>" + txtDiagnosisDescription.Text.Trim();
                            }
                            break;
                        case "OBG":
                        case "OGP":
                            strBody = "<table style='width:80%'><tr><td class='width: 207px;' colspan='2'>New Maternity Case  request submitted </td></tr><tr><td class='width: 207px;'>Membership Number</td><td>" + member.MembershipNumber.Trim() + "</td></tr><tr><td class='width: 207px;'>Member Name</td><td>" + member.MemberName.Trim() + "</td></tr><tr><td class='width: 207px;'>Mobile Number</td><td>" + member.MOBILE.Trim() + "</td></tr><tr><td class='width: 207px;'>Provider Name</td><td>" + Convert.ToString(Session["ProviderName"]).Trim() + "</td></tr><tr><td class='width: 207px;'>Requested Department</td><td>" + ddlDepartment.SelectedItem.Text.Trim() + "</td></tr></table><br><br>" + txtDiagnosisDescription.Text.Trim();
                            SendEmail("webmaster@bupame.com", "BAMaternity.Project@bupa.com.sa", "Maternity Member - Admission to the hospital request Request", strBody, null);

                            break;
                    }
                }
                if (txtNeedEyeTable.Text == "true1" && (righteyedistancesphere.Value == "" && lefteyedistancesphere.Value == "" && righteyenearsphere.Value == "" && lefteyenearsphere.Value == "" && righteyedistancecylinder.Value == "" && lefteyedistancecylinder.Value == "" && righteyenearcylinder.Value == "" && lefteyenearcylinder.Value == ""))
                {
                    lblNoTreatmentDetError.Text = "Please input mandatory : Correct Eye Table.";
                    lblNoTreatmentDetError.Visible = true;
                    return;
                }

                DT = (DataTable)Session["DT"];
                if (DT.Rows.Count > 0)
                {
                    switch (ddlDepartment.SelectedValue)
                    {
                        case "DEN":
                            #region Dental
                            ProcessDentalRequest();
                            break;
                            #endregion

                        case "PHA":
                            #region Pharmacy
                            ProcessPharRequest();
                            //ProcessPreAuthPharmacyRequest();
                            break;
                            #endregion
                        case "VAC":
                            #region Pharmacy
                            ProcessPharRequest();
                            break;
                            #endregion
                        case "OER":
                            #region Optical
                            ProcessOpticalRequest();
                            break;
                            #endregion

                        case "OGP":
                        case "OBG":
                            #region Pregnancy
                            //in case of follow-up request for OGP
                            if (ddlPharmacyDiag.SelectedValue == "")
                            {
                                if (val.Value == "N")
                                {
                                    //ProcessStdnPregRequest();
                                    ProcessStdnPreAuthDiagnosisRequest();

                                }
                                else
                                {
                                    ProcessStdnPreAuthDiagnosisRequest();
                                }
                            }
                            else
                            {
                                ProcessPregRequest();
                            }
                            break;
                            #endregion

                        default:
                            {
                                if (val.Value == "N")
                                {
                                    //ProcessStdnPregRequest();
                                    ProcessStdnPreAuthDiagnosisRequest();
                                }
                                else
                                {
                                    ProcessStdnPreAuthDiagnosisRequest();
                                }
                            }

                            break;
                    }
                }
                else
                {
                    lblNoTreatmentDetError.Text = "At least one Treatment details need to be filled";
                    lblNoTreatmentDetError.Visible = true;
                }
                Message1.Visible = true; /// 
                btnSubmit.Text = "Submit Request";
                btnSubmit.Style["display"] = "block"; //.Enabled = true;
                //btnSubmit.Enabled = true;
                Session["peauthupload"] = null;
                Session["sessionID"] = null;
            //}

        }
        catch (Exception ex)
        {
            Message1.Text = ex.StackTrace;
            //ExceptionManager.LogException(ex);
        }

    }

    protected void Button3_Click(object sender, System.EventArgs e)
    {
        try
        {
            lblNoTreatmentDetError.Text = "";

            updateVal(GridView1.SelectedIndex, ddlDepartment.SelectedValue);

            TotalCost = Convert.ToDecimal(txtSumCost.Text) - Convert.ToDecimal(txtTempCost.Text) + Convert.ToDecimal(txtCost.Text);
            txtSumCost.Text = Convert.ToString(TotalCost);
            TotalQuantity = Convert.ToInt32(txtSumQuantity.Text) - Convert.ToInt32(txtTempQuantity.Text) + Convert.ToInt32(txtQuantity.Text);
            txtSumQuantity.Text = Convert.ToString(TotalQuantity);

            reset();
            GridView1.SelectedIndex = -1;
            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnCancel.Visible = false;
            GridView1.Enabled = true;
            GridView1.DataBind();
            //// fixing issue adding multiple record  for "VAC" by sakthi on 22-Dec-2015
            //// could not able to add a new record due to  txtServiceDesc visibility
            //// New code started
            if (ddlDepartment.SelectedValue == "VAC" || ddlDepartment.SelectedValue == "ENT" || ddlDepartment.SelectedValue == "GFM")
            {
                txtServiceDesc.Visible = true;
                txtServiceDesc.Style["display"] = "block";
            }
            else
            {
                txtServiceDesc.Visible = false;
                txtServiceDesc.Style["display"] = "none";
            }

            if (val.Value == "N")
            {
                dvMultilevelddl.Visible = false;
                txtServiceDesc.Visible = true;
                txtServiceDesc.Style["display"] = "block";

            }

            ////if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["PID"])))
            if (!string.IsNullOrEmpty(PID))
            {
                dvMultilevelddl.Visible = false;
                txtServiceDesc.Visible = true;
                txtServiceDesc.Style["display"] = "block";
            }

            //// New code end
            //// old code started       
            ////   txtServiceDesc.Visible = false;
            ////   txtServiceDesc.Style["display"] = "none";
            //// old code started

        }
        catch (Exception ex) { }
    }

    protected void btnFetchDetails_Click(object sender, EventArgs e)
    {
        DisplayMemberExistResult();
    }

    private void DisplayMemberExistResult()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.RequestMbrAgeInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.RequestMbrAgeInfoResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestMbrAgeInfoRequest_DN();
            request.membershipNo = txtMembershipNo.Text;
            request.transactionID = TransactionManager.TransactionID();
            request.provCode = strProviderID;

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.RequestMbrAgeInfo(request);

            if (response.status == "0")
            {
                lblMemberName.Text = "<br><nobr>Name: <font size=3> " + response.memberName + "</font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Age: <font size=3> " + response.ageInYear.ToString() + " years  - ";


                if (response.gender == "F")
                    lblMemberName.Text += " ( Female )</font></nobr>";

                if (response.gender == "M")
                    lblMemberName.Text += " ( Male )</font></nobr>";

                //Check member Age if less than or equal 5
                if (Convert.ToInt32(response.ageInYear.ToString()) <= 5)
                {
                    Session["LessAge"] = "Yes";
                    chk.Visible = false;
                }
                else
                {
                    Session["LessAge"] = "No";
                }

                if (response.extIndicator == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "$.modal.close();");
                    litExtendedDate.Text = ((DateTime)response.extDate).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }

                txtMemberName.Text = response.memberName;
                txtAge.Text = response.ageInYear.ToString();
                ddlGender.Text = response.gender;//
                txtAgeInMonths.Text = response.ageInMonth.ToString();
                btnProceed.Enabled = true;
                txtMembershipNo.Enabled = false;
                btnProceed.Enabled = true;

                try
                {
                    MemberHelper memberRec = new MemberHelper(txtMembershipNo.Text, "");
                    Member member = memberRec.GetMemberDetails();
                    //if (Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                    if (BLCommon.PreAuthAllowed)
                    {
                        if (member.PreauthOpt == "F")
                        {
                            dvPreAuthOpt.InnerHtml = " <center><b>Member Preauth Option : Full Cover � No Exclusion</b></center> ";
                            dvPreAuthOpt.Visible = true;
                            //dvMultilevelddl.Visible=false;

                        }
                        else if (member.PreauthOpt == "N")
                        {

                            dvPreAuthOpt.InnerHtml = " <center><b>Member Preauth Option : No Pre-authorization Required</b></center> ";
                            dvPreAuthOpt.Visible = true;
                            //dvMultilevelddl.Visible=false;

                        }
                        else
                        {

                            //dvCatOptionsForm.InnerHtml = "<P style=' width:800px;overflow:auto;background-color: lightgreen;'><center><b>PreAuth Status : Standard</b></center></p>";
                            dvPreAuthOpt.Visible = true;
                            //dvMultilevelddl.Visible=false;
                        }
                    }
                }
                catch (Exception ex) { }

            }
            else
            {
                if (response.extIndicator == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "$.modal.close();');");
                    litExtendedDate.Text = ((DateTime)response.extDate).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }
                btnProceed.Enabled = false;
                lblMemberName.Text = response.errorMessage[0];
            }

        }
        catch (Exception ex)
        {

        }
    }

    private bool Validate(string strName)
    {
        Regex rx = new Regex("[^A-Z|^a-z|^ |^\t]");
        if (rx.IsMatch(strName))
        {
            return false;
        }
        else
        {
            return true;
        }

    }
    public DataTable LoadHRMServices(string sServiceCode)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListResponse_DN response;
        BupaCodeListDetail_DN[] BupaCodeList;
        DataTable dt = new DataTable();
        dt.TableName = "BupaHRMCode";
        dt.Columns.Add("servCode");
        dt.Columns.Add("servDesc");
        dt.Columns.Add("amount");
        dt.Columns.Add("ICDCode");
        dt.Columns.Add("servDisplay");

        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN();
            request.transactionID = TransactionManager.TransactionID();
            request.provCode = strProviderID;
            request.PAAutomation = sServiceCode;// ddlDepartment.SelectedValue;
            //ADC    24 Hrs Admission for Child
            //CLS    Colonoscopy
            //CRN    Chronic Medication
            //DEN    Dental
            //EDO    UGI Endoscopy
            //MAT    Maternity
            //MRK    MRI Knee
            //MRS    MRI Shoulder
            //MSP    MRI Spine
            //OPT    Optical
            //PNS    CT PNS
            //VAC    Vaccination
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ReqBupaCodeList(request);

            if (response.status == "0")
            {

                BupaCodeList = response.detail;
                foreach (BupaCodeListDetail_DN bcl in response.detail)
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = bcl.servCode;
                    dr[1] = bcl.servDesc;
                    dr[2] = bcl.amount;
                    dr[3] = bcl.ICDCode;
                    dr[4] = bcl.servDesc + " - " + bcl.amount;
                    dt.Rows.Add(dr);
                }
                //response.errorMessage;  
                //response.errorID;
                //response.transactionID;

            }


        }
        catch (Exception ex)
        {

        }
        return dt;
    }
    public DataTable LoadHRMServices()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqBupaCodeListResponse_DN response;
        BupaCodeListDetail_DN[] BupaCodeList;
        DataTable dt = new DataTable();
        dt.TableName = "BupaHRMCode";
        dt.Columns.Add("servCode");
        dt.Columns.Add("servDesc");
        dt.Columns.Add("amount");
        dt.Columns.Add("ICDCode");
        dt.Columns.Add("servDisplay");
     
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqBupaCodeListRequest_DN();
            request.transactionID = TransactionManager.TransactionID();
            request.provCode = strProviderID;
            request.PAAutomation = ddlDepartment.SelectedValue; 
            //ADC    24 Hrs Admission for Child
            //CLS    Colonoscopy
            //CRN    Chronic Medication
            //DEN    Dental
            //EDO    UGI Endoscopy
            //MAT    Maternity
            //MRK    MRI Knee
            //MRS    MRI Shoulder
            //MSP    MRI Spine
            //OPT    Optical
            //PNS    CT PNS
            //VAC    Vaccination
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ReqBupaCodeList(request);

            if (response.status == "0")
            {
              
                BupaCodeList = response.detail;
                foreach(BupaCodeListDetail_DN bcl in response.detail )
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = bcl.servCode;
                    dr[1] = bcl.servDesc;
                    dr[2] = bcl.amount;
                    dr[3] = bcl.ICDCode;
                    dr[4] = bcl.servDesc + " - " + bcl.amount;
                    dt.Rows.Add(dr);
                }
                //response.errorMessage;  
                //response.errorID;
                //response.transactionID;
            
            }
            
        
        } 
        catch (Exception ex)
        {

        }
        return dt;
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        try
        {

            ViewState["TrustedDoctor"] = null;
            if (tdPhysicianTextBox.Visible == false)
            {
                if (ddlPhysician.SelectedValue == "Others")
                {
                    txtNameOfPhysician.Text = txtOtherPhysicianName.Text;
                }
                else
                {
                    txtNameOfPhysician.Text = ddlPhysician.SelectedValue;

                    ProviderPhysiciansManagement objProviderPhysiciansManagement = ((List<ProviderPhysiciansManagement>)Session["ProviderPhysiciansManagementList"]).Find(item => item.PPM_PhysicianName == ddlPhysician.SelectedValue);
                    if (objProviderPhysiciansManagement != null)
                    {
                        ViewState["TrustedDoctor"] = objProviderPhysiciansManagement.PPM_IsTrusted;
                    }

                }
            }
            if (val.Value == "N")
            {
                dvMultilevelddl.Visible = false;
                txtServiceDesc.Visible = true;
                txtServiceDesc.Style["display"] = "block";

            }

            if (string.IsNullOrEmpty(txtNameOfPhysician.Text) || string.IsNullOrEmpty(txtPatientFileNo.Text) || ddlDepartment.SelectedValue == "0" || ddlTreatmentType.SelectedValue == "0")
            {
                Message1.Text = "Please fill in all required fields";
            }
            else if (!Validate(txtNameOfPhysician.Text))
            {
                Message1.Text = "Please enter only aplhabets in the Physician Name";
            }
            else
            {
                SqlConnection connection = null;
                try
                {


                    connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
                    MemberHelper memberRec = new MemberHelper(txtMembershipNo.Text, "");
                    Member member = memberRec.GetMemberDetails();
                    hidPrevPreg.Value = member.IVF_Prev_Pregnancy;
                    SqlParameter[] arParmsInsert = new SqlParameter[8];
                    arParmsInsert[0] = new SqlParameter("@ProCode", SqlDbType.NChar, 10);
                    arParmsInsert[0].Value = Session["ProviderID"];
                    arParmsInsert[1] = new SqlParameter("@ProName", SqlDbType.NVarChar, 100);
                    arParmsInsert[1].Value = Session["ProviderName"];
                    arParmsInsert[2] = new SqlParameter("@MemberShipNo", SqlDbType.NChar, 10);
                    arParmsInsert[2].Value = txtMembershipNo.Text;
                    arParmsInsert[3] = new SqlParameter("@MemberName", SqlDbType.NVarChar, 150);
                    arParmsInsert[3].Value = member.MemberName;
                    arParmsInsert[4] = new SqlParameter("@MemberType", SqlDbType.NChar, 10);
                    arParmsInsert[4].Value = member.VIP;
                    arParmsInsert[5] = new SqlParameter("@AddedBy", SqlDbType.NChar, 10);
                    arParmsInsert[5].Value = Session["ProviderUserName"];
                    arParmsInsert[6] = new SqlParameter("@Department", SqlDbType.NChar, 100);
                    arParmsInsert[6].Value = ddlDepartment.SelectedItem.Text;
                    arParmsInsert[7] = new SqlParameter("@Mobile", SqlDbType.NChar, 10);
                    arParmsInsert[7].Value = member.MOBILE;
                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spBupa_ProviderVerificationInsert", arParmsInsert);
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["LessAge"])))
                    {
                        if (Convert.ToString(Session["LessAge"]) == "Yes")
                        {
                            if (ddlTreatmentType.SelectedValue == "I")
                            {
                                txtCIDCode.Visible = false;
                                btnICD.Visible = false;
                            }
                            else
                            {
                                txtCIDCode.Visible = true;
                                btnICD.Visible = true;
                            }
                        }
                        else
                        {

                        }
                    }


                    preAuthOptStatus = string.IsNullOrEmpty(member.PreauthOpt) ? "S" : member.PreauthOpt;
                     BLCommon.PreAuthOpt = preAuthOptStatus;

                    try
                    {
                        LoadControlScript((DataTable)Session["dtServiceCategory"]);
                    }
                    catch (Exception ex) { }


                    //if (Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                        if (BLCommon.PreAuthAllowed)
                    {
                        Session["preAuthOptStatus"] = preAuthOptStatus;
                        if (member.PreauthOpt == "F")
                        {
                            dvCatOptionsForm.InnerHtml = "<P style=' width:800px;overflow:auto;background-color: lightgreen;'><center><b>Member Preauth Option : Full Cover � No Exclusion</b></center></p>";
                            //dvCatOptionsForm.Visible = true;
                            //dvMultilevelddl.Visible=false;

                        }
                        else if (member.PreauthOpt == "N")
                        {

                            dvCatOptionsForm.InnerHtml = "<P style='width:800px;overflow:auto;background-color: lightgreen;'><center><b>Member Preauth Option : No Pre-authorization Required</b></center></p>";
                            //dvCatOptionsForm.Visible = true;
                            //dvMultilevelddl.Visible=false;

                        }
                        else
                        {

                            //dvCatOptionsForm.InnerHtml = "<P style=' width:800px;overflow:auto;background-color: lightgreen;'><center><b>Member Preauth Option : Standard</b></center></p>";
                            //dvCatOptionsForm.Visible = true;
                            //dvMultilevelddl.Visible=false;
                        }
                    }
                    else
                    {
                        txtServiceDesc.Visible = true;
                        txtServiceDesc.Style["display"] = "block";
                    }
                    Message1.Text = "";
                    Panel1.Enabled = false;
                    PnlVerify.Enabled = false;
                    UPnlTreatmentDets.Visible = true;
                    updatePanel.Visible = true;
                    UPnlDiagnosis.Visible = true;
                    GridView1.Columns[0].Visible = false;
                    GridView1.Columns[3].Visible = false;
                    GridView1.Columns[4].Visible = false;
                    GridView1.Columns[5].Visible = false;
                    GridView1.Columns[8].Visible = false;
                    GridView1.Columns[9].Visible = false;
                    btnOtherFrame.Visible = false;
                    txtDateOfVisit.Text = DateTime.Today.ToShortDateString();

                    XmlDataSource xmlDS = new XmlDataSource();
                    XmlDataSource xmlDS2 = new XmlDataSource();

                    LoadCategoryDropdownMenu();

                    switch (ddlDepartment.SelectedValue)
                    {
                        case "DEN":
                            #region Dental
                            PnlDentalCheckList.Visible = true;
                            PnlDentalStandardCheckList.Visible = true;
                            PnlDiagnosisCommon.Visible = true;
                            PnlStandard.Visible = false;
                            PnlStandardCheckList.Visible = false;
                            lbl_DiagnosisDetails.Visible = true;
                            lbl_PleaseTick.Visible = true;
                            PnlOptical.Visible = false;
                            PnlPharmacy.Visible = false;
                            txtCIDCode.Visible = false;
                            btnICD.Visible = false;

                            if (BLCommon.HRMEnabled)
                            {
                                BupaCaesar bupaCode = new BupaCaesar();

                                if (BLCommon.BupaCodeLists != null)
                                {
                                    ddlServiceDesc.DataSource = BLCommon.BupaCodeLists; 
                                }
                                else
                                { 
                                    BLCommon.BupaCodeLists = bupaCode.LoadHRMServices(strProviderID, ddlDepartment.SelectedValue,"","");
                                    ddlServiceDesc.DataSource = BLCommon.BupaCodeLists; 
                                }
                                
                                //ddlServiceDesc.DataSource = LoadHRMServices();
                                ddlServiceDesc.DataTextField = "servDisplay";
                                ddlServiceDesc.DataValueField = "servCode";
                                ddlServiceDesc.DataBind();
                                

                            }
                            else
                            {
                                xmlDS.DataFile = "~/Provider/XMLFiles/ServiceDescriptionDental.xml";
                                ddlServiceDesc.DataSource = xmlDS;
                                ddlServiceDesc.DataTextField = "text";
                                ddlServiceDesc.DataValueField = "value";
                                ddlServiceDesc.DataBind();
                            }

                            if (!BLCommon.HRMEnabled)
                            {
                                xmlDS.DataFile = "~/Provider/XMLFiles/ServiceDescriptionDental1.xml";
                                ddlServiceDesc1.DataSource = SqlDatSourceDental1;
                                ddlServiceDesc1.DataTextField = "text";
                                ddlServiceDesc1.DataValueField = "value";
                                ddlServiceDesc1.DataBind();
                            }
                            ddlServiceDesc.Items.Insert(0,(new ListItem("-- Select --", "0")));
                            ddlServiceDesc.SelectedIndex = 0;    

                            //ddlServiceDesc.Items.Add(new ListItem("-- Select --", "0"));
                            //ddlServiceDesc.SelectedValue = "0";
                           
                            PanelDental.Visible = true;
                            ddlServiceDesc.Visible = true;
                            txtServiceDesc.Visible = false;
                            dvMultilevelddl.Visible = false;
                            lblDiagCode.Visible = true;
                            txtDiagCode.Visible = true;
                            GridView1.Columns[0].Visible = true;
                            GridView1.Columns[1].Visible = false;
                            GridView1.Columns[8].Visible = true;
                            chk.Visible = false;
                            break;
                            #endregion
                        case "PHA":
                            #region Pharmacy
                            lblPharNotes.Visible = true;
                            PnlDentalCheckList.Visible = false;
                            PnlDiagnosisCommon.Visible = false;
                            PnlStandard.Visible = false;
                            PnlStandardCheckList.Visible = false;
                            lbl_DiagnosisDetails.Visible = false;
                            lbl_PleaseTick.Visible = false;
                            PnlOptical.Visible = false;
                            PnlPharmacy.Visible = true;
                            PnlDentalStandardCheckList.Visible = false;
                            txtDateTo.Text = DateTime.Today.AddDays(27).ToShortDateString();
                            DateFrom.Text = DateTime.Today.ToShortDateString();
                            txtCIDCode.Visible = false;
                            btnICD.Visible = false;
                            xmlDS2.DataFile = "~/Provider/XMLFiles/DiagnosisDescriptionPharmcy.xml";
                            ddlPharmacyDiag.DataSource = xmlDS2;
                            ddlPharmacyDiag.DataTextField = "text";
                            ddlPharmacyDiag.DataValueField = "value";
                            ddlPharmacyDiag.DataBind();
                            xmlDS.DataFile = "~/Provider/XMLFiles/ServiceDescriptionPharmcyNew.xml";
                            ddlServiceDesc.DataSource = SqlDataSourcechronic;
                            ddlServiceDesc.DataTextField = "text";
                            ddlServiceDesc.DataValueField = "value";
                            ddlServiceDesc.DataBind();
                            ddlServiceDesc.Visible = true;
                            txtServiceDesc.Visible = true;
                            dvMultilevelddl.Visible = false;
                            PanelPharmacy.Visible = true;
                            GridView1.Columns[3].Visible = true;
                            GridView1.Columns[4].Visible = true;
                            GridView1.Columns[5].Visible = true;
                            chk.Visible = false;
                            break;
                            #endregion
                        case "OER":
                            #region Optical
                            PnlDentalCheckList.Visible = false;
                            PnlDiagnosisCommon.Visible = true;
                            PnlStandard.Visible = false;
                            PnlStandardCheckList.Visible = false;
                            lbl_DiagnosisDetails.Visible = false;
                            lbl_PleaseTick.Visible = false;
                            PnlOptical.Visible = true;
                            PnlPharmacy.Visible = false;
                            PnlDentalStandardCheckList.Visible = false;
                            txtCIDCode.Visible = false;
                            btnICD.Visible = false;
                            btnOtherFrame.Visible = true;

                            if (BLCommon.HRMEnabled)                                                        
                            {
                                BupaCaesar bupaCode = new BupaCaesar();
                                if (BLCommon.BupaCodeLists != null)
                                {
                                    ddlServiceDesc.DataSource = BLCommon.BupaCodeLists;
                                }
                                else
                               {
                                    //BLCommon.BupaCodeLists = bupaCode.LoadHRMServices(strProviderID, "OPT");
                                    BLCommon.BupaCodeLists = bupaCode.LoadHRMServices(strProviderID, "OPT","","");
                                    ddlServiceDesc.DataSource = BLCommon.BupaCodeLists;
                                }
                                
                                //ddlServiceDesc.DataSource = LoadHRMServices("OPT");
                                ddlServiceDesc.DataTextField = "servDisplay";

                                ddlServiceDesc.DataValueField = "servCode";
                                ddlServiceDesc.DataBind();

                               
                            }
                            else
                            {
                                xmlDS.DataFile = "~/Provider/XMLFiles/ServiceDescriptionOptical.xml";
                                ddlServiceDesc.DataSource = xmlDS;
                                ddlServiceDesc.DataTextField = "text";
                                ddlServiceDesc.DataValueField = "value";
                                ddlServiceDesc.DataBind();
                            }


                            ddlServiceDesc.Items.Insert(0,(new ListItem("-- Select --", "0")));
                            ddlServiceDesc.SelectedIndex = 0;    
                            //ddlServiceDesc.Items.Add(new ListItem("-- Select --", "0"));
                            //ddlServiceDesc.SelectedValue = "0";
                            
                            PanelOptical.Visible = true;
                            ddlServiceDesc.Visible = true;
                            txtServiceDesc.Visible = false;
                            dvMultilevelddl.Visible = false;
                            lblDiagCode.Visible = true;
                            txtDiagCode.Visible = true;
                            GridView1.Columns[0].Visible = true;
                            GridView1.Columns[1].Visible = false;
                            chk.Visible = false;
                            break;
                            #endregion
                        case "OGP":
                            #region Pregnancy
                            pnlMaternity.Visible = true;
                            PnlDentalCheckList.Visible = false;
                            PnlDiagnosisCommon.Visible = true;
                            PnlStandard.Visible = true;
                            PnlStandardCheckList.Visible = true;
                            lbl_DiagnosisDetails.Visible = true;
                            lbl_PleaseTick.Visible = true;
                            PnlOptical.Visible = false;
                            PnlDentalStandardCheckList.Visible = true;
                            PnlPharmacy.Visible = true;
                            txtDiagnosisDescription.Visible = false;
                            lblDiagnosisDescription.Visible = false;
                            btnRequestMedication.Visible = false;
                            txtCIDCode.Visible = false;
                            btnICD.Visible = false;
                            RequiredFieldValidator55.Enabled = false;
                            xmlDS2.DataFile = "~/Provider/XMLFiles/DiagnosisDescriptionPregnancy.xml";
                            ddlPharmacyDiag.DataSource = xmlDS2;
                            ddlPharmacyDiag.DataTextField = "text";
                            ddlPharmacyDiag.DataValueField = "value";
                            ddlPharmacyDiag.DataBind();
                            ddlServiceDesc.Visible = true;
                            ddlServiceDesc.Items.Clear();
                            ddlServiceDesc.Items.Add(new ListItem("Select from Diagnosis List", "0"));
                            //addServices();
                            txtServiceDesc.Visible = true; // false;
                            dvMultilevelddl.Visible = false;
                            txtServiceCode.Visible = true;
                            txtServiceCode.Style["display"] = "none";
                            chk.Visible = false;
                            break;
                            #endregion
                        case "VAC":
                            #region Pregnancy
                            PnlDentalCheckList.Visible = false;
                            PnlOptical.Visible = false;
                            PnlPharmacy.Visible = true;
                            txtDiagnosisDescription.Visible = false;
                            lblDiagnosisDescription.Visible = false;
                            btnRequestMedication.Visible = false;
                            txtCIDCode.Visible = false;
                            btnICD.Visible = false;
                            RequiredFieldValidator55.Enabled = false;
                            ddlPharmacyDiag.Items.Add(new ListItem("Select", "0"));
                            ddlPharmacyDiag.Items.Add(new ListItem("Vaccination Hajj/Umra", "ZA27.6"));
                            ddlPharmacyDiag.Items.Add(new ListItem("Vaccination MOH", "ZA27.5"));
                            ddlServiceDesc.Visible = true;
                            ddlServiceDesc.Items.Clear();
                            ddlServiceDesc.Items.Add(new ListItem("Select from Diagnosis List", "0"));
                            txtServiceDesc.Visible = true; // false;
                            dvMultilevelddl.Visible = false;
                            txtServiceCode.Visible = true;
                            txtServiceCode.Style["display"] = "none";
                            chk.Visible = false;
                            break;
                        #endregion
                        case "NEO":
                            #region Neonatal Screening
                            PnlDentalCheckList.Visible = false;
                            PnlDiagnosisCommon.Visible = true;
                            PnlStandard.Visible = true;
                            PnlStandardCheckList.Visible = true;
                            lbl_DiagnosisDetails.Visible = true;
                            lbl_PleaseTick.Visible = true;
                            PnlOptical.Visible = false;
                            PnlDentalStandardCheckList.Visible = true;
                            txtDiagnosisDescription.Visible = true;
                            lblDiagnosisDescription.Visible = true;
                            btnRequestMedication.Visible = false;
                            chk.Visible = true;
                            vldtrRFtxtServDesc.Enabled = false;
                            vldtrRFddlServDesc.Enabled = true;
                            txtServiceDesc.Visible = false;
                            ddlServiceDesc.Visible = true;
                            dvMultilevelddl.Visible = false;
                            txtMinor.Text = "false";
                            Message1.Visible = true;
                            xmlDS.DataFile = "~/Provider/XMLFiles/ServiceDescriptionNeonatalScreening.xml";
                            ddlServiceDesc.DataSource = xmlDS;
                            ddlServiceDesc.DataTextField = "text";
                            ddlServiceDesc.DataValueField = "value";
                            ddlServiceDesc.DataBind();
                            break;
                        #endregion
                        default:
                            PnlDentalCheckList.Visible = false;
                            PnlDiagnosisCommon.Visible = true;
                            PnlStandard.Visible = true;
                            PnlStandardCheckList.Visible = true;
                            lbl_DiagnosisDetails.Visible = true;
                            lbl_PleaseTick.Visible = true;
                            PnlOptical.Visible = false;
                            PnlDentalStandardCheckList.Visible = true;
                            txtDiagnosisDescription.Visible = true;
                            lblDiagnosisDescription.Visible = true;
                            btnRequestMedication.Visible = false;
                            long Age;
                            Age = ValidateAge();
                            if (Age <= 5 && ddlTreatmentType.SelectedValue == "I")
                            {
                                PnlPharmacy.Visible = true;
                                txtDiagnosisDescription.Visible = false;
                                lblDiagnosisDescription.Visible = false;
                                btnRequestMedication.Visible = false;

                                xmlDS2.DataFile = "~/Provider/XMLFiles/Minor.xml";
                                ddlPharmacyDiag.DataSource = xmlDS2;
                                ddlPharmacyDiag.DataTextField = "text";
                                ddlPharmacyDiag.DataValueField = "value";
                                ddlPharmacyDiag.DataBind();

                                ddlServiceDesc.Visible = true;
                                ddlServiceDesc.Items.Clear();
                                ddlServiceDesc.Items.Add(new ListItem("Select from Diagnosis List", "0"));
                                ddlServiceDesc.Items.Add(new ListItem("24 hours admission for care & management", "ADM001"));
                                ddlServiceDesc.Items.Add(new ListItem("24 hours PICU admission for care & management", "ADM002"));

                                txtServiceDesc.Visible = true; // false;
                                txtAge.Text = Age.ToString();
                                txtMinor.Text = "true";

                                //New change after full preAuth Implementation
                                //if (val.Value == "Y")
                                if (val.Value == "Y" && BLCommon.PreAuthAllowed && Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                                {
                                    txtDiagnosisDescription.Visible = true;
                                    lblDiagnosisDescription.Visible = true;
                                    ddlServiceDesc.Visible = false;
                                    ddlPharmacyDiag.Visible = false;
                                    Label5.Visible = false;
                                }
                                else
                                {
                                    txtServiceDesc.Visible = false;
                                }
                            }
                            else
                            {
                                vldtrRFtxtServDesc.Enabled = true;
                                vldtrRFddlServDesc.Enabled = false;
                                txtServiceDesc.Visible = true;
                                ddlServiceDesc.Visible = false;
                                txtServiceDesc.Visible = true;
                                txtServiceDesc.Style["display"] = "block";
                                ddlServiceDesc.Visible = false;
                                txtMinor.Text = "false";
                            }
                            if (ddlTreatmentType.SelectedValue == "O")
                            {
                                chk.Visible = true;
                            }

                            Message1.Visible = true;
                            break;
                    }


                    if (ddlTreatmentType.SelectedValue == "I")
                    {
                        txtQuantity.Text = "0";
                        txtQuantity.Style["display"] = "none";
                        lblQuantity.Visible = false;
                        vldtrRQuantity.Enabled = false;
                        txtQuantity.Text = "1";


                    }

                    DT = (DataTable)Session["dt"];
                    DT.Clear();

                    txtServiceDesc.Style["display"] = "none";
                    //txtServiceDesc.Style["display"] = "block"; // To be removed, after confirmation from business
                    //  LoadFirstRows(1);

                    GridView1.DataBind();
                    //bodyElement.Attributes["bgColor"] = "Green";
                    //Page.StyleSheetTheme = ""; 

                    if (ddlTreatmentType.SelectedValue == "I")
                    {
                        txtLengthOfStay.Visible = true;
                        lblLengthOfStay.Visible = true;
                        RFVldtrLengthofStay.Enabled = true;
                        CompareVldtrLengthofStay.Enabled = true;


                    }


                    //if (!Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                    if ((ddlDepartment.SelectedValue != "OER" && ddlDepartment.SelectedValue != "DEN") && !BLCommon.PreAuthAllowed && !Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
                    {
                        txtServiceDesc.Visible = true;
                        txtServiceDesc.Style["display"] = "block";
                    }

                    //By Athar, to disable service description text box
                    if(ddlDepartment.SelectedValue.ToUpper() == "NEO")
                    {
                        txtServiceDesc.Visible = false;
                        txtServiceDesc.Style["display"] = "none";
                    }

                    if (val.Value == "N")
                    {
                        dvMultilevelddl.Visible = false;
                        txtServiceDesc.Visible = true;
                        txtServiceDesc.Style["display"] = "block";
                    }


                }
                catch (Exception ex)
                {
                    Message1.Text = ex.Message; //"The connection with the database can�t be established. Application Error";
                    return;
                }
                finally
                {
                    connection.Close();
                }
            }
            if(BLCommon.HRMEnabled)
            if (ddlDepartment.SelectedValue != "OER" && ddlDepartment.SelectedValue != "DEN")
                btnSelectService.Visible = false;
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
            //ExceptionManager.LogException(ex);
        }
        lblval.Text = val.Value;
    }
    public bool addServices()
    {

        try
        {

            ddlServiceDesc.Items.Clear();

            ddlServiceDesc.Items.Add(new ListItem("2 Hours Post Prandial", "ANC042"));
            //ddlServiceDesc.Items.Add(new ListItem("Aldomet Tab","ANC079"));  
            ddlServiceDesc.Items.Add(new ListItem("Alpha Fetoprotein", "ANC043"));
            ddlServiceDesc.Items.Add(new ListItem("Amniocentesis", "ANC044"));

            //ddlServiceDesc.Items.Add(new ListItem("Analgesic/NSAID","ANC039"));             
            //ddlServiceDesc.Items.Add(new ListItem("Anti D","ANC080"));                   
            //ddlServiceDesc.Items.Add(new ListItem("Antibiotic","ANC036"));              
            ddlServiceDesc.Items.Add(new ListItem("Antibody Screening", "ANC045"));
            ddlServiceDesc.Items.Add(new ListItem("Anti-cardiolipin Test", "ANC046"));
            //ddlServiceDesc.Items.Add(new ListItem("Antiemetic (eg, Navidoxine)","ANC034"));                       
            //ddlServiceDesc.Items.Add(new ListItem("Antifungal","ANC037"));              
            ddlServiceDesc.Items.Add(new ListItem("Anti-phospolipid Test", "ANC047"));
            //ddlServiceDesc.Items.Add(new ListItem("Antispasmodic (eg, Buscopan)","ANC035"));                   
            //ddlServiceDesc.Items.Add(new ListItem("Ar Natal","ANC081"));                
            //ddlServiceDesc.Items.Add(new ListItem("Aspirin","ANC108"));                  
            ddlServiceDesc.Items.Add(new ListItem("Beta HCG (Qualitative)", "ANC048"));
            ddlServiceDesc.Items.Add(new ListItem("Beta HCG (Quantitative)", "ANC049"));
            ddlServiceDesc.Items.Add(new ListItem("Biophysical Profile", "ANC027"));
            ddlServiceDesc.Items.Add(new ListItem("Blood Group & Rh Factor", "ANC004"));
            //ddlServiceDesc.Items.Add(new ListItem("Calcichew","ANC082"));             
            //ddlServiceDesc.Items.Add(new ListItem("Calcigogen Tab","ANC083"));                 
            //ddlServiceDesc.Items.Add(new ListItem("Calcium Supplement","ANC032"));                      
            //ddlServiceDesc.Items.Add(new ListItem("Caltrate","ANC084"));                 
            //ddlServiceDesc.Items.Add(new ListItem("Canesten Cream","ANC085"));                
            ddlServiceDesc.Items.Add(new ListItem("CBC", "ANC001"));
            //ddlServiceDesc.Items.Add(new ListItem("Celestone Amp","ANC086")); 
            ddlServiceDesc.Items.Add(new ListItem("Chalmydia", "ANC050"));
            ddlServiceDesc.Items.Add(new ListItem("Chromosomal Analysis", "ANC051"));

            //ddlServiceDesc.Items.Add(new ListItem("Clexane Inj","ANC087"));                        
            ddlServiceDesc.Items.Add(new ListItem("CMV (Cytomegalovirus) IgG", "ANC021"));
            ddlServiceDesc.Items.Add(new ListItem("CMV (Cytomegalovirus) IgM", "ANC041"));
            ddlServiceDesc.Items.Add(new ListItem("Consultation (initial)", "ANC052"));
            ddlServiceDesc.Items.Add(new ListItem("Coombs test - INDIRECT", "ANC017"));
            ddlServiceDesc.Items.Add(new ListItem("Creatinine", "ANC053"));
            ddlServiceDesc.Items.Add(new ListItem("CTG (Cardiotocography)", "ANC020"));
            //ddlServiceDesc.Items.Add(new ListItem("Cyclogest","ANC088"));              
            //ddlServiceDesc.Items.Add(new ListItem("Daflon Tab","ANC089"));                        
            //ddlServiceDesc.Items.Add(new ListItem("Dexamethasone Inj","ANC090"));                        
            //ddlServiceDesc.Items.Add(new ListItem("Duphaston","ANC091"));             
            ddlServiceDesc.Items.Add(new ListItem("Fasting Blood Sugar (FBS)", "ANC054"));
            //ddlServiceDesc.Items.Add(new ListItem("Fefol Cap","ANC092"));              
            //ddlServiceDesc.Items.Add(new ListItem("Feroglobin","ANC093"));             
            //ddlServiceDesc.Items.Add(new ListItem("Ferosac Inj","ANC094"));                        
            //ddlServiceDesc.Items.Add(new ListItem("Ferose Tab","ANC095"));                       
            //ddlServiceDesc.Items.Add(new ListItem("Ferovit","ANC096"));                  
            ddlServiceDesc.Items.Add(new ListItem("Fetal Anomaly Scan", "ANC026"));
            //ddlServiceDesc.Items.Add(new ListItem("Folic Acid","ANC030"));   
            ddlServiceDesc.Items.Add(new ListItem("Follicle Stimulating Hormone", "ANC055"));

            ddlServiceDesc.Items.Add(new ListItem("G6PD", "ANC007"));
            ddlServiceDesc.Items.Add(new ListItem("Glucose Load", "ANC110"));
            ddlServiceDesc.Items.Add(new ListItem("Glucose Tolerance Test (GTT) 100 grams", "ANC064"));
            ddlServiceDesc.Items.Add(new ListItem("Glucose Tolerance Test (GTT) 50 grams", "ANC065"));
            ddlServiceDesc.Items.Add(new ListItem("Glycosylated Hemoglobin (HbA1c)", "ANC111"));
            ddlServiceDesc.Items.Add(new ListItem("Gonorrhea", "ANC056"));
            ddlServiceDesc.Items.Add(new ListItem("GTT (Glucose tolerance test)", "ANC016"));
            ddlServiceDesc.Items.Add(new ListItem("HbsAg", "ANC006"));
            ddlServiceDesc.Items.Add(new ListItem("Hemoglobin", "ANC023"));
            ddlServiceDesc.Items.Add(new ListItem("Hemoglobin Electrophoresis", "ANC057"));
            ddlServiceDesc.Items.Add(new ListItem("Hepatitis C", "ANC009"));
            ddlServiceDesc.Items.Add(new ListItem("Herpes Simplex Virus 1 (HSV1)", "ANC060"));
            ddlServiceDesc.Items.Add(new ListItem("Herpes Simplex Virus 2 (HSV2)", "ANC061"));

            ddlServiceDesc.Items.Add(new ListItem("High Vaginal Swab", "ANC058"));
            //ddlServiceDesc.Items.Add(new ListItem("Hip Natal","ANC097"));
            ddlServiceDesc.Items.Add(new ListItem("HIV Screening", "ANC059"));
            ddlServiceDesc.Items.Add(new ListItem("Human Papilloma Virus ", "ANC062"));
            //ddlServiceDesc.Items.Add(new ListItem("Iron Supplement","ANC031"));                
            //ddlServiceDesc.Items.Add(new ListItem("Juspirin","ANC109"));                  
            //ddlServiceDesc.Items.Add(new ListItem("Laxative","ANC040"));                
            ddlServiceDesc.Items.Add(new ListItem("Leuteinizing Hormone (LH)", "ANC063"));
            //ddlServiceDesc.Items.Add(new ListItem("Materna","ANC098"));                
            //ddlServiceDesc.Items.Add(new ListItem("Medication","ANC011"));                        
            //ddlServiceDesc.Items.Add(new ListItem("Momenta Cream","ANC099"));               
            //ddlServiceDesc.Items.Add(new ListItem("Multivitamin Supplement","ANC033"));                
            ddlServiceDesc.Items.Add(new ListItem("Normal Saline (IVF)", "ANC100"));
            //ddlServiceDesc.Items.Add(new ListItem("Osteocare","ANC101"));             
            //ddlServiceDesc.Items.Add(new ListItem("Paracetamol Tab","ANC102"));               
            ddlServiceDesc.Items.Add(new ListItem("Pelvimetry", "ANC019"));
            ddlServiceDesc.Items.Add(new ListItem("Post Prandial Blood Sugar", "ANC028"));
            ddlServiceDesc.Items.Add(new ListItem("Pregnancy Test", "ANC012"));
            //ddlServiceDesc.Items.Add(new ListItem("Proluton Inj","ANC103"));                       
            ddlServiceDesc.Items.Add(new ListItem("Random blood sugar (RBS)", "ANC003"));
            ddlServiceDesc.Items.Add(new ListItem("RPR (Rapid Plasma Reagin)", "ANC072"));

            ddlServiceDesc.Items.Add(new ListItem("Rubella Antibody IgG", "ANC005"));
            ddlServiceDesc.Items.Add(new ListItem("Rubella Antibody IgM", "ANC025"));
            //ddlServiceDesc.Items.Add(new ListItem("Seacal Tab","ANC104"));                        
            ddlServiceDesc.Items.Add(new ListItem("Serum Calcium", "ANC066"));
            ddlServiceDesc.Items.Add(new ListItem("Serum Iron", "ANC067"));
            ddlServiceDesc.Items.Add(new ListItem("Serum Prolactin", "ANC068"));
            ddlServiceDesc.Items.Add(new ListItem("SGOT (Serum Glutamic Oxaloacetic Transaminase)", "ANC069"));
            ddlServiceDesc.Items.Add(new ListItem("SGPT (Serum glutamic pyruvic transaminase)", "ANC015"));
            ddlServiceDesc.Items.Add(new ListItem("Sickle Cell", "ANC008"));
            ddlServiceDesc.Items.Add(new ListItem("Syphillis - (FTA - Abs) Screen", "ANC070"));

            //ddlServiceDesc.Items.Add(new ListItem("Tetanus Toxoid","ANC105"));                 
            ddlServiceDesc.Items.Add(new ListItem("Toxoplasmosis IgG", "ANC013"));
            ddlServiceDesc.Items.Add(new ListItem("Toxoplasmosis IgM", "ANC024"));
            ddlServiceDesc.Items.Add(new ListItem("TPHA", "ANC071"));
            ddlServiceDesc.Items.Add(new ListItem("Trichomonas Screen", "ANC074"));
            ddlServiceDesc.Items.Add(new ListItem("TSH (Thyroid Stimulating Hormone)", "ANC073"));
            //ddlServiceDesc.Items.Add(new ListItem("Tylenol Tab","ANC106"));                       
            ddlServiceDesc.Items.Add(new ListItem("Ultrasound (3 Dimension)", "ANC022"));
            ddlServiceDesc.Items.Add(new ListItem("Ultrasound (Pregnancy)", "ANC010"));
            ddlServiceDesc.Items.Add(new ListItem("Ultrasound (Transvaginal)", "ANC075"));
            ddlServiceDesc.Items.Add(new ListItem("Urine Analysis", "ANC002"));
            ddlServiceDesc.Items.Add(new ListItem("Urine Analysis (24 hours collection)", "ANC076"));
            ddlServiceDesc.Items.Add(new ListItem("Urine culture", "ANC014"));
            ddlServiceDesc.Items.Add(new ListItem("VDRL Screening ", "ANC078"));
            ddlServiceDesc.Items.Add(new ListItem("Vaginal Culture & Sensitivity", "ANC077"));

        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
            //ExceptionManager.LogException(ex);
        }
        return true;
    }
    public Boolean CheckIfExist(string SerDis)
    {

        DT = (DataTable)Session["dt"];

        for (int i = 0; i < DT.Rows.Count; i++)
        {

            if (SerDis == (string)DT.Rows[i][0])

                return false;

        }

        return true;

    }

    public void ProcessPregRequest()
    {

        DT = (DataTable)Session["dt"];
        int c = DT.Columns.Count - 1;
        int r = DT.Rows.Count - 1;
        string[,] str = new string[r + 1, c + 1];
        string strr = "";

        OS_DXC_WAP.CaesarWS.DiagnosisInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.DiagnosisInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.DiagnosisInfoResponse_DN response;

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        request.username = WebPublication.CaesarSvcUsername;
        request.password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();// TransactionID();// long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));

        ////if (Request.QueryString["PID"] != null && Request.QueryString["PID"].ToString() != "")
        if (!string.IsNullOrEmpty(PID))
        {
            ////request.preauthorisation_ID = long.Parse(Request.QueryString["PID"].ToString());
            request.preauthorisation_ID = long.Parse(PID);
            request.transactionType = "E";
        }


        if (ddlPharmacyDiag.SelectedValue != "Z39" && ddlPharmacyDiag.SelectedValue != "Z41.2" && ddlPharmacyDiag.SelectedValue != "Z41.3")
        {
            //CR179 New Parameters Start
            if (rdnMaternity.SelectedValue == "0")
            {
                request.IVF_Pregnancy = "Y";
                request.normal_Pregnancy = "N";
            }
            else if (rdnMaternity.SelectedValue == "1")
            {
                request.IVF_Pregnancy = "N";
                request.normal_Pregnancy = "Y";
            }

            bool blnResult;
            DateTime dtLastM;
            DateTime dtExpected;
            string[] dtFormats = { "dd/MM/yyyy", "d/M/yyyy", "dd/M/yyyy", "d/MM/yyyy" };
            blnResult = DateTime.TryParseExact(txtLastPeriodGreg.Text, dtFormats,
                        new CultureInfo("en-US"), DateTimeStyles.None, out dtLastM);
            if (blnResult)
            {
                request.last_Menstruation_Period = new DateTime(dtLastM.Year, dtLastM.Month, dtLastM.Day);
                //request.last_Menstruation_Period = Convert.ToDateTime("07/03/2015");
            }
            blnResult = DateTime.TryParseExact(txtExpectedDeliveryGreg.Text, dtFormats,
                       new CultureInfo("en-US"), DateTimeStyles.None, out dtExpected);
            if (blnResult)
            {
                request.expected_Delivery_Date = new DateTime(dtExpected.Year, dtExpected.Month, dtExpected.Day);
                //request.expected_Delivery_Date = Convert.ToDateTime("12/12/2015");
            }

            request.gravida = int.Parse(txtGravida.Text.Trim());
            request.para = int.Parse(txtPara.Text.Trim());
            request.live = int.Parse(txtLive.Text.Trim());
            request.abortion = int.Parse(txtAbortion.Text.Trim());
            request.death = int.Parse(txtDeath.Text.Trim());
        }

        request.treatmentType = ddlTreatmentType.SelectedValue; // txt_treatment_type.Text.Trim();
        request.departmentType = ddlDepartment.SelectedValue;
        request.providerFaxNo = strProviderFax; // txt_pv_fax.Text.Trim();
        request.providerCode = strProviderID;// "20087"; // txt_pv_code.Text.Trim();
        request.policyNo = txtPolicyNo.Text;// txt_pocy.Text.Trim();
        request.physician_Name = txtNameOfPhysician.Text;// txt_phy_name.Text.Trim();
        request.patientFileNo = txtPatientFileNo.Text; // txt_patientFile_no.Text.Trim();
        request.membershipNo = txtMembershipNo.Text;// txt_mbr.Text.Trim();
        request.memberName = txtMemberName.Text;// txt_mbr_name.Text.Trim();
        request.memberMobileNo = txtMobileNo.Text; // txt_mobile.Text.Trim();
        request.memberID_Igama = txtMemberID.Text;// txt_mbr_igm.Text.Trim();
        request.gender = ddlGender.Text; //.SelectedValue; // txt_gender.Text.Trim();
        if (ViewState["TrustedDoctor"] != null)
        {
            request.trustedDoctor = Convert.ToBoolean(ViewState["TrustedDoctor"].ToString()) == true ? "Y" : "N";
        }
        if (txtCardIssueNo.Text != "") request.cardIssueNumber = int.Parse(txtCardIssueNo.Text);
        request.age = int.Parse(txtAge.Text);
        if (txtSumCost.Text != "") request.estimatedAmount = Convert.ToDouble(txtSumCost.Text);
        if (txtSumQuantity.Text != "") request.quantity = long.Parse(txtSumQuantity.Text);

        request.bloodPressure = txtBloodPressure.Text; // BloodPressure.Trim();
        if (chkCheckup.Checked == true) request.checkUp = "1";
        //request.checkUp = chkCheckup.Text; // CheckUp.Trim();
        if (chkChronic.Checked == true) request.chronic = "1";
        //request.chronic = chkCheckup.Text.Trim();
        if (chkCongenital.Checked == true) request.congenital = "1";
        //request.congenital = chkCongenital.Text.Trim();
        if (txtPulse.Text.Trim().Length > 0) request.pulse = double.Parse(txtPulse.Text.Trim());
        if (chkPsychiatric.Checked == true) request.psychiatric = "1";
        //request.psychiatric = chkPsychiatric.Text.Trim();
        if (chkRTA.Checked == true) request.RTA = "1";
        request.dateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); //
        //request.dateOfAdmission = DateTime.ParseExact("07/03/2015", "dd/MM/yyyy", null);
        request.durationOfIllness = txtDurationOfIllness.Text.Trim();
        //request.estimatedAmount = double.Parse( EstimatedAmount.Trim());
        if (chkInfertility.Checked == true) request.infertility = "1";
        //request.infertility = Infertility.Trim();
        if (txtLastMenstruationPeriod.Text.Trim().Length > 0)
        {
            if (txtLastMenstruationPeriod.Text.ToString().Length > 11)
                request.lastMenstruationPeriod = DateTime.ParseExact(txtLastMenstruationPeriod.Text.Substring(0, 10), "dd/MM/yyyy", null);
            else
                request.lastMenstruationPeriod = DateTime.ParseExact(txtLastMenstruationPeriod.Text, "dd/MM/yyyy", null);
        }
        request.otherConditions = txtOtherConditions.Text.Trim();
        if (txtLengthOfStay.Text.Trim().Length > 0) request.lengthOfStay = Int32.Parse(txtLengthOfStay.Text.Trim());
        //request.quantity = Int64.Parse(Quantity.Trim());

        request.possibleLineOfTreatment = txtPossibleLineOfTreatment.Text.Trim();
        if (chkVaccination.Checked == true) request.vaccination = "1";
        if (txtTemperature.Text.Trim().Length > 0) request.temperature = double.Parse(txtTemperature.Text.Trim());

        if (chkWorkRelated.Checked == true) request.workRelated = "1";

        string strServDesc = txtServiceDesc.Text;
        string strPeriod = ddlPeriod.SelectedValue;
        string strDateFrom = DateFrom.Text;// CalendarDateString;
        string strDateTo = txtDateTo.Text;
        string strQuantity = txtQuantity.Text;
        string strCost = txtCost.Text;
        string strServiceCode = "";
        string strServiceType = "";
        string strteethMap = txtTeethMap.Value;
        string strlensType = ddlLensType.SelectedValue; // TxtLensType.Text;
        string code;
        string BenefitHead;


        #region "StdPreg"
        if (ddlPharmacyDiag.SelectedValue == "???.?")
            request.diagnosisDescription = txtOthers.Text;
        else
            request.diagnosisDescription = ddlPharmacyDiag.SelectedItem.ToString();// txtDiagnosisDescription.Text;


        //request.diagnosisDescription = txtDiagnosisDescription.Text;
        request.diagnosisCode = ddlPharmacyDiag.SelectedValue; // txtDiagCode.Text;
        request.chiefComplaintsAndMainSymptoms = txtChiefComplaintsAndMainSymptoms.Text;
        if (txtLengthOfStay.Text.Trim() != "")
            request.lengthOfStay = Int32.Parse(txtLengthOfStay.Text.Trim());
        request.dateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); // txtDateOfVisit.CalendarDate;
        //request.dateOfAdmission = DateTime.ParseExact("07/03/2015", "dd/MM/yyyy", null); 

        String[] detail_ServiceDescription = new String[DT.Rows.Count];
        String[] detail_ServiceCode = new String[DT.Rows.Count];
        String[] detail_ServiceType = new String[DT.Rows.Count];
        Int64[] detail_Quantity = new Int64[DT.Rows.Count];
        int[] detail_ItemNo = new int[DT.Rows.Count];
        String[] detail_BenefitHead = new String[DT.Rows.Count];
        Double[] detail_EstimatedCost = new double[DT.Rows.Count];
        DateTime[] detail_SupplyDateFrom = new DateTime[DT.Rows.Count]; // String2DateTime(new String[] { txt_supply_dateFm1.Text.Trim(), txt_supply_dateFm2.Text.Trim() });
        DateTime[] detail_SupplyDateTo = new DateTime[DT.Rows.Count]; // String2DateTime(new String[] { txt_supply_dateTo1.Text.Trim(), txt_supply_dateTo2.Text.Trim() });
        String[] detail_SupplyPeriod = new String[DT.Rows.Count];// cutoffInvalidStr(new String[] { txt_supply_period1.Text.Trim(), txt_supply_period2.Text.Trim() });
        for (int i = 0; i <= r; i++)
        {
            if (ddlPharmacyDiag.SelectedValue != "???.?")
            {
                strServiceCode = (String)DT.Rows[i][9];
                // request.diagnosisDescription = txtOthers.Text;

            }



            strServDesc = (String)DT.Rows[i][0];
            strPeriod = (String)DT.Rows[i][1];
            strQuantity = (String)DT.Rows[i][4];
            strCost = (String)DT.Rows[i][5];
            BenefitHead = (String)DT.Rows[i][10];

            if (DT.Rows[i][9].ToString().Length > 0)
                strServiceCode = (String)DT.Rows[i][9];

            if (DT.Rows[i][7].ToString().Length > 0)
                detail_ServiceType.SetValue("ME", i); // strServiceType = "ME";

            detail_ServiceDescription.SetValue(strServDesc, i);
            detail_ServiceCode.SetValue(strServiceCode, i);
            //detail_RegularLensesType.SetValue(strlensType, i);

            detail_Quantity.SetValue(Int64.Parse(strQuantity), i);
            detail_ItemNo.SetValue(i + 1, i);
            detail_EstimatedCost.SetValue(Double.Parse(strCost), i);

            detail_BenefitHead.SetValue(BenefitHead, i);
        }

        request.detail_BenefitHead = detail_BenefitHead;
        request.detail_EstimatedCost = detail_EstimatedCost;
        request.detail_ItemNo = detail_ItemNo;
        request.detail_Quantity = detail_Quantity;
        request.detail_ServiceCode = detail_ServiceCode;
        request.detail_ServiceType = detail_ServiceType;
        request.detail_ServiceDescription = detail_ServiceDescription;
        request.detail_SupplyDateFrom = detail_SupplyDateFrom;
        request.detail_SupplyDateTo = detail_SupplyDateTo;
        //////string  supplyDateFrom  = string.Empty;
        //////supplyDateFrom = Convert.ToString(detail_SupplyDateFrom[0]) ;
        //////string  supplyDateTo  = string.Empty;
        //////supplyDateFrom = Convert.ToString(detail_SupplyDateTo[0]) ;

        //////DateTime objSupplyDateFrom = !string.IsNullOrEmpty(supplyDateFrom) ? supplyDateFrom.Contains("0001") ? Convert.ToDateTime("01/01/2015") : detail_SupplyDateFrom[0] : Convert.ToDateTime("01/01/2015");
        //////DateTime objSupplyDateTo = !string.IsNullOrEmpty(supplyDateFrom) ? supplyDateFrom.Contains("0001") ? Convert.ToDateTime("01/01/2015") : detail_SupplyDateTo[0] : Convert.ToDateTime("01/01/2015");

        //////for (int count = 0; count < detail_SupplyDateFrom.Length; count++ )
        //////{
        //////    detail_SupplyDateFrom[count] = objSupplyDateFrom;
        //////    detail_SupplyDateTo[count] = objSupplyDateTo;
        //////}

        //////request.detail_SupplyDateFrom =  detail_SupplyDateFrom;        
        //////request.detail_SupplyDateTo = detail_SupplyDateTo;


        ////request.detail_SupplyDateFrom[0] = 
        ////request.detail_SupplyDateTo = detail_SupplyDateTo;
        request.detail_SupplyPeriod = detail_SupplyPeriod;
        ////request.detail = preauthSupportDocuments(uploadSession.Text, _uploadCategory, request.transactionID);
        request.detail = preauthSupportDocuments(Convert.ToString(Session["UploadSession"]), _uploadCategory, request.transactionID);


        
        ///request.transactionType
        request.exempted = lblval.Text;
        if (chk.Visible == true)
        {
            if (chk.Checked)
            {
                request.referral = "Y";
            }
            else
            {
                request.referral = "N";
            }
        }

        try
        {

            StringBuilder sb = new StringBuilder(200);

            //ExceptionManager.TrackWSPerformance("Preauth13_3", "ReqDiagInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(),  strProviderID, "", "");
            response = ws.ReqDiagInfo(request);
            //ExceptionManager.TrackWSPerformance("Preauth13_3", "ReqDiagInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), strProviderID, "", "");


            ////Convert object to Xml 
            ////If webservice is not responding fine, then send the request & response xml to Caesar team 
            ////Below code need to enable, it will saving the request & rsponse xml in specifeied path
            ////Strat
            ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DiagnosisInfoRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DiagnosisInfoRequest_DN>();
            ////XmlReq.Request(request, "DiagnosisInfo_Request");

            ////ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DiagnosisInfoResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DiagnosisInfoResponse_DN>();
            ////XmlResp.Response(response, "DiagnosisInfo_Response");
            ////End

            //  ddlServiceDesc.SelectedValue ddlPharmacyDiag.SelectedValue
            if (response.status != "0")
            {
                if (response.ext_Ind == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "$.modal.close();');");
                    litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("\n");

                //txtBox_output.Text 
                Message1.Visible = true;
                Message1.Text = sb.ToString();
            }
            else
            {

                if (ddlPharmacyDiag.SelectedValue == "???.?")
                {
                    var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR";
                    queryString = Cryption.Encrypt(queryString);
                    if (response.ext_Ind == "Y")
                    {
                        
                       
                        aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?val=" + queryString+"');");
                        ////aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR');");
                        litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                    }
                    else
                    {
                        ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR", false);
                        Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
                    }
                    //Message1.Text = "Reference No: <b>" + response.preAuthorizationID.ToString() + "</b>  <a href='CheckNewReply.aspx'>Check Reply</a>";
                    updatePanel.Visible = false;
                    Panel1.Visible = false;
                    PnlVerify.Visible = false;
                    UPnlDiagnosis.Visible = false;
                    UPnlTreatmentDets.Visible = false;
                }
                else
                {

                    if (ddlPharmacyDiag.SelectedValue != "???.?")
                    {
                        // Send the email
                        string provider = Session["ProviderName"].ToString();
                        string username = Session["ProviderUserName"].ToString();

                        var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR";
                        queryString = Cryption.Encrypt(queryString);

                        Notify(provider, username, response.preAuthorizationID.ToString());
                        if (response.ext_Ind == "Y")
                        {
                            ////aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR');");
                            aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?val=" + queryString + "');");
                            litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                        }
                        else
                        {
                            Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
                            ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR", false);
                        }

                        Message1.Text = "Success";
                    }
                    else
                    {
                        // Send the email
                        string provider = Session["ProviderName"].ToString();
                        string username = Session["ProviderUserName"].ToString();

                        Notify(provider, username, response.preAuthorizationID.ToString());                       

                        if (response.ext_Ind == "Y")
                        {
                            aCloseOverlay.Attributes.Add("onclick", "$.modal.close();");
                            litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                        }
                        Message1.Text = "Reference No: <b>" + response.preAuthorizationID.ToString() + "</b>  <a href='CheckNewReply.aspx'>Check Reply</a>";
                        updatePanel.Visible = false;
                        Panel1.Visible = false;
                        PnlVerify.Visible = false;
                        UPnlDiagnosis.Visible = false;
                        UPnlTreatmentDets.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {

            Message1.Text += "\n" + ex.Message;
            //ExceptionManager.LogException(ex);
        }

        # endregion

    }

    public void LoadFirstRows(int i)
    {
        DataRow dr = DT.NewRow();
        dr[0] = "1";
        dr[1] = "2";
        dr[2] = "01/01/2000";
        dr[3] = "3";
        dr[4] = "4";
        dr[5] = "5";
        dr[6] = "p001";
        dr[7] = "teeth";
        dr[8] = "Lens";
        dr[9] = "Code";
        DT.Rows.Add(dr);
    }

    public void ProcessDentalRequest()
    {

        DT = (DataTable)Session["dt"];
        int c = DT.Columns.Count - 1;
        int r = DT.Rows.Count - 1;
        string[,] str = new string[r + 1, c + 1];
        string strr = "";

        OS_DXC_WAP.CaesarWS.DentalInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.DentalInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.DentalInfoResponse_DN response;

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.TransactionID = TransactionManager.TransactionID();// TransactionID();// long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));

        request.TreatmentType = ddlTreatmentType.SelectedValue; // txt_treatment_type.Text.Trim();

        request.ProviderFaxNo = strProviderFax;// txt_pv_fax.Text.Trim();
        request.ProviderCode = strProviderID;// "20087"; // txt_pv_code.Text.Trim();
        request.PolicyNo = txtPolicyNo.Text;// txt_pocy.Text.Trim();
        request.Physician_Name = txtNameOfPhysician.Text;// txt_phy_name.Text.Trim();
        request.PatientFileNo = txtPatientFileNo.Text; // txt_patientFile_no.Text.Trim();
        request.MembershipNo = txtMembershipNo.Text;// txt_mbr.Text.Trim();
        request.MemberName = txtMemberName.Text;// txt_mbr_name.Text.Trim();
        request.MemberMobileNo = txtMobileNo.Text; // txt_mobile.Text.Trim();
        request.MemberID_Igama = txtMemberID.Text;// txt_mbr_igm.Text.Trim();
        request.Gender = ddlGender.Text; //.SelectedValue; // txt_gender.Text.Trim();
        request.DepartmentType = "DEN";
        if (ViewState["TrustedDoctor"] != null)
        {
            request.trustedDoctor = Convert.ToBoolean(ViewState["TrustedDoctor"].ToString()) == true ? "Y" : "N";
        }

        if (txtCardIssueNo.Text != "") request.CardIssueNumber = int.Parse(txtCardIssueNo.Text);
        request.Age = int.Parse(txtAge.Text);
        request.VerificationID = 0;
        // request.DepartmentType = "General";
        if (txtSumCost.Text != "") request.EstimatedAmount = Convert.ToDouble(txtSumCost.Text);
        if (txtSumQuantity.Text != "") request.Quantity = long.Parse(txtSumQuantity.Text);

        string strServDesc = txtServiceDesc.Text;
        string strPeriod = ddlPeriod.SelectedValue;
        string strDateFrom = DateFrom.Text;// CalendarDateString;
        string strDateTo = txtDateTo.Text;
        string strQuantity = txtQuantity.Text;
        string strCost = txtCost.Text;
        string strteethMap = txtTeethMap.Value;
        string strlensType = ddlLensType.SelectedValue; // TxtLensType.Text;
        string ServiceCode;
        string ServiceType = "";


        //bool isOldToothMap = false;
        //try
        //{
        //    DateTime fromDate;
        //    DateTime effectiveDate;
        //    fromDate = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null);
        //    effectiveDate = DateTime.ParseExact("01/10/2018", "dd/MM/yyyy", null);
        //    int dayDiff = (int)((fromDate - effectiveDate).TotalDays);
        //    isOldToothMap = dayDiff < 0;
        //}
        //catch (Exception)
        //{
        //}


        #region
        request.DiagnosisDescription = txtDiagnosisDescription.Text;
        ////CR Request by business on 20-Aug-2017 enabled by sakthi
        ////Start
        ////request.DiagnosisCode = txtDiagCode.Text;
        request.DiagnosisCode = BLCommon.HRMEnabled ? string.Empty : "K02";//// CAESAR expecting connstant value for non hrm, the constant value is K02 
        ////End
        request.ChiefComplaintsAndMainSymptoms = txtChiefComplaintsAndMainSymptoms.Text;
        request.DateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); // txtDateOfVisit.CalendarDate;

        if (chkCheckup.Checked == true) request.CheckUp = "1";
        if (chkCleaning.Checked == true) request.Cleaning = "1";
        if (chkCongenital.Checked == true) request.CongenitalDevelopmental = "1";
        if (chkOrthodontic.Checked == true) request.Orthodontics = "1";
        if (chkRTA.Checked == true) request.RTA = "1";
        if (chkWorkRelated.Checked == true) request.WorkRelatedAccident = "1";

        int[] Detail_ItemNo = new int[DT.Rows.Count];
        String[] Detail_BenefitHead = new String[DT.Rows.Count];
        String[] Detail_ServiceCode = new String[DT.Rows.Count];
        String[] Detail_ServiceType = new String[DT.Rows.Count];
        String[] Detail_ServiceDescription = new String[DT.Rows.Count];
        String[] Detail_ToothNo = new String[DT.Rows.Count];
        Int64[] Detail_Quantity = new Int64[DT.Rows.Count];
        Double[] Detail_EstimatedCost = new Double[DT.Rows.Count];

        //// CR 436 DCAF Tooth number changes
        //// Start
        String[] Detail_ToothNo_FDI = new String[DT.Rows.Count];
        //// End



        for (int i = 0; i <= r; i++)
        {
            strServDesc = (String)DT.Rows[i][0];
            strPeriod = (String)DT.Rows[i][1];
            strDateFrom = (String)DT.Rows[i][2];
            strDateTo = (String)DT.Rows[i][3];
            strQuantity = (String)DT.Rows[i][4];
            strCost = (String)DT.Rows[i][5];
            ServiceCode = (String)DT.Rows[i][6];
            strteethMap = (String)DT.Rows[i][7];
            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["HRMEnabled"]))
            if (BLCommon.HRMEnabled)
            {
                Detail_BenefitHead.SetValue("", i);
            }
            else{
            if (ServiceCode.StartsWith("D"))
            {
                ServiceType = "";  //ServiceCode = (String)DT.Rows[i][6];
                Detail_BenefitHead.SetValue(ServiceCode, i);
            }
            else
            {
                ServiceType = "ME"; // (String)DT.Rows[i][9];
                Detail_BenefitHead.SetValue("D16", i);
            }
            }

            Detail_ServiceDescription.SetValue(strServDesc, i);
            Detail_ServiceCode.SetValue(ServiceCode, i);
            Detail_ServiceType.SetValue(ServiceType, i);
            //// CR 436 DCAF Tooth number changes
            //// Start
            ////Detail_ToothNo.SetValue(strteethMap, i);
            //if (isOldToothMap)
            //{
            //    Detail_ToothNo.SetValue(GetOldToothNumber(strteethMap), i);
            //    Detail_ToothNo_FDI.SetValue(string.Empty, i);
            //}
            //else
            //{
            //    Detail_ToothNo.SetValue(string.Empty, i);
            //    Detail_ToothNo_FDI.SetValue(strteethMap, i);
            //}         
            Detail_ToothNo.SetValue(string.Empty, i);
            Detail_ToothNo_FDI.SetValue(strteethMap, i);
            //// End
            Detail_Quantity.SetValue(Int64.Parse(strQuantity), i);
            Detail_ItemNo.SetValue(i + 1, i);
            Detail_EstimatedCost.SetValue(Double.Parse(strCost), i);
        }

        request.detail_BenefitHead = Detail_BenefitHead;
        request.detail_EstimatedCost = Detail_EstimatedCost;
        request.detail_ItemNo = Detail_ItemNo;
        request.detail_Quantity = Detail_Quantity;
        request.detail_ServiceCode = Detail_ServiceCode;
        request.detail_ServiceType = Detail_ServiceType;
        request.detail_ServiceDescription = Detail_ServiceDescription;
        request.detail_ToothNo = Detail_ToothNo;
        //// CR 436 DCAF Tooth number changes
        //// Start  
        request.detail_ToothNo_FDI = Detail_ToothNo_FDI;
        //// End

        ////request.detail = preauthSupportDocuments(uploadSession.Text, _uploadCategory, request.TransactionID);
        request.detail = preauthSupportDocuments(Convert.ToString(Session["UploadSession"]), _uploadCategory, request.TransactionID);
        try
        {
            StringBuilder sb = new StringBuilder(200);
          
            //Strat
            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DentalInfoRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DentalInfoRequest_DN>();
            //XmlReq.Request(request, "DentalInfoRequest");

            response = ws.ReqDenInfo(request);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DentalInfoResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DentalInfoResponse_DN>();
            //XmlResp.Response(response, "DentalInfoResponse");
            //End

            if (response.Status != "0")
            {
                if (response.ext_Ind == "Y")
                {   
                    aCloseOverlay.Attributes.Add("onclick", "$.modal.close();');");
                    litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("\n");
                lblNoTreatmentDetError.Text = sb.ToString();
                Message1.Text = sb.ToString();
            }
            else
            {
                // Send the email
                string provider = Session["ProviderName"].ToString();
                string username = Session["ProviderUserName"].ToString();


                Notify(provider, username, response.PreAuthorizationID.ToString());
                var queryString = "PID=" + response.PreAuthorizationID + "&PC=" + request.ProviderCode + "&RType=NR" ;
                queryString = Cryption.Encrypt(queryString);

                Message1.Text = "Success";
                if (response.ext_Ind == "Y")
                {
                    ////aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?PID=" + response.PreAuthorizationID + "&PC=" + request.ProviderCode.Trim() + "&RType=NR');");
                    aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?val=" + queryString + "');");
                    litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }
                else
                {
                    ///Response.Redirect("PreauthDetails.aspx?PID=" + response.PreAuthorizationID + "&PC=" + request.ProviderCode.Trim() + "&RType=NR", false);
                    Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
                }
            }
        }
        catch (Exception ex)
        {

            Message1.Text += "\n" + ex.Message;
            //ExceptionManager.LogException(ex);
        }
        # endregion
    }

    public void ProcessStdnPregRequest()
    {

        DT = (DataTable)Session["dt"];
        int c = DT.Columns.Count - 1;
        int r = DT.Rows.Count - 1;
        string[,] str = new string[r + 1, c + 1];
        string strr = "";

        OS_DXC_WAP.CaesarWS.DiagnosisInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.DiagnosisInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.DiagnosisInfoResponse_DN response;

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        request.username = WebPublication.CaesarSvcUsername;
        request.password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();// TransactionID();// long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));

        ////if (Request.QueryString["PID"] != null && Request.QueryString["PID"].ToString() != "")
        if (!string.IsNullOrEmpty(PID))
        {
            ////request.preauthorisation_ID = long.Parse(Request.QueryString["PID"].ToString());
            request.preauthorisation_ID = long.Parse(PID);
            request.transactionType = "E";
        }
        request.treatmentType = ddlTreatmentType.SelectedValue; // txt_treatment_type.Text.Trim();
        ////if (Request.QueryString["PID"] == null) request.departmentType = ddlDepartment.SelectedValue;
        if (string.IsNullOrEmpty(PID)) request.departmentType = ddlDepartment.SelectedValue;
        request.providerFaxNo = strProviderFax;// txt_pv_fax.Text.Trim();
        request.providerCode = strProviderID;// "20087"; // txt_pv_code.Text.Trim();
        request.policyNo = txtPolicyNo.Text;// txt_pocy.Text.Trim();
        request.physician_Name = txtNameOfPhysician.Text;// txt_phy_name.Text.Trim();
        request.patientFileNo = txtPatientFileNo.Text; // txt_patientFile_no.Text.Trim();
        request.membershipNo = txtMembershipNo.Text;// txt_mbr.Text.Trim();
        request.memberName = txtMemberName.Text;// txt_mbr_name.Text.Trim();
        request.memberMobileNo = txtMobileNo.Text.Trim(); // txt_mobile.Text.Trim();
        request.memberID_Igama = txtMemberID.Text;// txt_mbr_igm.Text.Trim();
        request.gender = ddlGender.Text; //.SelectedValue; // txt_gender.Text.Trim();
        request.exempted = lblval.Text;
        if (ViewState["TrustedDoctor"] != null)
        {
            request.trustedDoctor = Convert.ToBoolean(ViewState["TrustedDoctor"].ToString()) == true ? "Y" : "N";
        }
        if (txtCardIssueNo.Text != "") request.cardIssueNumber = int.Parse(txtCardIssueNo.Text);
        request.age = int.Parse(txtAge.Text);

        if (chk.Visible == true)
        {
            if (chk.Checked)
            {
                request.referral = "Y";
            }
            else
            {
                request.referral = "N";
            }
        }

        if (txtSumCost.Text != "") request.estimatedAmount = Convert.ToDouble(txtSumCost.Text);
        if (txtSumQuantity.Text != "") request.quantity = long.Parse(txtSumQuantity.Text);

        request.bloodPressure = txtBloodPressure.Text; // BloodPressure.Trim();
        if (chkCheckup.Checked == true) request.checkUp = "1";
        if (chkChronic.Checked == true) request.chronic = "1";
        if (chkCongenital.Checked == true) request.congenital = "1";
        if (txtPulse.Text.Trim().Length > 0) request.pulse = double.Parse(txtPulse.Text.Trim());
        if (chkPsychiatric.Checked == true) request.psychiatric = "1";
        if (chkRTA.Checked == true) request.RTA = "1";
        request.dateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); // txtDateOfVisit.CalendarDate; // DateTime.ParseExact(txtDateOfVisit.CalendarDate, "dd/MM/yyyy HH:mm:ss", null);
        request.durationOfIllness = txtDurationOfIllness.Text.Trim();
        if (chkInfertility.Checked == true) request.infertility = "1";
        if (txtLastMenstruationPeriod.Text.Trim().Length > 0)
        {
            request.lastMenstruationPeriod = DateTime.ParseExact(txtLastMenstruationPeriod.Text, "dd/MM/yyyy", null);
        }
        request.otherConditions = txtOtherConditions.Text.Trim();
        if (txtLengthOfStay.Text.Trim().Length > 0) request.lengthOfStay = Int32.Parse(txtLengthOfStay.Text.Trim());

        request.possibleLineOfTreatment = txtPossibleLineOfTreatment.Text.Trim();
        if (chkVaccination.Checked == true) request.vaccination = "1";
        if (txtTemperature.Text.Trim().Length > 0) request.temperature = double.Parse(txtTemperature.Text.Trim());

        if (chkWorkRelated.Checked == true) request.workRelated = "1";

        string strServDesc = txtServiceDesc.Text;
        string strPeriod = ddlPeriod.SelectedValue;
        string strDateFrom = DateFrom.Text;// CalendarDateString;
        string strDateTo = txtDateTo.Text;
        string strQuantity = txtQuantity.Text;
        string strCost = txtCost.Text;
        string strServiceCode = "";
        string strServiceType = "";
        string strteethMap = txtTeethMap.Value;
        string strlensType = ddlLensType.SelectedValue; // TxtLensType.Text;
        string code;
        string strBenefitHead;
        double FinalTotalAmount = 0;
        long FinalTotalQuantity = 0;

        #region "StdPreg"

        if (txtMinor.Text == "true")
        {
            request.diagnosisDescription = ddlPharmacyDiag.SelectedItem.ToString();// txtDiagnosisDescription.Text;
            request.diagnosisCode = ddlPharmacyDiag.SelectedValue.ToString(); //txtDiagCode.Text;
        }
        else
        {
            request.diagnosisDescription = txtDiagnosisDescription.Text;
            if (!string.IsNullOrEmpty(txtDiagCode.Text.Trim()))
            {
                request.diagnosisCode = txtDiagCode.Text.Trim();
            }
            else
            {
                request.diagnosisCode = txtCIDCode.Text.Trim();
            }



        }

        request.chiefComplaintsAndMainSymptoms = txtChiefComplaintsAndMainSymptoms.Text;
        if (txtLengthOfStay.Text.Trim() != "")
            request.lengthOfStay = Int32.Parse(txtLengthOfStay.Text.Trim());
        request.dateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); // txtDateOfVisit.CalendarDate;

        String[] detail_ServiceDescription = new String[DT.Rows.Count];
        String[] detail_ServiceCode = new String[DT.Rows.Count];
        String[] detail_ServiceType = new String[DT.Rows.Count];
        Int64[] detail_Quantity = new Int64[DT.Rows.Count];
        int[] detail_ItemNo = new int[DT.Rows.Count];
        String[] detail_BenefitHead = new String[DT.Rows.Count];
        Double[] detail_EstimatedCost = new double[DT.Rows.Count];
        DateTime[] detail_SupplyDateFrom = new DateTime[DT.Rows.Count]; // String2DateTime(new String[] { txt_supply_dateFm1.Text.Trim(), txt_supply_dateFm2.Text.Trim() });
        DateTime[] detail_SupplyDateTo = new DateTime[DT.Rows.Count]; // String2DateTime(new String[] { txt_supply_dateTo1.Text.Trim(), txt_supply_dateTo2.Text.Trim() });
        String[] detail_SupplyPeriod = new String[DT.Rows.Count];// cutoffInvalidStr(new String[] { txt_supply_period1.Text.Trim(), txt_supply_period2.Text.Trim() });

        for (int i = 0; i <= r; i++)
        {

            if (ddlPharmacyDiag.SelectedValue == "???.?" && txtMinor.Text == "true")
            {
                //  strServiceCode = (String)DT.Rows[i][9];
                request.diagnosisDescription = txtOthers.Text;
            }

            strServDesc = (String)DT.Rows[i][0];
            strPeriod = (String)DT.Rows[i][1];
            strDateFrom = (String)DT.Rows[i][2];
            strDateTo = (String)DT.Rows[i][3];
            strQuantity = (String)DT.Rows[i][4];
            strCost = (String)DT.Rows[i][5];

            if (DT.Rows[i][10].ToString().Length > 0)
                strBenefitHead = (String)DT.Rows[i][10];
            else
                strBenefitHead = "S1";

            if (DT.Rows[i][9].ToString().Length > 0)
                strServiceCode = (String)DT.Rows[i][9];
            detail_ServiceDescription.SetValue(strServDesc, i);
            detail_ServiceCode.SetValue(strServiceCode, i);
            detail_ServiceType.SetValue(strServiceType, i);
            detail_Quantity.SetValue(Int64.Parse(strQuantity), i);
            detail_ItemNo.SetValue(i + 1, i);
            detail_EstimatedCost.SetValue(Double.Parse(strCost), i);

            detail_BenefitHead.SetValue(strBenefitHead, i);

            FinalTotalAmount += Double.Parse(strCost);
            FinalTotalQuantity += long.Parse(strQuantity);
        }

        request.estimatedAmount = FinalTotalAmount;
        request.quantity = FinalTotalQuantity;
        request.detail_BenefitHead = detail_BenefitHead;
        request.detail_EstimatedCost = detail_EstimatedCost;
        request.detail_ItemNo = detail_ItemNo;
        request.detail_Quantity = detail_Quantity;
        request.detail_ServiceCode = detail_ServiceCode;
        request.detail_ServiceType = detail_ServiceType;
        request.detail_ServiceDescription = detail_ServiceDescription;
        request.detail_SupplyDateFrom = detail_SupplyDateFrom;
        request.detail_SupplyDateTo = detail_SupplyDateTo;
        request.detail_SupplyPeriod = detail_SupplyPeriod;
        ///request.detail = preauthSupportDocuments(uploadSession.Text, _uploadCategory, request.transactionID);
        request.detail = preauthSupportDocuments(Convert.ToString(Session["UploadSession"]), _uploadCategory, request.transactionID);

        try
        {

            StringBuilder sb = new StringBuilder(200);

            //ExceptionManager.TrackWSPerformance("Preauth13_3", "ReqDiagInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), strProviderID, "", "");
            response = ws.ReqDiagInfo(request);
            //ExceptionManager.TrackWSPerformance("Preauth13_3", "ReqDiagInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), strProviderID, "",  "");

            if (response.status != "0")
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("\n");

                //txtBox_output.Text 
                Message1.Visible = true;
                Message1.Text = sb.ToString();
                UPnlTreatmentDets.Visible = false;
                UPnlDiagnosis.Visible = false;
                Panel1.Visible = false;
                lblNoTreatmentDetError.Text = sb.ToString();
                lblNoTreatmentDetError.Visible = true;
            }
            else
            {
                // Send the email
                string provider = Session["ProviderName"].ToString();
                string username = Session["ProviderUserName"].ToString();

                Notify(provider, username, response.preAuthorizationID.ToString());

                ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR", false);
                var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR";
                queryString = Cryption.Encrypt(queryString);
                Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
                Message1.Text = "Success";
            }
        }
        catch (Exception ex)
        {

            Message1.Text += "\n" + ex.Message;
            //ExceptionManager.LogException(ex);
        }
        # endregion
    }
    public static DataTable RemoveNullOrEmptyRows(DataTable dt)
    {
        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        {
            if (String.IsNullOrEmpty(dt.Rows[i]["OptionCode"].ToString()) || String.IsNullOrEmpty(dt.Rows[i]["OptionValue"].ToString()))
                dt.Rows[i].Delete();
        }
        return dt;
    }

    public void ProcessStdnPreAuthDiagnosisRequest()
    {
        try
        {

            DTOptions = (DataTable)Session["dtOptions"];
            DTOptions = RemoveNullOrEmptyRows(DTOptions);


            DT = (DataTable)Session["dt"];
            int c = DT.Columns.Count - 1;
            int r = DT.Rows.Count - 1;
            int OptionRows = DTOptions.Rows.Count - 1;
            string[,] str = new string[r + 1, c + 1];
            string strr = "";

            ServiceDepot_DNService ws;
            //ReqPharInfo Define
            DiagnosisInfoRequest_DN request;
            DiagnosisInfoResponse_DN response;

            AllExemptDetail_DN[] allexemptdtl = new AllExemptDetail_DN[0];

            request = new DiagnosisInfoRequest_DN();
            ws = new ServiceDepot_DNService();

            //Web Standard(Included Maternity and VAC) Master Data
            ln_txnid = TransactionManager.TransactionID();
            request.transactionID = ln_txnid;

            ////if (Request.QueryString["PID"] != null && Request.QueryString["PID"].ToString() != "")
            if (!string.IsNullOrEmpty(PID))
            {
                ////request.preauthorisation_ID = long.Parse(Request.QueryString["PID"].ToString());
                request.preauthorisation_ID = long.Parse(PID);
                request.transactionType = "E";
            }
            else
            {
                request.transactionType = "N";
            }
            //request.preauthorisation_ID = 0;


            ////request.detail = preauthSupportDocuments(uploadSession.Text, _uploadCategory, request.transactionID);
            request.detail = preauthSupportDocuments(Convert.ToString(Session["UploadSession"]), _uploadCategory, request.transactionID);

            request.treatmentType = ddlTreatmentType.SelectedValue; // txt_treatment_type.Text.Trim();
            ////if (Request.QueryString["PID"] == null) request.departmentType = ddlDepartment.SelectedValue;
            if (string.IsNullOrEmpty(PID)) request.departmentType = ddlDepartment.SelectedValue;
            request.providerFaxNo = strProviderFax;// txt_pv_fax.Text.Trim();
            request.providerCode = strProviderID;// "20087"; // txt_pv_code.Text.Trim();
            request.policyNo = txtPolicyNo.Text;// txt_pocy.Text.Trim();
            request.physician_Name = txtNameOfPhysician.Text;// txt_phy_name.Text.Trim();
            request.patientFileNo = txtPatientFileNo.Text; // txt_patientFile_no.Text.Trim();
            request.membershipNo = txtMembershipNo.Text;// txt_mbr.Text.Trim();
            request.memberName = txtMemberName.Text;// txt_mbr_name.Text.Trim();
            request.memberMobileNo = txtMobileNo.Text.Trim(); // txt_mobile.Text.Trim();
            request.memberID_Igama = txtMemberID.Text;// txt_mbr_igm.Text.Trim();
            request.gender = ddlGender.Text; //.SelectedValue; // txt_gender.Text.Trim();           
            request.exempted = lblval.Text;
            if (ViewState["TrustedDoctor"] != null)
            {
                request.trustedDoctor = Convert.ToBoolean(ViewState["TrustedDoctor"].ToString()) == true ? "Y" : "N";
            }
            if (txtCardIssueNo.Text != "") request.cardIssueNumber = int.Parse(txtCardIssueNo.Text);
            request.age = int.Parse(txtAge.Text);

            if (chk.Visible == true)
            {
                if (chk.Checked)
                {
                    request.referral = "Y";
                }
                else
                {
                    request.referral = "N";
                }
            }

            if (txtSumCost.Text != "") request.estimatedAmount = Convert.ToDouble(txtSumCost.Text);
            if (txtSumQuantity.Text != "") request.quantity = long.Parse(txtSumQuantity.Text);

            request.bloodPressure = txtBloodPressure.Text; // BloodPressure.Trim();
            if (chkCheckup.Checked == true) request.checkUp = "1";
            if (chkChronic.Checked == true) request.chronic = "1";
            if (chkCongenital.Checked == true) request.congenital = "1";
            if (txtPulse.Text.Trim().Length > 0) request.pulse = double.Parse(txtPulse.Text.Trim());
            if (chkPsychiatric.Checked == true) request.psychiatric = "1";
            if (chkRTA.Checked == true) request.RTA = "1";
            request.dateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); // txtDateOfVisit.CalendarDate; // DateTime.ParseExact(txtDateOfVisit.CalendarDate, "dd/MM/yyyy HH:mm:ss", null);
            request.durationOfIllness = txtDurationOfIllness.Text.Trim();
            if (chkInfertility.Checked == true) request.infertility = "1";
            if (txtLastMenstruationPeriod.Text.Trim().Length > 0)
            {
                request.lastMenstruationPeriod = DateTime.ParseExact(txtLastMenstruationPeriod.Text, "dd/MM/yyyy", null);
            }
            request.otherConditions = txtOtherConditions.Text.Trim();
            if (txtLengthOfStay.Text.Trim().Length > 0) request.lengthOfStay = Int32.Parse(txtLengthOfStay.Text.Trim());

            request.possibleLineOfTreatment = txtPossibleLineOfTreatment.Text.Trim();
            if (chkVaccination.Checked == true) request.vaccination = "1";
            if (txtTemperature.Text.Trim().Length > 0) request.temperature = double.Parse(txtTemperature.Text.Trim());

            if (chkWorkRelated.Checked == true) request.workRelated = "1";


            string strServDesc = txtServiceDesc.Text;
            string strPeriod = ddlPeriod.SelectedValue;
            string strDateFrom = DateFrom.Text;// CalendarDateString;
            string strDateTo = txtDateTo.Text;
            string strQuantity = txtQuantity.Text;
            string strCost = txtCost.Text;
            string strServiceCode = "";
            string strServiceType = "";
            string strteethMap = txtTeethMap.Value;
            string strlensType = ddlLensType.SelectedValue; // TxtLensType.Text;
            string code;
            string strBenefitHead;
            double FinalTotalAmount = 0;
            long FinalTotalQuantity = 0;



            if (txtMinor.Text == "true")
            {
                //// DiagnosisCode should be pass as  '???.?' instead of 0 added by sakthi on 22-Dec-2015 
                //// New code start
                request.diagnosisDescription = ddlPharmacyDiag.SelectedItem.ToString().Contains("-Select-") ? txtDiagnosisDescription.Text : ddlPharmacyDiag.SelectedItem.ToString();// txtDiagnosisDescription.Text;
                request.diagnosisCode = ddlPharmacyDiag.SelectedValue.ToString() == "0" ? "???.?" : ddlPharmacyDiag.SelectedValue.ToString(); //txtDiagCode.Text;
                ////Old code end
                //request.diagnosisDescription = ddlPharmacyDiag.SelectedItem.ToString();// txtDiagnosisDescription.Text;
                //request.diagnosisCode = ddlPharmacyDiag.SelectedValue.ToString(); //txtDiagCode.Text;
                ////Old code end
            }
            else
            {
                request.diagnosisDescription = txtDiagnosisDescription.Text;
                if (!string.IsNullOrEmpty(txtDiagCode.Text.Trim()))
                {
                    request.diagnosisCode = txtDiagCode.Text.Trim();
                }
                else
                {
                    request.diagnosisCode = txtCIDCode.Text.Trim();
                }

                if (ddlDepartment.SelectedValue == "NEO")
                    request.diagnosisCode = Convert.ToString(DT.Rows[0][6]);
               

            }

            request.chiefComplaintsAndMainSymptoms = txtChiefComplaintsAndMainSymptoms.Text;
            if (txtLengthOfStay.Text.Trim() != "")
                request.lengthOfStay = Int32.Parse(txtLengthOfStay.Text.Trim());
            request.dateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); // txtDateOfVisit.CalendarDate;

            String[] detail_ServiceDescription = new String[DT.Rows.Count];
            String[] detail_ServiceCode = new String[DT.Rows.Count];
            String[] detail_ServiceType = new String[DT.Rows.Count];
            Int64[] detail_Quantity = new Int64[DT.Rows.Count];
            int[] detail_ItemNo = new int[DT.Rows.Count];
            String[] detail_BenefitHead = new String[DT.Rows.Count];
            Double[] detail_EstimatedCost = new double[DT.Rows.Count];
            DateTime[] detail_SupplyDateFrom = new DateTime[DT.Rows.Count]; // String2DateTime(new String[] { txt_supply_dateFm1.Text.Trim(), txt_supply_dateFm2.Text.Trim() });
            DateTime[] detail_SupplyDateTo = new DateTime[DT.Rows.Count]; // String2DateTime(new String[] { txt_supply_dateTo1.Text.Trim(), txt_supply_dateTo2.Text.Trim() });
            String[] detail_SupplyPeriod = new String[DT.Rows.Count];// cutoffInvalidStr(new String[] { txt_supply_period1.Text.Trim(), txt_supply_period2.Text.Trim() });
            allexemptdtl = new AllExemptDetail_DN[DTOptions.Rows.Count];
            ExemptCat = new string[DT.Rows.Count];

            for (int i = 0; i <= r; i++)
            {

                if (ddlPharmacyDiag.SelectedValue == "???.?" && txtMinor.Text == "true")
                {
                    //  strServiceCode = (String)DT.Rows[i][9];
                    request.diagnosisDescription = txtOthers.Text;
                }

                strServDesc = (String)DT.Rows[i][0];
                strPeriod = (String)DT.Rows[i][1];
                strDateFrom = (String)DT.Rows[i][2];
                strDateTo = (String)DT.Rows[i][3];
                strQuantity = (String)DT.Rows[i][4];
                strCost = (String)DT.Rows[i][5];

                if (DT.Rows[i][10].ToString().Length > 0)
                    strBenefitHead = (String)DT.Rows[i][10];
                else
                    strBenefitHead = "S1";

                if (DT.Rows[i][9].ToString().Length > 0)
                { strServiceCode = (String)DT.Rows[i][9]; }
                else
                { strServiceCode = ""; }
                detail_ServiceDescription.SetValue(strServDesc, i);
                detail_ServiceCode.SetValue(strServiceCode, i);
                detail_ServiceType.SetValue(strServiceType, i);
                detail_Quantity.SetValue(Int64.Parse(strQuantity), i);
                detail_ItemNo.SetValue(i + 1, i);
                detail_EstimatedCost.SetValue(Double.Parse(strCost), i);

                detail_BenefitHead.SetValue(strBenefitHead, i);

                FinalTotalAmount += Double.Parse(strCost);
                FinalTotalQuantity += long.Parse(strQuantity);
                if (ddlDepartment.SelectedValue == "NEO")
                    ExemptCat[i] = Convert.ToString(DT.Rows[i][11]); // CLS, MSP, etc
                else
                    ExemptCat[i] = strServiceCode;

            }

            if (request.exempted != "N" && BLCommon.PreAuthAllowed && Convert.ToBoolean(Session["PreAuthAllowed"].ToString()))
            {
                request.detail_ExemptCat = ExemptCat;
            }
            //request.detail_ExemptCat = ExemptCat;
            request.detail_ItemNo = detail_ItemNo;
            request.detail_ServiceType = detail_ServiceType;
            request.detail_BenefitHead = detail_BenefitHead;
            request.detail_ServiceCode = detail_ServiceCode;
            request.detail_ServiceDescription = detail_ServiceDescription;
            request.detail_Quantity = detail_Quantity;
            request.detail_EstimatedCost = detail_EstimatedCost;
            request.detail_SupplyPeriod = detail_SupplyPeriod;

            //ReqPharInfo Define
            request.username = WebPublication.CaesarSvcUsername;
            request.password = WebPublication.CaesarSvcPassword;

            if (DTOptions.Rows.Count > 0)
            {
                //request.diagnosisCode = DTOptions.Rows[0]["OptionCode"].ToString();
                //request.diagnosisDescription = txtCIDCode.Text.Trim();
            }

            try
            {
                int LineNO = 0;
                int cnt = 0;

                ////New code added by sakthi on 10-Sep-2017
                ////Start
                allexemptdtl = new AllExemptDetail_DN[0];
                for (int j = 0; j <= OptionRows; j++)
                {
                     if (DTOptions.Rows[j]["OptionCode"].ToString().Contains("@"))
                     {
                         string[] words = DTOptions.Rows[j]["OptionCode"].ToString().Split('@');
                         Array.Resize(ref allexemptdtl, allexemptdtl.Length + (words.Length - 1));
                     }
                     else
                         Array.Resize(ref allexemptdtl, allexemptdtl.Length +1);
                    
                }
                int count = 0;
                for (int j = 0; j <= OptionRows; j++)
                {
                    if (DTOptions.Rows[j]["OptionCode"].ToString().Contains("@"))
                    {
                        string[] words = DTOptions.Rows[j]["OptionCode"].ToString().Split('@');
                        ////Array.Resize(ref allexemptdtl, allexemptdtl.Length + (words.Length - 2));

                        for (cnt = 1; cnt < words.Length; cnt++)
                        {
                            if (!string.IsNullOrEmpty(words[cnt]))
                            {
                                allexemptdtl[count] = new AllExemptDetail_DN();
                                allexemptdtl[count].sql_type = "CSR.WEB_EXEMPT_REC";
                                int i = 0;
                                for (i = 0; i < DT.Rows.Count; i++)
                                {
                                    if (DT.Rows[i]["ServiceCode"].ToString() == DTOptions.Rows[j]["CategoryCode"].ToString())
                                    {
                                        break;
                                    }

                                }
                                if (DTOptions.Rows[j]["CategoryCode"].ToString() == DT.Rows[LineNO]["ServiceCode"].ToString())
                                {
                                    allexemptdtl[count].itemNo = i + 1;
                                }
                                else
                                {
                                    allexemptdtl[count].itemNo = i + 1;
                                }

                                allexemptdtl[count].OptionCode = words[cnt]; // CLS001, MSP501
                                allexemptdtl[count].OptionValue = "Y";
                            }
                            count++;
                        }
                       
                    }
                    else
                    {
                        allexemptdtl[count] = new AllExemptDetail_DN();
                        allexemptdtl[count].sql_type = "CSR.WEB_EXEMPT_REC";

                       
                        int i = 0;
                        for (i = 0; i < DT.Rows.Count; i++)
                        {
                            if (DT.Rows[i]["ServiceCode"].ToString() == DTOptions.Rows[j]["CategoryCode"].ToString())
                            {
                                break;
                            }
                        }
                        if (DTOptions.Rows[j]["CategoryCode"].ToString() == DT.Rows[LineNO]["ServiceCode"].ToString())
                        {
                            allexemptdtl[count].itemNo = i + 1;
                        }
                        else
                        {
                            allexemptdtl[count].itemNo = i + 1;
                        }


                        allexemptdtl[count].OptionCode = (String)DTOptions.Rows[j]["OptionCode"]; // CLS001, MSP501
                        allexemptdtl[count].OptionValue = (String)DTOptions.Rows[j]["OptionValue"];
                        count++;
                    }


                }

                ////return;
                ////End
                ////Old Code 
                ////Start

                ////for (int j = 0; j <= OptionRows; j++)
                ////{
                ////    if (DTOptions.Rows[j]["OptionCode"].ToString().Contains("@"))
                ////    {
                ////        string[] words = DTOptions.Rows[j]["OptionCode"].ToString().Split('@');
                ////        Array.Resize(ref allexemptdtl, allexemptdtl.Length + (words.Length - 2));

                ////        for (cnt = 1; cnt < words.Length; cnt++)
                ////        {
                ////            if (!string.IsNullOrEmpty(words[cnt]))
                ////            {
                ////                allexemptdtl[j + (cnt - 1)] = new AllExemptDetail_DN();
                ////                allexemptdtl[j + (cnt - 1)].sql_type = "CSR.WEB_EXEMPT_REC";


                ////                ////New code Added by Sakthi on 04-Jan-2016, regards the item no is not passing correctly to caesar 
                ////                ////New code start
                ////                int i = 0;
                ////                for (i = 0; i < DT.Rows.Count; i++)
                ////                {
                ////                    if (DT.Rows[i]["ServiceCode"].ToString() == DTOptions.Rows[j]["CategoryCode"].ToString())
                ////                    {
                ////                        break;
                ////                    }

                ////                }
                ////                if (DTOptions.Rows[j]["CategoryCode"].ToString() == DT.Rows[LineNO]["ServiceCode"].ToString())
                ////                {
                ////                    allexemptdtl[j + (cnt - 1)].itemNo = i + 1;
                ////                }
                ////                else
                ////                {
                ////                    allexemptdtl[j + (cnt - 1)].itemNo = i + 1;
                ////                }
                ////                ////New code end

                ////                ////Old Code start
                ////                ////if (DTOptions.Rows[j]["CategoryCode"].ToString() == DT.Rows[LineNO]["ServiceCode"].ToString())
                ////                ////{
                ////                ////    allexemptdtl[j + (cnt - 1)].itemNo = LineNO + 1;
                ////                ////}
                ////                ////else
                ////                ////{
                ////                ////    allexemptdtl[j + (cnt - 1)].itemNo = ++LineNO + 1;
                ////                ////}
                ////                ////Old code end
                ////                allexemptdtl[j + (cnt - 1)].OptionCode = words[cnt]; // CLS001, MSP501
                ////                allexemptdtl[j + (cnt - 1)].OptionValue = "Y";
                ////            }
                ////        }
                ////        cnt -= 2;
                ////    }
                ////    else
                ////    {
                ////        allexemptdtl[j + cnt] = new AllExemptDetail_DN();
                ////        allexemptdtl[j + cnt].sql_type = "CSR.WEB_EXEMPT_REC";

                ////        ////New code Added by Sakthi on 04-Jan-2016, regards the item no is not passing correctly to caesar 
                ////        ////New code start
                ////        int i = 0;
                ////        for (i = 0; i < DT.Rows.Count; i++)
                ////        {
                ////            if (DT.Rows[i]["ServiceCode"].ToString() == DTOptions.Rows[j]["CategoryCode"].ToString())
                ////            {
                ////                break;
                ////            }
                ////        }
                ////        if (DTOptions.Rows[j]["CategoryCode"].ToString() == DT.Rows[LineNO]["ServiceCode"].ToString())
                ////        {
                ////            allexemptdtl[j + cnt].itemNo = i + 1;
                ////        }
                ////        else
                ////        {
                ////            allexemptdtl[j + cnt].itemNo = i + 1;
                ////        }
                ////        ////New code end

                ////        ////Old Code start
                ////        //if (DTOptions.Rows[j]["CategoryCode"].ToString() == DT.Rows[LineNO]["ServiceCode"].ToString())
                ////        //{
                ////        //    allexemptdtl[j + cnt].itemNo = LineNO + 1;
                ////        //}
                ////        //else
                ////        //{
                ////        //    allexemptdtl[j + cnt].itemNo = ++LineNO + 1;

                ////        //}
                ////        ////Old code end

                ////        allexemptdtl[j + cnt].OptionCode = (String)DTOptions.Rows[j]["OptionCode"]; // CLS001, MSP501
                ////        allexemptdtl[j + cnt].OptionValue = (String)DTOptions.Rows[j]["OptionValue"];
                ////    }


                ////}
                ////End

                //allexemptdtl = ArrangeDataSet(DTOptions);
                DataTable dtDLS = new DataTable();
                DataTable dtDLSserviceCnt = new DataTable();

                if (DT != null && DT.Rows.Count > 0)
                {
                    ////dtDLS = DT.AsEnumerable()
                    ////        .Where(t => t.Field<string>("ServiceCode") == "DLS")
                    ////        .CopyToDataTable();

                    dtDLS = DT.Clone();



                    foreach (DataRow dr in DT.Rows)
                    {

                        if (Convert.ToString(dr["ServiceCode"]).Trim().ToUpper().Equals("DLS"))
                        {
                            dtDLS.Rows.Add(dr.ItemArray);
                        }

                    }
                    //dtDLS = DT.Select("ServiceCode = 'DLS'").CopyToDataTable();
                    dtDLSserviceCnt = dtDLS;
                }


                if (dtDLS.Rows.Count >= 0)
                {
                    try
                    {
                        for (int j = 0; j < dtDLSserviceCnt.Rows.Count; j++)
                        {
                            DataTable dt = (DataTable)Session["dtServiceCategory"];
                            dt = dt.Select("category = '" + dtDLSserviceCnt.Rows[j]["serviceDesc"] + "'").CopyToDataTable();
                            dtDLS = DT.Select("ServiceDesc = '" + dtDLSserviceCnt.Rows[j]["serviceDesc"] + "'").CopyToDataTable();
                            int i = 0;
                            for (i = 0; i < DT.Rows.Count; i++)
                            {
                                if (DT.Rows[i]["ServiceDesc"] == dtDLS.Rows[0]["serviceDesc"])
                                {
                                    break;
                                }

                            }

                            if (dtDLS.Rows.Count >= 0)
                            {
                                if (allexemptdtl.Length > 0)
                                {
                                    Array.Resize(ref allexemptdtl, allexemptdtl.Length + 1);
                                    allexemptdtl[allexemptdtl.Length - 1] = new AllExemptDetail_DN();
                                    allexemptdtl[allexemptdtl.Length - 1].sql_type = "CSR.WEB_EXEMPT_REC";
                                    allexemptdtl[allexemptdtl.Length - 1].OptionCode = dt.Rows[0]["serviceCode"].ToString();
                                    allexemptdtl[allexemptdtl.Length - 1].OptionValue = "Y";
                                    allexemptdtl[allexemptdtl.Length - 1].itemNo = i + 1;//DT.Rows.Count;//
                                }
                                else
                                {
                                    Array.Resize(ref allexemptdtl, 1);
                                    allexemptdtl[0] = new AllExemptDetail_DN();
                                    allexemptdtl[0].sql_type = "CSR.WEB_EXEMPT_REC";
                                    allexemptdtl[0].OptionCode = dt.Rows[0]["serviceCode"].ToString();
                                    allexemptdtl[0].OptionValue = "Y";
                                    ////new code added by sakthi on 03-Jan-2016 regards, the item no is not passing correctly to caesar.
                                    ////Start
                                    allexemptdtl[0].itemNo = i + 1;
                                    ////End
                                    ////Old code start
                                    ////allexemptdtl[0].itemNo = 1; 
                                    ////Old code end

                                }

                            }
                        }
                    }
                    catch (Exception ex) { }
                }

            }
            catch (Exception ex) { }
            //request.exDetail = allexemptdtl; //2014-11-14 REF-1615 CR215 Nathan
            request.exDetail = nullLessArray(allexemptdtl);
            response = ws.ReqDiagInfo(request);


            //Convert object to Xml 
            //If webservice is not responding fine, then send the request & response xml to Caesar team 
            //Below code need to enable, it will saving the request & rsponse xml in specifeied path
            //Strat
            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DiagnosisInfoRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DiagnosisInfoRequest_DN>();
            //XmlReq.Request(request, "DiagnosisInfoSubmt_Request");

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DiagnosisInfoResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.DiagnosisInfoResponse_DN>();
            //XmlResp.Response(response, "DiagnosisInfoSubmit_Response");
            //End

            Response.Write("Txn ID: " + ln_txnid + "<br />");

            if (response.status != "0")
            {
                string sError = string.Empty;
                for (int i = 0; i < response.errorMessage.Length; i++)
                {
                    sError += " - " + response.errorMessage[i].ToString();
                }
                sError = " - " + response.errorMessage[0].ToString();
                //Response.Write(sError + "<br />");
                ScriptManager.RegisterStartupScript(this, GetType(), "PreAuthNotification", "alert('" + sError + "')", true); 
                lblNoTreatmentDetError.Text = sError;
                lblNoTreatmentDetError.Visible = true;
                btnSubmit.Text = "Submit Request";

                ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR&txnid=" + response.transactionID + "&sError=" + sError, false);
                var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR&txnid=" + response.transactionID + "&sError=" + sError;
                queryString = Cryption.Encrypt(queryString);
                Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
            }
            else
            {
                Response.Write("**Success**<br />");
                Response.Write("Transaction: " + response.transactionID + "<br />");
                Response.Write("Preauth ID: " + response.preAuthorizationID + "<br />");
                Response.Write("Extend Date: " + response.ext_Date + "<br />");
                Response.Write("Extend IND: " + response.ext_Ind + "<br />");

                LogPreAuthRequest(response);

                var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR&txnid=" + response.transactionID ;
                queryString = Cryption.Encrypt(queryString);
                Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
                ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR&txnid=" + response.transactionID, false);



            }



        }
        catch (Exception ex)
        {
            //Response.Redirect("PreauthDetails.aspx?PID=0000&PC=000&RType=NR&txnid=111111111&sError=" + ex.Message +" - "+ ex.StackTrace, false);
            Message1.Text = ex.Message + " - " + ex.StackTrace;
            Message1.Visible = true;
        }


    }
    //public static DataTable ArrangeDataSet(DataTable dtOpt)
    public static AllExemptDetail_DN[] ArrangeDataSet(DataTable dtOpt)
    {
        AllExemptDetail_DN[] allexemptdtl = null;
        DataTable dtTemp = null;
        dtTemp = dtOpt.Copy();
        dtTemp.Columns.Add(new DataColumn("LineNo"));
        dtTemp.Rows.Clear();
        DataRow dr = null;
        int lineItem = 1;
        try
        {


            for (int j = 0; j < dtOpt.Rows.Count; j++)
            {

                if (dtOpt.Rows[j]["OptionCode"].ToString().Contains("@"))
                {

                    string[] words = dtOpt.Rows[j]["OptionCode"].ToString().Split('@');

                    for (int cnt = 1; cnt < words.Length; cnt++)
                    {
                        if (!string.IsNullOrEmpty(words[cnt]))
                        {
                            dr = dtTemp.NewRow();
                            dr["CategoryCode"] = dtOpt.Rows[j]["CategoryCode"];
                            dr["OptionCode"] = words[cnt];
                            dr["OptionValue"] = dtOpt.Rows[j]["OptionValue"];
                            dr["LineNo"] = lineItem;
                            dtTemp.Rows.Add(dr);
                        }
                    }


                }
                else
                {
                    dr = dtTemp.NewRow();
                    dr["CategoryCode"] = dtOpt.Rows[j]["CategoryCode"];
                    dr["OptionCode"] = dtOpt.Rows[j]["OptionCode"];
                    dr["OptionValue"] = dtOpt.Rows[j]["OptionValue"];
                    dr["LineNo"] = lineItem;
                    dtTemp.Rows.Add(dr);

                }

                if (dtOpt.Rows.Count > 1)
                {
                    try
                    {
                        if (dtOpt.Rows[j]["CategoryCode"].ToString() != dtOpt.Rows[j + 1]["CategoryCode"].ToString())
                        {
                            lineItem++;
                        }
                    }
                    catch (Exception ex) { }
                }
            }


        }
        catch (Exception ex) { }

        try
        {
            allexemptdtl = new AllExemptDetail_DN[dtTemp.Rows.Count];
            for (int j = 0; j < dtTemp.Rows.Count; j++)
            {
                allexemptdtl[j] = new AllExemptDetail_DN();
                allexemptdtl[j].sql_type = "CSR.WEB_EXEMPT_REC";
                allexemptdtl[j].OptionCode = dtTemp.Rows[j]["OptionCode"].ToString();
                allexemptdtl[j].OptionValue = dtTemp.Rows[j]["OptionValue"].ToString();
                allexemptdtl[j].itemNo = Convert.ToInt32(dtTemp.Rows[j]["LineNo"]);

            }
        }
        catch (Exception ex) { }

        //return dtTemp;    
        return allexemptdtl;
    }
    public static AllExemptDetail_DN[] nullLessArray(AllExemptDetail_DN[] src)
    {
        AllExemptDetail_DN[] outputArray = null;
        try
        {
            //Array.Sort(src);
            //Array.Reverse(src);
            int index = Array.IndexOf(src, null);
            if (index == -1)
            {
                outputArray = src;
            }
            else
            {
                outputArray = new AllExemptDetail_DN[index];

                for (int counter = 0; counter < index; counter++)
                {
                    outputArray[counter] = src[counter];
                }
            }
        }
        catch (Exception ex) { }
        return outputArray;
    }
    public void LogPreAuthRequest(DiagnosisInfoResponse_DN response)
    {
        try
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);

            SqlParameter[] arParmsInsert = new SqlParameter[16];
            arParmsInsert[0] = new SqlParameter("@ProCode", SqlDbType.NChar, 10);
            arParmsInsert[0].Value = Session["ProviderID"];
            arParmsInsert[1] = new SqlParameter("@ProName", SqlDbType.NVarChar, 100);
            arParmsInsert[1].Value = Session["ProviderName"];
            arParmsInsert[2] = new SqlParameter("@MemberShipNo", SqlDbType.NChar, 10);
            arParmsInsert[2].Value = response.membershipNo;
            arParmsInsert[3] = new SqlParameter("@MemberName", SqlDbType.NVarChar, 150);
            arParmsInsert[3].Value = response.memberName;
            arParmsInsert[4] = new SqlParameter("@MemberType", SqlDbType.NChar, 10);
            arParmsInsert[4].Value = response.status;
            arParmsInsert[5] = new SqlParameter("@AddedBy", SqlDbType.NChar, 10);
            arParmsInsert[5].Value = Session["ProviderUserName"];
            arParmsInsert[6] = new SqlParameter("@Department", SqlDbType.NChar, 100);
            arParmsInsert[6].Value = response.department;
            arParmsInsert[7] = new SqlParameter("@Mobile", SqlDbType.NChar, 10);
            arParmsInsert[7].Value = response.patientFileNo;
            arParmsInsert[8] = new SqlParameter("@verification_id", SqlDbType.BigInt);
            arParmsInsert[8].Value = response.preAuthorizationID;  // Verification id
            arParmsInsert[9] = new SqlParameter("@swip_card", SqlDbType.Bit);
            arParmsInsert[9].Value = false;
            arParmsInsert[10] = new SqlParameter("@ContractNumber", SqlDbType.NChar, 10);
            arParmsInsert[10].Value = string.IsNullOrEmpty(response.service_Desc[0].ToString()) ? "" : response.service_Desc[0].ToString();
            arParmsInsert[11] = new SqlParameter("@PatientMobileNo", SqlDbType.NChar, 10);
            arParmsInsert[11].Value = response.IDCardNo;

            arParmsInsert[12] = new SqlParameter("@swipe_override", SqlDbType.Bit);
            arParmsInsert[12].Value = false;

            arParmsInsert[13] = new SqlParameter("@company_name", SqlDbType.NVarChar, 100);
            arParmsInsert[13].Value = response.notes[0].ToString();
            arParmsInsert[14] = new SqlParameter("@OTP", SqlDbType.NChar, 10);
            arParmsInsert[14].Value = response.preauthorisationStatus;
            arParmsInsert[15] = new SqlParameter("@OTPMessage", SqlDbType.NVarChar, 150);
            arParmsInsert[15].Value = response.transactionID;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spMemberOTPVerificationInsert", arParmsInsert);

        }
        catch (Exception ex) { }

    }
    public void ProcessPreAuthPharmacyRequest()
    {
        try
        {
            ServiceDepot_DNService ws;
            //ReqPharInfo Define
            PharmacyInfoRequest_DN request;
            PharmacyInfoResponse_DN response;

            AllExemptDetail_DN[] allexemptdtl = new AllExemptDetail_DN[1];

            //ReqDiagInfo Define
            //DiagnosisInfoRequest_DN request;
            //DiagnosisInfoResponse_DN response;


            //ReqPharInfo Define
            request = new PharmacyInfoRequest_DN();

            //ReqDiagInfo Define
            //request = new DiagnosisInfoRequest_DN();

            ws = new ServiceDepot_DNService();


            //Web Standard(Included Maternity and VAC) Master Data
            ln_txnid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssf"));
            request.transactionID = ln_txnid;
            request.transactionType = "N";
            //request.preauthorisation_ID = 0;

            //2013-11-25 Larry Lam REF-1240 CR180 - Test Data - Start

            PreauthDoc_DN[] PA_Doc = new PreauthDoc_DN[3];
            PA_Doc[0] = new PreauthDoc_DN();
            PA_Doc[0].sql_type = "CSR.PREA_REQ_DOC01";
            PA_Doc[0].TXN_ID = ln_txnid;
            PA_Doc[0].DOC_IMG_FILENAME = "";
            PA_Doc[0].DOC_IMG_LOCN = "";
            PA_Doc[0].DOC_PATH = "CR190_Test_Attachments\\2013\\09\\18\\FN11119600_001.doc";

            PA_Doc[1] = new PreauthDoc_DN();
            PA_Doc[1].sql_type = "CSR.PREA_REQ_DOC01";
            PA_Doc[1].TXN_ID = ln_txnid;
            PA_Doc[1].DOC_IMG_FILENAME = "";
            PA_Doc[1].DOC_IMG_LOCN = "";
            PA_Doc[1].DOC_PATH = "CR190_Test_Attachments\\2013\\09\\18\\FN11119600_002.doc";

            PA_Doc[2] = new PreauthDoc_DN();
            PA_Doc[2].sql_type = "CSR.PREA_REQ_DOC01";
            PA_Doc[2].TXN_ID = ln_txnid;
            PA_Doc[2].DOC_IMG_FILENAME = "";
            PA_Doc[2].DOC_IMG_LOCN = "";
            PA_Doc[2].DOC_PATH = "CR190_Test_Attachments\\2013\\09\\18\\FN11119600_003.doc";
            //2013-11-25 Larry Lam REF-1240 CR180 - Test Data - End

            //request.membershipNo = "4154861";
            //request.policyNo = "11125000";
            //request.memberID_Igama = "1";
            //request.memberName = "CR226 CR235 Test Mbr 1";
            ////request.memberMobileNo = "";
            ////request.cardIssueNumber = "";
            ////request.preauthorisation_ID = 547369;
            //request.age = 26;
            //request.gender = "M";
            //request.physician_Name = "Dr. Test";
            //request.providerCode = "20087";
            //request.providerFaxNo = "25586548";
            //request.patientFileNo = "2014002";
            //request.treatmentType = "O";
            //request.departmentType = "";
            //request.dateOfAdmission = new DateTime(2014, 11, 06);
            //request.lengthOfStay = 1;
            //request.quantity = 1;
            //request.estimatedAmount = 0;
            //request.diagnosisCode = "J00";
            //request.diagnosisDescription = "";
            ////request.verificationID = 0;
            //request.exempted = "N";
            //request.referral = "N";

            ////Maternity Testing data CR170 UAT MC Log
            //ItemNo = new int[] { 1 };
            //ServiceType = new string[] { "" };
            //BenefitHead = new string[] { "S1" };
            //ServiceCode = new string[] { "" };
            //ServiceDescription = new string[] { "Standard Medication" };
            //Quantity = new long[] { 1 };
            //EstimatedCost = new double[] { 100.0 };
            //SupplyPeriod = new string[] { "" };
            //ExemptCat = new string[] { "" }; //2014-11-14 REF-1615 CR215 Nathan

            //request.detail_ItemNo = ItemNo;
            //request.detail_ServiceType = ServiceType;
            //request.detail_BenefitHead = BenefitHead;
            //request.detail_ServiceCode = ServiceCode;
            //request.detail_ServiceDescription = ServiceDescription;
            //request.detail_Quantity = Quantity;
            //request.detail_EstimatedCost = EstimatedCost;
            //request.detail_SupplyPeriod = SupplyPeriod;
            //request.detail_ExemptCat = ExemptCat; //2014-11-14 REF-1615 CR215 Nathan
            ////request.detail_SupplyDateFrom = SupplyDateFrom;
            ////request.detail_SupplyDateTo = SupplyDateTo;

            //2014-11-14 REF-1615 CR215 Nathan Start
            allexemptdtl[0] = new AllExemptDetail_DN();
            allexemptdtl[0].sql_type = "CSR.WEB_EXEMPT_REC";
            allexemptdtl[0].itemNo = 1;
            allexemptdtl[0].OptionCode = "CLS001";
            allexemptdtl[0].OptionValue = "Y";
            //2014-11-14 REF-1615 CR215 Nathan End

            //ReqPharInfo Define
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            //ReqDiagInfo Define
            //request.username = WebPublication.CaesarSvcUsername;
            //request.password = WebPublication.CaesarSvcPassword;

            request.detail = PA_Doc;
            request.exDetail = allexemptdtl; //2014-11-14 REF-1615 CR215 Nathan
            //ReqPharInfo Define
            response = ws.ReqPharInfo(request);
            //ReqDiagInfo Define
            //response = ws.ReqDiagInfo(request);

            Response.Write("Txn ID: " + ln_txnid + "<br />");

            if (response.status != "0")
            {
                for (int i = 0; i < response.errorMessage.Length; i++)
                    Response.Write(response.errorMessage[i] + "<br />");
            }
            else
            {
                Response.Write("**Success**<br />");
                Response.Write("Transaction: " + response.transactionID + "<br />");
                Response.Write("Preauth ID: " + response.preAuthorizationID + "<br />");
                Response.Write("Extend Date: " + response.ext_Date + "<br />");
                Response.Write("Extend IND: " + response.ext_Ind + "<br />");
            }

            ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR", false);
            var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&RType=NR";
            queryString = Cryption.Encrypt(queryString);
            Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
        }
        catch (Exception ex)
        { }

    }

    public void ProcessPharRequest()
    {
        int NoOfChecked;

        DT = (DataTable)Session["dt"];
        int c = DT.Columns.Count - 1;
        int r = DT.Rows.Count - 1;
        string[,] str = new string[r + 1, c + 1];
        string strr = "";

        int RowCount;// = DT.Rows.Count;

        if (txtReqMed.Text != "false")
        {
            NoOfChecked = Convert.ToInt16(txtReqMed.Text);
            //r = NoOfChecked - 1; 
            RowCount = NoOfChecked;
        }
        else
        {
            RowCount = DT.Rows.Count;
        }

        OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.PharmacyInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.PharmacyInfoResponse_DN response;

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();// TransactionID();// long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));
        request.treatmentType = ddlTreatmentType.SelectedValue; // txt_treatment_type.Text.Trim();
        request.providerFaxNo = strProviderFax;// txt_pv_fax.Text.Trim();
        request.providerCode = strProviderID;// "20087"; // txt_pv_code.Text.Trim();
        request.policyNo = txtPolicyNo.Text;// txt_pocy.Text.Trim();
        request.physician_Name = txtNameOfPhysician.Text;// txt_phy_name.Text.Trim();
        request.patientFileNo = txtPatientFileNo.Text; // txt_patientFile_no.Text.Trim();
        request.membershipNo = txtMembershipNo.Text;// txt_mbr.Text.Trim();
        request.memberName = txtMemberName.Text;// txt_mbr_name.Text.Trim();
        request.memberMobileNo = txtMobileNo.Text; // txt_mobile.Text.Trim();
        request.memberID_Igama = txtMemberID.Text;// txt_mbr_igm.Text.Trim();
        request.gender = ddlGender.Text; //.SelectedValue; // txt_gender.Text.Trim();
        if (ViewState["TrustedDoctor"] != null)
        {
            request.trustedDoctor = Convert.ToBoolean(ViewState["TrustedDoctor"].ToString()) == true ? "Y" : "N";
        }
        if (txtCardIssueNo.Text != "") request.cardIssueNumber = int.Parse(txtCardIssueNo.Text);
        request.age = int.Parse(txtAge.Text);
        request.departmentType = ddlDepartment.SelectedValue;
        request.dateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); //txtDateOfVisit.CalendarDate;


        if (txtSumCost.Text != "") request.estimatedAmount = Convert.ToDouble(txtSumCost.Text);
        if (txtSumQuantity.Text != "") request.quantity = long.Parse(txtSumQuantity.Text);

        string strServDesc = txtServiceDesc.Text;
        string strServCode = txtServiceDesc.Text;
        string strServiceType = "";  // new field for cr108 for mediciation
        string strPeriod = ddlPeriod.SelectedValue;
        string strDateFrom = DateFrom.Text;// CalendarDateString;
        string strDateTo = txtDateTo.Text;
        string strQuantity = txtQuantity.Text;
        string strCost = txtCost.Text;
        string strteethMap = txtTeethMap.Value;
        string strlensType = ddlLensType.SelectedValue; // TxtLensType.Text;
        string code;

        #region "Phar"
        request.diagnosisDescription = ddlPharmacyDiag.SelectedItem.Text;// txtDiagnosisDescription.Text;
        request.diagnosisCode = ddlPharmacyDiag.SelectedValue;// txtDiagCode.Text;

        request.transactionType = "N";

        String[] detail_ServiceDescription = new String[RowCount];
        String[] detail_ServiceCode = new String[RowCount];
        String[] detail_ServiceType = new String[RowCount];
        Int64[] detail_Quantity = new Int64[RowCount];
        int[] detail_ItemNo = new int[RowCount];
        String[] detail_BenefitHead = new String[RowCount];
        Double[] detail_EstimatedCost = new double[RowCount];
        DateTime[] detail_SupplyDateFrom = new DateTime[RowCount]; // String2DateTime(new String[] { txt_supply_dateFm1.Text.Trim(), txt_supply_dateFm2.Text.Trim() });
        DateTime[] detail_SupplyDateTo = new DateTime[RowCount]; // String2DateTime(new String[] { txt_supply_dateTo1.Text.Trim(), txt_supply_dateTo2.Text.Trim() });
        String[] detail_SupplyPeriod = new String[RowCount];// cutoffInvalidStr(new String[] { txt_supply_period1.Text.Trim(), txt_supply_period2.Text.Trim() });

        int CheckedDataCount = 0;

        for (int i = 0; i <= r; i++)
        {
            strServDesc = (String)DT.Rows[i][0];
            if (ddlPharmacyDiag.SelectedValue != "???.?" && ddlDepartment.SelectedValue != "VAC")
            {
                strServCode = (String)DT.Rows[i][9];
                strServiceType = "ME";
                if (ddlDepartment.SelectedValue != "PHA")
                    request.diagnosisDescription = ddlPharmacyDiag.Text; // txtOthers.Text;
            }

            if (ddlDepartment.SelectedValue == "VAC")
                strServCode = (String)DT.Rows[i][9];



            if (ddlPharmacyDiag.SelectedValue == "???.?")
            {
                strServCode = "";
                request.diagnosisDescription = txtOthers.Text;
            }

            if (ddlPharmacyDiag.SelectedValue == "0" && btnRequestMedication.Enabled != true)
            {
                request.diagnosisCode = (String)DT.Rows[i][6];
                request.diagnosisDescription = (String)DT.Rows[i][10];
            }


            if (ddlPharmacyDiag.SelectedValue == "0" && btnRequestMedication.Enabled == true)
            {
                request.diagnosisCode = (String)DT.Rows[0][6];
                request.diagnosisDescription = (String)DT.Rows[0][10];
            }

            if (ddlPharmacyDiag.SelectedValue == "0" && txtReqMed.Text != "false")
            {
                request.diagnosisCode = (String)DT.Rows[0][6];
                request.diagnosisDescription = (String)DT.Rows[0][10];
            }

            strPeriod = (String)DT.Rows[i][1];
            strDateFrom = (String)DT.Rows[i][2];
            strDateTo = (String)DT.Rows[i][3];
            strQuantity = (String)DT.Rows[i][4];
            strCost = (String)DT.Rows[i][5];
            GridViewRow gr = GridView1.Rows[i];
            CheckBox check = (CheckBox)gr.FindControl("Chec");

            if (check.Checked == true || txtReqMed.Text == "false")
            {


                if (strPeriod == "1") strDateTo = DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", null).AddDays(27).ToShortDateString(); // DateTime.Today.AddDays(27).ToShortDateString();
                if (strPeriod == "2") strDateTo = DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", null).AddDays(55).ToShortDateString(); // DateTime.Today.AddDays(27).ToShortDateString();
                if (strPeriod == "3") strDateTo = DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", null).AddDays(83).ToShortDateString(); // DateTime.Today.AddDays(27).ToShortDateString();

                if (ddlDepartment.SelectedValue != "VAC")
                    DateTime.ParseExact(strDateTo, "dd/MM/yyyy", null).AddDays(27).ToShortDateString();

                detail_ServiceDescription.SetValue(strServDesc, CheckedDataCount);
                detail_ServiceCode.SetValue(strServCode, CheckedDataCount);
                detail_ServiceType.SetValue(strServiceType, CheckedDataCount);
                detail_Quantity.SetValue(Int64.Parse(strQuantity), CheckedDataCount);
                detail_ItemNo.SetValue(CheckedDataCount + 1, CheckedDataCount);
                detail_EstimatedCost.SetValue(Double.Parse(strCost), CheckedDataCount);

                if (ddlDepartment.SelectedValue == "VAC")
                {
                    if (ddlPharmacyDiag.SelectedValue == "ZA27.6")
                        detail_BenefitHead.SetValue("V2", CheckedDataCount);
                    else
                        detail_BenefitHead.SetValue("V1", CheckedDataCount);


                }
                else
                    detail_BenefitHead.SetValue("C1", CheckedDataCount);



                if (ddlDepartment.SelectedValue != "VAC")
                {
                    detail_SupplyDateFrom.SetValue(DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", null), CheckedDataCount);
                    //if (strDateTo.Trim().Length > 0) detail_SupplyDateTo.SetValue(DateTime.ParseExact(strDateTo, "dd/MM/yyyy", null), i);
                    if (strDateTo.Trim().Length > 0) detail_SupplyDateTo.SetValue(DateTime.ParseExact(strDateTo, "dd/MM/yyyy", null), CheckedDataCount);
                    detail_SupplyPeriod.SetValue(strPeriod, CheckedDataCount);
                }
                CheckedDataCount++;

            }

        }

        request.detail_BenefitHead = detail_BenefitHead;
        request.detail_EstimatedCost = detail_EstimatedCost;
        request.detail_ItemNo = detail_ItemNo;
        request.detail_Quantity = detail_Quantity;
        request.detail_ServiceCode = detail_ServiceCode;
        request.detail_ServiceType = detail_ServiceType;
        request.detail_ServiceDescription = detail_ServiceDescription;


        request.detail_SupplyDateFrom = detail_SupplyDateFrom;
        request.detail_SupplyDateTo = detail_SupplyDateTo;
        request.detail_SupplyPeriod = detail_SupplyPeriod;
        ////request.detail = preauthSupportDocuments(uploadSession.Text, _uploadCategory, request.transactionID);
        request.detail = preauthSupportDocuments(Convert.ToString(Session["UploadSession"]), _uploadCategory, request.transactionID);
        request.exempted = lblval.Text;

        if (chk.Visible == true)
        {
            if (chk.Checked)
            {
                request.referral = "Y";
            }
            else
            {
                request.referral = "N";
            }
        }


        try
        {

            StringBuilder sb = new StringBuilder(200);

            //ExceptionManager.TrackWSPerformance("Preauth13_3", "ReqPharInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), strProviderID, "", "");
            response = ws.ReqPharInfo(request);
            //ExceptionManager.TrackWSPerformance("Preauth13_3", "ReqPharInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), strProviderID, "", "");

            if (response.status != "0")
            {
                if (response.ext_Ind == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "$.modal.close();');");
                    litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("\n");
                Message1.Text = sb.ToString();
                Message1.Visible = true;
            }
            else
            {
                // Send the email
                string provider = Session["ProviderName"].ToString();
                string username = Session["ProviderUserName"].ToString();

                Notify(provider, username, response.preAuthorizationID.ToString());

                var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR&DEPT=" + ddlDepartment.SelectedValue;
                queryString = Cryption.Encrypt(queryString);

                if (response.ext_Ind == "Y")
                {
                    ////aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR&DEPT=" + ddlDepartment.SelectedValue + "');");
                    aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?val=" + queryString + "');");
                    litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }

                else
                {
                    ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR&DEPT=" + ddlDepartment.SelectedValue + "&", false);
                    Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
                }


                //

                Message1.Text = "Success";
            }
        }
        catch (Exception ex)
        {

            Message1.Text += "\n" + ex.Message;
            //ExceptionManager.LogException(ex);
        }
        # endregion

    }

    public void ProcessOpticalRequest()
    {

        DT = (DataTable)Session["dt"];
        int c = DT.Columns.Count - 1;
        int r = DT.Rows.Count - 1;
        string[,] str = new string[r + 1, c + 1];
        string strr = "";

        OS_DXC_WAP.CaesarWS.OpticalInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.OpticalInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.OpticalInfoResponse_DN response;

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();// TransactionID();// long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM") + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss").Substring(0, 1));


        //      request.verificationID = Int64.Parse(txt_verID.Text.Trim());
        request.treatmentType = ddlTreatmentType.SelectedValue; // txt_treatment_type.Text.Trim();

        request.departmentType = "OER";
        request.providerFaxNo = strProviderFax;// txt_pv_fax.Text.Trim();
        request.providerCode = strProviderID;// "20087"; // txt_pv_code.Text.Trim();
        request.policyNo = txtPolicyNo.Text;// txt_pocy.Text.Trim();
        request.physician_Name = txtNameOfPhysician.Text;// txt_phy_name.Text.Trim();
        request.patientFileNo = txtPatientFileNo.Text; // txt_patientFile_no.Text.Trim();
        request.membershipNo = txtMembershipNo.Text;// txt_mbr.Text.Trim();
        request.memberName = txtMemberName.Text;// txt_mbr_name.Text.Trim();
        request.memberMobileNo = txtMobileNo.Text; // txt_mobile.Text.Trim();
        request.memberID_Igama = txtMemberID.Text;// txt_mbr_igm.Text.Trim();
        request.gender = ddlGender.Text; //.SelectedValue; // txt_gender.Text.Trim();
        if (ViewState["TrustedDoctor"] != null)
        {
            request.trustedDoctor = Convert.ToBoolean(ViewState["TrustedDoctor"].ToString()) == true ? "Y" : "N";
        }
        if (txtCardIssueNo.Text != "") request.cardIssueNumber = int.Parse(txtCardIssueNo.Text);
        request.age = int.Parse(txtAge.Text);
        if (txtSumCost.Text != "") request.estimatedAmount = Convert.ToDouble(txtSumCost.Text);
        if (txtSumQuantity.Text != "") request.quantity = long.Parse(txtSumQuantity.Text);

        string strServDesc = txtServiceDesc.Text;
        string strPeriod = ddlPeriod.SelectedValue;
        string strDateFrom = DateFrom.Text;//.CalendarDateString;
        string strDateTo = txtDateTo.Text;
        string strQuantity = txtQuantity.Text;
        string strCost = txtCost.Text;
        string strteethMap = txtTeethMap.Value;
        string strlensType = "";// ddlLensType.SelectedValue; // TxtLensType.Text;
        string code = "";
        string ServiceCode = "";

        #region "Optical"
        request.diagnosisDescription = txtDiagnosisDescription.Text;
        ////CR Request by business on 20-Aug-2017 enabled by sakthi
        ////Start
        ////request.diagnosisCode = BLCommon.HRMEnabled ? string.Empty : txtDiagCode.Text;
        request.diagnosisCode = BLCommon.HRMEnabled ? string.Empty : "H52";//// CAESAR expecting connstant value for non hrm, the constant value is H52 
        ////End
        request.chiefComplaintsAndMainSymptoms = txtChiefComplaintsAndMainSymptoms.Text;
        // request.lengthOfStay = Int64.Parse(txtLengthOfStay.Text.Trim());
        request.dateOfAdmission = DateTime.ParseExact(txtDateOfVisit.Text, "dd/MM/yyyy", null); // txtDateOfVisit.CalendarDate;

        request.benefitType = "O";

        //request.benefitType = txt_ben_type.Text.Trim();
        //request.estimatedAmount = Double.Parse(txt_est_amt.Text.Trim());
        //request.quantity = Int64.Parse(txt_quan.Text.Trim());

        //if (!Convert.ToBoolean(ConfigurationManager.AppSettings["HRMEnabled"]))
        if (BLCommon.HRMEnabled)
        {
            if (righteyedistanceva.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyedistanceva.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeDistanceVA = Double.Parse(righteyedistanceva.Value.Trim());

            if (righteyedistancesphere.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyedistancesphere.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeDistanceSphere = Double.Parse(righteyedistancesphere.Value.Trim());

            if (righteyedistanceprism.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyedistanceprism.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeDistancePrism = Double.Parse(righteyedistanceprism.Value.Trim());

            if (righteyedistancecylinder.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyedistancecylinder.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeDistanceCylinder = Double.Parse(righteyedistancecylinder.Value.Trim());

            if (righteyedistanceaxis.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyedistanceaxis.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeDistanceAxis = Double.Parse(righteyedistanceaxis.Value.Trim());

            if (righteyenearva.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyenearva.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeNearVA = Double.Parse(righteyenearva.Value.Trim());

            if (righteyenearsphere.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyenearsphere.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeNearSphere = Double.Parse(righteyenearsphere.Value.Trim());

            if (righteyenearprism.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyenearprism.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeNearPrism = Double.Parse(righteyenearprism.Value.Trim());

            if (righteyenearcylinder.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyenearcylinder.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeNearCylinder = Double.Parse(righteyenearcylinder.Value.Trim());

            if (righteyenearaxis.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(righteyenearaxis.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.rightEyeNearAxis = Double.Parse(righteyenearaxis.Value.Trim());

            if (lefteyenearpd.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyenearpd.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.PD_Near = Double.Parse(lefteyenearpd.Value.Trim());

            if (lefteyedistancepd.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyedistancepd.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.PD_Distance = Double.Parse(lefteyedistancepd.Value.Trim());

            if (lefteyedistanceva.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyedistanceva.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeDistanceVA = Double.Parse(lefteyedistanceva.Value.Trim());

            if (lefteyedistancesphere.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyedistancesphere.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeDistanceSphere = Double.Parse(lefteyedistancesphere.Value.Trim());

            if (lefteyedistanceprism.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyedistanceprism.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeDistancePrism = Double.Parse(lefteyedistanceprism.Value.Trim());

            if (lefteyedistancecylinder.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyedistancecylinder.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeDistanceCylinder = Double.Parse(lefteyedistancecylinder.Value.Trim());

            if (lefteyedistanceaxis.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyedistanceaxis.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeDistanceAxis = Double.Parse(lefteyedistanceaxis.Value.Trim());

            if (lefteyenearva.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyenearva.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeNearVA = Double.Parse(lefteyenearva.Value.Trim());

            if (lefteyenearsphere.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyenearsphere.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeNearSphere = Double.Parse(lefteyenearsphere.Value.Trim());

            if (lefteyenearprism.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyenearprism.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeNearPrism = Double.Parse(lefteyenearprism.Value.Trim());

            if (lefteyenearcylinder.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyenearcylinder.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeNearCylinder = Double.Parse(lefteyenearcylinder.Value.Trim());

            if (lefteyenearaxis.Value.Trim().Length > 0)
                if (System.Text.RegularExpressions.Regex.IsMatch(lefteyenearaxis.Value.Trim(), @"^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$"))
                    request.leftEyeNearAxis = Double.Parse(lefteyenearaxis.Value.Trim());
    }
        Double[] detail_EstimatedCost = new Double[DT.Rows.Count];
        String[] detail_ServiceDescription = new String[DT.Rows.Count];
        String[] detail_ServiceCode = new String[DT.Rows.Count];
        String[] detail_RegularLensesType = new String[DT.Rows.Count];
        Int64[] detail_Quantity = new Int64[DT.Rows.Count];
        int[] detail_ItemNo = new int[DT.Rows.Count];
        String[] detail_BenefitHead = new String[DT.Rows.Count];
        String[] detail_ContactLensesType = new String[DT.Rows.Count];

        String[] detail_Varilux = new String[DT.Rows.Count];
        String[] detail_SingleVision = new String[DT.Rows.Count];
        String[] detail_SafetyThickness = new String[DT.Rows.Count];
        String[] detail_Photosensitive = new String[DT.Rows.Count];
        String[] detail_MultiCoated = new String[DT.Rows.Count];
        String[] detail_Medium = new String[DT.Rows.Count];
        String[] detail_Light = new String[DT.Rows.Count];
        String[] detail_Lenticular = new String[DT.Rows.Count];
        String[] detail_HighIndex = new String[DT.Rows.Count];
        String[] detail_Dark = new String[DT.Rows.Count];
        String[] detail_Colored = new String[DT.Rows.Count];
        String[] detail_Bifocal = new String[DT.Rows.Count];
        String[] detail_Aspheric = new String[DT.Rows.Count];
        String[] detail_AntiReflectingCoating = new String[DT.Rows.Count];
        String[] detail_Anti_scratch = new String[DT.Rows.Count];
        String[] detail_EyeTestReading = new String[DT.Rows.Count];
        String[] detail_EyeTestReadingLeft = new String[DT.Rows.Count];
        String[] detail_EyeTestReadingRight = new String[DT.Rows.Count];
        String[] detail_ServType = new String[DT.Rows.Count];
        

        for (int i = 0; i <= r; i++)
        {
            strServDesc = (String)DT.Rows[i][0];
            strPeriod = (String)DT.Rows[i][1];
            strDateFrom = (String)DT.Rows[i][2];
            strDateTo = (String)DT.Rows[i][3];
            strQuantity = (String)DT.Rows[i][4];
            strCost = (String)DT.Rows[i][5];
            if (DT.Rows[i][6] != "") code = (String)DT.Rows[i][6];
            ServiceCode = (String)DT.Rows[i][9];
            //strteethMap = (String)DT.Rows[i][7]; commented for cr108 as the statement if not applicable

            if (DT.Rows[i][9].ToString() == "O5") detail_ContactLensesType.SetValue((String)DT.Rows[i][8], i);
            GridViewRow gr = GridView1.Rows[i];
            CheckBox check = (CheckBox)gr.FindControl("Chec");

            detail_ServiceDescription.SetValue(strServDesc, i);
            detail_ServiceCode.SetValue(ServiceCode, i);
            detail_Quantity.SetValue(Int64.Parse(strQuantity), i);
            detail_ItemNo.SetValue(i + 1, i);
            detail_EstimatedCost.SetValue(Double.Parse(strCost), i);
            if (!BLCommon.HRMEnabled)
            { detail_BenefitHead.SetValue(DT.Rows[i][10].ToString(), i); }
            else
            {
                if (Convert.ToString(DT.Rows[i][10]) == "##OF##")
                    detail_BenefitHead.SetValue("O6", i);
                else 
                    detail_BenefitHead.SetValue("", i);
            }
            detail_Varilux.SetValue("N", i);
            detail_SingleVision.SetValue("N", i);// else detail_Varilux.SetValue("", i);
            detail_SafetyThickness.SetValue("N", i);
            detail_Photosensitive.SetValue("N", i);
            detail_MultiCoated.SetValue("N", i);
            detail_Medium.SetValue("N", i);
            detail_Light.SetValue("N", i);
            detail_Lenticular.SetValue("N", i);
            detail_HighIndex.SetValue("N", i);
            detail_Dark.SetValue("N", i);
            detail_Colored.SetValue("N", i);
            detail_Bifocal.SetValue("N", i);
            detail_Aspheric.SetValue("N", i);
            detail_AntiReflectingCoating.SetValue("N", i);
            detail_Anti_scratch.SetValue("N", i);
            detail_EyeTestReading.SetValue("N", i);
           // detail_EyeTestReadingLeft.SetValue(Convert.ToString(DT.Rows[i][12]), i);
           // detail_EyeTestReadingRight.SetValue(Convert.ToString(DT.Rows[i][13]), i);
            

            ////New Code Start Lens & Eye reading value not passing to CAESAR issue fixed.
            string eyeTestReadingLeft = (Convert.ToString(DT.Rows[i][12]) == "0" || Convert.ToString(DT.Rows[i][12]) == string.Empty) ? "" : Convert.ToString(DT.Rows[i][12]);
            string eyeTestReadingRight = (Convert.ToString(DT.Rows[i][13]) == "0" || Convert.ToString(DT.Rows[i][13]) == string.Empty) ? "" : Convert.ToString(DT.Rows[i][13]);
            detail_EyeTestReadingLeft.SetValue(eyeTestReadingLeft, i);
            detail_EyeTestReadingRight.SetValue(eyeTestReadingRight, i);
            ////New code End
            ////Old code Start
            ////detail_EyeTestReadingLeft.SetValue(Convert.ToString(DT.Rows[i][12]) == "0" ? "" : Convert.ToString(DT.Rows[i][12]), i);
            ////detail_EyeTestReadingRight.SetValue(Convert.ToString(DT.Rows[i][13]) == "0" ? "" : Convert.ToString(DT.Rows[i][13]), i);
            ////Old code end
            
            
            detail_ServType.SetValue("OS", i);

            BupaCodeEntity objBupaCodeEntity = GetBupaCodeEntity(ServiceCode);

            ////if (objBupaCodeEntity.BenefitHead == "O5" || DT.Rows[i][9].ToString() == "O5" )
            if (objBupaCodeEntity.BenefitHead == "O5" || DT.Rows[i][9].ToString() == "O5" || DT.Rows[i][10].ToString() == "O5")
            {

                ////New code Start Lens & Eye reading value not passing to CAESAR issue fixed.
                string contactLensesType = string.Empty;
                contactLensesType = Convert.ToString(DT.Rows[i][8]).Trim() == "" ? "0" : Convert.ToString(DT.Rows[i][8]);

                detail_ContactLensesType.SetValue(contactLensesType, i);
                ////New code Start
                ////Old code Start
                ////detail_ContactLensesType.SetValue((String)DT.Rows[i][8], i);
                ////Old code end
            }
            

          

            if (objBupaCodeEntity.BenefitHead == "O3" || DT.Rows[i][9].ToString() == "O3" || DT.Rows[i][10].ToString() == "O3" )
            //ServiceCode//dr[6] contains lens specification
            //if (DT.Rows[i][9].ToString() == "O3")
            {
              
                detail_RegularLensesType.SetValue((String)DT.Rows[i][8], i);

                if (code.Contains("Varilux")) detail_Varilux.SetValue("Y", i); else detail_Varilux.SetValue("N", i);
                if (code.Contains("Singlevision")) detail_SingleVision.SetValue("Y", i); else detail_SingleVision.SetValue("N", i);// else detail_Varilux.SetValue("", i);
                if (code.Contains("Safetythickness")) detail_SafetyThickness.SetValue("Y", i); else detail_SafetyThickness.SetValue("N", i);
                if (code.Contains("Photosensitive")) detail_Photosensitive.SetValue("Y", i); else detail_Photosensitive.SetValue("N", i);
                if (code.Contains("Multicoated")) detail_MultiCoated.SetValue("Y", i); else detail_MultiCoated.SetValue("N", i);
                if (code.Contains("Medium")) detail_Medium.SetValue("Y", i); else detail_Medium.SetValue("N", i);
                if (code.Contains("light")) detail_Light.SetValue("Y", i); else detail_Light.SetValue("N", i);
                if (code.Contains("Lenticular")) detail_Lenticular.SetValue("Y", i); else detail_Lenticular.SetValue("N", i);
                if (code.Contains("Highindex")) detail_HighIndex.SetValue("Y", i); else detail_HighIndex.SetValue("N", i);
                if (code.Contains("Dark")) detail_Dark.SetValue("Y", i); else detail_Dark.SetValue("N", i);
                if (code.Contains("Colored")) detail_Colored.SetValue("Y", i); else detail_Colored.SetValue("N", i);
                if (code.Contains("Bifocal")) detail_Bifocal.SetValue("Y", i); else detail_Bifocal.SetValue("N", i);
                if (code.Contains("Aspheric")) detail_Aspheric.SetValue("Y", i); else detail_Aspheric.SetValue("N", i);
                if (code.Contains("Antireflectingcoating")) detail_AntiReflectingCoating.SetValue("Y", i); else detail_AntiReflectingCoating.SetValue("N", i);
                if (code.Contains("Antiscratch")) detail_Anti_scratch.SetValue("Y", i); else detail_Anti_scratch.SetValue("N", i);
            }

          
        }

        request.detail_Varilux = detail_Varilux;
        request.detail_SingleVision = detail_SingleVision;
        request.detail_SafetyThickness = detail_SafetyThickness;
        request.detail_Photosensitive = detail_Photosensitive;
        request.detail_MultiCoated = detail_MultiCoated;
        request.detail_Medium = detail_Medium;
        request.detail_Light = detail_Light;
        request.detail_Lenticular = detail_Lenticular;
        request.detail_HighIndex = detail_HighIndex;
        request.detail_Dark = detail_Dark;
        request.detail_Colored = detail_Colored;
        request.detail_Bifocal = detail_Bifocal;
        request.detail_Aspheric = detail_Aspheric;
        request.detail_AntiReflectingCoating = detail_AntiReflectingCoating;
        request.detail_Anti_scratch = detail_Anti_scratch;

        request.detail_ServiceDescription = detail_ServiceDescription;
        request.detail_ServiceCode = detail_ServiceCode;
        request.detail_RegularLensesType = detail_RegularLensesType;
        request.detail_Quantity = detail_Quantity;
        request.detail_ItemNo = detail_ItemNo;
        request.detail_EstimatedCost = detail_EstimatedCost;
        request.detail_ContactLensesType = detail_ContactLensesType;
        request.detail_BenefitHead = detail_BenefitHead;
        //request.detail_EyeTestReading=detail_EyeTestReading;
        request.detail_EyeTestReadingRight=detail_EyeTestReadingRight;
        request.detail_EyeTestReadingLeft = detail_EyeTestReadingLeft;
        request.detail_ServType = detail_ServType;

        ////request.detail = preauthSupportDocuments(uploadSession.Text, _uploadCategory, request.transactionID);
        request.detail = preauthSupportDocuments(Convert.ToString(Session["UploadSession"]), _uploadCategory, request.transactionID);


        try
        {

            StringBuilder sb = new StringBuilder(200);

            //ExceptionManager.TrackWSPerformance("Preauth13_3", "ReqOptInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), strProviderID, "", "");
            //Strat
            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.OpticalInfoRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.OpticalInfoRequest_DN>();
            //XmlReq.Request(request, "OpticalInfoRequest");
            response = ws.ReqOptInfo(request);
            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.OpticalInfoResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.OpticalInfoResponse_DN>();
            //XmlResp.Response(response, "OpticalInfoResponse");
            //End
          
            
            //ExceptionManager.TrackWSPerformance("Preauth13_3", "ReqOptInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), strProviderID, "", "");


            if (response.status != "0")
            {
                if (response.ext_Ind == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "$.modal.close();');");
                    litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }
                for (int i = 0; i < response.errorMessage.Length; i++)
                    sb.Append(response.errorMessage[i]).Append("\n");
                lblNoTreatmentDetError.Text = sb.ToString();
                lblNoTreatmentDetError.Visible = true;
            }
            else
            {
                // Send the email
                string provider = Session["ProviderName"].ToString();
                string username = Session["ProviderUserName"].ToString();

                var queryString = "PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR";
                queryString = Cryption.Encrypt(queryString);

                Notify(provider, username, response.preAuthorizationID.ToString());
                if (response.ext_Ind == "Y")
                {
                    ////aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR');");
                    aCloseOverlay.Attributes.Add("onclick", "goto('PreauthDetails.aspx?val=" + queryString + "');");
                    litExtendedDate.Text = ((DateTime)response.ext_Date).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }
                else
                {
                    ////Response.Redirect("PreauthDetails.aspx?PID=" + response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1&RType=NR", false);
                    Response.Redirect("PreauthDetails.aspx?val=" + queryString, false);
                }


                Message1.Text = "Success";
            }
        }
        catch (Exception ex)
        {

            Message1.Text += "\n" + ex.Message;
            //ExceptionManager.LogException(ex);
        }
        # endregion

    }



    private long ValidateAge()
    {
        long strAge;
        OS_DXC_WAP.CaesarWS.RequestMbrAgeRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestMbrAgeRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestMbrAgeResponse_DN response;
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.provCode = strProviderID;
        request.membershipNo = long.Parse(txtMembershipNo.Text);
        request.transactionID = TransactionManager.TransactionID();
        StringBuilder sb = new StringBuilder(200);
        response = ws.RequestMbrAge(request);
        if (response.errorID[0] != "0")
        {
            for (int i = 0; i < response.errorID.Length; i++)
                sb.Append(response.errorMessage[i]).Append("\n");
            strAge = response.mbrAge;
        }
        else
        {
            strAge = response.mbrAge;
        }
        return strAge;
    }

    public void ProcessFollowUp(string PreauthID)
    {
        OS_DXC_WAP.CaesarWS.RequestMemberInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestMemberInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestMemberInfoResponse_DN response;

        dvMultilevelddl.Visible = false;

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();
        request.preAuthorisationID = long.Parse(PreauthID);
        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.ReqMbrInfo(request);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.RequestMemberInfoRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.RequestMemberInfoRequest_DN>();
            //XmlReq.Request(request, "RequestMemberInfoRequest_DN");

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.RequestMemberInfoResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.RequestMemberInfoResponse_DN>();
            //XmlResp.Response(response, "RequestMemberInfoResponse_DN");

            if (response.status.Trim() != "0")
            {
                UPnlDiagnosis.Visible = false;
                Pane1.Visible = false;
                Panel1.Visible = false;
                PnlVerify.Visible = false;
                Message1.Text = response.errorMessage;
            }
            else
            {
                if (response.checkUp == "Y") chkCheckup.Checked = true;
                if (response.chronic == "Y") chkChronic.Checked = true;
                if (response.congenital == "Y") chkCongenital.Checked = true;
                if (response.infertility == "Y") chkInfertility.Checked = true;
                if (response.psychiatric == "Y") chkPsychiatric.Checked = true;
                if (response.RTA == "Y") chkRTA.Checked = true;
                if (response.vaccination == "Y") chkVaccination.Checked = true;
                if (response.workRelated == "Y") chkWorkRelated.Checked = true;

                ddlDepartment.SelectedValue = response.department;
                lbldepartment1.Text = response.department;
                ddlGender.Text = response.gender;
                ddlTreatmentType.SelectedValue = response.treatmentType;
                txtAge.Text = response.age.ToString();
                txtBloodPressure.Text = response.bloodPressure;
                if (response.cardIssueNumber > 0)
                    txtCardIssueNo.Text = response.cardIssueNumber.ToString();
                txtChiefComplaintsAndMainSymptoms.Text = response.chiefComplaintsAndMainSymptoms;
                txtDiagCode.Text = response.diagnosisCode;
                txtDiagnosisDescription.Text = response.diagnosisDescription;
                txtDurationOfIllness.Text = response.durationOfIllness;
                if (response.lastMenstruationPeriod.ToString().Length < 11)
                    txtLastMenstruationPeriod.Text = response.lastMenstruationPeriod.ToString();
                else
                    txtLastMenstruationPeriod.Text = response.lastMenstruationPeriod.ToString().Substring(0, 10);
                txtLengthOfStay.Text = response.lengthofStay;
                txtMemberID.Text = response.memberID_Igama;
                txtMobileNo.Text = response.memberMobileNo;
                txtMemberName.Text = response.memberName;
                txtMembershipNo.Text = response.membershipNo;
                txtOthers.Text = response.otherConditions;
                txtPatientFileNo.Text = response.patientFileNo;
                if (response.physician_Name != null && response.physician_Name.IndexOf('&') > -1)
                {
                    txtNameOfPhysician.Text = RemoveSpecialCharacters(response.physician_Name.Replace("&", ""));
                }
                else
                {
                    txtNameOfPhysician.Text = response.physician_Name;
                }
                txtPolicyNo.Text = response.policyNo;
                txtPossibleLineOfTreatment.Text = response.possibleLineOfTreatment;
                txtPulse.Text = response.pulse;
                txtTemperature.Text = response.temperature;
                lblMemberName.Text = "<br><nobr>Name: - <font size=3> " + response.memberName + "</font>  Age: <font size=3> - " + response.age.ToString() + " years  - ";
                if (response.gender == "F")
                    lblMemberName.Text += " ( Female )</font></nobr>";
                if (response.gender == "M")
                    lblMemberName.Text += " ( Male )</font></nobr>";
                UPnlDiagnosis.Visible = true;
                PnlDiagnosisCommon.Visible = true;
                PnlStandard.Visible = true;
                PnlStandardCheckList.Visible = true;
                PnlDentalStandardCheckList.Visible = true;
                PnlVerify.Enabled = false;
                Panel1.Enabled = false;
                UPnlTreatmentDets.Visible = true;
                updatePanel.Visible = true;
                txtServiceDesc.Visible = true;
                ddlServiceDesc.Visible = false;
                txtServiceDesc.Visible = true;
                txtServiceDesc.Style["display"] = "block";
                ddlServiceDesc.Visible = false;
                txtMinor.Text = "false";
            }
            lblval.Text = val.Value;
        }
        catch (Exception ex)
        {
            Message1.Text += "\n" + ex.Message;
        }
    }

    protected void btnRequestMedication_Click(object sender, EventArgs e)
    {
        OS_DXC_WAP.CaesarWS.RequestMyMedicationRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestMyMedicationRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestMyMedicationResponse_DN response;
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();
        request.membershipNo = long.Parse(txtMembershipNo.Text);
        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.ReqMyMedication(request);
            if (response.errorID[0] != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i] + "\n");
                Message1.Text = sb.ToString();
            }
            else
            {
                txtSumQuantity.Text = "0";
                txtSumCost.Text = "0";
                TotalQuantity = 0;
                TotalCost = 0;
                DT = (DataTable)Session["dt"];
                DT.Clear();
                for (int i = 0; i < response.detail.Length; i++)
                {
                    DataRow dr = DT.NewRow();
                    dr[0] = response.detail[i].servDesc;
                    dr[1] = response.detail[i].supplyPeriod.ToString();
                    dr[2] = response.detail[i].supplyFrom.ToString().Substring(0, 10);
                    dr[3] = response.detail[i].supplyTo.ToString().Substring(0, 10);
                    dr[4] = response.detail[i].cost.ToString();
                    dr[5] = response.detail[i].estimateAmount.ToString("0.00");
                    dr[6] = response.detail[i].diagCode.Trim();
                    dr[7] = response.detail[i].estimateAmount.ToString("0.00");
                    dr[8] = "1";
                    dr[9] = response.detail[i].servCode;
                    dr[10] = response.detail[i].diagDesc;
                    DT.Rows.Add(dr);
                    TotalCost = Convert.ToDecimal(txtSumCost.Text) + Convert.ToDecimal(dr[5].ToString());
                    txtSumCost.Text = Convert.ToString(TotalCost);
                    TotalQuantity = Convert.ToInt32(txtSumQuantity.Text) + Convert.ToInt32(dr[4].ToString());
                    txtSumQuantity.Text = Convert.ToString(TotalQuantity);
                }
                GridView1.DataSource = DT;
                GridView1.Columns[9].Visible = true;
                txtReqMed.Text = "true";
                GridView1.DataBind();
                GridView1.Visible = true;
                updatePanel.Visible = true;
                ddlPharmacyDiag.Enabled = false;
                btnSubmit.Visible = true;
                txtSumCost.Visible = true;
                txtSumQuantity.Visible = true;
                strReqMed = "true";
                lblReqMedMessage.Visible = true;
                btnSubmit.ToolTip = "Note: Kindly review and 'uncheck' the medications that you would not like to include on the above list.";
            }
        }
        catch (Exception ex)
        {
            Message1.Text += "\n" + ex.Message;
        }
    }

    protected void btnVerificationID_Click(object sender, EventArgs e)
    {
        OS_DXC_WAP.CaesarWS.ReqMemberInfoByVIDRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqMemberInfoByVIDRequest_DN();
        OS_DXC_WAP.CaesarWS.ReqMemberInfoByVIDResponse_DN response;
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = TransactionManager.TransactionID();
        request.verificationID = long.Parse(txtVerificationID.Text);
        request.provCode = txtVerificationID.Text;
        try
        {
            StringBuilder sb = new StringBuilder(200);
            response = ws.ReqMemberInfoByVID(request);
            if (response.errorID[0] != "0")
            {
                for (int i = 0; i < response.errorID.Length; i++)
                    sb.Append(response.errorMessage[i] + "\n");
                Message1.Text = sb.ToString();
                if (response.errorID[0] == "E3030" || response.errorID[0] == "E0006")
                    btnProceed.Enabled = false;
                Panel1.Enabled = true;
            }
            else
            {
                PnlVerify.Enabled = false;
                Message1.Text = "";
                btnProceed.Enabled = true;
                Panel1.Enabled = true;
                ddlGender.Text = response.gender;
                txtAge.Text = response.age.ToString();
                txtCardIssueNo.Text = response.cardIssueNo.ToString();
                ddlDepartment.SelectedValue = response.deptType;
                txtMobileNo.Text = response.mbrMoblie;
                txtMemberName.Text = response.mbrName;
                txtMembershipNo.Text = response.mbrshipNo;
                txtMembershipNo.Enabled = false;
                txtMemberName.Enabled = false;
                txtAge.Enabled = false;
                ddlDepartment.Enabled = false;
                ddlGender.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            Message1.Text += "\n" + ex.Message;
        }
    }

    int TotalQuantity1;
    public int GetQuantity(int Quantity)
    {
        TotalQuantity1 += Quantity;
        return Quantity;
    }

    public int GetTotalQuantity()
    {
        return TotalQuantity1;
    }

    decimal TotalCost1;
    public decimal GetCost(decimal Cost)
    {
        TotalCost1 += Cost;
        return Cost;
    }

    public decimal GetTotalCost()
    {
        return TotalCost1;
    }

    private void DropDownListSort(ref DropDownList DLL)
    {
        ArrayList textList = new ArrayList();
        ArrayList valueList = new ArrayList();
        foreach (ListItem li in DLL.Items)
        {
            textList.Add(li.Text);
        }
        textList.Sort();
        foreach (object item in textList)
        {
            string value = DLL.Items.FindByText(item.ToString()).Value;
            valueList.Add(value);
        }
        DLL.Items.Clear();

        for (int i = 0; i < textList.Count; i++)
        {
            ListItem objItem = new ListItem(textList[i].ToString(), valueList[i].ToString());
            DLL.Items.Add(objItem);
        }
    }

    protected void ddlServiceDesc_DataBound(object sender, EventArgs e)
    {
        if (ddlDepartment.Text != "DEN")
            DropDownListSort(ref this.ddlServiceDesc);
    }

    protected void ddlServiceDesc1_DataBound(object sender, EventArgs e)
    {
        if (ddlDepartment.Text == "DEN")
        {
            DropDownListSort1(ref this.ddlServiceDesc, ref this.ddlServiceDesc1);
        }
    }

    private void DropDownListSort1(ref DropDownList DLL, ref DropDownList DLL1)
    {
        ArrayList textList = new ArrayList();
        ArrayList valueList = new ArrayList();
        foreach (ListItem li in DLL1.Items)
        {
            textList.Add(li.Text);
        }
        textList.Sort();
        foreach (object item in textList)
        {
            string value = DLL1.Items.FindByText(item.ToString()).Value;
            valueList.Add(value);
        }
        for (int i = 0; i < textList.Count; i++)
        {
            ListItem objItem = new ListItem(textList[i].ToString(), valueList[i].ToString());
            DLL.Items.Add(objItem);
        }
    }
    private void NotifyExecutive(string provider, string username, Member member) // CR
    {

        try
        {
            DataTable dtExecutiveList = BLCommon.GetData("select * from ExecutiveMemberList where membershipno ='" + member.MembershipNumber + "'  and Active=1");
            if (dtExecutiveList.Rows.Count > 0)
            {
                DTOptions = (DataTable)Session["dtOptions"];
                DTOptions = RemoveNullOrEmptyRows(DTOptions);
                DT = (DataTable)Session["dt"];

                // Send an email informing the team about a pre-auth In-Patient request 

                string sRMExecutiveFromEmail = ConfigurationManager.AppSettings["RMExecutiveFromEmail"];
                string sRMExecutiveToEmail = ConfigurationManager.AppSettings["RMExecutiveToEmail"];
                string sRMExecutiveSubject = ConfigurationManager.AppSettings["RMExecutiveSubject"];


                StringBuilder sbContent = new StringBuilder();
                sbContent.Append("<b>Dear</b> HCO/TOC/Marketing, ");

                sbContent.Append("<br><br><b>This email is :</b>");
                sbContent.Append("<br>1- To create instant notifications to the HCO team as soon as any RM executive is admitted.");
                sbContent.Append("<br>2- Create notification to the Marketing team to initiate hospitalization gift delivery.");
                sbContent.Append("<br>3- To conduct live health coaching to the Executives regarding their hospitalization.");

                sbContent.Append("<br><br><b><u>Visitors Details</u></b>");

                sbContent.Append("<br><br><b>Member�s Name : </b>" + member.MemberName);
                sbContent.Append("<br><b>Membership number : </b>" + member.MembershipNumber);
                sbContent.Append("<br><b>Client Name : </b>" + member.CompanyName);
                sbContent.Append("<br><b>Mobile number :</b> " + member.MOBILE);
                sbContent.Append("<br><b>Provider name :</b> " + provider);
                sbContent.Append("<br><br><b>Service Requested : </b>");
                sbContent.Append("<br><table style=\" border-collapse: collapse; border: 1px solid skyblue;\"><tr><th  style=\" border-collapse: collapse; border: 1px solid skyblue;\">Diagnosis code </td><th  style=\" border-collapse: collapse; border: 1px solid skyblue;\">Service description</td></tr>");
                foreach (DataRow dr in DT.Rows)
                {
                    sbContent.Append("<tr><td  style=\" border-collapse: collapse; border: 1px solid skyblue;\">" + dr["ServiceCode"].ToString() + "</td><td  style=\" border-collapse: collapse; border: 1px solid skyblue;\">" + dr["ServiceDesc"].ToString() + "</td></tr>");

                }

                sbContent.Append("</table>");
                sbContent.Append("<br><br><b>Thank You</b><br>Online Services");


                // Get the addressees
                string[] addressees = sRMExecutiveToEmail.Split(';');
                if (addressees.Length <= 0)
                {
                    addressees = sRMExecutiveToEmail.Split(',');
                }
                foreach (string toEmail in addressees)
                {
                    MailMessage mail = new MailMessage(sRMExecutiveFromEmail, toEmail, sRMExecutiveSubject, sbContent.ToString());
                    BLCommon.SendEmail(mail);
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    private void Notify(string provider, string username, string preauthID)
    {
        // Send an email informing the team about a pre-auth request
        string fromEmail = "donotreply@bupa.com.sa";

        string subject = "Pre-Auth reference:" + preauthID + " - attachments added by " + username + " from " + provider;
        string content = "Please view the attached uploaded documents added by " + username + " from " + provider + " for pre-auth reference: " + preauthID;

        // Get the list of attachments for this user
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(_InitialRequestID, Bupa.OSWeb.Helper.UploadCategory.PREAUTH_ADD);

        StringCollection attachments = new StringCollection();
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow r in d.Tables[0].Rows)
            {
                string attachPath = r["VirtualPath"].ToString();
                attachments.Add(attachPath);
            }
        }
        else
        {
            // No attachments and therefore, nothing to email
            return;
        }

        // Get the addressees
        string[] addressees = UploadPublication.PreAuthEmailAddresses();
        foreach (string toEmail in addressees)
        {
            SendEmail(fromEmail, toEmail, subject, content, attachments);
        }
    }

    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        try
        {
            // Setup the mail message
            fromEmail = "NoReply.os-uat@bupa.com.sa";
            MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

            // Deal with the attachments
            if (attachmentPaths != null)
            {
                foreach (string path in attachmentPaths)
                {
                    // Create the attachment
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(path));

                    // Add the attachment
                    mail.Attachments.Add(attachment);
                }
            }

            string smtpHost = WebPublication.EmailRelayAddress();
            int smtpPort = WebPublication.EmailRelayPort();

            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);

            System.Net.NetworkCredential cred = new System.Net.NetworkCredential();

            cred.UserName = Convert.ToString(ConfigurationManager.AppSettings["EmailUserName"]);
            cred.Password = Convert.ToString(ConfigurationManager.AppSettings["EmailUserPassword"]);
            //cred.Domain = "mail.myhost.com";
            smtp.Credentials = cred;
            smtp.UseDefaultCredentials = false;

            mail.IsBodyHtml = true;
            smtp.Send(mail);
        }
        catch (Exception ex) { }
    }

    public PreauthDoc_DN[] preauthSupportDocuments(string uploadSessionID, string function, long Transaction)
    {


        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedPreauthFileSet(uploadSessionID, function);
        OS_DXC_WAP.CaesarWS.PreauthDoc_DN[] _supportDocuments = new OS_DXC_WAP.CaesarWS.PreauthDoc_DN[0];
        int _count = 0;
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            _supportDocuments = new PreauthDoc_DN[d.Tables[0].Rows.Count];
            WebPublication _webPub = new WebPublication();
            foreach (DataRow r in d.Tables[0].Rows)
            {

                string attachPath = r["VirtualPath"].ToString();
                attachPath = attachPath.Replace("/", "\\");
                attachPath = attachPath.Replace("~", "");
                attachPath = attachPath.Replace("\\preauthUpload\\", "\\");
                _supportDocuments[_count] = new PreauthDoc_DN();
                _supportDocuments[_count].DOC_PATH = _webPub.DocLocation() + attachPath;
                _supportDocuments[_count].sql_type = "CSR.PREA_REQ_DOC01";
                _supportDocuments[_count].TXN_ID = Transaction;
                _count++;
            }
            return _supportDocuments;
        }
        else
        {
            return null;
        }

    }

    public static string RemoveSpecialCharacters(string str)
    {
        StringBuilder sb = new StringBuilder();
        foreach (char c in str)
        {
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == ' ' || c == '_' || c == '.')
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {

        LoadCategoryOptions();
        if (lblServiceDesc.Text != "OTH - Others")
        {
            lblServiceDesc.Text = txtServiceDesc.Text;
            dvlblServiceDesc.InnerHtml = txtServiceDesc.Text;
            dvlblServiceDesc.Visible = false;
        }
        else
        {
            // txtServiceDesc.Visible = false;
            // lblServiceDesc.Text = txtServiceDesc.Text;
        }

        if (lblServiceDesc.Text == "BRO - 1 day Bronchial Asthma Admission")
        {
            Message1.Text = "Attach lab test results/upload documents";
            Message1.Visible = true;

        }
        else
        {
            Message1.Text = "";
            Message1.Visible = false;
        }

    }

    public void LoadCategoryOptions()
    {
        try
        {
            LoadControl(ddlCategory.SelectedValue.ToString());
            txtServiceDesc.Text = ddlCategory.SelectedValue.ToString();

            if (txtServiceDesc.Text.Contains("DLS -"))
            {
                dvCatOptionsForm.Visible = false;

            }
            else
            {
                dvCatOptionsForm.Visible = true;
            }
            if (txtServiceDesc.Text.Contains("OTH"))
            {
                dvCatOptionsForm.Visible = false;
                lblServiceDesc.Text = txtServiceDesc.Text;
                txtServiceDesc.Text = "";
                txtServiceDesc.Visible = true;
            }
            else
            {
                txtServiceDesc.Visible = false;
                lblServiceDesc.Text = txtServiceDesc.Text;
            }


            if (!string.IsNullOrEmpty(Session["preAuthOptStatus"].ToString()))
            {
                preAuthOptStatus = Session["preAuthOptStatus"].ToString();
            }
            if (preAuthOptStatus == "F")
            {
                //dvCatOptionsForm.InnerHtml = "<P style=' width:800px;overflow:auto;background-color: lightgreen;'><center><b>Member Preauth Option : Full Cover � No Exclusion</b></center></p>";
                //dvCatOptionsForm.Visible = true;


            }
            else if (preAuthOptStatus == "N")
            {

                //dvCatOptionsForm.InnerHtml = "<P style='width:800px;overflow:auto;background-color: lightgreen;'><center><b>Member Preauth Option : No Pre-authorization Required</b></center></p>";
                //dvCatOptionsForm.Visible = true;


            }
            else
            {

                //dvCatOptionsForm.InnerHtml = "<P style=' width:800px;overflow:auto;background-color: lightgreen;'><center><b>Member Preauth Option : Standard</b></center></p>";
                //dvCatOptionsForm.Visible = true;
                //dvMultilevelddl.Visible=false;
            }
        }
        catch (Exception ex) { }

    }


    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        trOtherPhysicianName.Visible = false;
        txtOtherPhysicianName.Text = string.Empty;
        List<ProviderPhysiciansManagement> objProviderPhysiciansManagementList = WebApp.CommonClass.GetTrustedDoctorList(Session["ProviderID"].ToString(), ddlDepartment.SelectedValue);
        Session["ProviderPhysiciansManagementList"] = objProviderPhysiciansManagementList;
        if (objProviderPhysiciansManagementList != null)
        {
            if (objProviderPhysiciansManagementList.Count == 0)
            {
                tdPhysicianTextBox.Visible = true;
                tdPhysicianDropDown.Visible = false;
            }
            else
            {
                tdPhysicianTextBox.Visible = false;
                tdPhysicianDropDown.Visible = true;
                ddlPhysician.DataSource = objProviderPhysiciansManagementList;
                ddlPhysician.DataTextField = "PPM_PhysicianName";
                ddlPhysician.DataValueField = "PPM_PhysicianName";
                ddlPhysician.DataBind();
                ddlPhysician.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlPhysician.Items.Insert(1, new ListItem("Others", "Others"));
            }
        }
        else
        {
            tdPhysicianTextBox.Visible = true;
            tdPhysicianDropDown.Visible = false;
        }
    }

    protected void ddlPhysician_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPhysician.SelectedValue == "Others")
        {
            trOtherPhysicianName.Visible = true;
        }
        else
        {
            trOtherPhysicianName.Visible = false;
        }
    }

    public int IsFollowUpRequest
    {
        get
        {
            if (ViewState["IsFollowUpRequestAlert"] != null)
                return Convert.ToInt32(ViewState["IsFollowUpRequestAlert"]);
            else
                return 0;
        }
        set { ViewState["IsFollowUpRequestAlert"] = value; }
    }

    public void ReloadServiceList(string sProviderID, string sDepartment, string sServiceDesc, string sServiceCode)
    {
        try 
        {
            if (BLCommon.HRMEnabled)
            {
                BupaCaesar bupaCode = new BupaCaesar();

                if (BLCommon.BupaCodeLists != null)
                {
                    ddlServiceDesc.DataSource = BLCommon.BupaCodeLists;
                }
                else
                {
                    BLCommon.BupaCodeLists = bupaCode.LoadHRMServices(sProviderID, sDepartment, sServiceDesc, sServiceCode);
                    ddlServiceDesc.DataSource = BLCommon.BupaCodeLists;
                }

                //ddlServiceDesc.DataSource = LoadHRMServices();
                ddlServiceDesc.DataTextField = "servDisplay";
                ddlServiceDesc.DataValueField = "servCode";
                ddlServiceDesc.DataBind();


            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnBupaCodeFilter_Click(object sender, EventArgs e)
    {
        ddlServiceDesc.Attributes.Add("style", "display:block");
        lblServiceDescOtherFrame.Attributes.Add("style", "display:none");
        btnSelectService.Attributes.Add("style", "display:block; width:83.5% !important; background-size:15px;");
        tblBupaCodeList.Attributes.Add("style", "display:block; overflow: auto; width: 411px; max-height: 200px; position: relative; top: 0px; border: 1px solid skyblue;");
        rfvddlEyeTestReadingLeft.Enabled = true;
        rfvddlEyeTestReadingRight.Enabled = true;
        vldtrRFddlServDesc.Enabled = true;
        txtCost.Enabled = false;
        txtQuantity.Enabled = true;
        txtQuantity.Text = string.Empty;
        txtCost.Text = string.Empty;        
        hfOtherFrmae.Value = "";
    }
    protected void btnOtherFrame_Click(object sender, EventArgs e)
    {
        ddlServiceDesc.Attributes.Add("style", "display:none");
        lblServiceDescOtherFrame.Attributes.Add("style", "display:block; font-size:12px; margin-bottom:26px;");
        btnSelectService.Attributes.Add("style", "display:none");
        tblBupaCodeList.Attributes.Add("style", "display:none");
        vldtrRFddlServDesc.Enabled = false;
        rfvddlEyeTestReadingLeft.Enabled = false;
        rfvddlEyeTestReadingRight.Enabled = false;
        txtCost.Enabled = true;
        txtQuantity.Enabled = true;
        txtQuantity.Text = string.Empty;
        txtCost.Text = string.Empty;
        hfOtherFrmae.Value = "##OF##";

     
        
    }

    private string GetOldToothNumber(string fdiValue)
    {
        string oldToothNumber = string.Empty;
        try
        {
            switch (fdiValue.ToUpper().Trim())
            {
                case "11": oldToothNumber = "8"; break;
                case "21": oldToothNumber = "9"; break;
                case "22": oldToothNumber = "10"; break;
                case "23": oldToothNumber = "11"; break;
                case "24": oldToothNumber = "12"; break;
                case "25": oldToothNumber = "13"; break;
                case "26": oldToothNumber = "14"; break;
                case "27": oldToothNumber = "15"; break;
                case "28": oldToothNumber = "16"; break;
                case "12": oldToothNumber = "7"; break;
                case "13": oldToothNumber = "6"; break;
                case "14": oldToothNumber = "5"; break;
                case "15": oldToothNumber = "4"; break;
                case "16": oldToothNumber = "3"; break;
                case "17": oldToothNumber = "2"; break;
                case "18": oldToothNumber = "1"; break;
                case "48": oldToothNumber = "32"; break;
                case "47": oldToothNumber = "31"; break;
                case "46": oldToothNumber = "30"; break;
                case "45": oldToothNumber = "29"; break;
                case "44": oldToothNumber = "28"; break;
                case "43": oldToothNumber = "27"; break;
                case "42": oldToothNumber = "26"; break;
                case "41": oldToothNumber = "25"; break;
                case "31": oldToothNumber = "24"; break;
                case "32": oldToothNumber = "23"; break;
                case "33": oldToothNumber = "22"; break;
                case "34": oldToothNumber = "21"; break;
                case "35": oldToothNumber = "20"; break;
                case "36": oldToothNumber = "19"; break;
                case "37": oldToothNumber = "18"; break;
                case "38": oldToothNumber = "17"; break;
                case "X": oldToothNumber = "X"; break;
                case "55": oldToothNumber = "A"; break;
                case "54": oldToothNumber = "B"; break;
                case "53": oldToothNumber = "C"; break;
                case "52": oldToothNumber = "D"; break;
                case "51": oldToothNumber = "E"; break;
                case "61": oldToothNumber = "F"; break;
                case "62": oldToothNumber = "G"; break;
                case "63": oldToothNumber = "H"; break;
                case "64": oldToothNumber = "I"; break;
                case "65": oldToothNumber = "J"; break;
                case "75": oldToothNumber = "K"; break;
                case "74": oldToothNumber = "L"; break;
                case "73": oldToothNumber = "M"; break;
                case "72": oldToothNumber = "N"; break;
                case "71": oldToothNumber = "O"; break;
                case "81": oldToothNumber = "P"; break;
                case "82": oldToothNumber = "Q"; break;
                case "83": oldToothNumber = "R"; break;
                case "84": oldToothNumber = "S"; break;
                case "85": oldToothNumber = "T"; break;
                default:  break;
            }

        }
        catch (Exception)
        {

            
        }

        return oldToothNumber;
    }
}

public class PreAuthCategory
{

    public int CategoryID { get; set; }
    public int CategoryParentID { get; set; }
    public string CategoryCode { get; set; }
    public string Category { get; set; }

}

public static class Helper
{
    public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
    {
        try
        {
            List<T> list = new List<T>();

            foreach (var row in table.AsEnumerable())
            {
                T obj = new T();

                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }

                list.Add(obj);
            }

            return list;
        }
        catch
        {
            return null;
        }
    }

    public class ClassXMLGeneration<T>
    {
        public void Request(T classType, string req)
        {
            System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\CaesarXML\\" + req + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
            xmlSrQ.Serialize(file, classType);
            file.Close();


        }
        public void Response(T classType, string resp)
        {
            System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\CaesarXML\\" + resp + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
            xmlSr.Serialize(file, classType);
            file.Close();
        }
    }


}

