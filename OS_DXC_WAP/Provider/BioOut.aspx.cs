using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Text;
using System.Globalization;


public partial class BioOut : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //ProcessRequests1("http://localhost:25802/MaskedDiv/");

        ///      ProcessRequests1("http://www9.bupame.com/Provider/swipecard/");
        //   ProcessRequests1("http://www3.bupame.com/BioReceiver/");

        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }


        if (Session["Result"] != null)
            Response.Redirect("BO1.aspx");


    }

    public void ProcessRequests1(string prefixes)
    {
        if (!System.Net.HttpListener.IsSupported)
        {
            Console.WriteLine(
                "Windows XP SP2, Server 2003, or higher is required to " +
                "use the HttpListener class.");
            return;
        }
        // URI prefixes are required,
        if (prefixes == null || prefixes.Length == 0)
            throw new ArgumentException("prefixes");

        // Create a listener and add the prefixes.
        System.Net.HttpListener listener = new System.Net.HttpListener();
        //foreach (string s in prefixes)
        //{
        listener.Prefixes.Add(prefixes);
        // }

        try
        {
            // Start the listener to begin listening for requests.
            listener.Start();
            txt1.Text = "Listening...";

            // Set the number of requests this application will handle.
            int numRequestsToBeHandled = 10;

            //for (int i = 0; i < numRequestsToBeHandled; i++)
            //{
            HttpListenerResponse response = null;
            try
            {
                // Note: GetContext blocks while waiting for a request.
                HttpListenerContext context = listener.GetContext();
                txt1.Text += context.Request.Url.Query + "------" + context.Request.Url.Query.Substring(6) + "=======<br/>" + DateTime.Now.ToString(DateTimeFormatInfo.CurrentInfo) + "*********";

                if (context.Request.Url.Query != "")
                {

                   // response.Redirect("http://www9.bupame.com/Provider/swipecard/BioR1.aspx" + context.Request.Url.Query);
                
                
                }


                // Create the response.
                response = context.Response;
                //  context.Request.Url.Query;

                string responseString =
                    "<HTML><BODY>The time is currently " +
                    DateTime.Now.ToString(
                    DateTimeFormatInfo.CurrentInfo) +
                    "</BODY></HTML>";
                byte[] buffer =
                    System.Text.Encoding.UTF8.GetBytes(responseString);
                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
            }
            catch (HttpListenerException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (response != null)
                    response.Close();
            }

          // }
        }
        catch (HttpListenerException ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            // Stop listening for requests.
            listener.Close();
            txt1.Text += "Done Listening.";
        }
    }

}
