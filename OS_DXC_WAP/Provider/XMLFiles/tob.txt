   <form id="form1" runat="server">
        <div>
            <asp:Panel ID="Panel1" runat="server" Width="540px">
                <table style="width: 540px; font-size: small; font-family: Arial;">
                    <tr>
                        <td style="width: 85px" >
                            <span>Verification ID<?xml namespace="" prefix="O" ?></span>
                        </td>
                        <td style="width: 175px">
                            <span>
                                <asp:Label ID="lblVerificationID" runat="server" Width="120px"></asp:Label></span>
                        </td>
                        <td>
                            <span></span>
                        </td>
                        <td style="width: 105px">
                            <span></span>
                        </td>
                        <td style="width: 50px" >
                            <span>Date</span>
                        </td>
                        <td style="width: 125px">
                            <asp:Label ID="lblDate" runat="server" Width="120px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <span>Verification is valid only on the date specified above</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 85px">
                            <span></span>
                        </td>
                        <td style="width: 175px">
                            <span></span>
                        </td>
                        <td>
                            <span></span>
                        </td>
                        <td style="width: 105px">
                            <span></span>
                        </td>
                        <td style="width: 50px">
                            <span></span>
                        </td>
                        <td style="width: 125px">
                            <span></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#00afe1">
                            <span>MEMBER DETAILS</span>
                        </td>
                        <td bgcolor="#00afe1">
                            <span></span>
                        </td>
                        <td colspan="3" bgcolor="#00afe1">
                            <span>POLICY DETAILS</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Member No</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblMemberNo" runat="server" Width="175px"></asp:Label></td>
                        <td>
                            <span></span>
                        </td>
                        <td style="width: 105px" >
                            <span>Customer No:</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblCustomerNo" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Name</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblName" runat="server" Width="175px"></asp:Label></td>
                        <td>
                            <span style="font-size: 10pt; font-family: Arial;"></span>
                        </td>
                        <td style="width: 105px" >
                            <span>Customer Name:</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblCustomerName" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>ID No</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblIDNo" runat="server" Width="175px"></asp:Label></td>
                        <td>
                            <span></span>
                        </td>
                        <td style="width: 105px" >
                            <span>Scheme Name:</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblSchemeName" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Date of Birth</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblDateOfBirth" runat="server" Width="175px"></asp:Label></td>
                        <td>
                            <span></span>
                        </td>
                        <td style="width: 105px" >
                            <span>Effective Date:</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblEffectiveDate" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Member type</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblMemberType" runat="server" Width="175px"></asp:Label></td>
                        <td>
                            <span></span>
                        </td>
                        <td style="width: 105px" >
                            <span>From</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblFrom" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Age</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblAge" runat="server" Width="175px"></asp:Label></td>
                        <td>
                            <span></span>
                        </td>
                        <td style="width: 105px" >
                            <span>To</span>
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblTo" runat="server" Width="175px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Gender</span>
                        </td>
                        <td style="width: 175px">
                            <asp:Label ID="lblGender" runat="server" Width="175px"></asp:Label></td>
                        <td>
                            <span></span>
                        </td>
                        <td style="width: 105px">
                            <span></span>
                        </td>
                        <td style="width: 50px">
                            <span></span>
                        </td>
                        <td style="width: 125px">
                            <span></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 85px; height: 12px;">
                        </td>
                        <td style="width: 175px; height: 12px;">
                        </td>
                        <td style="height: 12px">
                        </td>
                        <td style="width: 105px; height: 12px;">
                        </td>
                        <td style="width: 50px; height: 12px;">
                        </td>
                        <td style="width: 125px; height: 12px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" bgcolor="#00afe1">
                            <span>PROVIDER DETAILS</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Provider</span>
                        </td>
                        <td colspan="5">
                            <span></span>
                            <asp:Label ID="lblProvider" runat="server" Width="320px"></asp:Label>
                            <span></span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Department</span>
                        </td>
                        <td colspan="5">
                            <span>
                                <asp:Label ID="lblDepartment" runat="server" Width="320px"></asp:Label></span>
                            <span></span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Verification result</span>
                        </td>
                        <td colspan="5">
                            <asp:Label ID="lblVerificationResult" runat="server" Width="320px"></asp:Label>
                            <span></span><span></span><span></span>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 85px" >
                            <span>Remarks:</span>
                        </td>
                        <td colspan="5">
                            <asp:Label ID="lblRemarks" runat="server" Width="320px"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div style="font-size: small; font-family: Arial; text-align: left" align="center">
            <asp:Panel ID="Panel5" runat="server" Width="540px">
                <table style="width: 540px; font-size: small; font-family: Arial;">
                    <tr>
                        <td colspan="4" bgcolor="#00afe1">
                            <span style="font-size: 10pt; font-family: Arial">TABLE OF BENEFITS</span></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" align="center" bgcolor="#00afe1">
                            <strong>Benefit type</strong></td>
                        <td style="width: 100px" align="center" bgcolor="#00afe1">
                            <strong>Details</strong></td>
                        <td style="width: 100px" align="center" bgcolor="#00afe1">
                            <strong>Treatment Covered</strong></td>
                        <td style="width: 100px" align="center" bgcolor="#00afe1">
                            <strong>Deductible</strong></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Chronic</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblChronicD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblChronicT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblChronicB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Congenital</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblCongenitalD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblCongenitalT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblCongenitalB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Cosmetic</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblCosmeticD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblCosmeticT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblCosmeticB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Developmental</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblDevelopmentalD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblDevelopmentalT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblDevelopmentalB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Dialysis</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblDialysisD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblDialysisT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblDialysisB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Hazardous Sports Injury</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblHazardousSportsInjuryD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblHazardousSportsInjuryT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblHazardousSportsInjuryB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Natural Changes</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblNaturalChangesD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblNaturalChangesT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblNaturalChangesB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Neo-natal care</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblNeonatalCareD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblNeonatalCareT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblNeonatalCareB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Other Conditions</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblOtherConditionsD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblOtherConditionsT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblOtherConditionsB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Physiotherapy</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblPhysiotherapyD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblPhysiotherapyT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblPhysiotherapyB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Screening</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblScreeningD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblScreeningT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblScreeningB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Vaccinations</td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblVaccinationsD" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblVaccinationsT" runat="server" Width="100px"></asp:Label></td>
                        <td style="width: 100px" align="center">
                            <asp:Label ID="lblVaccinationsB" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px; height: 21px">
                        </td>
                        <td style="width: 100px; height: 21px">
                        </td>
                        <td style="width: 100px; height: 21px">
                        </td>
                        <td style="width: 100px; height: 21px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            Annual limit</td>
                        <td colspan="1">
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lblAnnualLimit" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <em>* This reflects member annual limit not remaining limit<?xml namespace="" prefix="O" ?><?xml
                                namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><?xml
                                    namespace="" prefix="o" ?><?xml namespace="" prefix="o" ?><o:p></o:p></em></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <p>
                                <span style="font-size: 10pt; font-family: Arial">Treatment type</span></p>
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lbTreatmentType" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <p>
                                <span style="font-size: 10pt; font-family: Arial">Hospital Accomodation</span></p>
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lblHospitalAccomodation" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <span style="font-size: 10pt; font-family: Arial">Pre-authorisation limit</span></td>
                        <td colspan="3">
                            <asp:Label ID="lblPreauthorisationLimit" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <p>
                                <span>Referral letter required?</span></p>
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lblReferral" runat="server" Width="100px"></asp:Label></td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <asp:Panel ID="Panel2" runat="server" Width="540px">
                <table style="width: 540px">
                    <tr>
                        <td style="width: 140px" bgcolor="#00afe1">
                        </td>
                        <td style="width: 75px; text-align: center" bgcolor="#00afe1">
                            <span style="font-size: 10pt; font-family: Arial"><strong>Main member (amount)</strong></span></td>
                        <td style="width: 75px; text-align: center" bgcolor="#00afe1">
                            <span style="font-size: 10pt; font-family: Arial"><strong>Main member (%)</strong></span></td>
                        <td style="width: 75px; text-align: center" bgcolor="#00afe1">
                            <span style="font-size: 10pt; font-family: Arial"><strong>Dependent (amount)</strong></span></td>
                        <td style="width: 75px; text-align: center" bgcolor="#00afe1">
                            <span style="font-size: 10pt; font-family: Arial"><strong>Dependent (%)</strong></span></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <span style="font-size: 10pt; font-family: Arial"><strong>Out Patient Deductible</strong></span></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblOutPatientDeductibleMA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblOutPatientDeductibleMP" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblOutPatientDeductibleDA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblOutPatientDeductibleDP" runat="server" Width="75px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <span style="font-size: 10pt; font-family: Arial"><strong>In Patient Deductible</strong></span></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblInPatientDeductibleMA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblInPatientDeductibleMP" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblInPatientDeductibleDA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblInPatientDeductibleDP" runat="server" Width="75px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 140px" >
                            <span style="font-size: 10pt; font-family: Arial"><strong>TOB special instructions</strong></span></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblTobSpecialInstructionsMA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblTobSpecialInstructionsMP" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblTobSpecialInstructionsDA" runat="server" Width="75px"></asp:Label></td>
                        <td style="width: 75px">
                            <asp:Label ID="lblTobSpecialInstructionsDP" runat="server" Width="75px"></asp:Label></td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <asp:Panel ID="Panel3" runat="server" Height="50px" Width="540px">
                <p align="center" class="MsoNormal" style="text-align: center">
                    <b><u><span style="font-size: 10pt; font-family: 'Arial','sans-serif'">Pre-authorization
                        (Please refer to the Benefit Table Pre-<span class="SpellE">authorisation</span>
                        section)</span></u></b><span style="font-size: 10pt; font-family: 'Arial','sans-serif'"><?xml
                            namespace="" prefix="O" ?><o:p></o:p></span></p>
                <p class="MsoNormal">
                    <span style="font-size: 10pt; font-family: 'Arial','sans-serif'">Unless otherwise specified
                        in the Benefit Table Pre-authorization required for MRI, CT-Scan and if treatment
                        exceeds the SR1000 and for All Inpatients and Daycare, except emergencies, which
                        must be notified within 24 hours of admission. Ambulance is covered for all plans
                        based on medical necessity to and from hospital.
                        <o:p></o:p>
                    </span>
                </p>
                <p align="center" style="text-align: center">
                    <b><u><span>General Exclusions (Unless otherwise stated in above Benefit Table)</span></u></b><span
                        style="font-size: 10pt; font-family: 'Arial','sans-serif'"></span></p>
                <ol style="margin-top: 0in" type="1">
                    <li><span>Pregnancy and childbirth </span></li>
                    <li><span style="font-size: 10pt; font-family: 'Arial','sans-serif';">Contraception,
                        abortion, sterilization, sexual problems (including impotence), sex changes, assisted
                        reproduction (for example IVF treatment), termination of pregnancy </span></li>
                    <li><span>Dialysis </span></li>
                    <li><span>Naturally occurring conditions </span></li>
                    <li><span>AIDS, HIV, and sexually transmitted diseases </span></li>
                    <li><span>Cosmetic treatment unless it is a surgical operation to restore a person�s
                        appearance after an accident or as a result for surgery for cancer </span></li>
                    <li><span>Contamination, war, riots and natural disasters </span></li>
                    <li><span>Health hydrous, spa baths, nature cure clinics or similar establishments </span>
                    </li>
                    <li><span>Psychiatric Treatment, Mental and addictive conditions </span></li>
                    <li><span>Problems present at birth  congenital </span></li>
                    <li><span>Neo-natal problems </span></li>
                    <li><span>Vaccinations </span></li>
                    <li><span>Nursing at home </span></li>
                    <li><span>Work related accidents to the extent that the cost of treatment is payable
                        by GOSI </span></li>
                    <li><span>Artificial body parts, surgical aids and appliances unless implanted into
                        the body as an integral part of a surgical procedure </span></li>
                    <li><span>Weight control </span></li>
                    <li><span>Routine Dental treatment </span></li>
                    <li><span>Road traffic accidents to the extent that the cost of treatment is payable
                        by GOSI </span></li>
                    <li><span>Circumcision </span></li>
                    <li><span>Body piercing </span></li>
                    <li><span>Hazardous sports </span></li>
                    <li><span>Treatment not related to a medical condition </span></li>
                    <li><span>Un-prescribed drugs </span></li>
                    <li><span>Military service </span></li>
                    <li><span>Physiotherapy unless as part of post-surgical treatment authorized by a licensed
                        medical practitioner </span></li>
                    <li><span>Acupuncture, osteopathy, chiropractic and homeopathy </span></li>
                    <li><span>Self-inflicted </span></li>
                    <li><span>Associated non-medical and personal expenses<o:p></o:p></span> </li>
                </ol>
            </asp:Panel>
            <br />
            <br />
        </div>
    </form>























 if( document.form1.ddlPharmacyDiag != null && (document.form1.ddlPharmacyDiag.value == '???.?' || ( document.form1.ddlDepartment.value == 'OGP' && document.form1.ddlPharmacyDiag.value != '???.?' )) )
 {
 alert();  document.getElementById('txtServiceDesc').value=document.form1.ddlServiceDesc.options[document.form1.ddlServiceDesc.options.selectedIndex].text + '|' +  document.getElementById('ddlServiceDesc').value;
}

