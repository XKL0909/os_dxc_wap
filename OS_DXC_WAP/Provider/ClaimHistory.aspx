﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Provider_ClaimHistory" Codebehind="ClaimHistory.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<META HTTP-EQUIV=Refresh CONTENT='5' />

    <title>Untitled Page</title>
     <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
    <script type="text/javascript"  src="js/jquery.dataTables.js"></script>

    <%-- <script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScriptManager1.IsInAsyncPostBack)
        {
            System.Threading.Thread.Sleep(3000);
            
            if (Panel2.Controls.Contains(Image2)) {
                Panel2.Controls.Remove(Image2);
                Image2.Dispose();
            }

        }
    }
</script>--%>
    <style type="text/css">

#Content td{
	font-family:arial;
	font-size:12px;
	color:#606060;
}
.th{
 font-weight:bold;
 color:#666;
 font-size:12px;
}

.style1 {
	text-align: center;
}
.style2 {
	font-weight: bold;
	text-align: right;
}
.style3 {
	text-align: right;
}

.style5 {
	border-right: 1px solid #808080;
	border-bottom: 1px solid #808080;
	border-style: none none solid none;
	border-width: 0px 0px 1px 0px;
	text-align: center;
}
.style6 {
	border-right: 1px none #808080;
	border-bottom: 1px solid #808080;
	border-width: 0px 0px 1px 0px;
	text-align: right;
	border-left-style: none;
	border-top-style: none;
	border-bottom-color: #808080;
}
.style8 {
	font-family: Arial, sans-serif;
	font-size: 9pt;
}

.style9 {
	border-right: 0px none #808080;
	border-bottom: 1px solid #808080;
	text-align: left;
	font-size: 9pt;
	font-family: Arial, sans-serif;
	border-left-style: none;
	border-left-width: 0px;
	border-top-style: none;
	border-top-width: 0px;
}

.style10 {
	border-right: 0px none #808080;
	border-bottom: 1px solid #808080;
	text-align: center;
	font-family: Arial, sans-serif;
	font-size: 9pt;
	border-left-style: none;
	border-left-width: 0px;
	border-top-style: none;
	border-top-width: 0px;
	width:40%;
}

.style11 {
	font-family: Arial, sans-serif;
	font-weight: bold;
}
.style12 {
	font-family: Arial, sans-serif;
}

.style13 {
	font-size: medium;
}

.style14 {
	text-align: left;
}

</style>
    <style>
@media print {
.noPrint { display:none;}
.Hidden { display:inline;}
}

.Hidden { display:none;}
</style>
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_mediaScreen.css" media="screen">
    <link rel="stylesheet" href="styles.css" type="text/css" />
</head>
<body topmargin="0px" style="font-size:small;">
    <div class="adbanner" align="right">
        <img src="Logo.gif" width="168" height="50">
        <br>
        <span style="font-size: 8pt; font-family: Arial">Exclusive Agents in KSA BUPA Middle
            East Customer Service Toll Free No. 800 244 0307</span>
        <hr>
    </div>
    
      
        <table width=100% ><tr><td align=left>
            <label ID="Label1" ><font size=4>My Claim History</font></label>  </td>
            <td><a href="#" onclick="window.print()">
                <img class="noPrint" border="0" src="images/printer.jpg" alt="Print" style="height: 33px; text-align: right;">
            </a> </td></tr></table>
            
            
   
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

<%--        <script language="javascript" type="text/javascript">
   function clientActiveTabChanged(sender, args)
   {
      // Post back if it is the 3rd tab (0 based)
      if(sender.get_activeTabIndex() == 2) {
         __doPostBack('<%= this.btnTab2.UniqueID %>', '');
      }
      
      if(sender.get_activeTabIndex() == 0) {
         __doPostBack('<%= this.btnTab1.UniqueID %>', '');
      }
      
      }
        </script>
--%>
       
  <div class="noPrint" >   
        
</div>   
    
<div id="DivProgress" style="position:absolute; background-color:Transparent; margin-bottom:15px; margin-left:15px;"> 
 <asp:UpdateProgress ID="UpdateProgress2"   runat="server" DynamicLayout="true" DisplayAfter="50">
            <ProgressTemplate >
                <img src="spinner.gif" width="33" height="33" />
                <font face="verdana" size=1> Loading information ......</font>
            </ProgressTemplate>
        </asp:UpdateProgress>
</div>
     
        <asp:Label ID="Message1" runat="server" Height="15px" Width="527px"></asp:Label><br />
  <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
  <asp:TextBox CssClass="textbox" ID="txtBox_output"  Visible="false" runat="server"></asp:TextBox>
        <br />
        <br />
           <br />
        <br />
        <br />
        <br />
        
      
        
        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
        <HeaderTemplate>
        <table border="1">
        </HeaderTemplate>
        <ItemTemplate>
            <%  UrlValues = "PID=" + Eval("PREAUTH_ID") + "&PC=" + Eval("PROV_CODE") + "&EN=1";
                                                        UrlValues = Cryption.Encrypt(UrlValues);
                                                    %>

              <a href="PreauthDetails.aspx?val=<%=UrlValues%>">
                  <%--   <a href="PreauthDetails.aspx?PID=<%# Eval("PREAUTH_ID") %>&PC=<%# Eval("PROV_CODE")%>&EN=1" >--%>

        <tr>
        <td><asp:Label ID="lblTXTID" runat="server" Text='<%# Eval("TXN_ID") %>' /></td>
        <td><asp:Label ID="lblProvCode" runat="server" Text='<%# Eval("PROV_CODE") %>' /></td>
        <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("MBRSHP_NO") %>' /></td>
        <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("NAME") %>' /></td>
        <td><asp:Label ID="lblPreathID" runat="server" Text='<%# Eval("PREAUTH_ID") %>' /></td>
        <td><asp:Label ID="lblEpisodeNo" runat="server" Text='<%# Eval("EPISODE_ORDER") %>' /></td>
        <td><asp:Label ID="Label7" runat="server" Text='<%# Eval("STATUS") %>' /></td>
        <td><asp:Label ID="Label8" runat="server" Text='<%# Eval("RECEIVED_DATE") %>' /></td>
        <td><asp:Label ID="Label9" runat="server" Text='<%# Eval("ANSWERED_DATE") %>' /></td>
       
       
        <tr />
       </a>
        </ItemTemplate>
        <FooterTemplate>
        </table></FooterTemplate>
        
        </asp:Repeater>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            SelectCommand="SELECT top 10 [TXN_ID], [PROV_CODE], [MBRSHP_NO], [NAME], [PREAUTH_ID], [EPISODE_ORDER], [STATUS], [RECEIVED_DATE], [ANSWERED_DATE], [CHECKED_IND] FROM [PREAUTH_ALERT]  ">
            <SelectParameters>
                <asp:Parameter DefaultValue="'N'" Name="CHECKED_IND" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        
         <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
                <div style="display: none;" id="divSessionExpired">
                    <div>
                        <br />
                        <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                        <br />
                        <br />
                    </div>
                    <div class="inputEntity">
                        <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                        <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                            <asp:Literal runat="server" Text="No"></asp:Literal></a>
                    </div>
                </div>
				            
           <h3 style="display:none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>  
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
           
       </form> 
    <asp:DataList ID="DataList1" runat="server" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingItemStyle BackColor="White" />
        <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
    </asp:DataList>
   

</body>
</html>
