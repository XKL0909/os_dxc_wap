using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;

public partial class TOB : System.Web.UI.Page
{
    private string VerificationID;
    private string ProviderCode;
    private string RejectionReason;
    private string VerificationStatus;
    private string Dept;
    private string PlanID;
    private string ContractNo;
    private string TransactionDate;
    private string SearchMode;
    private string MembershipNo;
    private string strProviderName;
    private string strError;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;

    private Hashtable hasQueryValue;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        //Session Timeout Warning Dialog
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Session["Reset"] = true;
        Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
        SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
        int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);

        hasQueryValue = new Hashtable();
        string queryStringValues = string.Empty;

        if (Request.QueryString.Count > 0)
        {
            try
            {

                
                RejectionReason = "No error";               
                ProviderCode = (string)Session["ProviderID"];
                strProviderName = (string)Session["ProviderName"];

                var val = Request.QueryString["val"];
                queryStringValues = Cryption.Decrypt(val);
                hasQueryValue = Cryption.GetQueryValue(queryStringValues);


                VerificationID = hasQueryValue.ContainsKey("VID") ? Convert.ToString(hasQueryValue["VID"]) : string.Empty;
                SearchMode = hasQueryValue.ContainsKey("Type") ? Convert.ToString(hasQueryValue["Type"]) : string.Empty;
                VerificationStatus = hasQueryValue.ContainsKey("Status") ? Convert.ToString(hasQueryValue["Status"]) : string.Empty;
                Dept = hasQueryValue.ContainsKey("D") ? Convert.ToString(hasQueryValue["D"]) : string.Empty;
                MembershipNo = hasQueryValue.ContainsKey("MN") ? Convert.ToString(hasQueryValue["MN"]) : string.Empty;
            }
            catch (Exception ex)
            {
                Response.Write("Invalid rquest!");
                return;
            }

        }
        else
        {
            Response.Write("Invalid rquest!");
            return;
        }



     

        if (!Page.IsPostBack)
        {

            if (SearchMode == "In" && VerificationStatus == "A")
            {
                lnkBackOption.NavigateUrl = "~/Provider/MemberVerification1.aspx";

                if (Dept == "OGP" || Dept == "DEN" || Dept == "OER")
                    DisplayTOBDOMReport("N");
                else
                    DisplayTOBStdReport("N");

                DisplayTOBReport();
                if (strError == "true")
                {
                    lnkClaim.Visible = false;
                    lblVerificationResult.Text = "Rejected";
                }
            }

            if (SearchMode == "Search")
            {
                DisplayTOBReport();
                if (VerificationStatus == "0")
                    lblRemarks.Text = "Incomplete Transaction.";
                if (VerificationStatus == "R" && SearchMode == "In")
                    lblRemarks.Text = "Fingerprint does not match.";
            }
        }
        //Session Timeout Warning Dialog
        CheckSessionExpired();
        if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
        {
            CheckConcurrentUserStatus();
        }
        if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
        {
            CheckConcurrentUserStatus();
        }
    }

    private void DisplayTOBReport()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDResponse_DN response;

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);

        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqMbrVerifyByIDRequest_DN();

            request.verificationID = long.Parse(VerificationID);
            request.provCode = ProviderCode;
            request.transactionID = TransactionManager.TransactionID();

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ReqMbrVerificationByID(request);

            if (response.status == "0")
            {
                if (response.extIndicator == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "$.modal.close();");

                    litExtendedDate.Text = ((DateTime)response.extDate).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Overlay", "$(document).ready(function(){$('#basic-modal-content').modal();});", true);
                }

                PlanID = response.planID.Trim();
                ContractNo = response.contractNo;
                lblCustomerNo.Text = ContractNo;
                TransactionDate = response.transactionDate;
                VerificationStatus = response.verificationStatus;

                lblAge.Text = response.age.ToString();
                Dept = response.deptCode;
                lblDepartment.Text = response.department;
                lblGender.Text = response.gender;
                lblIDNo.Text = response.iqama;
                lblDOB.Text = response.memberDOB;
                lblMemberName.Text = response.memberName;
                lblBupaCardNo.Text = response.membershipNo;

                lblVerificationID.Text = response.verificationID.ToString();
                lblProviderName.Text = strProviderName;
                lblDateOfVisit.Text = DateTime.Now.ToString();

                if (strError == "true" && VerificationStatus == "R")
                {
                }

                if (SearchMode == "In" && VerificationStatus == "A")
                {
                    lblVerificationResult.Text = "Approved";
                    SqlParameter[] arParms = new SqlParameter[2];
                    arParms[0] = new SqlParameter("@verification_id", SqlDbType.BigInt);
                    arParms[0].Value = VerificationID;
                    arParms[1] = new SqlParameter("@verification_status", SqlDbType.VarChar, 50);
                    arParms[1].Value = "Approved";
                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spBupa_ProviderVerificationUpdate", arParms);
                }

                if (SearchMode == "Search")
                {
                    lblDateOfVisit.Text = TransactionDate;
                    if (VerificationStatus == "A")
                    {
                        lblVerificationResult.Text = "Approved";

                        if (Dept == "OGP" || Dept == "DEN" || Dept == "OER")
                            DisplayTOBDOMReport("Y");
                        else
                            DisplayTOBStdReport("Y");
                    }
                    else
                    {
                        if (Dept == "OGP" || Dept == "DEN" || Dept == "OER")
                            DisplayTOBDOMReport("Y");
                        else
                            DisplayTOBStdReport("Y");

                        lblVerificationResult.Text = "Rejected";
                        lnkClaim.Visible = false;
                        Panel2.Visible = false;
                        Panel3.Visible = false;
                        Panel5.Visible = false;
                    }
                }
            }
            else
            {
                if (SearchMode != "In")
                {
                    Panel1.Visible = false;
                    Panel2.Visible = false;
                    Panel3.Visible = false;
                    Panel5.Visible = false;
                }
            }
        }
        catch (Exception)
        {
        }
        finally
        {
            connection.Close();
        }
    }

    private void DisplayTOBStdReport(string SearchMode)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ReqTOBStandardRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqTOBStandardResponse_DN response;

        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqTOBStandardRequest_DN();

            request.transactionID = TransactionManager.TransactionID();
            request.planID = PlanID;
            request.contNo = ContractNo;
            request.mbrshipNo = MembershipNo;
            request.deptNameCode = Dept;
            request.providerCode = ProviderCode;
            request.verificationID = Int64.Parse(VerificationID.Trim());
            request.verificationStatus = VerificationStatus;

            request.txnDate = DateTime.Now;
            request.failReason = RejectionReason;
            request.searchMode = SearchMode;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.ReqTOBStandard(request);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBStandardRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBStandardRequest_DN>();
            //XmlReq.Request(request, "ReqTOBStandardRequest_DN");

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBStandardResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBStandardResponse_DN>();
            //XmlResp.Response(response, "ReqTOBStandardResponse_DN");


            if (response.status == "0")
            {
                if (response.extIndicator == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "goto('$.modal.close();");
                    litExtendedDate.Text = ((DateTime)response.extDate).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }

                lblEffectiveTo.Text = response.effectiveEndDate;
                lblVerificationID.Text = VerificationID;

                lblBupaCardNo.Text = MembershipNo;
                lblHospitalAccomodation.Text = response.bedType;
                lblCustomerNo.Text = ContractNo;
                //lblCustomerNo.Text = response.contractNo_CCHI;
                lblCustomerName.Text = response.customerName;
                lblInPatientDeductibleDA.Text = response.dependentDeductibleAmountForInPatient;                
                lblInPatientDeductibleDP.Text = response.dependentDeductiblePercentageForInPatient + " " + response.dependentDeductibleAmountForInPatient;                
                lblOutPatientDeductibleDA.Text = response.dependentDeductibleAmountForOutPatient;

                lblInPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForInPatient;                
                lblOutPatientDeductibleMA.Text = response.mainMemberDeductibleAmountForOutPatient;
                lblInPatientDeductibleMA.Text = response.mainMemberDeductibleAmountForInPatient;
                //// CR414 TOB Changes
                ////Start
                ////lblOutPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForOutPatient;
                ////lblOutPatientDeductibleDA.Text = response.dependentDeductiblePercentageForOutPatient;
                if (response.TOBTemplate == "Y")
                {
                    lblOutPatientDeductibleMP.Text = "*H - " + response.memberContributionForOutPatientTreatmentHospital + "</br>*P - " + response.memberContributionForOutPatientTreatmentPolyclinic + "</br>*M - " + response.memberContributionForOutPatientTreatmentMPN;
                    lblOutPatientDeductibleDP.Text = "*H - " + response.memberContributionForOutPatientTreatmentHospital + "</br>*P - " + response.memberContributionForOutPatientTreatmentPolyclinic + "</br>*M - " + response.memberContributionForOutPatientTreatmentMPN;
                    trNewTOB.Visible = true;
                }
                else
                {                    
                    lblOutPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForOutPatient;
                    lblOutPatientDeductibleDP.Text = response.dependentDeductiblePercentageForOutPatient;
                    trNewTOB.Visible = false;
                }
                ////End

                lblEffectiveFrom.Text = response.effectiveDate.ToString();

                lblOverallAnnualLimit.Text = response.overallLimited;
                lblPreauthorisationLimit.Text = response.preauthLimited;
                lblScheme.Text = response.schemeName;
                lblTobSpecialInstructionsMA.Text = response.specialInstruction;
                lblTreatmentType.Text = response.treatmentType;


                lblReferralLetterD.Text = response.dependentMandatoryReferralIndicator;
                lblReferralLetterM.Text = response.mainMemberMandatoryReferralIndicator;

                StringBuilder sbResponse = new StringBuilder(2200);
                sbResponse.Append("<table width=100% border=1  style=' border-style:none ; border-left-width: thin; border-left-color: silver; border-bottom-width: thin; border-bottom-color: silver; border-top-color: silver; border-right-width: thin; border-right-color: silver;'><tr><td colspan='4' bgcolor='white'><B>TABLE OF BENEFITS</B></td></tr><tr><td width=20%><b>Benefit Type</td><td width='52%'><b>Treatment Covered</td><td width='14%'><b>Sub Limit</b></td><td width=14%><b>Deductable</td></tr>");

                for (int i = 0; i < response.benefitDesc.Length; i++)
                {
					response.benefitTypeLimit[i] = !string.IsNullOrWhiteSpace(response.benefitTypeLimit[i]) ? response.benefitTypeLimit[i].Contains("99999999.99") ? "MAX Annual Limit" : response.benefitTypeLimit[i] : response.benefitTypeLimit[i];
                    sbResponse.Append("<tr><td><b>" + response.benefitDesc[i] + "</b></td><td>" + response.treatmentCoverd[i] + " &nbsp;</td><td>" + response.benefitTypeLimit[i] + "</td>");
                    if (response.benefitDesc[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                        sbResponse.Append("<td>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100 </td></tr>");  // response.service_Code
                    else
                        sbResponse.Append("<td>" + response.maxDeductibleLimitOnBenefitTypes[i] + " &nbsp; </td></tr>");  // response.service_Code

                }

                sbResponse.Append("</table>");
                ServiceListReport.Visible = true;
                ServiceListReport.InnerHtml = sbResponse.ToString();

                
            }
            else
            {
                Panel1.Visible = true;
                Panel2.Visible = false;// true;
                Panel3.Visible = false;
                Panel5.Visible = false;

                strError = "true";
                lblVerificationResult.Text = "Rejected";
                lblRemarks.Text = response.errorMessage[0];
                lnkClaim.Visible = false;
            }
        }
        catch (Exception)
        { }

    }

    private void DisplayTOBDOMReport(string SearchMode)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ReqTOBDOMRequest_DN request;
        OS_DXC_WAP.CaesarWS.ReqTOBDOMResponse_DN response;

        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.ReqTOBDOMRequest_DN();

            request.transactionID = TransactionManager.TransactionID();
            request.planID = PlanID;
            request.contNo = ContractNo;
            request.mbrshipNo = MembershipNo;
            request.deptNameCode = Dept;
            request.providerCode = ProviderCode;
            request.verificationID = Int64.Parse(VerificationID.Trim());
            request.verificationStatus = VerificationStatus;

            request.txnDate = DateTime.Now;
            request.failReason = RejectionReason;
            request.searchMode = SearchMode;
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.ReqTOBDOM(request);

            // ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBDOMRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBDOMRequest_DN>();
            // XmlReq.Request(request, "ReqTOBDOMRequest_DN");

            // ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBDOMResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReqTOBDOMResponse_DN>();
            // XmlResp.Response(response, "ReqTOBDOMResponse_DN");

            if (response.status == "0")
            {
                if (response.extIndicator == "Y")
                {
                    aCloseOverlay.Attributes.Add("onclick", "$.modal.close();");
                    litExtendedDate.Text = ((DateTime)response.extDate).ToString("dd/MM/yyyy");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Overlay", "$('#basic-modal-content').modal();", true);
                }


                lblReferralLetterD.Text = response.dependentMandatoryReferralIndicator;
                lblReferralLetterM.Text = response.mainMemberMandatoryReferralIndicator;

                lblVerificationID.Text = VerificationID;
                lblDateOfVisit.Text = DateTime.Now.ToString();
                lblEffectiveTo.Text = response.effectiveEndDate;
                lblBupaCardNo.Text = MembershipNo;
                lblHospitalAccomodation.Text = response.bedType;
                lblCustomerNo.Text = response.contractNo_CCHI;
                lblCustomerName.Text = response.customerName;
                lblInPatientDeductibleDA.Text = response.dependentDeductibleAmountForInPatient;
                lblOutPatientDeductibleDA.Text = response.dependentDeductibleAmountForOutPatient;
                lblInPatientDeductibleDP.Text = response.dependentDeductiblePercentageForInPatient + " " + response.dependentDeductibleAmountForInPatient;
                

                lblInPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForInPatient;
                
                lblOutPatientDeductibleMA.Text = response.mainMemberDeductibleAmountForOutPatient;
                lblInPatientDeductibleMA.Text = response.mainMemberDeductibleAmountForInPatient;

                //// CR414 TOB Changes
                ////Start
                ////lblOutPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForOutPatient;
                ////lblOutPatientDeductibleDP.Text = response.dependentDeductiblePercentageForOutPatient;
                if (response.TOBTemplate == "Y")
                {
                    lblOutPatientDeductibleMP.Text = "*H - " + response.memberContributionForOutPatientTreatmentHospital + "</br>*P - " + response.memberContributionForOutPatientTreatmentPolyclinic + "</br>*M - " + response.memberContributionForOutPatientTreatmentMPN;
                    lblOutPatientDeductibleDP.Text = "*H - " + response.memberContributionForOutPatientTreatmentHospital + "</br>*P - " + response.memberContributionForOutPatientTreatmentPolyclinic + "</br>*M - " + response.memberContributionForOutPatientTreatmentMPN;
                    trNewTOB.Visible = true;
                }
                else
                {                    
                    lblOutPatientDeductibleMP.Text = response.mainMemberDeductiblePercentageForOutPatient;
                    lblOutPatientDeductibleDP.Text = response.dependentDeductiblePercentageForOutPatient;
                    trNewTOB.Visible = false;
                }
                ////End

                if (response.effectiveDate != null)
                    lblEffectiveFrom.Text = response.effectiveDate.ToString();

                lblOverallAnnualLimit.Text = response.overallLimited;
                lblPreauthorisationLimit.Text = response.preauthLimited;
                lblScheme.Text = response.schemeName;
                lblTobSpecialInstructionsMA.Text = response.specialInstruction;
                lblTreatmentType.Text = response.treatmentType;

                StringBuilder sbResponse = new StringBuilder(2200);
                sbResponse.Append("<table width=100% border=1  style=' border-style:none ; border-left-width: thin; border-left-color: silver; border-bottom-width: thin; border-bottom-color: silver; border-top-color: silver; border-right-width: thin; border-right-color: silver;'><tr><td colspan='4' bgcolor='white'><B>TABLE OF BENEFITS</B></td></tr><tr><td width=20%><b>Benefit Type</td><td width='52%'><b>Treatment Covered</td><td width='14%'><b>Sub Limit</b></td><td width=14%><b>Deductable</td></tr>");

                for (int i = 0; i < response.benefitDesc.Length; i++)
                {
					response.benefitHeadSubLimit[i] = !string.IsNullOrWhiteSpace(response.benefitHeadSubLimit[i]) ? response.benefitHeadSubLimit[i].Contains("99999999.99") ? "MAX Annual Limit" : response.benefitHeadSubLimit[i] : response.benefitHeadSubLimit[i];
                    sbResponse.Append("<tr><td><b>" + response.benefitDesc[i] + "</b></td><td>" + response.treatmentCoverd[i] + " &nbsp;</td><td>" + response.benefitHeadSubLimit[i] + "</td>");
                    if (response.benefitDesc[i] == "Dental" && response.deductiblePercentageOnBenefitTypes[i] != null)
                        sbResponse.Append("<td>" + response.deductiblePercentageOnBenefitTypes[i] + " upto SR 100 </td></tr>");  // response.service_Code
                    else
                        sbResponse.Append("<td>" + response.maxDeductibleLimitOnBenefitTypes[i] + " &nbsp; </td></tr>");  // response.service_Code

                }

                sbResponse.Append("</table>");
                ServiceListReport.Visible = true;
                ServiceListReport.InnerHtml = sbResponse.ToString();
            }
            else
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel5.Visible = false;

                strError = "true";
                lnkClaim.Visible = false;
                lblRemarks.Text = response.errorMessage[0];
            }
        }
        catch (Exception)
        { }
    }

    protected void lnkPrintClaimForm_Click(object sender, EventArgs e)
    {
        ///Response.Redirect("ClaimForm.aspx?Dept=" + Dept + "&VID=" + VerificationID + "&MN=" + MembershipNo);
        var queryString = "Dept=" + Dept + "&VID=" + VerificationID + "&MN=" + MembershipNo;
        queryString = Cryption.Encrypt(queryString);
        Response.Redirect("ClaimForm.aspx?val=" + queryString);
    }

    protected void lnkPrintThermalForm_Click(object sender, EventArgs e)
    {
        Response.Redirect("Thermal1.aspx");
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientID"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["ProCurrentLogID"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }
}

public class ClassXMLGeneration<T>
{
    public void Request(T classType, string req)
    {
        System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
        System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\CaesarXML\\" + req + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
        xmlSrQ.Serialize(file, classType);
        file.Close();


    }
    public void Response(T classType, string resp)
    {
        System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
        System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\CaesarXML\\" + resp + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");
        xmlSr.Serialize(file, classType);
        file.Close();
    }
}