﻿<%@ Page language="c#" Inherits="AsynchronousProcessing.WebForm1" EnableViewState="false" Codebehind="ProcessingMessage.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PROCESSING...</title>
        <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
        <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
        <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js"></script>

		<meta http-equiv="refresh" content="1">
	</HEAD>
	<body style="CURSOR:wait">
        <div align="center">
            <b>PROCESSING...</b>
            <br>
            <img src="Processing.gif" width="219" height="13">
            <br>
            <asp:Label ID="StatusMessage" runat="server"></asp:Label>
        </div>

        <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
        <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
        <div style="display: none;" id="divSessionExpired">
            <div>
                <br />
                <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                <br />
                <br />
            </div>
            <div class="inputEntity">
                <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                    <asp:Literal runat="server" Text="No"></asp:Literal></a>
            </div>
        </div>

        <h3 style="display: none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="lnkSession" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                        <div class="header">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Session Expiring! </strong>
                                    </td>
                                    <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                                </tr>
                            </table>
                        </div>

                        <div class="body">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="text-align: left">
                                        <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                            Do you want to extend?</strong>
                                    </td>
                                    <td style="text-align: right">
                                        <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                            هل تريد تمديد الوقت؟ &nbsp;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="footer" align="center">
                            <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                            <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                        </div>
                    </asp:Panel>
                    <div id="mainContent">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
	</body>
</HTML>
