﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;
using System.IO;
using System.Runtime.InteropServices;
using System.Net;

public partial class Provider_Updateaspx : System.Web.UI.Page
{
    [DllImport("advapi32.dll", SetLastError = true)]
    static extern bool LogonUser(
      string principal,
      string authority,
      string password,
      LogonSessionType logonType,
      LogonProvider logonProvider,
      out IntPtr token);
    [DllImport("kernel32.dll", SetLastError = true)]
    static extern bool CloseHandle(IntPtr handle);
    enum LogonSessionType : uint
    {
        Interactive = 2,
        Network,
        Batch,
        Service,
        NetworkCleartext = 8,
        NewCredentials
    }
    enum LogonProvider : uint
    {
        Default = 0, // default for platform (use this!)
        WinNT35,     // sends smoke signals to authority
        WinNT40,     // uses NTLM
        WinNT50      // negotiates Kerb or NTLM
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create("../MemberQService.exe");
        HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
        int bufferSize = 1;
        Response.Clear();
        Response.ClearHeaders();
        Response.ClearContent();
        Response.AppendHeader("Content-Disposition:", "attachment; filename=MemberQService.exe");
        Response.AppendHeader("Content-Length", objResponse.ContentLength.ToString());
        Response.ContentType = "application/download";
        byte[] byteBuffer = new byte[bufferSize + 1];
        MemoryStream memStrm = new MemoryStream(byteBuffer, true);
        Stream strm = objRequest.GetResponse().GetResponseStream();
        byte[] bytes = new byte[bufferSize + 1];
        while (strm.Read(byteBuffer, 0, byteBuffer.Length) > 0)
        {
            Response.BinaryWrite(memStrm.ToArray());
            Response.Flush();
        }
        Response.Close();
        Response.End();
        memStrm.Close();
        memStrm.Dispose();
        strm.Dispose();
    }
}