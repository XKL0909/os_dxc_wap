﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PreauthNewRequest_Default" Codebehind="PreauthNewRequest.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div>
        
           <asp:Panel ID="Panel1" runat="server" Height="50px" Width="125px">
                <asp:Label ID="lblMemberDetails" runat="server" Text="Member Details"></asp:Label><br />
                <table style="width: 458px">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblMembershipNo" runat="server" Text="Membership No"></asp:Label></td>
                        <td style="width: 280px">
                            <asp:TextBox CssClass="textbox" ID="txtMembershipNo" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblMemberName" runat="server" Text="Member Name"></asp:Label></td>
                        <td style="width: 280px">
                            <asp:TextBox CssClass="textbox" ID="txtMemberName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label></td>
                        <td style="width: 280px">
                            <asp:TextBox CssClass="textbox" ID="txtAge" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label></td>
                        <td style="width: 280px">
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlGender" runat="server">
                                <asp:ListItem>-Select-</asp:ListItem>
                                <asp:ListItem>Male</asp:ListItem>
                                <asp:ListItem>Female</asp:ListItem>
                            </asp:DropDownList></div></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblMemberID" runat="server" Text="Member ID/Iqama ID"></asp:Label></td>
                        <td style="width: 280px">
                            <asp:TextBox CssClass="textbox" ID="txtMemberID" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblCardIssueNo" runat="server" Text="Card Issue No"></asp:Label></td>
                        <td style="width: 280px">
                            <asp:TextBox CssClass="textbox" ID="txtCardIssueNo" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblMobileNo" runat="server" Text="Mobile No"></asp:Label></td>
                        <td style="width: 280px">
                            <asp:TextBox CssClass="textbox" ID="txtMobileNo" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblPolicyNo" runat="server" Text="Policy No"></asp:Label></td>
                        <td style="width: 280px">
                            &nbsp;<asp:TextBox CssClass="textbox" ID="txtPolicyNo" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblNameOfPhysician" runat="server" Text="Name of Physician"></asp:Label></td>
                        <td style="width: 280px">
                            <asp:TextBox CssClass="textbox" ID="txtNameOfPhysician" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblPatientFileNo" runat="server" Text="Patient File No"></asp:Label></td>
                        <td style="width: 280px">
                            <asp:TextBox CssClass="textbox" ID="txtPatientFileNo" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label></td>
                        <td style="width: 280px">
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlDepartment" runat="server" Width="292px" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                <asp:ListItem>-Select-</asp:ListItem>
                            <asp:ListItem Value="CVC" >Cardio-Vascular Clinic</asp:ListItem>
                            <asp:ListItem Value="DEN" >Dental Clinic</asp:ListItem>
                            <asp:ListItem Value="DVC" >Derma &amp; Venereal Clinic</asp:ListItem>
                            <asp:ListItem Value="DEC" >Diabetes &amp; Endocrinology Clinic</asp:ListItem>
                            <asp:ListItem Value="ENT" >E.N.T. Clinic</asp:ListItem>
                            <asp:ListItem Value="GFM" >General Practitioner/Family Medicine Clinic</asp:ListItem>
                            <asp:ListItem Value="GEC" >G.I &amp; Endoscopies Clinic</asp:ListItem>
                            <asp:ListItem Value="IMC" >Internal Medicine Clinic</asp:ListItem>
                            <asp:ListItem Value="NPC" >Neuro &amp; Psychiatric Clinic</asp:ListItem>
                            <asp:ListItem Value="NSC" >Neuro-Surgery Clinic</asp:ListItem>
                            <asp:ListItem Value="OBG" >Obs &amp; Gynae Clinic</asp:ListItem>
                            <asp:ListItem Value="OGP" >Obs &amp; Gynae Clinic &amp; Pregnancy related</asp:ListItem>
                            <asp:ListItem Value="OPH" >Ophthalmic Clinic</asp:ListItem>
                            <asp:ListItem Value="OER" >Ophthalmic Clinic &amp; Error of Refraction</asp:ListItem>
                            <asp:ListItem Value="ORT" >Orthopedics Clinic</asp:ListItem>
                            <asp:ListItem Value="PUL" >Pulmonary Clinic</asp:ListItem>
                            <asp:ListItem Value="PED" >Pediatric Clinic</asp:ListItem>
                            <asp:ListItem Value="PHA" >Pharmacy / medication</asp:ListItem>
                            <asp:ListItem Value="SUR" >Surgery Clinic</asp:ListItem>
                            <asp:ListItem Value="URO" >Urology Clinic</asp:ListItem>
                            </asp:DropDownList></div></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTreatmentType" runat="server" Text="Treatment Type"></asp:Label></td>
                        <td style="width: 280px">
                            <div class="styled-select"><asp:DropDownList CssClass="DropDownListCssClass"  ID="ddlTreatmentType" runat="server">
                                <asp:ListItem Value="">-Select-</asp:ListItem>
                                <asp:ListItem>In Patient</asp:ListItem>
                                <asp:ListItem>Out Patient</asp:ListItem>
                            </asp:DropDownList></div></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 280px">
                            <asp:Button CssClass="submitButton" ID="Button1" runat="server" OnClick="Button1_Click" Text="Proceed" Width="80px" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <br />
   
   
            
            
            
             
                <asp:Label ID="lblDiagnosisDetails" runat="server" Text="Diagnosis details"></asp:Label><br />
             
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
              <ContentTemplate>
                
 <asp:Label ID="lblPleaseTick" runat="server" Text="Please Tick (x) where appropriate"></asp:Label><br />
   <table style="width: 588px">
                                <tr>
                                    <td style="width: 145px">
                                        <asp:Label ID="lblChronic" runat="server" Text="Chronic"></asp:Label></td>
                                    <td style="width: 183px">
                            <asp:CheckBox ID="chkChronic" runat="server" /></td>
                                    <td style="width: 191px">
                                        <asp:Label ID="lblRTA" runat="server" Text="RTA"></asp:Label></td>
                                    <td style="width: 100px">
                            <asp:CheckBox ID="chkRTA" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="width: 145px">
                                        <asp:Label ID="lblCheckup" runat="server" Text="Checkup"></asp:Label></td>
                                    <td style="width: 183px">
                            <asp:CheckBox ID="chkCheckup" runat="server" /></td>
                                    <td style="width: 191px">
                                        <asp:Label ID="lblWorkRelated" runat="server" Text="Work Related"></asp:Label></td>
                                    <td style="width: 100px">
                            <asp:CheckBox ID="chkWorkRelated" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="width: 145px">
                                        <asp:Label ID="lblCongenital" runat="server" Text="Congenital"></asp:Label></td>
                                    <td style="width: 183px">
                            <asp:CheckBox ID="chkCongenital" runat="server" /></td>
                                    <td style="width: 191px">
                                        <asp:Label ID="lblVaccination" runat="server" Text="Vaccination"></asp:Label></td>
                                    <td style="width: 100px">
                            <asp:CheckBox ID="chkVaccination" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="width: 145px">
                                        <asp:Label ID="lblPsychiatric" runat="server" Text="Psychiatric"></asp:Label></td>
                                    <td style="width: 183px">
                            <asp:CheckBox ID="chkPsychiatric" runat="server" /></td>
                                    <td style="width: 191px">
                                        <asp:Label ID="lblInfertility" runat="server" Text="Infertility"></asp:Label></td>
                                    <td style="width: 100px">
                            <asp:CheckBox ID="chkInfertility" runat="server" /></td>
                                </tr>
                            </table>
           </ContentTemplate> 
           
           
               
         </asp:UpdatePanel>  
       
       
       
       
 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
              <ContentTemplate>       <table>
                           <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblChiefComplaintsAndMainSymptoms" runat="server" Text="Chief complaints and main symptoms" Width="190px"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtChiefComplaintsAndMainSymptoms" runat="server" TextMode="MultiLine" Width="380px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDiagnosisDescription" runat="server" Text="Diagnosis description" Width="166px"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtDiagnosisDescription" runat="server" TextMode="MultiLine" Width="380px"></asp:TextBox></td>
                    </tr>
                    
                         <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDateOfVisit" runat="server" Text="Date of visit / admission" Width="172px"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtDateOfVisit" runat="server"></asp:TextBox></td>
                    </tr>
       </table> 
             
              </ContentTemplate>
              </asp:UpdatePanel>  
       
                <br />
       
 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
              <ContentTemplate>
                              <table style="width: 629px">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblPulse" runat="server" Text="Pulse"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtPulse" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTemperature" runat="server" Text="Temperature"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtTemperature" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblBloodPressure" runat="server" Text="Blood pressure"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtBloodPressure" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblLastMenstruationPeriod" runat="server" Text="Last menstruation period" Width="184px"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtLastMenstruationPeriod" runat="server"></asp:TextBox></td>
                    </tr>

                    
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblPossibleLineOfTreatment" runat="server" Text="Possible line of treatment" Width="190px"></asp:Label></td>
                        <td style="width: 439px">
                            &nbsp;<asp:TextBox CssClass="textbox" ID="txtPossibleLineOfTreatment" runat="server" TextMode="MultiLine" Width="370px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblOtherConditions" runat="server" Text="Other conditions" Width="177px"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtOtherConditions" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDurationOfIllness" runat="server" Text="Duration of illness" Width="171px"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtDurationOfIllness" runat="server"></asp:TextBox></td>
                    </tr>
               
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblLengthOfStay" runat="server" Text="Length of stay" Width="117px"></asp:Label></td>
                        <td style="width: 439px">
                            <asp:TextBox CssClass="textbox" ID="txtLengthOfStay" runat="server"></asp:TextBox></td>
                    </tr>
                </table>
             </ContentTemplate></asp:UpdatePanel>
                <br />
           
        
        </div>
    </form>
</body>
</html>
