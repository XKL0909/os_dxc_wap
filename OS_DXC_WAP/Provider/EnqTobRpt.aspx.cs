﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.Web;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.Configuration;
using System.Configuration;


public partial class EnqTobRpt : System.Web.UI.Page
{
    private Int64 ls_TxnID;
    private String ls_cont_no, ls_cont_yymm;
    private ReportDocument crDoc;
    private DataTable rptDT = new DataTable();


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        ls_TxnID = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddhhmmssf"));
        txt_TxnID.Text = ls_TxnID.ToString();
        //Session Timeout Warning Dialog
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Session["Reset"] = true;
        Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
        SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
        int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);

        if (!IsPostBack)
        {
            
            //Session Timeout Warning Dialog
            crViewer.ReportSource = Session["report"];
        }
        //Session Timeout Warning Dialog
        CheckSessionExpired();

    }


    protected void cmd_Submit_Click(object sender, EventArgs e)
    {

        OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
        OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqTOBRptResponse_DN response;

        request = new OS_DXC_WAP.CaesarWS.EnqTOBRptRequest_DN();
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();


        request.transactionID = Convert.ToInt64(txt_TxnID.Text);
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
       
        request.reportGroup = txt_RptGrp.Text;
        request.contNo = "2517100";//txt_ContNo.Text;
        ls_cont_no = "2517100";//txt_ContNo.Text;
        ls_cont_yymm = txt_Contyymm.Text;
        request.contYymm = txt_Contyymm.Text;
        request.provCode = txt_ProvCode.Text;

        //Optional Class ID parameter
        if (txt_ClassID.Text.Trim() != "")
        {
            request.classID = txt_ClassID.Text;
        }
        
        response = ws.EnqTOBRpt(request);

        if (response.status == "0")
        {
            DataColumn rptCol;
            DataRow rptRow;

            //rptDT.Columns.Add("SEQ_NO", typeof(long));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SEQ_NO";
            rptCol.DataType = Type.GetType("System.Int64");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("REC_TYPE", typeof(long));
            rptCol = new DataColumn();
            rptCol.ColumnName = "REC_TYPE";
            rptCol.DataType = Type.GetType("System.Int64");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("CONT_NO", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "CONT_NO";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 8;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("CUST_NAME", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "CUST_NAME";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 70;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("EFF_DATE", typeof(DateTime));
            rptCol = new DataColumn();
            rptCol.ColumnName = "EFF_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("TERM_DATE", typeof(DateTime));
            rptCol = new DataColumn();
            rptCol.ColumnName = "TERM_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);

            //Extension Term Date
            rptCol = new DataColumn();
            rptCol.ColumnName = "EXT_TERM_DATE";
            rptCol.DataType = Type.GetType("System.DateTime");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "CONT_TYPE";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 10;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("SCHEME_NAME_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_AMT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_PCT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MAX_DEDUCT_LMT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("BED_TYPE_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MATERNITY_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DENTAL_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            //rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("PRE_AUTH_OPT_A", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_A";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //2014-06-04 REF-1482 CR218 Nathan Start
            //rptDT.Columns.Add("HEART_VALVE_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("HEARING_AID_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ALZHEIMER_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("AUTISM_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DISABILITY_LIMIT_A", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_A";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);
            //2014-06-04 REF-1482 CR218 Nathan End


            //2014-06-17 REF-1493 CR222 Eric Shek, start
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_A";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_B";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);
            
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);


            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_C";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_DISPLAY_IND_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_SPI_D";
            rptCol.DataType = Type.GetType("System.String");
            rptDT.Columns.Add(rptCol);
        
            //2014-06-17 REF-1493 CR222 Eric Shek, End

            //rptDT.Columns.Add("SCHEME_NAME_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_AMT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_PCT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MAX_DEDUCT_LMT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("BED_TYPE_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MATERNITY_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DENTAL_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            //rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("PRE_AUTH_OPT_B", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_B";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //2014-06-04 REF-1482 CR218 Nathan Start
            //rptDT.Columns.Add("HEART_VALVE_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("HEARING_AID_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ALZHEIMER_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("AUTISM_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DISABILITY_LIMIT_B", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_B";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);
            //2014-06-04 REF-1482 CR218 Nathan End

            //rptDT.Columns.Add("SCHEME_NAME_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_AMT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_PCT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MAX_DEDUCT_LMT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("BED_TYPE_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MATERNITY_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DENTAL_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            //rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("PRE_AUTH_OPT_C", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_C";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //2014-06-04 REF-1482 CR218 Nathan Start
            //rptDT.Columns.Add("HEART_VALVE_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("HEARING_AID_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ALZHEIMER_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("AUTISM_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DISABILITY_LIMIT_C", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_C";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);
            //2014-06-04 REF-1482 CR218 Nathan End

            //rptDT.Columns.Add("SCHEME_NAME_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_AMT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_AMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DEDUCT_PCT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DEDUCT_PCT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MAX_DEDUCT_LMT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MAX_DEDUCT_LMT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("BED_TYPE_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "BED_TYPE_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("MATERNITY_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "MATERNITY_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DENTAL_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DENTAL_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            //rptCol.MaxLength = 20;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("OPTICAL_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "OPTICAL_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("PRE_AUTH_OPT_D", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "PRE_AUTH_OPT_D";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 200;
            rptDT.Columns.Add(rptCol);

            //2014-06-04 REF-1482 CR218 Nathan Start
            //rptDT.Columns.Add("HEART_VALVE_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEART_VALVE_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ORGAN_TRANS_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ORGAN_TRANS_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("HEARING_AID_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "HEARING_AID_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEWBORN_DIAG_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEWBORN_DIAG_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("ALZHEIMER_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "ALZHEIMER_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("AUTISM_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "AUTISM_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("DISABILITY_LIMIT_D", typeof(double));
            rptCol = new DataColumn();
            rptCol.ColumnName = "DISABILITY_LIMIT_D";
            rptCol.DataType = Type.GetType("System.Decimal");
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("NEW_CCHI_POLICY_IND", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "NEW_CCHI_POLICY_IND";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);
            //2014-06-04 REF-1482 CR218 Nathan End

            //rptDT.Columns.Add("SCHEME_NAME", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SCHEME_NAME";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 100;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("SP_INSTRUCT", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT1";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT2";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            rptCol = new DataColumn();
            rptCol.ColumnName = "SP_INSTRUCT3";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 4000;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("FIRST_INSTRUCT_REC", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "FIRST_INSTRUCT_REC";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;
            rptDT.Columns.Add(rptCol);

            //rptDT.Columns.Add("LAST_INSTRUCT_REC", typeof(String));
            rptCol = new DataColumn();
            rptCol.ColumnName = "LAST_INSTRUCT_REC";
            rptCol.DataType = Type.GetType("System.String");
            rptCol.MaxLength = 1;

            rptDT.Columns.Add(rptCol);

            //Response.Write("<div>");
            Response.Write("** Success **<br />");
            Response.Write("TransactionID: " + response.transactionID + "<br />");
            Response.Write("Total Records: " + response.detail.Length + "<br />");
            Response.Write("<table style=border:1px solid black>");
            Response.Write("<TR>");
            Response.Write("<td>SEQ_NO</td>");
            Response.Write("<td>REC_TYPE</td>");
            Response.Write("<td>CONT_NO</td>");
            Response.Write("<td>CUST_NAME</td>");
            Response.Write("<td>EFF_DATE</td>");
            Response.Write("<td>TERM_DATE</td>");
            Response.Write("<td>EXT_TERM_DATE</td>");
            Response.Write("<td>SCHEME_NAME_A</td>");
            Response.Write("<td>DEDUCT_AMT_A</td>");
            Response.Write("<td>DEDUCT_PCT_A</td>");
            Response.Write("<td>MAX_DEDUCT_LMT_A</td>");
            Response.Write("<td>BED_TYPE_A</td>");
            Response.Write("<td>MATERNITY_A</td>");
            Response.Write("<td>DENTAL_LIMIT_A</td>");
            Response.Write("<td>OPTICAL_A</td>");
            Response.Write("<td>OPTICAL_LIMIT_A</td>");
            Response.Write("<td>PRE_AUTH_OPT_A</td>");
            //2014-06-04 REF-1482 CR218 Nathan Start
            Response.Write("<td>HEART_VALVE_LIMIT_A</td>");
            Response.Write("<td>ORGAN_TRANS_LIMIT_A</td>");
            Response.Write("<td>HEARING_AID_LIMIT_A</td>");
            Response.Write("<td>NEWBORN_DIAG_LIMIT_A</td>");
            Response.Write("<td>ALZHEIMER_LIMIT_A</td>");
            Response.Write("<td>AUTISM_LIMIT_A</td>");
            Response.Write("<td>DISABILITY_LIMIT_A</td>");
            //2014-06-04 REF-1482 CR218 Nathan End
            Response.Write("<td>SCHEME_NAME_B</td>");
            Response.Write("<td>DEDUCT_AMT_B</td>");
            Response.Write("<td>DEDUCT_PCT_B</td>");
            Response.Write("<td>MAX_DEDUCT_LMT_B</td>");
            Response.Write("<td>BED_TYPE_B</td>");
            Response.Write("<td>MATERNITY_B</td>");
            Response.Write("<td>DENTAL_LIMIT_B</td>");
            Response.Write("<td>OPTICAL_B</td>");
            Response.Write("<td>OPTICAL_LIMIT_B</td>");
            Response.Write("<td>PRE_AUTH_OPT_B</td>");
            //2014-06-04 REF-1482 CR218 Nathan Start
            Response.Write("<td>HEART_VALVE_LIMIT_B</td>");
            Response.Write("<td>ORGAN_TRANS_LIMIT_B</td>");
            Response.Write("<td>HEARING_AID_LIMIT_B</td>");
            Response.Write("<td>NEWBORN_DIAG_LIMIT_B</td>");
            Response.Write("<td>ALZHEIMER_LIMIT_B</td>");
            Response.Write("<td>AUTISM_LIMIT_B</td>");
            Response.Write("<td>DISABILITY_LIMIT_B</td>");
            //2014-06-04 REF-1482 CR218 Nathan End
            Response.Write("<td>SCHEME_NAME_C</td>");
            Response.Write("<td>DEDUCT_AMT_C</td>");
            Response.Write("<td>DEDUCT_PCT_C</td>");
            Response.Write("<td>MAX_DEDUCT_LMT_C</td>");
            Response.Write("<td>BED_TYPE_C</td>");
            Response.Write("<td>MATERNITY_C</td>");
            Response.Write("<td>DENTAL_LIMIT_C</td>");
            Response.Write("<td>OPTICAL_C</td>");
            Response.Write("<td>OPTICAL_LIMIT_C</td>");
            Response.Write("<td>PRE_AUTH_OPT_C</td>");
            //2014-06-04 REF-1482 CR218 Nathan Start
            Response.Write("<td>HEART_VALVE_LIMIT_C</td>");
            Response.Write("<td>ORGAN_TRANS_LIMIT_C</td>");
            Response.Write("<td>HEARING_AID_LIMIT_C</td>");
            Response.Write("<td>NEWBORN_DIAG_LIMIT_C</td>");
            Response.Write("<td>ALZHEIMER_LIMIT_C</td>");
            Response.Write("<td>AUTISM_LIMIT_C</td>");
            Response.Write("<td>DISABILITY_LIMIT_C</td>");
            //2014-06-04 REF-1482 CR218 Nathan End
            Response.Write("<td>SCHEME_NAME_D</td>");
            Response.Write("<td>DEDUCT_AMT_D</td>");
            Response.Write("<td>DEDUCT_PCT_D</td>");
            Response.Write("<td>MAX_DEDUCT_LMT_D</td>");
            Response.Write("<td>BED_TYPE_D</td>");
            Response.Write("<td>MATERNITY_D</td>");
            Response.Write("<td>DENTAL_LIMIT_D</td>");
            Response.Write("<td>OPTICAL_D</td>");
            Response.Write("<td>OPTICAL_LIMIT_D</td>");
            Response.Write("<td>PRE_AUTH_OPT_D</td>");
            //2014-06-04 REF-1482 CR218 Nathan Start
            Response.Write("<td>HEART_VALVE_LIMIT_D</td>");
            Response.Write("<td>ORGAN_TRANS_LIMIT_D</td>");
            Response.Write("<td>HEARING_AID_LIMIT_D</td>");
            Response.Write("<td>NEWBORN_DIAG_LIMIT_D</td>");
            Response.Write("<td>ALZHEIMER_LIMIT_D</td>");
            Response.Write("<td>AUTISM_LIMIT_D</td>");
            Response.Write("<td>DISABILITY_LIMIT_D</td>");
            Response.Write("<td>NEW_CCHI_POLICY_IND</td>");
            //2014-06-04 REF-1482 CR218 Nathan End
            Response.Write("<td>SCHEME_NAME</td>");
            Response.Write("<td>SP_INSTRUCT1</td>");
            Response.Write("<td>SP_INSTRUCT2</td>");
            Response.Write("<td>SP_INSTRUCT3</td>");
            Response.Write("<td>FIRST_INSTRUCT_REC</td>");
            Response.Write("<td>LAST_INSTRUCT_REC</td>");
            Response.Write("</TR>");

            for (int i = 0; i < response.detail.Length; i++)
            {
                rptRow = rptDT.NewRow();
                rptRow["SEQ_NO"] = response.detail[i].Seqno;
                rptRow["REC_TYPE"] = response.detail[i].Rectype;
                rptRow["CONT_NO"] = response.detail[i].Contno;
                rptRow["CUST_NAME"] = response.detail[i].Custname;
                rptRow["EFF_DATE"] = response.detail[i].Effdate;
                rptRow["TERM_DATE"] = response.detail[i].Termdate;
                if (response.detail[i].ExtDate != null)
                {
                    rptRow["EXT_TERM_DATE"] = response.detail[i].ExtDate;
                }
                rptRow["SCHEME_NAME_A"] = response.detail[i].Schemenamea;
                rptRow["CONT_TYPE"] = response.detail[i].Conttype;
                rptRow["DEDUCT_AMT_A"] = response.detail[i].Deductamta;
                rptRow["DEDUCT_PCT_A"] = response.detail[i].Deductpcta;
                rptRow["MAX_DEDUCT_LMT_A"] = response.detail[i].Maxdeductlmta;
                rptRow["BED_TYPE_A"] = response.detail[i].Bedtypea;
                rptRow["MATERNITY_A"] = response.detail[i].Maternitya;
                rptRow["DENTAL_LIMIT_A"] = response.detail[i].Dentallimita;
                rptRow["OPTICAL_A"] = response.detail[i].Opticala;
                rptRow["OPTICAL_LIMIT_A"] = response.detail[i].Opticallimita;
                rptRow["PRE_AUTH_OPT_A"] = response.detail[i].Preauthopta;
                //2014-06-04 REF-1482 CR218 Nathan Start
                rptRow["HEART_VALVE_LIMIT_A"] = response.detail[i].HeartValvea;
                rptRow["ORGAN_TRANS_LIMIT_A"] = response.detail[i].OrganTransa;
                rptRow["HEARING_AID_LIMIT_A"] = response.detail[i].HearingAida;
                rptRow["NEWBORN_DIAG_LIMIT_A"] = response.detail[i].NewbornDiaga;
                rptRow["ALZHEIMER_LIMIT_A"] = response.detail[i].Alzheimera;
                rptRow["AUTISM_LIMIT_A"] = response.detail[i].Autisma;
                rptRow["DISABILITY_LIMIT_A"] = response.detail[i].Disabilitya;
                //2014-06-04 REF-1482 CR218 Nathan End
                

                rptRow["SCHEME_NAME_B"] = response.detail[i].Schemenameb;
                rptRow["DEDUCT_AMT_B"] = response.detail[i].Deductamtb;
                rptRow["DEDUCT_PCT_B"] = response.detail[i].Deductpctb;
                rptRow["MAX_DEDUCT_LMT_B"] = response.detail[i].Maxdeductlmtb;
                rptRow["BED_TYPE_B"] = response.detail[i].Bedtypeb;
                rptRow["MATERNITY_B"] = response.detail[i].Maternityb;
                rptRow["DENTAL_LIMIT_B"] = response.detail[i].Dentallimitb;
                rptRow["OPTICAL_B"] = response.detail[i].Opticalb;
                rptRow["OPTICAL_LIMIT_B"] = response.detail[i].Opticallimitb;
                rptRow["PRE_AUTH_OPT_B"] = response.detail[i].Preauthoptb;
                //2014-06-04 REF-1482 CR218 Nathan Start
                rptRow["HEART_VALVE_LIMIT_B"] = response.detail[i].HeartValveb;
                rptRow["ORGAN_TRANS_LIMIT_B"] = response.detail[i].OrganTransb;
                rptRow["HEARING_AID_LIMIT_B"] = response.detail[i].HearingAidb;
                rptRow["NEWBORN_DIAG_LIMIT_B"] = response.detail[i].NewbornDiagb;
                rptRow["ALZHEIMER_LIMIT_B"] = response.detail[i].Alzheimerb;
                rptRow["AUTISM_LIMIT_B"] = response.detail[i].Autismb;
                rptRow["DISABILITY_LIMIT_B"] = response.detail[i].Disabilityb;
                //2014-06-04 REF-1482 CR218 Nathan End
                rptRow["SCHEME_NAME_C"] = response.detail[i].Schemenamec;
                rptRow["DEDUCT_AMT_C"] = response.detail[i].Deductamtc;
                rptRow["DEDUCT_PCT_C"] = response.detail[i].Deductpctc;
                rptRow["MAX_DEDUCT_LMT_C"] = response.detail[i].Maxdeductlmtc;
                rptRow["BED_TYPE_C"] = response.detail[i].Bedtypec;
                rptRow["MATERNITY_C"] = response.detail[i].Maternityc;
                rptRow["DENTAL_LIMIT_C"] = response.detail[i].Dentallimitc;
                rptRow["OPTICAL_C"] = response.detail[i].Opticalc;
                rptRow["OPTICAL_LIMIT_C"] = response.detail[i].Opticallimitc;
                rptRow["PRE_AUTH_OPT_C"] = response.detail[i].Preauthoptc;
                //2014-06-04 REF-1482 CR218 Nathan Start
                rptRow["HEART_VALVE_LIMIT_C"] = response.detail[i].HeartValvec;
                rptRow["ORGAN_TRANS_LIMIT_C"] = response.detail[i].OrganTransc;
                rptRow["HEARING_AID_LIMIT_C"] = response.detail[i].HearingAidc;
                rptRow["NEWBORN_DIAG_LIMIT_C"] = response.detail[i].NewbornDiagc;
                rptRow["ALZHEIMER_LIMIT_C"] = response.detail[i].Alzheimerc;
                rptRow["AUTISM_LIMIT_C"] = response.detail[i].Autismc;
                rptRow["DISABILITY_LIMIT_C"] = response.detail[i].Disabilityc;
                //2014-06-04 REF-1482 CR218 Nathan End
                rptRow["SCHEME_NAME_D"] = response.detail[i].Schemenamed;
                rptRow["DEDUCT_AMT_D"] = response.detail[i].Deductamtd;
                rptRow["DEDUCT_PCT_D"] = response.detail[i].Deductpctd;
                rptRow["MAX_DEDUCT_LMT_D"] = response.detail[i].Maxdeductlmtd;
                rptRow["BED_TYPE_D"] = response.detail[i].Bedtyped;
                rptRow["MATERNITY_D"] = response.detail[i].Maternityd;
                rptRow["DENTAL_LIMIT_D"] = response.detail[i].Dentallimitd;
                rptRow["OPTICAL_D"] = response.detail[i].Opticald;
                rptRow["OPTICAL_LIMIT_D"] = response.detail[i].Opticallimitd;
                rptRow["PRE_AUTH_OPT_D"] = response.detail[i].Preauthoptd;
                //2014-06-04 REF-1482 CR218 Nathan Start
                rptRow["HEART_VALVE_LIMIT_D"] = response.detail[i].HeartValved;
                rptRow["ORGAN_TRANS_LIMIT_D"] = response.detail[i].OrganTransd;
                rptRow["HEARING_AID_LIMIT_D"] = response.detail[i].HearingAidd;
                rptRow["NEWBORN_DIAG_LIMIT_D"] = response.detail[i].NewbornDiagd;
                rptRow["ALZHEIMER_LIMIT_D"] = response.detail[i].Alzheimerd;
                rptRow["AUTISM_LIMIT_D"] = response.detail[i].Autismd;
                rptRow["DISABILITY_LIMIT_D"] = response.detail[i].Disabilityd;
                rptRow["NEW_CCHI_POLICY_IND"] = response.detail[i].NewCCHIPolicyInd;
                //2014-06-04 REF-1482 CR218 Nathan End
                rptRow["SCHEME_NAME"] = response.detail[i].Schemename;
                rptRow["SP_INSTRUCT1"] = response.detail[i].Spinstruct1;
                rptRow["SP_INSTRUCT2"] = response.detail[i].Spinstruct2;
                rptRow["SP_INSTRUCT3"] = response.detail[i].Spinstruct3;
                rptRow["FIRST_INSTRUCT_REC"] = response.detail[i].Firstinstructrec;
                rptRow["LAST_INSTRUCT_REC"] = response.detail[i].Lastinstructrec;


                //2014-06-18 REF-1493 CR222 Eric Shek, Start
                rptRow["SCHEME_DISPLAY_IND_A"] = response.detail[i].SchemeDisplayInda;
                rptRow["MATERNITY_SPI_A"] = response.detail[i].MaternitySPIa;
                rptRow["DENTAL_SPI_A"] = response.detail[i].DentalSPIa;
                rptRow["OPTICAL_SPI_A"] = response.detail[i].OpticalSPIa;
                rptRow["HEART_VALVE_SPI_A"] = response.detail[i].HeartValveSPIa;
                rptRow["ORGAN_TRANS_SPI_A"] = response.detail[i].OrganTransSPIa;
                rptRow["HEARING_AID_SPI_A"] = response.detail[i].HearingAidSPIa;
                rptRow["NEWBORN_DIAG_SPI_A"] = response.detail[i].NewbornDiagSPIa;
                rptRow["ALZHEIMER_SPI_A"] = response.detail[i].AlzheimerSPIa;
                rptRow["AUTISM_SPI_A"] = response.detail[i].AutismSPIa;
                rptRow["DISABILITY_SPI_A"] = response.detail[i].DisabilitySPIa;
                rptRow["SCHEME_DISPLAY_IND_B"] = response.detail[i].SchemeDisplayIndb;
                rptRow["MATERNITY_SPI_B"] = response.detail[i].MaternitySPIb;
                rptRow["DENTAL_SPI_B"] = response.detail[i].DentalSPIb;
                rptRow["OPTICAL_SPI_B"] = response.detail[i].OpticalSPIb;
                rptRow["HEART_VALVE_SPI_B"] = response.detail[i].HeartValveSPIb;
                rptRow["ORGAN_TRANS_SPI_B"] = response.detail[i].OrganTransSPIb;
                rptRow["HEARING_AID_SPI_B"] = response.detail[i].HearingAidSPIb;
                rptRow["NEWBORN_DIAG_SPI_B"] = response.detail[i].NewbornDiagSPIb;
                rptRow["ALZHEIMER_SPI_B"] = response.detail[i].AlzheimerSPIb;
                rptRow["AUTISM_SPI_B"] = response.detail[i].AutismSPIb;
                rptRow["DISABILITY_SPI_B"] = response.detail[i].DisabilitySPIb;
                rptRow["SCHEME_DISPLAY_IND_C"] = response.detail[i].SchemeDisplayIndc;
                rptRow["MATERNITY_SPI_C"] = response.detail[i].MaternitySPIc;
                rptRow["DENTAL_SPI_C"] = response.detail[i].DentalSPIc;
                rptRow["OPTICAL_SPI_C"] = response.detail[i].OpticalSPIc;
                rptRow["HEART_VALVE_SPI_C"] = response.detail[i].HeartValveSPIc;
                rptRow["ORGAN_TRANS_SPI_C"] = response.detail[i].OrganTransSPIc;
                rptRow["HEARING_AID_SPI_C"] = response.detail[i].HearingAidSPIc;
                rptRow["NEWBORN_DIAG_SPI_C"] = response.detail[i].NewbornDiagSPIc;
                rptRow["ALZHEIMER_SPI_C"] = response.detail[i].AlzheimerSPIc;
                rptRow["AUTISM_SPI_C"] = response.detail[i].AutismSPIc;
                rptRow["DISABILITY_SPI_C"] = response.detail[i].DisabilitySPIc;
                rptRow["SCHEME_DISPLAY_IND_D"] = response.detail[i].SchemeDisplayIndd;
                rptRow["MATERNITY_SPI_D"] = response.detail[i].MaternitySPId;
                rptRow["DENTAL_SPI_D"] = response.detail[i].DentalSPId;
                rptRow["OPTICAL_SPI_D"] = response.detail[i].OpticalSPId;
                rptRow["HEART_VALVE_SPI_D"] = response.detail[i].HeartValveSPId;
                rptRow["ORGAN_TRANS_SPI_D"] = response.detail[i].OrganTransSPId;
                rptRow["HEARING_AID_SPI_D"] = response.detail[i].HearingAidSPId;
                rptRow["NEWBORN_DIAG_SPI_D"] = response.detail[i].NewbornDiagSPId;
                rptRow["ALZHEIMER_SPI_D"] = response.detail[i].AlzheimerSPId;
                rptRow["AUTISM_SPI_D"] = response.detail[i].AutismSPId;
                rptRow["DISABILITY_SPI_D"] = response.detail[i].DisabilitySPId;
                //2014-06-18 REF-1493 CR222 Eric Shek, End

                rptDT.Rows.Add(rptRow);

                Response.Write("<tr>");
                Response.Write("<td>[" + response.detail[i].Seqno + "]</td>");
                Response.Write("<td>[" + response.detail[i].Rectype + "]</td>");
                Response.Write("<td>[" + response.detail[i].Contno + "]</td>");
                Response.Write("<td>[" + response.detail[i].Custname + "]</td>");
                Response.Write("<td>[" + response.detail[i].Effdate + "]</td>");
                Response.Write("<td>[" + response.detail[i].Termdate + "]</td>");
                if (response.detail[i].ExtDate != null)
                {
                    Response.Write("<td>[" + response.detail[i].ExtDate + "]</td>");
                }
                else
                {
                    Response.Write("<td>[No Extension Date]</td>");
                }
                Response.Write("<td>[" + response.detail[i].Conttype + "]</td>");
                Response.Write("<td>[" + response.detail[i].Schemenamea + "]</td>");
                Response.Write("<td>[" + response.detail[i].Deductamta + "]</td>");
                Response.Write("<td>[" + response.detail[i].Deductpcta + "]</td>");
                Response.Write("<td>[" + response.detail[i].Maxdeductlmta + "]</td>");
                Response.Write("<td>[" + response.detail[i].Bedtypea + "]</td>");
                Response.Write("<td>[" + response.detail[i].Maternitya + "]</td>");
                Response.Write("<td>[" + response.detail[i].Dentallimita + "]</td>");
                Response.Write("<td>[" + response.detail[i].Opticala + "]</td>");
                Response.Write("<td>[" + response.detail[i].Opticallimita + "]</td>");
                Response.Write("<td>[" + response.detail[i].Preauthopta + "]</td>");
                //2014-06-04 REF-1482 CR218 Nathan Start
                Response.Write("<td>[" + response.detail[i].HeartValvea + "]</td>");
                Response.Write("<td>[" + response.detail[i].OrganTransa + "]</td>");
                Response.Write("<td>[" + response.detail[i].HearingAida + "]</td>");
                Response.Write("<td>[" + response.detail[i].NewbornDiaga + "]</td>");
                Response.Write("<td>[" + response.detail[i].Alzheimera + "]</td>");
                Response.Write("<td>[" + response.detail[i].Autisma + "]</td>");
                Response.Write("<td>[" + response.detail[i].Disabilitya + "]</td>");
                //2014-06-04 REF-1482 CR218 Nathan End
                Response.Write("<td>[" + response.detail[i].Schemenameb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Deductamtb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Deductpctb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Maxdeductlmtb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Bedtypeb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Maternityb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Dentallimitb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Opticalb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Opticallimitb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Preauthoptb + "]</td>");
                //2014-06-04 REF-1482 CR218 Nathan Start
                Response.Write("<td>[" + response.detail[i].HeartValveb + "]</td>");
                Response.Write("<td>[" + response.detail[i].OrganTransb + "]</td>");
                Response.Write("<td>[" + response.detail[i].HearingAidb + "]</td>");
                Response.Write("<td>[" + response.detail[i].NewbornDiagb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Alzheimerb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Autismb + "]</td>");
                Response.Write("<td>[" + response.detail[i].Disabilityb + "]</td>");
                //2014-06-04 REF-1482 CR218 Nathan End
                Response.Write("<td>[" + response.detail[i].Schemenamec + "]</td>");
                Response.Write("<td>[" + response.detail[i].Deductamtc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Deductpctc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Maxdeductlmtc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Bedtypec + "]</td>");
                Response.Write("<td>[" + response.detail[i].Maternityc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Dentallimitc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Opticalc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Opticallimitc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Preauthoptc + "]</td>");
                //2014-06-04 REF-1482 CR218 Nathan Start
                Response.Write("<td>[" + response.detail[i].HeartValvec + "]</td>");
                Response.Write("<td>[" + response.detail[i].OrganTransc + "]</td>");
                Response.Write("<td>[" + response.detail[i].HearingAidc + "]</td>");
                Response.Write("<td>[" + response.detail[i].NewbornDiagc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Alzheimerc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Autismc + "]</td>");
                Response.Write("<td>[" + response.detail[i].Disabilityc + "]</td>");
                //2014-06-04 REF-1482 CR218 Nathan End
                Response.Write("<td>[" + response.detail[i].Schemenamed + "]</td>");
                Response.Write("<td>[" + response.detail[i].Deductamtd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Deductpctd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Maxdeductlmtd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Bedtyped + "]</td>");
                Response.Write("<td>[" + response.detail[i].Maternityd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Dentallimitd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Opticald + "]</td>");
                Response.Write("<td>[" + response.detail[i].Opticallimitd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Preauthoptd + "]</td>");
                //2014-06-04 REF-1482 CR218 Nathan Start
                Response.Write("<td>[" + response.detail[i].HeartValved + "]</td>");
                Response.Write("<td>[" + response.detail[i].OrganTransd + "]</td>");
                Response.Write("<td>[" + response.detail[i].HearingAidd + "]</td>");
                Response.Write("<td>[" + response.detail[i].NewbornDiagd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Alzheimerd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Autismd + "]</td>");
                Response.Write("<td>[" + response.detail[i].Disabilityd + "]</td>");
                Response.Write("<td>[" + response.detail[i].NewCCHIPolicyInd + "]</td>");
                //2014-06-04 REF-1482 CR218 Nathan End
                Response.Write("<td>[" + response.detail[i].Schemename + "]</td>");
                Response.Write("<td>[" + response.detail[i].Spinstruct1 + "]</td>");
                Response.Write("<td>[" + response.detail[i].Spinstruct2 + "]</td>");
                Response.Write("<td>[" + response.detail[i].Spinstruct3 + "]</td>");
                Response.Write("<td>[" + response.detail[i].Firstinstructrec + "]</td>");
                Response.Write("<td>[" + response.detail[i].Lastinstructrec + "]</td>");
                Response.Write("</tr>");
            }
            Response.Write("</table>");

            crDoc = new ReportDocument();
            crDoc.Load(MapPath(@"~\MRCUSTTOB02.rpt"));
            crDoc.SetDataSource(rptDT);
            crDoc.SetParameterValue("pc_report_id", "MRCUSTTOB02");
            crDoc.SetParameterValue("pc_report_group", "AL");
            crDoc.SetParameterValue("pv_company_name", "dd");
            crDoc.SetParameterValue("pd_report_date", "29/01/2013");
            crDoc.SetParameterValue("pv_title1", "ddd");
            crDoc.SetParameterValue("pc_rpt_grp", "AL");
            crDoc.SetParameterValue("pc_cont_no", ls_cont_no);
            crDoc.SetParameterValue("pc_cont_yymm", ls_cont_yymm);
            crViewer.ReportSource = crDoc;
            Session["report"] = crDoc;
        }
        else
        {
            Response.Write("Error : " + response.errorID + "<br>");
            Response.Write(response.errorMessage + "<br>");
        }
    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

}