﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using DevExpress.Web;
using DevExpress.Web.ASPxUploadControl;
using System.Web.UI;
using System.Drawing;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using DevExpress.Web.Demos;
using DevExpress.Web.ASPxGridView;
using System.Collections;

public partial class Provider_newservicecode : System.Web.UI.Page
{
    public string _ProviderCode = string.Empty;
    public string _ProviderName = string.Empty;
    public string _InitialRequestID = string.Empty;
    const string UploadDirectory = "~/backend/Uploads/";
    const string ThumbnailFileName = "ThumbnailImage.jpg";
    private Hashtable hasQueryValue;

    private string refno = string.Empty, status = string.Empty, mode = string.Empty; 

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
        {
            CheckConcurrentUserStatus();
        }

        btnUpload.Attributes.Add("onclick", "<script language=javascript1.2>window.location.reload(true);</script>");

        if(string.IsNullOrEmpty(Convert.ToString(Session["_InitialRequestID"])))
        {
            Response.Redirect("notify.aspx");
        }



        hasQueryValue = new Hashtable();
        string queryStringValues = string.Empty;

        if (Request.QueryString.Count > 0)
        {
            try
            {
                var val = Request.QueryString["val"];
                queryStringValues = Cryption.Decrypt(val);
                hasQueryValue = Cryption.GetQueryValue(queryStringValues);

                refno = hasQueryValue.ContainsKey("refno") ? Convert.ToString(hasQueryValue["refno"]).Trim() : string.Empty;
                status = hasQueryValue.ContainsKey("s") ? Convert.ToString(hasQueryValue["s"]) : string.Empty;
                mode = hasQueryValue.ContainsKey("mod") ? Convert.ToString(hasQueryValue["mod"]) : string.Empty;
                this.Session["ReferenceNumber"] = refno;
            }
            catch (Exception ex)
            {
                Response.Write("Invalid rquest!");
                return;
            }

        }
        else
        {
            Response.Write("Invalid rquest!");
            return;
        }

        _InitialRequestID = Convert.ToString(Session["_InitialRequestID"]);
        ///if (!string.IsNullOrEmpty(Request.QueryString["refno"]))
        if (!string.IsNullOrEmpty(refno))
        {
            _InitialRequestID = refno;
        }
        ////if (string.IsNullOrEmpty(Convert.ToString(Request.QueryString["s"])))
        if (string.IsNullOrEmpty(Convert.ToString(status)))
        {
            lblRefNo.Text = _InitialRequestID;
        }
        else
        {
            lblRefNo.Text = _InitialRequestID;
            ////lblstatus.Text = Convert.ToString(Request.QueryString["s"]);
            lblstatus.Text = Convert.ToString(status);
        }
        
        
        _ProviderCode = Convert.ToString(Session["ProviderID"]);
        _ProviderName = Convert.ToString(Session["ProviderName"]);
        lblProName.Text = _ProviderName;
        lblProCode.Text = _ProviderCode;
        //// lblstatus.Text = lblstatus.Text +  Convert.ToString(Request.QueryString["s"]);
        lblstatus.Text = lblstatus.Text + Convert.ToString(status);
        if (!string.IsNullOrEmpty(Convert.ToString(Session["added"])))
        {
            btnSubmit.Visible = true;
        }
        else
        {
            btnSubmit.Visible = false;
        }
        ////if (!string.IsNullOrEmpty(Request.QueryString["mod"]))
        if (!string.IsNullOrEmpty(mode))
        {
            ////if (Convert.ToString(Request.QueryString["mod"]) == "vw")
            if (Convert.ToString(mode) == "vw")
            {
                ((GridViewCommandColumn)detailGrid.Columns["CommandColumn"]).EditButton.Visible = false ;
                ((GridViewCommandColumn)detailGrid.Columns["CommandColumn"]).DeleteButton.Visible = false;
                detailGrid.Columns[0].Visible = false;
                ////switch (Convert.ToString(Request.QueryString["s"]))
                switch (Convert.ToString(status))
                {
                    case "For Confirmation":
                        hr1.Visible = false;
                        hr2.Visible = false;
                        hr.Visible = false;
                        fileupload.Enabled = true;
                        btnSubmitDoc.Visible = false;

                        btnSubmitDoc.ClientEnabled = false;
                        if (Convert.ToString(Session["submit"]) == "Yes")
                        {
                            btnSubmit.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                        }
                        detailGrid.Settings.ShowFilterRow = false;
                        break;
                    case "Rejected":
                        hr1.Visible = false;
                        hr2.Visible = false;
                        hr.Visible = false;
                        fileupload.Enabled = true;
                        btnSubmitDoc.Visible = false;

                        btnSubmitDoc.ClientEnabled = false;
                        if (Convert.ToString(Session["submit"]) == "Yes")
                        {
                            btnSubmit.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                        }
                        detailGrid.FilterEnabled = false;
                        detailGrid.Settings.ShowFilterRow = false;
                        break;
                    case "For Bupa Confirmation":
                        hr1.Visible = false;
                        hr2.Visible = false;
                        hr.Visible = false;
                        fileupload.Enabled = true;
                        btnSubmitDoc.Visible = false;
                        tblUp.Visible = false;
                        btnSubmitDoc.ClientEnabled = false;

                        btnSubmit.Visible = false;
                        break;
                    case "Submitted":
                        hr1.Visible = false;
                        hr2.Visible = false;
                        hr.Visible = false;
                        tblUp.Visible = false;
                        break;
                    case "Processing":
                        hr1.Visible = false;
                        hr2.Visible = false;
                        hr.Visible = false;
                        tblUp.Visible = false;
                        break;
                    case "Approved":
                        hr1.Visible = false;
                        hr2.Visible = false;
                        hr.Visible = false;
                        tblUp.Visible = false;
                        detailGrid.FilterEnabled = false;
                        detailGrid.Settings.ShowFilterRow = false;

                        break;
                    default:
                        hr1.Visible = false;
                        hr2.Visible = false;
                        hr.Visible = false;
                        fileupload.Enabled = true;
                        btnSubmitDoc.Visible = false;
                        btnSubmit.Visible = false;
                        detailGrid.Columns[0].Visible = true;
                        break;
                }


               
            }
            else
            {
                hr1.Visible = true;
                hr2.Visible = true;
                hr.Visible = true;
                fileupload.Enabled = true;
                btnSubmitDoc.Visible = false;
                btnSubmit.Visible = true;
                
                //tblUp.Visible = false;
            }
        }
        else
        {
            hr1.Visible = true;
            hr2.Visible = true;
            hr.Visible = true;
            fileupload.Enabled = true;
            btnSubmitDoc.Visible = false;
            btnSubmit.Visible = true;
            //tblUp.Visible = false;
            ((GridViewDataTextColumn)detailGrid.Columns["StatusR"]).Visible = false;
            ((GridViewDataTextColumn)detailGrid.Columns["RemarkR"]).Visible = false;
        }
    }


    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["ProCurrentLogID"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }

    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            //}
            //catch
            //{
            //    lblError.Text = "The connection with the database can´t be established. Application Error";

            //}

            SqlParameter[] arParms = new SqlParameter[7];

            // @ProductID Input Parameter 
            // assign value = 1
            arParms[0] = new SqlParameter("@RefNo", SqlDbType.NVarChar, 50);
            arParms[0].Value = _InitialRequestID;

            arParms[1] = new SqlParameter("@ProviderCode", SqlDbType.NVarChar, 10);
            arParms[1].Value = lblProCode.Text.Trim();

            // @ProductName Output Parameter
            arParms[2] = new SqlParameter("@Service_Code", SqlDbType.NVarChar, 10);
            arParms[2].Value = txtServiceCode.Text.Trim();

            // @UnitPrice Output Parameter
            arParms[3] = new SqlParameter("@Description", SqlDbType.NText);
            arParms[3].Value = txtDesc.Text.Trim();
            // @QtyPerUnit Output Parameter
            arParms[4] = new SqlParameter("@Price", SqlDbType.NVarChar, 5);
            arParms[4].Value = txtPrice.Text.Trim();
            // @ProductNo Output Parameter
            arParms[5] = new SqlParameter("@Service_Type", SqlDbType.NVarChar, 50);
            arParms[5].Value = cmServiceType.SelectedItem.Value;

            arParms[6] = new SqlParameter("@Status", SqlDbType.NVarChar, 50);
            arParms[6].Value = "Submitted";
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "insertServiceCode", arParms);

            txtPrice.Text = "";
            txtDesc.Text = "";
            txtServiceCode.Text = "";
            cmServiceType.SelectedIndex = 0;
            detailGrid.DataBind();
            Session["added"] = "Yes";
        }
        catch
        {

            lblError.Text = "The connection with the database can´t be established. Application Error";

        }
        finally {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SqlConnection connection = null;
        SqlParameter[] arParms = new SqlParameter[3];
        /// switch (Convert.ToString(Request.QueryString["s"]))
        switch (Convert.ToString(status))
        {
            case "For Confirmation":

                try
                {
                    connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
                    //}
                    //catch
                    //{


                    //}

                    arParms[0] = new SqlParameter("@RefNo", SqlDbType.NChar, 20);
                    ////arParms[0].Value = Convert.ToString(Request["refno"]).Trim();
                    arParms[0].Value = Convert.ToString(refno).Trim();

                    arParms[1] = new SqlParameter("@Status", SqlDbType.NVarChar, 50);
                    arParms[1].Value = "For Bupa Confirmation";

                    arParms[2] = new SqlParameter("@BupaUser", SqlDbType.NVarChar, 10);
                    arParms[2].Value = Convert.ToString(Session["user"]);

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Update_ServiceCodeStatus", arParms);
                    Session["_InitialRequestID"] = null;
                    Session["added"] = null;
                    Session["submit"] = null;
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    Response.Redirect("notify.aspx");
                }
                catch { }
                finally {

                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
                break;
            case "Rejected":

                try
                {
                    connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
                //}
                //catch
                //{


                //}

                arParms[0] = new SqlParameter("@RefNo", SqlDbType.NChar, 20);
                    ////arParms[0].Value = Convert.ToString(Request["refno"]).Trim();
                    arParms[0].Value = Convert.ToString(refno).Trim();

                    arParms[1] = new SqlParameter("@Status", SqlDbType.NVarChar, 50);
                arParms[1].Value = "For Bupa Confirmation";

                arParms[2] = new SqlParameter("@BupaUser", SqlDbType.NVarChar, 10);
                arParms[2].Value = Convert.ToString(Session["user"]);

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Update_ServiceCodeStatus", arParms);
                Session["_InitialRequestID"] = null;
                Session["added"] = null;
                Session["submit"] = null;
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                Response.Redirect("notify.aspx");
                }
                catch { }
                finally
                {

                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
                break;
            default:
               
                try
                {
                    connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
                //}
                //catch
                //{


                //}
               
                arParms[0] = new SqlParameter("@RefNo", SqlDbType.NChar, 20);
                arParms[0].Value = Convert.ToString(Session["_InitialRequestID"]).Trim();
            
                arParms[1] = new SqlParameter("@Status", SqlDbType.NVarChar, 50);
                arParms[1].Value = "Submitted";

                arParms[2] = new SqlParameter("@BupaUser", SqlDbType.NVarChar, 10);
                arParms[2].Value = Convert.ToString(Session["user"]);

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Update_ServiceCodeStatus", arParms);
                Session["_InitialRequestID"] = null;
                Session["added"] = null;
                Session["submit"] = null;
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                Response.Redirect("notify.aspx");
                }
                catch { }
                finally
                {

                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
                break;
        }


     
    }
    
    protected void uploadImageCtrl_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {

    }

	/*
    protected void SavePostedFile(FileUpload uploadedFile)
    {
		Boolean fileOK = true;
        if (uploadedFile.HasFile)
        {
            String fileExtension = System.IO.Path.GetExtension(uploadedFile.FileName).ToLower();
            String[] allowedExtensions = { ".pdf", ".jpg", ".jpeg", ".gif", ".png", ".bmp" };
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtension == allowedExtensions[i])
                {
                    fileOK = true;
                    break;
                }
            }
            if (fileOK)
            {

            string fileName = Path.Combine(MapPath(UploadDirectory), uploadedFile.FileName);
           // using (System.Drawing.Image original = System.Drawing.Image.FromStream(uploadedFile.FileContent))
          //  using (System.Drawing.Image thumbnail = PhotoUtils.Inscribe(original, 300))
            //{
            //    PhotoUtils.SaveToJpeg(thumbnail, fileName);
            //}
            uploadedFile.SaveAs(fileName);
            SqlConnection connection = null;
            try
            {
                connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch {}
            SqlParameter[] arParms = new SqlParameter[2];

        
            arParms[0] = new SqlParameter("@RefNo", SqlDbType.NChar, 20);
            arParms[0].Value = Convert.ToString(Request["refno"]);

            arParms[1] = new SqlParameter("@DocPath", SqlDbType.NVarChar, 400);
            arParms[1].Value = uploadedFile.FileName;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "[Insert_ServiceDoc]", arParms);
            Session["submit"] = "Yes";
            btnSubmit.Visible = true;
            
        }

	}
       
     
    }

	*/
	
	protected void SavePostedFile(FileUpload uploadedFile)
    {
        Boolean fileOK = false;
        if (uploadedFile.HasFile)
        {
            String fileExtension = System.IO.Path.GetExtension(uploadedFile.FileName).ToLower();
            //Added new code to check MimeType
            string fileExtention = System.IO.Path.GetExtension(uploadedFile.PostedFile.FileName).ToLower();
            Boolean mimeOK = false;
            CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
            string mime = findMimeFromDate.CheckFindMimeFromData(uploadedFile.PostedFile);
            String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
            String[] allowedExtensions = { ".pdf", ".jpg", ".jpeg", ".gif", ".png", ".bmp" };
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtension == allowedExtensions[i])
                {
                    fileOK = true;
                    break;
                }
            }
            for (int i = 0; i < allowedMime.Length; i++)
            {
                if (mime == allowedMime[i])
                {
                    mimeOK = true;
                    break;
                }
            }
            if (fileOK && mimeOK)
            {
                string fileName = Path.Combine(MapPath(UploadDirectory), uploadedFile.FileName);
                uploadedFile.SaveAs(fileName);
                SqlConnection connection = null;
                try
                {
                    connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
                }
                catch { }
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@RefNo", SqlDbType.NChar, 20);
                //// arParms[0].Value = Convert.ToString(Request["refno"]);
                arParms[0].Value = Convert.ToString(refno);

                arParms[1] = new SqlParameter("@DocPath", SqlDbType.NVarChar, 400);
                arParms[1].Value = uploadedFile.FileName;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "[Insert_ServiceDoc]", arParms);
                Session["submit"] = "Yes";
                btnSubmit.Visible = true;
				lblError.Text = "";
            }
			else
                lblError.Text = "Files must be of the following types: (pdf, jpg, jpeg, gif, png, bmp)";
        }
    }

	
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        SavePostedFile(fileupload);
        ASPxTreeList1.DataBind();
       
    }
    protected void btnSubmitDoc_Click(object sender, EventArgs e)
    {
        
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "myCloseScript", "window.close()", true);
    }
    protected void detailGrid_CustomCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
    {
        
    }
    protected string ShowURL(string _path)
    {

        return "../backend/Uploads/" + _path; 
    }
    protected void ASPxTreeList1_CustomCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomCallbackEventArgs e)
    {
        ASPxTreeList1.DataBind();
    }
}