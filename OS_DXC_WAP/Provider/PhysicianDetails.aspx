﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Provider_PhysicianDetails" Codebehind="PhysicianDetails.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/ProviderStyles.css" rel="stylesheet" />
    <script>
        window.validationFailed = function (radAsyncUpload, args) {
            var $row = $(args.get_row());
            var erorMessage = getErrorMessage(radAsyncUpload, args);
            var span = createError(erorMessage);
            $row.addClass("ruError");
            $row.append(span);
        }

        function getErrorMessage(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    return ("This file type is not supported.");
                }
                else {
                    return ("This file exceeds the maximum allowed size of 500 KB.");
                }
            }
            else {
                return ("not correct extension.");
            }
        }

        function createError(erorMessage) {
            var input = '<span class="ruErrorMessage">' + erorMessage + ' </span>';
            return input;
        }

        function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32)
                    return true;
                else {
                    alert('Special characters and Numbers are not accepted.');
                    return false;
                }
            }
            catch (err) {
                alert(err.Description);
            }
        }
    </script>
    <table align="center" width="720px">
        <tr>
            <td align="left" width="75%">
                <h1>Doctor's Details
                </h1>
            </td>

        </tr>
        <tr>
            <td height="6px"></td>
        </tr>
        <tr>
            <td colspan="2">

                <fieldset style="padding: 10px; width: 760px;">
                    <legend>
                        <h1 class="Fieldset">
                            <asp:Label ID="lblHeading" runat="server" Text="Manage Physicians"></asp:Label>
                        </h1>
                    </legend>
                    <table width="700px">
                        <tr>
                            <td colspan="4">
                                <asp:Label runat="server" ID="lblMessage" CssClass="label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td height="6px"></td>
                        </tr>
                        <tr>
                            <td width="120px">
                                <asp:Label ID="lblProviderCode" CssClass="label" runat="server" Text="Provider Code"></asp:Label>
                                <span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:Label ID="lblProviderCodevalue" CssClass="label" runat="server"></asp:Label>
                                <asp:TextBox runat="server" ID="txtProviderCode" MaxLength="10" Visible="false" CssClass="textboxPP" Width="90px"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="rfv1" ControlToValidate="txtProviderCode" EnableClientScript="false" ValidationGroup="SearchProvider"></asp:RequiredFieldValidator>
                                <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click" ValidationGroup="SearchProvider" CssClass="btnGradient" Visible="false" />
                            </td>

                            <td width="120px">
                                <asp:Label ID="lblProviderName" CssClass="label" runat="server" Text="Provider Name"></asp:Label>
                            </td>
                            <td width="190px">
                                <asp:Label ID="lblProviderNamevalue" CssClass="label" runat="server"></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblPhysicianName" CssClass="label" runat="server" Text="Physician Name"></asp:Label>
                                <span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPhysicianName" MaxLength="100" CssClass="textboxPP" onkeypress="return onlyAlphabets(event,this);"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtPhysicianName" EnableClientScript="false" ValidationGroup="MandatoryFields"></asp:RequiredFieldValidator>
                              
                            </td>
                            <td>
                                <asp:Label ID="lblDepartment" CssClass="label" runat="server" Text="Department"></asp:Label>
                                <span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDepartment" CssClass="DropDownList" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDepartment" EnableClientScript="false" ErrorMessage=""
                                    InitialValue="-1" ValidationGroup="MandatoryFields" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>


                            <td>
                                <asp:Label ID="lblStatus" CssClass="label" runat="server" Text="Status"></asp:Label>
                                <span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPhysicianStatus" CssClass="DropDownList" runat="server">
                                    <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                    <asp:ListItem>True</asp:ListItem>
                                    <asp:ListItem>False</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RfvStatus" runat="server" ControlToValidate="ddlPhysicianStatus" EnableClientScript="false" ErrorMessage=""
                                    InitialValue="-1" ValidationGroup="MandatoryFields" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td runat="server" visible="false">
                                <asp:Label ID="lblPhysiciantMainenance" CssClass="label" runat="server" Text="Is Trusted"></asp:Label>
                                <span style="color: red">*</span>
                            </td>
                            <td runat="server" visible="false">
                                <asp:DropDownList ID="ddlPhysicianMaintenance" CssClass="DropDownList" runat="server">
                                    <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                    <asp:ListItem>True</asp:ListItem>
                                    <asp:ListItem Selected="True">False</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RfvPhysicianMaintenance" runat="server" ControlToValidate="ddlPhysicianMaintenance" EnableClientScript="false" ErrorMessage=""
                                    InitialValue="-1" ValidationGroup="MandatoryFields" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: right; padding-right: 10px;">
                                <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" CssClass="btnGradient" />
                                <asp:Button runat="server" ID="btnAdd" OnClick="btnAdd_Click" CssClass="btnGradient" Text="Submit" ValidationGroup="MandatoryFields" />
                                <asp:Button runat="server" ID="btnUpdate" Visible="false" OnClick="btnUpdate_Click" CssClass="btnGradient" Text="Update" ValidationGroup="MandatoryFields" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
            <tr>
            <td height="6px"></td>
        </tr>
        <tr runat="server" visible="false" id="trUpload">
            <td colspan="4">
                <table>
                    <tr>
                        <td class="col1">
                            <asp:Label ID="lblDocumentUpload" CssClass="label" runat="server" Text="Select Document"></asp:Label>
                            <span style="color: red">*</span>
                        </td>
                        <td class="col2">
                            <telerik:RadAsyncUpload runat="server" ID="AsyncUpload1" Skin="Office2007" ChunkSize="1048576" MultipleFileSelection="Disabled" MaxFileInputsCount="1" AllowedFileExtensions=".xls,.XLS,.xlsx,.XSLX"
                                MaxFileSize="524288" OnFileUploaded="AsyncUpload1_FileUploaded" OnClientValidationFailed="validationFailed" UploadedFilesRendering="BelowFileInput" />

                        </td>
                        <td class="col1" style="padding-left: 60px;">
                            <asp:Button ID="btnUploadAttachment" runat="server" Text="Submit" CssClass="btnGradient" Width="100px" ValidationGroup="Attachments" OnClick="btnUploadAttachment_Click" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 6px;"></td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td height="6px"></td>
        </tr>
        <tr>
            <td colspan="2" style="align-items: center;">
                <telerik:RadGrid runat="server" ID="rgProviderManagement" AutoGenerateColumns="false" Width="900px" AllowFilteringByColumn="true" Skin="Office2007" AllowPaging="true" PageSize="10" OnNeedDataSource="rgProviderManagement_NeedDataSource" MasterTableView-CommandItemSettings-ShowAddNewRecordButton="true" OnItemCommand="rgProviderManagement_ItemCommand">
                    <MasterTableView ShowHeadersWhenNoRecords="true" NoMasterRecordsText="No records to display" CommandItemDisplay="Top" DataKeyNames="PPM_Id">
                        <CommandItemTemplate>
                            <table>
                                <tr>
                                    <td height="25px;">&nbsp;&nbsp; 
                            <asp:Label runat="server" Height="20px" ID="lblPharmacyHeading" Text="Physicians List" Font-Bold="true" Font-Size="Larger"></asp:Label>
                               
                             &nbsp;&nbsp;   &nbsp;&nbsp;   &nbsp;&nbsp;   &nbsp;&nbsp;   &nbsp;&nbsp;   &nbsp;&nbsp; 
                             <a id="hrefPriceList" style="cursor: pointer; color: blue;"
                                href="<%= Page.ResolveUrl("~/Docs/Provider Physician Template.xlsx") %>" title="Physician List">Click here for Upload Template </a>
                                               &nbsp;&nbsp;   &nbsp;&nbsp; 
                            <asp:Button runat="server" ID="btnUpload" Text="Upload Physicians" OnClick="btnUpload_Click" />
                                    </td>
                                </tr>
                            </table>

                        </CommandItemTemplate>
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Provider Code" DataField="PM_ProviderId" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Provider Name" DataField="ProviderName" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Physician Name" DataField="PPM_PhysicianName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Department" DataField="departmentdescription">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Is Trusted" DataField="PPM_IsTrusted" HeaderStyle-Width="80px" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Active" DataField="PPM_Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Action By" DataField="PPM_ActionBy" AllowFiltering="false" HeaderStyle-Width="80px">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Action Date" DataField="PPM_ActionDate" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Action" AllowFiltering="false" HeaderStyle-Width="70px">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="btnEditProvider" CommandName="EditProvider" Text="Delete" ImageUrl="~/images/Edit.png" ToolTip="Top" />
                                    <asp:ImageButton runat="server" ID="btnDeleteProvider" CommandName="DeleteProvider" Text="Delete" ImageUrl="~/images/Delete.png" ToolTip="Top" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>

                        </Columns>
                        <CommandItemStyle CssClass="rgCommandStyle" />
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Content>

