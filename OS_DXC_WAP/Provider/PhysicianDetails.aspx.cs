﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using WebApp;

public partial class Provider_PhysicianDetails : System.Web.UI.Page
{
    string ProviderId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        ProviderId = Convert.ToString(Session["ProviderID"]);
        lblMessage.Text = string.Empty;
        if (Page.IsPostBack == false)
        {
            try
            {


                BindGrid();
                FillDropDowns();
                lblProviderCodevalue.Text = ProviderId;
                using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
                {
                    List<TD_Provider> objProviderList = objTruestedDoctors.TD_Provider.Where(ItemFlow => ItemFlow.proID == ProviderId).ToList();
                    lblProviderNamevalue.Text = objProviderList[0].ProName;
                    lblProviderCodevalue.Text = objProviderList[0].proID;
                }
            }
            catch (Exception ex)
            {
                CommonClass.LogError("PageLoad", ex.Message, ex.InnerException, ex.StackTrace, Session["ProviderUserName"].ToString());
                lblMessage.Text = "We are unable to process your request.Please try later";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

    private void BindGrid()
    {
        try
        {
            using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
            {
                List<sp_GetTrustedProviderPhysicians_Result> list = objTruestedDoctors.sp_GetTrustedProviderPhysicians().Where(item => item.PPM_ProviderId == ProviderId).OrderByDescending(item => item.PPM_ActionDate).ToList();

                Session["TrustedProviderPhysicians"] = list;
            }
            rgProviderManagement.Rebind();
        }
        catch (Exception ex)
        {
            CommonClass.LogError("BindGrid", ex.Message, ex.InnerException, ex.StackTrace, Session["ProviderUserName"].ToString());
            lblMessage.Text = "We are unable to process your request.Please try later";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Providerdefault.aspx");
    }

    protected void rgProviderManagement_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (Session["TrustedProviderPhysicians"] != null)
            rgProviderManagement.DataSource = Session["TrustedProviderPhysicians"];
        else
            rgProviderManagement.DataSource = string.Empty;
    }
    protected void rgProviderManagement_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {

            if (e.CommandName == "EditProvider")
            {
                GridDataItem dataitem = e.Item as GridDataItem;
                int strId = Convert.ToInt32(dataitem.GetDataKeyValue("PPM_Id").ToString());
                using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
                {
                    ProviderPhysiciansManagement objProviderPhysiciansManagement = objTruestedDoctors.ProviderPhysiciansManagements.Where(item => item.PPM_Id == strId).SingleOrDefault();
                    ddlPhysicianMaintenance.SelectedIndex = ddlPhysicianMaintenance.Items.IndexOf(ddlPhysicianMaintenance.Items.FindByValue(objProviderPhysiciansManagement.PPM_IsTrusted.ToString()));
                    ddlPhysicianStatus.SelectedIndex = ddlPhysicianStatus.Items.IndexOf(ddlPhysicianStatus.Items.FindByValue(objProviderPhysiciansManagement.PPM_Status.ToString()));
                    ddlDepartment.SelectedIndex = ddlDepartment.Items.IndexOf(ddlDepartment.Items.FindByValue(objProviderPhysiciansManagement.PPM_Department.ToString()));
                    txtPhysicianName.Text = objProviderPhysiciansManagement.PPM_PhysicianName;
                    sp_GetTrustedProviderPhysicians_Result objsp_GetTrustedProviderPhysicians_Result1 = ((List<sp_GetTrustedProviderPhysicians_Result>)Session["TrustedProviderPhysicians"]).Where(item => item.PPM_Id == strId).SingleOrDefault();
                    lblProviderNamevalue.Text = objsp_GetTrustedProviderPhysicians_Result1.ProviderName;
                    lblProviderCodevalue.Text = objsp_GetTrustedProviderPhysicians_Result1.PPM_ProviderId;
                    ViewState["PPM_Id"] = objProviderPhysiciansManagement.PPM_Id;
                    btnAdd.Visible = false;
                    btnUpdate.Visible = true;
                    btnClear.Text = "Cancel";
                }

            }
            else if (e.CommandName == "DeleteProvider")
            {
                GridDataItem dataitem = e.Item as GridDataItem;
                int strId = Convert.ToInt32(dataitem.GetDataKeyValue("PPM_Id").ToString());
                using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
                {
                    ProviderPhysiciansManagement objProviderPhysicianMaintenance = objTruestedDoctors.ProviderPhysiciansManagements.Where(item => item.PPM_Id == strId).SingleOrDefault();
                    if (objProviderPhysicianMaintenance != null)
                    {
                        objTruestedDoctors.ProviderPhysiciansManagements.Add(objProviderPhysicianMaintenance);
                        objTruestedDoctors.Entry(objProviderPhysicianMaintenance).State = System.Data.Entity.EntityState.Deleted;
                        objTruestedDoctors.SaveChanges();
                        ClearFields();
                        BindGrid();
                        lblMessage.Text = "Request processed successfully.";
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        lblMessage.Text = "We are unable to process your request.Please try later";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            CommonClass.LogError("rgProvider_ItemCommand", ex.Message, ex.InnerException, ex.StackTrace, Session["ProviderUserName"].ToString());
            lblMessage.Text = "We are unable to process your request.Please try later";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearFields();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (lblProviderCodevalue.Text == "")
        {
            lblMessage.Text = "Invalid Provider code.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        if (Page.IsValid)
        {
            try
            {

                using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
                {
                    ProviderPhysiciansManagement objProviderPhysicianMaintenance = new ProviderPhysiciansManagement();
                    objProviderPhysicianMaintenance.PPM_ProviderId = lblProviderCodevalue.Text.Trim();
                    objProviderPhysicianMaintenance.PPM_PhysicianName = txtPhysicianName.Text.Trim();
                    objProviderPhysicianMaintenance.PPM_Department = ddlDepartment.SelectedValue;
                    objProviderPhysicianMaintenance.PPM_IsTrusted = false;
                    objProviderPhysicianMaintenance.PPM_Status = Convert.ToBoolean(ddlPhysicianStatus.SelectedValue);
                    objProviderPhysicianMaintenance.PPM_ActionBy = Session["ProviderUserName"].ToString();
                    objProviderPhysicianMaintenance.PPM_ActionDate = DateTime.Now;
                    objTruestedDoctors.ProviderPhysiciansManagements.Add(objProviderPhysicianMaintenance);
                    int ddlStatus = objTruestedDoctors.SaveChanges();
                    BindGrid();
                    ClearFields();
                    lblMessage.Text = "Request processed successfully.";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                }
            }
            catch (Exception ex)
            {
                CommonClass.LogError("btnAdd", ex.Message, ex.InnerException, ex.StackTrace, Session["ProviderUserName"].ToString());
                lblMessage.Text = "We are unable to process your request.Please try later";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        else
        {
            lblMessage.Text = "Fields marked with * are mandatory.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    private void FillDropDowns()
    {
        try
        {
            using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
            {
                List<BHA_Department> objDepartmentList = objTruestedDoctors.BHA_Department.ToList().OrderBy(items => items.DepartmentDescription).ToList();
                ddlDepartment.DataSource = objDepartmentList;
                ddlDepartment.DataTextField = "DepartmentDescription";
                ddlDepartment.DataValueField = "Departmentcode";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("--Select--", "-1"));
                Session["DepartmentList"] = objDepartmentList;
            }
        }
        catch { }
    }

    private void ClearFields()
    {
        txtPhysicianName.Text = string.Empty;
        ddlPhysicianMaintenance.SelectedIndex = -1;
        ddlDepartment.SelectedIndex = -1;
        ddlPhysicianStatus.SelectedIndex = -1;
        btnUpdate.Visible = false;
        btnClear.Text = "Clear";
        btnAdd.Visible = true;
        ViewState["PPM_Id"] = null;

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (lblProviderCodevalue.Text == "")
        {
            lblMessage.Text = "Invalid Provider code.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        if (Page.IsValid)
        {
            try
            {
                using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
                {
                    ProviderPhysiciansManagement objProviderPhysicianMaintenance = new ProviderPhysiciansManagement();
                    objProviderPhysicianMaintenance.PPM_Id = Convert.ToInt32(ViewState["PPM_Id"]);
                    objProviderPhysicianMaintenance.PPM_ProviderId = lblProviderCodevalue.Text.Trim();
                    objProviderPhysicianMaintenance.PPM_PhysicianName = txtPhysicianName.Text.Trim();
                    objProviderPhysicianMaintenance.PPM_Department = ddlDepartment.SelectedValue;
                    objProviderPhysicianMaintenance.PPM_IsTrusted = Convert.ToBoolean(ddlPhysicianMaintenance.SelectedValue);
                    objProviderPhysicianMaintenance.PPM_Status = Convert.ToBoolean(ddlPhysicianStatus.SelectedValue);
                    objProviderPhysicianMaintenance.PPM_ActionBy = Session["ProviderUserName"].ToString();
                    objProviderPhysicianMaintenance.PPM_ActionDate = DateTime.Now;
                    objTruestedDoctors.ProviderPhysiciansManagements.Add(objProviderPhysicianMaintenance);
                    objTruestedDoctors.Entry(objProviderPhysicianMaintenance).State = System.Data.Entity.EntityState.Modified;
                    objTruestedDoctors.SaveChanges();
                    BindGrid();
                    ClearFields();
                    lblMessage.Text = "Request processed successfully.";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                }
            }
            catch (Exception ex)
            {
                CommonClass.LogError("btnUpdate", ex.Message, ex.InnerException, ex.StackTrace, Session["ProviderUserName"].ToString());
                lblMessage.Text = "We are unable to process your request.Please try later";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        else
        {
            lblMessage.Text = "Fields marked with * are mandatory.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {



            using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
            {
                ProviderPhysicianMaster objProviderPhysicianMaster = objTruestedDoctors.ProviderPhysicianMasters.Where(item => item.PM_ProviderId.Trim() == ProviderId).SingleOrDefault();
                List<TD_Provider> objProviderList = objTruestedDoctors.TD_Provider.Where(ItemFlow => ItemFlow.proID == ProviderId).ToList();
                if (objProviderPhysicianMaster == null)
                {
                    lblMessage.Text = "Unable to verify the entered Provider under Trusted list.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    sp_GetTrustedProviderPhysicians_Result objGetTrustedProviderPhysicians_Result = objTruestedDoctors.sp_GetTrustedProviderPhysicians().Where(item => item.PM_ProviderId.Trim() == ProviderId).SingleOrDefault();
                    if (objGetTrustedProviderPhysicians_Result != null)
                    {
                        ProviderPhysiciansManagement objProviderPhysiciansManagement = objTruestedDoctors.ProviderPhysiciansManagements.Where(item => item.PPM_ProviderId.Trim() == ProviderId).SingleOrDefault();
                        ddlPhysicianMaintenance.SelectedIndex = ddlPhysicianMaintenance.Items.IndexOf(ddlPhysicianMaintenance.Items.FindByValue(objProviderPhysiciansManagement.PPM_IsTrusted.ToString()));
                        ddlPhysicianStatus.SelectedIndex = ddlPhysicianStatus.Items.IndexOf(ddlPhysicianStatus.Items.FindByValue(objProviderPhysiciansManagement.PPM_Status.ToString()));
                        ddlDepartment.SelectedIndex = ddlDepartment.Items.IndexOf(ddlDepartment.Items.FindByValue(objProviderPhysiciansManagement.PPM_Department.ToString()));
                        txtPhysicianName.Text = objProviderPhysiciansManagement.PPM_PhysicianName;
                        lblProviderNamevalue.Text = objGetTrustedProviderPhysicians_Result.ProviderName;
                        lblProviderCodevalue.Text = objGetTrustedProviderPhysicians_Result.PPM_ProviderId.Trim();
                        ViewState["PPM_Id"] = objGetTrustedProviderPhysicians_Result.PPM_Id;
                        btnAdd.Visible = false;
                        btnUpdate.Visible = true;
                        btnClear.Text = "Cancel";

                    }
                    else
                    {
                        lblProviderNamevalue.Text = objProviderList[0].ProName;
                        lblProviderCodevalue.Text = objProviderList[0].proID.Trim();
                        btnAdd.Visible = true;
                        btnUpdate.Visible = false;
                    }
                }

            }
        }
        catch (Exception ex)
        {
            CommonClass.LogError("btnSearch", ex.Message, ex.InnerException, ex.StackTrace, Session["ProviderUserName"].ToString());
            lblMessage.Text = "We are unable to process your request.Please try later";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        trUpload.Visible = true;
    }
    protected void AsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
    {
        try
        {
            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\" + AsyncUpload1.UploadedFiles[0].FileName;
            AsyncUpload1.UploadedFiles[0].SaveAs(FilePath);
            ViewState["FilePath"] = FilePath;

        }
        catch (Exception ex)
        {

            CommonClass.LogError("AsyncUpload", ex.Message, ex.InnerException, ex.StackTrace, Session["ProviderUserName"].ToString());
        }

    }
    protected void btnUploadAttachment_Click(object sender, EventArgs e)
    {
        //string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\"  + AsyncUpload1.UploadedFiles[0].FileName;
        //AsyncUpload1.UploadedFiles[0].SaveAs(FilePath);
        if (ViewState["FilePath"] == null)
        {
            lblMessage.Text = "Please select the document to process your request.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        DataTable dt = ParseBatchFile(ViewState["FilePath"].ToString());
        if (dt.Rows.Count > 0 && dt.Columns.Count == 2)
        {
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                try
                {
                    using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
                    {
                        ProviderPhysiciansManagement objProviderPhysicianMaintenance = new ProviderPhysiciansManagement();
                        objProviderPhysicianMaintenance.PPM_ProviderId = ProviderId;
                        string PhysicianName = dt.Rows[k][0].ToString().Trim();
                        PhysicianName = PhysicianName.Replace("(", " ").Replace(")", " ").Replace("-", " ").Replace("_", " ").Replace("*", " ").Replace("'", "").Replace(",", " ").Replace("[", " ").Replace("]", " ").Replace("&", " ").Replace(".", " ").Replace("/", " ").Replace("?", " ").Replace("!", " ").Replace("@", " ").Replace("#", " ").Replace("$", " ").Replace("%", " ").Replace("^", " ").Replace("+", " ").Replace("=", " ").Replace("<", " ").Replace(">", " ").Replace(":", " ").Replace(";", " ").Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "");
                        objProviderPhysicianMaintenance.PPM_PhysicianName = PhysicianName;
                        
                        List<BHA_Department> objDepartmentList = (List<BHA_Department>)Session["DepartmentList"];
                        BHA_Department objBHA_Department = objDepartmentList.Where(item => item.DepartmentDescription == dt.Rows[k][1].ToString()).SingleOrDefault();
                        if (objBHA_Department != null)
                        {
                            objProviderPhysicianMaintenance.PPM_Department = objBHA_Department.DepartmentCode;
                        }    
                        objProviderPhysicianMaintenance.PPM_IsTrusted = false;
                        objProviderPhysicianMaintenance.PPM_Status = true;
                        objProviderPhysicianMaintenance.PPM_ActionBy = Session["ProviderUserName"].ToString();
                        objProviderPhysicianMaintenance.PPM_ActionDate = DateTime.Now;
                        objTruestedDoctors.ProviderPhysiciansManagements.Add(objProviderPhysicianMaintenance);
                        objTruestedDoctors.SaveChanges();

                    }
                }
                catch (Exception ex)
                {
                    lblMessage.Text = "We are unable to process your request.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
            BindGrid();
            trUpload.Visible = false;
            lblMessage.Text = "Request processed successfully.";
            lblMessage.ForeColor = System.Drawing.Color.Green;
        }
        else
        {
            lblMessage.Text = "We are unable to process your request.Please make sure you have used the defined template for uploaded.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    private System.Data.DataTable ParseBatchFile(string finalExcelFilePath)
    {


        DataTable dtExcelData = new DataTable();

        string sConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + finalExcelFilePath + ";Extended Properties=" + Convert.ToChar(34).ToString() + "Excel 8.0;HDR=Yes" + Convert.ToChar(34).ToString();
        System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection(sConnectionString);

        try
        {
            if (cn.State != ConnectionState.Closed)
            {
                try
                {
                    cn.Close();
                }
                catch (Exception ex)
                {

                }
            }
            cn.Open();


            string strSQL = "SELECT * FROM [Physicians$]";
            DataSet oDataSet = new DataSet();
            OleDbCommand dbCommand = new OleDbCommand(strSQL, cn);
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter(dbCommand);
            DataTable dTable = new DataTable();

            dataAdapter.Fill(dtExcelData);
            //}
        }
        catch (Exception ex)
        {
            CommonClass.LogError("ParseBatchFile", ex.Message, ex.InnerException, ex.StackTrace, Session["ProviderUserName"].ToString());
        }
        finally
        {
            cn.Close();
        }

        return dtExcelData;
    }

}