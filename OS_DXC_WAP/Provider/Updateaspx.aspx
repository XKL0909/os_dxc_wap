﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Inner.master" AutoEventWireup="true" Inherits="Provider_Updateaspx" Codebehind="Updateaspx.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        Update machine Configuration file</p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">

 <style type="text/css">

 p.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	}
table.MsoTableGrid
	{border:solid windowtext 1.0pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	}
a:link
	{color:blue;
	text-decoration:underline;
	text-underline:single;
        }
</style>
    
    <div>
 

       <center> <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;mso-layout-grid-align:none;text-autospace:none">
            <b>
            <span >The new configuration file is not updated in your machine please follow the below steps.
<o:p></o:p>
            </span></b>
        </p></center>
        <p class="MsoNormal">
            <span lang="EN" style="mso-ansi-language:EN"><span style="mso-spacerun:yes">&nbsp;</span><o:p></o:p></span></p>
        <table border="0" cellpadding="0" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none">
            <tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
                <td style="width:27.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="37">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN">1<o:p></o:p></span></p>
                </td>
                <td style="width:450.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="601">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN">Click </span>
                        <a href="../MemberQService.exe"><span lang="EN" 
                            style="mso-ansi-language:EN">here</span></a><span lang="EN" style="mso-ansi-language:
  EN"> to update the old file<o:p></o:p></span></p>
                </td>
                <td style="width:450.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="601" 
                    rowspan="7">
                     <div class="highlightText" id="aa">
                     
                         <h4>if you already did these steps previously, please click <span>
                             <a href="MemberVerification1.aspx">here</a>, 
        otherwise follow these steps:</span></h4></div>
                    </td>
            </tr>
            <tr style="mso-yfti-irow:1">
                <td style="width:27.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="37">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN">2<o:p></o:p></span></p>
                </td>
                <td style="width:450.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="601">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN">Click on run as show in below screen<o:p></o:p></span></p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:2">
                <td style="width:27.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="37">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN"><o:p>&nbsp;</o:p></span></p>
                </td>
                <td style="width:450.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="601">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                      
                        <img border="0" height="280" 
                            src="..\images\update\1.png" 
                             width="415" /><span lang="EN" style="mso-ansi-language:EN"><o:p></o:p></span></p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:3">
                <td style="width:27.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="37">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN">3<o:p></o:p></span></p>
                </td>
                <td style="width:450.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="601">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        Click on run 
                        as show in below screen<o:p></o:p></p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:4">
                <td style="width:27.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="37">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN"><o:p>&nbsp;</o:p></span></p>
                </td>
                <td style="width:450.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="601">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                       
                        <img border="0" height="239" 
                            src="..\images\update\2.png" 
                             width="470" /><o:p></o:p></p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:5">
                <td style="width:27.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="37">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN">4<o:p></o:p></span></p>
                </td>
                <td style="width:450.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="601">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN">Click on Extract as show in below screen<span 
                            style="mso-tab-count:1"> </span></span><o:p></o:p>
                    </p>
                </td>
            </tr>
            <tr style="mso-yfti-irow:6">
                <td style="width:27.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="37">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                        <span lang="EN" style="mso-ansi-language:EN"><o:p>&nbsp;</o:p></span></p>
                </td>
                <td style="width:450.9pt;padding:0in 5.4pt 0in 5.4pt" valign="top" width="601">
                    <p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal">
                     
                        <img border="0" height="354" 
                            src="..\images\update\3.png" 
                             width="442" /><span lang="EN" style="mso-ansi-language:EN"><o:p></o:p></span></p>
                </td>
            </tr>
            </table>
        <p class="MsoNormal">
            <span lang="EN" style="mso-ansi-language:EN"><span style="mso-tab-count:5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span></span><!--[if gte vml 1]><v:shape id="Picture_x0020_1" o:spid="_x0000_i1028"
 type="#_x0000_t75" style='width:349.5pt;height:227.25pt;visibility:visible;
 mso-wrap-style:square'>
 <v:imagedata src="file:///C:\Users\DAHINW~1.BUP\AppData\Local\Temp\msohtmlclip1\01\clip_image005.png"
  o:title="" xmlns:v="urn:schemas-microsoft-com:vml"/>
</v:shape><![endif]--><span lang="EN" style="mso-ansi-language:
EN"><o:p></o:p></span>
        </p>
       
 

       </div>
</asp:Content>

