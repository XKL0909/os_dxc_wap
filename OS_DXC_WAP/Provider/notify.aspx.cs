﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using DevExpress.Web.ASPxGridView;

public partial class Provider_notify : System.Web.UI.Page
{
    public string _InitialRequestID = string.Empty;
    public string _ProviderCode = string.Empty;

    public string UrlValues = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
        {
            Response.Redirect("../default.aspx");
        }
        if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
        {
            CheckConcurrentUserStatus();
        }
        _ProviderCode = Convert.ToString(Session["ProviderID"]);
        if (string.IsNullOrEmpty(Convert.ToString(Session["_InitialRequestID"])))
        {
            _InitialRequestID = GetRefNo(_ProviderCode); // WebPublication.GenerateUniqueID;
            Session["_InitialRequestID"] = _InitialRequestID;
        }
        else
        {
            _InitialRequestID = Convert.ToString(Session["_InitialRequestID"]);
        }

       
        lblProName.Text = Convert.ToString(Session["ProviderName"]);
        lblProCode.Text = _ProviderCode;
    }

    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["ProCurrentLogID"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }

    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }

    protected string GetRefNo(string ProID)
    {
        SqlConnection connection = null;
        string strRefNo = "";
        string strID = "";
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        }
        catch
        {

        }
        // Set up parameters (2 input and 3 output) 
        SqlParameter[] arParms = new SqlParameter[2];
        arParms[0] = new SqlParameter("@ProviderCode", SqlDbType.BigInt);
        arParms[0].Value = ProID;

        // @QtyPerUnit Output Parameter
        arParms[1] = new SqlParameter("@_count", SqlDbType.Int);
        arParms[1].Direction = ParameterDirection.Output;

        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "[BME_ServiceCode_MaxID]", arParms);
        strID = Convert.ToString(arParms[1].Value).Trim();
        if(Convert.ToInt32(strID) < 10)
        {
            strID = "0" + strID;
        }
        strRefNo = ProID + "-" + strID;
        if (connection.State == ConnectionState.Open)
        {
            connection.Close();
        }
        return strRefNo;
    }






    protected void btnNew_Click(object sender, EventArgs e)
    {
        ////Response.Redirect("newservicecode.aspx?refno=" + _InitialRequestID.Trim());
        var querString = Cryption.Encrypt(_InitialRequestID.Trim());
        Response.Redirect("newservicecode.aspx?val=" + querString);
    }
}