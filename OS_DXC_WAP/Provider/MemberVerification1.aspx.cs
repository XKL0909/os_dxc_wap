using Bupa.OSWeb.Business;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using Utility.Configuration;

public partial class MemberVerification1 : System.Web.UI.Page
{
    string strProviderID;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strIsMinor;
    bool blnSwipeEnabled;

    void Page_Load(object sender, EventArgs e)
    {
        SessionManager.CheckSessionTimeoutForMemberVerify();

        if (string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
        {
            Response.Redirect("../default.aspx");
        }
        strProviderID = Convert.ToString(Session["ProviderID"]).ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Session["SwipCardEnabled"])))
        {
            blnSwipeEnabled = Convert.ToBoolean(Session["SwipCardEnabled"]);
        }
        else
        {
            blnSwipeEnabled = false;
        }

        if (blnSwipeEnabled == true)
        {
            rfvCardReader.ControlToValidate = "txtCardReader";
            btnClear.Visible = true;
            txtCardReader.Enabled = true;
            txtCardReader.Focus();
            lblPleaseSwipeCard.Visible = true;
            imgDiv.Visible = true;
            btnShow.Visible = true;
            lblError.Text = "";

        }
        else
        {
            txtCardReader.Enabled = false;
            btnClear.Visible = false;
            txtMembershipNo.Enabled = true;
            txtMembershipNo.Width = System.Web.UI.WebControls.Unit.Pixel(350);
            txtMembershipNo.Focus();
            lblPleaseSwipeCard.Visible = false;
            imgDiv.Visible = false;
            imgRightDiv.Style.Add("margin-left", "0px");
            btnShow.Visible = false;
        }

       

        if (!Page.IsPostBack)
        {
            BannedProvider MyProvider = new BannedProvider(Session["ProviderID"].ToString());
            if (MyProvider.isBanned())
            {
                string script = "$(document).ready(function(){DisableField();});";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "prevent", script, true);
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection connection = null;
            string strMembershopNo;
            //Start Aramco Addition For Batch Number
            if (txtMembershipNo.Text=="" && txtCardReader.Text == "" && !string.IsNullOrEmpty(txtAramcoBadgeNumber.Text))
            {
                string MemeberShipId = GetMembership(txtAramcoBadgeNumber.Text, Convert.ToString(Session["ProviderID"]));
                if (MemeberShipId.IndexOf(':') > 0 && MemeberShipId.Split(':')[0].Trim() == "Error")
                {
                    lblError.Text = MemeberShipId.Split(':')[1];
                    rfvCardReader.Enabled = false;
                    return;
                }
                else { 
                    txtMembershipNo.Text = MemeberShipId;
                    txtCardReader.Text = MemeberShipId;
                    rfvCardReader.Enabled = true;
                    }
            }
            //End Aramco Addition For Batch Number
            if (blnSwipeEnabled == true)
            {
                if (!string.IsNullOrEmpty(txtMembershipNo.Text))
                {
                    strMembershopNo = txtMembershipNo.Text;
                }
                else if (!string.IsNullOrEmpty(txtCardReader.Text))
                {
                    strMembershopNo = txtCardReader.Text;
                }
                else
                {
                    txtCardReader.Text = "";
                    txtMembershipNo.Text = "";
                    //reqFieldValAramcoBadgeNumber.Enabled = false;
                    rfvCardReader.Enabled = false;
                    goto GoExit;
                }
            }
            else
            {
                strMembershopNo = txtMembershipNo.Text;
            }

            Session["MembershipNo"] = strMembershopNo;
            Session["Dept"] = ddlDepartment.SelectedValue;

            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);

            SqlParameter[] arParms = new SqlParameter[2];
            arParms[0] = new SqlParameter("@MembershipNo", SqlDbType.NVarChar, 30);
            arParms[0].Value = strMembershopNo;
            arParms[1] = new SqlParameter("@TransactionNo", SqlDbType.NVarChar, 30);
            arParms[1].Direction = ParameterDirection.Output;
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spBiometricInsert", arParms);

            // Display results in text box using the values of output parameters	
            string result = arParms[1].Value + "";
            long MbrVrfID;
            //we need to the difference of number from Caesar to overcome already exist number

            string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("MemberVerificationCounter");

            MbrVrfID = long.Parse(result) + long.Parse(strConfigSabicContractNo); 
            MemberHelper memberRec = new MemberHelper(strMembershopNo, "");
            Member member = memberRec.GetMemberDetails();
            SqlParameter[] arParmsInsert = new SqlParameter[14];
            arParmsInsert[0] = new SqlParameter("@ProCode", SqlDbType.NChar, 10);
            arParmsInsert[0].Value = Session["ProviderID"];
            arParmsInsert[1] = new SqlParameter("@ProName", SqlDbType.NVarChar, 100);
            arParmsInsert[1].Value = Session["ProviderName"];
            arParmsInsert[2] = new SqlParameter("@MemberShipNo", SqlDbType.NChar, 10);
            arParmsInsert[2].Value = strMembershopNo;
            arParmsInsert[3] = new SqlParameter("@MemberName", SqlDbType.NVarChar, 150);
            arParmsInsert[3].Value = member.MemberName;
            arParmsInsert[4] = new SqlParameter("@MemberType", SqlDbType.NChar, 10);
            arParmsInsert[4].Value = member.VIP;
            arParmsInsert[5] = new SqlParameter("@AddedBy", SqlDbType.NChar, 10);
            arParmsInsert[5].Value = Session["ProviderUserName"];
            arParmsInsert[6] = new SqlParameter("@Department", SqlDbType.NChar, 100);
            arParmsInsert[6].Value = ddlDepartment.SelectedItem.Text;
            arParmsInsert[7] = new SqlParameter("@Mobile", SqlDbType.NChar, 10);
            arParmsInsert[7].Value = member.MOBILE;
            arParmsInsert[8] = new SqlParameter("@verification_id", SqlDbType.BigInt);
            arParmsInsert[8].Value = MbrVrfID;
            arParmsInsert[9] = new SqlParameter("@swip_card", SqlDbType.Bit);
            arParmsInsert[9].Value = blnSwipeEnabled;
            arParmsInsert[10] = new SqlParameter("@ContractNumber", SqlDbType.NChar, 10);
            arParmsInsert[10].Value = member.ContractNumber;
            arParmsInsert[11] = new SqlParameter("@PatientMobileNo", SqlDbType.NChar, 10);
            arParmsInsert[11].Value = txtPatientMobile.Text;

            arParmsInsert[12] = new SqlParameter("@swipe_override", SqlDbType.Bit);
            if (hidoverride.Value=="1")
                arParmsInsert[12].Value = true;
            else
                arParmsInsert[12].Value = false;

            arParmsInsert[13] = new SqlParameter("@company_name", SqlDbType.NVarChar, 100);
            arParmsInsert[13].Value = member.CompanyName;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spBupa_ProviderVerificationInsert", arParmsInsert);
            ////Response.Redirect("TOB.aspx?VID=" + MbrVrfID.ToString() + "&Type=In&MN=" + Session["MembershipNo"].ToString() + "&D=" + Session["Dept"].ToString() + "&Status=A", false);
            var queryString = "VID=" + MbrVrfID.ToString() + "&Type=In&MN=" + Session["MembershipNo"].ToString() + "&D=" + Session["Dept"].ToString() + "&Status=A";
            queryString = Cryption.Encrypt(queryString);
            Response.Redirect("TOB.aspx?val=" + queryString, false);
        GoExit: txtCardReader.Focus();
        }

        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            //reqFieldValAramcoBadgeNumber.Enabled = false;
            rfvCardReader.Enabled = false;
        }
    }

    [System.Web.Services.WebMethod]
    public static string GetMembership(string badgeNumber, string provider)
    {
        string membershipNo = string.Empty;
        try
        {

            OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
            OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN();
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
            request.customerNo = ConfigurationManager.AppSettings["AramcoContractCode"].ToString();
            request.staffNo = badgeNumber;
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqMbrListInfo(request);
            StringBuilder stringBuilder = new StringBuilder(200);
            if (response.errorID != "0")
            {
                stringBuilder.Append(response.errorMessage).Append("\n");
                membershipNo = "Error:" + stringBuilder.ToString();
            }
            else
                membershipNo = Convert.ToString(response.detail[0].membershipNo);


        }
        catch (Exception ex)
        {
            membershipNo = "Error :" + ex.Message;
        }
        return membershipNo;

    }
}