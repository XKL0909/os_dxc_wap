﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Net;

public partial class Member_PreauthDetailsMobileV2 : System.Web.UI.Page
{
    string PreauthID;
    string ProviderCode;
    string EpisodeNo;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    public string _Username = "";
    public string _PAID = "";
    public string _PROID = "";
    public string _View = "display:none";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            SessionManager.CheckSessionTimeout();

            ProviderCode = "20087";//
            
            if (Request.QueryString["ReqType"] == "M" || Request.QueryString["ReqType"] == "C")
            {
                ProviderCode = Request.QueryString["PC"];
                btnFollowUpRequest.Visible = false;
                btnCancelRequest.Visible = false;
                txtRenew.Visible = false;
            }
            else
            {

                ProviderCode = Session["ProviderID"].ToString();  // Request.QueryString["PC"];
            }

            PreauthID = Request.QueryString["PID"];
            string OptionType = Request.QueryString["OptionType"];
            string RType = Request.QueryString["RType"];
            EpisodeNo = Request.QueryString["EN"];
            string Dept =  Request.QueryString["DEPT"];


            _PROID = ProviderCode;
            _PAID = PreauthID;
            _Username = Convert.ToString(Session["ProviderUserName"]);
            if (OptionType == "CNR")
            {
                System.Data.SqlClient.SqlConnection sqlConnection1 = new System.Data.SqlClient.SqlConnection("Data Source=172.26.4.95;Initial Catalog=bme;User ID=sa;Password=sa;");
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "UPDATE PREAUTH_ALERT SET  CHECKED_IND = 'Y' where [PREAUTH_ID] = '" + PreauthID + "' and EPISODE_ORDER  = '" + EpisodeNo + "'";// "update Preauth_alert ( membershipNo,  VerificationID, VerificationStatus,RejectionReason ) values (" + ReturnVal[3] + ", " + ReturnVal[3] + ", '" + ReturnVal[1] + "' , '" + ReturnVal[4] + "' )";

                cmd.Connection = sqlConnection1;


                sqlConnection1.Open();
                cmd.ExecuteNonQuery();
                sqlConnection1.Close();
            }




            if (OptionType == "Cancel")
            {
                EpisodeNo = Request.QueryString["EN"];
                CancelPreAuth(PreauthID, EpisodeNo);
                return;
            }



            if (!Page.IsPostBack)
            {
                DisplayPreAuthHist();
            }


            if (RType == "NR")
            {
                if (Dept == "VAC")
                {
                    btnFollowUpRequest.Enabled = false;
                    txtRenew.Disabled = true;
                }

                //lnkBackOption.Text = "Submit New Request";  
                //lnkBackOption.NavigateUrl = "~/Provider/Preauth15.aspx";

            }
            Response.Write(GetPostBackControl());
        }

        catch (Exception ex)
        {
           // ExceptionManager.LogException(ex);
        }


    }
    protected void OnSaveClick(object sender, EventArgs e)
    {

        string parameter = Request["__EVENTARGUMENT"];

    } 


  



    private void DisplayPreAuthHist()
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN request;
        OS_DXC_WAP.CaesarWS.RequestPreauthByIDResponse_DN response;

        DataTable tablePA = new DataTable();
        tablePA.Columns.Add("ItemID", typeof(string));
        tablePA.Columns.Add("ProviderID", typeof(string));
        tablePA.Columns.Add("PAID", typeof(string));
        tablePA.Columns.Add("ServicesCode", typeof(string));
        tablePA.Columns.Add("Services Description", typeof(string));
        tablePA.Columns.Add("Dispensed", typeof(bool));

        DataColumn[] keyColumns = new DataColumn[1];
        keyColumns[0] = tablePA.Columns["ItemID"];
        tablePA.PrimaryKey = keyColumns;

        string _Dispends = "";
        string username = "";
        string _date = "";
        DateTime _dt;
        bool _addcheck = true;
        string _scode = "";
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.RequestPreauthByIDRequest_DN();
            if (EpisodeNo != null && EpisodeNo != "")
                request.episodeNo = long.Parse(EpisodeNo);
            else
                request.episodeNo = 0;
            request.preAuthorisationID = long.Parse(PreauthID);
            request.providerCode = ProviderCode;
            request.transactionID = TransactionManager.TransactionID(); // long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            //ExceptionManager.TrackWSPerformance("PreauthDetails", "RequestPreauthByID", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), ProviderCode, "", "");
            response = ws.RequestPreauthByID(request);// ws.RequestPreauthByID(request);
            //ExceptionManager.TrackWSPerformance("PreauthDetails", "RequestPreauthByID", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), ProviderCode, "", "");

            if (response.status.Trim() == "0")
            {

                if (response.preauthorisationStatus != null && response.preauthorisationStatus.Trim() == "WIP")
                {
                    UpdatePanel2.Visible = false;
                    btnCancelRequest.Visible = false;
                    btnRenewRequest.Visible = false;
                    btnFollowUpRequest.Visible = false;
                    txtRenew.Visible = false;
                    Label1.Visible = false;
                    if (Request.QueryString["RType"] != null && Request.QueryString["RType"].ToString() == "NR")
                        Message1.Text = "Reference No: <b>" + response.preAuthorizationID.ToString() + "</b>  <a href='CheckNewReply.aspx'>Check Reply</a>";
                    else
                        Message1.Text = "This preauthorisation is currently being processed by Bupa. Please check later.";// +response.preAuthorizationID.ToString() + "</b>  <a href='CheckNewReply.aspx'>Check Reply</a>";



                }
                else
                {
                    lblClass.Text = response.memberClass;
                    lbAge.Text = response.age.ToString();
                    lblDateOfVisit.Text = String.Format("{0:d}", response.dateOfAdmission);
                    lblDept.Text = response.department;
                    lblExpiryDate.Text = String.Format("{0:d}", response.expiryDate); //response.expiryDate.ToString();
                    lblIDCard.Text = response.IDCardNo;
                    lblInsurance_Co.Text = response.insurance_Company;
                    lblInsuredName.Text = response.memberName;
                    lblPatientFileNo.Text = response.patientFileNo;
                    lblDateTime.Text = response.date_Time.ToString();
                    lblPolicyHolder.Text = response.policyHolder;
                    lblPolicyNo.Text = response.membershipNo;
                    //need to check///lblPolicyNo.Text = response.
                    lblProviderFaxNo.Text = response.providerFaxNo;
                    lblProviderName.Text = response.provider_Name;
                    lblDateTime.Text = response.date_Time.ToString();
                    lblSex.Text = response.gender;
                    lblAdditionalComments.Text = response.add_Comments;
                    ////// need to check       lblApprovalNo.Text = response.
                    lblApprovalValidity.Text = String.Format("{0:d}", response.app_Validity); //response.app_Validity.ToString();
                    lblInsuranceOfficer.Text = response.insurance_Officer;
                    lblApprovalNo.Text = PreauthID;

                    if (response.chronic_Ind.Trim() != "N" || response.department == "DEN" || response.department == "OER")
                        btnFollowUpRequest.Visible = false;

                    if (response.department == "VAC")
                    {
                        btnFollowUpRequest.Visible = false;
                        btnCancelRequest.Visible = false;
                    }

                    lblComments.Text = response.comments;

                    lblPreauthorisationStatus.Text = response.preauthorisationStatus;
                    lblRoomType.Text = response.roomType;

                    StringBuilder sbResponse = new StringBuilder(2200);
                    if (response.chronic_Ind.Trim() == "Y" || response.supply_Period.Length > 0)
                        sbResponse.Append("<table   style='font-size:13px;width:100% ;' border=0><tr ><td width='15px'><b>Service Code<br>رمز الخدمة</td><td width='20px'><b>Supply Period<br>مدة الخدمة</td><td width='25px'><b>Supply From<br>من تاريخ</td><td width='25px'><b>Supply To<br>الى تاريخ</td><td width='20%' valign=top><b>Service Description<br>وصف الخدمة</td><td width='35%' valign=top><b>Notes<br>ملاحظات</b></td></tr>");
                    else
                        sbResponse.Append("<table  style='font-size:13px;width:100% ;' border=0 ><tr><td width='15px'><b>Service Code</td><td valign=top><b>Service Description</td><td valign=top><b>Notes</b></td></tr>");

                    for (int i = 0; i < response.service_Code.Length; i++)
                    {
                        sbResponse.Append("<tr>");
                        if (response.chronic_Ind.Trim() == "Y" || response.supply_Period.Length > 0)
                        {
                            sbResponse.Append("<td>");
                            if (response.service_Code[i] != null) sbResponse.Append(response.service_Code[i]);
                            sbResponse.Append("</td><td>");
                            if (response.supply_Period[i] != null) sbResponse.Append(response.supply_Period[i] + " Month/s");
                            sbResponse.Append("</td><td>");
                            if (response.supply_From[i] != null) sbResponse.Append(DateTime.ParseExact(String.Format("{0:d}", response.supply_From[i].Replace(" 00:00:00.0", "")), "yyyy-MM-dd", null).ToShortDateString());
                            sbResponse.Append("</td><td>");
                            if (response.supply_To[i] != null) sbResponse.Append(DateTime.ParseExact(String.Format("{0:d}", response.supply_To[i].Replace(" 00:00:00.0", "")), "yyyy-MM-dd", null).ToShortDateString());
                            sbResponse.Append("</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td>");
                            if (!response.notes[i].ToString().Contains("Rejected"))
                            {
                                if (ProviderCode == "22671")
                                {
                                    _View = "display:block";
                                    _Dispends = SelectRecord(i, ProviderCode, PreauthID, response.service_Code[i]);
                                }
                                else
                                {
                                    _View = "display:none";
                                    _Dispends = "";
                                }
                                
                                username = SelectRecordUser(i,ProviderCode, PreauthID, response.service_Code[i]);
                                _date = SelectRecordDate(i,ProviderCode, PreauthID, response.service_Code[i]);
                                _date = String.Format("{0:d/M/yyyy HH:mm:ss}", _date);
                                if (_Dispends == "")
                                {
                                    _Dispends = "false";
                                    if (string.IsNullOrEmpty(Convert.ToString(response.service_Code[i])))
                                    {
                                        InsertRecord(i, ProviderCode, PreauthID, i.ToString(), response.service_Desc[i]);
                                    }
                                    else
                                    {
                                        InsertRecord(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i]);
                                    }



                                   
                                    if (string.IsNullOrEmpty(response.service_Code[i]))
                                    {
                                        tablePA.Rows.Add(i, ProviderCode, PreauthID, "",  response.service_Desc[i], false);
                                        _addcheck = true;
                                    }
                                    else
                                    {
                                        _addcheck = true;
                                        tablePA.Rows.Add(i, ProviderCode, PreauthID, response.service_Code[i],  response.service_Desc[i], false);
                                    }
                                    
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(Convert.ToString(response.service_Code[i])))
                                    {
                                        _scode = "";
                                    }
                                    else
                                    {
                                        _scode = response.service_Code[i];
                                    }
                                    tablePA.Rows.Add(i, ProviderCode, PreauthID, _scode, response.service_Desc[i],  Convert.ToBoolean(_Dispends));
                                }
                                if (ProviderCode != "22671")
                                {
                                    _addcheck = false;
                                }
                                if (_addcheck)
                                {
                                    if (Convert.ToBoolean(_Dispends))
                                    {
                                        sbResponse.Append("<td><input id='" + response.service_Code[i] + "' type='checkbox' name='" + response.service_Code[i] + "' disabled='disabled' checked='checked' /></td><td>" + username + "</td><td>" + _date + "</td></tr>");
                                    }
                                    else
                                    {
                                        sbResponse.Append("<td><input id='" + response.service_Code[i] + "' type='checkbox' name='" + response.service_Code[i] + "' disabled='disabled' /></td><td>" + username + "</td><td>" + _date + "</td></tr>");
                                    }
                                }

                            }
                            else
                            {
                                sbResponse.Append("</tr>");
                            }
                        }

                        else
                        {
                            sbResponse.Append("<td>" + response.service_Code[i] + "</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td>");  // response.service_Code
                            if (!response.notes[i].ToString().Contains("Rejected"))
                            {
                                if (ProviderCode == "22671")
                                {
                                    _View = "display:block";
                                    _Dispends = SelectRecord(i, ProviderCode, PreauthID, response.service_Code[i]);
                                }
                                else
                                {
                                    _View = "display:none";
                                    _Dispends = "";
                                }
                               
                                username = SelectRecordUser(i,ProviderCode, PreauthID, response.service_Code[i]);
                                _date = SelectRecordDate(i,ProviderCode, PreauthID, response.service_Code[i]);
                                _date = String.Format("{0:d/M/yyyy HH:mm:ss}", _date);
                                if (_Dispends == "")
                                {
                                    _Dispends = "false";

                                    
                                    if (string.IsNullOrEmpty(response.service_Code[i]))
                                    {
                                        //tablePA.Rows.Add(ProviderCode, PreauthID, "", false);
                                        InsertRecord(i, ProviderCode, PreauthID, i.ToString(), response.service_Desc[i]);
                                        _addcheck = false;
                                    }
                                    else
                                    {
                                        _addcheck = true;
                                        InsertRecord(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i]);
                                        tablePA.Rows.Add(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i], false);
                                    }
                                    
                                }
                                else
                                {
                                     if (string.IsNullOrEmpty(response.service_Code[i]))
                                    {
                                        //tablePA.Rows.Add(ProviderCode, PreauthID, "", false);
                                    }
                                    else
                                    {
                                        tablePA.Rows.Add(i, ProviderCode, PreauthID, response.service_Code[i], response.service_Desc[i], Convert.ToBoolean(_Dispends));
                                    }
                                    
                                }

                                if (_addcheck)
                                {
                                    if (Convert.ToBoolean(_Dispends))
                                    {
                                        sbResponse.Append("<td><input id='" + response.service_Code[i] + "' type='checkbox' name='" + response.service_Code[i] + "' disabled='disabled' checked='checked' /></td><td>" + username + "</td><td>" + _date + "</td></tr>");
                                    }
                                    else
                                    {
                                        sbResponse.Append("<td><input id='" + response.service_Code[i] + "' type='checkbox' name='" + response.service_Code[i] + "' disabled='disabled' /></td><td>" + username + "</td><td>" + _date + "</td></tr>");
                                    }
                                }

                            }
                            else
                            {
                                sbResponse.Append("</tr>");
                            }
                        }
                    }
                    ServiceListReport.Visible = true;
                    ServiceListReport.InnerHtml = sbResponse.ToString() + "</table>";
                    DataSet ds = new DataSet();
                    ds.Tables.Add(tablePA);
                    Session["tablePA"] = ds;
                }
            }
            else
            {
                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                txtRenew.Visible = false;
                Message1.Text = response.errorMessage;// "Error Encounter. Please contact the administrator for further information.";// response.errorMessage[0].ToString();
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
            //ExceptionManager.LogException(ex);
        }


    }


    private string GetPostBackControl()
    {
        string Outupt = "";

        //get the __EVENTTARGET of the Control that cased a PostBack(except Buttons and ImageButtons)
        string targetCtrl = Page.Request.Params.Get("__EVENTTARGET");
        if (targetCtrl != null && targetCtrl != string.Empty)
        {
            Outupt = targetCtrl + " button make Postback";
        }
        else
        {
            //if button is cased a postback
            foreach (string str in Request.Form)
            {
                Control Ctrl = Page.FindControl(str);
                if (Ctrl is Button)
                {
                    Outupt = Ctrl.ID;
                    break;
                }
            }
        }
        return Outupt;
    }

    protected void Check(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;
        Response.Write(chk.Checked.ToString());
    }

    private void CancelPreAuth(string Preauth, string Episode)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
           OS_DXC_WAP.CaesarWS.SendPreauthCancellationRequest_DN request;
        OS_DXC_WAP.CaesarWS.SendPreauthCancellationResponse_DN response;

        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.SendPreauthCancellationRequest_DN();
            
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.preAuthorisationID = Int64.Parse(Preauth.Trim());
            request.episodeNo = Int64.Parse(Episode.Trim());
                      
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

          //  ExceptionManager.TrackWSPerformance("PreauthDetails", "CancelInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), ProviderCode, "", "");
            response = ws.CancelInfo(request);
            ///ExceptionManager.TrackWSPerformance("PreauthDetails", "CancelInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), ProviderCode, "", "");

            StringBuilder sbResponse = new StringBuilder(200);
            StringBuilder sb1 = new StringBuilder(200);

            if (response.status == "0")
            {
                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                txtRenew.Visible = false;

                Message1.Text = "Your request for Cancellation will be processed.";// response.errorMessage.Trim();
                
            }
            else
            {
                
                Message1.Text = response.errorMessage;// sb1.ToString(); // "Invalid information reported. Please type in the exact information.";
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
           // ExceptionManager.LogException(ex);
        }

    }

    protected void lnkCancelRequest_Click(object sender, EventArgs e)
    {

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.RequestCancellationStatusRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestCancellationStatusRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestCancellationStatusResponse_DN response;

        request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        request.preAuthorisationID = Int64.Parse( PreauthID.Trim());
        request.providerCode = ProviderCode;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;



        

        try
        {

            StringBuilder sb = new StringBuilder(200);

           // ExceptionManager.TrackWSPerformance("PreauthDetails", "RequestCancelStatus", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), ProviderCode, "", "");
            response = ws.RequestCancelStatus(request);
            //ExceptionManager.TrackWSPerformance("PreauthDetails", "RequestCancelStatus", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), ProviderCode, "", "");


            if (response.status.Trim() != "0")
            {
                    UpdatePanel2.Visible = false;
                    btnCancelRequest.Visible = false;
                    btnRenewRequest.Visible = false;
                   btnFollowUpRequest.Visible = false;
                    txtRenew.Visible = false;
                    Message1.Text = response.errorMessage.Trim();
            }
            else
            {

                StringBuilder sbResponse = new StringBuilder(2200);

                sbResponse.Append("<table border=1 width=900px><tr><td width=20%><b>Preauth ID</td><td><b>Episode Number</td><td><b>Episode Status</b></td><td><b>Service Description</b></td><td><b>Service Status</b></td><td><b>Approval Date</b></td><td><b>Member Name</b></td><td><b>Patient File No</b></td></tr>");

                for (int i = 0; i < response.episodeNo.Length; i++)
                {
                    ////sbResponse.Append("<a href='PreauthDetails.aspx?PID=" + response.preauthorisationID[i].ToString() + "&EN=" + response.episodeNo[i].ToString() + "&OptionType=Cancel'  style='cursor:hand;'><tr><td>" + response.preauthorisationID[i].ToString() + "</td><td>" + response.episodeNo[i].ToString() + "</td><td>" + response.episodeStatus[i] + "</td><td>" + response.serviceDescription[i] + "</td><td>" + response.lineItemStatus[i] + "</td><td>" +  String.Format("{0:d}", response.approvalDate[i]) + "</td><td>" + response.memberName[i] + "</td><td>" + response.patientFileNumber[i] + "</td>");  // response.service_Code

                    var queryString = "PID=" + response.preauthorisationID[i].ToString() + "&EN=" + response.episodeNo[i].ToString() + "&OptionType=Cancel";
                    queryString = Cryption.Encrypt(queryString);
                    sbResponse.Append("<a href='PreauthDetails.aspx?val=" + queryString + "' style='cursor:hand;'><tr><td>" + response.preauthorisationID[i].ToString() + "</td><td>" + response.episodeNo[i].ToString() + "</td><td>" + response.episodeStatus[i] + "</td><td>" + response.serviceDescription[i] + "</td><td>" + response.lineItemStatus[i] + "</td><td>" + String.Format("{0:d}", response.approvalDate[i]) + "</td><td>" + response.memberName[i] + "</td><td>" + response.patientFileNumber[i] + "</td>");  

                }
                CancelList.Visible = true;
                CancelList.InnerHtml = sbResponse.ToString();



                UpdatePanel2.Visible = false;
                btnCancelRequest.Visible = false;
                btnRenewRequest.Visible = false;
                btnFollowUpRequest.Visible = false;
                    txtRenew.Visible = false;
            }
        }
         catch (Exception ex)
        {
             Message1.Text= ex.Message;
            //ExceptionManager.LogException(ex);
        }


    }
 
    protected void btnRenewRequest_Click(object sender, EventArgs e)
    {

        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.SendRenewInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.SendRenewInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.SendRenewInfoResponse_DN response;


        request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        //request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
        request.transactionID = TransactionManager.TransactionID();// 

        request.preAuthorisationID = Int64.Parse(PreauthID.Trim());
        request.treatmentDate = txtRenewalDate.CalendarDate; // DateTime.Parse("12/12/2009"); // TreatmentDate;
        

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        try
        {

            StringBuilder sb = new StringBuilder(200);

            //ExceptionManager.TrackWSPerformance("PreauthDetails", "SendRenewInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Send", request.transactionID.ToString(), ProviderCode, "", "");
            response = ws.SendRenewInfo(request);
            //ExceptionManager.TrackWSPerformance("PreauthDetails", "SendRenewInfo", DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString(), "Recv", request.transactionID.ToString(), ProviderCode, "", "");

            if (response.status != "0")
            {
                    UpdatePanel2.Visible = false;
                    btnCancelRequest.Visible = false;
                    btnFollowUpRequest.Visible = false;
                    btnRenewRequest.Visible = false;
                    txtRenew.Visible = false;
                    Message1.Text = response.errorMessage;
            }
            else
            {
                Message1.Text = "Request Successfully send for Renewal";
                lblApprovalNo.Font.Bold = true;
                lbl_approvalNo.Text = "New Approval No:";
                lblDateOfVisit.Text = String.Format("{0:d}", response.dateOfAdmission);
                lblApprovalValidity.Text = String.Format("{0:d}", response.app_Validity);
                lblDateTime.Text = response.date_Time.ToString();
                lblPreauthorisationStatus.Text = response.preauthorisationStatus.Trim();

                StringBuilder sbResponse = new StringBuilder(2200);
                sbResponse.Append("<table width=900px><tr><td width=20%><b>Service Code</td><td><b>Service Description</td><td width='12%'><b>Notes</b></td></tr>");


                    for (int i = 0; i < response.service_Code.Length; i++)
                    {
                        sbResponse.Append("<tr>");
                         sbResponse.Append("<td>" + response.service_Code[i] + "</td><td>" + response.service_Desc[i] + "</td><td>" + response.notes[i] + "</td>");  // response.service_Code
                

                    }
                    ServiceListReport.Visible = true;
                    ServiceListReport.InnerHtml = sbResponse.ToString();

                lblApprovalNo.Text = response.preAuthorizationID.ToString();// request.preAuthorisationID.ToString();
                
            }
        }
          catch (Exception ex)
        {
            Message1.Text = ex.Message + ".<br>Please try again later";
            //ExceptionManager.LogException(ex);
        }

    }
   
    protected void btnFollowUpRequest_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("Preauth15.aspx?PID=" + PreauthID, false);// response.preAuthorizationID + "&PC=" + request.providerCode + "&EN=1");
        }
        catch (Exception ex)
        {
            //ExceptionManager.LogException(ex);
        }

    }

    protected void txtRenew_ServerClick(object sender, EventArgs e)
    {

    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        foreach (Control control in Controls) 
        { 
            CheckBox chkBox = control as CheckBox; 
            if (chkBox != null) 
            { 
                chkBox.Checked = true;
            }
        }
    }




    public void InsertRecord(int itemID, string _ProviderID, string _PAID, string _serviceCode, string Desc)
    {
       
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        }
        catch { }
        SqlParameter[] arParms = new SqlParameter[5];



        arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
        arParms[0].Value = itemID;

        arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
        arParms[1].Value = _ProviderID;

        arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
        arParms[2].Value = _PAID;

        arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
        arParms[3].Value = _serviceCode;

        arParms[4] = new SqlParameter("@Description", SqlDbType.NText);
        arParms[4].Value = Desc;


        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Insert_Dispensed", arParms);
    }

    public string SelectRecord(int itemID, string _ProviderID, string _PAID, string _serviceCode)
    {
        string _Value = "";
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        }
        catch { }
        SqlParameter[] arParms = new SqlParameter[5];

        arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
        arParms[0].Value = itemID;


            arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
            arParms[1].Value = _ProviderID;

            arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
            arParms[2].Value = _PAID;

            arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
            arParms[3].Value = _serviceCode;

            arParms[4] = new SqlParameter("@OutPut", SqlDbType.Bit);
            arParms[4].Direction = ParameterDirection.Output;
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Select_Dispensed", arParms);
            if (string.IsNullOrEmpty(arParms[4].Value.ToString()))
            {
                _Value = "";
            }
            else
            {
                _Value = arParms[4].Value.ToString();
            }
            return _Value;
    }

    public string SelectRecordUser(int itemID, string _ProviderID, string _PAID, string _serviceCode)
    {
        string _Value = "";
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        }
        catch { }
        SqlParameter[] arParms = new SqlParameter[5];

        arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
        arParms[0].Value = itemID;


        arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
        arParms[1].Value = _ProviderID;

        arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
        arParms[2].Value = _PAID;

        arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
        arParms[3].Value = _serviceCode;

        arParms[4] = new SqlParameter("@OutPut", SqlDbType.NVarChar,50);
        arParms[4].Direction = ParameterDirection.Output;
        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Select_Dispensed_user", arParms);
        if (string.IsNullOrEmpty(arParms[4].Value.ToString()))
        {
            _Value = "";
        }
        else
        {
            _Value = arParms[4].Value.ToString();
        }
        return _Value;
    }



    public string SelectRecordDate(int itemID , string _ProviderID, string _PAID, string _serviceCode)
    {
        string _Value = "";
        SqlConnection connection = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
        }
        catch { }
        SqlParameter[] arParms = new SqlParameter[5];

        arParms[0] = new SqlParameter("@itemID", SqlDbType.Int);
        arParms[0].Value = itemID;


        arParms[1] = new SqlParameter("@ProviderID", SqlDbType.NChar, 10);
        arParms[1].Value = _ProviderID;

        arParms[2] = new SqlParameter("@PAID", SqlDbType.NChar, 10);
        arParms[2].Value = _PAID;

        arParms[3] = new SqlParameter("@ServiceCode", SqlDbType.NChar, 50);
        arParms[3].Value = _serviceCode;

        arParms[4] = new SqlParameter("@OutPut", SqlDbType.NVarChar, 20);
        arParms[4].Direction = ParameterDirection.Output;
        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "Select_Dispensed_date", arParms);
        if (string.IsNullOrEmpty(arParms[4].Value.ToString()))
        {
            _Value = "";
        }
        else
        {
            _Value = arParms[4].Value.ToString();
        }
        return _Value;
    }



    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }

    protected void Unnamed1_Click(object sender, ImageClickEventArgs e)
    {
        string HTMLfile = Request.Url.ToString();

        WebRequest myRequest = WebRequest.Create(HTMLfile);


        WebResponse myResponse = myRequest.GetResponse();


        Stream ReceiveStream = myResponse.GetResponseStream();
        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");


        StreamReader readStream = new StreamReader(ReceiveStream, encode);


        Char[] read = new Char[256];
        int count = readStream.Read(read, 0, 256);

        using (StreamWriter sw = new StreamWriter(Server.MapPath("~") + "\\Mobile\\PreAuth" + Request["PID"] + ".htm"))
        {
            while (count > 0)
            {

                String str = new String(read, 0, count);
                sw.Write(str);
                count = readStream.Read(read, 0, 256);
            }
        }


        myResponse.Close();


        Response.Redirect("faxmobile.aspx?action=pre&PID=" + Request["PID"]);
    }
    protected void imagebutton2_Click(object sender, ImageClickEventArgs e)
    {
        string HTMLfile = Request.Url.ToString();

        WebRequest myRequest = WebRequest.Create(HTMLfile);


        WebResponse myResponse = myRequest.GetResponse();


        Stream ReceiveStream = myResponse.GetResponseStream();
        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");


        StreamReader readStream = new StreamReader(ReceiveStream, encode);


        Char[] read = new Char[256];
        int count = readStream.Read(read, 0, 256);

        using (StreamWriter sw = new StreamWriter(Server.MapPath("~") + "\\Mobile\\PreAuth" + Request["PID"] + ".htm"))
        {
            while (count > 0)
            {

                String str = new String(read, 0, count);
                sw.Write(str);
                count = readStream.Read(read, 0, 256);
            }
        }


        myResponse.Close();





        Response.Redirect("emailMobile.aspx?action=pre&PID=" + Request["PID"]);
    }
}
