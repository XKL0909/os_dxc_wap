﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserActivation : System.Web.UI.Page
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    private Hashtable hasQueryValue;
    private string provider;
    private int referenceNumber, otp;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        lblOTPMessge.Text = string.Empty;
        if (!Page.IsPostBack)
        {
            Session["CaptchaImageText"] = BLCommon.GenerateRandomCode();
            CaptchaText.InnerHtml = Session["CaptchaImageText"].ToString();

            divActivation.Style.Add("display", "none");
            Session["UserInfo"] = null;
            lblMessage.Text = string.Empty;
            hasQueryValue = new Hashtable();
            string queryStringValues = string.Empty;
            if (Request.QueryString.Count > 0)
            {
                try
                {
                    var val = Request.QueryString["val"];
                    queryStringValues = Cryption.Decrypt(val);
                    hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                    if (hasQueryValue != null && hasQueryValue.Count > 0)
                    {
                        //Client
                        referenceNumber = hasQueryValue.ContainsKey("referenceNumber") ? Convert.ToInt32(hasQueryValue["referenceNumber"]) : 0;
                        //Provider
                        provider = hasQueryValue.ContainsKey("provider") ? Convert.ToString(hasQueryValue["provider"]) : string.Empty;
                        otp = hasQueryValue.ContainsKey("otp") ? Convert.ToInt32(hasQueryValue["otp"]) : 0;
                        LoginInTypeForUser.Value = provider;
                        Session["UserType"] = provider;
                        Session["UserOTP"] = otp;
                        if (referenceNumber > 0 && otp > 0 && !string.IsNullOrEmpty(provider))
                        {
                            UserDetails objUserDetails = new UserDetails();
                            UserDetails userDetails = new UserDetails();
                            userDetails.ReferenceNumber = referenceNumber;
                            userDetails.Status = provider;
                            Session["UserInfo"] = userDetails;
                            var result = objUserDetails.GetUserDetailByReferenceNumber(userDetails);
                            if (result != null)
                            {
                                Session["UserMobileNumber"] = result.Mobile;
                                Session["UserName"] = result.Name;
                                lblUserName.Text = result.UserLoginID;
                                //txtOtp.Text = Convert.ToString(otp);
                            }
                            else
                            {
                                lblOTPMessge.Text = "Invalid Url or Url is Expired";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("Oops.aspx?aspxerrorpath=UserActivation");
                    //lblMessage.Text = ex.Message + ex.InnerException;
                   // return;
                }

            }
        }

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;

        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;
            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                responseCookie.Secure = true;
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += Page_PreLoad;
    }
    protected void Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti - XSRF token failed.");
            }
        }
    }

    protected void btnVerfication_Click(object sender, EventArgs e)
    {
        if (Session["UserInfo"] != null)
        {
           
            if (txtimgcode.Text == Session["CaptchaImageText"].ToString())
            {
                UserDetails userDetails = (UserDetails)Session["UserInfo"];
                if (userDetails != null)
                {
                    int otp = 0;
                    if (int.TryParse(txtOtp.Text, out otp))
                    {
                        userDetails.OTP = otp;
                    }
                    else
                    {
                        lblOTPMessge.Text = "Invalid OTP !";
                    }
                }
                var result = new UserDetails().VerifyOTP(userDetails);
                if (result != null)
                {

                    divActivation.Style.Add("display", "block");
                    divVerification.Style.Add("display", "none");
                    HeaderText.InnerText = "User Activation";
                }
                else
                {
                    lblOTPMessge.Text = "Invalid OTP";
                }
            }
            else
            {
                Session["CaptchaImageText"] = BLCommon.GenerateRandomCode();
                CaptchaText.InnerText = Session["CaptchaImageText"].ToString();
                lblOTPMessge.Text = "Invalid Captcha. Please enter valid Captcha text.";

            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Session["UserInfo"] != null)
        {
            UserDetails userDetails = (UserDetails)Session["UserInfo"];
            if (userDetails != null)
            {
                userDetails.Password = txtNewPassword.Text;
                var result = new UserDetails().UpdateProviderOrGSPWD(userDetails);
                if (result == 1)
                {
                    Session["UserInfo"] = null;
                    popUpSuccess.Visible = true;
                    popUpSuccess.Style.Add("display", "block");
                }
                else
                {
                    lblPwdMessge.Text = "Error while updating the password, please try again";
                    txtNewPassword.Text = "";
                    txtNewPassword.Text = "";
                }
            }
        }
    }

    protected void btnResentOTP_Click(object sender, EventArgs e)
    {
        lblOTPMessge.Text = "";
        txtOtp.Text = "";
        try
        {
            String OTP = "";

            //int pinCode = GeneralHelper.GenetareVerificationPin(1001, 9999);
            //while (pinCode < 0)
            //{
            //    pinCode = GeneralHelper.GenetareVerificationPin(1001, 9999);
            //}
            //OTP = pinCode.ToString();
            //Session["UserOTP"] = OTP;

            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["UserMobileNumber"]).Trim()))
            {
                string smsMessage = "Dear " + Convert.ToString(Session["UserName"]) + ", please use the following OTP " + Convert.ToString(Session["UserOTP"]) + " to activate the user." +
                            " Customer Services, Bupa Arabia";
                if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["UserType"])))
                {
                    if (Session["UserType"].ToString().ToUpper() == "G")
                    {
                        BLCommon.InsertSMS(Convert.ToString(Session["UserMobileNumber"]), smsMessage, ConfigurationManager.AppSettings["OSClientSMSSource"]);
                        lblOTPMessge.Text = "OTP has been sent";
                    }
                    if (Session["UserType"].ToString().ToUpper() == "P")
                    {
                        BLCommon.InsertSMS(Convert.ToString(Session["UserMobileNumber"]), smsMessage, ConfigurationManager.AppSettings["OSProviderSMSSource"]);
                        lblOTPMessge.Text = "OTP has been sent";
                    }
                }
            }
        }
        catch(Exception ex)
        {
            Response.Redirect("Oops.aspx");
        }
    }

    protected void CaptchaValidate_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Session["CaptchaImageText"] = BLCommon.GenerateRandomCode();
            CaptchaText.InnerText = Session["CaptchaImageText"].ToString();
            if (txtimgcode.Text == "" || txtimgcode.Text != "")
            {
                txtimgcode.Text = "";
                //divCaptchaImage.Visible = true;
                return;
            }
        }
        catch (Exception ex) { }
    }
}
