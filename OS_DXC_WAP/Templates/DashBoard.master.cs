﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Bupa.OSWeb.Business;
using System.Xml;
using System.Xml.Serialization;
using Utility.Configuration;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.Configuration;

public partial class Templates_DashBoard : System.Web.UI.MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
            
            CheckSessionExpired();         
             if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
             {
                CheckConcurrentUserStatus();
             }
             if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
             {
                 CheckConcurrentUserStatus();
             }
        }
        catch
        {

        }
        //Aramco PID Changes By Hussamuddin
        string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            MasterIDCTollFreeNumber.Visible = false;
            MasterContactUs.Visible = false;
            AramcoMasterContactUs.Visible = true;
            if(string.IsNullOrEmpty(Convert.ToString(Session["AramcoImagePath"]))){
                    DataSet ds = new DataSet();
                    ds = GetAramcoImageFromDb();
                    if (ds.Tables.Count > 0) { 
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (Convert.ToString(dr["ContractNos"]).Contains(Convert.ToString(Session["ClientID"])))
                        {
                            HtmlImage AramcoSrc = (HtmlImage)AramcoImg.FindControl("AltAramcoImg");
                            AramcoSrc.Src = Convert.ToString(dr["ImagePath"]);
                            Session["AramcoImagePath"] = Convert.ToString(dr["ImagePath"]);
                            AramcoImg.Visible = true;
                        }
                        else
                        {
                            AramcoImg.Visible = false;
                        }
                    }
                    }
                    else
                    {
                        AramcoImg.Visible = false;
                    }
               }
            else
            {
                HtmlImage AramcoSrc = (HtmlImage)AramcoImg.FindControl("AltAramcoImg");
                AramcoSrc.Src = Convert.ToString(Session["AramcoImagePath"]);
                AramcoImg.Visible = true;
            }

        }
        else
        {
            AramcoMasterContactUs.Visible = false;
            AramcoImg.Visible = false;
            //Aramco PID Changes By Hussamuddin   
            if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/client/"))
            {
                phClient.Visible = true;
                PHMember.Visible = false;
                PHProvider.Visible = false;
                MasterIDCTollFreeNumber.Visible = true;
            }
            else if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/member/"))
            {
                phClient.Visible = false;
                PHMember.Visible = true;
                PHProvider.Visible = false;
                MasterIDCTollFreeNumber.Visible = false;
            }
            else if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/provider/"))
            {
                phClient.Visible = false;
                PHMember.Visible = false;
                PHProvider.Visible = true;
                MasterIDCTollFreeNumber.Visible = false;
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
            {
                phClient.Visible = false;
                PHMember.Visible = false;
                PHProvider.Visible = false;
                MasterContactUs.Visible = false;
                MasterIDCTollFreeNumber.Visible = false;

                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["BrokerName"]) + " (" + Convert.ToString(Session["BrokerUserName"]) + ")";
            }
            if (IsSabic())
            {
                ScriptManager.RegisterStartupScript(this, typeof(string), "IsSabic", "IsSabic();", true);
            }
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["MemberName"]) + " (" + Convert.ToString(Session["MembershipNo"]) + ")";

            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {

                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ClientName"]) + " (" + Convert.ToString(Session["ClientUsername"]) + " - " + Convert.ToString(Session["BIName"]) + " )";

            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ProviderName"]) + " (" + Convert.ToString(Session["ProviderUserName"]) + ")";

            }

        }




    }

    protected void Page_Init(object sender, EventArgs e)
    {
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;

        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;
            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                responseCookie.Secure = true;
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;

    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti - XSRF token failed.");
            }
        }
    }

    public DataSet GetAramcoImageFromDb()
    {
        DataSet ds = new DataSet();
        try
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            SqlCommand cmd = new SqlCommand("Select * from Branding", conn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

        }
        catch (Exception ex)
        {

            throw;
        }

        return ds;
    }
    public bool IsSabic()
    {
        bool bFlag = false;
        try
        {
            if (Session["MembershipNo"] != null)
            {
                // Fetch the member's details to check whether SABIC or not. and if it is sabic to hide cheque option
                string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SabicContractNo");
                string strSecondSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SecondSabicContractNo");
                string strSabicTollFreeNo = CoreConfiguration.Instance.GetConfigValue("SabicTollFree");

                MemberHelper mh = new MemberHelper(Convert.ToString(Session["MembershipNo"]), "");
                Member m = mh.GetMemberDetails();


                if (strConfigSabicContractNo == m.ContractNumber || strSecondSabicContractNo == m.ContractNumber)
                {
                    bFlag = true;
                }
            }
          
        }
        catch(Exception ex)
        {

        }
        return bFlag;
    }
    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientID"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["ProCurrentLogID"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }
}

   