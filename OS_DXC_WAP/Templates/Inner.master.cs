﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using log4net;

using Tracer = Utility.Diagnostic.FileTrace;
using System.Configuration;
using System.Web.Configuration;

public partial class Templates_Inner : System.Web.UI.MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //Aramco PID Changes By Hussamuddin
        try
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect("~/CommClear.aspx", true);
            }

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
                CheckSessionExpired();

            if (Session["ProviderID"] != null && !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
                aChangePassword.HRef = "../Provider/ChangePass.aspx";
            else if (Session["ClientID"] != null && !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
                aChangePassword.HRef = "../Client/ChangePass.aspx";
            else if (Session["broker"] != null && !string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
                aChangePassword.HRef = "../broker/ChangePass.aspx";



            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                CheckConcurrentUserStatus();
            }
        }
        catch (Exception ex)
        {

        }


        string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            HtmlImage AramcoSrc = (HtmlImage)AramcoImg.FindControl("AltAramcoImg");
            AramcoSrc.Src = Convert.ToString(Session["AramcoImagePath"]);
            AramcoImg.Visible = true;
        }
        else
            AramcoImg.Visible = false;
        //Aramco PID Changes By Hussamuddin 
 		aChangePassword.Visible = true;
		if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/client/"))
        {
            phClient.Visible = true;
            PHMember.Visible = false;
            PHProvider.Visible = false;
            backToServices.Attributes.Add("href", "~/Client/Main.aspx");

           /// backToServices.HRef = "../Client/Main.aspx"; 
        }
        else if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/member/"))
        {
            phClient.Visible = false;
            PHMember.Visible = true;
            PHProvider.Visible = false;
            ///backToServices.HRef = "../Client/Main.aspx";
            backToServices.Attributes.Add("href", "~/Client/Main.aspx");
        }
        else if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/provider/"))
        {
            phClient.Visible = false;
            PHMember.Visible = false;
            PHProvider.Visible = true;
            ////backToServices.HRef = "../Provider/Main.aspx";
            backToServices.Attributes.Add("href", "~/Provider/Main.aspx");
        }
        else if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/broker/"))
        {
            phClient.Visible = false;
            PHMember.Visible = false;
            PHProvider.Visible = false;
            backToServices.HRef = "../broker/Main.aspx";
            backToServices.Attributes.Add("href", "~/broker/Main.aspx");
            //phBroker.Visible = false;
        }
		else
        {
            if(!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
                backToServices.Attributes.Add("href", "~/Client/Main.aspx");
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"]))) 
                backToServices.Attributes.Add("href", "~/Provider/Main.aspx");
            aChangePassword.Visible = false;

        }		

        string loggedInAs = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) ||
            !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])) || !string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
        {
            //logouttag.Visible = true;
            //logintag.Visible = false;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " +  Convert.ToString(Session["MemberName"]) + " (" + Convert.ToString(Session["MembershipNo"]) + ")";
                //lnkHome.HRef = "../member/default.aspx";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ClientName"]) + " (" + Convert.ToString(Session["ClientUsername"]) + " - " + Convert.ToString(Session["BIName"]) + " )";
                //lnkHome.HRef = "../client/default.aspx";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ProviderName"]) + " (" + Convert.ToString(Session["ProviderUserName"]) + ")";
                //lnkHome.HRef = "../provider/default.aspx";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["BrokerName"]) + " (" + Convert.ToString(Session["BrokerUsername"]) + ")";
                //lnkHome.HRef = "../provider/default.aspx";
            }
        }
        else
        {
            if (GetCurrentPageName() != "Registration.aspx" && GetCurrentPageName() != "forgotpassword.aspx")
            {
                Response.Redirect("../default.aspx");
                //logouttag.Visible = false;
                //logintag.Visible = true;
            }
            else
            {
                backdiv.Visible = false;
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;

        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;
            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                responseCookie.Secure = true;
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;

    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti - XSRF token failed.");
                }
            }
        }
        catch (InvalidOperationException ex)
        {
            //throw ex;
        }
    }

    public string GetCurrentPageName()
    {
        string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }

    protected void hlLogout_Click(object sender, EventArgs e)
    {
        
    }
    protected void hlLogout_Click1(object sender, EventArgs e)
    {
       
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
        {
            Request.Cookies.Remove("MembershipNo");
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
        {
            Request.Cookies.Remove("ClientID");
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
        {
            Request.Cookies.Remove("ProviderID");
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
        {
            Request.Cookies.Remove("BrokerNo");
        }
        Session.Abandon();

        
        Response.Redirect("../default.aspx");
    }
    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        //ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientID"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["ProCurrentLogID"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Session.Abandon();
            WebApp.CommonClass.SetRediectionByPath();
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}
