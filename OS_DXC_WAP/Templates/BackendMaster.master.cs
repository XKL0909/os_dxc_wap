﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Templates_BackendMaster : System.Web.UI.MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("default") == false)
        {

            if (Session["user"] == null)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                divLogout.Visible = true;
                lblUserName.Text = Session["user"].ToString();
            }

        }
        else
        {
            if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("/default") == true)
            {
                divLogout.Visible = false;
            }
            else
            {
                divLogout.Visible = true;
                if (Session["user"] == null)
                {
                    Response.Redirect("default.aspx");
                }
                else
                {
                    lblUserName.Text = Session["user"].ToString();
                }
            }
        }


    }

    protected void Page_Init(object sender, EventArgs e)
    {
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;

        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;
            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                responseCookie.Secure = true;
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;

    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        try 
        { 
        if (!IsPostBack)
        {
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti - XSRF token failed.");
            }
        }
            }
        catch(Exception ex)
        {

        }
    }


    protected void btnLogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("default.aspx");
    }
}
