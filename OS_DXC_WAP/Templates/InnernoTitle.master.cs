﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Web.Security;
using Tracer = Utility.Diagnostic.FileTrace;

public partial class Templates_InnernoTitle : System.Web.UI.MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.UrlReferrer == null)
        {
            Response.Redirect("~/CommClear.aspx", true);
        }

        if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/client/"))
        {
            phClient.Visible = true;
            PHMember.Visible = false;
            PHProvider.Visible = false;
        }
        else if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/member/"))
        {
            phClient.Visible = false;
            PHMember.Visible = true;
            PHProvider.Visible = false;
        }
        else if (Request.Url.AbsoluteUri.ToString().ToLower().Contains("/provider/"))
        {
            phClient.Visible = false;
            PHMember.Visible = false;
            PHProvider.Visible = true;
        }

        string loggedInAs = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
        {
            //logouttag.Visible = true;
            //logintag.Visible = false;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ClientName"]) + " (" + Convert.ToString(Session["ClientUsername"]) + " - " + Convert.ToString(Session["BIName"]) + " )";
                
           
              
                //lnkHome.HRef = "../member/default.aspx";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {

                lblLoginName.Text = "Welcome! You are logged in as " +  Convert.ToString(Session["ClientName"]) + " (" + Convert.ToString(Session["ClientUsername"]) + ")";
                //lnkHome.HRef = "../client/default.aspx";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ProviderName"]) + " (" + Convert.ToString(Session["ProviderUserName"]) + ")";
                //lnkHome.HRef = "../provider/default.aspx";
            }
        }
        else
        {
            if (GetCurrentPageName().ToLower() != "registration.aspx" && GetCurrentPageName().ToLower() != "forgotpassword.aspx" && GetCurrentPageName().ToLower() != "providerregistration.aspx")
            {
                Response.Redirect("../default.aspx");
                //logouttag.Visible = false;
                //logintag.Visible = true;
            }
            else
            {
                backdiv.Visible = false;
            }
        }


    }

    protected void Page_Init(object sender, EventArgs e)
    {
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;

        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;
            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                responseCookie.Secure = true;
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;

    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti - XSRF token failed.");
            }
        }
    }


    public string GetCurrentPageName()
    {
        string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }

    protected void hlLogout_Click(object sender, EventArgs e)
    {
        
    }
    protected void hlLogout_Click1(object sender, EventArgs e)
    {
        Session.Abandon();

        Response.Redirect("../default.aspx");
    }
}
