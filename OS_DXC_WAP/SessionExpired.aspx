﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspAjax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            //Session Timeout Warning Dialog
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Session["Reset"] = true;
            Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            System.Web.Configuration.SessionStateSection section = (System.Web.Configuration.SessionStateSection)config.GetSection("system.web/sessionState");
            int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);

            CheckSessionExpired();
        }
        catch
        {

        }
    }
    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }

</script>





<%--<script runat="server" >
    
  protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Session.Abandon();
       // Response.Redirect("<script>window.open");
       Response.Write("<script>window.open('" + "../providerdefault.aspx'"+ "," + "'_parent'" + ");" + ";</script>");
    }
    
     protected void Page_Load(object sender, EventArgs e)
    {    
    }  
    
</script>
--%>


<%--
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        
        body1.Attributes.Add("onLoad", "window.setTimeout(\"window.location.href='default4.aspx'\"," + (Session.Timeout * 60 * 1000) + 5000 + ");");

    }

    protected void Redirect(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Write("<script>window.open('../providerdefault.aspx','_parent');</script>"");
    }
    
</script>--%>

<html xmlns="http://www.w3.org/1999/xhtml">

<%-- Response.Write("<script>window.open('../clientdefault.aspx','_parent');</script>");--%>


<head runat="server">
    <title>Session Expired</title>
    <link href="../styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src="../javascript/SessionMsg.js"></script>
    <link rel="stylesheet" type="text/css" href="../Styles/SessionMsg.css" />
    <script type="text/javascript" src="../functions/jquery.fancybox.js"></script>
    <style type="text/css">
        *{
            padding:0;
            margin:0;
        }
       #wrapper{
           margin-left:auto;
           margin-right:auto;
           width:1200px;
       }
       .button{
           background-color:#0094ff;
           width:130px;
           height:130px;
           padding:10px;
           text-decoration:none;
           font-weight:bold;
           color:#fff;
           font-size:.7em;
       }
    </style>

</head>
<body style="font-family: Arial;" id="body1" runat="server">
    <form id="form1" runat="server">
        <div id="wrapper">

            <table width="97%">
                <tr valign="middle">
                    <td>
                        <asp:Label ID="lblAdd_Employee" runat="server" Text="" Font-Size="Large" Font-Names="Arial"></asp:Label>
                    </td>
                    <td style="text-align: right">
                        <img src="images/logo-new.jpg" width="150" height="130" />
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="padding-top:50px;text-align:center"><b> You current session has expired. Kindly log-In once again.</b>
                        
                    </td>
                    
                </tr>
                <tr>
                    <td colspan="2" style="padding:20px;text-align:center">
                         <a class="button" href="default.aspx">Login Again </a>
                    </td>
                </tr>
               
            </table>


            
        </div>
        <asp:ScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ScriptManager>
        <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
        <div style="display: none;" id="divSessionExpired">
            <div>
                <br />
                <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                <br />
                <br />
            </div>
            <div class="inputEntity">
                <asp:LinkButton runat="server" ID="aYes" class="blockedLink normaBluelBtn" OnClick="aYes_Click">
                            <asp:Literal runat="server" Text="Yes"></asp:Literal></asp:LinkButton>

                <a id="aNo" class="blockedLink normaBluelBtn" onclick="closeFancyBox();">
                    <asp:Literal runat="server" Text="No"></asp:Literal></a>
            </div>
        </div>

        <h3 style="display: none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="lnkSession" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                    <div class="header">
                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left">
                                    <strong style="font-size: larger">Session Expiring! </strong>
                                </td>
                                <td style="float: right;"><strong style="font-size: larger">!ستنتهي مدة الزيارة &nbsp;</strong></td>
                            </tr>
                        </table>
                    </div>

                    <div class="body">
                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="text-align: left">
                                    <strong style="font-size: larger">Your Session will expire in <span id="seconds"></span>&nbsp;seconds.<br />
                                        Do you want to extend?</strong>
                                </td>
                                <td style="text-align: right">
                                    <strong style="font-size: larger">ستنتهي صلاحية هذه الزيارة خلال &nbsp;<span id="Arseconds"></span>&nbsp;ثانية. 
                                                <br />
                                        هل تريد تمديد الوقت؟ &nbsp;</strong>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="footer" align="center">
                        <asp:Button ID="btnYes" runat="server" Text="Yes / نعم" CssClass="Savebutton" CausesValidation="false" />
                        <asp:Button ID="btnNo" runat="server" Text="No / لا" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                    </div>
                </asp:Panel>
                <div id="mainContent">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>



    </form>
</body>


<script language="javascript" type="text/javascript">

    function redirect() {
        alert();
        window.opener.location.href = 'providerdefault.aspx';

    }




</script>






</html>
