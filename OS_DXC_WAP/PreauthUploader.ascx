﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="PreauthUploader" Codebehind="PreauthUploader.ascx.cs" %>

<br />
<asp:FileUpload ID="uploader" runat="server" Width="300px" />
<asp:Button CssClass="submitButton" ID="btnUpload" runat="server" Text="Upload" onclick="btnUpload_Click" CausesValidation="false" />
<br />
<asp:Label ID="lblUpload" runat="server" ForeColor="#cc0000" />
<br />
<table style="border:0" width="100%">
    <tr>
        <td style="text-align:left">Max individual file upload size is 5 MB. Note that attempting to upload large files over a slow internet connection may result in a timeout.</td>
    </tr>
    <tr>
        <td style="text-align:right;direction:rtl">الحد الأقصى لحجم كل ملف يراد تحميله 5 
            ميجا بيت (MB). لاحظ أن محاولة تحميل ملفات كبيرة عبر اتصال إنترنت بطيء قد يؤدي إلى قطع اتصالك بالموقع</td>
    </tr>
</table>

<br />
<asp:GridView ID="dvFiles" runat="server" CellPadding="4" 
    EnableModelValidation="True" ForeColor="#333333" GridLines="None" Width="100%" AutoGenerateColumns="False" 
                DataKeyNames="UploadedID" AllowPaging="false" 
    OnRowCommand="dvFiles_RowCommand" EnableViewState="true" Visible="false" 
    OnRowDeleting="dvFiles_RowDeleting">
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
        <asp:BoundField DataField="UploadedID" HeaderText="UploadedID" 
            InsertVisible="False" ReadOnly="True" SortExpression="UploadedID" 
            Visible="false" ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="FilePath" HeaderText="FilePath" 
            SortExpression="FilePath" Visible="false" ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="DocName" HeaderText="Document Name" 
            SortExpression="DocName" Visible="false" ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="FriendlyDocName" HeaderText="Document Name" 
            SortExpression="FriendlyDocName" Visible="true" 
            ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="LastUpdateTimestamp" HeaderText="Uploaded" 
            SortExpression="LastUpdateTimestamp" Visible="true" 
            ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
        </asp:BoundField>
        <asp:ButtonField ButtonType="Button" Text="Delete?" CommandName="Delete" />
    </Columns>
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
</asp:GridView>
    