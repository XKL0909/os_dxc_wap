﻿using Bupa.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_DashBoardWelcome : System.Web.UI.UserControl
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    protected void Page_Load(object sender, EventArgs e)
    {
        try {

            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                member.Visible = true;

                client.Visible = false;
                provider.Visible = false;
                broker.Visible = false;
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
            {
                broker.Visible = true;

                member.Visible = false;
                client.Visible = false;
                provider.Visible = false;
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                member.Visible = false;
                client.Visible = true;
                provider.Visible = false;
                broker.Visible = false;

                ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
                OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN request;
                OS_DXC_WAP.CaesarWS.EnqContInfoResponse_DN response;

                request = new OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN();


                request.contNo = Convert.ToString(Session["ClientID"]);
                request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;

                response = ws.EnqContInfo(request);

                ////CR-PID_CAESAR(Remove the relation type - child for BUPA Direct Contract) by Sakthi on 07-Aug-2016
                ////Start
                Session["ContractType"] = response.cont_Type;
                ////End

                //CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
                Session.Add("OnlineTicked", response.onl_Reimb_Ind);
                if (response != null && !string.IsNullOrWhiteSpace(response.onl_Reimb_Ind))
                {
                    if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() == "Y") || (Session["IsVIP"] != null && Session["IsVIP"] == "VP"))
                        ;//Enable the Reimbursement Claim Submission
                }


                if (!string.IsNullOrEmpty(Convert.ToString(response.status)))
                {
                    if (Convert.ToInt32(response.status) >= 0)
                    {
                        if (response.cont_Type.Trim() == "CORC")
                        {
                            if ((DateTime.Now - Convert.ToDateTime(response.end_Date)).Days <= 30 && (DateTime.Now - Convert.ToDateTime(response.end_Date)).Days >= 0)
                            {
                                lblClient.Text = "Dear Bupa client, the healthcare cover for your company will end on " + Convert.ToDateTime(response.end_Date).ToShortDateString() + ". To avoid any delays in Bupa services, please arrange to renew your cover now. Call us on 8002440302 for assistance.";
                            }
                        }
                        else
                        {
                            if ((DateTime.Now - Convert.ToDateTime(response.end_Date)).Days <= 30 && (DateTime.Now - Convert.ToDateTime(response.end_Date)).Days >= 0)
                            {
                                lblClient.Text = "Dear Bupa member, your Bupa healthcare cover will end on " + Convert.ToDateTime(response.end_Date).ToShortDateString() + ". To avoid any delays in Bupa services, please arrange to renew your cover now. Call us on 8001160500 for assistance.";
                            }
                        }
                    }
                }


            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            {
                provider.Visible = true;

                member.Visible = false;
                client.Visible = false;
                broker.Visible = false;
                ProviderStatement();
            }
    }
    catch(Exception ex){}

    }

    private void ProviderStatement()
    {

        string statmentIndicator = string.Empty;
        try
        {

            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            OS_DXC_WAP.CaesarWS.EnqProvStmIndRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqProvStmIndResponse_DN response;
            request = new OS_DXC_WAP.CaesarWS.EnqProvStmIndRequest_DN();


            request.provCode = Convert.ToString(Session["ProviderID"]);
            request.transactionID = TransactionManager.TransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = ws.EnqProvStmInd(request);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqProvStmIndRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqProvStmIndRequest_DN>();
            //XmlReq.Request(request, "EnqProvStmIndRequest_DN");
                        
            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqProvStmIndResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.EnqProvStmIndResponse_DN>();
            //XmlResp.Response(response, "EnqProvStmIndResponse_DN");
            

            if (response != null && response.status == "0")
            {
                statmentIndicator = Convert.ToString(response.stmInd).ToUpper().Trim();
            }
           
           

        }
        catch (Exception ex)
        {   
            Logger.Current.WriteException(ex);
        }
        Session["StmInd"] = statmentIndicator;
       
    }
}