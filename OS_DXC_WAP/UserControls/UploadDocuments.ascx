﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_UploadDocuments" Codebehind="UploadDocuments.ascx.cs" %>



<div class="FormCol">
    <h3>Upload invoices and supportive documents</h3>
    <label>Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .bmp) 5MB limit per file</label>
    <div class="fieldHolder">
        <asp:FileUpload runat="server" ID="FileUploader" />
        <asp:LinkButton runat="server" ID="lBtnUpload" CssClass="backToBupa normaBluelBtn floatLeft" Text="Upload" OnClick="lBtnUpload_Click"></asp:LinkButton>

    </div>
    <div>Max individual file upload size is 5 MB. Note that attempting to upload large files over a slow internet connection may result in a timeout.</div>
    <br />
    <br />
    <div class="tableMain" runat="server" id="divDocumentsTable" visible="false">
        <div class="tableStyling">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>Documents</th>
                    <th></th>
                </tr>
                <asp:Repeater runat="server" ID="rptrDocuments" OnItemDataBound="rptrDocuments_ItemDataBound" OnItemCommand="rptrDocuments_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblDocumentName"></asp:Label></td>
                            <td>
                                <asp:LinkButton runat="server" ID="lBtnDelete">Delete</asp:LinkButton></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </div>


    <div class="errorMessage" runat="server" id="divVaidatorErrorMessageFileUpload"></div>
</div>



