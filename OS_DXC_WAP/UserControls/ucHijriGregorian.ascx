﻿<%@ Control Language="C#" ClientIDMode="Predictable"  AutoEventWireup="true" Inherits="HijriGregorianControlJS.ucHijriGregorian" Codebehind="ucHijriGregorian.ascx.cs" %>
<!-- http://stackoverflow.com/questions/73022/codefile-vs-codebehind 
    http://www.sitefinity.com/developer-network/forums/general-discussions-/adding-user-control-quot-could-not-load-type-39-sitefinitywebapp-controls-xxx-39-quot -->
<style type="text/css">


    .cls_radio_label {
        display:inline-block;
        width:16px;
        height:16px;
        margin-right:10px;
        left:0;
        margin-bottom:3px;
    }

    .day_text {
        width: 17px;
	    background: white;
	    border: 1px double #DDD;
	    border-radius: 1px;
	    box-shadow: 0 0 1px #333;
	    color: #666;
	    float: left;
	    padding: 1px 5px;
	    outline: none;
    }

    .month_text {
        width: 17px;
        background: white;
	    border: 1px double #DDD;
	    border-radius: 1px;
	    box-shadow: 0 0 1px #333;
	    color: #666;
	    float: left;
	    padding: 1px 5px;
	    outline: none;
    }

    .year_text {
        width: 33px;
        background: white;
	    border: 1px double #DDD;
	    border-radius: 1px;
	    box-shadow: 0 0 1px #333;
	    color: #666;
	    float: left;
	    padding: 1px 5px;
	    outline: none;
    }

    .date_slash {
        float: left;
        padding: 2px;
        font-weight: bold;
    }

    .date_div {
        float: left;
    }

    #wrapper {
         margin: 0 auto;
    }

    #divHijri {
        float: left;
    }

    #divGreg{
        padding-left:20px;
        float: left;
    }


</style>

<div id="wrapper" style="display: inline-block">
    <div id="divHijri">
        <div>
            <asp:RadioButton ID="rdnHijri" runat="server" GroupName="calendar"  CssClass="css-checkbox"/>
            <label for="rdnHijri" class="cls_radio_label">Hijri</label>
        </div>
        <div class="date_div" style="padding-left: 7px;">
            <asp:TextBox ID="txtHijriDay" runat="server" onkeypress="javascript:return allownumbers(event);" MaxLength="2"
                CssClass="day_text" Enabled="False" EnableTheming="True" autocomplete="off"></asp:TextBox>
        </div>
        <div class="date_slash">/</div>
        <div class="date_div">
            <asp:TextBox ID="txtHijriMonth" runat="server" onkeypress="javascript:return allownumbers(event);"
                CssClass="month_text" MaxLength="2" Enabled="False" autocomplete="off"></asp:TextBox>
        </div>
        <div class="date_slash">/</div>
        <div class="date_div">
            <asp:TextBox ID="txtHijriYear" runat="server" onkeypress="javascript:return allownumbers(event);"
                CssClass="year_text" MaxLength="4"  Enabled="False"  autocomplete="off"></asp:TextBox>
        </div>
    </div>
    <div id="divGreg">
        <div>
            <asp:RadioButton ID="rdnGreg" runat="server" GroupName="calendar"  CssClass="css-checkbox"/>
            <label for="rdnHijri" class="cls_radio_label">Gregorian</label>
        </div>
        <div class="date_div" style="padding-left: 7px;">
            <asp:TextBox ID="txtGregDay" runat="server" onkeypress="javascript:return allownumbers(event);" MaxLength="2"
                CssClass="day_text" Enabled="False" EnableTheming="True" autocomplete="off"></asp:TextBox>
        </div>
        <div class="date_slash">/</div>
        <div class="date_div">
            <asp:TextBox ID="txtGregMonth" runat="server" onkeypress="javascript:return allownumbers(event);"
                CssClass="month_text" MaxLength="2" Enabled="False" autocomplete="off"></asp:TextBox>
        </div>
        <div class="date_slash">/</div>
        <div class="date_div">
            <asp:TextBox ID="txtGregYear" runat="server" onkeypress="javascript:return allownumbers(event);"
                CssClass="year_text" MaxLength="4"  Enabled="False" autocomplete="off"></asp:TextBox>
        </div>
    </div>
    <div style="clear: left;">
            <asp:RequiredFieldValidator ID="rfvRequired" runat="server" ErrorMessage="Field is Mandatory" Enabled="false"
            ControlToValidate="txtHijriDay" ForeColor="Red" Display="Dynamic" Font-Size="Small"></asp:RequiredFieldValidator>
    </div>


    <asp:HiddenField ID="hidGregDay" runat="server" />
    <asp:HiddenField ID="hidGregMonth" runat="server" />
    <asp:HiddenField ID="hidGregYear" runat="server" />

    <asp:HiddenField ID="hidHijriDay" runat="server" />
    <asp:HiddenField ID="hidHijriMonth" runat="server" />
    <asp:HiddenField ID="hidHijriYear" runat="server" />


</div>
