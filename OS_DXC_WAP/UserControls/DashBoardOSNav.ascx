﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_DashBoardOSNav" Codebehind="DashBoardOSNav.ascx.cs" %>

<style type="text/css">
    .style1 {
        text-align: center;
    }

    .style2 {
        text-align: center;
        width: 135px;
    }

    .style3 {
        text-align: center;
        height: 91px;
    }

    .style4 {
        text-align: center;
        width: 135px;
        height: 91px;
    }

    .btnVideo {
        border-radius: 3px 9px 1px;
        background: #0886D6;
        padding: 2px 0px;
        width: 80px;
        height: 45px;
    }
</style>

<script language="javascript">
    function DiplayMesage() {

        if (document.getElementById('<%= AramcoCodeType.ClientID %>').value == "TPAC") {
            window.location.href("../client/Request.aspx");
            return;
        }
       <%-- else {
            document.getElementById('<%= divPopup.ClientID %>').style.visibility = "visible"
        }--%>
    }
    function SystemMesage() {
        alert('Dear Customer\nThis option is currently not available due to credit control. Please contact your Account Manager for details.');
    }
   <%-- function CloseDivPopUp(value) {
        document.getElementById('<%= divPopup.ClientID %>').style.visibility = "hidden";
    }--%>

    function DisplayVideoOption() {
        document.getElementById('<%= divVideoPopup.ClientID %>').style.visibility = "visible"
    }
    function CloseVideoOption(val) {
        document.getElementById('<%= divVideoPopup.ClientID %>').style.visibility = "hidden";
        if (val == 2)
            window.open("../Client/PlayVideo.aspx", '_blank', 'location=yes,height=550,width=900,scrollbars=yes,status=yes');
    }


</script>
<asp:HiddenField ID="AramcoCodeType" Value=""  runat="server" />
<%--<div id="divPopup" runat="server" style="top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999999999999; visibility: hidden">
    <div style="left: 35%; top: 35%; position: fixed;">
        <table class="orangeTips" style="width: 100%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
            <tr style="background-color: #0886D6; line-height: 30px">
                <td colspan="3" style="color: #fff; text-align: left">
                    <strong style="padding-left: 5px; font-size: larger"></strong>
                    <span style="float: right;">
                        <a onclick="CloseDivPopUp('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size: larger">X</a>
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="../client/AddMemberIntermediate.aspx" id="addEmp" runat="server">
                        <img id="Img2" runat="server" style="width: 250px" border="0" src="~/images/ico/client/Add_New.jpg" /></a>
                </td>
                <td>
                    <a href="../client/Request.aspx" id="a8" runat="server">
                        <img id="Img43" runat="server" style="width: 250px" border="0" src="~/images/ico/client/Add_Old.jpg" /></a>
                </td>
            </tr>
        </table>
    </div>
</div>--%>
<div id="divVideoPopup" runat="server" style="top: 0; left: 0;  right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999999999999; visibility:hidden  ">
            <div style="left: 35%; top:35%; position: fixed; width:400px;">
                <table style="width: 100%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                    <tr style="background-color: #0886D6; line-height: 30px">
                        <td colspan="3" style="color: #fff; text-align: left">
                            <strong style="padding-left:5px; font-size:larger">New add employee/dependent user guide video</strong>
                            <span style="float: right; "> 
                                <a onclick="CloseVideoOption('1');" style="color: #fff; cursor: pointer; font-weight: bold; margin-right: 10px; text-decoration: underline; font-size:larger">X</a>
                            </span>
                        </td>
                    </tr>
                    <tr style="background-color: #fff"" >
                        <td style="text-align:center; padding: 30px 0px 30px 0px;">
                           <input type="button" id="btnPlay" runat="server"  class="btnVideo" style="color:#fff !important;" onclick="CloseVideoOption('2')" value="Play"  />
                        </td>
                        <td style="text-align:center; padding: 30px 0px 30px 0px;">
                            <input type="button" id="btnSkip" runat="server"  class="btnVideo" style="color:#fff !important;" onclick="CloseVideoOption('1')" value="Skip"  />
                        </td>
                    </tr>
                   <%-- <tr style="background-color: #fff;">
                           <td colspan="2" style="padding: 30px 0px 30px 0px">
                                &nbsp;
                           </td>
                           </tr>--%>
                    
                    
                </table>
            </div>
        </div>
<div id="Member" runat="server" class="tipofTheDayBanner orangeTips" visible="false">
    <table cellpadding="5">
        <tr>
            <td align="left" colspan="9">
                <h2 class="dahsboardTitleWhite" style="color: White;">Member Services</h2>
            </td>
        </tr>
        <tr>
            <td class="style1" title="View personal information of member and dependents"><a href="../Member/MyDetails.aspx">
                <img id="Img1" runat="server" border="0"
                    src="~/images/ico/member/My-Details.jpg" /></a><br />
                My Details</td>
            <td class="style1" title="Check your benefit details"><a href="../Member/MyCoverage.aspx">
                <img id="Img6" runat="server" border="0" src="~/images/ico/member/My-Coverage.jpg" /></a><br />
                My Coverage</td>
            <td class="style1" title="Submit your request for online preauthorization for your chronic medication" style="display: none;"><a href="../Member/MyMed1.aspx">
                    <img id="Img7" runat="server"
                        border="0"
                        src="~/images/ico/member/My-Medication.jpg"
                        onclick="return Img4_onclick()" /></a><br />
                My Medication</td>
            <td class="style1" title="View the information of providers you can visit"><a href="../Member/MyNetwork.aspx">
                <img id="Img8" runat="server"
                    border="0"
                    src="~/images/ico/member/My-Network.jpg" /></a><br />
                My Network</td>
            <td class="style1" title="Check your claim details and status "><a href="../Member/ClaimHistory.aspx">
                <img id="Img9" runat="server"
                    border="0" src="~/images/ico/common/ClaimHistory.jpg" /></a><br />
                Claim History</td>
            <td class="style1" title="Check your preauthorization details and status">
                <a href="../Member/MyPreauthHistory.aspx">
                    <img id="Img10"
                        runat="server" border="0"
                        src="~/images/ico/common/Pre-authhistory.jpg" /></a><br />
                Preauth History
            </td>
            <td class="style1" title="Print your membership certificate online"><a href="MembershipCert2.aspx">
                <img id="Img31"
                    runat="server" border="0"
                    src="~/images/ico/member/Membership-Certificate.jpg" /></a><br />
                Certificate</td>
            <td runat="server" id="ReimbursementForm" class="style2" title="View the required supporting document listing" valign="top">
                <a href="javascript:;" onclick="$('#basic-modal-content').modal({minHeight:'65%',minWidth:'60%'});">
                    <img id="Img35" runat="server" alt=" Claim History" border="0"
                        src="~/images/ico/common/ClaimHistory.jpg" title=" Submit reimbursement" /></a><br />
                Submit reimbursement</td>
            <td class="style1">&nbsp;</td>
        </tr>
    </table>
</div>

<div id="Client" class="tipofTheDayBanner orangeTips" runat="server" visible="false">
    <table cellpadding="5">
        <tr>
            <td align="left" colspan="5">
                <h2 class="dahsboardTitleWhite" style="color: White;">&nbsp;</h2>
            </td>
        </tr>
        <tr>
            <td class="style1" title="Add a new members or dependant to your scheme" valign="top">
                <a href="../client/AddMemberIntermediate.aspx" id="addEmp" runat="server">
                    <img id="Img2" runat="server" border="0"
                        src="~/images/ico/client/AddEmployeeOld.jpg" /></a><br />
                 Add Employee / Dependent
            </td>
			<%--<td class="style1" title="Add a new members or dependant to your scheme"
                valign="top">
                <a href="../client/Request.aspx" id="a1" runat="server">
                    <img id="Img39" runat="server" border="0"
                        src="~/images/ico/client/addemployee.jpg" /></a><br />
                 Add Employee / Dependent
            </td> --%> 
			<%--<td class="style1" title="Add a new members or dependant to your scheme" valign="top">
                <a  style="cursor: pointer;" ID="DisplayPopUp" runat="server"><img id="Img48" runat="server" border="0"
                        src="~/images/ico/client/AddEmployeeOld.jpg" /><br />
                 </a>
				 Add Employee / Dependent
                
            </td>   --%> 			
            <td class="style1" title="Delete an employee on your scheme
"
                valign="top">
                <a href="../client/DeleteEmployeeEnhanced.aspx" id="delEmp" runat="server">
                    <img id="Img13" runat="server" alt="" border="0"
                        src="~/images/ico/client/DeleteEmployee.jpg" /></a><br />
                Delete Employee</td>
            <td class="style1" title="Delete a dependent on your scheme
"
                valign="top">
                <a href="#" id="delDep" runat="server">
                    <img id="Img14" runat="server" alt="" border="0"
                        src="../images/ico/client/Deletedependent.jpg" /></a><br />
                Delete Dependent</td>
            <td class="style1" title="Change the scheme level of a particular employee
"
                valign="top">
                <a href="#" id="chClass" runat="server">
                    <img id="Img4" runat="server" border="0"
                        src="~/images/ico/client/changeclass.jpg" /></a><br />
                Change class</td>
            <td class="style1" title="Change the branch/location or sub group of your employees
"
                valign="top">
                <a href="../client/changebranch.aspx" id="chBranch" runat="server">
                    <img id="Img11" runat="server" alt="" border="0"
                        src="~/images/ico/client/changebranch.jpg" /></a><br />
                Change Branch</td>

        </tr>
        <tr>
             <td class="style3" title="Search and track the status of membership maintenance request"
                valign="top">
                <a href="../client/trackinfo.aspx">
                    <img id="Img5" runat="server" border="0"
                        src="~/images/ico/client/TrackInfo.jpg" /></a><br />
                Track info</td>
             <td class="style3" title="Send a request to Bupa Arabia for card replacement"
                valign="top">
                <a href="../client/cardreplace.aspx">
                    <img id="Img12" runat="server" alt="" border="0"
                        src="~/images/ico/client/ChangeCard.jpg" /></a><br />
                Replace Card
                    Data Correction</td>
            <td class="style4" valign="top">
                <a href="../client/BatchUploadReqDocs.aspx" target="_self">
                    <img id="Img15" runat="server" border="0"
                        src="../images/ico/common/RequiredDocument.jpg" /></a><br />
                Submit Required
                    <br />
                Supporting Document</td>
                        <td class="style4" valign="top">

                <a href="../Docs/ReqDocs2012.pdf" target="_self">
                    <img id="Img32" runat="server" border="0"
                        src="../images/ico/common/RequiredDocument.jpg" /></a><br />
                Required
                    Supporting Document List</td>
            <td class="style1" title="View details about the main member and dependents"
                valign="top">
                <a href="ctntMydetails.aspx">
                    <img id="Img16" runat="server"
                        border="0" src="~/images/ico/member/My-Details.jpg" /></a><br />
                My Details</td>


        </tr>
        <tr>
             <td class="style1" title="View the scheme's coverage
"
                valign="top">
                <a href="ctntmycoverage.aspx">
                    <img id="Img23" runat="server" border="0" src="~/images/ico/member/My-Coverage.jpg" /></a><br />
                My Coverage </td>
            <td class="style1" title="View the information of providers the member can visit
"
                valign="top">
                <a href="ctntmynetwork.aspx">
                    <img id="Img24" runat="server"
                        border="0"
                        src="~/images/ico/member/My-Network.jpg"
                        title="My Network" /></a><br />
                My Network </td>

            <td class="style2" title="Request/print the certificate of membership
"
                valign="top">

                <a href="CtntMembershipCert.aspx" style="display: block">
                    <img id="Img29"
                        runat="server" border="0"
                        src="~/images/ico/member/Membership-Certificate.jpg" /></a>Certificate</td>
            <td runat="server" id="ReimbursementFormClient" class="style2" title="View the required supporting document listing
"
                valign="top">

                <a href="javascript:;" onclick="$('#basic-modal-content').modal({minHeight:'65%',minWidth:'60%'});">
                    <img id="Img30" runat="server" alt=" Claim History" border="0"
                        src="~/images/ico/common/ClaimHistory.jpg" title=" Submit reimbursement" /></a><br />
                Submit reimbursement</td>
            <td class="style1" title="View previously submitted claim
"
                valign="top">
                <a href="ctntclaimHistory.aspx">
                    <img id="Img27" runat="server"
                        border="0" src="~/images/ico/common/ClaimHistory.jpg" alt=" Claim History"
                        title=" Claim History" /></a><br />
                Claim History</td>
        </tr>
        <tr>
             <td class="style1" title="Search and track the status of membership maintenance request" valign="top">
                <a href="ctntPreauthhistory.aspx">
                    <img id="Img28"
                        runat="server" border="0"
                        src="~/images/ico/common/Pre-authhistory.jpg"
                        title="Preauth History" /></a><br />
                Preauth History </td>
            <td class="style1" title="View all the online invoices
"
                valign="top">
                <a href="#" id="aInvoice"  runat="server">
                    <img id="Img26" runat="server" border="1"
                        src="~/images/ico/common/OnlineInvoice.jpg" style="border-color: #FFFFFF" /></a><br />
                Online Invoice</td>
            <td class="style1" title="View the statement of accounts
"
                valign="top">
                <a href="#" id="aStatement" runat="server">
                    <img id="Img25"
                        runat="server" border="0"
                        src="~/images/ico/common/AccountStatement.jpg" /></a><br />
                Account Statements</td>
             <td class="style1" title="View all the online invoices" valign="top" runat="server">
                <a href="../client/CCHICap.aspx" id="aCCHICap" runat="server">
                    <img id="Img38" runat="server" border="0" src="../images/ico/common/RequiredDocument.jpg" title="CCHI Cap" />
                </a>
                <br />
                CCHI Cap
            </td>
            <td align="left"  title="User Guide" valign="top">
               <a href="../Docs/GSUserGuide.pdf" id="aUserManual" runat="server" target="_blank"><img id="Img37" runat="server" border="0" src="../images/ico/common/RequiredDocument.jpg" title="User Guide" />
                    </a><br />User Guide
				</td>
        </tr>
        <tr>             
            <td class="style1" title="New Addition Guide" valign="top">
                <a href="../Docs/CCHI-unified-manual-Web-Addition.pdf" target="_newtab" id="adddep" runat="server">
                    <img id="Img3" runat="server" border="0" src="../images/ico/common/RequiredDocument.jpg" /></a><br />
                New Addition Guide
			             </td>
             <td class="style1" title="New Addition Video" valign="top">
                <a  style="cursor: pointer;" ID="addMemberVideo" runat="server"><img id="Img49" runat="server" border="0"
                       src="../images/ico/common/RequiredDocument.jpg" /><br />
                 </a>
				 New Addition Video
                
            </td>  
            <td class="style1" title="User Management" runat="server" id="clientUserManagement" valign="top">
                <a  style="cursor: pointer;" ID="aClientUserManagement" href="../UserManagment.aspx" runat="server">
                    <img id="Img39" runat="server" border="0"
                       src="../images/ico/client/UserManagIcon.png" style="margin-top: 17px;" /><br />
                 </a>
				 User Management
                
            </td>  
            <td class="style1" title="View all the online invoices
"
                valign="top" runat="server" id="MIS" visible="false">
                <a href="/CognosMIS/Default.aspx" target="_blank">
                    <img id="Img34" runat="server" border="1"
                        src="~/images/ico/common/OnlineInvoice.jpg" style="border-color: #FFFFFF" title="MIS Report" /></a><br />
                MIS Report</td>
            <td class="style4" title="View the SME Renewal" valign="top" runat="server" id="tdSMERenewal" visible="false">
                <a href="../client/SMEintermediate.aspx" target="_blank" runat="server" id="hrefSMERenewal">
                    <img id="imgSMERenewal" runat="server" border="1" src="~/images/ico/common/eclaimssubmission.jpg" style="border-color: #FFFFFF" title="SME Renewal" /></a><br />
                SME Renewal
				</td>
              
        </tr>
    </table>
</div>

<!--Start Adding Page Customization for Aramco -->
<div id="AramcoClient" class="tipofTheDayBanner orangeTips" runat="server" visible="false">
    <table cellpadding="5">
        <tr>
            <td align="left" colspan="5">
                <h2 class="dahsboardTitleWhite" style="color: White;">&nbsp;</h2>
            </td>
        </tr>
        <tr>

            <td class="style4" title="Add a new members to your scheme" valign="top">
                <a href="../client/Request.aspx" id="a3" runat="server">
                    <img id="Img41" runat="server" border="0"
                        src="~/images/ico/client/AddEmployeeOld.jpg" /></a><br />
                Add member
            </td>
            
            <td class="style4" title="New Addition Guide" valign="top">
                <a href="../Docs/CCHI-unified-manual-Web-Addition.pdf" target="_newtab" id="a2" runat="server">
                    <img id="Img40" runat="server" border="0" src="../images/ico/common/RequiredDocument.jpg" /></a><br />
                New Addition Guide
            </td>

            <td class="style4" title="Delete member on your scheme" valign="top">
                <a href="../client/DeleteEmployeeEnhanced.aspx" id="A4" runat="server">
                    <img id="Img42" runat="server" alt="" border="0"
                        src="~/images/ico/client/DeleteEmployee.jpg" /></a><br />
                Delete member

            </td>

            <td class="style4" title="Change the scheme level of a particular employee" valign="top">
                <a href="#" id="A6" runat="server">
                    <img id="Img44" runat="server" border="0"
                        src="~/images/ico/client/changeclass.jpg" /></a><br />
                Change Scheme

            </td>
            <td class="style4" title="Change the branch/location or sub group of your employees" valign="top">
                <a href="../client/changebranch.aspx" id="A7" runat="server">
                    <img id="Img45" runat="server" alt="" border="0"
                        src="~/images/ico/client/changebranch.jpg" /></a><br />
                Change Branch

            </td>

            <td class="style4" title="Send a request to Bupa Arabia for card replacement" valign="top">
                <a href="../client/cardreplace.aspx">
                    <img id="Img46" runat="server" alt="" border="0"
                        src="~/images/ico/client/ChangeCard.jpg" /></a><br />
                Replace Card
                    Data Correction
            </td>
            

        </tr>
        <tr>
            <td class="style4" title="Search and track the status of membership maintenance request" valign="top">
                <a href="../client/trackinfo.aspx">
                    <img id="Img47" runat="server" border="0"
                        src="~/images/ico/client/TrackInfo.jpg" /></a><br />
                Track info

            </td>

            <td class="style4" title="View details about the main member and dependents" valign="top">
                <a href="ctntMydetails.aspx">
                    <img id="Img52" runat="server"
                        border="0" src="~/images/ico/member/My-Details.jpg" /></a><br />
                My Details

            </td>

            <td class="style4" title="View the scheme's coverage" valign="top">
                <a href="ctntmycoverage.aspx">
                    <img id="Img53" runat="server" border="0" src="~/images/ico/member/My-Coverage.jpg" /></a><br />
                My Coverage 

            </td>
            <td class="style4" title="View the information of providers the member can visit" valign="top">
                <a href="ctntmynetwork.aspx">
                    <img id="Img54" runat="server"
                        border="0"
                        src="~/images/ico/member/My-Network.jpg"
                        title="My Network" /></a><br />
                My Network 
            </td>
            
            <td class="style4" title="Request/print the certificate of membership" valign="top">
                <a href="CtntMembershipCert.aspx" style="display: block">
                    <img id="Img55"
                        runat="server" border="0"
                        src="~/images/ico/member/Membership-Certificate.jpg" /></a>Certificate
            </td>
           
             <td class="style4" title="View all the online invoices" valign="top">
                <a href="#" id="aAInvoice" runat="server">
                    <img id="Img58" runat="server" border="1"
                        src="~/images/ico/common/OnlineInvoice.jpg" style="border-color: #FFFFFF" /></a><br />
                Online Invoice
            </td>

        </tr>
        <tr>
            
            <td class="style4" title="View the statement of accounts" valign="top">
                <a href="#" id="aAStatement" runat="server">
                    <img id="Img59"
                        runat="server" border="0"
                        src="~/images/ico/common/AccountStatement.jpg" /></a><br />
                Account Statements
            </td>
            
        </tr>
        
    </table>
</div>
<!--End Adding Page Customization for Aramco -->


<div id="Provider" class="tipofTheDayBanner orangeTips" runat="server" visible="false">
    <table cellpadding="5">
        <tr>
            <td align="left" colspan="9">
                <h2 class="dahsboardTitleWhite" style="color: White;">Provider Services</h2>
            </td>
        </tr>
        <tr>

            <td align="center" class="boxcaptionOrange" runat="server" id="Option1"
                visible="true" title="Submit new preauthorization request
"
                valign="top">
                <div id='basic-modal'>

                    <a href="../Provider/Preauth15.aspx" class='basic'>
                        <img id="Img17" runat="server" border="0" src="~/images/ico/Provider/New-request.jpg" /></a><br />
                    New Request
		
                </div>
            </td>
            <td align="center" class="boxcaptionOrange" runat="server" id="Option2"
                visible="false" title="Check and print the status of your existing preauthorization requests
"
                valign="top">
                <div id='basic-modal1'>
                    <a href="../Provider/ExistingRequestSearch.aspx" class='basic'>
                        <img id="Img18" runat="server" border="0" src="~/images/ico/provider/Existingrequest.jpg" /></a><br />
                    Existing Request
                </div>
            </td>
            <td align="center" class="boxcaptionOrange" runat="server" id="Option3"
                visible="false" style="display: none" valign="top"><a href="../Provider/CheckNewReply.aspx" class="class">
                    <img id="Img19" runat="server" border="0" src="~/images/ico/provider/Checknewreply.jpg" title="My Medication" /></a><br />
                Check New Reply</td>
            <td align="center" class="boxcaptionOrange" runat="server" id="Option4"
                visible="false" title="Check the table of benefits of our latest contract 
"
                valign="top"><a href="../Provider/TOBSearch.aspx" target="_blank">
                    <img id="Img20" runat="server" border="0" src="~/images/ico/common/RequiredDocument.jpg" /></a><br />
                Table of Benefits</td>
            <td align="center" class="boxcaptionOrange" runat="server" id="Option5"
                visible="false" title="Submit your claim online using a downloadable template
"
                valign="top"><a href="../Provider/eclaims.aspx" target="_blank">
                    <img id="Img21" runat="server" border="0"
                        src="~/images/ico/common/eclaimssubmission.jpg" /></a><br />
                E-Claim Submission</td>
            <td align="center" class="boxcaptionOrange" runat="server" id="Option6"
                visible="false" title="Verify members eligibility and benefits
"
                valign="top">
                <a href="../Provider/MemberVerification1.aspx">
                    <img id="Img22" runat="server" border="0"
                        src="~/images/ico/provider/Verification.jpg" /></a><br />
                Verification
            </td>
            <td align="center" class="boxcaptionOrange" runat="server" id="Option7"
                visible="false" title="Verify members eligibility and benefits
"
                valign="top"><a href="../Provider/notify.aspx">
                    <img id="Img33" runat="server" border="0"
                        src="~/images/ico/common/eclaimssubmission.jpg" /></a><br />
                Notification of New<br />
                Service Code</td>
            <td align="center" class="boxcaptionOrange" title="Doctor's Details" runat="server" id="Option8" visible="false">

                <a href="../Provider/PhysicianDetails.aspx">
                    <img id="Img36" runat="server" border="0" src="~/images/ico/common/eclaimssubmission.jpg" /></a><br />
                Doctor's Details<br />


            </td>

        </tr>
        <tr>
            <td align="center" class="boxcaptionOrange" title="Doctor's Details" runat="server" id="Option9" visible="false">

                <a href="../Provider/BatchStatmentsDetail.aspx">
                    <img id="imgProviderStatmentIndicator" runat="server" border="0"  src="~/images/ico/common/AccountStatement.jpg" /></a><br />
                Batch Statments<br />

            </td>
            <td align="center" class="boxcaptionOrange" runat="server" id="providerUserManagement"
                title="User Management"
                valign="top">
                <a href="../UserManagment.aspx">
                    <img id="Img50" runat="server" border="0"
                        src="../images/ico/client/UserManagIcon.png" style="margin-top: 17px;" /></a><br />
                User Management
            
            </td>
            <td colspan="7"></td>
        </tr>
    </table>
   
</div>




