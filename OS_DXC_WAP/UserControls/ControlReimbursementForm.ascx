﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_ControlReimbursementForm" Codebehind="ControlReimbursementForm.ascx.cs" %>

<%@ Register Src="../Uploader.ascx" TagName="Uploader" TagPrefix="uc1" %>
<%--<asp:ScriptManager runat="server" ID="sm1"></asp:ScriptManager>--%>

<style>
.loaderHolder {
	position: relative;

}
.loader {
	position: absolute;
	width: 100%;
	height: 100%;
	z-index: 1;
	top:0;
	left:0;
}
.loaderContent {
	background: url(../images/bg.png);
	text-align: center;
	width: 100%;
	height: 100%;
	position: relative;
	float: left;
}
.loaderContent td {
	text-align: center;
	vertical-align: middle;
}
.loaderContent img {
	float: none;
}
.loaderContent h3 {
	font-size: 18px;
	font-weight: 700;
	font-family: Arial;
	color: #AA0609;
	direction: ltr;
	padding-top: 10px;
}

    input[type=checkbox],input[type=radio]  {
        border:none;
    }

.input {
    position: relative;

}

.tooltip {
    display: none;
    padding: 10px;
    font-family: Tahoma;
    font-size:15px;
}

.input:hover .tooltip {
    background: #0099ff;
    border-radius: 3px;
    bottom: -90px;
    color: white;
    display: inline;
    height: 60px;
    width:700px;
    left: 0;
    line-height: 30px;
    position: absolute;

}

.input:hover .tooltip:before {
    display: block;
    content: "";
    position: absolute;
    top: -5px;
    width: 0; 
	height: 0; 
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	
	border-bottom: 5px solid  #0099ff;

}        

</style>

<script type="text/javascript" src="js/NumberValidation.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        SetMobileNumberValidation("#<%= txtMobileNumber.ClientID %>");
        SetMobileNumberValidation("#<%= txtNumberOfInvoices.ClientID %>");
       <%-- SetIBANNumberValidation("#<%= txtIBANNumber.ClientID %>");--%>
        SetAmountValidation("#<%= txtTotalAmount.ClientID %>");
    });

    // to allow numbers only in the numbers text boxes
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.]")
        if (key == 8) {
            keychar = String.fromCharCode(key);
        }
        if (key == 11) {
            key = 8;
            keychar = String.fromCharCode(key);
        }
        return reg.test(keychar);
    }

    function CheckAgree(source, args) {
        var cbx = document.getElementById("<%= chkAgree.ClientID %>");
        if (cbx.checked == false) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
        if (!args.IsValid)
            $("[id$='<%= chkAgree.ClientID %>'] :checkbox:first").focus();
    }

    function ValidateReasonList(source, args) {
        var chkListModules = document.getElementById('<%= cblReasons.ClientID %>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
        if (!args.IsValid)
            $("[id$='<%= cblReasons.ClientID %>'] :checkbox:first").focus();
    }
</script>

    <script type="text/javascript">
        function jScript() {

            $(document).ready(function () {
                if ($("#RadioDiv input:radio:checked").val() == 2) {
                    document.getElementById('divEFT').style.display = "none";
                    document.getElementById('<%= rfv_ddlBankCountry.ClientID %>').enabled = false;
                    document.getElementById('<%= rfv_ddlBankCountry.ClientID %>').isvalid = false;
                    document.getElementById('<%= rfv_ddlBankName.ClientID %>').enabled = false;
                    document.getElementById('<%= rfv_ddlBankName.ClientID %>').isvalid = false;
                    document.getElementById('<%= rfvIBAN.ClientID %>').enabled = false;
                    document.getElementById('<%= rfvIBAN.ClientID %>').isvalid = false;
                }
                else {
                    document.getElementById('divEFT').style.display = "";
                    //this is temprary untill we recieve business response
                    document.getElementById('<%= rfv_ddlBankCountry.ClientID %>').enabled = true;
                    document.getElementById('<%= rfv_ddlBankCountry.ClientID %>').isvalid = true;
                    document.getElementById('<%= rfv_ddlBankName.ClientID %>').enabled = true;
                    document.getElementById('<%= rfv_ddlBankName.ClientID %>').isvalid = true;
                    document.getElementById('<%= rfvIBAN.ClientID %>').enabled = true;
                    document.getElementById('<%= rfvIBAN.ClientID %>').isvalid = true;
                }
            });
            $(document).ready(function jScript2() {
                $('#RadioDiv input').click(function () {
                    $("#info").text('Selected Value: ' + $("#RadioDiv input:radio:checked").val());
                    if ($("#RadioDiv input:radio:checked").val() == 2) {
                        document.getElementById('divEFT').style.display = "none";
                        document.getElementById('<%= rfv_ddlBankCountry.ClientID %>').enabled = false;
                        document.getElementById('<%= rfv_ddlBankCountry.ClientID %>').isvalid = false;
                        document.getElementById('<%= rfv_ddlBankName.ClientID %>').enabled = false;
                        document.getElementById('<%= rfv_ddlBankName.ClientID %>').isvalid = false;
                        document.getElementById('<%= rfvIBAN.ClientID %>').enabled = false;
                        document.getElementById('<%= rfvIBAN.ClientID %>').isvalid = false;
                    }
                    else {
                        //this is temprary untill we recieve business response
                        document.getElementById('divEFT').style.display = "";
                        document.getElementById('<%= rfv_ddlBankCountry.ClientID %>').enabled = true;
                        document.getElementById('<%= rfv_ddlBankCountry.ClientID %>').isvalid = true;
                        document.getElementById('<%= rfv_ddlBankName.ClientID %>').enabled = true;
                        document.getElementById('<%= rfv_ddlBankName.ClientID %>').isvalid = true;
                        document.getElementById('<%= rfvIBAN.ClientID %>').enabled = true;
                        document.getElementById('<%= rfvIBAN.ClientID %>').isvalid = true;
                    }
                });
            });
        }
</script>

<div>
    <asp:Image ID="imgSuc" runat="server" Visible="false" ImageUrl="~/images/reimp_req_suc.png" />
    <br />
    <asp:Label ID="lblMessage" runat="server" Visible="False"></asp:Label>
</div>
<asp:Panel runat="server" ID="pnlForm">
    <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
        <tr>
            <td style="background-color: #ffffff; height: 25px;" valign="top" align="center">
                <asp:Label ID="lblEr" runat="server" Text="" Font-Bold="true" ForeColor="Red" Font-Size="Large"></asp:Label></td>
        </tr>
        <tr>
            <td style="background-color: #ffffff; height: 25px;" valign="top">
                <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Member Details</span></strong>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <%-- Re-Calling JavaScirpt/jQuery function on each Asynchronous post back. --%>
            <script type="text/javascript" language="javascript">
                Sys.Application.add_load(jScript);

            </script>
            <%-- // Re-Calling function End --%>
            <div class="loaderHolder">
                <asp:UpdateProgress ID="loader" runat="server" AssociatedUpdatePanelID="updatePanel1" DisplayAfter="0" class="loader">
                    <ProgressTemplate>
                        <div class="loaderContent">
                            <table width="100%" height="100%">
                                <tr>
                                    <td>
                                        <img src="/Images/progress-indicator.gif" /><br />
                                        <h3>Loading...</h3>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <div class="containerClass">
                <div runat="server" id="divShowForMember" visible="false">
                    <div class="smallLabelClass">
                        <span style="color: #ff3300">
                            <strong>*</strong>
                        </span>
                        <asp:Label ID="Label12" runat="server" Font-Bold="False" ForeColor="#000036" Text="Select Member"></asp:Label>
                    </div>
                    <div class="controlClass" style="width: 30%;">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlDependents" AutoPostBack="true"
                                ClientIDMode="Static" runat="server" BackColor="#eff6fc" OnSelectedIndexChanged="ddlDependents_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMembershipNumber" InitialValue=""
                            Display="Dynamic" ValidationGroup="1" Width="115px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="labelClass" runat="server" id="divShowForClient" visible="false">
                    <span style="color: #ff3300">
                        <strong>*</strong>
                    </span>
                    <asp:Label ID="Label1" runat="server" Font-Bold="False" ForeColor="#000036" Text="Enter Membership Number"></asp:Label>
                </div>
                <div class="smallLabelClass" runat="server" id="divShowForMember2" visible="false" style="width: 111px;">
                    <span style="color: #ff3300">
                        <strong>*</strong>
                    </span>
                    <asp:Label ID="Label13" runat="server" Font-Bold="False" ForeColor="#000036" Text="Membership No."></asp:Label>
                </div>
                <div class="controlClass">
                    <div class="styled-select" style="float: left;">
                        <asp:TextBox CssClass="textbox" ID="txtMembershipNumber" runat="server" MaxLength="8" BackColor="#eff6fc"
                            onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                    </div>
                    <asp:Button CssClass="submitButton" ID="btnGetDetails" runat="server" Text="Get Details"
                        OnClick="btnGetDetails_Click" ValidationGroup="3" Style="margin-left: 15px; width: 100px; float: left;" Visible="false" />
                    <div class="clear"></div>
                    <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtMembershipNumber" InitialValue="" SetFocusOnError="true"
                        Display="Dynamic" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMembershipNumber" InitialValue=""
                        Display="Dynamic" ValidationGroup="3" Width="115px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>

                     <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </div>
                <div class="clear"></div>
            </div>
            
            <div>
                <div class="containerClass headerBox" runat="server" id="divShowForClient2" visible="false">
                    <div class="labelClass">
                        <span style="color: #ff3300">
                            <strong>*</strong>
                        </span>
                        <asp:Label ID="Label2" runat="server" Font-Bold="False" ForeColor="#000036" Text="Member Name"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:TextBox CssClass="textbox" ID="txtMemberName" runat="server" BackColor="#eff6fc"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="rfv2" runat="server" ControlToValidate="txtMemberName" InitialValue=""
                            Display="Dynamic" ValidationGroup="1" Width="115px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="containerClass headerBox">
                    <div class="labelClass">
                        <span style="color: #ff3300">
                            <strong>*</strong>
                        </span>
                        <asp:Label ID="Label3" runat="server" Font-Bold="False" ForeColor="#000036" Text="Company Name"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:TextBox CssClass="textbox" ID="txtCompanyName" runat="server" BackColor="#eff6fc"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="rfv3" runat="server" ControlToValidate="txtCompanyName" InitialValue=""
                            Display="Dynamic" ValidationGroup="1" Width="115px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear" runat="server" id="divClear"></div>
                <div class="containerClass headerBox">
                    <div class="labelClass">
                        <span style="color: #ff3300">
                            <strong>*</strong>
                        </span>
                        <asp:Label ID="Label4" runat="server" Font-Bold="False" ForeColor="#000036" Text="Mobile Number"></asp:Label><br />

                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:TextBox CssClass="textbox" ID="txtMobileNumber" runat="server" BackColor="#eff6fc" MaxLength="12" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                            <br />
                            <i style="font-size: small; color: green;">(966xxxxxxxxx or 971xxxxxxxxx)</i>
                        </div>
                        <asp:RequiredFieldValidator ID="rfv4" runat="server" ControlToValidate="txtMobileNumber" InitialValue=""
                            Display="Dynamic" ValidationGroup="1" Width="115px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear" runat="server" id="divClear2" visible="false"></div>
                <div class="containerClass headerBox">
                    <div class="labelClass">
                        <span style="color: #ff3300">
                            <strong>*</strong>
                        </span>
                        <asp:Label ID="Label5" runat="server" Font-Bold="False" ForeColor="#000036" Text="Email"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:TextBox CssClass="textbox" ID="txtEmail" runat="server" BackColor="#eff6fc"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="rfv5" runat="server" ControlToValidate="txtEmail" InitialValue="" SetFocusOnError="true"
                            Display="Dynamic" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rev1"
                            ControlToValidate="txtEmail" runat="server"
                            ErrorMessage="Invalid Email"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="1" Display="Dynamic" SetFocusOnError="true">
                        </asp:RegularExpressionValidator>
                    </div>
                    <div class="clear"></div>
                    <table>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" valign="top">
                                <strong style="color: red;">* </strong>Please If the mobile number or email is incorrect, amend it<br />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>

                </div>
            </div>
            <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
                <tr>
                    <td style="background-color: #ffffff; height: 25px;" valign="top">
                        <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Reasons for not using Bupa listed Health Care Providers</span></strong>
                    </td>
                </tr>
            </table>
            <div>
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <asp:CheckBoxList runat="server" ID="cblReasons" RepeatColumns="2" Width="400px" Style="float: left;" DataTextField="Name" DataValueField="ID">
                            </asp:CheckBoxList>
                        </td>
                        <td valign="bottom" style="position: relative" id="tdOtherReason">
                            <div class="controlClass" style="position: absolute; top: 51px; left: -125px;">
                                <div class="styled-select">
                                    <asp:TextBox CssClass="textbox" ID="txtOtherReason" runat="server" BackColor="#eff6fc" Style="height: 19px; width: 198px;"></asp:TextBox>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:CustomValidator runat="server" ID="cv2"
                                ClientValidationFunction="ValidateReasonList" ValidationGroup="1" ForeColor="Red"
                                ErrorMessage="Please select a reason for not using Bupa listed health care providers" SetFocusOnError="true"></asp:CustomValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
            <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
                <tr>
                    <td style="background-color: #ffffff; height: 25px;" valign="top">
                        <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Claim Details</span></strong>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" valign="top">Please fill the invoices details in the below field and press Add button<br />
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
            <asp:UpdatePanel runat="server" ID="updatePanel2">
                <ContentTemplate>
                    <div class="loaderHolder">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="updatePanel2" DisplayAfter="0" class="loader">
                            <ProgressTemplate>
                                <div class="loaderContent">
                                    <table width="100%" height="100%">
                                        <tr>
                                            <td>
                                                <img src="/Images/progress-indicator.gif" /><br />
                                                <h3>Loading...</h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <div style="width: 660px;">
                            <table cellpadding="0" style="font-size: small; font-family: Arial; width: 660px;">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Font-Bold="False" ForeColor="#000036" Text="Number of Invoices"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Font-Bold="False" ForeColor="#000036" Text="Total Amount"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Font-Bold="False" ForeColor="#000036" Text="Currency Code"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="styled-select">
                                            <asp:TextBox CssClass="textbox" ID="txtNumberOfInvoices" runat="server" BackColor="#eff6fc" Width="150px" MaxLength="3"></asp:TextBox>

                                        </div>
                                    </td>
                                    <td>
                                        <div class="styled-select">
                                            <asp:TextBox CssClass="textbox" ID="txtTotalAmount" runat="server" BackColor="#eff6fc" Width="150px" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="styled-select">
                                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlCurrencyCode"
                                                ClientIDMode="Static" runat="server" Width="150px"
                                                DataTextField="NameCode" DataValueField="NameCode" BackColor="#eff6fc">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Button CssClass="submitButton" ID="btnAddInvoice" runat="server" Text="Add" OnClick="btnAddInvoice_Click" ValidationGroup="2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfv10" runat="server" ControlToValidate="txtNumberOfInvoices" InitialValue="" SetFocusOnError="true"
                                            Display="Dynamic" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfv11" runat="server" ControlToValidate="txtTotalAmount" InitialValue="" SetFocusOnError="true"
                                            Display="Dynamic" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfv12" runat="server" ControlToValidate="ddlCurrencyCode" InitialValue="" SetFocusOnError="true"
                                            Display="Dynamic" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                            <asp:GridView runat="server" ID="gvInvoices" Visible="false" AutoGenerateColumns="false" Width="100%" Style="margin-top: 30px;" OnRowDataBound="gvInvoices_RowDataBound" OnRowCommand="gvInvoices_RowCommand" OnRowEditing="gvInvoices_RowEditing" OnRowDeleting="gvInvoices_RowDeleting" AutoGenerateDeleteButton="false" AutoGenerateEditButton="false">
                                <HeaderStyle BackColor="#5D7B9D" ForeColor="#333333" Font-Bold="true" BorderStyle="None" Height="30px" Font-Size="16px" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" BorderStyle="None" Height="30px" Font-Size="15px" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField HeaderText="Number of Invoices" DataField="NumberOfInvoices" />
                                    <asp:BoundField HeaderText="Total Amount" DataField="TotalAmount" />
                                    <asp:BoundField HeaderText="Currency Code" DataField="CurrencyCode" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btnDelete" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField runat="server" ID="hfInvoiceIndex" Value="0" />
                            <asp:TextBox runat="server" ID="txtGridView" Style="display: none;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGridView" InitialValue="" SetFocusOnError="true"
                                Display="Dynamic" ValidationGroup="1" Width="325px">Please enter your claim details and click Add button</asp:RequiredFieldValidator>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
                <tr>
                    <td style="background-color: #ffffff; height: 25px;" valign="top">
                        <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Payment Details</span></strong>

                    </td>
                </tr>
            </table>
            <asp:Literal ID="litPaymentDetails" runat="server" Visible="false">
               <div style="height:70px;">
                <span style="padding-top:10px;padding-bottom:50px;font-weight:normal; color:black;"> 
                    (The Payment will be done through your company)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    سيتم دفع المبلغ من خلال الشركة او المؤسسة التي تنتمون إليها
                </span><br />
                    
                </div>
            </asp:Literal>
            <div id="divPaymentDetails" runat="server">

                <div class="containerClass">
                    <div class="smallLabelClass">
                        <span style="color: #ff3300">
                            <strong>*</strong>
                        </span>
                        <asp:Label ID="Label9" runat="server" Font-Bold="False" ForeColor="#000036" Text="Payment Type"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div id="RadioDiv" class="styled-select">
                            <asp:RadioButtonList runat="server" ID="rblPaymentType" RepeatDirection="Horizontal">
                                <asp:ListItem Text="EFT (Electronic Fund Transfer)" Value="1" Selected="True"></asp:ListItem>                              
                            </asp:RadioButtonList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rblPaymentType" InitialValue="" SetFocusOnError="true"
                            Display="Dynamic" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <div id="divEFT">
                    <div id="divBankCountry" runat="server" class="containerClass showForBankTransfer">
                        <div class="smallLabelClass">
                            <span style="color: #ff3300">
                                <strong>*</strong>
                            </span>
                            <asp:Label ID="Label14" runat="server" Font-Bold="False" ForeColor="#000036" Text="Bank Country"></asp:Label>
                        </div>
                        <div class="controlClass">
                            <div>
                                   <asp:TextBox CssClass="textbox" ID="txtBankCountry" runat="server" MaxLength="100"></asp:TextBox>
<%--                                <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlBankCountry"
                                    ClientIDMode="Static" runat="server"
                                    DataTextField="Name" DataValueField="Code" BackColor="#eff6fc">
                                </asp:DropDownList>--%>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv_ddlBankCountry" runat="server" ControlToValidate="txtBankCountry"
                                InitialValue=""
                                SetFocusOnError="true"
                                Display="Dynamic" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div id="divBanks" runat="server" class="containerClass showForBankTransfer">
                        <div class="smallLabelClass">
                            <span style="color: #ff3300">
                                <strong>*</strong>
                            </span>
                            <asp:Label ID="Label10" runat="server" Font-Bold="False" ForeColor="#000036" Text="Name of the Bank"></asp:Label>
                        </div>
                        <div class="controlClass">
                            <div>
                              <asp:TextBox CssClass="textbox" ID="txtBankName" runat="server" MaxLength="100"></asp:TextBox>

<%--                                <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlBankName"
                                    ClientIDMode="Static" runat="server"
                                    DataTextField="Name" DataValueField="Code" BackColor="#eff6fc">
                                    <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                                    <asp:ListItem Text="National Commercial Bank (NCB)" Value="National Commercial Bank (NCB)"></asp:ListItem>
                                    <asp:ListItem Text="Riyadh Bank" Value="Riyadh Bank"></asp:ListItem>
                                    <asp:ListItem Text="Arab National Bank (ANB)" Value="Arab National Bank (ANB)"></asp:ListItem>
                                    <asp:ListItem Text="Saudi British Bank (SABB)" Value="Saudi British Bank (SABB)"></asp:ListItem>
                                    <asp:ListItem Text="SAMBA Bank" Value="SAMBA Bank"></asp:ListItem>
                                    <asp:ListItem Text="Banque Saudi Fransi" Value="Banque Saudi Fransi"></asp:ListItem>
                                    <asp:ListItem Text="Saudi Hollandi Bank" Value="Saudi Hollandi Bank"></asp:ListItem>
                                    <asp:ListItem Text="Bank Al Jazira" Value="Bank Al Jazira"></asp:ListItem>
                                    <asp:ListItem Text="Saudi Investment Bank" Value="Saudi Investment Bank"></asp:ListItem>
                                    <asp:ListItem Text="Al Rajhi Bank" Value="Al Rajhi Bank"></asp:ListItem>
                                    <asp:ListItem Text="Al Bilad Bank" Value="Al Bilad Bank"></asp:ListItem>
                                    <asp:ListItem Text="Islamic Development Bank" Value="Islamic Development Bank"></asp:ListItem>
                                    <asp:ListItem Text="Alinma Bank" Value="Alinma Bank"></asp:ListItem>
                                    <asp:ListItem Text="Emirates Bank" Value="Emirates Bank"></asp:ListItem>
                                    <asp:ListItem Text="Gulf International Bank" Value="Gulf International Bank"></asp:ListItem>
                                    <asp:ListItem Text="National Bank of Bahrain" Value="National Bank of Bahrain"></asp:ListItem>
                                    <asp:ListItem Text="National Bank of Kuwait" Value="National Bank of Kuwait"></asp:ListItem>
                                    <asp:ListItem Text="Bank Muscat" Value="Bank Muscat"></asp:ListItem>
                                    <asp:ListItem Text="Deutsche Bank" Value="Deutsche Bank"></asp:ListItem>
                                </asp:DropDownList>--%>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv_ddlBankName" runat="server" ControlToValidate="txtBankName" InitialValue=""
                                SetFocusOnError="true"
                                Display="Dynamic" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div id="divIBAN" class="containerClass showForBankTransfer">
                        <div class="smallLabelClass">
                            <span style="color: #ff3300">
                                <strong>*</strong>
                            </span>
                            <asp:Label ID="Label11" runat="server" Font-Bold="False" ForeColor="#000036" Text="IBAN Number"></asp:Label>
                        </div>
                        <div class="controlClass">
                            <div class="input">
                                <asp:TextBox CssClass="textbox" ID="txtIBANNumber" runat="server" MaxLength="24"></asp:TextBox>
                                <span class="tooltip">Please note that IBAN details should be for the main member and not for the dependents.<br />
                                    <span style="direction: rtl; padding-left:200px;">الرجاء ملاحظة ان رقم الآيبان يجب ان يكون عائد للموظف نفسه وليس للتابعين</span>
                                </span>
                            </div>
                            <asp:RegularExpressionValidator ID="revIBAN" runat="server"
                                ControlToValidate="txtIBANNumber" Display="Dynamic"
                                ValidationExpression="^[sS]{1}[aA]{1}[0-9]{11}[A-Za-z0-9]{1}[0-9]{10}$" ValidationGroup="1"
                                Width="116px"
                                SetFocusOnError="true">Incorrect IBAN No.</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfvIBAN" runat="server" ControlToValidate="txtIBANNumber" InitialValue="" SetFocusOnError="true"
                                Display="Dynamic" ValidationGroup="1" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                            <%-- SaudiIBAN="^SA\d{4}[0-9A-Z]{18}$"--%>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            </span>
        </ContentTemplate>
    </asp:UpdatePanel>

    <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
        <tr>
            <td style="background-color: #ffffff; height: 25px;" valign="top">
                <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload invoices and supportive documents</span></strong>
            </td>
        </tr>
    </table>
    <div style="width: 660px;">
        <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .bmp) 5MB limit per file" /><br />
        <uc1:Uploader runat="server" ID="uploader" />
        <asp:TextBox ID="txtFileUploadNo" runat="server" Width="1px" Height="1px" ForeColor="White" BorderColor="White"></asp:TextBox>

        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFileUploadNo" SetFocusOnError="true"
            Display="Dynamic" ValidationGroup="1" Text="You have to upload the supportive document. please choose the file and click upload"></asp:RequiredFieldValidator>
    </div>
    <br />
    <br />
    <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
        <tr>
            <td style="background-color: #ffffff; height: 25px;" valign="top">
                <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Declaration</span></strong>
            </td>
        </tr>
    </table>

    <div style="float: left; width: 400px;">
        I, the undersigned, declare that the information above is correct and that
         reimbursement requested is for expenses paid by me for the treatment of
         my covered condition. And I hereby authorize Bupa Arabia to pay the
         eligible expenses directly to the policyholder and in local currency (SR). 
        <br />
        <br />
        I hereby authorize any Doctor, Hospital, Clinic or Medical Provider, any 
        Insurance Company or any other Company, Institution or any other person
         who has any record or information about me and /or any of my family
         members to provide Bupa Arabia with the complete information, 	
        including copies of their record with reference to any sickness or accident,
         any treatment, examination, advice or hospitalization or any other
         information required by Bupa Arabia.
        <br />
        <br />
        I am fully aware that any person who intentionally makes any false and/or
         misleading statement and/or information to obtain reimbursement from 
        Bupa Arabia subject to penalization.
    <br />
        <br />
    </div>
    <div style="float: right; width: 400px; direction: rtl; font-size: 15px;">
        أقر أنا الموقع أدناه أن المعلومات المذكورة أعلاه صحيحة وأن التعويض المطلوب هو نظير نفقات دفعت من قبلي لغرض علاج حالتي المغطاة. وأنا بذلك أوافق أن تدفع شركة بوبا العربية النفقات المستحقه مباشرة وبالعملة المحليه 
( الريال السعودي ).
        <br />
        <br />
        أن أصرح لأي مستشفى، عياده أو مقدم خدمة طبية وأي شركة تأمين أو شركة أخرى، مؤسسة أو أي شخص أخر لديه سجلات أو معلومات عني و/ أو فرد من عائلتي باأن يزود شركة بوبا العربيه بالمعلومات الكاملة والتي تشملنسخا عن سجلات مع الإشارة إلى أي علة أو حادث، أي علاج، فحص، نصيحة طبية أو مكوث في المستشفى أو أي معلومة أخرى تطلبها شركة بوبا العربية.
        <br />
        <br />
        أنا على دراية تامة باأن أي شخص يقوم- متعمدا- بتقديم مستندات و/أو معلومات خاطئة و/ أو مضلله لهدف تعويض من بوبا العربية هو عرضه للمحاسبة.
    <br />
        <br />
    </div>
    <div class="clear"></div>
    <div style="float: left; width: 400px;">
        <asp:CheckBox runat="server" ID="chkAgree" />
        I have read and agreed to bupa terms and conditions
        <br />
        <br />
        <asp:CustomValidator ID="cv1" runat="server" ValidationGroup="1" ClientValidationFunction="CheckAgree" SetFocusOnError="true"
            ErrorMessage="You must agree to the terms and conditions in order to proceed" Display="dynamic" ForeColor="red"></asp:CustomValidator>
    </div>
    <div style="float: right; width: 400px; direction: rtl; font-size: 15px;">
        لقد قرأت ووافقت على شروط وأحكام بوبا
    <br />
        <br />
        <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="1" ClientValidationFunction="CheckAgree" SetFocusOnError="true"
            ErrorMessage="يجب الموافقة على الشروط والأحكام" Display="dynamic" ForeColor="red" Font-Bold="True" Font-Size="13px"></asp:CustomValidator>
    </div>
    <div class="clear"></div>
    <br />
    <br />
    <div style="float: right;">
        <asp:Button CssClass="submitButton" ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="1" BackColor="Control" OnClick="btnSubmit_Click" />
    </div>
    <div class="clear"></div>
</asp:Panel>