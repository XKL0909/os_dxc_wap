﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Apps" Codebehind="Apps.ascx.cs" %>
<div class="margBottom25">
                        <div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td ><h2 class="dahsboardTitle">Tools & Apps</h2></td>
                            </tr>
                        </table>
                        </div>
                        <div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="toolsandapps dashboardsAppsList">
                                <ul class="toolsListing">

                                <asp:DataList ID="DataList1" runat="server" Width="100%" 
                         RepeatColumns="3" 
                        RepeatDirection="Horizontal">
                <ItemTemplate>
                     <li><div class="toolsListingPad">
                                    <div class="toolsTitle"><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Bind("link") %>' Text="" ToolTip='<%# Bind("fulltitle") %>'><asp:Label ID="lblSize" runat="server" Text='<%# Bind("title") %>'></asp:Label></asp:HyperLink></div>
                                    
                                    <div class="toolsPic"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="toolsPiContainer"><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Bind("link") %>' Text="">
                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("imageURL") %>' ToolTip='<%# Bind("fulltitle") %>' /></asp:HyperLink></td></tr></table></div>
                                    <div class="toolsLink"><asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl='<%# Bind("link") %>' Text="">Go</asp:HyperLink></div>
                                </div></li>
                </ItemTemplate>
                <SelectedItemStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            </asp:DataList>

                               
                                </ul></td>
                            </tr>
                        </table>
                        </div>
                    </div>

                    <asp:XmlDataSource ID="XmlDataSourceTips" runat="server"></asp:XmlDataSource>