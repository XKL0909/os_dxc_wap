﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.Data;

public partial class UserControls_Tips : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        postRequest("test", "http://www.bupa.com.sa/_layouts/Bupa/GenerateXML.aspx?xmldata=tips ");

       
     }
   

    private void postRequest(String RequestXML, string ServerURL) 
    {
        int timeout = 90000; 
        int connectionLimit = 10; 
        string responseXML = string.Empty; 
        try 
        { 
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(ServerURL);
            webRequest.Timeout = timeout;
            webRequest.ServicePoint.ConnectionLimit = connectionLimit;
            webRequest.Method = "POST"; 
            webRequest.ContentType = "text/xml"; byte[] byteArray = Encoding.UTF8.GetBytes(RequestXML);
            Stream strm = webRequest.GetRequestStream();
            strm.Write(byteArray, 0, byteArray.Length); strm.Close();
            WebResponse webResponse = webRequest.GetResponse();
            Encoding enc = Encoding.GetEncoding("utf-8");
            StreamReader reader = new StreamReader(webResponse.GetResponseStream(), enc);
            
            DataSet ds = new DataSet();
            ds.ReadXml(reader);

            //ds.Tables[0].AsEnumerable().Take(5);
            DataTable dt = new DataTable();
            dt.Columns.Add("title");
            dt.Columns.Add("description");
            dt.Columns.Add("date");
            int i = 0;
            foreach (DataRow _row in ds.Tables[0].Rows)
            {
                i = i + 1;
                if(i <= 1)
                    dt.ImportRow(_row);
            }
            DataList1.DataSource = dt;
            DataList1.DataBind();
            responseXML = reader.ReadToEnd();
            reader.Close();
            webResponse.Close();
        }
        catch (Exception ex) 
        { 
            XmlDocument doc = new XmlDocument(); 
            doc.LoadXml("<HTTPPostError/>"); 
            doc.DocumentElement.InnerText = ex.Message; responseXML = doc.DocumentElement.OuterXml;
        } 
        
    }

   

    protected void DataList1_ItemCreated(object sender, DataListItemEventArgs e)
    {

        
          
            if (e.Item.ItemIndex >= 5)
            {
                e.Item.Visible = false;
            }
       
    }
}