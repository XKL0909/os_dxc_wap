﻿using System;
using System.Linq;
using System.Web.UI;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Bupa.OSWeb.Business;

public partial class UserControls_DashBoardOSNav : System.Web.UI.UserControl
{
    private OS_DXC_WAP.avayaWS.Service1 avaya;
    public bool _Warning = false;
    private string _strStatus = "";
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    protected void Page_Load(object sender, EventArgs e)
    {
		 //Aramco PID Changes By Hussamuddin
        string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
       {
           Client.Visible = false;
           AramcoClient.Visible = true;
           Member.Visible = false;
		   AramcoCodeType.Value = Convert.ToString(Session["ContractType"]).ToUpper();

            aAInvoice.HRef = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Invoice");
            aAStatement.HRef = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Statement");
            A6.HRef = "../client/changebranch.aspx?val=" + Cryption.Encrypt("optiontype=Class");
        }
       else
       {
           AramcoClient.Visible = false;

            delDep.HRef = "../client/DeleteEmployeeEnhanced.aspx?val=" + Cryption.Encrypt("EType=Dep");
            chClass.HRef = "../client/changebranch.aspx?val=" + Cryption.Encrypt("optiontype=Class");
            aInvoice.HRef = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Invoice");
            aStatement.HRef = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Statement");
           

            //Aramco PID Changes By Hussamuddin
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
			{
				//CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
				if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() == "Y") || (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP"))
					//Enable the Reimbursement Claim Submission
					ReimbursementForm.Visible = true;
				else
				{

					MemberHelper MyMemberH = new MemberHelper(Session["MembershipNo"].ToString(), "");
					Member MyMember = MyMemberH.GetMemberDetails();

					ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
					OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN request;
					OS_DXC_WAP.CaesarWS.EnqContInfoResponse_DN response;
					request = new OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN();

					request.contNo = Convert.ToString(MyMember.ContractNumber);
					request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
					request.Username = WebPublication.CaesarSvcUsername;
					request.Password = WebPublication.CaesarSvcPassword;

					response = ws.EnqContInfo(request);
					//CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
					if (response.onl_Reimb_Ind == "Y")
					{
						ReimbursementForm.Visible = true;
					}
					else
					{
						ReimbursementForm.Visible = false;
					}

				}


			}

            clientUserManagement.Visible = (Session["SuperUser"] != null && Convert.ToString(Session["SuperUser"]).ToLower().Contains("true"));
                        
			if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
			{
				Member.Visible = true;
				Client.Visible = false;
				Provider.Visible = false;
			}
			if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
			{

				//CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
				if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() == "Y") || (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP"))
					//Enable the Reimbursement Claim Submission
					ReimbursementFormClient.Visible = true;
				else
				{
					ReimbursementFormClient.Visible = false;
				}

				Member.Visible = false;
				Client.Visible = true;
				Provider.Visible = false;
				if (Convert.ToString(Session["ClientID"]).Trim() == "10436900" || Convert.ToString(Session["ClientID"]).Trim() == "11022800" || Convert.ToString(Session["ClientID"]).Trim() == "12381400")
				{
					MIS.Visible = true;
				}
				
				tdSMERenewal.Visible = WebApp.CommonClass.IsEnableSMERenewal(Convert.ToString(Session["ClientID"]));

                OS_DXC_WAP.avayaWS.Service1 _avaya = new OS_DXC_WAP.avayaWS.Service1();
				//// CR Deletion should enable all senario by Sakthi
				_strStatus = _avaya.checkContractStatus(Convert.ToString(Session["ClientID"]));
				Session["WarningEnable"] = _strStatus;
                addMemberVideo.Attributes.Add("onclick", "DisplayVideoOption()");

                if (_strStatus.Trim().ToLower() == "warning")
				{
					addEmp.HRef = "javascript:void()";
					addEmp.Attributes.Add("onclick", "SystemMesage()");

					adddep.HRef = "javascript:void()";
					adddep.Attributes.Add("onclick", "SystemMesage()");

                    //chClass.HRef = "../client/changebranch.aspx?optiontype=" + Cryption.Encrypt("Class");
                    //chClass.Attributes.Add("href", "../client/changebranch.aspx?optiontype=" + Cryption.Encrypt("Class"));
                    chClass.HRef = "javascript:void()";
                    chClass.Attributes.Add("onclick", "SystemMesage()");

					chBranch.HRef = "javascript:void()";
					chBranch.Attributes.Add("onclick", "SystemMesage()");
				}
			}
			if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
			{
				Member.Visible = false;
				Client.Visible = false;
				Provider.Visible = true;

				DataSet _ProPer = GetProviderPermission(Convert.ToString(Session["ProviderUserName"]));
				foreach (DataRow dr in _ProPer.Tables[0].Rows)
				{
					if (!string.IsNullOrEmpty(Convert.ToString(dr["NewRequest"])))
						Option1.Visible = Convert.ToBoolean(dr["NewRequest"]);
					if (!string.IsNullOrEmpty(Convert.ToString(dr["ExistingRequest"])))
						Option2.Visible = Convert.ToBoolean(dr["ExistingRequest"]);
					if (!string.IsNullOrEmpty(Convert.ToString(dr["CheckNewReply"])))
						Option3.Visible = Convert.ToBoolean(dr["CheckNewReply"]);
					if (!string.IsNullOrEmpty(Convert.ToString(dr["TableOfBenefits"])))
						Option4.Visible = Convert.ToBoolean(dr["TableOfBenefits"]);
					if (!string.IsNullOrEmpty(Convert.ToString(dr["EClaimsSubmission"])))
						Option5.Visible = Convert.ToBoolean(dr["EClaimsSubmission"]);
					if (!string.IsNullOrEmpty(Convert.ToString(dr["Verification"])))
						Option6.Visible = Convert.ToBoolean(dr["Verification"]);
					if (!string.IsNullOrEmpty(Convert.ToString(dr["ServiceCode"])))
						Option7.Visible = Convert.ToBoolean(dr["ServiceCode"]);

					using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
					{
						string ProviderId = Convert.ToString(Session["ProviderID"]);
						ProviderPhysicianMaster objProviderPhysicianMaster = objTruestedDoctors.ProviderPhysicianMasters.Where(item => item.PM_ProviderId.Trim() == ProviderId).SingleOrDefault();
						if (objProviderPhysicianMaster != null && objProviderPhysicianMaster.PM_MaintenanceRequired == true && objProviderPhysicianMaster.PM_Status==true)
					   {
						   Option8.Visible = true;
					   }
					}
				}
				Option1.Visible = true;
				Option2.Visible = true;
				Option3.Visible = true;
				Option4.Visible = true;
				Option5.Visible = true;
				Option6.Visible = true;
				Option7.Visible = true;
				Option9.Visible = true;

				//// CR-353 Provider statement
				if (Session["StmInd"] != null && Convert.ToString(Session["StmInd"]) == "Y")
				{
					imgProviderStatmentIndicator.Src = "~/images/ico/common/AccountStatementIndicator.png";
					imgProviderStatmentIndicator.Height = 64;
					imgProviderStatmentIndicator.Width = 64;
				}
				else
					imgProviderStatmentIndicator.Src = "~/images/ico/common/AccountStatement.jpg";

                providerUserManagement.Visible = (Session["SuperUser"] != null && Convert.ToString(Session["SuperUser"]).ToLower().Contains("true"));

               
            }
       }

    }

    protected DataSet GetProviderPermission(string userName)
    {
        DataSet ds = new DataSet();


        try
        {



            /*DataParameter UserName = new DataParameter(SqlDbType.NVarChar, "@proUsername", userName, ParameterDirection.Input, 30);

            DataParameter[] dpBasket = new DataParameter[] { UserName };
            
            ds = DataManager.ExecuteStoredProcedureCachedReturn(WebPublication.Key_ConnBupa_OS, "spGetProviderPermission", ref dpBasket, 60000, true);
            */
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            SqlCommand cmd = new SqlCommand("spGetProviderPermission", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@proUsername", userName);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

        }
        catch (Exception ex)
        {

            throw;
        }

        return ds;
    }


    protected void btnDiabetes_Click(object sender, ImageClickEventArgs e)
    {

    }
}