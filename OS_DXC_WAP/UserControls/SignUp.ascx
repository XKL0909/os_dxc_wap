﻿<%@ Control Language="C#" Inherits="UserControls_SignUp"  Codebehind="SignUp.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>


<style type="text/css">
    .style1 {
        margin-left: 80px;
    }

    .style2 {
    }

    .style3 {
        width: 30px;
    }

    .style4 {
        font-size: 13px;
        color: #558ED5;
    }

    .style5 {
        width: 53%;
    }

    .auto-style1 {
        width: 229px;
    }
</style>
<style type="text/css">
    /* Accordion */
    .accordionHeader {
        border: 1px solid #2F4F4F;
        color: white;
        background-color: #2E4d7B;
        font-family: Arial, Sans-Serif;
        font-size: 12px;
        font-weight: bold;
        padding: 5px;
        margin-top: 5px;
        cursor: pointer;
    }

    #master_content .accordionHeader a {
        color: white;
        background: none;
        text-decoration: none;
    }

        #master_content .accordionHeader a:hover {
            background: none;
            text-decoration: underline;
        }

    .accordionHeaderSelected {
        border: 1px solid #2F4F4F;
        color: white;
        background-color: #5078B3;
        font-family: Arial, Sans-Serif;
        font-size: 12px;
        font-weight: bold;
        padding: 5px;
        margin-top: 5px;
        cursor: pointer;
    }

    #master_content .accordionHeaderSelected a {
        color: #FFFFFF;
        background: none;
        text-decoration: none;
    }

        #master_content .accordionHeaderSelected a:hover {
            background: none;
            text-decoration: underline;
        }

    .accordionContent {
        background-color: #D3DEEF;
        border: 1px dashed #2F4F4F;
        border-top: none;
        padding: 5px;
        padding-top: 10px;
    }

    /* Content Page Layout */
    .demoarea {
        padding: 20px;
        background: #FFF url(images/demotop.png) no-repeat left top;
    }

        .demoarea p {
            padding: 5px;
        }

    .demoheading {
        padding-bottom: 20px;
        color: #5377A9;
        font-family: Arial, Sans-Serif;
        font-weight: bold;
        font-size: 1.5em;
    }

    .demobottom {
        height: 8px;
        font-size: 10px;
        background: #FFF url(images/demobottom.png) no-repeat left bottom;
    }

    .accordionHeader a {
        color: #FFFFFF;
        background: none;
        text-decoration: none;
    }

    a.accordionLink {
        color: white;
    }
</style>

<script type="text/javascript">
		function HideCaptchaMessage()
		{
		 document.getElementById('<%= WrongCaptcha.ClientID %>').style.visibility = "hidden";
        }
   // to allow numbers only in the numbers text boxes
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.]")
        if (key == 8) {
            keychar = String.fromCharCode(key);
        }
        if (key == 11) {
            key = 8;
            keychar = String.fromCharCode(key);
        }
        return reg.test(keychar);
    }
	    
   </script>
<script>
    function PrintContent() {
        var DocumentContainer = document.getElementById('AgreementDiv');
        var WindowObject = window.open(",'PrintWindow','width=3,height=3,top=3,left=1,toolbars=no,scrollbars=yes,status=no,resizable=no,'");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        //WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }



    function validateAgreement() {
        var textbox1 = document.getElementById("btnSubmit");
        if (document.getElementById("chkAgreement").checked == true) {
            //alert();
            textbox1.disabled = false;
            //document.getElementById("AgreementBtn").enabled = true;
        }
        else {
            textbox1.disabled = true;
        }
    }
</script>
<script type="text/javascript">
    function ValidateCheckBox(sender, args) {
        if (document.getElementById("<%=CheckBox1.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            document.getElementById("<%=CheckBox1.ClientID %>").focus();
            args.IsValid = false;
        }
    }
</script>

<div class="memberLoginComp" id="entryForm">
    
    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%" style="border: 0">
                <tr>
                    <td valign="top">
                        <table width="100%" style="border: 0">
                            <tr>
                                <td>
                                    <table width="100%" style="border: 0">
                                        <tr>
                                            <td style="width: 187px" align="left">
                                                <h1>Register as :</h1>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="UserTyperbList" runat="server"
                                                    RepeatDirection="Horizontal"
                                                    AutoPostBack="true"
                                                    OnSelectedIndexChanged="UserTyperbList_SelectedIndexChanged">
                                                    <asp:ListItem Text="Individual" Value="1" Group="OsType"></asp:ListItem>
                                                    <asp:ListItem Text="Healthcare Provider" Value="3" Group="OsType" Selected="True"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <table width="100%" style="border: 0">
                                        <tr>
                                            <td style="width: 75%" valign="top">
                                                <table width="100%" style="border: 0; vertical-align: top">
                                                    <tr>
                                                        <td valign="top" align="center" class="style5">

                                                            <dx:ASPxLabel ID="lblError" runat="server" Text="">
                                                            </dx:ASPxLabel>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td runat="server" id="tblMember" visible="true" valign="top" class="style5">

                                                            <table style="border: 0">
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Membership No.:</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtMembership" runat="server" Width="90px" MaxLength="8" BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMembership" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMembership" ErrorMessage="Only Numbers" ValidationExpression="\d{7}">*</asp:RegularExpressionValidator>--%>
                                                                                </td>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Saudi Id/ Iqama No.:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtChkSaudiIdIqamaNo" runat="server" Width="120px" MaxLength="10" BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtChkSaudiIdIqamaNo" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxButton ID="btnGetDetails" runat="server"
                                                                                        OnClick="btnGetDetails_Click" Text="Get Details">
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="style2" valign="top" colspan="2">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="style2" valign="top" colspan="2">
                                                                        <table width="100%" id="tblInfo" runat="server" visible="false" frame="box">
                                                                            <tr>
                                                                                <td valign="top" colspan="2" style="text-align: center; font-size: 14px;">
                                                                                    <u><b>Member Information</b></u>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td colspan="2"><b>&nbsp;&nbsp;&nbsp;Your Current Status</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style3">&nbsp;&nbsp;&nbsp;<strong>Name:</strong></td>
                                                                                            <td>
                                                                                                <dx:ASPxLabel ID="lblMemberName" runat="server"
                                                                                                    Text="">
                                                                                                </dx:ASPxLabel>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style3">&nbsp;&nbsp;&nbsp;<strong>Status:</strong></td>
                                                                                            <td>
                                                                                                <dx:ASPxLabel ID="lblStatus" runat="server"
                                                                                                    Text="">
                                                                                                </dx:ASPxLabel>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2"></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td>
                                                                                    <table>

                                                                                        <tr>
                                                                                            <td colspan="2">&nbsp;&nbsp;&nbsp;<b>Policy Information</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style3">&nbsp;&nbsp;&nbsp;<strong>Contract:</strong></td>
                                                                                            <td>
                                                                                                <dx:ASPxLabel ID="lblContract" runat="server"
                                                                                                    Text="">
                                                                                                </dx:ASPxLabel>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style3">&nbsp;&nbsp;&nbsp;<strong>Customer:</strong>
                                                                                            </td>
                                                                                            <td>
                                                                                                <dx:ASPxLabel ID="lblCustomerName" runat="server"
                                                                                                    Text="">
                                                                                                </dx:ASPxLabel>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>

                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td class="style2" valign="top" colspan="2">&nbsp;</td>
                                                                </tr>
                                                                <tr runat="server" id="r1" visible="false">
                                                                    <td class="auto-style1">Saudi ID / Iqama <strong style="color: red">*</strong></td>
                                                                    <td class="style1">
                                                                        <asp:HiddenField ID="hidIdentityNo" runat="server" />
                                                                        <asp:TextBox ID="txtID" runat="server" Width="200px" MaxLength="10"
                                                                            BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvTxtID" Display="Dynamic" runat="server" ControlToValidate="txtID" ErrorMessage="* Required Field" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                        <asp:CompareValidator runat="server" ID="cv_Identity"
                                                                            ErrorMessage="You Identity/Iqama No is not matching our records. Please correct"
                                                                            ControlToValidate="txtID" Type="String" Operator="Equal" Display="Dynamic"
                                                                            SetFocusOnError="true" />
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="r2" visible="false">
                                                                    <td class="auto-style1">Number of active dependents <strong style="color: red">*</strong></td>
                                                                    <td class="style1">

                                                                        <asp:TextBox ID="txtDependent" runat="server" Width="200px" MaxLength="2"
                                                                            BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfv_txtDependent" Display="Dynamic" runat="server" ControlToValidate="txtDependent" ErrorMessage="* Required Field" SetFocusOnError="true"></asp:RequiredFieldValidator>


                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="r3" visible="false">
                                                                    <td class="auto-style1">Email Address <strong style="color: red">*</strong></td>
                                                                    <td class="style1">

                                                                        <asp:TextBox ID="txtMemberEmail" runat="server" Width="200px" MaxLength="50"
                                                                            BackColor="#eff6fc"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfv_txtMemberEmail" runat="server" ControlToValidate="txtMemberEmail" ErrorMessage="* Required Field" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>

                                                                        <asp:RegularExpressionValidator ID="rev_txtMemberEmail" runat="server" ErrorMessage="Not valid email address" ControlToValidate="txtMemberEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true"></asp:RegularExpressionValidator>

                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="r4" visible="false">
                                                                    <td class="auto-style1">Mobile <span style="font-size: 10px; font-weight: bold;">(9665xxxxxxxx)</span> <strong style="color: red">*</strong></td>
                                                                    <td class="style1">

                                                                        <asp:TextBox ID="txtMobile" runat="server" Width="200px" MaxLength="14"
                                                                            BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfv_txtMobile" runat="server" ControlToValidate="txtMobile" ErrorMessage="* Required Field" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>



                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="r5" visible="false">
                                                                    <td class="auto-style1">Password <strong style="color: red">*</strong></td>
                                                                    <td class="style1">
                                                                        <asp:TextBox ID="txtPassword" runat="server" Width="200px" MaxLength="20" TextMode="Password"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtPassword" ErrorMessage="* Required Field" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="r6" visible="false">
                                                                    <td class="auto-style1">Retype password <strong style="color: red">*</strong></td>
                                                                    <td class="style1">
                                                                        <asp:TextBox ID="txtrePassword" runat="server" Width="200px" Password="true" TextMode="Password"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfv_txtrePassword" runat="server" ControlToValidate="txtrePassword" ErrorMessage="* Required Field" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator" ControlToCompare="txtPassword" Text="Password mismatch" ControlToValidate="txtrePassword" SetFocusOnError="true" Display="Dynamic"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="healthcareProfile" visible="false">
                                                                    <td style="width: 100%" colspan="2">

                                                                        <div class="demoarea">

                                                                            <div class="demoheading">
                                                                                <asp:Label ID="lblHealthcareProfile" runat="server" Text="Healthcare Profile" Visible="false"></asp:Label>
                                                                            </div>
                                                                            <ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0"
                                                                                HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                                                                                ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
                                                                                TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true" Visible="false">
                                                                                <Panes>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader1" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>

                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic1" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant1" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant1" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered1" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 3 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered1" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader2" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile2" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic2" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant2" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant2" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered2" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 3 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered2" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader3" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile3" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic3" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant3" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant3" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered3" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 3 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered3" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader4" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile4" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic4" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant4" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant4" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered4" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 4 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered4" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane5" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader5" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile5" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic5" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant5" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant5" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered5" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 5 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered5" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane6" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader6" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile6" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic6" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant6" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant6" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered6" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 6 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered6" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane7" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader7" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile7" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic7" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant7" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant7" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered7" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 7 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered7" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane8" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader8" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile8" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic8" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant8" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant8" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered8" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 8 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered8" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane9" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader9" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile9" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic9" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant9" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant9" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered9" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 9 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered9" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane10" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader10" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile10" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic10" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant10" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant10" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered10" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 10 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered10" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane11" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader11" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile11" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic11" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant11" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant11" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered11" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 11 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered11" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane12" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader12" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile12" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic12" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant12" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant12" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered12" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 12 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered12" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane13" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader13" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile13" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic13" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant13" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant13" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered13" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 13 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered13" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane14" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader14" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile14" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic14" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant14" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant14" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered14" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 14 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered14" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>
                                                                                    <ajaxToolkit:AccordionPane ID="AccordionPane15" runat="server" Visible="false">
                                                                                        <Header>
                                                                                            <a href="" class="accordionLink">
                                                                                                <asp:Label ID="lblHeader15" runat="server" Text="" /></a>
                                                                                        </Header>
                                                                                        <Content>
                                                                                            Mobile No:
                                                                                    <asp:TextBox ID="txtMobile15" runat="server"></asp:TextBox><br />
                                                                                            <br />
                                                                                            <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                            <asp:CheckBoxList ID="cblChronic15" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                            <asp:Label ID="lblPregnant15" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnPregnant15" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:Label ID="lblDelevered15" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 15 months?"></asp:Label>

                                                                                            <asp:RadioButtonList ID="rdnDelivered15" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                                <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </Content>
                                                                                    </ajaxToolkit:AccordionPane>

                                                                                </Panes>
                                                                            </ajaxToolkit:Accordion>
                                                                        </div>

                                                                        <br />
                                                                        <br />


                                                                    </td>

                                                                </tr>
                                                                <tr runat="server" id="agreement" visible="false">
                                                                    <td class="style2" colspan="2">
                                                                        <table style="font-size: small;">

                                                                            <tr>
                                                                                <td style="width:10px">
                                                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <a href="term.htm" target="_blank">Terms and condition</a> was read and accepted by member through Bupa Arabia’s website
                                                                            <br />
                                                                                    تمت قراءة هذه <a href="term.htm" target="_blank">الشروط والاحكام</a> وتمت الموافقه عليها من خلال موقع بوبا العربية الالكتروني
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="You should accept terms and conditions"
                                                                                        ClientValidationFunction="ValidateCheckBox">
                                                                                    </asp:CustomValidator><br />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td runat="server" id="tblClient" visible="false" valign="top" class="style5">
                                                            <table  style="border: 0;width:100%">
                                                                <tr>
                                                                    <td style="width: 300px">Customer Name</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtCustomerName" runat="server" Width="90%"
                                                                            BackColor="#eff6fc">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtCustomerName" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Contract No.</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtContract" runat="server" Width="90%" MaxLength="8"
                                                                            BackColor="#eff6fc">
                                                                           <%-- <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RegularExpression ErrorText="Incorrect Contract number" ValidationExpression="^[0-9]*$" />
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ControlToValidate="txtContract"  ErrorMessage="Invalid email address." ValidationExpression="^[0-9]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtContract" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Group Secretary Name</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtGroupSecretaryName" runat="server" Width="90%"
                                                                            BackColor="#eff6fc">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtGroupSecretaryName" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Email Address</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtGroupEmail" runat="server" Width="90%"
                                                                            BackColor="#eff6fc">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RegularExpression ErrorText="Invalid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ControlToValidate="txtGroupEmail"  ErrorMessage="Invalid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtGroupEmail" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Telephone Number</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtTel" runat="server" Width="90%" MaxLength="10"
                                                                            BackColor="#eff6fc">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RegularExpression ErrorText="Incorrect Tel Number" ValidationExpression="^[0-9]*$" />
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ControlToValidate="txtTel"  ErrorMessage="Incorrect Tel Number" ValidationExpression="^[0-9]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtTel" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Fax Number</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtFax" runat="server" Width="90%" MaxLength="10"
                                                                            BackColor="#eff6fc">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RegularExpression ErrorText="Incorrect Fax Numbert" ValidationExpression="^[0-9]*$" />
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ControlToValidate="txtFax"  ErrorMessage="Incorrect Fax Numbert" ValidationExpression="^[0-9]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtFax" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td runat="server" id="tblProvider" visible="false" valign="top" class="style5">
                                                            <table  style="border: 0;width:95%">
                                                                <tr>
                                                                    <td style="width: 300px">Name of requester</td>
                                                                    <td colspan="3">
                                                                        <dx:ASPxTextBox ID="txtRequesterName" runat="server" Width="100%"
                                                                            BackColor="#eff6fc" MaxLength="100">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>																																																								
                                                                        </dx:ASPxTextBox>
																		<asp:RegularExpressionValidator ID="rfvRequesterName" runat="server" ErrorMessage="Please enter only alphanumeric." ControlToValidate="txtRequesterName" ValidationExpression="^[0-9a-zA-Z ]+$"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtRequesterName" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Provider name</td>
                                                                    <td style="width: 442px">

                                                                        <dx:ASPxComboBox ID="txtProviderName" runat="server" CssClass="osWebListField"
                                                                            DataSourceID="SqlDataSource1" TextField="ProName" ValueField="proID"
                                                                            AutoResizeWithContainer="True" IncrementalFilteringMode="StartsWith"
                                                                            BackColor="#eff6fc" EnableIncrementalFiltering="True">
                                                                            <%--<ValidationSettings RequiredField-IsRequired="true" ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RequiredField IsRequired="True" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxComboBox>
                                                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                                                                            ConnectionString="<%$ ConnectionStrings:ConnBupa_OS %>"
                                                                            SelectCommand="SELECT DISTINCT proID, ProName FROM Provider WHERE (proActive = 1) ORDER BY ProName"></asp:SqlDataSource>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtProviderName" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 262px">Fax number</td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtProviderFax" runat="server" Width="130px" MaxLength="15"
                                                                            BackColor="#eff6fc">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RegularExpression ErrorText="Incorrect Fax Numbert" ValidationExpression="^[0-9]*$"  />
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ControlToValidate="txtProviderFax"  ErrorMessage="Incorrect Fax Numbert" ValidationExpression="^[0-9]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtProviderFax" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Name of Employee</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtEmployeeName" runat="server" Width="130px"
                                                                            BackColor="#eff6fc" MaxLength="100">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="true" ValidationGroup="Submit">
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtEmployeeName" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
																		<asp:RegularExpressionValidator ID="rfvEmpName" runat="server" ErrorMessage="Please enter only alphabets." ControlToValidate="txtEmployeeName" ValidationExpression="^[a-zA-Z ]+$"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td style="width: 262px">Current Position</td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtCurrentPosition" runat="server" Width="130px"
                                                                            BackColor="#eff6fc">
                                                                           <%-- <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtCurrentPosition" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Login username</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtUserName" runat="server" Width="130px" BackColor="#eff6fc">
                                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtUserName" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 262px">Telephone No</td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtProviderTel" runat="server" Width="130px" MaxLength="15" BackColor="#eff6fc">
                                                                           <%-- <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RegularExpression ErrorText="Incorrect Tel Numbert" ValidationExpression="^[0-9]*$" />
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>
                                                                        </dx:ASPxTextBox>
                                                                        <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ControlToValidate="txtProviderTel"  ErrorMessage="Incorrect Tel Numbert" ValidationExpression="^[0-9]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtProviderTel" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">Email Address</td>
                                                                    <td style="width: 442px">
                                                                        <dx:ASPxTextBox ID="txtProviderEmail" runat="server" Width="130px" BackColor="#eff6fc" ValidationGroup="Submit">
                                                                           <%-- <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                                                <RegularExpression ErrorText="Invalid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                                                <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                            </ValidationSettings>--%>

                                                                        </dx:ASPxTextBox>
                                                                        <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ControlToValidate="txtProviderEmail"  ErrorMessage="Invalid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtProviderEmail" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>

                                                                    </td>
                                                                    <td style="width: 262px">Extension</td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtExt" runat="server" Width="130px" MaxLength="5" ValidationGroup="Submit">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 300px">&nbsp;</td>
                                                                    <td style="width: 442px">&nbsp;</td>
                                                                    <td style="width: 262px">Mobile No</td>
                                                                    <td>
                                                                         <dx:ASPxTextBox ID="txtProviderMobile" runat="server" Width="130px" MaxLength="10" BackColor="#eff6fc" >
                                                                         </dx:ASPxTextBox>
                                                                        <asp:RegularExpressionValidator runat="server"  Display="Dynamic" ControlToValidate="txtProviderMobile"  ErrorMessage="Incorrect Mobile Numbert" ValidationExpression="^[0-9]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtProviderMobile" Display="Dynamic"  ErrorMessage="Field is required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top" bgcolor="#01aef0" style="color: White; width: 300px">

                                                <table width="100%" cellpadding="8" cellspacing="8" border="0">
                                                    <tr>
                                                        <td>
                                                            <strong>Member</strong> - 
            is a person currently insured under any schemes of Bupa Arabia. He may also be 
            referrend&nbsp; as a policy holder.<br />
                                                            <strong>
                                                                <br />
                                                                Provider</strong> - you may register as our provider if you are an 
            authorized staff from one of Bupa Arabia&#39;s healthcare providers.
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr runat="server" id="uploaderCtl">
                                            <td valign="top" colspan="2">

                                                <table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
                                                    runat="server" id="tblUploader">
                                                    <tr>
                                                        <td align="left">
                                                            <span style="color: #ff3300"><strong>*</strong></span><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
                                                        </td>
                                                        <td align="right">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">

                                                            <asp:FileUpload ID="FileUpload1" runat="server" EnableViewState="true" ViewStateMode="Enabled" /><br />

                                                            <asp:RequiredFieldValidator ID="rfvFileUpload1" runat="server" 
                                                                ErrorMessage="Please selcet file!" ControlToValidate="FileUpload1" ValidationGroup="Submit" ></asp:RequiredFieldValidator>

                                                            <asp:RegularExpressionValidator runat="server" ID="revFileUpload1" ControlToValidate="FileUpload1" 
             ErrorMessage="Image Files Only (.jpg, .bmp, .png, .gif, .pdf)"
             ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.jpeg|.JPEG|.bmp|.BMP|.png|.PNG|.pdf)$" ValidationGroup="Submit" />

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="style4">
                                                            <strong>Please attach official request letter bearing official stamp and 
                    signature from your department </strong>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                                <%--<ucCaptcha:CaptchaImage ID="CaptchaImage"  runat="server"  />--%>
                                                            

                                                            <div id="divCaptchaImage" runat="server">
                                                                <div>
                                                                    <asp:Label ID="lblmsg" runat="server" Font-Bold="True"
                                                                        ForeColor="Red" Text=""></asp:Label>
                                                                    <br />
                                                                </div>
                                                                <span style="color: #ff3300"><strong>*</strong></span><span>Insert Below Text </span>
                                                                <asp:TextBox  ID="txtimgcode" runat="server" onchange="HideCaptchaMessage()"></asp:TextBox><br />
                                                                <asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" ErrorMessage="Field is required" ControlToValidate="txtimgcode" ValidationGroup="Submit"></asp:RequiredFieldValidator>
																<span id="WrongCaptcha" runat="server" style="color: #ff3300; visibility:hidden"><strong>Wrong entry try again</strong></span>
                                                                <br />
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Captcha/CapImage.aspx" />
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                <asp:ImageButton id="CaptchaValidate" runat="server"  Style="width:81px"  OnClick="CaptchaValidate_Click" ImageUrl="../images/icons/reCaptchaImg.jpg">
                                                                                     
                                                                                </asp:ImageButton>
                                                                                
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                <br />

                                                            </div>
                                                                        
                                                        </td>
                                                        <td>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </td>
                                        </tr>

                                        <tr id="Tr1" runat="server">
                                            <td style="width: 75%" valign="top">
                                                <div class="demobottom" runat="server" id="Disclamer" visible="false">
                                                    "Disclaimer: The mobile number will be used by Bupa to contact you for healthcare services only"

                                                </div>
                                            </td>
                                            <td align="center" valign="bottom">
                                                <asp:Button ID="btnSubmit" runat="server" Enabled="False" Height="30px" 
                                                      OnClick="btnSubmit_Click" Text="Submit" Width="120px" CausesValidation="true"  ValidationGroup="Submit">
                                                </asp:Button>
                                                
                                            </td>
                                        </tr>


                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <%--        <tr>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" ValidationGroup="test" />
            </td>
        </tr>--%>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="CaptchaValidate" />
        </Triggers>
    </asp:UpdatePanel>
</div>

