﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_Hospital : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(Convert.ToString(Session["VIP"])))
        {
            if (Convert.ToString(Session["VIP"]) == "VP")
            {
                HA.Visible = true;
            }
            else
            {
                HA.Visible = false;
            }
        }

    }
}