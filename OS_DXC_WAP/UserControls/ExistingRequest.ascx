﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_ExistingRequest" Codebehind="ExistingRequest.ascx.cs" %>
<%@ Register Src="~/UserControls/ucHijriGregorian.ascx" TagPrefix="BUPA" TagName="ucHijriGregorian" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BUPA" TagName="uploader" Src="~/Uploader.ascx" %>


<style type="text/css">
    .labelClass {
        float: left;
        margin-right: 10px;
        line-height: 30px;
        width: 205px;
    }

    .controlClass {
        float: left;
        margin-right: 10px;
    }

    .middle {
        background-color: yellow;
        display: block;
        overflow: hidden;
    }

    .clear {
        clear: both;
    }

    .containerClass {
        height: 60px;
        width: 100%;
    }

    .controlClass input, .controlClass select {
        width: 260px;
    }

    .FloatingDiv {
        position: fixed;
        top: 0;
        left: 0;
        text-align: center;
        padding: 200px 0 0 0;
        z-index: 999;
        width: 0%;
        height: 0%;
        background: url('../Images/Floatingimg.png') repeat top left;
    }

    .Floatimage {
        display: block;
        width: 100%;
        height: 100%;
        vertical-align: middle;
    }

    .button {
        font: bold 13px Arial;
        text-decoration: none;
        background-color: #EEEEEE;
        color: #0099FF;
        padding: 2px 6px 2px 6px;
        border-top: 1px solid #CCCCCC;
        border-right: 1px solid #0099FF;
        border-bottom: 1px solid #0099FF;
        border-left: 1px solid #CCCCCC;
    }
</style>


<div id="main_entry_form">
    <%--<BUPA:MemberAndDependentsList runat="server" ID="MemberAndDependentsList" />--%>
    <asp:UpdatePanel runat="server" ID="updatePnl">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlELevelOfCover" />
        </Triggers>
        <ContentTemplate>
            <asp:UpdateProgress ID="uprgWorkRequest" runat="server">
                <ProgressTemplate>
                    <div class="FloatingDiv">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/ajax-loader.gif"
                            Style="height: 279px" /><br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div style="text-align: left;">
                <asp:Image ID="imgSuc" runat="server" Visible="false" ImageUrl="~/images/reimp_req_suc.png" /><br />
            </div>

            <asp:Label ID="lblEmpErrorMessage" runat="server" ForeColor="Red"></asp:Label>
            <br />
            <asp:Label ID="lblYaqeenError" runat="server" Text="" ForeColor="Red"></asp:Label>
            <br />
            <asp:Label ID="Message1" runat="server"></asp:Label>
            <br />

            <%--Employee Details--%>
            <table cellpadding="0" style="font-size: small; width: 700px; font-family: Arial">
                <tr>
                    <td style="background-color: #ffffff; color: red; height: 25px; font-size: small; font-weight: bold;" valign="top" align="center">
                        <asp:Literal ID="litMainError" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #ffffff; height: 25px;" valign="top">
                        <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Employee Details</span></strong>
                    </td>
                </tr>


            </table>

            <asp:PlaceHolder runat="server" ID="phCardReplaceReason" OnLoad="phCardReplaceReason_Load">
                <table>
                    <tr>
                        <td style="width: 245px; height: 25px;">
                            <span style="color: #ff0000">*</span><asp:Label ID="lblGender" runat="server" Text="Replacement Reason"
                                Width="148px"></asp:Label>
                        </td>
                        <td style="width: 275px; height: 25px;">
                            <div class="styled-select">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlRepReason" AutoPostBack="true" runat="server" Width="277px" BackColor="#eff6fc">
                                        <asp:ListItem Value="">-Select-</asp:ListItem>
                                        <asp:ListItem Value="REP001">Lost Card</asp:ListItem>
                                        <asp:ListItem Value="REP002">Damaged Card</asp:ListItem>
                                        <asp:ListItem Value="REP003" Enabled="False">Data Correction</asp:ListItem>
                                        <asp:ListItem Value="REP004">Wrong Member Name</asp:ListItem>
                                        <asp:ListItem Value="REP005">Wrong Date of birth</asp:ListItem>
                                        <asp:ListItem Value="REP006">Wrong Employee No.</asp:ListItem>
                                        <asp:ListItem Value="REP007">Wrong Nationality</asp:ListItem>
                                        <%-- <asp:ListItem Value="REP008">Wrong Saudi ID/Iqama</asp:ListItem>--%>
                                        <asp:ListItem Value="REP009">Wrong Gender</asp:ListItem>
                                        <%-- <asp:ListItem Value="REP010">Change Entry Number to IQAMA</asp:ListItem>--%>
                                        <asp:ListItem Value="REP012">Wrong Member Type</asp:ListItem>
                                        <asp:ListItem Value="REP013">Wrong ID Type</asp:ListItem>
                                        <asp:ListItem Value="REP014">Wrong ID Expiry Date</asp:ListItem>
                                        <asp:ListItem Value="REP015">Wrong Profession</asp:ListItem>
                                        <asp:ListItem Value="REP016">Wrong District</asp:ListItem>
                                        <asp:ListItem Value="REP017">Wrong Mobile</asp:ListItem>
                                        <asp:ListItem Value="REP018">Complete New CCHI Requirements</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlRepReason"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" Width="162px">Field is mandatory</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="phDeletionReason" OnLoad="phDeletionReason_Load">
                <table>
                    <tr>
                        <td align="left"><span style="color: #ff0000">*</span>Reason for deletion:&nbsp;</td>
                        <td align="left">
                            <div class="styled-select">
                                <asp:DropDownList CssClass="DropDownListCssClass" ID="ddDeleteReason" runat="server" BackColor="#eff6fc">
                                    <asp:ListItem Value="0">Select from the list</asp:ListItem>
                                    <asp:ListItem Value="CLA001">Member Promotions</asp:ListItem>
                                    <asp:ListItem Value="DEL001">Member decided to leave KSA (Exit visa only)</asp:ListItem>
                                    <asp:ListItem Value="DEL002">Member resigned from the company</asp:ListItem>
                                    <asp:ListItem Value="DEL003">Member was terminated from work / company</asp:ListItem>
                                    <asp:ListItem Value="DEL004">Member moved to another insurer</asp:ListItem>
                                    <asp:ListItem Value="DEL005">Member moved to another sponsor</asp:ListItem>
                                    <asp:ListItem Value="DEL006">Death of member</asp:ListItem>
                                    <asp:ListItem Value="DEL007">Wrong enrolment</asp:ListItem>
                                    <asp:ListItem Value="DEL008">Dependent Left KSA</asp:ListItem>
                                    <asp:ListItem Value="DEL009">Dependent passed away</asp:ListItem>
                                    <asp:ListItem Value="DEL010">Dependent moved to another insurer</asp:ListItem>
                                    <asp:ListItem Value="DEL011">Dependent moved to another sponsor</asp:ListItem>
                                    <asp:ListItem Value="DEL012">Divorced the partner</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvDeleteReason" runat="server" ControlToValidate="ddDeleteReason"
                                    Display="Static" ErrorMessage="Field is mandatory" InitialValue="-Select-" ValidationGroup="1" />
                            </div>
                        </td>
                        <td align="left"></td>
                    </tr>
                </table>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="PlaceHolder1" OnLoad="phChangeBranch_Load">
                <table>
                    <tr>
                        <td style="width: 213px; height: 25px;">
                            <span style="color: #ff0000">*</span><asp:Label ID="Label3" runat="server" Text="Reason for change"
                                Width="148px"></asp:Label>
                        </td>
                        <td style="width: 200px; height: 25px;">
                            <div class="styled-select">
                                <asp:DropDownList CssClass="DropDownListCssClass" ID="DropDownList1" runat="server" BackColor="#eff6fc">
                                    <asp:ListItem Value="0">Select from the list</asp:ListItem>
                                    <asp:ListItem Value="BRA001">Member was relocated to another jobsite</asp:ListItem>
                                    <asp:ListItem Value="BRA002">Other/Customer reason</asp:ListItem>
                                    <asp:ListItem Value="BRA003">Due to Budget/Cost Centre allocation</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlChangeReason" AutoPostBack="true"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" Width="162px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </asp:PlaceHolder>

            <asp:Panel ID="PanelChangeClass" runat="server" Visible="true" Enabled="false">
                <asp:PlaceHolder runat="server" ID="phChangeClass" OnLoad="phChangeClass_Load">
                    <table>
                        <tr>
                            <td style="width: 213px; height: 25px;">
                                <span style="color: #ff0000">*</span><asp:Label ID="lblChangeClass" runat="server" Text="Reason for change"
                                    Width="148px"></asp:Label>
                            </td>
                            <td style="width: 200px; height: 25px;">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlChangeClass" runat="server" AutoPostBack="true" BackColor="#eff6fc">
                                        <asp:ListItem Value="0">Select from the list</asp:ListItem>
                                        <asp:ListItem Value="CLA001">Member Promotions</asp:ListItem>
                                        <asp:ListItem Value="CLA002">Member Downgrade</asp:ListItem>
                                        <asp:ListItem Value="CLA003">Product Modification/Additional Class</asp:ListItem>
                                        <asp:ListItem Value="CLA004">Group/Customer Request</asp:ListItem>
                                        <asp:ListItem Value="CLA005">Master List Error</asp:ListItem>
                                        <asp:ListItem Value="CLA006">Others</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlChangeClass"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" Width="162px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                            </td>
                        </tr>

                    </table>
                </asp:PlaceHolder>
            </asp:Panel>
            <asp:Panel ID="PanelLevelOfCover" runat="server" Visible="false" Enabled="false">
                <%--Level of cover--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label10" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Level of cover"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlELevelOfCover" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlELevelOfCover_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="ddlELevelOfCover"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="Proceed" SetFocusOnError="true"
                            Width="107px">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEmpDep" runat="server">
                <%--Title--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label1" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Title"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlETitle" AutoPostBack="true"
                                ClientIDMode="Static" runat="server"
                                onchange="ValidateTitleGender(document.getElementById('ddlEGender').value, document.getElementById('ddlETitle').value, 'Emp');"
                                DataTextField="text" DataValueField="value" BackColor="#eff6fc">
                                <asp:ListItem Value="0">-Select-</asp:ListItem>
                                <asp:ListItem>Mr</asp:ListItem>
                                <asp:ListItem>Miss</asp:ListItem>
                                <asp:ListItem>Mrs</asp:ListItem>
                                <asp:ListItem>Ms</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="ddlETitle"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" InitialValue="0" Width="115px"
                            SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Gender--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label4" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Gender" Width="46px"></asp:Label><span
                                style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ClientIDMode="Static" ID="ddlEGender" AutoPostBack="true" runat="server" onchange="ValidateTitleGender(document.getElementById('ddlEGender').value, document.getElementById('ddlETitle').value, 'Emp');"
                                BackColor="#eff6fc">
                                <asp:ListItem Value="0">-Select-</asp:ListItem>
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlEGender"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                            Width="125px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Member Type--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label36" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Member Type"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEMemberType" AutoPostBack="true"
                                ClientIDMode="Static" runat="server"
                                DataTextField="mbrTypeDesc" DataValueField="mbrType" BackColor="#eff6fc">
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlEMemberType"
                            InitialValue="0" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="Proceed" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Sponsor Id--%>
                <div id="divEmpSponsorId" runat="server" class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label8" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Sponsor Id"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <asp:TextBox CssClass="textbox" ID="txtESponsorId" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><br />
                        <asp:RegularExpressionValidator
                            ID="revEmpSponsorID" runat="server" ControlToValidate="txtESponsorId"
                            Display="Dynamic" ErrorMessage="Incorrect Sponsor ID" ValidationExpression="[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
                            ValidationGroup="Proceed" Width="119px"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator
                            ID="rfvEmpSponsorID" runat="server" ControlToValidate="txtESponsorId"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="Proceed" Width="109px">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Hijri Year of Birth--%>
                <div id="divEmpHijriYearOfBirth" runat="server" class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label19"
                            runat="server" Font-Bold="False" ForeColor="#000036" Text="Hijri Year of Birth"></asp:Label>
                        <span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <asp:TextBox CssClass="textbox" ID="txtEmpHijriYearofBirth" runat="server" MaxLength="4"
                            BackColor="#eff6fc"></asp:TextBox>
                        <font color="black"></font>
                        <br />
                        <asp:RequiredFieldValidator ID="rfvEmpHijriYearOfBirth" runat="server"
                            ControlToValidate="txtEmpHijriYearofBirth" Display="Dynamic"
                            ErrorMessage="Field is mandatory" ValidationGroup="Proceed" Width="119px" />
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Joining date with company--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"></span><strong><span style="color: #ff3300">*<asp:Label
                            ID="Label13" runat="server" Font-Bold="False" ForeColor="#000036" Text="Joining date with company"
                            Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div>
                        <BUPA:ucHijriGregorian runat="server" ID="txtEJoiningDate" RequiredField="True"   Enabled="true" ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Join reason--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <asp:Label ID="Label15" runat="server" Font-Bold="False" ForeColor="#000036" Text="Join reason"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEJoinReason" AutoPostBack="true"
                                runat="server" Font-Names="Arial" Font-Size="Small">
                                <asp:ListItem Value="0">-Select-</asp:ListItem>
                                <asp:ListItem Value="RJOIN001">Member is studying outside Kingdom</asp:ListItem>
                                <asp:ListItem Value="RJOIN002">Dependent is studying outside Kingdom</asp:ListItem>
                                <asp:ListItem Value="RJOIN003">Customer forgot to add member from contract start date</asp:ListItem>
                                <asp:ListItem Value="RJOIN004">Customer forgot to add dependent from contract start date</asp:ListItem>
                                <asp:ListItem Value="RJOIN005">Main member / father forgot to add dependent from contract start date</asp:ListItem>
                                <asp:ListItem Value="RJOIN006">End of probationary period</asp:ListItem>
                                <asp:ListItem Value="RJOIN007">Employee is ill and needs treatment</asp:ListItem>
                                <asp:ListItem Value="RJOIN008">Dependent is ill and needs treatment</asp:ListItem>
                                <asp:ListItem Value="RJOIN009">Member was on long vacation</asp:ListItem>
                                <asp:ListItem Value="RJOIN010">Member promoted to higher grade which is entitled to add dependent/s</asp:ListItem>
                                <asp:ListItem Value="RJOIN011">Newly Hired Employee</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RFVldtrEJoinReason" InitialValue="0" runat="server" SetFocusOnError="true"
                            ControlToValidate="ddlEJoinReason" ErrorMessage="RequiredFieldValidator" Font-Names="Verdana"
                            Enabled="false" ValidationGroup="1" Display="Dynamic" Font-Size="Small" Text="Join Reason is mandatory"
                            Width="176px"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Employee No--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300">
                            <asp:Label ID="empind" runat="server" Text="*" Visible="false"></asp:Label></span><asp:Label ID="Label9" runat="server"
                                Font-Bold="False" ForeColor="#000036" Text="Employee No"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <asp:TextBox CssClass="textbox" ID="txtEEmployeeNo" runat="server" MaxLength="15" BackColor="#eff6fc"></asp:TextBox><br />
                     <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpNo" runat="server" ControlToValidate="txtEEmployeeNo"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="125px"
                                    SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Department Code--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <asp:Label ID="Label12" runat="server" Font-Bold="False" ForeColor="#000036" Text="Department Code"
                            Width="105px"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <asp:TextBox CssClass="textbox" ID="txtEDepartmentCode" runat="server" BackColor="White" MaxLength="10"
                            Rows="10"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeptCode" runat="server" ControlToValidate="txtEDepartmentCode"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                            Width="120px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Already covered by BUPA--%>
                <div class="containerClass">
                    <asp:CheckBox ID="chkPreviousMember" runat="server" onclick="if(this.checked == true) document.getElementById('RFVldtrPreviousMember').enabled  = true; else  document.getElementById('RFVldtrPreviousMember').enabled  = false;"
                        Text="Has the member or any of its dependents previously been covered by BUPA ?" />
                </div>
                <%--Previous Membership No.--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <asp:Label ID="Label17" runat="server" Font-Bold="False" ForeColor="#000036" Text="Previous Membership No."></asp:Label>
                    </div>
                    <div class="controlClass">
                        <asp:TextBox CssClass="textbox" ID="txtPreviousMember" runat="server" MaxLength="7" ValidationGroup="1"
                            BackColor="White"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RFVldtrPreviousMember"
                            runat="server" ControlToValidate="txtPreviousMember" Display="Dynamic" Enabled="false"
                            ErrorMessage="RequiredFieldValidator" ValidationGroup="1" Width="125px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                        <%--<asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPreviousMember"
                                            Display="Dynamic" ValidationExpression="[3|4|5][0-9][0-9][0-9][0-9][0-9][0-9]" SetFocusOnError="true"
                                            ErrorMessage="RegularExpressionValidator" ValidationGroup="1" Width="149px">Incorrect Membership No.</asp:RegularExpressionValidator>--%>
                    </div>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlEmpDetails" runat="server" Visible="false">
                <%--Complete Name (First, Middle, Last)--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label2" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Complete Name (First, Middle, Last)"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:TextBox CssClass="textbox" ID="txtEMemberName" ClientIDMode="Static" runat="server" MaxLength="40" ValidationGroup="1"
                                BackColor="#eff6fc"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtEMemberName"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" ValidationGroup="1"
                            Width="125px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator
                            ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtEMemberName"
                            Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                            Font-Bold="False" ValidationExpression="[a-zA-Z ]+" ValidationGroup="1" SetFocusOnError="true"></asp:RegularExpressionValidator>

                    </div>
                    <div class="clear"></div>
                    <asp:Literal ID="litErrorEmployeeExceed40" runat="server" Text="" Visible="false"></asp:Literal>
                </div>
                <%--Date Of Birth--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*<asp:Label ID="Label5" runat="server" Font-Bold="False"
                            ForeColor="#000036" Text="Date Of Birth"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span>
                    </div>
                    <div>
                        <BUPA:ucHijriGregorian runat="server" ID="txtEDateOfBirth" RequiredField="True"
                            Enabled="true" ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                    </div>
                    <div class="clear"></div>
                </div>
                <%--ID Type--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label27" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="ID Type"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEIDType" AutoPostBack="true"
                                ClientIDMode="Static" runat="server"
                                BackColor="#eff6fc">
                                <asp:ListItem Value="0">-Select-</asp:ListItem>
                                <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="ddlEIDType"
                            InitialValue="0" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="Proceed" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Marital Status--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <strong><span style="color: #ff3300"></span></strong>
                        <span style="color: #ff3300">*</span>
                        <asp:Label ID="Label29" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Marital Status"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEMaritalStatus" AutoPostBack="true"
                                ClientIDMode="Static" runat="server"
                                BackColor="#eff6fc">
                                <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ControlToValidate="ddlEMaritalStatus"
                            InitialValue="0" Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="1"
                            Width="122px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Saudi ID/ Iqama ID/ Border Entry--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><span style="color: #ff3300"><strong>*<asp:Label ID="Label7"
                            runat="server" Font-Bold="False" ForeColor="#000036" Text="Saudi ID/ Iqama ID/ Border Entry"
                            Width="185px"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span></span>
                    </div>
                    <div class="controlClass">
                        <asp:TextBox CssClass="textbox" ID="txtEmployeeID" runat="server" MaxLength="10"
                            BackColor="#eff6fc"
                            AutoPostBack="false"></asp:TextBox><br />
                        <asp:RequiredFieldValidator
                            ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtEmployeeID" SetFocusOnError="true"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="Proceed" Width="110px">Field is mandatory</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator
                            ID="revSuadi" runat="server" ControlToValidate="txtEmployeeID"
                            Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[1][0-9]{9}"
                            ValidationGroup="Proceed" Width="142px">Incorrect Saudi ID</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator
                            ID="revIqama" runat="server" ControlToValidate="txtEmployeeID"
                            Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[2][0-9]{9}"
                            ValidationGroup="Proceed" Width="142px">Incorrect Iqama ID</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator
                            ID="revBorder" runat="server" ControlToValidate="txtEmployeeID"
                            Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[3-5][0-9]{9}"
                            ValidationGroup="Proceed" Width="142px">Incorrect Border Entry ID</asp:RegularExpressionValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--ID Expiry Date--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>
                            <span style="color: #ff3300">*</span>
                            <asp:Label ID="Label28" runat="server" Font-Bold="False"
                                ForeColor="#000036" Text="ID Expiry Date"></asp:Label></strong><span style="color: #ff3300"><strong></strong></span></span>
                    </div>
                    <div>
                        <BUPA:ucHijriGregorian runat="server" ID="txtEIDExpDate" Enabled="true"
                            ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                    </div>
                    <div class="clear">
                        <asp:HiddenField ID="hidYakeenEmployee" runat="server" />
                    </div>
                </div>
                <%--Profession--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                            <span style="color: #ff3300">*</span>
                            <asp:Label ID="Label25" runat="server" Font-Bold="False" ForeColor="#000036" Text="Profession" Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <telerik:RadComboBox ID="ddProfession" runat="server" Height="280" Width="265"
                                Filter="Contains" MarkFirstMatch="true" ChangeTextOnKeyBoardNavigation="false"
                                DataTextField="profName" DataValueField="profCode" EmptyMessage="-- Select --" BackColor="#eff6fc">
                            </telerik:RadComboBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="ddProfession"
                            Display="Dynamic" ErrorMessage="Field is mandatory" ValidationGroup="1"
                            Width="125px" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--District--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                            <span style="color: #ff3300">*</span>
                            <asp:Label ID="Label24" runat="server" Font-Bold="False" ForeColor="#000036" Text="District"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList ID="ddDistrict" runat="server" BackColor="#eff6fc" DataValueField="distCode" DataTextField="distName">
                                <asp:ListItem Value=" "></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="ddDistrict"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="1"
                            Width="125px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Nationality--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="Label6" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Nationality"></asp:Label><span style="color: #ff3300"><strong></strong></span>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlENationality" AutoPostBack="true" Font-Size="9" runat="server" DataTextField="text" DataValueField="value" BackColor="#eff6fc">
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" InitialValue="0"
                            ControlToValidate="ddlENationality" Display="Dynamic" ErrorMessage="RequiredFieldValidator"
                            ValidationGroup="1" Width="114px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--Mobile No--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"></span><span style="color: #ff3300">
                            <span style="color: #ff3300">*</span>
                            <asp:Label ID="Label16" runat="server" Font-Bold="False" ForeColor="#000036" Text="Mobile No"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:TextBox CssClass="textbox" ID="txtEMobileNo" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox>
                        </div>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                            ControlToValidate="txtEMobileNo" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                            ValidationExpression="[0][5][0-9]{8}" ValidationGroup="1"
                            Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ControlToValidate="txtEMobileNo"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="1"
                            Width="120px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>



                <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/xmlfiles/Title.xml"></asp:XmlDataSource>
                <asp:XmlDataSource ID="XmlDataSource2" runat="server" DataFile="~/xmlfiles/Nationality.xml"></asp:XmlDataSource>
                </span>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlChangeBranch" Visible="false" Enabled="false">
                <%--Branch Code--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong>*</strong></span><span style="color: #ff3300"><strong></strong></span>
                        <span style="color: #ff3300"><strong></strong></span>
                        <asp:Label ID="Label11" runat="server"
                            Font-Bold="False" ForeColor="#000036" Text="Branch Code"></asp:Label>
                    </div>
                    <div class="controlClass">
                        <div class="styled-select">
                            <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlEBranchCode" AutoPostBack="true" runat="server" BackColor="#eff6fc">
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvddlEBranchCode" runat="server" ControlToValidate="ddlEBranchCode"
                            Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="1"
                            Width="120px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                    </div>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlChangeBranchReason" Visible="false" Enabled="false">
                <asp:PlaceHolder runat="server" ID="phChangeBranch" OnLoad="phChangeBranch_Load">
                    <table>
                        <tr>
                            <td style="width: 213px; height: 25px;">
                                <span style="color: #ff0000">*</span><asp:Label ID="lbl_Reason" runat="server" Text="Reason for change"
                                    Width="148px"></asp:Label>
                            </td>
                            <td style="width: 200px; height: 25px;">
                                <div class="styled-select">
                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlChangeReason" AutoPostBack="true" runat="server" BackColor="#eff6fc">
                                        <asp:ListItem Value="0">Select from the list</asp:ListItem>
                                        <asp:ListItem Value="CLA001">Member Promotions</asp:ListItem>
                                        <asp:ListItem Value="BRA001">Member was relocated to another jobsite</asp:ListItem>
                                        <asp:ListItem Value="BRA002">Other/Customer reason</asp:ListItem>
                                        <asp:ListItem Value="BRA003">Due to Budget/Cost Centre allocation</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlChangeReason"
                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" Width="162px" ValidationGroup="1">Field is mandatory</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:PlaceHolder>
            </asp:Panel>
            <asp:Panel ID="PanelEffectiveDate" runat="server" Visible="true" Enabled="true">
                <%--Start date for medical cover--%>
                <div class="containerClass">
                    <div class="labelClass">
                        <span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong style="vertical-align: text-top">
                            <span style="vertical-align: top; color: #ff3300"><strong></strong></span>*<asp:Label
                                ID="Label14" runat="server" Font-Bold="False" ForeColor="#000036" Text="Start date for medical cover"
                                Width="158px"></asp:Label></strong></span>
                    </div>
                    <div>
                        <BUPA:ucHijriGregorian runat="server" ID="txtERequestedStartDate" RequiredField="True"
                            Enabled="true" ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                    </div>
                    <div class="clear"></div>
                </div>
            </asp:Panel>


            <br />

            <asp:Panel ID="PanelMembershipNoOnly" runat="server" Visible="false">
                <table style="font-size: small; width: 396px;">
                    <tr>
                        <td style="background-color: #ffffff; height: 25px;" valign="top" colspan="2">
                            <strong><span style="font-size: 11pt; color: #0099ff">Employee Details</span></strong>
                        </td>
                        <td style="background-color: #ffffff; height: 25px;" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 122px" valign="top">
                            <nobr>
                                        <strong>
                                            <span style="COLOR: #ff3300">*</span>
                                        </strong>
                                        <asp:Label id="lblMainMemberNo" runat="server" Text="Membership Number" Width="128px"></asp:Label>
                                    </nobr>
                        </td>
                        <td style="width: 288px; margin-left: 40px;" valign="top">
                            <asp:TextBox CssClass="textbox" ID="txtMainMemberNo" runat="server" Width="164px" MaxLength="8" ValidationGroup="1"
                                BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMainMemberNo"
                                Display="Dynamic" ErrorMessage="RequiredFieldValidator" Width="250px" ValidationGroup="1"
                                SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 288px" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background-color: #ffffff; color: red; height: 25px; font-size: small; font-weight: bold;" valign="top"></td>
                    </tr>

                </table>
            </asp:Panel>
            <br />
            <br />

            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="background-color: #ffffff; height: 25px;" valign="top">
                                    <strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Dependant Details</span></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
                        </telerik:RadWindowManager>
                                    <asp:Literal ID="litDepMainError" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDependents" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                        Font-Names="Arial" Font-Size="11px" ForeColor="#333333"
                                        GridLines="None" OnRowCommand="gvDependents_RowCommand" OnRowDataBound="gvDependents_RowDataBound"
                                        OnSelectedIndexChanged="gvDependents_SelectedIndexChanged" Width="635px">
                                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="title" HeaderText="Title">
                                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CompleteName" HeaderText="Complete Name">
                                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RelationShip" HeaderText="Relation">
                                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DOB" DataFormatString="{0:d MMM yyyy }" HeaderText="Date of Birth"
                                                HtmlEncode="False">
                                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LevelCover" HeaderText="Level of Cover">
                                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RequestStartDate" HeaderText="Start Date">
                                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SponsorID" HeaderText="Sponsor ID">
                                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EmployeeNo" HeaderText="Employee No">
                                                <HeaderStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                                <ItemStyle BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
                                            </asp:BoundField>
                                            <asp:CommandField ShowSelectButton="True" SelectText="Edit">
                                                <HeaderStyle BackColor="White" BorderColor="White" />
                                                <ItemStyle Width="30px" HorizontalAlign="Center" BackColor="White" BorderColor="White" />
                                            </asp:CommandField>
                                            <%--  <asp:CommandField HeaderText="" ShowDeleteButton="True">
                                <HeaderStyle BackColor="White" BorderColor="White" />
                                <ItemStyle Width="30px" HorizontalAlign="Center" BackColor="White" BorderColor="White" />
                            </asp:CommandField>--%>
                                        </Columns>
                                        <RowStyle BackColor="White" />
                                        <EditRowStyle BackColor="#7C6F57" />
                                        <SelectedRowStyle BackColor="#4CAC27" BorderStyle="None" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle BackColor="#E6F5FF" Font-Bold="True" ForeColor="#404040" Font-Names="Arial"
                                            Font-Size="9pt" Font-Strikeout="False" />
                                    </asp:GridView>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlDependentDetail" runat="server" Visible="false">
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300">
                                                    <strong>*</strong>
                                                </span>
                                                <asp:Label ID="lblLevelOfCover" runat="server" Font-Bold="False" ForeColor="#000036" Text="Level of cover"></asp:Label>
                                                <span style="color: #ff3300">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlLevelOfCover" runat="server" BackColor="#eff6fc" OnSelectedIndexChanged="ddlLevelOfCover_SelectedIndexChanged" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlLevelOfCover"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                                                    Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300">
                                                    <strong>*</strong></span>
                                                <asp:Label ID="Label30" runat="server"
                                                    Font-Bold="False" ForeColor="#000036" Text="Member Type"></asp:Label>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlMemberType"
                                                        runat="server" DataTextField="mbrTypeDesc" DataValueField="mbrType"
                                                        BackColor="#eff6fc">
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ControlToValidate="ddlMemberType"
                                                    InitialValue="0" SetFocusOnError="true"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div id="divAgeGroup" class="containerClass" runat="server" style="display: none;">
                                            <div class="labelClass">
                                                <span style="color: #ff3300">
                                                    <strong>*</strong>
                                                </span>
                                                <asp:Label ID="Label20" runat="server" Font-Bold="False" ForeColor="#000036" Text="Age group"></asp:Label>
                                                <span style="color: #ff3300">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlAgeGroup" runat="server" BackColor="#eff6fc">
                                                        <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="1">Newborn baby (0 to 3 Months)</asp:ListItem>
                                                        <asp:ListItem Value="2">Older than 3 months</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfv_ddlAgeGroup" runat="server" ControlToValidate="ddlAgeGroup"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                                                    Width="107px" SetFocusOnError="true" Enabled="false">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div id="entryForm2">
                                            <div class="containerClass">
                                                <div class="labelClass">
                                                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="Label31" runat="server"
                                                        Font-Bold="False" ForeColor="#000036" Text="ID Type"></asp:Label>
                                                </div>
                                                <div class="controlClass">
                                                    <div class="styled-select">
                                                        <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlIDType" runat="server"
                                                            BackColor="#eff6fc">
                                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                            <asp:ListItem Text="National ID" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Iqama" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Border Entry" Value="4"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="ddlIDType"
                                                        InitialValue="0" SetFocusOnError="true"
                                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="115px">Field is mandatory</asp:RequiredFieldValidator>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div id="divDepID" runat="server" class="containerClass">
                                                <div class="labelClass">
                                                    <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="lblSaudiID"
                                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Saudi ID/ Iqama ID/ Border Entry"
                                                        Width="185px"></asp:Label>
                                                </div>
                                                <div class="controlClass">
                                                    <asp:TextBox CssClass="textbox" ID="txtDependentID" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><asp:Label
                                                        ID="lblSaudiNote" runat="server" Font-Bold="False" ForeColor="Black" Text=" (Must be different than the employee's Saudi Id/Iqama Id)"></asp:Label>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rfv_ID" runat="server" ControlToValidate="txtDependentID"
                                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Enabled="true"
                                                        Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator
                                                        ID="revDepSuadi" runat="server" ControlToValidate="txtDependentID"
                                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[1][0-9]{9}"
                                                        ValidationGroup="2" Width="142px">Incorrect Saudi ID</asp:RegularExpressionValidator>
                                                    <asp:RegularExpressionValidator
                                                        ID="revDepIqama" runat="server" ControlToValidate="txtDependentID"
                                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[2][0-9]{9}"
                                                        ValidationGroup="2" Width="142px">Incorrect Iqama ID</asp:RegularExpressionValidator>
                                                    <asp:RegularExpressionValidator
                                                        ID="revDepBorder" runat="server" ControlToValidate="txtDependentID"
                                                        Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[3-5][0-9]{9}"
                                                        ValidationGroup="2" Width="142px">Incorrect Border Entry ID</asp:RegularExpressionValidator>
                                                    <asp:Label ID="lblIDVal" ForeColor="Red" runat="server" Text="This Field is mandatory" Visible="false"></asp:Label>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div id="divDepSponsorID" runat="server" class="containerClass">
                                                <div class="labelClass">
                                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblSponsorId"
                                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Sponsor Id"></asp:Label>
                                                    <span style="color: #ff3300"><strong></strong></span>
                                                </div>
                                                <div class="controlClass">
                                                    <%--<telerik:RadComboBox ID="ddlDepSponsor" runat="server" Width="200px" Height="150px"
                                        EmptyMessage="Select/start typing" DataTextField="sponsorID"
                                        OnItemDataBound="ddlDepSponsor_ItemDataBound">
                                    </telerik:RadComboBox>--%>
                                                    <asp:TextBox CssClass="textbox" ID="txtSponsorId" runat="server" MaxLength="10"
                                                        BackColor="#eff6fc"></asp:TextBox>
                                                    <font color="black">Sponsor ID has to be the main member’s Saudi ID/Iqama number.</font>
                                                    <br />
                                                    <asp:RegularExpressionValidator
                                                        ID="revDepSponsorID" runat="server" ControlToValidate="txtSponsorId"
                                                        Display="Dynamic" ErrorMessage="Incorrect Sponsor ID" ValidationExpression="[1|2|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
                                                        ValidationGroup="2" Width="129px"></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator
                                                        ID="rfvDepSponsorID" runat="server" ControlToValidate="txtSponsorId"
                                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="119px">Field is mandatory</asp:RequiredFieldValidator>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div id="divDepHijriYearOfBirth" runat="server" class="containerClass">
                                                <div class="labelClass">
                                                    <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblHijri"
                                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Hijri Year of Birth"></asp:Label>
                                                    <span style="color: #ff3300"><strong></strong></span>
                                                </div>
                                                <div class="controlClass">
                                                    <asp:TextBox CssClass="textbox" ID="txtDepHijriYearofBirth" runat="server" MaxLength="4"
                                                        BackColor="#eff6fc"></asp:TextBox>
                                                    <font color="black"></font>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rfvDepHijriYearOfBirth" runat="server"
                                                        ControlToValidate="txtDepHijriYearofBirth" Display="Dynamic"
                                                        ErrorMessage="Field is mandatory" ValidationGroup="2" Width="119px" />
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <asp:Label ID="lblYaqeenDepError" runat="server" Text="" ForeColor="Red"></asp:Label>

                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblNationality"
                                                    runat="server" Font-Bold="False" ForeColor="#000036" Text="Nationality"></asp:Label><span
                                                        style="color: #ff3300"><strong></strong></span>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlNationality" runat="server" BackColor="#eff6fc" DataTextField="text" DataValueField="value">
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlNationality"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="111px" InitialValue="0"
                                                    SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear">
                                                <asp:HiddenField ID="hidYakeenDependent" runat="server" />
                                            </div>
                                        </div>
                                        <div id="divIDExpiryDate" runat="server" class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"><strong>
                                                    <span style="color: #ff3300">*</span>
                                                    <asp:Label ID="Label32" runat="server" Font-Bold="False"
                                                        ForeColor="#000036" Text="ID Expiry Date"></asp:Label></strong><span style="color: #ff3300">
                                                            <strong></strong></span></span>
                                            </div>
                                            <div>
                                                <BUPA:ucHijriGregorian runat="server" ID="txtIDExpDate" Enabled="true"
                                                    ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                                                <asp:Label ID="lblIDExpiryVal" ForeColor="Red" runat="server" Text="Field is mandatory" Visible="false"></asp:Label>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <strong><span style="color: #ff3300"></span></strong>
                                                <span style="color: #ff3300">*</span>
                                                <asp:Label ID="Label33" runat="server"
                                                    Font-Bold="False" ForeColor="#000036" Text="Marital Status"></asp:Label>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlMaritalStatus"
                                                        ClientIDMode="Static" runat="server" BackColor="#eff6fc">
                                                        <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Single"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Married"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Divorced"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Widowed"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ControlToValidate="ddlMaritalStatus" InitialValue="0"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="115px"
                                                    SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"><strong>*</strong></span><asp:Label ID="lblTitle" runat="server"
                                                    Font-Bold="False" ForeColor="#000036" Text="Title"></asp:Label>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlTitle" runat="server" BackColor="#eff6fc"
                                                        onchange="ValidateTitleGender(document.getElementById('ddlGender').value, document.getElementById('ddlTitle').value, 'Dep');"
                                                        DataTextField="text" DataValueField="value">
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlTitle"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="110px"
                                                    SetFocusOnError="true" InitialValue="0">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"><strong>*</strong><asp:Label ID="Label18" runat="server"
                                                    Font-Bold="False" ForeColor="#000036" Text="Gender" Width="46px"></asp:Label><span
                                                        style="color: #ff3300"><strong></strong></span></span>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlGender" runat="server" BackColor="#eff6fc"
                                                        onchange="ValidateTitleGender(document.getElementById('ddlGender').value, document.getElementById('ddlTitle').value, 'Dep');">
                                                        <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="M">Male</asp:ListItem>
                                                        <asp:ListItem Value="F">Female</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlGender"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2" SetFocusOnError="true"
                                                    Width="109px">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <strong><span style="color: #ff3300">*</span></strong><asp:Label ID="lblCompleteName"
                                                    runat="server" Font-Bold="False" ForeColor="#000036" Text="Complete Name (First, Middle, Last)"></asp:Label>
                                            </div>
                                            <div class="controlClass">
                                                <asp:TextBox CssClass="textbox" ID="txtMemberName" runat="server" MaxLength="40" BackColor="#eff6fc"></asp:TextBox><br />
                                                <asp:RequiredFieldValidator
                                                    ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtMemberName"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" Font-Bold="False" ValidationGroup="2"
                                                    Width="158px">Field is mandatory</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator
                                                    ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtMemberName"
                                                    Display="Dynamic" ErrorMessage="Incorrect name format, please use only alphabets."
                                                    Font-Bold="False" ValidationExpression="[a-zA-Z ]+" ValidationGroup="2"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="clear"></div>
                                            <asp:Literal ID="litErrorDepExceed40" runat="server" Text="" Visible="false"></asp:Literal>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"><span style="color: #ff3300"><strong>*</strong><asp:Label
                                                    ID="lblDateOfBirth" runat="server" Font-Bold="False" ForeColor="#000036" Text="Date Of Birth"></asp:Label><span
                                                        style="color: #ff3300"><strong></strong></span></span></span>
                                            </div>
                                            <div>
                                                <BUPA:ucHijriGregorian runat="server" ID="txtDateOfBirth" Enabled="true"
                                                    ValidationGroup="1" ErrorMsgForRequiredField="Please fill in the Date" />
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"><strong></strong></span><strong><span style="color: #ff3300">
                                                    <span style="color: #ff3300">*</span>
                                                    <asp:Label ID="Label35" runat="server" Font-Bold="False" ForeColor="#000036" Text="Profession" Width="153px"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList ID="ddDepProfession" runat="server" BackColor="#eff6fc">
                                                        <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="99402">Son/Daughter</asp:ListItem>
                                                        <asp:ListItem Value="99401">Husband/Wife</asp:ListItem>
                                                        <asp:ListItem Value="99403">Father/Mother</asp:ListItem>
                                                        <asp:ListItem Value="99404">Brother/Sister</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ControlToValidate="ddDepProfession"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="0" ValidationGroup="2"
                                                    Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"></span><strong><span style="color: #ff3300">
                                                    <span style="color: #ff3300">*</span>
                                                    <asp:Label ID="Label34" runat="server" Font-Bold="False" ForeColor="#000036" Text="District"></asp:Label></span></strong><span style="color: #ff3300"><strong></strong></span>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList ID="ddDepDistrict" runat="server" BackColor="#eff6fc" DataValueField="distCode" DataTextField="distName">
                                                        <asp:ListItem Value=" "></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ControlToValidate="ddDepDistrict"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="2"
                                                    Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"><strong></strong></span>
                                                <span style="color: #ff3300">*</span>
                                                <asp:Label ID="lblMobileNo" runat="server" Font-Bold="False" ForeColor="#000036"
                                                    Text="Mobile No"></asp:Label>
                                            </div>
                                            <div class="controlClass">
                                                <asp:TextBox CssClass="textbox" ID="txtMobileNo" runat="server" MaxLength="10" BackColor="#eff6fc"></asp:TextBox><br />
                                                <asp:RegularExpressionValidator
                                                    ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobileNo"
                                                    Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="[0][5][0-9]{8}"
                                                    ValidationGroup="2" Width="116px">Incorrect Mobile No.</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ControlToValidate="txtMobileNo"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" InitialValue="" ValidationGroup="2"
                                                    Width="107px" SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"></span><strong><span style="color: #ff3300">*<asp:Label
                                                    ID="lblJoinDate" runat="server" Font-Bold="False" ForeColor="#000036" Text="Arrival date in KSA"></asp:Label></span></strong>
                                            </div>
                                            <div>
                                                <BUPA:ucHijriGregorian runat="server" ID="txtJoiningDate" Enabled="true" RequiredField="True"
                                                    ValidationGroup="2" ErrorMsgForRequiredField="Please fill in the Date" />
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong></strong></span><span style="color: #ff3300"><strong style="vertical-align: text-top">
                                                    <span style="vertical-align: top; color: #ff3300"><strong></strong></span>*<asp:Label
                                                        ID="lblRequestedStartDate" runat="server" Font-Bold="False" ForeColor="#000036"
                                                        Text="Start date for medical cover" Width="160px"></asp:Label></strong></span>
                                            </div>
                                            <div>
                                                <BUPA:ucHijriGregorian runat="server" ID="txtRequestedStartDate" Enabled="true" RequiredField="True"
                                                    ValidationGroup="2" ErrorMsgForRequiredField="Please fill in the Date" />
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <span style="color: #ff3300">
                                                    <asp:Label ID="empind1" runat="server" Text="*" Visible="false"></asp:Label></span><asp:Label ID="lblEmployeeNo"
                                                        runat="server" Font-Bold="False" ForeColor="#000036" Text="Employee No" Width="83px"></asp:Label><span
                                                            style="color: #ff3300"><strong></strong></span>
                                            </div>
                                            <div class="controlClass">
                                                <asp:TextBox CssClass="textbox" ID="txtEmployeeNo" runat="server" MaxLength="15" BackColor="#eff6fc"></asp:TextBox><br />
                                                <%--<asp:RequiredFieldValidator
                                                    ID="RequiredFieldValidatorEmpNoDep" runat="server" ControlToValidate="txtEmployeeNo"
                                                    Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="2" Width="112px"
                                                    SetFocusOnError="true">Field is mandatory</asp:RequiredFieldValidator>--%>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <asp:Label ID="lblDepartmentCode" runat="server" Font-Bold="False" ForeColor="#000036"
                                                    Text="Department Code" Width="105px"></asp:Label>
                                            </div>
                                            <div class="controlClass">
                                                <asp:TextBox CssClass="textbox" ID="txtDepartmentCode" runat="server" BackColor="White" MaxLength="10"
                                                    Rows="10"></asp:TextBox>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="containerClass">
                                            <div class="labelClass">
                                                <asp:Label ID="lblJoinReason" runat="server" Font-Bold="False" ForeColor="#000036"
                                                    Text="Join reason" Width="97px"></asp:Label>
                                            </div>
                                            <div class="controlClass">
                                                <div class="styled-select">
                                                    <asp:DropDownList CssClass="DropDownListCssClass" ID="ddlJoinReason" runat="server" Font-Names="Arial" Font-Size="Small">
                                                        <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN001">Member is studying outside Kingdom</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN002">Dependent is studying outside Kingdom</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN003">Customer forgot to add member from contract start date</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN004">Customer forgot to add dependent from contract start date</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN005">Main member / father forgot to add dependent from contract start date</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN006">End of probationary period</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN007">Employee is ill and needs treatment</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN008">Dependent is ill and needs treatment</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN009">Member was on long vacation</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN010">Member promoted to higher grade which is entitled to add dependent/s</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN012">New Born baby</asp:ListItem>
                                                        <asp:ListItem Value="RJOIN013">Newly Married</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RFVldtrJoinReason" InitialValue="0" runat="server"
                                                    ControlToValidate="ddlJoinReason" ErrorMessage="RequiredFieldValidator" Font-Names="Verdana"
                                                    Enabled="false" ValidationGroup="2" Display="Dynamic" Font-Size="Small" Text="Join Reason is mandatory"
                                                    Width="176px" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </asp:Panel>

                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" valign="top">
                                    <asp:Button CssClass="submitButton" ID="btnUpdateDep" runat="server" Text="Update Dependent" Visible="False"
                                        OnClick="btnUpdateDep_Click" BackColor="Control" ValidationGroup="2" />
                                    <asp:Button CssClass="submitButton" ID="btnCancelUpdateDep"
                                        runat="server" OnClick="btnCancelUpdateDep_Click" Text="Cancel" Visible="False" BackColor="Control" />
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table>
            <br />

        </ContentTemplate>

    </asp:UpdatePanel>
</div>
<table style="width: 750px; font-size: 11px; font-family: Arial; border-right: black 0.5pt solid; border-top: black 0.5pt solid; border-left: black 0.5pt solid; border-bottom: black 0.5pt solid; border-collapse: collapse;"
    runat="server" id="tblUploader">
    <tr>
        <td align="left">
            <span style="color: #ff3300"><strong><span style="font-family: Arial; font-size: 11pt; color: #0099ff">Upload Supporting Documents</span></strong>
        </td>
        <td align="right">
            <a href="<%= Page.ResolveUrl("~/Docs/ReqDocs2012.pdf") %>" target="_blank">Required Document List</a>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label runat="server" ID="lblUploadExtension" Text="Please upload related documents here (allowed types are: .pdf, .jpg, .jpeg, .gif, .png, .bmp) 5MB limit per file" /><br />
            <BUPA:uploader runat="server" ID="uploader" />
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
</table>
<br />
<table style="width: 750px; font-size: 11px; font-family: Arial;" runat="server" id="Table2">
    <tr>
        <td align="right">
            <asp:Button CssClass="submitButton" ID="btnSubmit" runat="server"
                Text="Submit Request" ValidationGroup="1" OnClick="btnSubmit_Click" BackColor="Control" />
        </td>
    </tr>
</table>
