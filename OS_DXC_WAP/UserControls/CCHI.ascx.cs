﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class UserControls_CCHI : System.Web.UI.UserControl
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    string strMembershipNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        Message1.Text = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
        {
            lblmemTitle.Visible = false;
            txtmembership.Visible = false;
            btnCheck.Visible = false;
            DisplayMemberCertResult(Convert.ToString(Session["MembershipNo"]));
            
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
        {
            lblmemTitle.Visible = true;
            txtmembership.Visible = true;
            btnCheck.Visible = true;
        }
    }

    private void DisplayMemberCertResult(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response;
        string str_Exist = "";
        try
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                str_Exist = "Y";
            }
            else
            {
                str_Exist = DisplayMemberExistForClientResult(MemberNo, "");
            }
            
            if (str_Exist == "Y")
            {
                response = null;
                request = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
                request.membershipNo = MemberNo;
                request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;
                response = ws.EnqMbrDetInfo(request);

                if (response.detail[0].memberName != null)
                {
                    DisplayMbrDetInfo(response);
                }
                else
                {
                    lblcStatus.Text = "";
                    lblReason.Text = "";
                    Message1.Text = "Error Encounter. Please contact the administrator for further information.";
                }
            }
            else
            {
                lblcStatus.Text = "";
                lblReason.Text = "";
                Message1.Text = str_Exist;// "Inputted member does not exist in your group.";
            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message; 
            

        }

    }



    private string CheckMemberExist(string MemberNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.ChkMbrExistRequest_DN request;
        OS_DXC_WAP.CaesarWS.ChkMbrExistResponse_DN response;
        try
        {

            response = null;
            request = new OS_DXC_WAP.CaesarWS.ChkMbrExistRequest_DN();
            request.membershipNo = MemberNo;
            request.iqamaNo = "";// IqamaNo;
            request.totalNoOfFamily = ""; // DependentNo;
            request.transactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
            request.transactionID = TransactionManager.TransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.ChkMbrExist(request);


            if (response.memberName != null)
            {
                return "Exist";
            }
            else
            {
                return response.errorMessage[0];
            }

        }
        catch (Exception ex)
        {
            return "Error";

        }




    }




    private string DisplayMemberExistForClientResult(string SearchText, string SearchType)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        string strReturn = "";

        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        try
        {
            // Cursor.Current = Cursors.WaitCursor;
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
            request.customerNo = Convert.ToString(Session["ClientID"]);

           
                request.membershipNo = SearchText;
          

            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            response = ws.EnqMbrListInfo(request);


            if (response.detail == null)
            {

                strReturn = response.errorMessage;// "N";

            }
            else
            {
                strReturn = "Y";

            }
            return strReturn;
        }
        catch { }
        {
            return "error";
        }




    }


    private void DisplayMbrDetInfo(OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response)
    {
        StringBuilder sbResponse = new StringBuilder(200);
        if (response.status == "0")
        {
            Session["VIP"] = response.detail[0].VIPCode;
            if (!string.IsNullOrEmpty(response.detail[0].CCHIStatus))
            {
                if (response.detail[0].CCHIStatus.Trim() == "Rejected")
                {
                    lblcStatus.Text = response.detail[0].CCHIStatus;
                    lblReason.Text = response.detail[0].CCHIRejReason;
                    Statuslbl.Visible = true;
                    lblReasonTitle.Visible = true;
                }
                else
                {
                    lblcStatus.Text = response.detail[0].CCHIStatus;
                    lblReason.Text = "";
                    Statuslbl.Visible = true;
                    lblReasonTitle.Visible = false;
                }

            }
            else
            {
                lblcStatus.Text = "";
                lblReason.Text = "";
                Message1.Text = "No CCHI status update for this member";
            }

        }
        else
        {
            sbResponse.Append(response.errorID).Append(": ").Append(response.errorMessage);
            lblcStatus.Text = response.errorMessage;
        }
            



    }
    protected void btnCheck_Click(object sender, EventArgs e)
    {
        DisplayMemberCertResult(txtmembership.Text);
    }
}