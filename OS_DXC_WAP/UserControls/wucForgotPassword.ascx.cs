﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using Bupa.Core.Logging;
using System.Text;

public partial class UserControls_wucForgotPassword : System.Web.UI.UserControl
{
    private string _type = "";
    protected void Page_Load(object sender, EventArgs e)
    {
		// Stop Caching in IE
		Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

		// Stop Caching in Firefox
		Response.Cache.SetNoStore();
       
        btnLogin.Text = "Retrive Password";
        lblType.Text = "User Name:";
		//Captcha Forgot password by Hussamuddin
        if (!IsPostBack) { 
        Session["CaptchaImageText"] = GenerateRandomCode();
        CaptchaText.InnerHtml = Session["CaptchaImageText"].ToString();
        }
		//Captcha Forgot password by Hussamuddin
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
		//Captcha Forgot password by Hussamuddin
        if (txtimgcode.Text == Session["CaptchaImageText"].ToString())
        {
            divCaptchaImage.Visible = false;
            txtimgcode.Text = "";
            WrongCaptcha.Style.Add("visibility", "hidden");

        }
        else
        {
            txtimgcode.Text = "";
            divCaptchaImage.Visible = true;
            WrongCaptcha.Style.Add("visibility", "visible");
			Session["CaptchaImageText"] = GenerateRandomCode();
			CaptchaText.InnerHtml = Session["CaptchaImageText"].ToString();
            return;
        }
        //Captcha Forgot password by Hussamuddin
        switch (ASPxComboBox1.Value.ToString())
        {
            case "0":
                SendPassword("spGetUserEmail");
                break;
            case "1":
                SendPassword("spGetClientEmail");
                break;
            case "2":
                SendPassword("spGetProviderEmail");
                break;

        }
    }
    protected void ASPxComboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ASPxComboBox1.Value.ToString())
        {
            case "0":
			Response.Redirect("https://member.bupa.com.sa/ForgetPassword.aspx");
                lblType.Text = "Membership Number:";
                _type = "M";
                break;
            case "1":
                lblType.Text = "User Name:";
                _type = "C";
                break;
            case "2":
                lblType.Text = "User Name:";
                _type = "P";
                break;
        }
    }
    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }
    protected void SendPassword( string _sp)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Environment.NewLine);
            builder.Append("Application Path: " + Request.ApplicationPath.ToString());
            builder.Append(Environment.NewLine);
            builder.Append("User Host: " + Request.UserHostAddress.ToString());
            builder.Append(Environment.NewLine);
            builder.Append("Referrer: " + Request.UrlReferrer.ToString());
            builder.Append(Environment.NewLine);
            builder.Append("Is Authenticated: " + Request.IsAuthenticated.ToString());
            builder.Append(Environment.NewLine);
            builder.Append("Is Secure: " + Request.IsSecureConnection.ToString());
            builder.Append(Environment.NewLine);
            builder.Append("Operating System:: " + Request.Browser.Platform);
            builder.Append(Environment.NewLine);
            builder.Append("Browser: " + Request.Browser.Browser);
            builder.Append(Environment.NewLine);
            builder.Append("Browser Version:" + Request.Browser.Version);
            builder.Append(Environment.NewLine);
            //Lable1.Text = builder.ToString(); 
            Logger.Current.Warning("User with membership number: " + txtNumber.Text + Environment.NewLine + " as User Type:" + ASPxComboBox1.Text.ToString() + Environment.NewLine + " tried to reset password with following info:" + Environment.NewLine + builder.ToString());
        }
        catch (Exception ex)
        {
			//Captcha Forgot password by Hussamuddin
            txtimgcode.Text = "";
            divCaptchaImage.Visible = true;
            WrongCaptcha.Style.Add("visibility", "hidden");
			//Captcha Forgot password by Hussamuddin
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();

        SqlConnection connection = null;
        string fullname = "";
        string MemberEmail = "";
        string MemberPassword = "";

        try
        {
            try
            {
                connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                //Captcha Forgot password by Hussamuddin
				txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                WrongCaptcha.Style.Add("visibility", "hidden");
				//Captcha Forgot password by Hussamuddin
                return;
            }

            SqlParameter[] arParms = new SqlParameter[5];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = txtNumber.Text;

            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 30);
            arParms[1].Direction = ParameterDirection.Output;

            arParms[2] = new SqlParameter("@Email", SqlDbType.NVarChar, 30);
            arParms[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, _sp, arParms);

            fullname = arParms[2].Value + "...." + arParms[1].Value;

            MemberEmail = arParms[2].Value.ToString();
            MemberPassword = arParms[1].Value.ToString();
            fp.Visible = false;
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "There are some problems while trying to use the Data Access Application block, please check the following error messages: {0} " + Environment.NewLine + "  This test requires some modifications to the database. Please make sure the database has been initialized using the SetUpDataBase.bat database script, or from the Install Quickstart option on the Start menu.";
            //Captcha Forgot password by Hussamuddin
			txtimgcode.Text = "";
            divCaptchaImage.Visible = true;
            WrongCaptcha.Style.Add("visibility", "hidden");
			//Captcha Forgot password by Hussamuddin
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        if (MemberEmail != "")
        {
            System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
            switch (ASPxComboBox1.Value.ToString())
            {
                case "0":
                    mailMsg = new System.Net.Mail.MailMessage("webmaster@bupame.com", MemberEmail, "Mail your password service - Bupa", "Dear Member,<br>Your password is <b>" + MemberPassword + "</b>.<br>It was a pleasure serving you.<br><br>Regards,<br>BUPA ARABIA.");
                    break;
                case "1":
                    mailMsg = new System.Net.Mail.MailMessage("webmaster@bupame.com", MemberEmail, "Mail your password service - Bupa", "Dear Bupa Client,<br>Your password is <b>" + MemberPassword + "</b>.<br>It was a pleasure serving you.<br><br>Regards,<br>BUPA ARABIA.");
                    break;
                case "2":
                    mailMsg = new System.Net.Mail.MailMessage("webmaster@bupame.com", MemberEmail, "Mail your password service - Bupa", "Dear Bupa Health Provider,<br>Your password is <b>" + MemberPassword + "</b>.<br>It was a pleasure serving you.<br><br>Regards,<br>BUPA ARABIA.");
                    break;
            }

            mailMsg.IsBodyHtml = true;

            System.Net.Mail.SmtpClient smptClient = new System.Net.Mail.SmtpClient();// .SmtpMail.Send("admin@bupame.com", );
            try
            {
                smptClient.Send(mailMsg);
                lblError.ForeColor = System.Drawing.Color.Teal;
                sucessMessage.InnerHtml ="Password has been sent successfully to your registered email id. <br />If you face any issues in receiving your password kindly send us an email to <strong>Web@bupa.com.sa</strong>";
            }
            catch (Exception ex)
            {
                lblError.Text = "Please contact the administrator for further details.";
				//Captcha Forgot password by Hussamuddin
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                WrongCaptcha.Style.Add("visibility", "hidden");
                //Captcha Forgot password by Hussamuddin
            }

        }
        else
        {
            lblError.ForeColor = System.Drawing.Color.Red;
            lblError.Text = "Authentication Failed. No such Member exist. Please retry.";
        }
        lblError.Visible = true;
    }
    protected void CaptchaValidate_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.Session["CaptchaImageText"] = GenerateRandomCode();
            CaptchaText.InnerText = this.Session["CaptchaImageText"].ToString();
            if (txtimgcode.Text == "" || txtimgcode.Text != "")
            {
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                return;
            }
        }
        catch (Exception ex) { }
    }

    private string GenerateRandomCode()
    {
        Random r = new Random();
        string s = "";
        for (int j = 0; j < 5; j++)
        {
            int i = r.Next(3);
            int ch;
            switch (i)
            {
                case 1:
                    ch = r.Next(0, 9);
                    s = s + ch.ToString();
                    break;
                case 2:
                    ch = r.Next(65, 90);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
                case 3:
                    ch = r.Next(97, 122);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
                default:
                    ch = r.Next(97, 122);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
            }
            r.NextDouble();
            r.Next(100, 1999);
        }
        return s;
    }
    
}