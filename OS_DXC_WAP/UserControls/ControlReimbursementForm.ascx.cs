﻿using Bupa.Core.Logging;
using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using OS_DXC_WAP.CaesarWS;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Configuration;

public partial class UserControls_ControlReimbursementForm : System.Web.UI.UserControl
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    private string _uploadCategory = UploadCategory.Submit_Reimbursement;
    private string strClientID;
    private string ClaimsToEmail;
    private string strContractNo;
    bool blGroupSecretary = false;
    string currentPhysicalPath = HttpContext.Current.Request.PhysicalPath;
    bool blnSabicMember = false;

    public string  virtualPath = string.Empty;
    public string prefixFileNames = string.Empty;
    public string fileNames = string.Empty;

    public string ServerIP = string.Empty;
    public string ClientIP = string.Empty;
    public string ClientBrowserDetails = string.Empty;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //this is temprary untill we recieve business response
            rfv_ddlBankCountry.Enabled = false;
            rfv_ddlBankName.Enabled = false;
            revIBAN.Enabled = false;
            rfvIBAN.Enabled = false;

            if (!string.IsNullOrEmpty(Session["ClientID"] as string))
            //if (!string.IsNullOrEmpty(Session["ClientID"].ToString()))
            {
                strContractNo = Session["ClientID"].ToString();
                blGroupSecretary = true;
            }
            lblError.Text = "";
            Member m = null;
            MemberHelper mh;

            string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SabicContractNo");
            string strConfigSecondSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SecondSabicContractNo");
            ClaimsToEmail = ConfigurationManager.AppSettings["ClaimsToEmail"];

            Page.LoadComplete += new EventHandler(Page_LoadComplete);

            if (Session["MembershipNo"] != null)
            {
                strClientID = Session["MembershipNo"].ToString();
                // Fetch the member's details to check whether SABIC or not. and if it is sabic to hide cheque option
                mh = new MemberHelper(strClientID, "");
                m = mh.GetMemberDetails();
            }
            else if (txtMembershipNumber.Text.Trim() != "")
            {
                strClientID = txtMembershipNumber.Text.Trim();
                // Fetch the member's details to check whether SABIC or not. and if it is sabic to hide cheque option
                mh = new MemberHelper(strClientID, "");
                m = mh.GetMemberDetails();




            }
            else if (Session["ClientID"] != null)
            {
                strClientID = Session["ClientID"].ToString();
                m = null;
            }

            //if (m != null)
            //{
            //    Session["IsVIP"] = m.VIP;
            //    //CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
            //    if ((Session["OnlineTicked"] != null && Session["OnlineTicked"] == "1") || (Session["IsVIP"] != null && Session["IsVIP"] == "1"))
            //        ;//Enable the Reimbursement Claim Submission
            //    else
            //        throw new Exception("Online Claim Reimbusement is not allowed for this member:" + strClientID);
            //}
            if (m != null && (strConfigSabicContractNo == m.ContractNumber || strConfigSecondSabicContractNo == m.ContractNumber))
            {
                blnSabicMember = true;
                //divBanks.Visible = false;
                //divBankCountry.Visible = false;
                ClaimsToEmail = ConfigurationManager.AppSettings["ClaimsToEmailSabic"];
                if (rblPaymentType.Items.Count == 2)
                    rblPaymentType.Items.RemoveAt(1);

                txtIBANNumber.Enabled = false;

                revIBAN.Enabled = false;
                rfvIBAN.Enabled = false;
                rfv_ddlBankCountry.Enabled = false;
                rfv_ddlBankName.Enabled = false;
                //txtBankName.Visible = false;
                divBanks.Visible = true;
                divBankCountry.Visible = true;
                txtBankName.Visible = true;

                txtBankCountry.Visible = true;
                txtBankName.Visible = true;
                txtBankCountry.Enabled = false;
                txtBankName.Enabled = false;
                ViewState["SabicMember"] = "Sabic";

            }
            else
            {
                divBanks.Visible = true;
                divBankCountry.Visible = true;
                txtIBANNumber.Enabled = true;
                revIBAN.Enabled = true;
                rfvIBAN.Enabled = true;
                txtBankCountry.Enabled = true;
                txtBankName.Enabled = true;

                //txtIBANNumber.Enabled = false;
                //revIBAN.Enabled = false;
                //rfvIBAN.Enabled = false;
                //txtBankCountry.Enabled = false;
                //txtBankName.Enabled = false;

                txtBankName.Visible = true;
                ViewState["SabicMember"] = "Non Sabic";
            }

            if (!this.IsPostBack)
            {
                ViewState[UploadPublication.Session_RequestID] = Guid.NewGuid().ToString();

                if (Session["ClientID"] != null)
                {
                    strClientID = Session["ClientID"].ToString();
                    divShowForClient.Visible = true;
                    divShowForClient2.Visible = true;

                    btnGetDetails.Visible = true;
                    ViewState["Type"] = "Client";
                }
                else if (Session["MembershipNo"] != null)
                {
                    strClientID = Session["MembershipNo"].ToString();
                    divShowForMember.Visible = true;
                    divShowForMember2.Visible = true;
                    divClear.Visible = false;
                    divClear2.Visible = true;
                    BindDependents(Session["MembershipNo"].ToString());
                    txtMembershipNumber.ReadOnly = true;
                    ViewState["Type"] = "Member";
                }

                else
                {
                    HttpContext.Current.Response.Redirect("~/Default.aspx");
                }
                txtMemberName.ReadOnly = true;
                txtCompanyName.ReadOnly = true;
                BindReasons();
                BindCurrencies();
                //BindDdlBankCountries(false);
            }
            else
            {
                if (ViewState["Invoices"] != null)
                {
                    List<ReimburesmentClaimInvoice> MyInvoices = new List<ReimburesmentClaimInvoice>();
                    MyInvoices = (List<ReimburesmentClaimInvoice>)ViewState["Invoices"];
                    gvInvoices.DataSource = MyInvoices;
                    gvInvoices.DataBind();
                }
            }
            SetupUploader(_uploadCategory);

            //if (!this.IsPostBack)
            //{

            //}

            if (m != null && m.ClaimPayToInd == "C")
            {
                divPaymentDetails.Visible = false;
                litPaymentDetails.Visible = true;

            }
            else
            {
                divPaymentDetails.Visible = true;
                litPaymentDetails.Visible = false;

            }
        }
        catch (Exception ex)
        {
            // Get stack trace for the exception with source file information
            var st = new StackTrace(ex, true);
            // Get the top stack frame
            var frame = st.GetFrame(0);
            // Get the line number from the stack frame
            var line = frame.GetFileLineNumber();

            string msgHeader = "Online Service Error report: ";

            sendMail("Sakthiyendran.N@bupa.com.sa", msgHeader,
              "\r\nMessage: " + ex.Message
              + "\r\nMemberNo: " + Iif(Session["MembershipNo"] != null, Session["MembershipNo"], txtMembershipNumber.Text.Trim())
              + "\r\nFile: " + currentPhysicalPath
              + "\r\nLine: " + line.ToString()
              + "\r\nLine: Stack trace:" + ex.StackTrace);
            //CR-248 Error handling
            if (ex.Message.Contains("Online Claim Reimbusement is not allowed for this"))
                lblError.Text = ex.Message;
            else
                lblError.Text = "Error when getting member details: Membership number is already terminated";

            Response.Write(msgHeader + ex.Message);
        }
    }

    public static object Iif(bool cond, object left, object right)
    {
        return cond ? left : right;
    }

    public void sendMail(String toemail, string strMailSubject, string strMailBody)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(toemail);
            mail.From = new MailAddress("donotreply@bupa.com.sa");
            mail.Subject = strMailSubject;
            mail.Body = strMailBody;
            SmtpClient smtp = new SmtpClient();
            smtp.UseDefaultCredentials = false;
            smtp.Send(mail);
            mail.Dispose();
        }
        catch (SmtpException ex)
        {
            throw ex;
        }
    }

    //private void BindDdlBankCountries(bool IsGCC)
    //{
    //    NationalityControl MyNationalityC = new NationalityControl();
    //    List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
    //    MyNationalities.Insert(0, new Nationality(0, "", "- Select -", false));
    //    MyNationalities.Insert(1, new Nationality(1, "966", "Saudi Arabia", false));

    //    ddlBankCountry.DataValueField = "Code";
    //    ddlBankCountry.DataTextField = "Nationality1";
    //    ddlBankCountry.DataSource = MyNationalities;
    //    ddlBankCountry.DataBind();
    //}

    protected void Page_LoadComplete(object sender, System.EventArgs e)
    {
        if (!this.IsPostBack)
        {
            ddlCurrencyCode.Items.Insert(0, new ListItem("Saudi Arabia – SAR", "Saudi Arabia – SAR"));
        }
        int intTotalRecords = uploader.FilesCount;
        if (intTotalRecords != 0)
            txtFileUploadNo.Text = intTotalRecords.ToString();
        else
            txtFileUploadNo.Text = "";
    }

    #region DataBinding

    private void BindReasons()
    {
        ReasonControl MyReasonC = new ReasonControl();
        List<Reason> MyReasons = new List<Reason>();
        MyReasons = MyReasonC.GetAllReasons();
        cblReasons.DataSource = MyReasons;
        cblReasons.DataBind();
    }

    private void BindCurrencies()
    {
        CurrencyControl MyCurrencyC = new CurrencyControl();
        List<Currency> MyCurrencies = MyCurrencyC.GetAllCurrencies();
        MyCurrencies = MyCurrencies.OrderBy(n => n.Name).ToList(); //Where(n => n.ID == MyReasons[i].ReasonID).ToList()[0];
        ddlCurrencyCode.DataSource = MyCurrencies;
        ddlCurrencyCode.DataBind();
    }

    private void BindDependents(string strMembershipNo)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        request.membershipNo = strMembershipNo;
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        request.transactionID = WebPublication.GenerateTransactionID();
        response = ws.EnqMbrListInfo(request);
        if (response.errorID != "0")
        {
        }
        else
        {
            if (response.status == "0")
            {
                ddlDependents.Items.Clear();
                ListItem DepList = new ListItem();
                DepList.Text = "Member / Dependents List";
                DepList.Value = "";
                ddlDependents.Items.Add(DepList);
                foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
                {
                    DepList = new ListItem();
                    DepList.Text = dtl.memberName + " -- " + dtl.relationship;
                    DepList.Value = dtl.membershipNo.ToString();
                    ddlDependents.Items.Add(DepList);
                }
            }
        }
    }

    protected void ddlDependents_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindMemberDetails(ddlDependents.SelectedValue);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    protected void btnGetDetails_Click(object sender, EventArgs e)
    {
        try
        {
            BindMemberDetails(txtMembershipNumber.Text.Trim());
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    bool Check_Member_belong_to_client(string strMemberContractNo)
    {
        if (blGroupSecretary == false)
        {
            return true;
        }
        if (strContractNo == strMemberContractNo)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void BindMemberDetails(string MembershipNumber)
    {
        try
        {
            if (MembershipNumber != "")
            {
                txtMembershipNumber.Text = MembershipNumber;
                MemberHelper MyMemberH = new MemberHelper(MembershipNumber, "");
                Member MyMember = MyMemberH.GetMemberDetails();

                string strConfigSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SabicContractNo");
                string strConfigSecondSabicContractNo = CoreConfiguration.Instance.GetConfigValue("SecondSabicContractNo");
                if (MyMember != null && (strConfigSabicContractNo != MyMember.ContractNumber && strConfigSecondSabicContractNo != MyMember.ContractNumber))
                {
                    Session["IsVIP"] = MyMember.VIP;
                    //CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
                    if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() == "Y") || (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP"))
                    {
                    }
                    else if (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP")
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
                        {

                        }
                        else
                        {
                            if (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP")
                            {
                                ;
                            }
                            else
                            {
                                lblError.Text = "Online Claim Reimbusement is not allowed for this member:" + strClientID;
                                throw new Exception("Online Claim Reimbusement is not allowed for this member:" + strClientID);
                            }
                        }
                    }
                    else
                    {
                        lblError.Text = "Online Claim Reimbusement is not allowed for this member:" + strClientID;
                        throw new Exception("Online Claim Reimbusement is not allowed for this member:" + strClientID);
                    }
                    //if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() == "Y") || (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP"))
                    //    ;//Enable the Reimbursement Claim Submission
                    //else
                    //{
                    //    lblError.Text = "Online Claim Reimbusement is not allowed for this member:" + strClientID;
                    //    throw new Exception("Online Claim Reimbusement is not allowed for this member:" + strClientID);
                    //}
                }

                if (Check_Member_belong_to_client(MyMember.ContractNumber))
                {
                    txtCompanyName.Text = MyMember.CompanyName;
                    txtMemberName.Text = MyMember.MemberName;

                    txtEmail.Text = MyMember.EmployeeEmail;
                    txtMobileNumber.Text = MyMember.EmployeeMobile;
                    txtIBANNumber.Text = MyMember.EmployeeIBAN;

                    ViewState["IBAN"] = MyMember.EmployeeIBAN;
                    //ddlBankCountry.Items.Clear();
                    //txtBankName.Items.Clear();

                    //ddlBankCountry.Items.Add(new ListItem(MyMember.BankCountry, MyMember.BankCountry));
                    txtBankCountry.Text = MyMember.BankCountry;
                    if (txtBankCountry.Text.Trim() == "")
                        txtBankCountry.Text = "Saudi Arabia";
                    //txtBankName.Items.Add(new ListItem(MyMember.BankName, MyMember.BankName));
                    txtBankName.Text = MyMember.BankName;

                    if (blnSabicMember)
                    {
                        if (txtBankName.Text.Trim() == "")
                            txtBankName.Text = "-";
                    }
                }
                else
                {
                    lblError.Text = "Inputted 'Membership No' does not exist in your group.";
                }
            }
            else
            {
                txtMembershipNumber.Text = "";
                txtCompanyName.Text = "";
                txtMemberName.Text = "";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Invoice
    protected void btnAddInvoice_Click(object sender, EventArgs e)
    {
        ReimburesmentClaimInvoice MyInvoice = new ReimburesmentClaimInvoice();
        List<ReimburesmentClaimInvoice> MyInvoices = new List<ReimburesmentClaimInvoice>();
        if (hfInvoiceIndex.Value == "0") //Insert Mode
        {
            MyInvoice.NumberOfInvoices = short.Parse(txtNumberOfInvoices.Text);

            int index = txtTotalAmount.Text.IndexOf(".");
            if (index > 0)
                txtTotalAmount.Text = txtTotalAmount.Text.Substring(0, index);

            MyInvoice.TotalAmount = int.Parse(txtTotalAmount.Text);
            MyInvoice.CurrencyCode = ddlCurrencyCode.SelectedValue;

            if (ViewState["Invoices"] != null)
            {
                MyInvoices = (List<ReimburesmentClaimInvoice>)ViewState["Invoices"];
            }


        }
        else //Edit Mode
        {
            if (ViewState["Invoices"] != null)
            {
                MyInvoices = (List<ReimburesmentClaimInvoice>)ViewState["Invoices"];
                MyInvoice = MyInvoices[int.Parse(hfInvoiceIndex.Value)];

                MyInvoice.NumberOfInvoices = short.Parse(txtNumberOfInvoices.Text);
                MyInvoice.TotalAmount = int.Parse(txtTotalAmount.Text);
                MyInvoice.CurrencyCode = ddlCurrencyCode.SelectedValue;

                MyInvoices[int.Parse(hfInvoiceIndex.Value)] = MyInvoice;

            }
        }

        MyInvoices.Add(MyInvoice);
        ViewState["Invoices"] = MyInvoices;
        gvInvoices.DataSource = MyInvoices;
        gvInvoices.DataBind();
        gvInvoices.Visible = true;
        txtNumberOfInvoices.Text = "";
        txtTotalAmount.Text = "";
        ddlCurrencyCode.SelectedIndex = 0;
        txtGridView.Text = "1";
    }
    protected void gvInvoices_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            ((LinkButton)e.Row.FindControl("btnDelete")).CommandArgument = e.Row.RowIndex.ToString();
            ((LinkButton)e.Row.FindControl("btnDelete")).CommandName = "delete";

        }
    }
    protected void gvInvoices_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = int.Parse(e.CommandArgument.ToString());
        string mode = e.CommandName;
        List<ReimburesmentClaimInvoice> MyInvoices = new List<ReimburesmentClaimInvoice>();
        MyInvoices = (List<ReimburesmentClaimInvoice>)ViewState["Invoices"];
        if (mode == "delete")
        {
            MyInvoices.RemoveAt(index);
            ViewState["Invoices"] = MyInvoices;
            if (MyInvoices.Count == 0)
            {
                txtGridView.Text = "";
            }
            gvInvoices.DataSource = MyInvoices;
            gvInvoices.DataBind();
        }
    }
    protected void gvInvoices_RowEditing(object sender, GridViewEditEventArgs e) { }
    protected void gvInvoices_RowDeleting(object sender, GridViewDeleteEventArgs e) { }
    #endregion

    private void SetupUploader(string uploadCategory)
    {
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        // Bind to allow resolving of # tags in mark-up
        DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (chkAgree.Checked)
        {
            ReimburesmentClaim MyClaim = new ReimburesmentClaim(1, txtMemberName.Text, txtMembershipNumber.Text, txtCompanyName.Text,
                                                                txtMobileNumber.Text, txtEmail.Text, short.Parse(rblPaymentType.SelectedValue),
                                                                txtBankName.Text, txtIBANNumber.Text,
                                                                WebPublication.GenerateTransactionID().ToString());
            MyClaim.ID = MyClaim.Add();
            List<ReimburesmentClaimReason> MyReasons = new List<ReimburesmentClaimReason>();
            ReimburesmentClaimReason MyReason;

          
            for (int i = 0; i < cblReasons.Items.Count; i++)
            {
                ListItem MyItem = cblReasons.Items[i];
                if (MyItem.Selected == true)
                {
                    if (MyItem.Value != "6")
                    {
                        MyReason = new ReimburesmentClaimReason(int.Parse(MyItem.Value), MyClaim.ID, "");
                    }
                    else
                    {
                        MyReason = new ReimburesmentClaimReason(int.Parse(MyItem.Value), MyClaim.ID, txtOtherReason.Text);
                    }
                    MyReasons.Add(MyReason);
                }
            }
            MyReasons.InsertReimburesmentClaimReasonList<ReimburesmentClaimReason>();
            List<ReimburesmentClaimInvoice> MyInvoices = new List<ReimburesmentClaimInvoice>();
            ReimburesmentClaimInvoice MyInvoice;
            for (int i = 0; i < gvInvoices.Rows.Count; i++)
            {
                GridViewRow MyRow = gvInvoices.Rows[i];
                MyInvoice = new ReimburesmentClaimInvoice(short.Parse(MyRow.Cells[0].Text),
                                                        int.Parse(MyRow.Cells[1].Text),
                                                        MyRow.Cells[2].Text, MyClaim.ID);
                MyInvoices.Add(MyInvoice);
            }
            MyInvoices.InsertReimburesmentClaimInvoiceList<ReimburesmentClaimInvoice>();
            UpdateMemberBasicInfo();
            bool MailSentToBupa = false;

            if (ViewState["Type"].ToString().ToUpper() == "member".ToUpper())
            {
               
                MailSentToBupa = SendEmailToBupa(MyClaim.MemberName, MyClaim, MyReasons, MyInvoices, uploader.SessionID, true);
            }
            else if (ViewState["Type"].ToString().ToUpper() == "client".ToUpper())
            {
                MailSentToBupa = SendEmailToBupa(Session["ClientName"].ToString(), MyClaim, MyReasons, MyInvoices, uploader.SessionID, false);
            }



            if (MailSentToBupa)
            {
                SendEmailToMember(MyClaim.Email, MyClaim.MemberName, MyClaim.ReferenceNumber);
                pnlForm.Visible = false;
                imgSuc.Visible = true;
                lblMessage.Text = "Your initial reference is " + MyClaim.ReferenceNumber + ". You could track the status of your claim in the Claim History section where you could get the voucher number that serves as your final reference for this claim. Please quote the voucher number when contacting the customer services.";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                ////imgSuc.Visible = false;
                ////lblMessage.Text = "Please upload the required documents to be able to submit your claim.";
                ////lblMessage.Visible = true;
                ////lblMessage.ForeColor = System.Drawing.Color.Red;

                if (!MailSentToBupa)
                {
                    ////pnlForm.Visible = false;
                    ////imgSuc.Visible = true;
                    ////lblMessage.Text = "Your initial reference is " + MyClaim.ReferenceNumber + ". You could track the status of your claim in the Claim History section where you could get the voucher number that serves as your final reference for this claim. Please quote the voucher number when contacting the customer services.";
                    ////lblMessage.Visible = true;
                    ////lblMessage.ForeColor = System.Drawing.Color.Black;

                    imgSuc.Visible = false;
                    lblMessage.Text = "There was a problem sending the email. Please note the following for future refferences:" + Environment.NewLine +
                        "Your initial reference is " + MyClaim.ReferenceNumber + ". You could track the status of your claim in the Claim History section where you could get the voucher number that serves as your final reference for this claim. Please quote the voucher number when contacting the customer services."; ;
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Black; 
                }
                else
                {
                    imgSuc.Visible = false;
                    lblMessage.Text = "Please upload the required documents to be able to submit your claim.";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }


            }
        }
        else
        {
            lblMessage.Text = "You must agree to the terms and conditions in order to proceed.";
            imgSuc.Visible = false;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    private void UpdateMemberBasicInfo()
    {
        ServiceDepot_DNService ws;
        OS_DXC_WAP.CaesarWS.EnqUpdMbrInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqUpdMbrInfoResponse_DN response;
        request = new EnqUpdMbrInfoRequest_DN();
        ws = new ServiceDepot_DNService();

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        request.transactionID = WebPublication.GenerateTransactionID();
        request.membershipNo = txtMembershipNumber.Text;
        request.mbr_Email = txtEmail.Text;
        request.mbr_MobileNo = txtMobileNumber.Text;
        if (ViewState["IBAN"] != null)
            request.mbr_IBAN = ViewState["IBAN"].ToString();
        else
            request.mbr_IBAN = "";

        response = ws.EnqUpdMbrInfo(request);
    }

    private void SendEmailToMember(string Email, string MemberName, string TransactionNumber)
    {
        MailMessage mailMsg = new MailMessage();
        mailMsg.To.Add(Email);
        //////////////////mailMsg.To.Add("sakthiyendran.n@bupa.com.sa");

        mailMsg.From = new MailAddress("donotreply@bupa.com.sa");
        mailMsg.Subject = "Bupa Claims - " + TransactionNumber;
        mailMsg.Body = "Dear " + MemberName + ",<br/><br/>" +
                        "Thank you for submitting your reimbursement claim through online.<br/><br/>" +
                        "Your initial reference is " + TransactionNumber + ".  You could track the status of your claim in the Claim History section where you could get" +
                        " the voucher number that serves as your final reference for this claim.  Please quote the voucher number when contacting the customer services.<br/><br/><br/>" +
                        "Yours sincerely<br/>" +
                        "Customer Services<br/>" +
                        "Bupa Arabia";
        mailMsg.IsBodyHtml = true;

        EmailTracking emailTracking = new EmailTracking();
        EmailTracking objEmailTracking = new EmailTracking();

        objEmailTracking.ReimburesmentClaimID = TransactionNumber;
        objEmailTracking.EmailType = "Reimburesment Claim : Client";
        objEmailTracking.FromEmail = "donotreply@bupa.com.sa";
        objEmailTracking.ToEmail = Convert.ToString(mailMsg.To);
        objEmailTracking.MailSubject = mailMsg.Subject;
        objEmailTracking.MailBody = mailMsg.Body;
        objEmailTracking.IsSend = false;
        objEmailTracking.ActionType = "I";
        objEmailTracking.AttachmentFiles = prefixFileNames;
        objEmailTracking.VirtualPath = virtualPath;
        objEmailTracking.CreatedBy = strClientID;
        objEmailTracking.FileNames = fileNames;
        objEmailTracking.ClaimsType = Convert.ToString(ViewState["SabicMember"]);
        objEmailTracking.TrackingID = 0;
        objEmailTracking.Status = "Submitted";
        objEmailTracking.SubmissionType = Convert.ToString(ViewState["Type"]);
        objEmailTracking.ServerIP = ServerIP;
        objEmailTracking.ClientBrowserDetails = ClientBrowserDetails;
        objEmailTracking.ClientIP = ClientIP;
        objEmailTracking.Source = "OS-Old";

        emailTracking.InsertUpdateMailTracking(objEmailTracking);


        SmtpClient smptClient = new System.Net.Mail.SmtpClient();
        smptClient.Send(mailMsg);
        mailMsg.Dispose();

        objEmailTracking.ActionType = "U";
        objEmailTracking.IsSend = true;
        emailTracking.InsertUpdateMailTracking(objEmailTracking);
    }

    private bool SendEmailToBupa(string SenderName, ReimburesmentClaim MyClaim, List<ReimburesmentClaimReason> MyReasons, List<ReimburesmentClaimInvoice> MyInvoices, string SessionID, bool isMember)
    {

        if (Session["MembershipNo"] != null)
        {
            strClientID = Session["MembershipNo"].ToString();
        }
        else if (Session["ClientID"] != null)
        {
            strClientID = Session["ClientID"].ToString();
        }

        MailMessage mailMsg = new MailMessage();
        mailMsg.To.Add(ClaimsToEmail);
        //////////////////mailMsg.To.Add("sakthiyendran.n@bupa.com.sa");
        mailMsg.From = new MailAddress("donotreply@bupa.com.sa");
        //Changed the Subject as per Business request mail dated  : 17 Dec 2015
        mailMsg.Subject ="Online - " + SenderName + " - " + MyClaim.ReferenceNumber + "   " + MyClaim.CompanyName +".";
        mailMsg.Body = "Dear Team,<br/><br/>" +
                        "<ul><li>Reference Number: " + MyClaim.ReferenceNumber + "</li>" +
                        "<li>Membership Number: " + MyClaim.MembershipNumber + "</li>" +
                        "<li>Company: " + MyClaim.CompanyName + "</li>" +
                        "<li>Mobile Number: " + MyClaim.MobileNumber + "</li>" +
                        "<li>Email: " + MyClaim.Email + "</li>" +
                        "<li>User Type: " + ViewState["Type"].ToString() + "</li>" +
                        "<li>Username: " + strClientID + "</li>" +
                        "<li>Reasons: <ul>";
       
        ReasonControl MyReasonC = new ReasonControl();
        List<Reason> AllReasons = MyReasonC.GetAllReasons();
        Reason MyReason = new Reason();
        for (int i = 0; i < MyReasons.Count; i++)
        {
            MyReason = AllReasons.Where(n => n.ID == MyReasons[i].ReasonID).ToList()[0];
            if (MyReason.ID == 6)
            {
                mailMsg.Body += "<li>" + MyReason.Name + ": " + MyReasons[i].Other + "</li>";
            }
            else
            {
                mailMsg.Body += "<li>" + MyReason.Name + "</li>";
            }
        }

        if (MyClaim.PaymentType == 1)
        {
            mailMsg.Body += "</ul><li>Payment Option: EFT (Electronic Fund Transfer)</li>" +
                            "<li>Bank Name:" + MyClaim.BankName + "</li>";
            if (ViewState["IBAN"] != null && ViewState["IBAN"].ToString() != txtIBANNumber.Text.Trim())
                mailMsg.Body += "<li>IBAN No (New IBAN has been submitted) : " + MyClaim.IbanNumber + "</li>";
            else
                mailMsg.Body += "<li>IBAN No: " + MyClaim.IbanNumber + "</li>";
        }
        else if (MyClaim.PaymentType == 2)
        {
            mailMsg.Body += "</ul><li>Payment Option: Cheque</li>";
        }

        mailMsg.Body += "<li>Submitted Date & Time: " + DateTime.Now + "</li>";
        mailMsg.Body += "</ul><br/><br/><table cellpadding='0' cellspacing='0' border='1' style='width: 400px;text-align: center;'>";
        mailMsg.Body += "<tr style='background-color: rgb(0, 153, 255);color: #fff;'><td>No. of Invoices</td><td>Currency Code</td><td>Total Amount</td></tr>";
        for (int i = 0; i < MyInvoices.Count; i++)
        {
            mailMsg.Body += "<tr><td>" + MyInvoices[i].NumberOfInvoices + "</td><td>" + MyInvoices[i].CurrencyCode + "</td><td>" + MyInvoices[i].TotalAmount + "</td></tr>";
        }
        mailMsg.Body += "</table><br/>";

        mailMsg.Body += "Yours sincerely<br/>" +
                        "Customer Services<br/>" +
                        "Bupa Arabia";
        mailMsg.IsBodyHtml = true;
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        ////DataSet d = uploadManager.GetUploadedFileSet(SessionID, Bupa.OSWeb.Helper.UploadCategory.Submit_Reimbursement);
        StringCollection attachments = new StringCollection();

        DataTable dt = new DataTable();
        uploader.FindControl("dvFiles");
        GridView gv = new GridView();
        gv = (GridView)uploader.FindControl("dvFiles");
        dt = (DataTable)gv.DataSource;

        ////if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)

        virtualPath = string.Empty;
        prefixFileNames = string.Empty;
        fileNames = string.Empty;
        


        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow r in dt.Rows)
            {
                string attachPath = r["VirtualPath"].ToString();
                virtualPath += attachPath + "|" ;
                prefixFileNames += r["DocName"].ToString() + "|";
                fileNames += r["FriendlyDocName"].ToString() + "|";

                attachments.Add(attachPath);
            }

            virtualPath = !string.IsNullOrEmpty(virtualPath.Trim()) ? virtualPath.Remove(virtualPath.Length - 1) : string.Empty;
            prefixFileNames = !string.IsNullOrEmpty(prefixFileNames.Trim()) ? prefixFileNames.Remove(prefixFileNames.Length - 1) : string.Empty;
            fileNames = !string.IsNullOrEmpty(fileNames.Trim()) ? fileNames.Remove(fileNames.Length - 1) : string.Empty;
        }

        ////////if (attachments != null)
        ////////{
        ////////    foreach (string path in attachments)
        ////////    {
        ////////        // Create the attachment
        ////////        System.Net.Mail.Attachment attachment;
        ////////        //////if (System.IO.File.Exists(Server.MapPath(path)))
        ////////        //////{
        ////////        //////    attachment = new System.Net.Mail.Attachment(Server.MapPath(path));
        ////////        //////    // Add the attachment
        ////////        //////    mailMsg.Attachments.Add(attachment);
        ////////        //////}

        ////////        if (System.IO.File.Exists(path))
        ////////        {
        ////////            attachment = new System.Net.Mail.Attachment(path);
        ////////            // Add the attachment
        ////////            mailMsg.Attachments.Add(attachment);
        ////////        }
        ////////    }

        ////////    if (mailMsg.Attachments.Count == 0)
        ////////    {                
        ////////        return false;
        ////////    }
        ////////}
        ////////else
        ////////{   
        ////////    return false;
        ////////}

        EmailTracking emailTracking = new EmailTracking();
        EmailTracking objEmailTracking = new EmailTracking();

        ServerIP = GetServerIPAddress();
        ClientBrowserDetails = GetWebBrowserName();

        objEmailTracking.ReimburesmentClaimID = MyClaim.ReferenceNumber;
        objEmailTracking.EmailType = "Reimburesment Claim : BUPA Team";
        objEmailTracking.FromEmail = "donotreply@bupa.com.sa";
        objEmailTracking.ToEmail = Convert.ToString(mailMsg.To);
        objEmailTracking.MailSubject = mailMsg.Subject;
        objEmailTracking.MailBody = mailMsg.Body;
        objEmailTracking.IsSend = false;
        objEmailTracking.ActionType = "I";
        objEmailTracking.AttachmentFiles = prefixFileNames;
        objEmailTracking.VirtualPath = virtualPath;
        objEmailTracking.CreatedBy = strClientID;
        objEmailTracking.FileNames = fileNames;
        objEmailTracking.ClaimsType = Convert.ToString(ViewState["SabicMember"]);
        objEmailTracking.TrackingID = 0;
        objEmailTracking.Status = "Submitted";
        objEmailTracking.SubmissionType = isMember ? "Member" : "Client";
        objEmailTracking.ServerIP = ServerIP;
        objEmailTracking.ClientBrowserDetails = ClientBrowserDetails;
        objEmailTracking.ClientIP = ClientIP;
        objEmailTracking.Source = "OS-Old";

        emailTracking.InsertUpdateMailTracking(objEmailTracking);

        ///MyInvoiceEmail.InsertUpdateMailTracking(objEmailTracking);


        //////SmtpClient smptClient = new System.Net.Mail.SmtpClient();
        //////smptClient.Send(mailMsg);
        //////mailMsg.Dispose();

        //////objEmailTracking.ActionType = "U";
        //////objEmailTracking.IsSend = true;
        //////emailTracking.InsertUpdateMailTracking(objEmailTracking);

        return true;
    }

    public string GetServerIPAddress()
    {
        string serverIP = string.Empty;

        try
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName()); // `Dns.Resolve()` method is deprecated.
            IPAddress ipAddress = ipHostInfo.AddressList[0];

            serverIP =  ipAddress.ToString();
        }
        catch(Exception ex)
        {
            Logger.Current.WriteException(ex);
        }
        return serverIP;
    }

    public string GetWebBrowserName()
    {
        string webBrowserName = string.Empty;
        try
        {
            webBrowserName = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version;
        }
        catch (Exception ex)
        {
              Logger.Current.WriteException(ex);
        }
        return webBrowserName;
    }

}