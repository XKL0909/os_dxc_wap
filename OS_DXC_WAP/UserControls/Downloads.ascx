﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Downloads" Codebehind="Downloads.ascx.cs" %>
 <div class="margBottom25">
        <h2>Downloads</h2>
        <table border="0" cellpadding="0" cellspacing="0"  width="100%">
         <tr><td>
            <asp:DataList ID="DataList1" runat="server" Width="100%" onitemcreated="DataList1_ItemCreated">
           
                <ItemTemplate>
                
                    <tr>
                        <td>
                            <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label></td>
                        
                                    <td class="sizeField"><asp:Label ID="lblSize" runat="server" Text='<%# Bind("size") %>'></asp:Label></td>
                                    <td class="downloadsSep">&nbsp;</td>
                                    <td><a href="javascript:;"><asp:Image ID="imgIcon" ImageUrl='<%# Bind("icon") %>' runat="server" /></a></td>
                                    <td class="downloadsSep">&nbsp;</td>
                                    <td><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Bind("openlink") %>' CssClass="downloadLink" Text=""></asp:HyperLink></td>
                        
                    </tr>
                    
                </ItemTemplate>
                <SelectedItemStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                
            </asp:DataList>
            </td></tr>
             <tr>
                                <td class="gotoinbox" align="right">
                                    <a href="http://www.bupa.com.sa/English/AboutUs/MediaCenter/Pages/Downloads.aspx" >More ...</a></td>
                            </tr>
        </table>

 </div>

 
<asp:XmlDataSource ID="XmlDataSourceTips" runat="server"></asp:XmlDataSource>
