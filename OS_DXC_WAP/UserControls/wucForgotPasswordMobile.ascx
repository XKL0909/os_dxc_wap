﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_wucForgotPasswordMobile" Codebehind="wucForgotPasswordMobile.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>


<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>


<style type="text/css">
    .style1
    {
        width: 150px;
    }
    .style2
    {
        height: 30px;
    }
</style>


<div class="memberLoginComp" id="entryForm">
 <asp:ScriptManager ID="scriptManager" runat="server" />
            <table width="85%" border="0" cellspacing="0" cellpadding="0" align="center" id="fp" runat="server">
                <tr>
                    <td class="loginLeftSection" style="width:70%">
                    <div class="loginLeftSectionPad">
                        <asp:UpdatePanel runat="server" ID="updatePanel">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnLogin" />
    </Triggers>
    <ContentTemplate>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                                <td class="style1">
                                   
                                </td>
                                <td class="loginTitle" colspan="2">
                                    Forgot Password</td>
                            </tr>
                            <tr>
                                <td class="style1" valign="top" rowspan="2" align="center">
                                    &nbsp;</td>
                                <td class="loginLabel" colspan="2" valign="top">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style2" valign="top" >
                                    <asp:Label ID="lblType" runat="server" Text="Membership Number:"></asp:Label>
                                </td>
                        	    <td>
                                    <dx:ASPxTextBox ID="txtNumber" runat="server" Width="170px" 
                                        CssClass="loginTextField">
                                         <ValidationSettings RequiredField-IsRequired="true" ErrorDisplayMode="ImageWithTooltip" ErrorText="Field is required" />
                                        </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="style1" rowspan="2" valign="top">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    
                                    <asp:Button ID="btnLogin"  runat="server" 
                                        onclick="btnLogin_Click" CssClass="submitButton" Text="Submit" 
                                        CausesValidation="False"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    
    </ContentTemplate>
</asp:UpdatePanel>
                    </div>
                    </td>
                    <td class="loginLeftSection" align="center">
                        <table>
                            <tr>
                                <td>
                                    <img src="../images/User-icon.png" style="height: 78px; width: 81px" /></td>
                            </tr>
                            <tr>
                                <td>We will send the password to your registered email address</td>
                            </tr>
                        </table></td>
                    </tr>
                    </table>
                    <p align="center">
                                    <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#cc0000" />
                                </p>
                    </div>
                    

                    <script type="text/javascript">
                        $(document).ready(function () {
                            toggleFooter();
                        });

                        $(function () {
                            $('#slider,#Helpslider').anythingSlider();
                        });
</script>

    <script language="javascript" type="text/javascript">

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
        });

        prm.add_endRequest(function () {
            $('#entryForm').unblock();
        });

        // Progress indicator preloading.
        var preload = document.createElement('img');
        preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
        delete preload;

    </script>
