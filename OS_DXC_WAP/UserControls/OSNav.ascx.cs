﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class UserControls_OSNav : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
		//Aramco PID Changes By Hussamuddin
        string AramcoContractType = ConfigurationManager.AppSettings["AramcoContractType"].ToString().ToUpper();
        if (Convert.ToString(Session["ContractType"]).ToUpper() == AramcoContractType)
        {
            Client.Visible = false;
            AramcoClient.Visible = true;
            Member.Visible = false;
            Provider.Visible = false;

            aABlueNavChangeclass.HRef = "../client/changebranch.aspx?val=" + Cryption.Encrypt("optiontype=Class");
            aABlueNavInvoice.HRef = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Invoice");
            aABlueNavStatement.HRef = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Statement");

            if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() == "Y") || (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP"))              
                ReimbursementForm.Visible = true;
            else
                ReimbursementForm.Visible = false;
        }
        else
        {
            AramcoClient.Visible = false;
            //Aramco PID Changes By Hussamuddin

            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                Member.Visible = true;
                Client.Visible = false;
                Provider.Visible = false;
                if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() == "Y") || (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP"))
                    ReimbursementForm.Visible = true;
                else
                    ReimbursementForm.Visible = false;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {

                ////CR Enabling Deletion even warning 
                ////Start

                aBlueNavDeleteDependent.HRef = "../client/DeleteEmployeeEnhanced.aspx?val=" + Cryption.Encrypt("EType=Dep");
                aBlueNavChangeclass.HRef = "../client/changebranch.aspx?val=" + Cryption.Encrypt("optiontype=Class");
                aBlueNavStatement.HRef = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Statement");
                aBlueNavInvoice.HRef = "../client/creditcontrol1.aspx?val=" + Cryption.Encrypt("optiontype=Invoice");


                string warning = string.Empty;
                warning = string.IsNullOrEmpty(Convert.ToString(Session["WarningEnable"])) ? string.Empty : Convert.ToString(Session["WarningEnable"]);

                if (warning.Trim().ToLower().Equals("warning"))
                {
                    tdAddEmployeeDependent.Visible = false;
                    tdChangeclass.Visible = false;
                    tdChangeBranch.Visible = false;


                tdNewAdditionGuide.Visible = true;
                tdTrackinfo.Visible = true;
                tdSubmitRequired.Visible = true;
                tdReplaceCard.Visible = true;
                tdDeleteEmployee.Visible = true;
                tdDeleteDependent.Visible = true;
                trGsMemberDetails.Visible = true;
            }

            ////End
           




            

			Member.Visible = false;
            Client.Visible = true;
            Provider.Visible = false;
            if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() == "Y") || (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP"))                
                ReimbursementForm.Visible = true;
            else
                ReimbursementForm.Visible = false;
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
        {
            Member.Visible = false;
            Client.Visible = false;
            Provider.Visible = true;

            DataSet _ProPer = GetProviderPermission(Convert.ToString(Session["ProviderUserName"]));
            foreach (DataRow dr in _ProPer.Tables[0].Rows)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(dr["NewRequest"])))
                    Option1.Visible = Convert.ToBoolean(dr["NewRequest"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["ExistingRequest"])))
                    Option2.Visible = Convert.ToBoolean(dr["ExistingRequest"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["CheckNewReply"])))
                    Option3.Visible = Convert.ToBoolean(dr["CheckNewReply"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["TableOfBenefits"])))
                    Option4.Visible = Convert.ToBoolean(dr["TableOfBenefits"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["EClaimsSubmission"])))
                    Option5.Visible = Convert.ToBoolean(dr["EClaimsSubmission"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["Verification"])))
                    Option6.Visible = Convert.ToBoolean(dr["Verification"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["ServiceCode"])))
                    Option7.Visible = Convert.ToBoolean(dr["ServiceCode"]);
















            }
            using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
            



			{
                string ProviderId = Convert.ToString(Session["ProviderID"]);
                ProviderPhysicianMaster objProviderPhysicianMaster = objTruestedDoctors.ProviderPhysicianMasters.Where(item => item.PM_ProviderId.Trim() == ProviderId).SingleOrDefault();
                if (objProviderPhysicianMaster != null && objProviderPhysicianMaster.PM_MaintenanceRequired == true && objProviderPhysicianMaster.PM_Status == true)
                {
                    Option8.Visible = true;
                }
            }



















            //CR-248 Put the logic to check Contract level info for Onlineticked field and Add it in session
            if ((Session["OnlineTicked"] != null && Session["OnlineTicked"].ToString() != "N") || (Session["IsVIP"] != null && Session["IsVIP"].ToString() == "VP"))
                ReimbursementForm.Visible = true;
            else
                ReimbursementForm.Visible = false;

            //Enable the Reimbursement Claim Submission

            //// CR-353 Provider statement
            if (Session["StmInd"] != null && Convert.ToString(Session["StmInd"]) == "Y")
            {
                imgProviderStatmentIndicator.Src = "~/images/icons/common/AccountStatementIndicator.png";                
                imgProviderStatmentIndicator.Height = 64;
                imgProviderStatmentIndicator.Width = 64;
            }
            else
                imgProviderStatmentIndicator.Src = "~/images/icons/common/AccountStatement.jpg";





































        }
  


  }

}
    protected DataSet GetProviderPermission(string userName)
    {
        DataSet ds = new DataSet();


        try
        {



            /*DataParameter UserName = new DataParameter(SqlDbType.NVarChar, "@proUsername", userName, ParameterDirection.Input, 30);

            DataParameter[] dpBasket = new DataParameter[] { UserName };
            
            ds = DataManager.ExecuteStoredProcedureCachedReturn(WebPublication.Key_ConnBupa_OS, "spGetProviderPermission", ref dpBasket, 60000, true);
            */
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            SqlCommand cmd = new SqlCommand("spGetProviderPermission", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@proUsername", userName);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

        }
        catch (Exception ex)
        {

            throw;
        }

        return ds;
    }


    protected void btnDiabetes_Click(object sender, ImageClickEventArgs e)
    {

    }
}