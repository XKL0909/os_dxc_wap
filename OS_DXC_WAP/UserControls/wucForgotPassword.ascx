﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_wucForgotPassword" Codebehind="wucForgotPassword.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>


<%@ Register assembly="DevExpress.Web.ASPxEditors.v12.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>


<style type="text/css">
    .style1
    {
        width: 150px;
    }
    .style2
    {
        height: 30px;
    }
</style>


<div class="memberLoginComp" id="entryForm">
    <asp:ScriptManager ID="scriptManager" runat="server" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" id="fp" runat="server">
        <tr>
            <td style="width:64%">
                <div>
                    <asp:UpdatePanel runat="server" ID="updatePanel">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="CaptchaValidate" />
                        </Triggers>
                        <ContentTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="loginTitle" colspan="2">Forgot Password</td>
                                </tr>
                                <tr>
                                    <td valign="middle" style="text-align: right; padding-right: 10px;">Are You?</td>
                                    <td style="padding-top: 18px;">
                                        <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" AutoPostBack="true"
                                            CssClass="loginListField" IncrementalFilteringMode="Contains"
                                            OnSelectedIndexChanged="ASPxComboBox1_SelectedIndexChanged" SelectedIndex="0"
                                            ValueType="System.String">
                                            <Items>

                                                <dx:ListEditItem Selected="True" Text="Group Secretary" Value="1" />
                                                <dx:ListEditItem Text="Provider" Value="2" />
                                              
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" style="text-align: right; padding-right: 10px; padding-bottom: 19px;">
                                        <span style="color: #ff0000">*</span><asp:Label ID="lblType" runat="server" Text="Membership Number:"></asp:Label>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtNumber" runat="server" Width="170px" CssClass="loginTextField">
                                        </dx:ASPxTextBox>
                                        <asp:RequiredFieldValidator ID="ReqValtxtNumber" runat="server" ControlToValidate="txtNumber" ErrorMessage="Field is required" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div id="divCaptchaImage" runat="server" style="padding-left:29px">
                                            <span style="color: #ff3300"><strong>*</strong></span><span style="text-align: right; padding-right: 10px;">Insert Below Text </span>
                                            <asp:TextBox ID="txtimgcode" runat="server" onchange="HideCaptchaMessage()" Style="width: 166px; height: 22px;"></asp:TextBox>
                                            <br />
                                            <asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" ControlToValidate="txtimgcode" ErrorMessage="Field is required" Style="padding-left: 111px;" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            <br />
                                            <span id="WrongCaptcha" runat="server" style="color: #ff3300; padding-left: 111px; visibility: hidden"><strong>Wrong entry try again</strong></span>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div id="CaptchaText" runat="server" style="padding-top: 15px; width: 302px; height: 60px; background-image: url('../images/CaptchaBackground.png'); text-align: center; font-size: 40px; color: #61C0E0; font-weight: bold; letter-spacing: 3px; vertical-align: middle; font-family: 'GenericSansSerif'; font-style: italic;">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <asp:ImageButton ID="CaptchaValidate" runat="server" ImageUrl="../images/icons/reCaptchaImg.jpg" OnClick="CaptchaValidate_Click" Style="width: 81px" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                            </table>
                                            <br />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
					<div>
                        <table>
                            <tr>
                                <td></td>
                                <td style="text-align: right; padding-left: 277px;">
                                    <br />
                                    <asp:Button ID="btnLogin" runat="server" CausesValidation="true" CssClass="submitButton" OnClick="btnLogin_Click" Text="Submit" ValidationGroup="Submit" />
                                </td>
                            </tr>
                        </table>
                    </div>
                       
            </td>
            <td  style="border-right:1px solid #DDDDDD; width:36%" align="center">
                <table>
                    <tr>
                        <td>
                            <img src="../images/User-icon.png" style="height: 78px; width: 81px" /></td>
                    </tr>
                    <tr>
                        <td>We will send the password to your registered email address</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p align="center">
        <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#cc0000" />
        
    </p>
    <div id="sucessMessage" runat="server"></div>
</div>
                    

<script type="text/javascript">
    $(document).ready(function () {
        toggleFooter();
    });

    $(function () {
        $('#slider,#Helpslider').anythingSlider();
    });
</script>

<script type="text/javascript">

    prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_beginRequest(function () {
        $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
        });

        prm.add_endRequest(function () {
            $('#entryForm').unblock();
        });

        // Progress indicator preloading.
        var preload = document.createElement('img');
        preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
        delete preload;

        function HideCaptchaMessage() {
            document.getElementById('<%= WrongCaptcha.ClientID %>').style.visibility = "hidden";
        }
</script>
