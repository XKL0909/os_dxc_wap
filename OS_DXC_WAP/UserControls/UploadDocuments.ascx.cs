﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserControls_UploadDocuments : System.Web.UI.UserControl
{
    string vUploadedDocumentsPath = "/bupaaccess/ReimbursementNew/UploadedDocuments/";
    string vFileName;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Script", "initialize()", true);

            if (!IsPostBack)
            {
                Session["DocumentsTable"] = null;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void rptrDocuments_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataTable dtDocumentsTable = (DataTable)Session["DocumentsTable"];
                ((Label)e.Item.FindControl("lblDocumentName")).Text = dtDocumentsTable.Rows[e.Item.ItemIndex]["DocumentName"].ToString(); ;
                ((LinkButton)e.Item.FindControl("lBtnDelete")).CommandName = "Delete";
                ((LinkButton)e.Item.FindControl("lBtnDelete")).CommandArgument = e.Item.ItemIndex.ToString();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void lBtnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            //System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Script", "initialize()", true);

            divDocumentsTable.Visible = true;

            DataTable dtDocumentsTable = GetDocumentsTable();
            rptrDocuments.DataSource = dtDocumentsTable;
            rptrDocuments.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private DataTable GetDocumentsTable()
    {
        DataTable dtDocumentsTable = null;
        if (Session["DocumentsTable"] == null)
        {
            dtDocumentsTable = new DataTable("DocumentsTable");
            dtDocumentsTable.Columns.Add(new DataColumn("DocumentName", typeof(string)));

            Session["DocumentsTable"] = dtDocumentsTable;
        }
        else
        {
            dtDocumentsTable = (DataTable)Session["DocumentsTable"];
        }
        if (CheckFile())
        {
            DataRow drRow = dtDocumentsTable.NewRow();
            drRow["DocumentName"] = vFileName;
            dtDocumentsTable.Rows.Add(drRow);
            Session["DocumentsTable"] = dtDocumentsTable;
        }
        else
        {
            divVaidatorErrorMessageFileUpload.InnerHtml = "Invalid file type";
        }
        return dtDocumentsTable;
    }

    private Boolean CheckFile()
    {
        Boolean fileOK = false;
        Boolean mimeOK = false;
        String path = Server.MapPath(vUploadedDocumentsPath);
        if (FileUploader.HasFile)
        {
            String fileExtension = System.IO.Path.GetExtension(FileUploader.FileName).ToLower();

            //Added new code to check MimeType
            string fileExtention = System.IO.Path.GetExtension(FileUploader.PostedFile.FileName).ToLower();
            
            CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
            string mime = findMimeFromDate.CheckFindMimeFromData(FileUploader.PostedFile);
            String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
            
            
            String[] allowedExtensions = { ".pdf", ".jpg", ".jpeg", ".gif", ".png", ".bmp" };
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtension == allowedExtensions[i])
                {
                    fileOK = true;
                    break;
                }
            }
            for (int i = 0; i < allowedMime.Length; i++)
            {
                if (mime == allowedMime[i])
                {
                    mimeOK = true;
                    break;
                }
            }
        }

        if (fileOK && mimeOK)
        {
            try
            {
                vFileName = System.IO.Path.GetFileNameWithoutExtension(path + FileUploader.FileName) + DateTime.UtcNow.ToString("-ddMMyyyymmss") + "" + System.IO.Path.GetExtension(path + FileUploader.FileName);
                FileUploader.PostedFile.SaveAs(path + vFileName);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        else
        {
            return false;
        }


    }

    protected void rptrDocuments_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            //System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Script", "initialize()", true);

            if (e.CommandName == "Delete")
            {
                DataTable dtDocumentsTable = (DataTable)Session["DocumentsTable"];
                dtDocumentsTable.Rows.RemoveAt(Convert.ToInt32(e.CommandArgument));
                rptrDocuments.DataSource = dtDocumentsTable;
                rptrDocuments.DataBind();
                Session["DocumentsTable"] = dtDocumentsTable;
                divDocumentsTable.Visible = (dtDocumentsTable.Rows.Count > 0);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}