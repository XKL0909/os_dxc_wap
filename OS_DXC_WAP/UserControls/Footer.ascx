﻿<%@ Control Language="VB" AutoEventWireup="false" %>
<style type="text/css">
    .footerContectBlock {
        font-weight: bold;
        font-family: Arial;
        float: right;
        background: url(../images/contact-icon.png) left 10px no-repeat;
        padding-left: 50px;
        color: #fff;
        border-collapse: separate;
        border-spacing: 2px;
        border-color: gray;
    }

        .footerContectBlock .listingRow {
            border-collapse: separate;
            border-spacing: 2px;
            border-color: gray;
            font-weight: bold;
            font-family: Arial;
            display: inline-block;
            background: url(../images/contact-separator.gif) right center no-repeat;
            padding-right: 12px;
            margin-right: 10px;
            line-height: 24px;
            font-size: 18px;
            color: #fff;
        }
</style>
<div class="footerContainer">
    <table width="995" align="center" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td class="footerContainerPad">
                    <div class="footerButton"><a href="javascript:toggleFooter();" id="footerLink"></a></div>
                    <div id="footerLinks">

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="https://bupa.com.sa/business" target="">Bupa for Businesses</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="https://bupa.com.sa/business/bupa-corporate" target="">Bupa Corporate - For 100 lives and above</a></li>
                                                    <li><a href="https://bupa.com.sa/business/bupa-business" target="">Bupa Business - For less than 100 lives</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="https://bupa.com.sa/family/bupa-saudi-family" target="">Bupa For Saudi Families</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="https://buy.bupa.com.sa/default.aspx?Product=Family" target="">Buy Now</a></li>
                                                    <li><a href="https://bupa.com.sa/family/bupa-saudi-family" target="">Bupa Family – Newlyweds Program</a></li>
                                                    <li><a href="https://bupa.com.sa/family/bupa-saudi-family" target="">Bupa Family Inpatient</a></li>
                                                    <li><a href="https://bupa.com.sa/family/bupa-saudi-family" target="">Bupa Family Program</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="https://bupa.com.sa/family/expat-individual" target="">Bupa for Individuals</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="https://bupa.com.sa/family/expat-individual" target="">Overview</a></li>
                                                    <li><a href="https://bupa.com.sa/family/expat-individual" target="">Schemes</a></li>
                                                    <li><a href="https://bupa.com.sa/family/expat-individual" target="">Why Bupa Individuals?</a></li>
                                                    <li><a href="https://bupa.com.sa/family/expat-individual" target="">Get a Quote</a></li>
                                                    <li><a href="https://buy.bupa.com.sa/default.aspx?Product=Individual" target="">Buy Now</a></li>
                                                    <li><a href="https://bupa.com.sa/family/expat-individual" target="">Hospital Network</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="https://bupa.com.sa/family/bupa-domestic" target="">Bupa for Domestic Help</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="https://bupa.com.sa/family/bupa-domestic" target="">Overview</a></li>
                                                    <li><a href="https://bupa.com.sa/family/bupa-domestic" target="">Schemes</a></li>
                                                    <li><a href="https://bupa.com.sa/family/bupa-domestic" target="">Why Bupa Domestic?</a></li>
                                                    <li><a href="https://bupa.com.sa/register" target="">Get a Quote</a></li>
                                                    <li><a href="https://buy.bupa.com.sa/default.aspx?Product=Domestic%20Help" target="">Buy Now</a></li>
                                                    <li><a href="https://bupa.com.sa/family/bupa-domestic" target="">Hospital Network</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div style="clear: both"></div>
                                        <%--
                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="http://bupa.com.sa/English/HealthandWellness/Pages/default.aspx" target="">Health and Wellbeing</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="http://bupa.com.sa/English/HealthandWellness/HealthInformation/Pages/default.aspx" target="">Health Information</a></li>
                                                    <li><a href="http://bupa.com.sa/English/HealthandWellness/Pages/Bupa-Access.aspx" target="">Bupa Arabia Application</a></li>
                                                    <li><a href="http://bupa.com.sa/English/BupaInitiatives2/Pages/default.aspx" target="">Bupa Initiative</a></li>
                                                    <li><a href="http://bupa.com.sa/English/HealthandWellness/App-of-the-week/Pages/default.aspx" target="">App of the week</a></li>
                                                    <li><a href="http://bupa.com.sa/English/HealthandWellness/HealthTools/Pages/default.aspx" target="">Health Tools</a></li>
                                                      Disabled As per CR Raised on 24-Jan-2016 Start
                                                    <li><a href="http://bupa.com.sa/English/HealthandWellness/MembersExclusive/Pages/default.aspx" target="">Member Exclusives</a></li>
                                                    End 
                                                </ul>
                                            </div>
                                        </div>
                                        --%>
                                        <%--
                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="http://bupa.com.sa/English/BupaInitiatives2/Pages/default.aspx" target="">Initiatives</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="http://bupa.com.sa/English/BupaInitiatives2/Pages/HealthLounges.aspx" target="">The Lounge </a></li>
                                                    <li><a href="http://bupa.com.sa/English/BupaInitiatives2/Pages/Health-Lounge-at-Panorama-mall.aspx" target="">The Health Lounge at Panorama mall</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        --%>
                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="https://bupa.com.sa/about-us" target="">About Us</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="https://bupa.com.sa/about-us" target="">Profile</a></li>
                                                    <li><a href="https://bupa.com.sa/about-us#sustainbility" target="">Sustainability</a></li>
                                                    <li><a href="https://bupa.com.sa/en/news-detail/" target="">Media Center</a></li>
                                                    <li><a href="https://bupa.com.sa/customer-care" target="">Feedback</a></li>
                                                    <li><a href="https://bupa.com.sa/about-us" target="">Contact Us</a></li>
                                                    <li><a href="https://bupa.com.sa/about-us#csrVideos" target="">CSR</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="https://bupa.com.sa/customer-care" target="">Customer Care</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="https://bupa.com.sa/customer-care/faqs" target="">FAQs</a></li>
                                                    <li><a href="https://bupa.com.sa/customer-care" target="">Feedback Forms</a></li>
                                                    <li><a href="https://bupa.com.sa/about-us" target="">Our Online Presence</a></li>
                                                    <li><a href="https://bupa.com.sa/customer-care/report-a-fraud-or-abuse" target="">Report an Abuse or Fraud</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="footerSection">
                                            <div class="footerMap1"><a href="https://buy.bupa.com.sa/" target="">Buy Now</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="https://buy.bupa.com.sa/default.aspx?Product=Family" target="">Bupa For Saudi Families</a></li>
                                                    <li><a href="https://buy.bupa.com.sa/default.aspx?Product=Individual" target="">Bupa for Individuals</a></li>
                                                    <li><a href="https://buy.bupa.com.sa/default.aspx?Product=Domestic Help" target="">Bupa for Domestic Help</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="footerSection" style="float: right; style: display:none">
                                            <div class="footerMap1"><a href="https://bupa.com.sa/investor" target="">Investor Relations</a></div>
                                            <div class="footerMap2">
                                                <ul>
                                                    <li><a href="https://bupa.com.sa/investor" target="">An introduction to investment in the Company</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">Corporate Governance</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">Board Committees Terms of Reference</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">Policy &amp; Procedures Related to Disclosure &amp; Transparency </a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">Board Members</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/financial-info" target="">Financial Statement &amp; Board Reports</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">General Assembly Meeting Minutes</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">Company Announcements</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">Bupa Page on Tadawul</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">Bylaws</a></li>
                                                    <li><a href="https://bupa.com.sa/investor" target="">Bupa Prospectus</a></li>
                                                    <li><a href="https://bupa.com.sa/investor/governance" target="">Major Stakeholders</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div style="clear: both"></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
						
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("a").each(function (idx) {
                                    var buyURL = "https://buy.bupa.com.sa";
                                    var BupaURL = "http://www.bupa.com.sa";
                                    var lnk;
                                    if (true) {
                                        //Show Buy-Now & Get Quote
                                        if ($(this).attr('href') != null) {
                                            lnk = $(this).attr('href');
                                            if (lnk.toLowerCase().indexOf("onlinepurchase1.bupa.com.sa") >= 0) {
                                                $(this).attr('href', lnk.toLowerCase().replace("http://onlinepurchase1.bupa.com.sa", "https://buy.bupa.com.sa"));
                                            }
                                            //   if ((lnk.toLowerCase().indexOf("/apply-now.aspx") >= 0) && (lnk.toLowerCase().indexOf("/bupabusiness/") < 0) && (lnk.toLowerCase().indexOf("/bupacorporate/") < 0)) {
                                            //        $(this).hide();
                                            //        if ($(this).parents("div.subMenuLevel2").length) {
                                            //            $(this.previousSibling).remove();
                                            //        }
                                            //        $(this).parents("li").hide();
                                            //    }

                                            if (lnk.toLowerCase().indexOf("/buy-now.aspx") >= 0) {
                                                if (lnk.toLowerCase().indexOf("/bupafordomestichelp/") >= 0) {
                                                    buyURL += "/default.aspx?Product=Domestic%20Help";
                                                }
                                                else if (lnk.toLowerCase().indexOf("/bupaforindividuals/") >= 0) {
                                                    buyURL += "/default.aspx?Product=Individual";
                                                }
                                                else if (lnk.toLowerCase().indexOf("/bupaforsaudifamily/") >= 0) {
                                                    buyURL += "/default.aspx?Product=Family";
                                                }

                                                if (lnk.toLowerCase().indexOf("/arabic/") >= 0) {
                                                    $(this).attr("href", buyURL + "&lang=ar")
                                                }
                                                else {
                                                    $(this).attr("href", buyURL)
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        //Hide Buy-Now & Get Quote
                                        if ($(this).attr('href') != null) {
                                            lnk = $(this).attr('href');
                                            if (lnk.toLowerCase().indexOf("onlinepurchase1.bupa.com.sa") >= 0) {
                                                $(this).attr('href', lnk.toLowerCase().replace("http://onlinepurchase1.bupa.com.sa", "https://buy.bupa.com.sa"));
                                            }
                                            if (lnk.toLowerCase().indexOf("/buy-now.aspx") >= 0) {
                                                $(this).hide();
                                                if ($(this).parents("div.subMenuLevel2").length) {
                                                    $(this.previousSibling).remove();
                                                }
                                                $(this).parents("li").hide();
                                                if ($(this).parents("td.smartMenuLinks").length) {
                                                    $(this).attr('class', 'applyNowBtn');

                                                    if (lnk.toLowerCase().indexOf("/arabic/") >= 0) {
                                                        BupaURL += "/Arabic/";
                                                    }
                                                    else {
                                                        BupaURL += "/English/";
                                                    }

                                                    if (lnk.toLowerCase().indexOf("/bupafordomestichelp/") >= 0) {
                                                        BupaURL += "Products/BupaforDomesticHelp/Pages/Apply-Now.aspx";
                                                    }
                                                    else if (lnk.toLowerCase().indexOf("/bupaforindividuals/") >= 0) {
                                                        BupaURL += "Products/BupaforIndividuals/Pages/Apply-Now.aspx";
                                                    }
                                                    else if (lnk.toLowerCase().indexOf("/bupaforsaudifamily/") >= 0) {
                                                        BupaURL += "Products/BupaForSaudiFamily/BupaFamilyProgram/Pages/Apply-Now.aspx";
                                                    }

                                                    $(this).attr("href", BupaURL);

                                                    $(this).show();
                                                }

                                            }

                                            if (lnk.toLowerCase().indexOf("/onlinepurchase1.bupa.com.sa/") >= 0) {
                                                $(this).hide();
                                                if ($(this).parents("div.subMenuLevel2").length) {
                                                    $(this.previousSibling).remove();
                                                }
                                                $(this).parents("li").hide();
                                            }

                                            if (lnk.toLowerCase().indexOf("/get-a-quote.aspx") >= 0) {
                                                $(this).hide();
                                                if ($(this).parents("div.subMenuLevel2").length) {
                                                    $(this.previousSibling).remove();
                                                }
                                                $(this).parents("li").hide();
                                            }

                                            $(".needToOptions option").each(function (idx) {
                                                if (lnk.toLowerCase().indexOf("/buy-now.aspx") >= 0) {
                                                    $(this).hide();
                                                }
                                                if (lnk.toLowerCase().indexOf("/get-a-quote.aspx") >= 0) {
                                                    $(this).hide();
                                                }
                                            });
                                        }
                                    }
                                });
                            });

		     
                        </script>

                    </div>
                    <div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>

                                        <script type="text/javascript">
                                            function validate(email) {
                                                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                                                if (reg.test(email) == false) {
                                                    alert('please fill in a valid email');
                                                    return false;
                                                }
                                            }
                                        </script>
                                        <table border="0" align="right" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <img src="/images/newsletter-pic.jpg" alt="">
                                                    </td>
                                                    <td width="10">
                                                        <img src="/images/spacer.gif" alt="" width="10" height="1">
                                                    </td>
                                                    <td>
                                                        <div class="progressdiv" style="min-height: 10px !important;">
                                                            <div id="ctl00_JoinNewsletter1_UpdateProgress1" style="display: none;">

                                                                <div class="loader">
                                                                </div>
                                                                <div class="loaderimage">
                                                                    <img alt="" src="/images/ajax-loader.gif">
                                                                </div>

                                                            </div>
                                                            <div id="ctl00_JoinNewsletter1_uplPanel">

                                                                <div class="newsletterTitle"
                                                                    style="font-size: 16px; color: #FFF; margin-bottom: 10px; font-family: Arial;">
                                                                    E - SEHATY Newsletter
                                                                </div>
                                                                <div id="ctl00_JoinNewsletter1_pnlNewsLetter" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_JoinNewsletter1_btnSubscribe')">

                                                                    <div id="ctl00_JoinNewsletter1_divForm">
                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="newsletterField">
                                                                                            <input name="ctl00$JoinNewsletter1$txtEmail" type="text" value="Enter your email" id="ctl00_JoinNewsletter1_txtEmail" onkeydown="if(event.keyCode == 13){if(this.value=='Enter your email' || this.value==''){alert('Please fill in your email');return false;}validate(this.value);}" onfocus="if(this.value=='Enter your email'){this.value='';}" onblur="if(this.value==''){this.value='Enter your email';}">
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="submit" name="ctl00$JoinNewsletter1$btnSubscribe" value="" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$JoinNewsletter1$btnSubscribe&quot;, &quot;&quot;, true, &quot;newsletter&quot;, &quot;&quot;, false, false))" id="ctl00_JoinNewsletter1_btnSubscribe" class="subscribeButton">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div>
                                                                        <span id="ctl00_JoinNewsletter1_rev1" style="color: Red; display: none;"></span>
                                                                        <span id="ctl00_JoinNewsletter1_RequiredFieldValidator1" style="color: Red; display: none;"></span>
                                                                        <div id="ctl00_JoinNewsletter1_ValidationSummary1" style="color: Red; display: none;">
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                    <td>


                                        <div id="ctl00_FooterContact1_divEn" class="footerContectBlock">
                                            <div class="listingRow">
                                                Medical Team<br>
                                                800 440 4040
                                            </div>
                                            <div class="listingRow">
                                                Customer Service<br>
                                                9 2002 3009
                                            </div>
                                            <div class="listingRow last">
                                                Sales<br>
                                                920 000 456
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="copyrightContainer">
    <table width="995" align="center" border="0" cellspacing="0" cellpadding="0" class="footerLinks">
        <tbody>
            <tr>
                <td class="copyrightContainerPad">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td>Copyright © Bupa Arabia 2013. All rights reserved.
						<a href="https://bupa.com.sa/privacy-policy" target="">Terms &amp; Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://bupa.com.sa/sitemap" target="">Sitemap</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.bupa-intl.com" target="_blank">Bupa International</a>&nbsp;&nbsp;
                                    <span class="grey">|&nbsp;&nbsp;</span><a href="https://bupa.com.sa/business/bupa-arabia-international-health-plan" id="ctl00_FooterLinks_aLinkEnglish">Bupa International</a>

                                </td>
                                <td align="right">Designed and developed by <a target="_blank" href="https://bupa.com.sa/">Bupa</a></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
