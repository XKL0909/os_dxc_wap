﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class UserControls_Header : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) || 
            !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])) || !string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
        {
            lbLogin.Text = "Logout";
            lbReg.Text = "Services";
        }
        else
        {
            lbLogin.Text = "&nbsp;Login&nbsp;&nbsp;";
            lbReg.Text = "&nbsp;&nbsp;Register&nbsp;&nbsp;&nbsp;";
        }

        String strCookieName = "userLogin";

        HttpCookie cookie = HttpContext.Current.Request.Cookies[strCookieName];

        /*if (cookie == null)
        {
            Session.Clear();
            lbLogin.Text = "&nbsp;Login&nbsp;&nbsp;";
            lbReg.Text = "&nbsp;&nbsp;Register&nbsp;&nbsp;&nbsp;";
        }*/
       

    }
    protected void lbLogin_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) ||
            !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])) || !string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])) ||
            !string.IsNullOrEmpty(Convert.ToString(Request.Cookies["userLogin"])))
        {
            HttpCookie usrCookie = new HttpCookie("userLogin");
            usrCookie.Domain = ".bupa.com.sa";
            usrCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(usrCookie);
            
            if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
                {
                    BLCommon.UpdateClientLoginStatus(Convert.ToInt32(Session["ClientRefNo"]), false);
                    BLCommon.UpdateClientLogOff(Convert.ToInt32(Session["CurrentLogID"]));
                }
            }
            if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
                {
                    BLCommon.UpdateProviderLoginStatus(Convert.ToInt32(Session["pRefNo"]), false);
                    BLCommon.UpdateProviderLogOffTime(Convert.ToInt32(Session["ProCurrentLogID"]));
                }
            }
            Session.Clear();
            Session.Abandon();
            Response.Redirect("~/default.aspx");
        }
        else
        {
            Response.Redirect("~/default.aspx");
        }
    }
    protected void lbReg_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) || 
            !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])) || !string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"]))  )
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                Response.Redirect("~/member/main.aspx");
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {
                Response.Redirect("~/client/main.aspx");
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            {
                Response.Redirect("~/provider/main.aspx");
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
            {
                Response.Redirect("~/broker/default.aspx");
            }
            
        }
        else
        {
            if (GetCurrentPageName().ToLower() == "registration.aspx")
            {
                Response.Redirect("Registration.aspx");
            }
            else
            {
                Response.Redirect("~/reg/Registration.aspx");
            }
            
        }
    }

    public string GetCurrentPageName()
    {
        string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }
}