﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Header" Codebehind="Header.ascx.cs" %>

<style type="text/css">
    .mainMenu, .mainMenu:visited, .mainMenu:link, .membersLink, .membersLink:link, .membersLink:visited, .mainMenuHighlight {
        display: inline-block;
        font-size: 12px;
        color: #FFF;
        text-decoration: none;
        background: url(../externalResource/menu-back.gif) repeat-x;
        padding: 7px 11px 7px 12px;
        max-width: 62px;
        text-align: left;
    }

    #mainmenuLink2, #mainmenuLink4 {
        max-width: 96px;
    }

    .membersLink, .membersLink:link, .membersLink:visited {
        padding: 14px 11px 14px 12px;
        text-align: center;
    }

    .posRelative {
        position: relative;
    }

    .membersMenuSep {
        background: url(../externalResource/image.gif) no-repeat -428px top;
    }

    .membersLink, .membersLink:link, .membersLink:visited {
        font-weight: bold;
        background: url(../externalResource/membersmenu-back.gif) repeat-x;
    }

    .mainMenuHighlight {
        display: inline-block;
        font-size: 12px;
        color: #FFF;
        text-decoration: none;
        background: url(~/images/menu-back.gif) repeat-x;
    }
</style>

<div class="my-header">

    <table width="1020" align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="headerContainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <a href="https://bupa.com.sa/">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/logo-new.jpg" />
                            </a>
                            <td class="rightAlignment">
                                <table border="0" cellspacing="0" cellpadding="0" align="right">
                                    <tr>
                                        <td>
                                            <div class="topMenuContainer">
                                                <a href="https://careers.bupa.com.sa/" target="">Careers</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://bupa.com.sa/customer-care" target="">Customer Care</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://bupa.com.sa/about-us#sustainbility" target="">Sustainability</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://bupa.com.sa/about-us" target="">Contact Us</a>&nbsp;&nbsp;&nbsp;<a href="https://bupa.com.sa/en/" id="ctl00_TopLinks1_aEnglish" class="selected">English</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://bupa.com.sa/" id="ctl00_TopLinks1_aArabic"><span class="arabicText">عربي</span></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padBottom10">
                                            <table border="0" cellspacing="0" cellpadding="0" align="right">
                                                <tr>
                                                    <td>
                                                        <div class="callUsIcon">
                                                            <img src="/images/spacer.gif" alt="" width="1" height="1">
                                                        </div>
                                                    </td>
                                                    <td class="callUsNumber">Call us on <span dir="ltr">920 000 456</span></td>
                                                    <td class="valignMiddle">
                                                        <div class="needToList">
                                                            <select name="ctl00$QuickLinks1$ddlQuickLinks" id="ctl00_QuickLinks1_ddlQuickLinks" class="needToOptions myNeedToDropdown">
                                                                <option value="0" external="false">I need to</option>
                                                                <option value="https://bupa.com.sa/customer-care/make-a-complaint" external="false">Make a Complaint</option>
                                                                <option value="https://bupa.com.sa/family" external="false">Insure my Family</option>
                                                                <option value="https://bupa.com.sa/business" external="false">Get Products Information</option>
                                                                <option value="https://bupa.com.sa/about-us#branch" external="false">Reach your Branches</option>
                                                                <%--<option value="http://www.bupa.com.sa/English/HealthandWellness/MobileApplications/Pages/default.aspx" external="false">Download Apps</option>
                                                                <option value="http://www.bupa.com.sa/English/HealthandWellness/HealthInformation/Pages/default.aspx" external="false">Get Health Information</option>--%>

                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td width="10">
                                                        <img src="/images/spacer.gif" alt="" width="10" height="1"></td>
                                                    <td class="searchContainer">

                                                        <script type="text/javascript">
                                                            function validateSearchTop() {
                                                                if (document.getElementById('ctl00_Search1_txtSearch').value == '' || document.getElementById('ctl00_Search1_txtSearch').value == 'Enter a keyword') {
                                                                    alert('Please enter a keyword');
                                                                    return false;
                                                                }
                                                                return true;
                                                            }
                                                        </script>

                                                        <script type="text/javascript">
                                                            function dosearch(searchText)
                                                            {
                                                                
                                                                window.location.href = "https://bupa.com.sa/search-result?indexCatalogue=bupa&searchQuery=" + searchText + "&wordsMode=0";
                                                            }                                                            
                                                        </script>

                                                        <div id="ctl00_Search1_pnlSearch" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_Search1_btnSearch2')">

                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="searchTextField">
                                                                        <input name="txtSearch" type="text" value="Enter a keyword" id="txtSearch" onkeypress="dosearch1();" onfocus="if(this.value=='Enter a keyword'){this.value='';}" onblur="if(this.value==''){this.value='Enter a keyword';}" />
                                                                    </td>
                                                                    <td style="font-size: 0;">
                                                                        <input type="button" name="btnSearch2" value="" onclick="dosearch(document.getElementsByName('txtSearch')[0].value);" id="btnSearch2" class="searchButton" />
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </div>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="posRelative"><a href="https://bupa.com.sa/" target="_self" class="homeLink selected selected"><span class="menuArrow"></span></a></div>
                                                    </td>
                                                    <td class="mainMenuSep">
                                                        <img src="/images/spacer.gif" alt="" width="1" height="1"></td>
                                                    <td>
                                                        <div class="posRelative higherZindex">
                                                            <a href="https://bupa.com.sa/business" class="mainMenu " target="" id="mainmenuLink1" onmouseout="MM_showHideLayers('sub1','','hide');document.getElementById('mainmenuLink1').className='mainMenu ';" onmouseover="adjustOverlayPosition('#sub1');MM_showHideLayers('sub1','','show');document.getElementById('mainmenuLink1').className='mainMenuHighlight';">Bupa for Businesses<span class="menuArrow"></span></a><div class="absoluteTopSubMenu" id="sub1" onmouseout="MM_showHideLayers('sub1','','hide');document.getElementById('mainmenuLink1').className='mainMenu ';" onmouseover="MM_showHideLayers('sub1','','show');document.getElementById('mainmenuLink1').className='mainMenuHighlight';" style="left: -1px; visibility: hidden;">
                                                                <div class="absoluteTopSubMenuPad">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/business/bupa-corporate">Bupa Corporate - For 100 lives and above</a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="https://bupa.com.sa/business/bupa-corporate">Overview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/business/bupa-corporate">Schemes</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/business/bupa-corporate">Why Bupa Corporate?</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/register">Apply Now</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="#">Hospital Network</a></div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/business/bupa-business">Bupa Business - </a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="https://bupa.com.sa/business/bupa-business">Overview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/business/bupa-business">Schemes</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/business/bupa-business">Why Bupa Business?</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/register">Apply Now</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="#">Hospital Network</a></div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="mainMenuSep">
                                                        <img src="/images/spacer.gif" alt="" width="1" height="1"></td>
                                                    <td>
                                                        <div class="posRelative higherZindex">
                                                            <a href="https://bupa.com.sa/family/bupa-saudi-family" class="mainMenu " target="" id="mainmenuLink2" onmouseout="MM_showHideLayers('sub2','','hide');document.getElementById('mainmenuLink2').className='mainMenu ';" onmouseover="adjustOverlayPosition('#sub2');MM_showHideLayers('sub2','','show');document.getElementById('mainmenuLink2').className='mainMenuHighlight';">Bupa For Saudi Families<span class="menuArrow"></span></a><div class="absoluteTopSubMenu" id="sub2" onmouseout="MM_showHideLayers('sub2','','hide');document.getElementById('mainmenuLink2').className='mainMenu ';" onmouseover="MM_showHideLayers('sub2','','show');document.getElementById('mainmenuLink2').className='mainMenuHighlight';" style="left: -1px; visibility: hidden;">
                                                                <div class="absoluteTopSubMenuPad">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://buy.bupa.com.sa/default.aspx?Product=Family">Buy Now</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/bupa-saudi-family">Bupa Family – Newlyweds Program</a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Apply Now</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="buyquote" href="https://bupa.com.sa/family/bupa-saudi-family">Get Quote</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Schemes</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Hospital Network</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/bupa-saudi-family">Bupa Family Inpatient</a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Overview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Schemes</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Why Bupa Family?</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="buyquote" href="https://bupa.com.sa/family/bupa-saudi-family">Get a Quote</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="buyquote" href="https://buy.bupa.com.sa/default.aspx?Product=Family">Buy Now<sup class="new">New</sup></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Apply Now</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Hospital Network</a></div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <div class="subMenuLevel1"><a href="http://www.bupa.com.sa/English/BupaForSaudiFamily/BupaFamilyProgram/Pages/default.aspx">Bupa Family Program</a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Overview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Schemes</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Why Bupa Family?</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="buyquote" href="https://bupa.com.sa/family/bupa-saudi-family">Get a Quote</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="buyquote" href="https://buy.bupa.com.sa/default.aspx?Product=Family">Buy Now<sup class="new">New</sup></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-saudi-family">Hospital Network</a></div>
                                                                                    </div>
                                                                                </td>
                                                                                <td width="15">
                                                                                    <img src="/images/spacer.gif" alt="" width="15" height="1"></td>
                                                                                <td width="1">
                                                                                    <img src="../externalResource/saudifamilysmall22.jpg" style="max-width: 190px" alt=""></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="mainMenuSep">
                                                        <img src="/images/spacer.gif" alt="" width="1" height="1"></td>
                                                    <td>
                                                        <div class="posRelative higherZindex">
                                                            <a href="https://bupa.com.sa/family/expat-individual" class="mainMenu " target="" id="mainmenuLink3" onmouseout="MM_showHideLayers('sub3','','hide');document.getElementById('mainmenuLink3').className='mainMenu ';" onmouseover="adjustOverlayPosition('#sub3');MM_showHideLayers('sub3','','show');document.getElementById('mainmenuLink3').className='mainMenuHighlight';">Bupa for Individuals<span class="menuArrow"></span></a><div class="absoluteTopSubMenu" id="sub3" onmouseout="MM_showHideLayers('sub3','','hide');document.getElementById('mainmenuLink3').className='mainMenu ';" onmouseover="MM_showHideLayers('sub3','','show');document.getElementById('mainmenuLink3').className='mainMenuHighlight';" style="left: -121px; visibility: hidden;">
                                                                <div class="absoluteTopSubMenuPad">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/expat-individual">Overview</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/expat-individual">Schemes</a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="https://bupa.com.sa/family/expat-individual">Bronze</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/expat-individual">Green</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/expat-individual">Why Bupa Individuals?</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/expat-individual">Get a Quote</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://buy.bupa.com.sa/default.aspx?Product=Individual">Buy Now</a></div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/expat-individual">Hospital Network</a></div>
                                                                                    </div>
                                                                                </td>
                                                                                <td width="15">
                                                                                    <img src="/images/spacer.gif" alt="" width="15" height="1"></td>
                                                                                <td width="1"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="mainMenuSep">
                                                        <img src="/images/spacer.gif" alt="" width="1" height="1"></td>
                                                    <td>
                                                        <div class="posRelative higherZindex">
                                                            <a href="https://bupa.com.sa/family/bupa-domestic" class="mainMenu " target="" id="mainmenuLink4" onmouseout="MM_showHideLayers('sub4','','hide');document.getElementById('mainmenuLink4').className='mainMenu ';" onmouseover="adjustOverlayPosition('#sub4');MM_showHideLayers('sub4','','show');document.getElementById('mainmenuLink4').className='mainMenuHighlight';">Bupa for Domestic Help<span class="menuArrow"></span></a><div class="absoluteTopSubMenu" id="sub4" onmouseout="MM_showHideLayers('sub4','','hide');document.getElementById('mainmenuLink4').className='mainMenu ';" onmouseover="MM_showHideLayers('sub4','','show');document.getElementById('mainmenuLink4').className='mainMenuHighlight';" style="left: -208px; visibility: hidden;">
                                                                <div class="absoluteTopSubMenuPad">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/bupa-domestic">Overview</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/bupa-domestic">Schemes</a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="https://bupa.com.sa/family/bupa-domestic">Bronze</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="https://bupa.com.sa/family/bupa-domestic">Green</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/bupa-domestic">Why Bupa Domestic?</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/bupa-domestic">Get a Quote</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="https://buy.bupa.com.sa/default.aspx?Product=Domestic%20Help">Buy Now</a></div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <div class="subMenuLevel1"><a href="https://bupa.com.sa/family/bupa-domestic">Hospital Network</a></div>
                                                                                    </div>
                                                                                </td>
                                                                                <td width="15">
                                                                                    <img src="/images/spacer.gif" alt="" width="15" height="1"></td>
                                                                                <td width="1"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td class="mainMenuSep">
                                                        <img src="/images/spacer.gif" alt="" width="1" height="1"></td>
                                                    <td>

                                                        <a id="mainmenuLinkBupaService" href="https://bupa.com.sa/tebtom" target="" class="mainMenu "
                                                            onmouseover="document.getElementById('mainmenuLinkBupaService').className='mainMenuHighlight';"
                                                            onmouseout="document.getElementById('mainmenuLinkBupaService').className='mainMenu ';">Bupa Services</a>


                                                    </td>

                                                    <td class="mainMenuSep">
                                                        <img src="/images/spacer.gif" alt="" width="1" height="1">
                                                    </td>
                                                    <%--<td>
                                                        <div class="posRelative higherZindex">
                                                            <a href="http://www.bupa.com.sa/English/HealthandWellness/Pages/default.aspx" class="mainMenu " target="" id="mainmenuLink6" onmouseout="MM_showHideLayers('sub6','','hide');document.getElementById('mainmenuLink6').className='mainMenu ';" onmouseover="adjustOverlayPosition('#sub6');MM_showHideLayers('sub6','','show');document.getElementById('mainmenuLink6').className='mainMenuHighlight';">Health and Wellbeing<span class="menuArrow"></span></a><div class="absoluteTopSubMenu" id="sub6" onmouseout="MM_showHideLayers('sub6','','hide');document.getElementById('mainmenuLink6').className='mainMenu ';" onmouseover="MM_showHideLayers('sub6','','show');document.getElementById('mainmenuLink6').className='mainMenuHighlight';" style="left: -416px; visibility: hidden;">
                                                                <div class="absoluteTopSubMenuPad">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="http://www.bupa.com.sa/English/HealthandWellness/HealthInformation/Pages/default.aspx">Health Information</a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthInformation/Articles/Pages/default.aspx">Articles</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthInformation/FactSheets/Pages/default.aspx">Fact Sheets</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthInformation/Pages/FAQ.aspx">Q&amp;As</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthInformation/Pages/Tips.aspx">Tips</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="http://www.bupa.com.sa/English/HealthandWellness/Pages/Bupa-Access.aspx">Bupa Arabia Application</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="http://www.bupa.com.sa/English/BupaInitiatives2/Pages/default.aspx">Bupa Initiative</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="http://www.bupa.com.sa/English/HealthandWellness/App-of-the-week/Pages/default.aspx">App of the week</a></div>
                                                                                    </div>
                                                                                    <div class="subMenuRow">
                                                                                        <div class="subMenuLevel1"><a href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/default.aspx">Health Tools</a></div>
                                                                                        <div class="subMenuLevel2"><a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/BMI-Calculator.aspx">BMI Calculator</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Sleep-Calculator.aspx">Sleep Calculator</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Body-Frame-Size-Calculator.aspx">BFS Calculator</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Pregnancy-Calculator-.aspx">Pregnancy Calculator </a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Waist-to-Height-Calculator-.aspx">WHtR Calculator </a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Protein-Intake-Calculator.aspx">PI Calculator</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Memory-Check.aspx">Memory Check</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Cost-of-Smoking-Habits.aspx">Cost of Smoking Habits</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Nicotine-Dependence.aspx">Nicotine Dependence</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Fiber-Requirements-Calculator.aspx">Fiber Requirements Calculator</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="" href="http://www.bupa.com.sa/English/HealthandWellness/HealthTools/Pages/Fat-Intake-Calculator.aspx">Fat Intake Calculator</a></div>
                                                                                    </div>
                                                                                </td>
                                                                                <td width="15">
                                                                                    <img src="/images/spacer.gif" alt="" width="15" height="1"></td>
                                                                                <td width="1">
                                                                                    <img src="../externalResource/HealthMenu.jpg" style="max-width: 190px" alt=""></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>--%>
                                                    <td id="ctl00_MainMenu1_ExternalLinks1_td1" class="mainMenuSep">
                                                        <img src="/images/spacer.gif" alt="" width="1" height="1">
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lbLogin" runat="server" Text="" OnClick="lbLogin_Click"
                                                            CssClass="membersLink " CausesValidation="False"></asp:LinkButton></td>
                                                    <td class="membersMenuSep">
                                                        <img src="/images/spacer.gif" alt="" width="1" height="1"></td>
                                                    <td>
                                                        <asp:LinkButton ID="lbReg" runat="server" Text=""
                                                            CssClass="membersLink " CausesValidation="False" OnClick="lbReg_Click"></asp:LinkButton></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
