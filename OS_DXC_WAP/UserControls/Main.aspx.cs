﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Main : System.Web.UI.Page
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
        {
            Response.Redirect("../default.aspx");
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
        {
            if (DateTime.Now.Year == 2014 && DateTime.Now.Month == 1)
            {
                LabelMedicalDeclarationMessage.Text = @"Dear Bupa client, " + Environment.NewLine +
                    "<br />Medical declaration on Add Employee & Add Dependent has been changed." + Environment.NewLine +
                    "<br />For additional assistance call 8002440302.";
                Notify.Visible = true;
            }

            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqContInfoResponse_DN response;

            request = new OS_DXC_WAP.CaesarWS.EnqContInfoRequest_DN();


            request.contNo = Convert.ToString(Session["ClientID"]);
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
          

            response = ws.EnqContInfo(request);

	    MyInbox1.contractNumber = Convert.ToString(Session["ClientID"]);

            if (!string.IsNullOrEmpty(Convert.ToString(response.status)))
            {
                if (Convert.ToInt32(response.status) >= 0)
                {
                    if (response.cont_Type.Trim() == "CORC")
                    {
                        if ((Convert.ToDateTime(response.end_Date) - DateTime.Now).Days <= 30)
                        {
                            lblClient.Text = "Dear Bupa client, the healthcare cover for your company will expire on " + Convert.ToDateTime(response.end_Date).ToShortDateString() + ". To avoid any delays in Bupa services, Kindly contact your Relationship Manager for renewal. For additional assistance call 8002440302. Please disregard this message if you have renewed your policy.";
                            Notify.Visible = true;

                        }
                    }
                    else
                    {
                        if ((Convert.ToDateTime(response.end_Date) - DateTime.Now).Days <= 30)
                        {
                            lblClient.Text = "Dear Bupa member, your Bupa healthcare cover will expire on " + Convert.ToDateTime(response.end_Date).ToShortDateString() + ". , Kindly contact your sales executive for renewal. For additional assistance call 8001160500. Please disregard this message if you have renewed your policy.";
                            Notify.Visible = true;
                        }
                    }
                }
            }


        }
    }
    protected void imgClose_Click(object sender, ImageClickEventArgs e)
    {
        Notify.Visible = false;
    }
};