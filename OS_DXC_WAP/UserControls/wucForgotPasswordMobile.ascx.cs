﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

public partial class UserControls_wucForgotPasswordMobile : System.Web.UI.UserControl
{
    private string _type = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        btnLogin.Text = "Retrive Password";
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        SendPassword("spGetUserEmail");
    }

    private SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }
    protected void SendPassword( string _sp)
    {
        SqlConnection connection = null;
        string fullname = "";
        string MemberEmail = "";
        string MemberPassword = "";

        try
        {
            try
            {
                connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
            }
            catch
            {
                lblError.Text = "The connection with the database can´t be established. Application Error";
                return;
            }

            SqlParameter[] arParms = new SqlParameter[5];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = txtNumber.Text;

            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 30);
            arParms[1].Direction = ParameterDirection.Output;

            arParms[2] = new SqlParameter("@Email", SqlDbType.NVarChar, 30);
            arParms[2].Direction = ParameterDirection.Output;


            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, _sp, arParms);

            fullname = arParms[2].Value + "...." + arParms[1].Value;

            MemberEmail = arParms[2].Value.ToString();
            MemberPassword = arParms[1].Value.ToString();
            fp.Visible = false;
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

            lblError.Text = "There are some problems while trying to use the Data Access Application block, please check the following error messages: {0} " + Environment.NewLine + "  This test requires some modifications to the database. Please make sure the database has been initialized using the SetUpDataBase.bat database script, or from the Install Quickstart option on the Start menu.";
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }
        if (MemberEmail != "")
        {
            System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
            mailMsg = new System.Net.Mail.MailMessage("webmaster@bupame.com", MemberEmail, "Mail your password service - Bupa", "Dear Member,<br>Your password is <b>" + MemberPassword + "</b>.<br>It was a pleasure serving you.<br><br>Regards,<br>BUPA ARABIA.");
            

            mailMsg.IsBodyHtml = true;

            System.Net.Mail.SmtpClient smptClient = new System.Net.Mail.SmtpClient();
            try
            {
                smptClient.Send(mailMsg);
                lblError.ForeColor = System.Drawing.Color.Teal;
                lblError.Text = "Password has been successfully send to your email address.";
            }
            catch (Exception ex)
            {
                lblError.Text = "Please contact the administrator for further details.";
            }

        }
        else
        {
            lblError.ForeColor = System.Drawing.Color.Red;
            lblError.Text = "Authentication Failed. No such Member exist. Please retry.";
        }
        lblError.Visible = true;
    }
}