﻿using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Utility.Configuration;
using Utility.Data;
using System.IO;
using System.Runtime.InteropServices;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web;
using System.Drawing.Imaging;
using System.Drawing;

public partial class UserControls_SignUp : System.Web.UI.UserControl
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    DataSet ds = null;
    private static readonly string connectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
    string _result;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Stop Caching in IE
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

        // Stop Caching in Firefox
        Response.Cache.SetNoStore();
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
        UserTyperbList.Items[0].Attributes.CssStyle.Add("margin-right", "5px");
        UserTyperbList.Items[1].Attributes.CssStyle.Add("margin-left", "15px");
        UserTyperbList.Items[1].Selected = true;

        //uploaderCtl.Visible = true;
        tblProvider.Visible = true;
        tblMember.Visible = false;
        tblClient.Visible = false;
        uploaderCtl.Visible = true;
        btnSubmit.Visible = true;
        btnSubmit.Enabled = true;
        Disclamer.Visible = false;

        Session["NewRequest"] = WebPublication.GenerateTransactionID();
        if (!IsPostBack)
        {
            CleartControls();
            //uploaderCtl.Visible = false;
        }
        else
        {
            Page.DataBind();
        }
    }


    public static int GetAge(DateTime reference, DateTime birthday)
    {
        int age = reference.Year - birthday.Year;
        if (reference < birthday.AddYears(age)) age--;

        return age;
    }

    protected void CleartControls()
    {
        txtDependent.Text = "";
        txtRequesterName.Text = "";
        txtProviderName.Text = "";
        txtUserName.Text = "";
        txtPassword.Text = "";
        txtEmployeeName.Text = "";
        txtCurrentPosition.Text = "";
        txtProviderEmail.Text = "";
        txtProviderFax.Text = "";
        txtProviderTel.Text = "";
        txtExt.Text = "";
        txtProviderMobile.Text = "";
        txtCustomerName.Text = "";
        txtGroupEmail.Text = "";
        txtFax.Text = "";
        txtMembership.Text = "";
        txtPassword.Text = "";
        txtMemberEmail.Text = "";
        txtID.Text = "";
        txtMobile.Text = "";
        lblMemberName.Text = "";
        lblContract.Text = "";
        lblCustomerName.Text = "";
        lblMemberName.Text = "";
        lblStatus.Text = "";
        txtMembership.Enabled = true;
        r1.Visible = false;
        r2.Visible = false;
        r3.Visible = false;
        r4.Visible = false;
        r5.Visible = false;
        r6.Visible = false;
    }

    private void SendEmailToMember(string Email, string MemberName)
    {
        MailMessage mailMsg = new MailMessage();
        mailMsg.To.Add(Email);

        mailMsg.From = new MailAddress("donotreply@bupa.com.sa");
        mailMsg.Subject = "Bupa Membership";
        mailMsg.Body = "Dear " + MemberName + ",<br/><br/>" +
                        "Thank you for registering in Bupa online services.<br/><br/>" +
                        "Yours sincerely<br/>" +
                        "Customer Services<br/>" +
                        "Bupa Arabia";
        mailMsg.IsBodyHtml = true;

        SmtpClient smptClient = new System.Net.Mail.SmtpClient();// .SmtpMail.Send("admin@bupame.com", );
        smptClient.Send(mailMsg);
        mailMsg.Dispose();
    }

    protected void UserTyperbList_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";
        CleartControls();
        switch (UserTyperbList.SelectedValue.ToString())
        {
            case "2":
                tblClient.Visible = true;
                tblMember.Visible = false;
                tblProvider.Visible = false;
                uploaderCtl.Visible = false;
                CleartControls();
                break;
            case "3":
                tblProvider.Visible = true;
                tblMember.Visible = false;
                tblClient.Visible = false;
                uploaderCtl.Visible = true;
                btnSubmit.Visible = true;
                btnSubmit.Enabled = true;
                Disclamer.Visible = false;
                CleartControls();
                break;
            case "1":
                // tblMember.Visible = true;
                // tblClient.Visible = false;
                // tblProvider.Visible = false;
                // uploaderCtl.Visible = false;
                // tblInfo.Visible = false;
                // btnSubmit.Enabled = false;
                // agreement.Visible = false;
                // healthcareProfile.Visible = false;
                // Disclamer.Visible = false;
                // btnSubmit.Visible = true;
                CleartControls();
                Response.Redirect("https://member.bupa.com.sa/Register.aspx");
                break;
        }
    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string _refNo;
        string strContent = "";
        try
        {
            if (txtimgcode.Text == HttpContext.Current.Session["CaptchaImageText"].ToString())
            {
                divCaptchaImage.Visible = false;
                txtimgcode.Text = "";
                WrongCaptcha.Style.Add("visibility", "hidden");

            }
            else
            {
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                WrongCaptcha.Style.Add("visibility", "visible");
                return;
            }

            switch (UserTyperbList.SelectedValue.ToString())
            {
                case "2":
                    //Client
                    _refNo = NewClient();
                    if (string.IsNullOrEmpty(_refNo))
                    {
                        lblError.Text = "An error happened while saving the your data, Please try again";
                        lblError.ForeColor = System.Drawing.Color.Red;
                        lblError.Font.Bold = true;
                    }
                    else
                    {
                        lblError.Text = "Your request will be processed, Login details will be sent to your email address.";
                        lblError.ForeColor = System.Drawing.Color.Green;
                        lblError.Font.Bold = true;
                        uploaderCtl.Visible = false;
                        //btnSubmit.Visible = false;
                        tblClient.Visible = false;
                        tblMember.Visible = false;
                        tblProvider.Visible = false;
                        strContent = "Dear Membership maintenance team ,<br>";
                        strContent = strContent + "A request for client user account has been submitted with the following details: <br>";
                        strContent = strContent + "<table width='100%'>";
                        strContent = strContent + "<tr><td colspan='2' align='left'><b>Request Information:</b></td></tr>";
                        strContent = strContent + "<tr><td style='width: 131px'><b>Request ID</b></td><td>" + _refNo + "</td></tr>";
                        strContent = strContent + "<tr><td style='width: 131px'><b>Customer Name</b></td><td>" + txtCustomerName.Text.Trim() + "</td></tr>";
                        strContent = strContent + "<tr><td style='width: 131px'><b>Group Secretary Name</b></td><td>" + txtGroupSecretaryName.Text.Trim() + "</td></tr>";
                        strContent = strContent + "<tr><td style='width: 131px'><b>Request Date</b></td><td>" + System.DateTime.Now.ToShortDateString() + "</td></tr>";
                        strContent = strContent + "<tr><td colspan='2'>This is a computer generated email. Please do not reply to this email.</td></tr>";
                        strContent = strContent + "<tr><td colspan='2'>Yours sincerely,</td></tr>";
                        strContent = strContent + "<tr><td colspan='2'>Bupa Online Service</td></tr>";
                        strContent = strContent + "</table>";
                        SendEmail("webmaster@bupame.com", "aakif.irshad@bupa.com.sa", "Request for provider user account", strContent, null);
                        btnSubmit.Visible = false;
                    }
                    break;
                case "3":
                    //Provider
                    _refNo = NewProvider();
                    if (string.IsNullOrEmpty(_refNo))
                    {
                        //lblError.Text = "UserName already taken, Please try another user name.";
                        if (revFileUpload1.ErrorMessage == "")
                            lblError.Text = "Invalid Username /  اسم مستخدم غير صالح";
                        lblError.ForeColor = System.Drawing.Color.Red;
                        lblError.Font.Bold = true;
                        txtimgcode.Text = "";
                        divCaptchaImage.Visible = true;
                        WrongCaptcha.Style.Add("visibility", "hidden");
                    }
                    else
                    {
                        //lblError.Text = "Your request will be processed, A confirmation will be sent to your email address detailing the status of your request within 24 hours.";
                        lblError.Text = "Your request will be processed. A confirmation will be sent to your email address.";
                        lblError.ForeColor = System.Drawing.Color.Green;
                        lblError.Font.Bold = true;
                        uploaderCtl.Visible = false;
                        //btnSubmit.Visible = false;
                        tblClient.Visible = false;
                        tblMember.Visible = false;
                        tblProvider.Visible = false;
                        strContent = "Dear Provider Relations,<br>";
                        strContent = strContent + "A request for provider user account has been submitted with the following details: <br>";
                        strContent = strContent + "<table width='100%'>";
                        strContent = strContent + "<tr><td colspan='2' align='left'><b>Request Information:</b></td></tr>";
                        strContent = strContent + "<tr><td style='width: 131px'><b>Request ID</b></td><td>" + _refNo + "</td></tr>";
                        strContent = strContent + "<tr><td style='width: 131px'><b>Provider Name</b></td><td>" + txtProviderName.Text.Trim() + "</td></tr>";
                        strContent = strContent + "<tr><td style='width: 131px'><b>Requester Name</b></td><td>" + txtRequesterName.Text.Trim() + "</td></tr>";
                        strContent = strContent + "<tr><td style='width: 131px'><b>Request Date</b></td><td>" + System.DateTime.Now.ToShortDateString() + "</td></tr>";
                        strContent = strContent + "<tr><td colspan='2'>This is a computer generated email. Please do not reply to this email.</td></tr>";
                        strContent = strContent + "<tr><td colspan='2'>Yours sincerely,</td></tr>";
                        strContent = strContent + "<tr><td colspan='2'>Bupa Online Service</td></tr>";
                        strContent = strContent + "</table>";
                        SendEmail("webmaster@bupame.com", "prov_webrequest@bupa.com.sa", "Request for provider user account", strContent, null);
                        btnSubmit.Visible = false;
                        CleartControls();
                    }
                    break;
                default:
                    NewMember();
                    CleartControls();
                    break;
            }
        }
        catch (Exception ex)
        {
            txtimgcode.Text = "";
            divCaptchaImage.Visible = true;
            WrongCaptcha.Style.Add("visibility", "hidden");
        }

    }

    protected string NewProvider()
    {

        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            if (FileUpload1.PostedFile != null)
            {
                Boolean fileOK = false;
                if (FileUpload1.HasFile)
                {
                    revFileUpload1.ErrorMessage = "";
                    Boolean mimeOK = false;
                    System.Web.HttpPostedFile file = FileUpload1.PostedFile;
                    CheckMimeTypes findMimeFromDate = new CheckMimeTypes();
                    string mime = findMimeFromDate.CheckFindMimeFromData(file);
                    String[] allowedMime = findMimeFromDate.ReturnFileExtenstions("AllFiles");
                    String fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                    String[] allowedExtensions = { ".pdf", ".jpg", ".jpeg", ".gif", ".png", ".bmp" };
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                            break;
                        }
                    }

                    for (int i = 0; i < allowedMime.Length; i++)
                    {
                        if (mime == allowedMime[i])
                        {
                            mimeOK = true;
                            break;
                        }
                    }

                    if (fileOK && mimeOK)
                    {

                        string virtualPath = string.Empty;
                        string serverPath = string.Empty;
                        string uniquePrefix = string.Empty;
                        string fileName = string.Empty;
                        string destinationPath = string.Empty;

                        uniquePrefix = "RegistrationFile_" + Convert.ToString(Session["NewRequest"]);
                        fileName = FileUpload1.FileName;
                        serverPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_OnlineReimbursmentServerPath);
                        using (NetworkShareAccesser.Access("bjfs05", "Bupa_Mid_East", "AbcpdfUat", "DdfDdf890*()"))
                        {
                            string filename = FileUpload1.PostedFile.FileName;
                            int lastSlash = filename.LastIndexOf("\\");
                            string trailingPath = filename.Substring(lastSlash + 1);
                            string fullPath = serverPath + uniquePrefix + trailingPath;
                            FileUpload1.PostedFile.SaveAs(fullPath);

                            virtualPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_ProviderRegistrationVirtualPath) + uniquePrefix + fileName;
                            Session["DocPath"] = virtualPath;
                        }
                        DataParameter proID = new DataParameter(SqlDbType.NVarChar, "@proID", txtProviderName.Value, ParameterDirection.Input);
                        DataParameter requester = new DataParameter(SqlDbType.NVarChar, "@requester ", txtRequesterName.Text.Trim(), ParameterDirection.Input);
                        DataParameter ProName = new DataParameter(SqlDbType.NVarChar, "@ProName ", txtProviderName.Text.Trim(), ParameterDirection.Input);
                        DataParameter proUsername = new DataParameter(SqlDbType.NVarChar, "@proUsername", txtUserName.Text.Trim(), ParameterDirection.Input);
                        DataParameter proPassword = new DataParameter(SqlDbType.NVarChar, "@proPassword", txtPassword.Text.Trim(), ParameterDirection.Input);
                        DataParameter NameofEmployee = new DataParameter(SqlDbType.NVarChar, "@NameofEmployee", txtEmployeeName.Text.Trim(), ParameterDirection.Input);
                        DataParameter Position = new DataParameter(SqlDbType.NVarChar, "@Position", txtCurrentPosition.Text.Trim(), ParameterDirection.Input);
                        DataParameter proEmail = new DataParameter(SqlDbType.NVarChar, "@proEmail", txtProviderEmail.Text.Trim(), ParameterDirection.Input);
                        DataParameter proFaxNo = new DataParameter(SqlDbType.NVarChar, "@proFaxNo", txtProviderFax.Text.Trim(), ParameterDirection.Input);
                        DataParameter proActive = new DataParameter(SqlDbType.Bit, "@proActive", 0, ParameterDirection.Input);
                        DataParameter proNotes = new DataParameter(SqlDbType.NVarChar, "@proNotes", "", ParameterDirection.Input);
                        DataParameter Tel = new DataParameter(SqlDbType.NVarChar, "@Tel", txtProviderTel.Text.Trim(), ParameterDirection.Input);
                        DataParameter Ext = new DataParameter(SqlDbType.NVarChar, "@Ext", txtExt.Text.Trim(), ParameterDirection.Input);
                        DataParameter Mobile = new DataParameter(SqlDbType.NVarChar, "@Mobile", txtProviderMobile.Text.Trim(), ParameterDirection.Input);
                        DataParameter Salt = new DataParameter(SqlDbType.NVarChar, "@Salt", "", ParameterDirection.Input);
                        DataParameter LastLogon = new DataParameter(SqlDbType.DateTime, "@LastLogon", DateTime.Today, ParameterDirection.Input);
                        DataParameter DocPath = new DataParameter(SqlDbType.NVarChar, "@DocPath", Convert.ToString(Session["DocPath"]), ParameterDirection.Input);
                        DataParameter LastUpdateTimestamp = new DataParameter(SqlDbType.DateTime, "@LastUpdateTimestamp", DateTime.Today, ParameterDirection.Input);

                        DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", "", ParameterDirection.Output, 30);

                        DataParameter[] dpBasket = new DataParameter[] { proID, requester, ProName, proUsername, proPassword, NameofEmployee, Position, proEmail, proFaxNo, proActive, proNotes
                                ,Tel ,Ext ,Mobile ,Salt, LastLogon,LastUpdateTimestamp,DocPath,result};

                        DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.spRegisterProvider", ref dpBasket, 60000);
                        _result = dpBasket[18].ParamValue.ToString();
                    }
                    else
                    {
                        revFileUpload1.ErrorMessage = "Invalid File";
                    }
                }



            }
        }
        catch (Exception ex)
        {
            Bupa.Core.Logging.Logger.Current.WriteException(ex.Message + "MIME");
            throw;
        }

        return _result;
    }
    protected string NewClient()
    {
        string _result;
        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            DataParameter clientID = new DataParameter(SqlDbType.NVarChar, "@clientID", "", ParameterDirection.Input);
            DataParameter cliUsername = new DataParameter(SqlDbType.NVarChar, "@cliUsername ", "", ParameterDirection.Input);
            DataParameter cliPassword = new DataParameter(SqlDbType.NVarChar, "@cliPassword ", "", ParameterDirection.Input);
            DataParameter cliName = new DataParameter(SqlDbType.NVarChar, "@cliName ", txtCustomerName.Text.Trim(), ParameterDirection.Input);
            DataParameter cliEmail = new DataParameter(SqlDbType.NVarChar, "@cliEmail ", txtGroupEmail.Text.Trim(), ParameterDirection.Input);
            DataParameter cliFax = new DataParameter(SqlDbType.NVarChar, "@cliFax ", txtFax.Text.Trim(), ParameterDirection.Input);
            DataParameter cliNotes = new DataParameter(SqlDbType.NVarChar, "@cliNotes", "", ParameterDirection.Input);
            DataParameter cliExportToExcel = new DataParameter(SqlDbType.Bit, "@cliExportToExcel", false, ParameterDirection.Input);
            DataParameter Salt = new DataParameter(SqlDbType.NVarChar, "@Salt ", "", ParameterDirection.Input);
            DataParameter Active = new DataParameter(SqlDbType.Bit, "@Active", false, ParameterDirection.Input);

            DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", "", ParameterDirection.Output, 30);

            DataParameter[] dpBasket = new DataParameter[] { clientID, cliUsername, cliPassword, cliName, cliEmail, cliFax, cliNotes, cliExportToExcel, Salt, Active, result };

            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.OSInsertClient", ref dpBasket, 60000);
            _result = dpBasket[10].ParamValue.ToString();
        }
        catch (Exception ex)
        {
            throw;
        }

        return _result;
    }
    protected void NewMember()
    {
        bool MemberExist = false;
        string _AddMember = "";
        try
        {

            RegistrationApplication registrationApplication = new RegistrationApplication(txtMembership.Text,
                txtPassword.Text, txtMemberEmail.Text, txtID.Text,
                txtMobile.Text, lblMemberName.Text, lblContract.Text, "Web", "", "");

            _AddMember = RegisterUser(registrationApplication);
            if (_AddMember == "F")
            {
                lblError.Text = "Your membership number is already registered. Please log in your membership No and password. if you forgot your password, please click on 'Forgot Password?' and we will be happy to send your password to your registered e-mail address.";
                lblError.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblError.Text = "Successfully Registered." + lblMemberName.Text + ", You can login now.";
                lblError.ForeColor = System.Drawing.Color.Green;
                btnSubmit.Visible = false;

                SaveAccordionValuestoDB();
                SendEmailToMember(txtMemberEmail.Text.Trim(), lblMemberName.Text);
            }
            tblMember.Visible = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// to save the only visible dependents to database
    /// </summary>
    private void SaveAccordionValuestoDB()
    {
        if (AccordionPane1.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic1, rdnPregnant1, rdnDelivered1, lblHeader1.ToolTip);

        if (AccordionPane2.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic2, rdnPregnant2, rdnDelivered2, lblHeader2.ToolTip);

        if (AccordionPane3.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic3, rdnPregnant3, rdnDelivered3, lblHeader3.ToolTip);

        if (AccordionPane4.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic4, rdnPregnant4, rdnDelivered4, lblHeader4.ToolTip);

        if (AccordionPane5.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic5, rdnPregnant5, rdnDelivered5, lblHeader5.ToolTip);

        if (AccordionPane6.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic6, rdnPregnant6, rdnDelivered6, lblHeader6.ToolTip);

        if (AccordionPane7.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic7, rdnPregnant7, rdnDelivered7, lblHeader7.ToolTip);

        if (AccordionPane8.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic8, rdnPregnant8, rdnDelivered8, lblHeader8.ToolTip);

        if (AccordionPane9.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic9, rdnPregnant9, rdnDelivered9, lblHeader9.ToolTip);

        if (AccordionPane10.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic10, rdnPregnant10, rdnDelivered10, lblHeader10.ToolTip);

        if (AccordionPane11.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic11, rdnPregnant11, rdnDelivered11, lblHeader11.ToolTip);

        if (AccordionPane12.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic12, rdnPregnant12, rdnDelivered12, lblHeader12.ToolTip);

        if (AccordionPane13.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic13, rdnPregnant13, rdnDelivered13, lblHeader13.ToolTip);

        if (AccordionPane14.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic14, rdnPregnant14, rdnDelivered14, lblHeader14.ToolTip);

        if (AccordionPane15.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic15, rdnPregnant15, rdnDelivered15, lblHeader15.ToolTip);

    }

    /// <summary>
    /// To Open Connection and Save to database
    /// </summary>
    /// <param name="cblChronic"></param>
    /// <param name="rdnPregnant"></param>
    /// <param name="rdnDelivered"></param>
    /// <param name="strMembershipNo"></param>
    private void SaveThisAccordionChronicInfoToDB(CheckBoxList cblChronic, RadioButtonList rdnPregnant, RadioButtonList rdnDelivered, string strMembershipNo)
    {
        int intChronicId;

        for (int i = 0; i < cblChronic.Items.Count - 1; i++)
        {
            if (cblChronic.Items[i].Selected)
            {
                intChronicId = int.Parse(cblChronic.Items[i].Value);
                SaveAnswerToDB(long.Parse(strMembershipNo), intChronicId);
            }
        }
        if (rdnPregnant.SelectedIndex != -1 && rdnPregnant.SelectedIndex != 0)
        {
            intChronicId = int.Parse(strMembershipNo);
            SaveAnswerToDB(long.Parse(txtMembership.Text), intChronicId);
        }
        if (rdnDelivered.SelectedIndex != -1 && rdnDelivered.SelectedIndex != 0)
        {
            intChronicId = int.Parse(rdnDelivered.SelectedValue);
            SaveAnswerToDB(long.Parse(strMembershipNo), intChronicId);
        }

    }

    public static string RegisterUser(RegistrationApplication Reg)
    {
        string _result;

        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            DataParameter membership = new DataParameter(SqlDbType.NVarChar, "@MembershipNo", Reg.Membership, ParameterDirection.Input);
            DataParameter password = new DataParameter(SqlDbType.NVarChar, "@Password", Reg.Password, ParameterDirection.Input);
            DataParameter fullName = new DataParameter(SqlDbType.NVarChar, "@FullName1", Reg.Fullname, ParameterDirection.Input);
            DataParameter email = new DataParameter(SqlDbType.NVarChar, "@Email", Reg.Email, ParameterDirection.Input);
            DataParameter mobile = new DataParameter(SqlDbType.NVarChar, "@Mobile", Reg.Mobile, ParameterDirection.Input);
            DataParameter identity_id = new DataParameter(SqlDbType.NVarChar, "@Identity_id", Reg.ID, ParameterDirection.Input);
            DataParameter ContractNo = new DataParameter(SqlDbType.NVarChar, "@contract_no", Reg.contract_no, ParameterDirection.Input);

            DataParameter channel = new DataParameter(SqlDbType.NVarChar, "@channel", Reg.channel, ParameterDirection.Input);
            DataParameter identity_type = new DataParameter(SqlDbType.NVarChar, "@identity_type", Reg.identity_type, ParameterDirection.Input);
            DataParameter membertype = new DataParameter(SqlDbType.NVarChar, "@membertype", Reg.membertype, ParameterDirection.Input);

            DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", 30, ParameterDirection.Output);
            DataParameter[] dpBasket = new DataParameter[] { membership, password, fullName, email, mobile,identity_id,
                                                            ContractNo, channel, identity_type, membertype, result };
            //DataParameter[] dpBasket = new DataParameter[] { membership, password, fullName, email, mobile, result };
            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.spRegisterUser", ref dpBasket, 60000);
            _result = dpBasket[5].ParamValue.ToString();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return _result;
    }

    private bool checkMemberAlreadyRegistered()
    {
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        try
        {
            cmd.Connection = conn;
            cmd.CommandText = "CheckMemberExist";
            cmd.Parameters.Add(new SqlParameter("@MembershipNo", txtMembership.Text));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            var result = cmd.ExecuteScalar();

            if (result.ToString() != "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conn.Close();
        }

    }

    protected void btnGetDetails_Click(object sender, EventArgs e)
    {
        try
        {
            if (checkMemberAlreadyRegistered())
            {
                lblError.Text = "Your membership number is already registered. Please log in your membership No and password. if you forgot your password, please click on 'Forgot Password?' and we will be happy to send your password to your registered e-mail address.";
                lblError.ForeColor = System.Drawing.Color.Red;
                return;
            }

            Disclamer.Visible = true;
            // Grab the memberID
            string membershipNumber = txtMembership.Text;
            // Fetch the member's details
            MemberHelper mh = new MemberHelper(membershipNumber, "");
            Member m = mh.GetMemberDetails();
            lblCustomerName.Text = m.Customer;
            lblContract.Text = m.ContractNumber;
            lblMemberName.Text = m.MemberName;
            lblStatus.Text = m.Status;

            txtMobile.Text = m.MOBILE;
            //hidIdentityNo.Value = m.SaudiIqamaID;
            cv_Identity.ValueToCompare = m.SaudiIqamaID.ToString();
            MyAccordion.Visible = true;

            long ls_txnid;


            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN emp_request;
            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN emp_response;

            emp_request = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

            emp_request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
            emp_request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];

            ls_txnid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddhhmmssf"));
            emp_request.transactionID = ls_txnid;
            emp_request.membershipNo = membershipNumber;

            emp_response = ws.EnqMbrDetInfo(emp_request);

            if (emp_response.status == "0")
            {
                lblHealthcareProfile.Visible = true;
                for (int i = 0; i < emp_response.detail.Length; i++)
                {
                    ////if (emp_response.detail[i].ToString().Trim().Equals(txtChkSaudiIdIqamaNo.Text))
                    if (emp_response.detail[i].memberID.ToString().Trim().Equals(txtChkSaudiIdIqamaNo.Text))
                    {
                        AccordionPane1.Visible = true;
                        lblHeader1.Text = emp_response.detail[i].membershipNo + " - " + emp_response.detail[i].memberName;
                        lblHeader1.ToolTip = emp_response.detail[i].membershipNo;
                        DateTime dtDateOfBirth = Convert.ToDateTime(emp_response.detail[i].memberDOB);
                        Bind_Chronic_checkbox_radiobutton(cblChronic1, emp_response.detail[i].gender, rdnPregnant1, rdnDelivered1, lblPregnant1, lblDelevered1, GetAge(DateTime.Now, dtDateOfBirth));
                    }
                    else
                    {
                        tblInfo.Visible = false;
                        lblError.Text = "ID is not matching";
                        lblError.Visible = true;
                        lblError.ForeColor = System.Drawing.Color.Red;
                        return;

                    }
                }
                btnSubmit.Enabled = true;
            }

            OS_DXC_WAP.CaesarWS.EnqMbrDepGetInfoRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqMbrDepGetInfoResponse_DN response;

            ls_txnid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddhhmmssf"));

            request = new OS_DXC_WAP.CaesarWS.EnqMbrDepGetInfoRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            request.transactionID = ls_txnid;
            request.membershipNo = membershipNumber;

            response = ws.EnqMbrDepGetInfo(request);

            if (response.status == "0")
            {
                btnSubmit.Enabled = true;
                for (int i = 0; i < response.detail.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblHealthcareProfile.Visible = true;
                            AccordionPane1.Visible = true;
                            //txtMobile2.Text = response.detail[i].mobileNo;
                            lblHeader1.ToolTip = response.detail[i].membershipNo;
                            lblHeader1.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            Bind_Chronic_checkbox_radiobutton(cblChronic1, response.detail[i].gender, rdnPregnant1, rdnDelivered1, lblPregnant1, lblDelevered2, response.detail[i].mbrAge);
                            break;
                        case 1:
                            AccordionPane2.Visible = true;
                            //txtMobile2.Text = response.detail[i].mobileNo;
                            lblHeader2.ToolTip = response.detail[i].membershipNo;
                            lblHeader2.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            Bind_Chronic_checkbox_radiobutton(cblChronic2, response.detail[i].gender, rdnPregnant2, rdnDelivered2, lblPregnant2, lblDelevered2, response.detail[i].mbrAge);
                            break;
                        case 2:
                            lblHeader3.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader3.ToolTip = response.detail[i].membershipNo;
                            //txtMobile3.Text = response.detail[i].mobileNo;
                            AccordionPane3.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic3, response.detail[i].gender, rdnPregnant3, rdnDelivered3, lblPregnant3, lblDelevered3, response.detail[i].mbrAge);
                            break;
                        case 3:
                            lblHeader4.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader4.ToolTip = response.detail[i].membershipNo;
                            //txtMobile4.Text = response.detail[i].mobileNo;
                            AccordionPane4.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic4, response.detail[i].gender, rdnPregnant4, rdnDelivered4, lblPregnant4, lblDelevered4, response.detail[i].mbrAge);
                            break;
                        case 4:
                            lblHeader5.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile5.Text = response.detail[i].mobileNo;
                            lblHeader5.ToolTip = response.detail[i].membershipNo;
                            AccordionPane5.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic5, response.detail[i].gender, rdnPregnant5, rdnDelivered5, lblPregnant5, lblDelevered5, response.detail[i].mbrAge);
                            break;
                        case 5:
                            lblHeader6.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile6.Text = response.detail[i].mobileNo;
                            lblHeader6.ToolTip = response.detail[i].membershipNo;
                            AccordionPane6.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic6, response.detail[i].gender, rdnPregnant6, rdnDelivered6, lblPregnant6, lblDelevered6, response.detail[i].mbrAge);
                            break;
                        case 6:
                            lblHeader7.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile7.Text = response.detail[i].mobileNo;
                            lblHeader7.ToolTip = response.detail[i].membershipNo;
                            AccordionPane7.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic7, response.detail[i].gender, rdnPregnant7, rdnDelivered7, lblPregnant7, lblDelevered7, response.detail[i].mbrAge);
                            break;
                        case 7:
                            lblHeader8.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile8.Text = response.detail[i].mobileNo;
                            lblHeader8.ToolTip = response.detail[i].membershipNo;
                            AccordionPane8.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic8, response.detail[i].gender, rdnPregnant8, rdnDelivered8, lblPregnant8, lblDelevered8, response.detail[i].mbrAge);
                            break;
                        case 8:
                            lblHeader9.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile9.Text = response.detail[i].mobileNo;
                            lblHeader9.ToolTip = response.detail[i].membershipNo;
                            AccordionPane9.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic9, response.detail[i].gender, rdnPregnant9, rdnDelivered9, lblPregnant9, lblDelevered9, response.detail[i].mbrAge);
                            break;
                        case 9:
                            lblHeader10.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader10.ToolTip = response.detail[i].membershipNo;
                            //txtMobile10.Text = response.detail[i].mobileNo;
                            AccordionPane10.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic10, response.detail[i].gender, rdnPregnant10, rdnDelivered10, lblPregnant10, lblDelevered10, response.detail[i].mbrAge);
                            break;
                        case 10:
                            lblHeader11.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader11.ToolTip = response.detail[i].membershipNo;
                            //txtMobile11.Text = response.detail[i].mobileNo;
                            AccordionPane11.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic11, response.detail[i].gender, rdnPregnant11, rdnDelivered11, lblPregnant11, lblDelevered11, response.detail[i].mbrAge);
                            break;
                        case 11:
                            lblHeader12.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader12.ToolTip = response.detail[i].membershipNo;
                            //txtMobile12.Text = response.detail[i].mobileNo;
                            AccordionPane12.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic12, response.detail[i].gender, rdnPregnant12, rdnDelivered12, lblPregnant12, lblDelevered12, response.detail[i].mbrAge);
                            break;
                        case 12:
                            lblHeader13.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader13.ToolTip = response.detail[i].membershipNo;
                            //txtMobile13.Text = response.detail[i].mobileNo;
                            AccordionPane13.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic13, response.detail[i].gender, rdnPregnant13, rdnDelivered13, lblPregnant13, lblDelevered13, response.detail[i].mbrAge);
                            break;
                        case 13:
                            lblHeader14.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader14.ToolTip = response.detail[i].membershipNo;
                            //txtMobile14.Text = response.detail[i].mobileNo;
                            AccordionPane14.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic14, response.detail[i].gender, rdnPregnant14, rdnDelivered14, lblPregnant14, lblDelevered14, response.detail[i].mbrAge);
                            break;
                        case 14:
                            lblHeader15.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader15.ToolTip = response.detail[i].membershipNo;
                            //txtMobile15.Text = response.detail[i].mobileNo;
                            AccordionPane15.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic15, response.detail[i].gender, rdnPregnant15, rdnDelivered15, lblPregnant15, lblDelevered15, response.detail[i].mbrAge);
                            break;
                    }

                }
            }

            if (!string.IsNullOrEmpty(m.MemberName))
            {
                //yahya 
                txtMembership.Enabled = false;
                r1.Visible = true;
                r2.Visible = true;
                r3.Visible = true;
                r4.Visible = true;
                r5.Visible = true;
                r6.Visible = true;
                agreement.Visible = true;
                tblInfo.Visible = true;
                healthcareProfile.Visible = true;
                lblError.Text = "";
            }

        }
        catch (Exception ex)
        {

            lblError.Text = ex.Message;
        }
    }

    /// <summary>
    /// bind accordion controls with values
    /// </summary>
    /// <param name="cblChronic"></param>
    /// <param name="strGender"></param>
    /// <param name="rdnPregnant"></param>
    /// <param name="rdnDelivered"></param>
    /// <param name="lblPregnant"></param>
    /// <param name="lblDelevered"></param>
    /// <param name="intMemAge"></param>
    private void Bind_Chronic_checkbox_radiobutton(CheckBoxList cblChronic, string strGender, RadioButtonList rdnPregnant,
                            RadioButtonList rdnDelivered, Label lblPregnant, Label lblDelevered,
                            long intMemAge)
    {
        DataTable dt = GetDataTable("select * from   tblChronic where control_type='CheckBox'");
        cblChronic.DataSource = dt;
        cblChronic.DataValueField = "chronic_id";
        cblChronic.DataTextField = "chronic_name"; // something similar to this
        cblChronic.DataBind();

        if (strGender == "Female" && intMemAge > 18)
        {
            rdnDelivered.Visible = true;
            rdnPregnant.Visible = true;
            lblPregnant.Visible = true;
            lblDelevered.Visible = true;
        }
        else if (strGender == "Male")
        {
            rdnDelivered.Visible = false;
            rdnPregnant.Visible = false;
            lblDelevered.Visible = false;
            lblDelevered.Visible = false;
        }
    }

    #region "Database Functions"

    /// <summary>
    /// Save Customer Answers to Database
    /// </summary>
    /// <param name="intMemberShipNo"></param>
    /// <param name="intChroID"></param>
    protected void SaveAnswerToDB(long intMemberShipNo, int intChroID)
    {

        if (CheckRecordExist("select * from tblMemberChronic where chronic_id=" + intChroID + " and " + "membership_no=" + intMemberShipNo) == false)
        {
            SqlConnection connection = new SqlConnection(connectionString);

            SqlConnection objConn = new SqlConnection(connection.ConnectionString);

            SqlCommand objCmd = new SqlCommand();
            objCmd.Connection = objConn;

            objCmd.CommandType = CommandType.Text;
            objCmd.CommandText = "insert into tblMemberChronic (membership_no,chronic_id) values (" + intMemberShipNo + "," + intChroID + ")";

            try
            {
                objCmd.Connection.Open();
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCmd.Connection.Close();
            }
        }
    }

    /// <summary>
    /// to check if the record exist before inserting healthcare info
    /// </summary>
    /// <param name="sqlQuery"></param>
    /// <returns></returns>
    private bool CheckRecordExist(string sqlQuery)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        try
        {
            cmd.Connection = connection;
            cmd.CommandText = sqlQuery;
            cmd.CommandType = CommandType.Text;
            connection.Open();

            var result = cmd.ExecuteScalar();

            if (result != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// To get the health care info from database
    /// </summary>
    /// <param name="sql"></param>
    /// <returns></returns>
    public static DataTable GetDataTable(string sql)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.ConnectionString = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandType = CommandType.Text;
        command.CommandText = sql;
        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.SelectCommand = command;
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        return dt;
    }

    #endregion

    #region "Send Email"

    /// <summary>
    /// send Email to the Client
    /// </summary>
    /// <param name="fromEmail"></param>
    /// <param name="toEmail"></param>
    /// <param name="subject"></param>
    /// <param name="content"></param>
    /// <param name="attachmentPaths"></param>
    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        // Setup the mail message
        MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

        // Deal with the attachments
        if (attachmentPaths != null)
        {
            foreach (string path in attachmentPaths)
            {
                // Create the attachment
                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(path));

                // Add the attachment
                mail.Attachments.Add(attachment);
            }
        }

        string smtpHost = WebPublication.EmailRelayAddress();
        int smtpPort = WebPublication.EmailRelayPort();

        SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
        mail.IsBodyHtml = true;

        smtp.Send(mail);
    }
    #endregion


    protected void btnUpload_Click(object sender, EventArgs e)
    {

    }

    public void MoveFile(string sourcePath, string destinationPath)
    {
        //MTOM.MTOMService svc = new MTOM.MTOMService();
        //svc.UploadFile(sourcePath, destinationPath);
    }
    private static string GenerateRandomCode()
    {
        Random r = new Random();
        string s = "";
        for (int j = 0; j < 5; j++)
        {
            int i = r.Next(3);
            int ch;
            switch (i)
            {
                case 1:
                    ch = r.Next(0, 9);
                    s = s + ch.ToString();
                    break;
                case 2:
                    ch = r.Next(65, 90);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
                case 3:
                    ch = r.Next(97, 122);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
                default:
                    ch = r.Next(97, 122);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
            }
            r.NextDouble();
            r.Next(100, 1999);
        }
        return s;
    }
    protected void CaptchaValidate_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtimgcode.Text == "" || txtimgcode.Text != "")
            {
                txtimgcode.Text = "";
                divCaptchaImage.Visible = true;
                return;
            }
        }
        catch (Exception ex) { }
    }

}