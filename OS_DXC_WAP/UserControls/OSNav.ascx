﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_OSNav" Codebehind="OSNav.ascx.cs" %>
<div class="userLinks">
    <table cellpadding="4" cellspacing="1">




        <div id="Member" runat="server" visible="false">
            <tr>
                <td align="left" colspan="9">
                    <h1>Member Services </h1>
                </td>
            </tr>
            <tr>
                <td align="left" class="boxcaption" title="View personal information of member and dependents
"><a href="../Member/MyDetails.aspx">
    <img id="Img1" runat="server" border="0"
        src="~/images/icons/member/My Details.jpg"
        style="height: 85px; width: 70px" /></a><br />
                    My Details</td>
                <td align="left" class="boxcaption" title="Check your benefit details
"><a href="../Member/MyCoverage.aspx">
    <img id="Img6" runat="server" border="0" src="~/images/icons/member/My Coverage.jpg" style="height: 85px; width: 70px" /></a><br />
                    My Coverage</td>
                <td align="left" class="boxcaption" title="Submit your request for online preauthorization for your chronic medication
"
                    style="display: none;"><a href="../Member/MyMed1.aspx">
                        <img id="Img7" runat="server"
                            style="height: 85px; width: 70px" border="0"
                            src="~/images/icons/member/My Medication.jpg" onclick="return Img4_onclick()" /></a><br />
                    My Medication</td>
                <td align="left" class="boxcaption" title="View the information of providers you can visit
"><a href="../Member/MyNetwork.aspx">
    <img id="Img8" runat="server"
        style="height: 85px; width: 70px" border="0"
        src="~/images/icons/member/My Network.jpg" /></a><br />
                    My Network</td>
                <td align="left" class="boxcaption" title="Check your claim details and status 
"><a href="../Member/ClaimHistory.aspx">
    <img id="Img9" runat="server"
        border="0" style="height: 85px; width: 70px" src="~/images/icons/common/ClaimHistory.jpg" /></a><br />
                    Claim History</td>
                <td align="left" class="boxcaption" title="Check your preauthorization details and status
">
                    <a href="../Member/MyPreauthHistory.aspx">
                        <img id="Img10"
                            style="height: 85px; width: 70px" runat="server" border="0"
                            src="~/images/icons/common/Pre-authhistory.jpg" /></a><br />
                    Preauth History
                </td>
                <td align="left">&nbsp;</td>
                <td align="left"></td>
                <td>&nbsp;</td>
            </tr>
        </div>

        <div id="Client" runat="server" visible="false">
            <tr>
                <td align="left" colspan="9">
                    <h1>Client Services </h1>
                </td>
            </tr>
            <tr>
                <td align="left" class="boxcaption" title="Add a new members or dependant to your scheme" id="tdAddEmployeeDependent" runat="server">
                    <a href="../client/AddMemberIntermediate.aspx">
                        <img id="Img2" runat="server" border="0"
                            src="~/images/icons/client/AddEmployeeOld.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Add Employee / Dependent 
                </td>
               <%-- <td align="left" class="boxcaption" title="Add a new members or dependant to your scheme" id="td1" runat="server">
                    <a href="../client/request.aspx">
                        <img id="Img34" runat="server" border="0"
                            src="~/images/icons/client/addemployee.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Add Employee / Dependent 
                </td>--%>
                <td align="left" class="boxcaption" title=" New Addition Guide" id="tdNewAdditionGuide" runat="server">
                    <a href="../Docs/CCHI-unified-manual-Web-Addition.pdf" target="_newtab">
                        <img id="Img3" runat="server" border="0"
                            src="../images/icons/common/RequiredDocument.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    New Addition Guide
                </td>
                <td align="left" class="boxcaption" title="Change the scheme level of a particular employee
"
                    id="tdChangeclass" runat="server">
                    <a href="#" id="aBlueNavChangeclass" runat="server">
                        <img id="Img4" runat="server" border="0"
                            src="~/images/icons/client/changeclass.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Change class</td>
                <td align="left" class="boxcaption" title="Search and track the status of membership maintenance request
"
                    id="tdTrackinfo" runat="server">
                    <a href="../client/trackinfo.aspx">
                        <img id="Img5" runat="server" border="0"
                            src="~/images/icons/client/TrackInfo.jpg" style="height: 85px; width: 70px" /></a><br />
                    Track info</td>
                <td align="left" class="boxcaption" id="tdSubmitRequired" runat="server">

                    <a href="../client/BatchUploadReqDocs.aspx" target="_self">
                        <img id="Img15" runat="server" border="0"
                            src="../images/icons/common/RequiredDocument.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Submit Required
                <br />
                    Supporting Document</td>

                <td align="left" class="boxcaption" title="Change the branch/location or sub group of your employees
"
                    id="tdChangeBranch" runat="server">

                    <a href="../client/changebranch.aspx">
                        <img id="Img11" runat="server" alt="" border="0"
                            src="~/images/icons/client/changebranch.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Change Branch</td>

                <td align="left" class="boxcaption" title="Send a request to Bupa Arabia for card replacement
"
                    id="tdReplaceCard" runat="server">

                    <a href="../client/cardreplace.aspx">
                        <img id="Img12" runat="server" alt="" border="0"
                            src="~/images/icons/client/ChangeCard.jpg" style="height: 85px; width: 70px" /></a><br />
                    Replace Card
                Data Correction</td>
                <td align="left" class="boxcaption" title="Delete an employee on your scheme
"
                    id="tdDeleteEmployee" runat="server">

                    <a href="../client/DeleteEmployeeEnhanced.aspx">
                        <img id="Img13" runat="server" alt="" border="0"
                            src="~/images/icons/client/DeleteEmployee.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Delete Employee
                
                </td>
                <td align="left" class="boxcaption" title="Delete a dependent on your scheme
"
                    id="tdDeleteDependent" runat="server">

                    <a href="#" id="aBlueNavDeleteDependent" runat="server" >
                        <img id="Img14" runat="server" alt="" border="0"
                            src="../images/icons/client/Deletedependent.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Delete Dependent</td>

            </tr>




            <tr id="trGsMemberDetails" runat="server">
                <td runat="server" id="ReimbursementForm" align="left" class="boxcaption">
                    <a href="ReimbursementForm.aspx">
                        <img id="Img30" runat="server" alt=" Claim History" border="0"
                            src="~/images/icons/common/ClaimHistory.jpg" style="height: 85px; width: 70px" title=" Claim History" /></a><br />
                    Submit reimbursement</td>
                <td align="left" class="boxcaption" title="View details about the main member and dependents
">
                    <a href='<%= Page.ResolveUrl("~/client/ctntMydetails.aspx")%>'>
                        <img id="Img16" runat="server" border="0"
                            src="~/images/icons/member/My Details.jpg" style="height: 85px; width: 70px" /></a><br />
                    My Details</td>
                <td align="left" class="boxcaption" title="View the scheme's coverage
">
                    <a href='<%= Page.ResolveUrl("~/client/ctntmycoverage.aspx")%>'>
                        <img id="Img23" runat="server" border="0"
                            src="~/images/icons/member/My Coverage.jpg" style="height: 85px; width: 70px" /></a><br />
                    My Coverage</td>
                <td align="left" class="boxcaption">
                    <a href='<%= Page.ResolveUrl("~/client/ctntmynetwork.aspx")%>'>
                        <img id="Img24" runat="server" border="0"
                            src="~/images/icons/member/My Network.jpg" style="height: 85px; width: 70px" title="My Network" /></a><br />
                    My Network </td>
                <td align="left" class="boxcaption" title="View the statement of accounts
">

                    <a href='#' id="aBlueNavStatement" runat="server">
                        <img id="Img25" runat="server" border="0"
                            src="~/images/icons/common/AccountStatement.jpg" style="height: 85px; width: 70px" /></a><br />
                    Account Statements</td>

                <td align="left" class="boxcaption" title="Request/print the certificate of membership
">

                    <a href='<%= Page.ResolveUrl("~/client/CtntMembershipCert.aspx")%>' style="display: block">
                        <img id="Img29" runat="server" border="0"
                            src="~/images/icons/member/Membership Certificate.jpg" style="height: 85px; width: 70px" /></a>Certificate</td>

                <td align="left" class="boxcaption" title="View all the online invoices
">

                    <a href='#' id="aBlueNavInvoice" runat="server">
                        <img id="Img26" runat="server" border="1"
                            src="~/images/icons/common/OnlineInvoice.jpg" style="height: 85px; width: 70px" /></a><br />
                    Online Invoice</td>
                <td align="left" class="boxcaption" title="View previously submitted claim
">

                    <a href='<%= Page.ResolveUrl("~/client/ctntclaimHistory.aspx")%>'>
                        <img id="Img27" runat="server" alt=" Claim History" border="0"
                            src="~/images/icons/common/ClaimHistory.jpg" style="height: 85px; width: 70px" title=" Claim History" /></a><br />
                    Claim History</td>
                <td align="left" class="boxcaption" title="Search and track the status of membership maintenance request
">

                    <a href='<%= Page.ResolveUrl("~/client/ctntPreauthhistory.aspx")%>'>
                        <img id="Img28" runat="server" border="0"
                            src="~/images/icons/common/Pre-authhistory.jpg" style="height: 85px; width: 70px" title="Preauth History" /></a><br />
                    Preauth History</td>
					<td align="left" class="boxcaption" title="CCHI CAP">
                    <a href='<%= Page.ResolveUrl("~/client/CCHICap.aspx")%>'>
                        <img id="Img33" runat="server" border="0"
                            src="~/images/icons/common/RequiredDocument.jpg" style="height: 85px; width: 70px" title="CCHI CAP" /></a><br />
                    CCHI CAP</td>

            </tr>
        </div>
		
		<div id="AramcoClient" runat="server" visible="false">
            <tr>
                <td align="left" colspan="9">
                    <h1>Client Services </h1>
                </td>
            </tr>
            <tr>
                <td align="left"  class="boxcaption" title="Add a new members to your scheme" id="td2" runat="server">
                    <a href="../client/Request.aspx">
                        <img id="Img36" runat="server" border="0"
                            src="~/images/icons/client/AddEmployeeOld.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                     Add member
                </td>
                
                <td align="left" class="boxcaption" title=" New Addition Guide" id="td3" runat="server">
                    <a href="../Docs/CCHI-unified-manual-Web-Addition.pdf" target="_newtab">
                        <img id="Img37" runat="server" border="0"
                            src="../images/icons/common/RequiredDocument.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    New Addition Guide
                </td>
                
                <td align="left" class="boxcaption" title="Delete member on your scheme" id="td9" runat="server">
                    <a href="../client/DeleteEmployeeEnhanced.aspx">
                        <img id="Img43" runat="server" alt="" border="0"
                            src="~/images/icons/client/DeleteEmployee.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Delete member
                </td>
                
                <td align="left" class="boxcaption" title="Change the scheme level of a particular member" id="td4" runat="server">
                    <a href="#"  id="aABlueNavChangeclass" runat="server">
                        <img id="Img38" runat="server" border="0"
                            src="~/images/icons/client/changeclass.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Change Scheme
                </td>

                <td align="left" class="boxcaption" title="Change the branch/location or sub group of your member" id="td7" runat="server">
                    <a href="../client/changebranch.aspx">
                        <img id="Img41" runat="server" alt="" border="0"
                            src="~/images/icons/client/changebranch.jpg"
                            style="height: 85px; width: 70px" /></a><br />
                    Change Branch
                </td>
                <td align="left" class="boxcaption" title="Send a request to Bupa Arabia for card replacement" id="td8" runat="server">
                    <a href="../client/cardreplace.aspx">
                        <img id="Img42" runat="server" alt="" border="0"
                            src="~/images/icons/client/ChangeCard.jpg" style="height: 85px; width: 70px" /></a><br />
                 Replace Card
                Data Correction
                </td>

                <td align="left" class="boxcaption" title="Search and track the status of membership maintenance request" id="td5" runat="server">
                    <a href="../client/trackinfo.aspx">
                        <img id="Img39" runat="server" border="0"
                            src="~/images/icons/client/TrackInfo.jpg" style="height: 85px; width: 70px" /></a><br />
                    Track info
                </td>
                <td align="left" class="boxcaption" title="View details about the main member and dependents">
                    <a href='<%= Page.ResolveUrl("~/client/ctntMydetails.aspx")%>'>
                        <img id="Img46" runat="server" border="0"
                            src="~/images/icons/member/My Details.jpg" style="height: 85px; width: 70px" /></a><br />
                    My Details
                </td>
                <td align="left" class="boxcaption" title="View the scheme's coverage">
                    <a href='<%= Page.ResolveUrl("~/client/ctntmycoverage.aspx")%>'>
                        <img id="Img47" runat="server" border="0"
                            src="~/images/icons/member/My Coverage.jpg" style="height: 85px; width: 70px" /></a><br />
                    My Coverage
                </td>
                <td align="left" class="boxcaption">
                    <a href='<%= Page.ResolveUrl("~/client/ctntmynetwork.aspx")%>'>
                        <img id="Img48" runat="server" border="0"
                            src="~/images/icons/member/My Network.jpg" style="height: 85px; width: 70px" title="My Network" /></a><br />
                    My Network 
                </td>
            </tr>

            <tr id="tr1" runat="server">
                <td align="left" class="boxcaption" title="Request/print the certificate of membership">
                    <a href='<%= Page.ResolveUrl("~/client/CtntMembershipCert.aspx")%>' style="display: block">
                        <img id="Img50" runat="server" border="0"
                            src="~/images/icons/member/Membership Certificate.jpg" style="height: 85px; width: 70px" /></a>
                    Certificate
                </td>
                <td align="left" class="boxcaption" title="View all the online invoices">
                    <a href='#' id="aABlueNavInvoice" runat="server">
                        <img id="Img51" runat="server" border="1"
                            src="~/images/icons/common/OnlineInvoice.jpg" style="height: 85px; width: 70px" /></a><br />
                    Online Invoice
                </td>
                <td align="left" class="boxcaption" title="View the statement of accounts">
                    <a href='#' id="aABlueNavStatement" runat="server" >
                        <img id="Img35" runat="server" border="0"
                            src="~/images/icons/common/AccountStatement.jpg" style="height: 85px; width: 70px" /></a><br />
                    Account Statements
                </td>
            </tr>
        </div>
		
        <div id="Provider" runat="server" visible="false">
            <tr>
                <td align="left" colspan="9">
                    <h1>Provider Services </h1>
                </td>
            </tr>
            <tr>
                <td align="center" class="boxcaption" title="Submit new preauthorization request
">
                    <div runat="server" id="Option1" visible="false">
                        <a href="../Provider/Preauth15.aspx">
                            <img id="Img17" runat="server" style="height: 85px; width: 70px" border="0" src="~/images/icons/provider/New request.jpg" /></a><br />
                        New Request
                    </div>
                </td>
                <td align="center" class="boxcaption" title="Check and print the status of your existing preauthorization requests
">
                    <div runat="server" id="Option2" visible="false">
                        <a href="../Provider/ExistingRequestSearch.aspx">
                            <img id="Img18" runat="server" border="0" style="height: 85px; width: 70px" src="~/images/icons/provider/Existingrequest.jpg" /></a><br />
                        Existing Request
                    </div>
                </td>
                <td align="center" class="boxcaption" style="display: none" title="Check the table of benefits of our latest contract 
">
                    <div runat="server" id="Option3" visible="false">
                        <a href="../Provider/CheckNewReply.aspx">
                            <img id="Img19" runat="server" border="0" style="height: 85px; width: 70px" src="~/images/icons/provider/Checknewreply.jpg" /></a><br />
                        Check New Reply 
                    </div>
                </td>
                <td align="center" class="boxcaption" title="Check the table of benefits of our latest contract 
">
                    <div runat="server" id="Option4" visible="false">
                        <a href="../Provider/TOBSearch.aspx">
                            <img id="Img20" runat="server" border="0" style="height: 85px; width: 70px" src="~/images/icons/common/RequiredDocument.jpg" /></a><br />
                        Table of Benefits
                    </div>
                </td>
                <td align="center" class="boxcaption" title="Submit your claim online using a downloadable template
">
                    <div runat="server" id="Option5" visible="false">
                        <a href="../Provider/eclaims.aspx">
                            <img id="Img21" runat="server" border="0" style="height: 85px; width: 70px"
                                src="~/images/icons/common/eclaimssubmission.jpg" /></a><br />
                        E-Claim Submission
                    </div>
                </td>
                <td align="center" class="boxcaption" title="Verify members eligibility and benefits
">
                    <div runat="server" id="Option6" visible="false">
                        <a href="../Provider/MemberVerification1.aspx">
                            <img id="Img22" runat="server" border="0" style="height: 85px; width: 70px"
                                src="~/images/icons/provider/Verification.jpg" /></a><br />
                        Verification
                    </div>

                </td>
                <td align="center" class="boxcaption" title="Notification of New Service Code ">
                    <div runat="server" id="Option7" visible="false">
                        <a href="../Provider/notify.aspx">
                            <img id="Img31" runat="server" border="0" style="height: 85px; width: 70px"
                                src="~/images/icons/common/eclaimssubmission.jpg" /></a><br />
                        Notification of New<br />
                        Service Code
                    </div>
                </td>
                <td align="center" class="boxcaption" title="Doctor's Details">
                    <div runat="server" id="Option8" visible="false">
                        <a href="../Provider/PhysicianDetails.aspx">
                            <img id="Img32" runat="server" border="0" style="height: 85px; width: 70px" src="~/images/icons/common/RequiredDocument.jpg" /></a><br />
                        Doctor's Details
                    </div>
                </td>

            </tr>
            <tr>
                <td align="left" class="boxcaption" title="View batch statments detail"
">

                    <a href="../Provider/BatchStatmentsDetail.aspx">
                        <img id="imgProviderStatmentIndicator" runat="server" border="0"
                            src="~/images/icons/common/AccountStatement.jpg" style="height: 85px; width: 70px" /></a><br />
                    Batch Statments</td>
                <td colspan="8"></td>
            </tr>
        </div>
    </table>
</div>

