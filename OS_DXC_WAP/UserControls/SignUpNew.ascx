﻿<%@ Control Language="C#" Inherits="UserControls_SignUpNew" Codebehind="SignUpNew.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="DevExpress.Web.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script src="../Scripts/CommonScript.js"></script>
<script src="../Scripts/jquery-1.10.2.1.js"></script>
<link href="../Styles/jquery-ui.css" rel="stylesheet" />
<script src="../Scripts/jquery-ui.js"></script>
<style type="text/css">
    .style1 {
        margin-left: 80px;
    }

    .style2 {
    }

    .style3 {
        width: 30px;
    }

    .style4 {
        font-size: 13px;
        color: #558ED5;
    }

    .style5 {
        width: 53%;
    }

    .auto-style1 {
        width: 229px;
    }
</style>
<style type="text/css">
    /* Accordion */
    .accordionHeader {
        border: 1px solid #2F4F4F;
        color: white;
        background-color: #2E4d7B;
        font-family: Arial, Sans-Serif;
        font-size: 12px;
        font-weight: bold;
        padding: 5px;
        margin-top: 5px;
        cursor: pointer;
    }

    #master_content .accordionHeader a {
        color: white;
        background: none;
        text-decoration: none;
    }

        #master_content .accordionHeader a:hover {
            background: none;
            text-decoration: underline;
        }

    .accordionHeaderSelected {
        border: 1px solid #2F4F4F;
        color: white;
        background-color: #5078B3;
        font-family: Arial, Sans-Serif;
        font-size: 12px;
        font-weight: bold;
        padding: 5px;
        margin-top: 5px;
        cursor: pointer;
    }

    #master_content .accordionHeaderSelected a {
        color: #FFFFFF;
        background: none;
        text-decoration: none;
    }

        #master_content .accordionHeaderSelected a:hover {
            background: none;
            text-decoration: underline;
        }

    .accordionContent {
        background-color: #D3DEEF;
        border: 1px dashed #2F4F4F;
        border-top: none;
        padding: 5px;
        padding-top: 10px;
    }

    /* Content Page Layout */
    .demoarea {
        padding: 20px;
        background: #FFF url(images/demotop.png) no-repeat left top;
    }

        .demoarea p {
            padding: 5px;
        }

    .demoheading {
        padding-bottom: 20px;
        color: #5377A9;
        font-family: Arial, Sans-Serif;
        font-weight: bold;
        font-size: 1.5em;
    }

    .demobottom {
        height: 8px;
        font-size: 10px;
        background: #FFF url(images/demobottom.png) no-repeat left bottom;
    }

    .accordionHeader a {
        color: #FFFFFF;
        background: none;
        text-decoration: none;
    }

    a.accordionLink {
        color: white;
    }

    .ruErrorMessage {
        color: #ef0000;
        font-variant: small-caps;
        text-transform: lowercase;
        padding-bottom: 0;
    }

    .btnProvider {
        background: #25a0da;
        background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
        background-image: -moz-linear-gradient(top, #3498db, #2980b9);
        background-image: -ms-linear-gradient(top, #3498db, #2980b9);
        background-image: -o-linear-gradient(top, #3498db, #2980b9);
        background-image: linear-gradient(to bottom, #3498db, #2980b9);
        -webkit-border-radius: 24;
        -moz-border-radius: 24;
        border-radius: 10px;
        font-family: Arial;
        color: #ffffff;
        font-size: 16px;
        padding: 5px 5px 5px 5px;
        text-decoration: none;
    }

        .btnProvider:hover {
            background: #3cb0fd;
            background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
            background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
            text-decoration: none;
        }

    .buttonOld {
        color: #000000;
        cursor: pointer;
        font: normal 12px;
        width: 80px;
        vertical-align: middle;
        border: 1px solid #7F7F7F;
        padding: 1px;
        height: 30px;
    }
</style>
<script type="text/javascript">
    // to allow numbers only in the numbers text boxes
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.]")
        if (key == 8) {
            keychar = String.fromCharCode(key);
        }
        if (key == 11) {
            key = 8;
            keychar = String.fromCharCode(key);
        }
        return reg.test(keychar);
    }

    (function (global, undefined) {
        var demo = {};

        function alertCallBackFn(arg) {
            radalert("<strong>radalert</strong> returned the following result: <h3 style='color: #ff0000;'>" + arg + "</h3>", 350, 250, "Result");
        }




        global.alertCallBackFn = alertCallBackFn;

        global.$dialogsDemo = demo;
    })(window);

    window.validationFailed = function (radAsyncUpload, args) {       
        var $row = $(args.get_row());
        var erorMessage = getErrorMessage(radAsyncUpload, args);
        var span = createError(erorMessage);
        $row.addClass("ruError");
        $row.append(span);
    }

    function getErrorMessage(sender, args) {       
        var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
        if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
            if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                return ("This file type is not supported.");
            }
            else {
                return ("This file exceeds the maximum allowed size.");
            }
        }
        else {
            return ("not correct extension.");
        }
    }

    function createError(erorMessage) {
        var input = '<span class="ruErrorMessage">' + erorMessage + ' </span>';
        return input;
    }


</script>
<script>
    function PrintContent() {
        var DocumentContainer = document.getElementById('AgreementDiv');
        var WindowObject = window.open(",'PrintWindow','width=3,height=3,top=3,left=1,toolbars=no,scrollbars=yes,status=no,resizable=no,'");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        //WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }



    function validateAgreement() {
        var textbox1 = document.getElementById("btnSubmit");
        if (document.getElementById("chkAgreement").checked == true) {
            //alert();
            textbox1.disabled = false;
            //document.getElementById("AgreementBtn").enabled = true;
        }
        else {
            textbox1.disabled = true;
        }
    }
</script>
<script type="text/javascript">
    function ValidateCheckBox(sender, args) {
        if (document.getElementById("<%=CheckBox1.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            document.getElementById("<%=CheckBox1.ClientID %>").focus();
            args.IsValid = false;
        }
    }
</script>

<link href="../css/ProviderStyles.css" rel="stylesheet" />

<div class="memberLoginComp" id="entryForm">
    <table width="100%" style="border: 0">
        <tr>
            <td valign="top">
                <table width="100%" style="border: 0">
                    <tr>
                        <td>
                            <table width="100%" style="border: 0">
                                <tr runat="server" visible="false">
                                    <td style="width: 187px" align="left">
                                        <h1>Apply as :</h1>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="UserTyperbList" runat="server"
                                            RepeatDirection="Horizontal"
                                            AutoPostBack="true"
                                            OnSelectedIndexChanged="UserTyperbList_SelectedIndexChanged">
                                            <asp:ListItem Text="New Healthcare Provider" Selected="True" Value="4"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr runat="server" id="trProviderNote" >
                                    <td colspan="2">
                                        <table>
                                            <tr>
                                                <td style="vertical-align: top; width: 100px;">
                                                    <span class="label" style="color: red; font-weight: bold;">Important Note:</span> &nbsp;
                                                </td>
                                                <td>If saving this application as draft and not yet finalized with the other requirements,   log-in to the same page again using the Provider Name & your Preferred  Login ID
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <table width="100%" style="border: 0">
                                <tr>
                                    <td style="width: 75%" valign="top">
                                        <table width="100%" style="border: 0; vertical-align: top">
                                            <tr>
                                                <td valign="top" align="center" class="style5">

                                                    <dx:ASPxLabel ID="lblError" runat="server" Text="">
                                                    </dx:ASPxLabel>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td runat="server" id="tblMember" visible="true" valign="top" class="style5">

                                                    <table style="border: 0">
                                                        <tr>
                                                            <td colspan="2">
                                                                <table>
                                                                    <tr>
                                                                        <td>Membership No.
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtMembership" runat="server" Width="90px" MaxLength="8" BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMembership" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMembership" ErrorMessage="Only Numbers" ValidationExpression="\d{7}">*</asp:RegularExpressionValidator>--%>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnGetDetails" runat="server"
                                                                                OnClick="btnGetDetails_Click" Text="Get Details">
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style2" valign="top" colspan="2">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style2" valign="top" colspan="2">
                                                                <table width="100%" id="tblInfo" runat="server" visible="false" frame="box">
                                                                    <tr>
                                                                        <td valign="top" colspan="2" style="text-align: center; font-size: 14px;">
                                                                            <u><b>Member Information</b></u>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td colspan="2"><b>&nbsp;&nbsp;&nbsp;Your Current Status</b></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="style3">&nbsp;&nbsp;&nbsp;<strong>Name:</strong></td>
                                                                                    <td>
                                                                                        <dx:ASPxLabel ID="lblMemberName" runat="server"
                                                                                            Text="">
                                                                                        </dx:ASPxLabel>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="style3">&nbsp;&nbsp;&nbsp;<strong>Status:</strong></td>
                                                                                    <td>
                                                                                        <dx:ASPxLabel ID="lblStatus" runat="server"
                                                                                            Text="">
                                                                                        </dx:ASPxLabel>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2"></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                            <table>

                                                                                <tr>
                                                                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<b>Policy Information</b></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="style3">&nbsp;&nbsp;&nbsp;<strong>Contract:</strong></td>
                                                                                    <td>
                                                                                        <dx:ASPxLabel ID="lblContract" runat="server"
                                                                                            Text="">
                                                                                        </dx:ASPxLabel>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="style3">&nbsp;&nbsp;&nbsp;<strong>Customer:</strong>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxLabel ID="lblCustomerName" runat="server"
                                                                                            Text="">
                                                                                        </dx:ASPxLabel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="style2" valign="top" colspan="2">&nbsp;</td>
                                                        </tr>
                                                        <tr runat="server" id="r1" visible="false">
                                                            <td class="auto-style1">Saudi ID / Iqama <strong style="color: red">*</strong></td>
                                                            <td class="style1">
                                                                <asp:HiddenField ID="hidIdentityNo" runat="server" />
                                                                <asp:TextBox ID="txtID" runat="server" Width="200px" MaxLength="10"
                                                                    BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvTxtID" Display="Dynamic" runat="server" ControlToValidate="txtID" ErrorMessage="* Required Field" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                <asp:CompareValidator runat="server" ID="cv_Identity"
                                                                    ErrorMessage="You Identity/Iqama No is not matching our records. Please correct"
                                                                    ControlToValidate="txtID" Type="String" Operator="Equal" Display="Dynamic"
                                                                    SetFocusOnError="true" />
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="r2" visible="false">
                                                            <td class="auto-style1">Number of active dependents <strong style="color: red">*</strong></td>
                                                            <td class="style1">

                                                                <asp:TextBox ID="txtDependent" runat="server" Width="200px" MaxLength="2"
                                                                    BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfv_txtDependent" Display="Dynamic" runat="server" ControlToValidate="txtDependent" ErrorMessage="* Required Field" SetFocusOnError="true"></asp:RequiredFieldValidator>


                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="r3" visible="false">
                                                            <td class="auto-style1">Email Address <strong style="color: red">*</strong></td>
                                                            <td class="style1">

                                                                <asp:TextBox ID="txtMemberEmail" runat="server" Width="200px" MaxLength="50"
                                                                    BackColor="#eff6fc"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfv_txtMemberEmail" runat="server" ControlToValidate="txtMemberEmail" ErrorMessage="* Required Field" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>

                                                                <asp:RegularExpressionValidator ID="rev_txtMemberEmail" runat="server" ErrorMessage="Not valid email address" ControlToValidate="txtMemberEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true"></asp:RegularExpressionValidator>

                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="r4" visible="false">
                                                            <td class="auto-style1">Mobile <span style="font-size: 10px; font-weight: bold;">(9665xxxxxxxx)</span> <strong style="color: red">*</strong></td>
                                                            <td class="style1">

                                                                <asp:TextBox ID="txtMobile" runat="server" Width="200px" MaxLength="14"
                                                                    BackColor="#eff6fc" onkeypress="javascript:return allownumbers(event);"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfv_txtMobile" runat="server" ControlToValidate="txtMobile" ErrorMessage="* Required Field" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>



                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="r5" visible="false">
                                                            <td class="auto-style1">Password <strong style="color: red">*</strong></td>
                                                            <td class="style1">
                                                                <asp:TextBox ID="txtPassword" runat="server" Width="200px" MaxLength="20" TextMode="Password"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtPassword" ErrorMessage="* Required Field" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>

                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="r6" visible="false">
                                                            <td class="auto-style1">Retype password <strong style="color: red">*</strong></td>
                                                            <td class="style1">
                                                                <asp:TextBox ID="txtrePassword" runat="server" Width="200px" Password="true" TextMode="Password"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfv_txtrePassword" runat="server" ControlToValidate="txtrePassword" ErrorMessage="* Required Field" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator" ControlToCompare="txtPassword" Text="Password mismatch" ControlToValidate="txtrePassword" SetFocusOnError="true" Display="Dynamic"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="healthcareProfile" visible="false">
                                                            <td style="width: 100%" colspan="2">

                                                                <div class="demoarea">

                                                                    <div class="demoheading">
                                                                        <asp:Label ID="lblHealthcareProfile" runat="server" Text="Healthcare Profile" Visible="false"></asp:Label>
                                                                    </div>
                                                                    <ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0"
                                                                        HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                                                                        ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
                                                                        TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true" Visible="false">
                                                                        <Panes>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader1" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>

                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic1" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant1" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant1" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered1" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 3 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered1" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader2" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile2" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic2" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant2" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant2" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered2" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 3 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered2" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader3" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile3" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic3" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant3" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant3" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered3" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 3 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered3" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader4" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile4" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic4" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant4" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant4" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered4" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 4 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered4" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane5" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader5" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile5" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic5" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant5" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant5" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered5" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 5 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered5" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane6" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader6" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile6" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic6" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant6" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant6" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered6" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 6 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered6" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane7" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader7" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile7" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic7" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant7" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant7" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered7" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 7 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered7" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane8" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader8" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile8" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic8" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant8" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant8" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered8" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 8 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered8" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane9" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader9" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile9" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic9" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant9" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant9" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered9" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 9 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered9" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane10" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader10" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile10" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic10" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant10" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant10" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered10" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 10 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered10" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane11" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader11" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile11" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic11" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant11" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant11" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered11" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 11 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered11" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane12" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader12" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile12" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic12" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant12" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant12" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered12" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 12 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered12" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane13" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader13" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile13" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic13" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant13" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant13" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered13" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 13 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered13" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane14" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader14" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile14" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic14" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant14" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant14" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered14" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 14 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered14" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>
                                                                            <ajaxToolkit:AccordionPane ID="AccordionPane15" runat="server" Visible="false">
                                                                                <Header>
                                                                                    <a href="" class="accordionLink">
                                                                                        <asp:Label ID="lblHeader15" runat="server" Text="" /></a>
                                                                                </Header>
                                                                                <Content>
                                                                                    Mobile No:
                                                                                    <asp:TextBox ID="txtMobile15" runat="server"></asp:TextBox><br />
                                                                                    <br />
                                                                                    <strong>Do you have any Chronic Conditions? If Yes, Please select below list</strong>
                                                                                    <asp:CheckBoxList ID="cblChronic15" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList><br />
                                                                                    <asp:Label ID="lblPregnant15" runat="server" Visible="false" Text="Are You Pregnant?" Font-Bold="true"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnPregnant15" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="19">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:Label ID="lblDelevered15" runat="server" Visible="false" Font-Bold="true" Text="Have You delivered a baby within the past 15 months?"></asp:Label>

                                                                                    <asp:RadioButtonList ID="rdnDelivered15" runat="server" Visible="false" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                                        <asp:ListItem Value="23">Yes</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </Content>
                                                                            </ajaxToolkit:AccordionPane>

                                                                        </Panes>
                                                                    </ajaxToolkit:Accordion>
                                                                </div>

                                                                <br />
                                                                <br />


                                                            </td>

                                                        </tr>
                                                        <tr runat="server" id="agreement" visible="false">
                                                            <td class="style2" colspan="2">
                                                                <table style="font-size: small;">

                                                                    <tr>
                                                                        <td width="10">
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <a href="term.htm" target="_blank">Terms and condition</a> was read and accepted by member through Bupa Arabia’s website
                                                                            <br />
                                                                            تمت قراءة هذه <a href="term.htm" target="_blank">الشروط والاحكام</a> وتمت الموافقه عليها من خلال موقع بوبا العربية الالكتروني
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="You should accept terms and conditions"
                                                                                ClientValidationFunction="ValidateCheckBox">
                                                                            </asp:CustomValidator><br />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td runat="server" id="tblClient" visible="false" valign="top" class="style5">
                                                    <table width="100%" style="border: 0">
                                                        <tr>
                                                            <td style="width: 300px">Customer Name</td>
                                                            <td style="width: 442px">
                                                                <dx:ASPxTextBox ID="txtCustomerName" runat="server" Width="90%"
                                                                    BackColor="#eff6fc">
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                                        <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 300px">Contract No.</td>
                                                            <td style="width: 442px">
                                                                <dx:ASPxTextBox ID="txtContract" runat="server" Width="90%" MaxLength="8"
                                                                    BackColor="#eff6fc">
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                                        <RegularExpression ErrorText="Incorrect Contract number" ValidationExpression="^[0-9]*$" />
                                                                        <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 300px">Group Secretary Name</td>
                                                            <td style="width: 442px">
                                                                <dx:ASPxTextBox ID="txtGroupSecretaryName" runat="server" Width="90%"
                                                                    BackColor="#eff6fc">
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                                        <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 300px">Email Address</td>
                                                            <td style="width: 442px">
                                                                <dx:ASPxTextBox ID="txtGroupEmail" runat="server" Width="90%"
                                                                    BackColor="#eff6fc">
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                                        <RegularExpression ErrorText="Invalid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                                        <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 300px">Telephone Number</td>
                                                            <td style="width: 442px">
                                                                <dx:ASPxTextBox ID="txtTel" runat="server" Width="90%" MaxLength="10"
                                                                    BackColor="#eff6fc">
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                                        <RegularExpression ErrorText="Incorrect Tel Number" ValidationExpression="^[0-9]*$" />
                                                                        <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 300px">Fax Number</td>
                                                            <td style="width: 442px">
                                                                <dx:ASPxTextBox ID="txtFax" runat="server" Width="90%" MaxLength="10"
                                                                    BackColor="#eff6fc">
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                                                        <RegularExpression ErrorText="Incorrect Fax Numbert" ValidationExpression="^[0-9]*$" />
                                                                        <RequiredField IsRequired="true" ErrorText="Field is required" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td runat="server" id="tblProvider" visible="false" valign="top" class="style5">
                                                    <table width="95%" style="border: 0">
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblMessage" CssClass="label"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>

                                                                <div style="min-height: 350px; border: 1px; background-color: white;">
                                                                    <telerik:RadTabStrip runat="server" ID="RadTabStrip1" MultiPageID="RadMultiPage2" OnTabClick="RadTabStrip1_TabClick"
                                                                        Width="810px" SelectedIndex="0" Font-Bold="true" Skin="MetroTouch">
                                                                        <Tabs>
                                                                            <telerik:RadTab Text="Provider" Width="160px">
                                                                                <TabTemplate>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>Provider Details</td>
                                                                                            <td>
                                                                                                <asp:Image runat="server" ID="imgtab1" ImageUrl="~/images/navigate-right.png" Width="20px" Height="20px" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </TabTemplate>
                                                                            </telerik:RadTab>
                                                                            <telerik:RadTab Text="Facility Details" Width="150px" Enabled="false">
                                                                                <TabTemplate>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>Facility Details</td>
                                                                                            <td>
                                                                                                <asp:Image runat="server" ID="imgtab1" ImageUrl="~/images/navigate-right.png" Width="20px" Height="20px" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </TabTemplate>
                                                                            </telerik:RadTab>
                                                                            <telerik:RadTab Text="License Details" Width="150px" Enabled="false">
                                                                                  <TabTemplate>
                                                                                    <table><tr><td>License Details</td><td>
                                                                                        <asp:Image runat="server" ID="imgtab1" ImageUrl="~/images/navigate-right.png" Width="20px" Height="20px" />
                                                                                                                        </td></tr></table>
                                                                                </TabTemplate>
                                                                            </telerik:RadTab>
                                                                            <telerik:RadTab Text="Contact Person Details" Width="210px" Enabled="false">
                                                                                  <TabTemplate>
                                                                                    <table><tr><td>Contact Person Details</td><td>
                                                                                        <asp:Image runat="server" ID="imgtab1" ImageUrl="~/images/navigate-right.png" Width="20px" Height="20px" />
                                                                                                                        </td></tr></table>
                                                                                </TabTemplate>
                                                                            </telerik:RadTab>
                                                                            <telerik:RadTab Text="Attachments" Width="140px" Enabled="false">
                                                                                  <TabTemplate>
                                                                                    <table><tr><td>Attachments</td><td>
                                                                                     
                                                                                                                        </td></tr></table>
                                                                                </TabTemplate>
                                                                            </telerik:RadTab>
                                                                        </Tabs>

                                                                    </telerik:RadTabStrip>
                                                                    <telerik:RadMultiPage runat="server" ID="RadMultiPage2" CssClass="innerMultiPage" SelectedIndex="0">
                                                                        <telerik:RadPageView runat="server" ID="PageView1" BackColor="White">
                                                                            <table style="width: 100%">
                                                                                <tr>
                                                                                    <td colspan="4" style="height: 6px;"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblProviderName" CssClass="label" runat="server" Text="Provider Name"></asp:Label>
                                                                                        <span style="color: red">*</span>

                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtProviderName" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtProviderName" EnableClientScript="false" ErrorMessage="Please enter Provider Name"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblCountry" CssClass="label" runat="server" Text="Country"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <telerik:RadComboBox ID="ddlCountry" BackColor="#EFF6FC" AllowCustomText="false" CssClass="DropdownRad" runat="server" Width="162px" MarkFirstMatch="true" DropDownCssClass="multipleRowsColumns" AutoPostBack="true" Height="200px" ChangeTextOnKeyBoardNavigation="false" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" ErrorMessage="Invalid Country"
                                                                                            EmptyMessage="Select or enter Country">
                                                                                        </telerik:RadComboBox>

                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ControlToValidate="ddlCountry" EnableClientScript="false" ErrorMessage="Please enter City"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblLoginID" CssClass="label" runat="server" Text="Preferred Login ID"></asp:Label>
                                                                                        <span style="color: red">*</span>

                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtLoginID" CssClass="textboxPP" runat="server" Text="" Width="120px" MaxLength="12"></asp:TextBox>
                                                                                        &nbsp; &nbsp; 
                                                                                        <asp:ImageButton runat="server" ID="btnSearch" Height="20px" ImageAlign="Middle" OnClick="btnSearch_Click1" ImageUrl="~/images/search.png" />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ControlToValidate="txtLoginID" EnableClientScript="false" ErrorMessage="Please enter Provider Name"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblCity" CssClass="label" runat="server" Text="City"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <telerik:RadComboBox ID="ddlCity" Height="200px" BackColor="#EFF6FC" CssClass="DropdownRad" runat="server" Width="162px" ExpandDirection="Down" MarkFirstMatch="true" DropDownCssClass="multipleRowsColumns" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged"
                                                                                            EmptyMessage="Select or enter City">
                                                                                        </telerik:RadComboBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity" EnableClientScript="false" ErrorMessage="Please enter City"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblStreet" CssClass="label" runat="server" Text="Street"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtStreet" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtStreet" EnableClientScript="false" ErrorMessage="Please enter Street"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>

                                                                                    <td class="col1" runat="server" visible="false" id="tddistrict1">
                                                                                        <asp:Label ID="lblDistrict" CssClass="label" runat="server" Text="District"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2" runat="server" visible="false" id="tddistrict2">
                                                                                        <asp:TextBox ID="txtDistrict" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDistrict" EnableClientScript="false" ErrorMessage="Please enter District"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblPractice" CssClass="label" runat="server" Text="Practice"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <telerik:RadComboBox ID="ddlPractice" CssClass="DropdownRad" runat="server" Width="162px" Height="200px" ExpandDirection="Down" MarkFirstMatch="true" DropDownCssClass="multipleRowsColumns" BackColor="#EFF6FC" AutoPostBack="true" OnSelectedIndexChanged="ddlPractice_SelectedIndexChanged"
                                                                                            EmptyMessage="Select Practice">
                                                                                        </telerik:RadComboBox>


                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlPractice" EnableClientScript="false" ErrorMessage="Please select Practice"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblPOBox" CssClass="label" runat="server" Text="PO Box"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtPOBox" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPOBox" EnableClientScript="false" ErrorMessage="Please enter PO Box"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr runat="server" id="trPracticeOthers" visible="false">
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="Label3" CssClass="label" runat="server" Text="Other Practice Name"></asp:Label><span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtOtherPractice" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtOtherPractice" EnableClientScript="false"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td colspan="2"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblBuildingNo" CssClass="label" runat="server" Text="Building No"></asp:Label>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtBuildingNo" CssClass="textboxPP" runat="server" Text="" MaxLength="10"></asp:TextBox>

                                                                                    </td>

                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblZipCode" CssClass="label" runat="server" Text="ZIP Code"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtZipCode" CssClass="textboxPP" runat="server" Text="" MaxLength="10"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtZipCode" EnableClientScript="false" ErrorMessage="Please enter ZIP Code"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>



                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblEmailAddress" CssClass="label" runat="server" Text="E-Mail Address"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtEmailAddress" CssClass="textboxPP" runat="server" Text="" MaxLength="100"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtEmailAddress" EnableClientScript="false" ErrorMessage="Please enter E-Mail Address"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                                                                            runat="server" ErrorMessage="Please Enter Valid E-Mail Address"
                                                                                            ValidationGroup="MandatoryFields1" ControlToValidate="txtEmailAddress"
                                                                                            CssClass="requiredFieldValidateStyle" ForeColor="Red"
                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*
                                                                                        </asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblBankName" CssClass="label" runat="server" Text="Bank Name"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtBankName" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ControlToValidate="txtBankName" EnableClientScript="false" ErrorMessage="Please enter Bank Name"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <%--   <td class="col1">
                                                                                                                    <asp:Label ID="lblBankAccountNo" CssClass="label" runat="server" Text="Bank Account no."></asp:Label>
                                                                                    
                                                                                                                </td>
                                                                                                                <td class="col2">
                                                                                                                    <asp:TextBox ID="txtBankAccountNo" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>--%>
                                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="txtBankAccountNo" EnableClientScript="false" ErrorMessage="Please enter Bank Account no."
                                                                                                                        InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>--%>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblBankPayeeName" CssClass="label" runat="server" Text="Bank Payee Name"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtBankPayeeName" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtBankPayeeName" EnableClientScript="false" ErrorMessage="Please enter IBAN no."
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblIBANNo" CssClass="label" runat="server" Text="IBAN no."></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtIBANNo" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ControlToValidate="txtIBANNo" EnableClientScript="false" ErrorMessage="Please enter IBAN no."
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblGPSLongitude" CssClass="label" runat="server" Text="GPS - Longitude"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtGPSLongitude" CssClass="textboxPP" runat="server" Text="" MaxLength="15" onkeydown="return isNumericAndDecimal(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtGPSLongitude" EnableClientScript="false" ErrorMessage="Please enter GPS - Longitude"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>

                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblGPSLatitude" CssClass="label" runat="server" Text="GPS - Latitude"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtGPSLatitude" CssClass="textboxPP" runat="server" Text="" MaxLength="15" onkeydown="return isNumericAndDecimal(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtGPSLatitude" EnableClientScript="false" ErrorMessage="Please enter GPS - Latitude"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields1" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>

                                                                            </table>
                                                                        </telerik:RadPageView>
                                                                        <telerik:RadPageView runat="server" ID="PageView2" BackColor="White">
                                                                            <table style="width: 100%">
                                                                                <tr>
                                                                                    <td colspan="4" style="height: 6px;"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblNumberOfDoctors" CssClass="label" runat="server" Text="Number Of Doctors"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtNumberOfDoctors" CssClass="textboxPP" runat="server" Text="" MaxLength="4" onkeydown="return isNumberKey(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtNumberOfDoctors" EnableClientScript="false" ErrorMessage="Please enter Number Of Doctors"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields2" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblNumberOfRooms" CssClass="label" runat="server" Text="Number Of Rooms"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtNumberOfRooms" CssClass="textboxPP" runat="server" Text="" MaxLength="4" onkeydown="return isNumberKey(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtNumberOfRooms" EnableClientScript="false" ErrorMessage="Please enter Number Of Rooms"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields2" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblNumberOfBeds" CssClass="label" runat="server" Text="Number Of Ward/Bed"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtNumberOfBeds" CssClass="textboxPP" runat="server" Text="" MaxLength="4" onkeydown="return isNumberKey(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtNumberOfBeds" EnableClientScript="false" ErrorMessage="Please enter Number Of Beds"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields2" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblNumberOfNurses" CssClass="label" runat="server" Text="Number Of Nurses"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtNumberOfNurses" CssClass="textboxPP" runat="server" Text="" MaxLength="4" onkeydown="return isNumberKey(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtNumberOfNurses" EnableClientScript="false" ErrorMessage="Please enter Number Of Nurses"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields2" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblICUBeds" CssClass="label" runat="server" Text="Number of ICU Beds"></asp:Label><span style="color: red">*</span>

                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtICUBeds" CssClass="textboxPP" runat="server" Text="" MaxLength="4" onkeydown="return isNumberKey(this);">></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator79" runat="server" ControlToValidate="txtICUBeds" EnableClientScript="false" InitialValue="" ValidationGroup="MandatoryFields2" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblNICUBeds" CssClass="label" runat="server" Text="Number of NICU Beds"></asp:Label><span style="color: red">*</span>

                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtNICUBeds" CssClass="textboxPP" runat="server" Text="" MaxLength="4" onkeydown="return isNumberKey(this);">></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator81" runat="server" ControlToValidate="txtNICUBeds" EnableClientScript="false" InitialValue="" ValidationGroup="MandatoryFields2" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblOwnPharmacy" CssClass="label" runat="server" Text="Own Pharmacy"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:DropDownList ID="ddlOwnPharmacy" CssClass="DropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOwnPharmacy_SelectedIndexChanged">
                                                                                            <asp:ListItem Value="-1">-- Select --</asp:ListItem>
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="RfvOwnPharmacy" runat="server" ControlToValidate="ddlOwnPharmacy" EnableClientScript="false" InitialValue="-1" ValidationGroup="MandatoryFields2" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4">
                                                                                        <table>
                                                                                            <tr runat="server" id="trPrahmacy" visible="false">
                                                                                                <td>
                                                                                                    <telerik:RadGrid runat="server" ID="rgPharmacy" AutoGenerateColumns="false" Width="590px" AllowFilteringByColumn="false" Skin="Office2007" AllowPaging="false" OnNeedDataSource="rgPharmacy_NeedDataSource1" MasterTableView-CommandItemSettings-ShowAddNewRecordButton="true" OnItemCommand="rgPharmacy_ItemCommand">
                                                                                                        <MasterTableView ShowHeadersWhenNoRecords="true" NoMasterRecordsText="No records to display" CommandItemDisplay="Top">
                                                                                                            <CommandItemTemplate>
                                                                                                                &nbsp;&nbsp; 
                                                                        <asp:Label runat="server" ID="lblPharmacyHeading" Text="Pharmacy Details" Font-Bold="true" Font-Size="Larger"></asp:Label>&nbsp;&nbsp;
                                                                        <asp:Button runat="server" ID="btnrgAddPharmacy" Text="Add New Record" CommandName="AddPharmacy" CssClass="buttonOld" Width="130px" />
                                                                                                            </CommandItemTemplate>
                                                                                                            <Columns>

                                                                                                                <telerik:GridTemplateColumn HeaderText="Name of Pharmacy">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:TextBox runat="server" ID="txtPharmacyName" Text='<%# Bind("PharmacyName") %>'></asp:TextBox>
                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>
                                                                                                                <telerik:GridTemplateColumn HeaderText="CCHI LIcense Number">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:TextBox runat="server" ID="txtCCHILicenseNo" Text='<%# Bind("PharmacyCCHILinceseNo") %>' Width="100px"></asp:TextBox>
                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>
                                                                                                                <telerik:GridTemplateColumn HeaderText="Expiration Date">
                                                                                                                    <ItemTemplate>
                                                                                                                        <telerik:RadDatePicker ID="RadDatePicker2" Width="120px" runat="server" DateInput-DateFormat="dd MMM yyyy" SelectedDate='<%# Bind("PharmacyCCHILicenseExpiryDate") %>' onkeydown="return isDeleteOrBackSpaceButton(this);">
                                                                                                                        </telerik:RadDatePicker>

                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>
                                                                                                                <%--<telerik:GridBoundColumn HeaderText="Expiration Date" DataField="PharmacyCCHILicenseExpiryDate" DataFormatString="{0:d MMM yyyy}" DataType="System.DateTime" AllowFiltering="false"></telerik:GridBoundColumn>--%>
                                                                                                                <telerik:GridTemplateColumn HeaderText="Action">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:ImageButton runat="server" ID="btnDeletePharmacy" CommandName="DeletePharmacy" Text="Delete" ImageUrl="~/Images/Delete.png" ToolTip="Top" />
                                                                                                                        <asp:Label runat="server" ID="lblPharmacyId" Visible="false" Text='<%# Bind("ProviderPharmacyId") %>'></asp:Label>
                                                                                                                    </ItemTemplate>
                                                                                                                </telerik:GridTemplateColumn>

                                                                                                            </Columns>
                                                                                                            <CommandItemStyle CssClass="rgCommandStyle" />
                                                                                                        </MasterTableView>
                                                                                                    </telerik:RadGrid>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>

                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td style="vertical-align: top; padding-left: 10px;">
                                                                                                    <fieldset style="padding: 10px; width: 350px;">
                                                                                                        <legend>
                                                                                                            <h1 class="Fieldset">
                                                                                                                <asp:Label ID="Label2" runat="server" Text="Facilities"></asp:Label>
                                                                                                            </h1>
                                                                                                        </legend>
                                                                                                        <asp:CheckBoxList runat="server" CssClass="CheckboxList" ID="chkFacility" RepeatColumns="1"></asp:CheckBoxList>
                                                                                                        <br />
                                                                                                        <asp:CheckBoxList runat="server" CssClass="CheckboxList" ID="chkFacilityOthers" OnSelectedIndexChanged="chkFacilityOthers_SelectedIndexChanged" RepeatColumns="1" AutoPostBack="true">
                                                                                                            <asp:ListItem>Others</asp:ListItem>
                                                                                                        </asp:CheckBoxList>
                                                                                                        <table runat="server" visible="false" id="tbFacilityOthers">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="Label4" CssClass="label" runat="server" Text="Specify"></asp:Label>
                                                                                                                    <span style="color: red">*</span> &nbsp;                                                      
                                                                                                                    <asp:TextBox runat="server" ID="txtFacilityOthers" CssClass="textboxPP"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtFacilityOthers" EnableClientScript="false" InitialValue="" ValidationGroup="MandatoryFields2" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </fieldset>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <fieldset style="padding: 10px; width: 350px;">
                                                                                                        <legend>
                                                                                                            <h1 class="Fieldset">
                                                                                                                <asp:Label ID="Label1" runat="server" Text="Specialty"></asp:Label>
                                                                                                            </h1>
                                                                                                        </legend>
                                                                                                        <asp:CheckBoxList runat="server" CssClass="CheckboxList" ID="chkSpecilaity" RepeatColumns="2"></asp:CheckBoxList><br />
                                                                                                        <asp:CheckBoxList runat="server" CssClass="CheckboxList" ID="chkSpecilaityOthers" OnSelectedIndexChanged="chkSpecilaityOthers_SelectedIndexChanged" RepeatColumns="2" AutoPostBack="true">
                                                                                                            <asp:ListItem>Others</asp:ListItem>
                                                                                                        </asp:CheckBoxList><table runat="server" visible="false" id="tbSpecialityOthers">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="Label5" CssClass="label" runat="server" Text="Specify"></asp:Label>
                                                                                                                    <span style="color: red">*</span> &nbsp;  
                                                                                                                    <asp:TextBox runat="server" ID="txtSpecialityOthers" CssClass="textboxPP"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtSpecialityOthers" EnableClientScript="false" InitialValue="" ValidationGroup="MandatoryFields2" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </fieldset>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>

                                                                                </tr>


                                                                            </table>


                                                                        </telerik:RadPageView>
                                                                        <telerik:RadPageView runat="server" ID="PageView3" BackColor="White">
                                                                            <table style="width: 100%">
                                                                                <tr>
                                                                                    <td colspan="4" style="height: 6px;"></td>
                                                                                </tr>
                                                                                <tr>

                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblCCHILicenseNo" CssClass="label" runat="server" Text="CCHI License no"></asp:Label>
                                                                                        <span style="color: red" runat="server" id="spancchi">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtCCHILicenseNo" CssClass="textboxPP" runat="server" MaxLength="30" Text=""></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfvCCHI" runat="server" ControlToValidate="txtCCHILicenseNo" EnableClientScript="false" ErrorMessage="Please enter CCHI License no"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields3" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblCCHILicenseExpirationDates" CssClass="label" runat="server" Text="Expiration Date"></asp:Label>
                                                                                        <span style="color: red" runat="server" id="spancchiDate">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <telerik:RadDatePicker ID="txtCCHILicenseExpirationDates" Width="140px" runat="server" DateInput-DateFormat="dd MMM yyyy" onkeydown="return isDeleteOrBackSpaceButton(this);">
                                                                                        </telerik:RadDatePicker>
                                                                                        <asp:RequiredFieldValidator ID="rfvCCHIDate" runat="server" ControlToValidate="txtCCHILicenseExpirationDates" EnableClientScript="false" ErrorMessage="Please select CCHI License Expiration Date"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields3" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblMOHLicenseNo" CssClass="label" runat="server" Text="MOH License no"></asp:Label>
                                                                                        <span style="color: red" runat="server" id="spanMOH">*</span>
                                                                                    </td>
                                                                                    <td class="col2">

                                                                                        <asp:TextBox ID="txtMOHLicenseNo" CssClass="textboxPP" runat="server" Text="" MaxLength="30"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfvMOH" runat="server" ControlToValidate="txtMOHLicenseNo" EnableClientScript="false" ErrorMessage="Please enter MOH License no"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields3" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>

                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblMOHLicenseED" CssClass="label" runat="server" Text="Expiration Date"></asp:Label>
                                                                                        <span style="color: red" runat="server" id="spanMOHDate">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <telerik:RadDatePicker ID="txtMOHLicenseED" Width="140px" runat="server" DateInput-DateFormat="dd MMM yyyy" onkeydown="return isDeleteOrBackSpaceButton(this);">
                                                                                        </telerik:RadDatePicker>

                                                                                        <asp:RequiredFieldValidator ID="rfvMOHDate" runat="server" ControlToValidate="txtMOHLicenseED" EnableClientScript="false" ErrorMessage="Please select MOH License Expiration Date"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields3" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblCommRegistrationNo" CssClass="label" runat="server" Text="Commercial Registration no"></asp:Label>
                                                                                        <span style="color: red" runat="server" id="spanCRNo">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtCommRegistrationNo" CssClass="textboxPP" runat="server" Text="" MaxLength="30"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="rfvCRNo" runat="server" ControlToValidate="txtCommRegistrationNo" EnableClientScript="false" ErrorMessage="Please enter Commercial Registration no"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields3" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblCommRegistrationNoED" CssClass="label" runat="server" Text="Expiration Date"></asp:Label>
                                                                                        <span style="color: red" runat="server" id="spanCRDate">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <telerik:RadDatePicker ID="txtCommRegistrationNoED" Width="140px" runat="server" DateInput-DateFormat="dd MMM yyyy" onkeydown="return isDeleteOrBackSpaceButton(this);">
                                                                                        </telerik:RadDatePicker>
                                                                                        <asp:RequiredFieldValidator ID="rfvCRDate" runat="server" ControlToValidate="txtCommRegistrationNoED" EnableClientScript="false" ErrorMessage="Please select Commercial Registration no Expiration Date"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields3" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>


                                                                                </tr>
                                                                                <%--     <tr >
                                                                                                                                <td class="col1">
                                                                                                                                    <asp:Label ID="lblDoctorName" CssClass="label" runat="server" Text="Doctor Name"></asp:Label>
                                                                                                                                    <span style="color: red">*</span>
                                                                                                                                </td>
                                                                                                                                <td class="col2">
                                                                                                                                    <asp:TextBox ID="txtDoctorName" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ControlToValidate="txtDoctorName" EnableClientScript="false" ErrorMessage="Please enter Doctor Name"
                                                                                                                                        InitialValue="" ValidationGroup="MandatoryFields" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                                                                </td>
                                                                                                                                <td class="col1">
                                                                                                                                    <asp:Label ID="lblLicenseNo" CssClass="label" runat="server" Text="License no"></asp:Label>
                                                                                                                                    <span style="color: red">*</span>
                                                                                                                                </td>
                                                                                                                                <td class="col2">
                                                                                                                                    <asp:TextBox ID="txtLicenseNo" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ControlToValidate="txtLicenseNo" EnableClientScript="false" ErrorMessage="Please enter License no"
                                                                                                                                        InitialValue="" ValidationGroup="MandatoryFields" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                                                                </td>

                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="col1">
                                                                                                                                    <asp:Label ID="lblSpeciality" CssClass="label" runat="server" Text="Speciality"></asp:Label>
                                                                                                                                    <span style="color: red">*</span>
                                                                                                                                </td>
                                                                                                                                <td class="col2">
                                                                                                                                    <asp:TextBox ID="txtSpeciality" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ControlToValidate="txtSpeciality" EnableClientScript="false" ErrorMessage="Please enter Speciality"
                                                                                                                                        InitialValue="" ValidationGroup="MandatoryFields" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                                                                </td>
                                                                                                                            </tr>--%>
                                                                            </table>

                                                                        </telerik:RadPageView>
                                                                        <telerik:RadPageView runat="server" ID="PageView4" BackColor="White">
                                                                            <table style="width: 100%">
                                                                                <tr>
                                                                                    <td colspan="4" style="height: 6px;"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblContactPerson" CssClass="label" runat="server" Text="Full Name"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtContactPerson" CssClass="textboxPP" runat="server" Text="" MaxLength="50"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ControlToValidate="txtContactPerson" EnableClientScript="false" ErrorMessage="Please enter Contact Person"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields4" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblMobileNo" CssClass="label" runat="server" Text="Mobile no"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtMobileNo" CssClass="textboxPP" runat="server" Text="" MaxLength="20" onkeydown="return isNumberKey(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="txtMobileNo" EnableClientScript="false" ErrorMessage="Please enter Mobile no"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields4" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblContactPersonDept" CssClass="label" runat="server" Text="Department"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtContactPersonDept" CssClass="textboxPP" runat="server" MaxLength="50" Text=""></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ControlToValidate="txtContactPersonDept" EnableClientScript="false" ErrorMessage="Please enter Contact Person's Department"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields4" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblContactPersonEmail" CssClass="label" runat="server" Text="Email Address"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtContactPersonEmail" CssClass="textboxPP" runat="server" MaxLength="100" Text=""></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="txtContactPersonEmail" EnableClientScript="false" ErrorMessage="Please enter Contact Person's Email address"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields4" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                                                                            runat="server" ErrorMessage="Please Enter Valid Contact Person's E-Mail Address"
                                                                                            ValidationGroup="MandatoryFields4" ControlToValidate="txtContactPersonEmail"
                                                                                            CssClass="requiredFieldValidateStyle" ForeColor="Red"
                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*
                                                                                        </asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>

                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblContactPersonPosition" CssClass="label" runat="server" Text="Position or Title"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtContactPersonPosition" CssClass="textboxPP" runat="server" MaxLength="50" Text=""></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtContactPersonPosition" EnableClientScript="false" ErrorMessage="Please enter Contact Person's Department"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields4" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblFaxNumber" CssClass="label" runat="server" Text="Fax Number"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtFaxNumber" CssClass="textboxPP" runat="server" MaxLength="20" Text="" onkeydown="return isNumberKey(this);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ControlToValidate="txtFaxNumber" EnableClientScript="false" ErrorMessage="Please enter Fax Number"
                                                                                            InitialValue="" ValidationGroup="MandatoryFields4" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="col1">
                                                                                        <asp:Label ID="lblFacebookAddress" CssClass="label" runat="server" Text="Facebook Address"></asp:Label>

                                                                                    </td>
                                                                                    <td class="col2">
                                                                                        <asp:TextBox ID="txtFacebookAddress" CssClass="textboxPP" runat="server" MaxLength="50" Text=""></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4">
                                                                                        <%--     <asp:UpdatePanel runat="server" ID="upContactPersons">
                                                                                                                                        <ContentTemplate>--%>
                                                                                        <telerik:RadGrid runat="server" ID="rgAdditionalContacts" AutoGenerateColumns="false" Width="700px" AllowFilteringByColumn="false"
                                                                                            Skin="Office2007" AllowPaging="false" OnNeedDataSource="rgAdditionalContacts_NeedDataSource"
                                                                                            MasterTableView-CommandItemSettings-ShowAddNewRecordButton="true" OnItemCommand="rgAdditionalContacts_ItemCommand">
                                                                                            <MasterTableView ShowHeadersWhenNoRecords="true" NoMasterRecordsText="No records to display" CommandItemDisplay="Top">
                                                                                                <CommandItemTemplate>
                                                                                                    &nbsp;&nbsp; 
                                                                                                                                                        <asp:Label runat="server" ID="lblAdditionalContacts" Text="Additional Contacts" Font-Bold="true" Font-Size="Larger"></asp:Label>&nbsp;&nbsp;
                                                                                                                                                        <asp:Button runat="server" ID="btnrgAddContact" Text="Add New Record" CommandName="AddNewContact" CssClass="buttonOld" Width="130px" />
                                                                                                </CommandItemTemplate>
                                                                                                <Columns>

                                                                                                    <telerik:GridTemplateColumn HeaderText="Person Name*">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox runat="server" ID="txtPersonName" Text='<%# Bind("PC_PersonName") %>' CssClass="textboxPP"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Department*">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox runat="server" ID="txtPersonDepartment" Text='<%# Bind("PC_PersonDepartment") %>' Width="100px" CssClass="textboxPP"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Position*">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox runat="server" ID="txtPersonPosition" Text='<%# Bind("PC_PersonPosition") %>' Width="100px" CssClass="textboxPP"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Contact Number">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox runat="server" ID="txtContactNumber" Text='<%# Bind("PC_ContactNumber") %>' Width="100px" CssClass="textboxPP"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Email Address">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox runat="server" ID="txtConactEmailAddress" Text='<%# Bind("PC_EmailAddress") %>' Width="100px" CssClass="textboxPP"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <%--<telerik:GridBoundColumn HeaderText="Expiration Date" DataField="PharmacyCCHILicenseExpiryDate" DataFormatString="{0:d MMM yyyy}" DataType="System.DateTime" AllowFiltering="false"></telerik:GridBoundColumn>--%>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Action">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton runat="server" ID="btnDeleteContact" CommandName="DeleteNewContact" Text="Delete" ImageUrl="~/images/Delete.png" ToolTip="Top" />
                                                                                                            <asp:Label runat="server" ID="lblAdditionalContactId" Visible="false" Text='<%# Bind("PC_ContactId") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                </Columns>
                                                                                                <CommandItemStyle CssClass="rgCommandStyle" />
                                                                                            </MasterTableView>
                                                                                        </telerik:RadGrid>
                                                                                        <%-- </ContentTemplate>
                                                                                                                                    </asp:UpdatePanel>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </telerik:RadPageView>
                                                                        <telerik:RadPageView runat="server" ID="PageView5" BackColor="White">
                                                                            <table style="width: 100%">

                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <b>Note:</b> Following documents are mandatory for processing your Request.
                                                                                        <br />
                                                                                        <asp:Label Font-Bold="true" runat="server" ID="lblMandatoryDocumentNote" ForeColor="Blue"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="120px">
                                                                                        <asp:Label ID="lblDocumentType" CssClass="label" runat="server" Text="Document Type"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td width="300px">
                                                                                        <asp:DropDownList ID="ddlDocumentType" CssClass="DropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged">
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ControlToValidate="ddlDocumentType" EnableClientScript="false" ErrorMessage="Please select Document Type"
                                                                                            InitialValue="" ValidationGroup="Attachments" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr runat="server" visible="false" id="trDownloadTemplate">
                                                                                    <td>
                                                                                        <asp:Label runat="server" ID="lblDocumentTemplate" Text="Download Template"></asp:Label>
                                                                                    </td>
                                                                                    <td>

                                                                                        <a id="hrefPriceList" runat="server" visible="false" style="cursor: pointer;"
                                                                                            href="~/Docs/Pricelist and Discount Structure.xlsx" title="Price List">Price List </a>

                                                                                        <a id="hrefDoctorDetails" runat="server" visible="false" style="cursor: pointer;" href="~/Docs/Registry of Doctors and Licenses.xlsx" title="Doctor Details">Doctor Details </a>&nbsp;&nbsp; (Please download the available Template and use the same for upload. )
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="lblDocumentUpload" CssClass="label" runat="server" Text="Upload Document"></asp:Label>
                                                                                        <span style="color: red">*</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <telerik:RadAsyncUpload runat="server" ID="AsyncUpload1" Skin="Office2007" ChunkSize="1048576" MultipleFileSelection="Disabled" MaxFileInputsCount="1" AllowedFileExtensions=".jpg,.JPG,.png,.PNG,.gif,.GIF,.bmp,.BMP,.pdf,.PDF,.xls,.XLS,.xlsx,.XSLX"
                                                                                            MaxFileSize="5240000" OnFileUploaded="RadAsyncUpload1_FileUploaded" OnClientValidationFailed="validationFailed" UploadedFilesRendering="BelowFileInput" />
                                                                                  <asp:Label runat="server" ID="lblDOcMessage" CssClass="label" ForeColor="Blue" Text="Max file size is 5MB and supported file types are jpg | png | gif | bmp | pdf | xls | xlsx" ></asp:Label>      
                                                                                        <%--      <telerik:RadProgressArea runat="server" ID="RadProgressArea1" />--%>
                                                                                        <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ControlToValidate="AsyncUpload1" EnableClientScript="false" ErrorMessage="Please select File"
                                                                                                                                        InitialValue="" ValidationGroup="Attachments" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>--%>
                                                                                        <%--   <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="validateRadUpload"
                                                                                                                                        ErrorMessage="Please select at least one Text file" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>--%>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="height: 6px;"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="text-align: right; padding-right: 40%;">
                                                                                        <asp:Button ID="btnUpload" runat="server" Text="Add Document" CssClass="btnProvider" Width="150px" ValidationGroup="Attachments" OnClick="btnUploadAttachments_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="height: 6px;"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="text-align: center;">
                                                                                        <telerik:RadGrid runat="server" ID="rgAttachments" Skin="Office2007" AutoGenerateColumns="false" Width="600px" OnItemCommand="rgAttachments_ItemCommand"
                                                                                            OnNeedDataSource="rgAttachments_NeedDataSource">
                                                                                            <MasterTableView ShowHeadersWhenNoRecords="true" NoMasterRecordsText="No records to disaplay">
                                                                                                <Columns>

                                                                                                    <telerik:GridBoundColumn HeaderText="Document Type" DataField="PD_DocumentType">
                                                                                                    </telerik:GridBoundColumn>
                                                                                                    <telerik:GridHyperLinkColumn DataTextField="PD_DocumentName" HeaderText="View Document" Target="_blank" DataNavigateUrlFields="PD_DocumentPath">
                                                                                                    </telerik:GridHyperLinkColumn>
                                                                                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                                                                                        <ItemTemplate>

                                                                                                            <asp:ImageButton CommandName="DeleteAtt" runat="server" Text="Delete" ID="btnDelete" ImageUrl="~/images/Delete.png" />
                                                                                                            <asp:Label runat="server" ID="lblDocumentId" Text='<%# Bind("PD_DocumentId") %>' Visible="false"></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </telerik:GridTemplateColumn>
                                                                                                    <%--<telerik:GridButtonColumn HeaderText="Delete" ButtonType="ImageButton"    ImageUrl="../images/Delete.png" >
                                                                                                                                                </telerik:GridButtonColumn>--%>
                                                                                                </Columns>
                                                                                            </MasterTableView>

                                                                                        </telerik:RadGrid>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                        </telerik:RadPageView>
                                                                    </telerik:RadMultiPage>


                                                                </div>


                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table style="width: 95%; border: 0px;">
                                                        <tr>
                                                            <td>
                                                                <span style="color: red">*  </span>
                                                                <span>- Indicates Mandatory Fields</span>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right;">
                                                                <asp:Button runat="server" Text="Save as Draft" ID="btnSave" Width="160px" CssClass="btnProvider" OnClick="btnSave_Click" />
                                                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:Button runat="server" Text="Back" ID="btnBack" Width="80px" CssClass="btnProvider" OnClick="btnBack_Click" Visible="false" />
                                                                <asp:Button runat="server" Text="Next" ID="btnNext" Width="80px" CssClass="btnProvider" OnClick="btnNext_Click" ValidationGroup="MandatoryFields1" />
                                                                <asp:Button runat="server" Text="Submit" ID="btnAdd" Width="80px" CssClass="btnProvider" OnClick="btnAdd_Click" Visible="false" ValidationGroup="MandatoryFields" />
                                                                <asp:Button runat="server" Text="Update" ID="btnUpdate" Width="80px" CssClass="btnProvider" ValidationGroup="MandatoryFields" Visible="false" />
                                                                <asp:HiddenField runat="server" ID="hfProviderId" Value="" />
                                                                <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
                                                                </telerik:RadWindowManager>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td runat="server" id="tblProviderSuccessMessage" visible="false"  valign="top" class="style5"  >
                                                  <asp:Label CssClass="label" runat="server" ID="lblSuccessMessage" Font-Size="X-Large" ForeColor="Green">
                                                      We have processed your request.You will shortly receive email from us for future correspondence.
                                                  </asp:Label>  
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>


                                <tr runat="server" id="tblProviderNew" valign="top" class="style5">
                                    <td></td>
                                </tr>


                                <tr id="Tr1" runat="server">
                                    <td style="width: 75%" valign="top">
                                        <div class="demobottom" runat="server" id="Disclamer" visible="false">
                                            "Disclaimer: The mobile number will be used by Bupa to contact you for healthcare services only"

                                        </div>
                                    </td>
                                    <td align="center" valign="bottom">


                                        <dx:ASPxButton ID="btnSubmit" runat="server" Enabled="False" Height="30px"
                                            OnClick="btnSubmit_Click" Text="Submit" Width="120px" ClientIDMode="Static">
                                        </dx:ASPxButton>


                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <%--        <tr>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" ValidationGroup="test" />
            </td>
        </tr>--%>
    </table>
</div>

