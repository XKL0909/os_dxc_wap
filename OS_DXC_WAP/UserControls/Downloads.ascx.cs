﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.Data;

public partial class UserControls_Downloads : System.Web.UI.UserControl
{
       protected void Page_Load(object sender, EventArgs e)
    {
        postRequest("test", "http://www.bupa.com.sa/_layouts/Bupa/GenerateXML.aspx?xmldata=downloads");

       
     }
   

    private void postRequest(String RequestXML, string ServerURL) 
    {
        int timeout = 90000; 
        int connectionLimit = 10; 
        string responseXML = string.Empty; 
        try 
        { 
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(ServerURL);
            webRequest.Timeout = timeout;
            webRequest.ServicePoint.ConnectionLimit = connectionLimit;
            webRequest.Method = "POST"; 
            webRequest.ContentType = "text/xml"; byte[] byteArray = Encoding.UTF8.GetBytes(RequestXML);
            Stream strm = webRequest.GetRequestStream();
            strm.Write(byteArray, 0, byteArray.Length); strm.Close();
            WebResponse webResponse = webRequest.GetResponse();
            Encoding enc = Encoding.GetEncoding("utf-8");
            StreamReader reader = new StreamReader(webResponse.GetResponseStream(), enc);
            
            DataSet ds = new DataSet();
            ds.ReadXml(reader);

            //ds.Tables[0].AsEnumerable().Take(5);
            DataTable dt = new DataTable();
            dt.Columns.Add("title");
            dt.Columns.Add("openlink");
            dt.Columns.Add("downloadlink");
            dt.Columns.Add("size");
            dt.Columns.Add("extension");
            dt.Columns.Add("icon");
            dt.Columns.Add("Category");
            int i = 0;
            string strURL = "";
            foreach (DataRow _row in ds.Tables[0].Rows)
            {
                i = i + 1;
                //if(i <= 5)
                  //  dt.ImportRow(_row);
                try
                {
                    strURL = Convert.ToString(_row["openlink"]).Trim();
                    strURL = strURL.Remove(0, strURL.IndexOf("pdf="));
                    strURL = strURL.Replace("pdf=", "");
                    _row["openlink"] = strURL;
                }
                catch { }
                if (Convert.ToString(_row["title"]).Trim() == "Bupa Logo")
                {
                    dt.ImportRow(_row);
                }
                   if (Convert.ToString(_row["title"]).Trim() == "Bupa Business")
                {
                    dt.ImportRow(_row);
                }
                 if (Convert.ToString(_row["title"]).Trim() == "Declaration Form")
                {
                    dt.ImportRow(_row);
                }
                 if (Convert.ToString(_row["title"]).Trim() == "Membership Maintenance Form")
                {
                    dt.ImportRow(_row);
                }
                 if (Convert.ToString(_row["title"]).Trim() == "Dependents Addition form")
                {
                    dt.ImportRow(_row);
                }
               
                    

                        

            }
            DataList1.DataSource = dt;
            DataList1.DataBind();
            responseXML = reader.ReadToEnd();
            reader.Close();
            webResponse.Close();
        }
        catch (Exception ex) 
        { 
            XmlDocument doc = new XmlDocument(); 
            doc.LoadXml("<HTTPPostError/>"); 
            doc.DocumentElement.InnerText = ex.Message; responseXML = doc.DocumentElement.OuterXml;
        } 
        
    }

   

    protected void DataList1_ItemCreated(object sender, DataListItemEventArgs e)
    {

        
          
            if (e.Item.ItemIndex >= 5)
            {
                e.Item.Visible = false;
            }
       
    }

}