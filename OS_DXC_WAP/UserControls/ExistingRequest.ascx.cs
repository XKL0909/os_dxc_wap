﻿using Bupa.Core.Logging;
using Bupa.Core.Utilities.Configuration;
using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using OS_DXC_WAP.CaesarWS;
//using localPersonService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_ExistingRequest : System.Web.UI.UserControl
{
    string strClientID = string.Empty;
    string strPassedRefNo = string.Empty;
    string strPassedRefType = string.Empty;
    private static OS_DXC_WAP.CaesarWS.ServiceDepot_DNService _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
    private string _uploadCategory = UploadCategory.MEMBER_REREQUEST;
    DataTable DT;

    string refNo, refType, employeeType;

    private Hashtable hasQueryValue;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ClientID"].ToString() != null)
            strClientID = Session["ClientID"].ToString();
        else
            Response.Redirect("../default.aspx");


        hasQueryValue = new Hashtable();
        string queryStringValues = string.Empty;

        if (Request.QueryString.Count > 0)
        {
            try
            {



                var val = Request.QueryString["val"];
                queryStringValues = Cryption.Decrypt(val);
                hasQueryValue = Cryption.GetQueryValue(queryStringValues);

                refNo = hasQueryValue.ContainsKey("refno") ? Convert.ToString(hasQueryValue["refno"]) : string.Empty;
                refType = hasQueryValue.ContainsKey("refType") ? Convert.ToString(hasQueryValue["refType"]) : string.Empty;
                employeeType = hasQueryValue.ContainsKey("EType") ? Convert.ToString(hasQueryValue["EType"]) : string.Empty;
            }
            catch (Exception ex)
            {
               
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = "Invalid rquest!";
                return;
               
            }

        }
        else
        {
            Message1.ForeColor = System.Drawing.Color.Red;
            Message1.Text = "Invalid rquest!";
            return;
        }



        ////if (Request.QueryString["refno"] != null)
        if (!string.IsNullOrEmpty(refNo))
            strPassedRefNo = refNo;
        else
        {
            Message1.ForeColor = System.Drawing.Color.Red;
            Message1.Text = "Please check, No reference number found";
            return;
        }

        ////if (Request.QueryString["refType"] != null)
        if (!string.IsNullOrEmpty(refType))
            strPassedRefType = refType;
        else
        {
            Message1.ForeColor = System.Drawing.Color.Red;
            Message1.Text = "Please check, No reference type found";
            return;
        }

        HideAllPanels();

        _ws.Url = CoreConfiguration.Instance.GetConfigValue(WebPublication.Key_CaesarWSURL);


        //CaesarHelper ch = new CaesarHelper();
        //OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN response = ch.GetRejectedRequestDetailsByTrackInfoRefNo(strPassedRefNo, strClientID);


        //Session.Add("MbrRejListResponse_DN", response);

        if (!IsPostBack)
            LoadCaesarSourcedData(strClientID);

        //DisableAndHide_DDL_Validators_and_TXT_Controls(Page.Controls);


        if (strPassedRefType.Equals("CR"))
        {
            pnlAddEmpDep.Visible = true;
            pnlAddEmpDep.Enabled = true;
            pnlEmpDetails.Visible = true;
            pnlEmpDetails.Enabled = true;
        }
        else if (strPassedRefType.Equals("DE"))
        {
            pnlEmpDetails.Visible = true;
            pnlEmpDetails.Enabled = true;
        }
        else if (strPassedRefType.Equals("DD"))
        {
            pnlEmpDetails.Visible = true;
            pnlEmpDetails.Enabled = true;
        }
        else if (strPassedRefType.Equals("CB"))
        {
            pnlChangeBranch.Visible = true;
            pnlChangeBranch.Enabled = true;
            pnlChangeBranchReason.Visible = true;
            pnlChangeBranchReason.Enabled = true;
            PanelEffectiveDate.Visible = true;
            PanelEffectiveDate.Enabled = true;
        }
        else if (strPassedRefType.Equals("CC"))
        {
            pnlEmpDetails.Visible = true;
            pnlEmpDetails.Enabled = true;
            PanelLevelOfCover.Visible = true;
            PanelLevelOfCover.Enabled = true;
            PanelEffectiveDate.Visible = true;
            PanelEffectiveDate.Enabled = true;

        }
        else if (strPassedRefType.Equals("AE"))
        {
            pnlAddEmpDep.Enabled = true;
            pnlAddEmpDep.Visible = true;

            pnlEmpDetails.Visible = true;
            pnlEmpDetails.Enabled = true;

            PanelLevelOfCover.Visible = true;
            PanelLevelOfCover.Enabled = true;

            PanelEffectiveDate.Visible = true;
            PanelEffectiveDate.Enabled = true;

        }
        else
        {
            pnlAddEmpDep.Enabled = false;
            pnlAddEmpDep.Visible = false;
        }
        KeepTheJavaScriptStateBetweenPostBacks();
        SetupUploader(_uploadCategory);
        if ((Session["done"] != null && !Session["done"].ToString().Equals(strPassedRefNo)) || Session["done"] == null)
        {
            AddDependentInfoToViewState(null);
            if (!IsPostBack)
            {

                LoadRequestByRefNo(strPassedRefNo, strClientID);
            }
            //tblUploader.Visible = false;
        }
        else
        {
            tblUploader.Visible = false;
        }
    }

    private void SetupUploader(string uploadCategory)
    {
        // Setup the Uploader control
        string username = "Unknown Client";
        if (Session[WebPublication.Session_ClientUsername] != null)
            username = Session[WebPublication.Session_ClientUsername].ToString();

        string unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
        long fileByteLimit = long.Parse(unparsedFileSizeLimit);

        string allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
        uploader.SetupUploader(_uploadCategory, username, allowedExtent, fileByteLimit);

        DataBind();
    }
    protected void btnSubmit_Click(object sender, System.EventArgs e) //it was 500 lines :)
    {
        try
        {
            ////if (Request.QueryString["EType"] != null && Request.QueryString["EType"].ToUpper() == "DEP")
            if (!string.IsNullOrEmpty(employeeType) && employeeType.ToUpper() == "DEP" )
            {
            }
            else
            {
                try
                {
                    DateTime dt = Convert.ToDateTime(txtEJoiningDate.GregorianDate);
                }
                catch (Exception)
                {
                    //RadWindowManager1.RadAlert("Invalid Joining date with company. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
                    //return;
                    throw new Exception("Invalid Joining date with company. Please use dd/mm/yyyy format.");
                }
                try
                {
                    DateTime dt = Convert.ToDateTime(txtERequestedStartDate.GregorianDate);
                }
                catch (Exception)
                {
                    //RadWindowManager1.RadAlert("Invalid Start date for medical cover. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
                    //return;
                    throw new Exception("Invalid Start date for medical cover. Please use dd/mm/yyyy format.");
                }
            }

            //To check the name is not exceeding 40 char, because Caesar allow only 40 character maximum
            if (CheckNameCharcterCount(txtEMemberName, litErrorEmployeeExceed40, ddlEIDType))
                goto exitSubmit;

            ReSubmitCaesarRequest();

        }
        catch (Exception ex)
        {
            Message1.ForeColor = System.Drawing.Color.Red;
            Message1.Text = ex.Message + "" + ex.InnerException;
        }
    exitSubmit: ;
    }
    protected void ddlELevelOfCover_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
            OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqContMbrTypeListResponse_DN response;
            request = new OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            long ls_txnid = WebPublication.GenerateTransactionID3();
            request.transactionID = ls_txnid;
            request.contNo = strClientID;
            int intClsID = Convert.ToInt32(ddlELevelOfCover.SelectedValue.ToString());
            request.contCls = intClsID;
            OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN resp = (OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN)Session["MbrRejListResponse_DN"];
            if (resp.detail[0].MbrType.ToLower().Equals("e"))
                request.grp_Type = "E";
            else
                request.grp_Type = "D";

            response = ws.EnqContMbrTypeList(request);
            ddlEMemberType.Items.Clear();
            if (response.status == "0")
            {
                ddlEMemberType.Items.Add(new ListItem("-Select-", "0"));
                for (int i = 0; i < response.detail.Length; i++)
                {
                    ddlEMemberType.Items.Add(new ListItem(response.detail[i].mbrTypeDesc, response.detail[i].mbrType));
                }
            }
            else
            {
                ddlEMemberType.Items.Add(new ListItem("-- Not Applicable --", "0"));
                lblYaqeenError.Text = "The selected level of cover is not applicable for you.";
            }

        }
        catch (Exception ex)
        {
            lblYaqeenError.Text = ex.Message;
        }
    }

    private OS_DXC_WAP.CaesarWS.ReSubTxnRequest_DN CreateResubmit_BasicRequestObject()
    {
        OS_DXC_WAP.CaesarWS.ReSubTxnRequest_DN request = new OS_DXC_WAP.CaesarWS.ReSubTxnRequest_DN();

        try
        {
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            //request.TransactionType = "ADD MEMBER02";
            request.amd_Mbr_Ind = "Y";
            request.refNo = strPassedRefNo;
            request.transactionID = WebPublication.GenerateTransactionID3();
            //if (_isBatch)
            //{
            //    request.BatchIndicator = "Y";
            //}
            //else
            //{
            //    request.BatchIndicator = "N";
            //}
            request.memberName = new String[3];
            request.natureOfIllness = new String[3];
            request.periodOfDisability = new String[3];
            request.presentState = new String[3];
            request.previousMembershipNo = new String[3];
            request.medicalFacility = new String[3];
            for (int j = 0; j < 3; j++)
            {
                request.memberName[j] = "3";
                request.natureOfIllness[j] = "3";
                request.periodOfDisability[j] = "3";
                request.presentState[j] = "3";
                request.previousMembershipNo[j] = "3";
                request.medicalFacility[j] = "3";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return request;

    }
    private OS_DXC_WAP.CaesarWS.AllMbrDetail_DN FillRequestObjectForResubmission()
    {
        try
        {
            OS_DXC_WAP.CaesarWS.AllMbrDetail_DN requestDetails = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
            requestDetails.ContractNo = strClientID;
            requestDetails.BranchCode = ddlEBranchCode.SelectedValue;
            requestDetails.MbrName = txtEMemberName.Text;
            requestDetails.DOB = DateTime.ParseExact(txtEDateOfBirth.GregorianDate, "dd/MM/yyyy", null);

            requestDetails.ClassID = ddlELevelOfCover.SelectedValue;

            OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN response = (OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN)Session["MbrRejListResponse_DN"];
            if (response.errorMessage != null && string.IsNullOrWhiteSpace(response.errorMessage[0].ToString()))
            {
                if (response.detail != null && response.detail[0] != null)
                    requestDetails.MainMemberNo = response.detail[0].MainMbrNo;
            }
            else
            {
                throw new Exception(response.errorMessage[0].ToString());
            }


            requestDetails.DeptCode = txtEDepartmentCode.Text;
            requestDetails.IgamaID = txtEmployeeID.Text;
            requestDetails.SponsorID = txtESponsorId.Text;
            requestDetails.StaffNo = txtEEmployeeNo.Text;
            //char[] delimiters = new char[] { '-' };
            //string[] parts = ddlENationality.SelectedValue.Split(delimiters,
            //                 StringSplitOptions.RemoveEmptyEntries);
            //requestDetails.Nationality = parts[0];
            requestDetails.Nationality = getNationalityForCaesarRequest(ddlENationality);
            requestDetails.Gender = ddlEGender.SelectedValue;
            requestDetails.RequestStartDate = DateTime.ParseExact(txtERequestedStartDate.GregorianDate, "dd/MM/yyyy", null);
            requestDetails.EffectiveDate = DateTime.ParseExact(txtERequestedStartDate.GregorianDate, "dd/MM/yyyy", null);
            requestDetails.JoinDate = DateTime.ParseExact(txtEJoiningDate.GregorianDate, "dd/MM/yyyy", null);
            requestDetails.ArrivalDate = DateTime.ParseExact(txtEJoiningDate.GregorianDate, "dd/MM/yyyy", null);

            requestDetails.PreviousMembershipIndicator = "N";
            if (chkPreviousMember.Checked == true)
            {
                requestDetails.PreviousMembershipIndicator = "Y";
                requestDetails.PreviousMembershipNo = txtPreviousMember.Text;
            }
            if (ddlEJoinReason.SelectedValue != "0")
                requestDetails.Reason = ddlEJoinReason.SelectedValue;

            requestDetails.Title = ddlETitle.SelectedValue;
            requestDetails.MemberType = ddlEMemberType.SelectedValue;
            requestDetails.ID_Type = ddlEIDType.SelectedValue;
            if (ddlEMaritalStatus.SelectedValue != "0")
                requestDetails.Marital_Status = ddlEMaritalStatus.SelectedValue;

            if (!txtEIDExpDate.GregorianDate.Equals("00/00/0000"))
                requestDetails.ID_Expiry_Date = DateTime.ParseExact(txtEIDExpDate.GregorianDate, "dd/MM/yyyy", null);
            requestDetails.MBR_MOBILE_NO = txtEMobileNo.Text;
            requestDetails.SMS_PREF_LANG = "N";
            requestDetails.sql_type = "CSR.SUB_TXN_REC";
            requestDetails.Telephone = txtEMobileNo.Text;
            requestDetails.Phone = txtEMobileNo.Text;
            //AssignSurveyQuestionsValuesViewMode(ref request, 0);
            requestDetails.SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
            requestDetails.CCHI_City_Code = ddDistrict.SelectedValue;
            requestDetails.CCHI_Job_Code = ddProfession.SelectedValue;
            requestDetails.YakeenVerified = hidYakeenEmployee.Value;

            requestDetails.CorrDist = ddDistrict.SelectedValue;
            //requestDetails.ProfCode = ddProfession.SelectedValue;

            if (Session["ClientName"] == null)
                requestDetails.GroupSecretaryName = Session["ClientName"].ToString();

            return requestDetails;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void CheckIdentityAlreadyRegisteredForDependent(OS_DXC_WAP.avayaWS.Service1 svcAvaya, string strIDNo, int intDepIndex, ref string strMsg, ref int intCount)
    {
        try
        {
            string strCount;
            strMsg = "ID/Iqama number already registered on the same contract in our system<br>  رقم الهوية/ الاقامة مسجل مسبقاً ";
            strCount = svcAvaya.GetmbrCountByID(strIDNo, Convert.ToString(Session["ClientID"]));
            if (!string.IsNullOrEmpty(strCount) && Convert.ToInt32(strCount) > 0)
            {
                strMsg = strMsg + "<br> " + Convert.ToString(intDepIndex + 1) + " )- ID/Iqama : " + strIDNo;
                intCount = intCount + 1;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void ReSubmitCaesarRequest()
    {
        try
        {
            int intDepCount = 0;
            if (ViewState["dt"] != null)
            {
                DT = (DataTable)ViewState["dt"];
                intDepCount = DT.Rows.Count;
            }

            OS_DXC_WAP.avayaWS.Service1 _avaya = new OS_DXC_WAP.avayaWS.Service1();
            OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[] allmbrdtl;
            OS_DXC_WAP.CaesarWS.ReSubTxnRequest_DN request = CreateResubmit_BasicRequestObject();

            allmbrdtl = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[intDepCount + 1];
            if (IsAlreadyRegistered(_avaya, txtEmployeeID.Text))
                goto exitSubmit;

            int intDependentErrorCount = 0;
            string strDependentErrorMessage = "";

            if (intDepCount > 0)
            {
                for (int i = 0; i < intDepCount; i++)
                {
                    CheckIdentityAlreadyRegisteredForDependent(_avaya, (String)DT.Rows[i][5], i, ref strDependentErrorMessage, ref intDependentErrorCount);
                    GenerateTheRequestObjectForDependentViewMode(ref allmbrdtl, i);
                }
                if (intDependentErrorCount > 0)
                {
                    litDepMainError.Text = "<span style='color:red'>" + strDependentErrorMessage + "</span>";
                    return;
                }
            }

            allmbrdtl[0] = FillRequestObjectForResubmission();
            allmbrdtl[0].NoPeopleJoin = intDepCount + 1;

            _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            OS_DXC_WAP.CaesarWS.ReSubTxnResponse_DN response;

            request.mbrDetail = allmbrdtl;
            if (Session["ClientUsername"] != null)
            {
                if (Session["ClientUsername"].ToString().Length > 10)
                    request.submitBy = Session["ClientUsername"].ToString().Substring(0, 10);
                else
                    Session["ClientUsername"].ToString();
            }

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReSubTxnRequest_DN> XmlReq = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReSubTxnRequest_DN>();
            //XmlReq.Request(request, "ReSubTxnRequest_DN");

            response = _ws.ReSubTxn(request);

            //ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReSubTxnResponse_DN> XmlResp = new ClassXMLGeneration<OS_DXC_WAP.CaesarWS.ReSubTxnResponse_DN>();
            //XmlResp.Response(response, "ReSubTxnResponse_DN");




            if (response.Status != "0")
                ShowErrorFromCaesar(response);
            else
            {
                ShowSuccessfullySubmitted(response.refNo.ToString());
                pnlDependentDetail.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    exitSubmit: ;
    }

    private void GenerateTheRequestObjectForDependentViewMode(ref OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[] request, int intDepIndex)
    {
        try
        {
            int j = intDepIndex + 1;

            request[j] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
            request[j].ContractNo = strClientID;
            request[j].BranchCode = ddlEBranchCode.SelectedValue;
            request[j].MbrName = (String)DT.Rows[intDepIndex][1];
            request[j].DOB = DateTime.ParseExact((String)DT.Rows[intDepIndex][2], "dd/MM/yyyy", null);
            request[j].ClassID = (String)DT.Rows[intDepIndex][9];
            request[j].MainMemberNo = txtMainMemberNo.Text;
            request[j].DeptCode = (String)DT.Rows[intDepIndex][6];
            request[j].IgamaID = (String)DT.Rows[intDepIndex][5];
            request[j].SponsorID = (String)DT.Rows[intDepIndex][3];
            request[j].StaffNo = (String)DT.Rows[intDepIndex][4];
            request[j].Nationality = (String)DT.Rows[intDepIndex][8];
            request[j].Gender = (String)DT.Rows[intDepIndex][7];
            request[j].RequestStartDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][13], "dd/MM/yyyy", null);
            request[j].EffectiveDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][13], "dd/MM/yyyy", null);
            request[j].JoinDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][12], "dd/MM/yyyy", null);
            request[j].ArrivalDate = DateTime.ParseExact((String)DT.Rows[intDepIndex][12], "dd/MM/yyyy", null);
            request[j].PreviousMembershipIndicator = "N";
            request[j].PreviousMembershipNo = "";
            if ((String)DT.Rows[intDepIndex][10] != "0")
                request[j].Reason = (String)DT.Rows[intDepIndex][10];
            request[j].Title = (String)DT.Rows[intDepIndex][0];
            request[j].MBR_MOBILE_NO = (String)DT.Rows[intDepIndex][14];
            request[j].SMS_PREF_LANG = "N";
            request[j].sql_type = "CSR.SUB_TXN_REC";
            request[j].MemberType = (String)DT.Rows[intDepIndex][15];
            request[j].ID_Type = (String)DT.Rows[intDepIndex][16];
            request[j].YakeenVerified = (String)DT.Rows[intDepIndex][21];
            if ((String)DT.Rows[intDepIndex][17] != "00/00/0000")
                request[j].ID_Expiry_Date = DateTime.ParseExact((String)DT.Rows[intDepIndex][17], "dd/MM/yyyy", null);

            if ((String)DT.Rows[intDepIndex][18] != "0")
                request[j].Marital_Status = (String)DT.Rows[intDepIndex][18];

            request[j].CCHI_Job_Code = (String)DT.Rows[intDepIndex][19];
            request[j].CCHI_City_Code = (String)DT.Rows[intDepIndex][20];
            request[j].Telephone = (String)DT.Rows[intDepIndex][14];
            request[j].Phone = (String)DT.Rows[intDepIndex][14];
            //AssignSurveyQuestionsValuesViewMode(ref request, j);
            request[j].SupportDoc = uploader.SupportDocuments(uploader.SessionID, _uploadCategory);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private bool IsAlreadyRegistered(OS_DXC_WAP.avayaWS.Service1 svcAvaya, string strIDNo)
    {
        try
        {
            string strCount; bool isAlreadyRegistered = false;
            strCount = svcAvaya.GetmbrCountByID(strIDNo, Convert.ToString(Session["ClientID"]));
            if (!string.IsNullOrEmpty(strCount))
            {
                if (strCount.All(char.IsDigit))
                {
                    if (Convert.ToInt32(strCount) > 0)
                    {
                        litMainError.Visible = true;
                        litMainError.Text = "<span style='color:red'>ID/Iqama number already registered on the same contract in our system<br> رقم الهوية/ الاقامة مسجل مسبقاً " + "</span>";
                        isAlreadyRegistered = true;
                    }                   
                }
               
            }

            return isAlreadyRegistered;

        }
        catch (Exception ex)
        {
            throw ex;
        }
        
    }
    private void ShowSuccessfullySubmitted(string strRefNo)
    {
        try
        {
            Message1.ForeColor = System.Drawing.Color.Black;
            Message1.Text = "<br/><br/><br/>Thank you for submitting your request. We are now validating the submitted file." +
                "Your reference no. is " + strRefNo + ".<br/>";
            imgSuc.Visible = true;
            PanelMembershipNoOnly.Visible = false;
            btnSubmit.Visible = false;
            tblUploader.Visible = false;

            HideAllPanels();
            Session.Add("done", strRefNo);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void ShowErrorFromCaesar(OS_DXC_WAP.CaesarWS.ReSubTxnResponse_DN response)
    {
        try
        {
            StringBuilder sb = new StringBuilder(200);
            Message1.Text = "";
            for (int i = 0; i < response.errorMessage.Length; i++)
                sb.Append(response.errorMessage[i]).Append("<br/>");
            Message1.Text += sb.ToString();
            Message1.ForeColor = System.Drawing.Color.Red;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static string ReverseDate(string s)
    {
        try
        {
            return s.Substring(8, 2) + "/" + s.Substring(5, 2) + "/" + s.Substring(0, 4);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private bool CheckNameCharcterCount(TextBox txtName, Literal litError, DropDownList ddlId)
    {
        //we developed this function because some names come from yaqeen exceed 40 charcter
        //Caesar allow only 40 character maximum, we allow the user to edit
        if (txtName.Text.Trim().Length > 40)
        {
            litError.Visible = true;
            litError.Text = "<span style='color:red'>The name exceeded the accepted number of characters “40 characters”." +
                                "Please modify the name in order to process the request <br />" + Environment.NewLine +
                            "عدد أحرف الاسم تخطت الحد المسموح 40 حرف الرجاء تعديل الاسم بحيث لا يتخطى الحد المسموح" +
                            " </span>";
            txtName.Enabled = true;
            txtName.Focus();
            return true;
        }
        else if (txtName.Text.Trim().Length == 0 && ddlId.SelectedValue == "0")
        {
            //this is mean add dependent request
            return false;
        }
        else if (txtName.Text.Trim().Length == 0 && ddlId.SelectedValue != "1")
        {
            litError.Visible = true;
            litError.Text = "<span style='color:red'>The English name is not registered on NIC system. Please write the name and make sure it doesn’t exceed 40 digits<br />" +
                                "الاسم الانجليزي غير مسجل لدى مركزالمعلومات الوطني. الرجاء كتابة الاسم والتأكد بأن لا يتجاوز عدد 40 حرف" +
                            " </span>";
            txtName.Enabled = true;
            txtName.Focus();
            return true;
        }
        else if (txtName.Text.Trim().Length == 0)
        {
            litError.Visible = true;
            litError.Text = "<span style='color:red'>The English name is not registered on NIC system. Please write the name and make sure it doesn’t exceed 40 digits<br />" +
                                "الاسم الانجليزي غير مسجل لدى مركزالمعلومات الوطني. الرجاء كتابة الاسم والتأكد بأن لا يتجاوز عدد 40 حرف" +
                            " </span>";
            txtName.Enabled = true;
            txtName.Focus();
            return true;
        }
        else
        {
            return false;
        }
    }

    #region "View Mode Functions"
    private void LoadRequestByRefNo(string strPassedRefNo, string strContractNo)
    {
        try
        {
            Bupa.OSWeb.Business.CaesarHelper ch = new Bupa.OSWeb.Business.CaesarHelper();
            OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN response = ch.GetRejectedRequestDetailsByTrackInfoRefNo(strPassedRefNo, strContractNo);
            ////OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN response = (OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN)Session["MbrRejListResponse_DN"];
            if (response.errorMessage != null && string.IsNullOrWhiteSpace(response.errorMessage[0].ToString()))
            {
                MaintainEmployeePageControllsInViewMode();
                AssignEmployeeRequestValues(response);
                if (response.detail.Length > 1)
                {
                    for (int k = 1; k < response.detail.Length; k++)
                    {
                        AddDependentInfoToViewState(response.detail[k]);
                    }
                }
            }
            else
            {
                throw new Exception(response.errorMessage[0].ToString());
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("ORA"))
            {
                //Logger.Current.WriteException(ex);
                Logger.Current.WriteException(ex);
                //throw new Exception("There is some internal error, please contact 'Bupa Support Team'");
                Message1.Text = ("There is some internal error, please contact 'Bupa Support Team'");
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Font.Bold = true;
                HideAllPanels();
            }
            else if (ex.Message.Contains("No Rejected or Pending Transaction Found."))
            {
                //Logger.Current.WriteException(ex);
                Logger.Current.WriteException(ex);
                Message1.Text = ("For editing, Request should be in Rejected OR Pending state");
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Font.Bold = true;
                HideAllPanels();
            }
            else if (!string.IsNullOrEmpty(ex.Message))
            {
                Logger.Current.WriteException(ex);
                Message1.Text = (ex.Message);
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Font.Bold = true;
                HideAllPanels();
            }
            else
                throw ex;
        }
    }
    private void AssignEmployeeRequestValues(OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN response)
    {
        try
        {
            CaesarRequest caesarReq = new CaesarRequest();

            ddlELevelOfCover.SelectedValue = response.detail[0].ClsID.Trim();
            ddlELevelOfCover_SelectedIndexChanged(null, null);
            try
            {
                ddlEMemberType.SelectedValue = response.detail[0].MbrType;
            }
            catch (Exception)
            {
                ddlEMemberType.Text = response.detail[0].MbrType;
            }
            txtEmployeeID.Text = response.detail[0].IDCardNo;

            if (response.detail[0].IDCardNo.StartsWith("1"))
                ddlEIDType.SelectedIndex = 1;
            else if (response.detail[0].IDCardNo.StartsWith("2"))
                ddlEIDType.SelectedIndex = 2;
            else
                ddlEIDType.SelectedIndex = 3;

            txtESponsorId.Text = response.detail[0].SponsorID.Trim();

            ddlEMaritalStatus.SelectedValue = response.detail[0].MaritalStatus.Trim().Length > 0 ? response.detail[0].MaritalStatus.Trim() : "";
            ddlETitle.SelectedValue = response.detail[0].Title.Trim().Length > 0 ? response.detail[0].Title.Trim() : "";
            ddlEGender.SelectedValue = response.detail[0].Sex.Trim().Length > 0 ? response.detail[0].Sex.Trim() : "";
            txtEMemberName.Text = response.detail[0].MbrName.Trim().Length > 0 ? response.detail[0].MbrName.Trim() : "";

            ListItem li = ddlENationality.Items.Cast<ListItem>()
                       .Where(x => x.Value.Contains(response.detail[0].CtryName))
                       .LastOrDefault();
            ddlENationality.SelectedIndex = ddlENationality.Items.IndexOf(ddlENationality.Items.FindByText(li.Text));
            //ddlENationality.SelectedValue = response.detail[0].CtryName.Length > 0 ? response.detail[0].CtryName.Trim() : "";

            ddProfession.SelectedValue = response.detail[0].CCHIJobCode.Trim().Length > 0 ? response.detail[0].CCHIJobCode.Trim() : "";
            ddDistrict.SelectedValue = response.detail[0].CCHICityCode.Trim().Length > 0 ? response.detail[0].CCHICityCode.Trim() : "";
            txtEMobileNo.Text = response.detail[0].MbrMobileNo.Trim().Length > 0 ? response.detail[0].MbrMobileNo.Trim() : "";
            ddlEBranchCode.SelectedValue = response.detail[0].BranchCode.Length > 0 ? response.detail[0].BranchCode.Trim() : "";
            txtEEmployeeNo.Text = response.detail[0].StaffNo.Trim().Length > 0 ? response.detail[0].StaffNo.Trim() : "";
            txtEDepartmentCode.Text = response.detail[0].DeptCode.Trim().Length > 0 ? response.detail[0].DeptCode.Trim() : "";
            txtPreviousMember.Text = response.detail[0].PrevMbrNo.Trim().Length > 0 ? response.detail[0].PrevMbrNo.Trim() : "";
            try
            {
                try
                {
                    txtEDateOfBirth.GregorianDate = ReverseDate(response.detail[0].Dob.Trim().Length > 0 ? response.detail[0].Dob.Trim() : "");
                }
                catch (Exception)
                {

                }
                try
                {
                    txtEIDExpDate.GregorianDate = ReverseDate(response.detail[0].IDExpDate.Trim().Length > 0 ? response.detail[0].IDExpDate.Trim() : "");
                }
                catch (Exception)
                {

                }
                try
                {
                    txtEJoiningDate.GregorianDate = ReverseDate(response.detail[0].JoinDate.Trim().Length > 0 ? response.detail[0].JoinDate.Trim() : "");
                }
                catch (Exception)
                {

                }
                try
                {
                    txtERequestedStartDate.GregorianDate = ReverseDate(response.detail[0].EffDate.Trim().Length > 0 ? response.detail[0].EffDate.Trim() : "");
                }
                catch (Exception)
                {

                }

            }
            catch (Exception)
            {

            }
            if (!string.IsNullOrWhiteSpace(response.detail[0].SubReason.Trim()))
                ddlEJoinReason.SelectedValue = response.detail[0].SubReason.Trim();
            txtEmpHijriYearofBirth.Text = txtEDateOfBirth.HijriDate.Substring(6, 4).Trim();
            if (response.detail[0].PrevMbrInd == "Y")
                chkPreviousMember.Checked = true;

            KeepTheJavaScriptStateBetweenPostBacks();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void MaintainEmployeePageControllsInViewMode()
    {
        try
        {
            ddlELevelOfCover.Enabled = true;
            ddlEMemberType.Enabled = true;

            //Yakeen Fields
            ddlEIDType.Enabled = false;
            txtEmployeeID.Enabled = false;
            txtESponsorId.Enabled = false;
            txtEmpHijriYearofBirth.Enabled = false;
            txtEIDExpDate.Enabled = false;
            ddlETitle.Enabled = false;
            ddlEGender.Enabled = false;
            txtEMemberName.Enabled = false;
            if (txtDateOfBirth.HijriDate != null && txtDateOfBirth.HijriDate.ToString() != "00/00/0000" && txtDateOfBirth.GregorianDate != null && txtDateOfBirth.GregorianDate.ToString() != "00/00/0000")
                txtEDateOfBirth.Enabled = false;
            else
                txtEDateOfBirth.Enabled = false;

            ddlENationality.Enabled = false;
            //ddProfession.Enabled = false;
            ddlEMaritalStatus.Enabled = false;

            ddDistrict.Enabled = true;
            txtEMobileNo.Enabled = true;
            ddlEBranchCode.Enabled = true;
            txtEJoiningDate.Enabled = true;
            txtERequestedStartDate.Enabled = true;
            txtEEmployeeNo.Enabled = true;
            txtEDepartmentCode.Enabled = true;
            txtPreviousMember.Enabled = true;
            ddlEJoinReason.Enabled = true;
            pnlEmpDetails.Visible = true;
            btnSubmit.Text = "Update Request";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void KeepTheJavaScriptStateBetweenPostBacks()
    {
        try
        {
            if (ddlEMemberType.SelectedValue == "C")
            {
                revIqama.Attributes.Add("style", "display:block");
            }
            //txtEJoiningDate.Visible = true;
            if (ddlEIDType.SelectedValue == "1")
            {
                revBorder.Enabled = false;
                revIqama.Enabled = false;
                txtEJoiningDate.Enabled = false;
                //txtEJoiningDate.Visible = false;
            }


            else if (ddlEIDType.SelectedValue == "2")
            {
                revBorder.Enabled = false;
                revSuadi.Enabled = false;
                divEmpHijriYearOfBirth.Visible = false;
            }
            else if (ddlEIDType.SelectedValue == "4")
            {
                revIqama.Enabled = false;
                revSuadi.Enabled = false;
                divEmpHijriYearOfBirth.Visible = false;
            }


            //for Dependents
            if (ddlEIDType.SelectedValue == "1")
            {
                revDepBorder.Enabled = false;
                revDepIqama.Enabled = false;
            }
            else if (ddlEIDType.SelectedValue == "2")
            {
                revDepBorder.Enabled = false;
                revDepSuadi.Enabled = false;
                divEmpHijriYearOfBirth.Visible = false;
            }
            else if (ddlEIDType.SelectedValue == "4")
            {
                revDepIqama.Enabled = false;
                revDepSuadi.Enabled = false;
                divEmpHijriYearOfBirth.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region "Bind the dropdown lists with values"

    private void LoadCaesarSourcedData(string ClientID)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            PopulateClassList(ClientID);
            BindDdlProfession();
            BindDdlDistrict();
            BindDdlENationality(false);
            BindDdlNationality(false);
            BindDdlMemberType("");
            BindTitleDDL();
            PopulateBranchList(ClientID);
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }
    private void BindDdlNationality(bool IsGCC)
    {
        NationalityControl MyNationalityC = new NationalityControl();
        List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);

        MyNationalities = MyNationalities.OrderBy(q => q.Nationality1).ToList();

        MyNationalities.Insert(0, new Nationality(0, "0", "-- Select --", false));
        ddlNationality.DataValueField = "Code";
        ddlNationality.DataTextField = "Nationality1";
        ddlNationality.DataSource = MyNationalities;
        ddlNationality.DataBind();
    }
    private void BindDdlMemberType(string aa)
    {
        ListItem ListItem0 = new ListItem();
        ListItem0.Text = "-Select-";
        ListItem0.Value = "0";
        //OS_DXC_WAP.CaesarWS.ReqMbrTypeListRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqMbrTypeListRequest_DN();


        OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqContMbrTypeListResponse_DN response;

        request.transactionID = WebPublication.GenerateTransactionID();
        request.contNo = strClientID;

        int intClsID = 1;
        if (Session["MbrRejListResponse_DN"] != null)
        {
            OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN resp = (OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN)Session["MbrRejListResponse_DN"];
            if (resp != null && resp.detail != null && resp.detail[0] != null && resp.detail[0].ClsID != null)
            {
                if (!string.IsNullOrEmpty(resp.detail[0].ClsID))
                {
                    intClsID = Convert.ToInt32(resp.detail[0].ClsID);
                    request.contCls = intClsID;
                    request.Username = WebPublication.CaesarSvcUsername;
                    request.Password = WebPublication.CaesarSvcPassword;
                    //OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN resp = (OS_DXC_WAP.CaesarWS.MbrRejListResponse_DN)Session["MbrRejListResponse_DN"];
                    if (resp.detail[0].MbrType.ToLower().Equals("e"))
                        request.grp_Type = "E";
                    else
                        request.grp_Type = "D";
                }
                else
                    request.grp_Type = "E";
            }
            else
                request.grp_Type = "E";
        }
        else
            request.grp_Type = "E";
        response = _ws.EnqContMbrTypeList(request);

        ddlEMemberType.DataSource = response.detail;
        ddlEMemberType.DataBind();
        ddlEMemberType.Items.Insert(0, ListItem0);

        //var dict = new Dictionary<int, bool>();
        //foreach (var item in response.detail)
        //{

        //}
        request.grp_Type = "D";
        response = _ws.EnqContMbrTypeList(request);

        ddlMemberType.DataSource = response.detail;
        ddlMemberType.DataBind();
        ddlMemberType.Items.Insert(0, ListItem0);

    }
    private void BindDdlProfession()
    {
        OS_DXC_WAP.CaesarWS.ReqProfListRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqProfListRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        OS_DXC_WAP.CaesarWS.ReqProfListResponse_DN response;
        response = _ws.ReqProfList(request);
        ddProfession.DataSource = response.detail;
        ddProfession.DataBind();
    }
    private void BindDdlDistrict()
    {
        OS_DXC_WAP.CaesarWS.ReqDistListRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqDistListRequest_DN();
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;
        OS_DXC_WAP.CaesarWS.ReqDistListResponse_DN response;
        response = _ws.ReqDistList(request);
        OS_DXC_WAP.CaesarWS.DistListDetail_DN[] MyDistricts = new OS_DXC_WAP.CaesarWS.DistListDetail_DN[response.detail.Length + 1];
        response.detail.CopyTo(MyDistricts, 1);
        MyDistricts[0] = new OS_DXC_WAP.CaesarWS.DistListDetail_DN();
        MyDistricts[0].distCode = "";
        MyDistricts[0].distName = "-Select-";
        ddDistrict.DataSource = MyDistricts;
        ddDistrict.DataBind();
        ddDepDistrict.DataSource = MyDistricts;
        ddDepDistrict.DataBind();

    }
    private void BindTitleDDL()
    {
        ddlETitle.Items.Clear();
        ddlETitle.Items.Add(new ListItem("--Select--", "0"));
        ddlETitle.Items.Add(new ListItem("Mr", "Mr"));
        ddlETitle.Items.Add(new ListItem("Miss", "Miss"));
        ddlETitle.Items.Add(new ListItem("Mrs", "Mrs"));
        ddlETitle.Items.Add(new ListItem("Ms", "Ms"));
    }
    private void BindTitleDepDDL()
    {
        ddlTitle.Items.Clear();
        ddlTitle.Items.Add(new ListItem("--Select--", "0"));
        ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
        ddlTitle.Items.Add(new ListItem("Miss", "Miss"));
        ddlTitle.Items.Add(new ListItem("Mrs", "Mrs"));
        ddlTitle.Items.Add(new ListItem("Ms", "Ms"));
    }
    private void BindDdlENationality(bool IsGCC)
    {
        NationalityControl MyNationalityC = new NationalityControl();
        List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
        MyNationalities.Insert(0, new Nationality(0, "0", "-- Select --", false));
        ddlENationality.DataValueField = "Code";
        ddlENationality.DataTextField = "Nationality1";
        ddlENationality.DataSource = MyNationalities;
        ddlENationality.DataBind();
    }
    private void DisplayBranchListResult(OS_DXC_WAP.CaesarWS.ReqBrhListResponse_DN response)
    {
        if (response.status == "0")
        {
            ddlEBranchCode.Items.Clear();
            ListItem DepList0 = new ListItem();
            DepList0.Text = "-- Select --";
            DepList0.Value = "0";
            ddlEBranchCode.Items.Add(DepList0);
            foreach (OS_DXC_WAP.CaesarWS.BrhListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.branchDesc.Trim();
                DepList.Value = dtl.branchCode.Trim().ToString();
                ddlEBranchCode.Items.Add(DepList);
            }
        }
        else
        {
            litMainError.Visible = true;
            litMainError.Text = "<span style='color:red'>Please refresh the page (Ctrl+F5)</span>";
        }
    }
    protected void PopulateBranchList(string ClientID)
    {
        {
            OS_DXC_WAP.CaesarWS.ReqBrhListRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqBrhListRequest_DN();
            OS_DXC_WAP.CaesarWS.ReqBrhListResponse_DN response;
            request.membershipNo = long.Parse(strClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();

            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = _ws.ReqBrhList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                }
                else
                {
                    DisplayBranchListResult(response);
                }
            }
            catch (Exception ex)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = ex.Message;
            }
        }
    }
    private void DisplayClassListResult(OS_DXC_WAP.CaesarWS.ReqClsListResponse_DN response)
    {
        if (response.status == "0")
        {
            ddlELevelOfCover.Items.Clear();
            ddlLevelOfCover.Items.Clear();
            ListItem DepList0 = new ListItem();
            DepList0.Text = "-Select-";
            DepList0.Value = "0";
            ddlELevelOfCover.Items.Add(DepList0);

            foreach (OS_DXC_WAP.CaesarWS.ClsListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.className;
                DepList.Value = dtl.classID.ToString();
                ddlELevelOfCover.Items.Add(DepList);
            }

            ListItem DepListEmpl = new ListItem();
            DepList0.Text = "-Select-";
            DepList0.Value = "0";
            ddlLevelOfCover.Items.Add(DepListEmpl);
            foreach (OS_DXC_WAP.CaesarWS.ClsListDetail_DN dtl in response.detail)
            {
                ListItem DepList = new ListItem();
                DepList.Text = dtl.className;
                DepList.Value = dtl.classID.ToString();
                ddlLevelOfCover.Items.Add(DepList);
            }
        }
        else
        {
        }
    }
    protected void PopulateClassList(string ClientID)
    {
        {
            OS_DXC_WAP.CaesarWS.ReqClsListRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqClsListRequest_DN();
            OS_DXC_WAP.CaesarWS.ReqClsListResponse_DN response;
            request.contractNo = long.Parse(strClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();
            try
            {
                StringBuilder sb = new StringBuilder(200);
                response = _ws.ReqClsList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                    Message1.Text = sb.ToString();
                }
                else
                {
                    DisplayClassListResult(response);
                }
            }
            catch (Exception ex)
            {
                Message1.ForeColor = System.Drawing.Color.Red;
                Message1.Text = ex.Message;
            }
        }
    }
    // ideally you should keep this into any utility class and call it from the page_load event
    public static string DisableTheButton(Control pge, Control btn)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') {");
        sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
        sb.Append("this.value = 'Please wait...';");
        sb.Append("this.disabled = true;");
        sb.Append(pge.Page.GetPostBackEventReference(btn));
        sb.Append(";");
        return sb.ToString();
    }

    #endregion

    private void DisableAndHide_DDL_Validators_and_TXT_Controls(ControlCollection cntrlList)
    {
        return;

        foreach (Control control in cntrlList)
        {
            if (control is DropDownList)
            {
                DropDownList ddlThis = (DropDownList)control;
                ddlThis.Enabled = false;
                ddlThis.Attributes.Add("disabled", "disabled");
                control.Visible = false;
            }
            else
                if (control is TextBox)
                {
                    TextBox txtThis = (TextBox)control;
                    txtThis.Enabled = false;
                    control.Visible = false;
                }

                else if (control is RequiredFieldValidator)
                {
                    RequiredFieldValidator rfvThis = (RequiredFieldValidator)control;
                    rfvThis.Enabled = false;
                    rfvThis.Attributes.Add("disabled", "disabled");
                }
                else if (control is Label)
                {
                    Label lblThis = (Label)control;
                    lblThis.Visible = false;
                }
                else
                {
                    //DisableAndHide_DDL_Validators_and_TXT_Controls(control.Controls);
                }
        }
    }


    private void EnableControlsforDeleteEmployeeRequest()
    {
        return;

        ddlEIDType.Visible = true;
        ddlEMaritalStatus.Visible = true;
        txtEmployeeID.Visible = true;
        txtEIDExpDate.Visible = true;
        ddProfession.Visible = true;
        ddDistrict.Visible = true;
        ddlENationality.Visible = true;
        txtEMobileNo.Visible = true;
        ddlEJoinReason.Visible = false;
        txtERequestedStartDate.Visible = true;

        ddlEIDType.Enabled = true;
        ddlEMaritalStatus.Enabled = true;
        txtEmployeeID.Enabled = true;
        txtEIDExpDate.Enabled = true;
        ddProfession.Enabled = true;
        ddDistrict.Enabled = true;
        ddlENationality.Enabled = true;
        txtEMobileNo.Enabled = true;
        ddlEJoinReason.Enabled = false;
        txtERequestedStartDate.Enabled = true;

    }

    protected void phCardReplaceReason_Load(object sender, EventArgs e)
    {
        if (strPassedRefType.Equals("CR"))
            ((Control)sender).Visible = true;
        else
        {
            ((Control)sender).Visible = false;
        }

    }
    protected void phDeletionReason_Load(object sender, EventArgs e)
    {
        if (strPassedRefType.Equals("DE"))
            ((Control)sender).Visible = true;
        else
        {
            ((Control)sender).Visible = false;
        }
    }
    protected void phChangeBranch_Load(object sender, EventArgs e)
    {
        if (strPassedRefType.Equals("CB"))
            ((Control)sender).Visible = true;
        else
        {
            ((Control)sender).Visible = false;
        }
    }
    protected void phChangeClass_Load(object sender, EventArgs e)
    {
        if (strPassedRefType.Equals("CC"))
        {
            ((Control)sender).Visible = true;
            PanelChangeClass.Enabled = true;
        }
        else
        {
            ((Control)sender).Visible = false;
        }
    }
    protected void phAddEmpDep_Load(object sender, EventArgs e)
    {
        if (strPassedRefType.Equals("AE"))
            ((Control)sender).Visible = true;

    }

    private void HideAllPanels()
    {
        pnlAddEmpDep.Enabled = false;
        pnlAddEmpDep.Visible = false;

        pnlEmpDetails.Visible = false;
        pnlEmpDetails.Enabled = false;

        PanelLevelOfCover.Visible = false;
        PanelLevelOfCover.Enabled = false;

        PanelEffectiveDate.Visible = false;
        PanelEffectiveDate.Enabled = false;
    }

    protected void gvDependents_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void gvDependents_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference((System.Web.UI.Control)sender, "Select$" + e.Row.RowIndex.ToString()));

    }

    public void GetSelectedDependentInfo_AndBindToForm(int intIndex)
    {
        pnlDependentDetail.Visible = true;
        DT = (DataTable)ViewState["dt"];
        txtMemberName.Text = (String)DT.Rows[intIndex][1];
        txtMemberName.Enabled = false;
        txtDateOfBirth.GregorianDate = (String)DT.Rows[intIndex][2];
        txtDateOfBirth.Enabled = false;
        txtSponsorId.Text = (String)DT.Rows[intIndex][3];
        txtSponsorId.Enabled = false;
        txtEmployeeNo.Text = (String)DT.Rows[intIndex][4];
        txtDependentID.Text = (String)DT.Rows[intIndex][5];
        txtDependentID.Enabled = false;
        if (txtDependentID.Text == "")
        {
            rfvDepHijriYearOfBirth.Visible = false;
            txtDateOfBirth.Enabled = true;
            txtIDExpDate.Enabled = true;
            rfv_ID.Visible = false;
        }
        else
        {
            rfvDepHijriYearOfBirth.Visible = true;
            txtDateOfBirth.Enabled = false;
            txtIDExpDate.Enabled = false;
            rfv_ID.Enabled = true;
        }
        txtDepartmentCode.Text = (String)DT.Rows[intIndex][6];
        ddlGender.SelectedValue = (String)DT.Rows[intIndex][7];
        ddlGender.Enabled = false;
        ddlLevelOfCover.SelectedValue = (String)DT.Rows[intIndex][9];
        if (DT.Rows[intIndex][10].ToString() != "")
            ddlJoinReason.SelectedValue = (String)DT.Rows[intIndex][10];
        txtJoiningDate.GregorianDate = (String)DT.Rows[intIndex][12];
        txtRequestedStartDate.GregorianDate = (String)DT.Rows[intIndex][13];
        txtMobileNo.Text = DT.Rows[intIndex][14].ToString().Trim();
        ddlMemberType.SelectedIndex = ddlMemberType.Items.IndexOf(ddlMemberType.Items.FindByValue(DT.Rows[intIndex][15].ToString()));// (String)DT.Rows[intIndex][15];
        if (ddlMemberType.SelectedItem.Value == "S")
        {
            SetTheDefaultValuesForSpouse();
        }
        else if (ddlMemberType.SelectedItem.Value == "C")
        {
            SetTheDefaultValuesForChild();
        }
        else
        {
            BindTitleDepDDL();
        }
        ddlTitle.SelectedValue = (String)DT.Rows[intIndex][0];
        ddlTitle.Enabled = false;
        ddlIDType.SelectedValue = (String)DT.Rows[intIndex][16];
        ddlIDType.Enabled = false;
        if (ddlIDType.SelectedItem.Value == "1")
            SelectSaudiNationalityForSaudiMembers(ddlNationality);
        else
        {
            setNationalityinDropDownList(DT.Rows[intIndex][8].ToString(), ddlNationality);
            //ddlNationality.SelectedIndex = ddlNationality.Items.IndexOf(ddlNationality.Items.FindByValue(DT.Rows[intIndex][8].ToString()));// (String)DT.Rows[intIndex][8];
        }
        txtIDExpDate.GregorianDate = (String)DT.Rows[intIndex][17];
        //txtIDExpDate.Enabled = false;
        ddlMaritalStatus.SelectedValue = (String)DT.Rows[intIndex][18];
        ddDepProfession.SelectedValue = (String)DT.Rows[intIndex][19];
        if (DT.Rows[intIndex][20].ToString() != "")
            ddDepDistrict.SelectedValue = (String)DT.Rows[intIndex][20];
        hidYakeenDependent.Value = (String)DT.Rows[intIndex][21];
        DT.Rows[intIndex]["DOBHijriYear"] = txtDateOfBirth.HijriDate.Substring(txtDateOfBirth.HijriDate.Length - 4);
        txtDepHijriYearofBirth.Text = DT.Rows[intIndex]["DOBHijriYear"].ToString();
        txtDepHijriYearofBirth.Enabled = false;

        revDepSuadi.Enabled = false;
        revDepIqama.Enabled = false;
        revDepBorder.Enabled = false;
    }

    private void setNationalityinDropDownList(string NationalityCode, DropDownList ddl)
    {
        ListItem li = ddl.Items.Cast<ListItem>()
                       .Where(x => x.Value.Contains(NationalityCode))
                       .LastOrDefault();
        ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(li.Text));
    }

    private void SelectSaudiNationalityForSaudiMembers(DropDownList Nationality)
    {
        try
        {
            Nationality.SelectedItem.Text = "Saudi Arabia";
            Nationality.SelectedItem.Value = "966";
            Nationality.Enabled = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void SetTheDefaultValuesForSpouse()
    {
        try
        {

            ddlMaritalStatus.SelectedValue = "2";
            ddlMaritalStatus.Enabled = false;
            ddDepProfession.SelectedValue = "99401";
            ddDepProfession.Enabled = false;

            ddlTitle.Items.Clear();

            if (ddlGender.SelectedValue == "F")
            {
                ddlTitle.Items.Add(new ListItem("--Select--", "0"));
                ddlTitle.Items.Add(new ListItem("Mrs", "Mrs"));
                ddlTitle.Items.Add(new ListItem("Ms", "Ms"));
            }
            else if (ddlGender.SelectedValue == "M")
            {
                ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
                ddlTitle.SelectedValue = "Mr";
                ddlTitle.Enabled = false;
            }
            else
            {
                ddlTitle.Items.Add(new ListItem("--Select--", "0"));
                ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
                ddlTitle.Items.Add(new ListItem("Ms", "Ms"));
                ddlTitle.Items.Add(new ListItem("Mrs", "Mrs"));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void SetTheDefaultValuesForChild()
    {
        try
        {
            ddlMaritalStatus.SelectedValue = "1";
            ddlMaritalStatus.Enabled = false;
            ddDepProfession.SelectedValue = "99402";
            ddDepProfession.Enabled = false;

            ddlTitle.Items.Clear();
            if (ddlGender.SelectedValue == "F")
            {
                ddlTitle.Items.Add(new ListItem("Miss", "Miss"));
                ddlTitle.SelectedValue = "Miss";
                ddlTitle.Enabled = false;
            }
            else if (ddlGender.SelectedValue == "M")
            {
                ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
                ddlTitle.SelectedValue = "Mr";
                ddlTitle.Enabled = false;
            }
            else
            {
                ddlTitle.Items.Add(new ListItem("--Select--", "0"));
                ddlTitle.Items.Add(new ListItem("Mr", "Mr"));
                ddlTitle.Items.Add(new ListItem("Miss", "Miss"));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvDependents_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selectIndex = gvDependents.SelectedIndex;

        GetSelectedDependentInfo_AndBindToForm(selectIndex);
        Session["Parm"] = selectIndex;
        //param = (int)Session["Parm"];
        btnUpdateDep.Visible = true;
        btnCancelUpdateDep.Visible = true;

        lblYaqeenDepError.Visible = false;
        pnlDependentDetail.Visible = true;

        gvDependents.DataSource = DT;
        gvDependents.DataBind();
        // GetAllDependentsName();
    }

    //public void GetAllDependentsName()
    //{
    //    lblAllDependentsName.Text = "";
    //    lblEmployeeName.Text = txtEMemberName.Text;
    //    for (int i = 0; i < gvDependents.Rows.Count; i++)
    //    {
    //        if (i == 0)
    //        {
    //            lblAllDependentsName.Text = gvDependents.Rows[i].Cells[1].Text;
    //        }
    //        else
    //        {
    //            lblAllDependentsName.Text = lblAllDependentsName.Text + "; " + gvDependents.Rows[i].Cells[1].Text;
    //        }
    //    }
    //}

    protected bool CheckUnderAge(DateTime _bDate)
    {
        try
        {
            bool _Underage = false;
            DateTime _current = DateTime.Today;
            TimeSpan span = _current.Subtract(_bDate);
            int days = span.Days;
            if (days <= 90)
            {
                _Underage = true;
            }
            else
            {
                _Underage = false;
            }
            return _Underage;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void AddDependentInfoToViewState(OS_DXC_WAP.CaesarWS.MbrRejListDetail_DN objMemberInfo)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            if (ViewState["dt"] != null)
                DT = (DataTable)ViewState["dt"];
            else
            {
                DataSet DS = new DataSet();
                DS.ReadXmlSchema(Server.MapPath("DepdFile.xml"));
                Session["Dataset"] = DS;
                DT = DS.Tables[0];
            }
            if (objMemberInfo != null)
            {
                DataRow dr = DT.NewRow();
                dr[0] = objMemberInfo.Title;// ddlTitle.SelectedValue;
                dr[1] = objMemberInfo.MbrName;// txtMemberName.Text;
                dr[2] = Convert.ToDateTime(objMemberInfo.Dob).ToString("dd/MM/yyyy");// txtDateOfBirth.GregorianDate;
                dr[3] = objMemberInfo.SponsorID;// txtSponsorId.Text;
                dr[4] = "";// objMemberInfo.IDCardNo;
                dr[5] = objMemberInfo.IDCardNo;
                dr[6] = "";// objMemberInfo.;// txtDepartmentCode.Text;
                dr[7] = objMemberInfo.Sex;// ddlGender.SelectedValue;
                dr[8] = objMemberInfo.CtryName;// ddlNationality.SelectedValue;
                dr[9] = objMemberInfo.ClsID;// ddlLevelOfCover.SelectedValue;                
                dr[10] = "";// objMemberInfo.ddlJoinReason.SelectedValue;               
                dr[12] = Convert.ToDateTime(objMemberInfo.JoinDate).ToString("dd/MM/yyyy");// txtJoiningDate.GregorianDate;
                dr[13] = Convert.ToDateTime(objMemberInfo.EffDate).ToString("dd/MM/yyyy");// txtRequestedStartDate.GregorianDate;
                dr[14] = objMemberInfo.MbrMobileNo.Trim();// txtMobileNo.Text;
                dr[15] = objMemberInfo.MbrType;// ddlMemberType.SelectedValue;
                dr[16] = objMemberInfo.IDType;// ddlIDType.SelectedValue;
                dr[17] = Convert.ToDateTime(objMemberInfo.IDExpDate).ToString("dd/MM/yyyy");// txtIDExpDate.GregorianDate;
                dr[18] = objMemberInfo.MaritalStatus;// ddlMaritalStatus.SelectedValue;
                dr[19] = objMemberInfo.CCHIJobCode;// ddDepProfession.SelectedValue;
                dr[20] = objMemberInfo.CCHICityCode;// ddDepDistrict.SelectedValue;
                dr[21] = objMemberInfo.YakenVerifInd;// hidYakeenDependent.Value;
                dr["DOBHijriYear"] = "";// txtDepHijriYearofBirth.Text;
                DT.Rows.Add(dr);
            }
            //Reset_All_Input_Fields();
            gvDependents.DataSource = DT;
            gvDependents.DataBind();
            //GetAllDependentsName();

            ViewState["dt"] = DT;

        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }


    public bool UpdateDependentValuesInGrid(int index)
    {
        if (CheckUnderAge(Convert.ToDateTime(txtDateOfBirth.GregorianDate)) || ddlAgeGroup.SelectedValue != "1" || txtDependentID.Text.Trim() != "")
        {
            DT = (DataTable)ViewState["dt"];
            DT.Rows[index][0] = ddlTitle.SelectedValue;
            DT.Rows[index][1] = txtMemberName.Text;
            DT.Rows[index][2] = txtDateOfBirth.GregorianDate;
            DT.Rows[index][3] = txtSponsorId.Text;
            DT.Rows[index][4] = txtEmployeeNo.Text;
            DT.Rows[index][5] = txtDependentID.Text;
            DT.Rows[index][6] = txtDepartmentCode.Text;
            DT.Rows[index][7] = ddlGender.SelectedValue;
            DT.Rows[index][8] = getNationalityForCaesarRequest(ddlNationality);
            DT.Rows[index][9] = ddlLevelOfCover.SelectedValue;
            DT.Rows[index][10] = ddlJoinReason.SelectedValue;
            DT.Rows[index][12] = txtJoiningDate.GregorianDate;
            DT.Rows[index][13] = txtRequestedStartDate.GregorianDate;
            DT.Rows[index][14] = txtMobileNo.Text;
            DT.Rows[index][15] = ddlMemberType.SelectedValue;
            DT.Rows[index][16] = ddlIDType.SelectedValue;
            DT.Rows[index][17] = txtIDExpDate.GregorianDate;
            DT.Rows[index][18] = ddlMaritalStatus.SelectedValue;
            DT.Rows[index][19] = ddDepProfession.SelectedValue;
            DT.Rows[index][20] = ddDepDistrict.SelectedValue;
            DT.Rows[index][21] = hidYakeenDependent.Value;
            Reset_All_Input_Fields();
            gvDependents.DataSource = DT;
            gvDependents.DataBind();
            //GetAllDependentsName();

            ViewState["dt"] = DT;
            return true;
        }
        else
        {
            //litDepMainError.Text = "<br /><span style='color:red'>Child age is more than 90 days. Please make sure you entered the correct date of birth and try again or select Age Group “Older than 3 months” and provide ID/Iqama number.<br /></span> " +
            //                      "<br /><span style='color:red' dir='rtl'>" + "عمر الطفل يتجاوز 3 أشهر. الرجاء التأكد من تاريخ الميلاد المدخل والمحاولة مرة أخرى أواختيارالفئة العمرية (Age Group) أكبر من 3 أشهر (Older than 3 months) وتزويدنا برقم الهوية/الإقامة." + "</p></span>";
            // btnGetMemberDetails.Focus();

            return false;
        }
    }

    private string getNationalityForCaesarRequest(DropDownList ddl)
    {
        char[] delimiters = new char[] { '-' };
        string[] parts = ddl.SelectedValue.Split(delimiters,
                        StringSplitOptions.RemoveEmptyEntries);
        return parts[0];
    }
    protected void ddlLevelOfCover_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
            OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqContMbrTypeListResponse_DN response;
            request = new OS_DXC_WAP.CaesarWS.EnqContMbrTypeListRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;            
            long ls_txnid = WebPublication.GenerateTransactionID3();
            request.transactionID = ls_txnid;
            request.contNo = strClientID;
            int intClsID = Convert.ToInt32(ddlLevelOfCover.SelectedValue.ToString());
            request.contCls = intClsID;
            request.grp_Type = "D";
            response = ws.EnqContMbrTypeList(request);
            ddlMemberType.Items.Clear();
            lblYaqeenDepError.Text = "";
            if (response.status == "0")
            {
                ddlMemberType.Items.Add(new ListItem("-- Select --", "0"));
                for (int i = 0; i < response.detail.Length; i++)
                {
                    ddlMemberType.Items.Add(new ListItem(response.detail[i].mbrTypeDesc, response.detail[i].mbrType));
                }
            }
            else
            {
                ddlMemberType.Items.Add(new ListItem("-- Not Applicable --", "0"));
                lblYaqeenDepError.Visible = true;
                lblYaqeenDepError.Text = "The selected level of cover is not applicable for you.";
            }
        }
        catch (Exception ex)
        {
            lblYaqeenDepError.Text = ex.Message;
        }
    }

    protected void btnUpdateDep_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(txtJoiningDate.GregorianDate);
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid Arrival date in KSA. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
            return;
            //throw new Exception("Invalid Joining Date");
        }

        try
        {
            DateTime dt = Convert.ToDateTime(txtRequestedStartDate.GregorianDate);
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid Start date for medical cover. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
            return;
            //throw new Exception("Invalid Requested Date");
        }

        try
        {
            DateTime dt = Convert.ToDateTime(txtIDExpDate.GregorianDate);
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid ID Expiry date. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
            return;
            //throw new Exception("Invalid Requested Date");
        }

        try
        {
            DateTime dt = Convert.ToDateTime(txtDateOfBirth.GregorianDate);
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid Date of Birth. Please use dd/mm/yyyy format.", 330, 180, "Validation Message", "");
            return;
            //throw new Exception("Invalid Requested Date");
        }
        if (UpdateDependentValuesInGrid(gvDependents.SelectedIndex) == true)
        {
            gvDependents.SelectedIndex = -1;
            Reset_All_Input_Fields();
            btnUpdateDep.Visible = false;
            btnSubmit.Visible = true;
            pnlDependentDetail.Visible = false;
            btnCancelUpdateDep.Visible = false;
            //btnAdd.Visible = true;
        }
    }
    protected void btnCancelUpdateDep_Click(object sender, EventArgs e)
    {
        btnUpdateDep.Visible = false;
        gvDependents.SelectedIndex = -1;
        Reset_All_Input_Fields();

        btnCancelUpdateDep.Visible = false;
        btnSubmit.Visible = true;
        // btnAddDep.Visible = true;
        gvDependents.Visible = true;
        DT = (DataTable)ViewState["dt"];
        gvDependents.DataSource = DT;
        gvDependents.DataBind();
        //GetAllDependentsName();
        pnlDependentDetail.Visible = false;
        //lnkBtnAddNewDep.Visible = true;
    }

    public void Reset_All_Input_Fields()
    {
        txtMemberName.Text = "";
        txtDateOfBirth.Clear();
        txtEmployeeNo.Text = "";
        txtDependentID.Text = "";
        txtDepartmentCode.Text = "";
        txtDepHijriYearofBirth.Text = "";
        ddlGender.SelectedIndex = 0;
        ddlNationality.SelectedIndex = -1;
        ddlLevelOfCover.SelectedIndex = 0;
        ddlJoinReason.SelectedIndex = 0;
        txtJoiningDate.Clear();
        txtRequestedStartDate.Clear();
        txtMobileNo.Text = "";
        ddlMemberType.SelectedIndex = 0;
        ddlAgeGroup.SelectedIndex = 0;
        divAgeGroup.Attributes.Add("style", "display:none");
        ddlIDType.SelectedIndex = 0;
        txtIDExpDate.Clear();
        ddlMaritalStatus.SelectedIndex = 0;
        ddDepProfession.SelectedIndex = 0;
        ddDepDistrict.SelectedIndex = 0;
        gvDependents.DataBind();
    }

    public class ClassXMLGeneration<T>
    {
        public void Request(T classType, string req)
        {
            System.Xml.Serialization.XmlSerializer xmlSrQ = new System.Xml.Serialization.XmlSerializer(classType.GetType());
            System.IO.StreamWriter file = new StreamWriter("C:\\Temp\\" + req + ".xml");
            xmlSrQ.Serialize(file, classType);
            file.Close();


        }
        public void Response(T classType, string resp)
        {
            System.Xml.Serialization.XmlSerializer xmlSr = new System.Xml.Serialization.XmlSerializer(classType.GetType());
            System.IO.StreamWriter file = new StreamWriter("C:\\Temp\\" + resp + ".xml");
            xmlSr.Serialize(file, classType);
            file.Close();
        }

    }

}