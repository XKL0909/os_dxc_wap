﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_DashBoardWelcome" Codebehind="DashBoardWelcome.ascx.cs" %>
<style type="text/css">
    .style1
    {
        text-align: justify;
    }
</style>
<div class="careerBanner">
    <div runat="server" id="member" visible="false">
        <h2 class="dahsboardTitle">Message From Bupa</h2>
        <div style="text-align: justify; height: 100px; line-height: 150%">
            Welcome to Bupa online services. In this area you can view your membership details, 
            coverage limit and your provider network.  In addition to these you can also access many personalized services.<br />
            <br />
            Access your online services from your phone with 
            <a class="dahsboardTitle" href="http://www.bupa.com.sa/English/HealthandWellness/Pages/Bupa-Arabia.aspx" target="_blank" style="text-decoration: none; font-weight: bold;">Bupa Arabia mobile app</a>
            , available in English and Arabic.<br />
            <span style="float: right; direction: rtl; font-family: Tahoma; width: 100%;">استمتع بالخدمات الإلكترونية عن طريق جوالك مع 
                <a class="dahsboardTitle" href="http://www.bupa.com.sa/arabic/healthandwellness/pages/bupa-arabia.aspx" target="_blank" style="text-decoration: none; font-weight: bold;">تطبيق بوبا العربية</a>، 
                التطبيق متاح باللغتين العربية والإنجليزية.
            </span>
        </div>
    </div>
    <div runat="server" id="client" visible="false">
        <h2 class="dahsboardTitle">Message From Bupa</h2>
        <div style="text-align: justify">
            <asp:Label ID="lblClient" runat="server" Text="As a SME or a corporate client you can benefit from the Bupa Arabia online services for clients. You can choose from a wide range of services namely; ability to manage and submit maintenance request, view your employee details and many other services."></asp:Label>
        </div>
    </div>
    <div runat="server" id="broker" visible="false">
        <h2 class="dahsboardTitle">Message From Bupa</h2>
        <div style="text-align: justify">
            <asp:Label ID="Label1" runat="server" Text="As a SME or a corporate client you can benefit from the Bupa Arabia online services for clients. You can choose from a wide range of services namely; ability to manage and submit maintenance request, view your employee details and many other services."></asp:Label>
        </div>
    </div>
    <div runat="server" id="provider" visible="false">
        <h2 class="dahsboardTitle">Message From Bupa</h2>
        <div style="text-align: justify">
            Our partner providers are also welcome to benefit from Bupa online services. As a chosen healthcare provider, 
            and in our commitment of delivering improved services, we are pleased to provide you with tailor made services 
            for your need. You can use our online services below.
        </div>
    </div>
</div>
