﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class UserControls_DashBoardTOB : System.Web.UI.UserControl
{
    string strProviderID;
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
        {
            Response.Redirect("../default.aspx");
        }
        else
        {
            strProviderID = Session["ProviderID"].ToString();
            ReqConDltList(strProviderID);
        }
        
    }
    private void ReqConDltList(string ProviderID)
    {
        ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();


        OS_DXC_WAP.CaesarWS.RequestPlanContractInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestPlanContractInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.RequestPlanContractInfoResponse_DN response;

        request.TransactionID = TransactionManager.TransactionID();//  request.TransactionID = long.Parse(DateTime.Now.ToString("dd") + DateTime.Now.ToString("MM").Substring(1, 1) + DateTime.Now.ToString("%y") + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + DateTime.Now.ToString("ss"));
        request.ProviderCode = ProviderID;

        request.Username = WebPublication.CaesarSvcUsername;
        request.Password = WebPublication.CaesarSvcPassword;

        try
        {
            response = null;
            response = ws.ReqConDtl(request);

            StringBuilder sb1 = new StringBuilder(200);

            if (response.Status == "0")
            {


                DisplayContractListInfo(response);

            }
            else
            {
                sb1.Append(response.ErrorMessage).Append("<br/> ");

                Message1.Text = sb1.ToString(); // "Invalid information reported. Please type in the exact information.";

                Message1.Text += "."; // Branch changed successfully. Your reference number is " + response.ReferenceNo.ToString() + ".";

            }
        }
        catch (Exception ex)
        {
            Message1.Text = ex.Message;
        }

    }


    private void DisplayContractListInfo(OS_DXC_WAP.CaesarWS.RequestPlanContractInfoResponse_DN response)
    {
        ListItem itm;
        StringBuilder msge;


        string str1;
        string str2;
        str2 = "";
        str1 = "";
        StringBuilder sbResponse = new StringBuilder(2200);

        if (response.Status == "0")
        {

            sbResponse.Append("<table border=1px cellspacing='0'  bordercolor='silver'  style='border-collapse:collapse;font-size:12px; width:100%  ;'><thead bgcolor='#ffffff'><tr><th>Policy Number</th><th>Customer Name</th><th>Effective Date</th><th>Details</th></tr>	</thead><tbody> ");


            int totalCount = response.plan_ID.Length;
            int i;
            for (i = 0; i < 5; i++)
            {
                try
                {

                    ////sbResponse.Append("<tr ><td align='center'>" + response.contract_No[i] + "</td><td align='center'>" + response.customer_Name[i] + "</td><td align='center'>" + response.plan_Eff_Date[i].ToShortDateString() + "</td><td align='center'><a target='_blank' href='Statement.aspx?CustomerID=" + response.contract_No[i] + "&CYM=" + response.contractYearAndMonth[i] + "&SchemeID=" + response.plan_ID[i] + "'  style='cursor:pointer;text-decoration:underline;color:Blue;'>Details</a></td></td></tr>");
                    var queryString = "CustomerID=" + response.contract_No[i] + "&CYM=" + response.contractYearAndMonth[i] + "&SchemeID=" + response.plan_ID[i];
                    queryString = Cryption.Encrypt(queryString);
                    sbResponse.Append("<tr ><td align='center'>" + response.contract_No[i] + "</td><td align='center'>" + response.customer_Name[i] +
                        "</td><td align='center'>" + response.plan_Eff_Date[i].ToShortDateString() +
                        "</td><td align='center'><a target='_blank' href='Statement.aspx?val=" + queryString + "'  style='cursor:pointer;text-decoration:underline;color:Blue;'>Details</a></td></td></tr>");
                }
                catch { }



            }



            sbResponse.Append("</table>");
            CoverageListReport.Visible = true;
            CoverageListReport.Text = sbResponse.ToString();


        }
        else
        {
        }



    }
}