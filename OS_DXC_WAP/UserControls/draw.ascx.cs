﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility.Configuration;
using Utility.Data;
using System.Data;

public partial class UserControls_draw : System.Web.UI.UserControl
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    protected void Page_Load(object sender, EventArgs e)
    {
        string _didDraw = "0";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
        {
            lblMemberShip.Text = Convert.ToString(Session["MembershipNo"]);
            _didDraw = CheckDraw(Convert.ToString(Session["MembershipNo"]));
            if (Convert.ToInt32(_didDraw) >= 1)
            {
                lblMsg.Text = "Dear Member, you already entered the draw, the draw results date is 6 April 2013 and it is going to be announced on our FB page and Twitter. ";
                frm.Visible = false;
                t1.Visible = false;
                t2.Visible = false;
            }
            else
            {
                t1.Visible = true;
                t2.Visible = true;
            }
        }
        else
        {
            Response.Redirect("../default.aspx");
        }
       
    }
    protected void btnDraw_Click(object sender, EventArgs e)
    {
        if (RegisterUser(lblMemberShip.Text.Trim(), txtName.Text.Trim(), txtMobile.Text.Trim(), txtEmail.Text.Trim()) == "F")
        {
            lblMsg.Text = "Dear Member, you already entered the draw, the draw results date is 6 April 2013 and it is going to be announced on our FB page and Twitter. ";
            t1.Visible = false;
            t2.Visible = false;
            frm.Visible = false;
        }
        else
        {
            lblMsg.Text = "Thank you for your participation, the draw results date is 6 April 2013 and it is going to be announced on our FB page and Twitter";
            frm.Visible = false;
            t1.Visible = false;
            t2.Visible = false;
        }
    }

    public string RegisterUser(string _memid,string _fname, string _mobile, string _email)
    {
        string _result;

        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            DataParameter membership = new DataParameter(SqlDbType.NVarChar, "@MembershipNo", _memid, ParameterDirection.Input);
            DataParameter fullName = new DataParameter(SqlDbType.NVarChar, "@FullName1", _fname, ParameterDirection.Input);
            DataParameter email = new DataParameter(SqlDbType.NVarChar, "@Email", _email, ParameterDirection.Input);
            DataParameter mobile = new DataParameter(SqlDbType.NVarChar, "@Mobile", _mobile, ParameterDirection.Input);

            DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", 30, ParameterDirection.Output);

            DataParameter[] dpBasket = new DataParameter[] { membership, fullName, email, mobile, result };

            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.spDrawUser", ref dpBasket, 60000);
            _result = dpBasket[4].ParamValue.ToString();
        }
        catch (Exception ex)
        {

            throw;
        }


        return _result;
    }

    public string CheckDraw(string _memid)
    {
        string _result;

        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            DataParameter membership = new DataParameter(SqlDbType.NVarChar, "@MembershipNo", _memid, ParameterDirection.Input);
            
            DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", 30, ParameterDirection.Output);

            DataParameter[] dpBasket = new DataParameter[] { membership, result };

            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.spCheckDraw", ref dpBasket, 60000);
            _result = dpBasket[1].ParamValue.ToString();
        }
        catch (Exception ex)
        {

            throw;
        }


        return _result;
    }
    
}