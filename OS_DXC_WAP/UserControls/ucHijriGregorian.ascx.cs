﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Globalization;

namespace HijriGregorianControlJS
{
    public partial class ucHijriGregorian : System.Web.UI.UserControl
    {
        private Boolean blnRequired;
        private Boolean blnEnabled;
        //the date format for set and get
        private string strFormat = "dd/MM/yyyy";
        private CultureInfo arabicCulture = new CultureInfo("ar-SA");
        private CultureInfo englishCulture = new CultureInfo("en-US");

        /// <summary>
        /// clear method to reset the control
        /// </summary>
        public void Clear()
        {
            txtGregDay.Text = "";
            txtGregMonth.Text = "";
            txtGregYear.Text = "";
            txtHijriDay.Text = "";
            txtHijriMonth.Text = "";
            txtHijriYear.Text = "";
            hidGregDay.Value = "";
            hidGregMonth.Value = "";
            hidGregYear.Value = "";
            hidHijriDay.Value = "";
            hidHijriMonth.Value = "";
            hidHijriYear.Value = "";
            rdnGreg.Checked = false;
            rdnHijri.Checked = false;

        }

        /// <summary>
        /// Enable/Disable the user control
        /// </summary>
        [Category("ControlEnabled"), DefaultValue(typeof(bool), "false")]
        public bool Enabled
        {
            set
            {
                blnEnabled = value;
                if (blnEnabled == false)
                {
                    txtGregDay.Enabled = false;
                    txtGregMonth.Enabled = false;
                    txtGregYear.Enabled = false;
                    txtHijriDay.Enabled = false;
                    txtHijriMonth.Enabled = false;
                    txtHijriYear.Enabled = false;
                    rfvRequired.Enabled = false;
                    rdnGreg.Checked = false;
                    rdnHijri.Checked = false;
                    rdnGreg.Enabled = false;
                    rdnHijri.Enabled = false;
                }
                else
                {
                    rdnGreg.Enabled = true;
                    rdnHijri.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Required Field validator Properties
        /// </summary>
        public bool RequiredField
        {
            set { blnRequired = value; }
        }
        public string RequiredFieldText
        {
            set { rfvRequired.Text = value; }
        }
        public string ErrorMsgForRequiredField
        {
            set { rfvRequired.ErrorMessage = value; }
        }
        public string ValidationGroup
        {
            set { rfvRequired.ValidationGroup = value; }
        }

        /// <summary>
        /// To get and set the gregorian date
        /// </summary>
        public string GregorianDate
        {
            set
            {
                string strTempDate = string.Empty;
                txtGregDay.Text = value.Substring(0, 2);
                txtGregMonth.Text = value.Substring(3, 2);
                txtGregYear.Text = value.Substring(6, 4);
                hidGregDay.Value = value.Substring(0, 2);
                hidGregMonth.Value = value.Substring(3, 2);
                hidGregYear.Value = value.Substring(6, 4);
                strTempDate = ConvertGregorianToHijri(value.Replace("-", "/"));
                txtHijriDay.Text = strTempDate.Substring(0, 2);
                txtHijriMonth.Text = strTempDate.Substring(3, 2);
                txtHijriYear.Text = strTempDate.Substring(6, 4);
                hidHijriDay.Value = strTempDate.Substring(0, 2);
                hidHijriMonth.Value = strTempDate.Substring(3, 2);
                hidHijriYear.Value = strTempDate.Substring(6, 4);
            }
            get
            {
                return txtGregDay.Text.PadLeft(2, '0') + "/" + txtGregMonth.Text.PadLeft(2, '0') + "/" + txtGregYear.Text.ToString().PadLeft(4, '0');
            }
        }

        /// <summary>
        /// To Set and Get the Hijri Date
        /// </summary>
        public string HijriDate
        {
            set
            {
                string strTempDate = string.Empty;
                txtHijriDay.Text = value.Substring(0, 2);
                txtHijriMonth.Text = value.Substring(3, 2);
                txtHijriYear.Text = value.Substring(6, 4);
                hidHijriDay.Value = value.Substring(0, 2);
                hidHijriMonth.Value = value.Substring(3, 2);
                hidHijriYear.Value = value.Substring(6, 4);
                strTempDate = ConvertHijriToGregorian(value.Replace("-", "/"));
                txtGregDay.Text = strTempDate.Substring(0, 2);
                txtGregMonth.Text = strTempDate.Substring(3, 2);
                txtGregYear.Text = strTempDate.Substring(6, 4);
                hidGregDay.Value = strTempDate.Substring(0, 2);
                hidGregMonth.Value = strTempDate.Substring(3, 2);
                hidGregYear.Value = strTempDate.Substring(6, 4);
            }
            get
            {
                return txtHijriDay.Text.PadLeft(2, '0') + "/" + txtHijriMonth.Text.PadLeft(2, '0') + "/" + txtHijriYear.Text.PadLeft(4, '0');
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Register_JavaScript_functions();

            //If Required = true, it is Required Field then I check the content of all Textboxes and I update control to validate accordingly
            if (blnRequired == true)
            {
                rfvRequired.Enabled = true;
                if (txtGregDay.Text.Trim() == "")
                    rfvRequired.ControlToValidate = "txtGregDay";
                else if (txtGregMonth.Text.Trim() == "")
                    rfvRequired.ControlToValidate = "txtGregMonth";
                else if (txtGregYear.Text.Trim() == "")
                    rfvRequired.ControlToValidate = "txtGregYear";
                else if (txtHijriDay.Text.Trim() == "")
                    rfvRequired.ControlToValidate = "txtHijriDay";
                else if (txtHijriMonth.Text.Trim() == "")
                    rfvRequired.ControlToValidate = "txtHijriMonth";
                else if (txtHijriYear.Text.Trim() == "")
                    rfvRequired.ControlToValidate = "txtHijriYear";
            }

            if (Page.IsPostBack)
            {
                //to keep the javascript state between postback for radio buttons
                if (rdnGreg.Checked)
                {
                    txtGregDay.Enabled = true;
                    txtGregMonth.Enabled = true;
                    txtGregYear.Enabled = true;
                }
                else if (rdnHijri.Checked)
                {
                    txtHijriDay.Enabled = true;
                    txtHijriMonth.Enabled = true;
                    txtHijriYear.Enabled = true;
                }

                //to keep the javascript state between postback for text boxes
                if (hidGregDay.Value != "")
                    txtGregDay.Text = hidGregDay.Value;
                if (hidGregMonth.Value != "")
                    txtGregMonth.Text = hidGregMonth.Value;
                if (hidGregYear.Value != "")
                    txtGregYear.Text = hidGregYear.Value;
                if (hidHijriDay.Value != "")
                    txtHijriDay.Text = hidHijriDay.Value;
                if (hidHijriMonth.Value != "")
                    txtHijriMonth.Text = hidHijriMonth.Value;
                if (hidHijriYear.Value != "")
                    txtHijriYear.Text = hidHijriYear.Value;
            }
        }

        /// <summary>
        /// To convert Hijri date to Gregorian in case of the developer set the Hijri date
        /// </summary>
        /// <param name="strHijri">The Hijri date to be converted</param>
        /// <returns>Gregorian date in format dd/MM/yyyy</returns>
        private string ConvertHijriToGregorian(string strHijri)
        {
            try
            {
                DateTime tempDate = DateTime.ParseExact(strHijri, strFormat, arabicCulture.DateTimeFormat);
                return tempDate.ToString(strFormat, englishCulture.DateTimeFormat);
            }
            catch (Exception ex)
            {
                if (strHijri.Substring(0, 2) == "30")
                {
                    strHijri=strHijri.Replace("30/", "29/");
                    DateTime tempDate = DateTime.ParseExact(strHijri, strFormat, arabicCulture.DateTimeFormat);
                    return tempDate.ToString(strFormat, englishCulture.DateTimeFormat);
                }
                else
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// To convert Gregorian date to Hijri in case of the developer set the Gregorian date
        /// </summary>
        /// <param name="strGregorian">The Gregorian date to be converted</param>
        /// <returns>Hijri date in format dd/MM/yyyy</returns>
        private string ConvertGregorianToHijri(string strGregorian)
        {
            DateTime tempDate = DateTime.ParseExact(strGregorian, strFormat, englishCulture.DateTimeFormat);
            return tempDate.ToString(strFormat, arabicCulture.DateTimeFormat);
        }

        /// <summary>
        /// To register the javascript functions, I put the javascript here to avoid any issue incase of the parent page contain 
        /// update panel
        /// </summary>
        private void Register_JavaScript_functions()
        {
            //To Solve the JavaScript Issue in WebUserControl not working when used multiple times on same page 
            //For More Info Please check http://www.aspsnippets.com/Articles/Issue-JavaScript-in-WebUserControl-not-working-when-used-multiple-times-on-same-page.aspx
            string uniqueKey = Guid.NewGuid().ToString("N");

            //I am passing a unique key to each java script method name so that each user control renders its JavaScript methods and variables with a unique name.
            this.rdnHijri.Attributes["onclick"] = "return OnChangeRadio_" + uniqueKey + "();";
            this.rdnGreg.Attributes["onclick"] = "return OnChangeRadio_" + uniqueKey + "();";

            this.txtHijriDay.Attributes["onkeyup"] = "return calculateDate_" + uniqueKey + "('greg');";
            this.txtHijriMonth.Attributes["onkeyup"] = "return calculateDate_" + uniqueKey + "('greg');";
            this.txtHijriYear.Attributes["onkeyup"] = "return calculateDate_" + uniqueKey + "('greg');";

            this.txtGregDay.Attributes["onkeyup"] = "return calculateDate_" + uniqueKey + "('hijri');";
            this.txtGregMonth.Attributes["onkeyup"] = "return calculateDate_" + uniqueKey + "('hijri');";
            this.txtGregYear.Attributes["onkeyup"] = "return calculateDate_" + uniqueKey + "('hijri');";

            string strScript = "" +
             "   function OnChangeRadio_" + uniqueKey + "() " + System.Environment.NewLine +
             "   { " + System.Environment.NewLine +
                "document.getElementById('" + this.txtHijriDay.ClientID + "').disabled = true; " + System.Environment.NewLine +
                "document.getElementById('" + this.txtHijriMonth.ClientID + "').disabled = true; " + System.Environment.NewLine +
                "document.getElementById('" + this.txtHijriYear.ClientID + "').disabled = true; " + System.Environment.NewLine +
                "document.getElementById('" + this.txtGregDay.ClientID + "').disabled = true; " + System.Environment.NewLine +
                "document.getElementById('" + this.txtGregMonth.ClientID + "').disabled = true; " + System.Environment.NewLine +
                "document.getElementById('" + this.txtGregYear.ClientID + "').disabled = true; " + System.Environment.NewLine +
                "if (document.getElementById('" + this.rdnHijri.ClientID + "').checked == true) { " + System.Environment.NewLine +
                "    document.getElementById('" + this.txtHijriDay.ClientID + "').disabled = false; " + System.Environment.NewLine +
                "    document.getElementById('" + this.txtHijriMonth.ClientID + "').disabled = false; " + System.Environment.NewLine +
                "    document.getElementById('" + this.txtHijriYear.ClientID + "').disabled = false; " + System.Environment.NewLine +
                "    document.getElementById('" + this.txtHijriDay.ClientID + "').focus(); " + System.Environment.NewLine +
                "} " + System.Environment.NewLine +
                "else { " + System.Environment.NewLine +
                "    document.getElementById('" + this.txtGregDay.ClientID + "').disabled = false; " + System.Environment.NewLine +
                "    document.getElementById('" + this.txtGregMonth.ClientID + "').disabled = false; " + System.Environment.NewLine +
                "    document.getElementById('" + this.txtGregYear.ClientID + "').disabled = false; " + System.Environment.NewLine +
                "    document.getElementById('" + this.txtGregDay.ClientID + "').focus(); " + System.Environment.NewLine +
                "} " + System.Environment.NewLine +
            "}" + System.Environment.NewLine +
            "" + System.Environment.NewLine +
            "function calculateDate_" + uniqueKey + "(type) { " + System.Environment.NewLine +
            "   var hDay = document.getElementById('" + this.txtHijriDay.ClientID + "').value; " + System.Environment.NewLine +
            "   var hMonth = document.getElementById('" + this.txtHijriMonth.ClientID + "').value; " + System.Environment.NewLine +
            "   var hYear = document.getElementById('" + this.txtHijriYear.ClientID + "').value; " + System.Environment.NewLine +
            "" + System.Environment.NewLine +
            "   document.getElementById('" + this.hidHijriDay.ClientID + "').value = hDay;" + System.Environment.NewLine +
            "   document.getElementById('" + this.hidHijriMonth.ClientID + "').value = hMonth;" + System.Environment.NewLine +
            "   document.getElementById('" + this.hidHijriYear.ClientID + "').value = hYear;" + System.Environment.NewLine +
            "" + System.Environment.NewLine +
            "   var gDay = document.getElementById('" + this.txtGregDay.ClientID + "').value; " + System.Environment.NewLine +
            "   var gMonth = document.getElementById('" + this.txtGregMonth.ClientID + "').value; " + System.Environment.NewLine +
            "   var gYear = document.getElementById('" + this.txtGregYear.ClientID + "').value; " + System.Environment.NewLine +
            "" + System.Environment.NewLine +
            "   document.getElementById('" + this.hidGregDay.ClientID + "').value = gDay;" + System.Environment.NewLine +
            "   document.getElementById('" + this.hidGregMonth.ClientID + "').value = gMonth;" + System.Environment.NewLine +
            "    document.getElementById('" + this.hidGregYear.ClientID + "').value = gYear;" + System.Environment.NewLine +
            "" + System.Environment.NewLine +
            "   if (type = 'greg' && (hDay != '' && hMonth != '' && hYear != '')) { " + System.Environment.NewLine +
            "       if (document.getElementById('" + this.rdnHijri.ClientID + "').checked == true) { " + System.Environment.NewLine +
            "           convertToGreg_" + uniqueKey + "(hDay, hMonth, hYear); " + System.Environment.NewLine +
            "       } " + System.Environment.NewLine +
            "   } " + System.Environment.NewLine +
            "" + System.Environment.NewLine +
            "   if (type = 'hijri' && (gDay != '' && gMonth != '' && gYear != '')) { " + System.Environment.NewLine +
            "       if (document.getElementById('" + this.rdnGreg.ClientID + "').checked == true) { " + System.Environment.NewLine +
            "           convertToHijri_" + uniqueKey + "(gDay, gMonth, gYear); " + System.Environment.NewLine +
            "       } " + System.Environment.NewLine +
            "   } " + System.Environment.NewLine +
            "} " + System.Environment.NewLine +
            "" + System.Environment.NewLine +
        "function convertToHijri_" + uniqueKey + "(d, m, y) { " + System.Environment.NewLine +
        "    d = parseInt(d) " + System.Environment.NewLine +
        "    m = parseInt(m) " + System.Environment.NewLine +
        "    y = parseInt(y) " + System.Environment.NewLine +
         "" + System.Environment.NewLine +
        "    if ((y > 1582) || ((y == 1582) && (m > 10)) || ((y == 1582) && (m == 10) && (d > 14))) { " + System.Environment.NewLine +
        "        jd = intPart((1461 * (y + 4800 + intPart((m - 14) / 12))) / 4) + intPart((367 * (m - 2 - 12 * (intPart((m - 14) / 12)))) / 12) - " + System.Environment.NewLine +
        "            intPart((3 * (intPart((y + 4900 + intPart((m - 14) / 12)) / 100))) / 4) + d - 32075 " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    else { " + System.Environment.NewLine +
        "        jd = 367 * y - intPart((7 * (y + 5001 + intPart((m - 9) / 7))) / 4) + intPart((275 * m) / 9) + d + 1729777 " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    l = jd - 1948440 + 10632 " + System.Environment.NewLine +
        "    n = intPart((l - 1) / 10631) " + System.Environment.NewLine +
        "    l = l - 10631 * n + 354 " + System.Environment.NewLine +
        "    j = (intPart((10985 - l) / 5316)) * (intPart((50 * l) / 17719)) + (intPart(l / 5670)) * (intPart((43 * l) / 15238)) " + System.Environment.NewLine +
        "    l = l - (intPart((30 - j) / 15)) * (intPart((17719 * j) / 50)) - (intPart(j / 16)) * (intPart((15238 * j) / 43)) + 29 " + System.Environment.NewLine +
        "    m = intPart((24 * l) / 709) " + System.Environment.NewLine +
        "    d = l - intPart((709 * m) / 24) " + System.Environment.NewLine +
        "    y = 30 * n + j - 30 " + System.Environment.NewLine +
        "    document.getElementById('" + this.txtHijriDay.ClientID + "').value = ValidateHijriDayValue(d); " + System.Environment.NewLine +
        "    document.getElementById('" + this.txtHijriMonth.ClientID + "').value = ValidateHijriMonthValue(m); " + System.Environment.NewLine +
        "    document.getElementById('" + this.txtHijriYear.ClientID + "').value = Math.abs(y); " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "    document.getElementById('" + this.hidHijriDay.ClientID + "').value = ValidateHijriDayValue(d); " + System.Environment.NewLine +
        "    document.getElementById('" + this.hidHijriMonth.ClientID + "').value = ValidateHijriMonthValue(m); " + System.Environment.NewLine +
        "    document.getElementById('" + this.hidHijriYear.ClientID + "').value = Math.abs(y); " + System.Environment.NewLine +
        "} " + System.Environment.NewLine +
         "" + System.Environment.NewLine +
        "function convertToGreg_" + uniqueKey + "(d, m, y) { " + System.Environment.NewLine +
        "    d = parseInt(d) " + System.Environment.NewLine +
        "    m = parseInt(m) " + System.Environment.NewLine +
        "    y = parseInt(y) " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "    jd = intPart((11 * y + 3) / 30) + 354 * y + 30 * m - intPart((m - 1) / 2) + d + 1948440 - 385 " + System.Environment.NewLine +
        "    if (jd > 2299160) { " + System.Environment.NewLine +
        "        l = jd + 68569 " + System.Environment.NewLine +
        "        n = intPart((4 * l) / 146097) " + System.Environment.NewLine +
        "        l = l - intPart((146097 * n + 3) / 4) " + System.Environment.NewLine +
        "        i = intPart((4000 * (l + 1)) / 1461001) " + System.Environment.NewLine +
        "        l = l - intPart((1461 * i) / 4) + 31 " + System.Environment.NewLine +
        "        j = intPart((80 * l) / 2447) " + System.Environment.NewLine +
        "        d = l - intPart((2447 * j) / 80) " + System.Environment.NewLine +
        "        l = intPart(j / 11) " + System.Environment.NewLine +
        "        m = j + 2 - 12 * l " + System.Environment.NewLine +
        "        y = 100 * (n - 49) + i + l " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    else { " + System.Environment.NewLine +
        "        j = jd + 1402 " + System.Environment.NewLine +
        "        k = intPart((j - 1) / 1461) " + System.Environment.NewLine +
        "        l = j - 1461 * k " + System.Environment.NewLine +
        "        n = intPart((l - 1) / 365) - intPart(l / 1461) " + System.Environment.NewLine +
        "        i = l - 365 * n + 30 " + System.Environment.NewLine +
        "        j = intPart((80 * i) / 2447) " + System.Environment.NewLine +
        "        d = i - intPart((2447 * j) / 80) " + System.Environment.NewLine +
        "        i = intPart(j / 11) " + System.Environment.NewLine +
        "        m = j + 2 - 12 * i " + System.Environment.NewLine +
        "        y = 4 * k + n + i - 4716 " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    document.getElementById('" + this.txtGregDay.ClientID + "').value = d; " + System.Environment.NewLine +
        "    document.getElementById('" + this.txtGregMonth.ClientID + "').value = m; " + System.Environment.NewLine +
        "    document.getElementById('" + this.txtGregYear.ClientID + "').value = y; " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "    document.getElementById('" + this.hidGregDay.ClientID + "').value = d; " + System.Environment.NewLine +
        "    document.getElementById('" + this.hidGregMonth.ClientID + "').value = m; " + System.Environment.NewLine +
        "    document.getElementById('" + this.hidGregYear.ClientID + "').value = y; " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "} " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "function intPart(floatNum) { " + System.Environment.NewLine +
        "    if (floatNum < -0.0000001) { " + System.Environment.NewLine +
        "        return Math.ceil(floatNum - 0.0000001) " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    return Math.floor(floatNum + 0.0000001) " + System.Environment.NewLine +
        "} " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "function ValidateHijriDayValue(intValue) { " + System.Environment.NewLine +
        "    intValue = Math.abs(intValue); " + System.Environment.NewLine +
        "    intMaxValue = 30; " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "    if (intValue < 1 || intValue > intMaxValue) { " + System.Environment.NewLine +
        "        return 'NaN'; " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    else { " + System.Environment.NewLine +
        "        return intValue; " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "} " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "function ValidateHijriMonthValue(intValue) { " + System.Environment.NewLine +
        "    intValue = Math.abs(intValue); " + System.Environment.NewLine +
        "    intMaxValue = 12; " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "    if (intValue < 1 || intValue > intMaxValue) { " + System.Environment.NewLine +
        "        return 'NaN'; " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    else { " + System.Environment.NewLine +
        "        return intValue; " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "} " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "function ValidateHijriYearValue(intValue) { " + System.Environment.NewLine +
        "    intValue = Math.abs(intValue); " + System.Environment.NewLine +
        "    intMaxValue = 9999; " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "    if (intValue < 1 || intValue > intMaxValue) { " + System.Environment.NewLine +
        "        return 'NaN'; " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    else { " + System.Environment.NewLine +
        "        return intValue; " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "} " + System.Environment.NewLine +
        "" + System.Environment.NewLine +
        "function allownumbers(e) { " + System.Environment.NewLine +
        "    var key = window.event ? e.keyCode : e.which; " + System.Environment.NewLine +
        "    var keychar = String.fromCharCode(key); " + System.Environment.NewLine +
        "    var reg = new RegExp('[0-9.]') " + System.Environment.NewLine +
        "    if (key == 8) { " + System.Environment.NewLine +
        "        keychar = String.fromCharCode(key); " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    if (key == 11) { " + System.Environment.NewLine +
        "        key = 8; " + System.Environment.NewLine +
        "        keychar = String.fromCharCode(key); " + System.Environment.NewLine +
        "    } " + System.Environment.NewLine +
        "    return reg.test(keychar); " + System.Environment.NewLine +
        "}             " + System.Environment.NewLine;

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "us_JavaScript" + uniqueKey, strScript, true);
        }
    }
}