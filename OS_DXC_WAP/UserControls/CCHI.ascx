﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_CCHI" Codebehind="CCHI.ascx.cs" %>
 <asp:Panel ID="Panel1" runat="server" DefaultButton="btnCheck">
<table class="tipofTheDayBanner orangeTips" title="Your information has been submitted to CCHI and this is your status:">
    <tr>
        <td class="investorBannerTitle">CCHI</td>
    </tr>
    <tr>
        <td class="margBottom15">Your information has been submitted to CCHI and this is your status:</td>
    </tr>
    <tr>
        <td class="margBottom15"><strong class="fontSize16"><asp:Label ID="lblmemTitle" runat="server" Visible=false Text="Membership No."></asp:Label></strong></td>
    </tr>
    <tr>
        <td class="margBottom15">
            <table>
                <tr>
                    <td style="width:2px"><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ErrorMessage="*" ControlToValidate="txtmembership"></asp:RequiredFieldValidator></td>
                    <td><asp:TextBox ID="txtmembership" runat="server" Width="70px"></asp:TextBox></td>
                    <td><asp:Button ID="btnCheck" runat="server" Text="Check" 
            onclick="btnCheck_Click" /></td> 
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="margBottom15">
            <table>
                <tr>
                    <td><asp:Label ID="Statuslbl" runat="server" Visible=false Text="Status:"></asp:Label></td>
                    <td><asp:Label ID="lblcStatus" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
        <asp:Label ID="lblReasonTitle" runat="server" Visible=false Text="Reason:"></asp:Label> 
                    </td>
                    <td> 
        <asp:Label ID="lblReason" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
        <asp:Label ID="Message1" runat="server" Text=""></asp:Label></td>
                </tr>
            </table> </td>
    </tr>
</table>
</asp:Panel>