﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_draw" Codebehind="draw.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<style type="text/css">
    .style1
    {
        width: 92px;
    }
    </style>
<div id="Member" class="tipofTheDayBanner orangeTips" >
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="divForm" 
     >
    <tr>
        <td colspan="3">
            <h2 class="dahsboardTitleWhite" style="color:White;">
                Register now and win an iPad or iPhone:</h2>
        </td>
    </tr>
    <div id="frm" runat="server">
    <tr>
        <td align="left" colspan="4" class="formLabels"">Membership No
            <dx:ASPxLabel ID="lblMemberShip" runat="server" Text="" 
                CssClass="formLabels">
            </dx:ASPxLabel>
        </td>
    </tr>
    <tr>
        <td align="left" colspan="4" class="formLabels"">&nbsp;</td>
    </tr>
    <tr>
        <td class="formLabels" style="width:130px">Full Name</td>
        <td class="formLabels" style="width:130px">Mobile</td>
        <td class="formLabels" style="width:130px">Email</td>
        <td class="formLabels" style="width:130px"></td>
    </tr>
    <tr>
        <td>
            <dx:ASPxTextBox ID="txtName" runat="server" Width="170px" 
                Theme="Office2010Blue">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"></ValidationSettings>
            </dx:ASPxTextBox>
        </td>
        <td>
            <dx:ASPxTextBox ID="txtMobile" runat="server" Width="170px" Theme="Office2010Blue">
            
                       <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true">
                        <RegularExpression ValidationExpression="^[0-9,\.]+$"/>

                       </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
        <td class="style1">
            <dx:ASPxTextBox ID="txtEmail" runat="server" Width="170px" Theme="Office2010Blue">
            <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                       <RegularExpression ErrorText="Invalid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    <RequiredField IsRequired="true" ErrorText="Field is required" />
                                   </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
        <td>
           
            
            <dx:ASPxButton ID="btnDraw" runat="server" onclick="btnDraw_Click" 
                Text="Enter the Draw" Theme="Office2010Blue" Width="130"></dx:ASPxButton>
        </td>
    </tr>
    <tr>
        <td colspan="4" align="right">
           
            
            &nbsp;</td>
    </tr>
    </div>
    <tr>
        <td colspan="4" align="right" style="font-size:19px;" runat="server" id="t1" visible="false">
           بمشاركتك في هذه المسابقة أنت توافق على هذه الأحاكم والشروط.<a href="DrawTerms.htm" target="_blank">  اضغط هنا لقراءة الأحكام والشروط</a>
           
</td>
    </tr>
    <tr>
        <td colspan="4" align="left" style="font-size:15px;"  runat="server" id="t2" visible="false"> 
            By participating in this draw contest you agree on the following terms and Conditions. 
                <a href="DrawTerms.htm"  target="_blank">Click here to read the terms and conditions</a></td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <dx:ASPxLabel ID="lblMsg" runat="server" Text="" 
                Theme="Office2010Blue">
            </dx:ASPxLabel>
        </td>
    </tr>
</table>
</div>