﻿using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Web.UI.WebControls;
using Utility.Configuration;
using Utility.Data;
using WebApp;
using System.Linq;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using System.Web;
using System.Text;
using System.IO;
using System.ComponentModel;

public partial class UserControls_SignUpNew : System.Web.UI.UserControl
{
    private OS_DXC_WAP.CaesarWS.ServiceDepot_DNService ws;
    DataSet ds = null;
    private static readonly string connectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
    string strUserRole = "", strModule = "ProviderRegistration";
    string strUserId = "";
    int Stage = 1;
    int RequestId = 1;
    protected void Page_Load(object sender, EventArgs e)
    {

        lblMessage.Text = "";
        // UserTyperbList.Items[0].Attributes.CssStyle.Add("margin-right", "5px");
        //UserTyperbList.Items[1].Attributes.CssStyle.Add("margin-left", "15px");
        Session["NewRequest"] = WebPublication.GenerateTransactionID();
        if (!IsPostBack)
        {
            ClearFields();
            rgAdditionalContacts.Rebind();
            rgPharmacy.Rebind();
            txtCCHILicenseExpirationDates.MinDate = DateTime.Now;
            txtMOHLicenseED.MinDate = DateTime.Now;
            txtCommRegistrationNoED.MinDate = DateTime.Now;
            UserTyperbList_SelectedIndexChanged(sender, e);
            txtProviderName.Focus();
            //uploaderCtl.Visible = false;
            //string strContent = "Dear Provider ,<br>";
            //strContent = strContent + "Your request for online registration has been accounted and will be processed soon. <br>";
            //strContent = strContent + "One of Our Provider Relations officer will get in touch with you soon. <br>";
            //strContent = strContent + "<table width='100%'>";
            //strContent = strContent + "<tr><td colspan='2' align='left'><b>Request Information:</b></td></tr>";
            //strContent = strContent + "<tr><td style='width: 131px'><b>Request ID</b></td><td>" + 12 + "</td></tr>";
            //strContent = strContent + "<tr><td style='width: 131px'><b>Provider Name</b></td><td>" + txtProviderName.Text.Trim() + "</td></tr>";
            //strContent = strContent + "<tr><td style='width: 131px'><b>Requester Name</b></td><td>" + txtContactPerson.Text.Trim() + "</td></tr>";
            //strContent = strContent + "<tr><td style='width: 131px'><b>Request Date</b></td><td>" + System.DateTime.Now.ToShortDateString() + "</td></tr>";
            //strContent = strContent + "<tr><td colspan='2' align='left'><b>Your Login Credentials are:</b></td></tr>";
            //strContent = strContent + "<tr><td style='width: 131px'><b>User ID</b></td><td>" + txtEmailAddress.Text.Trim() + "</td></tr>";
            //strContent = strContent + "<tr><td style='width: 131px'><b>Pasword</b></td><td>" + "Bupa123" + "</td></tr>";
            //strContent = strContent + "Please " + "<a href=\"" + "http://onlineservices.bupa.com.sa" + "\">click Here</a>" + " for Login to Online Services. <br>";
            //strContent = strContent + "<tr><td colspan='2'>This is a computer generated email. Please do not reply to this email.</td></tr>";
            //strContent = strContent + "<tr><td colspan='2'>Yours sincerely,</td></tr>";
            //strContent = strContent + "<tr><td colspan='2'>Bupa Online Service</td></tr>";
            //strContent = strContent + "</table>";
            //SendEmail("webmaster@bupame.com", "lokesh.narayanappa@bupa.com.sa", "Request for provider user account", strContent, null);

        }
        else
        {
            //Page.DataBind();
        }
    }

    public static int GetAge(DateTime reference, DateTime birthday)
    {
        int age = reference.Year - birthday.Year;
        if (reference < birthday.AddYears(age)) age--;

        return age;
    }

    protected void CleartControls()
    {
        txtDependent.Text = "";
        //txtRequesterName.Text = "";
        //txtProviderName.Text = "";
        //txtUserName.Text = "";
        txtPassword.Text = "";
        //txtEmployeeName.Text = "";
        //txtCurrentPosition.Text = "";
        //txtProviderEmail.Text = "";
        //txtProviderFax.Text = "";
        //txtProviderTel.Text = "";
        //txtExt.Text = "";
        //txtProviderMobile.Text = "";
        txtCustomerName.Text = "";
        txtGroupEmail.Text = "";
        txtFax.Text = "";
        txtMembership.Text = "";
        txtPassword.Text = "";
        txtMemberEmail.Text = "";
        txtID.Text = "";
        txtMobile.Text = "";
        lblMemberName.Text = "";
        lblContract.Text = "";
        lblCustomerName.Text = "";
        lblMemberName.Text = "";
        lblStatus.Text = "";
        txtMembership.Enabled = true;
        r1.Visible = false;
        r2.Visible = false;
        r3.Visible = false;
        r4.Visible = false;
        r5.Visible = false;
        r6.Visible = false;
    }

    private void SendEmailToMember(string Email, string MemberName)
    {
        MailMessage mailMsg = new MailMessage();
        mailMsg.To.Add(Email);

        mailMsg.From = new MailAddress("donotreply@bupa.com.sa");
        mailMsg.Subject = "Bupa Membership";
        mailMsg.Body = "Dear " + MemberName + ",<br/><br/>" +
                        "Thank you for registering in Bupa online services.<br/><br/>" +
                        "Yours sincerely<br/>" +
                        "Customer Services<br/>" +
                        "Bupa Arabia";
        mailMsg.IsBodyHtml = true;

        SmtpClient smptClient = new System.Net.Mail.SmtpClient();// .SmtpMail.Send("admin@bupame.com", );
        smptClient.Send(mailMsg);
        mailMsg.Dispose();
    }

    protected void UserTyperbList_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";
        CleartControls();
        switch (UserTyperbList.SelectedValue.ToString())
        {
            case "2":
                tblClient.Visible = true;
                tblMember.Visible = false;
                tblProvider.Visible = false;
                //uploaderCtl.Visible = false;
                CleartControls();
                break;
            case "3":
                tblProvider.Visible = true;
                tblMember.Visible = false;
                tblClient.Visible = false;
                //uploaderCtl.Visible = true;
                btnSubmit.Visible = true;
                btnSubmit.Enabled = true;
                Disclamer.Visible = false;
                CleartControls();
                break;
            case "1":
                tblMember.Visible = true;
                tblClient.Visible = false;
                tblProvider.Visible = false;
                //uploaderCtl.Visible = false;
                tblInfo.Visible = false;
                btnSubmit.Enabled = false;
                agreement.Visible = false;
                healthcareProfile.Visible = false;
                Disclamer.Visible = false;
                btnSubmit.Visible = true;
                CleartControls();
                break;
            case "4":
                tblProvider.Visible = true;
                tblMember.Visible = false;
                tblClient.Visible = false;
                //uploaderCtl.Visible = true;
                btnSubmit.Visible = false;
                // btnSubmit.Enabled = true;
                Disclamer.Visible = false;
                CleartControls();
                FillDropDowns();
                break;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string _refNo;
        string strContent = "";
        switch (UserTyperbList.SelectedValue.ToString())
        {
            case "2":
                //Client
                _refNo = NewClient();
                if (string.IsNullOrEmpty(_refNo))
                {
                    lblError.Text = "An error happened while saving the your data, Please try again";
                    lblError.ForeColor = System.Drawing.Color.Red;
                    lblError.Font.Bold = true;
                }
                else
                {
                    lblError.Text = "Your request will be processed, Login details will be sent to your email address.";
                    lblError.ForeColor = System.Drawing.Color.Green;
                    lblError.Font.Bold = true;
                    //uploaderCtl.Visible = false;
                    //btnSubmit.Visible = false;
                    tblClient.Visible = false;
                    tblMember.Visible = false;
                    tblProvider.Visible = false;
                    strContent = "Dear Membership maintenance team ,<br>";
                    strContent = strContent + "A request for client user account has been submitted with the following details: <br>";
                    strContent = strContent + "<table width='100%'>";
                    strContent = strContent + "<tr><td colspan='2' align='left'><b>Request Information:</b></td></tr>";
                    strContent = strContent + "<tr><td style='width: 131px'><b>Request ID</b></td><td>" + _refNo + "</td></tr>";
                    strContent = strContent + "<tr><td style='width: 131px'><b>Customer Name</b></td><td>" + txtCustomerName.Text.Trim() + "</td></tr>";
                    strContent = strContent + "<tr><td style='width: 131px'><b>Group Secretary Name</b></td><td>" + txtGroupSecretaryName.Text.Trim() + "</td></tr>";
                    strContent = strContent + "<tr><td style='width: 131px'><b>Request Date</b></td><td>" + System.DateTime.Now.ToShortDateString() + "</td></tr>";
                    strContent = strContent + "<tr><td colspan='2'>This is a computer generated email. Please do not reply to this email.</td></tr>";
                    strContent = strContent + "<tr><td colspan='2'>Yours sincerely,</td></tr>";
                    strContent = strContent + "<tr><td colspan='2'>Bupa Online Service</td></tr>";
                    strContent = strContent + "</table>";
                    SendEmail("webmaster@bupame.com", "aakif.irshad@bupa.com.sa", "Request for provider user account", strContent, null);
                    btnSubmit.Visible = false;
                }
                break;
            case "3":
                {
                    //to call the new function created by lokesh
                    //Provider
                    //_refNo = NewProvider();
                    //if (string.IsNullOrEmpty(_refNo))
                    //{
                    //    lblError.Text = "UserName already taken, Please try another user name.";
                    //    lblError.ForeColor = System.Drawing.Color.Red;
                    //    lblError.Font.Bold = true;
                    //}
                    //else
                    //{
                    //    lblError.Text = "Your request will be processed, A confirmation will be sent to your email address detailing the status of your request within 24 hours.";
                    //    lblError.ForeColor = System.Drawing.Color.Green;
                    //    lblError.Font.Bold = true;
                    //    //uploaderCtl.Visible = false;
                    //    //btnSubmit.Visible = false;
                    //    tblClient.Visible = false;
                    //    tblMember.Visible = false;
                    //    tblProvider.Visible = false;
                    //    strContent = "Dear Provider Relations,<br>";
                    //    strContent = strContent + "A request for provider user account has been submitted with the following details: <br>";
                    //    strContent = strContent + "<table width='100%'>";
                    //    strContent = strContent + "<tr><td colspan='2' align='left'><b>Request Information:</b></td></tr>";
                    //    strContent = strContent + "<tr><td style='width: 131px'><b>Request ID</b></td><td>" + _refNo + "</td></tr>";
                    //    //strContent = strContent + "<tr><td style='width: 131px'><b>Provider Name</b></td><td>" + txtProviderName.Text.Trim() + "</td></tr>";
                    //    //strContent = strContent + "<tr><td style='width: 131px'><b>Requester Name</b></td><td>" + txtRequesterName.Text.Trim() + "</td></tr>";
                    //    strContent = strContent + "<tr><td style='width: 131px'><b>Request Date</b></td><td>" + System.DateTime.Now.ToShortDateString() + "</td></tr>";
                    //    strContent = strContent + "<tr><td colspan='2'>This is a computer generated email. Please do not reply to this email.</td></tr>";
                    //    strContent = strContent + "<tr><td colspan='2'>Yours sincerely,</td></tr>";
                    //    strContent = strContent + "<tr><td colspan='2'>Bupa Online Service</td></tr>";
                    //    strContent = strContent + "</table>";
                    //    SendEmail("webmaster@bupame.com", "prov_webrequest@bupa.com.sa", "Request for provider user account", strContent, null);
                    //    btnSubmit.Visible = false;
                    //}
                    break;
                }
            default:
                NewMember();
                break;
        }
    }

    protected string NewProvider()
    {
        string _result = "";
        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            //if (FileUpload1.PostedFile != null)
            //{
            //    string destinationPath = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_PROVIDERREGIDTRATIONDOC_PATH);//Key_PREAUTHDESTINATION_PATH
            //    string sourcePath = string.Format("{0}{1}", MapPath("~/RegistrationFiles/"), Session["NewRequest"] + "_" + FileUpload1.FileName);
            //    string fileName = sourcePath;
            //    //string fileName = string.Format("{0}{1}", destinationPath, Session["NewRequest"] + "_" + FileUpload1.FileName);
            //    FileUpload1.PostedFile.SaveAs(fileName);

            //    MoveFile(sourcePath, destinationPath + @"\" + Session["NewRequest"] + "_" + FileUpload1.FileName);

            //    //[Akif] I tried below option but I was unable to resolve UNC path problem. therefore using next line
            //    //Session["DocPath"] = destinationPath + Session["NewRequest"] + "_" + FileUpload1.FileName; //"~/RegistrationFiles/" + Session["NewRequest"] + "_" + FileUpload1.FileName;
            //    Session["DocPath"] = "~/RegistrationFiles/" + Session["NewRequest"] + "_" + FileUpload1.FileName;
            //}


            // DataParameter proID = new DataParameter(SqlDbType.NVarChar, "@proID", txtProviderName.Value, ParameterDirection.Input);
            //DataParameter requester = new DataParameter(SqlDbType.NVarChar, "@requester ", txtRequesterName.Text.Trim(), ParameterDirection.Input);
            //DataParameter ProName = new DataParameter(SqlDbType.NVarChar, "@ProName ", txtProviderName.Text.Trim(), ParameterDirection.Input);
            // DataParameter proUsername = new DataParameter(SqlDbType.NVarChar, "@proUsername", txtUserName.Text.Trim(), ParameterDirection.Input);
            // DataParameter proPassword = new DataParameter(SqlDbType.NVarChar, "@proPassword", txtPassword.Text.Trim(), ParameterDirection.Input);
            // DataParameter NameofEmployee = new DataParameter(SqlDbType.NVarChar, "@NameofEmployee", txtEmployeeName.Text.Trim(), ParameterDirection.Input);
            // DataParameter Position = new DataParameter(SqlDbType.NVarChar, "@Position", txtCurrentPosition.Text.Trim(), ParameterDirection.Input);
            // DataParameter proEmail = new DataParameter(SqlDbType.NVarChar, "@proEmail", txtProviderEmail.Text.Trim(), ParameterDirection.Input);
            // DataParameter proFaxNo = new DataParameter(SqlDbType.NVarChar, "@proFaxNo", txtProviderFax.Text.Trim(), ParameterDirection.Input);
            DataParameter proActive = new DataParameter(SqlDbType.Bit, "@proActive", 0, ParameterDirection.Input);
            DataParameter proNotes = new DataParameter(SqlDbType.NVarChar, "@proNotes", "", ParameterDirection.Input);
            //DataParameter Tel = new DataParameter(SqlDbType.NVarChar, "@Tel", txtProviderTel.Text.Trim(), ParameterDirection.Input);
            // DataParameter Ext = new DataParameter(SqlDbType.NVarChar, "@Ext", txtExt.Text.Trim(), ParameterDirection.Input);
            //DataParameter Mobile = new DataParameter(SqlDbType.NVarChar, "@Mobile", txtProviderMobile.Text.Trim(), ParameterDirection.Input);
            DataParameter Salt = new DataParameter(SqlDbType.NVarChar, "@Salt", "", ParameterDirection.Input);
            DataParameter LastLogon = new DataParameter(SqlDbType.DateTime, "@LastLogon", DateTime.Today, ParameterDirection.Input);
            DataParameter DocPath = new DataParameter(SqlDbType.NVarChar, "@DocPath", Convert.ToString(Session["DocPath"]), ParameterDirection.Input);
            DataParameter LastUpdateTimestamp = new DataParameter(SqlDbType.DateTime, "@LastUpdateTimestamp", DateTime.Today, ParameterDirection.Input);

            DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", "", ParameterDirection.Output, 30);

            //DataParameter[] dpBasket = new DataParameter[] { proID, requester, ProName, proUsername, proPassword, NameofEmployee, Position, proEmail, proFaxNo, proActive, proNotes
            //                    ,Tel ,Ext ,Mobile ,Salt, LastLogon,LastUpdateTimestamp,DocPath,result};

            //DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.spRegisterProvider", ref dpBasket, 60000);
            //_result = dpBasket[18].ParamValue.ToString();
        }
        catch (Exception ex)
        {
            throw;
        }

        return _result;
    }
    protected string NewClient()
    {
        string _result;
        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            DataParameter clientID = new DataParameter(SqlDbType.NVarChar, "@clientID", "", ParameterDirection.Input);
            DataParameter cliUsername = new DataParameter(SqlDbType.NVarChar, "@cliUsername ", "", ParameterDirection.Input);
            DataParameter cliPassword = new DataParameter(SqlDbType.NVarChar, "@cliPassword ", "", ParameterDirection.Input);
            DataParameter cliName = new DataParameter(SqlDbType.NVarChar, "@cliName ", txtCustomerName.Text.Trim(), ParameterDirection.Input);
            DataParameter cliEmail = new DataParameter(SqlDbType.NVarChar, "@cliEmail ", txtGroupEmail.Text.Trim(), ParameterDirection.Input);
            DataParameter cliFax = new DataParameter(SqlDbType.NVarChar, "@cliFax ", txtFax.Text.Trim(), ParameterDirection.Input);
            DataParameter cliNotes = new DataParameter(SqlDbType.NVarChar, "@cliNotes", "", ParameterDirection.Input);
            DataParameter cliExportToExcel = new DataParameter(SqlDbType.Bit, "@cliExportToExcel", false, ParameterDirection.Input);
            DataParameter Salt = new DataParameter(SqlDbType.NVarChar, "@Salt ", "", ParameterDirection.Input);
            DataParameter Active = new DataParameter(SqlDbType.Bit, "@Active", false, ParameterDirection.Input);

            DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", "", ParameterDirection.Output, 30);

            DataParameter[] dpBasket = new DataParameter[] { clientID, cliUsername, cliPassword, cliName, cliEmail, cliFax, cliNotes, cliExportToExcel, Salt, Active, result };

            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.OSInsertClient", ref dpBasket, 60000);
            _result = dpBasket[10].ParamValue.ToString();
        }
        catch (Exception ex)
        {
            throw;
        }

        return _result;
    }
    protected void NewMember()
    {
        bool MemberExist = false;
        string _AddMember = "";
        try
        {

            RegistrationApplication registrationApplication = new RegistrationApplication(txtMembership.Text,
                txtPassword.Text, txtMemberEmail.Text, txtID.Text,
                txtMobile.Text, lblMemberName.Text, lblContract.Text, "Web", "", "");

            _AddMember = RegisterUser(registrationApplication);
            if (_AddMember == "F")
            {
                lblError.Text = "Your membership number is already registered. Please log in your membership No and password. if you forgot your password, please click on 'Forgot Password?' and we will be happy to send your password to your registered e-mail address.";
                lblError.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblError.Text = "Successfully Registered." + lblMemberName.Text + ", You can login now.";
                lblError.ForeColor = System.Drawing.Color.Green;
                btnSubmit.Visible = false;

                SaveAccordionValuestoDB();
                SendEmailToMember(txtMemberEmail.Text.Trim(), lblMemberName.Text);
            }
            tblMember.Visible = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// to save the only visible dependents to database
    /// </summary>
    private void SaveAccordionValuestoDB()
    {
        if (AccordionPane1.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic1, rdnPregnant1, rdnDelivered1, lblHeader1.ToolTip);

        if (AccordionPane2.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic2, rdnPregnant2, rdnDelivered2, lblHeader2.ToolTip);

        if (AccordionPane3.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic3, rdnPregnant3, rdnDelivered3, lblHeader3.ToolTip);

        if (AccordionPane4.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic4, rdnPregnant4, rdnDelivered4, lblHeader4.ToolTip);

        if (AccordionPane5.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic5, rdnPregnant5, rdnDelivered5, lblHeader5.ToolTip);

        if (AccordionPane6.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic6, rdnPregnant6, rdnDelivered6, lblHeader6.ToolTip);

        if (AccordionPane7.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic7, rdnPregnant7, rdnDelivered7, lblHeader7.ToolTip);

        if (AccordionPane8.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic8, rdnPregnant8, rdnDelivered8, lblHeader8.ToolTip);

        if (AccordionPane9.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic9, rdnPregnant9, rdnDelivered9, lblHeader9.ToolTip);

        if (AccordionPane10.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic10, rdnPregnant10, rdnDelivered10, lblHeader10.ToolTip);

        if (AccordionPane11.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic11, rdnPregnant11, rdnDelivered11, lblHeader11.ToolTip);

        if (AccordionPane12.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic12, rdnPregnant12, rdnDelivered12, lblHeader12.ToolTip);

        if (AccordionPane13.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic13, rdnPregnant13, rdnDelivered13, lblHeader13.ToolTip);

        if (AccordionPane14.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic14, rdnPregnant14, rdnDelivered14, lblHeader14.ToolTip);

        if (AccordionPane15.Visible == true)
            SaveThisAccordionChronicInfoToDB(cblChronic15, rdnPregnant15, rdnDelivered15, lblHeader15.ToolTip);

    }

    /// <summary>
    /// To Open Connection and Save to database
    /// </summary>
    /// <param name="cblChronic"></param>
    /// <param name="rdnPregnant"></param>
    /// <param name="rdnDelivered"></param>
    /// <param name="strMembershipNo"></param>
    private void SaveThisAccordionChronicInfoToDB(CheckBoxList cblChronic, RadioButtonList rdnPregnant, RadioButtonList rdnDelivered, string strMembershipNo)
    {
        int intChronicId;

        for (int i = 0; i < cblChronic.Items.Count - 1; i++)
        {
            if (cblChronic.Items[i].Selected)
            {
                intChronicId = int.Parse(cblChronic.Items[i].Value);
                SaveAnswerToDB(long.Parse(strMembershipNo), intChronicId);
            }
        }
        if (rdnPregnant.SelectedIndex != -1 && rdnPregnant.SelectedIndex != 0)
        {
            intChronicId = int.Parse(strMembershipNo);
            SaveAnswerToDB(long.Parse(txtMembership.Text), intChronicId);
        }
        if (rdnDelivered.SelectedIndex != -1 && rdnDelivered.SelectedIndex != 0)
        {
            intChronicId = int.Parse(rdnDelivered.SelectedValue);
            SaveAnswerToDB(long.Parse(strMembershipNo), intChronicId);
        }

    }

    public static string RegisterUser(RegistrationApplication Reg)
    {
        string _result;

        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            DataParameter membership = new DataParameter(SqlDbType.NVarChar, "@MembershipNo", Reg.Membership, ParameterDirection.Input);
            DataParameter password = new DataParameter(SqlDbType.NVarChar, "@Password", Reg.Password, ParameterDirection.Input);
            DataParameter fullName = new DataParameter(SqlDbType.NVarChar, "@FullName1", Reg.Fullname, ParameterDirection.Input);
            DataParameter email = new DataParameter(SqlDbType.NVarChar, "@Email", Reg.Email, ParameterDirection.Input);
            DataParameter mobile = new DataParameter(SqlDbType.NVarChar, "@Mobile", Reg.Mobile, ParameterDirection.Input);
            DataParameter identity_id = new DataParameter(SqlDbType.NVarChar, "@Identity_id", Reg.ID, ParameterDirection.Input);
            DataParameter ContractNo = new DataParameter(SqlDbType.NVarChar, "@contract_no", Reg.contract_no, ParameterDirection.Input);

            DataParameter channel = new DataParameter(SqlDbType.NVarChar, "@channel", Reg.channel, ParameterDirection.Input);
            DataParameter identity_type = new DataParameter(SqlDbType.NVarChar, "@identity_type", Reg.identity_type, ParameterDirection.Input);
            DataParameter membertype = new DataParameter(SqlDbType.NVarChar, "@membertype", Reg.membertype, ParameterDirection.Input);

            DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", 30, ParameterDirection.Output);
            DataParameter[] dpBasket = new DataParameter[] { membership, password, fullName, email, mobile,identity_id, 
                                                            ContractNo, channel, identity_type, membertype, result };
            //DataParameter[] dpBasket = new DataParameter[] { membership, password, fullName, email, mobile, result };
            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.spRegisterUser", ref dpBasket, 60000);
            _result = dpBasket[5].ParamValue.ToString();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return _result;
    }

    private bool checkMemberAlreadyRegistered()
    {
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        try
        {
            cmd.Connection = conn;
            cmd.CommandText = "CheckMemberExist";
            cmd.Parameters.Add(new SqlParameter("@MembershipNo", txtMembership.Text));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            var result = cmd.ExecuteScalar();

            if (result.ToString() != "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conn.Close();
        }

    }

    protected void btnGetDetails_Click(object sender, EventArgs e)
    {
        try
        {
            if (checkMemberAlreadyRegistered())
            {
                lblError.Text = "Your membership number is already registered. Please log in your membership No and password. if you forgot your password, please click on 'Forgot Password?' and we will be happy to send your password to your registered e-mail address.";
                lblError.ForeColor = System.Drawing.Color.Red;
                return;
            }

            Disclamer.Visible = true;
            // Grab the memberID
            string membershipNumber = txtMembership.Text;
            // Fetch the member's details
            MemberHelper mh = new MemberHelper(membershipNumber, "");
            Member m = mh.GetMemberDetails();
            lblCustomerName.Text = m.Customer;
            lblContract.Text = m.ContractNumber;
            lblMemberName.Text = m.MemberName;
            lblStatus.Text = m.Status;

            txtMobile.Text = m.MOBILE;
            //hidIdentityNo.Value = m.SaudiIqamaID;
            cv_Identity.ValueToCompare = m.SaudiIqamaID.ToString();
            MyAccordion.Visible = true;

            long ls_txnid;


            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN emp_request;
            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN emp_response;

            emp_request = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            emp_request.Username = WebPublication.CaesarSvcUsername;
            emp_request.Password = WebPublication.CaesarSvcPassword;
            ls_txnid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddhhmmssf"));
            emp_request.transactionID = ls_txnid;
            emp_request.membershipNo = membershipNumber;

            emp_response = ws.EnqMbrDetInfo(emp_request);

            if (emp_response.status == "0")
            {
                lblHealthcareProfile.Visible = true;
                for (int i = 0; i < emp_response.detail.Length; i++)
                {
                    AccordionPane1.Visible = true;
                    lblHeader1.Text = emp_response.detail[i].membershipNo + " - " + emp_response.detail[i].memberName;
                    lblHeader1.ToolTip = emp_response.detail[i].membershipNo;
                    DateTime dtDateOfBirth = Convert.ToDateTime(emp_response.detail[i].memberDOB);
                    Bind_Chronic_checkbox_radiobutton(cblChronic1, emp_response.detail[i].gender, rdnPregnant1, rdnDelivered1, lblPregnant1, lblDelevered1, GetAge(DateTime.Now, dtDateOfBirth));
                }
                btnSubmit.Enabled = true;
            }

            OS_DXC_WAP.CaesarWS.EnqMbrDepGetInfoRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqMbrDepGetInfoResponse_DN response;

            ls_txnid = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddhhmmssf"));

            request = new OS_DXC_WAP.CaesarWS.EnqMbrDepGetInfoRequest_DN();
            ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;         

            request.transactionID = ls_txnid;
            request.membershipNo = membershipNumber;

            response = ws.EnqMbrDepGetInfo(request);

            if (response.status == "0")
            {
                btnSubmit.Enabled = true;
                for (int i = 0; i < response.detail.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblHealthcareProfile.Visible = true;
                            AccordionPane1.Visible = true;
                            //txtMobile2.Text = response.detail[i].mobileNo;
                            lblHeader1.ToolTip = response.detail[i].membershipNo;
                            lblHeader1.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            Bind_Chronic_checkbox_radiobutton(cblChronic1, response.detail[i].gender, rdnPregnant1, rdnDelivered1, lblPregnant1, lblDelevered2, response.detail[i].mbrAge);
                            break;
                        case 1:
                            AccordionPane2.Visible = true;
                            //txtMobile2.Text = response.detail[i].mobileNo;
                            lblHeader2.ToolTip = response.detail[i].membershipNo;
                            lblHeader2.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            Bind_Chronic_checkbox_radiobutton(cblChronic2, response.detail[i].gender, rdnPregnant2, rdnDelivered2, lblPregnant2, lblDelevered2, response.detail[i].mbrAge);
                            break;
                        case 2:
                            lblHeader3.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader3.ToolTip = response.detail[i].membershipNo;
                            //txtMobile3.Text = response.detail[i].mobileNo;
                            AccordionPane3.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic3, response.detail[i].gender, rdnPregnant3, rdnDelivered3, lblPregnant3, lblDelevered3, response.detail[i].mbrAge);
                            break;
                        case 3:
                            lblHeader4.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader4.ToolTip = response.detail[i].membershipNo;
                            //txtMobile4.Text = response.detail[i].mobileNo;
                            AccordionPane4.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic4, response.detail[i].gender, rdnPregnant4, rdnDelivered4, lblPregnant4, lblDelevered4, response.detail[i].mbrAge);
                            break;
                        case 4:
                            lblHeader5.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile5.Text = response.detail[i].mobileNo;
                            lblHeader5.ToolTip = response.detail[i].membershipNo;
                            AccordionPane5.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic5, response.detail[i].gender, rdnPregnant5, rdnDelivered5, lblPregnant5, lblDelevered5, response.detail[i].mbrAge);
                            break;
                        case 5:
                            lblHeader6.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile6.Text = response.detail[i].mobileNo;
                            lblHeader6.ToolTip = response.detail[i].membershipNo;
                            AccordionPane6.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic6, response.detail[i].gender, rdnPregnant6, rdnDelivered6, lblPregnant6, lblDelevered6, response.detail[i].mbrAge);
                            break;
                        case 6:
                            lblHeader7.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile7.Text = response.detail[i].mobileNo;
                            lblHeader7.ToolTip = response.detail[i].membershipNo;
                            AccordionPane7.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic7, response.detail[i].gender, rdnPregnant7, rdnDelivered7, lblPregnant7, lblDelevered7, response.detail[i].mbrAge);
                            break;
                        case 7:
                            lblHeader8.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile8.Text = response.detail[i].mobileNo;
                            lblHeader8.ToolTip = response.detail[i].membershipNo;
                            AccordionPane8.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic8, response.detail[i].gender, rdnPregnant8, rdnDelivered8, lblPregnant8, lblDelevered8, response.detail[i].mbrAge);
                            break;
                        case 8:
                            lblHeader9.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            //txtMobile9.Text = response.detail[i].mobileNo;
                            lblHeader9.ToolTip = response.detail[i].membershipNo;
                            AccordionPane9.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic9, response.detail[i].gender, rdnPregnant9, rdnDelivered9, lblPregnant9, lblDelevered9, response.detail[i].mbrAge);
                            break;
                        case 9:
                            lblHeader10.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader10.ToolTip = response.detail[i].membershipNo;
                            //txtMobile10.Text = response.detail[i].mobileNo;
                            AccordionPane10.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic10, response.detail[i].gender, rdnPregnant10, rdnDelivered10, lblPregnant10, lblDelevered10, response.detail[i].mbrAge);
                            break;
                        case 10:
                            lblHeader11.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader11.ToolTip = response.detail[i].membershipNo;
                            //txtMobile11.Text = response.detail[i].mobileNo;
                            AccordionPane11.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic11, response.detail[i].gender, rdnPregnant11, rdnDelivered11, lblPregnant11, lblDelevered11, response.detail[i].mbrAge);
                            break;
                        case 11:
                            lblHeader12.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader12.ToolTip = response.detail[i].membershipNo;
                            //txtMobile12.Text = response.detail[i].mobileNo;
                            AccordionPane12.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic12, response.detail[i].gender, rdnPregnant12, rdnDelivered12, lblPregnant12, lblDelevered12, response.detail[i].mbrAge);
                            break;
                        case 12:
                            lblHeader13.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader13.ToolTip = response.detail[i].membershipNo;
                            //txtMobile13.Text = response.detail[i].mobileNo;
                            AccordionPane13.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic13, response.detail[i].gender, rdnPregnant13, rdnDelivered13, lblPregnant13, lblDelevered13, response.detail[i].mbrAge);
                            break;
                        case 13:
                            lblHeader14.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader14.ToolTip = response.detail[i].membershipNo;
                            //txtMobile14.Text = response.detail[i].mobileNo;
                            AccordionPane14.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic14, response.detail[i].gender, rdnPregnant14, rdnDelivered14, lblPregnant14, lblDelevered14, response.detail[i].mbrAge);
                            break;
                        case 14:
                            lblHeader15.Text = response.detail[i].membershipNo + " - " + response.detail[i].memberName;
                            lblHeader15.ToolTip = response.detail[i].membershipNo;
                            //txtMobile15.Text = response.detail[i].mobileNo;
                            AccordionPane15.Visible = true;
                            Bind_Chronic_checkbox_radiobutton(cblChronic15, response.detail[i].gender, rdnPregnant15, rdnDelivered15, lblPregnant15, lblDelevered15, response.detail[i].mbrAge);
                            break;
                    }

                }
            }

            if (!string.IsNullOrEmpty(m.MemberName))
            {
                //yahya 
                txtMembership.Enabled = false;
                r1.Visible = true;
                r2.Visible = true;
                r3.Visible = true;
                r4.Visible = true;
                r5.Visible = true;
                r6.Visible = true;
                agreement.Visible = true;
                tblInfo.Visible = true;
                healthcareProfile.Visible = true;
                lblError.Text = "";
            }

        }
        catch (Exception ex)
        {

            lblError.Text = ex.Message;
        }
    }

    /// <summary>
    /// bind accordion controls with values
    /// </summary>
    /// <param name="cblChronic"></param>
    /// <param name="strGender"></param>
    /// <param name="rdnPregnant"></param>
    /// <param name="rdnDelivered"></param>
    /// <param name="lblPregnant"></param>
    /// <param name="lblDelevered"></param>
    /// <param name="intMemAge"></param>
    private void Bind_Chronic_checkbox_radiobutton(CheckBoxList cblChronic, string strGender, RadioButtonList rdnPregnant,
                            RadioButtonList rdnDelivered, Label lblPregnant, Label lblDelevered,
                            long intMemAge)
    {
        DataTable dt = GetDataTable("select * from   tblChronic where control_type='CheckBox'");
        cblChronic.DataSource = dt;
        cblChronic.DataValueField = "chronic_id";
        cblChronic.DataTextField = "chronic_name"; // something similar to this
        cblChronic.DataBind();

        if (strGender == "Female" && intMemAge > 18)
        {
            rdnDelivered.Visible = true;
            rdnPregnant.Visible = true;
            lblPregnant.Visible = true;
            lblDelevered.Visible = true;
        }
        else if (strGender == "Male")
        {
            rdnDelivered.Visible = false;
            rdnPregnant.Visible = false;
            lblDelevered.Visible = false;
            lblDelevered.Visible = false;
        }
    }

    #region "Database Functions"

    /// <summary>
    /// Save Customer Answers to Database
    /// </summary>
    /// <param name="intMemberShipNo"></param>
    /// <param name="intChroID"></param>
    protected void SaveAnswerToDB(long intMemberShipNo, int intChroID)
    {

        if (CheckRecordExist("select * from tblMemberChronic where chronic_id=" + intChroID + " and " + "membership_no=" + intMemberShipNo) == false)
        {
            SqlConnection connection = new SqlConnection(connectionString);

            SqlConnection objConn = new SqlConnection(connection.ConnectionString);

            SqlCommand objCmd = new SqlCommand();
            objCmd.Connection = objConn;

            objCmd.CommandType = CommandType.Text;
            objCmd.CommandText = "insert into tblMemberChronic (membership_no,chronic_id) values (" + intMemberShipNo + "," + intChroID + ")";

            try
            {
                objCmd.Connection.Open();
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCmd.Connection.Close();
            }
        }
    }

    /// <summary>
    /// to check if the record exist before inserting healthcare info
    /// </summary>
    /// <param name="sqlQuery"></param>
    /// <returns></returns>
    private bool CheckRecordExist(string sqlQuery)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();
        try
        {
            cmd.Connection = connection;
            cmd.CommandText = sqlQuery;
            cmd.CommandType = CommandType.Text;
            connection.Open();

            var result = cmd.ExecuteScalar();

            if (result != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// To get the health care info from database
    /// </summary>
    /// <param name="sql"></param>
    /// <returns></returns>
    public static DataTable GetDataTable(string sql)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.ConnectionString = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandType = CommandType.Text;
        command.CommandText = sql;
        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.SelectCommand = command;
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        return dt;
    }

    #endregion

    #region "Send Email"

    /// <summary>
    /// send Email to the Client
    /// </summary>
    /// <param name="fromEmail"></param>
    /// <param name="toEmail"></param>
    /// <param name="subject"></param>
    /// <param name="content"></param>
    /// <param name="attachmentPaths"></param>
    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        // Setup the mail message
        MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

        // Deal with the attachments
        if (attachmentPaths != null)
        {
            foreach (string path in attachmentPaths)
            {
                // Create the attachment
                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(path));

                // Add the attachment
                mail.Attachments.Add(attachment);
            }
        }

        string smtpHost = WebPublication.EmailRelayAddress();
        int smtpPort = WebPublication.EmailRelayPort();

        SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
        mail.IsBodyHtml = true;

        smtp.Send(mail);
    }

    public bool SendNotificationMail(string FromUser, string ToUser, string Subject, string BodyContent, string FilePath)
    {
        bool MailSent = false;
        try
        {
            MailMessage mMailMessage = new MailMessage();
            mMailMessage.From = new MailAddress(FromUser);
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["MailToUser"]) == true)
            {
                string[] mailList = ToUser.Split(';');
                foreach (string mailTo in mailList)
                {
                    mMailMessage.To.Add(new MailAddress(mailTo));
                }
            }
            else
            {
                mMailMessage.To.Add(new MailAddress("lokesh.narayanappa@bupa.com.sa"));
            }
            mMailMessage.Subject = Subject.Trim();
            mMailMessage.Body = BodyContent;
            string smtpHost = WebPublication.EmailRelayAddress();
            int smtpPort = WebPublication.EmailRelayPort();

            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
            mMailMessage.IsBodyHtml = true;

            smtp.Send(mMailMessage);
        }
        catch (Exception)
        {
            MailSent = false;
        }
        return MailSent;
    }
    #endregion

    protected void btnUpload_Click(object sender, EventArgs e)
    {

    }

    public void MoveFile(string sourcePath, string destinationPath)
    {
        //MTOM.MTOMService svc = new MTOM.MTOMService();
        //svc.UploadFile(sourcePath, destinationPath);
    }


    protected void RadTabStrip1_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
    {
        if (Page.IsValid)
        {
            if (RadTabStrip1.Tabs.Count == (Convert.ToInt32(e.Tab.ID.Substring(1, 1)) + 1))
            {
                //enable the submit button
                btnAdd.Visible = true;
                btnNext.Visible = false;
                btnBack.Visible = true;
                rgAttachments.Rebind();
            }
            else if (e.Tab.ID.Substring(1, 1) == "0")
            {
                btnAdd.Visible = false;
                btnNext.Visible = true;
                btnBack.Visible = false;
            }
            else
            {
                btnAdd.Visible = false;
                btnNext.Visible = true;
                btnBack.Visible = true;
            }
            RadTabStrip1.TabIndex = Convert.ToInt16(e.Tab.ID.Substring(1, 1));
            btnNext.ValidationGroup = "MandatoryFields" + ((RadTabStrip1.TabIndex) + 1);
        }
        else
        {
            RadWindowManager1.RadAlert("Fields marked with * are mandatory", 330, 180, "Validation Message", "");
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (Page.IsValid == true)
        {
            if (RadTabStrip1.TabIndex == 0)
            {
                using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
                {
                    ProviderProfile objProviderProfile = objOnlineProvidersEntities.ProviderProfiles.Where(item => item.PP_UserName == txtLoginID.Text.Trim()).SingleOrDefault();

                    if (objProviderProfile != null)
                    {
                        lblMessage.Text = "The Preferred Login ID entered is already used.Please try with different one. ";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                }
            }
            RadTabStrip1.Tabs[(RadTabStrip1.TabIndex) + 1].Selected = true;
            RadTabStrip1.Tabs[(RadTabStrip1.TabIndex) + 1].Enabled = true;
            RadMultiPage2.SelectedIndex = (RadTabStrip1.TabIndex) + 1;
            RadTabStrip1.TabIndex = Convert.ToInt16(RadTabStrip1.TabIndex + 1);
            btnAdd.Visible = false;
            btnBack.Visible = true;
            if (RadTabStrip1.Tabs.Count == (RadTabStrip1.TabIndex + 1))
            {
                btnAdd.Visible = true;
                btnNext.Visible = false;
            }
            else
            {
                btnNext.Visible = true;
                btnNext.ValidationGroup = "MandatoryFields" + ((RadTabStrip1.TabIndex) + 1);
            }
        }
        else
        {
            //display error message
            RadWindowManager1.RadAlert("Fields marked with * are mandatory", 330, 180, "Validation Message", "");
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        try
        {


            RadTabStrip1.Tabs[(RadTabStrip1.TabIndex) - 1].Selected = true;
            RadMultiPage2.SelectedIndex = (RadTabStrip1.TabIndex) - 1;
            RadTabStrip1.TabIndex = Convert.ToInt16(RadTabStrip1.TabIndex - 1);
            btnAdd.Visible = false;
            btnNext.Visible = true;
            btnNext.ValidationGroup = "MandatoryFields" + ((RadTabStrip1.TabIndex) + 1);
            if ((RadTabStrip1.TabIndex) == 0)
            {
                btnBack.Visible = false;
            }
        }
        catch (Exception)
        {

        }
    }


    private void FillDropDowns()
    {
        try
        {

            using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
            {
                List<ProviderPractice> objProviderPracticeList = objOnlineProvidersEntities.ProviderPractices.ToList().OrderBy(items => items.P_PracticeName).ToList();
                ddlPractice.DataSource = objProviderPracticeList;
                ddlPractice.DataTextField = "P_PracticeName";
                ddlPractice.DataValueField = "P_PracticeName";
                ddlPractice.DataBind();
                Session["ProviderPracticeList"] = objProviderPracticeList;

                List<ProviderDocumentsMaster> objProviderDocumentList = objOnlineProvidersEntities.ProviderDocumentsMasters.ToList().OrderBy(items => items.PD_DocumentType).ToList();
                ddlDocumentType.DataSource = objProviderDocumentList;
                ddlDocumentType.DataTextField = "PD_DocumentType";
                ddlDocumentType.DataValueField = "PD_DocumentType";
                ddlDocumentType.DataBind();
                ddlDocumentType.Items.Insert(0, new ListItem("--Select--", ""));
                StringBuilder sb = new StringBuilder();

                foreach (ProviderDocumentsMaster objProviderDocumentsMaster in objProviderDocumentList)
                {
                    if (objProviderDocumentsMaster.PD_Mandatory == true)
                    {
                        if (sb.Length == 0)
                        {
                            sb.Append(objProviderDocumentsMaster.PD_DocumentType);
                        }
                        else
                        {
                            sb.Append(", " + objProviderDocumentsMaster.PD_DocumentType);
                        }
                    }
                }
                lblMandatoryDocumentNote.Text = sb.ToString();
                //M_Countries objCountry = new M_Countries();
                //objCountry.CountryCode = "SA";
                //objCountry.CountryName = "Saudi Arabia";
                //RadComboBoxItem rb = new RadComboBoxItem();
                //            rb.Text = "lokesh";
                //            rb.Value = "SA";
                //            ddlCountry.Items.Add(rb);
                List<M_Countries> objCountryList = objOnlineProvidersEntities.M_Countries.ToList().OrderBy(item => item.CountryName).ToList();
                var itemToRemove = objCountryList.Single(r => r.CountryCode=="SA");
                objCountryList.Remove(itemToRemove);
                objCountryList.Insert(0, itemToRemove);
                ddlCountry.DataSource = objCountryList;
                ddlCountry.DataTextField = "Countryname";
                ddlCountry.DataValueField = "CountryCode";
                ddlCountry.DataBind();

                List<M_Cities> objCityList = objOnlineProvidersEntities.M_Cities.ToList().OrderBy(items => items.CityName).ToList();
                Session["CityList"] = objCityList;

                List<M_Speciality> objSpecialityList = objOnlineProvidersEntities.M_Speciality.ToList().OrderBy(items => items.SpecialityName).ToList();
                chkSpecilaity.DataSource = objSpecialityList;
                chkSpecilaity.DataTextField = "SpecialityName";
                chkSpecilaity.DataValueField = "SpecialityName";
                chkSpecilaity.DataBind();

                List<M_Facility> objFacilityList = objOnlineProvidersEntities.M_Facility.ToList().OrderBy(items => items.FacilityName).ToList();
                chkFacility.DataSource = objFacilityList;
                chkFacility.DataTextField = "FacilityName";
                chkFacility.DataValueField = "FacilityName";
                chkFacility.DataBind();



            }
        }
        catch (Exception ex)
        {

            CommonClass.LogError("FillDropDowns", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
        }

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid == true)
        {
            String MasterNotificationStatusCheck = "Submit";
            strUserRole = "Provider";
            try
            {
                using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
                {
                    using (var dbContextTransaction = objOnlineProvidersEntities.Database.BeginTransaction())
                    {

                        //DataTable dtAttachments = (DataTable)Session["AttachmentsTable"];
                        List<ProviderDocument> objProviderDocumentList = (List<ProviderDocument>)Session["AttachmentsTable"];

                        StringBuilder sb = new StringBuilder();
                        #region Validate Attached Docuemnts
                        List<ProviderDocumentsMaster> objProviderDocumentsMasterList = objOnlineProvidersEntities.ProviderDocumentsMasters.ToList().Where(item => item.PD_Mandatory == true).ToList();
                        if (Session["AttachmentsTable"] == null)
                        {
                            foreach (ProviderDocumentsMaster objPrividerDocumentMaster in objProviderDocumentsMasterList)
                            {
                                if (sb.Length == 0)
                                {
                                    sb.Append("<b>Following documents are mandatory.</b>");
                                }
                                sb.Append("<br />- " + objPrividerDocumentMaster.PD_DocumentType);

                            }
                        }
                        else
                        {
                            foreach (ProviderDocumentsMaster objPrividerDocumentMaster in objProviderDocumentsMasterList)
                            {
                                if (objProviderDocumentList.Where(item => item.PD_DocumentType == objPrividerDocumentMaster.PD_DocumentType).Count() == 0)
                                {
                                    if (sb.Length == 0)
                                    {
                                        sb.Append("<b>Following documents are mandatory. </b> ");
                                    }
                                    sb.Append("<br />- " + objPrividerDocumentMaster.PD_DocumentType);
                                }

                            }
                        }
                        if (sb.Length > 0)
                        {
                            RadWindowManager1.RadAlert(sb.ToString(), 330, 180, "Validation Message", "");
                            return;
                        }
                        #endregion

                        //Based on the Notification data get the next action plan from defined flow
                        int status = 0;
                        List<M_NotificationMaster> objM_NotificationMasterList = objOnlineProvidersEntities.M_NotificationMaster.ToList().Where(item => item.NM_ModuleName == strModule && item.NM_Role == strUserRole && item.NM_StageID == Stage && item.NM_Action == MasterNotificationStatusCheck).ToList();
                        if (objM_NotificationMasterList.Count > 0)
                        {
                            try
                            {
                                ProviderProfile objProviderProfile = new ProviderProfile();
                                objProviderProfile.PP_ProviderStatus = ProviderStatus.Submitted_For_Approval.ToString();
                                objOnlineProvidersEntities.ProviderProfiles.Add(GetProviderInfo(objProviderProfile));
                                status = objOnlineProvidersEntities.SaveChanges();
                                long RequestId = objProviderProfile.PP_ProviderId;

                                //insert Notification request on the workflow process
                                NotificationLog objNotification = new NotificationLog();
                                objNotification.N_NotifyTo = (objM_NotificationMasterList.First()).NM_AssignTo;
                                if ((objM_NotificationMasterList.First()).NM_NotifyType == "Queue")
                                {
                                    objNotification.N_NotifyToUser = "Queue";
                                }
                                else
                                {
                                    objNotification.N_NotifyTo = "";
                                }
                                objNotification.N_StageID = (objM_NotificationMasterList.First()).NM_StageID;
                                objNotification.N_NotifyFrom = strUserRole;
                                objNotification.N_RequestID = RequestId;//basically the id once the profile is saved in database
                                objNotification.N_Status = "Pending";
                                objNotification.N_ActionDate = DateTime.Now;
                                objOnlineProvidersEntities.NotificationLogs.Add(objNotification);
                                status = objOnlineProvidersEntities.SaveChanges();



                                #region Additional Contacts
                                UpdateProviderContacts(RequestId, true);
                                if (Session["ContactList"] != null)
                                {
                                    List<ProviderContact> objProviderContractList = (List<ProviderContact>)Session["ContactList"];
                                    foreach (ProviderContact objProviderContact in objProviderContractList)
                                    {
                                        objProviderContact.PC_ProviderId = RequestId;
                                        objOnlineProvidersEntities.ProviderContacts.Add(objProviderContact);

                                        if (objProviderContact.PC_ContactId > 0)
                                        {
                                            objOnlineProvidersEntities.Entry(objProviderContact).State = System.Data.Entity.EntityState.Modified;

                                        }
                                        status = objOnlineProvidersEntities.SaveChanges();
                                    }

                                }
                                #endregion

                                #region Pharmacy

                                UpdateProviderPharmacy(RequestId, true);
                                if (Session["PharmacyList"] != null)
                                {
                                    List<ProviderPharmacy> objProviderPharmacylist = (List<ProviderPharmacy>)Session["PharmacyList"];
                                    if (objProviderPharmacylist.Count > 0)
                                    {
                                        foreach (ProviderPharmacy objProviderPharmacy in objProviderPharmacylist)
                                        {
                                            objOnlineProvidersEntities.ProviderPharmacies.Add(objProviderPharmacy);

                                            if (objProviderPharmacy.ProviderPharmacyId > 0)
                                            {
                                                objOnlineProvidersEntities.Entry(objProviderPharmacy).State = System.Data.Entity.EntityState.Modified;

                                            }
                                            objOnlineProvidersEntities.SaveChanges();
                                        }

                                    }
                                }

                                #endregion


                                #region Attachements
                                if (Session["AttachmentsTable"] != null)
                                {
                                    if (hfProviderId.Value == "")
                                    {
                                        using (NetworkShareAccesser.Access("bjfs05", "Bupa_Mid_East", "AbcpdfUat", "DdfDdf890*()"))
                                        {
                                            string ServerFolderPath = ConfigurationManager.AppSettings["OnlineContractingDOcsFolderPath"].ToString();
                                            bool exists = System.IO.Directory.Exists(ServerFolderPath + RequestId);

                                            if (!exists)
                                            {
                                                Directory.Move(ServerFolderPath + txtLoginID.Text.Trim(), ServerFolderPath + RequestId);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        using (NetworkShareAccesser.Access("bjfs05", "Bupa_Mid_East", "AbcpdfUat", "DdfDdf890*()"))
                                        {
                                            string ServerFolderPath = ConfigurationManager.AppSettings["OnlineContractingDOcsFolderPath"].ToString();
                                            bool exists = System.IO.Directory.Exists(ServerFolderPath + RequestId);

                                            if (!exists)
                                            {
                                                Directory.Move(ServerFolderPath + (hfProviderId.Value + "S"), ServerFolderPath + RequestId);
                                            }

                                        }
                                    }
                                    List<ProviderDocument> objProviderDocumentlist = (List<ProviderDocument>)Session["AttachmentsTable"];
                                    if (objProviderDocumentlist.Count > 0)
                                    {
                                        foreach (ProviderDocument objProviderDocument in objProviderDocumentlist)
                                        {
                                            objProviderDocument.PD_ProviderId = RequestId;

                                            objProviderDocument.PD_DocumentPath = objProviderDocument.PD_DocumentPath.Substring(ConfigurationManager.AppSettings["OnlineContractingDOcs"].Length);
                                            if (hfProviderId.Value == "")
                                            {
                                                objProviderDocument.PD_DocumentPath = objProviderDocument.PD_DocumentPath.Replace(txtLoginID.Text.Trim(), RequestId.ToString());
                                            }
                                            else
                                            {
                                                objProviderDocument.PD_DocumentPath = objProviderDocument.PD_DocumentPath.Replace(hfProviderId.Value + "S", RequestId.ToString());
                                            }
                                            objProviderDocument.PD_Status = true;
                                            objOnlineProvidersEntities.ProviderDocuments.Add(objProviderDocument);
                                            if (objProviderDocument.PD_DocumentId > 0)
                                            {
                                                objOnlineProvidersEntities.Entry(objProviderDocument).State = System.Data.Entity.EntityState.Modified;

                                            }
                                            status = objOnlineProvidersEntities.SaveChanges();
                                        }
                                    }
                                }
                                #endregion

                                #region To delete the Draft Record if user has saved
                                if (hfProviderId.Value != "")
                                {
                                    if (RequestId != Convert.ToInt32(hfProviderId.Value))
                                    {
                                        ProviderProfileSave objProviderProfileSave = new ProviderProfileSave();
                                        objProviderProfileSave.PP_ProviderId = Convert.ToInt32(hfProviderId.Value);
                                        objOnlineProvidersEntities.ProviderProfileSaves.Add(objProviderProfileSave);
                                        objOnlineProvidersEntities.Entry(objProviderProfileSave).State = System.Data.Entity.EntityState.Deleted;
                                        status = objOnlineProvidersEntities.SaveChanges();
                                    }
                                }
                                #endregion

                                dbContextTransaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                CommonClass.LogError("btnAdd3", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
                                lblMessage.Text = "We are unable to process your request at the moment.Please try later.";
                                lblMessage.ForeColor = System.Drawing.Color.Red;
                                return;
                            }
                        }
                        else
                        {
                            //thow error to user ,unable to process your request.
                            return;
                        }
                        try
                        {


                            #region Provider Relations
                            //string strContent = "Dear Provider Relations,<br>";
                            //strContent = strContent + "A request for provider user account has been submitted with the following details: <br>";
                            //strContent = strContent + "<table width='100%'>";
                            //strContent = strContent + "<tr><td colspan='2' align='left'><b>Request Information:</b></td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Request ID</b></td><td>" + RequestId + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Provider Name</b></td><td>" + txtProviderName.Text.Trim() + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Requester Name</b></td><td>" + txtContactPerson.Text.Trim() + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Request Date</b></td><td>" + System.DateTime.Now.ToShortDateString() + "</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2'>This is a computer generated email. Please do not reply to this email.</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2'>Yours sincerely,</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2'>Bupa Online Service</td></tr>";
                            //strContent = strContent + "</table>";
                            //SendEmail("webmaster@bupame.com", "lokesh.narayanappa@bupa.com.sa", "Request for provider user account", strContent, null);
                            #endregion

                            try
                            {
                                if ((objM_NotificationMasterList.First()).NM_MailTemplateID != "")
                                {
                                    string[] MailTemplateIds = (objM_NotificationMasterList.First()).NM_MailTemplateID.Split(',');
                                    foreach (string strTemplateId in MailTemplateIds)
                                    {
                                        int TemplateId = Convert.ToInt32(strTemplateId);
                                        string MailTo = "";
                                        string Addressee = "";
                                        M_MailTemplates objM_MailTemplates = objOnlineProvidersEntities.M_MailTemplates.Where(item => item.MT_TemplateID == TemplateId && item.ModuleName == strModule).FirstOrDefault();
                                        if (objM_MailTemplates.MailTo == "Provider")
                                        {
                                            MailTo = txtContactPersonEmail.Text.Trim();
                                            Addressee = txtContactPerson.Text;
                                        }
                                        else if (objM_MailTemplates.MailTo == "Queue")
                                        {
                                            Addressee = objM_MailTemplates.BodyAddressee;
                                            List<ApplicationUser> objUsersList = objOnlineProvidersEntities.ApplicationUsers.ToList().
                                                              Where(item => item.UserRole == (objM_NotificationMasterList.First()).NM_AssignTo
                                                              && item.Status == true).ToList();
                                            if (objUsersList.Count > 0)
                                            {
                                                foreach (ApplicationUser objUser in objUsersList)
                                                {
                                                    if (MailTo == "")
                                                    {
                                                        MailTo = MailTo + objUser.EmailId;
                                                    }
                                                    else
                                                    {
                                                        MailTo = MailTo + ";" + objUser.EmailId;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {

                                        }
                                        if (MailTo != "")
                                        {
                                            string BodyContent = "";
                                            //CommonClass.SendNotificationMail("", objM_MailTemplates.Subject, objM_MailTemplates.BodyContent, objM_MailTemplates.TemplateName);
                                            StringBuilder sbBody = new StringBuilder();
                                            string Applicationpath = HttpContext.Current.Request.PhysicalApplicationPath;
                                            using (StreamReader reader = new StreamReader(Applicationpath + "\\" + ConfigurationManager.AppSettings["MailTemplatesFolder"] + "\\" + objM_MailTemplates.TemplateName.Trim() + ".html"))
                                            {

                                                String line = String.Empty;
                                                while ((line = reader.ReadLine()) != null)
                                                {
                                                    sbBody.Append(line);
                                                }
                                            }
                                            BodyContent = sbBody.ToString();
                                            BodyContent = BodyContent.Replace("<!--Body Message-->", objM_MailTemplates.BodyContent);
                                            BodyContent = BodyContent.Replace("<!--Addreseee-->", Addressee);
                                            SendNotificationMail("webmaster@bupame.com", MailTo, objM_MailTemplates.Subject, BodyContent, null);
                                        }
                                    }
                                }
                            }
                            catch (Exception exMail)
                            {
                                CommonClass.LogError("ApprovalMail", exMail.Message, exMail.InnerException, exMail.StackTrace, txtLoginID.Text);
                            }
                            //#region Provider
                            //string strContent = "Dear Provider ,<br>";
                            //strContent = strContent + "Your request for online registration has been accounted and will be processed soon. <br>";
                            //strContent = strContent + "One of Our Provider Relations officer will get in touch with you soon. <br>";
                            //strContent = strContent + "<table width='100%'>";
                            //strContent = strContent + "<tr><td colspan='2' align='left'><b>Request Information:</b></td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Request ID</b></td><td>" + RequestId + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Provider Name</b></td><td>" + txtProviderName.Text.Trim() + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Requester Name</b></td><td>" + txtContactPerson.Text.Trim() + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Request Date</b></td><td>" + System.DateTime.Now.ToShortDateString() + "</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2' align='left'><b>Your Login Credentials are:</b></td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>User ID</b></td><td>" + txtEmailAddress.Text.Trim() + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Pasword</b></td><td>" + "Bupa123" + "</td></tr>";
                            //strContent = strContent + "Please " + "<a href=\"" + "http://onlineservices.bupa.com.sa" + "\">click Here</a>" + " for Login to Online Services. <br>";
                            //strContent = strContent + "<tr><td colspan='2'>This is a computer generated email. Please do not reply to this email.</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2'>Yours sincerely,</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2'>Bupa Online Service</td></tr>";
                            //strContent = strContent + "</table>";
                            //SendEmail("webmaster@bupame.com", "lokesh.narayanappa@bupa.com.sa", "Request for provider user account", strContent, null);
                            //#endregion

                            //#region Comnnercial OPerations

                            //strContent = "Dear Commercial Operations,<br>";
                            //strContent = strContent + "A request for provider user account has been submitted with the following details: <br>";
                            //strContent = strContent + "<table width='100%'>";
                            //strContent = strContent + "<tr><td colspan='2' align='left'><b>Request Information:</b></td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Request ID</b></td><td>" + RequestId + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Provider Name</b></td><td>" + txtProviderName.Text.Trim() + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Requester Name</b></td><td>" + txtContactPerson.Text.Trim() + "</td></tr>";
                            //strContent = strContent + "<tr><td style='width: 131px'><b>Request Date</b></td><td>" + System.DateTime.Now.ToShortDateString() + "</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2'>This is a computer generated email. Please do not reply to this email.</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2'>Yours sincerely,</td></tr>";
                            //strContent = strContent + "<tr><td colspan='2'>Bupa Online Service</td></tr>";
                            //strContent = strContent + "</table>";
                            //SendEmail("webmaster@bupame.com", "lokesh.narayanappa@bupa.com.sa", "Request for provider user account", strContent, null);

                            //#endregion
                        }
                        catch (Exception ex2)
                        {
                            CommonClass.LogError("btnAdd2", ex2.Message, ex2.InnerException, ex2.StackTrace, txtLoginID.Text);
                        }

                    }

                    ClearFields();
                    //lblMessage.Text = "We have processed your request.You will shortly receive mail from us for future correspondence.";
                    //lblMessage.ForeColor = System.Drawing.Color.Green;
                    tblProviderSuccessMessage.Visible = true;
                    tblProvider.Visible = false;
                    trProviderNote.Visible = false;
                    //RadWindowManager1.RadAlert("Your request has been processed.<br />Provider Relations Officer will be contacting you soon.<br />For further Communication please use the Login Credentials sent to your Registered E-mail", 330, 180, "Validation Message", "");
                }
            }
            catch (Exception ex)
            {

                CommonClass.LogError("btnAdd", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
                lblMessage.Text = "We are unable to process your request at the moment.Please try later.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }

        }
        else
        {
            //display error message
            RadWindowManager1.RadAlert("Please provide details for the fields marked with *.", 330, 180, "Validation Message", "");
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid == true)
        {
            try
            {
                OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities();
                ProviderProfile objProviderProfile = new ProviderProfile();
                objOnlineProvidersEntities.ProviderProfiles.Add(GetProviderInfo(objProviderProfile));
                objProviderProfile.PP_ProviderStatus = ProviderStatus.Submitted_For_Approval.ToString();
                objOnlineProvidersEntities.Entry(objProviderProfile).State = System.Data.Entity.EntityState.Modified;
                int status = objOnlineProvidersEntities.SaveChanges();
                btnAdd.Visible = true;
                btnUpdate.Visible = false;
                ClearFields();
                Response.Redirect("providers.aspx");
            }
            catch (Exception ex)
            {

                CommonClass.LogError("btnUpdate", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
            }

        }
        else
        {
            //display error message
        }
    }

    protected void btnNewUser_Click(object sender, EventArgs e)
    {
        // pnlSearch.Visible = false;
        // pnlProviderDetails.Visible = true;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearFields();
        // pnlSearch.Visible = true;
        // pnlProviderDetails.Visible = false;
    }

    private void ClearFields()
    {
        try
        {
            txtProviderName.Text = string.Empty;
            txtLoginID.Text = string.Empty;
            ddlCity.Text = string.Empty;
            ddlCity.DataSource = null;
            ddlCity.DataBind();
            ddlCity.ClearSelection();
            ddlCountry.ClearSelection();
            ddlCountry.Text = "";
            txtStreet.Text = string.Empty;
            txtPOBox.Text = string.Empty;
            ddlPractice.ClearSelection();
            trPracticeOthers.Visible = false;
            txtDistrict.Text = string.Empty;
            txtBuildingNo.Text = string.Empty;
            txtZipCode.Text = string.Empty;
            txtEmailAddress.Text = string.Empty;
            txtGPSLongitude.Text = string.Empty;
            txtGPSLatitude.Text = string.Empty;
            txtNICUBeds.Text = string.Empty;
            txtCCHILicenseNo.Text = string.Empty;
            txtCCHILicenseExpirationDates.SelectedDate = null;
            txtMOHLicenseNo.Text = string.Empty;
            txtMOHLicenseED.SelectedDate = null;
            txtCommRegistrationNo.Text = string.Empty;
            txtCommRegistrationNoED.SelectedDate = null;
            //txtDoctorName.Text = string.Empty;
            //txtLicenseNo.Text = string.Empty;
            //txtSpeciality.Text = string.Empty;
            txtNumberOfDoctors.Text = string.Empty;
            txtNumberOfRooms.Text = string.Empty;
            txtNumberOfBeds.Text = string.Empty;
            txtNumberOfNurses.Text = string.Empty;
            txtICUBeds.Text = string.Empty;
            txtContactPerson.Text = string.Empty;
            txtMobileNo.Text = string.Empty;
            txtContactPersonDept.Text = string.Empty;
            txtContactPersonEmail.Text = string.Empty;
            txtContactPersonPosition.Text = string.Empty;
            txtFaxNumber.Text = string.Empty;
            txtFacebookAddress.Text = string.Empty;
            txtBankName.Text = string.Empty;
            txtBankPayeeName.Text = string.Empty;
            txtIBANNo.Text = string.Empty;
            btnAdd.Visible = true;
            btnUpdate.Visible = false;
            btnBack.Visible = false;
            Session["AttachmentsTable"] = null;
            Session["ContactList"] = null;
            Session["PharmacyList"] = null;
            rgAttachments.Rebind();
            rgAdditionalContacts.Rebind();
            rgPharmacy.Rebind();
            RadTabStrip1.Tabs[0].Selected = true;
            ddlOwnPharmacy.SelectedIndex = -1;
            btnNext.ValidationGroup = "MandatoryFields1";
            for (int k = 1; k < RadTabStrip1.Tabs.Count; k++)
            {
                RadTabStrip1.Tabs[k].Enabled = false;
            }
            RadMultiPage2.SelectedIndex = 0;
            RadTabStrip1.TabIndex = 0;
            btnAdd.Visible = false;
            btnNext.Visible = true;
            hfProviderId.Value = "";
            txtFacilityOthers.Text = string.Empty;
            txtSpecialityOthers.Text = string.Empty;
            tbSpecialityOthers.Visible = false;
            tbFacilityOthers.Visible = false;
            chkFacilityOthers.Items[0].Selected = false;
            chkSpecilaityOthers.Items[0].Selected = false;
            FillDropDowns();
            txtProviderName.Enabled = true;
            txtLoginID.Enabled = true;
        }
        catch (Exception ex)
        {


        }
    }

    protected void
        btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities();
            //List<ProviderProfile > provider  = objOnlineProvidersEntities.ProviderProfiles.SelectMany(a => a.PP_LicenseNo == txtProviderSearch.Text).ToList();
            //var address = objOnlineProvidersEntities.ProviderProfiles.First(a => a.PP_LicenseNo == txtProviderSearch.Text);
            var provider = objOnlineProvidersEntities.ProviderProfiles.Where(a => a.PP_CCHILicenseNo == "").FirstOrDefault();
            if (provider != null)
            {
                //List<ProviderProfile > objprovider  = objOnlineProvidersEntities.ProviderProfiles.ToList();
                SetProviderInfo(provider);
                // pnlSearch.Visible = false;
                // pnlProviderDetails.Visible = true;
                // lblProviderStatus.Text = provider.PP_ProviderStatus.Replace('_', ' ');
                if (provider.PP_ProviderStatus == ProviderStatus.Saved_As_Draft.ToString()
                    || provider.PP_ProviderStatus == ProviderStatus.Rejected.ToString() //with timeperiod check for updation
                    || provider.PP_ProviderStatus == ProviderStatus.Cancelled.ToString())
                {
                    btnAdd.Visible = false;
                    btnUpdate.Visible = true;
                }
                else
                {
                    // pnlProviderDetails.Enabled = false;
                    btnAdd.Visible = false;
                    //  btnCancel.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {

            CommonClass.LogError("btnSearch", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
        }


    }

    protected void SearchProviderInfo(int ProviderId)
    {
        try
        {
            OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities();
            //List<ProviderProfile > provider  = objOnlineProvidersEntities.ProviderProfiles.SelectMany(a => a.PP_LicenseNo == txtProviderSearch.Text).ToList();
            //var address = objOnlineProvidersEntities.ProviderProfiles.First(a => a.PP_LicenseNo == txtProviderSearch.Text);
            var provider = objOnlineProvidersEntities.ProviderProfiles.Where(a => a.PP_ProviderId == ProviderId).FirstOrDefault();
            if (provider != null)
            {
                //List<ProviderProfile > objprovider  = objOnlineProvidersEntities.ProviderProfiles.ToList();
                SetProviderInfo(provider);
                // pnlSearch.Visible = false;
                // pnlProviderDetails.Visible = true;
                // lblProviderStatus.Text = provider.PP_ProviderStatus.Replace('_', ' ');
                if (provider.PP_ProviderStatus == ProviderStatus.Saved_As_Draft.ToString()
                    || provider.PP_ProviderStatus == ProviderStatus.Rejected.ToString() //with timeperiod check for updation
                    || provider.PP_ProviderStatus == ProviderStatus.Cancelled.ToString())
                {
                    btnAdd.Visible = false;
                    btnUpdate.Visible = true;
                }
                else
                {
                    //  pnlProviderDetails.Enabled = false;
                    btnAdd.Visible = false;
                    //  btnCancel.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {

            CommonClass.LogError("SearchProviderInfo", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
        }


    }

    private void SetProviderInfo(ProviderProfile providerProfile)
    {
        try
        {

            ViewState["ProviderStatus"] = providerProfile.PP_ProviderStatus;
            txtProviderName.Text = providerProfile.PP_ProviderName;
            txtLoginID.Text = providerProfile.PP_UserName;
            ddlCity.Text = providerProfile.PP_City;
            ddlCountry.Text = providerProfile.PP_Country;
            txtStreet.Text = providerProfile.PP_Street;
            txtPOBox.Text = providerProfile.PP_POBox;
            ddlPractice.SelectedValue = providerProfile.PP_Practice;
            txtDistrict.Text = providerProfile.PP_District;
            txtBuildingNo.Text = providerProfile.PP_BuildingNo;
            txtZipCode.Text = providerProfile.PP_ZIPCode;
            txtEmailAddress.Text = providerProfile.PP_EmailAddress;
            txtGPSLongitude.Text = providerProfile.PP_GPSLongitude;
            txtGPSLatitude.Text = providerProfile.PP_GPSLatitude;
            txtNICUBeds.Text = providerProfile.PP_NICUBeds.ToString();
            txtCCHILicenseNo.Text = providerProfile.PP_CCHILicenseNo;
            txtCCHILicenseExpirationDates.SelectedDate = providerProfile.PP_CCHILIcenseExpirationDate;
            txtMOHLicenseNo.Text = providerProfile.PP_MOHLicenseNo;
            txtMOHLicenseED.SelectedDate = providerProfile.PP_MOHLIcenseNoExpirationDate;
            txtCommRegistrationNo.Text = providerProfile.PP_CommRegistrationNo;
            txtCommRegistrationNoED.SelectedDate = providerProfile.PP_CommRegNoExpirationDate;
            //txtDoctorName.Text = providerProfile.PP_DoctorName;
            //txtLicenseNo.Text = providerProfile.PP_LicenseNo;
            //txtSpeciality.Text = providerProfile.PP_Speciality;
            txtNumberOfDoctors.Text = providerProfile.PP_NumberOfDoctors.ToString();
            txtNumberOfRooms.Text = providerProfile.PP_NumberOfRooms.ToString();
            txtNumberOfBeds.Text = providerProfile.PP_NumberOfBeds.ToString();
            txtNumberOfNurses.Text = providerProfile.PP_NumberOfNurses.ToString();
            txtICUBeds.Text = providerProfile.PP_ICUBeds.ToString();
            txtContactPerson.Text = providerProfile.PP_ContactPerson;
            txtMobileNo.Text = providerProfile.PP_MobileNo.ToString();
            txtContactPersonDept.Text = providerProfile.PP_ContactPersonDept;
            txtContactPersonEmail.Text = providerProfile.PP_ContactPersonEmail;
            txtContactPersonPosition.Text = providerProfile.PP_ContactPersonPosition;
            txtFaxNumber.Text = providerProfile.PP_FaxNumber.ToString();
            txtFacebookAddress.Text = providerProfile.PP_FacebookAddress;
            txtBankName.Text = providerProfile.PP_BankName;
            txtBankPayeeName.Text = providerProfile.PP_BankPayeeName;
            txtIBANNo.Text = providerProfile.PP_IBANNo;
            if (providerProfile.PP_SpecialityOthers != "")
            {
                txtSpecialityOthers.Text = providerProfile.PP_SpecialityOthers;
                tbSpecialityOthers.Visible = true;
                chkSpecilaityOthers.Items[0].Selected = true;
            }
            if (providerProfile.PP_FacilityOthers != "")
            {
                txtFacilityOthers.Text = providerProfile.PP_FacilityOthers;
                tbFacilityOthers.Visible = true;
                chkFacilityOthers.Items[0].Selected = true;
            }
            hfProviderId.Value = providerProfile.PP_ProviderId.ToString();
        }
        catch (Exception ex)
        {
            CommonClass.LogError("SetProviderInfo", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
        }

    }

    private void SetProviderInfoSave(ProviderProfileSave providerProfile)
    {
        try
        {

            ViewState["ProviderStatus"] = providerProfile.PP_ProviderStatus;
            txtProviderName.Text = providerProfile.PP_ProviderName;
            txtLoginID.Text = providerProfile.PP_UserName;
            ddlCity.Text = providerProfile.PP_City;
            ddlCountry.Text = providerProfile.PP_Country;
            txtStreet.Text = providerProfile.PP_Street;
            txtPOBox.Text = providerProfile.PP_POBox;
            List<ProviderPractice> objProviderPracticeList = (List<ProviderPractice>)Session["ProviderPracticeList"];
            ProviderPractice objProviderPractice = objProviderPracticeList.Where(item => item.P_PracticeName == providerProfile.PP_Practice).SingleOrDefault();
            if (objProviderPractice == null)
            {
                ddlPractice.SelectedValue = "Others";
                trPracticeOthers.Visible = true;
                txtOtherPractice.Text = providerProfile.PP_Practice;
            }
            else
            {
                ddlPractice.SelectedValue = providerProfile.PP_Practice;
                trPracticeOthers.Visible = false;
            }
            txtDistrict.Text = providerProfile.PP_District;
            txtBuildingNo.Text = providerProfile.PP_BuildingNo;
            txtZipCode.Text = providerProfile.PP_ZIPCode;
            txtEmailAddress.Text = providerProfile.PP_EmailAddress;
            txtGPSLongitude.Text = providerProfile.PP_GPSLongitude;
            txtGPSLatitude.Text = providerProfile.PP_GPSLatitude;
            txtNICUBeds.Text = providerProfile.PP_NICUBeds.ToString();
            txtCCHILicenseNo.Text = providerProfile.PP_CCHILicenseNo;
            txtCCHILicenseExpirationDates.SelectedDate = providerProfile.PP_CCHILIcenseExpirationDate.ToString().Contains("01/01/0001") == false ? providerProfile.PP_CCHILIcenseExpirationDate : null;
            txtMOHLicenseNo.Text = providerProfile.PP_MOHLicenseNo;
            txtMOHLicenseED.SelectedDate = providerProfile.PP_MOHLIcenseNoExpirationDate.ToString().Contains("01/01/0001") == false ? providerProfile.PP_MOHLIcenseNoExpirationDate : null;
            txtCommRegistrationNo.Text = providerProfile.PP_CommRegistrationNo;
            txtCommRegistrationNoED.SelectedDate = providerProfile.PP_CommRegNoExpirationDate.ToString().Contains("01/01/0001") == false ? providerProfile.PP_CommRegNoExpirationDate : null;
            //txtDoctorName.Text = providerProfile.PP_DoctorName;
            //txtLicenseNo.Text = providerProfile.PP_LicenseNo;
            //txtSpeciality.Text = providerProfile.PP_Speciality;
            txtNumberOfDoctors.Text = providerProfile.PP_NumberOfDoctors.ToString();
            txtNumberOfRooms.Text = providerProfile.PP_NumberOfRooms.ToString();
            txtNumberOfBeds.Text = providerProfile.PP_NumberOfBeds.ToString();
            txtNumberOfNurses.Text = providerProfile.PP_NumberOfNurses.ToString();
            txtICUBeds.Text = providerProfile.PP_ICUBeds.ToString();
            txtContactPerson.Text = providerProfile.PP_ContactPerson;
            txtMobileNo.Text = providerProfile.PP_MobileNo.ToString();
            txtContactPersonDept.Text = providerProfile.PP_ContactPersonDept;
            txtContactPersonEmail.Text = providerProfile.PP_ContactPersonEmail;
            txtContactPersonPosition.Text = providerProfile.PP_ContactPersonPosition;
            txtFaxNumber.Text = providerProfile.PP_FaxNumber.ToString();
            txtFacebookAddress.Text = providerProfile.PP_FacebookAddress;
            txtBankName.Text = providerProfile.PP_BankName;
            txtBankPayeeName.Text = providerProfile.PP_BankPayeeName;
            txtIBANNo.Text = providerProfile.PP_IBANNo;
            ddlOwnPharmacy.SelectedIndex = ddlOwnPharmacy.Items.IndexOf(ddlOwnPharmacy.Items.FindByValue(providerProfile.PP_OwnPharmacy));
            if (ddlOwnPharmacy.SelectedValue == "No")
            {
                trPrahmacy.Visible = true;
            }
            else
            {
                trPrahmacy.Visible = false;

            }
            if (providerProfile.PP_Speciality != null)
            {
                string[] strFacility = providerProfile.PP_Speciality.Split(';');
                for (int m = 0; m < chkSpecilaity.Items.Count; m++)
                {
                    for (int str = 0; str < strFacility.Length; str++)
                    {
                        if (chkSpecilaity.Items[m].Value == strFacility[str])
                        {
                            chkSpecilaity.Items[m].Selected = true;
                            break;
                        }
                    }
                }
            }
            if (providerProfile.PP_Facilities != null)
            {
                string[] strFacility = providerProfile.PP_Facilities.Split(';');
                for (int m = 0; m < chkFacility.Items.Count; m++)
                {
                    for (int str = 0; str < strFacility.Length; str++)
                    {
                        if (chkFacility.Items[m].Value == strFacility[str])
                        {
                            chkFacility.Items[m].Selected = true;
                            break;
                        }
                    }
                }
            }
            if (providerProfile.PP_SpecialityOthers != "")
            {
                txtSpecialityOthers.Text = providerProfile.PP_SpecialityOthers;
                tbSpecialityOthers.Visible = true;
                chkSpecilaityOthers.Items[0].Selected = true;
            }
            if (providerProfile.PP_FacilityOthers != "")
            {
                txtFacilityOthers.Text = providerProfile.PP_FacilityOthers;
                tbFacilityOthers.Visible = true;
                chkFacilityOthers.Items[0].Selected = true;
            }
            hfProviderId.Value = providerProfile.PP_ProviderId.ToString();
        }
        catch (Exception ex)
        {
            CommonClass.LogError("SetProviderInfoSave", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
        }

    }


    private ProviderProfile GetProviderInfo(ProviderProfile objProviderProfile)
    {
        try
        {

            // hfProviderId.Value = objProviderProfile.PP_ProviderId.ToString();
            objProviderProfile.PP_ProviderName = txtProviderName.Text;
            objProviderProfile.PP_UserName = txtLoginID.Text;
            objProviderProfile.PP_Password = Guid.NewGuid().ToString().Substring(0, 8);
            objProviderProfile.PP_City = ddlCity.Text;
            objProviderProfile.PP_Country = ddlCountry.Text;
            objProviderProfile.PP_Street = txtStreet.Text;
            objProviderProfile.PP_POBox = txtPOBox.Text;
            if (ddlPractice.SelectedValue == "Others")
            {
                objProviderProfile.PP_Practice = txtOtherPractice.Text.Trim();
            }
            else
            {
                objProviderProfile.PP_Practice = ddlPractice.SelectedValue;
            }
            objProviderProfile.PP_District = txtDistrict.Text;
            objProviderProfile.PP_BuildingNo = txtBuildingNo.Text;
            objProviderProfile.PP_ZIPCode = txtZipCode.Text;
            if (ddlCountry.SelectedValue == "SA")
            {
                M_Cities objCity = ((List<M_Cities>)Session["CityList"]).Where(item => item.CityName == ddlCity.Text).FirstOrDefault();
                if (objCity != null)
                {
                    objProviderProfile.PP_Region = objCity.Region;
                }
            }
            else
            {
                objProviderProfile.PP_Region = "International";

            }
            objProviderProfile.PP_EmailAddress = txtEmailAddress.Text;
            objProviderProfile.PP_GPSLongitude = txtGPSLongitude.Text;
            objProviderProfile.PP_GPSLatitude = txtGPSLatitude.Text;
            objProviderProfile.PP_NICUBeds = int.Parse(txtNICUBeds.Text);
            objProviderProfile.PP_CCHILicenseNo = txtCCHILicenseNo.Text;
            objProviderProfile.PP_CCHILIcenseExpirationDate = Convert.ToDateTime(txtCCHILicenseExpirationDates.SelectedDate);
            objProviderProfile.PP_MOHLicenseNo = txtMOHLicenseNo.Text;
            objProviderProfile.PP_MOHLIcenseNoExpirationDate = Convert.ToDateTime(txtMOHLicenseED.SelectedDate);
            objProviderProfile.PP_CommRegistrationNo = txtCommRegistrationNo.Text;
            objProviderProfile.PP_CommRegNoExpirationDate = Convert.ToDateTime(txtCommRegistrationNoED.SelectedDate);
            //objProviderProfile.PP_DoctorName = txtDoctorName.Text;
            //objProviderProfile.PP_LicenseNo = txtLicenseNo.Text;
            //objProviderProfile.PP_Speciality = txtSpeciality.Text;
            objProviderProfile.PP_NumberOfDoctors = int.Parse(txtNumberOfDoctors.Text);
            objProviderProfile.PP_NumberOfRooms = int.Parse(txtNumberOfRooms.Text);
            objProviderProfile.PP_NumberOfBeds = int.Parse(txtNumberOfBeds.Text);
            objProviderProfile.PP_NumberOfNurses = int.Parse(txtNumberOfNurses.Text);
            objProviderProfile.PP_ICUBeds = int.Parse(txtICUBeds.Text);
            objProviderProfile.PP_ContactPerson = txtContactPerson.Text;
            objProviderProfile.PP_MobileNo = decimal.Parse(txtMobileNo.Text);
            objProviderProfile.PP_ContactPersonDept = txtContactPersonDept.Text;
            objProviderProfile.PP_ContactPersonEmail = txtContactPersonEmail.Text;
            objProviderProfile.PP_ContactPersonPosition = txtContactPersonPosition.Text;
            objProviderProfile.PP_FaxNumber = decimal.Parse(txtFaxNumber.Text);
            objProviderProfile.PP_FacebookAddress = txtFacebookAddress.Text;
            objProviderProfile.PP_BankName = txtBankName.Text;
            objProviderProfile.PP_BankPayeeName = txtBankPayeeName.Text;
            objProviderProfile.PP_IBANNo = txtIBANNo.Text;
            objProviderProfile.PP_OwnPharmacy = ddlOwnPharmacy.SelectedValue;
            objProviderProfile.PP_ActionBy = "";
            objProviderProfile.PP_ActionDate = DateTime.Now;
            objProviderProfile.PP_CreatedDate = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            for (int m = 0; m < chkSpecilaity.Items.Count; m++)
            {
                if (chkSpecilaity.Items[m].Selected == true)
                {
                    if (sb.Length == 0)
                        sb.Append(chkSpecilaity.Items[m].Value);
                    else
                        sb.Append(";" + chkSpecilaity.Items[m].Value);
                }
            }
            objProviderProfile.PP_Speciality = sb.ToString();
            sb = new StringBuilder();
            for (int m = 0; m < chkFacility.Items.Count; m++)
            {
                if (chkFacility.Items[m].Selected == true)
                {
                    if (sb.Length == 0)
                        sb.Append(chkFacility.Items[m].Value);
                    else
                        sb.Append(";" + chkFacility.Items[m].Value);
                }
            }
            objProviderProfile.PP_Facilities = sb.ToString();
            objProviderProfile.PP_SpecialityOthers = txtSpecialityOthers.Text.Trim();
            objProviderProfile.PP_FacilityOthers = txtFacilityOthers.Text.Trim();
            // objProviderProfile.PP_ProviderStatus = ViewState["ProviderStatus"].ToString();
            using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
            {
                ProviderProfileSave objProviderProfileSave = objOnlineProvidersEntities.ProviderProfileSaves.Where(item => item.PP_UserName == txtLoginID.Text.Trim()).SingleOrDefault();
                if (objProviderProfileSave == null)
                {
                    objProviderProfile.PP_ProviderId = hfProviderId.Value != "" ? Convert.ToInt32(hfProviderId.Value) : 0;
                }
            }
            return objProviderProfile;
        }
        catch (Exception ex)
        {

            CommonClass.LogError("GetProviderInfo", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
            return objProviderProfile;
        }
    }

    private ProviderProfileSave GetProviderInfoSave(ProviderProfileSave objProviderProfile)
    {
        try
        {

            // hfProviderId.Value = objProviderProfile.PP_ProviderId.ToString();
            objProviderProfile.PP_ProviderName = txtProviderName.Text;
            objProviderProfile.PP_UserName = txtLoginID.Text;
            objProviderProfile.PP_Password = Guid.NewGuid().ToString().Substring(0, 8);
            objProviderProfile.PP_City = ddlCity.Text;
            objProviderProfile.PP_Country = ddlCountry.Text;
            objProviderProfile.PP_Street = txtStreet.Text;
            objProviderProfile.PP_POBox = txtPOBox.Text;
            if (ddlPractice.SelectedValue == "Others")
            {
                objProviderProfile.PP_Practice = txtOtherPractice.Text.Trim();
            }
            else
            {
                objProviderProfile.PP_Practice = ddlPractice.SelectedValue;
            }
            objProviderProfile.PP_District = txtDistrict.Text;
            objProviderProfile.PP_BuildingNo = txtBuildingNo.Text;
            objProviderProfile.PP_ZIPCode = txtZipCode.Text;
            if (ddlCountry.SelectedValue == "SA")
            {
                M_Cities objCity = ((List<M_Cities>)Session["CityList"]).Where(item => item.CityName == ddlCity.Text).FirstOrDefault();
                if (objCity != null)
                {
                    objProviderProfile.PP_Region = objCity.Region;
                }
            }
            else
            {
                objProviderProfile.PP_Region = "International";

            }
            objProviderProfile.PP_EmailAddress = txtEmailAddress.Text;
            objProviderProfile.PP_GPSLongitude = txtGPSLongitude.Text;
            objProviderProfile.PP_GPSLatitude = txtGPSLatitude.Text;
            objProviderProfile.PP_NICUBeds = txtNICUBeds.Text != "" ? int.Parse(txtNICUBeds.Text) : 0;
            objProviderProfile.PP_CCHILicenseNo = txtCCHILicenseNo.Text;
            objProviderProfile.PP_CCHILIcenseExpirationDate = Convert.ToDateTime(txtCCHILicenseExpirationDates.SelectedDate);
            objProviderProfile.PP_MOHLicenseNo = txtMOHLicenseNo.Text;
            objProviderProfile.PP_MOHLIcenseNoExpirationDate = Convert.ToDateTime(txtMOHLicenseED.SelectedDate);
            objProviderProfile.PP_CommRegistrationNo = txtCommRegistrationNo.Text;
            objProviderProfile.PP_CommRegNoExpirationDate = Convert.ToDateTime(txtCommRegistrationNoED.SelectedDate);
            //objProviderProfile.PP_DoctorName = txtDoctorName.Text;
            //objProviderProfile.PP_LicenseNo = txtLicenseNo.Text;
            //objProviderProfile.PP_Speciality = txtSpeciality.Text;
            objProviderProfile.PP_NumberOfDoctors = txtNumberOfDoctors.Text != "" ? int.Parse(txtNumberOfDoctors.Text) : 0;
            objProviderProfile.PP_NumberOfRooms = txtNumberOfRooms.Text != "" ? int.Parse(txtNumberOfRooms.Text) : 0;
            objProviderProfile.PP_NumberOfBeds = txtNumberOfBeds.Text != "" ? int.Parse(txtNumberOfBeds.Text) : 0;
            objProviderProfile.PP_NumberOfNurses = txtNumberOfNurses.Text != "" ? int.Parse(txtNumberOfNurses.Text) : 0;
            objProviderProfile.PP_ICUBeds = txtICUBeds.Text != "" ? int.Parse(txtICUBeds.Text) : 0;
            objProviderProfile.PP_ContactPerson = txtContactPerson.Text;
            objProviderProfile.PP_MobileNo = txtMobileNo.Text != "" ? decimal.Parse(txtMobileNo.Text) : 0;
            objProviderProfile.PP_ContactPersonDept = txtContactPersonDept.Text;
            objProviderProfile.PP_ContactPersonEmail = txtContactPersonEmail.Text;
            objProviderProfile.PP_ContactPersonPosition = txtContactPersonPosition.Text;
            objProviderProfile.PP_FaxNumber = txtFaxNumber.Text != "" ? decimal.Parse(txtFaxNumber.Text) : 0;
            objProviderProfile.PP_FacebookAddress = txtFacebookAddress.Text;
            objProviderProfile.PP_BankName = txtBankName.Text;
            objProviderProfile.PP_BankPayeeName = txtBankPayeeName.Text;
            objProviderProfile.PP_IBANNo = txtIBANNo.Text;
            objProviderProfile.PP_OwnPharmacy = ddlOwnPharmacy.SelectedValue;
            objProviderProfile.PP_ActionBy = "";
            objProviderProfile.PP_ActionDate = DateTime.Now;
            objProviderProfile.PP_CreatedDate = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            for (int m = 0; m < chkSpecilaity.Items.Count; m++)
            {
                if (chkSpecilaity.Items[m].Selected == true)
                {
                    if (sb.Length == 0)
                        sb.Append(chkSpecilaity.Items[m].Value);
                    else
                        sb.Append(";" + chkSpecilaity.Items[m].Value);
                }
            }
            objProviderProfile.PP_Speciality = sb.ToString();
            sb = new StringBuilder();
            for (int m = 0; m < chkFacility.Items.Count; m++)
            {
                if (chkFacility.Items[m].Selected == true)
                {
                    if (sb.Length == 0)
                        sb.Append(chkFacility.Items[m].Value);
                    else
                        sb.Append(";" + chkFacility.Items[m].Value);
                }
            }
            objProviderProfile.PP_Facilities = sb.ToString();
            objProviderProfile.PP_SpecialityOthers = txtSpecialityOthers.Text.Trim();
            objProviderProfile.PP_FacilityOthers = txtFacilityOthers.Text.Trim();
            // objProviderProfile.PP_ProviderStatus = ViewState["ProviderStatus"].ToString();
            objProviderProfile.PP_ProviderId = hfProviderId.Value != "" ? Convert.ToInt32(hfProviderId.Value) : 0;
            return objProviderProfile;
        }
        catch (Exception ex)
        {

            CommonClass.LogError("GetProviderInfo", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
            return objProviderProfile;
        }
    }

    protected void btnUploadAttachments_Click(object sender, EventArgs e)
    {
        try
        {


            if (Page.IsValid == true && AsyncUpload1.UploadedFiles.Count > 0)
            {
                string UrlPath = ConfigurationManager.AppSettings["OnlineContractingDOcs"];// HttpContext.Current.Request.Url.ToString().Substring(0, HttpContext.Current.Request.Url.ToString().IndexOf(HttpContext.Current.Request.RawUrl.ToString()));


                //if (Session["AttachmentsTable"] == null)
                //{
                //    AttachmentsTable();
                //}

                //DataTable dtAttachments = (DataTable)Session["AttachmentsTable"];
                //int minAccountLevel = int.MaxValue;
                //int maxAccountLevel = int.MinValue;
                //foreach (DataRow dr1 in dtAttachments.Rows)
                //{
                //    int accountLevel = Convert.ToInt32(dr1["PD_DocumentId"]);
                //    minAccountLevel = Math.Min(minAccountLevel, accountLevel);
                //    maxAccountLevel = Math.Max(maxAccountLevel, accountLevel);
                //}
                //DataRow dr = dtAttachments.NewRow();
                //dr["PD_DocumentId"] = maxAccountLevel + 1;
                //dr["PD_DocumentType"] = ddlDocumentType.SelectedValue;
                //dr["PD_DocumentName"] = AsyncUpload1.UploadedFiles[0].FileName;
                //string ProviderName = txtProviderName.Text.Trim().Replace(" ", "");
                //string FilePath = AppDomain.CurrentDomain.BaseDirectory + "ProviderDocs\\" + ProviderName + AsyncUpload1.UploadedFiles[0].FileName;

                //AsyncUpload1.UploadedFiles[0].SaveAs(FilePath);
                //UrlPath = UrlPath + "\\ProviderDocs\\" + ProviderName + AsyncUpload1.UploadedFiles[0].FileName;
                //dr["PD_DocumentPath"] = UrlPath;
                //dr["PD_DocumentPathLocation"] = "\\ProviderDocs\\" + ProviderName + AsyncUpload1.UploadedFiles[0].FileName;
                //dtAttachments.Rows.Add(dr);
                List<ProviderDocument> objProviderDocumentList;
                if (Session["AttachmentsTable"] != null)
                {
                    objProviderDocumentList = (List<ProviderDocument>)Session["AttachmentsTable"];
                }
                else
                {
                    objProviderDocumentList = new List<ProviderDocument>();
                }
                ProviderDocument objProviderDocument = new ProviderDocument();
                objProviderDocument.PD_DocumentType = ddlDocumentType.SelectedValue;
                objProviderDocument.PD_DocumentName = AsyncUpload1.UploadedFiles[0].FileName;
                string ProviderName = txtProviderName.Text.Trim().Replace(" ", "");
                // string FilePath = AppDomain.CurrentDomain.BaseDirectory + "ProviderDocs\\" + ProviderName + AsyncUpload1.UploadedFiles[0].FileName;

                //AsyncUpload1.UploadedFiles[0].SaveAs(FilePath);
                string FileNameServer = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + objProviderDocument.PD_DocumentName;
                string ServerFolderPath = ConfigurationManager.AppSettings["OnlineContractingDOcsFolderPath"].ToString();
                string FolderName = "";
                if (hfProviderId.Value == "")
                {
                    FolderName = txtLoginID.Text.Trim();
                }
                else
                {
                    FolderName = hfProviderId.Value + "S";

                }
                using (NetworkShareAccesser.Access("bjfs05", "Bupa_Mid_East", "AbcpdfUat", "DdfDdf890*()"))
                {


                    bool exists = System.IO.Directory.Exists(ServerFolderPath + FolderName);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(ServerFolderPath + FolderName);

                    AsyncUpload1.UploadedFiles[0].SaveAs(ServerFolderPath + FolderName + "/" + FileNameServer);
                    // File.Copy(FilePath, @"\\bjfs05\POD\" +  objProviderDocument.PD_DocumentName);

                }
                //UrlPath = UrlPath + "\\ProviderDocs\\" + ProviderName + AsyncUpload1.UploadedFiles[0].FileName;
                UrlPath = UrlPath + FolderName + "/" + FileNameServer;
                objProviderDocument.PD_DocumentPath = UrlPath;
                //objProviderDocument.PD_DocumentPath = objProviderDocument.PD_DocumentName;
                objProviderDocument.PD_ActionBy = txtLoginID.Text.Trim();
                objProviderDocument.PD_ActionDate = DateTime.Now;
                objProviderDocumentList.Add(objProviderDocument);
                Session["AttachmentsTable"] = objProviderDocumentList;


                // Session["AttachmentsTable"] = dtAttachments;
                rgAttachments.Rebind();
                ddlDocumentType.SelectedIndex = -1;
                trDownloadTemplate.Visible = false;
                hrefPriceList.Visible = false;
                hrefDoctorDetails.Visible = false;
            }
        }
        catch (Exception ex)
        {
            CommonClass.LogError("btnUploadAttachments", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
            lblMessage.Text = "We are unable to process your request.Please try adding the file again.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    private void AttachmentsTable()
    {

        DataTable Dt = new DataTable();
        Dt.Columns.Add("PD_DocumentId");
        Dt.Columns.Add("PD_DocumentType");
        Dt.Columns.Add("PD_DocumentName");
        Dt.Columns.Add("PD_DocumentPath");
        Dt.Columns.Add("PD_DocumentPathLocation");
        Session["AttachmentsTable"] = Dt;
    }
    public void RadAsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
    {
        const int MaxTotalBytes = 1048576; // 1 MB
        long totalBytes = 0;

        var liItem = new HtmlGenericControl("li");
        liItem.InnerText = e.File.FileName;


        if (totalBytes < MaxTotalBytes)
        {
            // Total bytes limit has not been reached, accept the file
            e.IsValid = true;
            totalBytes += e.File.ContentLength;
        }
        else
        {
            // Limit reached, discard the file
            e.IsValid = false;
        }

        //if (e.IsValid)
        //{

        //    ValidFiles.Visible = true;
        //    ValidFilesList.Controls.AddAt(0, liItem);

        //}
        //else
        //{

        //    InvalidFiles.Visible = true;
        //    InValidFilesList.Controls.AddAt(0, liItem);
        //}
    }

    protected void rgAttachments_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "DeleteAtt")
        {
            List<ProviderDocument> objProviderDocumentList = (List<ProviderDocument>)Session["AttachmentsTable"];
            ProviderDocument objProviderDocument = objProviderDocumentList[e.Item.ItemIndex];
            if (objProviderDocument.PD_DocumentId > 0)
            {
                using (OnlineServicesEntities objOnlineServicesEntities = new OnlineServicesEntities())
                {

                    objOnlineServicesEntities.ProviderDocuments.Add(objProviderDocument);
                    objOnlineServicesEntities.Entry(objProviderDocument).State = System.Data.Entity.EntityState.Deleted;
                    int responsestatus = objOnlineServicesEntities.SaveChanges();
                }
            }
            objProviderDocumentList.RemoveAt(e.Item.ItemIndex);
            Session["AttachmentsTable"] = objProviderDocumentList;
            rgAttachments.Rebind();
            //DataTable dtAttachments = (DataTable)Session["AttachmentsTable"];

            //GridDataItem item = e.Item as GridDataItem;
            //Label lblDocumentId = (Label)item.FindControl("lblDocumentId");
            //DataRow[] drr = dtAttachments.Select("DocumentId='" + lblDocumentId.Text + "' ");
            //for (int i = 0; i < drr.Length; i++)
            //{
            //    drr[i].Delete();
            //}
            //dtAttachments.AcceptChanges();
            //Session["AttachmentsTable"] = dtAttachments;
            //rgAttachments.Rebind();           
        }
    }
    protected void rgAttachments_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (Session["AttachmentsTable"] != null)
        {
            rgAttachments.DataSource = Session["AttachmentsTable"];
        }
        else
        {
            rgAttachments.DataSource = string.Empty;
        }
    }

    private void UpdateProviderContacts(long ProviderId, bool status)
    {

        if (rgAdditionalContacts.Items.Count > 0)
        {
            List<ProviderContact> objProviderContactList = new List<ProviderContact>();
            for (int i = 0; i < rgAdditionalContacts.Items.Count; i++)
            {
                TextBox txtPersonName = (TextBox)rgAdditionalContacts.Items[i].FindControl("txtPersonName");
                TextBox txtPersonDepartment = (TextBox)rgAdditionalContacts.Items[i].FindControl("txtPersonDepartment");
                TextBox txtPersonPosition = (TextBox)rgAdditionalContacts.Items[i].FindControl("txtPersonPosition");
                TextBox txtContactNumber = (TextBox)rgAdditionalContacts.Items[i].FindControl("txtContactNumber");
                TextBox txtConactEmailAddress = (TextBox)rgAdditionalContacts.Items[i].FindControl("txtConactEmailAddress");
                Label lblAdditionalContactId = (Label)rgAdditionalContacts.Items[i].FindControl("lblAdditionalContactId");
                ProviderContact objProviderContact = new ProviderContact();
                objProviderContact.PC_ProviderId = ProviderId;
                objProviderContact.PC_PersonName = txtPersonName.Text;
                objProviderContact.PC_PersonDepartment = txtPersonDepartment.Text;
                objProviderContact.PC_PersonPosition = txtPersonPosition.Text;
                objProviderContact.PC_ContactNumber = txtContactNumber.Text;
                objProviderContact.PC_EmailAddress = txtConactEmailAddress.Text;
                objProviderContact.PC_Status = status;
                objProviderContact.PC_ActionBy = txtLoginID.Text;
                objProviderContact.PC_ActionDate = DateTime.Now;
                objProviderContact.PC_ContactId = lblAdditionalContactId.Text != "" ? Convert.ToInt32(lblAdditionalContactId.Text) : 0;
                if (txtPersonName.Text != "" && txtPersonDepartment.Text != "" && txtPersonPosition.Text != "")
                {
                    objProviderContactList.Add(objProviderContact);
                }
            }
            Session["ContactList"] = objProviderContactList;
        }
    }

    protected void rgAdditionalContacts_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (Session["ContactList"] != null)
            rgAdditionalContacts.DataSource = Session["ContactList"];
        else
        {
            //List<ProviderContact> objProviderContactList = new List<ProviderContact>();
            //for (int i = 0; i < 5; i++)
            //{
            //    ProviderContact objProviderContact = new ProviderContact();
            //    objProviderContact.PC_PersonName = "";
            //    objProviderContact.PC_PersonDepartment = "";
            //    objProviderContact.PC_PersonPosition = "";
            //    objProviderContact.PC_ContactNumber = "";
            //    objProviderContact.PC_EmailAddress = "";
            //    objProviderContactList.Add(objProviderContact);

            //}
            //Session["ContactList"] = objProviderContactList;
            rgAdditionalContacts.DataSource = string.Empty;
        }
    }
    protected void rgAdditionalContacts_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "AddNewContact")
        {
            try
            {


                //pnlPharmacy.Visible = true;
                UpdateProviderContacts(0, false);
                List<ProviderContact> objProviderContactList;
                if (Session["ContactList"] != null)
                {
                    objProviderContactList = (List<ProviderContact>)Session["ContactList"];
                }
                else
                {
                    objProviderContactList = new List<ProviderContact>();
                }
                ProviderContact objProviderContact = new ProviderContact();
                objProviderContact.PC_PersonName = "";
                objProviderContact.PC_PersonDepartment = "";
                objProviderContact.PC_PersonPosition = "";
                objProviderContact.PC_ContactNumber = "";
                objProviderContact.PC_EmailAddress = "";
                objProviderContactList.Add(objProviderContact);
                Session["ContactList"] = objProviderContactList;

                rgAdditionalContacts.Rebind();
            }
            catch (Exception ex)
            {


            }
        }
        else if (e.CommandName == "DeleteNewContact")
        {
            try
            {
                // e.Item.ItemIndex;
                UpdateProviderContacts(0, false);
                List<ProviderContact> objProviderContactList = (List<ProviderContact>)Session["ContactList"];
                ProviderContact objProviderContact = objProviderContactList[e.Item.ItemIndex];
                if (objProviderContact.PC_ContactId > 0)
                {
                    using (OnlineServicesEntities objOnlineServicesEntities = new OnlineServicesEntities())
                    {
                        objOnlineServicesEntities.ProviderContacts.Add(objProviderContact);
                        objOnlineServicesEntities.Entry(objProviderContact).State = System.Data.Entity.EntityState.Deleted;
                        int responsestatus = objOnlineServicesEntities.SaveChanges();
                    }
                }
                objProviderContactList.RemoveAt(e.Item.ItemIndex);
                Session["ContactList"] = objProviderContactList;
                rgAdditionalContacts.Rebind();
            }
            catch (Exception ex)
            {
                rgAdditionalContacts.Rebind();

            }
        }
    }



    protected void rgPharmacy_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "AddPharmacy")
        {

            UpdateProviderPharmacy(0, false);
            List<ProviderPharmacy> objProviderPharmacyList;
            if (Session["PharmacyList"] != null)
            {
                objProviderPharmacyList = (List<ProviderPharmacy>)Session["PharmacyList"];
            }
            else
            {
                objProviderPharmacyList = new List<ProviderPharmacy>();
            }
            ProviderPharmacy objProviderPharmacy = new ProviderPharmacy();
            objProviderPharmacy.PharmacyName = "";
            objProviderPharmacy.PharmacyCCHILinceseNo = "";
            objProviderPharmacy.PharmacyCCHILicenseExpiryDate = null;
            objProviderPharmacyList.Add(objProviderPharmacy);
            Session["PharmacyList"] = objProviderPharmacyList;
            rgPharmacy.Rebind();
        }
        else if (e.CommandName == "DeletePharmacy")
        {
            // e.Item.ItemIndex;
            UpdateProviderPharmacy(0, false);
            List<ProviderPharmacy> objProviderPharmacyList = (List<ProviderPharmacy>)Session["PharmacyList"];

            ProviderPharmacy objProviderPharmacy = objProviderPharmacyList[e.Item.ItemIndex];
            if (objProviderPharmacy.ProviderPharmacyId > 0)
            {
                using (OnlineServicesEntities objOnlineServicesEntities = new OnlineServicesEntities())
                {

                    objOnlineServicesEntities.ProviderPharmacies.Add(objProviderPharmacy);
                    objOnlineServicesEntities.Entry(objProviderPharmacy).State = System.Data.Entity.EntityState.Deleted;
                    int responsestatus = objOnlineServicesEntities.SaveChanges();
                }
            }
            objProviderPharmacyList.RemoveAt(e.Item.ItemIndex);
            Session["PharmacyList"] = objProviderPharmacyList;
            rgPharmacy.Rebind();

        }
    }
    protected void ddlOwnPharmacy_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlOwnPharmacy.SelectedValue == "No")
        {
            trPrahmacy.Visible = true;
            rgPharmacy.Rebind();
        }
        else
        {
            trPrahmacy.Visible = false;

        }
    }
    private void UpdateProviderPharmacy(long ProviderId, bool status)
    {

        if (rgPharmacy.Items.Count > 0)
        {
            List<ProviderPharmacy> objProviderPharmacyList = new List<ProviderPharmacy>();
            for (int i = 0; i < rgPharmacy.Items.Count; i++)
            {
                TextBox txtPharmacyName = (TextBox)rgPharmacy.Items[i].FindControl("txtPharmacyName");
                TextBox txtCCHILicenseNo = (TextBox)rgPharmacy.Items[i].FindControl("txtCCHILicenseNo");
                // TextBox txtExpirationDate = (TextBox)rgPharmacy.Items[i].FindControl("txtExpirationDate");
                RadDatePicker RadDatePicker2 = (RadDatePicker)rgPharmacy.Items[i].FindControl("RadDatePicker2");
                Label lblPharmacyId = (Label)rgPharmacy.Items[i].FindControl("lblPharmacyId");
                ProviderPharmacy objProviderPharmacy = new ProviderPharmacy();
                objProviderPharmacy.PharmacyName = txtPharmacyName.Text;
                objProviderPharmacy.PharmacyCCHILinceseNo = txtCCHILicenseNo.Text;
                objProviderPharmacy.ProviderID = ProviderId;
                objProviderPharmacy.Status = status;
                objProviderPharmacy.ActionBy = txtLoginID.Text;
                objProviderPharmacy.ActionDate = DateTime.Now;
                objProviderPharmacy.ProviderPharmacyId = lblPharmacyId.Text != "" ? Convert.ToInt32(lblPharmacyId.Text) : 0;
                if (RadDatePicker2.SelectedDate.HasValue)
                {
                    objProviderPharmacy.PharmacyCCHILicenseExpiryDate = Convert.ToDateTime(RadDatePicker2.SelectedDate);
                }

                objProviderPharmacyList.Add(objProviderPharmacy);
            }
            Session["PharmacyList"] = objProviderPharmacyList;
        }
    }
    private void UpdateProviderDocuments(long ProviderId, bool status)
    {

        if (rgPharmacy.Items.Count > 0)
        {
            List<ProviderPharmacy> objProviderPharmacyList = new List<ProviderPharmacy>();
            for (int i = 0; i < rgPharmacy.Items.Count; i++)
            {
                TextBox txtPharmacyName = (TextBox)rgPharmacy.Items[i].FindControl("txtPharmacyName");
                TextBox txtCCHILicenseNo = (TextBox)rgPharmacy.Items[i].FindControl("txtCCHILicenseNo");
                // TextBox txtExpirationDate = (TextBox)rgPharmacy.Items[i].FindControl("txtExpirationDate");
                RadDatePicker RadDatePicker2 = (RadDatePicker)rgPharmacy.Items[i].FindControl("RadDatePicker2");
                Label lblPharmacyId = (Label)rgPharmacy.Items[i].FindControl("lblPharmacyId");
                ProviderPharmacy objProviderPharmacy = new ProviderPharmacy();
                objProviderPharmacy.PharmacyName = txtPharmacyName.Text;
                objProviderPharmacy.PharmacyCCHILinceseNo = txtCCHILicenseNo.Text;
                objProviderPharmacy.ProviderID = ProviderId;
                objProviderPharmacy.Status = status;
                objProviderPharmacy.ActionBy = txtLoginID.Text;
                objProviderPharmacy.ActionDate = DateTime.Now;
                objProviderPharmacy.ProviderPharmacyId = lblPharmacyId.Text != "" ? Convert.ToInt32(lblPharmacyId.Text) : 0;
                if (RadDatePicker2.SelectedDate.HasValue)
                {
                    objProviderPharmacy.PharmacyCCHILicenseExpiryDate = Convert.ToDateTime(RadDatePicker2.SelectedDate);
                }

                objProviderPharmacyList.Add(objProviderPharmacy);
            }
            Session["PharmacyList"] = objProviderPharmacyList;
        }
    }

    protected void rgPharmacy_NeedDataSource1(object sender, GridNeedDataSourceEventArgs e)
    {
        if (Session["PharmacyList"] != null)
        {
            //DataTable dt = CommonClass.ToDataTable<ProviderPharmacy>(((List<ProviderPharmacy>)Session["PharmacyList"]));
            //dt.TableName = "ProviderPharmacy";
            rgPharmacy.DataSource = Session["PharmacyList"];
        }
        else
            rgPharmacy.DataSource = string.Empty;
    }


    protected void ddlCountry_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

        if (ddlCountry.SelectedValue == "")
        {
            ddlCountry.Text = "";
            ddlCity.Text = "";
            ddlCity.DataSource = null;
            ddlCity.DataBind();
            return;
        }
        try
        {
            if (ddlCountry.SelectedValue != "SA")
            {
                ddlCity.AllowCustomText = true;
                ddlCity.MarkFirstMatch = false;
                tddistrict1.Visible = true;
                tddistrict2.Visible = true;
                spancchi.Visible = false;
                spancchiDate.Visible = false;
                spanCRNo.Visible = false;
                spanCRDate.Visible = false;
                spanMOH.Visible = false;
                spanMOHDate.Visible = false;
                rfvCCHI.Visible = false;
                rfvCCHIDate.Visible = false;
                rfvMOHDate.Visible = false;
                rfvMOH.Visible = false;
                rfvCRDate.Visible = false;
                rfvCRNo.Visible = false;
                if (txtMobileNo.Text != "")
                    txtMobileNo.Text = "";
                if (txtFaxNumber.Text != "")
                    txtFaxNumber.Text = "";
            }
            else
            {
                ddlCity.AllowCustomText = false;
                ddlCity.MarkFirstMatch = true;
                tddistrict1.Visible = false;
                tddistrict2.Visible = false;
                if (txtMobileNo.Text == "")
                    txtMobileNo.Text = "966";
                if (txtFaxNumber.Text == "")
                    txtFaxNumber.Text = "966";
                spancchi.Visible = true;
                spancchiDate.Visible = true;
                spanCRNo.Visible = true;
                spanCRDate.Visible = true;
                spanMOH.Visible = true;
                spanMOHDate.Visible = true;
                rfvCCHI.Visible = true;
                rfvCCHIDate.Visible = true;
                rfvMOHDate.Visible = true;
                rfvMOH.Visible = true;
                rfvCRDate.Visible = true;
                rfvCRNo.Visible = true;

            }
            ddlCity.Text = "";

            ddlCity.DataSource = ((List<M_Cities>)Session["CityList"]).Where(item => item.CountryCode == ddlCountry.SelectedValue).ToList();
            ddlCity.DataTextField = "cityname";
            ddlCity.DataValueField = "cityname";
            ddlCity.DataBind();
            txtLoginID.Focus();
        }
        catch (Exception)
        {


        }
    }
    protected void ddlCity_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            if (ddlCountry.SelectedValue == "SA")
            {
                M_Cities objCity = ((List<M_Cities>)Session["CityList"]).Where(item => item.CityName == ddlCity.Text).FirstOrDefault();
                if (objCity != null)
                {
                    txtDistrict.Text = objCity.District;
                }
            }
            else
            {
                txtDistrict.Text = "";

            }
            txtStreet.Focus();

        }
        catch (Exception)
        {


        }
    }

    //private bool CheckForDuplicateProviderLogin(string LoginID)
    //{
    //    using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
    //    {
    //        ProviderProfileSave objProviderProfileSave = objOnlineProvidersEntities.ProviderProfileSaves.Where(item=>item.PP_UserName==txtLoginID.Text.Trim()).SingleOrDefault();
    //        ProviderProfile objProviderProfile = objOnlineProvidersEntities.ProviderProfiles.Where(item => item.PP_UserName == txtLoginID.Text.Trim()).SingleOrDefault();

    //    }
    //}

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid == true && txtLoginID.Text.Trim() != "" && txtProviderName.Text.Trim() != "")
        {

            String MasterNotificationStatusCheck = "Submit";
            strUserRole = "Provider";
            try
            {
                using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
                {
                    ProviderProfile objProviderProfile = objOnlineProvidersEntities.ProviderProfiles.Where(item => item.PP_UserName == txtLoginID.Text.Trim()).SingleOrDefault();

                    if (objProviderProfile != null)
                    {
                        lblMessage.Text = "The Preferred Login ID entered is already used.Please try with different one. ";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }

                    ProviderProfileSave objProviderProfileSave = objOnlineProvidersEntities.ProviderProfileSaves.Where(item => item.PP_UserName == txtLoginID.Text.Trim()).SingleOrDefault();
                    if (objProviderProfileSave != null)
                    {
                        if (objProviderProfileSave.PP_ProviderName != txtProviderName.Text.Trim())
                        {
                            lblMessage.Text = "The Preferred Login ID entered is already used.Please try with different one. ";
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }

                }
                using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
                {

                    using (var dbContextTransaction = objOnlineProvidersEntities.Database.BeginTransaction())
                    {
                        //Based on the Notification data get the next action plan from defined flow
                        int status = 0;
                        List<M_NotificationMaster> objM_NotificationMasterList = objOnlineProvidersEntities.M_NotificationMaster.ToList().Where(item => item.NM_ModuleName == strModule && item.NM_Role == strUserRole && item.NM_StageID == Stage && item.NM_Action == MasterNotificationStatusCheck).ToList();
                        if (objM_NotificationMasterList.Count > 0)
                        {
                            try
                            {
                                ProviderProfileSave objProviderProfile = new ProviderProfileSave();
                                objProviderProfile.PP_ProviderStatus = ProviderStatus.Saved_As_Draft.ToString();

                                objOnlineProvidersEntities.ProviderProfileSaves.Add(GetProviderInfoSave(objProviderProfile));
                                if (hfProviderId.Value != "")
                                {
                                    objOnlineProvidersEntities.Entry(objProviderProfile).State = System.Data.Entity.EntityState.Modified;
                                }
                                status = objOnlineProvidersEntities.SaveChanges();
                                long RequestId = objProviderProfile.PP_ProviderId;




                                #region Additional Contacts
                                UpdateProviderContacts(RequestId, false);
                                if (Session["ContactList"] != null)
                                {
                                    List<ProviderContact> objProviderContractList = (List<ProviderContact>)Session["ContactList"];
                                    foreach (ProviderContact objProviderContact in objProviderContractList)
                                    {
                                        objOnlineProvidersEntities.ProviderContacts.Add(objProviderContact);
                                        if (objProviderContact.PC_ContactId > 0)
                                        {
                                            objOnlineProvidersEntities.Entry(objProviderContact).State = System.Data.Entity.EntityState.Modified;
                                        }
                                        status = objOnlineProvidersEntities.SaveChanges();
                                    }

                                }
                                #endregion

                                #region Pharmacy

                                UpdateProviderPharmacy(RequestId, false);
                                if (Session["PharmacyList"] != null)
                                {
                                    List<ProviderPharmacy> objProviderPharmacylist = (List<ProviderPharmacy>)Session["PharmacyList"];
                                    if (objProviderPharmacylist.Count > 0)
                                    {
                                        foreach (ProviderPharmacy objProviderPharmacy in objProviderPharmacylist)
                                        {
                                            objOnlineProvidersEntities.ProviderPharmacies.Add(objProviderPharmacy);
                                            if (objProviderPharmacy.ProviderPharmacyId > 0)
                                            {
                                                objOnlineProvidersEntities.Entry(objProviderPharmacy).State = System.Data.Entity.EntityState.Modified;

                                            }

                                            status = objOnlineProvidersEntities.SaveChanges();
                                        }

                                    }
                                }

                                #endregion

                                #region Attachements
                                if (Session["AttachmentsTable"] != null)
                                {
                                    string FolderName = RequestId.ToString() + "S";
                                    if (hfProviderId.Value == "")
                                    {
                                        using (NetworkShareAccesser.Access("bjfs05", "Bupa_Mid_East", "AbcpdfUat", "DdfDdf890*()"))
                                        {
                                            string ServerFolderPath = ConfigurationManager.AppSettings["OnlineContractingDOcsFolderPath"].ToString();
                                            bool exists = System.IO.Directory.Exists(ServerFolderPath + FolderName);

                                            if (!exists)
                                            {
                                                Directory.Move(ServerFolderPath + txtLoginID.Text.Trim(), ServerFolderPath + FolderName);
                                            }

                                        }
                                    }

                                    List<ProviderDocument> objProviderDocumentlist = (List<ProviderDocument>)Session["AttachmentsTable"];
                                    if (objProviderDocumentlist.Count > 0)
                                    {

                                        foreach (ProviderDocument objProviderDocument in objProviderDocumentlist)
                                        {
                                            objProviderDocument.PD_ProviderId = RequestId;
                                            //string Newpath = ConfigurationManager.AppSettings["OnlineContractingDOcs"].Length;
                                            objProviderDocument.PD_DocumentPath = objProviderDocument.PD_DocumentPath.Substring(ConfigurationManager.AppSettings["OnlineContractingDOcs"].Length);
                                            if (hfProviderId.Value == "")
                                            {
                                                objProviderDocument.PD_DocumentPath = objProviderDocument.PD_DocumentPath.Replace(txtLoginID.Text.Trim(), FolderName);
                                            }
                                            objOnlineProvidersEntities.ProviderDocuments.Add(objProviderDocument);
                                            if (objProviderDocument.PD_DocumentId > 0)
                                            {
                                                objOnlineProvidersEntities.Entry(objProviderDocument).State = System.Data.Entity.EntityState.Modified;

                                            }
                                            status = objOnlineProvidersEntities.SaveChanges();
                                        }
                                    }
                                }
                                #endregion

                                dbContextTransaction.Commit();
                                btnSearch_Click1(sender, null);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                CommonClass.LogError("btnSave3", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
                                lblMessage.Text = "We are unable to process your request at the moment.Please try later.";
                                lblMessage.ForeColor = System.Drawing.Color.Red;
                                return;
                            }
                        }
                        else
                        {
                            //thow error to user ,unable to process your request.
                            return;
                        }

                    }

                    //ClearFields();
                    lblMessage.Text = "Data has been saved successfully.";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    //RadWindowManager1.RadAlert("Your request has been processed.<br />Provider Relations Officer will be contacting you soon.<br />For further Communication please use the Login Credentials sent to your Registered E-mail", 330, 180, "Validation Message", "");
                }
            }
            catch (Exception ex)
            {

                CommonClass.LogError("btnSave", ex.Message, ex.InnerException, ex.StackTrace, txtLoginID.Text);
                lblMessage.Text = "We are unable to process your request at the moment.Please try later.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }

        }
        else
        {
            //display error message
            RadWindowManager1.RadAlert("Please enter Provider Name and Preferred Login ID.", 330, 180, "Validation Message", "");
        }
    }

    protected void btnSearch_Click1(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        if (Page.IsValid == true && txtLoginID.Text.Trim() != "" && txtProviderName.Text.Trim() != "")
        {
            using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
            {
                ProviderProfile objProviderProfile = objOnlineProvidersEntities.ProviderProfiles.Where(item => item.PP_UserName == txtLoginID.Text.Trim()).SingleOrDefault();

                if (objProviderProfile != null)
                {
                    if (objProviderProfile.PP_ProviderName == txtProviderName.Text.Trim())
                    {
                        lblMessage.Text = "Existing request for the provider is under approval process and cannot be viewed.";
                    }
                    else
                    {
                        lblMessage.Text = "The Preferred Login ID entered is already used.Please try with different one. ";
                    }
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    return;
                }

                ProviderProfileSave objProviderProfileSave = objOnlineProvidersEntities.ProviderProfileSaves.Where(item => item.PP_UserName == txtLoginID.Text.Trim()).SingleOrDefault();
                if (objProviderProfileSave != null)
                {
                    if (objProviderProfileSave.PP_ProviderName != txtProviderName.Text.Trim())
                    {
                        lblMessage.Text = "The Preferred Login ID entered is already used.Please try with different one. ";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                    else
                    {
                        txtProviderName.Enabled = false;
                        txtLoginID.Enabled = false;
                        SetProviderInfoSave(objProviderProfileSave);

                        //pharmacy details
                        //contact person details
                        //document details
                        string UrlPath = ConfigurationManager.AppSettings["OnlineContractingDOcs"];// HttpContext.Current.Request.Url.ToString().Substring(0, HttpContext.Current.Request.Url.ToString().IndexOf(HttpContext.Current.Request.RawUrl.ToString()));
                        List<ProviderContact> objProviderContact = objOnlineProvidersEntities.ProviderContacts.ToList().Where(items => items.PC_ActionBy == txtLoginID.Text.Trim()).ToList();
                        Session["ContactList"] = objProviderContact;
                        rgAdditionalContacts.Rebind();


                        List<ProviderPharmacy> objProviderPharmacyList = objOnlineProvidersEntities.ProviderPharmacies.ToList().Where(item => item.ActionBy == txtLoginID.Text.Trim()).ToList();
                        if (objProviderPharmacyList.Count > 0)
                        {
                            Session["PharmacyList"] = objProviderPharmacyList;
                            rgPharmacy.Rebind();
                            trPrahmacy.Visible = true;
                        }


                        List<ProviderDocument> objProviderPracticeList = objOnlineProvidersEntities.ProviderDocuments.ToList().Where(items => items.PD_ActionBy == txtLoginID.Text.Trim()).ToList();
                        foreach (ProviderDocument objProviderDocument in objProviderPracticeList)
                        {
                            objProviderDocument.PD_DocumentPath = UrlPath + objProviderDocument.PD_DocumentPath;
                        }
                        Session["AttachmentsTable"] = objProviderPracticeList;
                        rgAttachments.Rebind();
                        if (ddlCountry.Text != "Saudi Arabia")
                        {
                            ddlCity.AllowCustomText = true;
                            ddlCity.MarkFirstMatch = false;
                            tddistrict1.Visible = true;
                            tddistrict2.Visible = true;
                            spancchi.Visible = false;
                            spancchiDate.Visible = false;
                            spanCRNo.Visible = false;
                            spanCRDate.Visible = false;
                            spanMOH.Visible = false;
                            spanMOHDate.Visible = false;
                            rfvCCHI.Visible = false;
                            rfvCCHIDate.Visible = false;
                            rfvMOHDate.Visible = false;
                            rfvMOH.Visible = false;
                            rfvCRDate.Visible = false;
                            rfvCRNo.Visible = false;
                        }
                        else
                        {
                            ddlCity.AllowCustomText = false;
                            ddlCity.MarkFirstMatch = true;
                            tddistrict1.Visible = false;
                            tddistrict2.Visible = false;
                            spancchi.Visible = true;
                            spancchiDate.Visible = true;
                            spanCRNo.Visible = true;
                            spanCRDate.Visible = true;
                            spanMOH.Visible = true;
                            spanMOHDate.Visible = true;
                            rfvCCHI.Visible = true;
                            rfvCCHIDate.Visible = true;
                            rfvMOHDate.Visible = true;
                            rfvMOH.Visible = true;
                            rfvCRDate.Visible = true;
                            rfvCRNo.Visible = true;

                        }
                    }
                }

            }
        }
        else
        {
            //display error message
            RadWindowManager1.RadAlert("Please enter Provider Name and Preferred Login ID.", 330, 180, "Validation Message", "");
        }
    }
    protected void ddlPractice_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (ddlPractice.SelectedValue == "Others")
        {
            trPracticeOthers.Visible = true;
            txtOtherPractice.Text = string.Empty;
        }
        else
        {
            trPracticeOthers.Visible = false;
        }
    }
    protected void chkFacilityOthers_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (chkFacilityOthers.Items[0].Selected == true)
        {
            tbFacilityOthers.Visible = true;
        }
        else
        {
            tbFacilityOthers.Visible = false;
        }
    }
    protected void chkSpecilaityOthers_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (chkSpecilaityOthers.Items[0].Selected == true)
        {
            tbSpecialityOthers.Visible = true;
        }
        else
        {
            tbSpecialityOthers.Visible = false;
        }
    }
    protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        hrefPriceList.Visible = false;
        hrefDoctorDetails.Visible = false;
        if (ddlDocumentType.SelectedItem.Text == "Price List")
        {
            trDownloadTemplate.Visible = true;
            hrefPriceList.Visible = true;
        }
        else if (ddlDocumentType.SelectedItem.Text == "Doctor Details")
        {
            trDownloadTemplate.Visible = true;
            hrefDoctorDetails.Visible = true;
        }
        else
        {
            trDownloadTemplate.Visible = false;
        }
    }
}