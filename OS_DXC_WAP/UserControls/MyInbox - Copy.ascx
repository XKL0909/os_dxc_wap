﻿<%@ Control Language="VB" ClassName="MyInbox" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">
    Dim cFX As New fx
    Dim UserEmails As List(Of Email)
    Dim MyEmail As Email
    Dim link As String
    Private ConnString As String = ""
    Private WebsiteURL As String = "http://onlineservices.bupa.com.sa/communication/"
    Private errortemp As String = "<tr><td>{0}</td></tr>"
    
    Protected Sub Page_Load(sender As Object, e As System.EventArgs)
        Try
            If Not Page.IsPostBack Then
                If ConfigurationManager.ConnectionStrings("ConnString") IsNot Nothing Then
                    ConnString = ConfigurationManager.ConnectionStrings("ConnString").ToString
                End If
                If ConfigurationManager.ConnectionStrings("websiteURL") IsNot Nothing Then
                    WebsiteURL = ConfigurationManager.ConnectionStrings("websiteURL").ToString
                End If
                hlMyInbox.HRef = WebsiteURL & "Inbox.aspx"
                
		

		getUserIDAndType()

		

                If IsNumeric(userID.Value) AndAlso IsNumeric(userType.Value) Then
                    BindData()
                Else
                    litError.Text = String.Format(cFX.errortemp, "Please login in order to display data.")
                End If
            End If
        Catch ex As Exception
            cFX.LogError(ex, litError)
        End Try
    End Sub
    
       Private _contractNumber As String
    Public Property contractNumber As String
        Get
            Return _contractNumber
        End Get
        Set(value As String)
            _contractNumber = value
        End Set
    End Property
    
    Private Sub BindData()
        Try
            UserEmails = GetUserEmails(CInt(userID.Value), CInt(userType.Value))
                        
            Select Case UserEmails.Count
                Case Is > 1
                    litUnread.Text = String.Format("(You have {0} unread messages)", UserEmails.Count)
                Case Is = 1
                    litUnread.Text = "(You have 1 unread message)"
            End Select
            If UserEmails.Count > 0 Then
                rptData.DataSource = UserEmails
                rptData.DataBind()
            Else
                litError.Text = String.Format(cFX.errortemp, "You don't have any new messages.")
                rptData.Visible = False
            End If
        Catch ex As Exception
            cFX.LogError(ex, litError)
        End Try
    End Sub
    
    Private Sub getUserIDAndType()
        If HttpContext.Current.Request.Cookies("userLogin") IsNot Nothing Then
            Dim UserInfoCookieCollection As System.Collections.Specialized.NameValueCollection
            UserInfoCookieCollection = HttpContext.Current.Request.Cookies("userLogin").Values
            userID.Value = UserInfoCookieCollection("UserID")
            userType.Value = UserInfoCookieCollection("UserType")
        End If
    End Sub
    
    Protected Sub rptData_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                MyEmail = CType(e.Item.DataItem, Email)
                link = WebsiteURL & "ViewEmail.aspx?id=" & MyEmail.ID & IIf(MyEmail.IsTOB, "&type=1&nore=1", "&nore=1")
                CType(e.Item.FindControl("litSubject"), Literal).Text = MyEmail.Subject
                If MyEmail.isUrgent = True Then
                    CType(e.Item.FindControl("litSubject"), Literal).Text &= "<b style='color: red; padding: 0 3px;  font-family: verdana;'>!</b>"
                End If
                CType(e.Item.FindControl("litDate"), Literal).Text = Format(MyEmail.myDate, "dd/MM/yyyy hh:mm tt")
                CType(e.Item.FindControl("litFrom"), Literal).Text = MyEmail.FromFullName
                If ConfigurationManager.AppSettings("EnableTamperURL") IsNot Nothing AndAlso (ConfigurationManager.AppSettings("EnableTamperURL") = "1" Or ConfigurationManager.AppSettings("EnableTamperURL").ToLower = "true") Then
                    link = cFX.CreateTamperProofURL(link.ToString.Split("?")(0), "", link.ToString.Split("?")(1))
                End If
                CType(e.Item.FindControl("trEmail"), HtmlTableRow).Attributes.Add("onclick", String.Format("window.location = '{0}';", link))
            End If
        Catch ex As Exception
            cFX.LogError(ex, litError)
        End Try
    End Sub
    
    Private Function GetUserEmails(receiverID As Integer, receiverType As Integer) As List(Of Email)
        Dim cmd As New SqlCommand
        Dim cn As New SqlConnection
        cn.ConnectionString = ConnString
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "Select * From viewContactCenterDetailed where istob=0 and toID=@toID and userType=@userType and StatusID = 1 order by id desc"
        cmd.Parameters.AddWithValue("@toID", receiverID)
        cmd.Parameters.AddWithValue("@userType", receiverType)
        Dim dr As IDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Dim MyOS_ContactCenter As Email
        UserEmails = New List(Of Email)
        
        While dr.Read
            MyOS_ContactCenter = New Email
            MyOS_ContactCenter.ID = dr("ID")
            MyOS_ContactCenter.FromID = dr("FromID")
            MyOS_ContactCenter.ToID = dr("ToID")
            MyOS_ContactCenter.EmailGoupID = dr("EmailGoupID")
            MyOS_ContactCenter.Subject = dr("Subject")
            MyOS_ContactCenter.Message = dr("Message")
            MyOS_ContactCenter.myDate = dr("Date")
            MyOS_ContactCenter.IsTOB = dr("IsTOB")
            MyOS_ContactCenter.CustomerName = dr("CustomerName")
            MyOS_ContactCenter.CustomerNo = dr("CustomerNo")
            MyOS_ContactCenter.ClassID = dr("ClassID")
            MyOS_ContactCenter.Scheme = dr("Scheme")
            MyOS_ContactCenter.Attachment = dr("Attachment")
            MyOS_ContactCenter.StatusID = dr("StatusID")
            MyOS_ContactCenter.userType = dr("userType")
            MyOS_ContactCenter.Status = dr("Status")
            MyOS_ContactCenter.senderType = dr("senderType")
            MyOS_ContactCenter.fullBody = dr("fullBody")
            MyOS_ContactCenter.isUrgent = dr("isUrgent")
            If dr("senderType") = 1 Then
                MyOS_ContactCenter.FromUsername = IIf(dr("memberUsername") Is DBNull.Value, "", dr("memberUsername"))
                MyOS_ContactCenter.FromFullName = IIf(dr("fullname") Is DBNull.Value, "", dr("fullname"))
            ElseIf dr("senderType") = 2 Then
                MyOS_ContactCenter.FromUsername = IIf(dr("providerUsername") Is DBNull.Value, "", dr("providerUsername"))
                MyOS_ContactCenter.FromFullName = IIf(dr("ProName") Is DBNull.Value, "", dr("ProName"))
            ElseIf dr("senderType") = 3 Then
                MyOS_ContactCenter.FromUsername = IIf(dr("clientUsername") Is DBNull.Value, "", dr("clientUsername"))
                MyOS_ContactCenter.FromFullName = IIf(dr("cliName") Is DBNull.Value, "", dr("cliName"))
            ElseIf dr("senderType") = 5 OrElse dr("senderType") = 9 Then
                MyOS_ContactCenter.FromUsername = ConfigurationManager.AppSettings("AdminName")
                MyOS_ContactCenter.FromFullName = ConfigurationManager.AppSettings("AdminName")
            Else
                MyOS_ContactCenter.FromUsername = ""
                MyOS_ContactCenter.FromFullName = ""
            End If
            MyOS_ContactCenter.locked = dr("locked")
            MyOS_ContactCenter.lockedBy = dr("lockedBy")
            MyOS_ContactCenter.lockedOn = dr("lockedOn")
            MyOS_ContactCenter.lockedByName = IIf(dr("lockedByName") Is DBNull.Value, "", dr("lockedByName"))
            MyOS_ContactCenter.repliedOn = dr("repliedOn")
            UserEmails.Add(MyOS_ContactCenter)
        End While
        dr.Close()
        
        Return UserEmails
    End Function
    
    Class Email
        Private vID As Int32

        Public Property ID() As Int32
            Get
                Return vID
            End Get
            Set(value As Int32)
                vID = value
            End Set
        End Property

        Private vFromID As Int32

        Public Property FromID() As Int32
            Get
                Return vFromID
            End Get
            Set(value As Int32)
                vFromID = value
            End Set
        End Property

        Private vToID As Int32

        Public Property ToID() As Int32
            Get
                Return vToID
            End Get
            Set(value As Int32)
                vToID = value
            End Set
        End Property

        Private vEmailGoupID As Int32

        Public Property EmailGoupID() As Int32
            Get
                Return vEmailGoupID
            End Get
            Set(value As Int32)
                vEmailGoupID = value
            End Set
        End Property

        Private vSubject As String

        Public Property Subject() As String
            Get
                Return vSubject
            End Get
            Set(value As String)
                vSubject = value
            End Set
        End Property

        Private vMessage As String

        Public Property Message() As String
            Get
                Return vMessage
            End Get
            Set(value As String)
                vMessage = value
            End Set
        End Property

        Private vmyDate As DateTime

        Public Property myDate() As DateTime
            Get
                Return vmyDate
            End Get
            Set(value As DateTime)
                vmyDate = value
            End Set
        End Property

        Private vIsTOB As Boolean

        Public Property IsTOB() As Boolean
            Get
                Return vIsTOB
            End Get
            Set(value As Boolean)
                vIsTOB = value
            End Set
        End Property

        Private vCustomerName As String

        Public Property CustomerName() As String
            Get
                Return vCustomerName
            End Get
            Set(value As String)
                vCustomerName = value
            End Set
        End Property

        Private vCustomerNo As String
        Public Property CustomerNo As String
            Get
                Return vCustomerNo
            End Get
            Set(value As String)
                vCustomerNo = value
            End Set
        End Property

        Private vClassID As String
        Public Property ClassID As String
            Get
                Return vClassID
            End Get
            Set(value As String)
                vClassID = value
            End Set
        End Property

        Private vuserType As Integer
        Public Property userType As Integer
            Get
                Return vuserType
            End Get
            Set(value As Integer)
                vuserType = value
            End Set
        End Property

        Private vsenderType As Integer
        Public Property senderType As Integer
            Get
                Return vsenderType
            End Get
            Set(value As Integer)
                vsenderType = value
            End Set
        End Property

        Private vScheme As String

        Public Property Scheme() As String
            Get
                Return vScheme
            End Get
            Set(value As String)
                vScheme = value
            End Set
        End Property

        Private vAttachment As String

        Public Property Attachment() As String
            Get
                Return vAttachment
            End Get
            Set(value As String)
                vAttachment = value
            End Set
        End Property

        Private vfullBody As String
        Public Property fullBody As String
            Get
                Return vfullBody
            End Get
            Set(value As String)
                vfullBody = value
            End Set
        End Property

        Private vStatusID As Integer

        Public Property StatusID() As Integer
            Get
                Return vStatusID
            End Get
            Set(value As Integer)
                vStatusID = value
            End Set
        End Property

        Private vStatus As String
        Public Property Status As String
            Get
                Return vStatus
            End Get
            Set(value As String)
                vStatus = value
            End Set
        End Property

        Private Shared vSortBy As String
        Public Shared Property SortBy() As String
            Get
                Return vSortBy
            End Get
            Set(ByVal value As String)
                vSortBy = value
            End Set
        End Property

        Private vFromUsername As String
        Public Property FromUsername As String
            Get
                Return vFromUsername
            End Get
            Set(value As String)
                vFromUsername = value
            End Set
        End Property

        Private vFromFullName As String
        Public Property FromFullName As String
            Get
                Return vFromFullName
            End Get
            Set(value As String)
                vFromFullName = value
            End Set
        End Property

        Private vToUsername As String
        Public Property ToUsername As String
            Get
                Return vToUsername
            End Get
            Set(value As String)
                vToUsername = value
            End Set
        End Property

        Private vToFullName As String
        Public Property ToFullName As String
            Get
                Return vToFullName
            End Get
            Set(value As String)
                vToFullName = value
            End Set
        End Property

        Private visUrgent As Boolean
        Public Property isUrgent As Boolean
            Get
                Return visUrgent
            End Get
            Set(value As Boolean)
                visUrgent = value
            End Set
        End Property

        Private vlocked As Boolean
        Public Property locked As Boolean
            Get
                Return vlocked
            End Get
            Set(value As Boolean)
                vlocked = value
            End Set
        End Property

        Private vlockedBy As Integer
        Public Property lockedBy As Integer
            Get
                Return vlockedBy
            End Get
            Set(value As Integer)
                vlockedBy = value
            End Set
        End Property

        Private vlockedByName As String
        Public Property lockedByName As String
            Get
                Return vlockedByName
            End Get
            Set(value As String)
                vlockedByName = value
            End Set
        End Property

        Private vlockedOn As DateTime
        Public Property lockedOn As DateTime
            Get
                Return vlockedOn
            End Get
            Set(value As DateTime)
                vlockedOn = value
            End Set
        End Property

        Private vrepliedOn As DateTime
        Public Property repliedOn As DateTime
            Get
                Return vrepliedOn
            End Get
            Set(value As DateTime)
                vrepliedOn = value
            End Set
        End Property
    End Class
    
    Class fx
        Public errortemp As String = "<tr><td>{0}</td></tr>"
        Private Const SecretSalt = "H3#@*ALMLLlk31q4l1ncL#@..."
        
        Public Sub LogError(ex As Exception, ByRef litError As Literal)
            litError.Text = String.Format(errortemp, ex.Message & "<br/>" & ex.StackTrace)
        End Sub
        
        Public Function CreateTamperProofURL(ByVal url As String, ByVal nonTamperProofParams As String, ByVal tamperProofParams As String) As String
            Dim tpURL As String = url
            Dim strQS As String = ""
            If nonTamperProofParams.Length > 0 OrElse tamperProofParams.Length > 0 Then
                url &= "?"
            End If
            If nonTamperProofParams.Length > 0 Then
                strQS &= nonTamperProofParams
                If tamperProofParams.Length > 0 Then strQS &= "&"
            End If
            If tamperProofParams.Length > 0 Then strQS &= tamperProofParams
            If tamperProofParams.Length > 0 Then
                strQS &= String.Concat("&Digest=", GetDigest(tamperProofParams))
            End If
            Return url & strQS
        End Function

        Function GetDigest(ByVal strStringToHash As String) As String
            Dim Digest As String = String.Empty
            Dim input As String = String.Concat(SecretSalt, strStringToHash, SecretSalt)
            Dim hashedDataBytes As Byte()
            Dim encoder As New System.Text.UTF8Encoding
            Dim md5Hasher As New System.Security.Cryptography.MD5CryptoServiceProvider
            hashedDataBytes = md5Hasher.ComputeHash(encoder.GetBytes(input))
            Dim sb As New System.Text.StringBuilder
            Dim outputByte As Byte
            For Each outputByte In hashedDataBytes
                sb.Append(outputByte.ToString("x2").ToUpper)
            Next outputByte
            Digest = sb.ToString
            Return Digest
        End Function

        Public Function EnsureURLNotTampered(ByVal tamperProofParams As String, ByVal Digest As String) As Boolean
            Dim expectedDigest As String = GetDigest(tamperProofParams)
            Dim receivedDigest As String = Digest
            If receivedDigest Is Nothing Then
                Return False
            Else
                receivedDigest = receivedDigest.Replace(" ", "+")
                If String.Compare(expectedDigest, receivedDigest) <> 0 Then
                    Return False
                Else
                    Return True
                End If
            End If
        End Function
                
    End Class
</script>

<style type="text/css">
    .margBottom25 {
    margin-bottom: 15px;
    }
    h2.dahsboardTitle {
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 15px;
    }
    .dahsboardTitle{
    color: #B85D03 !important;
    text-decoration: none;
    }
    td.gotoinbox a{
        color: #B85D03;
        text-decoration: none;
    }
    td.gotoinbox {
    font-size: 12px;
    padding-bottom: 15px;
    vertical-align: bottom;
    }
    .emailsTable td {
    padding: 7px 0;
    border-bottom: 1px solid #BCBCBC;
    font-size: 12px;
    color: #333;
    font-weight: bold;
    }
    td {
    vertical-align: top;
    font-family: Arial;
    }
    .emailsTable td a, td.gotoinbox a:hover {
    text-decoration: none;
    color: #333;
    }
    .dahsboardTitle span {
    font-weight: normal;
    }
    .emailsTable {
    background: url(/images/tableborder-top.gif) repeat-x;
    }
	.emailsTable tr.row {
		cursor:pointer;
	}
	.emailsTable tr.row:hover td {
		color: #B85D03 !important;
	}
</style>
<asp:HiddenField ID="userID" runat="server" />
<asp:HiddenField ID="userType" runat="server" />
<div class="margBottom25">
    <div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><h2 class="dahsboardTitle">My Inbox <span><asp:Literal ID="litUnread" runat="server" /></span></h2></td>
            <td align="right" class="gotoinbox"><a id="hlMyInbox" runat="server" href="/Inbox.aspx">Go To My Inbox</a></td>
        </tr>
    </table>
    </div>
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="emailsTable">
            <asp:Repeater ID="rptData" runat="server" OnItemDataBound="rptData_ItemDataBound">
                <ItemTemplate>
                    <tr id="trEmail" runat="server" class="row">
                        <td class="emailUserName"><asp:Literal ID="litFrom" runat="server"></asp:Literal></td>
                        <td><asp:Literal ID="litSubject" runat="server"></asp:Literal></td>
                        <td width="160px"><asp:Literal ID="litDate" runat="server"></asp:Literal></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Literal ID="litError" runat="server"></asp:Literal>
        </table>
    </div>
</div>