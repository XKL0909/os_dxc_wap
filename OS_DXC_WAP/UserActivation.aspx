﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" Inherits="UserActivation" Codebehind="UserActivation.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="UserControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Header.ascx" TagName="Header" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc2" TagName="Footer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <script type="text/javascript" src="../functions/Login/jquery.js"></script>

    <link rel="stylesheet" type="text/css" href="styles/Login/bupa.css" />
    <link rel="stylesheet" type="text/css" href="styles/Login/home-slider.css" />
    <link rel="stylesheet" type="text/css" href="styles/Login/help-slider.css" />
    <link rel="stylesheet" type="text/css" href="Styles/lightblue.css" />
    <link href="Scripts/Bupa/MessageBar/CSS/skin.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="Styles/Impromptu.css" />
    <link rel="stylesheet" type="text/css" href="Styles/OSWebStyle.css" />
    <script type='text/javascript' src="../javascript/ManageUser.js"></script>
    <title>Password Reset - Bupa</title>
    <script src="Scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <%-- <script type="text/javascript" src="<%# Page.ResolveUrl("Scripts/jquery-1.7.2.min.js") %>"></script>
    <script type="text/javascript" src="functions/functions.js"></script>
    <link href="styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
     <script type='text/javascript' src="javascript/SessionMsg.js"></script>
     <link rel="stylesheet" type="text/css" href="Styles/SessionMsg.css" />
    <script type="text/javascript" src="functions/jquery.fancybox.js"></script>--%>
    <script type="text/javascript">
        function RedirectUserToDefault() {
            if (document.getElementById('LoginInTypeForUser').value == 'g')
                RedirectDefaultPage.href = "../Default.aspx?LoginType=1";
            else
                RedirectDefaultPage.href = "../Default.aspx?LoginType=2";
        }
    </script>

    <style type="text/css">
 
  div.botright{
    display:block;

  
     position:fixed;
    top: 50%;
    left: 50%;

    width: 600px;
    height:230px;
    
	padding: 25px 15px;
    
    z-index:999999;
     margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/


    /*give it some background and border*/
  
  }

 p.MsoNormal
	{margin-bottom:.0001pt;
	font-size:16.0pt;
	font-family:"Calibri","sans-serif";
	    margin-left: 0in;
        margin-right: 0in;
        margin-top: 0in;
    }

</style>

</head>
<body id="bupaWebsite">


    <form runat="server">
        <asp:HiddenField ID="LoginInTypeForUser" runat="server" />

        <div class="socialMediaIcons">
            <a target="_blank" href="https://www.facebook.com/BupaArabia" class="facebookIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Facebook" width="1" height="1"></a>
            <a target="_blank" href="https://twitter.com/bupaarabia" class="twitterIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Twitter" width="1" height="1"></a>
            <a target="_blank" href="http://www.youtube.com/bupaarabia" class="youtubeIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Youtube" width="1" height="1"></a>
            <a style="display: none" href="javascript:;" class="emailFriendIcon">
                <img src="/Style Library/images/spacer.gif" alt="Email Friend" title="Email Friend" width="1" height="1"></a>
            <a target="_blank" style="display: none" href="http://test.borninteractive.net:8081/wpbupa" class="blogIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Youtube" width="1" height="1"></a>
            <a href="http://instagram.com/bupaarabia" target="_blank" class="instagramIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Instagram" width="1" height="1"></a>
            <a href="http://www.linkedin.com/company/bupa-arabia" target="_blank" class="linkedInIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="linkedIn" width="1" height="1"></a>
        </div>
        <uc2:Header ID="Header1" runat="server" />
        &nbsp;<div>
            <table width="995" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="centerBack" style="background: #E6E6E6 url(images/BackgroundImage.jpg) center 0 no-repeat;">
                        <div class="margBottom20">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="pageTitle">
                                                    <h1>bupa online services</h1>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="subWhiteCenter">
                            <div class="pageHeadRow">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td id="HeaderText" runat="server" style="font-size: 15px">User Verification</td>
                                        <td align="right"></td>
                                    </tr>
                                </table>
                            </div>
                            <link href="css/basic.css" rel="stylesheet" type="text/css" />
                            <link href="css/basic_ie.css" rel="stylesheet" type="text/css" />

                            <asp:ScriptManager runat="server" ID="scriptManager" />
                            <style type="text/css">
                        body {
                        }

                        .modalBackground {
                            background-color: #111111;
                            filter: alpha(opacity=60);
                            opacity: 0.6;
                        }



                        .modalPopupActivation {
                            background-color: #FFFFFF;
                            width: 600px;
                            border: 3px solid #0075C6;
                            padding: 0;
                        }

                            .modalPopupActivation .header {
                                background-color: #0075C6;
                                height: 30px;
                                color: White;
                                line-height: 30px;
                                text-align: center;
                                font-weight: bold;
                                font-family: Verdana;
                                font-size: 8pt;
                            }

                            .modalPopupActivation .body {
                                min-height: 50px;
                                line-height: 30px;
                                text-align: center;
                                font-weight: bold;
                                font-family: Verdana;
                                font-size: 8pt;
                            }

                            .modalPopupActivation .footer {
                                padding: 6px;
                            }

                            .modalPopupActivation .yes .no {
                                height: 23px;
                                color: White;
                                line-height: 23px;
                                text-align: center;
                                font-weight: bold;
                                cursor: pointer;
                                border-radius: 4px;
                                font-family: Verdana;
                                font-size: 8pt;
                            }

                            .modalPopupActivation .yes {
                                background-color: #211222;
                                border: 1px solid #121111;
                            }

                            .modalPopupActivation .no {
                                background-color: #111111;
                                border: 1px solid #333333;
                            }
                    </style>

                            <div style="width: 800px; margin: 0 auto; font-family: Verdana, Arial; font-size: 12px; padding: 70px 0px 70px 0px">

                                <asp:Label runat="server" ID="lblMessage"></asp:Label>
                                <div id="divVerification" class="modalPopupActivation" runat="server">
                                    <div class="header">
                                        User Verification
                                    </div>
                                    <div class="body">
                                        <table style="width: 500px; margin-top: 20px;">

                                            <tr>
                                                <td style="width: 270px">User Name</td>
                                                <td style="text-align: left" colspan="2">
                                                    <asp:Label runat="server" ID="lblUserName"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>Enter OTP
                    <span style="color: red">*</span>
                                                </td>
                                                <td style="text-align: left">
                                                    <asp:TextBox ID="txtOtp" runat="server" MaxLength="6" ForeColor="Red" AutoCompleteType="None"></asp:TextBox>
                                                    
                                                    <%-- <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                                ControlToValidate="txtOtp" ErrorMessage="Invalid OTP" ValidationGroup="verify" />--%>
                                                </td>
                                                <td><asp:RequiredFieldValidator ID="rfvtxtOtp" runat="server" ControlToValidate="txtOtp"
                                                        EnableClientScript="true" Enabled="true" ErrorMessage="Fiels is mandatory"
                                                        Font-Bold="False" Font-Size="XX-Small" SetFocusOnError="false"
                                                        ValidationGroup="verify" Width="233px" Display="Dynamic"></asp:RequiredFieldValidator></td>
                                            </tr>
                                            <tr>
                                                <td>Enter captcha</td>
                                                <td>
                                                    <asp:TextBox ID="txtimgcode" runat="server"  MaxLength="6"></asp:TextBox>
                                                </td>
                                                <td>
                                                     <asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" ControlToValidate="txtimgcode" ErrorMessage="Fiels is mandatory"
                                                        Display="Dynamic" ValidationGroup="verify"  Font-Bold="False" Font-Size="XX-Small" SetFocusOnError="false"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="CaptchaText" runat="server" style="padding-top: 15px; width: 350px; height: 60px; background-image: url('../images/CaptchaBackground.png'); text-align: center; font-size: 40px; color: #61C0E0; font-weight: bold; letter-spacing: 3px; vertical-align: middle; font-family: 'GenericSansSerif'; font-style: italic;">
                                                    </div>
                                                </td>
                                                <td>
                                                     <asp:ImageButton ID="CaptchaValidate" runat="server" ImageUrl="../images/icons/reCaptchaImg.jpg" OnClick="CaptchaValidate_Click" Style="width: 81px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:Label runat="server" ID="lblOTPMessge" ForeColor="Red"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="footer" align="center">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnVerfication" runat="server" Text="Verify" ValidationGroup="verify" OnClick="btnVerfication_Click" />
                                                </td>
                                                <td style="padding-right: 50px;">
                                                    <asp:Button ID="btnResentOTP" runat="server" OnClick="btnResentOTP_Click" Text="Resend OTP"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>

                                </div>

                                <div id="divActivation" class="modalPopupActivation" runat="server" style="display: inline">
                                    <div class="header">
                                        User Activation
                                    </div>
                                    <div class="body">
                                        <table style="width: 500px; margin-top: 20px;">

                                            <tr>
                                                <td style="width: 200px">New Password</td>
                                                <td style="text-align: left">
                                                    <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" AutoCompleteType="None"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvtxtNewPassword" runat="server" ControlToValidate="txtNewPassword"
                                                        EnableClientScript="true" Enabled="true" ErrorMessage="Fiels is mandatory"
                                                        Font-Bold="False" Font-Size="XX-Small" SetFocusOnError="false"
                                                        ValidationGroup="Activate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator Style="color: red !important; line-height: 15px!important;" ID="revtxtNewPassword" runat="server" Display="Dynamic" ControlToValidate="txtNewPassword" ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}"
                                                        ValidationGroup="Activate" ErrorMessage="<br/>New password must be at least 8 characters long * 1 Upper Case letter <br/>* 1 Lower Case letter <br/>* 1 Number <br/>* 1 Special character from the following ? ! @ $ % & *" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 200px">Confirm Password</td>
                                                <td style="text-align: left">
                                                    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" AutoCompleteType="None"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvtxtConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                                        EnableClientScript="true" Enabled="true" ErrorMessage="Fiels is mandatory"
                                                        Font-Bold="False" Font-Size="XX-Small" SetFocusOnError="false"
                                                        ValidationGroup="Activate" Width="233px" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator Style="color: red !important; line-height: 15px!important;" ID="revtxtConfirmPassword" runat="server" Display="Dynamic" ControlToValidate="txtNewPassword" ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}"
                                                        ValidationGroup="Activate" ErrorMessage="<br/>Confirm password must be at least 8 characters long <br/>* 1 Upper Case letter <br/>* 1 Lower Case letter <br/>* 1 Number <br/>* 1 Special character from the following ? ! @ $ % & *" />
                                                    <asp:CompareValidator ID="cvtxtConfirmPassword" runat="server" ControlToCompare="txtNewPassword"
                                                        ControlToValidate="txtConfirmPassword" ErrorMessage="CompareValidator" Font-Names="Verdana"
                                                        Font-Size="XX-Small" Display="Dynamic" ValidationGroup="Activate">Passwords are not identical.</asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label runat="server" ID="lblPwdMessge"></asp:Label></td>
                                            </tr>

                                        </table>
                                    </div>
                                    <div class="footer" align="center">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="Activate" />
                                    </div>

                                </div>

                                <div id="popUpSuccess" runat="server" style="display: none; top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999;" visible="false">
                                    <div style="left: 30%; top: 10%; position: absolute;">
                                        <table style="width: 100%; background-color: white;">
                                            <tr style="background-color: #0886D6; line-height: 30px">
                                                <td colspan="2" style="color: #fff; text-align: left; padding-right: 8px; border-bottom-color: cyan; border-bottom-width: medium; border-bottom-style: outset;">
                                                    <strong style="padding-left: 5px; font-size: larger">BUPA - Online Services</strong>
                                                    <span style="float: right;">
                                                        <strong style="font-size: x-large">بوبا - خدمات الإلكترونية</strong>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 500px; height: 130px; background-color: white; padding: 25px" cellpadding="0" cellspacing="0">
                                                        <tr style="line-height: 47px;">
                                                            <td colspan="2" style="font-size: 16px">User has been activated successfully. Please login now</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 390px;"></td>
                                                            <td>

                                                                <a id="RedirectDefaultPage" style="padding: 10px 30px; text-align: center; color: white; line-height: 36px; text-decoration: none; background-color: rgb(8, 134, 214);" onclick="RedirectUserToDefault()" href="#">Ok</a>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>


                                </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="copyrightContainer">
                            <uc2:Footer runat="server" ID="Footer" />

                        </div>
                    </td>

                </tr>
            </table>

        </div>

        <script type="text/javascript" src="functions/Login/jquery.js"></script>
        <script type="text/javascript" src="functions/Login/jquery.anythingslider.min.js"></script>
        <script type="text/javascript" src="functions/Login/cufon.js"></script>
        <script type="text/javascript" src="functions/Login/Gotham_Book_400.font.js"></script>
        <script type="text/javascript" src="functions/Login/cufon.init.js"></script>
        <script type="text/javascript" src="functions/Login/functions.js"></script>
        <script type="text/javascript" src="Scripts/jquery.blockUI.js"></script>

        <script type="text/javascript">
    $(document).ready(function () {
        toggleFooter();
    });

    $(function () {
        $('#slider,#Helpslider').anythingSlider();
    });
        </script>



        <a href="#divSessionExpired" class="fancybox5" style="display: none;"></a>
        <div style="display: none;" id="divSessionExpired">
            <div>
                <br />
                <asp:Literal runat="server" ID="ltExpierySessionMessage" Text="do you want to continue ??"></asp:Literal>
                <br />
                <br />
            </div>
            <div class="inputEntity">
            </div>
        </div>

        <h3 style="display: none">Session Idle:<span id="secondsIdle"></span>seconds.</h3>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="lnkSession" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeSessionTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkSession"
                    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                    <div class="header">
                        Session Expiring!
                    </div>

                    <div class="body">
                        Your Session will expire in <span id="seconds"></span>seconds.<br />
                        Do you want to reset?
                    </div>

                    <div class="footer" align="right">
                        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="Savebutton" CausesValidation="false" />
                        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="ButtonCancel" CausesValidation="false" OnClientClick="javascript:RedirectPage()" />
                    </div>
                </asp:Panel>
                <div id="mainContent">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>




