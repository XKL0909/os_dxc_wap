﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Default" Codebehind="Default.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="UserControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Header.ascx" TagName="Header" TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc2" TagName="Footer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" type="text/css" href="styles/Login/bupa.css" />
    <link rel="stylesheet" type="text/css" href="styles/Login/home-slider.css" />
    <link rel="stylesheet" type="text/css" href="styles/Login/help-slider.css" />
    <link rel="stylesheet" type="text/css" href="Styles/lightblue.css" />
       <link href="Scripts/Bupa/MessageBar/CSS/skin.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="Styles/Impromptu.css" />
    <link rel="stylesheet" type="text/css" href="Styles/OSWebStyle.css" />
	<title>Login - Bupa</title>

    <!-- <script type="text/javascript" src="javascript/jquery-3.3.1.min.js"></script> -->
  <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/jquery-1.7.2.min.js") %>"></script>
    <script type="text/javascript" src="functions/functions.js"></script>
    <style type="text/css">
        .btnCss {
            width: auto !important;
            background-color: #0886D6;
            cursor: pointer;
            color: #fff !important;
            line-height: 30px !important;
            outline: none;
            border: none;
            font-weight: bold;
            font-size: 13px !important;
        }

            .btnCss:hover {
                background-color: #00a1e4;
            }
    </style>

<style type="text/css">
        div.botright {
            display: block;
            position: fixed;
    top: 50%;
    left: 50%;
    width: 600px;
            height: 230px;
	padding: 25px 15px;
            z-index: 999999;
     margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    /*give it some background and border*/
  }

        p.MsoNormal {
            margin-bottom: .0001pt;
            font-size: 16.0pt;
            font-family: "Calibri","sans-serif";
	    margin-left: 0in;
        margin-right: 0in;
        margin-top: 0in;
    }
</style>

    <script type="text/javascript" src="javascript/sha256.min.js"></script>
    <script type="text/javascript">
        function getHashPassword() {
            document.getElementById('<%= hdfLCPTP.ClientID %>').value = sha256((document.getElementById('txtPassword').value).toLowerCase());
            document.getElementById('txtPassword').value = sha256(document.getElementById('txtPassword').value);
        }
    </script>

</head>
<body id="bupaWebsite">
<form runat="server" id="frmLogin" defaultbutton="btnLogin">
        <asp:HiddenField ID="hdfLCPTP" runat="server" />
<asp:ScriptManager runat="server" ID="scriptManager" />

<div class="socialMediaIcons">
            <a target="_blank" href="https://www.facebook.com/BupaArabia" class="facebookIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Facebook" width="1" height="1"></a>
            <a target="_blank" href="https://twitter.com/bupaarabia" class="twitterIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Twitter" width="1" height="1"></a>
            <a target="_blank" href="http://www.youtube.com/bupaarabia" class="youtubeIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Youtube" width="1" height="1"></a>
            <a style="display: none" href="javascript:;" class="emailFriendIcon">
                <img src="/Style Library/images/spacer.gif" alt="Email Friend" title="Email Friend" width="1" height="1"></a>
            <a target="_blank" style="display: none" href="http://test.borninteractive.net:8081/wpbupa" class="blogIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Youtube" width="1" height="1"></a>
            <a href="http://instagram.com/bupaarabia" target="_blank" class="instagramIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Instagram" width="1" height="1"></a>
            <a href="http://www.linkedin.com/company/bupa-arabia" target="_blank" class="linkedInIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="linkedIn" width="1" height="1"></a>
</div>
<uc2:Header ID="Header1" runat="server" />
        <div>
<table width="995" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
                    <td class="centerBack" style="background: #E6E6E6 url(images/BackgroundImage.jpg) center 0 no-repeat;">
        <div class="margBottom20">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                                                <td class="pageTitle">
                                                    <h1>bupa online services</h1>
                                                </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </div>
        <div class="subWhiteCenter">
        	<div class="pageHeadRow">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="breadCrumb">You are here: <a href="javascript:;">Home</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="javascript:;" class="selected">Log in</a></td>
                    <td align="right"></td>
                </tr>
            </table>
            </div>
            <div class="memberLoginComp" id="entryForm">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                                        <td class="loginLeftSection" colspan="2" style="font-size: 16px; display: none;" align="center">
                        <strong>
                        <br />
                                                A chance to <span style="color: #D30000;">win</span> an iPad or iPhone by activating your membership now for  <span style="color: #D30000;">FREE!</span>
                                                <br />
                        </strong> 
                        <br />
                                            <p class="MsoNormal" style="direction: rtl">
                            <span dir="RTL" lang="AR-SA" 
                                                    style="font-family: Arial,sans-serif; font-size: 18px;"><strong><span style="color: #D30000;">فرصة لربح</span> اي باد او اي فون عند التفعيل <span style="color: #D30000;">المجاني</span> لعضويتك</strong></span><o:p></o:p>
                                            </p>
                       <br />
                    </td>
    </tr>
                <tr>
                    <td class="loginLeftSection">
                    <div class="loginLeftSectionPad">
						<asp:UpdatePanel runat="server" ID="updatePanel">
    					<Triggers>
        				<asp:AsyncPostBackTrigger ControlID="btnLogin" />
    					</Triggers>
    					<ContentTemplate>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2" class="loginTitle">Log in to Bupa Online Services</td>
                            </tr>
                            <tr>
                                <td class="loginLabel">Are You?</td>
                                <td>
                                                                    <asp:DropDownList ID="cbLoginType" CssClass="LoginDDL" runat="server" Width="170px" Height="25px" Enabled="true">
                                                                        <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Member" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Group Secretary" Value="1"></asp:ListItem>        
                                                                        <asp:ListItem Text="Provider" Value="2"></asp:ListItem>
                                         <%--   <asp:ListItem Text="NCB" Value="3" ></asp:ListItem>--%>
                                    </asp:DropDownList>
                                    
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ControlToValidate="cbLoginType" Display="Dynamic" 
                                        ErrorMessage="&lt;img src='images/error-message.png' /&gt;Please enter a User ID" 
                                        Text="&lt;img src='images/error-message.png' /&gt;"></asp:RequiredFieldValidator></td>
                            </tr>
                           
                            <tr>
                                                                <td class="loginLabel">&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                                                <td class="loginLabel">Log in ID</td>
                                <td>
                                    <dx:ASPxTextBox ID="txtUsername" runat="server" CssClass="loginTextField" 
                                        Width="170px" MaxLength="40" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" 
                                        ControlToValidate="txtUsername" Display="Dynamic" 
                                        ErrorMessage="&lt;img src='images/error-message.png' /&gt;Please enter a User ID" 
                                        Text="&lt;img src='images/error-message.png' /&gt;"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="loginLabel">Password</td>
                        	    <td>
                                                                    <asp:TextBox ID="txtPassword" runat="server" Width="170px" CssClass="loginTextField" TextMode="Password" AutoCompleteType="None"></asp:TextBox>
                                </td>
                                <td></td>
                            </tr>
							
							<tr>
                                                                <td class="loginLabel">Enter captcha</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtimgcode" runat="server" CssClass="loginTextField" MaxLength="6"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" ControlToValidate="txtimgcode" ErrorMessage="&lt;img src='images/error-message.png'" 
                                                                        Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <div id="CaptchaText" runat="server" style="padding-top: 15px; width: 302px; height: 60px; background-image: url('../images/CaptchaBackground.png'); text-align: center; font-size: 40px; color: #61C0E0; font-weight: bold; letter-spacing: 3px; vertical-align: middle; font-family: 'GenericSansSerif'; font-style: italic;">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="CaptchaValidate" runat="server" ImageUrl="../images/icons/reCaptchaImg.jpg" OnClick="CaptchaValidate_Click" Style="width: 81px" />
                                                                </td>
                                                            </tr>
							
                            <tr valign="top">
                                <td colspan="3" align="center">
                                    <dx:ASPxLabel ID="lblError" runat="server" ForeColor="#cc0000" />
									<br />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">
                                                                    <asp:LinkButton ID="btnLogin" CssClass="loginBtn" runat="server" OnClick="btnLogin_Click" OnClientClick="getHashPassword()" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" class="forgotPasswordCont">Forgot your <a href="Reg/forgotpassword.aspx">password</a>?</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="forgotPasswordCont" colspan="2">New,  <a href="Reg/Registration.aspx">Register Now</a>? </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                                                <td colspan="2">&nbsp;</td>
                            </tr>
                                                            <tr style="display: none">
                                                                <td style="vertical-align: middle" rowspan="2" align="center">
                                                                    <a href="http://www.facebook.com/page.bupa.arabia/app_114389655412743" target="_blank">
                                                                        <img src="images/facebook1.png" width="32" height="32" style="border: 0px" />
                                    </a>
                                </td>
                                                                <td colspan="2" style="font-size: 11px" align="center">Like us NOW on <a href="http://www.facebook.com/page.bupa.arabia/app_114389655412743" target="_blank">Facebook </a>and check the full draw details</td>
                            </tr>
                                                            <tr style="display: none;">
                                                                <td colspan="2" style="font-size: 13px" align="center">تابعنا على <a href="http://www.facebook.com/page.bupa.arabia/app_114389655412743" target="_blank">الفيس بوك </a>وتصفح تفاصيل السحب الأن</td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                                                <td colspan="3" dir="rtl" style="font-family: Arial,sans-serif; font-size: 14px;">&nbsp;</td>
                                                            </tr>
                                                        </table>

                                                        <div id="divPopupConcurrentLogin" runat="server" style="top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999;">
                                                            <div style="left: 20%; top: 40%; position: fixed;">
                                                                <table style="background-color: #E5F7FF; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                                                                    <tr style="background-color: #0886D6; line-height: 30px">
                                                                        <td colspan="3" style="color: #fff; text-align: left">
                                                                            <strong style="padding-left: 5px; font-size: larger">BUPA - Online Services</strong>
                                                                            <span style="float: right;">
                                                                                <strong style="font-size: x-large">بوبا - خدمات الإلكترونية</strong>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="margin: auto" colspan="3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" align="center">
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: left;">
                                                                            <div style="padding-left: 10px; padding-right: 10px;">
                                                                                <asp:Panel ID="pnlCorporateEnglish" runat="server">
                                                                                    <div style="margin-top: 0; margin-bottom: 0;">
                                                                                        <asp:Label ID="lblConcurMsg" runat="server">
                                                                                        </asp:Label>
                                                                                        <div style="margin-top: 0; margin-bottom: 0;">
                                                                                            Click <b>Try Again</b> to try another UserId for Log On.
                                                                                        </div>
                                                                                        <div style="margin-top: 0; margin-bottom: 0;">
                                                                                            Click <b>Continue</b> to terminate currently running logon session and re-logon.
                                                                                        </div>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </td>
                                                                        <td>&nbsp;</td>
                                                                        <td style="text-align: right">
                                                                            <div style="padding-right: 10px; padding-left: 10px;">
                                                                                <div style="padding-right: 10px; padding-left: 10px;">
                                                                                    <asp:Panel ID="pnlCorporateArabic" runat="server">
                                                                                       <asp:Label ID="lblConcurMsgAr" runat="server"></asp:Label>
                                        <br />
                                                                                        <div>انقر على "المحاولة مرة أخرى" لمحاولة تسجيل الدخول بإسم مستخدم اخر.</div>
                                                                                        انقر على "متابعة" لإنهاء تسجيل الدخول من الجهاز الاخر وإعادة تسجيل الدخول من هذا الجهاز.

                                                                                    </asp:Panel>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: right; padding-bottom: 10px; padding-top: 20px;">
                                                                           <asp:Button ID="btnTryAgain" runat="server" Text="Try Again / محاولة مرة أخرى " OnClick="btnTryAgain_Click" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td style="padding-top: 20px; padding-left: 20px;">
                                                                             <asp:Button ID="btnConcurContinue" runat="server" Text="Continue / استمر" OnClick="btnConcurContinue_Click" />
                                                                        </td>
                            </tr>
                        </table>
                                                            </div>
                                                        </div>
                    
    				</ContentTemplate>
				</asp:UpdatePanel>
 			&nbsp;
				</div>
			</td>
            <td valign="top" class="loginTitle">&nbsp;  
			    Bupa Arabia Online Services
				<table width="100%" border="0" cellspacing="5" cellpadding="3">
    				<tbody>
        				<tr>
            				<td class="servicesText">Bupa Arabia 
                			is now offering all registered members access to an exclusive range 
                			of online services, designed to save time and let you concentrate on what really 
                			matters most - your health... Register today and you&#39;ll gain access to the 
                			following online Bupa services:<br />
                                <ul class="MyBullets">
                    				<li><span style="font-size: xx-large">&nbsp;. </span>View the live status of your Bupa Medical preapproval</li>
                    				<li><span style="font-size: xx-large">&nbsp;. </span>Instant access to a view of all benefits for yourself and your dependants</li>
                    				<li><span style="font-size: xx-large">&nbsp;. </span>Access to a fully downloadable Medical Certificate</li>
                    				<li style="display: none;"><span style="font-size: xx-large">&nbsp;. </span>View prescribed medication records & order refills online</li>
                    				<li><span style="font-size: xx-large">&nbsp;. </span>*NEW* Exclusive for diabetes patients - manage your treatment online</li>
                    				<li><span style="font-size: xx-large">&nbsp;. </span>Access list of all Bupa locations, including full directory of Bupa network of hospitals</li>
                				</ul> 
            					<br />
                   				Got a question?
                        		<br />
                        		We&#39;d love to help. Take a look at our 
                					<asp:HyperLink ID="HyperLink1" runat="server" CssClass="likeLinks" NavigateUrl="http://www.bupa.com.sa/English/CustomerCareCenter/Pages/default.aspx">Customer 
                        				Support</asp:HyperLink>&nbsp;page
                   
                   </td>
                </tr>
        		<tr>
            		<td align="right">
                 		<div><a href="Reg/Registration.aspx" class="registerBtn">Register Now</a></div>
					</td>
                </tr>
         	</tbody>   
		</table>
      </td>
    </tr>
</table>
</div>
            <div id="popupAttemptLeft" runat="server" style="display: none; top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999;" visible="false">

                                <div style="left: 28%; top: 40%; position: fixed;">
                                    <table style="background-color: #fff; width: 132%; font-family: Calibri; font-size: small;" cellpadding="0" cellspacing="0">
                                        <tr style="background-color: #f44336; line-height: 30px">
                                            <td colspan="3" style="color: #fff; text-align: left">
                                                <strong style="padding-left: 5px; font-size: larger">Password Attempt Left </strong>
                                                <span style="float: right;">
                                                    <strong style="font-size: larger">انتهاء صلاحية كلمة المرور&nbsp;</strong>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="margin: auto" colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; width: 60%">
                                                <div style="padding-left: 10px; padding-right: 10px;">
                                                    <asp:Panel ID="Panel1" runat="server">
                                                        <div style="margin-top: 0; margin-bottom: 0;">Dear Client, </div>
                                                    </asp:Panel>
                                                    <asp:Panel runat="server" ID="Panel2">
                                                        <div id="ExpMessage" runat="server" style="margin-top: 0; margin-bottom: 0;">
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td style="text-align: right">
                                                <div>
                                                    <asp:Panel ID="Panel4" runat="server">
                                                        <div style="margin-top: 0; margin-bottom: 0; padding-right: 10px;">،عزيزي العميل</div>
                                                    </asp:Panel>
                                                    <div id="ExpMessagear" runat="server" style="padding-right: 10px; text-align: right; padding-left: 10px;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <br />
                                                <input id="btnLater" runat="server" type="button" onclick="closePasswordExpWindow(1)" class="btnCss" value="Close" />
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div id="PopupUnSupported" runat="server" style="display: none; top: 0; left: 0; right: 0; bottom: 0; position: fixed; background-image: url('../images/popup_bg.png'); z-index: 99999;" visible="false">
                                <div style="left: 30%; top: 10%; position: absolute;">
                                    <table style="width: 60%; background-color: white" cellpadding="0" cellspacing="0">
                                        <tr style="line-height: 47px;">
                                            <td></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td><a style="padding-top: 10px; float: right; cursor: pointer" onclick="ClosePopWindow(1)">
                                                <img style="width: 35px" src="images/BrowserImages/Popupclose.png" alt="" /></a></td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="font-size: 21px; font-weight: bold; font-style: normal; text-align: left; font-variant: normal;">Unsupported Browser
                                                                <br />
                                            </td>
                                            <td>&nbsp;</td>
                                            <td style="font-size: 22px; text-align: right; font-weight: bold; font-style: normal; font-variant: normal;">متصفح غير مدعوم
                                                                <br />
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <br />
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="font-size: 15px; font-style: normal; text-align: left; font-variant: normal; color: #FF0000;">As of 13 May 2018, you will not be able to access Bupa Online Services on this browser, unless you are using one of the below browser version. For more details please contact us on <a href="mailto:OS.Experiance@bupa.com.sa">OS.Experiance@bupa.com.sa</a>
                                                <br />
                                            </td>
                                            <td>&nbsp;</td>
                                            <td style="font-size: 15px; text-align: right; font-style: normal; font-variant: normal; color: #FF0000;">اعتباراً من ١٣ مايو ٢٠١٨ لن تتمكن من الوصول إلى الخدمات الإكترونية على هذا المتصفح، يرجى استخدام احد المتصفحات التالية. لمزيد  من التفاصيل الرجاء التواصل معنا على <a href="mailto:OS.Experiance@bupa.com.sa">OS.Experiance@bupa.com.sa</a>
                                                <br />
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                        </tr>
                                        <tr>
                                            <br />
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="font-size: 14px; font-weight: bold; font-style: normal; text-align: left; font-variant: normal;">We have updated our technologies
                                                                <br />
                                                <br />
                                            </td>
                                            <td>&nbsp;</td>
                                            <td style="font-size: 20px; text-align: right; font-weight: bold; font-style: normal; font-variant: normal; word-spacing: 3px">لقد قمنا بتحديث تقنياتنا
                                                                <br />
                                                <br />
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="text-align: left; color: #808080; font-size: 12px;">You're using a browser which did not support TLS 1.2<br />
                                                Please use the following browser which are listed below
                                                                <br />
                                                <br />

                                            </td>
                                            <td>&nbsp;</td>
                                            <td style="text-align: right; color: #808080; font-size: 14px">للأسف متصفحك غير مدعوم من قِبل موقعنا الإلكتروني. نرجوا تحديث / تثبيت أحد
                                                                    .المتصفحات التالية للاستفادة الكاملة من موقعنا
                                                                <br />
                                                <br />
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <table style="width: 100%; background-color: white" cellpadding="0" cellspacing="0">
                                                    <tr>

                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <br />
                                                            <br />
                                                            <br />
                                                        </td>
                                                        <td style="text-align: center">
                                                            <img src="images/BrowserImages/ChromeImage.png" alt="" style="width: 65px;" /><br />
                                                            <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">
                                                                <span style="color: #3399FF; text-align: center; font-size: 12px">Chrome
                                                                                <br />
                                                                </span></a>
                                                            <span style="color: #999999; text-align: center; font-size: 12px">version 33 and above</span>
                                                        </td>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                        <td style="text-align: center">
                                                            <img src="images/BrowserImages/FireFoxImage.png" alt="" style="width: 65px; padding-top: 4px;" /><br />
                                                            <a href="https://www.mozilla.org/en-US/firefox/new" target="_blank">
                                                                <span style="color: #3399FF; text-align: center; font-size: 12px">FireFox
                                                                                <br />
                                                                </span>

                                                            </a>
                                                            <span style="color: #999999; text-align: center; font-size: 12px">version 27 and above</span>
                                                        </td>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                        <td style="text-align: center">
                                                            <img src="images/BrowserImages/IEImage.png" alt="" style="width: 65px;" /><br />
                                                            <a href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads" target="_blank">
                                                                <span style="color: #3399FF; text-align: center; font-size: 12px">Internet Explorer
                                                                                <br />
                                                                </span></a>
                                                            <span style="color: #999999; text-align: center; font-size: 12px">version 11 and above</span>
                                                        </td>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                        <td style="text-align: center">
                                                            <img src="images/BrowserImages/SafariImage.jpg" alt="" style="width: 65px;" /><br />
                                                            <a href="https://support.apple.com/downloads/safari" target="_blank">
                                                                <span style="color: #3399FF; text-align: center; font-size: 12px">Safari
                                                                                <br />
                                                                </span></a>
                                                            <span style="color: #999999; text-align: center; font-size: 12px">version 7 and above</span>
                                                        </td>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                    </tr>
                                                    <tr style="line-height: 20px;">
                                                        <td valign="top" colspan="4" style="text-align: center; padding-bottom: 20px;">
                                                            <br />
                                                            <strong style="color: #FF0000;"><span runat="server" id="Numberdays"></span>&nbsp;days remaining </strong>
                                                        </td>
                                                        <td valign="top" colspan="5" style="text-align: center; padding-bottom: 20px;">
                                                            <br />
                                                            <strong style="color: #FF0000;"><span runat="server" id="ArNumberdays"></span>&nbsp;الأيام المتبقية</strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            
<div class="copyrightContainer">
    <uc2:Footer runat="server" ID="Footer" />
<%--    <uc1:Footer ID="Footer1" runat="server" />
<table width="995" align="center" border="0" cellspacing="0" cellpadding="0" class="footerLinks">
<tr>
        <td class="copyrightContainerPad">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>Copyright © Bupa Arabia 2012. All rights reserved. <a href="http://www.bupa.com.sa/English/Pages/Terms-Conditions.aspx">Terms &amp; Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.bupa.com.sa/English/Pages/Sitemap.aspx">Sitemap</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.bupa.com.sa/English/Pages/Privacy-Policy.aspx">Privacy Policy</a></td>
        		<td align="right">Powered by <a href="http://www.borninteractive.com/">Born Interactive</a></td>
            </tr>
        </table>
        </td>
    </tr>
</table>--%>
		</div>
	</div>
    </td>
   </tr>

</table>
</div>


<script type="text/javascript" src="functions/Login/jquery.js"></script>
<script type="text/javascript" src="functions/Login/jquery.anythingslider.min.js"></script>
<script type="text/javascript" src="functions/Login/cufon.js"></script>
<script type="text/javascript" src="functions/Login/Gotham_Book_400.font.js"></script>
<script type="text/javascript" src="functions/Login/cufon.init.js"></script>
<script type="text/javascript" src="functions/Login/functions.js"></script>
<script type="text/javascript" src="Scripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        function closePasswordExpWindow(value) {
            var ctrl = document.getElementById("<%=popupAttemptLeft.ClientID%>");
         ctrl.style.display = "none";
     }
        function ClosePopWindow(value) {
            var ctrl = document.getElementById("<%=PopupUnSupported.ClientID%>");
            ctrl.style.display = "none";
        }
    </script>
<script type="text/javascript">
    $(document).ready(function () {
        toggleFooter();
    });

    $(function () {
        $('#slider,#Helpslider').anythingSlider();
    });
</script>

    <script language="javascript" type="text/javascript">

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
        });

        prm.add_endRequest(function () {
            $('#entryForm').unblock();
        });

        // Progress indicator preloading.
        var preload = document.createElement('img');
        preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
        delete preload;

    </script>

</form>
<!--Begin Comm100 Live Chat Code
<div id="comm100-button-73"></div>
<script type="text/javascript">
    var Comm100API = Comm100API || new Object;
    Comm100API.chat_buttons = Comm100API.chat_buttons || [];
    var comm100_chatButton = new Object;
    comm100_chatButton.code_plan = 73;
    comm100_chatButton.div_id = 'comm100-button-73';
    Comm100API.chat_buttons.push(comm100_chatButton);
    Comm100API.site_id = 217576;
    Comm100API.main_code_plan = 73;

    var comm100_lc = document.createElement('script');
    comm100_lc.type = 'text/javascript';
    comm100_lc.async = true;
    comm100_lc.src = 'https://chatserver.comm100.com/livechat.ashx?siteId=' + Comm100API.site_id;
    var comm100_s = document.getElementsByTagName('script')[0];
    comm100_s.parentNode.insertBefore(comm100_lc, comm100_s);

    setTimeout(function() {
        if (!Comm100API.loaded) {
            var lc = document.createElement('script');
            lc.type = 'text/javascript';
            lc.async = true;
            lc.src = 'https://hostedmax.comm100.com/chatserver/livechat.ashx?siteId=' + Comm100API.site_id;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(lc, s);
        }
    }, 5000)
</script>
End Comm100 Live Chat Code-->

</body>
</html>
