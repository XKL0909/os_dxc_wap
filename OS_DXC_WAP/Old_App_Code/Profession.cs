﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class Profession
{
    public string Code { get; set; }
    public string ProfessionName { get; set; }
}