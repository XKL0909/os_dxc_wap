﻿using System;
using System.Collections.Generic;
using System.Web;

using log4net;
using Bupa.Core.Logging;
using Utility.Configuration;
using System.Text;
using System.Data;
using Bupa.Core.Logging;

namespace Bupa.OSWeb.Business
{
    public sealed class CaesarRequest
    {
        #region Properties

        public string RefNo { get; set; }

        public string MainMbrNo { get; set; }
        public string MembershipNo { get; set; }
        public string MemberName { get; set; }
        public string DOB { get; set; }
        public string IDType { get; set; }
        public string MaritalStatus { get; set; }
        public string IDCardNo { get; set; }
        public string IDExpDate { get; set; }
        public string ProfessionCode { get; set; }
        public string DistCode { get; set; }
        public string Nationality { get; set; }
        public string MbrMobileNo { get; set; }
        public string RejReason { get; set; }
        public string EffectiveDate { get; set; }
        public string ClasssID { get; set; }
        public string BranchCode { get; set; }
        public string MemberType { get; set; }
        public string SponsorID { get; set; }
        public string MemberTitle { get; set; }
        public string JoinDate { get; set; }
        public string EmployeeNo { get; set; }
        public string DepartmentCode { get; set; }

        
        public string rejCode { get; set; }
        public string rej_Reason { get; set; }
        public string RejDesc { get; set; }

        /*
        public string MbrTypeEffDate { get; set; }

        public string AdjCode { get; set; }
        public string ApprovedGcc { get; set; }
        public string ApprovedVisa { get; set; }
        public string ArrivalDate { get; set; }
        public string AttachDoc { get; set; }

        public string BatchInd { get; set; }

        public string CCHICityCode { get; set; }
        public string CCHIJobCode { get; set; }

        public string ClsID { get; set; }
        public string ContNo { get; set; }

        public string CorrDob { get; set; }
        public string CorrID { get; set; }
        public string CorrName { get; set; }
        public string CorrSex { get; set; }
        public string CorrSponsorID { get; set; }
        public string CorrTitle { get; set; }
        public string CrtDate { get; set; }
        public string CrtUser { get; set; }
        public string CtryName { get; set; }

        public string CustContacted { get; set; }
        public string DocSource { get; set; }


        public string Email { get; set; }
        public string Fax { get; set; }
        public string FaxNotes { get; set; }
        public string FaxNotesAbr { get; set; }

        public string IntNotes { get; set; }
        public string Notes { get; set; }
        public string OutsideKsaInd { get; set; }
        public string PendingDate { get; set; }
        public string PendingFrom { get; set; }
        public string PhoneNo { get; set; }
        public string PrevMbrInd { get; set; }
        public string PrevMbrNo { get; set; }

        public string PrintEmailInd { get; set; }
        public string PrintFaxInd { get; set; }
        public string ProcessDate { get; set; }
        public string ProcessedDate { get; set; }
        public string ProcessedUser { get; set; }



        public string RegDate { get; set; }
        public string RevertInd { get; set; }
        public string SeqNo { get; set; }
        public string Sex { get; set; }
        public string ShipUpdDate { get; set; }
        public string ShipUpdUser { get; set; }
        public string SmsPrefLang { get; set; }
        public string sql_type { get; set; }
        public string SQLTypeName { get; set; }
        public string StaffNo { get; set; }
        public string SubmitBy { get; set; }
        public string SubmitDate { get; set; }
        public string SubmittedDate { get; set; }
        public string SubmittedUser { get; set; }
        public string SubReason { get; set; }
        public string TermDate { get; set; }
        public string TransID { get; set; }
        public string TxnAction { get; set; }
        public string TxnStatus { get; set; }
        public string UpdDate { get; set; }
        public string UpdUser { get; set; }
        public string ValidSponsorID { get; set; }
        public string WorklistID { get; set; }
        public string WorkType { get; set; }
        public string YakenVerifInd { get; set; }
         * */

        #endregion
    }

    public class RequestRejectionReason
    {
        
        public string rejReason { get; set; }
    }
}