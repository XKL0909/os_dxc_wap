﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

using log4net;
using Tracer = Utility.Diagnostic.FileTrace;

namespace Bupa.Web.Security
{
    public class RegistrationApplication
    {
        #region Fields

        private static ILog _log = LogManager.GetLogger(typeof(RegistrationApplication));

        private string _membershipNo;
        private string _iD;
        private string _dependent;
        private string _email;
        private string _mobile;
        private string _password;
        private string _fullName;

        #endregion

        #region Public Properties

        public RegistrationApplication RegisterationBase
        {
            get { return this; }
        }

        public string Membership
        {
            get { return _membershipNo; }

        }
        public string ID
        {
            get { return _iD; }
        }

        public string Dependent
        {
            get { return _dependent; }
        }

        public string Email
        {
            get { return _email; }
        }

        public string Mobile
        {
            get { return _mobile; }
        }

        public string Password
        {
            get { return _password; }
        }

        public string Fullname
        {
            get { return _fullName; }
        }

        #endregion

        #region Ctor

        public RegistrationApplication(string membership, string password, string email, string id, string Mobile, string fullName)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
                _membershipNo = membership;
                _password = password;
                _email = email;
                _iD = id;
                _mobile = Mobile;
                _fullName = fullName;
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }

            Tracer.WriteMemberExit(ref _log);
        }

        #endregion
    }
}