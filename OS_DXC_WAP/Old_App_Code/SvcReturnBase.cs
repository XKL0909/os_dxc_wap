﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;

namespace Bupa.Web.Business.Services
{
    public class SvcReturnBase
    {
        private string _submissionReference;

        private StringCollection _errorDescription;
        private StringCollection _errorCode;

        public string SubmissionReference
        {
            get { return _submissionReference; }
            set { _submissionReference = value; }
        }

        public StringCollection ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }

        public StringCollection ErrorDescription
        {
            get { return _errorDescription; }
            set { _errorDescription = value; }
        }

        public SvcReturnBase()
        {
            _errorDescription = new StringCollection();
            _errorCode = new StringCollection();
        }
    }
}