﻿using System;

using log4net;
using Tracer = Utility.Diagnostic.FileTrace;

namespace Bupa.OSWeb.Business
{
    public class Contract
    {
        #region Fields

        private static ILog _log = LogManager.GetLogger(typeof(Member));

        private string _companyName = string.Empty;
        private DateTime _startDate = DateTime.MinValue;
        private DateTime _endDate = DateTime.MinValue;
        private string _contractNumber = string.Empty;

        #endregion

        #region Properties


        
        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }

       

        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

       

        #endregion

        #region Ctor

        public Contract(string contractNumber)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
                
                _contractNumber = contractNumber;
                Tracer.WriteLine(ref _log, "_contractNumber is: " + _contractNumber);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
            }
            Tracer.WriteMemberExit(ref _log);
        }

        #endregion
    }
}