﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility.Helper;

public class CaesarDataManager
{
    public static List<Profession> GetAllProfessions(string connectionString)
    {
        var professions = new List<Profession>();
        try
        {
            string sql = "select SYS_CODE, EDESC from sy_sys_code where SYS_TYPE Like'PROF%' order by EDESC";
            var result = OracleManager.ExecuteQuery(connectionString, sql);

            professions = result.AsEnumerable().Select(x => new Profession()
            {
                Code = x["SYS_CODE"].ToString().Trim(),
                ProfessionName = x["EDESC"].ToString().Trim(),

            }).ToList();
        }
        catch (Exception)
        {
            throw;
        }
        return professions;
    }

    public static List<District> GetAllDistricts(string connectionString)
    {
        var districts = new List<District>();
        try
        {
            string sql = "select DIST_CODE, EDESC from dc_district where EDESC != 'Reimbursement' order by EDESC";
            var result = OracleManager.ExecuteQuery(connectionString, sql);

            districts = result.AsEnumerable().Select(x => new District()
            {
                Code = x["DIST_CODE"].ToString().Trim(),
                DistrictName = x["EDESC"].ToString().Trim(),

            }).ToList();
        }
        catch (Exception)
        {
            throw;
        }
        return districts;
    }
    
}