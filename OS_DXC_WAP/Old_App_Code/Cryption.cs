﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Collections;

public class Cryption
{
   public static string Encrypt(string  plainText)
    {        
        byte[] initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
        byte[] saltValueBytes = Encoding.ASCII.GetBytes(SaltValue);        
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);        
        PasswordDeriveBytes password = new PasswordDeriveBytes(KeyValue, saltValueBytes, HashAlgorithm, PasswordIterations);        
        byte[] keyBytes = password.GetBytes(KeySize / 8);      
        RijndaelManaged symmetricKey = new RijndaelManaged();        
        symmetricKey.Mode = CipherMode.CBC;       
        ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);        
        MemoryStream memoryStream = new MemoryStream();       
        CryptoStream cryptoStream = new CryptoStream(memoryStream,  encryptor,  CryptoStreamMode.Write);
       
        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);      
        cryptoStream.FlushFinalBlock();
        
        byte[] cipherTextBytes = memoryStream.ToArray();
        memoryStream.Close();
        cryptoStream.Close();
        
        string cipherText = Convert.ToBase64String(cipherTextBytes);
        return cipherText;
    }
    public static string Decrypt (string cipherText)
    {
        
        byte[] initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
        byte[] saltValueBytes = Encoding.ASCII.GetBytes(SaltValue);
        //byte[] cipherTextBytes = Convert.FromBase64String(cipherText);     
        byte[] cipherTextBytes = Convert.FromBase64String(cipherText.Replace(" ", "+"));
        PasswordDeriveBytes password = new PasswordDeriveBytes(KeyValue,  saltValueBytes, HashAlgorithm, PasswordIterations);       
        byte[] keyBytes = password.GetBytes(KeySize / 8);       
        RijndaelManaged symmetricKey = new RijndaelManaged();        
        symmetricKey.Mode = CipherMode.CBC;
       
        ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);       
        MemoryStream memoryStream = new MemoryStream(cipherTextBytes);       
        CryptoStream cryptoStream = new CryptoStream(memoryStream,  decryptor,  CryptoStreamMode.Read);

        byte[] plainTextBytes = new byte[cipherTextBytes.Length];
        int decryptedByteCount = cryptoStream.Read(plainTextBytes,  0,  plainTextBytes.Length);
        
        memoryStream.Close();
        cryptoStream.Close();
        string plainText = Encoding.UTF8.GetString(plainTextBytes,  0,  decryptedByteCount);

        return plainText;
    }


    public static Hashtable GetQueryValue(string queryStringValues)
    {
        Hashtable queryTable = null;
        if (!string.IsNullOrEmpty(queryStringValues))
        {
            string[] queryParameters = queryStringValues.Split('&');
            if(queryParameters.Length > 0)
            {
                queryTable = new Hashtable();
                foreach (string item in queryParameters)
                {
                    string[] items = item.Split('=');
                    if(items.Length == 2 )
                        queryTable.Add(items[0], items[1]);
                }
            }


        }

        return queryTable;
    }

    private const string SaltValue = "s@1tValue";
    private const string HashAlgorithm = "SHA256";
    private const int PasswordIterations = 2;
    private const string InitVector = "@1B2c3D4e5F6g7H8";
    private const int KeySize = 256;
    private const string KeyValue = "aibarABUPA@sppA#";

   

}