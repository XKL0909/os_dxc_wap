﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BO
{
    public class Member
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SponsorID { get; set; }
        public string LvlCover { get; set; }
        public string PersonId { get; set; }
        public string IdType { get; set; }
        public string fileName { get; set; }
        public byte[] fileData { get; set; }
        public bool isDec { get; set; }
        public string IsMain { get; set; }
        public string HijDob { get; set; }
    }
    public class MemberList : List<Member>
    {
    }
}