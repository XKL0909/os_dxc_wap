﻿using Bupa.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for YakeenDetail
/// </summary>
namespace BO
{

    [Serializable]
    public class YakeenDetail
    {
        public YakeenDetail()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string IDNumber { get; set; }   
        public string SponsorID { get; set; }   
        public string ArabicFullName { get; set; }
        public string EnglishFullName { get; set; }
        public DateTime?  DOB { get; set; }
        public Gender Gender { get; set; }
        public string IdExpiryDate { get; set; }
        public string VisaExpiryDate { get; set; } 
        public string NationalityCode { get; set; }  
        public string OccupationCode { get; set; }
        public string OccupationArabic { get; set; }
        public string JobCode { get; set; }
        public string Relationship { get; set; }
        public string ErrorMessage  { get; set; }
        public string ExceptionMessage { get; set; }
        public int SocialStatus { get; set; }

    }
}