﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ControlType
/// </summary>
public enum ControlType
{
	TextBox,
    DropDown,
    CheckBox,
    Radio
}

public enum EmployeeType
{
    Employee,
    Dependent
}

public enum IdType
{
    Saudi,
    NonSaudi
}