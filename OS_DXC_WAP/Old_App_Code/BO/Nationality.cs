﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Nationality
/// </summary>
/// 
namespace BO
{
    public class Nationalities
    {
        public Nationalities()
        {
            //
            // TODO: Add constructor logic here
            //
        }
                public Int32 ID { get; set; }

        public String Code { get; set; }

        public String Nationality { get; set; }

        public Boolean IsGCC { get; set; }

        public String CCHICode { get; set; }

        public String MappedCode { get; set; }
    }
}