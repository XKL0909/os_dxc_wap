﻿using System;
using System.Collections.Generic;
using System.Web;

using log4net;
using Bupa.Core.Logging;
using Utility.Configuration;
using System.Text;
using Bupa.Core.Logging;

namespace Bupa.OSWeb.Business
{
    public class Member
    {
        #region Fields

        private static ILog _log = LogManager.GetLogger(typeof(Member));

        private string _memberName = string.Empty;

        private string _employeeEmail = string.Empty;
        private string _employeeIBAN = string.Empty;
        private string _employeeMobile = string.Empty;

        private string _membershipNumber = string.Empty;
        private string _companyName = string.Empty;
        private string _saudiIqamaID = string.Empty;
        private DateTime _startDate = DateTime.MinValue;
        private DateTime _endDate = DateTime.MinValue;
        private string _gender = string.Empty;
        private DateTime _birthDate = DateTime.MinValue;
        private string _employeeNumber = string.Empty;
        private string _memberType = string.Empty;
        private string _sponsorID = string.Empty;
        private string _status = string.Empty;
        private string _customer = string.Empty;
        private string _classID = string.Empty;
        private string _branchCode = string.Empty;
        private string _nationality = string.Empty;
        private string _title = string.Empty;
        private string _vip = string.Empty;
        private string _mobile = string.Empty;
        private string _setname = string.Empty;
        private string _idtype = string.Empty;
        private DateTime? _idexpirydate = DateTime.MinValue;
        private string _profession = string.Empty;
        private string _district = string.Empty;
        private string _maritalstatus = string.Empty;
        private string _IVF_Prev_Pregnancy = string.Empty;
        private string _MemberTypeDescription = string.Empty;
        private string _ClaimPayToInd = string.Empty;

        private string _contractNumber = string.Empty;

        private string _BankCountry = string.Empty;
        private string _BankName = string.Empty;
        private string _PreauthOpt = string.Empty;

        #endregion

        #region Properties


        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string MemberTypeDescription
        {
            get { return _MemberTypeDescription; }
            set { _MemberTypeDescription = value; }
        }

        public string ClaimPayToInd
        {
            get { return _ClaimPayToInd; }
            set { _ClaimPayToInd = value; }
        }

        public string BankCountry
        {
            get { return _BankCountry; }
            set { _BankCountry = value; }
        }

        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }

        public string Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }


        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }

        public string MembershipNumber
        {
            get { return _membershipNumber; }
            set { _membershipNumber = value; }
        }

        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        public string SaudiIqamaID
        {
            get { return _saudiIqamaID; }
            set { _saudiIqamaID = value; }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        public string EmployeeNumber
        {
            get { return _employeeNumber; }
            set { _employeeNumber = value; }
        }

        public string EmployeeEmail
        {
            get { return _employeeEmail; }
            set { _employeeEmail = value; }
        }

        public string EmployeeMobile
        {
            get { return _employeeMobile; }
            set { _employeeMobile = value; }
        }

        public string EmployeeIBAN
        {
            get { return _employeeIBAN; }
            set { _employeeIBAN = value; }
        }

        public string MemberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }

        public string SponsorID
        {
            get { return _sponsorID; }
            set { _sponsorID = value; }
        }

        public string ClassID
        {
            get { return _classID; }
            set { _classID = value; }
        }

        public string BranchCode
        {
            get { return _branchCode; }
            set { _branchCode = value; }
        }

        public string Nationality
        {
            get { return _nationality; }
            set { _nationality = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string VIP
        {
            get { return _vip; }
            set { _vip = value; }
        }

        public string MOBILE
        {
            get { return _mobile; }
            set { _mobile = value; }
        }


        public string SETNAME
        {
            get { return _setname; }
            set { _setname = value; }
        }

        public string IDType
        {
            get { return _idtype; }
            set { _idtype = value; }
        }

        public DateTime? IDExpiryDate
        {
            get { return _idexpirydate; }
            set { _idexpirydate = value; }
        }

        public string Profession
        {
            get { return _profession; }
            set { _profession = value; }
        }

        public string District
        {
            get { return _district; }
            set { _district = value; }
        }

        public string MaritalStatus
        {
            get { return _maritalstatus; }
            set { _maritalstatus = value; }
        }

        public string IVF_Prev_Pregnancy
        {
            get { return _IVF_Prev_Pregnancy; }
            set { _IVF_Prev_Pregnancy = value; }
        }

        public string PreauthOpt
        {
            get { return _PreauthOpt; }
            set { _PreauthOpt = value; }
        }
        #endregion

        #region Ctor

        public Member(string memberID, string contractNumber)
        {
            Logger.Tracer.WriteMemberEntry();
            try
            {
                _membershipNumber = memberID;
                ////Tracer.WriteLine(ref _log, "_membershipNumber is: " + _membershipNumber);

                _contractNumber = contractNumber;
                ////Tracer.WriteLine(ref _log, "_contractNumber is: " + _contractNumber);
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
            }
            Logger.Tracer.WriteMemberExit();
        }

        #endregion
    }
}