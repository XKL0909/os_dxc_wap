﻿using System;
using log4net;
using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;
using System.Text;
using System.Data;
using OS_DXC_WAP.CaesarWS;

namespace Bupa.OSWeb.Business
{
    public sealed class ContractHelper
    {
        #region Fields

        private static ILog _log = LogManager.GetLogger(typeof(Member));
        private static ServiceDepot_DNService _ws = new ServiceDepot_DNService();

        private string _contractNumber = string.Empty;

        #endregion

        #region Ctor


        public ContractHelper(string contractNumber)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
               
                _contractNumber = contractNumber;

                _ws.Url = Utility.Configuration.CoreConfiguration.Instance.GetConfigValue(WebPublication.Key_CaesarWSURL);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
            }
            Tracer.WriteMemberExit(ref _log);
        }

        #endregion

        #region Public Methods

        //public Member GetContractDetails()
        //{
        //    Tracer.WriteMemberEntry(ref _log);

        //    Contract m = new Contract(_contractNumber);
        //    try
        //    {
        //        OS_DXC_WAP.CaesarWS.ReqContractTypeRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqContractTypeRequest_DN();

                
        //        request.TransactionID = WebPublication.GenerateTransactionID();
        //        request.Username = WebPublication.CaesarSvcUsername;
        //        request.Password = WebPublication.CaesarSvcPassword;
        //        request.ContractNo = _contractNumber;
        //        OS_DXC_WAP.CaesarWS.ReqContractTypeResponse_DN ContractServices = _ws.ReqContractType(request);
        //        if (ContractServices.Status != "0")
        //        {
        //            throw new Exception("Error when getting member details: " + ContractServices.errorMessage);
        //        }
        //        else
        //        {
                    
        //            //m.CompanyName = ContractServices.
        //            //Tracer.WriteLine(ref _log, "m.CompanyName is: " + m.CompanyName);

        //            //m.EndDate = ContractServices.detail[0].;
        //            //Tracer.WriteLine(ref _log, "m.CompanyName is: " + m.CompanyName);

        //            //m.CompanyName = ContractServices.detail[0].;
        //            //Tracer.WriteLine(ref _log, "m.CompanyName is: " + m.CompanyName);

        //            //m.CompanyName = ContractServices.detail[0].customerName;
        //            //Tracer.WriteLine(ref _log, "m.CompanyName is: " + m.CompanyName);

                    

                    
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracer.WriteException(ref _log, ex);
        //        throw;
        //    }

        //    Tracer.WriteMemberExit(ref _log);
        //    return m;
        //}

        public string DeleteMember(Member m, string deleteReasonCode, DateTime effectiveDate , string [] _docs)
        {
            Tracer.WriteMemberEntry(ref _log);

            string caesarReference = string.Empty;
            try
            {
                OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request = new OS_DXC_WAP.CaesarWS.SubTxnRequest_DN();

                request.TransactionID = WebPublication.GenerateTransactionID();
                request.TransactionType = "DELETE MEMBER";
                request.BatchIndicator = "N";

                OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[] mbrdetailarry = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[1];
                mbrdetailarry[0] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                mbrdetailarry[0].sql_type = "SUB_TXN_REC";
                mbrdetailarry[0].ContractNo = m.ContractNumber;
                mbrdetailarry[0].MembershipNo = m.MembershipNumber;
                mbrdetailarry[0].MemberDeleteDate = (DateTime)effectiveDate; //Member Termination Date
                mbrdetailarry[0].Reason = deleteReasonCode;
                mbrdetailarry[0].MbrName = m.MemberName;
                mbrdetailarry[0].SupportDoc = _docs;
                request.detail = mbrdetailarry;
                
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;

                OS_DXC_WAP.CaesarWS.SubTxnResponse_DN deleteMemberService = _ws.SubTxn(request);
                if (deleteMemberService.Status != "0")
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < deleteMemberService.errorID.Length; i++)
                        sb.Append(deleteMemberService.errorMessage[i]).Append("<br/>");

                    throw new Exception("Error when deleting member: " + sb.ToString());
                }
                else
                {
                    // Deletion was successful
                    caesarReference = deleteMemberService.ReferenceNo.ToString();
                }
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }
            Tracer.WriteLine(ref _log, "caesarReference is: " + caesarReference);

            Tracer.WriteMemberExit(ref _log);
            return caesarReference;
        }


       
        #endregion
    }
}