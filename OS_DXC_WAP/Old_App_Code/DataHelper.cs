﻿using Bupa.Core.Logging;
using Bupa.Core.Utilities.Configuration;
using Bupa.Core.Utilities.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace OnlinseServicesDashBoard
{
    public class DataHelper
    {

        public static DataSet GetPreAuthData(string connectionString, DateTime fromDate, DateTime toDate, string contractNumber = "")
        {


            DataSet ds = new DataSet();

            try
            {
                StringBuilder query = new StringBuilder();
                query.AppendLine(@"select 
  --p.cont_no, 
  --e.cont_yymm, p.prov_code, 
  pv.prov_name,
  case when dc.area_code >= 'A' then
    (select rtrim(dc.area_code)||edesc from sy_sys_code where sys_type = 'AREA_CODE' and sys_code = dc.area_code)
  else
    (select edesc from sy_sys_code where sys_type = 'COUNTRY_CODE' and sys_code = dc.area_code)
  end REGION,
  dc.edesc CITY, cl.SCHEME_NAME, m.mbrshp_no, m.name MBR_NAME, m.MBR_MOBILE_NO, m.VIP_CODE, 
  i.SERV_TYPE, p.claim_type,
  --(select edesc from sy_sys_code where sys_type = 'CA_CAT' and sys_code = i.CODE_CAT) CODE_CAT,
  (select edesc from sy_sys_code where sys_type = 'BENEFIT_HEAD' and sys_code = i.ben_head) ben_head,
  (select edesc from sy_sys_code where sys_type = 'BENEFIT_TYPE' and sys_code = cast(substr(i.ben_head,1,1) as char(1))) ben_type,
  e.DIAG_CODE Primary_ICD, i.DIAG_CODE Line_ICD10,
  (select edesc from db_disability where code_type = 'D' and dis_code = i.diag_code) line_ICD_Desc, e.EST_AMT EST_AMT,
  (select edesc from sy_sys_code where sys_type = 'STATUS_PREAUTH' and sys_code = e.STATUS) Preauth_Status,
  p.preauth_id, 
  --e.PREAUTH_EPISODE_ID, 
  to_char(w.RECEIVED_DATE,'YYYY/MM/DD') as RECEIVED_DATE, e.UPD_DATE_ADJUD response_date,
  (select replace(REJ_REASON_DESC,'#',i.REJ_PARAM1) from CA_REJECTION_REASON where REJ_REASON_ID = i.REJ_REASON_ID) rej_reason
  --i.PREAUTH_EPISODE_ITEM_ID, e.PHYSICIANS_NAME, i.serv_code,   i.SEGMENT_PROTOCOL_IND,
  --(select edesc from sy_sys_code where sys_type = 'STATUS_EPISODE_ITEM' and sys_code = i.status) Item_Status,
  ,i.LINE_ITEM_EST_AMT Line_Amount
  --i.LINE_ITEM_NOTES, e.UPD_USER Episode_Update_User
from ca_preauth p, CA_PREAUTH_EPISODE e, CA_PREAUTH_EPISODE_ITEM i, CA_PREAUTH_EPISODE_WORK_ITEM ew, CA_WORK_ITEM w,
  pv_provider pv, dc_district dc, mr_cont_cls cl, mr_member m
where p.prov_code = pv.prov_code and p.crt_date >= date'2014-6-1'
and dc.dist_code = pv.CORR_DIST_CODE
and w.RECEIVED_DATE >= date'" + fromDate.ToString("yyyy-MM-dd") + @"'
and w.RECEIVED_DATE <= date'" + toDate.ToString("yyyy-MM-dd") + @"'
and cl.cont_no = p.cont_no and cl.cont_yymm = e.cont_yymm and cl.cls_id = e.cls_id
and p.PREAUTH_ID = e.PREAUTH_ID
and e.PREAUTH_EPISODE_ID = i.PREAUTH_EPISODE_ID
and e.PREAUTH_EPISODE_ID = ew.PREAUTH_EPISODE_ID and ew.WORK_ITEM_ID = w.WORK_ITEM_ID
and m.cont_no = p.cont_no and m.mbr_no = p.mbr_no and m.cont_no in (" + contractNumber + ")");

                var result = OracleManager.ExecuteQuery(connectionString, query.ToString());
                if (result != null)
                {
                    //get the data rows and do
                    ds.Tables.Add(result);
                }
            }
            catch (Exception ex)
            {
                // Logger.Current.WriteException(ex);
                throw;
            }
            //Logger.Tracer.WriteMemberExit();
            return ds;
        }

        public static DataSet GetReimbursementClaimsData(string connectionString, DateTime fromDate, DateTime toDate, string contractNumber = "")
        {

            DataSet ds = new DataSet();
            try
            {
                StringBuilder query = new StringBuilder();
                query.AppendLine(@"select 
    h.cont_no, 
    h.vou_no, 
  --h.cont_yymm, 
  --h.prov_code, 
    --cl.SCHEME_NAME, 
    m.mbrshp_no, m.name MBR_NAME, m.MBR_MOBILE_NO, m.STAFF_NO,
    CASE 
        when m.branch_name is not null then m.branch_name || ' ' || (select branch_desc from mr_cust_branch where cont_no = m.cont_no and branch_code = m.branch_code)
        when m.branch_name is null then (select branch_desc from mr_cust_branch where cont_no = m.cont_no and branch_code = m.branch_code)
        else '-'
    end as BRANCH_NAME,
    pv.rprov_name, pv.REGION,
    (Select MBR_TYPE_DESC from MR_MBR_RATE_TYPE where MBR_TYPE = m.MBR_TYPE) MBR_RELATION,
    h.claim_type,
  --m.VIP_CODE, m.DOB,
  --h.serv_type, 
  --(select edesc from sy_sys_code where sys_type = 'BENEFIT_HEAD' and sys_code = h.ben_head) Ben_head,
  --(select edesc from sy_sys_code where sys_type = 'STATUS_CLAIM' and sys_code = h.STATUS ) Line_Item_Status,h.status,
  --(select edesc from sy_sys_code where sys_type = 'STATUS_CLAIM' and sys_code = h.STATUS ) Line_Item_Status0,
  --h.diag_code ICD10,
  (select edesc from sy_sys_code where sys_type = 'BENEFIT_TYPE' and sys_code = cast(substr(h.ben_head,1,1) as char(1))) ben_type,
  (select edesc from db_disability where code_type = 'D' and dis_code = h.diag_code) Line_ICD_Desc, h.PRESENT_AMT billed_amt, h.adj_amt Paid_Amount, h.REJ_AMT,
CASE 
     WHEN h.status =  'RJ' THEN 'Reject'
     WHEN h.status =  'AC' THEN 'Accept'
     WHEN h.status =  'RV' THEN 'Reverse'
     WHEN h.status =  'SU' THEN 'Suspense'
     else '-'
END AS Line_Item_Status,
  --h.submit_by,
  (select edesc from sy_sys_code where sys_type = 'CLM_REJ_CODE' and sys_code = h.REJ_REASON) REJ_REASON,
  --(select edesc from sy_sys_code where sys_type = 'PAY_METHOD' and sys_code = h.PAY_METHOD) PAY_METHOD,
  
  --h.CHQ_NO, h.BANK_CODE, h.PAY_ACCT_NO, 
  to_char(h.crt_date,'YYYY/MM/DD') as crt_date,
  to_char(h.incur_date_from,'YYYY/MM/DD') as incur_date_from, 
  to_char(h.PAY_DATE,'YYYY/MM/DD') as  PAY_DATE, 
  --to_char(h.VOU_RCV_DATE,'YYYY/MM/DD') as VOU_RCV_DATE, 
    h.remarks
  from cl_clm_hist_line H
    left join mr_cont_cls cl on cl.cont_no = h.cont_no and cl.cont_yymm = h.cont_yymm and cl.cls_id = h.cls_id
    left join mr_member m on m.cont_no = h.cont_no and m.mbr_no = h.mbr_no
    left join PV_PROV_REIMBUR pv on h.prov_code = pv.rprov_code
  where h.crt_date >= date'" + fromDate.ToString("yyyy-MM-dd") + "' and h.crt_date <= date'" + toDate.ToString("yyyy-MM-dd") + "' and h.submit_by = 'M' and h.cont_no in (" + contractNumber+ ")");



                var result = OracleManager.ExecuteQuery(connectionString, query.ToString());
                if (result != null)
                {
                    //get the data rows and do
                    ds.Tables.Add(result);

                    foreach (DataTable table in ds.Tables)
                    {
                        foreach (DataRow dr in table.Rows)
                        {
                            //if(!string.IsNullOrEmpty(dr["Paid_Amount"].ToString()) && !string.IsNullOrEmpty(dr["billed_amt"].ToString())>0  )
                            //if(int.Parse(dr["Paid_Amount"].ToString())>0 && int.Parse(dr["billed_amt"].ToString())>0  )
                            if (dr["LINE_ITEM_STATUS"].ToString().Equals("Accept"))
                                if (!dr["Paid_Amount"].ToString().Equals(dr["billed_amt"].ToString()))
                                {
                                    dr["Line_Item_Status"] = "PartAccept";
                                }
                            try
                            {
                                object value = dr["PAY_DATE"];
                                if (value == DBNull.Value)
                                {
                                    dr["remarks"] = "Not paid";
                                }
                                else
                                {
                                    DateTime cdt = Convert.ToDateTime(dr["crt_date"].ToString());
                                    DateTime pdt = Convert.ToDateTime(dr["PAY_DATE"].ToString());

                                    int weekendCount = 0;
                                    TimeSpan diff = pdt - cdt;
                                    int days = diff.Days;
                                    for (var i = 0; i < days; i++)
                                    {
                                        var testDate = cdt.AddDays(i);
                                        if (testDate.DayOfWeek == DayOfWeek.Friday)
                                            weekendCount = weekendCount + 1;
                                        if (testDate.DayOfWeek == DayOfWeek.Saturday)
                                            weekendCount = weekendCount + 1;
                                    }

                                    double totalDays = (pdt - cdt).TotalDays;
                                    totalDays = totalDays - weekendCount;
                                    if (totalDays == 0)
                                        totalDays = 1;
                                    dr["remarks"] = totalDays;

                                }
                            }
                            catch (Exception)
                            {


                            }



                        }
                    }


                }

            }
            catch (Exception ex)
            {
                //Logger.Current.WriteException(ex);
                throw;
            }
            // Logger.Tracer.WriteMemberExit();
            return ds;
        }

        public static DataSet GetDirectClaimsData(string connectionString, DateTime fromDate, DateTime toDate, string contractNumber = "")
        {

            DataSet ds = new DataSet();
            try
            {
                StringBuilder query = new StringBuilder();
                query.AppendLine(@"select h.cont_no, h.cont_yymm, h.prov_code, pv.prov_name,
  (select rtrim(dc.area_code)||s.edesc from sy_sys_code s, dc_district dc where sys_type = 'AREA_CODE' and sys_code = dc.area_code
  and dc.dist_code = pv.CORR_DIST_CODE) REGION,
  (select dc.edesc from dc_district dc where dc.dist_code = pv.CORR_DIST_CODE) CITY,
  cl.SCHEME_NAME, m.mbrshp_no, m.name MBR_NAME, m.MBR_MOBILE_NO, m.VIP_CODE, m.DOB,
  h.serv_type, h.claim_type,
  (select edesc from sy_sys_code where sys_type = 'BENEFIT_HEAD' and sys_code = h.ben_head) ben_head,
  (select edesc from sy_sys_code where sys_type = 'BENEFIT_TYPE' and sys_code = cast(substr(h.ben_head,1,1) as char(1))) ben_type,
  h.diag_code Line_ICD,
  (select edesc from db_disability where code_type = 'D' and dis_code = h.diag_code) Line_ICD_Desc, h.PRESENT_AMT billed_amt,
  (select edesc from sy_sys_code where sys_type = 'STATUS_CLAIM' and sys_code = h.STATUS) Line_Item_Status,
  h.vou_no, h.submit_by,
  (select edesc from sy_sys_code where sys_type = 'CLM_REJ_REASON' and sys_code = h.REJ_REASON) REJ_REASON,
  (select edesc from sy_sys_code where sys_type = 'PAY_METHOD' and sys_code = h.PAY_METHOD) PAY_METHOD,
  h.PAY_DATE, h.CHQ_NO, h.BANK_CODE, h.PAY_ACCT_NO, h.batch_id, b.period, h.incur_date_from, to_char(h.crt_date,'YYYY/MM/DD') as crt_date
from cl_clm_hist_line H
left join mr_cont_cls cl on cl.cont_no = h.cont_no and cl.cont_yymm = h.cont_yymm and cl.cls_id = h.cls_id
left join mr_member m on m.cont_no = h.cont_no and m.mbr_no = h.mbr_no
inner join CL_BATCH_HEADER b on h.batch_id = b.batch_id
inner join pv_provider pv on h.prov_code = pv.prov_code
where h.crt_date >= date'" + fromDate.ToString("yyyy-MM-dd") + "' and h.submit_by = 'P' and cl.cont_no=" + contractNumber);



                var result = OracleManager.ExecuteQuery(connectionString, query.ToString());
                if (result != null)
                {
                    //get the data rows and do
                    ds.Tables.Add(result);
                }

            }
            catch (Exception ex)
            {
                //Logger.Current.WriteException(ex);
                throw;
            }
            //Logger.Tracer.WriteMemberExit();
            return ds;
        }

        public static DataSet GetMembersData(string connectionString, DateTime fromDate, DateTime toDate, string contractNumber = "")
        {

            DataSet ds = new DataSet();
            try
            {
                StringBuilder query = new StringBuilder();
                query.AppendLine(@"");
                var result = OracleManager.ExecuteQuery(connectionString, query.ToString());
                if (result != null)
                {
                    //get the data rows and do
                    ds.Tables.Add(result);
                }
                throw new NotImplementedException("Qeury string not provided");
            }
            catch (Exception ex)
            {
                //Logger.Current.WriteException(ex);
                throw;
            }
            //Logger.Tracer.WriteMemberExit();
            return ds;
        }

    }
}