﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

using log4net;
using Tracer = Utility.Diagnostic.FileTrace;

public class RegistrationApplication
{
	#region Fields

        private static ILog _log = LogManager.GetLogger(typeof(RegistrationApplication));

        private string _membershipNo;
        private string _iD;
        private string _dependent;
        private string _email;
        private string _mobile;
        private string _password;
        private string _fullName;
        private string _contract_no;
        private string _channel;
        private string _identity_type;
        private string _membertype;
       

        #endregion

        #region Public Properties

        public RegistrationApplication RegisterationBase
        {
            get { return this; }
        }

        public string Membership
        {
            get { return _membershipNo; }

        }
        public string ID
        {
            get { return _iD; }
        }

        public string Dependent
        {
            get { return _dependent; }
        }

        public string Email
        {
            get { return _email; }
        }

        public string Mobile
        {
            get { return _mobile; }
        }

        public string Password
        {
            get { return _password; }
        }

        public string Fullname
        {
            get { return _fullName; }
        }

        public string contract_no
        {
            get { return _contract_no; }
        }

        public string channel
        {
            get { return _channel; }
        }

        public string identity_type
        {
            get { return _identity_type; }
        }

        public string membertype
        {
            get { return _membertype; }
        }

        #endregion

        #region Ctor

        public RegistrationApplication(string membership, string password, string email, string id, string Mobile, string fullName,
                            string contract_no, string channel, string identity_type, string membertype)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
                _membershipNo = membership;
                _password = password;
                _email = email;
                _iD = id;
                _mobile = Mobile;
                _fullName = fullName;
                _contract_no = contract_no;                 _channel = channel;
                _identity_type = identity_type;
                _membertype = membertype;
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }

            Tracer.WriteMemberExit(ref _log);
        }

        #endregion
}