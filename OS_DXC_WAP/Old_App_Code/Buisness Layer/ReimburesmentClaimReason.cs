﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReimburesmentClaimReason
/// </summary>
public class ReimburesmentClaimReason
{
    private int _id = 0;
    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    private int _reasonID = 0;
    public int ReasonID
    {
        get { return _reasonID; }
        set { _reasonID = value; }
    }

    private int _reimburesmentClaimID = 0;
    public int ReimburesmentClaimID
    {
        get { return _reimburesmentClaimID; }
        set { _reimburesmentClaimID = value; }
    }

    private string _other = string.Empty;
    public string Other
    {
        get { return _other; }
        set { _other = value; }
    }

	public ReimburesmentClaimReason(){}

    public ReimburesmentClaimReason( int ReasonID, int ReimburesmentClaimID, string Other)
    {
        this.ReasonID = ReasonID;
        this.ReimburesmentClaimID = ReimburesmentClaimID;
        this.Other = Other;
    }

    public void AddReimburesmentClaimReason()
    {
        Provider.ConcreteDataAccess().InsertReimburesmentClaimReason(this);
    }
}