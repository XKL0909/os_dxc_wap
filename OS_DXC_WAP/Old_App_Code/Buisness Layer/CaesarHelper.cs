﻿using Bupa.Yakeen.AccessLayer.Models;
using CaesarApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for CaesarHelper
/// </summary>
public class CaesarHelper
{


    public static Person GetYakeenInfo(string Id, string sponsor=null, string hijiri = null)
    {
        Person MyPerson = new Person();

        if (Id.Trim().StartsWith("1"))
        {
            MyPerson = YakeenManager.GetMemberInfoFromYakeen(Id.Trim(), hijiri);
        }
        else if (Id.Trim().StartsWith("2"))
        {
            MyPerson = YakeenManager.GetMemberInfoFromYakeen(Id.Trim(), sponsor.Trim());
        }
        else if (Id.Trim().StartsWith("4"))
        {
            MyPerson = YakeenManager.GetMemberInfoFromYakeen(Id.Trim(), sponsor.Trim());
        }

        return MyPerson;
    }

    public static List<ContMbrTypeListDetail_DN> FilterMemberTypeByContacts(ContMbrTypeListDetail_DN[] detail, string cover)
    {

        //var cover = "152";
        string schems = System.Configuration.ConfigurationManager.AppSettings["ContractSchems"];
        string contractType = Convert.ToString(HttpContext.Current.Session["ContractType"]);
        if (detail.Count() > 0)
        {
            var memberTypes = detail.ToList();
            if (contractType.Trim().ToUpper().Equals("BDRC") || contractType.Trim().ToUpper().Equals("BDSC"))
            {
                var childItem = memberTypes.Where(t => t.mbrTypeDesc.Trim().ToUpper().Equals("CHILD")).SingleOrDefault();
                if (childItem != null)
                {
                    var schemeList = schems.Split(',').ToList<string>();
                    if (schemeList.Contains(cover))
                    {
                        var lstMemberType = memberTypes.Where(t => t.mbrTypeDesc != childItem.mbrTypeDesc);
                        return lstMemberType.ToList();
                    }

                }
            }
            return memberTypes.ToList();
        }
        return null;
    }
}