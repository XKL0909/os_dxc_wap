﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for GeneralHelper
/// </summary>
public class GeneralHelper
{

    public static int GetAgeFromDob(DateTime dob)
    {
        DateTime now = DateTime.Today;
        int age = now.Year - dob.Year;
        if (now < dob.AddYears(age)) age--;

        return age;
    }

    public static string GetJoiningReason(bool? isNewBorn)
    {
        string result = "";
        if (isNewBorn != null && isNewBorn == true)
        {
            result = "RJOIN012";
        }
        else
        {
            result = "RJOIN012";
        }
        return result;
    }

    public static string GetIdTypeFromIdNumber(string idNo)
    {
        string result = "";
        if (idNo.Trim().StartsWith("1"))
        {
            result = "1";
        }
        else if (idNo.Trim().StartsWith("2"))
        {
            result = "2";
        }
        else if (idNo.Trim().StartsWith("3") || idNo.Trim().StartsWith("4") || idNo.Trim().StartsWith("5"))
        {
            result = "4";
        }
        return result;

    }


    public static bool SetDropDown(string code, DropDownList ddl)
    {
        try
        {

            if (!string.IsNullOrEmpty(code))
            {
                ListItem li = ddl.Items.Cast<ListItem>()
                            .Where(x => x.Value.Trim() == code.Trim())
                            .LastOrDefault();
                ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(li.Value));
                ddl.Enabled = false;
                return (li != null);
            }
        }
        catch (Exception)
        {

        }
        return false;
    }

    public static bool SetDropDownContains(string code, DropDownList ddl)
    {
        try
        {

            if (!string.IsNullOrEmpty(code))
            {
                ListItem li = ddl.Items.Cast<ListItem>()
                            .Where(x => x.Value.Trim().Contains(code))
                            .LastOrDefault();
                ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(li.Value));
                ddl.Enabled = false;
                return (li != null);
            }
        }
        catch (Exception ex)
        {
            string str = ex.Message;
        }
        return false;
    }



    public static string ArabicToEnglish(string NameAr, string NameEn)
    {
        string bupa_fullname = "", bupa_arabicname = "";
        bool isArabicInEnglish = false, isEnglishInArabic = false;

        if (!string.IsNullOrWhiteSpace(NameAr))
        {
            if (Regex.IsMatch(NameAr.Trim(), "^[\u0600-\u06FF]+"))
            {
                bupa_arabicname = Regex.Replace(NameAr.Trim(), @"[^ \u0600-\u06FF]+", "");
            }
            else
            {
                bupa_arabicname = Regex.Replace(NameAr.Trim(), @"[^0-9 a-z A-Z]+", "");
                isEnglishInArabic = true;
            }
        }
        if (!string.IsNullOrWhiteSpace(NameEn))
        {
            if (Regex.IsMatch(NameEn.Trim(), "^[\u0600-\u06FF]+"))
            {
                bupa_fullname = Regex.Replace(NameEn.Trim(), @"[^ \u0600-\u06FF]+", "");
                isArabicInEnglish = true;
            }
            else
            {
                bupa_fullname = Regex.Replace(NameEn.Trim(), @"[^0-9 a-z A-Z]+", "");
            }
        }

        if (isArabicInEnglish && isEnglishInArabic)
        {
            string name = bupa_fullname;
            bupa_fullname = bupa_arabicname;
            bupa_arabicname = name;
        }
        else if (isArabicInEnglish && !isEnglishInArabic)
        {
            bupa_arabicname = bupa_fullname;
            bupa_fullname = "";
        }
        else if (isEnglishInArabic && !isArabicInEnglish)
        {
            bupa_fullname = bupa_arabicname;
            bupa_arabicname = "";
        }

        return bupa_fullname.Trim();
    }

    public static int GetProfessionByRelation(string mbrType)
    {
        int result = 0;
        if (mbrType == "C" || mbrType == "SO" || mbrType == "D")
        {
            result = 99402;
        }
        else if (mbrType == "S")
        {
            result = 99401;
        }
        return result;

    }

    public static string GetTitleByRelation(string mbrType)
    {
        string result = "";
        if (mbrType == "C" || mbrType == "SO")
        {
            result = "Mr";
        }
        else if (mbrType == "D")
        {
            result = "Miss";
        }
        else if (mbrType == "S")
        {
            result = "Mrs";
        }
        return result;

    }

    public static int GetMaritialStatusByRelation(string mbrType)
    {
        int result = 0;
        if (mbrType == "C" || mbrType == "SO")
        {
            result = 1;
        }
        else if (mbrType == "D")
        {
            result = 1;
        }
        else if (mbrType == "S")
        {
            result = 2;
        }
        return result;

    }

    public static String GenerateOTP()
    {
        string numbers = "1234567890";
        string characters = numbers;
        int length = 4;
        string otp = string.Empty;
        for (int i = 0; i < length; i++)
        {
            string character = string.Empty;
            do
            {
                int index = new Random().Next(0, characters.Length);
                character = characters.ToCharArray()[index].ToString();
            } while (otp.IndexOf(character) != -1);
            otp += character;
        }
        return otp;
    }

    public static int GenetareVerificationPin(int min, int max)
    {
        RNGCryptoServiceProvider rand = new RNGCryptoServiceProvider();
        uint scale = uint.MaxValue;
        while (scale == uint.MaxValue)
        {
            // Get four random bytes.
            byte[] bytes = new byte[6];
            rand.GetBytes(bytes);

            // Convert that into an uint.
            scale = BitConverter.ToUInt32(bytes, 0);
        }

        // Add min to the scaled difference between max and min.
        return (int)(min + (max - min) * (scale / (double)uint.MaxValue));
    }


    public static string GetFormatDate(string dateValue)
    {
        string[] dateIndex = new string[3];
        if (!string.IsNullOrEmpty(dateValue))
        {
            dateValue = dateValue.Trim();
            dateIndex = dateValue.Split(new Char[] { '-', '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (dateIndex.Length >= 3 && Convert.ToInt32(dateIndex[1]) > 12)
            {
                string[] formats = { "M/d/yyyy", "d/M/yyyy", "M-d-yyyy", "d-M-yyyy", "d-MMM-yy", "d-MMMM-yyyy" };
                for (int i = 0; i < formats.Length; i++)
                {
                    DateTime date;
                    if (DateTime.TryParseExact(dateValue, formats[i], System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out date))
                    {
                        dateValue = date.ToString("dd-MM-yyyy");
                        break;
                    }
                }
            }
        }
        return dateValue;
    }


}