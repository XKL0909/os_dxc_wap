﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmailTracking
/// </summary>
public class EmailTracking
{
    public long TrackingID { get; set; }
    public string ReimburesmentClaimID { get; set; }
    public string EmailType { get; set; }
    public string FromEmail { get; set; }
    public string ToEmail { get; set; }
    public bool IsSend { get; set; }
    public string MailSubject { get; set; }
    public string MailBody { get; set; }
    public DateTime CreatedDate { get; set; }
    public string CreatedBy { get; set; }
    public DateTime UpdatedDate { get; set; }
    public string UpdatedBy { get; set; }
    public string AttachmentFiles { get; set; }
    public string VirtualPath { get; set; }
    public string ActionType { get; set; }
    public string FileNames { get; set; }
    public string ClaimsType { get; set; }
    public string Status { get; set; }
    public string SubmissionType { get; set; }
    public string ServerIP { get; set; }
    public string ClientIP { get; set; }
    public string ClientBrowserDetails { get; set; }
    public string Source { get; set; }


    public EmailTracking() { }

    public void InsertUpdateMailTracking(EmailTracking emailTracking)
    {
        Provider.ConcreteDataAccess().InsertUpdateMailTracking(emailTracking);
    }

}