﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UnifiedPolicy.Services;
using UnifiedPolicy.Services.Dto;

public enum NormalizedType
{
    MemberWithDependent,
    IndividualMember,
    IndividualDependent,
}

public class NormalizeManager
{
    public NormalizeManager()
    {
    }

    public static List<UnifiedRequestDetail> Normalize(int requestID)
    {
        UnifiedRequestDetailService reqDetailService = new UnifiedRequestDetailService();
        //var allRequests = reqDetailService.Get(requestID).OrderBy(t => t.SecSponsorId).ThenBy(t => t.Sponsor).ToList();
        var allRequests = reqDetailService.GetByOrder(requestID).ToList();

        return allRequests;
    }

    public static List<UnifiedRequestDetail> NormalizeAddMemberRequest(int requestID)
    {
        UnifiedRequestDetailService reqDetailService = new UnifiedRequestDetailService();
        //var allRequests = reqDetailService.Get(requestID).OrderBy(t => t.SecSponsorId).ThenBy(t => t.Sponsor).ToList();
        var allRequests = reqDetailService.GetByOrderAddMemberRequest(requestID).ToList();

        return allRequests;
    }

    //public static List<UnifiedRequestDetail> Normalize(int requestID)
    //{
    //    UnifiedRequestDetailService reqDetailService = new UnifiedRequestDetailService();

    //    var allRequests = reqDetailService.Get(requestID);

    //    var allMainMembers = allRequests.Where(t => t.IsMainMemeber == true);

    //    var onlyMainMembers = allMainMembers.Select(t => t.IdNumber);

    //    var mainMemberWithDep = allRequests.Where(t => onlyMainMembers.Contains(t.SecSponsorId)); //depndents with main member

    //    var mainMemberOnlyHavingDep = mainMemberWithDep.Select(t => t.SecSponsorId).Distinct();

    //    var mainMemberHavingDep = allMainMembers.Where(t => mainMemberOnlyHavingDep.Contains(t.IdNumber));//main member with dependent


    //    var singleMembers = allMainMembers.Except(mainMemberHavingDep); //Main member without dependents

    //    var allDepends = allRequests.Where(t => t.SecSponsorId != null);

    //    // var singleDependents = allDepends.Except(mainMemberWithDep); //Dependents withput main member

    //    var singleDependents = allRequests.Where(t => t.IsIndDep == true  && (t.MemberType !="E" || t.MemberType != "FE" || t.MemberType != "SE"));

    //    List <UnifiedPolicy.Services.Dto.UnifiedRequestDetail> empWithDep = new List<UnifiedRequestDetail>();

    //    foreach (var mainM in mainMemberHavingDep)
    //    {
    //        if (mainM != null)
    //        {
    //            empWithDep.Add(mainM);
    //            var deps = mainMemberWithDep.Where(t => t.SecSponsorId == mainM.IdNumber);
    //            if (deps != null)
    //            {
    //                foreach (var dep in deps)
    //                {
    //                    empWithDep.Add(dep);
    //                }

    //            }

    //        }
    //    }


    //    var all = empWithDep.Union(singleMembers).Union(singleDependents).OrderBy(x => x.Sponsor).ToList();
    //    return all;
    //}





}