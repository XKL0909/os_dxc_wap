﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for CurrencyControl
/// </summary>
public class CurrencyControl
{
	public CurrencyControl(){}

    public List<Currency> GetAllCurrencies() { 
        List<Currency> MyList = new List<Currency>();
        IDataReader Idr = Provider.ConcreteDataAccess().GetAllCurrencies();
        Currency MyCurrency;

        while(Idr.Read()){
            MyCurrency = new Currency();
            MyCurrency.ID = int.Parse(Idr["ID"].ToString());
            MyCurrency.Name = Idr["Name"].ToString();
            MyCurrency.Code = Idr["Code"].ToString();
            MyCurrency.NameCode = Idr["Name"].ToString() + " - " + Idr["Code"];
            MyList.Add(MyCurrency);
        }
        Idr.Close();
        return MyList;
    }
}