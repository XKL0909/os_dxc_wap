﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for ReasonControl
/// </summary>
public class ReasonControl
{
	public ReasonControl(){}

    public List<Reason> GetAllReasons()
    {
        List<Reason> MyList = new List<Reason>();
        IDataReader Idr = Provider.ConcreteDataAccess().GetAllReasons();
        Reason MyReason;
        while(Idr.Read()){
            MyReason = new Reason();
            MyReason.ID = int.Parse(Idr["ID"].ToString());
            MyReason.Name = Idr["Name"].ToString();
            MyList.Add(MyReason);
        }
        Idr.Close();
        return MyList;
    }
}