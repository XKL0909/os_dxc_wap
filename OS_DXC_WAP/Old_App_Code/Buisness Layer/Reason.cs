﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Reason
/// </summary>
public class Reason
{
    private int _id = 0;
    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    private string _name = string.Empty;
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

	public Reason(){}

    public Reason(int ID,string Name)
    {
        this.ID = ID;
        this.Name = Name;
    }
}
