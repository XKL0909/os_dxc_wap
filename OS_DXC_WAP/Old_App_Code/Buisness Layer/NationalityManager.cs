﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BO;
using DAL;
/// <summary>
/// Summary description for NationalityManager
/// </summary>
public class NationalityManager
{
    public static List<Nationalities> Get()
    {
        return DAL.NationalityDB.Get(false);
    }
}