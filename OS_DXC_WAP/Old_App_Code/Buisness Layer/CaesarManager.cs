﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CaesarApi;
using UnifiedPolicy.Services.Dto;
using System.Text;
using System.Globalization;
using UnifiedPolicy.Services;
using OnlineServices.CCHI.Dto;
/// <summary>
/// Summary description for CaesarManager
/// </summary>
/// 
namespace Caesar
{
    public class CaesarManager
    {
        private static string UserName = System.Configuration.ConfigurationManager.AppSettings["CaesarSvc Username"];
        private static string Password = System.Configuration.ConfigurationManager.AppSettings["CaesarSvc Password"];
        List<Error> errors = new List<Error>();

        private static long GenerateTransactionID()
        {

            long txnID;
            try
            {
                Random random = new Random(Guid.NewGuid().GetHashCode());
                int seed = unchecked(DateTime.Now.Ticks.GetHashCode());

                Random rnd = new Random();

                int intFirst = rnd.Next(1, 9);
                int intSecond = rnd.Next(0, 9);
                int intThird = rnd.Next(0, 9);
                int intLast = rnd.Next(0, 9);

                string unparsed = intFirst.ToString() + intSecond.ToString() + intThird.ToString() +
                    DateTime.Now.ToString("MMddHHmmssf") + intLast.ToString(); //+ rnd.Next(9999).ToString();
                ////Tracer.WriteLine(ref _log, "unparsed is: " + unparsed);

                txnID = long.Parse(unparsed);
            }
            catch (Exception e)
            {

                throw e;
            }
            return txnID;
        }

        public static EnqContInfoResponse_DN GetMemberTypeByCover(string clientId)
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();

            var request = new EnqContInfoRequest_DN();
            request.contNo = Convert.ToString(clientId);
            request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
            request.Username = UserName;
            request.Password = Password;

            return Client.EnqContInfo(request);
        }


        public static EnqContMbrTypeListResponse_DN GetMemberTypeByCover(int levelCover, string memberType, string contractNo)
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            EnqContMbrTypeListRequest_DN request = new EnqContMbrTypeListRequest_DN();
            EnqContMbrTypeListResponse_DN response = new EnqContMbrTypeListResponse_DN();

            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            request.contNo = contractNo;
            request.contCls = levelCover;
            request.grp_Type = memberType;
            response = Client.EnqContMbrTypeList(request);

            return response;
        }

        public static MbrDetail_DN[] GetMembership(string ClientID, string membership)
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            EnqMbrDetInfoRequest_DN request = new EnqMbrDetInfoRequest_DN();
            EnqMbrDetInfoResponse_DN response = new EnqMbrDetInfoResponse_DN();
            request.membershipNo = membership.Trim();
            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            EnqMbrDetInfoResponse_DN memberService = Client.EnqMbrDetInfo(request);

            if (memberService != null && memberService.detail != null && memberService.detail.Count() > 0)
            {
                if (memberService.detail[0].membershipNo != "")
                {
                    if (memberService.detail[0].custNo.Trim() == ClientID.Trim())
                    {

                    }
                    else
                    {
                        memberService.detail[0].membershipNo = "";
                    }
                }
            }
            else
            {
                memberService.detail = null;
            }


            return memberService.detail;
        }

        public static DistListDetail_DN[] GetDistrict()
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            ReqDistListRequest_DN request = new ReqDistListRequest_DN();
            ReqDistListResponse_DN response = new ReqDistListResponse_DN();

            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            response = Client.ReqDistList(request);
            DistListDetail_DN[] MyDistricts = new DistListDetail_DN[response.detail.Length];
            response.detail.CopyTo(MyDistricts, 0);
            for (int i = 0; i < response.detail.Count(); i++)
            {
                response.detail[i].distCode = response.detail[i].distCode;
                response.detail[i].distName = response.detail[i].distName.Trim();
            }
            return response.detail;


        }

        public static ClsListDetail_DN[] GetClass(string ClientID)
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            ReqClsListRequest_DN request = new ReqClsListRequest_DN();
            ReqClsListResponse_DN response;
            request.contractNo = long.Parse(ClientID);
            request.Username = UserName;
            request.Password = Password;
            request.transactionID = GenerateTransactionID();
            response = Client.ReqClsList(request);
            for (int i = 0; i < response.detail.Count(); i++)
            {
                response.detail[i].classID = response.detail[i].classID;
                response.detail[i].className = response.detail[i].className.Trim();
            }
            return response.detail;


        }

        public static EnqSponsorIDListDtl_DN[] GetSponsor(string contNo)
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            EnqSponsorIDListRequest_DN request = new EnqSponsorIDListRequest_DN();
            EnqSponsorIDListResponse_DN response = new EnqSponsorIDListResponse_DN();
            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            request.contNo = contNo;
            response = Client.EnqSponsorIDList(request);
            for (int i = 0; i < response.detail.Count(); i++)
            {
                response.detail[i].sponsorID = response.detail[i].sponsorID.Trim();
            }
            return response.detail;
        }

        public static MbrTypeListDetail_DN[] GetEmployeeMemberType(string type)
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            ReqMbrTypeListRequest_DN request = new ReqMbrTypeListRequest_DN();
            ReqMbrTypeListResponse_DN response = new ReqMbrTypeListResponse_DN();
            request.TransactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            request.grp_Type = type;
            response = Client.ReqMbrTypeList(request);
            for (int i = 0; i < response.detail.Count(); i++)
            {
                response.detail[i].mbrType = response.detail[i].mbrType.Trim();
                response.detail[i].mbrTypeDesc = response.detail[i].mbrTypeDesc.Trim();
            }
            return response.detail;

        }

        public static MbrTypeListDetail_DN[] GetDependentMemberType()
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            ReqMbrTypeListRequest_DN request = new ReqMbrTypeListRequest_DN();
            ReqMbrTypeListResponse_DN response = new ReqMbrTypeListResponse_DN();
            request.TransactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            request.grp_Type = "D";
            response = Client.ReqMbrTypeList(request);
            for (int i = 0; i < response.detail.Count(); i++)
            {
                response.detail[i].mbrType = response.detail[i].mbrType.Trim();
                response.detail[i].mbrTypeDesc = response.detail[i].mbrTypeDesc.Trim();
            }
            return response.detail;

        }

        public static ProfListDetail_DN[] GetProfession()
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            ReqProfListRequest_DN request = new ReqProfListRequest_DN();
            ReqProfListResponse_DN response = new ReqProfListResponse_DN();
            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            response = Client.ReqProfList(request);
            for (int i = 0; i < response.detail.Count(); i++)
            {
                response.detail[i].profName = response.detail[i].profName.Trim();
                response.detail[i].profCode = response.detail[i].profCode.Trim();
            }
            return response.detail;
        }

        public static BrhListDetail_DN[] GetBranch(string contNo)
        {
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            ReqBrhListRequest_DN request = new ReqBrhListRequest_DN();
            ReqBrhListResponse_DN response = new ReqBrhListResponse_DN();
            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            request.membershipNo = long.Parse(contNo);
            response = Client.ReqBrhList(request);
            for (int i = 0; i < response.detail.Count(); i++)
            {
                response.detail[i].branchCode = response.detail[i].branchCode.Trim();
                response.detail[i].branchDesc = response.detail[i].branchDesc.Trim();
            }
            return response.detail;
        }

        protected bool SupportDocument(string ClientID)
        {
            bool _required = false;

            if (WebIndecator("WebSupDocInd", ClientID))
            {
                _required = true;
            }
            else
            {
                _required = false;
            }
            return _required;
        }
        public static bool WebIndecator(string _type, string _contract)
        {
            bool _mandatory = false;
            RequestWebIndicatorsRequest_DN request = new RequestWebIndicatorsRequest_DN();
            RequestWebIndicatorsResponse_DN response = new RequestWebIndicatorsResponse_DN();
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            request.contractNo = _contract;
            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;

            response = Client.RequestWebIndicators(request);
            if (response.status != "0")
            {
                HttpContext.Current.Response.Write("Error when getting details: " + response.errorMessage[0]);

            }
            else
            {
                switch (_type)
                {
                    case "WebSupDocInd":
                        if (response.webSupDocInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;
                    case "StaffNoInd":
                        if (response.staffNoInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;
                    case "DeptCodeInd":
                        if (response.deptCodeInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;
                    case "DepdIdInd":
                        if (response.depdIdInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;
                    case "SuportDocs":
                        if (response.webSupDocInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;

                }

            }
            return _mandatory;
        }

        public List<Error> Push(List<UnifiedRequestDetail> Detail)
        {
            UnifiedFileuploadService uploadService = new UnifiedFileuploadService();
            CultureInfo culture = new CultureInfo("en-GB");

            SubTxnRequest_DN request = new SubTxnRequest_DN();
            ServiceDepot_DNService Client = new ServiceDepot_DNService();

            request.TransactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;

            request.detail = new AllMbrDetail_DN[Detail.Count];


            if (Detail.Count == 1)
                request.BatchIndicator = "N";
            else
                request.BatchIndicator = "Y";

            request.TransactionType = "ADD MEMBER02";
            int x = 0;
            foreach (var item in Detail)
            {
                request.detail[x] = new AllMbrDetail_DN();
                request.detail[x].ContractNo = item.ContNumber;
                request.detail[x].BranchCode = item.BranchCode;
                request.detail[x].MbrName = item.Name;
                
                request.detail[x].DOB = formatDate(item.DOB);
                request.detail[x].ClassID = item.Cover;


                request.detail[x].DeptCode = item.Department;

                request.detail[x].IgamaID = item.IdNumber;
                request.detail[x].ID_Type = item.IdType;

                if (!string.IsNullOrEmpty(item.IDExpiry))
                    request.detail[x].ID_Expiry_Date = DateTime.Parse(item.IDExpiry, culture);
                //change
                if (!string.IsNullOrEmpty(item.MbrshipNo))
                {
                    request.detail[x].MainMemberNo = item.MbrshipNo;
                }
                request.detail[x].SponsorID = item.Sponsor;
                if (!string.IsNullOrEmpty(item.SecSponsorId))
                {
                    request.detail[x].SponsorID = item.SecSponsorId;
                }
                //change


                request.detail[x].StaffNo = item.EmployeeNo;
                // request.detail[x].Nationality = item.Nationality.Trim();
                request.detail[x].Nationality = GetNationalityValue(item.Nationality.Trim());
                request.detail[x].Gender = item.Gender;

                if (!string.IsNullOrEmpty(item.PolicyStartOn))
                    request.detail[x].RequestStartDate = DateTime.Parse(item.PolicyStartOn, culture);

                if (!string.IsNullOrEmpty(item.PolicyStartOn))
                {
                    request.detail[x].EffectiveDate = DateTime.Parse(item.PolicyStartOn, culture);
                }

                if (!string.IsNullOrEmpty(item.ArrivalOn))
                    request.detail[x].ArrivalDate = DateTime.Parse(item.ArrivalOn, culture);

                if (!string.IsNullOrEmpty(item.JoinOn))
                {
                    request.detail[x].JoinDate = DateTime.Parse(item.JoinOn, culture);
                }

                request.detail[x].PreviousMembershipIndicator = (item.IsReturned == true) ? "Y" : "N";
                request.detail[x].PreviousMembershipNo = item.PrevMembership;
                request.detail[x].Reason = item.JoiningReason;
                request.detail[x].Title = item.Title;
                request.detail[x].MemberType = item.MemberType;
                request.detail[x].MBR_MOBILE_NO = item.MobileNo;
                request.detail[x].SMS_PREF_LANG = string.Empty;
                request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                request.detail[x].CCHI_City_Code = item.District;
                request.detail[x].CCHI_Job_Code = item.Profession;


                request.detail[x].YakeenVerified = (item.IsYakeen == true) ? "Y" : "N";

                if (!string.IsNullOrEmpty(item.MaritalStatus))
                    request.detail[x].Marital_Status = item.MaritalStatus;


                if (SupportDocument(item.ContNumber))
                {
                    var docs = uploadService.FindByRequestID(item.Id);
                    if (docs.Count > 0)
                    {
                        string[] SupportDocs = new string[docs.Count];
                        int i = 0;
                        foreach (var doc in docs)
                        {
                            //SupportDocs[i] = DocLocation() + doc.Name;
                            SupportDocs[i] = DocLocation(Convert.ToDateTime(doc.CreatedOn)) + doc.Name;
                            i++;
                        }
                        request.detail[x].SupportDoc = SupportDocs;
                    }
                }



                x++;
            }
            if (Detail != null && Detail.Count > 0)
            {
                CallServiceAddMember(request, Client);
            }
            else
            {
                errors.Add(new Error { Index = j, Id = int.Parse("0"), Type = "Error", Messege = "No, Records Found" });
            }


            return errors;
        }

        public List<Error> PushAddMember(List<UnifiedRequestDetail> Detail)
        {
            ////UnifiedFileuploadService uploadService = new UnifiedFileuploadService();
            AddMemberFileuploadService addMemberFileuploadService = new AddMemberFileuploadService();
            CultureInfo culture = new CultureInfo("en-GB");

            SubTxnRequest_DN request = new SubTxnRequest_DN();
            ServiceDepot_DNService Client = new ServiceDepot_DNService();

            request.TransactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;

            request.detail = new AllMbrDetail_DN[Detail.Count];


            if (Detail.Count == 1)
                request.BatchIndicator = "N";
            else
                request.BatchIndicator = "Y";

            request.TransactionType = "ADD MEMBER02";
            int x = 0;
            int mainEmployeeRecordId = 0;
            if (Detail != null && Detail.Count > 0)
            {
                bool isAttachment = SupportDocument(Detail[0].ContNumber);
                foreach (var item in Detail)
                {
                    request.detail[x] = new AllMbrDetail_DN();
                    request.detail[x].ContractNo = item.ContNumber;
                    request.detail[x].BranchCode = item.BranchCode;
                    request.detail[x].MbrName = item.Name;

                    request.detail[x].DOB = formatDate(item.DOB);
                    request.detail[x].ClassID = item.Cover;


                    request.detail[x].DeptCode = item.Department;

                    request.detail[x].IgamaID = item.IdNumber;
                    request.detail[x].ID_Type = item.IdType;

                    if (!string.IsNullOrEmpty(item.IDExpiry))
                        request.detail[x].ID_Expiry_Date = DateTime.Parse(item.IDExpiry, culture);
                    //change
                    if (!string.IsNullOrEmpty(item.MbrshipNo))
                    {
                        request.detail[x].MainMemberNo = item.MbrshipNo;
                    }
                    request.detail[x].SponsorID = item.Sponsor;
                    if (!string.IsNullOrEmpty(item.SecSponsorId))
                    {
                        request.detail[x].SponsorID = item.SecSponsorId;
                    }
                    //change


                    request.detail[x].StaffNo = item.EmployeeNo;
                    // request.detail[x].Nationality = item.Nationality.Trim();
                    request.detail[x].Nationality = GetNationalityValue(item.Nationality.Trim());
                    request.detail[x].Gender = item.Gender;

                    if (!string.IsNullOrEmpty(item.PolicyStartOn))
                    {
                        request.detail[x].RequestStartDate = DateTime.Parse(item.PolicyStartOn, culture);
                        request.detail[x].EffectiveDate = DateTime.Parse(item.PolicyStartOn, culture);
                        request.detail[x].ArrivalDate = DateTime.Parse(item.PolicyStartOn, culture);
                        request.detail[x].JoinDate = DateTime.Parse(item.PolicyStartOn, culture);
                    }
                    request.detail[x].PreviousMembershipIndicator = (item.IsReturned == true) ? "Y" : "N";
                    request.detail[x].PreviousMembershipNo = item.PrevMembership;
                    request.detail[x].Reason = item.JoiningReason;
                    request.detail[x].Title = item.Title;
                    request.detail[x].MemberType = item.MemberType;
                    request.detail[x].MBR_MOBILE_NO = item.MobileNo;
                    request.detail[x].SMS_PREF_LANG = string.Empty;
                    request.detail[x].sql_type = "CSR.SUB_TXN_REC";
                    request.detail[x].CCHI_City_Code = item.District;
                    request.detail[x].CCHI_Job_Code = item.Profession;


                    request.detail[x].YakeenVerified = (item.IsYakeen == true) ? "Y" : "N";

                    if (!string.IsNullOrEmpty(item.MaritalStatus))
                        request.detail[x].Marital_Status = item.MaritalStatus;


                    if (isAttachment)
                    {
                    
                        if (item.RequestType == "ED")
                        {
                            if (item.MemberType == "E" || item.MemberType == "FE" || item.MemberType == "SE")
                                mainEmployeeRecordId = item.Id;

                        }
                        else
                            mainEmployeeRecordId = item.Id;
                        var docs = addMemberFileuploadService.FindByRequestID(mainEmployeeRecordId);
                        if (docs.Count > 0)
                        {
                            string[] SupportDocs = new string[docs.Count];
                            int i = 0;
                            foreach (var doc in docs)
                            {
                                //SupportDocs[i] = DocLocation() + doc.Name;
                                SupportDocs[i] = DocLocation(Convert.ToDateTime(doc.CreatedOn)) + doc.Name;
                                i++;
                            }
                            request.detail[x].SupportDoc = SupportDocs;
                        }
                    }
                    x++;
                }
                if (request.detail.Count() > 0)
                    CallServiceAddMember(request, Client);
            }
            else
            {
                errors.Add(new Error { Index = j, Id = int.Parse("0"), Type = "Error", Messege = "No, Records Found" });
            }

            return errors;
        }
        private DateTime formatDate(string date)
        {
            DateTime newDAteFormatted = new DateTime();
            if (!string.IsNullOrEmpty(date)) { 
            string datetosplit = date;
            string[] splitdate = datetosplit.Split('-');
            int yyyy = 0;
            Int32.TryParse(splitdate[2], out yyyy);
            int mm = 0;
            Int32.TryParse(splitdate[1], out mm);
            int dd = 0;
            Int32.TryParse(splitdate[0], out dd);
            newDAteFormatted = new DateTime(yyyy, mm, dd);
            return newDAteFormatted;
            }
            return newDAteFormatted;
        }
       private string GetNationalityValue(string value)
        {
            char[] delimiters = new char[] { '-' };
            string[] parts = value.Split(delimiters,
                            StringSplitOptions.RemoveEmptyEntries);
            return parts[0];

        }

        public string DocLocation(DateTime date)
        {
            string _path = "";
            string strYear = date.Year.ToString();
            string strMonth = date.Month.ToString();
            string strDay = date.Day.ToString();

            _path = strYear + @"\" + strMonth + @"\" + strDay + @"\";
            return _path;
        }

        public string DocLocation()
        {
            string _path = "";
            string strYear = System.DateTime.Today.Year.ToString();
            string strMonth = System.DateTime.Today.Month.ToString();
            string strDay = System.DateTime.Today.Day.ToString();

            _path = strYear + @"\" + strMonth + @"\" + strDay + @"\";
            return _path;
        }

        int j = 0;
        public void CallServiceAddMember(SubTxnRequest_DN request, ServiceDepot_DNService Client)
        {
            StringBuilder sb = new StringBuilder();

            SubTxnResponse_DN response = new SubTxnResponse_DN();
            request.Username = UserName;
            request.Password = Password;

            try
            {
                string strrequest = request.Serialize();
                response = Client.SubTxn02(request);
                if (response.Status != "0")
                {
                    string er = "";
                    for (int i = 0; i < response.errorMessage.Length; i++)
                    {
                        // sb.Append("Error:" + response.errorMessage[i]).Append("<br/>");
                        er += response.errorMessage[i];
                    }
                    errors.Add(new Error { Index = j, Id = 0, Type = "Error", Messege = er });
                }
                else
                {
                    sb.Append(response.ReferenceNo.ToString());
                    errors.Add(new Error { Index = j, Id = int.Parse(response.ReferenceNo.ToString()), Type = "Success", Messege = "Success" });
                }
            }
            catch (Exception ex)
            {
                errors.Add(new Error { Index = j, Id = int.Parse(response.ReferenceNo.ToString()), Type = "Error", Messege = ex.Message });
            }

            j++;
        }

        public Indicator GetMandatoryIndicator(string contractNumber)
        {

            EnqContInfoRequest_DN request = new EnqContInfoRequest_DN();
            EnqContInfoResponse_DN response = new EnqContInfoResponse_DN();
            ServiceDepot_DNService Client = new ServiceDepot_DNService();
            request.contNo = contractNumber;
            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            response = Client.EnqContInfo(request);
            Indicator indicator = new Indicator();

            if (response != null && response.status == "0")
            {
                indicator.EmployeeNumberMandatory = Convert.ToString(response.mandStaffNoInd);
                indicator.DepartmentMandatory = Convert.ToString(response.mandDeptCodeInd);
                indicator.AttachmentMandatory = Convert.ToString(response.mandIDNoInd);
            }

            return indicator;
        }

        public bool GetMemberExistsIndicator(string idNumber, string contractNumber)
        {

            EnqExistMbrIDRequest_DN request = new EnqExistMbrIDRequest_DN();
            EnqExistMbrIDResponse_DN response = new EnqExistMbrIDResponse_DN();
            ServiceDepot_DNService Client = new ServiceDepot_DNService();

            string idType = idNumber.StartsWith("2") ? "2" : idNumber.StartsWith("1") ? "1" : "4";

            request.contNo = contractNumber;
            request.IDNo = idNumber;
            request.IDType = idType;
            request.transactionID = GenerateTransactionID();
            request.Username = UserName;
            request.Password = Password;
            response = Client.EnqExistMbrID(request);
            bool isExists = false;
            string idNumberExists = Convert.ToString(response.existMbrInd);
            isExists = string.IsNullOrEmpty(idNumberExists) ? false : idNumberExists == "Y" ? true : false;

            return isExists;
        }


        public Member GetMemberDetails(string membershipNumber, string contractNo)
        {
            Member m = new Member();
            try
            {

                EnqMbrDetInfoRequest_DN request = new EnqMbrDetInfoRequest_DN();
                EnqMbrDetInfoResponse_DN response = new EnqMbrDetInfoResponse_DN();
                ServiceDepot_DNService Client = new ServiceDepot_DNService();


                request.membershipNo = membershipNumber;
                request.transactionID = GenerateTransactionID();
                request.Username = UserName;
                request.Password = Password;
                response = Client.EnqMbrDetInfo(request);

                m.Status = "Failed";

                if (response.errorID != "0")
                {

                    if (response.errorMessage.Trim() == "Member not active.")
                    {
                        m.ErrrorMessage = "Invalid member number / " + response.errorMessage;
                    }
                    else
                    {
                        m.ErrrorMessage = "Error when getting member details: " + response.errorMessage;
                    }
                }
                else
                {
                    if (response.detail != null && response.detail.Count() > 0)
                    {
                        m.MemberName = response.detail[0].memberName;
                        m.SponsorID = response.detail[0].sponsorID;
                        m.MembershipNumber = response.detail[0].membershipNo.ToString();
                        m.IDCardNo = response.detail[0].memberID;
                        m.Status = "Success";
                        m.ClassID = response.detail[0].ClassID;
                        m.EmployeeNumber = response.detail[0].employeeNo;
                        m.BranchCode = response.detail[0].BranchCode;
                        m.IdExpiryDate = response.detail[0].endDate;
                        m.District = response.detail[0].distCode;
                        m.MobileNumber = response.detail[0].mobileNo;
                        m.InsuranceEffectiveDate = response.detail[0].startDate;
                       




                    }
                    else
                        m.ErrrorMessage = "Error when getting member details: " + response.errorMessage;


                }
            }
            catch (Exception ex)
            {

                m.ErrrorMessage = ex.Message.ToString();

            }

            return m;

        }

        public static string GetSMEDependentScheme(string contractNo, string mainMemberCover)
        {
            string schemeCode = "0";
            string className = string.Empty;
            string[] classNames;
            try
            {
                if (!string.IsNullOrEmpty(contractNo) && !string.IsNullOrEmpty(mainMemberCover))
                {
                    schemeCode = mainMemberCover;                   
                    ClsListDetail_DN[] lClassDetails = CaesarManager.GetClass(contractNo);

                    if (lClassDetails != null && lClassDetails.Count() > 0)
                    {

                        var mainClass = lClassDetails.Where(x => x.classID == Convert.ToInt64(mainMemberCover)).Single();

                        if (mainClass != null)
                        {
                            className = mainClass.className;


                            if (!string.IsNullOrEmpty(className))
                            {
                                //// Remove (Senior  Dot dot)  from scheme name and set the related scheme on 16-Jul-2018 by Sakthi
                                //// Start 
                                className = RemoveSpecialCoverName(className);
                                 ////End 
                                 classNames = className.Split('-');
                                if (classNames.Length > 0)
                                {
                                    className = classNames[classNames.Length - 1].Trim();
                                    if (!string.IsNullOrEmpty(className))
                                        className += " (S)";
                                }

                                var dependentClass = lClassDetails.Where(x => x.className.Contains(className)).FirstOrDefault();
                                if (dependentClass != null)
                                    schemeCode = Convert.ToString(dependentClass.classID);


                            }
                        }

                    }



                }
            }
            catch (Exception ex)
            {
            }
            return schemeCode;
        }

        public static string GetRegularScheme(string contractNo, string mainMemberCover)
        {
            string schemeCode = "0";
            string className = string.Empty;
            string[] classNames;
            try
            {
                if (!string.IsNullOrEmpty(contractNo) && !string.IsNullOrEmpty(mainMemberCover))
                {
                    schemeCode = mainMemberCover;
                    ClsListDetail_DN[] lClassDetails = CaesarManager.GetClass(contractNo);

                    if (lClassDetails != null && lClassDetails.Count() > 0)
                    {

                        var mainClass = lClassDetails.Where(x => x.classID == Convert.ToInt64(mainMemberCover)).Single();

                        if (mainClass != null)
                        {
                            className = mainClass.className;


                            if (!string.IsNullOrEmpty(className))
                            {
                                classNames = className.Split('-');
                                var tempClsNames = classNames[1].Trim().Split(' ');
                                className = tempClsNames[1].Trim();
                                if (!string.IsNullOrEmpty(className))
                                {
                                    var dependentClass = lClassDetails.Where(x => x.className.Contains(className.Trim())).FirstOrDefault();
                                    if (dependentClass != null)
                                    {
                                        schemeCode = dependentClass.classID.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return schemeCode;
        }

        //// Senior scheme issue with dependent on 16-Jul-2018 by Sakthi
        //// Start
        #region Senior scheme
        public static string RemoveSpecialCoverName(string className)
        {
            var formatClassName = className;
            try
            {
                if (!string.IsNullOrEmpty(className))
                {
                    var removeList = new string[] { "Senior", "senior", ".", "SENIOR", "(dot)", "(DOT)", "(Dot)", "." };
                    removeList.ToList().ForEach(item => formatClassName = formatClassName.Replace(item, ""));
                   
                }
                //string tempClassName = className.ToLower().Trim();
                //if (tempClassName.EndsWith("senior") || tempClassName.EndsWith("(dot)") || tempClassName.EndsWith("."))
                //    className = className.Replace("Senior", "").Replace("senior", "").Replace("SENIOR", "").Replace("(Dot)", "").Replace("(dot)", "").Replace("(DOT)", "").Replace(".", "");

            }
            catch (Exception)
            {
            }
            return Convert.ToString(formatClassName.Trim());
        }
        public static string GetSMEDependentSeniorScheme(string contractNo, string mainMemberCover)
        {
            string schemeCode = "0";
            string prefixClassName = string.Empty, suffixClassName = string.Empty, className = string.Empty;         
            string[] classNames;
            try
            {
                if (!string.IsNullOrEmpty(contractNo) && !string.IsNullOrEmpty(mainMemberCover))
                {
                    schemeCode = mainMemberCover;
                    ClsListDetail_DN[] lClassDetails = CaesarManager.GetClass(contractNo);

                    if (lClassDetails != null && lClassDetails.Count() > 0)
                    {

                        var mainClass = lClassDetails.Where(x => x.classID == Convert.ToInt64(mainMemberCover)).Single();

                        if (mainClass != null)
                        {
                            className = mainClass.className;


                            if (!string.IsNullOrEmpty(className))
                            {  
                                classNames = className.Split('-');
                                if (classNames.Length > 0)
                                {
                                    className = classNames[classNames.Length - 1].Trim();
                                    if (!string.IsNullOrEmpty(className))
                                    {
                                        className = RemoveSpecialCoverName(className);
                                        prefixClassName = "Senior " + className;
                                        suffixClassName = className + " Senior";
                                    }
                                }

                                var suffixDependentClass = lClassDetails.Where(x => x.className.Contains(suffixClassName)).FirstOrDefault();
                                if (suffixDependentClass != null)
                                    schemeCode = Convert.ToString(suffixDependentClass.classID);
                                else
                                {
                                    var prefixDependentClass = lClassDetails.Where(x => x.className.Contains(prefixClassName)).FirstOrDefault();
                                    if (prefixDependentClass != null)
                                        schemeCode = Convert.ToString(prefixDependentClass.classID);
                                }
                            }
                        }

                    }



                }
            }
            catch (Exception ex)
            {
            }
            return schemeCode;
        }
        public static string GetDependentRegularSchemeIsMainMemberSenior(string contractNo, string mainMemberCover)
        {
            string schemeCode = "0";
            string className = string.Empty;
            string[] classNames;
            try
            {
                if (!string.IsNullOrEmpty(contractNo) && !string.IsNullOrEmpty(mainMemberCover))
                {
                    schemeCode = mainMemberCover;
                    ClsListDetail_DN[] lClassDetails = CaesarManager.GetClass(contractNo);

                    if (lClassDetails != null && lClassDetails.Count() > 0)
                    {

                        var mainClass = lClassDetails.Where(x => x.classID == Convert.ToInt64(mainMemberCover)).Single();

                        if (mainClass != null)
                        {
                            className = mainClass.className;


                            if (!string.IsNullOrEmpty(className))
                            {
                                classNames = className.Split('-');
                                className = classNames[1].Trim();
                                if (!string.IsNullOrEmpty(className))
                                {
                                    className = RemoveSpecialCoverName(className);
                                    var dependentClass = lClassDetails.Where(x => x.className.Contains(className.Trim()) && !x.className.Contains(mainMemberCover) &&  !x.className.Contains(className.Trim() + " (S)")).FirstOrDefault();
                                    if (dependentClass != null)
                                    {
                                        schemeCode = dependentClass.classID.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return schemeCode;
        }
        public static string GetDependentSingleSchemeIsMainMemberSenior(string contractNo, string mainMemberCover)
        {
            string schemeCode = "0";
            string className = string.Empty;
            string[] classNames;
            try
            {
                if (!string.IsNullOrEmpty(contractNo) && !string.IsNullOrEmpty(mainMemberCover))
                {
                    schemeCode = mainMemberCover;
                    ClsListDetail_DN[] lClassDetails = CaesarManager.GetClass(contractNo);

                    if (lClassDetails != null && lClassDetails.Count() > 0)
                    {

                        var mainClass = lClassDetails.Where(x => x.classID == Convert.ToInt64(mainMemberCover)).Single();

                        if (mainClass != null)
                        {
                            className = mainClass.className;


                            if (!string.IsNullOrEmpty(className))
                            {
                                classNames = className.Split('-');
                                className = classNames[1].Trim();
                                if (!string.IsNullOrEmpty(className))
                                {
                                    className = RemoveSpecialCoverName(className).Trim();
                                    className += " (S)";
                                    var dependentClass = lClassDetails.Where(x => x.className.Contains(className.Trim())).FirstOrDefault();
                                    if (dependentClass != null)
                                    {
                                        schemeCode = dependentClass.classID.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return schemeCode;
        }
        public static string GetMainMemberSchemeName(string contractNo, string mainMemberCover)
        {
            string schemeCode = "0";
            string className = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(contractNo) && !string.IsNullOrEmpty(mainMemberCover))
                {
                    schemeCode = mainMemberCover;
                    ClsListDetail_DN[] lClassDetails = CaesarManager.GetClass(contractNo);

                    if (lClassDetails != null && lClassDetails.Count() > 0)
                    {

                        var mainClass = lClassDetails.Where(x => x.classID == Convert.ToInt64(mainMemberCover)).Single();

                        if (mainClass != null)
                        {
                            schemeCode = mainClass.className;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return schemeCode;
        }
        #endregion
        //// End
        public class Error
        {
            public int Index { get; set; }
            public string Type { get; set; }
            public int Id { get; set; }
            public String Messege { get; set; }
        }
        [Serializable]
        public class Indicator
        {
            public string EmployeeNumberMandatory { get; set; }
            public string DepartmentMandatory { get; set; }
            public string AttachmentMandatory { get; set; }
        }
    }
}