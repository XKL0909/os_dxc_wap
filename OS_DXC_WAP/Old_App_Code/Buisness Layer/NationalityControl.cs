﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for NationalityControl
/// </summary>
public class NationalityControl
{
	public NationalityControl()
	{
		
	}

    public List<Nationality> GetAllNationalities(bool IsGCC = false)
    {
        List<Nationality> MyList = new List<Nationality>();
        IDataReader Idr = Provider.ConcreteDataAccess().GetAllNationalities(IsGCC);
        Nationality MyNationality;

        while (Idr.Read())
        {
            MyNationality = new Nationality();
            MyNationality.ID = int.Parse(Idr["ID"].ToString());
            MyNationality.Nationality1 = Idr["Nationality"].ToString();
            MyNationality.Code = Idr["MappedCode"].ToString();
            MyNationality.IsGCC = bool.Parse(Idr["IsGCC"].ToString());
            MyList.Add(MyNationality);
        }
        Idr.Close();
        return MyList;
    }
}