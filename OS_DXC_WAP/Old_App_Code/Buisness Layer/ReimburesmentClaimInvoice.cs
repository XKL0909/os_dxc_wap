﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReimburesmentClaimInvoice
/// </summary>

[Serializable]
public class ReimburesmentClaimInvoice
{
    private int _id = 0;
    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    private short _numberOfInvoices = 0;
    public short NumberOfInvoices
    {
        get { return _numberOfInvoices; }
        set { _numberOfInvoices = value; }
    }

    private float _totalAmount = 0;
    public float TotalAmount
    {
        get { return _totalAmount; }
        set { _totalAmount = value; }
    }

    private string _currencyCode = string.Empty;
    public string CurrencyCode
    {
        get { return _currencyCode; }
        set { _currencyCode = value; }
    }

    private int _reimburesmentClaimID = 0;
    public int ReimburesmentClaimID
    {
        get { return _reimburesmentClaimID; }
        set { _reimburesmentClaimID = value; }
    }

	public ReimburesmentClaimInvoice(){}

    public ReimburesmentClaimInvoice(short NumberOfInvoices, float TotalAmount, string CurrencyCode, int ReimburesmentClaimID)
    {
        this.NumberOfInvoices = NumberOfInvoices;
        this.TotalAmount = TotalAmount;
        this.CurrencyCode = CurrencyCode;
        this.ReimburesmentClaimID = ReimburesmentClaimID;
    }

    public void AddReimburesmentClaimInvoice()
    {
        Provider.ConcreteDataAccess().InsertReimburesmentClaimInvoice(this);
    }

    
}

