﻿using BO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Bupa.Yakeen.AccessLayer.Models;
using Bupa.Models.Enums;

/// <summary>
/// Summary description for YakeenManager
/// </summary>y
public class YakeenManager
{
   
    private static string YakeenValidationHours = ConfigurationManager.AppSettings["YakeenDataValidHours"];
    private static string FromSourceName = ConfigurationManager.AppSettings["FromSourceName"];
    private static string FromDepartmentName = ConfigurationManager.AppSettings["FromDepartmentName"];
    private static string FromUserName = "CCHI_Unified";


    public static  Person GetMemberInfoFromYakeen(string personId, string varificationString)
    {

        PersonServiceClient pWS = new PersonServiceClient();
        ////pWS.GetPersonInfo(personId, varificationString, Convert.ToInt32(YakeenValidationHours), true, YakeenSearhByTypesYakeenSearchByType.HijriDOB, true, FromUserName, FromDepartmentName, FromSourceName);

        Person personInfo = new Person();
        //if (personId.StartsWith("1"))
        //    personInfo = pWS.GetPersonInfo(personId, varificationString, Convert.ToInt32(YakeenValidationHours), true, YakeenSearhByTypesYakeenSearchByType.HijriDOB, true, FromUserName, FromDepartmentName, FromSourceName);
        //else if (personId.StartsWith("2"))
        //    personInfo = pWS.GetPersonInfo(personId, varificationString, Convert.ToInt32(YakeenValidationHours), true, YakeenSearhByTypesYakeenSearchByType.IqamaId, true, FromUserName, FromDepartmentName, FromSourceName);
        //else
        //    personInfo = pWS.GetPersonInfo(personId, varificationString, Convert.ToInt32(YakeenValidationHours), true, YakeenSearhByTypesYakeenSearchByType.BorderEntry, true, FromUserName, FromDepartmentName, FromSourceName);
        //return personInfo;

        if (personId.StartsWith("1"))
            personInfo = pWS.GetPersonInfo(personId, varificationString, Convert.ToInt32(YakeenValidationHours),  YakeenSearhByTypesYakeenSearchByType.HijriDOB,  FromUserName, FromDepartmentName, FromSourceName);
        else if (personId.StartsWith("2"))
            personInfo = pWS.GetPersonInfo(personId, varificationString, Convert.ToInt32(YakeenValidationHours), YakeenSearhByTypesYakeenSearchByType.IqamaId,  FromUserName, FromDepartmentName, FromSourceName);
        else
            personInfo = pWS.GetPersonInfo(personId, varificationString, Convert.ToInt32(YakeenValidationHours),  YakeenSearhByTypesYakeenSearchByType.BorderEntry, FromUserName, FromDepartmentName, FromSourceName);
        return personInfo;


    }

    public static List<YakeenDetail> GetMembersDetailsFromYakeen(string idNumber, string secondId, long dependentHijiriYear = 0, long dependentBorderNumber = 0)
    {
        YakeenDetail yakeenDetail = new YakeenDetail();
        List<YakeenDetail> lstYakeenDetail = new List<YakeenDetail>();
        //PersonService pWS = new PersonService();
        PersonServiceClient pWS = new PersonServiceClient();
        Person personInfo = new Person();
        Person personDependentInfo = new Person();
        DependentsLists dependentsLists = new DependentsLists();



        //if (idNumber.StartsWith("1"))
        //    personInfo = pWS.GetPersonInfo(idNumber, secondId, Convert.ToInt32(YakeenValidationHours), true, YakeenSearhByTypesYakeenSearchByType.HijriDOB, true, FromUserName, FromDepartmentName, FromSourceName);
        //else if (idNumber.StartsWith("2"))
        //    personInfo = pWS.GetPersonInfo(idNumber, secondId, Convert.ToInt32(YakeenValidationHours), true, YakeenSearhByTypesYakeenSearchByType.IqamaId, true, FromUserName, FromDepartmentName, FromSourceName);
        //else
        //    personInfo = pWS.GetPersonInfo(idNumber, secondId, Convert.ToInt32(YakeenValidationHours), true, YakeenSearhByTypesYakeenSearchByType.BorderEntry, true, FromUserName, FromDepartmentName, FromSourceName);

        if (idNumber.StartsWith("1"))
            personInfo = pWS.GetPersonInfo(idNumber, secondId, Convert.ToInt32(YakeenValidationHours), YakeenSearhByTypesYakeenSearchByType.HijriDOB,  FromUserName, FromDepartmentName, FromSourceName);
        else if (idNumber.StartsWith("2"))
            personInfo = pWS.GetPersonInfo(idNumber, secondId, Convert.ToInt32(YakeenValidationHours),  YakeenSearhByTypesYakeenSearchByType.IqamaId,  FromUserName, FromDepartmentName, FromSourceName);
        else
            personInfo = pWS.GetPersonInfo(idNumber, secondId, Convert.ToInt32(YakeenValidationHours), YakeenSearhByTypesYakeenSearchByType.BorderEntry,  FromUserName, FromDepartmentName, FromSourceName);


        if (personInfo != null)
        {
            yakeenDetail = new YakeenDetail();
            yakeenDetail.IDNumber = personInfo.PersonID;
            yakeenDetail.SponsorID = personInfo.SponsorId;
            yakeenDetail.ArabicFullName = personInfo.FullArabicName;
            yakeenDetail.EnglishFullName = personInfo.FullEnglishName;
            yakeenDetail.DOB = personInfo.BirthDateGregorian;
            yakeenDetail.Gender = personInfo.Gender;
            yakeenDetail.IdExpiryDate = personInfo.IdExpiryDate;
            yakeenDetail.VisaExpiryDate = personInfo.VisaExpiryDate;
            yakeenDetail.NationalityCode = personInfo.NationalityCode;
            yakeenDetail.OccupationCode = personInfo.OccupationCode;
            yakeenDetail.OccupationArabic = personInfo.OccupationAr;
            yakeenDetail.JobCode = personInfo.JobCode;
            yakeenDetail.Relationship = personInfo.Relationship;
            yakeenDetail.ErrorMessage = personInfo.ErrorMessage;
            yakeenDetail.ExceptionMessage = personInfo.ExceptionMessage;
            yakeenDetail.SocialStatus = personInfo.PersonID.StartsWith("1") ? personInfo.SocialStatus == 1 ? personInfo.SocialStatus + 1 : 0 : personInfo.SocialStatus;
            ////yakeenDetail.SocialStatus = personInfo.PersonID.StartsWith("1") ? personInfo.SocialStatus + 1 : personInfo.SocialStatus;
            lstYakeenDetail.Add(yakeenDetail);
        }

        if (dependentHijiriYear != 0)
        {
            if (dependentBorderNumber != 0)
                idNumber = dependentBorderNumber.ToString();

            bool isSaudi = false;
            isSaudi = idNumber.StartsWith("1") ? true : false;

            //YakeenValidationHours = YakeenValidationHours;

            var searchBy = isSaudi ? YakeenSearhByTypesYakeenSearchByType.HijriDOB : YakeenSearhByTypesYakeenSearchByType.IqamaId;


            ///var dependent = pWS.GetDependentsInfo( idNumber, dependentHijiriYear, true, Convert.ToInt32(YakeenValidationHours), true, searchBy, true, FromUserName, FromDepartmentName, FromSourceName, isSaudi, true);
            var dependent = pWS.GetDependentsInfo(idNumber, dependentHijiriYear,  Convert.ToInt32(YakeenValidationHours),  searchBy,  FromUserName, FromDepartmentName, FromSourceName, isSaudi);

            ////dependentsLists = new DependentsLists();
            if (dependent != null)
            {
                ////dependent = dependent.Where(x => x.Kinship != null && x.Kinship != string.Empty).SelectMany();

                var depNew = dependent.Where(x => x.Kinship != null && x.Kinship != "0" && x.Kinship.Trim() != string.Empty);

                foreach (var item in depNew /*depNew[]*/)
                {
                    YakeenDetail depDE = new YakeenDetail();

                    if (!string.IsNullOrEmpty(item.Kinship) && Convert.ToString(item.Kinship) != "0" && Convert.ToString(item.Kinship) != "-1" && !string.IsNullOrEmpty(item.PersonID))
                    {
                        string relationCode = string.Empty;
                        relationCode = GetYakeenRelation(item.Kinship.Trim(), Convert.ToString(item.Gender));
                        if (!string.IsNullOrEmpty(relationCode))
                        {
                            depDE.IDNumber = item.PersonID;
                            depDE.SponsorID = item.SponsorId;
                            depDE.ArabicFullName = item.FullArabicName;
                            depDE.EnglishFullName = item.FullEnglishName;
                            depDE.DOB = item.BirthDateGregorian;
                            depDE.Gender = item.Gender;
                            depDE.IdExpiryDate = item.IdExpiryDate;
                            depDE.VisaExpiryDate = item.VisaExpiryDate;
                            depDE.NationalityCode = item.NationalityCode;
                            depDE.OccupationCode = item.OccupationCode;
                            depDE.OccupationArabic = item.OccupationAr;
                            depDE.JobCode = item.JobCode;
                            depDE.Relationship = relationCode;
                            depDE.ErrorMessage = item.ErrorMessage;
                            depDE.ExceptionMessage = item.ExceptionMessage;
                            lstYakeenDetail.Add(depDE);
                        }
                    }
                }
            }

        }
        return lstYakeenDetail;

    }

    private static string GetYakeenRelation(string retelatioArabic, string gender)
    {
        string relatationEnglish = string.Empty;
        switch (retelatioArabic.ToLower())
        {
            case "طفل":
            case "child":
                relatationEnglish = "C";
                break;
            case "بنت":
            case "daughter":
                relatationEnglish = "D";
                break;
            case "ابن":
            case "son":
                relatationEnglish = "SO";
                break;
            case "زوجة":
            case "spouse":           
            case "wife":
            case "husband":
            case "زوج":
                relatationEnglish = "S";
                break;                           
            case "أخت":
            case "sister":
                relatationEnglish = "SI";
                break;
            case "أخ":
            case "brother":
                relatationEnglish = "B";
                break;
            case "ام":
            case "mother":
                relatationEnglish = "M";
                break;
            case "اب":
            case "father":
                relatationEnglish = "F";
                break;
            case "غيره":
            case "other":
                relatationEnglish = "T";
                break;
            case "والد":
            case "parent":
                {
                    if(gender.ToLower().Equals("male"))
                    relatationEnglish = "F";
                    else if (gender.ToLower().Equals("female"))
                        relatationEnglish = "M";
                    else 
                        relatationEnglish = string.Empty;
                    break;
                }
            default:
                relatationEnglish = retelatioArabic;
                break;

        }
        return relatationEnglish;


    }


}