﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReimburesmentClaim
/// </summary>
public class ReimburesmentClaim
{
    private int _id = 0;
    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    private short _type = 0;
    public short Type
    {
        get { return _type; }
        set { _type = value; }
    }

    private string _memberName = string.Empty;
    public string MemberName
    {
        get { return _memberName; }
        set { _memberName = value; }
    }

    private string _membershipNumber = string.Empty;
    public string MembershipNumber
    {
        get { return _membershipNumber; }
        set { _membershipNumber = value; }
    }

    private string _companyName = string.Empty;
    public string CompanyName
    {
        get { return _companyName; }
        set { _companyName = value; }
    }

    private string _mobileNumber = string.Empty;
    public string MobileNumber
    {
        get { return _mobileNumber; }
        set { _mobileNumber = value; }
    }

    private string _email = string.Empty;
    public string Email
    {
        get { return _email; }
        set { _email = value; }
    }

    private short _paymentType = 0;
    public short PaymentType
    {
        get { return _paymentType; }
        set { _paymentType = value; }
    }

    private string _bankName = string.Empty;
    public string BankName
    {
        get { return _bankName; }
        set { _bankName = value; }
    }

    private string _ibanNumber = string.Empty;
    public string IbanNumber
    {
        get { return _ibanNumber; }
        set { _ibanNumber = value; }
    }

    private string _referenceNumber = string.Empty;
    public string ReferenceNumber
    {
        get { return _referenceNumber; }
        set { _referenceNumber = value; }
    }

	public ReimburesmentClaim(){}


    public ReimburesmentClaim(short Type,string MemberName, string MembershipNumber, string CompanyName, string MobileNumber, string Email
                                , short PaymentType, string BankName, string IbanNumber, string ReferenceNumber)
    {
        this.Type = Type;
        this.MembershipNumber = MembershipNumber;
        this.CompanyName = CompanyName;
        this.MobileNumber = MobileNumber;
        this.Email = Email;
        this.PaymentType = PaymentType;
        this.BankName = BankName;
        this.IbanNumber = IbanNumber;
        this.ReferenceNumber = ReferenceNumber;
        this.MemberName = MemberName;
    }

    public int Add() {
        int NewID = 0;
        NewID = Provider.ConcreteDataAccess().InsertReimburesmentClaim(this);
        return NewID;
    }

}