﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserDetails
/// </summary>
/// 
[Serializable]
public class UserDetails
{


    public long ReferenceNumber { get; set; }
    public string UserGroupID { get; set; }
    public string UserLoginID { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string Mobile { get; set; }
    public bool IsActive { get; set; }
    public string Status { get; set; }
    public DateTime UpdatedDate { get; set; }
    public int UpdatedBy { get; set; }
    public bool IsProviderActive { get; set; }
    public int OTP { get; set; }
    public bool IsUserActPending { get; set; }
    public string Password {get;set;}
    public UserDetails() { }

    

    public List<UserDetails> GetUserDetails(UserDetails userDetails)
    {
       return Provider.ConcreteDataAccess().GetUserDetails(userDetails);
    }

    public void SetActivation(UserDetails userDetails)
    {
         Provider.ConcreteDataAccess().SetActivation(userDetails);
    }

    public UserDetails GetUserDetailByReferenceNumber(UserDetails userDetails)
    {
        return Provider.ConcreteDataAccess().GetUserDetailByReferenceNumber(userDetails);
    }

    public UserDetails VerifyOTP(UserDetails userDetails)
    {
        return Provider.ConcreteDataAccess().VerifyOTP(userDetails);
    }

    public int UpdateProviderOrGSPWD(UserDetails userDetails)
    {
        return Provider.ConcreteDataAccess().UpdateProviderOrGSPWD(userDetails);
    }
    public void ActivationNewUser(UserDetails userDetails)
    {
         Provider.ConcreteDataAccess().ActivateNewUser(userDetails);
    }

    public bool VerifyOtp(UserDetails userDetails)
    {
        return Provider.ConcreteDataAccess().VerifyOtp(userDetails);
    }



}