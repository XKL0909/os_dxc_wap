﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Extensions
/// </summary>
public static class MyExtensions 
{
    public static void InsertReimburesmentClaimInvoiceList<T>(this List<ReimburesmentClaimInvoice> mylist)
    {
        Provider.ConcreteDataAccess().InsertReimburesmentClaimInvoiceList(mylist);
    }

    public static void InsertReimburesmentClaimReasonList<T>(this List<ReimburesmentClaimReason> mylist)
    {
        Provider.ConcreteDataAccess().InsertReimburesmentClaimReasonList(mylist);
    }
}