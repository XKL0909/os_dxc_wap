﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Nationality
/// </summary>
public class Nationality
{
    private int _id = 0;
    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    private string _nationality = string.Empty;
    public string Nationality1
    {
        get { return _nationality; }
        set { _nationality = value; }
    }

    private string _code = string.Empty;
    public string Code
    {
        get { return _code; }
        set { _code = value; }
    }

    private bool _isGCC = false;
    public bool IsGCC
    {
        get { return _isGCC; }
        set { _isGCC = value; }
    }
	public Nationality()
	{
		
	}

    public Nationality(int ID,String Code, String Nationality, bool IsGCC)
    {
        this.ID = ID;
        this.Code = Code;
        this.Nationality1 = Nationality;
        this.IsGCC = IsGCC;
    }
}