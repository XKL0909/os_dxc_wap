﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Currency
/// </summary>
public class Currency
{
    private int _id= 0;
    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    private string _name = string.Empty;
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    private string _nameCode = string.Empty;
    public string NameCode
    {
        get { return _nameCode; }
        set { _nameCode = value; }
    }

    private string _code = string.Empty;
    public string Code
    {
        get { return _code; }
        set { _code = value; }
    }

	public Currency()
	{
		
	}

    public Currency(int ID,string Name,string Code)
    {
        this.ID = ID;
        this.Name = Name;
        this.Code = Code;
    }
    
}