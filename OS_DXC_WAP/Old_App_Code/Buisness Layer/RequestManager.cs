﻿using OnlineServices.CCHI.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UnifiedPolicy.Services;


public class RequestManager
{
    private UnifiedRequestDetailService _reqDetailService;
    AddMemberRequestDetailService _addMemberRequestDetailService;


    public RequestManager()
    {
        _reqDetailService = new UnifiedRequestDetailService();
        _addMemberRequestDetailService = new AddMemberRequestDetailService();
    }

    public List<Cap<int>> GetCCHICounts(int reqId)
    {

        var reqDetails = _reqDetailService.Get(reqId);
        List<Cap<int>> caps = new List<Cap<int>>();
        if (reqDetails != null && reqDetails.Count > 0)
        {
            var sponsorIds = reqDetails.Select(t => t.Sponsor).Distinct();

            if (sponsorIds != null && sponsorIds.Count() > 0)
            {
                foreach (var sponsorId in sponsorIds)
                {
                    Cap<int> cap = new Cap<int>();
                    cap.SponsorId = sponsorId;

                    var saudiMain = reqDetails.Where(t => t.IdNumber != null && t.Sponsor == sponsorId && t.IsMainMemeber == true && t.IdNumber.StartsWith("1")).Count();
                    var nonSaudiMain = reqDetails.Where(t => t.IdNumber != null && t.Sponsor == sponsorId && t.IsMainMemeber == true &&
                                                        (t.IdNumber.StartsWith("2") || t.IdNumber.StartsWith("3") || t.IdNumber.StartsWith("4"))).Count();

                    var saudiDependent = reqDetails.Where(t => t.IdNumber != null && t.Sponsor == sponsorId && t.IsMainMemeber == false && t.IdNumber.StartsWith("1")).Count();

                    var nonSaudiDependent = reqDetails.Where(t => t.IdNumber != null && t.Sponsor == sponsorId && t.IsMainMemeber == false && (t.IdNumber.StartsWith("2")
                          || t.IdNumber.StartsWith("3") || t.IdNumber.StartsWith("4"))).Count();

                    //select * from UnifiedRequestDetail where HijriDOBYear != '0' and MemberType not in ('E','FE','SE') order by 1 desc

                    var memberTypes = new string[] { "E", "FE", "SE" };

                    var newBornBabay = reqDetails.Where(t => t.IdNumber == null);
                    if (newBornBabay.Count() > 0)
                    {

                        //var newBornSaudiDependent = reqDetails.Where(t=>t.Sponsor == sponsorId && t.IdNumber !=null&& t.SecSponsorId.StartsWith("1")).Count();
                        //var newBorNonSaudiDependent = reqDetails.Where(t=> t.Sponsor == sponsorId &&  t.IdNumber != null &&(t.SecSponsorId.StartsWith("2")
                        //  || t.SecSponsorId.StartsWith("3") || t.SecSponsorId.StartsWith("4"))).Count();

                        var newBornSaudiDependent = newBornBabay.Where(t=> t.Sponsor == sponsorId && t.SecSponsorId.StartsWith("1")).Count();
                        var newBorNonSaudiDependent = newBornBabay.Where(t => t.Sponsor == sponsorId  && (t.SecSponsorId.StartsWith("2")
                          || t.SecSponsorId.StartsWith("3") || t.SecSponsorId.StartsWith("4"))).Count();

                        saudiDependent = saudiDependent + newBornSaudiDependent;
                        nonSaudiDependent = nonSaudiDependent + newBorNonSaudiDependent;
                    }


                    cap.SaudiMain = saudiMain;
                    cap.SaudiDependent = saudiDependent;
                    cap.NonSaudiMain = nonSaudiMain;
                    cap.NonSaudiDependent = nonSaudiDependent;

                    caps.Add(cap);
                }
            }
        }
        return caps;

    }

    public List<Cap<int>> GetCCHICountsAddMember(int reqId)
    {
       

        var reqDetails = _addMemberRequestDetailService.Get(reqId);
        List<Cap<int>> caps = new List<Cap<int>>();
        if (reqDetails != null && reqDetails.Count > 0)
        {
            var sponsorIds = reqDetails.Select(t => t.Sponsor).Distinct();

            if (sponsorIds != null && sponsorIds.Count() > 0)
            {
                foreach (var sponsorId in sponsorIds)
                {
                    Cap<int> cap = new Cap<int>();
                    cap.SponsorId = sponsorId;

                    var saudiMain = reqDetails.Where(t => t.IdNumber != null && t.Sponsor == sponsorId && t.IsMainMemeber == true && t.IdNumber.StartsWith("1")).Count();
                    var nonSaudiMain = reqDetails.Where(t => t.IdNumber != null && t.Sponsor == sponsorId && t.IsMainMemeber == true &&
                                                        (t.IdNumber.StartsWith("2") || t.IdNumber.StartsWith("3") || t.IdNumber.StartsWith("4"))).Count();

                    var saudiDependent = reqDetails.Where(t => t.IdNumber != null && t.Sponsor == sponsorId && t.IsMainMemeber == false && t.IdNumber.StartsWith("1")).Count();

                    var nonSaudiDependent = reqDetails.Where(t => t.IdNumber != null && t.Sponsor == sponsorId && t.IsMainMemeber == false && (t.IdNumber.StartsWith("2")
                          || t.IdNumber.StartsWith("3") || t.IdNumber.StartsWith("4"))).Count();

                    //select * from UnifiedRequestDetail where HijriDOBYear != '0' and MemberType not in ('E','FE','SE') order by 1 desc

                    var memberTypes = new string[] { "E", "FE", "SE" };

                    var newBornBabay = reqDetails.Where(t => t.IdNumber == null);
                    if (newBornBabay.Count() > 0)
                    {

                        //var newBornSaudiDependent = reqDetails.Where(t=>t.Sponsor == sponsorId && t.IdNumber !=null&& t.SecSponsorId.StartsWith("1")).Count();
                        //var newBorNonSaudiDependent = reqDetails.Where(t=> t.Sponsor == sponsorId &&  t.IdNumber != null &&(t.SecSponsorId.StartsWith("2")
                        //  || t.SecSponsorId.StartsWith("3") || t.SecSponsorId.StartsWith("4"))).Count();

                        var newBornSaudiDependent = newBornBabay.Where(t => t.Sponsor == sponsorId && t.SecSponsorId.StartsWith("1")).Count();
                        var newBorNonSaudiDependent = newBornBabay.Where(t => t.Sponsor == sponsorId && (t.SecSponsorId.StartsWith("2")
                          || t.SecSponsorId.StartsWith("3") || t.SecSponsorId.StartsWith("4"))).Count();

                        saudiDependent = saudiDependent + newBornSaudiDependent;
                        nonSaudiDependent = nonSaudiDependent + newBorNonSaudiDependent;
                    }


                    cap.SaudiMain = saudiMain;
                    cap.SaudiDependent = saudiDependent;
                    cap.NonSaudiMain = nonSaudiMain;
                    cap.NonSaudiDependent = nonSaudiDependent;

                    caps.Add(cap);
                }
            }
        }
        return caps;

    }
}