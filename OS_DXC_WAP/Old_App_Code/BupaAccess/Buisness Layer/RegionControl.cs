﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for RegionControl
/// </summary>
public class RegionControl
{
	public RegionControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public List<Region> GetRegions(string XMLPath) {
        List<Region> MyRegions = new List<Region>();
        Region MyRegion;
        XDocument MyDocument = XDocument.Load(XMLPath);
        List<XElement> MyElements = MyDocument.Descendants("Region").ToList();
        foreach (XElement ele in MyElements) {
            MyRegion = new Region(ele.Attribute("Name").Value.ToString(), ele.Attribute("Code").Value.ToString());
            MyRegions.Add(MyRegion);
        }
        return MyRegions;
    }
}