﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthGeneralInformation
/// </summary>
public class MyHealthGeneralInformation
{
	public MyHealthGeneralInformation()
	{
		//
		// TODO: Add constructor logic here
		//
	}



    private Int32 vID;
    public Int32 ID
    {
        get { return vID; }
        set { vID = value; }
    }


    private string vMembershipNo;
    public string MembershipNo
    {
        get { return vMembershipNo; }
        set { vMembershipNo = value; }
    }


    private string vDeviceID;
    public string DeviceID
    {
        get { return vDeviceID; }
        set { vDeviceID = value; }
    }


    private Int32 vAge;
    public Int32 Age
    {
        get { return vAge; }
        set { vAge = value; }
    }


    private string vWaistSizeInch;
    public string WaistSizeInch
    {
        get { return vWaistSizeInch; }
        set { vWaistSizeInch = value; }
    }


    private string vWeightinKg;
    public string WeightinKg
    {
        get { return vWeightinKg; }
        set { vWeightinKg = value; }
    }


    private string vHeightinCm;
    public string HeightinCm
    {
        get { return vHeightinCm; }
        set { vHeightinCm = value; }
    }


    private string vemail;
    public string email
    {
        get { return vemail; }
        set { vemail = value; }
    }


    private string vGender;
    public string Gender
    {
        get { return vGender; }
        set { vGender = value; }
    }


    private string vname;
    public string name
    {
        get { return vname; }
        set { vname = value; }
    }


    private string vWaistSizeCm;
    public string WaistSizeCm
    {
        get { return vWaistSizeCm; }
        set { vWaistSizeCm = value; }
    }


    private string vMobileNo;
    public string MobileNo
    {
        get { return vMobileNo; }
        set { vMobileNo = value; }
    }

}