﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CaeserException
/// </summary>
public class CaeserException
{
    #region Properties
        private bool _result = false;
        public bool Result
        {
            get { return _result; }
            set { _result = value; }
        }
        private string _errorMessage = string.Empty;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        private bool _isSabic = false;
        public bool IsSabic
        {
            get { return _isSabic; }
            set { _isSabic = value; }
        }
    #endregion
    public CaeserException()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}