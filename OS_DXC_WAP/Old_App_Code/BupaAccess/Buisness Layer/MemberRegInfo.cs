﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MemberRegInfo
/// </summary>
public class MemberRegInfo : CaeserException
{
    #region Properties
        private string _memberName = string.Empty;
        private string _membershipNumber = string.Empty;
        private string _customer = string.Empty;
        private string _contractNumber = string.Empty;
        private string _status = string.Empty;

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public string MembershipNumber
        {
            get { return _membershipNumber; }
            set { _membershipNumber = value; }
        }


        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }

    #endregion

}