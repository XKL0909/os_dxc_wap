﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthDietAndNutritionControl
/// </summary>
public class MyHealthDietAndNutritionControl
{
	public MyHealthDietAndNutritionControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public List<MyHealthDietAndNutrition> GetAllMyHealthDietAndNutrition()
    {
        List<MyHealthDietAndNutrition> L = new List<MyHealthDietAndNutrition>();
        MyHealthDietAndNutrition MyMyHealthDietAndNutrition = default(MyHealthDietAndNutrition);
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetAllMyHealthDietAndNutrition();
        while (dr.Read())
        {
            MyMyHealthDietAndNutrition = new MyHealthDietAndNutrition();
            MyMyHealthDietAndNutrition.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthDietAndNutrition.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthDietAndNutrition.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthDietAndNutrition.SoftDrinks = dr["SoftDrinks"].ToString();
            MyMyHealthDietAndNutrition.Fruits = dr["Fruits"].ToString();
            MyMyHealthDietAndNutrition.Vegetables = dr["Vegetables"].ToString();
            MyMyHealthDietAndNutrition.DairyProducts = dr["DairyProducts"].ToString();
            MyMyHealthDietAndNutrition.EnergyDrinks = dr["EnergyDrinks"].ToString();
            MyMyHealthDietAndNutrition.Nuts = dr["Nuts"].ToString();
            MyMyHealthDietAndNutrition.SugaryJuices = dr["SugaryJuices"].ToString();
            MyMyHealthDietAndNutrition.Sweets = dr["Sweets"].ToString();
            MyMyHealthDietAndNutrition.HighFibber = dr["HighFibber"].ToString();
            MyMyHealthDietAndNutrition.FattyOily = dr["FattyOily"].ToString();
            MyMyHealthDietAndNutrition.waterPerDay = dr["waterPerDay"].ToString();
            MyMyHealthDietAndNutrition.CoffeeTea = dr["CoffeeTea"].ToString();
            L.Add(MyMyHealthDietAndNutrition);
        }
        dr.Close();
        return L;
    }

    public MyHealthDietAndNutrition GetMyHealthDietAndNutrition(int ID)
    {
        MyHealthDietAndNutrition MyMyHealthDietAndNutrition = new MyHealthDietAndNutrition();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthDietAndNutrition(ID);
        if (dr.Read())
        {
            MyMyHealthDietAndNutrition.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthDietAndNutrition.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthDietAndNutrition.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthDietAndNutrition.SoftDrinks = dr["SoftDrinks"].ToString();
            MyMyHealthDietAndNutrition.Fruits = dr["Fruits"].ToString();
            MyMyHealthDietAndNutrition.Vegetables = dr["Vegetables"].ToString();
            MyMyHealthDietAndNutrition.DairyProducts = dr["DairyProducts"].ToString();
            MyMyHealthDietAndNutrition.EnergyDrinks = dr["EnergyDrinks"].ToString();
            MyMyHealthDietAndNutrition.Nuts = dr["Nuts"].ToString();
            MyMyHealthDietAndNutrition.SugaryJuices = dr["SugaryJuices"].ToString();
            MyMyHealthDietAndNutrition.Sweets = dr["Sweets"].ToString();
            MyMyHealthDietAndNutrition.HighFibber = dr["HighFibber"].ToString();
            MyMyHealthDietAndNutrition.FattyOily = dr["FattyOily"].ToString();
            MyMyHealthDietAndNutrition.waterPerDay = dr["waterPerDay"].ToString();
            MyMyHealthDietAndNutrition.CoffeeTea = dr["CoffeeTea"].ToString();
        }
        dr.Close();
        return MyMyHealthDietAndNutrition;
    }

    public MyHealthDietAndNutrition GetMyHealthDietAndNutrition(string value,string type)
    {
        MyHealthDietAndNutrition MyMyHealthDietAndNutrition = new MyHealthDietAndNutrition();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthDietAndNutrition(value, type);
        if (dr.Read())
        {
            MyMyHealthDietAndNutrition.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthDietAndNutrition.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthDietAndNutrition.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthDietAndNutrition.SoftDrinks = dr["SoftDrinks"].ToString();
            MyMyHealthDietAndNutrition.Fruits = dr["Fruits"].ToString();
            MyMyHealthDietAndNutrition.Vegetables = dr["Vegetables"].ToString();
            MyMyHealthDietAndNutrition.DairyProducts = dr["DairyProducts"].ToString();
            MyMyHealthDietAndNutrition.EnergyDrinks = dr["EnergyDrinks"].ToString();
            MyMyHealthDietAndNutrition.Nuts = dr["Nuts"].ToString();
            MyMyHealthDietAndNutrition.SugaryJuices = dr["SugaryJuices"].ToString();
            MyMyHealthDietAndNutrition.Sweets = dr["Sweets"].ToString();
            MyMyHealthDietAndNutrition.HighFibber = dr["HighFibber"].ToString();
            MyMyHealthDietAndNutrition.FattyOily = dr["FattyOily"].ToString();
            MyMyHealthDietAndNutrition.waterPerDay = dr["waterPerDay"].ToString();
            MyMyHealthDietAndNutrition.CoffeeTea = dr["CoffeeTea"].ToString();
        }
        dr.Close();
        return MyMyHealthDietAndNutrition;
    }

    public void AddMyHealthDietAndNutrition(MyHealthDietAndNutrition MyMyHealthDietAndNutrition)
    {
        MobileProvider.ConcreteDataAccess().InsertMyHealthDietAndNutrition(MyMyHealthDietAndNutrition.ID, MyMyHealthDietAndNutrition.MembershipNo, MyMyHealthDietAndNutrition.DeviceID, MyMyHealthDietAndNutrition.SoftDrinks, MyMyHealthDietAndNutrition.Fruits, MyMyHealthDietAndNutrition.Vegetables, MyMyHealthDietAndNutrition.DairyProducts, MyMyHealthDietAndNutrition.EnergyDrinks, MyMyHealthDietAndNutrition.Nuts, MyMyHealthDietAndNutrition.SugaryJuices,
        MyMyHealthDietAndNutrition.Sweets, MyMyHealthDietAndNutrition.HighFibber, MyMyHealthDietAndNutrition.FattyOily, MyMyHealthDietAndNutrition.waterPerDay, MyMyHealthDietAndNutrition.CoffeeTea);
    }

    public void ModifyMyHealthDietAndNutrition(MyHealthDietAndNutrition MyMyHealthDietAndNutrition)
    {
        MobileProvider.ConcreteDataAccess().UpdateMyHealthDietAndNutrition(MyMyHealthDietAndNutrition.ID, MyMyHealthDietAndNutrition.MembershipNo, MyMyHealthDietAndNutrition.DeviceID, MyMyHealthDietAndNutrition.SoftDrinks, MyMyHealthDietAndNutrition.Fruits, MyMyHealthDietAndNutrition.Vegetables, MyMyHealthDietAndNutrition.DairyProducts, MyMyHealthDietAndNutrition.EnergyDrinks, MyMyHealthDietAndNutrition.Nuts, MyMyHealthDietAndNutrition.SugaryJuices,
        MyMyHealthDietAndNutrition.Sweets, MyMyHealthDietAndNutrition.HighFibber, MyMyHealthDietAndNutrition.FattyOily, MyMyHealthDietAndNutrition.waterPerDay, MyMyHealthDietAndNutrition.CoffeeTea);
    }

    public void DeleteMyHealthDietAndNutrition(int ID)
    {
        MobileProvider.ConcreteDataAccess().DeleteMyHealthDietAndNutrition(ID);
    }

}