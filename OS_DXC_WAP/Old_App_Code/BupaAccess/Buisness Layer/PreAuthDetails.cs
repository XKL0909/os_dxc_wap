﻿using OS_DXC_WAP.CaesarWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PreAuthDetails
/// </summary>
public class PreAuthDetails : EnqPreHisInfoResponse_DN
{
    private string _membershipNumber = string.Empty;
    public string membershipNumber
    {
        get { return _membershipNumber; }
        set { _membershipNumber = value; }
    }

    public PreAuthDetails() { 
    
    }
    public PreAuthDetails(EnqPreHisInfoResponse_DN response, string membershipNumber)
	{
        this.detail = response.detail;
        this.errorID = response.errorID;
        this.errorMessage = response.errorMessage;
        this.status = response.status;
        this.transactionID = response.transactionID;
        this.membershipNumber = membershipNumber;
	}
}