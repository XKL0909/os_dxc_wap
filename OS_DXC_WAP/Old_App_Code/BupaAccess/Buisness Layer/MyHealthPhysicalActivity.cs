﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthPhysicalActivity
/// </summary>
public class MyHealthPhysicalActivity
{
	public MyHealthPhysicalActivity()
	{
		//
		// TODO: Add constructor logic here
		//
	}



    private Int32 vID;
    public Int32 ID
    {
        get { return vID; }
        set { vID = value; }
    }


    private string vMembershipNo;
    public string MembershipNo
    {
        get { return vMembershipNo; }
        set { vMembershipNo = value; }
    }


    private string vDeviceID;
    public string DeviceID
    {
        get { return vDeviceID; }
        set { vDeviceID = value; }
    }


    private string vSleepPerDay;
    public string SleepPerDay
    {
        get { return vSleepPerDay; }
        set { vSleepPerDay = value; }
    }


    private string vExercisePerWeek;
    public string ExercisePerWeek
    {
        get { return vExercisePerWeek; }
        set { vExercisePerWeek = value; }
    }

}