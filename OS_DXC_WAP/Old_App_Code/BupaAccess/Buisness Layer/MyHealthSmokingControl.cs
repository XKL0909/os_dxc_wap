﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthSmokingControl
/// </summary>
public class MyHealthSmokingControl
{
	public MyHealthSmokingControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public List<MyHealthSmoking> GetAllMyHealthSmoking()
    {
        List<MyHealthSmoking> L = new List<MyHealthSmoking>();
        MyHealthSmoking MyMyHealthSmoking = default(MyHealthSmoking);
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetAllMyHealthSmoking();
        while (dr.Read())
        {
            MyMyHealthSmoking = new MyHealthSmoking();
            MyMyHealthSmoking.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthSmoking.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthSmoking.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthSmoking.smokeCigarettes = bool.Parse(dr["smokeCigarettes"].ToString());
            MyMyHealthSmoking.smokeShisha = bool.Parse(dr["smokeShisha"].ToString());
            MyMyHealthSmoking.CigarettesPerDay = dr["CigarettesPerDay"].ToString();
            MyMyHealthSmoking.ShishaPerWeek = dr["ShishaPerWeek"].ToString();
            MyMyHealthSmoking.ShishaPerYear = dr["ShishaPerYear"].ToString();
            MyMyHealthSmoking.CigarettesPerYear = dr["CigarettesPerYear"].ToString();
            L.Add(MyMyHealthSmoking);
        }
        dr.Close();
        return L;
    }

    public MyHealthSmoking GetMyHealthSmoking(int ID)
    {
        MyHealthSmoking MyMyHealthSmoking = new MyHealthSmoking();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthSmoking(ID);
        if (dr.Read())
        {
            MyMyHealthSmoking.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthSmoking.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthSmoking.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthSmoking.smokeCigarettes = bool.Parse(dr["smokeCigarettes"].ToString());
            MyMyHealthSmoking.smokeShisha = bool.Parse(dr["smokeShisha"].ToString());
            MyMyHealthSmoking.CigarettesPerDay = dr["CigarettesPerDay"].ToString();
            MyMyHealthSmoking.ShishaPerWeek = dr["ShishaPerWeek"].ToString();
            MyMyHealthSmoking.ShishaPerYear = dr["ShishaPerYear"].ToString();
            MyMyHealthSmoking.CigarettesPerYear = dr["CigarettesPerYear"].ToString();
        }
        dr.Close();
        return MyMyHealthSmoking;
    }

    public MyHealthSmoking GetMyHealthSmoking(string value,string type)
    {
        MyHealthSmoking MyMyHealthSmoking = new MyHealthSmoking();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthSmoking(value, type);
        if (dr.Read())
        {
            MyMyHealthSmoking.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthSmoking.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthSmoking.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthSmoking.smokeCigarettes = bool.Parse(dr["smokeCigarettes"].ToString());
            MyMyHealthSmoking.smokeShisha = bool.Parse(dr["smokeShisha"].ToString());
            MyMyHealthSmoking.CigarettesPerDay = dr["CigarettesPerDay"].ToString();
            MyMyHealthSmoking.ShishaPerWeek = dr["ShishaPerWeek"].ToString();
            MyMyHealthSmoking.ShishaPerYear = dr["ShishaPerYear"].ToString();
            MyMyHealthSmoking.CigarettesPerYear = dr["CigarettesPerYear"].ToString();
        }
        dr.Close();
        return MyMyHealthSmoking;
    }

    public void AddMyHealthSmoking(MyHealthSmoking MyMyHealthSmoking)
    {
        MobileProvider.ConcreteDataAccess().InsertMyHealthSmoking(MyMyHealthSmoking.ID, MyMyHealthSmoking.MembershipNo, MyMyHealthSmoking.DeviceID, MyMyHealthSmoking.smokeCigarettes, MyMyHealthSmoking.smokeShisha, MyMyHealthSmoking.CigarettesPerDay, MyMyHealthSmoking.ShishaPerWeek, MyMyHealthSmoking.ShishaPerYear, MyMyHealthSmoking.CigarettesPerYear);
    }

    public void ModifyMyHealthSmoking(MyHealthSmoking MyMyHealthSmoking)
    {
        MobileProvider.ConcreteDataAccess().UpdateMyHealthSmoking(MyMyHealthSmoking.ID, MyMyHealthSmoking.MembershipNo, MyMyHealthSmoking.DeviceID, MyMyHealthSmoking.smokeCigarettes, MyMyHealthSmoking.smokeShisha, MyMyHealthSmoking.CigarettesPerDay, MyMyHealthSmoking.ShishaPerWeek, MyMyHealthSmoking.ShishaPerYear, MyMyHealthSmoking.CigarettesPerYear);
    }

    public void DeleteMyHealthSmoking(int ID)
    {
        MobileProvider.ConcreteDataAccess().DeleteMyHealthSmoking(ID);
    }
}