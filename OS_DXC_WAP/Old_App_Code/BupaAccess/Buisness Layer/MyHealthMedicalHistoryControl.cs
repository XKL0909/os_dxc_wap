﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthMedicalHistoryControl
/// </summary>
public class MyHealthMedicalHistoryControl
{
	public MyHealthMedicalHistoryControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public List<MyHealthMedicalHistory> GetAllMyHealthMedicalHistory()
    {
        List<MyHealthMedicalHistory> L = new List<MyHealthMedicalHistory>();
        MyHealthMedicalHistory MyMyHealthMedicalHistory = default(MyHealthMedicalHistory);
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetAllMyHealthMedicalHistory();
        while (dr.Read())
        {
            MyMyHealthMedicalHistory = new MyHealthMedicalHistory();
            MyMyHealthMedicalHistory.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthMedicalHistory.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthMedicalHistory.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthMedicalHistory.Diabetes_family = bool.Parse(dr["Diabetes_family"].ToString());
            MyMyHealthMedicalHistory.Depression = bool.Parse(dr["Depression"].ToString());
            MyMyHealthMedicalHistory.Fatigue = bool.Parse(dr["Fatigue"].ToString());
            MyMyHealthMedicalHistory.HaveYouBeenHospitalized = bool.Parse(dr["HaveYouBeenHospitalized"].ToString());
            MyMyHealthMedicalHistory.Nightmares = bool.Parse(dr["Nightmares"].ToString());
            MyMyHealthMedicalHistory.Snoring = bool.Parse(dr["Snoring"].ToString());
            MyMyHealthMedicalHistory.Asthma = bool.Parse(dr["Asthma"].ToString());
            MyMyHealthMedicalHistory.SleepDisorderInsomnia = bool.Parse(dr["SleepDisorderInsomnia"].ToString());
            MyMyHealthMedicalHistory.HeartProblem = bool.Parse(dr["HeartProblem"].ToString());
            MyMyHealthMedicalHistory.HighBloodPressure = bool.Parse(dr["HighBloodPressure"].ToString());
            MyMyHealthMedicalHistory.Cancer = bool.Parse(dr["Cancer"].ToString());
            MyMyHealthMedicalHistory.HighCholesterol = bool.Parse(dr["HighCholesterol"].ToString());
            MyMyHealthMedicalHistory.Diabetes = bool.Parse(dr["Diabetes"].ToString());
            MyMyHealthMedicalHistory.HighCholesterol_family = bool.Parse(dr["HighCholesterol_family"].ToString());
            MyMyHealthMedicalHistory.Cancer_family = bool.Parse(dr["Cancer_family"].ToString());
            MyMyHealthMedicalHistory.Epilepsy = bool.Parse(dr["Epilepsy"].ToString());
            MyMyHealthMedicalHistory.Arthritis = bool.Parse(dr["Arthritis"].ToString());
            MyMyHealthMedicalHistory.Migraine = bool.Parse(dr["Migraine"].ToString());
            MyMyHealthMedicalHistory.HighBloodPressure_family = bool.Parse(dr["HighBloodPressure_family"].ToString());
            MyMyHealthMedicalHistory.BackPain = bool.Parse(dr["BackPain"].ToString());
            MyMyHealthMedicalHistory.haveAnySurgery = bool.Parse(dr["haveAnySurgery"].ToString());
            MyMyHealthMedicalHistory.HeartProblem_family = bool.Parse(dr["HeartProblem_family"].ToString());
            MyMyHealthMedicalHistory.ColonPolyps = bool.Parse(dr["ColonPolyps"].ToString());
            MyMyHealthMedicalHistory.ChronicInflammatory = bool.Parse(dr["ChronicInflammatory"].ToString());
            MyMyHealthMedicalHistory.Depression_family = bool.Parse(dr["Depression_family"].ToString());
            L.Add(MyMyHealthMedicalHistory);
        }
        dr.Close();
        return L;
    }

    public MyHealthMedicalHistory GetMyHealthMedicalHistory(string value, string type)
    {
        MyHealthMedicalHistory MyMyHealthMedicalHistory = new MyHealthMedicalHistory();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthMedicalHistory(value, type);
        if (dr.Read())
        {
            MyMyHealthMedicalHistory.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthMedicalHistory.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthMedicalHistory.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthMedicalHistory.Diabetes_family = bool.Parse(dr["Diabetes_family"].ToString());
            MyMyHealthMedicalHistory.Depression = bool.Parse(dr["Depression"].ToString());
            MyMyHealthMedicalHistory.Fatigue = bool.Parse(dr["Fatigue"].ToString());
            MyMyHealthMedicalHistory.HaveYouBeenHospitalized = bool.Parse(dr["HaveYouBeenHospitalized"].ToString());
            MyMyHealthMedicalHistory.Nightmares = bool.Parse(dr["Nightmares"].ToString());
            MyMyHealthMedicalHistory.Snoring = bool.Parse(dr["Snoring"].ToString());
            MyMyHealthMedicalHistory.Asthma = bool.Parse(dr["Asthma"].ToString());
            MyMyHealthMedicalHistory.SleepDisorderInsomnia = bool.Parse(dr["SleepDisorderInsomnia"].ToString());
            MyMyHealthMedicalHistory.HeartProblem = bool.Parse(dr["HeartProblem"].ToString());
            MyMyHealthMedicalHistory.HighBloodPressure = bool.Parse(dr["HighBloodPressure"].ToString());
            MyMyHealthMedicalHistory.Cancer = bool.Parse(dr["Cancer"].ToString());
            MyMyHealthMedicalHistory.HighCholesterol = bool.Parse(dr["HighCholesterol"].ToString());
            MyMyHealthMedicalHistory.Diabetes = bool.Parse(dr["Diabetes"].ToString());
            MyMyHealthMedicalHistory.HighCholesterol_family = bool.Parse(dr["HighCholesterol_family"].ToString());
            MyMyHealthMedicalHistory.Cancer_family = bool.Parse(dr["Cancer_family"].ToString());
            MyMyHealthMedicalHistory.Epilepsy = bool.Parse(dr["Epilepsy"].ToString());
            MyMyHealthMedicalHistory.Arthritis = bool.Parse(dr["Arthritis"].ToString());
            MyMyHealthMedicalHistory.Migraine = bool.Parse(dr["Migraine"].ToString());
            MyMyHealthMedicalHistory.HighBloodPressure_family = bool.Parse(dr["HighBloodPressure_family"].ToString());
            MyMyHealthMedicalHistory.BackPain = bool.Parse(dr["BackPain"].ToString());
            MyMyHealthMedicalHistory.haveAnySurgery = bool.Parse(dr["haveAnySurgery"].ToString());
            MyMyHealthMedicalHistory.HeartProblem_family = bool.Parse(dr["HeartProblem_family"].ToString());
            MyMyHealthMedicalHistory.ColonPolyps = bool.Parse(dr["ColonPolyps"].ToString());
            MyMyHealthMedicalHistory.ChronicInflammatory = bool.Parse(dr["ChronicInflammatory"].ToString());
            MyMyHealthMedicalHistory.Depression_family = bool.Parse(dr["Depression_family"].ToString());
        }
        dr.Close();
        return MyMyHealthMedicalHistory;
    }

    public void AddMyHealthMedicalHistory(MyHealthMedicalHistory MyMyHealthMedicalHistory)
    {
        MobileProvider.ConcreteDataAccess().InsertMyHealthMedicalHistory(MyMyHealthMedicalHistory.ID, MyMyHealthMedicalHistory.MembershipNo, MyMyHealthMedicalHistory.DeviceID, MyMyHealthMedicalHistory.Diabetes_family, MyMyHealthMedicalHistory.Depression, MyMyHealthMedicalHistory.Fatigue, MyMyHealthMedicalHistory.HaveYouBeenHospitalized, MyMyHealthMedicalHistory.Nightmares, MyMyHealthMedicalHistory.Snoring, MyMyHealthMedicalHistory.Asthma,
        MyMyHealthMedicalHistory.SleepDisorderInsomnia, MyMyHealthMedicalHistory.HeartProblem, MyMyHealthMedicalHistory.HighBloodPressure, MyMyHealthMedicalHistory.Cancer, MyMyHealthMedicalHistory.HighCholesterol, MyMyHealthMedicalHistory.Diabetes, MyMyHealthMedicalHistory.HighCholesterol_family, MyMyHealthMedicalHistory.Cancer_family, MyMyHealthMedicalHistory.Epilepsy, MyMyHealthMedicalHistory.Arthritis,
        MyMyHealthMedicalHistory.Migraine, MyMyHealthMedicalHistory.HighBloodPressure_family, MyMyHealthMedicalHistory.BackPain, MyMyHealthMedicalHistory.haveAnySurgery, MyMyHealthMedicalHistory.HeartProblem_family, MyMyHealthMedicalHistory.ColonPolyps, MyMyHealthMedicalHistory.ChronicInflammatory, MyMyHealthMedicalHistory.Depression_family);
    }

    public void ModifyMyHealthMedicalHistory(MyHealthMedicalHistory MyMyHealthMedicalHistory)
    {
        MobileProvider.ConcreteDataAccess().UpdateMyHealthMedicalHistory(MyMyHealthMedicalHistory.ID, MyMyHealthMedicalHistory.MembershipNo, MyMyHealthMedicalHistory.DeviceID, MyMyHealthMedicalHistory.Diabetes_family, MyMyHealthMedicalHistory.Depression, MyMyHealthMedicalHistory.Fatigue, MyMyHealthMedicalHistory.HaveYouBeenHospitalized, MyMyHealthMedicalHistory.Nightmares, MyMyHealthMedicalHistory.Snoring, MyMyHealthMedicalHistory.Asthma,
        MyMyHealthMedicalHistory.SleepDisorderInsomnia, MyMyHealthMedicalHistory.HeartProblem, MyMyHealthMedicalHistory.HighBloodPressure, MyMyHealthMedicalHistory.Cancer, MyMyHealthMedicalHistory.HighCholesterol, MyMyHealthMedicalHistory.Diabetes, MyMyHealthMedicalHistory.HighCholesterol_family, MyMyHealthMedicalHistory.Cancer_family, MyMyHealthMedicalHistory.Epilepsy, MyMyHealthMedicalHistory.Arthritis,
        MyMyHealthMedicalHistory.Migraine, MyMyHealthMedicalHistory.HighBloodPressure_family, MyMyHealthMedicalHistory.BackPain, MyMyHealthMedicalHistory.haveAnySurgery, MyMyHealthMedicalHistory.HeartProblem_family, MyMyHealthMedicalHistory.ColonPolyps, MyMyHealthMedicalHistory.ChronicInflammatory, MyMyHealthMedicalHistory.Depression_family);
    }

    public void DeleteMyHealthMedicalHistory(int ID)
    {
        MobileProvider.ConcreteDataAccess().DeleteMyHealthMedicalHistory(ID);
    }

    //=======================================================
    //Service provided by Telerik (www.telerik.com)
    //Conversion powered by NRefactory.
    //Twitter: @telerik
    //Facebook: facebook.com/telerik
    //=======================================================

}