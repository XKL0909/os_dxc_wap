﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using Utility.Configuration;
using Utility.Data;
using Microsoft.ApplicationBlocks.Data;
using NReco.PdfGenerator;

/// <summary>
/// Summary description for MemberControl
/// </summary>
public class MemberControl
{
    #region Fields
        private static OS_DXC_WAP.CaesarWS.ServiceDepot_DNService _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
    #endregion

    public MemberRegInfo GetMemberRegistrationDetails(string MembershipNumber)
    {

        MemberRegInfo MyMember = new MemberRegInfo();
        try
        {
            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();

            request.membershipNo = MembershipNumber;
            request.transactionID = WebPublication.GenerateTransactionID();
            request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
            request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];

            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN memberService = _ws.EnqMbrDetInfo(request);
            if (memberService.errorID != "0")
            {
                MyMember.Result = false;
                MyMember.ErrorMessage = "Error when getting member details: " + memberService.errorMessage;
            }
            else
            {

                MyMember.MemberName = memberService.detail[0].memberName;

                MyMember.MembershipNumber = memberService.detail[0].membershipNo.ToString();

                MyMember.Status = memberService.detail[0].status;
                MyMember.Customer = memberService.detail[0].customerName;

                MyMember.ContractNumber = memberService.detail[0].custNo;

                MyMember.Result = true;
                MyMember.ErrorMessage = "";
            }
        }
        catch (Exception ex)
        {
            MyMember.Result = false;
            MyMember.ErrorMessage = ex.Message;
        }

        return MyMember;
    }

    public string RegisterUser(RegistrationApplication Reg)
    {
        string _result;

        string _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
        try
        {
            DataParameter membership = new DataParameter(SqlDbType.NVarChar, "@MembershipNo", Reg.Membership, ParameterDirection.Input);
            DataParameter password = new DataParameter(SqlDbType.NVarChar, "@Password", Reg.Password, ParameterDirection.Input);
            DataParameter fullName = new DataParameter(SqlDbType.NVarChar, "@FullName1", Reg.Fullname, ParameterDirection.Input);
            DataParameter email = new DataParameter(SqlDbType.NVarChar, "@Email", Reg.Email, ParameterDirection.Input);
            DataParameter mobile = new DataParameter(SqlDbType.NVarChar, "@Mobile", Reg.Mobile, ParameterDirection.Input);

            DataParameter result = new DataParameter(SqlDbType.NVarChar, "@Result", 30, ParameterDirection.Output);

            DataParameter[] dpBasket = new DataParameter[] { membership, password, fullName, email, mobile, result };

            DataManager.ExecuteStoredProcedureCachedReturn(_connection, "dbo.spRegisterUser", ref dpBasket, 60000);
            _result = dpBasket[5].ParamValue.ToString();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
        return _result;
    }

    public CaeserException LoginMember(string _username, string _password)
    {
        SqlConnection connection = null;
        CaeserException MyExp = new CaeserException();
        try
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);
                connection.Open();
            }
            catch(Exception ex)
            {
                MyExp.Result = false;
                MyExp.ErrorMessage = ex.Message;
                connection.Close();
                return MyExp;
            }
            // Set up parameters (2 input and 3 output) 
            SqlParameter[] arParms = new SqlParameter[5];
            arParms[0] = new SqlParameter("@User_Name", SqlDbType.NVarChar, 30);
            arParms[0].Value = _username;

            arParms[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 30);
            arParms[1].Value = _password;

            // @ProductName Output Parameter
            arParms[2] = new SqlParameter("@MembershipNo", SqlDbType.NVarChar, 30);
            arParms[2].Direction = ParameterDirection.Output;

            // @UnitPrice Output Parameter
            arParms[3] = new SqlParameter("@FullName1", SqlDbType.NVarChar, 100);
            arParms[3].Direction = ParameterDirection.Output;

            // @QtyPerUnit Output Parameter
            arParms[4] = new SqlParameter("@Email", SqlDbType.NVarChar, 30);
            arParms[4].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spCheckUserExist", arParms);

            if (!string.IsNullOrEmpty(Convert.ToString(arParms[2].Value)))
            {
                MyExp.Result = true;
                MemberRegInfo MyMember = new MemberRegInfo();
                MemberControl MyMemberC = new MemberControl();
                MyMember = MyMemberC.GetMemberRegistrationDetails(_username);
                if (MyMember.ContractNumber == "32559700" || MyMember.ContractNumber == "33586300")
                {
                    MyExp.IsSabic = true;
                }
                else {
                    MyExp.IsSabic = false;
                }
                MyExp.ErrorMessage = "";
                connection.Close();
            }
            else
            {
                MyExp.Result = false;
                MyExp.ErrorMessage = "Authentication Failed. Incorrect Membership No. or Password. Please try again.";
                connection.Close();
            }
        }
        catch (Exception ex)
        {
            MyExp.Result = false;
            MyExp.ErrorMessage = ex.Message;
            connection.Close();
            return MyExp;
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }

        return MyExp;
    }

    public List<Dependent> GetDependentsMembershipNumbers(string MemberNo)
    {
        List<Dependent> MyDependents = new List<Dependent>();
        Dependent MyDependent;
        _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrListInfoRequest_DN();
        OS_DXC_WAP.CaesarWS.EnqMbrListInfoResponse_DN response;
        request.membershipNo = MemberNo;
        request.transactionID = WebPublication.GenerateTransactionID();
        request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
        request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];
        try
        {
            response = _ws.EnqMbrListInfo(request);
            if (response.errorID != "0")
            {
                return null;
            }
            else
            {
                foreach (OS_DXC_WAP.CaesarWS.MbrListDetail_DN dtl in response.detail)
                {
                    if (dtl.membershipNo.ToString() != MemberNo)
                    {
                        MyDependent = new Dependent();
                        MyDependent.MembershipNumber = dtl.membershipNo.ToString();
                        MyDependent.MemberName = dtl.memberName;
                        MyDependent.RelationShip = dtl.relationship;
                        MyDependents.Add(MyDependent);
                    }
                }

                return MyDependents;
            }
        }
        catch (Exception ex)
        {
            return null;
        }

    }

    #region MyDetails
    public OS_DXC_WAP.CaesarWS.MbrDetail_DN GetMemberDetails(string MemberNo)
    {
        _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
        OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            request.membershipNo = MemberNo;
            request.transactionID = WebPublication.GenerateTransactionID();
            request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
            request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];
            response = _ws.EnqMbrDetInfo(request);

            if (response.detail[0].memberName != null)
            {
                //HttpContext.Current.Response.Write(response.detail[0].endDate);
                //response.detail[0].endDate = DateTime.Parse(String.Format("{0:d}", response.detail[0].endDate));
                //response.detail[0].startDate = DateTime.Parse(String.Format("{0:d}", response.detail[0].startDate));
                //response.detail[0].memberDOB = (DateTime)(String.Format("{0:d}", response.detail[0].memberDOB));
                return response.detail[0];
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;

        }

    }

    public List<OS_DXC_WAP.CaesarWS.MbrDetail_DN> GetMemberAndDependentsDetails(string MemberNo) {
        try
        {
            List<OS_DXC_WAP.CaesarWS.MbrDetail_DN> AllDetails = new List<OS_DXC_WAP.CaesarWS.MbrDetail_DN>();
            OS_DXC_WAP.CaesarWS.MbrDetail_DN MyDetail = new OS_DXC_WAP.CaesarWS.MbrDetail_DN();
            MyDetail = this.GetMemberDetails(MemberNo);
            AllDetails.Add(MyDetail);
            List<Dependent> MyDependents = new List<Dependent>();
            MyDependents = this.GetDependentsMembershipNumbers(MemberNo);
            if (MyDependents != null)
            {
                for (int i = 0; i < MyDependents.Count; i++)
                {
                    MyDetail = new OS_DXC_WAP.CaesarWS.MbrDetail_DN();
                    MyDetail = this.GetMemberDetails(MyDependents[i].MembershipNumber);
                    AllDetails.Add(MyDetail);
                }
            }
            return AllDetails;
        }
        catch (Exception ex) {
            return null;
        }
    }
    #endregion

    #region ClaimHistory
    public ClaimHistoryDetails GetMemberClaimHistory(string MemberNo)
    {
        _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqClmHisInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqClmHisInfoResponse_DN response;
        try
        {
            response = null;
            request = new OS_DXC_WAP.CaesarWS.EnqClmHisInfoRequest_DN();
            request.MembershipNo = MemberNo;
            request.TransactionID = WebPublication.GenerateTransactionID();
            request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
            request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];

            response = _ws.EnqClmHisInfo(request);

            if (response.detail != null)
            {
                ClaimHistoryDetails MyClaimHistory = new ClaimHistoryDetails(response.detail, MemberNo);
                return MyClaimHistory;

            }
            else {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public List<ClaimHistoryDetails> GetMemberAndDependentsClaimHistory(string MemberNo)
    {
        try
        {
            List<ClaimHistoryDetails> AllDetails = new List<ClaimHistoryDetails>();
            ClaimHistoryDetails MyDetail;
            MyDetail = this.GetMemberClaimHistory(MemberNo);
            AllDetails.Add(MyDetail);
            List<Dependent> MyDependents = new List<Dependent>();
            MyDependents = this.GetDependentsMembershipNumbers(MemberNo);
            if (MyDependents != null)
            {
                for (int i = 0; i < MyDependents.Count; i++)
                {
                    MyDetail = this.GetMemberClaimHistory(MyDependents[i].MembershipNumber);
                    AllDetails.Add(MyDetail);
                }
            }
            return AllDetails;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    #endregion

    #region MyCoverage
    public CoverageDetails GetCoverage(string MemberNo)
        {
            
            _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

            OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN response;
            try
            {
                // Cursor.Current = Cursors.WaitCursor;
                response = null;
                request = new OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN();
                request.membershipNo = MemberNo;
                request.transactionID = WebPublication.GenerateTransactionID();
                request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
                request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];

                response = _ws.EnqProdCovInfo(request);
                //HttpContext.Current.Response.Write(response.status);
                if (response.className != null)
                {
                    CoverageDetails MyCoverage = new CoverageDetails(response);
                    MyCoverage.membershipNumber = MemberNo;
                    return MyCoverage;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;

            }

        }
        public List<OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN> GetMemberAndDependentsCoverage(string MemberNo)
    {
        try
        {
            List<OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN> AllDetails = new List<OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN>();
            OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN MyDetail = new OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN();
            MyDetail = this.GetCoverage(MemberNo);
            AllDetails.Add(MyDetail);
            List<Dependent> MyDependents = new List<Dependent>();
            MyDependents = this.GetDependentsMembershipNumbers(MemberNo);
            if (MyDependents != null) { 
                for (int i = 0; i < MyDependents.Count; i++)
                {
                    MyDetail = new OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN();
                    MyDetail = this.GetCoverage(MyDependents[i].MembershipNumber);
                    AllDetails.Add(MyDetail);
                }
            }
            return AllDetails;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    #endregion

    #region MyNetwork
        public OS_DXC_WAP.CaesarWS.ProvDetail_DN[] GetRegionProviders(string MemberNo,string Code){
    try
    {
        _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        OS_DXC_WAP.CaesarWS.EnqProvLocInfoRequest_DN request;
        OS_DXC_WAP.CaesarWS.EnqProvLocInfoResponse_DN response;

        response = null;
        request = new OS_DXC_WAP.CaesarWS.EnqProvLocInfoRequest_DN();
        request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
        request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];
        request.transactionID = WebPublication.GenerateTransactionID();
        request.membershipNo = MemberNo;
        request.region = Code;
        response = _ws.EnqProvLocInfo(request);
        if (response.errorID != "0")
        {
            return null;
        }
        else
        {
            return response.detail;

        }
    }
    catch (Exception ex) {
        return null;
    }
    }
    #endregion

    #region Preauthorization
        public PreAuthDetails GetPreAuthorization(string MemberNo)
        {
            _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

            OS_DXC_WAP.CaesarWS.EnqPreHisInfoRequest_DN request;
            OS_DXC_WAP.CaesarWS.EnqPreHisInfoResponse_DN response;
            try
            {
                // Cursor.Current = Cursors.WaitCursor;
                response = null;
                request = new OS_DXC_WAP.CaesarWS.EnqPreHisInfoRequest_DN();
                request.membershipNo = MemberNo;
                request.transactionID = WebPublication.GenerateTransactionID();
                request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
                request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];

                response = _ws.EnqPreHisInfo(request);

                if (response.detail != null)
                {
                    PreAuthDetails MyPreAuthDetails = new PreAuthDetails(response, MemberNo);
                    return MyPreAuthDetails;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;

            }
        }
        public List<PreAuthDetails> GetMemberAndDependentsPreAuthorization(string MemberNo)
        {
            try
            {
                List<PreAuthDetails> AllDetails = new List<PreAuthDetails>();
                PreAuthDetails MyDetail = new PreAuthDetails();
                MyDetail = this.GetPreAuthorization(MemberNo);
                AllDetails.Add(MyDetail);
                List<Dependent> MyDependents = new List<Dependent>();
                MyDependents = this.GetDependentsMembershipNumbers(MemberNo);
                if (MyDependents != null)
                {
                    for (int i = 0; i < MyDependents.Count; i++)
                    {
                        MyDetail = new  PreAuthDetails();
                        MyDetail = this.GetPreAuthorization(MyDependents[i].MembershipNumber);
                        AllDetails.Add(MyDetail);
                    }
                }
                return AllDetails;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    #endregion

    #region MyCertificate
        public OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN GetCertificateDetails(string MemberNo)
        {
            _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();
            OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN request;
            OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response;
            try
            {
                response = null;
                request = new OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoRequest_DN();
                request.membershipNo = long.Parse(MemberNo);
                request.transactionID = WebPublication.GenerateTransactionID();
                request.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
                request.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];
                request.memberLoginIND = "Y";
                response = _ws.ReqPrtMbrCrtInfo(request);

                if (response.memberName != null)
                {
                    return response;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;

            }

        }

        public string GenerateCertificateAsHtml(OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response)
        {
            StringBuilder MyCertificate = new StringBuilder(10000);

            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request1;
            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response1;

            OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN request2;
            OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsResponse_DN response2;

            OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN request3;
            OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN response3;

            request1 = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            request1.membershipNo = Convert.ToString(response.membershipNo.ToString());

            request1.Username = WebPublication.CaesarSvcUsername;
            request1.Password = WebPublication.CaesarSvcPassword;
            request1.transactionID = WebPublication.GenerateTransactionID();
            response1 = _ws.EnqMbrDetInfo(request1);

            request2 = new OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN();
            request2.contractNo = response1.detail[0].custNo;
            request2.membershipNo = Convert.ToString(response.membershipNo.ToString());
            request2.Username = WebPublication.CaesarSvcUsername;
            request2.Password = WebPublication.CaesarSvcPassword;
            request2.transactionID = WebPublication.GenerateTransactionID();
            request2.providerCode = "";

            response2 = _ws.ReqTOB(request2);



            response3 = null;
            request3 = new OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN();
            request3.membershipNo = Convert.ToString(response.membershipNo.ToString());
            request3.transactionID = WebPublication.GenerateTransactionID();

            request3.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
            request3.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];

            response3 = _ws.EnqProdCovInfo(request3);

            System.IO.StreamReader sr = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("CertificateTemplate.html"));
            MyCertificate.Append(sr.ReadToEnd());
            sr.Close();
            //if (response3.status == "0")
            //    MyCertificate = MyCertificate.Replace("", response3.proratedOverallAnnualLimit.ToString("C").Replace("£", ""));

            MyCertificate = MyCertificate.Replace("#Subject#", "Certificate of Membership");
            MyCertificate = MyCertificate.Replace("#SubjectAr#", "شهادة عضوية");
            MyCertificate = MyCertificate.Replace("#Name#", response.memberName);
            MyCertificate = MyCertificate.Replace("#CustomerName#", response1.detail[0].customerName);
            MyCertificate = MyCertificate.Replace("#CustNo#" , response1.detail[0].custNo);
            MyCertificate = MyCertificate.Replace("#Cover#" , response1.detail[0].schemeName);
            MyCertificate = MyCertificate.Replace("#MemberName#" , response.memberName);
            MyCertificate = MyCertificate.Replace("#MembershipNumber#" , response.membershipNo.ToString());
            MyCertificate = MyCertificate.Replace("#ClassName#" , response.className);
            MyCertificate = MyCertificate.Replace("#Options#" , response.option);
            MyCertificate = MyCertificate.Replace("#IssueDate#" , DateTime.Now.ToShortDateString());
            MyCertificate = MyCertificate.Replace("#EffectiveFrom#" , String.Format("{0:d}", response.effFrom));
            MyCertificate = MyCertificate.Replace("#YearOfBirth#" , String.Format("{0:d}", response.DOB));
            MyCertificate = MyCertificate.Replace("#StaffNo#" , response.staffNo);
            MyCertificate = MyCertificate.Replace("#Contribution#" , response.deductible);
            MyCertificate = MyCertificate.Replace("#EffectiveTo#" , String.Format("{0:d}", response.effTo));
            MyCertificate = MyCertificate.Replace("#RoomType#", response.roomType);
            return MyCertificate.ToString();
        }

        public string GenerateVisaApplicationAsHtml(OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response)
        {
            StringBuilder MyCertificate = new StringBuilder(10000);

            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request1;
            OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN response1;

            OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN request2;
            OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsResponse_DN response2;

            OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN request3;
            OS_DXC_WAP.CaesarWS.EnqProdCovInfoResponse_DN response3;

            request1 = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
            request1.membershipNo = Convert.ToString(response.membershipNo.ToString());

            request1.Username = WebPublication.CaesarSvcUsername;
            request1.Password = WebPublication.CaesarSvcPassword;
            request1.transactionID = WebPublication.GenerateTransactionID();
            response1 = _ws.EnqMbrDetInfo(request1);

            request2 = new OS_DXC_WAP.CaesarWS.RequestTableOfBenefitsRequest_DN();
            request2.contractNo = response1.detail[0].custNo;
            request2.membershipNo = Convert.ToString(response.membershipNo.ToString());
            request2.Username = WebPublication.CaesarSvcUsername;
            request2.Password = WebPublication.CaesarSvcPassword;
            request2.transactionID = WebPublication.GenerateTransactionID();
            request2.providerCode = "";

            response2 = _ws.ReqTOB(request2);



            response3 = null;
            request3 = new OS_DXC_WAP.CaesarWS.EnqProdCovInfoRequest_DN();
            request3.membershipNo = Convert.ToString(response.membershipNo.ToString());
            request3.transactionID = WebPublication.GenerateTransactionID();

            request3.Username = ConfigurationManager.AppSettings["CaesarSvc Username"];
            request3.Password = ConfigurationManager.AppSettings["CaesarSvc Password"];

            response3 = _ws.EnqProdCovInfo(request3);

            System.IO.StreamReader sr = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("VisaApplicationTemplate.html"));
            MyCertificate.Append(sr.ReadToEnd());
            sr.Close();
            if (response3.status == "0")
                MyCertificate = MyCertificate.Replace("#Amount#", response3.proratedOverallAnnualLimit.ToString("C").Replace("£", ""));

            MyCertificate = MyCertificate.Replace("#Subject#", "Membership certificate for visa application");
            MyCertificate = MyCertificate.Replace("#SubjectAr#", "شهادة عضوية لطلب التأشيرة");
            MyCertificate = MyCertificate.Replace("#ContractName#", response1.detail[0].customerName);
            MyCertificate = MyCertificate.Replace("#ContractID#", response1.detail[0].custNo);
            MyCertificate = MyCertificate.Replace("#LevelOfCover#", response1.detail[0].schemeName);
            MyCertificate = MyCertificate.Replace("#MemberName#", response.memberName);
            MyCertificate = MyCertificate.Replace("#MembershipNumber#", response.membershipNo.ToString());
            MyCertificate = MyCertificate.Replace("#YearOfBirth#", String.Format("{0:d}", response.DOB));
            MyCertificate = MyCertificate.Replace("#ExpiryDate#", String.Format("{0:d}", response.effTo));
            return MyCertificate.ToString();
        }

        public string ConvertCertificateToPDF(string HTMLCertificate) {

            String filename = ConfigurationManager.AppSettings["PhysicalPath"] + "/BupaAccess/Certificate/GeneratedCertificates/Certificate-" + DateTime.Now.ToString("yyyyMMddhhmmssff") + ".pdf";
            Byte[] pdfBytes; 
            HtmlToPdfConverter myPDfConverter = new HtmlToPdfConverter();
            myPDfConverter.Orientation = PageOrientation.Portrait;
            
            PageMargins myMargins = new PageMargins();
            myMargins.Bottom = 0;
            myMargins.Top = 0;
            myMargins.Left = 0;
            myMargins.Right = 0;
            myPDfConverter.Margins = myMargins;
            myPDfConverter.Size = PageSize.A4;
            //myPDfConverter.Zoom = 90

            pdfBytes = myPDfConverter.GeneratePdf(HTMLCertificate);
            System.IO.MemoryStream myMemoryStream = new System.IO.MemoryStream(pdfBytes);

            System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath(filename), pdfBytes);
            return filename;
        }

        public string GenerateCertificate(string MemberNo, string type) {
            string CertificatePath = "";
            string CertificateHtml = "";
            OS_DXC_WAP.CaesarWS.ReqPrtMbrCrtInfoResponse_DN response = GetCertificateDetails(MemberNo);
            switch (type) { 
                case "visa":
                    CertificateHtml = GenerateVisaApplicationAsHtml(response);
                    break;
                case "certificate":
                    CertificateHtml = GenerateCertificateAsHtml(response);
                    break;
            }
            CertificatePath = ConvertCertificateToPDF(CertificateHtml);
            return CertificatePath;
        }
    #endregion
}