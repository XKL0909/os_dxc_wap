﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthStressControl
/// </summary>
public class MyHealthStressControl
{
	public MyHealthStressControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public List<MyHealthStress> GetAllMyHealthStress()
    {
        List<MyHealthStress> L = new List<MyHealthStress>();
        MyHealthStress MyMyHealthStress = default(MyHealthStress);
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetAllMyHealthStress();
        while (dr.Read())
        {
            MyMyHealthStress = new MyHealthStress();
            MyMyHealthStress.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthStress.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthStress.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthStress.otherEnvironmentalStress = dr["otherEnvironmentalStress"].ToString();
            MyMyHealthStress.irritated = dr["irritated"].ToString();
            MyMyHealthStress.Unemployment = dr["Unemployment"].ToString();
            MyMyHealthStress.WorkOverload = dr["WorkOverload"].ToString();
            MyMyHealthStress.TooManyChildren = dr["TooManyChildren"].ToString();
            MyMyHealthStress.RoleAmbiguity = dr["RoleAmbiguity"].ToString();
            MyMyHealthStress.difficultToTolerateInterruptions = dr["difficultToTolerateInterruptions"].ToString();
            MyMyHealthStress.HealthIssues = dr["HealthIssues"].ToString();
            MyMyHealthStress.hardToCalmDown = dr["hardToCalmDown"].ToString();
            MyMyHealthStress.difficultToRelax = dr["difficultToRelax"].ToString();
            MyMyHealthStress.Loneliness = dr["Loneliness"].ToString();
            MyMyHealthStress.Divorce = dr["Divorce"].ToString();
            MyMyHealthStress.regularHealthTips = dr["regularHealthTips"].ToString();
            MyMyHealthStress.impatientWithDelays = dr["impatientWithDelays"].ToString();
            MyMyHealthStress.upsetRatherEasily = dr["upsetRatherEasily"].ToString();
            MyMyHealthStress.JobChange = dr["JobChange"].ToString();
            MyMyHealthStress.ParentalConflict = dr["ParentalConflict"].ToString();
            MyMyHealthStress.upsetOverMinorThings = dr["upsetOverMinorThings"].ToString();
            MyMyHealthStress.inflexibleWorkingHours = dr["inflexibleWorkingHours"].ToString();
            MyMyHealthStress.Promotions = dr["Promotions"].ToString();
            MyMyHealthStress.Marriage = dr["Marriage"].ToString();
            MyMyHealthStress.TooMuchToDo = dr["TooMuchToDo"].ToString();
            MyMyHealthStress.optimisticPerson = dr["optimisticPerson"].ToString();
            MyMyHealthStress.easilyGetNervous = dr["easilyGetNervous"].ToString();
            MyMyHealthStress.financialDifficulties = dr["financialDifficulties"].ToString();
            MyMyHealthStress.LowSelfConfidence = dr["LowSelfConfidence"].ToString();
            MyMyHealthStress.otherMajorWorkRelatedStress = dr["otherMajorWorkRelatedStress"].ToString();
            MyMyHealthStress.OverReact = dr["OverReact"].ToString();
            MyMyHealthStress.DeathInTheFamily = dr["DeathInTheFamily"].ToString();
            MyMyHealthStress.LackOfSocialSupport = dr["LackOfSocialSupport"].ToString();
            MyMyHealthStress.losingControl = dr["losingControl"].ToString();
            L.Add(MyMyHealthStress);
        }
        dr.Close();
        return L;
    }

    public MyHealthStress GetMyHealthStress(int ID)
    {
        MyHealthStress MyMyHealthStress = new MyHealthStress();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthStress(ID);
        if (dr.Read())
        {
            MyMyHealthStress.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthStress.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthStress.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthStress.otherEnvironmentalStress = dr["otherEnvironmentalStress"].ToString();
            MyMyHealthStress.irritated = dr["irritated"].ToString();
            MyMyHealthStress.Unemployment = dr["Unemployment"].ToString();
            MyMyHealthStress.WorkOverload = dr["WorkOverload"].ToString();
            MyMyHealthStress.TooManyChildren = dr["TooManyChildren"].ToString();
            MyMyHealthStress.RoleAmbiguity = dr["RoleAmbiguity"].ToString();
            MyMyHealthStress.difficultToTolerateInterruptions = dr["difficultToTolerateInterruptions"].ToString();
            MyMyHealthStress.HealthIssues = dr["HealthIssues"].ToString();
            MyMyHealthStress.hardToCalmDown = dr["hardToCalmDown"].ToString();
            MyMyHealthStress.difficultToRelax = dr["difficultToRelax"].ToString();
            MyMyHealthStress.Loneliness = dr["Loneliness"].ToString();
            MyMyHealthStress.Divorce = dr["Divorce"].ToString();
            MyMyHealthStress.regularHealthTips = dr["regularHealthTips"].ToString();
            MyMyHealthStress.impatientWithDelays = dr["impatientWithDelays"].ToString();
            MyMyHealthStress.upsetRatherEasily = dr["upsetRatherEasily"].ToString();
            MyMyHealthStress.JobChange = dr["JobChange"].ToString();
            MyMyHealthStress.ParentalConflict = dr["ParentalConflict"].ToString();
            MyMyHealthStress.upsetOverMinorThings = dr["upsetOverMinorThings"].ToString();
            MyMyHealthStress.inflexibleWorkingHours = dr["inflexibleWorkingHours"].ToString();
            MyMyHealthStress.Promotions = dr["Promotions"].ToString();
            MyMyHealthStress.Marriage = dr["Marriage"].ToString();
            MyMyHealthStress.TooMuchToDo = dr["TooMuchToDo"].ToString();
            MyMyHealthStress.optimisticPerson = dr["optimisticPerson"].ToString();
            MyMyHealthStress.easilyGetNervous = dr["easilyGetNervous"].ToString();
            MyMyHealthStress.financialDifficulties = dr["financialDifficulties"].ToString();
            MyMyHealthStress.LowSelfConfidence = dr["LowSelfConfidence"].ToString();
            MyMyHealthStress.otherMajorWorkRelatedStress = dr["otherMajorWorkRelatedStress"].ToString();
            MyMyHealthStress.OverReact = dr["OverReact"].ToString();
            MyMyHealthStress.DeathInTheFamily = dr["DeathInTheFamily"].ToString();
            MyMyHealthStress.LackOfSocialSupport = dr["LackOfSocialSupport"].ToString();
            MyMyHealthStress.losingControl = dr["losingControl"].ToString();
        }
        dr.Close();
        return MyMyHealthStress;
    }

    public MyHealthStress GetMyHealthStress(string value,string type)
    {
        MyHealthStress MyMyHealthStress = new MyHealthStress();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthStress(value, type);
        if (dr.Read())
        {
            MyMyHealthStress.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthStress.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthStress.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthStress.otherEnvironmentalStress = dr["otherEnvironmentalStress"].ToString();
            MyMyHealthStress.irritated = dr["irritated"].ToString();
            MyMyHealthStress.Unemployment = dr["Unemployment"].ToString();
            MyMyHealthStress.WorkOverload = dr["WorkOverload"].ToString();
            MyMyHealthStress.TooManyChildren = dr["TooManyChildren"].ToString();
            MyMyHealthStress.RoleAmbiguity = dr["RoleAmbiguity"].ToString();
            MyMyHealthStress.difficultToTolerateInterruptions = dr["difficultToTolerateInterruptions"].ToString();
            MyMyHealthStress.HealthIssues = dr["HealthIssues"].ToString();
            MyMyHealthStress.hardToCalmDown = dr["hardToCalmDown"].ToString();
            MyMyHealthStress.difficultToRelax = dr["difficultToRelax"].ToString();
            MyMyHealthStress.Loneliness = dr["Loneliness"].ToString();
            MyMyHealthStress.Divorce = dr["Divorce"].ToString();
            MyMyHealthStress.regularHealthTips = dr["regularHealthTips"].ToString();
            MyMyHealthStress.impatientWithDelays = dr["impatientWithDelays"].ToString();
            MyMyHealthStress.upsetRatherEasily = dr["upsetRatherEasily"].ToString();
            MyMyHealthStress.JobChange = dr["JobChange"].ToString();
            MyMyHealthStress.ParentalConflict = dr["ParentalConflict"].ToString();
            MyMyHealthStress.upsetOverMinorThings = dr["upsetOverMinorThings"].ToString();
            MyMyHealthStress.inflexibleWorkingHours = dr["inflexibleWorkingHours"].ToString();
            MyMyHealthStress.Promotions = dr["Promotions"].ToString();
            MyMyHealthStress.Marriage = dr["Marriage"].ToString();
            MyMyHealthStress.TooMuchToDo = dr["TooMuchToDo"].ToString();
            MyMyHealthStress.optimisticPerson = dr["optimisticPerson"].ToString();
            MyMyHealthStress.easilyGetNervous = dr["easilyGetNervous"].ToString();
            MyMyHealthStress.financialDifficulties = dr["financialDifficulties"].ToString();
            MyMyHealthStress.LowSelfConfidence = dr["LowSelfConfidence"].ToString();
            MyMyHealthStress.otherMajorWorkRelatedStress = dr["otherMajorWorkRelatedStress"].ToString();
            MyMyHealthStress.OverReact = dr["OverReact"].ToString();
            MyMyHealthStress.DeathInTheFamily = dr["DeathInTheFamily"].ToString();
            MyMyHealthStress.LackOfSocialSupport = dr["LackOfSocialSupport"].ToString();
            MyMyHealthStress.losingControl = dr["losingControl"].ToString();
        }
        dr.Close();
        return MyMyHealthStress;
    }

    public void AddMyHealthStress(MyHealthStress MyMyHealthStress)
    {
        MobileProvider.ConcreteDataAccess().InsertMyHealthStress(MyMyHealthStress.ID, MyMyHealthStress.MembershipNo, MyMyHealthStress.DeviceID, MyMyHealthStress.otherEnvironmentalStress, MyMyHealthStress.irritated, MyMyHealthStress.Unemployment, MyMyHealthStress.WorkOverload, MyMyHealthStress.TooManyChildren, MyMyHealthStress.RoleAmbiguity, MyMyHealthStress.difficultToTolerateInterruptions,
        MyMyHealthStress.HealthIssues, MyMyHealthStress.hardToCalmDown, MyMyHealthStress.difficultToRelax, MyMyHealthStress.Loneliness, MyMyHealthStress.Divorce, MyMyHealthStress.regularHealthTips, MyMyHealthStress.impatientWithDelays, MyMyHealthStress.upsetRatherEasily, MyMyHealthStress.JobChange, MyMyHealthStress.ParentalConflict,
        MyMyHealthStress.upsetOverMinorThings, MyMyHealthStress.inflexibleWorkingHours, MyMyHealthStress.Promotions, MyMyHealthStress.Marriage, MyMyHealthStress.TooMuchToDo, MyMyHealthStress.optimisticPerson, MyMyHealthStress.easilyGetNervous, MyMyHealthStress.financialDifficulties, MyMyHealthStress.LowSelfConfidence, MyMyHealthStress.otherMajorWorkRelatedStress,
        MyMyHealthStress.OverReact, MyMyHealthStress.DeathInTheFamily, MyMyHealthStress.LackOfSocialSupport, MyMyHealthStress.losingControl);
    }

    public void ModifyMyHealthStress(MyHealthStress MyMyHealthStress)
    {
        MobileProvider.ConcreteDataAccess().UpdateMyHealthStress(MyMyHealthStress.ID, MyMyHealthStress.MembershipNo, MyMyHealthStress.DeviceID, MyMyHealthStress.otherEnvironmentalStress, MyMyHealthStress.irritated, MyMyHealthStress.Unemployment, MyMyHealthStress.WorkOverload, MyMyHealthStress.TooManyChildren, MyMyHealthStress.RoleAmbiguity, MyMyHealthStress.difficultToTolerateInterruptions,
        MyMyHealthStress.HealthIssues, MyMyHealthStress.hardToCalmDown, MyMyHealthStress.difficultToRelax, MyMyHealthStress.Loneliness, MyMyHealthStress.Divorce, MyMyHealthStress.regularHealthTips, MyMyHealthStress.impatientWithDelays, MyMyHealthStress.upsetRatherEasily, MyMyHealthStress.JobChange, MyMyHealthStress.ParentalConflict,
        MyMyHealthStress.upsetOverMinorThings, MyMyHealthStress.inflexibleWorkingHours, MyMyHealthStress.Promotions, MyMyHealthStress.Marriage, MyMyHealthStress.TooMuchToDo, MyMyHealthStress.optimisticPerson, MyMyHealthStress.easilyGetNervous, MyMyHealthStress.financialDifficulties, MyMyHealthStress.LowSelfConfidence, MyMyHealthStress.otherMajorWorkRelatedStress,
        MyMyHealthStress.OverReact, MyMyHealthStress.DeathInTheFamily, MyMyHealthStress.LackOfSocialSupport, MyMyHealthStress.losingControl);
    }

    public void DeleteMyHealthStress(int ID)
    {
        MobileProvider.ConcreteDataAccess().DeleteMyHealthStress(ID);
    }

}