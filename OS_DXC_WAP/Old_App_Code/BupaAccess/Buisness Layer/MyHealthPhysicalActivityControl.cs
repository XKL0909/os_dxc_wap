﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthPhysicalActivityControl
/// </summary>
public class MyHealthPhysicalActivityControl
{
	public MyHealthPhysicalActivityControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public List<MyHealthPhysicalActivity> GetAllMyHealthPhysicalActivity()
    {
        List<MyHealthPhysicalActivity> L = new List<MyHealthPhysicalActivity>();
        MyHealthPhysicalActivity MyMyHealthPhysicalActivity = default(MyHealthPhysicalActivity);
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetAllMyHealthPhysicalActivity();
        while (dr.Read())
        {
            MyMyHealthPhysicalActivity = new MyHealthPhysicalActivity();
            MyMyHealthPhysicalActivity.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthPhysicalActivity.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthPhysicalActivity.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthPhysicalActivity.SleepPerDay = dr["SleepPerDay"].ToString();
            MyMyHealthPhysicalActivity.ExercisePerWeek = dr["ExercisePerWeek"].ToString();
            L.Add(MyMyHealthPhysicalActivity);
        }
        dr.Close();
        return L;
    }

    public MyHealthPhysicalActivity GetMyHealthPhysicalActivity(int ID)
    {
        MyHealthPhysicalActivity MyMyHealthPhysicalActivity = new MyHealthPhysicalActivity();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthPhysicalActivity(ID);
        if (dr.Read())
        {
            MyMyHealthPhysicalActivity.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthPhysicalActivity.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthPhysicalActivity.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthPhysicalActivity.SleepPerDay = dr["SleepPerDay"].ToString();
            MyMyHealthPhysicalActivity.ExercisePerWeek = dr["ExercisePerWeek"].ToString();
        }
        dr.Close();
        return MyMyHealthPhysicalActivity;
    }

    public MyHealthPhysicalActivity GetMyHealthPhysicalActivity(string value,string type)
    {
        MyHealthPhysicalActivity MyMyHealthPhysicalActivity = new MyHealthPhysicalActivity();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthPhysicalActivity(value, type);
        if (dr.Read())
        {
            MyMyHealthPhysicalActivity.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthPhysicalActivity.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthPhysicalActivity.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthPhysicalActivity.SleepPerDay = dr["SleepPerDay"].ToString();
            MyMyHealthPhysicalActivity.ExercisePerWeek = dr["ExercisePerWeek"].ToString();
        }
        dr.Close();
        return MyMyHealthPhysicalActivity;
    }

    public void AddMyHealthPhysicalActivity(MyHealthPhysicalActivity MyMyHealthPhysicalActivity)
    {
        MobileProvider.ConcreteDataAccess().InsertMyHealthPhysicalActivity(MyMyHealthPhysicalActivity.ID, MyMyHealthPhysicalActivity.MembershipNo, MyMyHealthPhysicalActivity.DeviceID, MyMyHealthPhysicalActivity.SleepPerDay, MyMyHealthPhysicalActivity.ExercisePerWeek);
    }

    public void ModifyMyHealthPhysicalActivity(MyHealthPhysicalActivity MyMyHealthPhysicalActivity)
    {
        MobileProvider.ConcreteDataAccess().UpdateMyHealthPhysicalActivity(MyMyHealthPhysicalActivity.ID, MyMyHealthPhysicalActivity.MembershipNo, MyMyHealthPhysicalActivity.DeviceID, MyMyHealthPhysicalActivity.SleepPerDay, MyMyHealthPhysicalActivity.ExercisePerWeek);
    }

    public void DeleteMyHealthPhysicalActivity(int ID)
    {
        MobileProvider.ConcreteDataAccess().DeleteMyHealthPhysicalActivity(ID);
    }

}