﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SMSControl
/// </summary>
public class SMSControl
{
	public SMSControl()
	{
		
	}

    public void AddSMS(string sender, string Receiver, string msg, string msgtype, string operator1, string status, DateTime sendtime)
    {
        MobileProviderOnline.ConcreteDataAccess().InsertSMS(sender, Receiver, msg, msgtype, operator1, status, sendtime);
    }
}