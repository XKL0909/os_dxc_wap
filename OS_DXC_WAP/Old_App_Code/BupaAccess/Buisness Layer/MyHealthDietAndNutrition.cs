﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthDietAndNutrition
/// </summary>
public class MyHealthDietAndNutrition
{
	public MyHealthDietAndNutrition()
	{
		//
		// TODO: Add constructor logic here
		//
	}



    private Int32 vID;
    public Int32 ID
    {
        get { return vID; }
        set { vID = value; }
    }


    private string vMembershipNo;
    public string MembershipNo
    {
        get { return vMembershipNo; }
        set { vMembershipNo = value; }
    }


    private string vDeviceID;
    public string DeviceID
    {
        get { return vDeviceID; }
        set { vDeviceID = value; }
    }


    private string vSoftDrinks;
    public string SoftDrinks
    {
        get { return vSoftDrinks; }
        set { vSoftDrinks = value; }
    }


    private string vFruits;
    public string Fruits
    {
        get { return vFruits; }
        set { vFruits = value; }
    }


    private string vVegetables;
    public string Vegetables
    {
        get { return vVegetables; }
        set { vVegetables = value; }
    }


    private string vDairyProducts;
    public string DairyProducts
    {
        get { return vDairyProducts; }
        set { vDairyProducts = value; }
    }


    private string vEnergyDrinks;
    public string EnergyDrinks
    {
        get { return vEnergyDrinks; }
        set { vEnergyDrinks = value; }
    }


    private string vNuts;
    public string Nuts
    {
        get { return vNuts; }
        set { vNuts = value; }
    }


    private string vSugaryJuices;
    public string SugaryJuices
    {
        get { return vSugaryJuices; }
        set { vSugaryJuices = value; }
    }


    private string vSweets;
    public string Sweets
    {
        get { return vSweets; }
        set { vSweets = value; }
    }


    private string vHighFibber;
    public string HighFibber
    {
        get { return vHighFibber; }
        set { vHighFibber = value; }
    }


    private string vFattyOily;
    public string FattyOily
    {
        get { return vFattyOily; }
        set { vFattyOily = value; }
    }


    private string vwaterPerDay;
    public string waterPerDay
    {
        get { return vwaterPerDay; }
        set { vwaterPerDay = value; }
    }


    private string vCoffeeTea;
    public string CoffeeTea
    {
        get { return vCoffeeTea; }
        set { vCoffeeTea = value; }
    }

}