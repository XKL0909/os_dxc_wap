﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Result
/// </summary>
public class Result
{
    #region Properties
    private bool _status = false;
    public bool Status
    {
        get { return _status; }
        set { _status = value; }
    }
    private string _msg = string.Empty;
    public string Msg
    {
        get { return _msg; }
        set { _msg = value; }
    }
    private Object _data = null;
    public Object Data
    {
        get { return _data; }
        set { _data = value; }
    }
    #endregion

	public Result()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}