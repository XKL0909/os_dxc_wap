﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Dependent
/// </summary>
public class Dependent
{
    #region Properties
    private string _memberName = string.Empty;
    public string MemberName
    {
        get { return _memberName; }
        set { _memberName = value; }
    }
    private string _relationship = string.Empty;
    public string RelationShip
    {
        get { return _relationship; }
        set { _relationship = value; }
    }
    private string _membershipNumber = string.Empty;
    public string MembershipNumber
    {
        get { return _membershipNumber; }
        set { _membershipNumber = value; }
    }
    #endregion
	public Dependent()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}