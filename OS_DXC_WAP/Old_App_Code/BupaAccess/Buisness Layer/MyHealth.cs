﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealth
/// </summary>
public class MyHealth
{
	public MyHealth()
	{
        this.ID = ID;
        this.Exercise = Exercise;
        this.SleepDisorder = SleepDisorder;
        this.Smoker = Smoker;
        this.Asthma = Asthma;
        this.Arthritis = Arthritis;
        this.BackPain = BackPain;
        this.Cancer = Cancer;
        this.Depression = Depression;
        this.Diabetes = Diabetes;
        this.HeartProblem = HeartProblem;
        this.HighBloodPressure = HighBloodPressure;
        this.HighCholesterol = HighCholesterol;
        this.MembershipNumber = MembershipNumber;
        this.PregnantSpouse = PregnantSpouse;
        this.Stressed = Stressed;
        this.DeviceID = DeviceID;
	}



    private Int32 vID;
    public Int32 ID
    {
        get { return vID; }
        set { vID = value; }
    }


    //private string vSex;
    //public string Sex
    //{
    //    get { return vSex; }
    //    set { vSex = value; }
    //}


    //private Int32 vAge;
    //public Int32 Age
    //{
    //    get { return vAge; }
    //    set { vAge = value; }
    //}


    //private Int32 vHeight;
    //public Int32 Height
    //{
    //    get { return vHeight; }
    //    set { vHeight = value; }
    //}


    //private Int32 vWeight;
    //public Int32 Weight
    //{
    //    get { return vWeight; }
    //    set { vWeight = value; }
    //}


    //private Int32 vWaistCircumfrence;
    //public Int32 WaistCircumfrence
    //{
    //    get { return vWaistCircumfrence; }
    //    set { vWaistCircumfrence = value; }
    //}


    private bool vExercise;
    public bool Exercise
    {
        get { return vExercise; }
        set { vExercise = value; }
    }

    private bool vStressed;
    public bool Stressed
    {
        get { return vStressed; }
        set { vStressed = value; }
    }

    private bool vDepression;
    public bool Depression
    {
        get { return vDepression; }
        set { vDepression = value; }
    }

    private bool vPregnantSpouse;
    public bool PregnantSpouse
    {
        get { return vPregnantSpouse; }
        set { vPregnantSpouse = value; }
    }


    //private Int32 vNumberOfExercies;
    //public Int32 NumberOfExercies
    //{
    //    get { return vNumberOfExercies; }
    //    set { vNumberOfExercies = value; }
    //}


    //private bool vConsumeVegetablesFruits;
    //public bool ConsumeVegetablesFruits
    //{
    //    get { return vConsumeVegetablesFruits; }
    //    set { vConsumeVegetablesFruits = value; }
    //}


    //private bool vConsumeCola;
    //public bool ConsumeCola
    //{
    //    get { return vConsumeCola; }
    //    set { vConsumeCola = value; }
    //}


    //private Int32 vColaBottlesNumber;
    //public Int32 ColaBottlesNumber
    //{
    //    get { return vColaBottlesNumber; }
    //    set { vColaBottlesNumber = value; }
    //}


    //private Int32 vFiberServingsNumber;
    //public Int32 FiberServingsNumber
    //{
    //    get { return vFiberServingsNumber; }
    //    set { vFiberServingsNumber = value; }
    //}


    //private Int32 vFriedServingsNumber;
    //public Int32 FriedServingsNumber
    //{
    //    get { return vFriedServingsNumber; }
    //    set { vFriedServingsNumber = value; }
    //}


    //private Int32 vSleepHoursNumber;
    //public Int32 SleepHoursNumber
    //{
    //    get { return vSleepHoursNumber; }
    //    set { vSleepHoursNumber = value; }
    //}


    //private bool vSnore;
    //public bool Snore
    //{
    //    get { return vSnore; }
    //    set { vSnore = value; }
    //}


    private bool vSleepDisorder;
    public bool SleepDisorder
    {
        get { return vSleepDisorder; }
        set { vSleepDisorder = value; }
    }


    //private Int32 vWorkHours;
    //public Int32 WorkHours
    //{
    //    get { return vWorkHours; }
    //    set { vWorkHours = value; }
    //}


    //private string vAnxietyFrequency;
    //public string AnxietyFrequency
    //{
    //    get { return vAnxietyFrequency; }
    //    set { vAnxietyFrequency = value; }
    //}


    private bool vSmoker;
    public bool Smoker
    {
        get { return vSmoker; }
        set { vSmoker = value; }
    }


    //private string vSmokeType;
    //public string SmokeType
    //{
    //    get { return vSmokeType; }
    //    set { vSmokeType = value; }
    //}


    //private Int32 vYearsSmokingNumber;
    //public Int32 YearsSmokingNumber
    //{
    //    get { return vYearsSmokingNumber; }
    //    set { vYearsSmokingNumber = value; }
    //}


    //private Int32 vSticksNumber;
    //public Int32 SticksNumber
    //{
    //    get { return vSticksNumber; }
    //    set { vSticksNumber = value; }
    //}


    //private bool vShishaSmoker;
    //public bool ShishaSmoker
    //{
    //    get { return vShishaSmoker; }
    //    set { vShishaSmoker = value; }
    //}


    //private Int32 vNumberOfShishaSmokes;
    //public Int32 NumberOfShishaSmokes
    //{
    //    get { return vNumberOfShishaSmokes; }
    //    set { vNumberOfShishaSmokes = value; }
    //}


    private bool vAsthma;
    public bool Asthma
    {
        get { return vAsthma; }
        set { vAsthma = value; }
    }


    private bool vArthritis;
    public bool Arthritis
    {
        get { return vArthritis; }
        set { vArthritis = value; }
    }


    private bool vBackPain;
    public bool BackPain
    {
        get { return vBackPain; }
        set { vBackPain = value; }
    }


    private bool vCancer;
    public bool Cancer
    {
        get { return vCancer; }
        set { vCancer = value; }
    }

    //private bool vEpilipsy;
    //public bool Epilipsy
    //{
    //    get { return vEpilipsy; }
    //    set { vEpilipsy = value; }
    //}

    private bool vDiabetes;
    public bool Diabetes
    {
        get { return vDiabetes; }
        set { vDiabetes = value; }
    }


    private bool vHeartProblem;
    public bool HeartProblem
    {
        get { return vHeartProblem; }
        set { vHeartProblem = value; }
    }


    private bool vHighBloodPressure;
    public bool HighBloodPressure
    {
        get { return vHighBloodPressure; }
        set { vHighBloodPressure = value; }
    }


    private bool vHighCholesterol;
    public bool HighCholesterol
    {
        get { return vHighCholesterol; }
        set { vHighCholesterol = value; }
    }


    //private bool vColonChronicInflamatoryDiseases;
    //public bool ColonChronicInflamatoryDiseases
    //{
    //    get { return vColonChronicInflamatoryDiseases; }
    //    set { vColonChronicInflamatoryDiseases = value; }
    //}


    //private bool vColonPolyps;
    //public bool ColonPolyps
    //{
    //    get { return vColonPolyps; }
    //    set { vColonPolyps = value; }
    //}


    //private bool vHospitalized;
    //public bool Hospitalized
    //{
    //    get { return vHospitalized; }
    //    set { vHospitalized = value; }
    //}


    //private bool vSurgery;
    //public bool Surgery
    //{
    //    get { return vSurgery; }
    //    set { vSurgery = value; }
    //}


    //private bool vFamilyMemberCancer;
    //public bool FamilyMemberCancer
    //{
    //    get { return vFamilyMemberCancer; }
    //    set { vFamilyMemberCancer = value; }
    //}


    //private bool vFamilyMemberDiabetes;
    //public bool FamilyMemberDiabetes
    //{
    //    get { return vFamilyMemberDiabetes; }
    //    set { vFamilyMemberDiabetes = value; }
    //}


    //private bool vFamilyMemberHeartProblem;
    //public bool FamilyMemberHeartProblem
    //{
    //    get { return vFamilyMemberHeartProblem; }
    //    set { vFamilyMemberHeartProblem = value; }
    //}


    //private bool vFamilyMemberHighBloodPressure;
    //public bool FamilyMemberHighBloodPressure
    //{
    //    get { return vFamilyMemberHighBloodPressure; }
    //    set { vFamilyMemberHighBloodPressure = value; }
    //}


    //private bool vFamilyMemberCholesterol;
    //public bool FamilyMemberCholesterol
    //{
    //    get { return vFamilyMemberCholesterol; }
    //    set { vFamilyMemberCholesterol = value; }
    //}


    private string vMembershipNumber;
    public string MembershipNumber
    {
        get { return vMembershipNumber; }
        set { vMembershipNumber = value; }
    }

    private string vDeviceID;
    public string DeviceID
    {
        get { return vDeviceID; }
        set { vDeviceID = value; }
    }

}