﻿using OS_DXC_WAP.CaesarWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CoverageDetails
/// </summary>
public class CoverageDetails : EnqProdCovInfoResponse_DN
{
    private string _membershipNumber = string.Empty;
    public string membershipNumber
    {
        get { return _membershipNumber; }
        set { _membershipNumber = value; }
    }

	public CoverageDetails(EnqProdCovInfoResponse_DN response)
	{
        this.className = response.className;
        this.companyName = response.companyName;
        this.detail = response.detail;
        this.errorID = response.errorID;
        this.errorMessage = response.errorMessage;
        this.proratedOverallAnnualLimit = response.proratedOverallAnnualLimit;
        this.status = response.status;
        this.transactionID = response.transactionID;
	}
}