﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthStress
/// </summary>
public class MyHealthStress
{
	public MyHealthStress()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private Int32 vID;
    public Int32 ID
    {
        get { return vID; }
        set { vID = value; }
    }


    private string vMembershipNo;
    public string MembershipNo
    {
        get { return vMembershipNo; }
        set { vMembershipNo = value; }
    }


    private string vDeviceID;
    public string DeviceID
    {
        get { return vDeviceID; }
        set { vDeviceID = value; }
    }


    private string votherEnvironmentalStress;
    public string otherEnvironmentalStress
    {
        get { return votherEnvironmentalStress; }
        set { votherEnvironmentalStress = value; }
    }


    private string virritated;
    public string irritated
    {
        get { return virritated; }
        set { virritated = value; }
    }


    private string vUnemployment;
    public string Unemployment
    {
        get { return vUnemployment; }
        set { vUnemployment = value; }
    }


    private string vWorkOverload;
    public string WorkOverload
    {
        get { return vWorkOverload; }
        set { vWorkOverload = value; }
    }


    private string vTooManyChildren;
    public string TooManyChildren
    {
        get { return vTooManyChildren; }
        set { vTooManyChildren = value; }
    }


    private string vRoleAmbiguity;
    public string RoleAmbiguity
    {
        get { return vRoleAmbiguity; }
        set { vRoleAmbiguity = value; }
    }


    private string vdifficultToTolerateInterruptions;
    public string difficultToTolerateInterruptions
    {
        get { return vdifficultToTolerateInterruptions; }
        set { vdifficultToTolerateInterruptions = value; }
    }


    private string vHealthIssues;
    public string HealthIssues
    {
        get { return vHealthIssues; }
        set { vHealthIssues = value; }
    }


    private string vhardToCalmDown;
    public string hardToCalmDown
    {
        get { return vhardToCalmDown; }
        set { vhardToCalmDown = value; }
    }


    private string vdifficultToRelax;
    public string difficultToRelax
    {
        get { return vdifficultToRelax; }
        set { vdifficultToRelax = value; }
    }


    private string vLoneliness;
    public string Loneliness
    {
        get { return vLoneliness; }
        set { vLoneliness = value; }
    }


    private string vDivorce;
    public string Divorce
    {
        get { return vDivorce; }
        set { vDivorce = value; }
    }


    private string vregularHealthTips;
    public string regularHealthTips
    {
        get { return vregularHealthTips; }
        set { vregularHealthTips = value; }
    }


    private string vimpatientWithDelays;
    public string impatientWithDelays
    {
        get { return vimpatientWithDelays; }
        set { vimpatientWithDelays = value; }
    }


    private string vupsetRatherEasily;
    public string upsetRatherEasily
    {
        get { return vupsetRatherEasily; }
        set { vupsetRatherEasily = value; }
    }


    private string vJobChange;
    public string JobChange
    {
        get { return vJobChange; }
        set { vJobChange = value; }
    }


    private string vParentalConflict;
    public string ParentalConflict
    {
        get { return vParentalConflict; }
        set { vParentalConflict = value; }
    }


    private string vupsetOverMinorThings;
    public string upsetOverMinorThings
    {
        get { return vupsetOverMinorThings; }
        set { vupsetOverMinorThings = value; }
    }


    private string vinflexibleWorkingHours;
    public string inflexibleWorkingHours
    {
        get { return vinflexibleWorkingHours; }
        set { vinflexibleWorkingHours = value; }
    }


    private string vPromotions;
    public string Promotions
    {
        get { return vPromotions; }
        set { vPromotions = value; }
    }


    private string vMarriage;
    public string Marriage
    {
        get { return vMarriage; }
        set { vMarriage = value; }
    }


    private string vTooMuchToDo;
    public string TooMuchToDo
    {
        get { return vTooMuchToDo; }
        set { vTooMuchToDo = value; }
    }


    private string voptimisticPerson;
    public string optimisticPerson
    {
        get { return voptimisticPerson; }
        set { voptimisticPerson = value; }
    }


    private string veasilyGetNervous;
    public string easilyGetNervous
    {
        get { return veasilyGetNervous; }
        set { veasilyGetNervous = value; }
    }


    private string vfinancialDifficulties;
    public string financialDifficulties
    {
        get { return vfinancialDifficulties; }
        set { vfinancialDifficulties = value; }
    }


    private string vLowSelfConfidence;
    public string LowSelfConfidence
    {
        get { return vLowSelfConfidence; }
        set { vLowSelfConfidence = value; }
    }


    private string votherMajorWorkRelatedStress;
    public string otherMajorWorkRelatedStress
    {
        get { return votherMajorWorkRelatedStress; }
        set { votherMajorWorkRelatedStress = value; }
    }


    private string vOverReact;
    public string OverReact
    {
        get { return vOverReact; }
        set { vOverReact = value; }
    }


    private string vDeathInTheFamily;
    public string DeathInTheFamily
    {
        get { return vDeathInTheFamily; }
        set { vDeathInTheFamily = value; }
    }


    private string vLackOfSocialSupport;
    public string LackOfSocialSupport
    {
        get { return vLackOfSocialSupport; }
        set { vLackOfSocialSupport = value; }
    }


    private string vlosingControl;
    public string losingControl
    {
        get { return vlosingControl; }
        set { vlosingControl = value; }
    }

}