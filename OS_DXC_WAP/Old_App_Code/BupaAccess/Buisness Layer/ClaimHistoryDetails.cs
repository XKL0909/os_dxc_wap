﻿using OS_DXC_WAP.CaesarWS;

/// <summary>
/// Summary description for ClaimHistoryDetails
/// </summary>
public class ClaimHistoryDetails
{
	private string _membershipNumber = string.Empty;
    public string membershipNumber
    {
        get { return _membershipNumber; }
        set { _membershipNumber = value; }
    }

    private EnqClmHisDetail_DN[] _detail = null;
    public EnqClmHisDetail_DN[] Detail
    {
        get { return _detail; }
        set { _detail = value; }
    }

    public ClaimHistoryDetails(EnqClmHisDetail_DN[] Detail, string membershipNumber)
	{
        this.membershipNumber = membershipNumber;
        this.Detail = Detail;
	}
}