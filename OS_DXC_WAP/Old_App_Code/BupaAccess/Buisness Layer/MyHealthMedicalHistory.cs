﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthMedicalHistory
/// </summary>
public class MyHealthMedicalHistory
{
	public MyHealthMedicalHistory()
	{
		//
		// TODO: Add constructor logic here
		//
	}



    private Int32 vID;
    public Int32 ID
    {
        get { return vID; }
        set { vID = value; }
    }


    private string vMembershipNo;
    public string MembershipNo
    {
        get { return vMembershipNo; }
        set { vMembershipNo = value; }
    }


    private string vDeviceID;
    public string DeviceID
    {
        get { return vDeviceID; }
        set { vDeviceID = value; }
    }


    private bool vDiabetes_family;
    public bool Diabetes_family
    {
        get { return vDiabetes_family; }
        set { vDiabetes_family = value; }
    }


    private bool vDepression;
    public bool Depression
    {
        get { return vDepression; }
        set { vDepression = value; }
    }


    private bool vFatigue;
    public bool Fatigue
    {
        get { return vFatigue; }
        set { vFatigue = value; }
    }


    private bool vHaveYouBeenHospitalized;
    public bool HaveYouBeenHospitalized
    {
        get { return vHaveYouBeenHospitalized; }
        set { vHaveYouBeenHospitalized = value; }
    }


    private bool vNightmares;
    public bool Nightmares
    {
        get { return vNightmares; }
        set { vNightmares = value; }
    }


    private bool vSnoring;
    public bool Snoring
    {
        get { return vSnoring; }
        set { vSnoring = value; }
    }


    private bool vAsthma;
    public bool Asthma
    {
        get { return vAsthma; }
        set { vAsthma = value; }
    }


    private bool vSleepDisorderInsomnia;
    public bool SleepDisorderInsomnia
    {
        get { return vSleepDisorderInsomnia; }
        set { vSleepDisorderInsomnia = value; }
    }


    private bool vHeartProblem;
    public bool HeartProblem
    {
        get { return vHeartProblem; }
        set { vHeartProblem = value; }
    }


    private bool vHighBloodPressure;
    public bool HighBloodPressure
    {
        get { return vHighBloodPressure; }
        set { vHighBloodPressure = value; }
    }


    private bool vCancer;
    public bool Cancer
    {
        get { return vCancer; }
        set { vCancer = value; }
    }


    private bool vHighCholesterol;
    public bool HighCholesterol
    {
        get { return vHighCholesterol; }
        set { vHighCholesterol = value; }
    }


    private bool vDiabetes;
    public bool Diabetes
    {
        get { return vDiabetes; }
        set { vDiabetes = value; }
    }


    private bool vHighCholesterol_family;
    public bool HighCholesterol_family
    {
        get { return vHighCholesterol_family; }
        set { vHighCholesterol_family = value; }
    }


    private bool vCancer_family;
    public bool Cancer_family
    {
        get { return vCancer_family; }
        set { vCancer_family = value; }
    }


    private bool vEpilepsy;
    public bool Epilepsy
    {
        get { return vEpilepsy; }
        set { vEpilepsy = value; }
    }


    private bool vArthritis;
    public bool Arthritis
    {
        get { return vArthritis; }
        set { vArthritis = value; }
    }


    private bool vMigraine;
    public bool Migraine
    {
        get { return vMigraine; }
        set { vMigraine = value; }
    }


    private bool vHighBloodPressure_family;
    public bool HighBloodPressure_family
    {
        get { return vHighBloodPressure_family; }
        set { vHighBloodPressure_family = value; }
    }


    private bool vBackPain;
    public bool BackPain
    {
        get { return vBackPain; }
        set { vBackPain = value; }
    }


    private bool vhaveAnySurgery;
    public bool haveAnySurgery
    {
        get { return vhaveAnySurgery; }
        set { vhaveAnySurgery = value; }
    }


    private bool vHeartProblem_family;
    public bool HeartProblem_family
    {
        get { return vHeartProblem_family; }
        set { vHeartProblem_family = value; }
    }


    private bool vColonPolyps;
    public bool ColonPolyps
    {
        get { return vColonPolyps; }
        set { vColonPolyps = value; }
    }


    private bool vChronicInflammatory;
    public bool ChronicInflammatory
    {
        get { return vChronicInflammatory; }
        set { vChronicInflammatory = value; }
    }


    private bool vDepression_family;
    public bool Depression_family
    {
        get { return vDepression_family; }
        set { vDepression_family = value; }
    }


}