﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for MyHealthControl
/// </summary>
public class MyHealthControl
{
	public MyHealthControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public List<MyHealth> GetAllMyHealth()
    {
        List<MyHealth> L = new List<MyHealth>();
        MyHealth MyHealthObject = default(MyHealth);
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetAllMyHealth();
        while (dr.Read())
        {
            MyHealthObject = new MyHealth();
            MyHealthObject.ID = int.Parse(dr["ID"].ToString());
            MyHealthObject.Exercise = bool.Parse (dr["Exercise"].ToString());
            MyHealthObject.SleepDisorder = bool.Parse(dr["SleepDisorder"].ToString());
            MyHealthObject.Smoker = bool.Parse(dr["Smoker"].ToString());
            MyHealthObject.Asthma = bool.Parse(dr["Asthma"].ToString());
            MyHealthObject.Arthritis = bool.Parse(dr["Arthritis"].ToString());
            MyHealthObject.BackPain = bool.Parse(dr["BackPain"].ToString());
            MyHealthObject.Cancer = bool.Parse(dr["Cancer"].ToString());
            MyHealthObject.Diabetes = bool.Parse(dr["Diabetes"].ToString());
            MyHealthObject.HeartProblem = bool.Parse(dr["HeartProblem"].ToString());
            MyHealthObject.HighBloodPressure = bool.Parse(dr["HighBloodPressure"].ToString());
            MyHealthObject.HighCholesterol = bool.Parse(dr["HighCholesterol"].ToString());
            MyHealthObject.MembershipNumber = dr["MembershipNumber"].ToString();
            MyHealthObject.DeviceID = dr["DeviceID"].ToString();
            MyHealthObject.Stressed = bool.Parse(dr["Stressed"].ToString());
            MyHealthObject.PregnantSpouse = bool.Parse(dr["PregnantSpouse"].ToString());
            MyHealthObject.Depression = bool.Parse(dr["Depression"].ToString());
            L.Add(MyHealthObject);
        }
        dr.Close();
        return L;
    }

    public MyHealth GetMyHealth(int ID)
    {
        MyHealth MyHealthObject = new MyHealth();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealth(ID);
        if (dr.Read())
        {
            MyHealthObject.ID = int.Parse(dr["ID"].ToString());
            MyHealthObject.Exercise = bool.Parse(dr["Exercise"].ToString());
            MyHealthObject.SleepDisorder = bool.Parse(dr["SleepDisorder"].ToString());
            MyHealthObject.Smoker = bool.Parse(dr["Smoker"].ToString());
            MyHealthObject.Asthma = bool.Parse(dr["Asthma"].ToString());
            MyHealthObject.Arthritis = bool.Parse(dr["Arthritis"].ToString());
            MyHealthObject.BackPain = bool.Parse(dr["BackPain"].ToString());
            MyHealthObject.Cancer = bool.Parse(dr["Cancer"].ToString());
            MyHealthObject.Diabetes = bool.Parse(dr["Diabetes"].ToString());
            MyHealthObject.HeartProblem = bool.Parse(dr["HeartProblem"].ToString());
            MyHealthObject.HighBloodPressure = bool.Parse(dr["HighBloodPressure"].ToString());
            MyHealthObject.HighCholesterol = bool.Parse(dr["HighCholesterol"].ToString());
            MyHealthObject.MembershipNumber = dr["MembershipNumber"].ToString();
            MyHealthObject.DeviceID = dr["DeviceID"].ToString();
            MyHealthObject.Stressed = bool.Parse(dr["Stressed"].ToString());
            MyHealthObject.PregnantSpouse = bool.Parse(dr["PregnantSpouse"].ToString());
            MyHealthObject.Depression = bool.Parse(dr["Depression"].ToString());
        }
        dr.Close();
        return MyHealthObject;
    }

    public MyHealth GetMyHealth(string value, string type)
    {
        MyHealth MyHealthObject = new MyHealth();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealth(value, type);
        if (dr.Read())
        {
            MyHealthObject.ID = int.Parse(dr["ID"].ToString());
            MyHealthObject.Exercise = bool.Parse(dr["Exercise"].ToString());
            MyHealthObject.SleepDisorder = bool.Parse(dr["SleepDisorder"].ToString());
            MyHealthObject.Smoker = bool.Parse(dr["Smoker"].ToString());
            MyHealthObject.Asthma = bool.Parse(dr["Asthma"].ToString());
            MyHealthObject.Arthritis = bool.Parse(dr["Arthritis"].ToString());
            MyHealthObject.BackPain = bool.Parse(dr["BackPain"].ToString());
            MyHealthObject.Cancer = bool.Parse(dr["Cancer"].ToString());
            MyHealthObject.Diabetes = bool.Parse(dr["Diabetes"].ToString());
            MyHealthObject.HeartProblem = bool.Parse(dr["HeartProblem"].ToString());
            MyHealthObject.HighBloodPressure = bool.Parse(dr["HighBloodPressure"].ToString());
            MyHealthObject.HighCholesterol = bool.Parse(dr["HighCholesterol"].ToString());
            MyHealthObject.MembershipNumber = dr["MembershipNumber"].ToString();
            MyHealthObject.DeviceID = dr["DeviceID"].ToString();
            MyHealthObject.Stressed = bool.Parse(dr["Stressed"].ToString());
            MyHealthObject.PregnantSpouse = bool.Parse(dr["PregnantSpouse"].ToString());
            MyHealthObject.Depression = bool.Parse(dr["Depression"].ToString());
        }
        dr.Close();
        return MyHealthObject;
    }

    public MyHealth GetMyHealth(string MembershipNumber)
    {
        MyHealth MyHealthObject = new MyHealth();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealth(MembershipNumber);
        if (dr.Read())
        {
            MyHealthObject.ID = int.Parse(dr["ID"].ToString());
            MyHealthObject.Exercise = bool.Parse(dr["Exercise"].ToString());
            MyHealthObject.SleepDisorder = bool.Parse(dr["SleepDisorder"].ToString());
            MyHealthObject.Smoker = bool.Parse(dr["Smoker"].ToString());
            MyHealthObject.Asthma = bool.Parse(dr["Asthma"].ToString());
            MyHealthObject.Arthritis = bool.Parse(dr["Arthritis"].ToString());
            MyHealthObject.BackPain = bool.Parse(dr["BackPain"].ToString());
            MyHealthObject.Cancer = bool.Parse(dr["Cancer"].ToString());
            MyHealthObject.Diabetes = bool.Parse(dr["Diabetes"].ToString());
            MyHealthObject.HeartProblem = bool.Parse(dr["HeartProblem"].ToString());
            MyHealthObject.HighBloodPressure = bool.Parse(dr["HighBloodPressure"].ToString());
            MyHealthObject.HighCholesterol = bool.Parse(dr["HighCholesterol"].ToString());
            MyHealthObject.MembershipNumber = dr["MembershipNumber"].ToString();
            MyHealthObject.DeviceID = dr["DeviceID"].ToString();
            MyHealthObject.Stressed = bool.Parse(dr["Stressed"].ToString());
            MyHealthObject.PregnantSpouse = bool.Parse(dr["PregnantSpouse"].ToString());
            MyHealthObject.Depression = bool.Parse(dr["Depression"].ToString());
        }
        dr.Close();
        return MyHealthObject;
    }

    public MyHealth GetMyHealthUsingDeviceID(string DeviceID)
    {
        MyHealth MyHealthObject = new MyHealth();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthUsingDeviceID(DeviceID);
        if (dr.Read())
        {
            MyHealthObject.ID = int.Parse(dr["ID"].ToString());
            MyHealthObject.Exercise = bool.Parse(dr["Exercise"].ToString());
            MyHealthObject.SleepDisorder = bool.Parse(dr["SleepDisorder"].ToString());
            MyHealthObject.Smoker = bool.Parse(dr["Smoker"].ToString());
            MyHealthObject.Asthma = bool.Parse(dr["Asthma"].ToString());
            MyHealthObject.Arthritis = bool.Parse(dr["Arthritis"].ToString());
            MyHealthObject.BackPain = bool.Parse(dr["BackPain"].ToString());
            MyHealthObject.Cancer = bool.Parse(dr["Cancer"].ToString());
            MyHealthObject.Diabetes = bool.Parse(dr["Diabetes"].ToString());
            MyHealthObject.HeartProblem = bool.Parse(dr["HeartProblem"].ToString());
            MyHealthObject.HighBloodPressure = bool.Parse(dr["HighBloodPressure"].ToString());
            MyHealthObject.HighCholesterol = bool.Parse(dr["HighCholesterol"].ToString());
            MyHealthObject.MembershipNumber = dr["MembershipNumber"].ToString();
            MyHealthObject.DeviceID = dr["DeviceID"].ToString();
            MyHealthObject.Stressed = bool.Parse(dr["Stressed"].ToString());
            MyHealthObject.PregnantSpouse = bool.Parse(dr["PregnantSpouse"].ToString());
            MyHealthObject.Depression = bool.Parse(dr["Depression"].ToString());
        }
        dr.Close();
        return MyHealthObject;
    }

    public void AddMyHealth(MyHealth MyHealthObject)
    {
        MobileProvider.ConcreteDataAccess().InsertMyHealth(MyHealthObject.ID, MyHealthObject.Exercise, MyHealthObject.SleepDisorder, MyHealthObject.Smoker, MyHealthObject.Asthma, MyHealthObject.Arthritis, MyHealthObject.BackPain, MyHealthObject.Cancer, MyHealthObject.Depression, MyHealthObject.Diabetes, MyHealthObject.HeartProblem,
        MyHealthObject.HighBloodPressure, MyHealthObject.HighCholesterol, MyHealthObject.Stressed, MyHealthObject.PregnantSpouse, MyHealthObject.MembershipNumber, MyHealthObject.DeviceID);
    }

    public void ModifyMyHealth(MyHealth MyHealthObject)
    {
        MobileProvider.ConcreteDataAccess().UpdateMyHealth(MyHealthObject.ID, MyHealthObject.Exercise, MyHealthObject.SleepDisorder, MyHealthObject.Smoker, MyHealthObject.Asthma, MyHealthObject.Arthritis, MyHealthObject.BackPain, MyHealthObject.Cancer, MyHealthObject.Depression, MyHealthObject.Diabetes, MyHealthObject.HeartProblem,
        MyHealthObject.HighBloodPressure, MyHealthObject.HighCholesterol, MyHealthObject.Stressed, MyHealthObject.PregnantSpouse, MyHealthObject.MembershipNumber, MyHealthObject.DeviceID);
    }

    public void DeleteMyHealth(int ID)
    {
        MobileProvider.ConcreteDataAccess().DeleteMyHealth(ID);
    }
}