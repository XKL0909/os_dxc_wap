﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthSmoking
/// </summary>
public class MyHealthSmoking
{
	public MyHealthSmoking()
	{
		//
		// TODO: Add constructor logic here
		//
	}



    private Int32 vID;
    public Int32 ID
    {
        get { return vID; }
        set { vID = value; }
    }


    private string vMembershipNo;
    public string MembershipNo
    {
        get { return vMembershipNo; }
        set { vMembershipNo = value; }
    }


    private string vDeviceID;
    public string DeviceID
    {
        get { return vDeviceID; }
        set { vDeviceID = value; }
    }


    private bool vsmokeCigarettes;
    public bool smokeCigarettes
    {
        get { return vsmokeCigarettes; }
        set { vsmokeCigarettes = value; }
    }


    private bool vsmokeShisha;
    public bool smokeShisha
    {
        get { return vsmokeShisha; }
        set { vsmokeShisha = value; }
    }


    private string vCigarettesPerDay;
    public string CigarettesPerDay
    {
        get { return vCigarettesPerDay; }
        set { vCigarettesPerDay = value; }
    }


    private string vShishaPerWeek;
    public string ShishaPerWeek
    {
        get { return vShishaPerWeek; }
        set { vShishaPerWeek = value; }
    }


    private string vShishaPerYear;
    public string ShishaPerYear
    {
        get { return vShishaPerYear; }
        set { vShishaPerYear = value; }
    }


    private string vCigarettesPerYear;
    public string CigarettesPerYear
    {
        get { return vCigarettesPerYear; }
        set { vCigarettesPerYear = value; }
    }

}