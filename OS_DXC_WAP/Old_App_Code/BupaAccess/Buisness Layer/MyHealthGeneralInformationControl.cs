﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyHealthGeneralInformationControl
/// </summary>
public class MyHealthGeneralInformationControl
{
	public MyHealthGeneralInformationControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public List<MyHealthGeneralInformation> GetAllMyHealthGeneralInformation()
    {
        List<MyHealthGeneralInformation> L = new List<MyHealthGeneralInformation>();
        MyHealthGeneralInformation MyMyHealthGeneralInformation = default(MyHealthGeneralInformation);
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetAllMyHealthGeneralInformation();
        while (dr.Read())
        {
            MyMyHealthGeneralInformation = new MyHealthGeneralInformation();
            MyMyHealthGeneralInformation.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthGeneralInformation.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthGeneralInformation.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthGeneralInformation.Age = int.Parse(dr["Age"].ToString());
            MyMyHealthGeneralInformation.WaistSizeInch = dr["WaistSizeInch"].ToString();
            MyMyHealthGeneralInformation.WeightinKg = dr["WeightinKg"].ToString();
            MyMyHealthGeneralInformation.HeightinCm = dr["HeightinCm"].ToString();
            MyMyHealthGeneralInformation.email = dr["email"].ToString();
            MyMyHealthGeneralInformation.Gender = dr["Gender"].ToString();
            MyMyHealthGeneralInformation.name = dr["name"].ToString();
            MyMyHealthGeneralInformation.WaistSizeCm = dr["WaistSizeCm"].ToString();
            MyMyHealthGeneralInformation.MobileNo = dr["MobileNo"].ToString();
            L.Add(MyMyHealthGeneralInformation);
        }
        dr.Close();
        return L;
    }


    public MyHealthGeneralInformation GetMyHealthGeneralInformation(int ID)
    {
        MyHealthGeneralInformation MyMyHealthGeneralInformation = new MyHealthGeneralInformation();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthGeneralInformation(ID);
        if (dr.Read())
        {
            MyMyHealthGeneralInformation.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthGeneralInformation.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthGeneralInformation.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthGeneralInformation.Age = int.Parse(dr["Age"].ToString());
            MyMyHealthGeneralInformation.WaistSizeInch = dr["WaistSizeInch"].ToString();
            MyMyHealthGeneralInformation.WeightinKg = dr["WeightinKg"].ToString();
            MyMyHealthGeneralInformation.HeightinCm = dr["HeightinCm"].ToString();
            MyMyHealthGeneralInformation.email = dr["email"].ToString();
            MyMyHealthGeneralInformation.Gender = dr["Gender"].ToString();
            MyMyHealthGeneralInformation.name = dr["name"].ToString();
            MyMyHealthGeneralInformation.WaistSizeCm = dr["WaistSizeCm"].ToString();
            MyMyHealthGeneralInformation.MobileNo = dr["MobileNo"].ToString();
        }
        dr.Close();
        return MyMyHealthGeneralInformation;
    }

    public MyHealthGeneralInformation GetMyHealthGeneralInformation(string value,string type)
    {
        MyHealthGeneralInformation MyMyHealthGeneralInformation = new MyHealthGeneralInformation();
        IDataReader dr = MobileProvider.ConcreteDataAccess().GetMyHealthGeneralInformation(value, type);
        if (dr.Read())
        {
            MyMyHealthGeneralInformation.ID = int.Parse(dr["ID"].ToString());
            MyMyHealthGeneralInformation.MembershipNo = dr["MembershipNo"].ToString();
            MyMyHealthGeneralInformation.DeviceID = dr["DeviceID"].ToString();
            MyMyHealthGeneralInformation.Age = int.Parse(dr["Age"].ToString());
            MyMyHealthGeneralInformation.WaistSizeInch = dr["WaistSizeInch"].ToString();
            MyMyHealthGeneralInformation.WeightinKg = dr["WeightinKg"].ToString();
            MyMyHealthGeneralInformation.HeightinCm = dr["HeightinCm"].ToString();
            MyMyHealthGeneralInformation.email = dr["email"].ToString();
            MyMyHealthGeneralInformation.Gender = dr["Gender"].ToString();
            MyMyHealthGeneralInformation.name = dr["name"].ToString();
            MyMyHealthGeneralInformation.WaistSizeCm = dr["WaistSizeCm"].ToString();
            MyMyHealthGeneralInformation.MobileNo = dr["MobileNo"].ToString();
        }
        dr.Close();
        return MyMyHealthGeneralInformation;
    }

    public void AddMyHealthGeneralInformation(MyHealthGeneralInformation MyMyHealthGeneralInformation)
    {
        MobileProvider.ConcreteDataAccess().InsertMyHealthGeneralInformation(MyMyHealthGeneralInformation.ID, MyMyHealthGeneralInformation.MembershipNo, MyMyHealthGeneralInformation.DeviceID, MyMyHealthGeneralInformation.Age, MyMyHealthGeneralInformation.WaistSizeInch, MyMyHealthGeneralInformation.WeightinKg, MyMyHealthGeneralInformation.HeightinCm, MyMyHealthGeneralInformation.email, MyMyHealthGeneralInformation.Gender, MyMyHealthGeneralInformation.name,
        MyMyHealthGeneralInformation.WaistSizeCm, MyMyHealthGeneralInformation.MobileNo);
    }

    public void ModifyMyHealthGeneralInformation(MyHealthGeneralInformation MyMyHealthGeneralInformation)
    {
        MobileProvider.ConcreteDataAccess().UpdateMyHealthGeneralInformation(MyMyHealthGeneralInformation.ID, MyMyHealthGeneralInformation.MembershipNo, MyMyHealthGeneralInformation.DeviceID, MyMyHealthGeneralInformation.Age, MyMyHealthGeneralInformation.WaistSizeInch, MyMyHealthGeneralInformation.WeightinKg, MyMyHealthGeneralInformation.HeightinCm, MyMyHealthGeneralInformation.email, MyMyHealthGeneralInformation.Gender, MyMyHealthGeneralInformation.name,
        MyMyHealthGeneralInformation.WaistSizeCm, MyMyHealthGeneralInformation.MobileNo);
    }

    public void DeleteMyHealthGeneralInformation(int ID)
    {
        MobileProvider.ConcreteDataAccess().DeleteMyHealthGeneralInformation(ID);
    }

}