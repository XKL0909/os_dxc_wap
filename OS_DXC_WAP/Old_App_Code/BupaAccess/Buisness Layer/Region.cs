﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Region
/// </summary>
public class Region
{
    #region Properties
        private string _regionName = string.Empty;
        public string RegionName
        {
            get { return _regionName; }
            set { _regionName = value; }
        }
        private string _code = string.Empty;
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
    #endregion

	public Region(string RegionName,string Code)
	{
        this.RegionName = RegionName;
        this.Code = Code;
	}
}