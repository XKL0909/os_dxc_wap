﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

/// <summary>
/// Summary description for MobileProvider
/// </summary>
public class MobileProvider
{
	public MobileProvider()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static MobileDataAccessConcrete ConcreteDataAccess()
    {
        return new MobileDataAccessConcrete(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString());
    }
}