﻿    using System;
    using System.Data.SqlClient;
    using System.Data;

    /// <summary>
    /// Summary description for MobileDataAccessConcrete
    /// </summary>
    public class MobileDataAccessConcrete
    {
	    private string ConnString;
        public MobileDataAccessConcrete(string CnString)
        {
	        ConnString = CnString;
        }

        #region MyHealth

        public IDataReader GetAllMyHealth()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealth";
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealth(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealth Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealth(string value, string type)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealth";
            if (type == "deviceid")
            {
                cmd.CommandText += " where DeviceID=@DeviceID";
                cmd.Parameters.AddWithValue("@DeviceID", value);
            }
            else
            {
                cmd.CommandText += " where MembershipNumber=@MembershipNumber";
                cmd.Parameters.AddWithValue("@MembershipNumber", value);
            }

            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealth(string MembershipNumber)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealth Where MembershipNumber=@MembershipNumber";
            cmd.Parameters.AddWithValue("@MembershipNumber", MembershipNumber);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthUsingDeviceID(string DeviceID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealth Where DeviceID=@DeviceID";
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public void InsertMyHealth(Int32 ID, bool Exercise, bool SleepDisorder, bool Smoker, bool Asthma, bool Arthritis, bool BackPain, bool Cancer, bool Depression, bool Diabetes, bool HeartProblem,bool HighBloodPressure, bool HighCholesterol, bool Stressed, bool PregnantSpouse, string MembershipNumber, string DeviceID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into MyHealth(Exercise,SleepDisorder,Smoker,Asthma,Arthritis,BackPain,Cancer,Depression,Diabetes,HeartProblem,HighBloodPressure,HighCholesterol,Stressed,PregnantSpouse,MembershipNumber,DeviceID) Values (@Exercise,@SleepDisorder,@Smoker,@Asthma,@Arthritis,@BackPain,@Cancer,@Depression,@Diabetes,@HeartProblem,@HighBloodPressure,@HighCholesterol,@Stressed,@PregnantSpouse,@MembershipNumber,@DeviceID)";
            cmd.Parameters.AddWithValue("@Exercise", Exercise);
            cmd.Parameters.AddWithValue("@Depression", Depression);
            cmd.Parameters.AddWithValue("@Stressed", Stressed);
            cmd.Parameters.AddWithValue("@PregnantSpouse", PregnantSpouse);
            cmd.Parameters.AddWithValue("@SleepDisorder", SleepDisorder);
            cmd.Parameters.AddWithValue("@Smoker", Smoker);
            cmd.Parameters.AddWithValue("@Asthma", Asthma);
            cmd.Parameters.AddWithValue("@Arthritis", Arthritis);
            cmd.Parameters.AddWithValue("@BackPain", BackPain);
            cmd.Parameters.AddWithValue("@Cancer", Cancer);
            cmd.Parameters.AddWithValue("@Diabetes", Diabetes);
            cmd.Parameters.AddWithValue("@HeartProblem", HeartProblem);
            cmd.Parameters.AddWithValue("@HighBloodPressure", HighBloodPressure);
            cmd.Parameters.AddWithValue("@HighCholesterol", HighCholesterol);
            cmd.Parameters.AddWithValue("@MembershipNumber", MembershipNumber);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateMyHealth(Int32 ID, bool Exercise, bool SleepDisorder, bool Smoker, bool Asthma, bool Arthritis, bool BackPain, bool Cancer, bool Depression, bool Diabetes, bool HeartProblem, bool HighBloodPressure, bool HighCholesterol, bool Stressed, bool PregnantSpouse, string MembershipNumber, string DeviceID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Update MyHealth Set Exercise=@Exercise,Depression=@Depression,Stressed=@Stressed,PregnantSpouse=@PregnantSpouse,SleepDisorder=@SleepDisorder,Smoker=@Smoker,Asthma=@Asthma,Arthritis=@Arthritis,BackPain=@BackPain,Cancer=@Cancer,Diabetes=@Diabetes,HeartProblem=@HeartProblem,HighBloodPressure=@HighBloodPressure,HighCholesterol=@HighCholesterol,MembershipNumber=@MembershipNumber,DeviceID=@DeviceID Where ID=@ID";
            cmd.Parameters.AddWithValue("@Exercise", Exercise);
            cmd.Parameters.AddWithValue("@Depression", Depression);
            cmd.Parameters.AddWithValue("@Stressed", Stressed);
            cmd.Parameters.AddWithValue("@PregnantSpouse", PregnantSpouse);
            cmd.Parameters.AddWithValue("@SleepDisorder", SleepDisorder);
            cmd.Parameters.AddWithValue("@Smoker", Smoker);
            cmd.Parameters.AddWithValue("@Asthma", Asthma);
            cmd.Parameters.AddWithValue("@Arthritis", Arthritis);
            cmd.Parameters.AddWithValue("@BackPain", BackPain);
            cmd.Parameters.AddWithValue("@Cancer", Cancer);
            cmd.Parameters.AddWithValue("@Diabetes", Diabetes);
            cmd.Parameters.AddWithValue("@HeartProblem", HeartProblem);
            cmd.Parameters.AddWithValue("@HighBloodPressure", HighBloodPressure);
            cmd.Parameters.AddWithValue("@HighCholesterol", HighCholesterol);
            cmd.Parameters.AddWithValue("@MembershipNumber", MembershipNumber);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteMyHealth(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Delete From MyHealth Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        #endregion

        #region MyHealthDietAndNutrition

        public IDataReader GetAllMyHealthDietAndNutrition()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthDietAndNutrition";
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthDietAndNutrition(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthDietAndNutrition Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthDietAndNutrition(string value, string type)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthDietAndNutrition";
            if (type == "deviceid")
            {
                cmd.CommandText += " where DeviceID=@DeviceID";
                cmd.Parameters.AddWithValue("@DeviceID", value);
            }
            else
            {
                cmd.CommandText += " where MembershipNo=@MembershipNo";
                cmd.Parameters.AddWithValue("@MembershipNo", value);
            }

            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public void InsertMyHealthDietAndNutrition(Int32 ID, string MembershipNo = "", string DeviceID = "", string SoftDrinks = "", string Fruits = "", string Vegetables = "", string DairyProducts = "", string EnergyDrinks = "", string Nuts = "", string SugaryJuices = "",
        string Sweets = "", string HighFibber = "", string FattyOily = "", string waterPerDay = "", string CoffeeTea = "")
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into MyHealthDietAndNutrition(MembershipNo,DeviceID,SoftDrinks,Fruits,Vegetables,DairyProducts,EnergyDrinks,Nuts,SugaryJuices,Sweets,HighFibber,FattyOily,waterPerDay,CoffeeTea) Values (@MembershipNo,@DeviceID,@SoftDrinks,@Fruits,@Vegetables,@DairyProducts,@EnergyDrinks,@Nuts,@SugaryJuices,@Sweets,@HighFibber,@FattyOily,@waterPerDay,@CoffeeTea)";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@SoftDrinks", SoftDrinks);
            cmd.Parameters.AddWithValue("@Fruits", Fruits);
            cmd.Parameters.AddWithValue("@Vegetables", Vegetables);
            cmd.Parameters.AddWithValue("@DairyProducts", DairyProducts);
            cmd.Parameters.AddWithValue("@EnergyDrinks", EnergyDrinks);
            cmd.Parameters.AddWithValue("@Nuts", Nuts);
            cmd.Parameters.AddWithValue("@SugaryJuices", SugaryJuices);
            cmd.Parameters.AddWithValue("@Sweets", Sweets);
            cmd.Parameters.AddWithValue("@HighFibber", HighFibber);
            cmd.Parameters.AddWithValue("@FattyOily", FattyOily);
            cmd.Parameters.AddWithValue("@waterPerDay", waterPerDay);
            cmd.Parameters.AddWithValue("@CoffeeTea", CoffeeTea);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateMyHealthDietAndNutrition(Int32 ID, string MembershipNo, string DeviceID, string SoftDrinks, string Fruits, string Vegetables, string DairyProducts, string EnergyDrinks, string Nuts, string SugaryJuices,
        string Sweets, string HighFibber, string FattyOily, string waterPerDay, string CoffeeTea)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Update MyHealthDietAndNutrition Set MembershipNo=@MembershipNo,DeviceID=@DeviceID,SoftDrinks=@SoftDrinks,Fruits=@Fruits,Vegetables=@Vegetables,DairyProducts=@DairyProducts,EnergyDrinks=@EnergyDrinks,Nuts=@Nuts,SugaryJuices=@SugaryJuices,Sweets=@Sweets,HighFibber=@HighFibber,FattyOily=@FattyOily,waterPerDay=@waterPerDay,CoffeeTea=@CoffeeTea Where ID=@ID";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@SoftDrinks", SoftDrinks);
            cmd.Parameters.AddWithValue("@Fruits", Fruits);
            cmd.Parameters.AddWithValue("@Vegetables", Vegetables);
            cmd.Parameters.AddWithValue("@DairyProducts", DairyProducts);
            cmd.Parameters.AddWithValue("@EnergyDrinks", EnergyDrinks);
            cmd.Parameters.AddWithValue("@Nuts", Nuts);
            cmd.Parameters.AddWithValue("@SugaryJuices", SugaryJuices);
            cmd.Parameters.AddWithValue("@Sweets", Sweets);
            cmd.Parameters.AddWithValue("@HighFibber", HighFibber);
            cmd.Parameters.AddWithValue("@FattyOily", FattyOily);
            cmd.Parameters.AddWithValue("@waterPerDay", waterPerDay);
            cmd.Parameters.AddWithValue("@CoffeeTea", CoffeeTea);
            cmd.Parameters.AddWithValue("@ID", ID);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteMyHealthDietAndNutrition(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Delete From MyHealthDietAndNutrition Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        #endregion

        #region MyHealthGeneralInformation

        public IDataReader GetAllMyHealthGeneralInformation()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthGeneralInformation";
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthGeneralInformation(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthGeneralInformation Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthGeneralInformation(string value, string type)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthGeneralInformation";
            if (type == "deviceid")
            {
                cmd.CommandText += " where DeviceID=@DeviceID";
                cmd.Parameters.AddWithValue("@DeviceID", value);
            }
            else {
                cmd.CommandText += " where MembershipNo=@MembershipNo";
                cmd.Parameters.AddWithValue("@MembershipNo", value);
            }
            
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public void InsertMyHealthGeneralInformation(Int32 ID, string MembershipNo = "", string DeviceID = "", Int32 Age = 0, string WaistSizeInch = "", string WeightinKg = "", string HeightinCm = "", string email = "", string Gender = "", string name = "",
        string WaistSizeCm = "", string MobileNo = "")
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into MyHealthGeneralInformation(MembershipNo,DeviceID,Age,WaistSizeInch,WeightinKg,HeightinCm,email,Gender,name,WaistSizeCm,MobileNo) Values (@MembershipNo,@DeviceID,@Age,@WaistSizeInch,@WeightinKg,@HeightinCm,@email,@Gender,@name,@WaistSizeCm,@MobileNo)";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@Age", Age);
            cmd.Parameters.AddWithValue("@WaistSizeInch", WaistSizeInch);
            cmd.Parameters.AddWithValue("@WeightinKg", WeightinKg);
            cmd.Parameters.AddWithValue("@HeightinCm", HeightinCm);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@Gender", Gender);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@WaistSizeCm", WaistSizeCm);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateMyHealthGeneralInformation(Int32 ID, string MembershipNo, string DeviceID, Int32 Age, string WaistSizeInch, string WeightinKg, string HeightinCm, string email, string Gender, string name,
        string WaistSizeCm, string MobileNo)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Update MyHealthGeneralInformation Set MembershipNo=@MembershipNo,DeviceID=@DeviceID,Age=@Age,WaistSizeInch=@WaistSizeInch,WeightinKg=@WeightinKg,HeightinCm=@HeightinCm,email=@email,Gender=@Gender,name=@name,WaistSizeCm=@WaistSizeCm,MobileNo=@MobileNo Where ID=@ID";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@Age", Age);
            cmd.Parameters.AddWithValue("@WaistSizeInch", WaistSizeInch);
            cmd.Parameters.AddWithValue("@WeightinKg", WeightinKg);
            cmd.Parameters.AddWithValue("@HeightinCm", HeightinCm);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@Gender", Gender);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@WaistSizeCm", WaistSizeCm);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
            cmd.Parameters.AddWithValue("@ID", ID);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteMyHealthGeneralInformation(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Delete From MyHealthGeneralInformation Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        #endregion

        #region MyHealthMedicalHistory

        public IDataReader GetAllMyHealthMedicalHistory()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthMedicalHistory";
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthMedicalHistory(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthMedicalHistory Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthMedicalHistory(string value, string type)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthMedicalHistory";
            if (type == "deviceid")
            {
                cmd.CommandText += " where DeviceID=@DeviceID";
                cmd.Parameters.AddWithValue("@DeviceID", value);
            }
            else
            {
                cmd.CommandText += " where MembershipNo=@MembershipNo";
                cmd.Parameters.AddWithValue("@MembershipNo", value);
            }

            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public void InsertMyHealthMedicalHistory(Int32 ID, string MembershipNo, string DeviceID, bool Diabetes_family, bool Depression, bool Fatigue, bool HaveYouBeenHospitalized, bool Nightmares, bool Snoring, bool Asthma,
        bool SleepDisorderInsomnia, bool HeartProblem, bool HighBloodPressure, bool Cancer, bool HighCholesterol, bool Diabetes, bool HighCholesterol_family, bool Cancer_family, bool Epilepsy, bool Arthritis,
        bool Migraine, bool HighBloodPressure_family, bool BackPain, bool haveAnySurgery, bool HeartProblem_family, bool ColonPolyps, bool ChronicInflammatory, bool Depression_family)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into MyHealthMedicalHistory(MembershipNo,DeviceID,Diabetes_family,Depression,Fatigue,HaveYouBeenHospitalized,Nightmares,Snoring,Asthma,SleepDisorderInsomnia,HeartProblem,HighBloodPressure,Cancer,HighCholesterol,Diabetes,HighCholesterol_family,Cancer_family,Epilepsy,Arthritis,Migraine,HighBloodPressure_family,BackPain,haveAnySurgery,HeartProblem_family,ColonPolyps,ChronicInflammatory,Depression_family) Values (@MembershipNo,@DeviceID,@Diabetes_family,@Depression,@Fatigue,@HaveYouBeenHospitalized,@Nightmares,@Snoring,@Asthma,@SleepDisorderInsomnia,@HeartProblem,@HighBloodPressure,@Cancer,@HighCholesterol,@Diabetes,@HighCholesterol_family,@Cancer_family,@Epilepsy,@Arthritis,@Migraine,@HighBloodPressure_family,@BackPain,@haveAnySurgery,@HeartProblem_family,@ColonPolyps,@ChronicInflammatory,@Depression_family)";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@Diabetes_family", Diabetes_family);
            cmd.Parameters.AddWithValue("@Depression", Depression);
            cmd.Parameters.AddWithValue("@Fatigue", Fatigue);
            cmd.Parameters.AddWithValue("@HaveYouBeenHospitalized", HaveYouBeenHospitalized);
            cmd.Parameters.AddWithValue("@Nightmares", Nightmares);
            cmd.Parameters.AddWithValue("@Snoring", Snoring);
            cmd.Parameters.AddWithValue("@Asthma", Asthma);
            cmd.Parameters.AddWithValue("@SleepDisorderInsomnia", SleepDisorderInsomnia);
            cmd.Parameters.AddWithValue("@HeartProblem", HeartProblem);
            cmd.Parameters.AddWithValue("@HighBloodPressure", HighBloodPressure);
            cmd.Parameters.AddWithValue("@Cancer", Cancer);
            cmd.Parameters.AddWithValue("@HighCholesterol", HighCholesterol);
            cmd.Parameters.AddWithValue("@Diabetes", Diabetes);
            cmd.Parameters.AddWithValue("@HighCholesterol_family", HighCholesterol_family);
            cmd.Parameters.AddWithValue("@Cancer_family", Cancer_family);
            cmd.Parameters.AddWithValue("@Epilepsy", Epilepsy);
            cmd.Parameters.AddWithValue("@Arthritis", Arthritis);
            cmd.Parameters.AddWithValue("@Migraine", Migraine);
            cmd.Parameters.AddWithValue("@HighBloodPressure_family", HighBloodPressure_family);
            cmd.Parameters.AddWithValue("@BackPain", BackPain);
            cmd.Parameters.AddWithValue("@haveAnySurgery", haveAnySurgery);
            cmd.Parameters.AddWithValue("@HeartProblem_family", HeartProblem_family);
            cmd.Parameters.AddWithValue("@ColonPolyps", ColonPolyps);
            cmd.Parameters.AddWithValue("@ChronicInflammatory", ChronicInflammatory);
            cmd.Parameters.AddWithValue("@Depression_family", Depression_family);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateMyHealthMedicalHistory(Int32 ID, string MembershipNo, string DeviceID, bool Diabetes_family, bool Depression, bool Fatigue, bool HaveYouBeenHospitalized, bool Nightmares, bool Snoring, bool Asthma,
        bool SleepDisorderInsomnia, bool HeartProblem, bool HighBloodPressure, bool Cancer, bool HighCholesterol, bool Diabetes, bool HighCholesterol_family, bool Cancer_family, bool Epilepsy, bool Arthritis,
        bool Migraine, bool HighBloodPressure_family, bool BackPain, bool haveAnySurgery, bool HeartProblem_family, bool ColonPolyps, bool ChronicInflammatory, bool Depression_family)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Update MyHealthMedicalHistory Set MembershipNo=@MembershipNo,DeviceID=@DeviceID,Diabetes_family=@Diabetes_family,Depression=@Depression,Fatigue=@Fatigue,HaveYouBeenHospitalized=@HaveYouBeenHospitalized,Nightmares=@Nightmares,Snoring=@Snoring,Asthma=@Asthma,SleepDisorderInsomnia=@SleepDisorderInsomnia,HeartProblem=@HeartProblem,HighBloodPressure=@HighBloodPressure,Cancer=@Cancer,HighCholesterol=@HighCholesterol,Diabetes=@Diabetes,HighCholesterol_family=@HighCholesterol_family,Cancer_family=@Cancer_family,Epilepsy=@Epilepsy,Arthritis=@Arthritis,Migraine=@Migraine,HighBloodPressure_family=@HighBloodPressure_family,BackPain=@BackPain,haveAnySurgery=@haveAnySurgery,HeartProblem_family=@HeartProblem_family,ColonPolyps=@ColonPolyps,ChronicInflammatory=@ChronicInflammatory,Depression_family=@Depression_family Where ID=@ID";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@Diabetes_family", Diabetes_family);
            cmd.Parameters.AddWithValue("@Depression", Depression);
            cmd.Parameters.AddWithValue("@Fatigue", Fatigue);
            cmd.Parameters.AddWithValue("@HaveYouBeenHospitalized", HaveYouBeenHospitalized);
            cmd.Parameters.AddWithValue("@Nightmares", Nightmares);
            cmd.Parameters.AddWithValue("@Snoring", Snoring);
            cmd.Parameters.AddWithValue("@Asthma", Asthma);
            cmd.Parameters.AddWithValue("@SleepDisorderInsomnia", SleepDisorderInsomnia);
            cmd.Parameters.AddWithValue("@HeartProblem", HeartProblem);
            cmd.Parameters.AddWithValue("@HighBloodPressure", HighBloodPressure);
            cmd.Parameters.AddWithValue("@Cancer", Cancer);
            cmd.Parameters.AddWithValue("@HighCholesterol", HighCholesterol);
            cmd.Parameters.AddWithValue("@Diabetes", Diabetes);
            cmd.Parameters.AddWithValue("@HighCholesterol_family", HighCholesterol_family);
            cmd.Parameters.AddWithValue("@Cancer_family", Cancer_family);
            cmd.Parameters.AddWithValue("@Epilepsy", Epilepsy);
            cmd.Parameters.AddWithValue("@Arthritis", Arthritis);
            cmd.Parameters.AddWithValue("@Migraine", Migraine);
            cmd.Parameters.AddWithValue("@HighBloodPressure_family", HighBloodPressure_family);
            cmd.Parameters.AddWithValue("@BackPain", BackPain);
            cmd.Parameters.AddWithValue("@haveAnySurgery", haveAnySurgery);
            cmd.Parameters.AddWithValue("@HeartProblem_family", HeartProblem_family);
            cmd.Parameters.AddWithValue("@ColonPolyps", ColonPolyps);
            cmd.Parameters.AddWithValue("@ChronicInflammatory", ChronicInflammatory);
            cmd.Parameters.AddWithValue("@Depression_family", Depression_family);
            cmd.Parameters.AddWithValue("@ID", ID);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteMyHealthMedicalHistory(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Delete From MyHealthMedicalHistory Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        #endregion

        #region MyHealthPhysicalActivity

        public IDataReader GetAllMyHealthPhysicalActivity()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthPhysicalActivity";
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthPhysicalActivity(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthPhysicalActivity Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthPhysicalActivity(string value, string type)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthPhysicalActivity";
            if (type == "deviceid")
            {
                cmd.CommandText += " where DeviceID=@DeviceID";
                cmd.Parameters.AddWithValue("@DeviceID", value);
            }
            else
            {
                cmd.CommandText += " where MembershipNo=@MembershipNo";
                cmd.Parameters.AddWithValue("@MembershipNo", value);
            }

            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public void InsertMyHealthPhysicalActivity(Int32 ID, string MembershipNo = "", string DeviceID = "", string SleepPerDay = "", string ExercisePerWeek = "")
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into MyHealthPhysicalActivity(MembershipNo,DeviceID,SleepPerDay,ExercisePerWeek) Values (@MembershipNo,@DeviceID,@SleepPerDay,@ExercisePerWeek)";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@SleepPerDay", SleepPerDay);
            cmd.Parameters.AddWithValue("@ExercisePerWeek", ExercisePerWeek);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateMyHealthPhysicalActivity(Int32 ID, string MembershipNo, string DeviceID, string SleepPerDay, string ExercisePerWeek)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Update MyHealthPhysicalActivity Set MembershipNo=@MembershipNo,DeviceID=@DeviceID,SleepPerDay=@SleepPerDay,ExercisePerWeek=@ExercisePerWeek Where ID=@ID";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@SleepPerDay", SleepPerDay);
            cmd.Parameters.AddWithValue("@ExercisePerWeek", ExercisePerWeek);
            cmd.Parameters.AddWithValue("@ID", ID);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteMyHealthPhysicalActivity(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Delete From MyHealthPhysicalActivity Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        #endregion

        #region MyHealthSmoking

        public IDataReader GetAllMyHealthSmoking()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthSmoking";
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthSmoking(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthSmoking Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthSmoking(string value, string type)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthSmoking";
            if (type == "deviceid")
            {
                cmd.CommandText += " where DeviceID=@DeviceID";
                cmd.Parameters.AddWithValue("@DeviceID", value);
            }
            else
            {
                cmd.CommandText += " where MembershipNo=@MembershipNo";
                cmd.Parameters.AddWithValue("@MembershipNo", value);
            }

            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public void InsertMyHealthSmoking(Int32 ID, string MembershipNo = "", string DeviceID = "", bool smokeCigarettes = false, bool smokeShisha = false, string CigarettesPerDay = "", string ShishaPerWeek = "", string ShishaPerYear = "", string CigarettesPerYear = "")
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into MyHealthSmoking(MembershipNo,DeviceID,smokeCigarettes,smokeShisha,CigarettesPerDay,ShishaPerWeek,ShishaPerYear,CigarettesPerYear) Values (@MembershipNo,@DeviceID,@smokeCigarettes,@smokeShisha,@CigarettesPerDay,@ShishaPerWeek,@ShishaPerYear,@CigarettesPerYear)";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@smokeCigarettes", smokeCigarettes);
            cmd.Parameters.AddWithValue("@smokeShisha", smokeShisha);
            cmd.Parameters.AddWithValue("@CigarettesPerDay", CigarettesPerDay);
            cmd.Parameters.AddWithValue("@ShishaPerWeek", ShishaPerWeek);
            cmd.Parameters.AddWithValue("@ShishaPerYear", ShishaPerYear);
            cmd.Parameters.AddWithValue("@CigarettesPerYear", CigarettesPerYear);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateMyHealthSmoking(Int32 ID, string MembershipNo, string DeviceID, bool smokeCigarettes, bool smokeShisha, string CigarettesPerDay, string ShishaPerWeek, string ShishaPerYear, string CigarettesPerYear)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Update MyHealthSmoking Set MembershipNo=@MembershipNo,DeviceID=@DeviceID,smokeCigarettes=@smokeCigarettes,smokeShisha=@smokeShisha,CigarettesPerDay=@CigarettesPerDay,ShishaPerWeek=@ShishaPerWeek,ShishaPerYear=@ShishaPerYear,CigarettesPerYear=@CigarettesPerYear Where ID=@ID";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@smokeCigarettes", smokeCigarettes);
            cmd.Parameters.AddWithValue("@smokeShisha", smokeShisha);
            cmd.Parameters.AddWithValue("@CigarettesPerDay", CigarettesPerDay);
            cmd.Parameters.AddWithValue("@ShishaPerWeek", ShishaPerWeek);
            cmd.Parameters.AddWithValue("@ShishaPerYear", ShishaPerYear);
            cmd.Parameters.AddWithValue("@CigarettesPerYear", CigarettesPerYear);
            cmd.Parameters.AddWithValue("@ID", ID);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteMyHealthSmoking(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Delete From MyHealthSmoking Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        #endregion

        #region MyHealthStress

        public IDataReader GetAllMyHealthStress()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthStress";
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthStress(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthStress Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public IDataReader GetMyHealthStress(string value, string type)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Select * From MyHealthStress";
            if (type == "deviceid")
            {
                cmd.CommandText += " where DeviceID=@DeviceID";
                cmd.Parameters.AddWithValue("@DeviceID", value);
            }
            else
            {
                cmd.CommandText += " where MembershipNo=@MembershipNo";
                cmd.Parameters.AddWithValue("@MembershipNo", value);
            }

            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        public void InsertMyHealthStress(Int32 ID, string MembershipNo = "", string DeviceID = "", string otherEnvironmentalStress = "", string irritated = "", string Unemployment = "", string WorkOverload = "", string TooManyChildren = "", string RoleAmbiguity = "", string difficultToTolerateInterruptions = "",
        string HealthIssues = "", string hardToCalmDown = "", string difficultToRelax = "", string Loneliness = "", string Divorce = "", string regularHealthTips = "", string impatientWithDelays = "", string upsetRatherEasily = "", string JobChange = "", string ParentalConflict = "",
        string upsetOverMinorThings = "", string inflexibleWorkingHours = "", string Promotions = "", string Marriage = "", string TooMuchToDo = "", string optimisticPerson = "", string easilyGetNervous = "", string financialDifficulties = "", string LowSelfConfidence = "", string otherMajorWorkRelatedStress = "",
        string OverReact = "", string DeathInTheFamily = "", string LackOfSocialSupport = "", string losingControl = "")
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into MyHealthStress(MembershipNo,DeviceID,otherEnvironmentalStress,irritated,Unemployment,WorkOverload,TooManyChildren,RoleAmbiguity,difficultToTolerateInterruptions,HealthIssues,hardToCalmDown,difficultToRelax,Loneliness,Divorce,regularHealthTips,impatientWithDelays,upsetRatherEasily,JobChange,ParentalConflict,upsetOverMinorThings,inflexibleWorkingHours,Promotions,Marriage,TooMuchToDo,optimisticPerson,easilyGetNervous,financialDifficulties,LowSelfConfidence,otherMajorWorkRelatedStress,OverReact,DeathInTheFamily,LackOfSocialSupport,losingControl) Values (@MembershipNo,@DeviceID,@otherEnvironmentalStress,@irritated,@Unemployment,@WorkOverload,@TooManyChildren,@RoleAmbiguity,@difficultToTolerateInterruptions,@HealthIssues,@hardToCalmDown,@difficultToRelax,@Loneliness,@Divorce,@regularHealthTips,@impatientWithDelays,@upsetRatherEasily,@JobChange,@ParentalConflict,@upsetOverMinorThings,@inflexibleWorkingHours,@Promotions,@Marriage,@TooMuchToDo,@optimisticPerson,@easilyGetNervous,@financialDifficulties,@LowSelfConfidence,@otherMajorWorkRelatedStress,@OverReact,@DeathInTheFamily,@LackOfSocialSupport,@losingControl)";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@otherEnvironmentalStress", otherEnvironmentalStress);
            cmd.Parameters.AddWithValue("@irritated", irritated);
            cmd.Parameters.AddWithValue("@Unemployment", Unemployment);
            cmd.Parameters.AddWithValue("@WorkOverload", WorkOverload);
            cmd.Parameters.AddWithValue("@TooManyChildren", TooManyChildren);
            cmd.Parameters.AddWithValue("@RoleAmbiguity", RoleAmbiguity);
            cmd.Parameters.AddWithValue("@difficultToTolerateInterruptions", difficultToTolerateInterruptions);
            cmd.Parameters.AddWithValue("@HealthIssues", HealthIssues);
            cmd.Parameters.AddWithValue("@hardToCalmDown", hardToCalmDown);
            cmd.Parameters.AddWithValue("@difficultToRelax", difficultToRelax);
            cmd.Parameters.AddWithValue("@Loneliness", Loneliness);
            cmd.Parameters.AddWithValue("@Divorce", Divorce);
            cmd.Parameters.AddWithValue("@regularHealthTips", regularHealthTips);
            cmd.Parameters.AddWithValue("@impatientWithDelays", impatientWithDelays);
            cmd.Parameters.AddWithValue("@upsetRatherEasily", upsetRatherEasily);
            cmd.Parameters.AddWithValue("@JobChange", JobChange);
            cmd.Parameters.AddWithValue("@ParentalConflict", ParentalConflict);
            cmd.Parameters.AddWithValue("@upsetOverMinorThings", upsetOverMinorThings);
            cmd.Parameters.AddWithValue("@inflexibleWorkingHours", inflexibleWorkingHours);
            cmd.Parameters.AddWithValue("@Promotions", Promotions);
            cmd.Parameters.AddWithValue("@Marriage", Marriage);
            cmd.Parameters.AddWithValue("@TooMuchToDo", TooMuchToDo);
            cmd.Parameters.AddWithValue("@optimisticPerson", optimisticPerson);
            cmd.Parameters.AddWithValue("@easilyGetNervous", easilyGetNervous);
            cmd.Parameters.AddWithValue("@financialDifficulties", financialDifficulties);
            cmd.Parameters.AddWithValue("@LowSelfConfidence", LowSelfConfidence);
            cmd.Parameters.AddWithValue("@otherMajorWorkRelatedStress", otherMajorWorkRelatedStress);
            cmd.Parameters.AddWithValue("@OverReact", OverReact);
            cmd.Parameters.AddWithValue("@DeathInTheFamily", DeathInTheFamily);
            cmd.Parameters.AddWithValue("@LackOfSocialSupport", LackOfSocialSupport);
            cmd.Parameters.AddWithValue("@losingControl", losingControl);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void UpdateMyHealthStress(Int32 ID, string MembershipNo, string DeviceID, string otherEnvironmentalStress, string irritated, string Unemployment, string WorkOverload, string TooManyChildren, string RoleAmbiguity, string difficultToTolerateInterruptions,
        string HealthIssues, string hardToCalmDown, string difficultToRelax, string Loneliness, string Divorce, string regularHealthTips, string impatientWithDelays, string upsetRatherEasily, string JobChange, string ParentalConflict,
        string upsetOverMinorThings, string inflexibleWorkingHours, string Promotions, string Marriage, string TooMuchToDo, string optimisticPerson, string easilyGetNervous, string financialDifficulties, string LowSelfConfidence, string otherMajorWorkRelatedStress,
        string OverReact, string DeathInTheFamily, string LackOfSocialSupport, string losingControl)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Update MyHealthStress Set MembershipNo=@MembershipNo,DeviceID=@DeviceID,otherEnvironmentalStress=@otherEnvironmentalStress,irritated=@irritated,Unemployment=@Unemployment,WorkOverload=@WorkOverload,TooManyChildren=@TooManyChildren,RoleAmbiguity=@RoleAmbiguity,difficultToTolerateInterruptions=@difficultToTolerateInterruptions,HealthIssues=@HealthIssues,hardToCalmDown=@hardToCalmDown,difficultToRelax=@difficultToRelax,Loneliness=@Loneliness,Divorce=@Divorce,regularHealthTips=@regularHealthTips,impatientWithDelays=@impatientWithDelays,upsetRatherEasily=@upsetRatherEasily,JobChange=@JobChange,ParentalConflict=@ParentalConflict,upsetOverMinorThings=@upsetOverMinorThings,inflexibleWorkingHours=@inflexibleWorkingHours,Promotions=@Promotions,Marriage=@Marriage,TooMuchToDo=@TooMuchToDo,optimisticPerson=@optimisticPerson,easilyGetNervous=@easilyGetNervous,financialDifficulties=@financialDifficulties,LowSelfConfidence=@LowSelfConfidence,otherMajorWorkRelatedStress=@otherMajorWorkRelatedStress,OverReact=@OverReact,DeathInTheFamily=@DeathInTheFamily,LackOfSocialSupport=@LackOfSocialSupport,losingControl=@losingControl Where ID=@ID";
            cmd.Parameters.AddWithValue("@MembershipNo", MembershipNo);
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
            cmd.Parameters.AddWithValue("@otherEnvironmentalStress", otherEnvironmentalStress);
            cmd.Parameters.AddWithValue("@irritated", irritated);
            cmd.Parameters.AddWithValue("@Unemployment", Unemployment);
            cmd.Parameters.AddWithValue("@WorkOverload", WorkOverload);
            cmd.Parameters.AddWithValue("@TooManyChildren", TooManyChildren);
            cmd.Parameters.AddWithValue("@RoleAmbiguity", RoleAmbiguity);
            cmd.Parameters.AddWithValue("@difficultToTolerateInterruptions", difficultToTolerateInterruptions);
            cmd.Parameters.AddWithValue("@HealthIssues", HealthIssues);
            cmd.Parameters.AddWithValue("@hardToCalmDown", hardToCalmDown);
            cmd.Parameters.AddWithValue("@difficultToRelax", difficultToRelax);
            cmd.Parameters.AddWithValue("@Loneliness", Loneliness);
            cmd.Parameters.AddWithValue("@Divorce", Divorce);
            cmd.Parameters.AddWithValue("@regularHealthTips", regularHealthTips);
            cmd.Parameters.AddWithValue("@impatientWithDelays", impatientWithDelays);
            cmd.Parameters.AddWithValue("@upsetRatherEasily", upsetRatherEasily);
            cmd.Parameters.AddWithValue("@JobChange", JobChange);
            cmd.Parameters.AddWithValue("@ParentalConflict", ParentalConflict);
            cmd.Parameters.AddWithValue("@upsetOverMinorThings", upsetOverMinorThings);
            cmd.Parameters.AddWithValue("@inflexibleWorkingHours", inflexibleWorkingHours);
            cmd.Parameters.AddWithValue("@Promotions", Promotions);
            cmd.Parameters.AddWithValue("@Marriage", Marriage);
            cmd.Parameters.AddWithValue("@TooMuchToDo", TooMuchToDo);
            cmd.Parameters.AddWithValue("@optimisticPerson", optimisticPerson);
            cmd.Parameters.AddWithValue("@easilyGetNervous", easilyGetNervous);
            cmd.Parameters.AddWithValue("@financialDifficulties", financialDifficulties);
            cmd.Parameters.AddWithValue("@LowSelfConfidence", LowSelfConfidence);
            cmd.Parameters.AddWithValue("@otherMajorWorkRelatedStress", otherMajorWorkRelatedStress);
            cmd.Parameters.AddWithValue("@OverReact", OverReact);
            cmd.Parameters.AddWithValue("@DeathInTheFamily", DeathInTheFamily);
            cmd.Parameters.AddWithValue("@LackOfSocialSupport", LackOfSocialSupport);
            cmd.Parameters.AddWithValue("@losingControl", losingControl);
            cmd.Parameters.AddWithValue("@ID", ID);
            for (int i = 0; i < cmd.Parameters.Count - 1; i++)
            {
                if (cmd.Parameters[i].Value == null)
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void DeleteMyHealthStress(int ID)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Delete From MyHealthStress Where ID=@ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        #endregion

        #region SMS
        public void InsertSMS(string sender, string Receiver, string msg, string msgtype, string operator1, string status, DateTime sendtime)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into OnlineServiceSMS(sender,Receiver,msg,msgtype,operator,status,sendtime) Values (@sender,@Receiver,@msg,@msgtype,@operator,@status,@sendtime)";
            cmd.Parameters.AddWithValue("@sender", sender);
            cmd.Parameters.AddWithValue("@Receiver", Receiver);
            cmd.Parameters.AddWithValue("@msg", msg);
            cmd.Parameters.AddWithValue("@msgtype", msgtype);
            cmd.Parameters.AddWithValue("@operator", operator1);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@sendtime", sendtime);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        #endregion

          #region Member

        public IDataReader CheckMemberExists(string MembershipNumber)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "SELECT * FROM Member WHERE membershipno = @MembershipNo";
            cmd.Parameters.AddWithValue("@MembershipNo",MembershipNumber);
            IDataReader Idr = default(IDataReader);
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }

        #endregion
    }