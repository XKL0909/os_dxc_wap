﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasePage
/// </summary>
public partial class BasePage : System.Web.UI.Page
{
	public BasePage()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    protected override void InitializeCulture()
    {
        string vCulture = "";
        if (Request.QueryString["Lang"] != null && Request.QueryString["Lang"].ToString().ToLower() == "ar")
        {
            vCulture = System.Configuration.ConfigurationManager.AppSettings["LanguageArabicCulture"];
        }
        else
        {
            vCulture = System.Configuration.ConfigurationManager.AppSettings["LanguageEnglishCulture"];
        }
        
        System.Globalization.CultureInfo vCltr = new System.Globalization.CultureInfo(vCulture);
        System.Threading.Thread.CurrentThread.CurrentCulture = vCltr;
        System.Threading.Thread.CurrentThread.CurrentUICulture = vCltr;

    }



}