﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class District
{
    public string Code { get; set; }
    public string DistrictName { get; set; }
}