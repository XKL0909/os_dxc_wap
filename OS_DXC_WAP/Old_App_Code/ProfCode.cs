﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for ProfCode
/// </summary>
public class ProfCode
{
    private int _id = 0;
    private string _profcode = string.Empty;

    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    public string ProfCodeValue
    {
        get { return _profcode; }
        set { _profcode = value; }
    }

    public ProfCode()
    { 
    
    }

	public ProfCode(int ID,string ProfCode)
	{
        _id = ID;
        _profcode = ProfCode;

	}
}

public class ProfCodeControl
{
    public List<ProfCode> GetAllProfCodes()
    {

        List<ProfCode> MyCodes = new List<ProfCode>();
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
        cn.Open();
        cmd.Connection = cn;
        cmd.CommandText = "Select * From ProfCodes";
        IDataReader Idr; 
        Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        
        ProfCode MyCode;
        while(Idr.Read()){
            MyCode = new ProfCode();
            MyCode.ID = int.Parse(Idr["ID"].ToString());
            MyCode.ProfCodeValue = Idr["ProfCode"].ToString();
        }
        Idr.Close();
        cn.Close();
        return MyCodes;
    }

    public ProfCode GetProfCode(String ProfCode)
    {

        ProfCode MyCode = new ProfCode();
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
        cn.Open();
        cmd.Connection = cn;
        cmd.CommandText = "Select * From ProfCodes where ProfCode=@ProfCode";
        cmd.Parameters.AddWithValue("@ProfCode", ProfCode);
        IDataReader Idr;
        Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        if (Idr.Read())
        {
            MyCode.ID = int.Parse(Idr["ID"].ToString());
            MyCode.ProfCodeValue = Idr["ProfCode"].ToString();
        }
        Idr.Close();
        cn.Close();
        return MyCode;
    }

    public List<string> GetAllProfCodesAsString() {

        List<string> MyCodes = new List<string>();
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
        cn.Open();
        cmd.Connection = cn;
        cmd.CommandText = "Select * From ProfCodes";
        IDataReader Idr; 
        Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        
        string MyCode;
        while(Idr.Read()){

            MyCode = Idr["ProfCode"].ToString();
        }
        Idr.Close();
        cn.Close();
        return MyCodes;
    }

    public void DeleteAllProfCodes() {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
        cn.Open();
        cmd.Connection = cn;
        cmd.CommandText = "Delete From ProfCodes";
        cmd.ExecuteNonQuery();
        cn.Close();
    }

    public void InsertProdCodesList(List<ProfCode> MyCodes)
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
        cn.Open();
        cmd.Connection = cn;
        for (int i = 0; i < MyCodes.Count; i++) {
            cmd.Parameters.Clear();
            cmd.CommandText = "Insert into ProfCodes(ProfCode) values(@ProfCode)";
            cmd.Parameters.AddWithValue("@ProfCode", MyCodes[i].ProfCodeValue);
            cmd.ExecuteNonQuery();
        }
        
        cn.Close();
    }
}