using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

/// <summary>
/// Summary description for SessionManager
/// </summary>
public static class SessionManager
{
    //public SessionManager()
    //{
    //    //
    //    // TODO: Add constructor logic here
    //    //
    //}

    public static HttpContext Context;
    public static HttpSessionState Session;
    public static HttpRequest Request;
    public static HttpResponse Response;
   // private Session ;

  static  public void CheckSessionTimeout()
    {
        //if ( Context.Session != null)
        //{
        //    if (Session.IsNewSession)
        //    {
        //        string szCookieHeader = Request.Headers["Cookie"];
        //        if ((null != szCookieHeader) && (szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0))
        //        {
        //            Response.Redirect("../ProviderDefault.aspx");
        //        }
        //    }
        //}


        HttpContext context = HttpContext.Current;
        HttpCookieCollection cookies = context.Request.Cookies;

        if (context.Session != null)
        {
            if (context.Session.IsNewSession)
            {
                string szCookieHeader = context.Request.Headers["Cookie"];
                if ((null != szCookieHeader) && (szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0))
                {
                    context.Response.Redirect("../SessionExpired.aspx");
                }


                if (cookies["starttime"] == null)
                {
                    HttpCookie cookie = new HttpCookie("starttime", DateTime.Now.ToString());
                    cookie.Path = "/";
                    context.Response.Cookies.Add(cookie);
                }
                else
                {
                    context.Response.Redirect("PageLoggingOut.aspx");

                }
            }

        }

        //if (cookies["starttime"] == null)
        //{
        //    HttpCookie cookie = new HttpCookie("starttime", DateTime.Now.ToString());
        //    cookie.Path = "/";
        //    context.Response.Cookies.Add(cookie);
        //}
        //else
        //{
        //    context.Response.Redirect("PageLoggingOut.aspx");
        //}    






        //    string msgSession = "Warning: Within next 3 minutes, if you do not do anything, " + " our system will redirect to the login page. Please save changed data.";
        //    //time to remind, 3 minutes before session ends
        //    int int_MilliSecondsTimeReminder = (this.Session.Timeout * 60000) - 3 * 60000; 
        //    //time to redirect, 5 milliseconds before session ends
        //    int int_MilliSecondsTimeOut = (this.Session.Timeout * 60000) - 5; 

        //    string str_Script = @"
        //            var myTimeReminder, myTimeOut; 
        //            clearTimeout(myTimeReminder); 
        //            clearTimeout(myTimeOut); " +
        //            "var sessionTimeReminder = " + 
        //        int_MilliSecondsTimeReminder.ToString() + "; " +
        //            "var sessionTimeout = " + int_MilliSecondsTimeOut.ToString() + ";" +
        //            "function doReminder(){ alert('" + msgSession + "'); }" +
        //            "function doRedirect(){ window.location.href='ProviderDefault.aspx'; }" + @"
        //            myTimeReminder=setTimeout('doReminder()', sessionTimeReminder); 
        //            myTimeOut=setTimeout('doRedirect()', sessionTimeout); ";

        //     ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), 
        //           "CheckSessionOut", str_Script, true);
    }


    static public void CheckSessionTimeoutForMemberVerify()
    {
        //if ( Context.Session != null)
        //{
        //    if (Session.IsNewSession)
        //    {
        //        string szCookieHeader = Request.Headers["Cookie"];
        //        if ((null != szCookieHeader) && (szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0))
        //        {
        //            Response.Redirect("../ProviderDefault.aspx");
        //        }
        //    }
        //}


        HttpContext context = HttpContext.Current;
        HttpCookieCollection cookies = context.Request.Cookies;

        if (context.Session != null)
        {
            if (context.Session.IsNewSession)
            {
                string szCookieHeader = context.Request.Headers["Cookie"];
                if ((null != szCookieHeader) && (szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0))
                {
                    context.Response.Redirect("../SessionExpiredForMbrVerify.aspx");
                }


                if (cookies["starttime"] == null)
                {
                    HttpCookie cookie = new HttpCookie("starttime", DateTime.Now.ToString());
                    cookie.Path = "/";
                    context.Response.Cookies.Add(cookie);
                }
                else
                {
                    context.Response.Redirect("PageLoggingOut.aspx");

                }
            }

        }

        //if (cookies["starttime"] == null)
        //{
        //    HttpCookie cookie = new HttpCookie("starttime", DateTime.Now.ToString());
        //    cookie.Path = "/";
        //    context.Response.Cookies.Add(cookie);
        //}
        //else
        //{
        //    context.Response.Redirect("PageLoggingOut.aspx");
        //}    






        //    string msgSession = "Warning: Within next 3 minutes, if you do not do anything, " + " our system will redirect to the login page. Please save changed data.";
        //    //time to remind, 3 minutes before session ends
        //    int int_MilliSecondsTimeReminder = (this.Session.Timeout * 60000) - 3 * 60000; 
        //    //time to redirect, 5 milliseconds before session ends
        //    int int_MilliSecondsTimeOut = (this.Session.Timeout * 60000) - 5; 

        //    string str_Script = @"
        //            var myTimeReminder, myTimeOut; 
        //            clearTimeout(myTimeReminder); 
        //            clearTimeout(myTimeOut); " +
        //            "var sessionTimeReminder = " + 
        //        int_MilliSecondsTimeReminder.ToString() + "; " +
        //            "var sessionTimeout = " + int_MilliSecondsTimeOut.ToString() + ";" +
        //            "function doReminder(){ alert('" + msgSession + "'); }" +
        //            "function doRedirect(){ window.location.href='ProviderDefault.aspx'; }" + @"
        //            myTimeReminder=setTimeout('doReminder()', sessionTimeReminder); 
        //            myTimeOut=setTimeout('doRedirect()', sessionTimeout); ";

        //     ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), 
        //           "CheckSessionOut", str_Script, true);
    }



}


