﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Configuration;
using BO;
using System.Data.SqlClient;
/// <summary>
/// Summary description for NationalityDB
/// </summary>
namespace DAL
{
    public class NationalityDB
    {
        private static string connection = System.Configuration.ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString();


        public static List<Nationalities> Get(bool isGcc)
        {

            System.Data.IDbConnection db = new SqlConnection(connection);
            string query = "select ID,LTRIM(RTRIM(Code)) as Code,LTRIM(RTRIM(Nationality)) as Nationality,IsGCC,LTRIM(RTRIM(CCHICode)) as CCHICode,LTRIM(RTRIM(MappedCode)) as MappedCode FROM Nationality  where IsGCC=@IsGCC";

            var entities = db.Query<Nationalities>(query, new { isGcc }).ToList();
            return entities;
        }


    }
}