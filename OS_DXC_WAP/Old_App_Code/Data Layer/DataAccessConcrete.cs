﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for DataAccessConcrete
/// </summary>
public class DataAccessConcrete
{
    private string ConnString;
    private UserDetails _userDetails;
    

    public DataAccessConcrete(){}

    public DataAccessConcrete(string CnString)
    {
        ConnString = CnString;
    }

    #region ReimburesmentClaimInvoice

    public void InsertReimburesmentClaimInvoice(ReimburesmentClaimInvoice MyInvoice)
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        ////cn.ConnectionString = ConnString; ///Disabled old connection mapping with new connection for OS New & OS Old by sakthi on 09-Apr-2016
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_Reimburesment"].ToString();//ConfigurationManager.AppSettings["ConnBupa_Reimburesment"];
        cn.Open();
        cmd.Connection = cn;
        cmd.CommandText = "Insert Into ReimburesmentClaimInvoice(NumberOfInvoices,TotalAmount,CurrencyCode,ReimburesmentClaimID) Values (@NumberOfInvoices,@TotalAmount,@CurrencyCode,@ReimburesmentClaimID)";
        cmd.Parameters.AddWithValue("@NumberOfInvoices", MyInvoice.NumberOfInvoices);
        cmd.Parameters.AddWithValue("@TotalAmount", MyInvoice.TotalAmount);
        cmd.Parameters.AddWithValue("@CurrencyCode", MyInvoice.CurrencyCode);
        cmd.Parameters.AddWithValue("@ReimburesmentClaimID", MyInvoice.ReimburesmentClaimID);
        cmd.ExecuteNonQuery();
        cn.Close();
    }

    public void InsertReimburesmentClaimInvoiceList(List<ReimburesmentClaimInvoice> MyInvoices)
    {
        if (MyInvoices.Count > 0)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ////cn.ConnectionString = ConnString; ///Disabled old connection mapping with new connection for OS New & OS Old by sakthi on 09-Apr-2016
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_Reimburesment"].ToString();//ConfigurationManager.AppSettings["ConnBupa_Reimburesment"];
            cn.Open();
            cmd.Connection = cn;
            cmd.Parameters.Clear();
            for (int i = 0; i < MyInvoices.Count; i++)
            {
                cmd.CommandText += "Insert Into ReimburesmentClaimInvoice(NumberOfInvoices,TotalAmount,CurrencyCode,ReimburesmentClaimID) Values (@NumberOfInvoices" + i + ",@TotalAmount" + i + ",@CurrencyCode" + i + ",@ReimburesmentClaimID" + i + ")";
                cmd.Parameters.AddWithValue("@NumberOfInvoices" + i, MyInvoices[i].NumberOfInvoices);
                cmd.Parameters.AddWithValue("@TotalAmount" + i, MyInvoices[i].TotalAmount);
                cmd.Parameters.AddWithValue("@CurrencyCode" + i, MyInvoices[i].CurrencyCode);
                cmd.Parameters.AddWithValue("@ReimburesmentClaimID" + i, MyInvoices[i].ReimburesmentClaimID);
            }
            cmd.ExecuteNonQuery();
            cn.Close();
        }
    }

    #endregion

    #region ReimburesmentClaim
        public int InsertReimburesmentClaim(ReimburesmentClaim MyClaim)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ////cn.ConnectionString = ConnString; ///Disabled old connection mapping with new connection for OS New & OS Old by sakthi on 09-Apr-2016
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_Reimburesment"].ToString();//ConfigurationManager.ConnectionStrings["ConnBupa_Reimburesment"].ToString();
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into ReimbursementClaim(Type,MemberName,MembershipNumber,CompanyName,MobileNumber,Email,PaymentType,BankName,IbanNumber,ReferenceNumber) Values (@Type,@MemberName,@MembershipNumber,@CompanyName,@MobileNumber,@Email,@PaymentType,@BankName,@IbanNumber,@ReferenceNumber); select scope_identity();";
            cmd.Parameters.AddWithValue("@Type", MyClaim.Type);
            cmd.Parameters.AddWithValue("@MembershipNumber", MyClaim.MembershipNumber);
            cmd.Parameters.AddWithValue("@CompanyName", MyClaim.CompanyName);
            cmd.Parameters.AddWithValue("@MobileNumber", MyClaim.MobileNumber);
            cmd.Parameters.AddWithValue("@Email", MyClaim.Email);
            cmd.Parameters.AddWithValue("@PaymentType", MyClaim.PaymentType);
            cmd.Parameters.AddWithValue("@BankName", MyClaim.BankName);
            cmd.Parameters.AddWithValue("@IbanNumber", MyClaim.IbanNumber);
            cmd.Parameters.AddWithValue("@ReferenceNumber", MyClaim.ReferenceNumber);
            cmd.Parameters.AddWithValue("@MemberName", MyClaim.MemberName);
            int NewID = int.Parse(cmd.ExecuteScalar().ToString());
            cn.Close();
            return NewID;
        }
    #endregion

    #region ReimburesmentClaimReason
        public void InsertReimburesmentClaimReason(ReimburesmentClaimReason MyReason)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ////cn.ConnectionString = ConnString; //Disabled old connection mapping with new connection for OS New & OS Old by sakthi on 09-Apr-2016
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_Reimburesment"].ToString();//ConfigurationManager.AppSettings["ConnBupa_Reimburesment"];
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "Insert Into ReimburesmentClaimReason(ReimburesmentClaimID,ReasonID,Other) Values (@ReimburesmentClaimID,@ReasonID,@Other)";
            cmd.Parameters.AddWithValue("@ReimburesmentClaimID", MyReason.ReimburesmentClaimID);
            cmd.Parameters.AddWithValue("@ReasonID", MyReason.ReasonID);
            cmd.Parameters.AddWithValue("@Other", MyReason.Other);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void InsertReimburesmentClaimReasonList(List<ReimburesmentClaimReason> MyReasons)
        {
            if (MyReasons.Count > 0)
            {
                SqlCommand cmd = new SqlCommand();
                SqlConnection cn = new SqlConnection();
                ////cn.ConnectionString = ConnString;  //Disabled old connection mapping with new connection for OS New & OS Old by sakthi on 09-Apr-2016
                cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_Reimburesment"].ToString();//ConfigurationManager.AppSettings["ConnBupa_Reimburesment"];
                cn.Open();
                cmd.Connection = cn;
                cmd.Parameters.Clear();
                for (int i = 0; i < MyReasons.Count; i++)
                {
                    cmd.CommandText += "Insert Into ReimburesmentClaimReason(ReimburesmentClaimID,ReasonID,Other) Values (@ReimburesmentClaimID" + i + ",@ReasonID" + i + ",@Other" + i + ")";
                    cmd.Parameters.AddWithValue("@ReimburesmentClaimID" + i, MyReasons[i].ReimburesmentClaimID);
                    cmd.Parameters.AddWithValue("@ReasonID" + i, MyReasons[i].ReasonID);
                    cmd.Parameters.AddWithValue("@Other" + i, MyReasons[i].Other);
                }
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
    #endregion

        #region
        public void InsertUpdateMailTracking(EmailTracking emailTracking)
        {

            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ////cn.ConnectionString = ConnString; //Disabled old connection mapping with new connection for OS New & OS Old by sakthi on 09-Apr-2016
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_Reimburesment"].ToString();//ConfigurationManager.AppSettings["ConnBupa_Reimburesment"];
            cn.Open();
            cmd.Connection = cn;
            cmd = new SqlCommand("BME_EmailTracking_InsertUpdate", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ReimburesmentClaimID", emailTracking.ReimburesmentClaimID));
            cmd.Parameters.Add(new SqlParameter("@EmailType", emailTracking.EmailType));
            cmd.Parameters.Add(new SqlParameter("@FromEmail", emailTracking.FromEmail));
            cmd.Parameters.Add(new SqlParameter("@ToEmail", emailTracking.ToEmail));           
            cmd.Parameters.Add(new SqlParameter("@IsSend", emailTracking.IsSend));
            cmd.Parameters.Add(new SqlParameter("@MailSubject", emailTracking.MailSubject));
            cmd.Parameters.Add(new SqlParameter("@MailBody", emailTracking.MailBody));
            cmd.Parameters.Add(new SqlParameter("@CreatedBy", emailTracking.CreatedBy));
            cmd.Parameters.Add(new SqlParameter("@AttachmentFiles", emailTracking.AttachmentFiles));
            cmd.Parameters.Add(new SqlParameter("@VirtualPath", emailTracking.VirtualPath));
            cmd.Parameters.Add(new SqlParameter("@ActionType", emailTracking.ActionType));
            cmd.Parameters.Add(new SqlParameter("@FileNames", emailTracking.FileNames));
            cmd.Parameters.Add(new SqlParameter("@TrackingID", emailTracking.TrackingID));
            cmd.Parameters.Add(new SqlParameter("@ClaimsType", emailTracking.ClaimsType));
            cmd.Parameters.Add(new SqlParameter("@Status", emailTracking.Status));
            cmd.Parameters.Add(new SqlParameter("@SubmissionType", emailTracking.SubmissionType));
            cmd.Parameters.Add(new SqlParameter("@ServerIP", emailTracking.ServerIP));
            cmd.Parameters.Add(new SqlParameter("@ClientIP", emailTracking.ClientIP));
            cmd.Parameters.Add(new SqlParameter("@ClientBrowserDetails", emailTracking.ClientBrowserDetails));
            cmd.Parameters.Add(new SqlParameter("@Source", emailTracking.Source));
            cmd.ExecuteNonQuery();
            cn.Close();

        }
    //Sakthi user management
   //start
    public List<UserDetails> GetUserDetails(UserDetails userDetails)
    {
        List<UserDetails> lstUserDetails = new List<UserDetails>();
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
       

        cn.ConnectionString = ConnString;
        try
        {
            cn.Open();
            cmd.Connection = cn;
            cmd = new SqlCommand("sp_GetUserManagmentActivation", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@UserGroupID", userDetails.UserGroupID));
            cmd.Parameters.Add(new SqlParameter("@Provider", userDetails.Status));
            cmd.Parameters.Add(new SqlParameter("@RefernceNumber", userDetails.ReferenceNumber));
            IDataReader iDataReader;
            iDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            while (iDataReader.Read())
            {
                UserDetails objUserDetails = new UserDetails();
                objUserDetails.ReferenceNumber = int.Parse(Convert.ToString(iDataReader["ReferenceNumber"]));
                objUserDetails.UserLoginID = Convert.ToString(iDataReader["UserLoginID"]);
                objUserDetails.UserGroupID = Convert.ToString(iDataReader["UserGroupID"]);
                objUserDetails.Name = Convert.ToString(iDataReader["Name"]);
                objUserDetails.Email = Convert.ToString(iDataReader["Email"]);
                objUserDetails.Mobile = Convert.ToString(iDataReader["Mobile"]);
                objUserDetails.IsUserActPending = Convert.ToString(iDataReader["IsUserActPending"]) == "1" ? true : false;
                objUserDetails.IsActive = Convert.ToString(iDataReader["IsActive"]) == "1" ? true : false;
                objUserDetails.Status = objUserDetails.IsActive ? "Yes" : "No";
                lstUserDetails.Add(objUserDetails);
            }
            iDataReader.Close();
            
        }
        catch (Exception)
        {
            lstUserDetails = null;
            throw;
        }   
        finally
        {
            cn.Close();
        }
        return lstUserDetails;



    }

    public UserDetails GetUserDetailByReferenceNumber(UserDetails userDetails)
    {
        
        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();

        cn.ConnectionString = ConnString;
        try
        {
            cn.Open();
            cmd.Connection = cn;
            cmd = new SqlCommand("sp_GetUserDetailByReferenceNumber", cn);
            cmd.CommandType = CommandType.StoredProcedure;            
            cmd.Parameters.Add(new SqlParameter("@Provider", userDetails.Status));
            cmd.Parameters.Add(new SqlParameter("@RefernceNumber", userDetails.ReferenceNumber));
            IDataReader iDataReader;
            iDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            while (iDataReader.Read())
            {
                _userDetails = new UserDetails();
                _userDetails.ReferenceNumber = int.Parse(Convert.ToString(iDataReader["ReferenceNumber"]));
                _userDetails.UserLoginID = Convert.ToString(iDataReader["UserLoginID"]);
                _userDetails.UserGroupID = Convert.ToString(iDataReader["UserGroupID"]);
                _userDetails.Name = Convert.ToString(iDataReader["Name"]);
                _userDetails.Email = Convert.ToString(iDataReader["Email"]);
                _userDetails.Mobile = Convert.ToString(iDataReader["Mobile"]);
                _userDetails.IsActive = Convert.ToString(iDataReader["IsActive"]) == "1" ? true : false;
                _userDetails.Status = userDetails.IsActive ? "Yes" : "No";
               
            }
            iDataReader.Close();

        }
        catch (Exception)
        {
            _userDetails = null;
            throw;
        }
        finally
        {
            cn.Close();
        }
        return _userDetails;



    }
    public UserDetails VerifyOTP(UserDetails userDetails)
    {

        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();

        cn.ConnectionString = ConnString;
        try
        {
            cn.Open();
            cmd.Connection = cn;
            cmd = new SqlCommand("sp_VerifyOTP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Provider", userDetails.Status));
            cmd.Parameters.Add(new SqlParameter("@RefernceNumber", userDetails.ReferenceNumber));
            cmd.Parameters.Add(new SqlParameter("@Otp", userDetails.OTP));

            IDataReader iDataReader;
            iDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            while (iDataReader.Read())
            {
                _userDetails = new UserDetails();
                _userDetails.ReferenceNumber = int.Parse(Convert.ToString(iDataReader["ReferenceNumber"]));
                _userDetails.UserLoginID = Convert.ToString(iDataReader["UserLoginID"]);
                _userDetails.UserGroupID = Convert.ToString(iDataReader["UserGroupID"]);
                _userDetails.Name = Convert.ToString(iDataReader["Name"]);
                _userDetails.Email = Convert.ToString(iDataReader["Email"]);
                _userDetails.Mobile = Convert.ToString(iDataReader["Mobile"]);
                _userDetails.IsActive = Convert.ToString(iDataReader["IsActive"]) == "1" ? true : false;
                _userDetails.Status = userDetails.IsActive ? "Yes" : "No";

            }
            iDataReader.Close();

        }
        catch (Exception ex)
        {
            _userDetails = null;
            throw ex;
        }
        finally
        {
            cn.Close();
        }
        return _userDetails;



    }
    public int UpdateProviderOrGSPWD(UserDetails userDetails)
    {

        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();

        cn.ConnectionString = ConnString;
        try
        {
            
            cmd.Connection = cn;
            cmd = new SqlCommand("sp_UpdateProviderOrGSPWD", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Provider", userDetails.Status));
            cmd.Parameters.Add(new SqlParameter("@RefernceNumber", userDetails.ReferenceNumber));
            cmd.Parameters.Add(new SqlParameter("@password", userDetails.Password));
            cmd.Parameters.Add(new SqlParameter("@HashPassword", BUPA_EncryptWithSha256.EncryptionManager.Encrypt(userDetails.Password)));
            cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
            cn.Open();
            cmd.ExecuteNonQuery();
            int contractID = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            return contractID;

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            cn.Close();
        }
       

    }



    public void SetActivation(UserDetails userDetails)
    {

        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConnString;         
        try
        {
            cn.Open();
            cmd.Connection = cn;
            cmd = new SqlCommand("sp_SetUserActivation", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@RefernceNumber", userDetails.ReferenceNumber));
            cmd.Parameters.Add(new SqlParameter("@IsActive", userDetails.IsActive));
            cmd.Parameters.Add(new SqlParameter("@Provider", userDetails.Status));
            cmd.Parameters.Add(new SqlParameter("@ModifiedBy", userDetails.UpdatedBy));
            cmd.ExecuteNonQuery();           
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            cn.Close();
        }
       

    }

    public void ActivateNewUser(UserDetails userDetails)
    {

        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConnString;
        try
        {
            cn.Open();
            cmd.Connection = cn;
            cmd = new SqlCommand("sp_ActivateNewUser", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@RefernceNumber", userDetails.ReferenceNumber));            
            cmd.Parameters.Add(new SqlParameter("@Provider", userDetails.Status));
            cmd.Parameters.Add(new SqlParameter("@Password", userDetails.Password));            
            cmd.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            cn.Close();
        }


    }

    public bool VerifyOtp(UserDetails userDetails)
    {
        bool isValid = false;
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConnString;
        try
        {
            cn.Open();
            cmd.Connection = cn;
            cmd = new SqlCommand("sp_VerifyUserOtp", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@RefernceNumber", userDetails.ReferenceNumber));
            cmd.Parameters.Add(new SqlParameter("@Provider", userDetails.Status));
            cmd.Parameters.Add(new SqlParameter("@Otp", userDetails.OTP));
            var refernce = cmd.ExecuteScalar();
            isValid = refernce != null ? true : false;
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            cn.Close();
        }
        return isValid;

    }
    //End
    #endregion


    #region Reason
    public IDataReader GetAllReasons()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "select * from Reason";
            IDataReader Idr;
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //cn.Close();
            return Idr;
        }
    #endregion

    #region Currency
        public IDataReader GetAllCurrencies()
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "select * from Currency";
            IDataReader Idr;
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }
    #endregion

        #region Nationality
        public IDataReader GetAllNationalities(bool IsGCC)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConnString;
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "select * from Nationality ";
            if (IsGCC) {
                cmd.CommandText += " where IsGCC=@IsGCC  order by Nationality";
                cmd.Parameters.AddWithValue("@IsGCC", IsGCC);
            }
            IDataReader Idr;
            Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Idr;
        }
        #endregion


}