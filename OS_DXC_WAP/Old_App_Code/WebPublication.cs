using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using log4net;
using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;
using Utility.Data;
using System.Configuration;

public class WebPublication
{

    private static readonly string Key_CaesarSvcUsername = "CaesarSvc Username";
    private static readonly string Key_CaesarSvcPassword = "CaesarSvc Password";

    // Configuration Keys
    public static readonly string Key_CaesarWSURL = "OS_DXC_WAP.CaesarWS.ServiceDepot_DN";

    // Configuration Keys - Database
    //public static readonly string Key_ConnBupa_OS = "bme_demoConnectionString";
    public static readonly string Key_ConnBupa_OS = "ConnBupa_OS";
    public static readonly string Key_DBConnectionTimeout = "DBConnectionTimeout";

    // Configuration Keys - Email Configuration
    public static readonly string Key_EmailRelayAddress = "EmailRelayAddress";
    public static readonly string Key_EmailRelayPort = "EmailRelayPort";

    // Session Keys
    public static readonly string Session_ClientUsername = "ClientUsername";
    public static readonly string Session_ClientFullName = "ClientName";

    private static ILog _log = LogManager.GetLogger(typeof(WebPublication));

    public static string CaesarSvcUsername
    {
        get
        {
            string caesarSvcUsername = Convert.ToString(ConfigurationManager.AppSettings[Key_CaesarSvcUsername]);
            try
            {
                string v = CoreConfiguration.Instance.GetConfigValue(Key_CaesarSvcUsername);
                caesarSvcUsername = v;
            }
            catch (Exception e)
            {
                throw e;
            }

            Tracer.WriteLine(ref _log, "caesarSvcUsername is: " + caesarSvcUsername);
            return caesarSvcUsername;
        }
    }

    public static string CaesarSvcPassword
    {
        get
        {
            string caesarSvcPassword = Convert.ToString(ConfigurationManager.AppSettings[Key_CaesarSvcPassword]);
            try
            {
                string v = CoreConfiguration.Instance.GetConfigValue(Key_CaesarSvcPassword);
                caesarSvcPassword = v;
            }
            catch (Exception e)
            {
                throw e;
            }

            Tracer.WriteLine(ref _log, "caesarSvcPassword is: " + caesarSvcPassword);
            return caesarSvcPassword;
        }
    }

    public static int DBConnectionTimeout()
    {
        Tracer.WriteMemberEntry(ref _log);

        int dbConnectionTimeout = 60000;
        try
        {
            string v = CoreConfiguration.Instance.GetConfigValue(Key_DBConnectionTimeout);
            dbConnectionTimeout = Int32.Parse(v);
        }
        catch (Exception e)
        {
            Tracer.WriteException(ref _log, e);
        }

        Tracer.WriteMemberExit(ref _log);
        return dbConnectionTimeout;
    }

    public static string EmailRelayAddress()
    {
        string emailRelayAddress = Convert.ToString(ConfigurationManager.AppSettings[Key_EmailRelayAddress]);
        try
        {
            string v = CoreConfiguration.Instance.GetConfigValue(Key_EmailRelayAddress);
            emailRelayAddress = v;
        }
        catch (Exception e)
        {
            throw e;
        }

        return emailRelayAddress;
    }

    public static int EmailRelayPort()
    {
        int emailRelayPort = 25;
        try
        {
            string v = CoreConfiguration.Instance.GetConfigValue(Key_EmailRelayPort);
            emailRelayPort = Int32.Parse(v);
        }
        catch (Exception e)
        {
            throw e;
        }
        return emailRelayPort;
    }

    //public static long GenerateTransactionID()
    //{
    //    Tracer.WriteMemberEntry(ref _log);

    //    long txnID;
    //    try
    //    {
    //        //System.Threading.Thread.Sleep(1000);
    //        //Random rnd = new Random();

    //        // TransactionID should be 10 digits in length
    //        //Random rand = new Random();
    //        //string unparsed = DateTime.Now.ToString("yyyyMMddHHmmssfffff") ;
    //        string unparsed = DateTime.Now.ToString("yyMMddHHmmssfff"); //+ rnd.Next(9999).ToString();
    //        Tracer.WriteLine(ref _log, "unparsed is: " + unparsed);

    //        txnID = long.Parse(unparsed);
    //    }
    //    catch (Exception e)
    //    {
    //        Tracer.WriteException(ref _log, e);
    //        throw;
    //    }

    //    Tracer.WriteMemberExit(ref _log);
    //    return txnID;
    //}

    public static long GenerateTransactionID()
    {
        Tracer.WriteMemberEntry(ref _log);

        long txnID;
        try
        {
            //System.Threading.Thread.Sleep(1000);
            //Random rnd = new Random();

            // TransactionID should be 10 digits in length
            //Random rand = new Random();
            //string unparsed = DateTime.Now.ToString("yyyyMMddHHmmssfffff") ;
            Random random = new Random(Guid.NewGuid().GetHashCode());
            int seed = unchecked(DateTime.Now.Ticks.GetHashCode());

            Random rnd = new Random();

            int intFirst = rnd.Next(1, 9);
            int intSecond = rnd.Next(0, 9);
            int intThird = rnd.Next(0, 9);
            int intLast = rnd.Next(0, 9);

            string unparsed = intFirst.ToString() + intSecond.ToString() + intThird.ToString() +
                DateTime.Now.ToString("MMddHHmmssf") + intLast.ToString(); //+ rnd.Next(9999).ToString();
            Tracer.WriteLine(ref _log, "unparsed is: " + unparsed);

            txnID = long.Parse(unparsed);
        }
        catch (Exception e)
        {
            Tracer.WriteException(ref _log, e);
            throw;
        }

        Tracer.WriteMemberExit(ref _log);
        return txnID;
    }

    public static long GenerateTransactionID2()
    {
        Tracer.WriteMemberEntry(ref _log);

        long txnID;
        try
        {
            //System.Threading.Thread.Sleep(1000);
            //Random rnd = new Random();

            // TransactionID should be 10 digits in length
            //Random rand = new Random();
            //string unparsed = DateTime.Now.ToString("yyyyMMddHHmmssfffff") ;
            Random random = new Random(Guid.NewGuid().GetHashCode());
            int seed = unchecked(DateTime.Now.Ticks.GetHashCode());

            Random rnd = new Random();

            int intFirst = rnd.Next(1, 9);
            int intSecond = rnd.Next(0, 9);
            int intThird = rnd.Next(0, 9);
            int intLast = rnd.Next(0, 9);

            string unparsed = DateTime.Now.ToString("MMddHHmmssf") + intFirst.ToString() + intSecond.ToString() + intThird.ToString() + intLast.ToString(); //+ rnd.Next(9999).ToString();
            Tracer.WriteLine(ref _log, "unparsed is: " + unparsed);

            txnID = long.Parse(unparsed);
        }
        catch (Exception e)
        {
            Tracer.WriteException(ref _log, e);
            throw;
        }

        Tracer.WriteMemberExit(ref _log);
        return txnID;
    }

    public static long GenerateTransactionID3()
    {
        Tracer.WriteMemberEntry(ref _log);

        long txnID;
        try
        {
            //System.Threading.Thread.Sleep(1000);
            //Random rnd = new Random();

            // TransactionID should be 10 digits in length
            //Random rand = new Random();
            //string unparsed = DateTime.Now.ToString("yyyyMMddHHmmssfffff") ;
            Random random = new Random(Guid.NewGuid().GetHashCode());
            int seed = unchecked(DateTime.Now.Ticks.GetHashCode());

            Random rnd = new Random();

            int intFirst = rnd.Next(1, 9);
            int intSecond = rnd.Next(0, 9);
            int intThird = rnd.Next(0, 9);
            int intLast = rnd.Next(0, 9);

            string unparsed = intFirst.ToString() + intSecond.ToString() + intThird.ToString() + intLast.ToString() + DateTime.Now.ToString("MMddHHmmssf") ; 
            Tracer.WriteLine(ref _log, "unparsed is: " + unparsed);

            txnID = long.Parse(unparsed);
        }
        catch (Exception e)
        {
            Tracer.WriteException(ref _log, e);
            throw;
        }

        Tracer.WriteMemberExit(ref _log);
        return txnID;
    }


    public static string GenerateUniqueID
    {
        get
        {
            Tracer.WriteMemberEntry(ref _log);

            string uniqueID;
            try
            {
                System.Threading.Thread.Sleep(1000);

                // TransactionID should be 10 digits in length
                string unparsed = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                Tracer.WriteLine(ref _log, "unparsed is: " + unparsed);

                uniqueID = unparsed;
            }
            catch (Exception e)
            {
                Tracer.WriteException(ref _log, e);
                throw;
            }

            Tracer.WriteMemberExit(ref _log);
            return uniqueID;
        }
    }

    public string DocLocation()
    {
        string _path = "";
        string strYear = System.DateTime.Today.Year.ToString();
        string strMonth = System.DateTime.Today.Month.ToString();
        string strDay = System.DateTime.Today.Day.ToString();

        _path =  strYear + @"\" + strMonth + @"\" + strDay ;
        return _path;
    } 
}