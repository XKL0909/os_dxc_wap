﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for BannedProvider
/// </summary>
public class BannedProvider
{
    private int _id = 0;
    private string _proid = string.Empty;

    public int ID
    {
        get { return _id; }
        set { _id = value; }
    }

    public string ProID
    {
        get { return _proid; }
        set { _proid = value; }
    }

    public BannedProvider(){}

    public BannedProvider(int ID, string ProID)
	{
        _id = ID;
        _proid = ProID;

	}

    public BannedProvider(string ProID)
    {
        _proid = ProID;

    }

    public bool isBanned() 
    {
        bool banned = false;

        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString;
        cn.Open();
        cmd.Connection = cn;
        cmd.CommandText = "Select * From BannedProvider where ProID=@ProID";
        cmd.Parameters.AddWithValue("ProID", this.ProID);
        IDataReader Idr;
        Idr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        if(Idr.Read())
        {
            banned = true;
        }
        Idr.Close();
        cn.Close();

        return banned;
    }

}