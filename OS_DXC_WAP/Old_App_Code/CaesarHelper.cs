﻿using System;
using System.Collections.Generic;
using System.Web;
using log4net;
using Bupa.Core.Logging;
using System.Text;
using System.Data;
using Bupa.Core.Logging;
using System.Web.UI.WebControls;
using Bupa.Core.Utilities.Database;
using Bupa.Core.Utilities.Configuration;
using OS_DXC_WAP.CaesarWS;

namespace Bupa.OSWeb.Business
{
    public sealed class CaesarHelper
    {
        #region Fields

        private static ServiceDepot_DNService _ws = new ServiceDepot_DNService();

        private string _UserName = string.Empty;
        private string _Password = string.Empty;
        private long _TransactionNumber = 0;

        #endregion

        #region Ctor


        public CaesarHelper()
        {
            Logger.Tracer.WriteMemberEntry();
            try
            {
                _UserName = WebPublication.CaesarSvcUsername;
                _Password = WebPublication.CaesarSvcPassword;
                _TransactionNumber = WebPublication.GenerateTransactionID3();
                _ws.Url = CoreConfiguration.Instance.GetConfigValue(WebPublication.Key_CaesarWSURL);
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
            }
            Logger.Tracer.WriteMemberExit();
        }

        #endregion

        #region Public Methods
        public void BindDdlMemberType(string groupType /*E for employee and D for Dependent*/, ref DropDownList ddl)
        {
            ListItem ListItem0 = new ListItem();
            AddDefaultItemInDropDownList(ref ddl);
           
            ReqMbrTypeListRequest_DN request = new ReqMbrTypeListRequest_DN();
            request.TransactionID = WebPublication.GenerateTransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.grp_Type = groupType;

            ReqMbrTypeListResponse_DN response;
            response = _ws.ReqMbrTypeList(request);

            ddl.DataSource = response.detail;
            ddl.DataBind();
        }
        public void BindDdlProfession(ref DropDownList ddl)
        {
            AddDefaultItemInDropDownList(ref ddl);
            ReqProfListRequest_DN request = new ReqProfListRequest_DN();
            request.transactionID = WebPublication.GenerateTransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            ReqProfListResponse_DN response;

            response = _ws.ReqProfList(request);

            ddl.DataSource = response.detail;
            ddl.DataBind();
        }
        public void BindDdlDistrict(ref DropDownList ddl)
        {
            AddDefaultItemInDropDownList(ref ddl);

            ReqDistListRequest_DN request = new ReqDistListRequest_DN();
            request.transactionID = WebPublication.GenerateTransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            ReqDistListResponse_DN response;

            response = _ws.ReqDistList(request);

            DistListDetail_DN[] MyDistricts = new DistListDetail_DN[response.detail.Length + 1];
            response.detail.CopyTo(MyDistricts, 1);
            MyDistricts[0] = new DistListDetail_DN();

            ddl.DataSource = MyDistricts;
            ddl.DataBind();
        }
        public void BindTitleDDL(ref DropDownList ddl)
        {
            ddl.Items.Clear();
            AddDefaultItemInDropDownList(ref ddl);
            ddl.Items.Add(new ListItem("Mr", "Mr"));
            ddl.Items.Add(new ListItem("Miss", "Miss"));
            ddl.Items.Add(new ListItem("Mrs", "Mrs"));
            ddl.Items.Add(new ListItem("Ms", "Ms"));
        }
        public void BindNationalities(bool IsGCC, ref DropDownList ddl)
        {
            AddDefaultItemInDropDownList(ref ddl);
            NationalityControl MyNationalityC = new NationalityControl();
            List<Nationality> MyNationalities = MyNationalityC.GetAllNationalities(IsGCC);
            ddl.DataValueField = "Code";
            ddl.DataTextField = "Nationality1";
            ddl.DataSource = MyNationalities;
            ddl.DataBind();
        }
        public string BindBranchList(string ClientID, ref DropDownList ddl)
        {
            ReqBrhListRequest_DN request = new ReqBrhListRequest_DN();
            ReqBrhListResponse_DN response;
            request.membershipNo = long.Parse(ClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();
            StringBuilder sb = new StringBuilder(200);

            try
            {
                response = _ws.ReqBrhList(request);
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                }
                else
                {
                    AddDefaultItemInDropDownList(ref ddl);
                    foreach (BrhListDetail_DN dtl in response.detail)
                    {
                        ListItem DepList = new ListItem();
                        DepList.Text = dtl.branchDesc;
                        DepList.Value = dtl.branchCode.ToString();
                        ddl.Items.Add(DepList);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sb.ToString();
        }
        public string BindClassList(string ClientID, ref DropDownList ddl)
        {
            
            StringBuilder sb = new StringBuilder(200);
            try
            {
                ReqClsListResponse_DN response = GetClassList(ClientID);
                ClsListDetail_DN[] classList = response.detail;
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                }
                else
                {
                    ddl.Items.Clear();
                    AddDefaultItemInDropDownList(ref ddl);
                    foreach (ClsListDetail_DN dtl in classList)
                    {
                        ListItem DepList = new ListItem();
                        DepList.Text = dtl.className;
                        DepList.Value = dtl.classID.ToString();
                        ddl.Items.Add(DepList);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sb.ToString();
        }
        public DataSet GetDependentsInCaesar(string membershipNumber)
        {
            DataSet dsResult = new DataSet();

            EnqMbrDepGetInfoRequest_DN request = new EnqMbrDepGetInfoRequest_DN();
            EnqMbrDepGetInfoResponse_DN response;

            request.membershipNo = membershipNumber;
            request.transactionID = WebPublication.GenerateTransactionID();
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = _ws.EnqMbrDepGetInfo(request);

            DataTable dt = MemberDependentParserForCaesar(response);

            dsResult.Tables.Add(dt);

            return dsResult;
        }
        public DataTable GetClassListDataSet(string ClientID)
        {

             DataTable dtResult = new DataTable();StringBuilder sb = new StringBuilder(200);
            try
            {
                ReqClsListResponse_DN response = GetClassList(ClientID);
                ClsListDetail_DN[] classList = response.detail;
                if (response.status != "0")
                {
                    sb.Append(response.errorMessage).Append("\n");
                }
                else
                {
                   
                    dtResult = DataManager.ConvertToDataTable(classList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtResult;
        }
        public MbrRejListResponse_DN GetRejectedRequestDetailsByTrackInfoRefNo(string strPassedRefNo, string strContractNo)
        {
            Logger.Tracer.WriteMemberEntry();

            MbrRejListRequest_DN request;
            MbrRejListResponse_DN response;

            string caesarReference = string.Empty;

            try
            {
                request = new MbrRejListRequest_DN();
                request.transactionID = _TransactionNumber;
                request.contNo = strContractNo;
                request.refNo = strPassedRefNo;
                request.Username = _UserName;
                request.Password = _Password;

                response = _ws.MbrRejList(request);

            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
                throw;
            }

            Logger.Tracer.WriteMemberExit();

            return response;
        }
        public string GetClassName(string ClientID, string ClassID)
        {
            string _strClassName = "";
            ReqClsListRequest_DN request = new ReqClsListRequest_DN();
            ReqClsListResponse_DN response;
            request.membershipNo = long.Parse(ClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = TransactionManager.TransactionID();
            try
            {
                response = _ws.ReqClsList(request);
                if (response.status != "0")
                {
                }
                else
                {
                    if (response.status == "0")
                    {
                        foreach (ClsListDetail_DN dtl in response.detail)
                        {
                            if (Convert.ToString(dtl.classID).Trim() == ClassID.Trim())
                            {
                                _strClassName = dtl.className;
                            }
                        }
                    }
                }
                return _strClassName;
            }
            catch (Exception ex)
            {
                return "No Class";
            }
        }
        #endregion


        #region Private functions
        private static ReqClsListResponse_DN GetClassList(string ClientID)
        {
            ReqClsListRequest_DN request = new ReqClsListRequest_DN();
            ReqClsListResponse_DN response;
            request.membershipNo = long.Parse(ClientID);
            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;
            request.transactionID = WebPublication.GenerateTransactionID();
            response = _ws.ReqClsList(request);
            return response;
        }
        private DataTable MemberDependentParserForCaesar(EnqMbrDepGetInfoResponse_DN response)
        {

            //Session["CaesarDepIds"] = null;
            MbrDetail_DN[] detail = response.detail;
            string errorID = response.errorID;
            string errorMessage = response.errorMessage;
            string status = response.status;
            long transactionID = response.transactionID;
            DataTable dtResult = new DataTable();

            List<string> depIqmaIdsCaesar = new List<string>();
            if (detail != null)
            {
                foreach (var item in detail)
                {
                    item.ClassID = GetClassName(item.membershipNo, item.ClassID);

                }
                dtResult = DataManager.ConvertToDataTable(detail);
            }
            //Session["CaesarDepIds"] = depIqmaIdsCaesar;
            return dtResult;
        }
        private static void AddDefaultItemInDropDownList(ref DropDownList ddl)
        {
            ddl.Items.Clear();
            ListItem DepList0 = new ListItem();
            DepList0.Text = "-- Select --";
            DepList0.Value = "0";
            ddl.Items.Add(DepList0);
        }
        #endregion
        
       
    }
}