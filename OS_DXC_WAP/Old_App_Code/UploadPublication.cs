using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;

using log4net;

using Bupa.Core.Logging;
using Utility.Configuration;
using Utility.Data;

namespace Bupa.OSWeb.Helper
{
    public struct UploadResult
    {
        public bool Result;
        public string DestinationPath;
        public StringCollection ErrorReason;
    }

    public class UploadCategory
    {
        public static readonly string PREAUTH_ADD = "PreAuth Addition";
        public static readonly string MEMBER_ADDEMP = "Membership Add Employee";
        public static readonly string MEMBER_ADDDEP = "Membership Add Dependent";
        public static readonly string MEMBER_CHGCLASS = "Membership Change Class";
        public static readonly string MEMBER_CHGBRANCH = "Membership Change Branch";
        public static readonly string MEMBER_REPCARD = "Membership Replace Card";
        public static readonly string MEMBER_REREQUEST = "Membership ReSubmit Rejected Request";
        public static readonly string Submit_Reimbursement = "Submit Reimbursement";
        public static readonly string Submit_eClaims = "Submit eClaims";
        public static readonly string PREAUTH_UPLOAD = "Preauth Upload";

        public static readonly string MEMBER_DELEMP = "Membership Delete Employee";
        public static readonly string MEMBER_DELDEP = "Membership Delete Dependent";

        public static readonly string MEMBER_BATCH_CARDREPLACE = "Membership Batch Card Replacement";
        public static readonly string MEMBER_BATCH_CHGCLASS = "Membership Batch Change Class";
        public static readonly string MEMBER_BATCH_CHGBRANCH = "Membership Batch Change Branch";
        public static readonly string MEMBER_BATCH_ADDDEP = "Membership Batch Add Dependent";
        public static readonly string MEMBER_BATCH_ADDEMPDEP = "Membership Batch Add Employee Dependent";
        public static readonly string MEMBER_BATCH_DELEMP = "Membership Batch Delete Employee";
        public static readonly string SUBMIT_REQUIRED_DOCS = "Submit Required Documents";


    }

    public class UploadPublication
    {
        private static ILog _log = LogManager.GetLogger(typeof(UploadPublication));

        public static readonly string Key_MEMBERSHIP_MaxFileUploadSize = "MEMBERSHIP_MaxFileUploadSize";
        public static readonly string Key_MEMBERSHIP_MaxFileUploadSizeeClaims = "MEMBERSHIP_MaxFileUploadSizeeClaims";
        public static readonly string Key_BatchMaxFileSetByteSize = "BatchMaxFileSetByteSize";
        public static readonly string Key_BatchMaxFileSetByteSizeeClaims = "BatchMaxFileSetByteSizeeClaims";


        public static readonly string Key_MaxFileSetByteSize = "MaxFileSetByteSize";
        public static readonly string Key_MaxFileSetByteSizeeClaims = "MaxFileSetByteSizeeClaims";
        public static readonly string Key_UploadAllowedExtensions = "UploadAllowedExtensions";
        public static readonly string Key_UploadAllowedExtensionseClaims = "UploadAllowedExtensionseClaims";
        public static readonly string Key_PREAUTH_EmailAddress = "PREAUTH_EmailAddress";
        public static readonly string Key_MEMBERSHIP_EmailAddresses = "MEMBERSHIP_EmailAddresses";
        public static readonly string Key_SUPPORT_DOCUMENTS_PATH = "SupportDocumentPath";

        public static readonly string Key_PREAUTHFOLDER_PATH = "PREAUTH_UploadDirectory";
        public static readonly string Key_PREAUTHDESTINATION_PATH = "PREAUTH_DestinationPath";
        public static readonly string Key_PROVIDERREGIDTRATIONDOC_PATH = "ProviderReg_DestinationPath";
        //
        // Query String Arguments
        public static readonly string QSKey_UploadCategory = "UploadCategory";
        public static readonly string QSKey_UserID = "UserID";
        public static readonly string QSKey_RequestID = "RequestID";
        public static readonly string QSKey_BatchUpload = "BatchUpload";


        public static readonly string spBupa_OSInsertUpload = "dbo.spBupa_OSInsertUpload";
        public static readonly string spBupa_OSInsertUploadPreauth = "dbo.spBupa_OSInsertUploadedPreauth";
        public static readonly string spBupa_OSInsertUploadSupportDocs = "dbo.spBupa_OSInsertUploadSupportDocs";
        public static readonly string spBupa_OSGetUploads = "dbo.spBupa_OSGetUploads";
        public static readonly string spBupa_OSGetUploadsPreauth = "dbo.spBupa_OSGetUploadsPreauth";
        public static readonly string spBupa_OSGetUploadSupportDocs = "dbo.spBupa_OSGetUploadsSupportDocs";
        public static readonly string spBupa_OSGetUploadSupportDocsSet = "dbo.spBupa_OSGetUploadsSupportDocsSet";
        public static readonly string spBupa_OSDeleteUpload = "dbo.spBupa_OSDeleteUpload";
        public static readonly string spBupa_OSDeleteUploadPreAuth = "dbo.spBupa_OSDeleteUploadedPreauth";
        public static readonly string spBupa_OSGetUploadFileSetSize = "dbo.spBupa_OSGetUploadFileSetSize";


        /////Added By Sakthi on 18-Feb-2016,  stroing file in common server and retriving from multiple server
        ////Start
        //// Online Reimbursment 
        public static readonly string Key_OnlineReimbursmentServerPath = "ONLINEREIMBURSMENT_SERVERPATH";
        public static readonly string Key_OnlineReimbursmentVirtualPath = "ONLINEREIMBURSMENT_VIRTUALPATH";

        //// Provider Registration
        public static readonly string Key_ProviderRegistrationServerPath = "PROVIDERREGISTRATION_SERVERPATH";
        public static readonly string Key_ProviderRegistrationVirtualPath = "PROVIDERREGISTRATION_VIRTUALPATH";


        public static readonly string Key_OnlineReimbursmentServerPathCommon = "FTPClaimServer";       
        public static readonly string Key_OnlineReimbursmentUserName = "FTPClaimUser";
        public static readonly string Key_OnlineReimbursmentUserCredential = "FTPClaimPassword";
        public static readonly string Key_OnlineReimbursmentDomain = "FTPServerDomain";        

        ////End 



        /*
        public static readonly string spBupa_OSInsertUpload = "dbo.spBupa_OSInsertUpload";
        public static readonly string spBupa_OSInsertUploadSupportDocs = "dbo.spBupa_OSInsertUploadSupportDocs";
        public static readonly string spBupa_OSGetUploads = "dbo.spBupa_OSGetUploads";
        public static readonly string spBupa_OSGetUploadSupportDocs = "dbo.spBupa_OSGetUploadsSupportDocs";
        public static readonly string spBupa_OSGetUploadSupportDocsSet = "dbo.spBupa_OSGetUploadsSupportDocsSet";
        public static readonly string spBupa_OSDeleteUpload = "dbo.spBupa_OSDeleteUpload";
        public static readonly string spBupa_OSGetUploadFileSetSize = "dbo.spBupa_OSGetUploadFileSetSize";
        */

        // Session Arguements
        public static readonly string Session_RequestID = "Session_RequestID";

        public static string[] PreAuthEmailAddresses()
        {
            string[] preAuthEmailAddresses = new string[] { };
            try
            {
                string v = CoreConfiguration.Instance.GetConfigValue(Key_PREAUTH_EmailAddress);
                preAuthEmailAddresses = v.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            }
            catch (Exception e)
            {
                throw e;
            }

            return preAuthEmailAddresses;
        }

        public static string[] MembershipEmailAddresses()
        {
            string[] membershipEmailAddresses = new string[] { };
            try
            {
                string v = CoreConfiguration.Instance.GetConfigValue(Key_MEMBERSHIP_EmailAddresses);
                membershipEmailAddresses = v.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            }
            catch (Exception e)
            {
                throw e;
            }

            return membershipEmailAddresses;
        }

        public static string AllowedExtensions()
        {
            string allowed = ".jpg, .jpeg, .gif, .png, .bmp";
            try
            {
                allowed = CoreConfiguration.Instance.GetConfigValue(Key_UploadAllowedExtensions);
            }
            catch
            {
                throw;
            }

            return allowed;
        }


    }
}