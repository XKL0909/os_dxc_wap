﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.InteropServices;
using System.Configuration;
using System.IO;
using System.Web.UI;


/// <summary>
/// Summary description for CheckMimeTypes
/// </summary>
public class CheckMimeTypes : System.Web.UI.Page
{
    [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
    private extern static System.UInt32 FindMimeFromData(System.UInt32 pBC,
     [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
     [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
     System.UInt32 cbSize, [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
     System.UInt32 dwMimeFlags,
     out System.UInt32 ppwzMimeOut,
     System.UInt32 dwReserverd);
    
	
    public string CheckFindMimeFromData(System.Web.HttpPostedFile file)
    {
       byte[] document = new byte[file.ContentLength];
       file.InputStream.Read(document, 0, file.ContentLength);
        System.UInt32 mimetype;
       FindMimeFromData(0, null, document, 256, null, 0, out mimetype, 0);
       System.IntPtr mimeTypePtr = new IntPtr(mimetype);
       string mime = Marshal.PtrToStringUni(mimeTypePtr);
       Marshal.FreeCoTaskMem(mimeTypePtr);
       return mime;
   }
    public string[] ReturnFileExtenstions(string getType)
    {
        string[] fileMime = { };
        if (getType == "AllFiles")
        {
            String filetypes = ConfigurationManager.AppSettings["AllowedAllMimeTypes"];
            fileMime = filetypes.Split(',');
        }
        else if (getType == "ExcelFile")
        {
            String filetypes = ConfigurationManager.AppSettings["AllowedClaimMimeTypes"];
            fileMime = filetypes.Split(',');
        }

        return fileMime;
    }

    public bool ValidateExcelFile(System.Web.HttpPostedFile file)
    {
        bool output = false;
        try 
        { 
                String fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                BinaryReader reader;
                if (fileExtension == ".xlsx")
                {
                    file.SaveAs(Server.MapPath("/backend/TempFiles/TestExcel.xlsx"));
                    reader = new BinaryReader(new FileStream(Convert.ToString(Server.MapPath("/backend/TempFiles/TestExcel.xlsx")), FileMode.Open, FileAccess.Read, FileShare.None));
                }
                else
                {
                    file.SaveAs(Server.MapPath("/backend/TempFiles/TestExcel.xls"));
                    reader = new BinaryReader(new FileStream(Convert.ToString(Server.MapPath("/backend/TempFiles/TestExcel.xls")), FileMode.Open, FileAccess.Read, FileShare.None));
                }
                reader.BaseStream.Position = 0x0; // The offset you are reading the data from  
                byte[] data = reader.ReadBytes(0x10); // Read 16 bytes into an array  
                string data_as_hex = BitConverter.ToString(data);
                reader.Close();
                string my = "";
                if (fileExtension == ".xlsx")
                {
                    System.IO.File.Delete(Server.MapPath("/backend/TempFiles/TestExcel.xlsx"));
                    my = data_as_hex.Substring(0, 26);
                }
                else
                {
                    System.IO.File.Delete(Server.MapPath("/backend/TempFiles/TestExcel.xls"));
                    my = data_as_hex.Substring(0, 23);
                }

                
                switch (my)
                {
                    case "D0-CF-11-E0-A1-B1-1A-E1": //xls
                        output = true;
                        break;
                    case "50-4B-03-04-14-00-06-00-08":   //xlsx
                        output = true;
                        break;
                    case "null":
                        output = false;
                        break;
                }
                
        }
        catch (Exception ex) { }
        return output;
    }

}