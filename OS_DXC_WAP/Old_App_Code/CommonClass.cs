﻿using Bupa.Core.Logging;
using OS_DXC_WAP.crmuat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;


namespace WebApp
{
    public static class CommonClass
    {
        public static void LogError(string ErrorType, Exception ex)
        {
            using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
            {
                try
                {
                    ErrorLog objErrorLog = new ErrorLog();
                    objErrorLog.EL_UserId = "Lokesh";//sessionmanager userid
                    objErrorLog.EL_ErrorType = ErrorType;
                    objErrorLog.EL_ErrorMessage = ex.Message;
                    objErrorLog.EL_StackTrace = ex.StackTrace.Substring(0, 990);
                    objErrorLog.EL_ActionTime = DateTime.Now;
                    objOnlineProvidersEntities.ErrorLogs.Add(objErrorLog);
                    objOnlineProvidersEntities.SaveChanges();
                }
                catch (Exception ex2)
                {


                }

            }
        }

        public static void LogError(string ErrorType, string ErrorMessage, Exception InnerException, string StackTrace,string UserId)
        {
            Logger.Tracer.WriteMemberEntry();
            try
            {
                using (OnlineServicesEntities objOnlineProvidersEntities = new OnlineServicesEntities())
                {
                    try
                    {
                        ErrorLog objErrorLog = new ErrorLog();
                        objErrorLog.EL_UserId = UserId;//sessionmanager userid
                        objErrorLog.EL_ErrorType = ErrorType;
                        objErrorLog.EL_ErrorMessage = ErrorMessage;
                        if (InnerException != null)
                        {
                            objErrorLog.EL_InnerException = InnerException.InnerException != null ? InnerException.InnerException.Message.ToString() : "";
                        }
                        if (StackTrace != null)
                        { 
                          if(StackTrace.Length>990)
                          {
                              objErrorLog.EL_StackTrace = StackTrace.Substring(0, 990);
                          }
                          else
                          {
                              objErrorLog.EL_StackTrace = StackTrace;

                          }
                        }
                        
                        objErrorLog.EL_ActionTime = DateTime.Now;
                        objOnlineProvidersEntities.ErrorLogs.Add(objErrorLog);
                        objOnlineProvidersEntities.SaveChanges();
                    }
                    catch (Exception ex2)
                    {
                        string strInnerException = ex2.InnerException.InnerException != null ? ex2.InnerException.InnerException.Message.ToString() : "";
                        LogApplicationError(ex2.Message, ex2.StackTrace + ";InnerException - " + strInnerException);

                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
                //throw;
            }
            Logger.Tracer.WriteMemberExit();

        }



        public static void LogApplicationError(string sErrMsg, string stacetrace)
        {
            //CreateLogFiles();
            try
            {
                string sPathName = System.Configuration.ConfigurationManager.AppSettings["ErrorLogPath"].ToString();

                string sErrorTime = DateTime.Now.ToString();
                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                StreamWriter sw = new StreamWriter(sPathName, true);
                sw.WriteLine(sLogFormat + "-- Error Mess: " + sErrMsg + " Stack :" + stacetrace);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static List<ProviderPhysiciansManagement> GetTrustedDoctorList(string ProviderId, string department)
        {
            List<ProviderPhysiciansManagement> objProviderPhysiciansManagementList = null;
            try
            {


                using (OnlineServicesEntities objTruestedDoctors = new OnlineServicesEntities())
                {
                    ProviderPhysicianMaster objProviderPhysicianMaster = objTruestedDoctors.ProviderPhysicianMasters.Where(item => item.PM_ProviderId.Trim() == ProviderId).SingleOrDefault();
                    if (objProviderPhysicianMaster != null && objProviderPhysicianMaster.PM_MaintenanceRequired == true && objProviderPhysicianMaster.PM_Status == true)
                    {
                        objProviderPhysiciansManagementList = objTruestedDoctors.ProviderPhysiciansManagements.Where(item => item.PPM_Department == department
                                                                                                                       && item.PPM_ProviderId.Trim() == ProviderId
                                                                                                                      && item.PPM_Status == true).OrderBy(item => item.PPM_PhysicianName).ToList();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return objProviderPhysiciansManagementList;
        }

        public static bool IsEnableSMERenewal(string contractID)
        {
            bool isEnable = false;
            try
            {
                if (!string.IsNullOrEmpty(contractID))
                {
                    List<string> lstPricingUploadByContractId = FindPricingUploadByContractId(contractID, GetSageSession());
                    isEnable = (lstPricingUploadByContractId != null && lstPricingUploadByContractId.Count > 0) ? true : false;
                }
            }
            catch(Exception ex)
            {
                ///throw;
            }

            return isEnable;    
               
        }

        private static OS_DXC_WAP.crmuat.WebService GetSageSession()
        {
            OS_DXC_WAP.crmuat.WebService binding = new OS_DXC_WAP.crmuat.WebService();
            try
            {

                OS_DXC_WAP.crmuat.logonresult SID = new OS_DXC_WAP.crmuat.logonresult();
                ///SID = binding.logon(ConfigurationManager.AppSettings["SageUsername"], ConfigurationManager.AppSettings["SagePassword"]);
                ////SID = binding.logon(ConfigurationManager.AppSettings["SageUsername"], ConfigurationManager.AppSettings["SagePassword"]);
                SID = binding.logon("ECom", "Bupa3C0m2013");
                binding.SessionHeaderValue = new OS_DXC_WAP.crmuat.SessionHeader();
                binding.SessionHeaderValue.sessionId = SID.sessionid;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }
            return binding;
        }
        public static List<string> FindPricingUploadByContractId(string strContractID, OS_DXC_WAP.crmuat.WebService binding)
        {
            List<string> result = new List<string>();
            OS_DXC_WAP.crmuat.WebService CRMService = binding;         

            queryrecordresult aresult = binding.queryrecord(@"prup_recordid", "prup_contractno like '%" + strContractID + "%' and  prup_status = 'Active' ", "pricingupload", "");
            ewarebase[] CRMBase = aresult.records;

            if (aresult.records != null)
                for (int intCount = 0; intCount < aresult.records.Count(); intCount++)
                {
                    string duplicateLeadIdNumber = aresult.records[intCount].records[0].value;
                    result.Add(duplicateLeadIdNumber);
                }
            result = result.Distinct().ToList();
            return result;
        }

        public static bool SendNotificationMail(string FromUser, string ToUser, string Subject, string BodyContent, string FilePath)
        {
            bool MailSent = false;
            try
            {
                MailMessage mMailMessage = new MailMessage();
                mMailMessage.From = new MailAddress(FromUser);
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["MailToUser"]) == true)
                {
                    string[] mailList = ToUser.Split(';');
                    foreach (string mailTo in mailList)
                    {
                        mMailMessage.To.Add(new MailAddress(mailTo));
                    }
                }
                else
                {
                    mMailMessage.To.Add(new MailAddress("lokesh.narayanappa@bupa.com.sa"));
                }
                mMailMessage.Subject = Subject.Trim();
                mMailMessage.Body = BodyContent;
                if (FilePath != "")
                {
                    mMailMessage.Attachments.Add(new Attachment(FilePath));
                }
                string smtpHost = WebPublication.EmailRelayAddress();
                int smtpPort = WebPublication.EmailRelayPort();

                SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
                mMailMessage.IsBodyHtml = true;

                smtp.Send(mMailMessage);
            }
            catch (Exception)
            {
                MailSent = false;
            }
            return MailSent;
        }
		
		public static void SetRediectionByPath()
        {
            var url = HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower();
            if (url.Contains("/client/"))
            {
                HttpContext.Current.Response.Redirect("../default.aspx");
            }
            else if (url.Contains("/provider/"))
            {
                HttpContext.Current.Response.Redirect("../default.aspx");
            }
            else
            {
                HttpContext.Current.Response.Redirect("default.aspx");
            }
        }



        ////Added By Sakthi on 03-Nov-2016 for Person Service revamp
        //// Start
        public static int YakeenValidationHours = Convert.ToInt32(ConfigurationManager.AppSettings["YakeenValidationHours"]);
        public static string FromSourceName = Convert.ToString(ConfigurationManager.AppSettings["FromSourceName"]);
        public static string FromDepartmentName = Convert.ToString(ConfigurationManager.AppSettings["FromDepartmentName"]);
        public static string FromUserName = Convert.ToString(HttpContext.Current.Session["ClientUsername"]);        
        ////End
       
 

    }
   
    public enum ProviderStatus
    {
        Submitted_For_Approval,
        Saved_As_Draft,
        Approved,
        Rejected,
        Cancelled,
        InProcess_With_Relations,
        InProcess_With_CommercialOperations
    }
}