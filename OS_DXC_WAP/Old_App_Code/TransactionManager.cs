using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;

public static class TransactionManager
{
    public static HttpContext Context;
    public static HttpSessionState Session;
    public static HttpRequest Request;
    public static HttpResponse Response;

    static public long TransactionID()
    {
        long txnID;
        try
        {
            byte[] buffer = Guid.NewGuid().ToByteArray();
            txnID = BitConverter.ToInt64(buffer, 0);
        }
        catch (Exception ex)
        {

            throw;
        }
        return txnID;

    }

   
}


