﻿using System;
using System.Collections.Generic;
using System.Web;

using log4net;
using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;
using System.Text;

namespace Bupa.Web.Business.Services
{
    public class MemberDetails : SvcReturnBase
    {
        #region Fields

        private static ILog _log = LogManager.GetLogger(typeof(MemberDetails));

        private string _memberName = string.Empty;
        private string _membershipNumber = string.Empty;
        private string _companyName = string.Empty;
        private string _saudiIqamaID = string.Empty;
        private DateTime _startDate = DateTime.MinValue;
        private DateTime _endDate = DateTime.MinValue;
        private string _gender = string.Empty;
        private DateTime _birthDate = DateTime.MinValue;
        private string _employeeNumber = string.Empty;
        private string _memberType = string.Empty;
        private string _sponsorID = string.Empty;

        private string _contractNumber = string.Empty;

        #endregion

        #region Properties

        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }

        public string MembershipNumber
        {
            get { return _membershipNumber; }
            set { _membershipNumber = value; }
        }

        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        public string SaudiIqamaID
        {
            get { return _saudiIqamaID; }
            set { _saudiIqamaID = value; }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        public string EmployeeNumber
        {
            get { return _employeeNumber; }
            set { _employeeNumber = value; }
        }

        public string MemberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }

        public string SponsorID
        {
            get { return _sponsorID; }
            set { _sponsorID = value; }
        }

        #endregion

        #region Ctor

        public MemberDetails(string memberID, string contractNumber)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
                _membershipNumber = memberID;
                Tracer.WriteLine(ref _log, "_membershipNumber is: " + _membershipNumber);

                _contractNumber = contractNumber;
                Tracer.WriteLine(ref _log, "_contractNumber is: " + _contractNumber);
               
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
            }
            Tracer.WriteMemberExit(ref _log);
        }

        #endregion
    }
}