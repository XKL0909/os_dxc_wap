﻿using System;
using System.Collections.Generic;
using System.Web;
using log4net;
using Bupa.Core.Logging;
using Utility.Configuration;
using System.Text;
using System.Data;
using Bupa.Core.Logging;
using System.IO;
using System.Configuration;

namespace Bupa.OSWeb.Business
{
    public sealed class MemberHelper
    {
        #region Fields

        private static ILog _log = LogManager.GetLogger(typeof(Member));
        private static OS_DXC_WAP.CaesarWS.ServiceDepot_DNService _ws = new OS_DXC_WAP.CaesarWS.ServiceDepot_DNService();

        private string _membershipNumber = string.Empty;
        private string _contractNumber = string.Empty;

        #endregion

        #region Ctor


        public MemberHelper(string membershipNumber, string contractNumber)
        {
            Logger.Tracer.WriteMemberEntry();
            try
            {
                _membershipNumber = membershipNumber;
                _contractNumber = contractNumber;

                _ws.Url = CoreConfiguration.Instance.GetConfigValue(WebPublication.Key_CaesarWSURL);
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
            }
            Logger.Tracer.WriteMemberExit();
        }

        #endregion

        #region Public Methods

        public Member GetMemberDetails()
        {
            Member m = new Member(_membershipNumber, _contractNumber);
            try
            {
                OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.EnqMbrDetInfoRequest_DN();
                request.membershipNo = m.MembershipNumber;
                request.transactionID = WebPublication.GenerateTransactionID();
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;
                OS_DXC_WAP.CaesarWS.EnqMbrDetInfoResponse_DN memberService = _ws.EnqMbrDetInfo(request);             

                if (memberService.errorID != "0")
                {
                    if (memberService.errorMessage.Trim() == "Member not active.")
                        throw new Exception("Invalid member number / " + memberService.errorMessage);
                    else
                    throw new Exception("Error when getting member details: " + memberService.errorMessage);
                }
                else
                {
                    m.ClaimPayToInd = memberService.detail[0].clmPayToInd;
                    //m.ClaimPayToInd = "M";

                    m.BankName = memberService.detail[0].bankName;
                    m.BankCountry = memberService.detail[0].bankCountry;
                    
                    m.MemberTypeDescription = memberService.detail[0].memberTypeDescription;
                    m.IVF_Prev_Pregnancy = memberService.detail[0].IVF_Pregnancy_IND;
                    m.MemberName = memberService.detail[0].memberName;
                    m.MembershipNumber = memberService.detail[0].membershipNo.ToString();
                    m.CompanyName = memberService.detail[0].companyName;
                    m.SaudiIqamaID = memberService.detail[0].memberID;
                    m.EndDate = (DateTime)memberService.detail[0].endDate;
                    m.Gender = memberService.detail[0].gender;
                    m.BirthDate = (DateTime)memberService.detail[0].memberDOB;
                    m.EmployeeNumber = memberService.detail[0].employeeNo;
                    m.MemberType = memberService.detail[0].memberTypeDescription;
                    m.StartDate = (DateTime)memberService.detail[0].startDate;
                    m.SponsorID = memberService.detail[0].sponsorID;
                    m.Status = memberService.detail[0].status;
                    m.Customer = memberService.detail[0].customerName;
                    m.ContractNumber = memberService.detail[0].custNo;
                    m.ClassID = memberService.detail[0].ClassID;
                    m.BranchCode = memberService.detail[0].BranchCode;
                    m.Nationality = memberService.detail[0].countryName;
                    m.VIP = memberService.detail[0].VIPCode;
                    m.MOBILE = memberService.detail[0].mobileNo;
                    m.SETNAME = memberService.detail[0].netSetName;
                    m.EmployeeEmail = memberService.detail[0].email;
                    m.EmployeeMobile = memberService.detail[0].mobileNo;
                    m.EmployeeIBAN = memberService.detail[0].IBAN;
                    m.PreauthOpt = memberService.detail[0].preauthOpt;

                    OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoRequest_DN request2 = new OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoRequest_DN();
                    request2.membershipNo = m.MembershipNumber;
                    request2.transactionID = WebPublication.GenerateTransactionID();
                    request2.Username = WebPublication.CaesarSvcUsername;
                    request2.Password = WebPublication.CaesarSvcPassword;
                    OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoResponse_DN memberService2 = _ws.ReqMbrCCHIInfo(request2);
                    if (memberService2.errorID != "0")
                    {
                        if (memberService2.errorMessage.Trim() == "Member not active")
                            throw new Exception("Invalid member number / " + memberService2.errorMessage);
                        else
                        throw new Exception("Error when getting member details: " + memberService2.errorMessage);
                    }
                    else
                    {
                        m.Nationality = memberService2.nationality;
                        m.IDType = memberService2.IDType;
                        m.IDExpiryDate = memberService2.IDExpDate;
                        m.Profession = memberService2.profession;
                        m.District = memberService2.district;
                        m.MaritalStatus = memberService2.maritalStatus;
                        m.MOBILE = memberService2.mobileNo;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
                throw ex;
            }

            Logger.Tracer.WriteMemberExit();
            return m;
        }

        public Member GetCCHIMemberDetails()
        {
            Logger.Tracer.WriteMemberEntry();
            Member m = new Member(_membershipNumber, _contractNumber);
            try
            {
                OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoRequest_DN request = new OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoRequest_DN();
                request.membershipNo = m.MembershipNumber;
                request.transactionID = WebPublication.GenerateTransactionID();
                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;

                OS_DXC_WAP.CaesarWS.ReqMbrCCHIInfoResponse_DN memberService = _ws.ReqMbrCCHIInfo(request);
                if (memberService.errorID != "0")
                {
                    throw new Exception("Error when getting member details: " + memberService.errorMessage);
                }
                else
                {
                    m.SaudiIqamaID = memberService.IDNumber;
                    m.Nationality = memberService.nationality;
                    m.IDType = memberService.IDType;
                    m.IDExpiryDate = memberService.IDExpDate;
                    m.Profession = memberService.profession;
                    m.District = memberService.district;
                }
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
                throw;
            }
            Logger.Tracer.WriteMemberExit();
            return m;
        }

        public string DeleteMember(Member m, string deleteReasonCode, DateTime effectiveDate, string[] _docs)
        {
            Logger.Tracer.WriteMemberEntry();

            string caesarReference = string.Empty;
            try
            {
                OS_DXC_WAP.CaesarWS.SubTxnRequest_DN request = new OS_DXC_WAP.CaesarWS.SubTxnRequest_DN();

                request.TransactionID = WebPublication.GenerateTransactionID();
                request.TransactionType = "DELETE MEMBER";
                request.BatchIndicator = "N";

                OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[] mbrdetailarry = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN[1];
                mbrdetailarry[0] = new OS_DXC_WAP.CaesarWS.AllMbrDetail_DN();
                mbrdetailarry[0].sql_type = "CSR.SUB_TXN_REC";
                mbrdetailarry[0].ContractNo = m.ContractNumber;
                mbrdetailarry[0].MembershipNo = m.MembershipNumber;
                mbrdetailarry[0].MemberDeleteDate = (DateTime)effectiveDate; //Member Termination Date
                mbrdetailarry[0].Reason = deleteReasonCode;
                mbrdetailarry[0].MbrName = m.MemberName;
                mbrdetailarry[0].SupportDoc = _docs;
                mbrdetailarry[0].CCHI_City_Code = m.District;
                mbrdetailarry[0].CCHI_Job_Code = m.Profession;
                mbrdetailarry[0].ID_Expiry_Date = m.IDExpiryDate;
                mbrdetailarry[0].ID_Type = m.IDType;
                mbrdetailarry[0].IgamaID = m.SaudiIqamaID;
                if (m.MaritalStatus != "0")
                {
                    mbrdetailarry[0].Marital_Status = m.MaritalStatus;
                }
                request.detail = mbrdetailarry;

                request.Username = WebPublication.CaesarSvcUsername;
                request.Password = WebPublication.CaesarSvcPassword;

                OS_DXC_WAP.CaesarWS.SubTxnResponse_DN deleteMemberService = _ws.SubTxn(request);
                if (deleteMemberService.Status != "0")
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < deleteMemberService.errorID.Length; i++)
                        sb.Append(deleteMemberService.errorMessage[i]).Append("<br/>");

                    throw new Exception("Error when deleting member: " + sb.ToString());
                }
                else
                {
                    // Deletion was successful
                    caesarReference = deleteMemberService.ReferenceNo.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
                throw;
            }
            ////Tracer.WriteLine(ref _log, "caesarReference is: " + caesarReference);

            Logger.Tracer.WriteMemberExit();
            return caesarReference;
        }



        #endregion
    }
}