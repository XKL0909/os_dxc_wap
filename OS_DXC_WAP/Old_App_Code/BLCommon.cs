﻿using System.Linq;
using System.Web;
using Bupa.OSWeb.Business;
using Bupa.OSWeb.Helper;
using OS_DXC_WAP.CaesarWS;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net.Mail;
using System.Reflection;
using System.Text;

/// <summary>
/// Summary description for BLCommon
/// </summary>
public class BLCommon
{
    public BLCommon()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private static SqlConnection GetConnection(string connectionString)
    {
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }

    public static void GenerateControl(String sServiceCode, out StringBuilder sbHTML, out StringBuilder csJScript)
    {


        string sHeader = sServiceCode;
        sServiceCode = sServiceCode.Substring(0, 3);

        string sql = "select OptionCode,OptionDescription,QID,Question,OptionInputType, SUBSTRING(OptionCode,0,4) ServiceCategory from preauthoption Op,PreAuthDiagnosisQuestions Dq  where OptionCode like  '" + sServiceCode + "%'  and Dq.QuestionID=Op.QID order by OptionCode, OptionDescription";
        DataTable dtCategoryOptions = GetData(sql);
        sql = " select distinct QuestionID,Question,IsMandatory from dbo.PreAuthDiagnosisQuestions Dq,preauthoption Op  where OptionCode like '" + sServiceCode + "%'  and Dq.QuestionID=Op.QID order by QuestionId ";
        DataTable dtQuestions = GetData(sql);
        StringBuilder sb = new StringBuilder();
        StringBuilder sbTr1 = new StringBuilder();
        StringBuilder sbTr2 = new StringBuilder();
        StringBuilder csText = new StringBuilder();
        string sColor = "snow";

        sb.Append("<center><p style='color: #013A61;font-weight: bold;align-content: center;color:white;'>" + sHeader + "</p></center><hr><table border=0 style='font-size: 11px;border-spacing:0px !important;background-color: snow;'>");
        csText.Append("<script type=\"text/javascript\"> function filldata() {");
        //csText.Append("function filldata() {");
        csText.Append("var selectedItem = '';");

        int iQid = 0, inQid = 0, mColSize = 10, iCol = 1;
        int i = 1;
        string sCntrlName = string.Empty;
        mColSize = Convert.ToInt32(ConfigurationManager.AppSettings["PreAuthFormColomns"].ToString());
        if (mColSize < 1) mColSize = 10;

        DataView dv;
        DataTable dtMaxTableCol = GetData("select MAX(col) as mCol from  ( select pa.qid, count(*) as col  from   PreAuthOption pa  where optioncode like  '" + sServiceCode + "%'  group  by pa.qid) as colcount;");

        foreach (DataRow drq in dtQuestions.Rows) // Loop over the rows.
        {
            sbTr1.Clear();
            sbTr2.Clear();

            if (sColor == "#BFEBFB")
                sColor = "snow";
            else
                sColor = "#BFEBFB";

            sbTr1.Append("<tr style='background-color:" + sColor + ";'>");
            if (!string.IsNullOrEmpty(drq["IsMandatory"].ToString()) && drq["IsMandatory"].ToString() == "True")
            {

                sbTr1.Append("<td style='width:175px' id='td" + drq["QuestionId"] + "' ><b>" + (i++) + ". " + drq["Question"] + "<font style=\"color: red;\">&nbsp;*");
                sbTr1.Append("<asp:RequiredFieldValidator ID=\"rfv" + sCntrlName + "\" runat=\"server\" ControlToValidate=\"" + sCntrlName + "\" ErrorMessage=\"*\" ForeColor=\"red\" SetFocusOnError=\"true\"></asp:RequiredFieldValidator>");
                sbTr1.Append("</font></b></td>");
            }
            else
            {
                sbTr1.Append("<td style='width:175px'  id='td" + drq["QuestionId"] + "' ><b>" + (i++) + ". " + drq["Question"] + "</b></td>");

            }

            sbTr2.Append("<tr style='background-color:" + sColor + ";'>");
            sbTr2.Append("<td style='width:175px'  ><b>" + "&nbsp;" + "</b></td>");

            dv = dtCategoryOptions.DefaultView;
            dv.RowFilter = "QID =" + drq["QuestionId"];
            iCol = 1;

            foreach (DataRowView drv in dv) // Loop over the rows.
            {

                inQid = Convert.ToInt32(drv["QID"].ToString());

                if (drv["OptionInputType"].ToString() == "Radio" || drv["OptionInputType"].ToString() == "checkbox")
                {
                    if (!string.IsNullOrEmpty(drq["IsMandatory"].ToString()) && drq["IsMandatory"].ToString() == "True" && iCol == 1)
                    {
                        //if ("BRO001,NEO001".Contains(drv["OptionCode"].ToString()) && (PreAuthOpt == "F" || PreAuthOpt == "N"))
                        if ("BRO001,NEO001".Contains(drv["OptionCode"].ToString()))
                        {
                            sbTr1.Append("<td style='' ><center><input type='" + drv["OptionInputType"] + "'   ID='" + drv["OptionCode"] + "' Name='" + drv["QID"] + "' runat=\"server\" Text='" + drv["OptionCode"] + "' value='" + drv["OptionCode"] + "'  checked /> " + "" + "</center></td>");
                            sbTr2.Append("<td style='vertical-align: top;' ><center> " + drv["OptionDescription"] + "</center></td>");
                            sCntrlName = drv["QID"].ToString();
                        }
                        else
                        {
                            sbTr1.Append("<td style='' ><center><input type='" + drv["OptionInputType"] + "'   ID='" + drv["OptionCode"] + "' Name='" + drv["QID"] + "' runat=\"server\" Text='" + drv["OptionCode"] + "' value='" + drv["OptionCode"] + "'   /> " + "" + "</center></td>");
                            sbTr2.Append("<td style='vertical-align: top;' ><center> " + drv["OptionDescription"] + "</center></td>");
                            sCntrlName = drv["QID"].ToString();
                        }
                    }
                    else
                    {
                        sbTr1.Append("<td style='' ><center><input type='" + drv["OptionInputType"] + "'   ID='" + drv["OptionCode"] + "' Name='" + drv["QID"] + "' runat=\"server\" Text='" + drv["OptionCode"] + "' value='" + drv["OptionCode"] + "' /> " + "" + "</center></td>");
                        sbTr2.Append("<td style='vertical-align: top;' ><center> " + drv["OptionDescription"] + "</center></td>");
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(drq["IsMandatory"].ToString()) && drq["IsMandatory"].ToString() == "True" && iCol == 1)
                    {
                        sbTr1.Append("<td style='' ><center><input type='" + drv["OptionInputType"] + "' size=3  maxlength=3 onkeypress=\"return isNumber(event)\"   ID='" + drv["OptionCode"] + "' Name='" + drv["QID"] + "' runat=\"server\" Text='" + drv["OptionCode"] + "' value='' /> " + "" + "</center></td>");
                        sbTr2.Append("<td style='vertical-align: top;' ><center> " + drv["OptionDescription"] + "</center></td>");
                        sCntrlName = drv["OptionCode"].ToString();
                    }
                    else
                    {
                        sbTr1.Append("<td style='' ><center><input type='" + drv["OptionInputType"] + "' size=3  maxlength=3 onkeypress=\"return isNumber(event)\"   ID='" + drv["OptionCode"] + "' Name='" + drv["QID"] + "' runat=\"server\" Text='" + drv["OptionCode"] + "' value='' /> " + "" + "</center></td>");
                        sbTr2.Append("<td style='vertical-align: top;' ><center> " + drv["OptionDescription"] + "</center></td>");
                    }

                }

                if (iQid != inQid)
                {
                    if (drv["OptionInputType"].ToString() == "Radio")
                    {
                        csText.Append("var radioValue" + drv["OptionCode"] + " = $(\"input[name='" + drv["QID"] + "']:checked\").val();");
                        csText.Append("if (radioValue" + drv["OptionCode"] + ") {");
                        csText.Append("selectedItem += \"& " + drv["QID"] + "= \" + radioValue" + drv["OptionCode"] + ";");
                        csText.Append("}");

                    }
                    else if (drv["OptionInputType"].ToString() == "checkbox")
                    {
                        csText.Append("var checkboxValue" + drv["OptionCode"] + " =''; ");
                        csText.Append("$(\"input[name='" + drv["QID"] + "']:checked\").each(function() {   checkboxValue" + drv["OptionCode"] + "+= '@' + this.value;});");
                        csText.Append("selectedItem += \"& " + drv["QID"] + "= \" + checkboxValue" + drv["OptionCode"] + ";");


                    }
                    else
                    {

                        csText.Append("var TextValue" + drv["OptionCode"] + " = $(\"#" + drv["OptionCode"] + "\").val();");
                        csText.Append("selectedItem += \"& " + drv["QID"] + "@" + drv["OptionCode"] + "= \" + TextValue" + drv["OptionCode"] + " ;");
                    }

                    iQid = inQid;
                }

                if (iCol >= mColSize)
                {
                    sbTr1.Append("</tr>");
                    sbTr2.Append("</tr>");
                    sb.Append(sbTr1.ToString());
                    sb.Append(sbTr2.ToString());

                    sbTr1.Clear();
                    sbTr2.Clear();

                    sbTr1.Append("<tr style='background-color:" + sColor + ";'><td  style='' >&nbsp;</td>");
                    sbTr2.Append("<tr style='background-color:" + sColor + ";'><td  style='' >&nbsp;</td>");

                    iCol = 1;
                }
                iCol++;

            }

            for (iCol--; iCol <= mColSize; iCol++)
            {

                sbTr1.Append("<td style='' > " + "" + "</td>");
                sbTr2.Append("<td style='' > " + "" + "</td>");

            }

            sbTr1.Append("</tr>");
            sbTr2.Append("</tr>");
            sb.Append(sbTr1.ToString());
            sb.Append(sbTr2.ToString());
            sb.Append("<tr><td colspan='" + (mColSize + 1) + "'><hr /></td></tr>");

            if (PreAuthOpt == "F")
            {
                break;
            }
            else if (PreAuthOpt == "N")
            {

                break;
            }
        }

        sb.Append("</table>");
        sb.Append("<input type=\"hidden\" id=\"OptionSelectedValue\" runat=\"server\">");
        //dvServiceCategoryOptions.InnerHtml = sb.ToString();
        sbHTML = sb;
        csText.Append(" $(\"#OptionSelectedValue\").val(selectedItem);");
        csText.Append(" $(\"#txtHiddenOptions\").val(selectedItem);");
        //csText.Append("alert(selectedItem);");
        //csText.Append("}");
        csText.Append("}</script>");
        csJScript = csText;
        //if (!cs.IsClientScriptBlockRegistered(csType, csName))
        //{
        //    cs.RegisterClientScriptBlock(csType, csName, csText.ToString());
        //}


    }

    public static void GenerateControlScript(String sServiceCode, out StringBuilder sbHTML, out StringBuilder csJScript)
    {
        string sHeader = sServiceCode;
        sServiceCode = sServiceCode.Substring(0, 3);

        string sql = "select OptionCode,OptionDescription,QID,Question,OptionInputType, SUBSTRING(OptionCode,0,4) ServiceCategory from preauthoption Op,PreAuthDiagnosisQuestions Dq  where OptionCode like  '" + sServiceCode + "%'  and Dq.QuestionID=Op.QID order by OptionCode, OptionDescription";
        DataTable dtCategoryOptions = GetData(sql);
        sql = " select distinct QuestionID,Question,IsMandatory from dbo.PreAuthDiagnosisQuestions Dq,preauthoption Op  where OptionCode like '" + sServiceCode + "%'  and Dq.QuestionID=Op.QID order by QuestionId ";
        DataTable dtQuestions = GetData(sql);
        StringBuilder sb = new StringBuilder();
        StringBuilder csText = new StringBuilder();

        sb.Append("<center><p style='color: #013A61;font-weight: bold;align-content: center;'>" + sHeader + "</p></center><hr><table border=0 style='font-size: 11px;'>");
        //csText.Append("<script type=\"text/javascript\"> function filldata() {");
        csText.Append("function " + sServiceCode + "() {");
        csText.Append("var selectedItem = '';");

        int iQid = 0, inQid = 0;
        int i = 1;
        DataView dv;

        foreach (DataRow drq in dtQuestions.Rows) // Loop over the rows.
        {
            sb.Append("<tr style='border: 1px solid #333333;'>");
            sb.Append("<td style='border-bottom: 1px solid #333333;'  ><b>" + (i++) + "</b></td>");
            sb.Append("<td style='border-bottom: 1px solid #333333;'  ><b>" + drq["Question"] + "</b></td>");



            dv = dtCategoryOptions.DefaultView;
            dv.RowFilter = "QID =" + drq["QuestionId"];

            foreach (DataRowView drv in dv) // Loop over the rows.
            {
                inQid = Convert.ToInt32(drv["QID"].ToString());

                if (drv["OptionInputType"].ToString() == "Radio" || drv["OptionInputType"].ToString() == "checkbox")
                {
                    sb.Append("<td style='border-bottom: 1px solid #333333;' ><input type='" + drv["OptionInputType"] + "'   ID='" + drv["OptionCode"] + "' Name='" + drv["QID"] + "' runat=\"server\" Text='" + drv["OptionCode"] + "' value='" + drv["OptionCode"] + "' /> " + drv["OptionDescription"] + "</td>");
                }
                else
                {
                    sb.Append("<td style='border-bottom: 1px solid #333333;' ><input type='" + drv["OptionInputType"] + "'  size=3   ID='" + drv["OptionCode"] + "' Name='" + drv["QID"] + "' runat=\"server\" Text='" + drv["OptionCode"] + "' value='' /> " + drv["OptionDescription"] + "</td>");
                }

                if (iQid != inQid)
                {
                    if (drv["OptionInputType"].ToString() == "Radio")
                    {
                        csText.Append("var radioValue" + drv["OptionCode"] + " = $(\"input[name='" + drv["QID"] + "']:checked\").val();");
                        csText.Append("if (radioValue" + drv["OptionCode"] + ") {");
                        csText.Append("selectedItem += \"& " + drv["QID"] + "= \" + radioValue" + drv["OptionCode"] + ";");
                        csText.Append("}");

                    }
                    else if (drv["OptionInputType"].ToString() == "checkbox")
                    {
                        csText.Append("var checkboxValue" + drv["OptionCode"] + " =''; ");
                        csText.Append("$(\"input[name='" + drv["QID"] + "']:checked\").each(function() {   checkboxValue" + drv["OptionCode"] + "+= '@' + this.value;});");
                        csText.Append("selectedItem += \"& " + drv["QID"] + "= \" + checkboxValue" + drv["OptionCode"] + ";");


                    }
                    else
                    {

                        csText.Append("var TextValue" + drv["OptionCode"] + " = $(\"#" + drv["OptionCode"] + "\").val();");
                        csText.Append("selectedItem += \"& " + drv["QID"] + "@" + drv["OptionCode"] + "= \" + TextValue" + drv["OptionCode"] + " ;");
                    }

                    iQid = inQid;
                }


            }

            sb.Append("</tr>");

            if (PreAuthOpt == "F")
            {
                break;
            }
            else if (PreAuthOpt == "N")
            {

                break;
            }
        }

        sb.Append("</table>");
        sb.Append("<input type=\"hidden\" id=\"OptionSelectedValue\" runat=\"server\">");
        //dvServiceCategoryOptions.InnerHtml = sb.ToString();
        sbHTML = sb;
        csText.Append(" $(\"#OptionSelectedValue\").val(selectedItem);");
        csText.Append(" $(\"#txtHiddenOptions\").val(selectedItem);");

        //csText.Append(" alert(selectedItem);");

        csText.Append(" var bStat = 0; var res = selectedItem.split(\"&\"); for (i = 0; i < res.length; i++) {  if (res[i].trim().split(\"=\").length == 2 && res[i].trim().split(\"=\")[1] != \"\") {bStat = 1;}  } ");
        csText.Append(" var qCount = " + dtQuestions.Select("IsMandatory = True").Length + ";");

        csText.Append(" var tempC=''; ");
        foreach (DataRow drq in dtQuestions.Rows) // Loop over the rows.
        {

            csText.Append("   tempC=\"#td" + drq["QuestionId"] + "\";");
            csText.Append("  $(tempC).css('color', 'red'); ");
        }


        //csText.Append(" var ValCount = 0;  for (i = 0; i < res.length; i++)  { if(res[i].trim()!=\"\"){  var Val = res[i].split('=')[1];  if (Val.trim() != \"\") { ValCount++; }  }  }");
        csText.Append(" var tempF=''; var ValCount = 0;  for (i = 0; i < res.length; i++)  { if(res[i].trim()!=\"\"){  var Val = res[i].split('=')[1];  if (Val.trim() != \"\") { ValCount++; ");
        csText.Append("   if(res[i].split('=')[0].trim().split('@').length>0){   tempF=\"#td\" + res[i].split('=')[0].trim().split('@')[0].trim();    }else ");
        csText.Append("   tempF=\"#td\" + res[i].split('=')[0].trim(); ");
        csText.Append("  $(tempF).css('color', 'green'); ");
        csText.Append(" }  }  }");


        csText.Append(" if( $(\"#dvPreAuthOpt\").text().indexOf(\"No Pre\") > -1)  {   qCount=1;  } ");
        csText.Append(" if (qCount > ValCount  &&  \"" + sServiceCode + "\"!=\"DLS\" ) {");
        csText.Append("  $(\"#dvErrorMessage\").html(' You have answered only <b>' + ValCount + '</b> mandatory questions, out of <b>' + qCount +'</b>.<br /> Please answer the remaining mandatory questions to proceed.');   $(\"#dvErrorMessage\").css('display','block');  ");
        csText.Append(" return false;}else{   $(\"#dvErrorMessage\").css('display','block');  $(\"#dvErrorMessage\").html(' Please check above mandatory fields, if you have missed Quantitiy or Estimated Cost. '); } ");
        csText.Append("}");
        csJScript = csText;



    }
    public static Boolean SendEmail(MailMessage email)
    {
        Boolean bStatus = true;
        try
        {

            email.IsBodyHtml = true;
            email.BodyEncoding = Encoding.UTF8;

            string smtpHost = WebPublication.EmailRelayAddress();  // "AppRelay.Bupame.com"
            int smtpPort = WebPublication.EmailRelayPort();    // 25

            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);

            System.Net.NetworkCredential cred = new System.Net.NetworkCredential();

            cred.UserName = Convert.ToString(ConfigurationManager.AppSettings["EmailUserName"]);
            cred.Password = Convert.ToString(ConfigurationManager.AppSettings["EmailUserPassword"]);

            //cred.Domain = "mail.myhost.com";
            smtp.Credentials = cred;

            //ServicePointManager.ServerCertificateValidationCallback =     delegate(object s, X509Certificate certificate,   X509Chain chain, SslPolicyErrors sslPolicyErrors)     { return true; };

            //smtp.UseDefaultCredentials = false;
            //smtp.EnableSsl = false;

            smtp.Send(email);
        }
        catch (Exception ex)
        {
            bStatus = false;
        }
        return bStatus;
    }
    public static DataTable GetData(string query)
    {
        DataTable table = new DataTable();

        SqlConnection connection = null;
        DataSet dsCategories = null;
        try
        {
            connection = GetConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);

            dsCategories = SqlHelper.ExecuteDataset(connection, CommandType.Text, query);

            table = dsCategories.Tables[0];

            connection.Close();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            connection.Close();
        }


        return table;
    }

    public static void UpdateClientLoginStatus(int cRefNO, bool loginStatus)
    {
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@CRefNo", SqlDbType.Int);
                arParms[0].Value = cRefNO;
                arParms[1] = new SqlParameter("@IsLoggedIn", SqlDbType.Bit);
                arParms[1].Value = loginStatus;
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "UpdateClientLoginStatus", arParms);
            }
        }
        catch (Exception ex)
        {

        }
    }

    public static void UpdateProviderLoginStatus(int cRefNO, bool loginStatus)
    {
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@pRefNo", SqlDbType.Int);
                arParms[0].Value = cRefNO;
                arParms[1] = new SqlParameter("@IsLoggedIn", SqlDbType.Bit);
                arParms[1].Value = loginStatus;
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "UpdateProviderLoginStatus", arParms);
            }
        }
        catch (Exception ex)
        {

        }
    }

    
 public static int InsertClientLogIn(string clientUserName)
    {
        int currentID = 0;
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@ClientUserName", SqlDbType.NVarChar);
                arParms[0].Value = clientUserName;
                arParms[1] = new SqlParameter("@CurrentID", SqlDbType.Int);
                arParms[1].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "InsertClientLoginTime", arParms);
                currentID = Convert.ToInt32(arParms[1].Value);
            }
        }
        catch (Exception ex)
        {

        }
        return currentID;
    }

 public static int InsertProviderLogIn(string proUserName)
    {
        int currentID = 0;
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@pRefNo", SqlDbType.NVarChar,50);
                arParms[0].Value = proUserName;
                arParms[1] = new SqlParameter("@CurrentID", SqlDbType.Int);
                arParms[1].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "InsertProviderLoginTime", arParms);
                currentID = Convert.ToInt32(arParms[1].Value);
            }
        }
        catch (Exception ex)
        {

        }
        return currentID;
    }
	
	
 
 

    public static void UpdateClientLogOff(int currentID)
    {
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[1];
                arParms[0] = new SqlParameter("@CurrentID", SqlDbType.Int);
                arParms[0].Value = currentID;
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "UpdateClientLogOffTime", arParms);
            }
        }
        catch (Exception ex)
        {

        }
    }

    public static void UpdateProviderLogOffTime(int currentID)
    {
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[1];
                arParms[0] = new SqlParameter("@CurrentID", SqlDbType.Int);
                arParms[0].Value = currentID;
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "UpdateProviderLogOffTime", arParms);
            }
        }
        catch (Exception ex)
        {

        }
    }
public static void UpdateConcurClientLogOff(string clientUserName,int RefID)
    {
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@ClientUserName", SqlDbType.NVarChar);
                arParms[0].Value = clientUserName;
                arParms[1] = new SqlParameter("@ReferID", SqlDbType.Int);
                arParms[1].Value = RefID;
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "UpdateConcurClientLogOff", arParms);
            }
        }
        catch (Exception ex)
        {

        }
    }

 
    public static void UpdateConcurProviderLogOffTime(string proUserName, int RefID)
    {
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@ProUserName", SqlDbType.NVarChar);
                arParms[0].Value = proUserName;
                arParms[1] = new SqlParameter("@ReferID", SqlDbType.Int);
                arParms[1].Value = RefID;
                SqlHelper.ExecuteNonQuery(sqlCon, CommandType.StoredProcedure, "UpdateConcurProviderLogOff", arParms);
            }
        }
        catch (Exception ex)
        {

        }
    }
   
    //
    public static string GetConcurrentUserLogOffStatus(int currentID, string source)
    {
        string logOffStatus = string.Empty;
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString()))
            {
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@CUrrentId", SqlDbType.Int);
                arParms[0].Value = currentID;
                arParms[1] = new SqlParameter("@Source", SqlDbType.VarChar);
                arParms[1].Value = source;
                logOffStatus = (SqlHelper.ExecuteScalar(sqlCon, CommandType.StoredProcedure, "GetConcurrentUserLogOffStatus", arParms).ToString());
            }
        }
        catch(Exception ex)
        {
            logOffStatus = ex.Message;
        }
        return logOffStatus;
    }

    public static string PreAuthOpt = string.Empty;
	  public static bool HRMEnabled
    {
        get
        {

            return Convert.ToBoolean(HttpContext.Current.Session["HRMEnabled"]);
        }
        set
        {

            //if (HttpContext.Current.Session["HRMEnabled"] == null)
            HttpContext.Current.Session["HRMEnabled"] = value;
        }
    }
    public static bool PreAuthAllowed
    {
        get
        {

            return Convert.ToBoolean(HttpContext.Current.Session["PreAuthAllowed"]);
        }
        set
        {

            //if (HttpContext.Current.Session["PreAuthAllowed"] == null)
            HttpContext.Current.Session["PreAuthAllowed"] = value;
        }
    }
    public static DataTable BupaCodeLists
    {
        get
        {
            return (DataTable)HttpContext.Current.Session["BupaCodeLists"];
        }
        set
        {
            HttpContext.Current.Session["BupaCodeLists"] = value;
        }
    }
public static List<BupaCodeListDetail_DN> BupaCodeListDetail
    {
        get
        {
            return (List<BupaCodeListDetail_DN>)HttpContext.Current.Session["BupaCodeListDetail"];
        }
        set
        {
            HttpContext.Current.Session["BupaCodeListDetail"] = value;
        }
    }
	
	public static string ReturnMemberShipNumberForStaffnumber(string Staffnumber,string Customerno)
	{
		string MemberORStaff = "";
        ServiceDepot_DNService ws = new ServiceDepot_DNService();
		EnqMbrListInfoRequest_DN request = new EnqMbrListInfoRequest_DN();
		EnqMbrListInfoResponse_DN response = new EnqMbrListInfoResponse_DN();
		response = null;
		request = new EnqMbrListInfoRequest_DN();
		request.customerNo = Customerno;
		request.staffNo = Staffnumber;
		request.transactionID = long.Parse(String.Format("{0}{1}{2}{3}{4}{5}{6}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));
		request.Username = WebPublication.CaesarSvcUsername;
		request.Password = WebPublication.CaesarSvcPassword;
		response = ws.EnqMbrListInfo(request);
		StringBuilder stringBuilder = new StringBuilder(200);
		if (response.errorID != "0")
		{
			MemberORStaff = "Error :" + response.errorMessage;
		}
		else
			MemberORStaff = Convert.ToString(response.detail[0].membershipNo);

		return MemberORStaff;
	}

    public static int InsertSMS(string receiverMobile, string message, string source)
    {
        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ToString();
        try
        {
            cmd.Connection = cn;
            cmd = new SqlCommand("InsertSMS", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Receiver", receiverMobile));
            cmd.Parameters.Add(new SqlParameter("@Message", message));
            cmd.Parameters.Add(new SqlParameter("@Source", source));
            cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
            cn.Open();
            cmd.ExecuteNonQuery();
            int result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            cn.Close();
        }
    }
	
	public static string GenerateRandomCode()
    {
        Random r = new Random();
        string s = "";
        for (int j = 0; j < 5; j++)
        {
            int i = r.Next(3);
            int ch;
            switch (i)
            {
                case 1:
                    ch = r.Next(0, 9);
                    s = s + ch.ToString();
                    break;
                case 2:
                    ch = r.Next(65, 90);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
                case 3:
                    ch = r.Next(97, 122);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
                default:
                    ch = r.Next(97, 122);
                    s = s + Convert.ToChar(ch).ToString();
                    break;
            }
            r.NextDouble();
            r.Next(100, 1999);
        }
        return s;
    }
}