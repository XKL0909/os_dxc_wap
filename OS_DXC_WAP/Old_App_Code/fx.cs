using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace extensions
{
    public static class fx
    {
        private const string SecretSalt = "H3#@*ALMLLlk31q4l1ncL#@...";

        public static string Encrypt(this String str)
        {
            string key = null;
            if (System.Configuration.ConfigurationManager.AppSettings["SecretSalt"] != null && System.Configuration.ConfigurationManager.AppSettings["SecretSalt"].Length > 0)
            {
                key = System.Configuration.ConfigurationManager.AppSettings["SecretSalt"];
            }
            else
            {
                key = SecretSalt;
            }
            string Encrypted = string.Empty;
            string input = string.Concat(key, str, key);
            byte[] hashedDataBytes = null;
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
            hashedDataBytes = md5Hasher.ComputeHash(encoder.GetBytes(input));
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            byte outputByte = 0;
            foreach (byte outputByte_loopVariable in hashedDataBytes)
            {
                outputByte = outputByte_loopVariable;
                sb.Append(outputByte.ToString("x2").ToUpper());
            }
            Encrypted = sb.ToString();
            return Encrypted;
        }

        public static bool ValidateEncryption(this String Encrypted, string str)
        {
            if (Encrypted == null)
            {
                return false;
            }
            else
            {
                string.Compare("", "");
                if (string.Compare(str.Encrypt(), Encrypted.ToString()) != 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}