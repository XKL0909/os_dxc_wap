using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using log4net;
using Tracer = Utility.Diagnostic.FileTrace;

using Utility.Data;
using Utility.Configuration;
using System.Data.SqlClient;
using System.Collections.Specialized;
using OS_DXC_WAP.CaesarWS;

namespace Bupa.OSWeb.Helper
{
    public class UploadManager
    {
        private static ILog _log = LogManager.GetLogger(typeof(UploadManager));

        private string _connection = string.Empty;
        private int _dbTimeout = 60000;
        private long _maxFilesetSize;
        private long _fileSizeLimit;
        private static ServiceDepot_DNService _ws = new ServiceDepot_DNService();
        public UploadManager(long maxFilesetSize, long fileSizeLimit)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
                _connection = CoreConfiguration.Instance.GetConnValue(WebPublication.Key_ConnBupa_OS);
                Tracer.WriteLine(ref _log, "_connection is: " + _connection);

                _dbTimeout = WebPublication.DBConnectionTimeout();
                Tracer.WriteLine(ref _log, "_dbTimeout is: " + _dbTimeout);

                _maxFilesetSize = maxFilesetSize;
                Tracer.WriteLine(ref _log, "_maxFilesetSize is: " + _maxFilesetSize);

                _fileSizeLimit = fileSizeLimit;
                Tracer.WriteLine(ref _log, "_fileSizeLimit is: " + _fileSizeLimit);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }
            Tracer.WriteMemberExit(ref _log);
        }

        public static string GetNextRequest()
        {
            Tracer.WriteMemberEntry(ref _log);

            string requestID = string.Empty;
            try
            {
                int year = DateTime.Now.Year;
                int day = DateTime.Now.Day;
                int month = DateTime.Now.Month;
                int hour = DateTime.Now.Hour;
                int minute = DateTime.Now.Minute;
                int second = DateTime.Now.Second;
                int millisecond = DateTime.Now.Millisecond;

                requestID = year.ToString() + month.ToString() + day.ToString() + hour.ToString() + minute.ToString() + second.ToString() + millisecond.ToString();
                Tracer.WriteLine(ref _log, "requestID is: " + requestID);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }
            Tracer.WriteLine(ref _log, "requestID is: " + requestID);

            Tracer.WriteMemberExit(ref _log);
            return requestID;
        }

        public bool IsBelowFileSizeLimit(string sourcePath, long fileSizeLimit)
        {
            Tracer.WriteMemberEntry(ref _log);

            bool success = false;
            try
            {
                FileInfo fi = new FileInfo(sourcePath);
                long size = fi.Length;
                Tracer.WriteLine(ref _log, "size is: " + size);

                if (size > fileSizeLimit)
                {
                    Tracer.WriteLine(ref _log, "File: " + sourcePath + " exceeds the individual file limit. Size is: " + size + " - limit is: " + fileSizeLimit);
                }
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }
            Tracer.WriteMemberExit(ref _log);
            return success;
        }

        public UploadResult Add(string sessionID, string uploadCategory, string sourcePath, string virtualPath, string friendlyFileName, string userID)
        {
            Tracer.WriteMemberEntry(ref _log);

            UploadResult uploaded = new UploadResult();
            uploaded.ErrorReason = new StringCollection();

            try
            {
                uploaded.Result = false;

                // Check whether the file can be added
                FileInfo fi = new FileInfo(sourcePath);
                string extractedFileName = fi.Name;
                Tracer.WriteLine(ref _log, "extractedFileName is: " + extractedFileName);

                // Get the size of the file in bytes
                long fileByteSize = fi.Length;
                Tracer.WriteLine(ref _log, "fileByteSize is: " + fileByteSize);

                // Check the individual upload size 
                if (fileByteSize > _fileSizeLimit)
                {
                    // The file uploaded exceeds the individual file size limit allowed
                    Tracer.WriteLine(ref _log, "File: " + sourcePath + " exceeds the individual file limit. Size is: " + fileByteSize + " - limit is: " + _fileSizeLimit);

                    string fileSizeMB = (fileByteSize / 1048576).ToString();
                    Tracer.WriteLine(ref _log, "fileByteSize is: " + fileByteSize);

                    string fileSizeLimitMB = (_fileSizeLimit / 1048576).ToString();
                    Tracer.WriteLine(ref _log, "fileSizeLimitMB is: " + fileSizeLimitMB);

                    uploaded.Result = false;
                    uploaded.ErrorReason.Add("This file exceeds the total file size allowed to be uploaded. Total allowed size is: " + fileSizeLimitMB + "MB. File size is: " + fileSizeMB + "MB.");
                }
                else
                {
                    // Get the total uploaded size
                    long currentFileSetByteSize = GetFileSetByteSize(sessionID, uploadCategory);
                    Tracer.WriteLine(ref _log, "currentFileSetByteSize is: " + currentFileSetByteSize);

                    long expectedFileByteSize = fileByteSize + currentFileSetByteSize;
                    Tracer.WriteLine(ref _log, "expectedFileByteSize is: " + expectedFileByteSize);

                    if (expectedFileByteSize > _maxFilesetSize)
                    {
                        // The total files uploaded + new file uploaded exceeds the fileset size limit
                        Tracer.WriteLine(ref _log, "Cannot commit this upload as it exceeds the upload size set");
                        Remove(sourcePath);

                        string fileSetSizeLimitMB = (_maxFilesetSize / 1048576).ToString();
                        Tracer.WriteLine(ref _log, "fileSetSizeLimitMB is: " + fileSetSizeLimitMB);

                        string uploadedSoFar = (currentFileSetByteSize / 1048576).ToString();
                        Tracer.WriteLine(ref _log, "uploadedSoFar is: " + uploadedSoFar);

                        uploaded.Result = false;
                        uploaded.ErrorReason.Add("This file exceeds the total file size allowed to be uploaded. Total allowed size is: " + fileSetSizeLimitMB + "MB. Total uploaded so far: " + uploadedSoFar + "MB.");
                    }
                    else
                    {
                        // Continue committing the file to the database
                        DataParameter dpSessionID = new DataParameter(System.Data.SqlDbType.VarChar, "sessionID", sessionID, System.Data.ParameterDirection.Input);
                        DataParameter dpUploadedSourceID = new DataParameter(System.Data.SqlDbType.VarChar, "uploadedSourceID", uploadCategory, System.Data.ParameterDirection.Input);
                        DataParameter dpDocName = new DataParameter(System.Data.SqlDbType.NVarChar, "docName", extractedFileName, System.Data.ParameterDirection.Input);
                        DataParameter dpFriendlyDocName = new DataParameter(System.Data.SqlDbType.NVarChar, "friendlyDocName", friendlyFileName, System.Data.ParameterDirection.Input);
                        DataParameter dpFilePath = new DataParameter(System.Data.SqlDbType.NVarChar, "filePath", sourcePath, System.Data.ParameterDirection.Input);
                        DataParameter dpVirtualPath = new DataParameter(System.Data.SqlDbType.NVarChar, "virtualPath", virtualPath, System.Data.ParameterDirection.Input);
                        DataParameter dpFileSize = new DataParameter(System.Data.SqlDbType.Decimal, "fileSize", fileByteSize, System.Data.ParameterDirection.Input);
                        DataParameter dpUserID = new DataParameter(System.Data.SqlDbType.VarChar, "userID", userID, System.Data.ParameterDirection.Input);

                        DataParameter[] dpBasket = new DataParameter[] { dpSessionID, dpUploadedSourceID, dpDocName, dpFriendlyDocName, dpFilePath, dpVirtualPath, dpFileSize, dpUserID };
                        DataManager.ExecuteStoredProcedureNonQuery(_connection, UploadPublication.spBupa_OSInsertUpload, ref dpBasket, _dbTimeout);

                        // Set the file size for the fileset
                        currentFileSetByteSize = expectedFileByteSize;
                        Tracer.WriteLine(ref _log, "_currentFileSetByteSize after upload is: " + currentFileSetByteSize);

                        uploaded.Result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }
            Tracer.WriteMemberExit(ref _log);
            return uploaded;
        }



        public UploadResult AddPreauthDocs(string sessionID, string uploadCategory, string sourcePath, string virtualPath, string friendlyFileName, string userID)
        {
            Tracer.WriteMemberEntry(ref _log);

            UploadResult uploaded = new UploadResult();
            uploaded.ErrorReason = new StringCollection();

            try
            {
                uploaded.Result = false;

                // Check whether the file can be added
                FileInfo fi = new FileInfo(sourcePath);
                string extractedFileName = fi.Name;
                Tracer.WriteLine(ref _log, "extractedFileName is: " + extractedFileName);

                // Get the size of the file in bytes
                long fileByteSize = fi.Length;
                Tracer.WriteLine(ref _log, "fileByteSize is: " + fileByteSize);

                // Check the individual upload size 
                if (fileByteSize > _fileSizeLimit)
                {
                    // The file uploaded exceeds the individual file size limit allowed
                    Tracer.WriteLine(ref _log, "File: " + sourcePath + " exceeds the individual file limit. Size is: " + fileByteSize + " - limit is: " + _fileSizeLimit);

                    string fileSizeMB = (fileByteSize / 1048576).ToString();
                    Tracer.WriteLine(ref _log, "fileByteSize is: " + fileByteSize);

                    string fileSizeLimitMB = (_fileSizeLimit / 1048576).ToString();
                    Tracer.WriteLine(ref _log, "fileSizeLimitMB is: " + fileSizeLimitMB);

                    uploaded.Result = false;
                    uploaded.ErrorReason.Add("This file exceeds the total file size allowed to be uploaded. Total allowed size is: " + fileSizeLimitMB + "MB. File size is: " + fileSizeMB + "MB.");
                }
                else
                {
                    // Get the total uploaded size
                    long currentFileSetByteSize = GetFileSetByteSize(sessionID, uploadCategory);
                    Tracer.WriteLine(ref _log, "currentFileSetByteSize is: " + currentFileSetByteSize);

                    long expectedFileByteSize = fileByteSize + currentFileSetByteSize;
                    Tracer.WriteLine(ref _log, "expectedFileByteSize is: " + expectedFileByteSize);

                    if (expectedFileByteSize > _maxFilesetSize)
                    {
                        // The total files uploaded + new file uploaded exceeds the fileset size limit
                        Tracer.WriteLine(ref _log, "Cannot commit this upload as it exceeds the upload size set");
                        Remove(sourcePath);

                        string fileSetSizeLimitMB = (_maxFilesetSize / 1048576).ToString();
                        Tracer.WriteLine(ref _log, "fileSetSizeLimitMB is: " + fileSetSizeLimitMB);

                        string uploadedSoFar = (currentFileSetByteSize / 1048576).ToString();
                        Tracer.WriteLine(ref _log, "uploadedSoFar is: " + uploadedSoFar);

                        uploaded.Result = false;
                        uploaded.ErrorReason.Add("This file exceeds the total file size allowed to be uploaded. Total allowed size is: " + fileSetSizeLimitMB + "MB. Total uploaded so far: " + uploadedSoFar + "MB.");
                    }
                    else
                    {
                        // Continue committing the file to the database
                        DataParameter dpSessionID = new DataParameter(System.Data.SqlDbType.VarChar, "sessionID", sessionID, System.Data.ParameterDirection.Input);
                        DataParameter dpUploadedSourceID = new DataParameter(System.Data.SqlDbType.VarChar, "uploadedSourceID", uploadCategory, System.Data.ParameterDirection.Input);
                        DataParameter dpDocName = new DataParameter(System.Data.SqlDbType.NVarChar, "docName", extractedFileName, System.Data.ParameterDirection.Input);
                        DataParameter dpFriendlyDocName = new DataParameter(System.Data.SqlDbType.NVarChar, "friendlyDocName", friendlyFileName, System.Data.ParameterDirection.Input);
                        DataParameter dpFilePath = new DataParameter(System.Data.SqlDbType.NVarChar, "filePath", sourcePath, System.Data.ParameterDirection.Input);
                        DataParameter dpVirtualPath = new DataParameter(System.Data.SqlDbType.NVarChar, "virtualPath", virtualPath, System.Data.ParameterDirection.Input);
                        DataParameter dpFileSize = new DataParameter(System.Data.SqlDbType.Decimal, "fileSize", fileByteSize, System.Data.ParameterDirection.Input);
                        DataParameter dpUserID = new DataParameter(System.Data.SqlDbType.VarChar, "userID", userID, System.Data.ParameterDirection.Input);

                        DataParameter[] dpBasket = new DataParameter[] { dpSessionID, dpUploadedSourceID, dpDocName, dpFriendlyDocName, dpFilePath, dpVirtualPath, dpFileSize, dpUserID };
                        DataManager.ExecuteStoredProcedureNonQuery(_connection, UploadPublication.spBupa_OSInsertUploadPreauth, ref dpBasket, _dbTimeout);

                        // Set the file size for the fileset
                        currentFileSetByteSize = expectedFileByteSize;
                        Tracer.WriteLine(ref _log, "_currentFileSetByteSize after upload is: " + currentFileSetByteSize);

                        uploaded.Result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }
            Tracer.WriteMemberExit(ref _log);
            return uploaded;
        }


        public void AddSupportDocs(string sessionID, string friendlyFileName, string userID, string Refnum, string Seq)
        {
            Tracer.WriteMemberEntry(ref _log);

            UploadResult uploaded = new UploadResult();
            uploaded.ErrorReason = new StringCollection();

            try
            {
                uploaded.Result = false;

                        DataParameter dpSessionID = new DataParameter(System.Data.SqlDbType.VarChar, "sessionID", sessionID, System.Data.ParameterDirection.Input);

                        DataParameter dpDocName = new DataParameter(System.Data.SqlDbType.NVarChar, "docName", friendlyFileName, System.Data.ParameterDirection.Input);
                        DataParameter dpUserID = new DataParameter(System.Data.SqlDbType.VarChar, "userID", userID, System.Data.ParameterDirection.Input);
                        DataParameter dprefNum = new DataParameter(System.Data.SqlDbType.VarChar, "refnum", Refnum, System.Data.ParameterDirection.Input);
                        DataParameter dpSeq = new DataParameter(System.Data.SqlDbType.VarChar, "seq", Seq, System.Data.ParameterDirection.Input);

                        DataParameter[] dpBasket = new DataParameter[] { dpSessionID,  dpDocName, dpUserID , dprefNum,dpSeq};
                        DataManager.ExecuteStoredProcedureNonQuery(_connection, UploadPublication.spBupa_OSInsertUploadSupportDocs, ref dpBasket, _dbTimeout);


                       
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }
            Tracer.WriteMemberExit(ref _log);
           
        }

        public DataSet GetUploadedFileSet(string sessionID, string uploadCategory)
        {
            Tracer.WriteMemberEntry(ref _log);

            DataSet fileSet = new DataSet();
            try
            {
                // Get the list of files committed to the upload
                // Selection should be based on the sessionID and the upload category
                // Continue committing the file to the database
                DataParameter dpSessionID = new DataParameter(System.Data.SqlDbType.VarChar, "sessionID", sessionID, System.Data.ParameterDirection.Input);
                DataParameter dpUploadedSourceID = new DataParameter(System.Data.SqlDbType.VarChar, "uploadedSourceID", uploadCategory, System.Data.ParameterDirection.Input);

                DataParameter[] dpBasket = new DataParameter[] { dpSessionID, dpUploadedSourceID };
                fileSet = DataManager.ExecuteStoredProcedureCachedReturn(_connection, UploadPublication.spBupa_OSGetUploads, ref dpBasket, _dbTimeout);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }

            Tracer.WriteMemberExit(ref _log);
            return fileSet;
        }


        public DataSet GetUploadedPreauthFileSet(string sessionID, string uploadCategory)
        {
            Tracer.WriteMemberEntry(ref _log);

            DataSet fileSet = new DataSet();
            try
            {
                // Get the list of files committed to the upload
                // Selection should be based on the sessionID and the upload category
                // Continue committing the file to the database
                DataParameter dpSessionID = new DataParameter(System.Data.SqlDbType.VarChar, "sessionID", sessionID, System.Data.ParameterDirection.Input);
                DataParameter dpUploadedSourceID = new DataParameter(System.Data.SqlDbType.VarChar, "uploadedSourceID", uploadCategory, System.Data.ParameterDirection.Input);

                DataParameter[] dpBasket = new DataParameter[] { dpSessionID, dpUploadedSourceID };
                fileSet = DataManager.ExecuteStoredProcedureCachedReturn(_connection, UploadPublication.spBupa_OSGetUploadsPreauth, ref dpBasket, _dbTimeout);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }

            Tracer.WriteMemberExit(ref _log);
            return fileSet;
        }


        public void Remove(int uploadedID, string sourcePath)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
                // Remove the file from the uploaded area records
                DataParameter dpUploadedID = new DataParameter(System.Data.SqlDbType.Int, "uploadedID", uploadedID, System.Data.ParameterDirection.Input);

                DataParameter[] dpBasket = new DataParameter[] { dpUploadedID };
                DataManager.ExecuteStoredProcedureNonQuery(_connection, UploadPublication.spBupa_OSDeleteUpload, ref dpBasket, _dbTimeout);

                // Delete the file from the server
                Remove(sourcePath);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
            }
            Tracer.WriteMemberExit(ref _log);
        }

        public void RemovePreAuth(int uploadedID, string sourcePath)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
                // Remove the file from the uploaded area records
                DataParameter dpUploadedID = new DataParameter(System.Data.SqlDbType.Int, "uploadedID", uploadedID, System.Data.ParameterDirection.Input);

                DataParameter[] dpBasket = new DataParameter[] { dpUploadedID };
                DataManager.ExecuteStoredProcedureNonQuery(_connection, UploadPublication.spBupa_OSDeleteUploadPreAuth, ref dpBasket, _dbTimeout);

                // Delete the file from the server
                Remove(sourcePath);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
            }
            Tracer.WriteMemberExit(ref _log);
        }

        private void Remove(string sourcePath)
        {
            Tracer.WriteMemberEntry(ref _log);
            try
            {
                // Removes the file due to size limitations
                File.Delete(sourcePath);
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
            }
            Tracer.WriteMemberExit(ref _log);
        }

        private long GetFileSetByteSize(string sessionID, string uploadCategory)
        {
            Tracer.WriteMemberEntry(ref _log);

            long filesetSize = 0;
            try
            {
                DataParameter dpSessionID = new DataParameter(System.Data.SqlDbType.VarChar, "sessionID", sessionID, System.Data.ParameterDirection.Input);
                DataParameter dpUploadedSourceID = new DataParameter(System.Data.SqlDbType.VarChar, "uploadedSourceID", uploadCategory, System.Data.ParameterDirection.Input);

                DataParameter[] dpBasket = new DataParameter[] { dpSessionID, dpUploadedSourceID };
                DataSet result = DataManager.ExecuteStoredProcedureCachedReturn(_connection, UploadPublication.spBupa_OSGetUploadFileSetSize, ref dpBasket, _dbTimeout);

                if (result != null && result.Tables.Count > 0)
                    if (result.Tables[0].Rows.Count > 0)
                    {
                        filesetSize = long.Parse(result.Tables[0].Rows[0]["FileSize"].ToString());
                    }
            }
            catch (Exception ex)
            {
                Tracer.WriteException(ref _log, ex);
                throw;
            }

            Tracer.WriteMemberExit(ref _log);
            return filesetSize;
        }

        public bool WebIndecator(string _type, string _contract)
        {
            bool _mandatory = false;
            OS_DXC_WAP.CaesarWS.RequestWebIndicatorsRequest_DN request = new OS_DXC_WAP.CaesarWS.RequestWebIndicatorsRequest_DN();
            OS_DXC_WAP.CaesarWS.RequestWebIndicatorsResponse_DN response = new OS_DXC_WAP.CaesarWS.RequestWebIndicatorsResponse_DN();

            request.contractNo = _contract;
            request.transactionID = WebPublication.GenerateTransactionID();

            request.Username = WebPublication.CaesarSvcUsername;
            request.Password = WebPublication.CaesarSvcPassword;

            response = _ws.RequestWebIndicators(request);
            if (response.status != "0")
            {
              HttpContext.Current.Response.Write("Error when getting details: " + response.errorMessage[0]);
                
            }
            else
            {
                switch (_type)
                {
                    case "WebSupDocInd":
                        if (response.webSupDocInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;
                    case "StaffNoInd":
                        if (response.staffNoInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;
                    case "DeptCodeInd":
                        if (response.deptCodeInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;
                    case "DepdIdInd":
                        if (response.depdIdInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;
                    case "SuportDocs":
                      if (response.webSupDocInd.Trim() == "Y")
                        {
                            _mandatory = true;
                        }
                        else
                        {
                            _mandatory = false;
                        }
                        break;

                }
                
            }
            return _mandatory;
        }
       
    }
    
}