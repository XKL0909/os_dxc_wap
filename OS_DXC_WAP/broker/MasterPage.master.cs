﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class broker_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string loggedInAs = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])) || !string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])) ||
            !string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])) ||!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["MembershipNo"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["MemberName"]) + " (" + Convert.ToString(Session["MembershipNo"]) + ")";
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["ClientID"])))
            {

                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ClientName"]) + " (" + Convert.ToString(Session["ClientUsername"]) + ")";
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["ProviderID"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["ProviderName"]) + " (" + Convert.ToString(Session["ProviderUserName"]) + ")";
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
            {
                lblLoginName.Text = "Welcome! You are logged in as " + Convert.ToString(Session["BrokerName"]) + " (" + Convert.ToString(Session["BrokerUserName"]) + ")";
            }
        }
        else
        {
            if (GetCurrentPageName() != "Registration.aspx" && GetCurrentPageName() != "forgotpassword.aspx")
            {
                Response.Redirect("../default.aspx");
                //logouttag.Visible = false;
                //logintag.Visible = true;
            }
            else
            {
                //backdiv.Visible = false;
            }
        }

    }

    public string GetCurrentPageName()
    {
        string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }

    protected void hlLogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();

        Response.Redirect("../default.aspx");
    }
  
}
