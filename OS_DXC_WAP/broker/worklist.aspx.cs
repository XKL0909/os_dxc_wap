﻿
using OS_DXC_WAP.crmuat;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class broker_worklist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        WebService binding = GetSageSession();
        if (Session["BrokerName"] != null)
            GetFamilyLeadsBySource(binding, string.IsNullOrWhiteSpace(Session["BrokerName"].ToString()) ? "bupa_website" : Session["BrokerName"].ToString());
        else
            Response.Redirect("~/Default.aspx");
        // GetFamilyLeadsBySource(binding, "bupa_website" );
    }

    private static WebService GetSageSession()
    {
        WebService binding = new WebService();
        binding.Url = ConfigurationManager.AppSettings["crmuat.WebServices"];
        string webServiceURL = binding.Url ;

        try
        {

            logonresult SID = new logonresult();
            SID = binding.logon(ConfigurationManager.AppSettings["SageUsername"], ConfigurationManager.AppSettings["SagePassword"]);
            binding.SessionHeaderValue = new SessionHeader();
            binding.SessionHeaderValue.sessionId = SID.sessionid;
        }
        catch (Exception)
        {

            throw;
        }
        return binding;
    }

    void GetFamilyLeadsBySource(WebService binding, string sourceOfLead) // source can be like bupa web site, NCB etc..
    {
        lead lead = new lead();
        lead.source = sourceOfLead;

        ewarebase[] leadList = new ewarebase[1];
        leadList[0] = lead;
        //var CRM = Server.CreateObject("eWare.eWareSelfService");
        //record = eWare.FindRecord("lead", "lead_source='"+sourceOfLead+"'");
        //queryresult qr = binding.query("select top 1 * from lead", "lead");

        //ORIGINAL
        //queryrecordresult aresult = binding.queryrecord("lead_opportunityid,lead_personsalutation,lead_personarabicsalutation,lead_personfirstname,lead_personlastname,lead_personarabicfirstname,lead_personarabiclastname,lead_phonenumbermobile,lead_status,lead_numberoflives,lead_addresscityid,lead_regdate", "lead_source='" + sourceOfLead + "'", "lead", "lead_leadid");
        string secondFilter = "NCB Incoming";
        queryrecordresult aresult = binding.queryrecord(@"lead_opportunityid,lead_personcardid IDNumber,lead_personsalutation + lead_personfirstname  +  lead_personlastname Name,
lead_personmobilecountrycode + lead_personmobilenumber Mobile,lead_status Status,lead_numberoflives Members,lead_addresscityid City,lead_createddate [Create date], lead_source Source", "lead_source like '%" + sourceOfLead + "%'", "lead", "lead_leadid");
        
        DataTable dtLead = SageCrmRecordArrayToDataTable(aresult);

        DataTable dtOppo = new DataTable();
        DataTable dtScheme = new DataTable();
        DataTable dtResult = new DataTable();

        ParseSageResultDataTable(binding, dtLead, ref dtOppo, ref dtScheme);
        dtResult = JoinDataTables(dtLead, dtOppo, "opportunityid", "oppId");
        dtResult = JoinDataTables(dtResult, dtScheme, "opportunityid", "oppoId");

        DataSet ds = new DataSet();
        ds.Tables.Add(dtResult);
        if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
        {
            RadGrid1.DataSource = ds;
            RadGrid1.DataBind();
        }
        else
        {
            pnlBtnExcel.Enabled = false;
        }
    }

    private static void ParseSageResultDataTable(WebService binding, DataTable dt, ref DataTable dtOppo, ref DataTable dtScheme)
    {
        if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
            foreach (DataRow row in dt.Rows)
            {
                if (!string.IsNullOrWhiteSpace(row["opportunityid"].ToString()))
                {
                    queryrecordresult qrr = GetFamilyOpportunityByLead_Oppo_Id(binding, row["opportunityid"].ToString());
                    if (qrr.records != null && qrr.records.Count() > 0)
                        dtOppo = SageCrmRecordArrayToDataTable(qrr, dtOppo);

                    queryrecordresult qrrPolicy = GetSchemeBy_OppoId(binding, row["opportunityid"].ToString());
                    if (qrrPolicy.records != null && qrrPolicy.records.Count() > 0)
                        dtScheme = SageCrmRecordArrayToDataTable(qrrPolicy, dtScheme);


                }

                queryrecordresult qrrCity = GetCityNameBy_CityId(binding, row["city"].ToString());
                if (qrrCity.records != null && qrrCity.records.Count() > 0)
                {
                    row["city"] = qrrCity.records[0].records[0].value;
                }
                row["Create date"] = DateTime.Parse(row["Create date"].ToString()).ToShortDateString();
                if (row["Source"].ToString().ToLower().Equals("ncbincoming")) { row["Source"] = "NCB Incoming"; }
            }
    }


    public static queryrecordresult GetFamilyOpportunityByLead_Oppo_Id(WebService binding, string lead_Opportunity_Id)
    {
        queryrecordresult aresult = binding.queryrecord(@"oppo_opportunityid oppId,oppo_totalnumberofmembers [No. Of members],
        oppo_expectedinceptiondate [Inception Date],oppo_forecast [Forecast],oppo_contractnumber [Contract number],oppo_sadadref [Sadad ref.], 
        oppo_stage [Stage], oppo_cardsdeliverydate [Card delivery date], oppo_sadadstatus", "Oppo_OpportunityId='" + lead_Opportunity_Id + "'", "Opportunity", "");
        return aresult;
    }
    public static queryrecordresult GetSchemeBy_OppoId(WebService binding, string lead_Opportunity_Id)
    {
        queryrecordresult aresult = binding.queryrecord("plcy_opportunityid plcy_oppoId,plcy_customername [Scheme]", "plcy_OpportunityId='" + lead_Opportunity_Id + "'", "Policy", "");
        return aresult;
    }
    public static queryrecordresult GetCityNameBy_CityId(WebService binding, string city_Record_Id)
    {
        queryrecordresult aresult = binding.queryrecord("city_City [City Name]", "city_recordid='" + city_Record_Id + "'", "City", "");
        return aresult;
    }

    public DataTable JoinDataTables(DataTable dt1, DataTable dt2, string joinTable1Field, string joinTable2Field)
    {
        DataTable targetTable = new DataTable();
        if (dt1 != null && dt1.Rows != null && dt1.Rows.Count > 0)
        {
            targetTable = dt1.Clone();
            var dt2Columns = dt2.Columns.OfType<DataColumn>().Select(dc =>
            new DataColumn(dc.ColumnName, dc.DataType, dc.Expression, dc.ColumnMapping));

            var dt2FinalColumns = from dc in dt2Columns.AsEnumerable()
                                  where targetTable.Columns.Contains(dc.ColumnName) == false
                                  select dc;

            targetTable.Columns.AddRange(dt2FinalColumns.ToArray());

            var rowData = from row1 in dt1.AsEnumerable()
                          join row2 in dt2.AsEnumerable()
                          on row1[joinTable1Field] equals row2[joinTable2Field]
                          into gj
                          from row2 in gj.DefaultIfEmpty()
                          select row1.ItemArray.Concat((row2 == null) ? (dt2.NewRow().ItemArray) : row2.ItemArray).ToArray();

            foreach (object[] values in rowData)
                targetTable.Rows.Add(values);


        }
        return targetTable;
    }
    private static DataTable SageCrmRecordArrayToDataTable(queryrecordresult aresult, DataTable dt = null)
    {
        try
        {
            if (aresult != null && aresult.records != null && aresult.records.Count() > 0)
            {
                crmrecord[] leadsRawList = aresult.records;
                if (dt == null)
                    dt = new DataTable();
                DataRow dr = dt.NewRow();

                DataColumn dc = new DataColumn();
                foreach (crmrecord item in leadsRawList)
                {
                    dr = dt.NewRow();
                    foreach (var item1 in item.records)
                    {
                        if (!dt.Columns.Contains(item1.name))
                            dt.Columns.Add(item1.name);

                        dr[item1.name] = item1.value;

                    }
                    dt.Rows.Add(dr);
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        return dt;
    }

    protected void RadGrid1_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
    {
        e.Column.FilterControlWidth = Unit.Pixel(80);
        e.Column.ItemStyle.Width = Unit.Pixel(80);

        if (e.Column.UniqueName.ToLower() == "oppid" || e.Column.UniqueName.ToLower() == "oppoid" || e.Column.UniqueName.ToLower() == "opportunityid")
        {
            e.Column.Visible = false;
        }
        //IDNumber
        else if (e.Column.UniqueName.ToLower() == "idnumber")
        {
            e.Column.FilterControlWidth = Unit.Pixel(160);
            e.Column.ItemStyle.Width = Unit.Pixel(160);
            e.Column.HeaderText = "ID Number";
        }
        else if (e.Column.UniqueName.ToLower() == "name")
        {
            e.Column.FilterControlWidth = Unit.Pixel(160);
            e.Column.ItemStyle.Width = Unit.Pixel(160);
            e.Column.HeaderText = "Name";
        }
        else if (e.Column.UniqueName.ToLower() == "status")
        {
            e.Column.FilterControlWidth = Unit.Pixel(70);
            e.Column.ItemStyle.Width = Unit.Pixel(70);
            e.Column.HeaderText = "Status";
        }
        else if (e.Column.UniqueName.ToLower() == "status")
        {
            e.Column.FilterControlWidth = Unit.Pixel(70);
            e.Column.ItemStyle.Width = Unit.Pixel(70);
            e.Column.HeaderText = "Status";
        }
        else if (e.Column.UniqueName.ToLower() == "mobile")
        {
            e.Column.FilterControlWidth = Unit.Pixel(95);
            e.Column.ItemStyle.Width = Unit.Pixel(95);
            e.Column.HeaderText = "Mobile";
        }
        else if (e.Column.UniqueName.ToLower() == "forecast")
        {
            e.Column.FilterControlWidth = Unit.Pixel(95);
            e.Column.ItemStyle.Width = Unit.Pixel(95);
            e.Column.HeaderText = "Total Premium";
        }
        else if (e.Column.UniqueName.ToLower() == "sadadstatus")
        {
            e.Column.FilterControlWidth = Unit.Pixel(100);
            e.Column.ItemStyle.Width = Unit.Pixel(100);
            e.Column.HeaderText = "Sadad Status";

        }

        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        e.Column.HeaderText = textInfo.ToTitleCase(e.Column.HeaderText);
    }

    protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if(RadGrid1.Columns.Contains("sadad"))
        //Is it a GridDataItem
        if (e.Item is GridDataItem)
        {
            //Get the instance of the right type
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            //Check the formatting condition

            if (dataBoundItem["sadadstatus"]!=null && !string.IsNullOrWhiteSpace(dataBoundItem["sadadstatus"].Text) && dataBoundItem["sadadstatus"].Text == ".")
                dataBoundItem["sadadstatus"].Text = "Pending";
        }
    }

    protected void btnExcel_Click(object sender, System.EventArgs e)
    {
        RadGrid1.ExportSettings.ExportOnlyData = true;
        RadGrid1.ExportSettings.IgnorePaging = true;
        RadGrid1.ExportSettings.OpenInNewWindow = true;
        RadGrid1.MasterTableView.ExportToExcel();
    }

}