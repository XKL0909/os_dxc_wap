﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/DashBoard.master" AutoEventWireup="true" Inherits="broker_leadsupload" Codebehind="leadsupload.aspx.cs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
    <style type="text/css">
        .DataMissingError
        {
            color: Red;
        }

         .DataFormatError
        {
            color: yellow;
        }
         .DataDuplicateError
        {
            color: orangered;
        }
    </style>
    <script>
        function setbtnUploadVisibility() {
            var FileUploadPath = document.getElementById('fileUpload').value;
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
            if (Extension == 'xlsx' || Extension == 'xls')//Upload will happen only if .xlsx /.xls ext
                document.getElementById('btnUpload').style.visibility = "hidden";
            else
                alert('Please select a valid EXCEL template file to upload');
        }
    </script>
    <table style="width: 100%">
        <tr>
            <td>
                <h2 style="text-align: left">Upload medical insurance leads for Bupa Arabia</h2>
            </td>
            <td style="width: 30%; text-align: right">
                <a style="text-align: right; direction: ltr;" href="worklist.aspx" class="likeLinks">View leads</a>
            </td>
        </tr>
    </table>

    <table style="width: 60%" class="Tab">
        <tr>
            <td colspan="2">To upload medical insurance leads for Bupa Arabia download the <a href="Templates\LeadTemplate.xlsx" class="likeLinks">Lead template file</a> </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <br />
                <asp:FileUpload ID="fileUpload" runat="server" Width="524px" />
            </td>
        </tr>
        <tr>
            <td colspan="2">

                <asp:Button runat="server" ID="btnUpload" Text="Read & Validate Data" OnClick="btnValidate_Click"></asp:Button>

                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <asp:Label ID="lblResult" runat="server" ForeColor="Red" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <asp:Label ID="lblNote" ForeColor="Red" runat="server" Text=""></asp:Label>
            </td>
        </tr>

        <tr>
            <td colspan="2" align="right" width="800px">&nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Upload leads" Visible="false" OnClick="btnSubmit_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                 
    <telerik:radscriptmanager id="RadScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
                </asp:ScriptReference>
            </Scripts>
        </telerik:radscriptmanager>
                <telerik:radajaxloadingpanel id="ralp" runat="server" skin="Default"></telerik:radajaxloadingpanel>
                <telerik:radajaxmanager id="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="pnlBtnExcel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ReimbursementClaimDetailsGrid" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:radajaxmanager>

                <telerik:radajaxpanel runat="server" id="RadAjaxPanel1" loadingpanelid="ralp">
    <telerik:radgrid width="890px" id="gvData" runat="server" allowfilteringbycolumn="True" allowpaging="True" allowsorting="True" 
        OnColumnCreated="gvData_ColumnCreated" OnItemDataBound="gvData_ItemDataBound" >
        
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Scrolling AllowScroll="True" UseStaticHeaders="False" />
            </ClientSettings>

        </telerik:radgrid>
        </telerik:radajaxpanel>
                <br />
                <asp:Label Text="<b>Note: </b>[<span style='background-color:red; color:white' > Red background </span>- Missing mandatory value | <span style='color:red' > Red text </span> - Value is not in proper format | <span style='background-color:yellow' > Yellow background </span> - Lead already exist for Id number ]" runat="server" ID="labelLagends" Visible="false" Font-Size="Smaller"></asp:Label>
                
            </td>
        </tr>

        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>





</asp:Content>

