﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/DashBoard.master" AutoEventWireup="true" Inherits="broker_worklist" Codebehind="worklist.aspx.cs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<%--<%@ Register Src="../UserControls/DashBoardWelcome.ascx" TagName="DashBoardWelcome" TagPrefix="uc1" %>--%>
<%@ Register Src="../UserControls/DashBoardOSNav.ascx" TagName="DashBoardOSNav" TagPrefix="uc2" %>

<%@ Register Src="../UserControls/Tips.ascx" TagName="Tips" TagPrefix="uc3" %>

<%@ Register Src="../UserControls/HealthArt.ascx" TagName="HealthArt" TagPrefix="uc4" %>

<%@ Register Src="../UserControls/Apps.ascx" TagName="Apps" TagPrefix="uc5" %>

<%@ Register Src="../UserControls/Downloads.ascx" TagName="Downloads" TagPrefix="uc7" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
<%-- <style type="text/css">
        .style1
        {
            text-align: justify;
        }
    </style>
    <div class="careerBanner">
        <div>
            <h2 class="dahsboardTitle">Peace of mind and ease of payment</h2>
            <div style="text-align: justify">
                <asp:Label ID="Label2" runat="server" Text="Get Bupa family health insurance and take advantage of interest free installment plan only with NCB."></asp:Label>
                <br />
            </div>
            <h2 class="dahsboardTitle">How to Navigate the portal?</h2>
            <div style="text-align: justify">
                <asp:Label ID="Label1" runat="server" Text="it is like <b>1</b>.Login <b>2</b>.Upload leads <b>3</b>. View work list. <b>4</b>. Pay using SADAD and <b>done</b>"></asp:Label>
            </div>
        </div>
    </div>
<uc1:DashBoardWelcome ID="DashBoardWelcome1" runat="server" />--%>
    
    <telerik:radscriptmanager id="RadScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
                </asp:ScriptReference>
            </Scripts>
        </telerik:radscriptmanager>
    <div style="width: 890px; text-align: right; vertical-align: bottom;">
        <asp:Button ID="pnlBtnExcel" Width="150px" Text="Export to Excel" OnClick="btnExcel_Click"
            runat="server" />
    </div>
    <br />

    <telerik:Radajaxloadingpanel id="ralp" runat="server" skin="Default" ></telerik:Radajaxloadingpanel>
    <telerik:radajaxmanager id="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="pnlBtnExcel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ReimbursementClaimDetailsGrid" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:radajaxmanager>

    <telerik:radajaxpanel runat="server" id="RadAjaxPanel1" loadingpanelid="ralp">
    <telerik:radgrid width="890px" id="RadGrid1" runat="server" allowfilteringbycolumn="True" allowpaging="True" allowsorting="True" oncolumncreated="RadGrid1_ColumnCreated" OnItemDataBound="RadGrid1_ItemDataBound">
            
        <AlternatingItemStyle BackColor="LightGray" />
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Scrolling AllowScroll="True" UseStaticHeaders="False" />
            </ClientSettings>
        </telerik:radgrid>
        </telerik:radajaxpanel>
    <br />



  <%-- <uc3:Tips ID="Tips1" runat="server" />

    <uc4:HealthArt ID="HealthArt1" runat="server" />
    <uc5:Apps ID="Apps1" runat="server" />

   


    <uc2:DashBoardOSNav ID="DashBoardOSNav" runat="server" />--%>


</asp:Content>