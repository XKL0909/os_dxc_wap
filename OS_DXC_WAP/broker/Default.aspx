﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/DashBoard.master" AutoEventWireup="true" Inherits="broker_Default" Codebehind="Default.aspx.cs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<%--<%@ Register Src="../UserControls/DashBoardWelcome.ascx" TagName="DashBoardWelcome" TagPrefix="uc1" %>--%>
<%@ Register Src="../UserControls/DashBoardOSNav.ascx" TagName="DashBoardOSNav" TagPrefix="uc2" %>

<%--<%@ Register Src="../UserControls/Tips.ascx" TagName="Tips" TagPrefix="uc3" %>--%>

<%@ Register Src="../UserControls/HealthArt.ascx" TagName="HealthArt" TagPrefix="uc4" %>

<%@ Register Src="../UserControls/Apps.ascx" TagName="Apps" TagPrefix="uc5" %>

<%@ Register Src="../UserControls/Downloads.ascx" TagName="Downloads" TagPrefix="uc7" %>


<asp:Content ContentPlaceHolderID="ContentPlaceHolderLeftNav" ID="BrokerContactUS" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="215">
                <div class="contactBanner">
                    <div class="contactTitle">Contact us</div>
                    <div class="mostReadRow">
                        For Technical Inquiries:<br />
                        <span>Amal Alkhawari<br />
+966 920 000 456 <br /> Ext 3354 </span>
                        <div class="mostReadRow"><a href="mailto:Amal.AlKhawari@bupa.com.sa">Amal.AlKhawari@bupa.com.sa</a></div>

                        
                        <div class="mostReadRow">
                            <br />
                            For Sales Inquiries:
                            <br />
                            <span>800-302-0000</span>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">


    <style type="text/css">
        .style1
        {
            text-align: justify;
        }
    </style>
    <div class="careerBanner">
        <div>
            <h2 class="dahsboardTitle">Peace of mind and ease of payment</h2>
            <div style="text-align: justify">
                <asp:Label ID="Label2" runat="server" Text="Get Bupa family health insurance and take advantage of interest free installment plan only with NCB."></asp:Label>
                <br />
            </div>
           <%-- <h2 class="dahsboardTitle">How to Navigate the portal?</h2>
            <div style="text-align: justify">
                <asp:Label ID="Label1" runat="server" Text="it is like <b>1</b>.Login <b>2</b>.Upload leads <b>3</b>. View work list. <b>4</b>. Pay using SADAD and <b>done</b>"></asp:Label>
            </div>--%>
        </div>
    </div>



    <div class="boxcaption" style="width: 100%; height: auto">
        <table cellpadding="10" cellspacing="30" class="boxcaption">
            <tr>
                <td align="left" class="boxcaption"><a href="leadsupload.aspx">
                    <img id="Img7" runat="server"
                        border="0" style="height: 85px; width: 80px" src="~/images/leadsUpload.jpg" alt=" Claim History"
                        title=" Leads Upload" /></a><br />
                    Upload Leads </td>
                <td align="left" class="boxcaption"><a href="worklist.aspx">
                    <img id="Img8"
                        style="height: 85px; width: 80px" runat="server" border="0"
                        src="~/images/worklist.jpg" alt="List of already uploaded leads"
                        title="Uploaded leads" /></a><br />
                    Work list
                </td>
                <td align="left" class="boxcaption"><a href="dashboard.aspx">
                    <img id="Img21" runat="server"
                        border="0" src="~/images/dashboard.jpg"
                        style="height: 85px; width: 80px" /></a><br />
                    Dashboard</td>
            </tr>
        </table>
    </div>

    <br />

    <%--<uc3:Tips ID="Tips1" runat="server" />--%>

    <uc4:HealthArt ID="HealthArt1" runat="server" />
    <uc5:Apps ID="Apps1" runat="server" />

    <div class="botright" runat="server" id="Notify" visible="false">
        <table width="100%">
            <tr>
                <td valign="top" style="color: White">
                    <h3>Message From Bupa</h3>
                </td>
                <td align="right" valign="bottom"></td>
            </tr>

            <tr>
                <td colspan="2" style="color: White">
                    <asp:Label ID="lblClient" ForeColor="White" runat="server"></asp:Label>
                    <asp:Label ID="LabelMedicalDeclarationMessage" ForeColor="White" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>

                <td align="right" colspan="2">
                    <img src="../member/images/feedback.png" width="32" height="32" style="border: 0px" /></td>
            </tr>
        </table>


    </div>


    <uc2:DashBoardOSNav ID="DashBoardOSNav" runat="server" />


</asp:Content>

