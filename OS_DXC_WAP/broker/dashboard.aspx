﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/DashBoard.master" AutoEventWireup="true" Inherits="broker_dashboard" Codebehind="dashboard.aspx.cs" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolderLeftNav" ID="BrokerContactUS" runat="server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="215">
                <div class="contactBanner">
                    <div class="contactTitle">Contact us</div>
                    <div class="mostReadRow">
                        For Technical Inquiries:<br />
                        <span>Walaa Alsulaiman
                            <br />
                            +966 920 000 456
                            <br />
                            Ext 3213 </span>
                        <div class="mostReadRow"><a href="mailto:Walaa.Alsulaiman@bupa.com.sa">Walaa.Alsulaiman@bupa.com.sa</a></div>


                        <div class="mostReadRow">
                            <br />
                            For Sales Inquiries:
                            <br />
                            <span>800 116 0500</span>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="Server">
    <style>
        .DashboardTable
        {
            vertical-align: middle;
            text-align: center;
            border-radius: 10px 11px;
            font-family: 'Arial Rounded MT';
            margin: 3px;
            padding: 3px;
            margin-left: 10px;
        }
    </style>

    <table class="DashboardTable" style="font-size: larger;" cellpadding="3px" cellspacing="10px">
        <thead>

            <tr>
                <td>Leads</td>
                <td>Opportunities</td>
                <td>Financials</td>
            </tr>

        </thead>
        <tr>
            <td class="DashboardTable" style="height: 200px; width: 200px; background-color: teal; color: whitesmoke">
                <asp:Label ID="TotalNCBLead" Text="" runat="server" />
                <asp:Label ID="NCBLeadToday" Text="" runat="server" />
                <asp:Label ID="NCBLeadInSevenDays" Text="" runat="server" />
                <asp:Label ID="NCBLeadInThirtyDays" Text="" runat="server" />
                <asp:Label ID="LeadsToOppoRatio" Text="" runat="server" />
            </td>
            <td class="DashboardTable" style="height: 200px; width: 200px; background-color: navy; color: whitesmoke">
                <asp:Label ID="TotalNCBOpportunities" Text="" runat="server" />
                <asp:Label ID="TotalNCBPendingOpportunities" Text="" runat="server" />
                <asp:Label ID="TotalSADADPaidOpportunities" Text="" runat="server" />
                <asp:Label ID="TotalLostOpportunities" Text="" runat="server" />
                <asp:Label ID="OpportunityPaidRatio" Text="" runat="server" />

            </td>
            <td class="DashboardTable" style="height: 200px; width: 200px; background-color: orangered; color: whitesmoke">
                <asp:Label ID="TotalExpectedPremium" Text="" runat="server" />
                <asp:Label ID="TotalPaidPremium" Text="" runat="server" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>




    <br />
    <br />

    <br />
    <br />



    <br />
    <br />



</asp:Content>

