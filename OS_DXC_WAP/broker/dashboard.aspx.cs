﻿using OS_DXC_WAP.crmuat;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class broker_dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        WebService binding = GetSageSession();

        GetFamilyLeadsBySource(binding, string.IsNullOrWhiteSpace(Session["BrokerName"].ToString()) ? "bupa_website" : Session["BrokerName"].ToString());
        // GetFamilyLeadsBySource(binding, "bupa_website" );
    }

    private static WebService GetSageSession()
    {
        WebService binding = new WebService();
        try
        {

            logonresult SID = new logonresult();
            SID = binding.logon(ConfigurationManager.AppSettings["SageUsername"], ConfigurationManager.AppSettings["SagePassword"]);
            binding.SessionHeaderValue = new SessionHeader();
            binding.SessionHeaderValue.sessionId = SID.sessionid;
        }
        catch (Exception)
        {

            throw;
        }
        return binding;
    }
    private static DataTable SageCrmRecordArrayToDataTable(queryrecordresult aresult, DataTable dt = null)
    {
        try
        {
            if (aresult != null && aresult.records != null && aresult.records.Count() > 0)
            {
                crmrecord[] leadsRawList = aresult.records;
                if (dt == null)
                    dt = new DataTable();
                DataRow dr = dt.NewRow();

                DataColumn dc = new DataColumn();
                foreach (crmrecord item in leadsRawList)
                {
                    dr = dt.NewRow();
                    foreach (var item1 in item.records)
                    {
                        if (!dt.Columns.Contains(item1.name))
                            dt.Columns.Add(item1.name);

                        dr[item1.name] = item1.value;

                    }
                    dt.Rows.Add(dr);
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        return dt;
    }
    public static queryrecordresult GetFamilyOpportunityByLead_Oppo_Id(WebService binding, string lead_Opportunity_Id)
    {
        queryrecordresult aresult = binding.queryrecord(@"oppo_opportunityid oppId,oppo_totalnumberofmembers [No. Of members],
        oppo_expectedinceptiondate [Inception Date],oppo_forecast [Forecast],oppo_contractnumber [Contract number],oppo_sadadref [Sadad ref.], 
        oppo_stage [Stage], oppo_cardsdeliverydate [Card delivery date], oppo_sadadstatus", "Oppo_OpportunityId='" + lead_Opportunity_Id + "'", "Opportunity", "");
        return aresult;
    }


    void GetFamilyLeadsBySource(WebService binding, string sourceOfLead) // source can be like bupa web site, NCB etc..
    {
        int leadsCount = 0;
        int oppCount = 0;
        int oppTotalSadadPaid = 0;

        int leadsUploadedToday = 0;
        int leadsUploadedSevenDays = 0;
        int leadsUploadedThirtyDays = 0;
        double leadsConversionRatio = 0;
        double OppoPaidRatio = 0;
        int lostOpposCount = 0;


        double ncbOppoTotalPaidPremium = 0;
        double ncbOppoTotalForcastPremium = 0;

        lead lead = new lead();
        lead.source = sourceOfLead;

        ewarebase[] leadList = new ewarebase[1];
        leadList[0] = lead;
        //var CRM = Server.CreateObject("eWare.eWareSelfService");
        //record = eWare.FindRecord("lead", "lead_source='"+sourceOfLead+"'");
        //queryresult qr = binding.query("select top 1 * from lead", "lead");

        //ORIGINAL
        //queryrecordresult aresult = binding.queryrecord("lead_opportunityid,lead_personsalutation,lead_personarabicsalutation,lead_personfirstname,lead_personlastname,lead_personarabicfirstname,lead_personarabiclastname,lead_phonenumbermobile,lead_status,lead_numberoflives,lead_addresscityid,lead_regdate", "lead_source='" + sourceOfLead + "'", "lead", "lead_leadid");

        queryrecordresult aresult = binding.queryrecord(@"lead_opportunityid,lead_personsalutation + lead_personfirstname  +  lead_personlastname Name,
lead_personmobilecountrycode + lead_personmobilenumber Mobile,lead_status Status,lead_numberoflives Members,lead_addresscityid City,lead_createddate [Create date]", "lead_source='" + sourceOfLead + "'", "lead", "lead_leadid");

        DataTable dtLead = SageCrmRecordArrayToDataTable(aresult);
        DataTable dtOppo = new DataTable();
        DataTable dtScheme = new DataTable();
        DataTable dtResult = new DataTable();


        if (dtLead != null && dtLead.Rows.Count > 0)
            foreach (DataRow row in dtLead.Rows)
            {
                if (!string.IsNullOrWhiteSpace(row["opportunityid"].ToString()))
                {
                    queryrecordresult qrr = GetFamilyOpportunityByLead_Oppo_Id(binding, row["opportunityid"].ToString());
                    if (qrr.records != null && qrr.records.Count() > 0)
                        dtOppo = SageCrmRecordArrayToDataTable(qrr, dtOppo);
                }

                //Create date
                if (row["Create date"] != null && !string.IsNullOrWhiteSpace(row["Create date"].ToString()))
                {
                    DateTime dt = DateTime.Now;
                    DateTime.TryParse(row["Create date"].ToString(), out dt);
                    if (dt.Date.Equals(DateTime.Now.Date))
                        leadsUploadedToday = leadsUploadedToday + 1;
                    else if (dt.Date >= DateTime.Now.Date.AddDays(-7))
                        leadsUploadedSevenDays = leadsUploadedSevenDays + 1;
                    else if (dt.Date >= DateTime.Now.Date.AddDays(-30))
                        leadsUploadedThirtyDays = leadsUploadedThirtyDays + 1;
                }
            }
        if (dtOppo != null && dtOppo.Rows.Count > 0)
        foreach (DataRow rowOppo in dtOppo.Rows)
        {
            if (!string.IsNullOrWhiteSpace(rowOppo["sadadstatus"].ToString()))
            {
                if (!string.IsNullOrWhiteSpace(rowOppo["Forecast"].ToString()) && !rowOppo["sadadstatus"].ToString().ToLower().Equals("paid"))
                {
                    ncbOppoTotalForcastPremium = ncbOppoTotalForcastPremium + Convert.ToDouble(rowOppo["Forecast"].ToString().Trim());
                }

                if (rowOppo["sadadstatus"].ToString().ToLower().Equals("paid"))
                {
                    oppTotalSadadPaid = oppTotalSadadPaid + 1;
                    ncbOppoTotalPaidPremium = ncbOppoTotalPaidPremium + Convert.ToDouble(rowOppo["Forecast"].ToString().Trim());
                }

                if (rowOppo["Stage"].ToString().ToLower().Equals("deallost"))
                    lostOpposCount = lostOpposCount + 1;


            }
            else if (!string.IsNullOrWhiteSpace(rowOppo["Forecast"].ToString()))
            {
                ncbOppoTotalForcastPremium = ncbOppoTotalForcastPremium + Convert.ToDouble(rowOppo["Forecast"].ToString().Trim());
            }
        }



        if (dtLead != null && dtLead.Rows.Count > 0)
        {
            leadsCount = dtLead.Rows.Count;
        }
        if (dtOppo != null && dtOppo.Rows.Count > 0)
        {
            oppCount = dtOppo.Rows.Count;
        }
        //total expected premium= ncbOppoTotalPremium;
        //leadsUploadedToday = 0;
        //leadsUploadedSevenDays = 0;
        //leadsUploadedThirtyDays = 0;

        NCBLeadToday.Text = "<br/><br/>Today <br/><b><font size='5'>" + leadsUploadedToday + "</font></b>";
        NCBLeadInSevenDays.Text = "<br/><br/>Within 1 Week <br/><b><font size='5'>" + leadsUploadedSevenDays + "</font></b>";
        NCBLeadInThirtyDays.Text = "<br/><br/>Witin 1 Month <br/><b><font size='5'>" + leadsUploadedThirtyDays + "</font></b>";

        leadsConversionRatio = (Convert.ToDouble(oppCount) / Convert.ToDouble(leadsCount)) * 100;
        OppoPaidRatio = (Convert.ToDouble(oppTotalSadadPaid) / Convert.ToDouble(oppCount)) * 100;


        TotalNCBLead.Text = "<br/>NCB Leads <br/><b><font size='5'>" + leadsCount + "</font></b>";
        TotalNCBOpportunities.Text = "<br/>Leads To Oppo.<br/> <b><font size='5'>" + oppCount + "</font></b>";
        TotalExpectedPremium.Text = "<br/>Expected Premium <br/><b><font size='5'>" + ncbOppoTotalForcastPremium.ToString("C2", CultureInfo.CreateSpecificCulture("ar-SA")) + "</font></b>";//
        TotalSADADPaidOpportunities.Text = "<br/><br/>Paid<br/><b><font size='5'>" + oppTotalSadadPaid + "</font></b>";//

        TotalNCBPendingOpportunities.Text = "<br/><br/>Pending for payment<br/> <b><font size='5'>" + (oppCount - oppTotalSadadPaid - lostOpposCount) + "</font></b>";//
        TotalLostOpportunities.Text = "<br/><br/>Lost<br/> <b><font size='5'>" + (lostOpposCount) + "</font></b>";//
        //ar-SA
        TotalPaidPremium.Text = "<br/><br/>Paid By NCB<br/><b><font size='5'>" + ncbOppoTotalPaidPremium.ToString("C2", CultureInfo.CreateSpecificCulture("ar-SA")) + "</font></b>";//

        LeadsToOppoRatio.Text = "<br/><br/>Lead to Oppo. <br/> <b><font size='5'>" + String.Format("{0:0.##}", leadsConversionRatio) + "</font></b>%";// leadsConversionRatio.ToString();
        OpportunityPaidRatio.Text = "<br/><br/>Deal Won<br/> <b><font size='5'>" + String.Format("{0:0.##}", OppoPaidRatio) + "</font></b>%";// OppoPaidRatio.ToString();
    }

}