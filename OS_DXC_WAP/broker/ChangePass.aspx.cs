using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Broker_ChangePass : System.Web.UI.Page
{
    string strClientID;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["BrokerNo"] == null)
            Response.Write("<script>window.open('../default.aspx','_parent');</script>");        
        else
        {

            strClientID = Session["BrokerNo"].ToString();
        }

     

    }
    protected void JoinBtn_Click(object sender, EventArgs e)
    {
       
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnBupa_OS"].ConnectionString);       
        string result = "";

        try
        {          
            
            SqlParameter[] arParms = new SqlParameter[6];

            arParms[0] = new SqlParameter("@UserID", SqlDbType.NVarChar, 30);
            arParms[0].Value = Session["ClientID"].ToString(); 

            arParms[1] = new SqlParameter("@Username", SqlDbType.NVarChar, 30);
            arParms[1].Value = Session["ClientUsername"].ToString(); 

            
            arParms[2] = new SqlParameter("@OldPassword", SqlDbType.NVarChar, 30);
            arParms[2].Value = txtOldPassword.Text;

            
            arParms[3] = new SqlParameter("@NewPassword", SqlDbType.NVarChar, 30);
            arParms[3].Value =  txtPassword.Text;

            arParms[4] = new SqlParameter("@UserType", SqlDbType.NVarChar, 30);
            arParms[4].Value = "Client"; 

            
            arParms[5] = new SqlParameter("@Result", SqlDbType.NVarChar, 30);
            arParms[5].Direction = ParameterDirection.Output;           

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spChangeUserPassword", arParms);

           
            result =   arParms[5].Value + "";

           
        }
        catch (Exception ex)
        {
            string errMessage = "";
            for (Exception tempException = ex; tempException != null; tempException = tempException.InnerException)
            {
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine;
            }

           
        }
        finally
        {
            if (connection != null)
                connection.Dispose();
        }

        lblMessage.Visible = true;
        
       

        if (result == "Found")
        {
            PnlChangePass.Visible = false;
            lblMessage.ForeColor = System.Drawing.Color.Green; 
            lblMessage.Text = "<BR>Password successfully changed.";
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "Incorrect Password. Please type in the correct 'Old Password'.";
        }
     
        

    }
}
