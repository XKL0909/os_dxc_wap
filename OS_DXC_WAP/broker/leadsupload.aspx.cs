﻿using Bupa.Core.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Excel;
using System.Text;
using System.IO;
using System.Configuration;
using System.Drawing;
using Telerik.Web.UI;
using OS_DXC_WAP.crmuat;

public partial class broker_leadsupload : System.Web.UI.Page
{
    string _location = string.Empty;
    string filename = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private static WebService GetSageSession()
    {
        WebService binding = new WebService();
        try
        {

            logonresult SID = new logonresult();
            SID = binding.logon(ConfigurationManager.AppSettings["SageUsername"], ConfigurationManager.AppSettings["SagePassword"]);
            binding.SessionHeaderValue = new SessionHeader();
            binding.SessionHeaderValue.sessionId = SID.sessionid;
        }
        catch (Exception)
        {

            throw;
        }
        return binding;
    }
    private DataSet GetExcel(string fileName)
    {
        StringBuilder sb = new StringBuilder();
        Logger.Tracer.WriteMemberEntry();
        try
        {
            try
            {
                FileStream stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
                Excel.IExcelDataReader excelReader;
                if (Path.GetExtension(fileName) == ".xlsx")
                {
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    Session["extn"] = ".xlsx";
                }
                else
                {
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    Session["extn"] = ".xls";
                }

                //3. DataSet - Create column names from first row
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet result = excelReader.AsDataSet(true);

                if (result != null && result.Tables != null && result.Tables.Count > 0)
                {
                    //Create the list of id numbers so can be validated for duplicate leads by Id number
                    List<string> leadIdNumbers = new List<string>();
                    int i = 0;
                    int emptyRowsCount = 0;
                    //5. Data Reader methods
                    while (excelReader.Read())
                    {
                        i = i + 1;
                        if (i == 1)
                            continue;

                        #region commented
                        //Future works: get columns in excel by index, and check for any validation here
                        /*
                     
                     
                          if (excelReader.GetString(0) != null && excelReader.GetString(0).Length >= 1)
                           {
                              //get column 1 value
                                try
                                {
                                    string strDate = excelReader[1].ToString();
                                    
                                }
                                catch (Exception ex)
                                {
                        
                                }
                                //get gender
                                excelReader.GetString(2);

                            }
                     
                      
                         0 Inciption date	 -Optional
                         1 Salutation	-Optional
                         2 Arabic Salutation	-Optional
                         3 First name	
                         4 Arabic First Name	
                         5 Last name	
                         6 Arabic Last Name	
                         7 Prefered Language	-Optional
                         8 Bestway to Contact	-Optional
                         9 Nationality	-Optional
                         10 City	
                         11 Region	
                         12 District	-Optional
                         13 Mobile country Code	
                         14 Mobile Number	
                         15 E-mail	
                         16 Product	
                         17 Source	
                         18 Country	
                         19 Number of Family Member	
                         20 ID Number	
                         21 Work Sector	
                         22 Salary Bracket	
                         23 Debt Burden Ratio
                    
                            */

                        #endregion
                        #region validation for empty values and build the error texts //currently we not using this- intead using cell colors to show errors
                        ////3- First Name English 
                        //if (excelReader.GetString(2) == null || excelReader.GetString(2).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") English first name need to be filled to upload leads");
                        ////4-Arabic first name
                        //if (excelReader.GetString(3) == null || excelReader.GetString(3).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Arabic first Name need to be filled to upload leads");
                        ////5- Last name in english
                        //if (excelReader.GetString(4) == null || excelReader.GetString(4).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") English Last name need to be filled to upload leads");
                        ////6- Arabic last name
                        //if (excelReader.GetString(5) == null || excelReader.GetString(5).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Arabic last name need to be filled to upload leads");
                        ////10- City
                        //if (excelReader.GetString(9) == null || excelReader.GetString(9).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") City need to be filled to upload leads");
                        ////11- 
                        //if (excelReader.GetString(11) == null || excelReader.GetString(11).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") First Name English need to be filled to upload leads");
                        ////13- Mobile country Code
                        //if (excelReader.GetString(13) == null || excelReader.GetString(13).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Mobile country code need to be filled to upload leads");
                        ////14- Mobile Number
                        //if (excelReader.GetString(14) == null || excelReader.GetString(14).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Mobile Number need to be filled to upload leads");
                        ////15- E-mail
                        //if (excelReader.GetString(15) == null || excelReader.GetString(15).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") E-mail need to be filled to upload leads");
                        ////16- Product Always set to Family
                        ////if (excelReader.GetString(16) == null || excelReader.GetString(16).Length < 1)
                        ////    sb.Append("<br/> In row number (" + ( i-1) + ") Product need to be filled to upload leads");
                        ////17- Source Always set to NCB
                        ////if (excelReader.GetString(17) == null || excelReader.GetString(17).Length < 1)
                        ////    sb.Append("<br/> In row number (" + ( i-1) + ") Source need to be filled to upload leads");
                        ////18- Country
                        //if (excelReader.GetString(18) == null || excelReader.GetString(18).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Country need to be filled to upload leads");
                        ////19- Number of Family Member	
                        //if (excelReader.GetString(19) == null || excelReader.GetString(19).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Number of Family Member need to be filled to upload leads");
                        
                        //9- Nationality
                        if (excelReader.GetString(8) != null && !excelReader.GetString(8).ToString().Equals("Saudi"))
                            sb.Append("<br/> In row number (" + (i - 1) + ") Only Saudi nationals allowed");
                        //14- ID Number
                        if (excelReader.GetString(14) == null || excelReader.GetString(14).Length < 1)
                            sb.Append("<br/> In row number (" + (i - 1) + ") ID Number need to be filled to upload leads");
                        else
                        {
                            leadIdNumbers.Add(excelReader.GetString(14).ToString());
                        }
                        ////21- Work Sector
                        //if (excelReader.GetString(21) == null || excelReader.GetString(21).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Work Sector need to be filled to upload leads");
                        ////22- Salary Bracket
                        //if (excelReader.GetString(22) == null || excelReader.GetString(22).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Salary Bracket	need to be filled to upload leads");
                        ////23- Debt Burden Ratio
                        //if (excelReader.GetString(23) == null || excelReader.GetString(23).Length < 1)
                        //    sb.Append("<br/> In row number (" + (i - 1) + ") Debt Burden Ratio need to be filled to upload leads");
                        #endregion

                        if (emptyRowsCount >= 3)
                        {
                            sb.Clear();
                            sb.Append("<br/> <i>System found " + (i - 4) + " records to upload, if it is not correct please check Excel template file and delete empty rows to retry!</i>");
                            break;
                        }
                    }
                    if (sb != null && sb.ToString().Length > 0)
                    {
                        //sb.Append("<br/> Empty values found in excel sheet, please provide valid values");
                        //sb.Clear();
                        sb.Append("<br/> <br/>Please correct the issues to upload the leads");
                        lblResult.Text = sb.ToString();
                        btnSubmit.Visible = false;
                    }
                    else
                    {
                        sb.Clear();
                        sb.Append("<br/>System found " + (i - 1) + " leads to upload, if it is not correct please check uploaded file");
                        lblResult.ForeColor = Color.DarkOrange;
                        lblResult.Font.Italic = false;
                        lblResult.Text = sb.ToString();
                        btnSubmit.Visible = true;
                    }

                    //add leadIdNumbers in session
                    Session["LeadIdNumbers"] = leadIdNumbers;
                }
                else
                {
                    sb.Clear();
                    sb.Append("<br/>No leads to upload, please check uploaded file");
                    lblResult.ForeColor = Color.DarkOrange;
                    lblResult.Font.Italic = false;
                    lblResult.Text = sb.ToString();
                    lblResult.Visible = true;
                }
                Logger.Tracer.WriteMemberExit();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Current.WriteException(ex);
                throw;
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }
    protected void btnValidate_Click(object sender, EventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();
        try
        {
            //labelLagends.Visible = true;
            if (fileUpload.HasFile)
            {
                string fileExtention = System.IO.Path.GetExtension(fileUpload.PostedFile.FileName).ToLower();
                lblResult.Text = "";
                if (fileExtention == ".xls" || fileExtention == ".xlsx")
                {
                    filename = System.IO.Path.GetFileNameWithoutExtension(fileUpload.PostedFile.FileName) + DateTime.Now.ToString("yyyyddMHHmmss") + System.IO.Path.GetExtension(fileUpload.PostedFile.FileName);
                    Session["filename"] = filename;

                    var path = Path.Combine(ConfigurationManager.AppSettings["BrokerLeadsDocPath"], filename);
                    string physicalServerPath = Server.MapPath(path);
                    fileUpload.SaveAs(physicalServerPath);
                    DataSet ds = GetExcel(physicalServerPath);
                    if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                    {
                        ds.Tables[0].Columns["ID Number"].SetOrdinal(1);
                        List<string> duplicateLeadIdnumbers = new List<string>();
                        //check for duplicated leads by id number and add these in session to color the id number in grid cell later during item data bound
                        if (Session["LeadIdNumbers"] != null)
                        {
                            List<string> currentLeadIdnumbers = (List<string>)Session["LeadIdNumbers"];

                            duplicateLeadIdnumbers = FindLeadsInSageByIdNumber(currentLeadIdnumbers, GetSageSession());
                            if (duplicateLeadIdnumbers.Count > 0)
                                Session["LeadIdNumbers"] = duplicateLeadIdnumbers;
                            else
                            {
                                duplicateLeadIdnumbers = new List<string>();
                                Session["LeadIdNumbers"] = duplicateLeadIdnumbers;
                            }
                        }
                        else
                        {
                            duplicateLeadIdnumbers = new List<string>();
                            Session["LeadIdNumbers"] = duplicateLeadIdnumbers;
                        }
                        Session["DatasetLeads"] = ds;

                        bool showButtons = true;
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            lblNote.Text = lblNote.Text + " <br> WRONG TEMPLATE. Kindly download the template and make sure all columns are filled before you tray again.";
                            btnSubmit.Visible = false;
                        }
                        else
                        {
                            gvData.DataSource = ds;
                            gvData.DataBind();

                           
                            
                            if (showButtons && !lblResult.Text.Contains("correct the issues"))
                            {
                                btnSubmit.Visible = true;

                            }
                            if ( duplicateLeadIdnumbers.Count > 0)
                            {
                                labelLagends.Visible = true;
                                btnSubmit.Visible = false;
                                lblResult.Text = lblResult.Text + "<br /> <span style='color:orangered' > Error: Duplicate leads found! </span>";
                            }

                            if (lblResult.Text.Contains("correct the issues"))
                            {
                                labelLagends.Visible = true;
                                btnSubmit.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        lblNote.Text = lblNote.Text + " <br> WRONG TEMPLATE. Kindly download the template and make sure all columns are filled before you tray again.";
                        btnSubmit.Visible = false;
                    }
                }
                else
                {
                    lblNote.Text = lblNote.Text + " <br> WRONG TEMPLATE. Kindly download the template and make sure all columns are filled before you tray again.";
                    btnSubmit.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }
    public List<lead> CreateLeadObject(DataSet dataSet, WebService ws)
    {
        List<lead> listNewLeadObject = new List<lead>();
        DataTable dt = dataSet.Tables[0];
        List<string> userIdsFromWebConfigToAssignLeads = new List<string>();
        userIdsFromWebConfigToAssignLeads = ConfigurationManager.AppSettings["BrokerLeadsAssignToUsers"].Split(',').ToList();
        int i = 0;
        foreach (DataRow row in dt.Rows)
        {
            lead newLeadObject = new lead();
            if (userIdsFromWebConfigToAssignLeads.Count > 0 && !string.IsNullOrWhiteSpace(userIdsFromWebConfigToAssignLeads[i]))
            {
                newLeadObject.assigneduserid = int.Parse(userIdsFromWebConfigToAssignLeads[i].Trim());
            }
            else
                newLeadObject.assigneduserid = 1;
            //reset to first agent if already assigned to next 2 agents
            if (i < (userIdsFromWebConfigToAssignLeads.Count-1))
                i = i + 1;
            else
                i = 0;

            List<string> cityAndRegionIds = FindCityIdInSageByCityName(row["City"].ToString(), ws);

            newLeadObject.personsalutation = row["Salutation"].ToString();
            newLeadObject.personarabicsalutation = GetArabicSalutationByEnglishSalutation(row["Salutation"].ToString());
            newLeadObject.personfirstname = row["First name"].ToString();
            newLeadObject.personarabicfirstname = row["Arabic First Name"].ToString();
            newLeadObject.personlastname = row["Last name"].ToString();
            newLeadObject.personarabiclastname = row["Arabic Last Name"].ToString();
            newLeadObject.personpreferedlanguage = row["Prefered Language"].ToString();
            newLeadObject.personbestwaytocontact = row["Bestway to Contact"].ToString();
            newLeadObject.personnationality = "SA";//validate rule: Non saudi not allowed- if saudi then "SA"  =row["Nationality"].ToString();
            newLeadObject.personcardid = int.Parse( row["ID Number"].ToString());
            
            newLeadObject.addresscityid = int.Parse(cityAndRegionIds[0]);
            newLeadObject.addressregion = int.Parse(cityAndRegionIds[1]);
            
            newLeadObject.district = row["District"].ToString();
            newLeadObject.personmobilecountrycode = "+966";//=row["Mobile Country Code"].ToString();
            newLeadObject.personmobilenumber = row["Mobile Number"].ToString();
            newLeadObject.personemail = row["E-mail"].ToString();
            newLeadObject.mainproductinterest = "Family";//=row["Product"].ToString();
            newLeadObject.source = "NCB";//=row["Source"].ToString();
            newLeadObject.companycountry = "SA";//=row["Country"].ToString();
            newLeadObject.numberoflives = int.Parse(row["Number of Family Member"].ToString());
            
            newLeadObject.personworksector = row["Work Sector"].ToString();
            newLeadObject.personsalary = row["Salary Bracket"].ToString();
            newLeadObject.persondbr = int.Parse(row["Debt Burden Ratio"].ToString());

            newLeadObject.status = "In Progress";
            newLeadObject.stage = "Logged";
            newLeadObject.createddate = DateTime.Now;

            newLeadObject.assigneduseridSpecified = true;
            newLeadObject.numberoflivesSpecified = true;
            newLeadObject.addresscityidSpecified = true;
            newLeadObject.addressregionSpecified = true;
            newLeadObject.createddateSpecified = true;
            newLeadObject.inceptiondateSpecified = true;
            newLeadObject.persondbrSpecified = true;
            newLeadObject.personcardidSpecified = true;
            

            newLeadObject.inceptiondate = row["Inciption date"] == null || string.IsNullOrWhiteSpace(row["Inciption date"].ToString()) ? DateTime.Now : DateTime.Parse(row["Inciption date"].ToString());

            listNewLeadObject.Add(newLeadObject);

        }
        //new template



        return listNewLeadObject;
    }
    public bool AddLeadsToSage(List<lead> newLeadsObject, WebService binding)
    {
        bool result = false;
        foreach (var leadItem in newLeadsObject)
        {
            ewarebase[] CRMBase = new ewarebase[1];
            crmid newRecordID = new crmid();

            CRMBase[0] = leadItem;

            addresult CRMAddResult = binding.add("lead", CRMBase);
            result = CRMAddResult.size > 0 ? true : false;
        }

        binding.logoff(binding.SessionHeaderValue.sessionId);
        return result;
    }

    public string GetArabicSalutationByEnglishSalutation(string englishSalutation)
    {
        /*
        Dr.                   
        Eng.
        Mr.
        Mrs.
        Ms.
        Prof.
        
        الدكتور
        المهندس
        السيد
        السيده
        الآنسه
        البروفيسور
        */
        string arabicSalutaiton = string.Empty;
        switch (englishSalutation)
        {
            case "Mr.":
                arabicSalutaiton="السيد";
                break;
            case "Dr.":
                arabicSalutaiton = "الدكتور";
                break;
            case "Eng.":
                arabicSalutaiton = "المهندس";
                break;
            case "Mrs.":
                arabicSalutaiton = "السيده";
                break;
            case "Ms.":
                arabicSalutaiton = "الآنسه";
                break;
            case "Prof.":
                arabicSalutaiton = "البروفيسور";
                break;
            default:
                break;

        }
        return arabicSalutaiton;

    }
    public List<string> FindLeadsInSageByIdNumber(List<string> listOfLeadPersonIdNumbers, WebService binding)
    {
        List<string> result = new List<string>();
        WebService CRMService = binding;
        foreach (var item in listOfLeadPersonIdNumbers)
        {
            queryrecordresult aresult = binding.queryrecord(@"lead_personcardid", "lead_personcardid='" + item + "'", "lead", "");

            ewarebase[] CRMBase = aresult.records;
            if (aresult.records != null)
                for (int intCount = 0; intCount < aresult.records.Count(); intCount++)
                {

                    string duplicateLeadIdNumber = aresult.records[intCount].records[0].value;
                    result.Add(duplicateLeadIdNumber);
                }
        }
        result = result.Distinct().ToList();
        return result;
    }
    public List<string> FindCityIdInSageByCityName(string cityName, WebService binding)
    {
        List<string> result = new List<string>();
        WebService CRMService = binding;
        queryrecordresult aresult = binding.queryrecord(@"city_recordid, city_regionid", "city_city='" + cityName + "'", "city", "");
        ewarebase[] CRMBase = aresult.records;
        if (aresult.records != null)
            for (int intCount = 0; intCount < aresult.records.Count(); intCount++)
            {
                string cityId = aresult.records[intCount].records[0].value;
                string regionId = aresult.records[intCount].records[1].value;
                result.Add(cityId);
                result.Add(regionId);
            }
        result = result.Distinct().ToList();
        return result;
    }
    //Not in use
    public string FindRegionIdByCityId(string cityName, WebService binding)
    {
        List<string> result = new List<string>();
        WebService CRMService = binding;
        queryrecordresult aresult = binding.queryrecord(@"city_regionid", "city_city='" + cityName + "'", "city", "");
        ewarebase[] CRMBase = aresult.records;
        if (aresult.records != null)
            for (int intCount = 0; intCount < aresult.records.Count(); intCount++)
            {
                string duplicateLeadIdNumber = aresult.records[intCount].records[0].value;
                result.Add(duplicateLeadIdNumber);
            }
        result = result.Distinct().ToList();
        return result[0];
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Logger.Tracer.WriteMemberEntry();

        try
        {
            if (Session["DatasetLeads"] != null)
            {
                DataSet ds = (DataSet)Session["DatasetLeads"];
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    WebService ws = GetSageSession();
                    List<lead> leadsList = CreateLeadObject(ds, ws);
                    AddLeadsToSage(leadsList, ws);

                    lblResult.ForeColor = System.Drawing.Color.Green;
                    lblResult.Text = "Leads have been added to work list";
                    gvData.DataSource = null;
                    gvData.DataBind();
                    btnSubmit.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Current.WriteException(ex);
            lblResult.Text = ex.Message;
            throw;
        }
        Logger.Tracer.WriteMemberExit();
    }
    protected void gvData_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
    {
        e.Column.ItemStyle.BorderStyle = BorderStyle.Solid;
        e.Column.ItemStyle.BorderColor = Color.LightGray;
        e.Column.ItemStyle.BorderWidth = Unit.Pixel(1);

        if (e.Column.UniqueName=="ID Number")
        {
            e.Column.OrderIndex = 1;
        }
        //else if (e.Column.IsBoundToFieldName("ProductName"))
        //{
        //    e.Column.ItemStyle.CssClass = "MyClass2";
        //}
    }
    protected void gvData_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        //Is it a GridDataItem
        if (e.Item is GridDataItem)
        {
            //Get the instance of the right type
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            //Check the formatting condition
            if (string.IsNullOrWhiteSpace(dataBoundItem["ID Number"].Text) || dataBoundItem["ID Number"].Text == "&nbsp;")
                dataBoundItem["ID Number"].BackColor = Color.Red;
            else if (dataBoundItem["ID Number"].Text.Length < 10)
            {
                dataBoundItem["ID Number"].ForeColor = Color.OrangeRed;
            }
            else
            {
                if (Session["LeadIdNumbers"] != null)
                {
                    List<string> existingLeadIdNumbers = (List<string>)Session["LeadIdNumbers"];
                    if (existingLeadIdNumbers.Exists(x => x.Equals(dataBoundItem["ID Number"].Text)))
                    {
                        dataBoundItem["ID Number"].BackColor = Color.Yellow;

                        SetFocus(dataBoundItem["ID Number"]);
                        btnSubmit.Visible = false;
                    }
                }
            }
            if (string.IsNullOrWhiteSpace(dataBoundItem["First name"].Text) || dataBoundItem["First name"].Text == "&nbsp;")
                dataBoundItem["First name"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["Arabic First Name"].Text) || dataBoundItem["Arabic First Name"].Text == "&nbsp;")
                dataBoundItem["Arabic First Name"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["Last name"].Text) || dataBoundItem["Last name"].Text == "&nbsp;")
                dataBoundItem["Last name"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["Arabic Last Name"].Text) || dataBoundItem["Arabic Last Name"].Text == "&nbsp;")
                dataBoundItem["Arabic Last Name"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["City"].Text) || dataBoundItem["City"].Text == "&nbsp;")
                dataBoundItem["City"].BackColor = Color.Red;
            //if (string.IsNullOrWhiteSpace(dataBoundItem["Region"].Text) || dataBoundItem["Region"].Text == "&nbsp;")
            //    dataBoundItem["Region"].BackColor = Color.Red;
            //if (string.IsNullOrWhiteSpace(dataBoundItem["Mobile country Code"].Text) || dataBoundItem["Mobile country Code"].Text == "&nbsp;")
            //    dataBoundItem["Mobile country Code"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["Mobile Number"].Text) || dataBoundItem["Mobile Number"].Text == "&nbsp;")
                dataBoundItem["Mobile Number"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["E-mail"].Text) || dataBoundItem["E-mail"].Text == "&nbsp;")
                dataBoundItem["E-Mail"].BackColor = Color.Red;
            else if (!dataBoundItem["E-mail"].Text.Contains('@'))
            {
                dataBoundItem["E-Mail"].ForeColor = Color.OrangeRed;
            }
            //if (string.IsNullOrWhiteSpace(dataBoundItem["Product"].Text) || dataBoundItem["Product"].Text == "&nbsp;")
            //    dataBoundItem["Product"].BackColor = Color.Red;
            //if (string.IsNullOrWhiteSpace(dataBoundItem["Source"].Text) || dataBoundItem["Source"].Text == "&nbsp;")
            //    dataBoundItem["Source"].BackColor = Color.Red;
            //if (string.IsNullOrWhiteSpace(dataBoundItem["Country"].Text) || dataBoundItem["Country"].Text == "&nbsp;")
            //    dataBoundItem["Country"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["Number of Family Member"].Text) || dataBoundItem["Number of Family Member"].Text == "&nbsp;")
                dataBoundItem["Number of Family Member"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["Work Sector"].Text) || dataBoundItem["Work Sector"].Text == "&nbsp;")
                dataBoundItem["Work Sector"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["Salary Bracket"].Text) || dataBoundItem["Salary Bracket"].Text == "&nbsp;")
                dataBoundItem["Salary Bracket"].BackColor = Color.Red;
            if (string.IsNullOrWhiteSpace(dataBoundItem["Debt Burden Ratio"].Text) || dataBoundItem["Debt Burden Ratio"].Text == "&nbsp;")
                dataBoundItem["Debt Burden Ratio"].BackColor = Color.Red;
        }
    }
}