﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class broker_Default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
        {
            Response.Redirect("../default.aspx");
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["BrokerNo"])))
        {
            lblClient.Text = "Welcome to Bupa portal for NCB";
            Notify.Visible = false;
        }

    }


}