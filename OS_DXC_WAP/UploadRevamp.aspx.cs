using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bupa.OSWeb.Helper;

using log4net;
using Tracer = Utility.Diagnostic.FileTrace;
using Utility.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.Configuration;

public partial class UploadRevamp : System.Web.UI.Page
{
    private ILog _log = LogManager.GetLogger(typeof(UploadRevamp));
    private string[] supportDocs;
    private string allowedExtent, unparsedFileSizeLimit, unparsedFileSetSizeLimit, unparsedIsBatch;
    private Hashtable hasQueryValue;
    public string UploadCategory;
    public string RequestID;
    public string isresub;
    public int SEQ;
    public string t;
    public string BatchUpload;
    public string UserID;
    protected void Page_Load(object sender, EventArgs e)
    {

        Tracer.WriteMemberEntry(ref _log);
        try
        {
            lblMessage.Text = string.Empty;
            try
            {
                
                    //Session Timeout Warning Dialog
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Session["Reset"] = true;
                    Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                    SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                    int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionTimeoutAlert(" + timeout + ");", true);
                
                CheckSessionExpired();
                if (ConfigurationManager.AppSettings["isClientConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
                {
                    CheckConcurrentUserStatus();
                }
                if (ConfigurationManager.AppSettings["isProviderConcurrentLogonEnabled"].ToString().ToUpper() == "Y")
                {
                    CheckConcurrentUserStatus();
                }
            }
            catch
            {

            }

            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginInfo"])))
            {
                Response.Redirect("default.aspx", true);
            }
            else
            {

                hasQueryValue = new Hashtable();
                string queryStringValues = string.Empty;
                if (Request.QueryString.Count > 0)
                {
                    try
                    {
                        var val = Request.QueryString["val"];
                        queryStringValues = Cryption.Decrypt(val);
                        hasQueryValue = Cryption.GetQueryValue(queryStringValues);
                    }
                    catch (Exception ex)
                    {
                        Tracer.WriteException(ref _log, ex);
                        lblMessage.Text = ex.Message + ex.InnerException;
                        return;
                    }

                }


                if (hasQueryValue != null && hasQueryValue.Count > 0)
                {
                    UploadCategory = hasQueryValue.ContainsKey("UploadCategory") ? Convert.ToString(hasQueryValue["UploadCategory"]) : string.Empty;
                    RequestID = hasQueryValue.ContainsKey("RequestID") ? Convert.ToString(hasQueryValue["RequestID"]) : string.Empty;
                    isresub = hasQueryValue.ContainsKey("isresub") ? Convert.ToString(hasQueryValue["isresub"]) : string.Empty;
                    SEQ = hasQueryValue.ContainsKey("seq") ? Convert.ToInt32(hasQueryValue["seq"]) : 0;
                    t = hasQueryValue.ContainsKey("t") ? Convert.ToString(hasQueryValue["t"]) : string.Empty;
                    BatchUpload = hasQueryValue.ContainsKey("BatchUpload") ? Convert.ToString(hasQueryValue["BatchUpload"]) : string.Empty;
                    UserID = hasQueryValue.ContainsKey("UserID") ? Convert.ToString(hasQueryValue["UserID"]) : string.Empty;
                }

                Session["hashQuery"] = hasQueryValue;



                if (UploadCategory == "Submit Reimbursement" || UploadCategory == "Submit eClaims")
                {
                    btnSend.Visible = true;
                    lbtnClose.Visible = false;
                    allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensionseClaims);
                    unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSizeeClaims);
                    unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSizeeClaims);
                    unparsedIsBatch = "true";
                }
                else
                {
                    btnSend.Visible = false;
                    lbtnClose.Visible = true;
                    allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
                    unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
                    unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSize);
                    unparsedIsBatch = Convert.ToString(BatchUpload);
                }

                DataBind();
                if (unparsedIsBatch.ToLower() == "true")
                {
                    // Set the limit to be different for batch uploads
                    if (UploadCategory == "Submit Reimbursement" || UploadCategory == "Submit eClaims")
                    {
                        unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_BatchMaxFileSetByteSizeeClaims);
                    }
                    else
                    {
                        unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_BatchMaxFileSetByteSize);
                    }

                }
                Tracer.WriteLine(ref _log, "unparsedFileSetSizeLimit is: " + unparsedFileSetSizeLimit);
                long fileByteLimit = long.Parse(unparsedFileSizeLimit);
                long fileSetByteLimit = long.Parse(unparsedFileSetSizeLimit);


                lblUploadExtension.Text = "You may upload related documents here (allowed types are: " + allowedExtent + "). Total complete size allowed for all files uploaded is " + unparsedFileSetSizeLimit + " .";

                uploader.AllowedExtensions = allowedExtent;
                uploader.MaxFileSize = fileByteLimit;
                uploader.MaxFileSetSize = fileSetByteLimit;
            }



            /*if (string.IsNullOrEmpty(Convert.ToString(Session["LoginInfo"])))
			{
				Response.Redirect("default.aspx", true);
			}
			else
			{
				if (Request["UploadCategory"] == "Submit Reimbursement" || Request["UploadCategory"] == "Submit eClaims")
				{
					btnSend.Visible = true;
					lbtnClose.Visible = false;
					allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensionseClaims);
					unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSizeeClaims);
					unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSizeeClaims);
					unparsedIsBatch = "true";
				}
				else
				{
					btnSend.Visible = false;
					lbtnClose.Visible = true;
					allowedExtent = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_UploadAllowedExtensions);
					unparsedFileSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MEMBERSHIP_MaxFileUploadSize);
					unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_MaxFileSetByteSize);
					unparsedIsBatch = Convert.ToString(Request.QueryString[UploadPublication.QSKey_BatchUpload]);
				}
				
				DataBind();
				if (unparsedIsBatch.ToLower() == "true")
				{
					// Set the limit to be different for batch uploads
					if (Request["UploadCategory"] == "Submit Reimbursement" || Request["UploadCategory"] == "Submit eClaims")
					{
						unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_BatchMaxFileSetByteSizeeClaims);
					}
					else
					{
						unparsedFileSetSizeLimit = CoreConfiguration.Instance.GetConfigValue(UploadPublication.Key_BatchMaxFileSetByteSize);
					}
					
				}
				Tracer.WriteLine(ref _log, "unparsedFileSetSizeLimit is: " + unparsedFileSetSizeLimit);
				long fileByteLimit = long.Parse(unparsedFileSizeLimit);
				long fileSetByteLimit = long.Parse(unparsedFileSetSizeLimit);

				
				lblUploadExtension.Text = "You may upload related documents here (allowed types are: " + allowedExtent + "). Total complete size allowed for all files uploaded is " + unparsedFileSetSizeLimit + " .";

				uploader.AllowedExtensions = allowedExtent;
				uploader.MaxFileSize = fileByteLimit;
				uploader.MaxFileSetSize = fileSetByteLimit;
			}*/

        }
        catch (Exception ex)
        {
            Tracer.WriteException(ref _log, ex);
        }
        Tracer.WriteMemberExit(ref _log);
    }
    protected void lbtnClose_Click(object sender, EventArgs e)
    {


        // Get the list of attachments for this user

        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(RequestID, UploadCategory);
        DataSet ds = (DataSet)Session["DataSet"];

        int _count = 0;
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            supportDocs = new string[d.Tables[0].Rows.Count];
            foreach (DataRow r in d.Tables[0].Rows)
            {

                string attachPath = r["VirtualPath"].ToString();
                supportDocs[_count] = attachPath;
                _count++;
            }
            int _ctn = 1;
            if (isresub == "yes")
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    if (dr["SEQ"].ToString() == Convert.ToString(SEQ))
                    {
                        dr["SupportDocs"] = "";
                        for (int i = 0; i < supportDocs.Length; i++)
                        {
                            if (i == 0)
                            {
                                dr["SupportDocs"] = supportDocs[i].Replace("~/Uploads/", @"\");
                            }
                            else
                            {
                                dr["SupportDocs"] = dr["SupportDocs"] + ";" + supportDocs[i].Replace("~/Uploads/", @"\");
                            }

                        }
                    }
                }
            }
            else
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr["SEQ"].ToString() == Convert.ToString(SEQ + 1))
                    {
                        dr["SupportDocs"] = "";
                        for (int i = 0; i < supportDocs.Length; i++)
                        {
                            if (i == 0)
                            {
                                dr["SupportDocs"] = supportDocs[i].Replace("~/Uploads/", @"\");
                            }
                            else
                            {
                                dr["SupportDocs"] = dr["SupportDocs"] + ";" + supportDocs[i].Replace("~/Uploads/", @"\");
                            }

                        }
                    }
                }
            }

            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.opener.Refresh(); window.close()", true);
        }
        else
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["SEQ"].ToString() == Convert.ToString(SEQ + 1))
                {

                    dr["SupportDocs"] = "";
                }
            }
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.opener.Refresh(); window.close()", true);
            return;
        }




        // Get the list of attachments for this user
        /*  Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
          DataSet d = uploadManager.GetUploadedFileSet(Request["RequestID"], Request["UploadCategory"]);
          DataSet ds = (DataSet)Session["DataSet"];

          int _count = 0;
          if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
          {
              supportDocs = new string[d.Tables[0].Rows.Count];
              foreach (DataRow r in d.Tables[0].Rows)
              {

                  string attachPath = r["VirtualPath"].ToString();
                  supportDocs[_count] = attachPath;
                  _count++;
              }
              int _ctn = 1;
              if (Request["isresub"] == "yes")
              {
                  foreach (DataRow dr in ds.Tables[0].Rows)
                  {

                      if (dr["SEQ"].ToString() == Convert.ToString(Convert.ToInt32(Request["SEQ"])))
                      {
                          dr["SupportDocs"] = "";
                          for (int i = 0; i < supportDocs.Length; i++)
                          {
                              if (i == 0)
                              {
                                  dr["SupportDocs"] = supportDocs[i].Replace("~/Uploads/", @"\");
                              }
                              else
                              {
                                  dr["SupportDocs"] = dr["SupportDocs"] + ";" + supportDocs[i].Replace("~/Uploads/", @"\");
                              }

                          }
                      }
                  }
              }
              else
              {
                  foreach (DataRow dr in ds.Tables[0].Rows)
                  {
                      if (dr["SEQ"].ToString() == Convert.ToString(Convert.ToInt32(Request["SEQ"]) + 1))
                      {
                          dr["SupportDocs"] = "";
                          for (int i = 0; i < supportDocs.Length; i++)
                          {
                              if (i == 0)
                              {
                                  dr["SupportDocs"] = supportDocs[i].Replace("~/Uploads/", @"\");
                              }
                              else
                              {
                                  dr["SupportDocs"] = dr["SupportDocs"] + ";" + supportDocs[i].Replace("~/Uploads/", @"\");
                              }

                          }
                      }
                  }
              }

              this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.opener.Refresh(); window.close()", true);
          }
          else
          {
              foreach (DataRow dr in ds.Tables[0].Rows)
              {
                  if (dr["SEQ"].ToString() == Convert.ToString(Convert.ToInt32(Request["SEQ"]) + 1))
                  {

                          dr["SupportDocs"] = "";          
                  }
              }
              this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.opener.Refresh(); window.close()", true);
              return;
          }
          */



    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        string fromEmail = "donotreply@bupa.com.sa";
        string name = "";
        string code = "";
        if (!string.IsNullOrEmpty(t))
        {
            if (t == "mem")
            {
                name = Convert.ToString(Session["MemberName"]);
                code = Convert.ToString(Session["MembershipNo"]);
            }
            else
            {
                name = Convert.ToString(Session["ProviderName"]);
                code = Convert.ToString(Session["ProviderID"]);

            }
        }
        else
        {
            name = Convert.ToString(Session["ClientName"]);
            code = Convert.ToString(Session["ClientID"]);
        }
        string subject = " Attachments added by " + name + " (" + code + ")";
        string content = "Please view the attached uploaded documents added by " + name + " (" + code + ") ";

        // Get the list of attachments for this user
        Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
        DataSet d = uploadManager.GetUploadedFileSet(RequestID, UploadCategory);

        StringCollection attachments = new StringCollection();
        if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow r in d.Tables[0].Rows)
            {
                string attachPath = r["VirtualPath"].ToString();
                attachments.Add(attachPath);
            }
        }
        else
        {
            // No attachments and therefore, nothing to email
            return;
        }

        // Get the addressees
        if (t != "pro")
        {
            SendEmail(fromEmail, "reimbursement@bupa.com.sa", subject, content, attachments);
        }
        else
        {
            SendEmaileClaims("CcuEclaims@bupa.com.sa", "ccu@bupa.com.sa", subject, content, attachments);
        }

        /* // Send an email informing the team about a pre-auth request
         string fromEmail = "donotreply@bupa.com.sa";
         string name = "";
         string code = "";
         if (!string.IsNullOrEmpty(Request["t"]))
         {
             if (Request["t"] == "mem")
             {
                 name = Convert.ToString(Session["MemberName"]);
                 code = Convert.ToString(Session["MembershipNo"]);
             }
             else
             {
                 name = Convert.ToString(Session["ProviderName"]);
                 code = Convert.ToString(Session["ProviderID"]);

             }
         }
         else 
         {
             name = Convert.ToString(Session["ClientName"]);
             code = Convert.ToString(Session["ClientID"]);
         }
         string subject = " Attachments added by " + name + " (" + code + ")";
         string content = "Please view the attached uploaded documents added by " + name + " (" + code + ") ";

         // Get the list of attachments for this user
         Bupa.OSWeb.Helper.UploadManager uploadManager = new Bupa.OSWeb.Helper.UploadManager(Int32.MinValue, Int32.MinValue);
         DataSet d = uploadManager.GetUploadedFileSet(Request["RequestID"], Request["UploadCategory"]);

         StringCollection attachments = new StringCollection();
         if (d != null && d.Tables.Count > 0 && d.Tables[0].Rows.Count > 0)
         {
             foreach (DataRow r in d.Tables[0].Rows)
             {
                 string attachPath = r["VirtualPath"].ToString();
                 attachments.Add(attachPath);
             }
         }
         else
         {
             // No attachments and therefore, nothing to email
             return;
         }

         // Get the addressees
         if (Request["t"] != "pro")
         {
             SendEmail(fromEmail, "reimbursement@bupa.com.sa", subject, content, attachments);
         }
         else
         {
             SendEmaileClaims("CcuEclaims@bupa.com.sa", "ccu@bupa.com.sa", subject, content, attachments);
         }*/

    }


    private void SendEmaileClaims(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {
        
        // Setup the mail message

        using (MailMessage mail = new MailMessage(new MailAddress(fromEmail, "Bupa Arabia CCU Online Services"), new MailAddress(toEmail, "")))
        {
            mail.Subject = subject;
            mail.Body = content;

            if (attachmentPaths != null)
            {

                foreach (string path in attachmentPaths)
                {
                    // Create the attachment
                    System.Net.Mail.Attachment attachment;
                    if (System.IO.File.Exists(Server.MapPath(path)))
                    {
                        attachment = new System.Net.Mail.Attachment(Server.MapPath(path));
                        // Add the attachment
                        mail.Attachments.Add(attachment);
                    }
                }
            }
            string smtpHost = WebPublication.EmailRelayAddress();
            int smtpPort = WebPublication.EmailRelayPort();

            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
            mail.IsBodyHtml = true;
            smtp.Send(mail);
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }


    }
    private void SendEmail(string fromEmail, string toEmail, string subject, string content, StringCollection attachmentPaths)
    {

        if (t == "pro")
        {
            fromEmail = "";
        }
        MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

        // Deal with the attachments
        if (attachmentPaths != null)
        {
            foreach (string path in attachmentPaths)
            {
                // Create the attachment
                System.Net.Mail.Attachment attachment;
                if (System.IO.File.Exists(Server.MapPath(path)))
                {
                    attachment = new System.Net.Mail.Attachment(Server.MapPath(path));
                    // Add the attachment
                    mail.Attachments.Add(attachment);
                }
            }
        }

        string smtpHost = WebPublication.EmailRelayAddress();
        int smtpPort = WebPublication.EmailRelayPort();

        SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
        mail.IsBodyHtml = true;
        smtp.Send(mail);
        this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);

        /*// Setup the mail message
        if (Request["t"] == "pro")
        {
            fromEmail = "";
        }
        MailMessage mail = new MailMessage(fromEmail, toEmail, subject, content);

        // Deal with the attachments
        if (attachmentPaths != null)
        {
            foreach (string path in attachmentPaths)
            {
                // Create the attachment
                System.Net.Mail.Attachment attachment;
                if (System.IO.File.Exists(Server.MapPath(path)))
                {
                    attachment = new System.Net.Mail.Attachment(Server.MapPath(path));
                    // Add the attachment
                    mail.Attachments.Add(attachment);
                }
            }
        }

        string smtpHost = WebPublication.EmailRelayAddress();
        int smtpPort = WebPublication.EmailRelayPort();

        SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
        mail.IsBodyHtml = true;
        smtp.Send(mail);
        this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        */
   

    }

    protected void aYes_Click(object sender, EventArgs e)
    {
        //  Response.Redirect(Request.RawUrl);

        // Session["SessionExpire"] = false;
    }
    private void CheckSessionExpired()
    {
        int vSessionExpiryCount = int.Parse(ConfigurationManager.AppSettings["SessionExpiryCount"].ToString());
        int vSessionInMilisecond = (Session.Timeout * 60000) - vSessionExpiryCount;

        ltExpierySessionMessage.Text = string.Format("Timeout", vSessionExpiryCount / 60000);
        //// string url = "~/Default.aspx?ErrMsg=1" + ((vLanguage == "") ? "" : "&Lang=" + vLanguage);

        string url = ConfigurationManager.AppSettings["SessionPath"];

        string scripttext = "SessionExpired(" + vSessionInMilisecond.ToString() + ");SessionExpiredEnded(" + Session.Timeout * 60000 + ",\"" + url + "\");";
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "SessionExpieredKey", scripttext, true);

    }
    private void CheckConcurrentUserStatus()
    {
        string status = string.Empty;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["ClientID"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["CurrentLogID"]), "C");
        }
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["pRefNo"])))
        {
            status = BLCommon.GetConcurrentUserLogOffStatus(Convert.ToInt32(Session["pRefNo"]), "P");
        }

        if (!string.IsNullOrWhiteSpace(status))
        {
            Response.Redirect("../default.aspx");
        }
    }

}