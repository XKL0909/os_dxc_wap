
using System;

partial class DateTimePicker : System.Web.UI.UserControl
{

    #region "Private and Default values"
    private string mCulture = "en-GB";
    private string mSelectedDateFormat = "dd/MM/yyyy";
    private int mYearMinimum = 1970;
    private int mYearMaximum = 2029;
    private bool mNullable = true;

    #endregion

    #region "Public Properties"

    public bool Nullable
    {
        get { return mNullable; }
        set { mNullable = value; }
    }

    public string Culture
    {
        get { return mCulture; }
        set { mCulture = value; }
    }

    public string SelectedDateFormat
    {
        get { return mSelectedDateFormat; }
        set { mSelectedDateFormat = value; }
    }

    public int YearMinimum
    {
        set { mYearMinimum = value; }
    }

    public int YearMaximum
    {
        set { mYearMaximum = value; }
    }

    public DateTime SelectedDate
    {
        get
        {
            if (!string.IsNullOrEmpty(DateTextBox.Text))
            {
                return DateTime.Parse(DateTextBox.Text, new System.Globalization.CultureInfo(mCulture));
            }
            else
            {
                return null;
            }
        }
        set { DateTextBox.Text = value.ToString(mSelectedDateFormat, new System.Globalization.CultureInfo(mCulture)); }
    }

    #endregion

    #region "private and protected methods"

    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        //Popup result is the selected date
        PopupControlExtender1.Commit(Calendar1.SelectedDate.ToString(mSelectedDateFormat, new System.Globalization.CultureInfo(mCulture)));
    }

    private void SelectCalendar()
    {
        if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
        {
            Calendar1.VisibleDate = DateTime.Parse(ddlMonth.SelectedValue + "/" + Day(DateTime.Now()) + "/" + ddlYear.SelectedValue);

        }
    }

    protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        SelectCalendar();
    }

    protected void ddlYear_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        SelectCalendar();
    }
    #endregion
}
