﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Oops" Codebehind="Oops.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Src="UserControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Header.ascx" TagName="Header" TagPrefix="uc2" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc2" TagName="Footer" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" type="text/css" href="styles/Login/bupa.css" />
    <link rel="stylesheet" type="text/css" href="styles/Login/home-slider.css" />
    <link rel="stylesheet" type="text/css" href="styles/Login/help-slider.css" />
    <link rel="stylesheet" type="text/css" href="Styles/lightblue.css" />
    <link href="Scripts/Bupa/MessageBar/CSS/skin.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="Styles/Impromptu.css" />
    <link rel="stylesheet" type="text/css" href="Styles/OSWebStyle.css" />
    <title>Something went wrong</title>

    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/jquery-1.7.2.min.js") %>"></script>
    <script type="text/javascript" src="functions/functions.js"></script>
    <style type="text/css">
        .auto-style1
        {
            color: #0066FF;
        }
    </style>

    <style type="text/css">
        div.botright
        {
            display: block;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 600px;
            height: 230px;
            padding: 25px 15px;
            z-index: 999999;
            margin-top: -9em; /*set to a negative number 1/2 of your height*/
            margin-left: -15em; /*set to a negative number 1/2 of your width*/
            /*give it some background and border*/
        }

        p.MsoNormal
        {
            margin-bottom: .0001pt;
            font-size: 16.0pt;
            font-family: "Calibri","sans-serif";
            margin-left: 0in;
            margin-right: 0in;
            margin-top: 0in;
        }
    </style>

</head>
<body id="bupaWebsite">
    <form runat="server" id="frmm">
        <div class="socialMediaIcons">
            <a target="_blank" href="https://www.facebook.com/BupaArabia" class="facebookIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Facebook" width="1" height="1"></a>
            <a target="_blank" href="https://twitter.com/bupaarabia" class="twitterIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Twitter" width="1" height="1"></a>
            <a target="_blank" href="http://www.youtube.com/bupaarabia" class="youtubeIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Youtube" width="1" height="1"></a>
            <a style="display: none" href="javascript:;" class="emailFriendIcon">
                <img src="/Style Library/images/spacer.gif" alt="Email Friend" title="Email Friend" width="1" height="1"></a>
            <a target="_blank" style="display: none" href="http://test.borninteractive.net:8081/wpbupa" class="blogIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Youtube" width="1" height="1"></a>
            <a href="http://instagram.com/bupaarabia" target="_blank" class="instagramIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="Instagram" width="1" height="1"></a>
            <a href="http://www.linkedin.com/company/bupa-arabia" target="_blank" class="linkedInIcon">
                <img src="/Style Library/images/spacer.gif" alt="" title="linkedIn" width="1" height="1"></a>
        </div>
        <uc2:Header ID="Header1" runat="server" />
        &nbsp;<div>
            <table width="995" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="centerBack" style="background: #E6E6E6 url(images/BackgroundImage.jpg) center 0 no-repeat;">
                        <div class="margBottom20">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="pageTitle">
                                                    <h1>bupa online services</h1>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="subWhiteCenter">
                            <div class="pageHeadRow">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <h1>An Error has Occurred! </h1>
                                <p>
                                    <strong>Something went wrong with the current request</strong>...
                                </p>
                                <p>
                                    We are sorry for inconvenience. Please come again and retry after sometime..
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="copyrightContainer">
            <uc2:Footer runat="server" ID="Footer" />

        </div>
        <script type="text/javascript" src="functions/Login/jquery.js"></script>
        <script type="text/javascript" src="functions/Login/jquery.anythingslider.min.js"></script>
        <script type="text/javascript" src="functions/Login/cufon.js"></script>
        <script type="text/javascript" src="functions/Login/Gotham_Book_400.font.js"></script>
        <script type="text/javascript" src="functions/Login/cufon.init.js"></script>
        <script type="text/javascript" src="functions/Login/functions.js"></script>
        <script type="text/javascript" src="Scripts/jquery.blockUI.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                toggleFooter();
            });

            $(function () {
                $('#slider,#Helpslider').anythingSlider();
            });
        </script>

        <script language="javascript" type="text/javascript">

            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(function () {
                $('#entryForm').block({ message: '<br/><img src="<%=Page.ResolveUrl("~/Images/progress_indicator.gif") %>" /><h1>Please wait ...</h1>' });
            });

            prm.add_endRequest(function () {
                $('#entryForm').unblock();
            });

            // Progress indicator preloading.
            var preload = document.createElement('img');
            preload.src = '<%=Page.ResolveUrl("~/Images/progress-indicator.gif") %>';
        delete preload;

        </script>

    </form>

</body>
</html>
