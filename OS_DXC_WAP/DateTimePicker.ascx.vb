
Partial Class DateTimePicker
    Inherits System.Web.UI.UserControl

#Region "Private and Default values"
    Private mCulture As String = "en-GB"
    Private mSelectedDateFormat As String = "dd/MM/yyyy"
    Private mYearMinimum As Integer = 1970
    Private mYearMaximum As Integer = 2029
    Private mNullable As Boolean = True

#End Region

#Region "Public Properties"

    Public Property Nullable() As Boolean
        Get
            Return mNullable
        End Get
        Set(ByVal value As Boolean)
            mNullable = value
        End Set
    End Property

    Public Property Culture() As String
        Get
            Return mCulture
        End Get
        Set(ByVal value As String)
            mCulture = value
        End Set
    End Property

    Public Property SelectedDateFormat() As String
        Get
            Return mSelectedDateFormat
        End Get
        Set(ByVal value As String)
            mSelectedDateFormat = value
        End Set
    End Property

    Public WriteOnly Property YearMinimum() As Integer
        Set(ByVal value As Integer)
            mYearMinimum = value
        End Set
    End Property

    Public WriteOnly Property YearMaximum() As Integer
        Set(ByVal value As Integer)
            mYearMaximum = value
        End Set
    End Property

    Public Property SelectedDate() As DateTime
        Get
            If DateTextBox.Text <> "" Then
                Return DateTime.Parse(DateTextBox.Text, New System.Globalization.CultureInfo(mCulture))
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DateTime)
            DateTextBox.Text = value.ToString(mSelectedDateFormat, New System.Globalization.CultureInfo(mCulture))
        End Set
    End Property

#End Region

#Region "private and protected methods"

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs)
        'Popup result is the selected date
        PopupControlExtender1.Commit(Calendar1.SelectedDate.ToString(mSelectedDateFormat, New System.Globalization.CultureInfo(mCulture)))
    End Sub

    Private Sub SelectCalendar()
        If ddlYear.SelectedValue <> "" Then
            Calendar1.VisibleDate = DateTime.Parse(ddlMonth.SelectedValue & "/" & Day(DateTime.Now()) & "/" & ddlYear.SelectedValue)
        End If

    End Sub

    Protected Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SelectCalendar()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SelectCalendar()
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            Dim mIndex As Integer
            For mIndex = mYearMinimum To mYearMaximum
                ddlYear.Items.Add(New ListItem(mIndex.ToString(), mIndex.ToString()))
            Next
            ddlYear.SelectedValue = Year(DateTime.Now())
            ddlMonth.SelectedValue = Month(DateTime.Now())
            lbtnClearDate.Visible = mNullable
        End If
    End Sub

    Protected Sub lbtnClearDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnClearDate.Click
        DateTextBox.Text = ""
        PopupControlExtender1.Commit("")
    End Sub

    Protected Sub lbtnToday_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnToday.Click
        ddlYear.SelectedValue = Year(DateTime.Now())
        ddlMonth.SelectedValue = Month(DateTime.Now())
        SelectCalendar()
        Calendar1.SelectedDate = DateTime.Now()
        PopupControlExtender1.Commit(Calendar1.SelectedDate.ToString(mSelectedDateFormat, New System.Globalization.CultureInfo(mCulture)))
    End Sub

#End Region
End Class
