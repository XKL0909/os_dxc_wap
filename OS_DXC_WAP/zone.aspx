﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_zone" Codebehind="zone.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>How to use WebPartZone and ZoneTemplate in asp.net</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h2 style="color:Red">WebPartZone and ZoneTemplate<asp:WebPartManager 
                ID="WebPartManager1" runat="server">
            </asp:WebPartManager>
        </h2>
        <asp:WebPartZone ID="WebPartZone1" runat="server" BorderColor="#CCCCCC" 
            Font-Names="Verdana" Padding="6">
            <ZoneTemplate>
                <asp:Image 
                     ID="Image1" 
                     runat="server" 
                     ImageUrl="~/images/dolphin.jpg" 
                     Title="Dolphin"
                     />
            </ZoneTemplate>
            <MenuLabelHoverStyle ForeColor="#E2DED6" />
            <MenuLabelStyle ForeColor="White" />
            <MenuPopupStyle BackColor="#5D7B9D" BorderColor="#CCCCCC" BorderWidth="1px" 
                Font-Names="Verdana" Font-Size="0.6em" />
            <MenuVerbHoverStyle BackColor="#F7F6F3" BorderColor="#CCCCCC" 
                BorderStyle="Solid" BorderWidth="1px" ForeColor="#333333" />
            <MenuVerbStyle BorderColor="#5D7B9D" BorderStyle="Solid" BorderWidth="1px" 
                ForeColor="White" />
            <TitleBarVerbStyle Font-Size="0.6em" Font-Underline="False" ForeColor="White" />
            <EmptyZoneTextStyle Font-Size="0.8em" />
            <HeaderStyle Font-Size="0.7em" ForeColor="#CCCCCC" HorizontalAlign="Center" />
            <PartChromeStyle BackColor="#F7F6F3" BorderColor="#E2DED6" Font-Names="Verdana" 
                ForeColor="White" />
            <PartStyle Font-Size="0.8em" ForeColor="#333333" />
            <PartTitleStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="0.8em" 
                ForeColor="White" />
        </asp:WebPartZone>
    </div>
    </form>
</body>
</html>