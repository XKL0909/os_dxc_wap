From: "Saved by Windows Internet Explorer 10"
Subject: 
Date: Sat, 23 Mar 2013 01:08:40 +0300
MIME-Version: 1.0
Content-Type: text/html;
	charset="utf-8"
Content-Transfer-Encoding: quoted-printable
Content-Location: https://raw.github.com/wiggin/jQuery-Easy-Confirm-Dialog-plugin/master/jquery.easy-confirm-dialog.js
X-MimeOLE: Produced By Microsoft MimeOLE V6.2.9200.16384

=EF=BB=BF<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!DOCTYPE html PUBLIC "" ""><HTML><HEAD><META content=3D"IE=3D10.000"=20
http-equiv=3D"X-UA-Compatible">

<META http-equiv=3D"Content-Type" content=3D"text/html; =
charset=3Dutf-8">
<META name=3D"GENERATOR" content=3D"MSHTML 10.00.9200.16525"></HEAD>
<BODY>
<PRE>/**=0A=
 * jQuery Easy Confirm Dialog plugin 1.2=0A=
 *=0A=
 * Copyright (c) 2010 Emil Janitzek (http://projectshadowlight.org)=0A=
 * Based on Confirm 1.3 by Nadia Alramli (http://nadiana.com/)=0A=
 *=0A=
 * Samples and instructions at: =0A=
 * http://projectshadowlight.org/jquery-easy-confirm-dialog/=0A=
 *=0A=
 * This script is free software: you can redistribute it and/or modify =
it =0A=
 * under the terms of the GNU General Public License as published by the =
Free =0A=
 * Software Foundation, either version 3 of the License, or (at your =
option)=0A=
 * any later version.=0A=
 */=0A=
(function($) {=0A=
    $.easyconfirm =3D {};=0A=
    $.easyconfirm.locales =3D {};=0A=
    $.easyconfirm.locales.enUS =3D {=0A=
        title: 'Are you sure?',=0A=
        text: 'Are you sure that you want to perform this action?',=0A=
        button: ['Cancel', 'Confirm'],=0A=
        closeText: 'close'=0A=
    };=0A=
    $.easyconfirm.locales.svSE =3D {=0A=
        title: '=C3=84r du s=C3=A4ker?',=0A=
        text: '=C3=84r du s=C3=A4ker p=C3=A5 att du vill genomf=C3=B6ra =
denna =C3=A5tg=C3=A4rden?',=0A=
        button: ['Avbryt', 'Bekr=C3=A4fta'],=0A=
        closeText: 'st=C3=A4ng'=0A=
    };=0A=
    $.easyconfirm.locales.itIT =3D {=0A=
        title: 'Sei sicuro?',=0A=
        text: 'Sei sicuro di voler compiere questa azione?',=0A=
        button: ['Annulla', 'Conferma'],=0A=
        closeText: 'chiudi'=0A=
    };=0A=
=0A=
    $.fn.easyconfirm =3D function(options) {=0A=
=0A=
        var _attr =3D $.fn.attr;=0A=
=0A=
        $.fn.attr =3D function(attr, value) {=0A=
            // Let the original attr() do its work.=0A=
            var returned =3D _attr.apply(this, arguments);=0A=
=0A=
            // Fix for jQuery 1.6+=0A=
            if (attr =3D=3D 'title' &amp;&amp; returned =3D=3D=3D =
undefined) =0A=
                returned =3D '';=0A=
=0A=
            return returned;=0A=
        };=0A=
=0A=
        var options =3D jQuery.extend({=0A=
            eventType: 'click',=0A=
            icon: 'help'=0A=
        }, options);=0A=
=0A=
        var locale =3D jQuery.extend({}, $.easyconfirm.locales.enUS, =
options.locale);=0A=
=0A=
        // Shortcut to eventType.=0A=
        var type =3D options.eventType;=0A=
=0A=
        return this.each(function() {=0A=
            var target =3D this;=0A=
            var $target =3D jQuery(target);=0A=
=0A=
            // If no events present then and if there is a valid url, =
then trigger url change=0A=
            var urlClick =3D function() {=0A=
                    if (target.href) {=0A=
                        var length =3D String(target.href).length;=0A=
                        if (target.href.substring(length - 1, length) =
!=3D '#') =0A=
                            document.location =3D target.href;=0A=
                    }=0A=
                };=0A=
=0A=
            // If any handlers where bind before triggering, lets save =
them and add them later=0A=
            var saveHandlers =3D function() {=0A=
                    var events =3D jQuery.data(target, 'events');=0A=
                    if (events) {=0A=
                        target._handlers =3D new Array();=0A=
                        $.each(events[type], function(){=0A=
                            target._handlers.push(this);=0A=
                        });=0A=
=0A=
                        $target.unbind(type);=0A=
                    }=0A=
                };=0A=
            // Re-bind old events=0A=
            var rebindHandlers =3D function() {=0A=
                    if (target._handlers !=3D=3D undefined) {=0A=
                        jQuery.each(target._handlers, function() {=0A=
                            $target.bind(type, this);=0A=
                        });=0A=
                    }=0A=
                };=0A=
=0A=
            if ($target.attr('title') !=3D=3D null &amp;&amp; =
$target.attr('title').length &gt; 0) =0A=
                locale.text =3D $target.attr('title');=0A=
=0A=
            var dialog =3D (options.dialog =3D=3D=3D undefined || =
typeof(options.dialog) !=3D 'object') ? =0A=
                $('&lt;div class=3D"dialog confirm"&gt;' + locale.text + =
'&lt;/div&gt;') : =0A=
                options.dialog;=0A=
=0A=
            var buttons =3D {};=0A=
            buttons[locale.button[1]] =3D function() {=0A=
                // Unbind overriding handler and let default actions =
pass through=0A=
                $target.unbind(type, handler);=0A=
=0A=
                // Close dialog=0A=
                $(dialog).dialog("close");=0A=
=0A=
                // Check if there is any events on the target=0A=
                if (jQuery.data(target, 'events')) {=0A=
                    // Trigger click event.=0A=
                    $target.click();=0A=
                }=0A=
                else {=0A=
                    // No event trigger new url=0A=
                    urlClick();=0A=
                }=0A=
=0A=
                init();=0A=
=0A=
            };=0A=
            buttons[locale.button[0]] =3D function() {=0A=
                $(dialog).dialog("close");=0A=
            };=0A=
=0A=
            $(dialog).dialog({=0A=
                autoOpen: false,=0A=
                resizable: false,=0A=
                draggable: true,=0A=
                closeOnEscape: true,=0A=
                width: 'auto',=0A=
                minHeight: 120,=0A=
                maxHeight: 200,=0A=
                buttons: buttons,=0A=
                title: locale.title,=0A=
                closeText: locale.closeText,=0A=
                modal: true=0A=
            });=0A=
=0A=
            // Handler that will override all other actions=0A=
            var handler =3D function(event) {=0A=
                    $(dialog).dialog('open');=0A=
                    event.stopImmediatePropagation();=0A=
                    event.preventDefault();=0A=
                    return false;=0A=
                };=0A=
=0A=
            var init =3D function() {=0A=
                    saveHandlers();=0A=
                    $target.bind(type, handler);=0A=
                    rebindHandlers();=0A=
                };=0A=
=0A=
            init();=0A=
=0A=
        });=0A=
=0A=
    };=0A=
})(jQuery);</PRE></BODY></HTML>
